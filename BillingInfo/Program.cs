﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.sap.Interface.Service;
using com.pttict.sap.Interface.Model;
using com.pttict.sap.Interface.sap.billinginfo.send;
using ProjectCPAIEngine.DAL.Entity;
using Newtonsoft.Json.Linq;
using com.pttict.engine.utility;

namespace BillingInfo
{
    public class Program
    {
        static void Main(string[] args)
        {
            BillingInfoSendServiceConnectorImpl BIService = new BillingInfoSendServiceConnectorImpl();
            BillingInfoService service = new BillingInfoService();
            ConfigManagement configManagement = new ConfigManagement();
            string jsonConfig = configManagement.getDownstreamConfig("CIP_BILLINGINFO_SEND");

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.GLOBAL_CONFIG
                             where v.GCG_KEY.ToUpper() == "CIP_BILLING_INFO"
                             select new
                             {
                                 dValue = v.GCG_VALUE
                             });
                if (query != null)
                {
                    if (query.ToList().Count() > 0)
                    {
                        string jsonBillingInfo = query.ToList()[0].dValue;

                        BIService.connect(jsonConfig, jsonBillingInfo);
                    }
                    else
                    {
                        Console.WriteLine("Cannot read Global Config \"CIP_BILLING_INFO\"");
                    }
                }
                else
                {
                    Console.WriteLine("Cannot read Global Config \"CIP_BILLING_INFO\"");
                }
            }
        }
    }
}