﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HtmlCapture
{
    public class HTMLToImage
    {

        static string pageWithTable;
        int sizeHeight = 0;int sizeWidth = 0;
        int sizA4Width = 800;
        public HTMLToImage()
        {
           
        }

        string url = "";string outputFolder = "";int return_val = 0;
        string return_Path = "";int threadCounter = 0;
        public string GetWebpage(string _url, string _outputFolder)
        {
           int tmpsizeHeight = 700; int tmpsizeWidth = 800;
            return GetWebpage(_url, _outputFolder, tmpsizeHeight, tmpsizeWidth);
        }
        public string  GetWebpage(string _url,string _outputFolder,int _sizeHeight,int _sizeWidth)
        {
            sizeHeight = _sizeHeight;
            sizeWidth = _sizeWidth;
            url = _url;
            outputFolder = _outputFolder;
            threadCounter += 1;
            Thread thread = new Thread(new ThreadStart(GenerateScreenshot));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            
            while (threadCounter > 0)
            {
                Thread.Sleep(500); //Make it pause for half second so that we don’t spin the cpu out of control.
            }
            thread.Abort();
            return return_Path;
        }
        public void GenerateScreenshot()
        {


            //openDebug();
            List<Bitmap> imageList = new List<Bitmap>();
            
            WebBrowser wb = new WebBrowser();
            wb.Visible = true;
            wb.ScrollBarsEnabled = false;
            wb.ScriptErrorsSuppressed = true;



            wb.Navigate(url);            
            int count = 0;
            while (wb.ReadyState != WebBrowserReadyState.Complete /*&& count < 1000*/)
            {
                Application.DoEvents();
                count++;
            }
            wb.Width= sizeWidth;
            wb.Height = sizeHeight;

            Bitmap bitmap = new Bitmap(sizeWidth, sizeHeight);
            wb.Focus();
            Thread.Sleep(1000);
            NativeMethods.GetImage(wb.Document.DomDocument, bitmap, Color.White);

            string outputFilePath = outputFolder + DateTime.Now.ToString("yyyymmddhhmmssfff")+".png";
            bitmap.Save(outputFilePath, ImageFormat.Png);

            string outputFilePathresize = outputFolder + DateTime.Now.ToString("yyyymmddhhmmssfff") + ".png";
            Image Image = Image.FromFile(outputFilePath);
            Image NewImage = ResizeImageFixedWidth(Image, sizA4Width);
            NewImage.Save(outputFilePathresize, ImageFormat.Png);            
            return_Path = outputFilePathresize;
            #region no used code
            /*
            int A4Width = 512;
            int A4Height = 800;               

            **/
            #endregion
            wb.Dispose();
            wb = null;
            threadCounter = 0;
        }

        public static Image ResizeImageFixedWidth(Image imgToResize, int width)
        {
            
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = ((float)width / (float)sourceWidth);

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        public void Window_Error(object sender, HtmlElementErrorEventArgs e)
        {
            e.Handled = true;
        }

        public string getJSVariable(WebBrowser wb, string variableName)
        {
            //debug("===== getJS =====");
            object[] codeString = { String.Format("{0};", variableName) };
            string output = "";
            if (wb.Document != null)
            {
                object obj = wb.Document.InvokeScript("eval", codeString);
                if (obj != null)
                {
                    output = obj.ToString();
                    //debug(variableName+" found , value=");
                }
            }
            if (output == null)
            {
                //debug("===== exit =====");
                return "null";
            }
            else
            {
                //debug("===== exit =====");
                return (output=="")?"0":output;
            }
        }

        public Bitmap CropBitmap(Bitmap bitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {

            Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
            Bitmap cropped = bitmap.Clone(rect, bitmap.PixelFormat);
            return cropped;
        }

        public int _loadAndSave(string url, string outputFolder)
        {
            GetWebpage(url, outputFolder);
            int imagesCount = return_val;
            return imagesCount;

        }

        public int loadAndSave(string url, string outputFolder)
        {

            int imageCount = 0;
            var t = new Thread(delegate ()
            {
                imageCount = _loadAndSave(url, outputFolder);
            });

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            //closeDebug();
            return imageCount;

        }
    }
}
