﻿using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HtmlCapture
{
    public class URLToPDF
    {
        public void GenerateURLToPDF(string URL)
        {
            HTMLToImage _html = new HTMLToImage();
            string PathImage = _html.GetWebpage(URL, AppDomain.CurrentDomain.BaseDirectory);
            GenImageToPDF(PathImage);
        }

        public void GenImageToPDF(string ImagePath)
        {
            string destinaton = ImagePath.Replace("png", "pdf");
            PdfDocument doc = new PdfDocument();
            PdfPage pdfPage = new PdfPage();
            pdfPage.Orientation = PageOrientation.Portrait;

           

            doc.Pages.Add(pdfPage);

            XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
            XImage img = XImage.FromFile(ImagePath);

            try
            {
                xgr.DrawImage(img, 0, 0);
                doc.Save(destinaton);
                doc.Close();
            }
            catch (Exception ex)
            {
                destinaton = "";
            }
            
        }


    }
}
