﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoRecord.Model
{
    public class ResultFromMtSapMemoMapping
    {
        public string MetNum { get; set; }
        public string CostName { get; set; }
        public string PlanningType { get; set; }
        public string PlanningGroup { get; set; }
    }
}
