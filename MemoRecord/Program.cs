﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.sap.Interface.Service;
using com.pttict.sap.Interface.Model;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.sap.po.change;
using System.Web;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;
using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;
using DSMail.model;
using DSMail.service;
using com.pttict.engine.downstream;
using com.pttict.engine.utility;
using com.pttict.engine.model;
using com.pttict.downstream.common.utilities;
using MemoRecord.Model;
using Newtonsoft.Json.Linq;
using System.IO;

namespace MemoRecord
{
    public class Program
    {
        static string logName = "";
        static bool highLogLevel = false;
        static bool silentMode = false;
        static void Main(string[] args)
        {
            try
            {
                #region Assign Variables

                if (args.Length > 0 && args[0] == "1")
                {
                    highLogLevel = true;
                }
                if (args.Length > 1 && args[1] == "true")
                {
                    silentMode = true;
                }
                var today = DateTime.Now;
                logName = "MemoRecord_" + today.Year.ToString() + today.Month.ToString("00") + today.Day.ToString("00") +
                    "_" + today.Hour.ToString("00") + today.Minute.ToString("00") + today.Second.ToString("00") + ".log";

                List<MemoRecordDeleteModel> modelToDeleteList = new List<MemoRecordDeleteModel>();
                List<FailedRecord> failedRecordList = new List<FailedRecord>();

                #endregion

                #region GetAllPoHeaders()

                List<PIT_PO_HEADER> poHeaderList = GetAllPoHeaders();
                if (poHeaderList == null)
                {
                    Log("Not found any record of PO Headers in DB or error occurred, the application will terminated.");
                    Environment.Exit(0);
                }

                #endregion

                #region GetAllSapMemoes()

                List<PIT_SAP_MEMO_NO> sapMemoList = GetAllSapMemoes();
                if (sapMemoList == null)
                {
                    Log("Cannot get SAP Memo from DB, the application will terminated.");
                    Environment.Exit(0);
                }

                #endregion

                #region Get new PO List

                Log("Getting new PO List to insert to PIT_SAP_MEMO_NO");
                var newPoList = poHeaderList.Where(x => !sapMemoList.Select(a => a.SMN_PO_NO).ToArray().Contains(x.PO_NO)).ToList();
                Log("Found " + newPoList.Count() + " new PO items.");
                Log("");

                #endregion

                #region Create new SAP record

                Log("*** Creating new SAP record ***");
                Log("");
                CreateNewSapRecord(newPoList, failedRecordList);
                Log("*** Finished Created new SAP record ***");
                Log("");

                #endregion

                #region Update SAP record

                Log("*** Updating SAP record ***");
                Log("");
                UpdateSapRecord(poHeaderList, sapMemoList, modelToDeleteList, failedRecordList);
                Log("*** Finished Updated SAP record ***");
                Log("");

                #endregion

                #region Delete SAP Record

                Log("*** Deleting SAP Record ***");
                Log("");
                DeleteSapRecord(modelToDeleteList, failedRecordList);
                Log("*** Finished Deleted SAP Record ***");
                Log("");

                #endregion

                //if (failedRecordList.Count() > 0) //If found some error when send data to SAP, summarize and send email
                //{
                //    SendEmailResult(failedRecordList);
                //}

                Log("Finished executed Memo Record, bye for now.....");
            }
            catch(Exception ex)
            {
                Log("UnControlable error occurred while running Memo Record - " + ex.Message);
                if(highLogLevel)
                {
                    Log(ex.ToString());
                }
            }
        }

        public static void CreateNewSapRecord(List<PIT_PO_HEADER> poHeaderList, List<FailedRecord> failedRecordList)
        {
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");

                if (poHeaderList.Count() == 0)
                {
                    Log("No new PO Header to create.");
                    return;
                }

                PIT_SAP_MEMO_NO_DAL dal = new PIT_SAP_MEMO_NO_DAL();

                foreach (var header in poHeaderList)
                {
                    Log("Performing PO " + header.PO_NO);
                    var poItemList = GetAllItemsByPoNo(header.PO_NO);

                    if (poItemList != null)
                    {
                        foreach (var item in poItemList)
                        {
                            var poDate = DateTime.ParseExact(header.PO_DATE.Value.ToString("dd/MM/yyyy"), "dd/MM/yyyy", cultureinfo);
                            int lastDayInMonth = DateTime.DaysInMonth(poDate.Year, poDate.Month);

                            for (int day = 1; day <= lastDayInMonth; day++)
                            {                                
                                Log("Performing Po Item - " + item.PO_ITEM);
                                var resultFromMtSapMemoMapping = GetMtSapMemoMapping(item.ACC_NUM_VENDOR, item.MET_NUM);

                                Log("Preparing data for send to SAP for item - " + item.PO_ITEM);
                                MemoRecordCreateModel modelToAdd = new MemoRecordCreateModel();
                                string merkm = (resultFromMtSapMemoMapping.CostName == "Crude/Feedstock/Product") ? resultFromMtSapMemoMapping.MetNum : resultFromMtSapMemoMapping.CostName;
                                if (merkm == null)
                                {
                                    merkm = "";
                                }
                                decimal wrshb = (Convert.ToDecimal(item.VOLUME) * Convert.ToDecimal(item.UNIT_PRICE)) / DateTime.DaysInMonth(poDate.Year, poDate.Month);
                                wrshb = Convert.ToDecimal(wrshb.ToString("0.##")); //Kepp only 2 digit because too many digit causes the crash when send to SAP

                                string datum = poDate.AddDays(lastDayInMonth - poDate.Day).ToString("yyyy-MM-dd");
                                string planningDate = "";
                                int expiryDays = GetExpirayDays(item.ACC_NUM_VENDOR);
                                if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                                {
                                    planningDate = poDate.AddDays(lastDayInMonth - poDate.Day).AddDays(expiryDays).ToString("yyyy-MM-dd");
                                }
                                else //If not found, add 3 months to expiry days
                                {
                                    planningDate = poDate.AddDays(lastDayInMonth - poDate.Day).AddMonths(3).ToString("yyyy-MM-dd");
                                }
                                string sgtxt = GetVendorShortName(item.ACC_NUM_VENDOR) + "/" + merkm + "/" + header.CARGO_NO;

                                modelToAdd.AVDAT = String.IsNullOrEmpty(planningDate) ? "" : planningDate;
                                modelToAdd.BUKRS = String.IsNullOrEmpty(header.COMPANY_CODE) ? "" : header.COMPANY_CODE;
                                modelToAdd.DSART = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningType) ? "" : resultFromMtSapMemoMapping.PlanningType;
                                modelToAdd.DISPW = String.IsNullOrEmpty(item.CURRENCY) ? "" : item.CURRENCY;
                                modelToAdd.DATUM = String.IsNullOrEmpty(datum) ? "" : datum;
                                modelToAdd.GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup;                                
                                modelToAdd.GVALT = poDate.Year.ToString() + "-" + poDate.Month.ToString("00") + "-" + day.ToString("00");
                                modelToAdd.KURST = "M";
                                modelToAdd.MERKM = String.IsNullOrEmpty(merkm) ? "" : merkm;
                                modelToAdd.REFER = String.IsNullOrEmpty(item.ACC_NUM_VENDOR) ? "" : item.ACC_NUM_VENDOR;
                                modelToAdd.SGTXT = String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                                modelToAdd.TESTRUN = "";
                                modelToAdd.VOART = "";
                                modelToAdd.WRSHB = wrshb;
                                modelToAdd.WRSHBSpecified = true;
                                modelToAdd.XINVR = (item.CURRENCY.ToUpper() == "THB") ? "X" : "";
                                modelToAdd.ZUONR = String.IsNullOrEmpty(item.TRIP_NO) ? "" : item.TRIP_NO;

                                if (highLogLevel)
                                {
                                    Log("modelToAdd.AVDAT = " + modelToAdd.AVDAT);
                                    Log("modelToAdd.BUKRS = " + modelToAdd.BUKRS);
                                    Log("modelToAdd.DSART = " + modelToAdd.DSART);
                                    Log("modelToAdd.DISPW = " + modelToAdd.DISPW);
                                    Log("modelToAdd.DATUM = " + modelToAdd.DATUM);
                                    Log("modelToAdd.GRUPP = " + modelToAdd.GRUPP);
                                    Log("modelToAdd.GVALT = " + poDate.Year.ToString() + "-" + poDate.Month.ToString("00") + "-" + day.ToString("00"));
                                    Log("modelToAdd.KURST = " + modelToAdd.KURST);
                                    Log("modelToAdd.MERKM = " + modelToAdd.MERKM);
                                    Log("modelToAdd.REFER = " + modelToAdd.REFER);
                                    Log("modelToAdd.SGTXT = " + modelToAdd.SGTXT);
                                    Log("modelToAdd.TESTRUN = " + modelToAdd.TESTRUN);
                                    Log("modelToAdd.VOART = " + modelToAdd.VOART);
                                    Log("modelToAdd.WRSHB = " + modelToAdd.WRSHB);
                                    Log("modelToAdd.WRSHBSpecified = " + modelToAdd.WRSHBSpecified);
                                    Log("modelToAdd.XINVR = " + modelToAdd.XINVR);
                                    Log("modelToAdd.ZUONR = " + modelToAdd.ZUONR);
                                }

                                Log("Getting configuration from DownStream CIP_MEMO_CREATE.");
                                MemoRecordCreateServiceConnectorImpl MRService = new MemoRecordCreateServiceConnectorImpl();
                                MemoRecordService service = new MemoRecordService();
                                ConfigManagement configManagement = new ConfigManagement();
                                string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CREATE");
                                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer(); ;
                                string content = javaScriptSerializer.Serialize(modelToAdd);

                                if (highLogLevel)
                                {
                                    Log("jsonConfig = " + jsonConfig);
                                    Log("content = " + content);
                                }

                                Log("Sending the content to SAP");
                                var result = MRService.connect(jsonConfig, content);
                                if ((result == null) || (result.TYPE.ToUpper() != "S")) //Failed add new SAP record
                                {
                                    string reason = (result == null) ? "Unexpected error" : result.MESSAGE;
                                    Log("Result from SAP is FAILED for PO - " + item.PO_NO + ", Po Item - " + item.PO_ITEM);
                                    Log("Reason = " + reason);
                                    failedRecordList.Add(new FailedRecord
                                    {
                                        PoNo = item.PO_NO,
                                        PoItem = item.PO_ITEM,
                                        MemoNo = "",
                                        Reason = (result == null) ? "Unexpected error" : result.MESSAGE,
                                        Action = "CREATE"
                                    });
                                }
                                else //Added new record to SAP successfully, then add the new record to PIT_SAP_MEMO_NO
                                {
                                    Log("Send to SAP completed for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM + ", Memo No - " + result.MESSAGE_V1);
                                    try
                                    {
                                        PIT_SAP_MEMO_NO sapMemo = new PIT_SAP_MEMO_NO();
                                        string poDateForDB = day.ToString("00") + "/" + poDate.Month.ToString("00") + "/" + poDate.Year.ToString();
                                        sapMemo.SMN_MEMO_NO = result.MESSAGE_V1;
                                        sapMemo.SMN_PO_NO = header.PO_NO;
                                        sapMemo.SMN_PO_ITEM = item.PO_ITEM;
                                        sapMemo.SMN_PO_DATE = DateTime.ParseExact(poDateForDB, "dd/MM/yyyy", cultureinfo);
                                        sapMemo.SMN_IS_DELETE = "";
                                        sapMemo.SMN_CREATED_DATE = DateTime.Now;
                                        sapMemo.SMN_CREATED_BY = "MemoRecord";
                                        sapMemo.SMN_UPDATED_DATE = DateTime.Now;
                                        sapMemo.SMN_UPDATED_BY = "MemoRecord";

                                        Log("Saving the result to PIT_SAP_MEMO_NO for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM + ", Memo No - " + result.MESSAGE_V1);
                                        dal.Save(sapMemo);
                                        Log("Saved to PIT_SAP_MEMO_NO for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM + " completely");
                                    }
                                    catch
                                    {
                                        Log("Error occurred while saving to PIT_SAP_MEMO_NO for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM);
                                        failedRecordList.Add(new FailedRecord
                                        {
                                            PoNo = item.PO_NO,
                                            PoItem = item.PO_ITEM,
                                            MemoNo = "",
                                            Reason = "Error occurred while inserting the record to PIT_SAP_MEMO_NO",
                                            Action = "CREATE"
                                        });
                                    }
                                }

                            }

                        }
                    }

                    Log("Finsihed Performed PO " + header.PO_NO);
                }
            }
            catch(Exception ex)
            {
                Log("Error occurred while creating new SAP record - " + ex.Message);
                if(highLogLevel)
                {
                    Log(ex.ToString());
                }
                throw ex;
            }
        }

        public static void UpdateSapRecord(List<PIT_PO_HEADER> poHeaderList, List<PIT_SAP_MEMO_NO> sapMemoList, List<MemoRecordDeleteModel> modelToDeleteList, List<FailedRecord> failedRecordList)
        {
            List<PIT_PO_ITEM> failedList = new List<PIT_PO_ITEM>();
            PIT_SAP_MEMO_NO_DAL dal = new PIT_SAP_MEMO_NO_DAL();

            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");

                foreach (var header in poHeaderList)
                {
                    Log("Performing PO Header - " + header.PO_NO);
                    //Get PO Items only updated latest than the reord on PIT_SAP_MEMO_NO
                    var poItemList = GetAllItemsByPoNo(header.PO_NO);

                    if (poItemList != null)
                    {
                        foreach (var item in poItemList)
                        {
                            Log("Performing PO Item - " + item.PO_ITEM);
                            var poDate = DateTime.ParseExact(header.PO_DATE.Value.ToString("dd/MM/yyyy"), "dd/MM/yyyy", cultureinfo);
                            int lastDayInMonth = DateTime.DaysInMonth(poDate.Year, poDate.Month);

                            for (int day = 1; day <= lastDayInMonth; day++)
                            {
                                var generatedPoDate = DateTime.ParseExact(day.ToString("00") + "/" + poDate.Month.ToString("00") + "/" + poDate.Year.ToString(), "dd/MM/yyyy", cultureinfo);
                                var matchedSapMemo = sapMemoList.Where(x => x.SMN_PO_NO == item.PO_NO && x.SMN_PO_ITEM == item.PO_ITEM && 
                                                                        item.UPDATED_DATE > x.SMN_UPDATED_DATE && 
                                                                        x.SMN_PO_DATE.Equals(generatedPoDate) && 
                                                                        String.IsNullOrEmpty(x.SMN_IS_DELETE)).ToList();
                                if (matchedSapMemo.Count() != 0) //Found the existing record in SAP Memo
                                {
                                    var resultFromMtSapMemoMapping = GetMtSapMemoMapping(item.ACC_NUM_VENDOR, item.MET_NUM);

                                    Log("Preparing data for send to SAP for item - " + item.PO_ITEM);
                                    MemoRecordChangeModel modelToChange = new MemoRecordChangeModel();
                                    string merkm = (resultFromMtSapMemoMapping.CostName == "Crude/Feedstock/Product") ? resultFromMtSapMemoMapping.MetNum : resultFromMtSapMemoMapping.CostName;
                                    if (merkm == null)
                                    {
                                        merkm = "";
                                    }
                                    decimal wrshb = (Convert.ToDecimal(item.VOLUME) * Convert.ToDecimal(item.UNIT_PRICE)) / DateTime.DaysInMonth(poDate.Year, poDate.Month);
                                    wrshb = Convert.ToDecimal(wrshb.ToString("0.##")); //Keep only 2 digit because too many digit causes the crash when send to SAP

                                    string datum = poDate.AddDays(lastDayInMonth - poDate.Day).ToString("yyyy-MM-dd");
                                    string planningDate = "";
                                    int expiryDays = GetExpirayDays(item.ACC_NUM_VENDOR);
                                    if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                                    {
                                        planningDate = poDate.AddDays(lastDayInMonth - poDate.Day).AddDays(expiryDays).ToString("yyyy-MM-dd");
                                    }
                                    else //If not found, add 3 months to expiry days
                                    {
                                        planningDate = poDate.AddDays(lastDayInMonth - poDate.Day).AddMonths(3).ToString("yyyy-MM-dd");
                                    }

                                    decimal dmshb = 0;
                                    if (item.CURRENCY.ToLower() == "usd")
                                    {
                                        dmshb = 0;
                                    }
                                    else
                                    {
                                        dmshb = wrshb;
                                    }

                                    modelToChange.AVDAT = planningDate;
                                    modelToChange.BUKRS = header.COMPANY_CODE;
                                    modelToChange.DATUM = datum;
                                    modelToChange.DISPW = item.CURRENCY;
                                    modelToChange.DMSHB = dmshb;
                                    modelToChange.DMSHBSpecified = true;
                                    modelToChange.GRUPP = resultFromMtSapMemoMapping.PlanningGroup;
                                    modelToChange.GVALT = poDate.Year.ToString() + "-" + poDate.Month.ToString("00") + "-" + day.ToString("00");
                                    modelToChange.IDENR = matchedSapMemo[0].SMN_MEMO_NO;
                                    modelToChange.KURST = "M";
                                    modelToChange.MERKM = item.MET_NUM;
                                    modelToChange.REFER = item.ACC_NUM_VENDOR;
                                    modelToChange.SGTXT = GetVendorShortName(item.ACC_NUM_VENDOR) + "/" + merkm + "/" + header.CARGO_NO;
                                    modelToChange.TESTRUN = "";
                                    modelToChange.VOART = "";
                                    modelToChange.WRSHB = wrshb;
                                    modelToChange.WRSHBSpecified = true;
                                    modelToChange.XINVR = (item.CURRENCY.ToUpper() == "THB") ? "X" : "";
                                    modelToChange.ZUONR = item.TRIP_NO;

                                    if (highLogLevel)
                                    {
                                        Log("modelToChange.AVDAT = " + modelToChange.AVDAT);
                                        Log("modelToChange.BUKRS = " + modelToChange.BUKRS);
                                        Log("modelToChange.DISPW = " + modelToChange.DISPW);
                                        Log("modelToChange.DMSHB = " + modelToChange.DMSHB);
                                        Log("modelToChange.DMSHBSpecified = " + modelToChange.DMSHBSpecified);
                                        Log("modelToChange.DATUM = " + modelToChange.DATUM);
                                        Log("modelToChange.GRUPP = " + modelToChange.GRUPP);
                                        Log("modelToChange.GVALT = " + modelToChange.GVALT);
                                        Log("modelToChange.IDENR = " + modelToChange.IDENR);
                                        Log("modelToChange.KURST = " + modelToChange.KURST);
                                        Log("modelToChange.MERKM = " + modelToChange.MERKM);
                                        Log("modelToChange.REFER = " + modelToChange.REFER);
                                        Log("modelToChange.SGTXT = " + modelToChange.SGTXT);
                                        Log("modelToChange.TESTRUN = " + modelToChange.TESTRUN);
                                        Log("modelToChange.VOART = " + modelToChange.VOART);
                                        Log("modelToChange.WRSHB = " + modelToChange.WRSHB);
                                        Log("modelToChange.WRSHBSpecified = " + modelToChange.WRSHBSpecified);
                                        Log("modelToChange.XINVR = " + modelToChange.XINVR);
                                        Log("modelToChange.ZUONR = " + modelToChange.ZUONR);
                                    }

                                    Log("Getting configuration from CIP_MEMO_CHANGE");
                                    MemoRecordChangeServiceConnectorImpl MRService = new MemoRecordChangeServiceConnectorImpl();
                                    MemoRecordService service = new MemoRecordService();
                                    ConfigManagement configManagement = new ConfigManagement();
                                    string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CHANGE");
                                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                    string content = javaScriptSerializer.Serialize(modelToChange);
                                    if (highLogLevel)
                                    {
                                        Log("jsonConfig = " + jsonConfig);
                                        Log("content = " + content);
                                    }

                                    Log("Sending the content to SAP");
                                    var result = MRService.connect(jsonConfig, content);
                                    if (result.TYPE.ToUpper() != "S") //Failed add new SAP record
                                    {
                                        Log("Result from SAP is FAILED for PO - " + item.PO_NO + ", Po Item - " + item.PO_ITEM);
                                        Log("Reason = " + result.MESSAGE);
                                        failedRecordList.Add(new FailedRecord
                                        {
                                            PoNo = item.PO_NO,
                                            PoItem = item.PO_ITEM,
                                            MemoNo = matchedSapMemo[0].SMN_MEMO_NO,
                                            Reason = result.MESSAGE,
                                            Action = "CHANGE"
                                        });
                                    }
                                    else
                                    {
                                        Log("Send to SAP completed for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM + ", Memo No - " + matchedSapMemo[0].SMN_MEMO_NO);
                                        try
                                        {
                                            using (EntityCPAIEngine context = new EntityCPAIEngine())
                                            {
                                                string poDateForDB = day.ToString("00") + "/" + poDate.Month.ToString("00") + "/" + poDate.Year.ToString();
                                                var _search = context.PIT_SAP_MEMO_NO.Find(matchedSapMemo[0].SMN_MEMO_NO);
                                                _search.SMN_PO_NO = header.PO_NO;
                                                _search.SMN_PO_ITEM = item.PO_ITEM;
                                                _search.SMN_PO_DATE = DateTime.ParseExact(poDateForDB, "dd/MM/yyyy", cultureinfo);
                                                _search.SMN_UPDATED_DATE = DateTime.Now;
                                                _search.SMN_UPDATED_BY = "MemoRecord";

                                                Log("Updating the result to PIT_SAP_MEMO_NO for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM + ", Memo No - " + matchedSapMemo[0].SMN_MEMO_NO);
                                                context.SaveChanges();
                                                Log("Updated to PIT_SAP_MEMO_NO for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM + " completely");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Log("Error occurred while updating to PIT_SAP_MEMO_NO for PO - " + item.PO_NO + ", PO Item - " + item.PO_ITEM);
                                            if (highLogLevel)
                                            {
                                                Log(ex.ToString());
                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }

                    if (!String.IsNullOrEmpty(header.IS_DELETE))
                    {
                        Log("Found IS_DELETE for PO Header - " + header.PO_NO);
                        //Get all memo which PoNo same as the deleted PO header's PoNo
                        //Because we mark to be deleted at header level not item level
                        //So all item related with this header will be deleted too
                        Log("Getting deleted memo for PO Header - " + header.PO_NO + " and update date - " + header.UPDATED_DATE);
                        var deletedMemoes = sapMemoList.Where(x => x.SMN_PO_NO == header.PO_NO && header.UPDATED_DATE > x.SMN_UPDATED_DATE && 
                                                                String.IsNullOrEmpty(x.SMN_IS_DELETE)).ToList();

                        foreach (var deletedMemo in deletedMemoes)
                        {
                            Log("Added to delete list for Memo No - " + deletedMemo.SMN_MEMO_NO + " and company code - " + header.COMPANY_CODE);
                            modelToDeleteList.Add(new MemoRecordDeleteModel
                            {
                                BUKRS = header.COMPANY_CODE,
                                IDENR = deletedMemo.SMN_MEMO_NO,
                                TESTRUN = ""
                            });
                        }
                    }

                    Log("Finished Performed PO Header - " + header.PO_NO);
                }
            }
            catch(Exception ex)
            {
                Log("Error occurred while Updating Memo Record - " + ex.Message);
                if(highLogLevel)
                {
                    Log(ex.ToString());
                }
            }
        }

        public static void DeleteSapRecord(List<MemoRecordDeleteModel> modelToDeleteList, List<FailedRecord> failedRecordList)
        {
            List<PIT_PO_ITEM> failedList = new List<PIT_PO_ITEM>();
            
            if(modelToDeleteList == null || modelToDeleteList.Count() == 0)
            {
                Log("No record to delete.");
                return;
            }

            foreach (var model in modelToDeleteList)
            {
                Log("Perfoming Memo No - " + model.IDENR);

                Log("Getting configuration from CIP_MEMO_DELETE");
                MemoRecordDeleteServiceConnectorImpl MRService = new MemoRecordDeleteServiceConnectorImpl();
                MemoRecordService service = new MemoRecordService();
                ConfigManagement configManagement = new ConfigManagement();
                string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_DELETE");
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string content = javaScriptSerializer.Serialize(model);
                if (highLogLevel)
                {
                    Log("jsonConfig = " + jsonConfig);
                    Log("content = " + content);
                }

                Log("Sending the content to SAP");
                var result = MRService.connect(jsonConfig, content);
                if (result.TYPE.ToUpper() != "S") //Failed add new SAP record
                {
                    Log("Result from SAP is FAILED for Memo No - " + model.IDENR);
                    Log("Reason = " + result.MESSAGE);
                    failedRecordList.Add(new FailedRecord
                    {
                        PoNo = "",
                        PoItem = 0,
                        MemoNo = model.IDENR,
                        Reason = result.MESSAGE,
                        Action = "DELETE"
                    });
                }
                else
                {
                    Log("Send to SAP completed for Memo No - " + model.IDENR);
                    try
                    {
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var _search = context.PIT_SAP_MEMO_NO.Find(model.IDENR);
                            _search.SMN_IS_DELETE = "TRUE";
                            _search.SMN_UPDATED_DATE = DateTime.Now;
                            _search.SMN_UPDATED_BY = "MemoRecord";

                            Log("Updating the result to PIT_SAP_MEMO_NO for Memo No - " + model.IDENR);
                            context.SaveChanges();
                            Log("Updated the result to PIT_SAP_MEMO_NO for Memo No - " + model.IDENR + " completely");
                        }
                    }
                    catch(Exception ex)
                    {
                        Log("Error occurred while deleting to PIT_SAP_MEMO_NO for Memo No - " + model.IDENR);
                        if(highLogLevel)
                        {
                            Log(ex.ToString());
                        }
                    }
                }

                Log("Finished Perfomed Memo No - " + model.IDENR);
            }
        }

        public static List<PIT_PO_HEADER> GetAllPoHeaders() 
        {
            try
            {
                Log("*** Getting all PO Headers from DB. ***");
                Log("");
                List<PIT_PO_HEADER> poHeaderList = new List<PIT_PO_HEADER>();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from r in context.PIT_PO_HEADER
                                 where r.COMPANY_CODE == "1100" || r.COMPANY_CODE == "1400"
                                 select new
                                 {
                                     dPoNo = r.PO_NO,
                                     dCompanyCode = r.COMPANY_CODE,
                                     dAccNumVendor = r.ACC_NUM_VENDOR,
                                     dPurchasingGroup = r.PURCHASING_GROUP,
                                     dCurrency = r.CURRENCY,
                                     dFxBot = r.FX_BOT,
                                     dPoDate = r.PO_DATE,
                                     dIncoterms = r.INCOTERMS,
                                     dIsDelete = r.IS_DELETE,
                                     dOriginCountry = r.ORIGIN_COUNTRY,
                                     dCargoNo = r.CARGO_NO,
                                     dSoNo = r.SO_NO,
                                     dCreatedDate = r.CREATED_DATE,
                                     dCreatedBy = r.CREATED_BY,
                                     dUpdatedDate = r.UPDATED_DATE,
                                     dUpdatedBy = r.UPDATED_BY
                                 });

                    if (query != null && query.ToList().Count() > 0)
                    {
                        Log("Found PO Headers in DB.");
                        foreach (var header in query)
                        {
                            Log("Adding " + header.dPoNo + " to list.");
                            poHeaderList.Add(new PIT_PO_HEADER
                            {
                                PO_NO = header.dPoNo,
                                COMPANY_CODE = header.dCompanyCode,
                                ACC_NUM_VENDOR = header.dAccNumVendor,
                                PURCHASING_GROUP = header.dPurchasingGroup,
                                CURRENCY = header.dCurrency,
                                FX_BOT = header.dFxBot,
                                PO_DATE = header.dPoDate,
                                INCOTERMS = header.dIncoterms,
                                IS_DELETE = header.dIsDelete,
                                ORIGIN_COUNTRY = header.dOriginCountry,
                                CARGO_NO = header.dCargoNo,
                                SO_NO = header.dSoNo,
                                CREATED_DATE = header.dCreatedDate,
                                CREATED_BY = header.dCreatedBy,
                                UPDATED_DATE = header.dUpdatedDate,
                                UPDATED_BY = header.dUpdatedBy
                            });
                        }
                    }
                    else
                    {
                        Log("Not found any PO Headers in DB.");
                        return null;
                    }
                }

                Log("*** Get all PO Headers completed, found " + poHeaderList.Count() + " POs. ***");
                Log("");
                return poHeaderList;
            }
            catch(Exception ex)
            {
                Log("Error occurred while getting all PO Headers from DB - " + ex.Message);
                if(highLogLevel)
                {
                    Log(ex.ToString());
                }
                return null;
            }
        }

        public static List<PIT_PO_ITEM> GetAllItemsByPoNo(string poNo)
        {
            try
            {
                Log("Get all PO Items from PO Header - " + poNo);
                List<PIT_PO_ITEM> poItemList = new List<PIT_PO_ITEM>();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from r in context.PIT_PO_ITEM where r.PO_NO.Equals(poNo)
                                 select new
                                 {
                                    dPoItem = r.PO_ITEM,
                                    dPoNo = r.PO_NO,
                                    dShortText = r.SHORT_TEXT,
                                    dMetNum = r.MET_NUM,
                                    dPlant = r.PLANT,
                                    dStorageLocation = r.STORAGE_LOCATION,
                                    dTripNo = r.TRIP_NO,
                                    dVolume = r.VOLUME,
                                    dVolumeUnit = r.VOLUME_UNIT,
                                    dUnitPrice = r.UNIT_PRICE,
                                    dCurreny = r.CURRENCY,
                                    dAccNumVendor = r.ACC_NUM_VENDOR,
                                    dTranId = r.TRIP_NO,
                                    dCargoNo = r.CARGO_NO,
                                    dSoUsername = r.SO_USERNAME,
                                    dUpdatedDate = r.UPDATED_DATE,
                                    dUpdateBy = r.UPDATED_BY
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        foreach(var item in query)
                        {
                            poItemList.Add(new PIT_PO_ITEM
                            {
                                PO_ITEM = item.dPoItem,
                                PO_NO = item.dPoNo,
                                SHORT_TEXT = item.dShortText,
                                MET_NUM = item.dMetNum,
                                PLANT = item.dPlant,
                                STORAGE_LOCATION = item.dStorageLocation,
                                TRIP_NO = item.dTripNo,
                                VOLUME = item.dVolume,
                                VOLUME_UNIT = item.dVolumeUnit,
                                UNIT_PRICE = item.dUnitPrice,
                                CURRENCY = item.dCurreny,
                                ACC_NUM_VENDOR = item.dAccNumVendor,
                                TRAN_ID = item.dTranId,
                                CARGO_NO = item.dCargoNo,
                                SO_USERNAME = item.dSoUsername,
                                UPDATED_DATE = item.dUpdatedDate,
                                UPDATED_BY = item.dUpdateBy
                            });                            
                        }

                        Log("Found " + poItemList.Count() + " PO Items related to PO Header - " + poNo);
                        return poItemList;
                    }
                    else
                    {
                        Log("Not found any PO Items related to PO Header - " + poNo);
                        return null;
                    }
                }
            }
            catch(Exception ex)
            {
                Log("Error Occurred while getting PO Items related to PO Header - " + poNo);
                if(highLogLevel)
                {
                    Log(ex.ToString());
                }
                return null;
            }            
        }

        public static List<PIT_SAP_MEMO_NO> GetAllSapMemoes()
        {
            try
            {
                Log("*** Getting all SAP Memo from DB. ***");
                Log("");
                List<PIT_SAP_MEMO_NO> sapMemoList = new List<PIT_SAP_MEMO_NO>();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from r in context.PIT_SAP_MEMO_NO
                                 select new
                                 {
                                     dMemoNo = r.SMN_MEMO_NO,
                                     dPoNo = r.SMN_PO_NO,
                                     dPoItem = r.SMN_PO_ITEM,
                                     dPoDate = r.SMN_PO_DATE,
                                     dIsDelete = r.SMN_IS_DELETE,
                                     dCreatedDate = r.SMN_CREATED_DATE,
                                     dCreatedBy = r.SMN_CREATED_BY,
                                     dUpdatedDate = r.SMN_UPDATED_DATE,
                                     dUpdatedBy = r.SMN_UPDATED_BY
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        Log("Found SAP Memo record in DB (PIT_SAP_MEMO_NO).");
                        foreach (var memo in query)
                        {
                            Log("Adding SAP Memo " + memo.dMemoNo + " to list");
                            sapMemoList.Add(new PIT_SAP_MEMO_NO
                            {
                                SMN_MEMO_NO = memo.dMemoNo,
                                SMN_PO_NO = memo.dPoNo,
                                SMN_PO_ITEM = memo.dPoItem,
                                SMN_PO_DATE = memo.dPoDate,
                                SMN_IS_DELETE = memo.dIsDelete,
                                SMN_CREATED_DATE = memo.dCreatedDate,
                                SMN_CREATED_BY = memo.dCreatedBy,
                                SMN_UPDATED_DATE = memo.dUpdatedDate,
                                SMN_UPDATED_BY = memo.dUpdatedBy
                            });
                        }
                    }
                    else
                    {
                        Log("Not found any record of SAP Memo in DB (PIT_SAP_MEMO_NO).");

                        return new List<PIT_SAP_MEMO_NO>();
                    }
                }
                Log("*** Get SAP Memo from DB completed, found " + sapMemoList.Count() + " items. ***");
                Log("");
                return sapMemoList;
            }
            catch(Exception ex)
            {
                Log("Error occurred while getting SAP Memo record from DB (PIT_SAP_MEMO_NO) - " + ex.Message);
                Log(ex.ToString());

                return null;
            }
        }        

        public static ResultFromMtSapMemoMapping GetMtSapMemoMapping(string accNumVendor, string metNum)
        {
            try
            {
                Log("Getting SAP Memo Mapping of for vendor - " + accNumVendor);
                decimal accNumVendorDecimal = Convert.ToDecimal(accNumVendor);
                accNumVendor = accNumVendorDecimal.ToString();
                ResultFromMtSapMemoMapping result = new ResultFromMtSapMemoMapping();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from r in context.MT_SAP_MEMO_MAPPING
                                 where r.SMM_ACC_NUM_VENDOR == accNumVendor
                                 select new
                                 {
                                     dCostName = r.SMM_COST_NAME,
                                     dMetNum = r.SMM_MET_NUM,
                                     dPlanningType = r.SMM_PLANNING_TYPE,
                                     dPlanningGroup = r.SMM_PLANNING_GROUP
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        var resultList = query.ToList();
                        result.CostName = resultList[0].dCostName;
                        result.MetNum = metNum;
                        result.PlanningType = resultList[0].dPlanningType;
                        result.PlanningGroup = resultList[0].dPlanningGroup;
                        Log("Found SAP Memo Mapping matched with vendor - " + accNumVendor + ", MetNum = " + resultList[0].dMetNum);
                    }
                    else
                    {
                        Log("Not found SAP Memo Mapping matched with vendor - " + accNumVendor + ", search by MET_NUM instead");
                        Log("Using MET_NUM = " + metNum);
                        var query2 = (from m in context.MT_SAP_MEMO_MAPPING
                                      where m.SMM_MET_NUM == metNum
                                      select new
                                      {
                                          dCostName = m.SMM_COST_NAME,
                                          dMetNum = m.SMM_MET_NUM,
                                          dPlanningType = m.SMM_PLANNING_TYPE,
                                          dPlanningGroup = m.SMM_PLANNING_GROUP
                                      });
                        if (query2 != null && query2.ToList().Count() > 0)
                        {
                            var resultList = query2.ToList();
                            result.CostName = resultList[0].dCostName;
                            result.MetNum = metNum;
                            result.PlanningType = resultList[0].dPlanningType;
                            result.PlanningGroup = resultList[0].dPlanningGroup;
                            Log("Found SAP Memo Mapping matched with vendor - " + accNumVendor + ", MetNum = " + resultList[0].dMetNum);
                        }
                        else
                        {
                            Log("Not found SAP Memo Mapping matched with MET_NUM - " + metNum + ", search by MAT_GROUP instead");
                            var query3 = (from m in context.MT_MATERIALS
                                          where m.MET_NUM == metNum
                                          select new
                                          {
                                              dMatGroup = m.MET_MAT_GROUP
                                          });
                            if (query3 != null && query3.ToList().Count() > 0)
                            {
                                string matGroup = query3.ToList()[0].dMatGroup;
                                var query4 = (from m in context.MT_SAP_MEMO_MAPPING
                                              where m.SMM_MAT_GROUP == matGroup
                                              select new
                                              {
                                                  dCostName = m.SMM_COST_NAME,
                                                  dMetNum = m.SMM_MET_NUM,
                                                  dPlanningType = m.SMM_PLANNING_TYPE,
                                                  dPlanningGroup = m.SMM_PLANNING_GROUP
                                              });
                                if (query4 != null && query4.ToList().Count() > 0)
                                {
                                    var resultList = query4.ToList();
                                    result.CostName = resultList[0].dCostName;
                                    result.MetNum = metNum;
                                    result.PlanningType = resultList[0].dPlanningType;
                                    result.PlanningGroup = resultList[0].dPlanningGroup;
                                    Log("Found SAP Memo Mapping matched with MAT_GROUP - " + matGroup);
                                }
                                else
                                {
                                    var query5 = (from m in context.MT_SAP_MEMO_MAPPING
                                                  where String.IsNullOrEmpty(m.SMM_ACC_NUM_VENDOR) && String.IsNullOrEmpty(m.SMM_MET_NUM) && String.IsNullOrEmpty(m.SMM_MAT_GROUP)
                                                  select new
                                                  {
                                                      dCostName = m.SMM_COST_NAME,
                                                      dMetNum = m.SMM_MET_NUM,
                                                      dPlanningType = m.SMM_PLANNING_TYPE,
                                                      dPlanningGroup = m.SMM_PLANNING_GROUP
                                                  });
                                    if (query5 != null && query5.ToList().Count() > 0)
                                    {
                                        var resultList = query5.ToList();
                                        result.CostName = resultList[0].dCostName;
                                        result.MetNum = metNum;
                                        result.PlanningType = resultList[0].dPlanningType;
                                        result.PlanningGroup = resultList[0].dPlanningGroup;
                                        Log("Using the default value for account number vendor = " + accNumVendor);
                                    }
                                    else
                                    {
                                        Log("Not found any record matched with account number vendor = " + accNumVendor);
                                    }
                                }
                            }
                            else
                            {
                                Log("Not found matched MAT_GROUP in MT_MATERIALS");
                            }
                        }
                    }
                }
                return result;
            }
            catch(Exception ex)
            {
                Log("Error occurred while getting SAP Memo Mapping for vendor " + accNumVendor + " - " + ex.Message);
                if(highLogLevel)
                {
                    Log(ex.ToString());
                }
                return new ResultFromMtSapMemoMapping();
            }
        }
        
        public static string GetVendorShortName(string accNumVendor)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from r in context.MT_VENDOR
                                 where r.VND_ACC_NUM_VENDOR == accNumVendor
                                 select new
                                 {
                                     dShortName = r.VND_SORT_FIELD
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dShortName;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public static int GetExpirayDays(string accNumVendor)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from r in context.MT_SAP_MEMO_EXPIRE
                             where r.SME_ACC_NUM_VENDOR == accNumVendor
                             select new
                             {
                                 dExpiryDays = r.SME_DAY_FOR_EXPIRE
                             });
                if(query != null && query.ToList().Count() > 0)
                {
                    return Convert.ToInt32(query.ToList()[0].dExpiryDays);
                }
                else
                {
                    return -1;
                }
            }
        }

        public static void SendEmailResult(List<FailedRecord> failedRecordList)
        {
            string mailTo = "";

            //Read email list from Global Config
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.GLOBAL_CONFIG
                             where v.GCG_KEY.ToUpper() == "CIP_MEMO"
                             select new
                             {
                                 dValue = v.GCG_VALUE
                             });
                if (query != null)
                {
                    if (query.ToList().Count() > 0)
                    {
                        string jsonEmailList = query.ToList()[0].dValue;

                        var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        JObject json = JObject.Parse(String.IsNullOrEmpty(jsonEmailList) ? "" : jsonEmailList);

                        EmailList emailList = (EmailList)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(EmailList));

                        foreach(var email in emailList.EMAIL_LIST)
                        {
                            mailTo += email.EMAIL + ";";
                        }
                    }
                    else
                    {
                        Console.WriteLine("Cannot read Global Config \"CIP_MEMO\"");
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("Cannot read Global Config \"CIP_MEMO\"");
                    return;
                }
            }

            StateModel stateModel = new StateModel();
            string currentCode = "";

            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig("MAIL_EWM");
            String content = configManagement.getDownstreamContent("MAIL_EWM");

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = "Failure report for Memo Record (" + DateTime.Now.ToString("dd-MM-yyyy") + ")";
            contentObj.mail_body = "Dear Receivers,<br /><br />This is the report of the failures when send the data to SAP by Memo Record.<br /><br />";
            contentObj.mail_body += "<table border=\"1\"><tr><td align=\"center\" width=\"20%\">PO no.</td><td align=\"center\" width=\"20%\">PO Item</td><td align=\"center\" width=\"20%\">MEMO no.</td><td align=\"center\" width=\"20%\">Reason</td><td align=\"center\" width=\"20%\">Action</td></tr>";

            foreach(var failRecord in failedRecordList)
            {
                contentObj.mail_body += "<tr><td align=\"center\" width=\"20%\">" + failRecord.PoNo + "</td><td align=\"center\" width=\"20%\">" + failRecord.PoItem + "</td><td align=\"center\" width=\"20%\">" + failRecord.MemoNo + "</td><td align=\"center\" width=\"20%\">" + failRecord.Reason + "</td><td align=\"center\" width=\"20%\">" + failRecord.Action + "</td></tr>";
            }

            contentObj.mail_body += "</table>";

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            currentCode = downResp.getResultCode();
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();

        }

        public static void Log(string log)
        {
            var today = DateTime.Now;                       
            var currentPath = AppDomain.CurrentDomain.BaseDirectory;

            if (Directory.Exists(currentPath + "Logs"))
            {
                using (StreamWriter sw = File.AppendText(currentPath + "Logs" + "\\" + logName))
                {
                    var now = DateTime.Now;
                    var timeStamp = now.Day.ToString("00") + "/" + now.Month.ToString("00") + "/" + now.Year.ToString() + "_"
                        + now.Hour.ToString("00") + ":" + now.Minute.ToString("00") + ":" + now.Second.ToString("00") + "> ";
                    sw.WriteLine(timeStamp + log);
                    if(!silentMode)
                    {
                        Console.WriteLine(timeStamp + log);
                    }
                }
            }
        }
    }
}
