﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.CPAI
{
    public class DALGraphBoard : IDisposable
    {
        public List<MT_CRUDE_ROUTES> Get_MT_CRUDE_ROUTE(DateTime DateStart, DateTime DateEnd)
        {
            List<MT_CRUDE_ROUTES> _lstCR = new List<Entity.MT_CRUDE_ROUTES>();
            using (var context = new EntityCPAIEngine())
            {
                _lstCR = (from my_CR in context.MT_CRUDE_ROUTES
                          where  my_CR.MCR_DATE >= DateStart
                            && my_CR.MCR_DATE <= DateEnd
                            && my_CR.MCR_STATUS.ToUpper()=="ACTIVE"
                          select my_CR).ToList();
            }
            return _lstCR;
        }

        public List<string> GetTD()
        {
            List<string> lstTD = new List<string>();
            lstTD.Add("TD2");
            lstTD.Add("TD3");
            lstTD.Add("TD15");
            return lstTD;
        }

        public List<string> GetSTATUS()
        {
            List<string> lst = new List<string>();
            lst.Add("SUBMIT");
            lst.Add("CANCEL");
            
            return lst;
        }



        #region IDisposable Members

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        //IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Free other state (managed objects).
            }
            // Free your own state (unmanaged objects).
            // Set large fields to null.
            disposed = true;
        }

        #endregion
    }
}
