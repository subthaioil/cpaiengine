﻿using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.DAL
{
    public class ErrorLogDAL : IDisposable
    {
        public List<Error_LogDetailModel> SelectErrorLog(string _fromdate,string _todate, string Module,string Function)
        {
            return new List<Error_LogDetailModel>();
        }
        public void InsertErrorLog(Error_LogDetailModel model)
        {

        }

        #region IDisposable Members

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        //IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Free other state (managed objects).
            }
            // Free your own state (unmanaged objects).
            // Set large fields to null.
            disposed = true;
        }

        #endregion
    }
}