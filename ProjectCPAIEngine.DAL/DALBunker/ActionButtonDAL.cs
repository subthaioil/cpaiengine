﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ActionButtonDAL
    {
        public List<CPAI_ACTION_BUTTON> findByRowId(string rowId)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_BUTTON> results = entity.CPAI_ACTION_BUTTON.Where(x => x.ABT_ROW_ID.ToUpper() == rowId.ToUpper() && x.ABT_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_BUTTON>();
                }
            }
        }
        public List<CPAI_ACTION_BUTTON> findByRowIdAndType(string rowId,string type)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_BUTTON> results = entity.CPAI_ACTION_BUTTON.Where(x => x.ABT_ROW_ID.ToUpper() == rowId.ToUpper() && x.ABT_TYPE == type && x.ABT_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_BUTTON>();
                }
            }
        }
    }
}
