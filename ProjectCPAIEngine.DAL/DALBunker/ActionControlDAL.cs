﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ActionControlDAL
    {
        public List<CPAI_ACTION_CONTROL> findByCurrentStatusAndFunction(string currentStatus, List<string> lstAcfFunction)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_CONTROL> results = entity.CPAI_ACTION_CONTROL.Where(x => x.ACT_CURRENT_STATUS.ToUpper() == currentStatus.ToUpper() && x.ACT_STATUS == "ACTIVE" ).ToList();
                if (results != null && results.Count() > 0)
                {
                    var realResult = from a in results where a.ACT_FK_ACTION_FUNCTION != null && lstAcfFunction.Contains(a.ACT_FK_ACTION_FUNCTION) select a;
                    return realResult.ToList<CPAI_ACTION_CONTROL>();
                }
                else
                {
                    return new List<CPAI_ACTION_CONTROL>();
                }
            }
        }

        public List<CPAI_ACTION_CONTROL> findByCurrentStatusAndFunctionMB(string currentStatus, List<string> lstAcfFunction)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_CONTROL> results = entity.CPAI_ACTION_CONTROL.Where(x => x.ACT_CURRENT_STATUS.ToUpper() == currentStatus.ToUpper() && x.ACT_STATUS == "ACTIVE" && x.ACT_IGNORE_MB != "1").ToList();
                if (results != null && results.Count() > 0)
                {
                    var realResult = from a in results where a.ACT_FK_ACTION_FUNCTION != null && lstAcfFunction.Contains(a.ACT_FK_ACTION_FUNCTION) select a;
                    return realResult.ToList<CPAI_ACTION_CONTROL>();
                }
                else
                {
                    return new List<CPAI_ACTION_CONTROL>();
                }
            }
        }

        public Boolean Save(CPAI_ACTION_CONTROL actionControl)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_ACTION_CONTROL.Add(actionControl);
                    context.SaveChanges();
                };
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
