﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ActionFunctionDAL
    {
        public List<CPAI_ACTION_FUNCTION> findByUserGroupAndSystem(string userGroup, string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(x => x.ACF_USR_GROUP.ToUpper() == userGroup.ToUpper() && x.ACF_SYSTEM.ToUpper() == system.ToUpper() && x.ACF_STATUS.ToUpper() == "ACTIVE").ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }
        public List<CPAI_ACTION_FUNCTION> findByUserGroupAndSystemDelegate(List<string> lstUserGroup, string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                lstUserGroup = lstUserGroup.ConvertAll(d => d.ToUpper());
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(x => lstUserGroup.Contains(x.ACF_USR_GROUP.ToUpper()) && x.ACF_SYSTEM.ToUpper() == system.ToUpper() && x.ACF_STATUS.ToUpper() == "ACTIVE").ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }



        public List<CPAI_ACTION_FUNCTION> findByUserGroupAndSystemAndFuncitonId(string userGroup, string system, string fk_funciton_id)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {


                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(x => x.ACF_USR_GROUP.ToUpper() == userGroup.ToUpper() && x.ACF_SYSTEM.ToUpper() == system.ToUpper() && x.ACF_FK_FUNCTION.ToUpper() == fk_funciton_id.ToUpper() && x.ACF_STATUS.ToUpper() == "ACTIVE").ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }

        public List<CPAI_ACTION_FUNCTION> findByUserGroupAndSystemAndFuncitonIdDelegate(List<string> lstUserGroup, string system, string fk_funciton_id)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                lstUserGroup = lstUserGroup.ConvertAll(d => d.ToUpper());
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(x => lstUserGroup.Contains(x.ACF_USR_GROUP.ToUpper()) && x.ACF_SYSTEM.ToUpper() == system.ToUpper() && x.ACF_FK_FUNCTION.ToUpper() == fk_funciton_id.ToUpper() && x.ACF_STATUS.ToUpper() == "ACTIVE").ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }

        public List<CPAI_ACTION_FUNCTION> findByFunctionRowId(string functionRowId)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(x => x.ACF_FK_FUNCTION.ToUpper() == functionRowId.ToUpper() && x.ACF_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }

        public List<CPAI_ACTION_FUNCTION> findCPAIBunkerAcf(string functionRowId, string userGroup, string userRowId, string system, string type)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(
                    x => x.ACF_FK_FUNCTION.ToUpper() == functionRowId.ToUpper() &&
                    ((x.ACF_USR_GROUP.ToUpper() == userGroup.ToUpper() && x.ACF_FK_USER.ToUpper() == userRowId.ToUpper()) || x.ACF_USR_GROUP.ToUpper() == userGroup.ToUpper()) &&
                    x.ACF_SYSTEM.ToUpper() == system.ToUpper() &&
                    x.ACF_TYPE.ToUpper() == type.ToUpper()
                    && x.ACF_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }

        public List<CPAI_ACTION_FUNCTION> findCPAIBunkerAcfs(string functionRowId, string[] userGroup, string userRowId, string system, string type)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(
                    x => x.ACF_FK_FUNCTION.ToUpper() == functionRowId.ToUpper() &&
                    ((userGroup.Contains(x.ACF_USR_GROUP.ToUpper()) && x.ACF_FK_USER.ToUpper() == userRowId.ToUpper()) || userGroup.Contains(x.ACF_USR_GROUP.ToUpper())) &&
                    x.ACF_SYSTEM.ToUpper() == system.ToUpper() &&
                    x.ACF_TYPE.ToUpper() == type.ToUpper()
                    && x.ACF_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }

        public List<CPAI_ACTION_FUNCTION> findAcfDelegate(string functionRowId, List<string> lstUserGroup, string userRowId, string system, string type)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                lstUserGroup = lstUserGroup.ConvertAll(d => d.ToUpper());
                List<CPAI_ACTION_FUNCTION> results = entity.CPAI_ACTION_FUNCTION.Where(
                    x => x.ACF_FK_FUNCTION.ToUpper() == functionRowId.ToUpper() &&
                    (lstUserGroup.Contains(x.ACF_USR_GROUP.ToUpper())) &&
                    x.ACF_SYSTEM.ToUpper() == system.ToUpper() &&
                    x.ACF_TYPE.ToUpper() == type.ToUpper()
                    && x.ACF_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_FUNCTION>();
                }
            }
        }
    }
}
