
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Service;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ActionMessageDAL
    {

        public List<CPAI_ACTION_MESSAGE> findUserGroup(string action, string system, string type, string funciton)
        {
            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    var grouped = from p in results

                                  group p by p.AMS_USR_GROUP into g
                                  select g;



                    var realResult = new List<CPAI_ACTION_MESSAGE>();
                    for (int i = 0; i < grouped.Count(); i++)
                    {
                        var eachGroup = grouped.ElementAt(i);
                        if (eachGroup.Count() == 1)
                        {
                            realResult.Add(eachGroup.ElementAt(0));
                        }
                        else
                        {
                            List<CPAI_ACTION_MESSAGE> SortedList = eachGroup.OrderByDescending(o => o.AMS_FK_USER).ToList();
                            for (int j = 0; j < SortedList.Count; j++)
                            {
                                if (SortedList.ElementAt(j).AMS_FK_USER != null)
                                {
                                    realResult.Add(SortedList.ElementAt(j));
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    return realResult;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }

        public List<CPAI_ACTION_MESSAGE> findUserGroupMail_M05(string action, string system, string type, string funciton, string prevStatus, string chkSendMail_M05)
        {

            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = new List<CPAI_ACTION_MESSAGE>();
                results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                   x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                   x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                   x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                   x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                   x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                   x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }

        public List<CPAI_ACTION_MESSAGE> findUserGroupMail(string action, string system, string type, string funciton, string prevStatus, string charterFor)
        {

            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = new List<CPAI_ACTION_MESSAGE>();

                ////Flow 1,6,7,23,25,26
                if (action == "REJECT" || action == "CANCEL" || action == "REJECT_CON" || action == "REJECT_CON_SH")
                {
                    var query = (from a in entity.CPAI_ACTION_MESSAGE
                                 where a.AMS_ACTION.ToUpper() == action.ToUpper() &&
                                 a.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                                 a.AMS_TYPE.ToUpper() == type.ToUpper() &&
                                 a.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                                 a.AMS_MAIL_FLAG.ToUpper() == "Y" &&
                                 a.AMS_STATUS == "ACTIVE" &&
                                 a.AMS_CURRENT_STATUS.Contains(prevStatus)
                                 select a).ToList();

                    if (query.Count == 0)
                    {
                        if (prevStatus != "DRAFT")
                        {
                            results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                            x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                            x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                            x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                            x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                            x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                            x.AMS_STATUS == "ACTIVE" &&
                            x.AMS_CURRENT_STATUS == null).OrderBy(o => o.AMS_USR_GROUP).ToList();
                        }
                    }
                    else
                    {
                        results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                        x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                        x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                        x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                        x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                        x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                        x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();
                    }

                }
                else
                {
                    results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                    x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                    x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                    x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                    x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                    x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                    x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();
                }


                if (results != null && results.Count() > 0)
                {
                    if (funciton == "F10000025")
                    {
                        if (action == "REJECT_CON" && prevStatus == "WAITING_CONFIRM")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                else if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "REJECT_CON_SH" && prevStatus == "WAITING_CONFIRM_SH")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                                if (charterFor == "CMLA")
                                {
                                    if (z.AMS_USR_GROUP == "CMLA_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                else
                                {
                                    if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                    {
                                        if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "REJECT" && prevStatus == "WAITING CERTIFIED")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "CMVP" || z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                                if (charterFor == "CMLA")
                                {
                                    if (z.AMS_USR_GROUP == "CMLA_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                else
                                {
                                    if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                    {
                                        if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "REJECT" && prevStatus == "WAITING APPROVE")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "EVPC" || z.AMS_USR_GROUP == "CMVP" || z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                                if (charterFor == "CMLA")
                                {
                                    if (z.AMS_USR_GROUP == "CMLA_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                else
                                {
                                    if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                    {
                                        if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "APPROVE_2" && prevStatus == "WAITING_CONFIRM_SH")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "CMVP" || z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                                if (charterFor == "CMLA")
                                {
                                    if (z.AMS_USR_GROUP == "CMLA_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                else
                                {
                                    if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                    {
                                        if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "APPROVE_CON" || action == "APPROVE_CON_SH")
                        {
                            if (action == "APPROVE_CON")
                            {
                                List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                                foreach (var z in results)
                                {
                                    if (z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                    else if (z.AMS_USR_GROUP == charterFor)
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                return new_Results;
                            }
                            else
                            {
                                List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                                foreach (var z in results)
                                {
                                    if (z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                    if (charterFor == "CMLA")
                                    {
                                        if (z.AMS_USR_GROUP == "CMLA_SH")
                                        {
                                            new_Results.Add(z);
                                        }
                                        else if (z.AMS_USR_GROUP == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                    else
                                    {
                                        if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                        {
                                            if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                            {
                                                new_Results.Add(z);
                                            }
                                        }
                                        else if (z.AMS_USR_GROUP == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                                return new_Results;
                            }
                        }
                        else if (action == "APPROVE_3" && prevStatus == "WAITING CERTIFIED")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "EVPC" || z.AMS_USR_GROUP == "CMVP" || z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                                if (charterFor == "CMLA")
                                {
                                    if (z.AMS_USR_GROUP == "CMLA_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                else
                                {
                                    if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                    {
                                        if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "APPROVE_4" && prevStatus == "WAITING APPROVE")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "EVPC" || z.AMS_USR_GROUP == "CMVP" || z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                                if (z.AMS_USR_GROUP == charterFor)
                                {
                                    new_Results.Add(z);
                                }
                                if (charterFor == "CMLA")
                                {
                                    if (z.AMS_USR_GROUP == "CMLA_SH")
                                    {
                                        new_Results.Add(z);
                                    }
                                }
                                else
                                {
                                    if (z.AMS_USR_GROUP == "CMPSII_SH" || z.AMS_USR_GROUP == "CMPSDS_SH" || z.AMS_USR_GROUP == "CMPSIE_SH")
                                    {
                                        if (z.AMS_USR_GROUP.Substring(0, 6) == charterFor)
                                        {
                                            new_Results.Add(z);
                                        }
                                    }
                                }
                            }
                            return new_Results;
                        }
                        else if (action == "REJECT" && prevStatus == "WAITING VERIFY")
                        {
                            List<CPAI_ACTION_MESSAGE> new_Results = new List<CPAI_ACTION_MESSAGE>();
                            foreach (var z in results)
                            {
                                if (z.AMS_USR_GROUP == "CMMT" || z.AMS_USR_GROUP == "CMMT_SH")
                                {
                                    new_Results.Add(z);
                                }
                            }
                            return new_Results;
                        }
                        else
                        {
                            return results;
                        }
                    }
                    else
                    {
                        return results;
                    }

                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }

        public List<CPAI_ACTION_MESSAGE> findUserGroupMailDummy(string action, string system, string type, string funciton)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                x.AMS_FK_FUNCTION.ToUpper() == funciton.ToUpper() &&
                x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }

        public List<CPAI_ACTION_MESSAGE> findUserGroupNoti(string action, string system, string type, string funciton)
        {
            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                x.AMS_NOTI_FLAG.ToUpper() == "Y".ToUpper() &&
                x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    var grouped = from p in results

                                  group p by p.AMS_USR_GROUP into g
                                  select g;



                    var realResult = new List<CPAI_ACTION_MESSAGE>();
                    for (int i = 0; i < grouped.Count(); i++)
                    {
                        var eachGroup = grouped.ElementAt(i);
                        if (eachGroup.Count() == 1)
                        {
                            realResult.Add(eachGroup.ElementAt(0));
                        }
                        else
                        {
                            List<CPAI_ACTION_MESSAGE> SortedList = eachGroup.OrderByDescending(o => o.AMS_FK_USER).ToList();
                            for (int j = 0; j < SortedList.Count; j++)
                            {
                                if (SortedList.ElementAt(j).AMS_FK_USER != null)
                                {
                                    realResult.Add(SortedList.ElementAt(j));
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    return realResult;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }
    }
}