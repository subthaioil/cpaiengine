﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ActionMobileControlDAL
    {

        public List<CPAI_MOBILE_CONTROL> findByUserGroupTypeAndSystem(string userGroup, string system, string type)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_MOBILE_CONTROL> results = entity.CPAI_MOBILE_CONTROL.Where(x => x.AMC_USR_GROUP.ToUpper() == userGroup.ToUpper() 
                                                                                    && x.AMC_SYSTEM.ToUpper() == system.ToUpper()
                                                                                    && x.AMC_TYPE.ToUpper() == type.ToUpper()
                                                                                    && x.AMC_STATUS.ToUpper() == "ACTIVE").ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_MOBILE_CONTROL>();
                }
            }
        }

        public List<CPAI_MOBILE_CONTROL> findByUserGroupAndSystem(string userGroup, string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_MOBILE_CONTROL> results = entity.CPAI_MOBILE_CONTROL.Where(x => x.AMC_USR_GROUP.ToUpper() == userGroup.ToUpper()
                                                                                    && x.AMC_SYSTEM.ToUpper() == system.ToUpper()
                                                                                    && x.AMC_STATUS.ToUpper() == "ACTIVE").ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_MOBILE_CONTROL>();
                }
            }
        }

    }
}
