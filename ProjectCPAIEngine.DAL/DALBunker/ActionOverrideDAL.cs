﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ActionOverrideDAL
    {
        public List<CPAI_ACTION_OVERRIDE> findByFkActionControl(string fkActionControl)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_OVERRIDE> results = entity.CPAI_ACTION_OVERRIDE.Where(x => x.ACO_FK_ACTION_CONTROL.ToUpper() == fkActionControl.ToUpper() && x.ACO_STATUS == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_OVERRIDE>();
                }
            }
        }
    }
}
