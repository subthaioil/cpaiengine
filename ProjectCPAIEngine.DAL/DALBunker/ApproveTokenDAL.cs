﻿using System;
using System.Collections.Generic;
using ProjectCPAIEngine.DAL.Entity;
using System.Linq;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;
using System.Diagnostics;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ApproveTokenDAL
    {


        public void Save(CPAI_APPROVE_TOKEN approveToken)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    Debug.WriteLine(approveToken.TOK_CREATED_BY);
                    Debug.WriteLine(approveToken.TOK_CREATED_DATE);
                    Debug.WriteLine(approveToken.TOK_FK_USER);
                    Debug.WriteLine(approveToken.TOK_ROW_ID);
                    Debug.WriteLine(approveToken.TOK_STATUS);
                    Debug.WriteLine(approveToken.TOK_TOKEN);
                    Debug.WriteLine(approveToken.TOK_TRASACTION_ID);
                    Debug.WriteLine(approveToken.TOK_UPDATED_BY);
                    Debug.WriteLine(approveToken.TOK_UPDATED_DATE);
                    Debug.WriteLine(approveToken.TOK_USED_TYPE);
                    Debug.WriteLine(approveToken.TOK_USER_SYSTEM);
                   
                    context.CPAI_APPROVE_TOKEN.Add(approveToken);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}