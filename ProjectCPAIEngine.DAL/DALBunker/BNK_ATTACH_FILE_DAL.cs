﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class BNK_ATTACH_FILE_DAL
    {
        public void Save(BNK_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.BNK_ATTACH_FILE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(BNK_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.BNK_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BNK_ATTACH_FILE> GetFileByDataID(string BNKID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _bnkFile = context.BNK_ATTACH_FILE.Where(x => x.BAF_FK_BNK_DATA.ToUpper() == BNKID.ToUpper()).ToList();
                    return _bnkFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BNK_ATTACH_FILE> GetFileByDataID(string BNKID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _bnkFile = context.BNK_ATTACH_FILE.Where(x => x.BAF_FK_BNK_DATA.ToUpper() == BNKID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _bnkFile = _bnkFile.Where(x => x.BAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _bnkFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
