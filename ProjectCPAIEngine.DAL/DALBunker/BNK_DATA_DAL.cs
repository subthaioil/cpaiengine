﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class BNK_DATA_DAL
    {
        public void Save(BNK_DATA bnkData, EntityCPAIEngine context)
        {
            try
            {
                context.BNK_DATA.Add(bnkData);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteByTransactionId(string txId, EntityCPAIEngine context)
        {
            bool isSuccess = false;
            try
            {
                //string sql = "DELETE FROM BNK_DATA WHERE BDA_ROW_ID = :BDA_ROW_ID";
                //var result = context.Database.ExecuteSqlCommand(sql, new OracleParameter(":BDA_ROW_ID", txId));
                //context.SaveChanges();
                context.Database.ExecuteSqlCommand("DELETE FROM BNK_DATA WHERE BDA_ROW_ID = '" + txId + "'");
                context.SaveChanges();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public void Update(BNK_DATA data, EntityCPAIEngine context)
        {
            try
            {
               
                    var _search = context.BNK_DATA.Find(data.BDA_ROW_ID);
                    #region Set Value
                    _search.BDA_REASON = data.BDA_REASON;
                    //if (data.BDA_EXPLANATION != null)
                    //{
                    //        _search.BDA_EXPLANATION = data.BDA_EXPLANATION;
                    //}
                    //if (data.BDA_NOTE != null)
                    //{
                    //        _search.BDA_NOTE = data.BDA_NOTE;
                    //}
                    _search.BDA_STATUS = data.BDA_STATUS;
                    _search.BDA_UPDATED_BY = data.BDA_UPDATED_BY;
                    _search.BDA_UPDATED = data.BDA_UPDATED;
                    #endregion
                    context.SaveChanges();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void UpdateExplanation(BNK_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.BNK_DATA.Find(data.BDA_ROW_ID);
                #region Set Value
                if (_search != null)
                {
                    _search.BDA_EXPLANATION = data.BDA_EXPLANATION;
                    _search.BDA_UPDATED_BY = data.BDA_UPDATED_BY;
                    _search.BDA_UPDATED = data.BDA_UPDATED;
                    #endregion
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNote(BNK_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.BNK_DATA.Find(data.BDA_ROW_ID);
                #region Set Value
                if(_search != null)
                {
                    _search.BDA_NOTE = data.BDA_NOTE;
                    _search.BDA_UPDATED_BY = data.BDA_UPDATED_BY;
                    _search.BDA_UPDATED = data.BDA_UPDATED;
                    #endregion
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BNK_DATA GetByID(string BDA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.BNK_DATA.Where(x => x.BDA_ROW_ID.ToUpper() == BDA_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
