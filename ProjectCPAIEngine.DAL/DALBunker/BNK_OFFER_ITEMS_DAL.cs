﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using ProjectCPAIEngine.DAL.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class BNK_OFFER_ITEMS_DAL
    {
        public void Save(BNK_OFFER_ITEMS bnkOfferItems, EntityCPAIEngine context)
        {
            try
            {
                context.BNK_OFFER_ITEMS.Add(bnkOfferItems);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BNK_OFFER_ITEMS> GetByID(string BDA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.BNK_OFFER_ITEMS.Where(x => x.BOI_FK_BNK_DATA.ToUpper() == BDA_ROW_ID.ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
