﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class BNK_PRODUCT_ITEMS_DAL
    {
        public void Save(BNK_PRODUCT_ITEMS bnkProductItems, EntityCPAIEngine context)
        {
            try
            {
                context.BNK_PRODUCT_ITEMS.Add(bnkProductItems);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BNK_PRODUCT_ITEMS> GetProductItems()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from p in context.BNK_PRODUCT_ITEMS select p).Distinct();
                return query.ToList();
            }
        }

        public List<BNK_PRODUCT_ITEMS> GetByID(string BDA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.BNK_PRODUCT_ITEMS.Where(x => x.BPI_FK_BNK_DATA.ToUpper() == BDA_ROW_ID.ToUpper()).OrderBy(x => x.BPI_ORDER).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
