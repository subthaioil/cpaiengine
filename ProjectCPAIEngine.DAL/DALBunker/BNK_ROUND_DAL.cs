﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class BNK_ROUND_DAL
    {
        public void Save(BNK_ROUND bnkRound, EntityCPAIEngine context)
        {
            try
            {
                context.BNK_ROUND.Add(bnkRound);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BNK_ROUND> GetByID(string BRI_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.BNK_ROUND.Where(x => x.BRD_FK_ROUND_ITEMS.ToUpper() == BRI_ROW_ID.ToUpper()).OrderBy(x => x.BRD_ROUND_ORDER).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
