﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class BNK_ROUND_ITEMS_DAL
    {
        public void Save(BNK_ROUND_ITEMS bnkRoundItems, EntityCPAIEngine context)
        {
            try
            {
                context.BNK_ROUND_ITEMS.Add(bnkRoundItems);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BNK_ROUND_ITEMS> GetByID(string BOI_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.BNK_ROUND_ITEMS.Where(x => x.BRI_FK_OFFER_ITEMS.ToUpper() == BOI_ROW_ID.ToUpper()).OrderBy(x => x.BRI_ROUND_NO).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
