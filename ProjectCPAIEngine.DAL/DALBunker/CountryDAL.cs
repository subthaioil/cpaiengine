﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class CountryDAL
    {
        public static MT_COUNTRY getCountryByAbbrv(string abbrv)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_COUNTRY
                             where v.MCT_LAND1 == abbrv
                             select v).FirstOrDefault();
                return query;
            }
        }
    }
}
