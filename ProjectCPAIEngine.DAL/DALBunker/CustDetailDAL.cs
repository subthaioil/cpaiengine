﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class CustDetailDAL
    {
        public static List<MT_CUST_DETAIL> GetCustDetail(string system, string status, string type)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_CUST_DETAIL
                            join vc in context.MT_CUST_CONTROL on v.MCD_ROW_ID equals vc.MCR_FK_CUST_CONTROL_ID
                            where vc.MCR_SYSTEM.ToUpper() == system && vc.MCR_STATUS.ToUpper() == status && vc.MCR_TYPE.ToUpper() == type && v.MCD_NATION.ToUpper() == "I"
                            select v).Distinct().OrderBy(m => m.MCD_NAME_1).ToList();
                            List<MT_CUST_DETAIL> custs = query.ToList();
                            //List<MT_CUST_DETAIL> distinct =  custs
                            //                                .GroupBy(MT_CUST_DETAIL => MT_CUST_DETAIL.MCD_FK_CUS)
                            //                                .Select(g => g.First())
                            //                                .ToList();
                return custs;
            }
        }


        public static MT_CUST_DETAIL GetCustDetailById(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_CUST_DETAIL> results = context.MT_CUST_DETAIL.Where(x => x.MCD_FK_CUS.ToUpper() == id.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_CUST_DETAIL();
                }
            }
        }

        public static MT_CUST_DETAIL GetCustDetailByRowId(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_CUST_DETAIL> results = context.MT_CUST_DETAIL.Where(x => x.MCD_ROW_ID.ToUpper() == id.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_CUST_DETAIL();
                }
            }
        }

        public static string GetCustSortNameById(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string results = context.MT_CUST_DETAIL.Where(x => x.MCD_ROW_ID.ToUpper() == id.ToUpper()).Select(y => y.MCD_SORT_FIELD).FirstOrDefault();
                if (results != null && results != "")
                {
                    return results;
                }
                else
                {
                    return "";
                }
            }
        }

        // Get Data Vendor Master and Color code
        public static List<CustColor> GetCustomerColor(string type, string status, string nation)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from vc in context.MT_CUST_CONTROL.Where(p => p.MCR_SYSTEM.ToUpper().Equals("CPAI") && p.MCR_TYPE.ToUpper().Equals(type) && p.MCR_STATUS.ToUpper().Equals(status) )
                            join v in context.MT_CUST_DETAIL on vc.MCR_FK_CUST_CONTROL_ID equals v.MCD_ROW_ID
                            select new CustColor
                            {
                                CUSTOMER = v,
                                CUSTOMER_COLOR_CODE = vc.MCR_COLOR_CODE
                            };
                if (!string.IsNullOrEmpty(nation))
                    query = query.Where(p => p.CUSTOMER.MCD_NATION.ToUpper().Equals(nation));

                return query.Distinct().ToList();
            }
        }

    }
}
