﻿using System;
using System.Collections.Generic;
using ProjectCPAIEngine.DAL.Entity;
using System.Linq;
using Oracle.ManagedDataAccess.Client;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class DataHistoryDAL
    {

        public CPAI_DATA_HISTORY findByDthRowId(String dthRowId)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_ROW_ID == dthRowId).ToList();
                if (results != null && (results.Count() == 1))
                {
                    return results[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public List<CPAI_DATA_HISTORY> findByDthTxnRef(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }

        public List<CPAI_DATA_HISTORY> findApproveByDthTxnRef(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef && x.DTH_REJECT_FLAG == "N").ToList();
                if (results != null)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }

        //public void Update(StateModel stateModel, DATA_HISTORY dataHistory)
        //{
        //    try
        //    {
        //        using (var context = new EntityCPAIEngine())
        //        {
        //            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
        //            DateTime now = DateTime.Now;

        //            context.DATA_HISTORY.
        //            context.SaveChanges();
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        public void UpdateRejectFlag(string txRef, string updateBy, string flag)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    DateTime now = DateTime.Now;
                    //"update mytable set timestamp = TO_DATE ('" + DateTime.Now.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')"
                    //string sql = "UPDATE CPAI_DATA_HISTORY SET DTH_REJECT_FLAG = :Flag, DTH_UPDATED_BY = :UpdateBy, DTH_UPDATED_DATE = TO_DATE ('" + DateTime.Now.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')  WHERE DTH_TXN_REF = :TxRef";
                    string sql = "UPDATE CPAI_DATA_HISTORY SET DTH_REJECT_FLAG = :Flag, DTH_UPDATED_BY = :UpdateBy, DTH_UPDATED_DATE = TO_DATE ('" + DateTime.Now.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')  WHERE DTH_TXN_REF = :TxRef";

                    /*var args = new DbParameter[] {
                         new OracleParameter { ParameterName = ":Flag", Value = flag },
                         new OracleParameter { ParameterName = ":TxRef", Value = txRef },
                         new OracleParameter { ParameterName = ":UpdateBy", Value = updateBy }
                      };
                     var result = context.Database.ExecuteSqlCommand(sql, args);*/

                    /* OracleParameter pm1 = new OracleParameter(":Flag", flag);
                   OracleParameter pm2 = new OracleParameter(":TxRef", txRef);
                   OracleParameter pm3 = new OracleParameter(":UpdateBy", updateBy);
                   object[] parameters = new object[] { pm1, pm2, pm3 };
                    var result = context.Database.ExecuteSqlCommand(sql, parameters);*/


                    var result = context.Database.ExecuteSqlCommand(
                       sql,
                       new OracleParameter(":Flag", flag),
                       new OracleParameter(":UpdateBy", updateBy),
                       new OracleParameter(":TxRef", txRef));

                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateRejectFlag2(string txRef, string updateBy, string flag)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var historyData = context.CPAI_DATA_HISTORY.Where(a => a.DTH_TXN_REF == txRef && a.DTH_REJECT_FLAG == "N").ToList();
                            foreach (var item in historyData)
                            {
                                if (item.DTH_ACTION == "APPROVE_1" || item.DTH_ACTION == "APPROVE_CON")
                                {
                                    item.DTH_REJECT_FLAG = "N";
                                    item.DTH_UPDATED_DATE = DateTime.Now;
                                    item.DTH_UPDATED_BY = updateBy;
                                    context.SaveChanges();                                   
                                }
                                else
                                {
                                    item.DTH_REJECT_FLAG = "Y";
                                    item.DTH_UPDATED_DATE = DateTime.Now;
                                    item.DTH_UPDATED_BY = updateBy;
                                    context.SaveChanges();
                                }
                            }
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback(); 
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_DATA_HISTORY dataHistory)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_DATA_HISTORY.Add(dataHistory); //   .AddObject(dataHistory);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}