﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class FunctionTransactionDAL
    {
        private const string BUNKER_FUNCTION_CODE = "1";
        private const string CHARTER_IN_FUNCTION_CODE = "6";
        private const string CHARTER_OUT_CMMT_FUNCTION_CODE = "7";
        private const string FREIGHT_FUNCTION_CODE = "8";
        private const string SCHEDULE_FUNCTION_CODE = "9";
        private const string TCE_WS_FUNCTION_CODE = "12";
        private const string TCE_MK_FUNCTION_CODE = "15";
        private const string CRUDE_PURCHASE_FUNCTION_CODE = "23";
        private const string CHARTER_IN_CMMT_FUNCTION_CODE = "25";
        private const string CHARTER_OUT_FUNCTION_CODE = "26";
        private const string SCHEDULE_S_FUNCTION_CODE = "34";
        private const string COOL_FUNCTION_CODE = "40";
        private const string COOL_EXPERT_FUNCTION_CODE = "41";
        private const string COOL_EXPERT_SH_FUNCTION_CODE = "42";
        private const string VCOOL_FUNCTION_CODE = "63";
        private const string VCOOL_EXPERT_FUNCTION_CODE = "64";
        private const string VCOOL_EXPERT_SH_FUNCTION_CODE = "65";

        private const string HEDG_DEAL_FUNCTION_CODE = "37";
        private const string HEDG_TICKET_FUNCTION_CODE = "38";
        private const string PRE_DEAL_FUNCTION_CODE = "53";
        private const string HEDG_ANNUAL_FUNCTION_CODE = "70";
        private const string HEDG_STRCMT_FUNCTION_CODE = "73";



        public List<FunctionTransaction> findCPAIBunkerTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                // get list of FUNCTION_TRANSACTION
                if (lstStatus.Count == 1 && lstStatus[0].Equals("ALL"))
                {
                    list = entity.FUNCTION_TRANSACTION.Where(
                        x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper()
                             && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW" 
                        ).OrderByDescending(m => m.FTX_UPDATED).ToList();
                }
                else
                {
                    list = entity.FUNCTION_TRANSACTION.Where(
                        x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper()
                        && lstStatus.Contains(x.FTX_INDEX4.ToUpper())).OrderByDescending(m => m.FTX_UPDATED).ToList();
                }

                // filter data by input parameters
                list = filterDataByIndex(list, lstIndexValues);

                // find name by index id
                results = findName(list, ACF_FK_FUNCTION);

                if (results != null && results.Count() > 0)
                {
                    if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                    {
                        return results;
                    }
                    else
                    {
                        DateTime fDate = parseInputDate(fromDate);
                        DateTime tDate = parseInputDate(toDate);
                        var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null && (parsePurchaseDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate) && (parsePurchaseDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) <= tDate) select a;
                        return realResult.ToList();
                    }
                }
                else
                {
                    return new List<FunctionTransaction>();
                }
            }
        }

        public List<FUNCTION_TRANSACTION> findByTransactionId(string transactionId)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FUNCTION_TRANSACTION> results = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_TRANS_ID.ToUpper() == transactionId.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<FUNCTION_TRANSACTION>();
                }
            }
        }

        public List<FUNCTION_TRANSACTION> findByTransactionIdSystemType(string transactionId, string system, string type)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FUNCTION_TRANSACTION> results = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_TRANS_ID.ToUpper() == transactionId.ToUpper() && x.FTX_INDEX1.ToUpper() == system.ToUpper() && x.FTX_INDEX2.ToUpper() == type.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<FUNCTION_TRANSACTION>();
                }
            }
        }

        public List<FUNCTION_TRANSACTION> findByTransactionIdDealNo(string dealNo)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FUNCTION_TRANSACTION> results = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_INDEX6.ToUpper() == dealNo).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<FUNCTION_TRANSACTION>();
                }
            }
        }

        public List<FunctionTransaction> findCPAISchTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                // get list of FUNCTION_TRANSACTION
                if (lstStatus.Count == 1 && lstStatus[0].Equals("ALL"))
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper()
                    && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW"
                    ).ToList();
                }
                else
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper() &&
                                                             lstStatus.Contains(x.FTX_INDEX4.ToUpper())).ToList();
                }

                // filter data by input parameters
                list = filterDataByIndex(list, lstIndexValues);

                // find name by index id
                results = findName(list, ACF_FK_FUNCTION);

                if (results != null && results.Count() > 0)
                {
                    if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                    {
                        return results;
                    }
                    else
                    {
                        DateTime fDate = parseInputDate(fromDate);
                        DateTime tDate = parseInputDate(toDate);
                        var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null && a.FUNCTION_TRANSACTION.FTX_INDEX7 != null && (parsePurchaseDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate) && (parsePurchaseDate(a.FUNCTION_TRANSACTION.FTX_INDEX7) <= tDate) select a;
                        return realResult.ToList();
                    }
                }
                else
                {
                    return new List<FunctionTransaction>();
                }
            }
        }

        public List<FunctionTransaction> findCPAISchCMCSTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                // get list of FUNCTION_TRANSACTION
                if (lstStatus.Count == 1 && lstStatus[0].Equals("ALL"))
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper()
                    && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW"
                    ).ToList();
                }
                else
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper() &&
                                                             lstStatus.Contains(x.FTX_INDEX4.ToUpper())).ToList();
                }

                // filter data by input parameters
                list = filterDataByIndex(list, lstIndexValues);

                // find name by index id
                results = findName(list, ACF_FK_FUNCTION);

                if (results != null && results.Count() > 0)
                {
                    if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                    {
                        return results;
                    }
                    else
                    {
                        DateTime fDate = parseInputDate(fromDate);
                        DateTime tDate = parseInputDate(toDate);
                        var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null && a.FUNCTION_TRANSACTION.FTX_INDEX7 != null && (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6, "dd/MM/yyyy HH:mm") >= fDate) && (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX7, "dd/MM/yyyy HH:mm") <= tDate) select a;
                        return realResult.ToList();
                    }
                }
                else
                {
                    return new List<FunctionTransaction>();
                }
            }
        }

        public List<FunctionTransaction> findCPAITceTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                // get list of FUNCTION_TRANSACTION
                if (lstStatus.Count == 1 && lstStatus[0].Equals("ALL"))
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper()
                     && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW").ToList();
                }
                else
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper() &&
                                                             lstStatus.Contains(x.FTX_INDEX4.ToUpper())).ToList();
                }

                // filter data by input parameters
                list = filterDataByIndex(list, lstIndexValues);

                // find name by index id
                results = findName(list, ACF_FK_FUNCTION);

                if (results != null && results.Count() > 0)
                {
                    //if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                    //{
                    //    return results;
                    //}
                    //else
                    //{
                    //    DateTime fDate = parseInputDate(fromDate);
                    //    DateTime tDate = parseInputDate(toDate);
                    //    var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null && a.FUNCTION_TRANSACTION.FTX_INDEX7 != null && (parsePurchaseDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate) && (parsePurchaseDate(a.FUNCTION_TRANSACTION.FTX_INDEX7) <= tDate) select a;
                    //    return realResult.ToList();
                    //}
                    return results;
                }
                else
                {
                    return new List<FunctionTransaction>();
                }
            }
        }

        public List<FunctionTransaction> findCPAICoolTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate, List<string> units, List<string> areas, bool isTracking,string UserName, bool isBoarding)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                // get list of FUNCTION_TRANSACTION
                if (lstStatus.Count == 1 && lstStatus[0].Equals("ALL"))
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == COOL_FUNCTION_CODE.ToUpper()
                    && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW").ToList();
                }
                else
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == COOL_FUNCTION_CODE.ToUpper() &&
                                                             lstStatus.Contains(x.FTX_INDEX4.ToUpper())).ToList();
                }

                // filter data by input parameters
                list = filterDataByIndex(list, lstIndexValues);

                List<FUNCTION_TRANSACTION> child_list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper()
                    && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW" && x.FTX_PARENT_TRX_ID != null).ToList();

                // find name by index id
                results = findNameCool(list, child_list, ACF_FK_FUNCTION, units, areas, isTracking, UserName, isBoarding);



                if (results != null && results.Count() > 0)
                {
                    if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                    {
                        return results;
                    }
                    else
                    {
                        DateTime fDate = parseInputDate(fromDate);
                        DateTime tDate = parseInputDate(toDate);

                        var realResult = from a in results
                                         where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null
                                         //&& (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate)
                                         //&& (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) <= tDate)
                                         select a;

                        var realResultList = realResult.ToList();
                        realResultList = realResultList.Where(a => (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate) && (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) <= tDate)).ToList();

                        return realResultList.ToList();

                        //var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null && (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate) && (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) <= tDate) select a;
                        //return realResult.ToList();
                    }
                }
                else
                {
                    return new List<FunctionTransaction>();
                }
            }
        }

        public List<FunctionTransaction> findCPAIHedgTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                if (lstStatus.Count == 1 && lstStatus[0].ToUpper().Equals("ALL"))
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper() && (x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW")).OrderByDescending(a => a.FTX_INDEX5).OrderBy(b => b.FTX_INDEX6).ToList();
                else
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == ACF_FK_FUNCTION.ToUpper() && lstStatus.Contains(x.FTX_INDEX4.ToLower())).OrderByDescending(a => a.FTX_INDEX5).OrderBy(b => b.FTX_INDEX6).ToList();

                list = filterDataByIndex(list, lstIndexValues);
                results = findNameHedge(list, ACF_FK_FUNCTION);

                if (results != null && results.Count() > 0)
                    return results;
                else
                    return new List<FunctionTransaction>();
            }
        }

        public List<FunctionTransaction> findCPAIVcoolTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<FunctionTransaction> results = new List<FunctionTransaction>();
                List<FUNCTION_TRANSACTION> list;

                // get list of FUNCTION_TRANSACTION
                if (lstStatus.Count == 1 && lstStatus[0].Equals("ALL"))
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW"
                    && ((x.FTX_FK_FUNCTION.ToUpper() == VCOOL_FUNCTION_CODE.ToUpper() && x.FTX_INDEX4 != "CONSENSUS APPROVAL")
                    || (x.FTX_FK_FUNCTION.ToUpper() == VCOOL_EXPERT_FUNCTION_CODE.ToUpper())
                    || (x.FTX_FK_FUNCTION.ToUpper() == VCOOL_EXPERT_SH_FUNCTION_CODE.ToUpper()))).ToList();

                    FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
                    for (int i = list.Count() - 1; i >= 0; i--)
                    {
                        if (list[i].FTX_FK_FUNCTION == VCOOL_EXPERT_FUNCTION_CODE || list[i].FTX_FK_FUNCTION == VCOOL_EXPERT_SH_FUNCTION_CODE)
                        {
                            FUNCTION_TRANSACTION parentFunction = ftxMan.GetFnTranDetail(list[i].FTX_PARENT_TRX_ID);
                            if (parentFunction != null && parentFunction.FTX_INDEX4 != "CONSENSUS APPROVAL")
                            {
                                list.RemoveAt(i);
                            }
                        }
                    }

                }
                else
                {
                    list = entity.FUNCTION_TRANSACTION.Where(x => lstStatus.Contains(x.FTX_INDEX4.ToUpper())
                    && ((x.FTX_FK_FUNCTION.ToUpper() == VCOOL_FUNCTION_CODE.ToUpper() && x.FTX_INDEX4 != "CONSENSUS APPROVAL")
                    || x.FTX_FK_FUNCTION.ToUpper() == VCOOL_EXPERT_FUNCTION_CODE.ToUpper() 
                    || x.FTX_FK_FUNCTION.ToUpper() == VCOOL_EXPERT_SH_FUNCTION_CODE.ToUpper())).ToList();

                    FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
                    for (int i = list.Count() - 1; i >= 0; i--)
                    {
                        if (list[i].FTX_FK_FUNCTION == VCOOL_EXPERT_FUNCTION_CODE || list[i].FTX_FK_FUNCTION == VCOOL_EXPERT_SH_FUNCTION_CODE)
                        {
                            FUNCTION_TRANSACTION parentFunction = ftxMan.GetFnTranDetail(list[i].FTX_PARENT_TRX_ID);
                            if (parentFunction != null && parentFunction.FTX_INDEX4 != "CONSENSUS APPROVAL")
                            {
                                list.RemoveAt(i);
                            }
                        }
                    }
                }

                // filter data by input parameters
                list = filterDataByIndex(list, lstIndexValues);

                // find name by index id
                results = findNameVcool(list, ACF_FK_FUNCTION);

                if (results != null && results.Count() > 0)
                {
                    if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                    {
                        return results;
                    }
                    else
                    {
                        //DateTime fDate = parseInputDate(fromDate, "dd-MMM-yyyy");

                        DateTime fDate = parseInputDate(fromDate);
                        DateTime tDate = parseInputDate(toDate);
                        var realResult = from a in results
                                         where a.FUNCTION_TRANSACTION.FTX_INDEX6 != null
                                         //&& (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate)
                                         //&& (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) <= tDate)
                                         select a;

                        var realResultList = realResult.ToList();
                        realResultList = realResultList.Where(a => (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) >= fDate) && (parseInputDate(a.FUNCTION_TRANSACTION.FTX_INDEX6) <= tDate)).ToList();

                        return realResultList.ToList();
                    }
                }
                else
                {
                    return new List<FunctionTransaction>();
                }
            }
        }

        #region Utils
        public DateTime parsePurchaseDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", null, DateTimeStyles.None);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime parseSearchMonth(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "MM/yyyy", CultureInfo.CurrentCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = new DateTime();
                if (dateTimeString.Contains(':') && dateTimeString.Contains('-') && dateTimeString.Contains('.'))
                {
                    r = DateTime.ParseExact(dateTimeString.Substring(0,20), "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else if (dateTimeString.Contains(':') && dateTimeString.Contains('/') && dateTimeString.Length >= 19)
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else if (dateTimeString.Contains(':') && dateTimeString.Contains('/'))
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                }
                else if (dateTimeString.Contains(':') && dateTimeString.Contains('-') && dateTimeString.Length >= 20)
                {
                    r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else if (dateTimeString.Contains(':') && dateTimeString.Contains('-'))
                {
                    r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                }
                else if (dateTimeString.Contains('-'))
                {
                    r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                //DateTime r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime parseDateForCool(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime parseDateForVCool(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                return DateTime.MinValue;
            }
        }

        public DateTime parseDateForTenorHedging(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "MMM yy", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                return DateTime.MinValue;
            }
        }

        public DateTime parseInputDate(string date, string format)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, format, CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<FunctionTransaction> findName(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                if (ACF_FK_FUNCTION.Equals(BUNKER_FUNCTION_CODE))
                {
                    IEnumerable<FunctionTransaction> temp = from ftx in list
                                                            join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                                                            from v in v2.DefaultIfEmpty() // outer join
                                                            join s in cpaiEntity.MT_VENDOR on ftx.FTX_INDEX12 equals s.VND_ACC_NUM_VENDOR into s2
                                                            from s in s2.DefaultIfEmpty()
                                                            join b in cpaiEntity.BNK_DATA on ftx.FTX_TRANS_ID equals b.BDA_ROW_ID into b2
                                                            from b in b2.DefaultIfEmpty()
                                                            select new FunctionTransaction
                                                            {
                                                                FUNCTION_TRANSACTION = ftx,
                                                                VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                                                                SUPPLIER_NAME = s == null ? "" : s.VND_NAME1,
                                                                DELIVERY_DATE_FROM = b == null ? "" : b.BDA_DELIVERY_DATE_FROM == null || b.BDA_DELIVERY_DATE_FROM == DateTime.MinValue ? "" : b.BDA_DELIVERY_DATE_FROM.Value.ToString("dd/MM/yyyy"),
                                                                DELIVERY_DATE_TO = b == null ? "" : b.BDA_DELIVERY_DATE_TO == null || b.BDA_DELIVERY_DATE_TO == DateTime.MinValue ? "" : b.BDA_DELIVERY_DATE_TO.Value.ToString("dd/MM/yyyy"),
                                                                CURRENCY_SYMBOL = b == null ? "" : b.BDA_CURRENCY_SYMBOL
                                                            };

                    results = temp.ToList();

                    var item_data = from ftx in results
                                    join oi in cpaiEntity.BNK_OFFER_ITEMS on ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID equals oi.BOI_FK_BNK_DATA
                                    where oi.BOI_FINAL_FLAG == "Y"
                                    join ri in cpaiEntity.BNK_ROUND_ITEMS on oi.BOI_ROW_ID equals ri.BRI_FK_OFFER_ITEMS
                                    join r in cpaiEntity.BNK_ROUND on ri.BRI_ROW_ID equals r.BRD_FK_ROUND_ITEMS
                                    select new
                                    {
                                        ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID,
                                        ri.BRI_ROUND_NO,
                                        r.BRD_ROUND_ORDER,
                                        r.BRD_ROUND_TYPE,
                                        r.BRD_ROUND_VALUE
                                    };

                    for (int i = 0; i < results.Count; i++)
                    {
                        var products = item_data.Where(x => x.FTX_TRANS_ID == results[i].FUNCTION_TRANSACTION.FTX_TRANS_ID).Select(x => x.BRD_ROUND_TYPE).Distinct();
                        foreach (var product in products)
                        {
                            var items = item_data.Where(x => x.FTX_TRANS_ID == results[i].FUNCTION_TRANSACTION.FTX_TRANS_ID && x.BRD_ROUND_TYPE == product).OrderByDescending(x => x.BRI_ROUND_NO).OrderByDescending(x => x.BRD_ROUND_ORDER).Take(1).ToList();
                            results[i].UNIT_PRICE_ORDER += items[0].BRD_ROUND_ORDER + "|";
                            results[i].UNIT_PRICE_VALUE += items[0].BRD_ROUND_VALUE + "|";
                            results[i].UNIT_PRICE_TYPE += items[0].BRD_ROUND_TYPE + "|";
                        }
                    }
                }
                else if (ACF_FK_FUNCTION.Equals(CHARTER_IN_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                               from v in v2.DefaultIfEmpty() // outer join
                               join c in cpaiEntity.MT_VENDOR on ftx.FTX_INDEX11 equals c.VND_ACC_NUM_VENDOR into c2
                               from c in c2.DefaultIfEmpty() // outer join
                               join d in cpaiEntity.CHI_DATA on ftx.FTX_TRANS_ID equals d.IDA_ROW_ID into d2
                               from d in d2.DefaultIfEmpty() // outer join
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                                   BROKER_NAME = c == null ? "" : c.VND_NAME1,
                                   EXTEN_COST = d == null ? "" : d.IDA_P_EXTEN_COST,
                                   EST_FREIGHT = d == null ? "" : d.IDA_P_EST_FREIGHT
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(CHARTER_IN_CMMT_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join c in cpaiEntity.MT_VENDOR on ftx.FTX_INDEX11 equals c.VND_ACC_NUM_VENDOR into c2
                               from c in c2.DefaultIfEmpty() // outer join
                               join d in cpaiEntity.CHI_DATA on ftx.FTX_TRANS_ID equals d.IDA_ROW_ID into d2
                               from d in d2.DefaultIfEmpty() // outer join
                               join e in cpaiEntity.MT_CUST_DETAIL on ftx.FTX_INDEX10 equals e.MCD_ROW_ID into e2
                               from e in e2.DefaultIfEmpty() // outer join
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   BROKER_NAME = c == null ? "" : c.VND_NAME1,
                                   EXTEN_COST = d == null ? "" : d.IDA_P_EXTEN_COST,
                                   CHARTERER_NAME = e == null ? "" : e.MCD_NAME_1,
                                   EST_FREIGHT = d == null ? "" : d.IDA_P_EST_FREIGHT
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                //else if (ACF_FK_FUNCTION.Equals(CHARTER_IN_CMMT_FUNCTION_CODE))
                //{
                //    var temp = from ftx in list
                //               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                //               from v in v2.DefaultIfEmpty() // outer join
                //               join d in cpaiEntity.CHI_DATA on ftx.FTX_TRANS_ID equals d.IDA_ROW_ID
                //               select new FunctionTransaction
                //               {
                //                   FUNCTION_TRANSACTION = ftx,
                //                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                //                   BROKER_NAME = ftx == null ? "" : ftx.FTX_INDEX11,
                //                   EXTEN_COST = d == null ? "" : d.IDA_P_EXTEN_COST,
                //                   EST_FREIGHT = d == null ? "" : d.IDA_P_EST_FREIGHT
                //               };
                //    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                //}
                else if (ACF_FK_FUNCTION.Equals(CHARTER_OUT_CMMT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(CHARTER_OUT_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                               from v in v2.DefaultIfEmpty() // outer join
                               join c in cpaiEntity.MT_CUST_DETAIL on ftx.FTX_INDEX11 equals c.MCD_ROW_ID into c2
                               from c in c2.DefaultIfEmpty() // outer join
                               join vd in cpaiEntity.MT_VENDOR on ftx.FTX_INDEX10 equals vd.VND_ACC_NUM_VENDOR into vd2
                               from vd in vd2.DefaultIfEmpty()
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                                   CHARTERER_NAME = c == null ? "" : c.MCD_NAME_1,
                                   BROKER_NAME = vd == null ? "" : vd.VND_NAME1
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(FREIGHT_FUNCTION_CODE))
                {
                    //broker = cust index_12
                    //charterer =cust index_10
                    //port = port  index_13 ,index_14
                    //cargo = MT_MATERIALS index_11
                    var temp = from ftx in list

                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                               from v in v2.DefaultIfEmpty() // outer join

                                   //join m in cpaiEntity.CPAI_FREIGHT_CARGO on ftx.FTX_TRANS_ID equals m.FDC_FK_FREIGHT_DATA into m2
                                   //from m in m2.DefaultIfEmpty()

                               join c in cpaiEntity.MT_CUST_DETAIL on ftx.FTX_INDEX10 equals c.MCD_ROW_ID into c2
                               from c in c2.DefaultIfEmpty()
                                   //.Where(z => z.MCD_NATION == "I")
                                   //.DefaultIfEmpty()

                               join b in cpaiEntity.MT_VENDOR on ftx.FTX_INDEX12 equals b.VND_ACC_NUM_VENDOR into b2
                               from b in b2.DefaultIfEmpty()

                               join d in cpaiEntity.MT_PORT on ftx.FTX_INDEX13 equals d.MLP_LOADING_PORT_ID.ToString() into d2
                               from d in d2.DefaultIfEmpty()

                               join l in cpaiEntity.MT_PORT on ftx.FTX_INDEX14 equals l.MLP_LOADING_PORT_ID.ToString() into l2
                               from l in l2.DefaultIfEmpty()

                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                                   CHARTERER_NAME = c == null ? "" : c.MCD_NAME_1,
                                   BROKER_NAME = b == null ? "" : b.VND_NAME1,
                                   //CARGO_NAME = m == null ? "" : m.MET_MAT_DES_ENGLISH,
                                   DISCHARGE_PORT_NAME = d == null ? "" : d.MLP_LOADING_PORT_NAME,
                                   LOAD_PORT_NAME = l == null ? "" : l.MLP_LOADING_PORT_NAME
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(SCHEDULE_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                               from v in v2.DefaultIfEmpty() // outer join
                               join c in cpaiEntity.MT_CUST_DETAIL on ftx.FTX_INDEX10 equals c.MCD_ROW_ID into c2
                               from c in c2.DefaultIfEmpty()
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                                   SUPPLIER_NAME = c == null ? "" : c.MCD_NAME_1
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(SCHEDULE_S_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(TCE_WS_FUNCTION_CODE))
                {
                    var tempCharter = from coi in cpaiEntity.CHI_OFFER_ITEMS.Where(c => c.IOI_FINAL_FLAG == "Y")
                                      join b in cpaiEntity.MT_VENDOR
                                      on coi.IOI_FK_VENDOR equals b.VND_ACC_NUM_VENDOR into c2
                                      from c in c2.DefaultIfEmpty()

                                      select new FunctionTransaction
                                      {
                                          CHARTERER_NAME = coi == null ? "" : coi.IOI_FK_CHI_DATA,
                                          BROKER_NAME = c == null ? "" : c.VND_NAME1
                                      };

                    var temp = from ftx in list

                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                               from v in v2.DefaultIfEmpty() // outer join

                               join c in tempCharter on ftx.FTX_INDEX7 equals c.CHARTERER_NAME into c2
                               from c in c2.DefaultIfEmpty()

                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                                   BROKER_NAME = c == null ? "" : c.BROKER_NAME
                               };

                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(TCE_MK_FUNCTION_CODE))
                {
                    var temp = from ftx in list

                               join v in cpaiEntity.MT_VEHICLE on ftx.FTX_INDEX5 equals v.VEH_ID into v2
                               from v in v2.DefaultIfEmpty() // outer join


                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   VESSEL_NAME = v == null ? "" : v.VEH_VEH_TEXT,
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(CRUDE_PURCHASE_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join d in cpaiEntity.CDP_DATA on ftx.FTX_TRANS_ID equals d.CDA_ROW_ID into d2
                               from d in d2.DefaultIfEmpty()
                               join m in cpaiEntity.MT_MATERIALS on ftx.FTX_INDEX10 equals m.MET_NUM into m2
                               from m in m2.DefaultIfEmpty()
                               join v in cpaiEntity.MT_VENDOR on ftx.FTX_INDEX12 equals v.VND_ACC_NUM_VENDOR into v2
                               from v in v2.DefaultIfEmpty()
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   PRODUCT_NAME = m != null ? m.MET_MAT_DES_ENGLISH : d != null ? d.CDA_MATERIALS_OTHERS : "",
                                   LOADING_PERIOD_FROM = d == null ? "" : d.CDA_LOADING_DATE_FROM == null || d.CDA_LOADING_DATE_FROM == DateTime.MinValue ? "" : d.CDA_LOADING_DATE_FROM.Value.ToString("dd/MM/yyyy"),
                                   LOADING_PERIOD_TO = d == null ? "" : d.CDA_LOADING_DATE_TO == null || d.CDA_LOADING_DATE_TO == DateTime.MaxValue ? "" : d.CDA_LOADING_DATE_TO.Value.ToString("dd/MM/yyyy"),
                                   DISCHARGING_PERIOD_FROM = d == null ? "" : d.CDA_DISCHARGING_DATE_FROM == null || d.CDA_DISCHARGING_DATE_FROM == DateTime.MinValue ? "" : d.CDA_DISCHARGING_DATE_FROM.Value.ToString("dd/MM/yyyy"),
                                   DISCHARGING_PERIODE_TO = d == null ? "" : d.CDA_DISCHARGING_DATE_TO == null || d.CDA_DISCHARGING_DATE_TO == DateTime.MaxValue ? "" : d.CDA_DISCHARGING_DATE_TO.Value.ToString("dd/MM/yyyy"),
                                   SUPPLIER_NAME = v == null ? "" : v.VND_NAME1
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(HEDG_DEAL_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join m in cpaiEntity.MT_MATERIALS on ftx.FTX_INDEX10 equals m.MET_NUM into m2
                               from m in m2.DefaultIfEmpty()
                               join mc in cpaiEntity.MT_MATERIALS_CONTROL.Where(x => x.MMC_SYSTEM.Equals("HEDG") && x.MMC_STATUS.Equals("ACTIVE")) on ftx.FTX_INDEX10 equals mc.MMC_FK_MATRIALS into mc2
                               from mc in mc2.DefaultIfEmpty()
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   UNDERLYING_NAME = mc != null ? mc.MMC_NAME : m != null ? m.MET_MAT_DES_ENGLISH : string.Empty
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(HEDG_TICKET_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join m in cpaiEntity.MT_MATERIALS on ftx.FTX_INDEX10 equals m.MET_NUM into m2
                               from m in m2.DefaultIfEmpty()
                               join mc in cpaiEntity.MT_MATERIALS_CONTROL.Where(x => x.MMC_SYSTEM.Equals("HEDG") && x.MMC_STATUS.Equals("ACTIVE")) on ftx.FTX_INDEX10 equals mc.MMC_FK_MATRIALS into mc2
                               from mc in mc2.DefaultIfEmpty()
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   UNDERLYING_NAME = mc != null ? mc.MMC_NAME : m != null ? m.MET_MAT_DES_ENGLISH : string.Empty
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION.Equals(PRE_DEAL_FUNCTION_CODE))
                {
                    var temp = from ftx in list
                               join m in cpaiEntity.MT_MATERIALS on ftx.FTX_INDEX10 equals m.MET_NUM into m2
                               from m in m2.DefaultIfEmpty()
                               join mc in cpaiEntity.MT_MATERIALS_CONTROL.Where(x => x.MMC_SYSTEM.Equals("HEDG") && x.MMC_STATUS.Equals("ACTIVE")) on ftx.FTX_INDEX10 equals mc.MMC_FK_MATRIALS into mc2
                               from mc in mc2.DefaultIfEmpty()                               
                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   UNDERLYING_NAME = mc != null ? mc.MMC_NAME : m != null ? m.MET_MAT_DES_ENGLISH : string.Empty
                               };
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
            }

            return results;
        }

        private List<FunctionTransaction> findNameCool(List<FUNCTION_TRANSACTION> list, List<FUNCTION_TRANSACTION> child_list, string ACF_FK_FUNCTION, List<string> units, List<string> areas, bool isTracking,string UserName, bool isBoarding)
        {
            MT_UNIT_DAL unitDal = new MT_UNIT_DAL();
            List<string> UserGroup = new List<string>();
            if (areas.Contains(""))
            {
                areas.Remove("");
            }
            if (units.Contains(""))
            {
                units.Remove("");
            }
            if (!string.IsNullOrEmpty(UserName))
            {
                DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                UserGroup = perDAL.GetUserGroupSortBySystem(UserName,"COOL");
            }
            for (int i = 0; i < areas.Count; i++)
            {
                List<MT_UNIT> unit = unitDal.getUnitByAreaName(areas[i].ToUpper());
                if (unit != null)
                {
                    for (int j = 0; j < unit.Count; j++)
                    {
                        if (!units.Contains(unit[j].MUN_NAME.ToUpper()))
                        {
                            units.Add(unit[j].MUN_NAME.ToUpper());
                        }
                    }
                }
            }
            List<FunctionTransaction> results = new List<FunctionTransaction>();

            if(isTracking)
            {
                using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
                {
                    if (ACF_FK_FUNCTION.Equals(COOL_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_SH_FUNCTION_CODE))
                    {
                        var temp = from ftx in list
                                   join d in cpaiEntity.COO_DATA on ftx.FTX_TRANS_ID equals d.CODA_ROW_ID into d2
                                   from d in d2.DefaultIfEmpty()
                                   join a in cpaiEntity.COO_ASSAY on ftx.FTX_TRANS_ID equals a.COAS_FK_COO_DATA into a2
                                   from a in a2.DefaultIfEmpty()
                                   join e in cpaiEntity.COO_EXPERT on ftx.FTX_TRANS_ID equals e.COEX_FK_COO_DATA into e2
                                   from e in e2.DefaultIfEmpty()
                                   join c in cpaiEntity.COO_CAM on ftx.FTX_TRANS_ID equals c.COCA_FK_COO_DATA into c2
                                   from c in c2.DefaultIfEmpty()
                                   select new FunctionTransaction
                                   {
                                       FUNCTION_TRANSACTION = ftx,
                                       ASSAY_FILE = a != null ? a.COAS_FILE_PATH : "",
                                       ASSAY_FROM = d != null ? d.CODA_ASSAY_TYPE : "",
                                       UNIT = e != null ? e.COEX_UNIT : "",
                                       UNIT_STATUS = e != null ? e.COEX_ACTIVATION_STATUS : "",
                                       REQUESTER_NAME = d != null ? d.CODA_REQUESTED_BY : "",
                                       DRAFT_APPROVER_NAME = d != null ? d.CODA_DRAFT_APPROVED_BY : "",
                                       FINAL_APPROVER_NAME = d != null ? d.CODA_FINAL_APPROVED_BY : "",
                                       DRAFT_CAM_FILE = d != null ? d.CODA_DRAFT_CAM_PATH : "",
                                       FINAL_CAM_FILE = d != null ? d.CODA_FINAL_CAM_PATH : "",
                                       DRAFT_SUBMIT_DATE = d != null ? d.CODA_DRAFT_SUBMIT_DATE.HasValue ? d.CODA_DRAFT_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       DRAFT_APPROVE_DATE = d != null ? d.CODA_DRAFT_APPROVE_DATE.HasValue ? d.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       FINAL_SUBMIT_DATE = d != null ? d.CODA_FINAL_SUBMIT_DATE.HasValue ? d.CODA_FINAL_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       FINAL_APPROVE_DATE = d != null ? d.CODA_FINAL_APPROVE_DATE.HasValue ? d.CODA_FINAL_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       STATUS = d != null ? d.CODA_STATUS : "",
                                       COO_FK_COO_CAM = c != null ? c.COCA_ROW_ID : "",
                                       NOTE = d != null ? d.CODA_NOTE : "",
                                       UPDATED_BY = d != null ? d.CODA_UPDATED_BY : "",
                                       UPDATED_DATE = d != null ? d.CODA_UPDATED.ToString("dd-MMM-yyyy HH.mm") : "",
                                       COO_EXPERT = ""
                                   };

                        //filter transaction
                        if (units.Count > 0)
                        {
                            results = temp.Where(x => units.Contains(x.UNIT.ToUpper()) && x.UNIT_STATUS != "UNSELECTED").GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                        }
                        else
                        {
                            results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                        }
                        //results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();

                        if (results.Count > 0)
                        {
                            foreach (var result in results)
                            {
                                COO_SUP_DOC_DAL supDocDAL = new COO_SUP_DOC_DAL();
                                List<COO_SUP_DOC> supDoc = supDocDAL.GetAllByID(result.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                                foreach (var item in supDoc)
                                {
                                    result.SUPPORT_FILE += item.COSD_FILE_PATH + ":" + item.COSD_FILE_TYPE + "|";
                                }

                                result.DENSITY = (from s in cpaiEntity.COO_SPIRAL
                                                  where s.COSP_HEADER == "Whole Crude" && s.COSP_KEY == "Density at 15.0C (g/cc)"
                                                  && s.COSP_FK_COO_CAM == result.COO_FK_COO_CAM
                                                  select s.COSP_VALUE).FirstOrDefault() ?? "";
                                result.SULPHUR = (from s in cpaiEntity.COO_SPIRAL
                                                  where s.COSP_HEADER == "Whole Crude" && s.COSP_KEY == "Sulphur (Total) (% wgt)"
                                                  && s.COSP_FK_COO_CAM == result.COO_FK_COO_CAM
                                                  select s.COSP_VALUE).FirstOrDefault() ?? "";
                                result.ACID = (from s in cpaiEntity.COO_SPIRAL
                                               where s.COSP_HEADER == "Whole Crude" && s.COSP_KEY == "Acidity (mgKOH/g)"
                                               && s.COSP_FK_COO_CAM == result.COO_FK_COO_CAM
                                               select s.COSP_VALUE).FirstOrDefault() ?? "";

                                COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                                List<COO_EXPERT> expert = expertDal.GetAllByID(result.FUNCTION_TRANSACTION.FTX_TRANS_ID).ToList();
                                result.UNIT = "";
                                foreach (var item in expert.OrderBy(x => x.COEX_AREA_ORDER).ThenBy(x => x.COEX_UNIT_ORDER))
                                {
                                    if (item.COEX_ACTIVATION_STATUS != "UNSELECTED")
                                    {
                                        result.COO_EXPERT += item.COEX_AREA + ";" + item.COEX_UNIT + ";" + item.COEX_ATTACHED_FILE + "#";
                                        string name = UsersDAL.getFullName(item.COEX_UPDATED_BY);
                                        if (item.COEX_STATUS == "APPROVE_3")
                                            result.UNIT += item.COEX_UNIT + ":" + item.COEX_STATUS + ":" + name + ":" + (item.COEX_SUBMIT_DATE.HasValue ? item.COEX_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : item.COEX_UPDATED.ToString("dd-MMM-yyyy HH.mm")) + "|";
                                        else if (item.COEX_STATUS == "APPROVED")
                                            result.UNIT += item.COEX_UNIT + ":" + item.COEX_STATUS + ":" + name + ":" + (item.COEX_APPROVE_DATE.HasValue ? item.COEX_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : item.COEX_UPDATED.ToString("dd-MMM-yyyy HH.mm")) + "|";
                                        else
                                            result.UNIT += item.COEX_UNIT + ":" + item.COEX_STATUS + ":" + name + ":" + item.COEX_UPDATED.ToString("dd-MMM-yyyy HH.mm") + "|";
                                    }
                                }

                                if (result.STATUS == "WAITING APPROVE DRAFT CAM")
                                    result.DRAFT_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.REQUESTER_NAME + ":" + result.DRAFT_SUBMIT_DATE;
                                else
                                    result.DRAFT_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.DRAFT_APPROVER_NAME + ":" + result.DRAFT_APPROVE_DATE;

                                if (result.STATUS == "WAITING APPROVE FINAL CAM")
                                    result.FINAL_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.REQUESTER_NAME + ":" + result.FINAL_SUBMIT_DATE;
                                else
                                    result.FINAL_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.FINAL_APPROVER_NAME + ":" + result.FINAL_APPROVE_DATE;

                                
                            }
                        }

                        //if (!isTracking)
                        //{
                        //    if (ACF_FK_FUNCTION.Equals(COOL_EXPERT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_SH_FUNCTION_CODE))
                        //    {
                        //        results = results.Where(x => x.STATUS == "WAITING EXPERT APPROVAL").ToList();
                        //    }
                        //    else
                        //    {
                        //        results = results.Where(x => x.STATUS != "WAITING EXPERT APPROVAL").ToList();
                        //    }
                        //}

                        //if (ACF_FK_FUNCTION.Equals(COOL_EXPERT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_SH_FUNCTION_CODE))
                        //{
                        //    results.RemoveAll(x => child_list.Where(z => x.FUNCTION_TRANSACTION.FTX_TRANS_ID == z.FTX_PARENT_TRX_ID).Count() == 0);
                        //}
                    }

                }
            }
            else
            {
                using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
                {
                    if (ACF_FK_FUNCTION.Equals(COOL_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_SH_FUNCTION_CODE))
                    {
                        var temp = from ftx in list
                                   join d in cpaiEntity.COO_DATA on ftx.FTX_TRANS_ID equals d.CODA_ROW_ID into d2
                                   from d in d2.DefaultIfEmpty()
                                   join a in cpaiEntity.COO_ASSAY on ftx.FTX_TRANS_ID equals a.COAS_FK_COO_DATA into a2
                                   from a in a2.DefaultIfEmpty()
                                   join e in cpaiEntity.COO_EXPERT on ftx.FTX_TRANS_ID equals e.COEX_FK_COO_DATA into e2
                                   from e in e2.DefaultIfEmpty()
                                   join c in cpaiEntity.COO_CAM on ftx.FTX_TRANS_ID equals c.COCA_FK_COO_DATA into c2
                                   from c in c2.DefaultIfEmpty()
                                   select new FunctionTransaction
                                   {
                                       FUNCTION_TRANSACTION = ftx,
                                       ASSAY_FILE = a != null ? a.COAS_FILE_PATH : "",
                                       ASSAY_FROM = d != null ? d.CODA_ASSAY_TYPE : "",
                                       UNIT = e != null ? e.COEX_UNIT : "",
                                       UNIT_STATUS = e != null ? e.COEX_ACTIVATION_STATUS : "",
                                       REQUESTER_NAME = d != null ? d.CODA_REQUESTED_BY : "",
                                       DRAFT_APPROVER_NAME = d != null ? d.CODA_DRAFT_APPROVED_BY : "",
                                       FINAL_APPROVER_NAME = d != null ? d.CODA_FINAL_APPROVED_BY : "",
                                       DRAFT_CAM_FILE = d != null ? d.CODA_DRAFT_CAM_PATH : "",
                                       FINAL_CAM_FILE = d != null ? d.CODA_FINAL_CAM_PATH : "",
                                       DRAFT_SUBMIT_DATE = d != null ? d.CODA_DRAFT_SUBMIT_DATE.HasValue ? d.CODA_DRAFT_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       DRAFT_APPROVE_DATE = d != null ? d.CODA_DRAFT_APPROVE_DATE.HasValue ? d.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       FINAL_SUBMIT_DATE = d != null ? d.CODA_FINAL_SUBMIT_DATE.HasValue ? d.CODA_FINAL_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       FINAL_APPROVE_DATE = d != null ? d.CODA_FINAL_APPROVE_DATE.HasValue ? d.CODA_FINAL_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                       STATUS = d != null ? d.CODA_STATUS : "",
                                       COO_FK_COO_CAM = c != null ? c.COCA_ROW_ID : "",
                                       NOTE = d != null ? d.CODA_NOTE : "",
                                       UPDATED_BY = d != null ? d.CODA_UPDATED_BY : "",
                                       UPDATED_DATE = d != null ? d.CODA_UPDATED.ToString("dd-MMM-yyyy HH.mm") : "",
                                       COO_EXPERT = ""
                                   };

                        //filter transaction
                        if (units.Count > 0)
                        { 
                            results = temp.Where(x => units.Contains((x.UNIT ?? "").ToUpper()) && x.UNIT_STATUS != "UNSELECTED").GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                        }
                        else
                        {
                            results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                        }
                        //results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();

                        if (results.Count > 0)
                        {
                            int index = 0;
                            List<int> RemoveTable = new List<int>();
                            foreach (var result in results)
                            {
                                bool Temp = false;

                                COO_SUP_DOC_DAL supDocDAL = new COO_SUP_DOC_DAL();
                                List<COO_SUP_DOC> supDoc = supDocDAL.GetAllByID(result.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                                foreach (var item in supDoc)
                                {
                                    result.SUPPORT_FILE += item.COSD_FILE_PATH + ":" + item.COSD_FILE_TYPE + "|";
                                }

                                result.DENSITY = (from s in cpaiEntity.COO_SPIRAL
                                                  where s.COSP_HEADER == "Whole Crude" && s.COSP_KEY == "Density at 15.0C (g/cc)"
                                                  && s.COSP_FK_COO_CAM == result.COO_FK_COO_CAM
                                                  select s.COSP_VALUE).FirstOrDefault() ?? "";
                                result.SULPHUR = (from s in cpaiEntity.COO_SPIRAL
                                                  where s.COSP_HEADER == "Whole Crude" && s.COSP_KEY == "Sulphur (Total) (% wgt)"
                                                  && s.COSP_FK_COO_CAM == result.COO_FK_COO_CAM
                                                  select s.COSP_VALUE).FirstOrDefault() ?? "";
                                result.ACID = (from s in cpaiEntity.COO_SPIRAL
                                               where s.COSP_HEADER == "Whole Crude" && s.COSP_KEY == "Acidity (mgKOH/g)"
                                               && s.COSP_FK_COO_CAM == result.COO_FK_COO_CAM
                                               select s.COSP_VALUE).FirstOrDefault() ?? "";

                                COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                                List<COO_EXPERT> expert = expertDal.GetAllByID(result.FUNCTION_TRANSACTION.FTX_TRANS_ID).ToList();
                                expert = expert.Where(x => units.Contains(x.COEX_UNIT.ToUpper())).ToList();
                                if (areas.Contains("EXPERT_SH".ToUpper()))
                                {
                                    if (result.STATUS != "CANCEL")
                                    {
                                        var QueryCheck = expert.Where(x => x.COEX_STATUS == "APPROVE_3");
                                        if (QueryCheck.Count() == 0)
                                        {
                                            Temp = true;
                                        }
                                    }
                                    else
                                    {
                                        Temp = true;
                                    }
                                }
                                else if(units.Contains("EXPERT".ToUpper()))
                                {
                                    if (result.STATUS == "WAITING EXPERT APPROVAL")
                                    {
                                        var QueryCheck = expert.Where(x => x.COEX_STATUS == "REJECT" || x.COEX_STATUS == "APPROVE_2");
                                        if (QueryCheck.Count() == 0)
                                        {
                                            //Temp == True จะเป็นตัดทิ้ง
                                            Temp = true;
                                        }
                                    }
                                    else
                                    {
                                        Temp = true;
                                    }
                                }
                                else if (UserGroup.Contains("SCEP".ToUpper()))
                                {
                                    if (isBoarding)
                                    {
                                        if (result.STATUS == "WAITING APPROVE DRAFT CAM")
                                            Temp = true;
                                        else if (result.STATUS == "WAITING APPROVE FINAL CAM")
                                            Temp = true;
                                        else if (result.STATUS == "WAITING EXPERT APPROVAL")
                                            Temp = true;
                                    }

                                }
                                else if (UserGroup.Contains("SCEP_SH".ToUpper()))
                                {
                                    if (isBoarding)
                                    {
                                        if (result.STATUS == "WAITING APPROVE DRAFT CAM")
                                            Temp = false;
                                        else if (result.STATUS == "WAITING APPROVE FINAL CAM")
                                            Temp = false;
                                        else
                                            Temp = true;
                                    }
                                }


                                if (result.STATUS == "WAITING APPROVE DRAFT CAM")
                                    result.DRAFT_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.REQUESTER_NAME + ":" + result.DRAFT_SUBMIT_DATE;
                                else
                                    result.DRAFT_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.DRAFT_APPROVER_NAME + ":" + result.DRAFT_APPROVE_DATE;

                                if (result.STATUS == "WAITING APPROVE FINAL CAM")
                                    result.FINAL_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.REQUESTER_NAME + ":" + result.FINAL_SUBMIT_DATE;
                                else
                                    result.FINAL_CAM_STATUS = "xxxx:" + result.STATUS + ":" + result.FINAL_APPROVER_NAME + ":" + result.FINAL_APPROVE_DATE;

                                if (Temp)
                                {
                                    RemoveTable.Add(index);
                                }
                                index++;
                            }
                            foreach(var item in RemoveTable.OrderByDescending(x => x))
                            {
                                results.RemoveAt(item);
                            }
                        }

                        //if (!isTracking)
                        //{
                        //    if (ACF_FK_FUNCTION.Equals(COOL_EXPERT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_SH_FUNCTION_CODE))
                        //    {
                        //        results = results.Where(x => x.STATUS == "WAITING EXPERT APPROVAL").ToList();
                        //    }
                        //    else
                        //    {
                        //        results = results.Where(x => x.STATUS != "WAITING EXPERT APPROVAL").ToList();
                        //    }
                        //}

                        //if (ACF_FK_FUNCTION.Equals(COOL_EXPERT_FUNCTION_CODE) || ACF_FK_FUNCTION.Equals(COOL_EXPERT_SH_FUNCTION_CODE))
                        //{
                        //    results.RemoveAll(x => child_list.Where(z => x.FUNCTION_TRANSACTION.FTX_TRANS_ID == z.FTX_PARENT_TRX_ID).Count() == 0);
                        //}
                    }

                }
            }

            return results;
        }

        private List<FunctionTransaction> findNameVcool(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            List<FunctionTransaction> newResults = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                var temp = from ftx in list
                           where ftx.FTX_FK_FUNCTION == VCOOL_FUNCTION_CODE
                           join d in cpaiEntity.VCO_DATA on ftx.FTX_TRANS_ID equals d.VCDA_ROW_ID into d2
                           from d in d2.DefaultIfEmpty()
                           join c in cpaiEntity.VCO_COMMENT on ftx.FTX_TRANS_ID equals c.VCCO_FK_VCO_DATA into c2
                           from c in c2.DefaultIfEmpty()

                           select new FunctionTransaction
                           {
                               FUNCTION_TRANSACTION = ftx,
                               LOADING_PERIOD_FROM = d != null ? d.VCDA_LOADING_DATE_FROM.HasValue ? d.VCDA_LOADING_DATE_FROM.Value.ToString("dd-MMM-yyyy") : "" : "",
                               LOADING_PERIOD_TO = d != null ? d.VCDA_LOADING_DATE_TO.HasValue ? d.VCDA_LOADING_DATE_TO.Value.ToString("dd-MMM-yyyy") : "" : "",
                               DISCHARGING_PERIOD_FROM = d != null ? d.VCDA_DISCHARGING_DATE_FROM.HasValue ? d.VCDA_DISCHARGING_DATE_FROM.Value.ToString("dd-MMM-yyyy") : "" : "",
                               DISCHARGING_PERIODE_TO = d != null ? d.VCDA_DISCHARGING_DATE_TO.HasValue ? d.VCDA_DISCHARGING_DATE_TO.Value.ToString("dd-MMM-yyyy") : "" : "",
                               FINAL_PRICE = d != null ? d.VCDA_FORMULA_PRICE ?? "" : "",
                               MAXIMUM_BUYING_PRICE = d != null ? d.VCDA_PREMIUM_MAXIMUM ?? "" : "",
                               SUPPLIER_NAME = d != null ? d.VCDA_SUPPLIER ?? "" : "",
                               VOLUMN_KBBL_MIN = d != null ? d.VCDA_QUANTITY_KBBL_MIN ?? "" : "",
                               VOLUMN_KBBL_MAX = d != null ? d.VCDA_QUANTITY_KBBL_MAX ?? "" : "",
                               VOLUMN_KT_MIN = d != null ? d.VCDA_QUANTITY_KT_MIN ?? "" : "",
                               VOLUMN_KT_MAX = d != null ? d.VCDA_QUANTITY_KT_MAX ?? "" : "",
                               LP_RESULT = c != null ? c.VCCO_LP_RUN_SUMMARY_MOBILE ?? "" : "",
                               TPC_MONTH = d != null ? d.VCDA_TPC_PLAN_MONTH ?? "" : "",
                               TPC_YEAR = d != null ? d.VCDA_TPC_PLAN_YEAR ?? "" : "",
                               TN_STATUS = d != null ? d.VCDA_TN_STATUS ?? "" : "",
                               SC_STATUS = d != null ? d.VCDA_SC_STATUS ?? "" : "",
                               CREATED_DATE =  d.VCDA_CREATED.ToString("dd/MM/yyyy"), 
                           };

                var temp2 = from ftx in list
                            where ftx.FTX_FK_FUNCTION == VCOOL_EXPERT_FUNCTION_CODE || ftx.FTX_FK_FUNCTION == VCOOL_EXPERT_SH_FUNCTION_CODE
                            join d in cpaiEntity.VCO_DATA on ftx.FTX_PARENT_TRX_ID equals d.VCDA_ROW_ID into d2
                            from d in d2.DefaultIfEmpty()
                            join c in cpaiEntity.VCO_COMMENT on ftx.FTX_PARENT_TRX_ID equals c.VCCO_FK_VCO_DATA into c2
                            from c in c2.DefaultIfEmpty()

                            select new FunctionTransaction
                            {
                                FUNCTION_TRANSACTION = ftx,
                                LOADING_PERIOD_FROM = d != null ? d.VCDA_LOADING_DATE_FROM.HasValue ? d.VCDA_LOADING_DATE_FROM.Value.ToString("dd-MMM-yyyy") : "" : "",
                                LOADING_PERIOD_TO = d != null ? d.VCDA_LOADING_DATE_TO.HasValue ? d.VCDA_LOADING_DATE_TO.Value.ToString("dd-MMM-yyyy") : "" : "",
                                DISCHARGING_PERIOD_FROM = d != null ? d.VCDA_DISCHARGING_DATE_FROM.HasValue ? d.VCDA_DISCHARGING_DATE_FROM.Value.ToString("dd-MMM-yyyy") : "" : "",
                                DISCHARGING_PERIODE_TO = d != null ? d.VCDA_DISCHARGING_DATE_TO.HasValue ? d.VCDA_DISCHARGING_DATE_TO.Value.ToString("dd-MMM-yyyy") : "" : "",
                                FINAL_PRICE = d != null ? d.VCDA_FORMULA_PRICE ?? "" : "",
                                MAXIMUM_BUYING_PRICE = d != null ? d.VCDA_PREMIUM_MAXIMUM ?? "" : "",
                                SUPPLIER_NAME = d != null ? d.VCDA_SUPPLIER ?? "" : "",
                                VOLUMN_KBBL_MIN = d != null ? d.VCDA_QUANTITY_KBBL_MIN ?? "" : "",
                                VOLUMN_KBBL_MAX = d != null ? d.VCDA_QUANTITY_KBBL_MAX ?? "" : "",
                                VOLUMN_KT_MIN = d != null ? d.VCDA_QUANTITY_KT_MIN ?? "" : "",
                                VOLUMN_KT_MAX = d != null ? d.VCDA_QUANTITY_KT_MAX ?? "" : "",
                                LP_RESULT = c != null ? c.VCCO_LP_RUN_SUMMARY_MOBILE ?? "" : "",
                                TPC_MONTH = d != null ? d.VCDA_TPC_PLAN_MONTH ?? "" : "",
                                TPC_YEAR = d != null ? d.VCDA_TPC_PLAN_YEAR ?? "" : "",
                                TN_STATUS = d != null ? d.VCDA_TN_STATUS ?? "" : "",
                                SC_STATUS = d != null ? d.VCDA_SC_STATUS ?? "" : "",
                                CREATED_DATE = d.VCDA_CREATED.ToString("dd/MM/yyyy"),
                            };

                //filter transaction
                results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                results.AddRange(temp2.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList());

                //newResults = results.OrderByDescending(a => Convert.ToDateTime(a.CREATED_DATE)).ToList();
            }

            return results;
        }

        private List<FunctionTransaction> findNameHedge(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                if (ACF_FK_FUNCTION == HEDG_ANNUAL_FUNCTION_CODE)
                {
                    var temp = from ftx in list
                               where ftx.FTX_FK_FUNCTION == HEDG_ANNUAL_FUNCTION_CODE
                               join h in cpaiEntity.HEDG_ANNUAL_FW on ftx.FTX_TRANS_ID equals h.HAF_ROW_ID into h2
                               from h in h2.DefaultIfEmpty()
                               join c in cpaiEntity.MT_COMPANY on ftx.FTX_INDEX10 equals c.MCO_COMPANY_CODE into c2
                               from c in c2.DefaultIfEmpty()

                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   COMPANY = c != null ? c.MCO_SHORT_NAME : "",
                                   NOTE = h != null ? h.HAF_NOTE : ""
                               };

                    //filter transaction
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
                else if (ACF_FK_FUNCTION == HEDG_STRCMT_FUNCTION_CODE)
                {
                    var temp = from ftx in list
                               where ftx.FTX_FK_FUNCTION == HEDG_STRCMT_FUNCTION_CODE
                               join h in cpaiEntity.HEDG_STR_CMT_FW on ftx.FTX_TRANS_ID equals h.HSCF_ROW_ID into h2
                               from h in h2.DefaultIfEmpty()
                               join c in cpaiEntity.MT_COMPANY on ftx.FTX_INDEX10 equals c.MCO_COMPANY_CODE into c2
                               from c in c2.DefaultIfEmpty()

                               select new FunctionTransaction
                               {
                                   FUNCTION_TRANSACTION = ftx,
                                   COMPANY = c != null ? c.MCO_SHORT_NAME : "",
                                   NOTE = h != null ? h.HSCF_NOTE : "",
                                   EXCEED_FLAG = h != null ? h.HSCF_EXCEED_FLAG : ""
                               };

                    //filter transaction
                    results = temp.GroupBy(p => p.FUNCTION_TRANSACTION.FTX_ROW_ID).Select(g => g.First()).ToList();
                }
            }

            return results;
        }

        private List<FUNCTION_TRANSACTION> filterDataByIndex(List<FUNCTION_TRANSACTION> results, List<string> lstIndexValues)
        {
            if (!string.IsNullOrEmpty(lstIndexValues[0]))
            {
                string val1 = lstIndexValues[0] != null ? lstIndexValues[0].ToUpper() : lstIndexValues[0];
                if (val1.Contains("|"))
                {
                    List<string> lstValues = val1.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX1 != null && lstValues.Contains(a.FTX_INDEX1.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX1 != null && a.FTX_INDEX1.ToUpper().Contains(val1) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[1]))
            {
                string val2 = lstIndexValues[1] != null ? lstIndexValues[1].ToUpper() : lstIndexValues[1];
                if (val2.Contains("|"))
                {
                    List<string> lstValues = val2.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX2 != null && lstValues.Contains(a.FTX_INDEX2.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX2 != null && a.FTX_INDEX2.ToUpper().Contains(val2) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[2]))
            {
                string val3 = lstIndexValues[2] != null ? lstIndexValues[2].ToUpper() : lstIndexValues[2];
                if (val3.Contains("|"))
                {
                    List<string> lstValues = val3.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX3 != null && lstValues.Contains(a.FTX_INDEX3.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX3 != null && a.FTX_INDEX3.ToUpper().Contains(val3) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[3]))
            {
                string val4 = lstIndexValues[3] != null ? lstIndexValues[3].ToUpper() : lstIndexValues[3];
                if (val4.Contains("|"))
                {
                    List<string> lstValues = val4.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX4 != null && lstValues.Contains(a.FTX_INDEX4.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX4 != null && a.FTX_INDEX4.ToUpper().Contains(val4) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[4]))
            {
                string val5 = lstIndexValues[4] != null ? lstIndexValues[4].ToUpper() : lstIndexValues[4];
                if (val5.Contains("|"))
                {
                    List<string> lstValues = val5.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX5 != null && lstValues.Contains(a.FTX_INDEX5.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX5 != null && a.FTX_INDEX5.ToUpper().Contains(val5) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[5]))
            {
                string val6 = lstIndexValues[5] != null ? lstIndexValues[5].ToUpper() : lstIndexValues[5];
                if (val6.Contains("|"))
                {
                    List<string> lstValues = val6.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX6 != null && lstValues.Contains(a.FTX_INDEX6.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX6 != null && a.FTX_INDEX6.ToUpper().Contains(val6) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[6]))
            {
                string val7 = lstIndexValues[6] != null ? lstIndexValues[6].ToUpper() : lstIndexValues[6];
                if (val7.Contains("|"))
                {
                    List<string> lstValues = val7.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX7 != null && lstValues.Contains(a.FTX_INDEX7.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX7 != null && a.FTX_INDEX7.ToUpper().Contains(val7) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[7]))
            {
                string val8 = lstIndexValues[7] != null ? lstIndexValues[7].ToUpper() : lstIndexValues[7];
                if (val8.Contains("|"))
                {
                    List<string> lstValues = val8.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX8 != null && lstValues.Contains(a.FTX_INDEX8.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX8 != null && a.FTX_INDEX8.ToUpper().Contains(val8) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[8]))
            {
                string val9 = lstIndexValues[8] != null ? lstIndexValues[8].ToUpper() : lstIndexValues[8];
                if (val9.Contains("|"))
                {
                    List<string> lstValues = val9.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX9 != null && lstValues.Contains(a.FTX_INDEX9.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX9 != null && a.FTX_INDEX9.ToUpper().Contains(val9) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[9]))
            {
                string val10 = lstIndexValues[9] != null ? lstIndexValues[9].ToUpper() : lstIndexValues[9];
                if (val10.Contains("|"))
                {
                    List<string> lstValues = val10.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX10 != null && lstValues.Contains(a.FTX_INDEX10.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX10 != null && a.FTX_INDEX10.ToUpper().Contains(val10) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[10]))
            {
                string val11 = lstIndexValues[10] != null ? lstIndexValues[10].ToUpper() : lstIndexValues[10];
                if (val11.Contains("|"))
                {
                    List<string> lstValues = val11.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX11 != null && lstValues.Contains(a.FTX_INDEX11.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    if (lstIndexValues[0] == "FREIGHT")
                    {
                        using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
                        {
                            var temp = from r in results
                                       join m in cpaiEntity.CPAI_FREIGHT_CARGO on r.FTX_TRANS_ID equals m.FDC_FK_FREIGHT_DATA
                                       where m.FDC_FK_CARGO != null && m.FDC_FK_CARGO.ToUpper() == val11 select r;
                            results = temp.ToList();
                        }
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX11 != null && a.FTX_INDEX11.ToUpper().Contains(val11) select a;
                        results = temp.ToList();
                    }
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[11]))
            {
                string val12 = lstIndexValues[11] != null ? lstIndexValues[11].ToUpper() : lstIndexValues[11];
                if (val12.Contains("|"))
                {
                    List<string> lstValues = val12.Split('|').ToList();
                    if (lstIndexValues[0] == "HEDG")
                    {
                        var temp = from a in results where a.FTX_INDEX12 != null && parsePurchaseDate(a.FTX_INDEX12) >= parsePurchaseDate(lstValues[0]) && parsePurchaseDate(a.FTX_INDEX12) <= parsePurchaseDate(lstValues[1]) select a;
                        results = temp.ToList();
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX12 != null && lstValues.Contains(a.FTX_INDEX12.ToUpper()) select a;
                        results = temp.ToList();
                    }
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX12 != null && a.FTX_INDEX12.ToUpper().Contains(val12) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[12]))
            {
                string val13 = lstIndexValues[12] != null ? lstIndexValues[12].ToUpper() : lstIndexValues[12];
                if (val13.Contains("|"))
                {
                    List<string> lstValues = val13.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX13 != null && lstValues.Contains(a.FTX_INDEX13.ToUpper()) select a;
                    results = temp.ToList();
                }
                else if (val13.Split(',').Count() == 3)
                {
                    string[] lstValues = val13.Split(','); 
                    var temp = from a in results where a.FTX_INDEX13 != null && lstValues[0].Contains(a.FTX_INDEX13.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX13 != null && a.FTX_INDEX13.ToUpper().Contains(val13) select a;
                    results = temp.ToList();
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[13]))
            {
                string val14 = lstIndexValues[13] != null ? lstIndexValues[13].ToUpper() : lstIndexValues[13];
                if (val14.Contains("|"))
                {
                    List<string> lstValues = val14.Split('|').ToList();
                    if (lstIndexValues[0] == "HEDG")
                    {
                        var temp = from a in results where a.FTX_INDEX14 != null && parsePurchaseDate(a.FTX_INDEX14) >= parsePurchaseDate(lstValues[0]) && parsePurchaseDate(a.FTX_INDEX14) <= parsePurchaseDate(lstValues[1]) select a;
                        results = temp.ToList();
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX14 != null && lstValues.Contains(a.FTX_INDEX14.ToUpper()) select a;
                        results = temp.ToList();
                    }
                }
                else if (val14.Split(',').Count() == 3)
                {
                    string[] lstValues = val14.Split(',');
                    var temp = from a in results where a.FTX_INDEX14 != null && lstValues[0].Contains(a.FTX_INDEX14.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    var temp = from a in results where a.FTX_INDEX14 != null && a.FTX_INDEX14.ToUpper().Contains(val14) select a;
                    results = temp.ToList();
                }
            }
            
            var tpc_plan_from = string.IsNullOrEmpty(lstIndexValues[14]) ? new MonthYearResult() :  ExtractMonthYear(lstIndexValues[14]);
            var tpc_plan_to = string.IsNullOrEmpty(lstIndexValues[15]) ? new MonthYearResult() : ExtractMonthYear(lstIndexValues[15]);
            if (tpc_plan_from.IsSuccess || tpc_plan_to.IsSuccess)
            {
                if (!string.IsNullOrEmpty(tpc_plan_from.Month))
                {
                    if (!string.IsNullOrEmpty(tpc_plan_from.Year) && !string.IsNullOrEmpty(tpc_plan_to.Month) && !string.IsNullOrEmpty(tpc_plan_to.Year))
                    {
                        /*****Search by TPCMonthFrom-TPCYearFrom and TPCMonthTo-TPCYearTo*****/
                        var monthYearFrom = FunctionTransactionDAL.GetMonthYearNo(tpc_plan_from.Month, tpc_plan_from.Year);
                        var monthYearTo = FunctionTransactionDAL.GetMonthYearNo(tpc_plan_to.Month, tpc_plan_to.Year);
                        results = results.Where(c =>
                        {
                            var tpcMonthYearNo = FunctionTransactionDAL.GetMonthYearNo(c.FTX_INDEX15, c.FTX_INDEX16);
                            return (monthYearFrom <= tpcMonthYearNo) && (tpcMonthYearNo <= monthYearTo);
                        }).ToList();
                    }
                    else if (string.IsNullOrEmpty(tpc_plan_from.Year))
                    {
                        /*****Search by TPCMonthFrom only*****/
                        var temp = from a in results where a.FTX_INDEX15 != null && a.FTX_INDEX15.Equals(tpc_plan_from.Month, StringComparison.OrdinalIgnoreCase) select a;
                        results = temp.ToList();
                    }
                    else
                    {
                        /*****Search by TPCMonthFrom and TPCYearFrom*****/
                        var temp = from a in results where a.FTX_INDEX15 != null && a.FTX_INDEX16 != null && a.FTX_INDEX15.Equals(tpc_plan_from.Month, StringComparison.OrdinalIgnoreCase) && a.FTX_INDEX16.Equals(tpc_plan_from.Year, StringComparison.OrdinalIgnoreCase) select a;
                        results = temp.ToList();
                    }
                }
                else if (!string.IsNullOrEmpty(tpc_plan_from.Year))
                {
                    /*****Search by TPCYearFrom only*****/
                    var temp = from a in results where a.FTX_INDEX16 != null && a.FTX_INDEX16.Equals(tpc_plan_from.Year, StringComparison.OrdinalIgnoreCase) select a;
                    results = temp.ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(lstIndexValues[14]))
                {
                    string val15 = lstIndexValues[14] != null ? lstIndexValues[14].ToUpper() : lstIndexValues[14];
                    if (val15.Contains("|"))
                    {
                        List<string> lstValues = val15.Split('|').ToList();
                        var temp = from a in results where a.FTX_INDEX15 != null && lstValues.Contains(a.FTX_INDEX15.ToUpper()) select a;
                        results = temp.ToList();
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX15 != null && a.FTX_INDEX15.ToUpper().Contains(val15) select a;
                        results = temp.ToList();
                    }
                }
                if (!string.IsNullOrEmpty(lstIndexValues[15]))
                {
                    string val16 = lstIndexValues[15] != null ? lstIndexValues[15].ToUpper() : lstIndexValues[15];
                    if (lstIndexValues[0].Contains("|"))
                    {
                        List<string> lstValues = val16.Split('|').ToList();
                        var temp = from a in results where a.FTX_INDEX16 != null && lstValues.Contains(a.FTX_INDEX16.ToUpper()) select a;
                        results = temp.ToList();
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX16 != null && a.FTX_INDEX16.ToUpper().Contains(val16) select a;
                        results = temp.ToList();
                    }
                }
            }
            
            if (!string.IsNullOrEmpty(lstIndexValues[16]))
            {
                string val17 = lstIndexValues[16] != null ? lstIndexValues[16].ToUpper() : lstIndexValues[16];
                if (val17.Contains("|"))
                {
                    List<string> lstValues = val17.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX17 != null && lstValues.Contains(a.FTX_INDEX17.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    if (!string.IsNullOrEmpty(lstIndexValues[17]) && lstIndexValues[0] == "VCOOL")
                    {
                        string val18 = lstIndexValues[17];
                        var temp = from a in results
                                   where a.FTX_INDEX17 != null && a.FTX_INDEX18 != null && ((parseDateForVCool(a.FTX_INDEX17) >= parseDateForVCool(val17) && parseDateForVCool(a.FTX_INDEX17) <= parseDateForVCool(val18))
                                    || (parseDateForVCool(a.FTX_INDEX18) >= parseDateForVCool(val17) && parseDateForVCool(a.FTX_INDEX18) <= parseDateForVCool(val18)))
                                   select a;
                        results = temp.ToList();
                    } else
                    {
                        var temp = from a in results where a.FTX_INDEX17 != null && a.FTX_INDEX17.ToUpper().Contains(val17) select a;
                        results = temp.ToList();
                    }
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[17]))
            {
                string val18 = lstIndexValues[17] != null ? lstIndexValues[17].ToUpper() : lstIndexValues[17];
                if (val18.Contains("|"))
                {
                    List<string> lstValues = val18.Split('|').ToList();
                    if (!string.IsNullOrEmpty(lstIndexValues[0]) && !string.IsNullOrEmpty(lstValues[0]) && !string.IsNullOrEmpty(lstValues[1]))
                    {
                        if (lstIndexValues[0] == "TCE_WS")
                        {
                            var temp = from a in results where a.FTX_INDEX18 != null && parsePurchaseDate(a.FTX_INDEX18) >= parsePurchaseDate(lstValues[0]) && parsePurchaseDate(a.FTX_INDEX18) <= parsePurchaseDate(lstValues[1]) select a;
                            results = temp.ToList();
                        }
                        else if (lstIndexValues[0] == "TCE_MK")
                        {
                            var temp = from a in results where a.FTX_INDEX18 != null && parseSearchMonth(a.FTX_INDEX18) >= parseSearchMonth(lstValues[0]) && parseSearchMonth(a.FTX_INDEX18) <= parseSearchMonth(lstValues[1]) select a;
                            results = temp.ToList();
                        }
                        else
                        {
                            var temp = from a in results where a.FTX_INDEX18 != null && lstValues.Contains(a.FTX_INDEX18.ToUpper()) select a;
                            results = temp.ToList();
                        }
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX18 != null && lstValues.Contains(a.FTX_INDEX18.ToUpper()) select a;
                        results = temp.ToList();
                    }
                }
                else
                {
                    if (lstIndexValues[0] != "VCOOL")
                    {
                        var temp = from a in results where a.FTX_INDEX18 != null && a.FTX_INDEX18.ToUpper().Contains(val18) select a;
                        results = temp.ToList();
                    }
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[18]))
            {
                string val19 = lstIndexValues[18] != null ? lstIndexValues[18].ToUpper() : lstIndexValues[18];
                if (val19.Contains("|"))
                {
                    List<string> lstValues = val19.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX19 != null && lstValues.Contains(a.FTX_INDEX19.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    if (!string.IsNullOrEmpty(lstIndexValues[19]) && lstIndexValues[0] == "CHARTERING")
                    {
                        string val20 = lstIndexValues[19];
                        var temp = from a in results
                                   where a.FTX_INDEX19 != null && a.FTX_INDEX20 != null && ((parsePurchaseDate(a.FTX_INDEX19) >= parseInputDate(val19) && parsePurchaseDate(a.FTX_INDEX19) <= parseInputDate(val20))
                                    || (parsePurchaseDate(a.FTX_INDEX20) >= parseInputDate(val19) && parsePurchaseDate(a.FTX_INDEX20) <= parseInputDate(val20)))
                                   select a;
                        results = temp.ToList();
                    }
                    else if (!string.IsNullOrEmpty(lstIndexValues[19]) && lstIndexValues[0] == "COOL")
                    {
                        string val20 = lstIndexValues[19];
                        var temp = from a in results where a.FTX_INDEX19 != null && ((parseDateForCool(a.FTX_INDEX19) >= parseDateForCool(val19)) && (parseDateForCool(a.FTX_INDEX19) <= parseDateForCool(val20))) select a;
                        results = temp.ToList();
                    }
                    else if (!string.IsNullOrEmpty(lstIndexValues[19]) && lstIndexValues[0] == "VCOOL")
                    {
                        string val20 = lstIndexValues[19];
                        var temp = from a in results
                                   where a.FTX_INDEX19 != null && a.FTX_INDEX20 != null && ((parseDateForCool(a.FTX_INDEX19) >= parseDateForCool(val19) && parseDateForCool(a.FTX_INDEX19) <= parseDateForCool(val20))
                                    || (parseDateForCool(a.FTX_INDEX20) >= parseDateForCool(val19) && parseDateForCool(a.FTX_INDEX20) <= parseDateForCool(val20)))
                                   select a;
                        results = temp.ToList();
                    }
                    else if (!string.IsNullOrEmpty(lstIndexValues[19]) && lstIndexValues[0] == "HEDG")
                    {
                        string val20 = lstIndexValues[19];
                        var temp = from a in results
                                   where a.FTX_INDEX19 != null && a.FTX_INDEX20 != null && ((parseDateForTenorHedging(a.FTX_INDEX19) >= parseDateForTenorHedging(val19) && parseDateForTenorHedging(a.FTX_INDEX19) <= parseDateForTenorHedging(val20))
                                    || (parseDateForTenorHedging(a.FTX_INDEX20) >= parseDateForTenorHedging(val19) && parseDateForTenorHedging(a.FTX_INDEX20) <= parseDateForTenorHedging(val20)))
                                   select a;
                        results = temp.ToList();
                    }
                    else
                    {
                        var temp = from a in results where a.FTX_INDEX19 != null && a.FTX_INDEX19.ToUpper().Contains(val19) select a;
                        results = temp.ToList();
                    }
                }
            }
            if (!string.IsNullOrEmpty(lstIndexValues[19]))
            {
                string val20 = lstIndexValues[19] != null ? lstIndexValues[19].ToUpper() : lstIndexValues[19];
                if (val20.Contains("|"))
                {
                    List<string> lstValues = val20.Split('|').ToList();
                    var temp = from a in results where a.FTX_INDEX20 != null && lstValues.Contains(a.FTX_INDEX20.ToUpper()) select a;
                    results = temp.ToList();
                }
                else
                {
                    if (lstIndexValues[0] != "CHARTERING" && lstIndexValues[0] != "COOL" && lstIndexValues[0] != "VCOOL" && lstIndexValues[0] != "HEDG")
                    {
                        var temp = from a in results where a.FTX_INDEX20 != null && a.FTX_INDEX20.ToUpper().Contains(val20) select a;
                        results = temp.ToList();
                    }
                }
            }
            return results;
        }
        
        private static DateTime? ParseDate(string dateString)
        {
            var FORMAT_DATE = "dd-MMM-yyyy";
            var cultureInfo = new CultureInfo("en-US");

            var pattern = string.Format(@"(?<day>(?<=\s|^)\d{{2}})[-/](?<month>{0})[-/](?<year>\d{{4}}(?=\s|$))", string.Join("|", cultureInfo.DateTimeFormat.AbbreviatedMonthNames.Take(12)));
            var match = new Regex(pattern).Match(dateString);
            var date = match.Success ? (DateTime?)DateTime.ParseExact(match.Value, FORMAT_DATE, cultureInfo) : null;
            return date;
        }
        #endregion

        public List<FUNCTION_TRANSACTION> GetFunctionTransaction(Dictionary<string, string> lstDic)
        {
            List<FUNCTION_TRANSACTION> lstFunction = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                var query = entity.FUNCTION_TRANSACTION.ToList();

                string Val = "";

                if (lstDic.TryGetValue("AppID".ToUpper(), out Val)) query = query.Where(x => x.FTX_APP_ID != null && x.FTX_APP_ID.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("ReqTranID".ToUpper(), out Val)) query = query.Where(x => x.FTX_REQ_TRANS != null && x.FTX_REQ_TRANS.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("TranID".ToUpper(), out Val)) query = query.Where(x => x.FTX_TRANS_ID != null && x.FTX_TRANS_ID.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("FromDate".ToUpper(), out Val)) query = query.Where(x => x.FTX_CREATED != null && x.FTX_CREATED >= Convert.ToDateTime(ConvertDateFormatBack(Val))).ToList();
                if (lstDic.TryGetValue("ToDate".ToUpper(), out Val)) query = query.Where(x => x.FTX_CREATED != null && x.FTX_CREATED <= Convert.ToDateTime(ConvertDateFormatBack(Val)).AddDays(1)).ToList();
                if (lstDic.TryGetValue("Status".ToUpper(), out Val)) query = query.Where(x => x.FTX_RETURN_STATUS != null && x.FTX_RETURN_STATUS.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("FunctionID".ToUpper(), out Val)) query = query.Where(x => x.FTX_FK_FUNCTION != null && x.FTX_FK_FUNCTION.ToUpper().Contains(Val.ToUpper())).ToList();


                if (lstDic.TryGetValue("INDEX1".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX1 != null && x.FTX_INDEX1.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX2".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX2 != null && x.FTX_INDEX2.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX3".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX3 != null && x.FTX_INDEX3.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX4".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX5".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX5 != null && x.FTX_INDEX5.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX6".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX6 != null && x.FTX_INDEX6.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX7".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX7 != null && x.FTX_INDEX7.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX8".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX8 != null && x.FTX_INDEX8.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX9".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX9 != null && x.FTX_INDEX9.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX10".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX10 != null && x.FTX_INDEX10.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX11".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX11 != null && x.FTX_INDEX11.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX12".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX12 != null && x.FTX_INDEX12.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX13".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX13 != null && x.FTX_INDEX13.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX14".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX14 != null && x.FTX_INDEX14.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX15".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX15 != null && x.FTX_INDEX15.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX16".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX16 != null && x.FTX_INDEX16.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX17".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX17 != null && x.FTX_INDEX17.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX18".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX18 != null && x.FTX_INDEX18.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX19".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX19 != null && x.FTX_INDEX19.ToUpper().Contains(Val.ToUpper())).ToList();
                if (lstDic.TryGetValue("INDEX20".ToUpper(), out Val)) query = query.Where(x => x.FTX_INDEX20 != null && x.FTX_INDEX20.ToUpper().Contains(Val.ToUpper())).ToList();

                lstFunction = query.ToList();


            }
            return lstFunction;
        }

        public List<FUNCTION_TRANSACTION> FindByTransactionByFuncIndex(string[] function, string[] index, string type)
        {
            List<FUNCTION_TRANSACTION> lstFunction = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                lstFunction = entity.FUNCTION_TRANSACTION.Where(x => function.Contains(x.FTX_FK_FUNCTION) && type.Contains(x.FTX_INDEX2) && index.Contains(x.FTX_INDEX4)).ToList();
            }

            return lstFunction;
        }

        public List<FUNCTION_TRANSACTION> FindByTransactionByFuncIndex(string[] function, string[] index, string[] type)
        {
            List<FUNCTION_TRANSACTION> lstFunction = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                lstFunction = entity.FUNCTION_TRANSACTION.Where(x => function.Contains(x.FTX_FK_FUNCTION) && type.Contains(x.FTX_INDEX2) && index.Contains(x.FTX_INDEX4)).ToList();
            }

            return lstFunction;
        }

        public List<FUNCTION_TRANSACTION> FindByTransactionByFuncIndex(string[] function, string[] index)
        {
            List<FUNCTION_TRANSACTION> lstFunction = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                lstFunction = entity.FUNCTION_TRANSACTION.Where(x => function.Contains(x.FTX_FK_FUNCTION)  && index.Contains(x.FTX_INDEX4)).ToList();
            }

            return lstFunction;
        }

        public FUNCTION_TRANSACTION GetFnTranDetail(string TranID)
        {
            FUNCTION_TRANSACTION fnDetail = new FUNCTION_TRANSACTION();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                fnDetail = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_TRANS_ID == TranID).FirstOrDefault();
            }
            return fnDetail;
        }

        public List<FUNCTION_TRANSACTION> GetDealByPreDeal(string preDealNo)
        {
            List<FUNCTION_TRANSACTION> result = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                result = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_INDEX15 == preDealNo && x.FTX_FK_FUNCTION == HEDG_DEAL_FUNCTION_CODE).ToList();
            }
            return result;
        }

        public List<FUNCTION_TRANSACTION> GetDealByTicket(string ticketNo)
        {
            List<FUNCTION_TRANSACTION> result = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                result = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_INDEX7 == ticketNo && x.FTX_FK_FUNCTION == HEDG_DEAL_FUNCTION_CODE).ToList();
            }
            return result;
        }

        public List<FUNCTION_TRANSACTION> GetDealByDealNo(string dealNo)
        {
            List<FUNCTION_TRANSACTION> result = new List<FUNCTION_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                result = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_INDEX6.Trim().ToUpper() == dealNo.Trim().ToUpper() && x.FTX_FK_FUNCTION == HEDG_DEAL_FUNCTION_CODE).ToList();
            }
            return result;
        }

        public List<EXTEND_TRANSACTION> GetFnTranExthen(string TranRowID)
        {
            List<EXTEND_TRANSACTION> fnDetail = new List<EXTEND_TRANSACTION>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                fnDetail = entity.EXTEND_TRANSACTION.Where(x => x.ETX_FK_FUNC_TRX == TranRowID).ToList();
            }
            return fnDetail;
        }

        public List<FUNCTIONS> GetFNNamelst()
        {
            List<FUNCTIONS> fnLst = new List<FUNCTIONS>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                fnLst = entity.FUNCTIONS.ToList();
            }
            return fnLst;
        }

        public List<string> GetStatuslst()
        {
            List<string> LstStatus = new List<string>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                LstStatus = entity.FUNCTION_TRANSACTION.Select(x => x.FTX_RETURN_STATUS).Distinct().ToList();
            }
            return LstStatus;
        }

        public List<FUNCTION_ACTIVITY> GetFnTranActivity(string TranRowID)
        {
            List<FUNCTION_ACTIVITY> fnActivity = new List<FUNCTION_ACTIVITY>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                fnActivity = entity.FUNCTION_ACTIVITY.Where(x => x.FAC_FK_FUNC_TRX == TranRowID).ToList();
            }
            return fnActivity;
        }

        public List<TRANSACTION_LOG> GetFnTranLog(string TranRowID, string RequestID)
        {
            List<TRANSACTION_LOG> fnActivity = new List<TRANSACTION_LOG>();
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                fnActivity = entity.TRANSACTION_LOG.Where(x => x.TXL_TABLE_ROW_ID.Contains(TranRowID) && x.TXL_TABLE_ROW_ID.Contains(RequestID)).ToList();
            }
            return fnActivity;
        }

        public string ConvertDateFormatBack(string DateString)
        {
            //form Database Format dd/mm/yyyy
            if (DateString == null) return "";
            string[] datesplit = DateString.Split('/');
            if (DateString.Length >= 10 && datesplit.Length >= 3)
            {
                return string.Format("{0}-{1}-{2}", datesplit[2], datesplit[1], datesplit[0]);
            }
            else
            {
                return "";
            }
        }

        public string GetReqTransIDByTransactionId(string transactionId)
        {
            string rtn = "";
            List<FUNCTION_TRANSACTION> objList = new List<FUNCTION_TRANSACTION>();
            objList = findByTransactionId(transactionId);
            if(objList != null && objList.Count > 0)
            {
                rtn = objList.FirstOrDefault().FTX_REQ_TRANS;
            }
            return rtn;
        }

        public void UpdateTicket(FUNCTION_TRANSACTION data)
        {
            try
            {
                using (var context = new EntitiesEngine())
                {
                    var _search = context.FUNCTION_TRANSACTION.SingleOrDefault(p => p.FTX_ROW_ID == data.FTX_ROW_ID);
                    #region Set Value 
                    _search.FTX_INDEX6 = data.FTX_INDEX6;
                    _search.FTX_INDEX7 = data.FTX_INDEX7;
                    _search.FTX_INDEX20 = data.FTX_INDEX20;
                    _search.FTX_UPDATED_BY = data.FTX_UPDATED_BY;
                    _search.FTX_UPDATED = data.FTX_UPDATED;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateHedgActivationStatus(string id, string status)
        {
            try
            {
                using (var context = new EntitiesEngine())
                {
                    var _search = context.FUNCTION_TRANSACTION.SingleOrDefault(p => p.FTX_TRANS_ID == id);
                    #region Set Value 
                    _search.FTX_INDEX13 = status;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Extract month and year from date string
        /// </summary>
        /// <param name="dateString">"01-Sep-2017" or "00-Sep-0000" or "00-MMM-2017" or "00-Sep-2017"</param>
        /// <returns></returns>
        private MonthYearResult ExtractMonthYear(string dateString)
        {
            var defaultMonth = "MMM";
            var cultureInfo = new CultureInfo("en-US");
            var pattern = string.Format(@"(?<day>(?<=\s|^)\d{{2}})[-/](?<month>{0})[-/](?<year>\d{{4}}(?=\s|$))", string.Join("|", cultureInfo.DateTimeFormat.AbbreviatedMonthNames.Take(12)) + "|" + defaultMonth);
            var match = new Regex(pattern).Match(dateString);

            var success = match.Success;
            var monthName = string.Empty;
            var yearNo = string.Empty;
            if (success)
            {
                var month = match.Groups["month"];
                monthName = month.Value == defaultMonth ? null : month.Value;

                var year = match.Groups["year"];
                yearNo = year.Value == "0000" ? null : year.Value;
            }
            return new MonthYearResult { IsSuccess = success, Month = monthName, Year = yearNo };
        }
        
        private static Dictionary<string, int> abbrMonthDictionary = Enumerable.Range(0, 12).ToDictionary(i => new System.Globalization.CultureInfo("en-US").DateTimeFormat.AbbreviatedMonthNames[i], i => i + 1);
        public static int GetMonthYearNo(string month, string year)
        {
            var tpcMonthYearNo = 0;
            var yearNo = 0;
            if (int.TryParse(year, out yearNo))
            {
                var monthNo = !string.IsNullOrEmpty(month) && abbrMonthDictionary.ContainsKey(month) ? abbrMonthDictionary[month] : 0;
                tpcMonthYearNo = (yearNo * 10000) + (monthNo * 100);
            }
            return tpcMonthYearNo;
        }
    }

    internal struct MonthYearResult
    {
        internal bool IsSuccess { get; set; }
        internal string Month { get; set; }
        internal string Year { get; set; }
    }
}
