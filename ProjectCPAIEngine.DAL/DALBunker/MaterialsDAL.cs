﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class MaterialsDAL
    {
        public static List<MT_MATERIALS> GetMaterialOrigin()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            where v.MET_ORIG_CRTY != null
                            select v;
                return query.ToList();
            }
        }

        public static List<MT_MATERIALS> GetMaterialAll()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            select v;
                return query.ToList();
            }
        }

        public static List<MT_MATERIALS> GetMaterials(string system, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            join vc in context.MT_MATERIALS_CONTROL on v.MET_NUM equals vc.MMC_FK_MATRIALS
                            where vc.MMC_SYSTEM.ToUpper() == system && vc.MMC_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        public static MT_MATERIALS getMaterialById(string system, string status, string id)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            join vc in context.MT_MATERIALS_CONTROL on v.MET_NUM equals vc.MMC_FK_MATRIALS
                            where vc.MMC_SYSTEM.ToUpper() == system && vc.MMC_STATUS.ToUpper() == status && v.MET_NUM == id
                            select v;
                return query.FirstOrDefault();
            }
        }

        public static MT_MATERIALS getMaterialById_New(string system,string status,string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from d in context.MT_MATERIALS
                            where d.MET_CREATE_TYPE == "COOL" && d.MET_MAT_DES_ENGLISH != null && d.MET_NUM == id

                            select d;

                            //orderby new { d.MET_MAT_DES_ENGLISH }
                             //select new { mKey = d.MET_NUM, mValue = d.MET_MAT_DES_ENGLISH };

                return query.FirstOrDefault();
            }
        }

        public static MT_MATERIALS GetMaterialById(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_MATERIALS> results = context.MT_MATERIALS.Where(x => x.MET_NUM.ToUpper() == id.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_MATERIALS();
                }
            }
        }

        public static MT_MATERIALS GetMaterialByNameAndOrigin(string name, string origin)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                MT_MATERIALS result = context.MT_MATERIALS.Where(x => x.MET_MAT_DES_ENGLISH.ToUpper() == name.ToUpper() && x.MET_ORIG_CRTY.ToUpper() == origin.ToUpper()).FirstOrDefault();
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<MT_MATERIALS> GetMaterialsClearLine()
        {                       
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            where v.MET_NUM.Contains("Y") 
                            && v.MET_FLA_MAT_DELETION == null
                            && v.MET_MAT_TYPE == "ZOIL"
                            select v;
                return query.ToList();
            }
        }


        public static List<MT_MATERIALS> GetMaterialsThroughput()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            where v.MET_MAT_TYPE == "ZSRV"
                            && v.MET_FLA_MAT_DELETION == null
                            select v;
                return query.ToList();
            }
        }
    }
}
