﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class UserGroupDAL
    {
        public CPAI_USER_GROUP findByUserAndSystem(string usrLogin, string usrSystem)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE"
                                                && x.USG_DELEGATE_FLAG.ToUpper() != "Y"  //25072017 add by poo for delegate
                                                ).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new CPAI_USER_GROUP();
                }
            }
        }

        public string findByUserAndSystems(string usrLogin, string usrSystem)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<string> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE"
                                                && x.USG_DELEGATE_FLAG.ToUpper() != "Y"  //25072017 add by poo for delegate
                                                ).Select(x => x.USG_USER_GROUP).ToList();
                if (results != null && results.Count() > 0)
                {
                    return string.Join("|", results);
                }
                else
                {
                    return "";
                }
            }
        }

        public CPAI_USER_GROUP findByUserAndSystemCool(string usrLogin, string usrSystem)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                && x.USG_USED_FLAG.ToUpper() == "Y"
                && x.USG_STATUS.ToUpper() == "ACTIVE"
                && x.USG_DELEGATE_FLAG.ToUpper() != "Y" //07082017 add by job for delegate 
                && (x.USG_USER_GROUP == "EXPERT" || x.USG_USER_GROUP == "EXPERT_SH" || x.USG_USER_GROUP == "SCEP" || x.USG_USER_GROUP == "SCEP_SH" || x.USG_USER_GROUP == "COOL-System Admin" || x.USG_USER_GROUP == "COOL-VIEWONLY")).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new CPAI_USER_GROUP();
                }
            }
        }
        public CPAI_USER_GROUP findByUserAndSystemDelegate(string usrLogin, string usrSystem)
        {
            //for deligate by poo 25072017
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                DateTime date = DateTime.Now.Date;
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE"
                                                && x.USG_DELEGATE_FLAG.ToUpper() == "Y"  //25072017 add by poo for delegate 
                                                && (x.USG_AFFECT_DATE <= date && x.USG_END_DATE >= date)
                                                ).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new CPAI_USER_GROUP();
                }
            }
        }
        public CPAI_USER_GROUP findByUserAndSystemDelegateCool(string usrLogin, string usrSystem)
        {
            //for deligate by poo 25072017
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                DateTime date = DateTime.Now.Date;
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE"
                                                && x.USG_DELEGATE_FLAG.ToUpper() == "Y"  //25072017 add by poo for delegate 
                                                && (x.USG_AFFECT_DATE <= date && x.USG_END_DATE >= date)
                                                && (x.USG_USER_GROUP == "EXPERT" || x.USG_USER_GROUP == "EXPERT_SH" || x.USG_USER_GROUP == "SCEP" || x.USG_USER_GROUP == "SCEP_SH" || x.USG_USER_GROUP == "COOL-System Admin" || x.USG_USER_GROUP == "COOL-VIEWONLY")
                                                ).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new CPAI_USER_GROUP();
                }
            }
        }

        public CPAI_USER_GROUP findByUserAndSystemDelegateVCool(string usrLogin, string usrSystem)
        {
            //for deligate vcool by ball 01092017
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                DateTime date = DateTime.Now.Date;
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE"
                                                && x.USG_DELEGATE_FLAG.ToUpper() == "Y"  //01092017 add by ball for vcool delegate 
                                                && (x.USG_AFFECT_DATE <= date && x.USG_END_DATE >= date)
                                                && (x.USG_USER_GROUP == "SCEP" || x.USG_USER_GROUP == "SCEP_SH" || x.USG_USER_GROUP == "SCSC" || x.USG_USER_GROUP == "SCSC_SH" || x.USG_USER_GROUP == "TNPB" 
                                                 || x.USG_USER_GROUP == "CMVP" || x.USG_USER_GROUP == "SCVP" || x.USG_USER_GROUP == "TNVP" || x.USG_USER_GROUP == "EVPC" || x.USG_USER_GROUP == "CMCS" || x.USG_USER_GROUP == "VCOOL-System Admin")
                                                ).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new CPAI_USER_GROUP();
                }
            }
        }
        public List<CPAI_USER_GROUP> findByUserDelegate(string usrLogin)
        {
            //for deligate by poo 25072017
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                DateTime date = DateTime.Now.Date;
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == usrLogin.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE"
                                                && x.USG_DELEGATE_FLAG.ToUpper() == "Y" 
                                                && (x.USG_AFFECT_DATE <= date && x.USG_END_DATE >= date)
                                                ).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_USER_GROUP>();
                }
            }
        }
        public CPAI_USER_GROUP findByUserGroupAndSystem(String userGroup, String usrSystem)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USG_USER_GROUP.ToUpper() == userGroup.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new CPAI_USER_GROUP();
                }
            }
        }
        public List<CPAI_USER_GROUP> findByGroup(String usrGroup)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_USER_GROUP> results = entity.CPAI_USER_GROUP.Where(x => x.USG_USER_GROUP.ToUpper() == usrGroup.ToUpper()
                                                && x.USG_USED_FLAG.ToUpper() == "Y"
                                                && x.USG_STATUS.ToUpper() == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_USER_GROUP>();
                }
            }
        }

        public List<USERS> findUserByGroup(String usrGroup)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> lstUsers = new List<USERS>();

                var usrs = from ug in entity.CPAI_USER_GROUP
                           join u in entity.USERS on ug.USG_FK_USERS equals u.USR_ROW_ID
                           where
ug.USG_USER_GROUP == usrGroup &&
ug.USG_USED_FLAG.ToUpper() == "Y" &&
ug.USG_STATUS.ToUpper() == "ACTIVE"
                           select new { u.USR_LOGIN, u.USR_EMAIL };

                if (usrs != null)
                {
                    foreach (var usr in usrs)
                    {
                        USERS u = new USERS();
                        u.USR_EMAIL = usr.USR_EMAIL;
                        u.USR_LOGIN = usr.USR_LOGIN;
                        lstUsers.Add(u);
                    }
                }
                return lstUsers;
            }
        }

        public List<USERS> findUserByGroupSystem(string usrGroup, string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> lstUsers = new List<USERS>();

                var usrs = from ug in entity.CPAI_USER_GROUP
                           join u in entity.USERS on ug.USG_FK_USERS equals u.USR_ROW_ID
                           where
                            ug.USG_USER_GROUP == usrGroup &&
                            ug.USG_USER_SYSTEM == system &&
                            ug.USG_USED_FLAG.ToUpper() == "Y" &&
                            ug.USG_STATUS.ToUpper() == "ACTIVE"
                           select new { u.USR_LOGIN, u.USR_EMAIL, u.USR_NOTI_ID, u.USR_OS };

                if (usrs != null)
                {
                    foreach (var usr in usrs)
                    {
                        USERS u = new USERS();
                        u.USR_EMAIL = usr.USR_EMAIL;
                        u.USR_LOGIN = usr.USR_LOGIN;
                        u.USR_NOTI_ID = usr.USR_NOTI_ID;
                        u.USR_OS = usr.USR_OS;
                        lstUsers.Add(u);
                    }
                }
                return lstUsers;
            }
        }

        public List<USERS> findUserBySystem(string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> lstUsers = new List<USERS>();

                var usrs = from ug in entity.CPAI_USER_GROUP
                           join u in entity.USERS on ug.USG_FK_USERS equals u.USR_ROW_ID
                           where
                            (ug.USG_USER_SYSTEM == system || null == system) &&
                            ug.USG_USED_FLAG.ToUpper() == "Y" &&
                            ug.USG_STATUS.ToUpper() == "ACTIVE"
                           select new { u.USR_ROW_ID, u.USR_LOGIN, u.USR_EMAIL, u.USR_NOTI_ID, u.USR_OS };

                if (usrs != null)
                {
                    foreach (var usr in usrs)
                    {
                        USERS u = new USERS();
                        u.USR_ROW_ID = usr.USR_ROW_ID;
                        u.USR_EMAIL = usr.USR_EMAIL;
                        u.USR_LOGIN = usr.USR_LOGIN;
                        u.USR_NOTI_ID = usr.USR_NOTI_ID;
                        u.USR_OS = usr.USR_OS;
                        lstUsers.Add(u);
                    }
                }
                return lstUsers;
            }
        }

        public List<USERS> findUserMailByGroupSystem(string usrGroup, string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> lstUsers = new List<USERS>();
                DateTime date = DateTime.Now.Date;
                var usrs = from ug in entity.CPAI_USER_GROUP
                           join u in entity.USERS on ug.USG_FK_USERS equals u.USR_ROW_ID
                           where
                            ug.USG_USER_GROUP == usrGroup &&
                            ug.USG_USER_SYSTEM == system &&
                            ug.USG_USED_FLAG.ToUpper() == "Y" &&
                            ug.USG_STATUS.ToUpper() == "ACTIVE" &&
                            (ug.USG_DELEGATE_FLAG == null || (ug.USG_DELEGATE_FLAG == "Y" && (ug.USG_AFFECT_DATE >= date && ug.USG_END_DATE <= date))) && ////for user group deligate by poo 25072017
                            u.USR_EMAIL_FLAG == "Y" 
                           select new { u.USR_ROW_ID,u.USR_LOGIN, u.USR_EMAIL, u.USR_NOTI_ID, u.USR_OS };

                if (usrs != null)
                {
                    foreach (var usr in usrs)
                    {
                        USERS u = new USERS();
                        u.USR_EMAIL = usr.USR_EMAIL;
                        u.USR_LOGIN = usr.USR_LOGIN;
                        u.USR_NOTI_ID = usr.USR_NOTI_ID;
                        u.USR_OS = usr.USR_OS;
                        u.USR_ROW_ID = usr.USR_ROW_ID;
                        lstUsers.Add(u);
                    }
                }
                return lstUsers;
            }
        }



        public List<USERS> findUserNotiByGroupSystem(string usrGroup, string system)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> lstUsers = new List<USERS>();
                DateTime date = DateTime.Now.Date;
                var usrs = from ug in entity.CPAI_USER_GROUP
                           join u in entity.USERS on ug.USG_FK_USERS equals u.USR_ROW_ID
                           where
                            ug.USG_USER_GROUP == usrGroup &&
                            ug.USG_USER_SYSTEM == system &&
                            ug.USG_USED_FLAG.ToUpper() == "Y" &&
                            ug.USG_STATUS.ToUpper() == "ACTIVE" &&
                            (ug.USG_DELEGATE_FLAG == null || (ug.USG_DELEGATE_FLAG == "Y" && (ug.USG_AFFECT_DATE >= date && ug.USG_END_DATE <= date))) && ////for user group deligate by poo 25072017
                            u.USR_NOTI_FLAG == "Y"
                           select new { u.USR_LOGIN, u.USR_EMAIL, u.USR_NOTI_ID, u.USR_OS };

                if (usrs != null)
                {
                    foreach (var usr in usrs)
                    {
                        USERS u = new USERS();
                        u.USR_EMAIL = usr.USR_EMAIL;
                        u.USR_LOGIN = usr.USR_LOGIN;
                        u.USR_NOTI_ID = usr.USR_NOTI_ID;
                        u.USR_OS = usr.USR_OS;
                        lstUsers.Add(u);
                    }
                }
                return lstUsers;
            }
        }

        public List<CPAI_USER_GROUP> getUserList(string usrLogin, string usrSystem)
        {
            DateTime date = DateTime.Now.Date;
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from u in context.USERS
                            join ug in context.CPAI_USER_GROUP on u.USR_ROW_ID equals ug.USG_FK_USERS
                            where ug.USG_USER_SYSTEM.ToUpper() == usrSystem && u.USR_LOGIN.ToUpper() == usrLogin.ToUpper() && ug.USG_STATUS.ToUpper() == "ACTIVE" 
                            && ((ug.USG_AFFECT_DATE <= date && ug.USG_END_DATE >= date) || ug.USG_DELEGATE_FLAG == null)  //07082017 add by job for delegate 
                            select ug;
                return query.ToList();
            }
        }
    }
}