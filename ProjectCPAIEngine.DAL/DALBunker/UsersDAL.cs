﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class UsersDAL
    {

        public void Update(USERS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.USERS.Find(data.USR_ROW_ID);
                    #region Set Value
                    _search.USR_OS = data.USR_OS;
                    _search.USR_APP_VERSION = data.USR_APP_VERSION;
                    _search.USR_NOTI_ID = data.USR_NOTI_ID;
                    _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                    _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<USERS> findByRowId(String usrRowId)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> results = entity.USERS.Where(x => x.USR_ROW_ID == usrRowId && x.USR_STATUS.ToUpper() == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<USERS>();
                }
            }
        }
        //public List<USERS> findByUserGroup(String usrGroup)
        //{
        //    using (EntityCPAIEngine entity = new EntityCPAIEngine())
        //    {
        //        List<USERS> results = entity.USERS.Where(x => x.USR_GROUP == usrGroup && x.USR_STATUS == "ACTIVE").ToList();
        //        if (results != null && results.Count() > 0)
        //        {
        //            return results;
        //        }
        //        else
        //        {
        //            return new List<USERS>();
        //        }
        //    }
        //}

        public List<USERS> findByLogin(String usrLogin)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> results = entity.USERS.Where(x => x.USR_LOGIN.ToUpper() == usrLogin.ToUpper() && x.USR_STATUS.ToUpper() == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<USERS>();
                }
            }
        }

        public List<USERS> findByLoginAndSystem(String usrLogin, String usrSystem)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<USERS> results = entity.USERS.Where(x => x.USR_LOGIN.ToUpper() == usrLogin.ToUpper() && x.USR_SYSTEM.ToUpper() == usrSystem.ToUpper() && x.USR_STATUS.ToUpper() == "ACTIVE").ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<USERS>();
                }
            }
        }

        public static string getFullName(string usrLogin)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                USERS results = entity.USERS.Where(x => x.USR_LOGIN.ToUpper() == usrLogin.ToUpper() && x.USR_STATUS.ToUpper() == "ACTIVE").FirstOrDefault();
                if (results != null)
                {
                    return results.USR_FIRST_NAME_EN + " " + results.USR_LAST_NAME_EN;
                }
                else
                {
                    return usrLogin;
                }
            }
        }

    }
}