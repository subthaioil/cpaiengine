﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class VehicleDAL
    {
        public static List<MT_VEHICLE> GetVehicle(string system, string status, string type)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VEHICLE
                            join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE
                            where vc.MCC_SYSTEM.ToUpper() == system && vc.MCC_STATUS.ToUpper() == status && vc.MCC_TYPE.ToUpper() == type
                            select v;
                return query.ToList();
            }
        }

        public static MT_VEHICLE GetVehicleById(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_VEHICLE> results = context.MT_VEHICLE.Where(x => x.VEH_ID.ToUpper() == id.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_VEHICLE();
                }
            }
        }

        public static List<MT_VEHICLE> GetDetailById(string system, string status, string type, string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VEHICLE
                            join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE
                            where vc.MCC_SYSTEM.ToUpper() == system && vc.MCC_STATUS.ToUpper() == status && vc.MCC_TYPE.ToUpper() == type
                            && v.VEH_ID == id
                            select v;
                return query.ToList();
            }
        }

    }
}
