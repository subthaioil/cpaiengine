﻿using com.pttict.engine.dal.Entity;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Entity
{
    public class CDO_DATA_WRAP : CDO_DATA
    {

    }

    public class CDOButtonAction
    {
        public string name { get; set; }
        public string page_url { get; set; }
        public string call_xml { get; set; }

    }

    public partial class CDO_DATA
    {
        public string REQ_TRAN_ID { get; set; }
        public string TRAN_ID { get; set; }

        public string USER_GROUP { get; set; }
        public string CDA_REQUEST_BY { get; set; }

        #region extra attrivutes
        //Extra
        public bool IS_DISABLED { get; set; }
        public bool IS_DISABLED_NOTE { get; set; }
        public List<CDOButtonAction> Buttons { get; set; }
        #endregion


        public void UpdateChild()
        {
            //EntityCPAIEngine db = new EntityCPAIEngine();
            //db.Configuration.ProxyCreationEnabled = false;
            //this.IS_DISABLED = this.CDA_STATUS != "DRAFT";
            //this.IS_DISABLED_NOTE = (this.CDA_STATUS == "APPROVED" || this.CDA_STATUS == "CANCEL");

            //this.IS_DISABLED = this.CDA_STATUS != "DRAFT";
            //this.IS_DISABLED_NOTE = (this.CDA_STATUS == "APPROVED" || this.CDA_STATUS == "CANCEL");

            //this.CDA_REQUEST_BY = CDO_USER_DAL.GetName(CDA_CREATED_BY);

            //this.CDO_ATTACH_FILE = db.CDO_ATTACH_FILE.Where(x => x.CAT_FK_CDO_DATA == this.CDA_ROW_ID).OrderBy(x => x.CAT_ROW_ID).ToList();
            //this.CDO_BIDDING_ITEMS = db.CDO_BIDDING_ITEMS.Where(x => x.CBI_FK_CDO_DATA == this.CDA_ROW_ID).OrderBy(x => x.CBI_ROW_ID).ToList();
            //foreach (CDO_BIDDING_ITEMS e in this.CDO_BIDDING_ITEMS)
            //{
            //    EntityCPAIEngine db2 = new EntityCPAIEngine();
            //    db2.Configuration.ProxyCreationEnabled = false;
            //    e.CDO_ROUND = db2.CDO_ROUND.Where(x => x.CRN_FK_CDO_BIDDING_ITEMS == e.CBI_ROW_ID).OrderBy(x => x.CRN_ROW_ID).ToList();
            //}
            //this.CDO_CRUDE_PURCHASE_DETAIL = GET_CDO_CRUDE_PURCHASE_DETAIL();
            //this.CDO_PROPOSAL_ITEMS = db.CDO_PROPOSAL_ITEMS.Where(x => x.CSI_FK_CDO_DATA == this.CDA_ROW_ID).OrderBy(x => x.CSI_ROW_ID).ToList();
        }

        //private List<CDO_CRUDE_PURCHASE_DETAIL> GET_CDO_CRUDE_PURCHASE_DETAIL()
        //{
        //    EntityCPAIEngine db = new EntityCPAIEngine();
        //    List<CDO_CRUDE_PURCHASE_DETAIL_EXT> cdo_ext = db.Database.SqlQuery<CDO_CRUDE_PURCHASE_DETAIL_EXT>(
        //        "SELECT * FROM VW_CDO_CRUDE_PURCHASE_DETAIL WHERE CPD_FK_CDO_DATA = '" + this.CDA_ROW_ID + "'"
        //    ).ToList();
        //    List<CDO_CRUDE_PURCHASE_DETAIL> results = new List<CDO_CRUDE_PURCHASE_DETAIL>();
        //    foreach (CDO_CRUDE_PURCHASE_DETAIL_EXT e in cdo_ext)
        //    {
        //        CDO_CRUDE_PURCHASE_DETAIL a = (CDO_CRUDE_PURCHASE_DETAIL)e;
        //        results.Add(a);
        //    }
        //    return results;
        //}
    }
}
