﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
    public class CDP_DATA_DAL
    {
        public void Save(CDP_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CDP_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CDP_DATA data, EntityCPAIEngine context)
        {
            try
            {               
                var _search = context.CDP_DATA.Find(data.CDA_ROW_ID);
                #region Set Value
                _search.CDA_REASON = data.CDA_REASON;
                //if (data.CDA_EXPLANATION != null)
                //{
                //    _search.CDA_EXPLANATION = data.CDA_EXPLANATION;
                //}
                //if ( data.CDA_NOTE != null)
                //{
                //    _search.CDA_NOTE = data.CDA_NOTE;
                //}
                _search.CDA_STATUS = data.CDA_STATUS;
                _search.CDA_UPDATED_BY = data.CDA_UPDATED_BY;
                _search.CDA_UPDATED = data.CDA_UPDATED;
                #endregion
                context.SaveChanges();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNote(CDP_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CDP_DATA.Find(data.CDA_ROW_ID);
                #region Set Value
                if(_search != null)
                {
                    _search.CDA_NOTE = data.CDA_NOTE; 
                    _search.CDA_EXPLANATION = data.CDA_EXPLANATION; 
                    _search.CDA_UPDATED_BY = data.CDA_UPDATED_BY;
                    _search.CDA_UPDATED = data.CDA_UPDATED;
                    #endregion
                    context.SaveChanges();
                } 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
             
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CDP_DATA WHERE CDA_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteByTransactionId(string TransID, EntityCPAIEngine context)
        {
            bool isSuccess = false;
            try
            {
                string sql = "DELETE FROM CDP_DATA WHERE CDA_ROW_ID = :CDA_ROW_ID";
                var result = context.Database.ExecuteSqlCommand(sql, new OracleParameter(":CDA_ROW_ID", TransID));
                context.SaveChanges();

                isSuccess = true;
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        //public List<CDP_DATA> GetCDPDataStatusApprove(bool EditMode)
        //{
        //    try
        //    {
        //        using (var context = new EntityCPAIEngine())
        //        {
        //            List<CDP_DATA> lstReturn = new List<CDP_DATA>();
        //            var lstSubmitTCE = context.CPAI_TCE_WS.Where(x => x.CTW_STATUS == "SUBMIT").ToList();
        //            var lstCHI = context.CDP_DATA.Where(x => x.CDA_STATUS.ToUpper() == "APPROVED" && x.CDA_FREIGHT == "WS").ToList();
        //            if (lstCHI != null)
        //            {
        //                for (int ii = 0; ii < lstCHI.Count; ii++)
        //                {
        //                    if (EditMode)
        //                    {
        //                        if (lstSubmitTCE.Where(x => x.CTW_FK_CDP_DATA == lstCHI[ii].CDA_ROW_ID).ToList().Count > 0) continue;
        //                    }
        //                    string PurchaseNO = lstCHI[ii].CDA_PURCHASE_NO;
        //                    var lstOffer = (from chi in context.CDP_DATA
        //                                    join CDP_off in context.CDP_OFFER_ITEMS on chi.CDA_ROW_ID equals CDP_off.IOI_FK_CDP_DATA
        //                                    where chi.CDA_PURCHASE_NO == PurchaseNO// && CDP_off.IOI_FINAL_FLAG == "Y"
        //                                    select CDP_off).ToList();

        //                    if (lstOffer != null)
        //                    {
        //                        for (int i = 0; i < lstOffer.Count; i++)
        //                        {
        //                            string IOI_ROW_ID = lstOffer[i].IOI_ROW_ID;
        //                            var lstRItem = (from CDP_rounI in context.CDP_ROUND_ITEMS where CDP_rounI.IRI_FK_OFFER_ITEMS == IOI_ROW_ID select CDP_rounI).ToList();

        //                            if (lstRItem != null)
        //                            {
        //                                for (int ir = 0; ir < lstRItem.Count; ir++)
        //                                {
        //                                    string IRI_ROW_ID = lstRItem[ir].IRI_ROW_ID;
        //                                    var lstRound = (from CDP_roun in context.CDP_ROUND where CDP_roun.IIR_FK_ROUND_ITEMS == IRI_ROW_ID select CDP_roun).ToList();
        //                                    for (int r = 0; r < lstRound.Count; r++)
        //                                    {
        //                                        lstRItem[ir].CDP_ROUND.Add(lstRound[r]);
        //                                    }
        //                                    lstOffer[i].CDP_ROUND_ITEMS.Add(lstRItem[ir]);
        //                                }
        //                            }
        //                            lstCHI[ii].CDP_OFFER_ITEMS.Add(lstOffer[i]);
        //                        }

        //                    }
        //                    lstReturn.Add(lstCHI[ii]);

        //                }
        //            }
        //            return lstReturn;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public List<CDP_DATA> GetCHIDataStatusApprove(object sTATUS_APPROVED)
        //{
        //    throw new NotImplementedException();
        //}

        public string GetCDPNOByID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CDP_DATA.Where(x => x.CDA_ROW_ID.ToUpper() == ID.ToUpper()).ToList();
                    if (_Data.Count > 0) return _Data[0].CDA_PURCHASE_NO;
                    else return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CDP_DATA GetByID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CDP_DATA.Where(x => x.CDA_ROW_ID.ToUpper() == ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
