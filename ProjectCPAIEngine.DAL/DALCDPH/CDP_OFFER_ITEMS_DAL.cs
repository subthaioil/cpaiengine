﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
   public class CDP_OFFER_ITEMS_DAL
    {
        public void Save(CDP_OFFER_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_OFFER_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CDP_OFFER_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_OFFER_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CDP_OFFER_ITEMS> GetByID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiOffer = context.CDP_OFFER_ITEMS.Where(x => x.COI_FK_CDP_DATA.ToUpper() == ID.ToUpper()).ToList();
                    return _chiOffer;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
