﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
    public class CDP_ROUND_DAL
    {
        public void Save(CDP_ROUND data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_ROUND.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CDP_ROUND data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_ROUND.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CDP_ROUND> GetByID(string CRI_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Round = context.CDP_ROUND.Where(x => x.CRD_FK_ROUND_ITEMS.ToUpper() == CRI_ROW_ID.ToUpper()).ToList();
                    return _Round;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
