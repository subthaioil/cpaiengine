﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
    public class CDP_ROUND_ITEMS_DAL
    {
        public void Save(CDP_ROUND_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_ROUND_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save(CDP_ROUND_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_ROUND_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CDP_ROUND_ITEMS> GetByID(string COI_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiItems = context.CDP_ROUND_ITEMS.Where(x => x.CRI_FK_OFFER_ITEMS.ToUpper() == COI_ROW_ID.ToUpper()).OrderBy(x => x.CRI_ROUND_NO).ToList();
                    return _chiItems;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
