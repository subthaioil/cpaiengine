﻿using System;
using CustomExtension;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Entity
{
    public class CDS_CRUDE_PURCHASE_DETAIL_EXT: CDS_CRUDE_PURCHASE_DETAIL
    {

    }
    public partial class CDS_CRUDE_PURCHASE_DETAIL
    {
        public string SHOW_DOC_DATE
        {
            get
            {
                if (PURCHASE_DATE != null)
                {
                    return PURCHASE_DATE.ToString("MMMM dd yyyy");
                }
                return "";
            }
        }
        public string SHOW_QUANTITY
        {
            get
            {
                if (QUANTITY_MIN != null && QUANTITY_MAX != null && QUANTITY_UNIT != null)
                {
                    return QUANTITY_MIN + " - " + QUANTITY_MAX + " " + QUANTITY_UNIT;
                }
                return "";
            }
        }
        public string CDA_ROW_ID { get; set; }
        public string PURCHASE_NO { get; set; }
        public string MET_NUM { get; set; }
        public string CRUDE_NAME { get; set; }
        public string ORIGIN { get; set; }
        public string VND_ACC_NUM_VENDOR { get; set; }
        public string SUPPLIER { get; set; }
        public string CONTACT_PERSON { get; set; }
        public string QUANTITY_MIN { get; set; }
        public string QUANTITY_MAX { get; set; }
        public string QUANTITY_UNIT { get; set; }
        public string INCOTERMS { get; set; }
        public string PURCHASING_PRICE { get; set; }
        public string MARKET_SOURCE { get; set; }
        public string LP_MAX_PURCHASE_PRICE { get; set; }
        public string BENCHMARK_PRICE { get; set; }
        public string RANK_ROUND { get; set; }
        public Nullable<Decimal> MARGIN_VS_LP { get; set; }
        public string MARGIN_VS_LP_UNIT { get; set; }
        public System.DateTime PURCHASE_DATE { get; set; }
        public string FEEDSTOCK { get; set; }
        public string MARGIN { get; set; }
        public string CREATED_BY { get; set; }

        public string REQ_TRAN_ID { get; set; }
        public string REASON { get; set; }
        public string TYPE { get; set; }
        public string URL
        {
            get
            {
                string link = "../../CPAIMVC/CrudePurchase";
                return string.Format("{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}", link, CDA_ROW_ID.SEncrypt(), REQ_TRAN_ID.SEncrypt(), PURCHASE_NO.SEncrypt(), REASON.SEncrypt(), TYPE.SEncrypt());
            }
        }
    }
}
