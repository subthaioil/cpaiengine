﻿using ProjectCPAIEngine.DAL.DALCDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Entity
{
    public class CDS_DATA_WRAP: CDS_DATA
    {

    }

    public class CDSButtonAction
    {
        public string name { get; set; }
        public string page_url { get; set; }
        public string call_xml { get; set; }

    }

    public partial class CDS_DATA
    {
        public string REQ_TRAN_ID { get; set; }
        public string TRAN_ID { get; set; }

        public string USER_GROUP { get; set; }
        public string CDA_REQUEST_BY { get; set; }

        #region extra attrivutes
        //Extra
        public bool IS_DISABLED { get; set; }
        public bool IS_DISABLED_NOTE { get; set; }
        public List<CDSButtonAction> Buttons { get; set; }
        #endregion


        public void UpdateChild()
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            db.Configuration.ProxyCreationEnabled = false;
            this.IS_DISABLED = this.CDA_STATUS != "DRAFT";
            this.IS_DISABLED_NOTE = (this.CDA_STATUS == "APPROVED" || this.CDA_STATUS == "CANCEL");

            this.IS_DISABLED = this.CDA_STATUS != "DRAFT";
            this.IS_DISABLED_NOTE = (this.CDA_STATUS == "APPROVED" || this.CDA_STATUS == "CANCEL");

            this.CDA_REQUEST_BY = CDS_USER_DAL.GetName(CDA_CREATED_BY);

            this.CDS_ATTACH_FILE = db.CDS_ATTACH_FILE.Where(x => x.CAT_FK_CDS_DATA == this.CDA_ROW_ID).OrderBy(x => x.CAT_ROW_ID).ToList();
            this.CDS_BIDDING_ITEMS = db.CDS_BIDDING_ITEMS.Where(x => x.CBI_FK_CDS_DATA == this.CDA_ROW_ID).OrderBy(x => x.CBI_ROW_ID).ToList();
            foreach(CDS_BIDDING_ITEMS e in this.CDS_BIDDING_ITEMS)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                e.CDS_ROUND = db2.CDS_ROUND.Where(x => x.CRN_FK_CDS_BIDDING_ITEMS == e.CBI_ROW_ID).OrderBy(x => x.CRN_ROW_ID).ToList();
            }
            this.CDS_CRUDE_PURCHASE_DETAIL = GET_CDS_CRUDE_PURCHASE_DETAIL();
            this.CDS_PROPOSAL_ITEMS = db.CDS_PROPOSAL_ITEMS.Where(x => x.CSI_FK_CDS_DATA == this.CDA_ROW_ID).OrderBy(x => x.CSI_ROW_ID).ToList();
        }

        private List<CDS_CRUDE_PURCHASE_DETAIL> GET_CDS_CRUDE_PURCHASE_DETAIL()
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            List<CDS_CRUDE_PURCHASE_DETAIL_EXT> cds_ext = db.Database.SqlQuery<CDS_CRUDE_PURCHASE_DETAIL_EXT>(
                "SELECT * FROM VW_CDS_CRUDE_PURCHASE_DETAIL WHERE CPD_FK_CDS_DATA = '" + this.CDA_ROW_ID + "'"
            ).ToList();
            List<CDS_CRUDE_PURCHASE_DETAIL> results = new List<CDS_CRUDE_PURCHASE_DETAIL>();
            foreach(CDS_CRUDE_PURCHASE_DETAIL_EXT e in cds_ext)
            {
                CDS_CRUDE_PURCHASE_DETAIL a = (CDS_CRUDE_PURCHASE_DETAIL)e;
                results.Add(a);
            }
            return results;
        }
    }
}
