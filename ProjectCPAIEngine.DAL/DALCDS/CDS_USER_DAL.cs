﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDS
{
    public class CDS_USER_DAL
    {
        private static EntityCPAIEngine context = new EntityCPAIEngine();

        public static string GetName(string userid)
        {
            string result = "";
            try
            {
                USERS u = context.USERS.Where(a => a.USR_LOGIN.ToUpper() == userid.ToUpper()).FirstOrDefault();
                //USERS u = context.USERS.Where(m => m.USR_LOGIN.ToUpper() == (userid ?? "").ToUpper()).FirstOrDefault();
                result = u.USR_FIRST_NAME_EN + " " + u.USR_LAST_NAME_EN;
            }
            catch (Exception e)
            {

            }
            return result;
        }
    }
}
