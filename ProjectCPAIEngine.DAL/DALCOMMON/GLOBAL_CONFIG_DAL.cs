﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOMMON
{
    public class GLOBAL_CONFIG_DAL
    {
        public string Get(string key)
        {
            string result = "";
            EntityCPAIEngine db = new EntityCPAIEngine();
            GLOBAL_CONFIG gc = db.GLOBAL_CONFIG.Where(m => m.GCG_KEY == key).FirstOrDefault();
            if (gc != null)
            {
                result = gc.GCG_VALUE;
            }
            return result;
        }

        public bool Contain(string key, string target)
        {
            target = target ?? "";
            target = target.ToLower();
            string result = "";
            EntityCPAIEngine db = new EntityCPAIEngine();
            GLOBAL_CONFIG gc = db.GLOBAL_CONFIG.Where(m => m.GCG_KEY == key).FirstOrDefault();
            List<string> strings = new List<string>();
            if (gc != null)
            {
                string val = gc.GCG_VALUE;
                val = val.ToLower();
                strings = val.Split('|').ToList();
            }

            return strings.Contains(target);
        }
    }
}
