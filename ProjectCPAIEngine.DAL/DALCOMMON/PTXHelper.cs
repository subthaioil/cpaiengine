﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOMMON
{
    public class PTXHelper
    {
        public static string CreateLogDir(string base_dir)
        {
            string out_put_dir = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\" + base_dir + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + "\\";
            Directory.CreateDirectory(out_put_dir);
            return out_put_dir;
        }

        public static void Log(string message, string mode = "C")
        {
            string out_put_dir = CreateLogDir("PTX_SPECIAL");
            string filePath = out_put_dir + "USER_INPUT.txt";
            switch (mode)
            {
                case "C":
                    filePath = out_put_dir + "COMMON.txt";
                    break;
                default:
                    filePath = out_put_dir + mode + ".txt";
                    break;
            }

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(message);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }

            //var microsoftDateFormatSettings = new JsonSerializerSettings
            //{
            //    DateParseHandling = DateParseHandling.None,
            //    DateFormatHandling = DateFormatHandling.IsoDateFormat,
            //    Formatting = Formatting.Indented,
            //    //DateTimeZoneHandling = DateTimeZoneHandling.Local,
            //    //DateParseHandling = DateParseHandling.DateTimeOffset
            //};
            //using (StreamWriter writer = new StreamWriter(filePath, true))
            //{
            //    string json = JsonConvert.SerializeObject(data, microsoftDateFormatSettings);
            //    writer.WriteLine(json);
            //    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            //}

        }
    }
}
