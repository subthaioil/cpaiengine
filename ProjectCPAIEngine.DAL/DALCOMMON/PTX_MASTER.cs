﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOMMON
{
    public class SELECT_CRITERIA
    {
        public string ID { get; set; }
        public string VALUE { get; set; }
    }

    public class PTX_VENDOR_CRITERIA : SELECT_CRITERIA
    {
        public string TYPE { get; set; }
    }

    public class PTX_CUSTOMER_CRITERIA : SELECT_CRITERIA
    {
        public string COMPANY_CODE { get; set; }
        public string PAYMENT_TERM_DETAIL { get; set; }
    }

    public class PTX_MASTER_CRITERIA : PTX_CUSTOMER_CRITERIA
    {
        public string MET_PRODUCT_DENSITY { get; set; }
        public string NAME { get; set; }
        public string GROUP { get; set; }
        public string DETAIL { get; set; }
        public string FK_MT_CUST { get; set; }
        public bool INCLUDED_FREIGHT { get; set; }
    }


    public class PTX_INCOTERM_CRITERIA : SELECT_CRITERIA
    {
        public bool INCLUDED_FREIGHT { get; set; }
    }

    public class PTX_JETTY_CRITERIA : SELECT_CRITERIA
    {
        public string CREATE_TYPE { get; set; }
    }

    public class GLOBALCONFIG_CRUDE_SALE_OP
    {
        public string[] TOLERANCE { get; set; }
        public string[] QUANTITY_UNIT { get; set; }
    }

    public class PTX_MASTER
    {
        public List<PTX_CUSTOMER_CRITERIA> GetCustomer()
        {
            return new List<PTX_CUSTOMER_CRITERIA>();
        }
    }
}
