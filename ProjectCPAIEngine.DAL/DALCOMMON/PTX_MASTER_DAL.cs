using Newtonsoft.Json;
using CustomExtension;
using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOMMON
{
    public class PTX_MASTER_DAL
    {
        EntityCPAIEngine context = new EntityCPAIEngine();

        public string GetGlobalConfigValue(string key)
        {
            string result = "";
            GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == key).FirstOrDefault();
            if (gc != null)
            {
                result = gc.GCG_VALUE;
            }
            return result;
        }

        public string[] GetArrayFromGlobalConfig(string key)
        {
            string raw = this.GetGlobalConfigValue(key);
            if (raw != null)
            {
                string[] result = JsonConvert.DeserializeObject<string[]>(raw);
                return result;
            }
            return new string[0];
        }

        public List<SELECT_CRITERIA> GetDataFromGlobalConfigArray(string key)
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            string[] ret_from_gc = this.GetArrayFromGlobalConfig(key);
            foreach (string elem in ret_from_gc)
            {
                SELECT_CRITERIA cur = new SELECT_CRITERIA();
                cur.ID = elem;
                cur.VALUE = elem;
                results.Add(cur);
            }
            return results;
        }

        public List<MT_CUST_DETAIL> GetCustomers(string system, string type = "OTHER", string status = "ACTIVE")
        {
            system = system.ToUpper();
            type = (type ?? "OTHER").ToUpper();

            List<MT_CUST_DETAIL> result = context.MT_CUST_CONTROL.Where(m =>
                    m.MCR_SYSTEM.ToUpper() == system &&
                    m.MCR_TYPE.ToUpper() == type &&
                    m.MCR_STATUS.ToUpper() == status &&
                    m.MT_CUST_DETAIL != null
                ).Select(m => m.MT_CUST_DETAIL).ToList();
            return result;
        }

        public List<PTX_VENDOR_CRITERIA> GetSupplier(string system, string status = "ACTIVE")
        {
            List<MT_VENDOR_CONTROL> vendors = context.MT_VENDOR_CONTROL.Where(m =>
                m.MVC_SYSTEM.ToUpper() == system &&
                m.MVC_STATUS.ToUpper() == status &&
                m.MT_VENDOR != null
            ).ToList();

            List<PTX_VENDOR_CRITERIA> results = vendors.Select(m => new PTX_VENDOR_CRITERIA()
            {
                ID = m.MT_VENDOR.VND_ACC_NUM_VENDOR,
                VALUE = (m.MT_VENDOR.VND_NAME1 ?? "") + " " +  (m.MT_VENDOR.VND_NAME2 ?? ""),
                TYPE = m.MVC_TYPE
            }).ToList();
            return results;
        }

        public List<SELECT_CRITERIA> GetVGO()
        {
            List<CDS_CRUDE_PURCHASE_DETAIL_EXT> cdp = this.SearchCrudeRefs(null);
            List<SELECT_CRITERIA> results = cdp.Where(m => m.FEEDSTOCK == "VGO").Select(m => new SELECT_CRITERIA()
            {
                ID = m.CRUDE_NAME,
                VALUE = m.CRUDE_NAME
            })
            .Distinct().ToList();
            return results;
        }

        public List<SELECT_CRITERIA> GetLR()
        {
            List<CDS_CRUDE_PURCHASE_DETAIL_EXT> cdp = this.SearchCrudeRefs(null);
            List<SELECT_CRITERIA> results = cdp.Where(m => m.FEEDSTOCK == "LR").Distinct().Select(m => new SELECT_CRITERIA()
            {
                ID = m.CRUDE_NAME,
                VALUE = m.CRUDE_NAME
            })
            .Distinct().ToList();
            return results;
        }

        public List<SELECT_CRITERIA> GetDropDownMaster(string table_name, string status = "ACTIVE")
        {
            List<SELECT_CRITERIA> result = context.Database.SqlQuery<SELECT_CRITERIA>("SELECT ID, VALUE FROM " + table_name + " WHERE STATUS = '" + status + "'").ToList();
            return result;
        }
        public List<PTX_CUSTOMER_CRITERIA> GetCustomerList(string system, string type = "OTHER")
        {
            List<PTX_CUSTOMER_CRITERIA> customers = new List<PTX_CUSTOMER_CRITERIA>();
            List<MT_CUST_DETAIL> customer_details = this.GetCustomers(system, type);
            foreach (MT_CUST_DETAIL cust in customer_details)
            {
                PTX_CUSTOMER_CRITERIA criteria = new PTX_CUSTOMER_CRITERIA();
                criteria.ID = cust.MCD_FK_CUS;
                criteria.VALUE = cust.MCD_NAME_1 + " " + (cust.MCD_NAME_2 ?? "");
                criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
                criteria.PAYMENT_TERM_DETAIL = PAF_MASTER_DAL.GetPaymentTermForCustomer(cust.MCD_FK_CUS, cust.MCD_FK_COMPANY);
                customers.Add(criteria);
            }
            customers = customers.OrderBy(m => m.VALUE).ToList();
            return customers;
        }

        public List<SELECT_CRITERIA> GetMatrials(string system, string status = "ACTIVE")
        {
            List<SELECT_CRITERIA> result = new List<SELECT_CRITERIA>();
            //List<MT_MATERIALS> mats = context.MT_MATERIALS_CONTROL.Where(m =>
            //    m.MMC_SYSTEM.ToUpper() == system.ToUpper() &&
            //    m.MMC_STATUS.ToUpper() == status.ToUpper() &&
            //    m.MT_MATERIALS != null
            //)
            //.Select(m => m.MT_MATERIALS)
            //.ToList();
            List<MT_MATERIALS> mats = context.MT_MATERIALS.Where(m => (m.MET_CREATE_TYPE ?? "").ToUpper() == "COOL").ToList();

            mats = mats.Where(m => m.MET_MAT_DES_ENGLISH != null).OrderBy(m => m.MET_MAT_DES_ENGLISH).ToList();
            foreach (MT_MATERIALS mm in mats)
            {
                result.Add(new SELECT_CRITERIA() {
                    ID = mm.MET_NUM,
                    VALUE = mm.MET_MAT_DES_ENGLISH
                });
            }
            return result;
        }

        public List<SELECT_CRITERIA> GetIncoterms()
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            List<MT_INCOTERMS> incoterms = context.MT_INCOTERMS.Where(m => m.MIN_STATUS == "ACTIVE").OrderBy(m => m.MIN_INCOTERMS).ToList();
            foreach (MT_INCOTERMS elem in incoterms)
            {
                SELECT_CRITERIA cur = new SELECT_CRITERIA();
                cur.ID = elem.MIN_INCOTERMS;
                cur.VALUE = elem.MIN_INCOTERMS;
                //cur.INCLUDED_FREIGHT = elem.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                results.Add(cur);
            }
            return results;
        }


        public List<SELECT_CRITERIA> GetPricePerQuantityUnit()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_PRICE_PER_QAUNTITY_UNIT");
            return results;
        }

        public List<SELECT_CRITERIA> GetSaleType()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_SALE_TYPE");
            return results;
        }

        public List<SELECT_CRITERIA> GetSaleMethod()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_SALE_METHOD");
            return results;
        }

        public List<SELECT_CRITERIA> GetFeedStock()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_FEEDSTOCK");
            return results;
        }

        public List<SELECT_CRITERIA> GetPaymentTerm()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_PAYMENT_TERM");
            return results;
        }

        public List<SELECT_CRITERIA> GetProposalReason()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_PROPOSAL_REASON");
            return results;
        }

        public List<SELECT_CRITERIA> GetToleranceOption()
        {
            List<SELECT_CRITERIA> results = this.GetDropDownMaster("CDS_MT_TOLERANCE_OPTION");
            return results;
        }

        public List<SELECT_CRITERIA> GetBenchmarkPrice()
        {
            List<SELECT_CRITERIA> results = context.FORMULA_BENCHMARK.Select(m => new SELECT_CRITERIA() { ID = m.CODE, VALUE = m.NAME }).ToList();
            return results;
        }

        public List<SELECT_CRITERIA> GetTolerance()
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            string raw = this.GetGlobalConfigValue("JSON_CRUDE_SALE_OP");
            GLOBALCONFIG_CRUDE_SALE_OP r = Newtonsoft.Json.JsonConvert.DeserializeObject<GLOBALCONFIG_CRUDE_SALE_OP>(raw);
            if (r != null && r.TOLERANCE != null) {
                results = r.TOLERANCE.Select(m => new SELECT_CRITERIA() { ID = m, VALUE = m }).ToList();
            }
            return results;
        }

        public List<SELECT_CRITERIA> GetQuantityUnit()
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            string raw = this.GetGlobalConfigValue("JSON_CRUDE_SALE_OP");
            GLOBALCONFIG_CRUDE_SALE_OP r = Newtonsoft.Json.JsonConvert.DeserializeObject<GLOBALCONFIG_CRUDE_SALE_OP>(raw);
            if (r != null && r.QUANTITY_UNIT != null)
            {
                results = r.QUANTITY_UNIT.Select(m => new SELECT_CRITERIA() { ID = m, VALUE = m }).ToList();
            }
            return results;
        }

        public List<CDS_CRUDE_PURCHASE_DETAIL_EXT> SearchCrudeRefs(CDS_CRUDE_REF_CRITERIA criteria)
        {
            List<CDS_CRUDE_PURCHASE_DETAIL_EXT> results = context.Database.SqlQuery<CDS_CRUDE_PURCHASE_DETAIL_EXT>("SELECT * FROM VW_CRUDE_PURCHASE_REF").ToList();
           
            if (criteria != null)
            {
                if (!String.IsNullOrEmpty(criteria.FEEDSTOCK))
                {
                    results = results.Where(m => m.FEEDSTOCK == criteria.FEEDSTOCK).ToList();
                }

                if (criteria.FEEDSTOCK == "CRUDE" && !String.IsNullOrEmpty(criteria.NAME))
                {
                    results = results.Where(m => m.MET_NUM == criteria.NAME).ToList();
                }
                else if (criteria.FEEDSTOCK != "CRUDE" && !String.IsNullOrEmpty(criteria.NAME))
                {
                    results = results.Where(m => m.MET_NUM == criteria.NAME).ToList();
                }

                if (criteria.PURCHASE_DATE_FROM != null && criteria.PURCHASE_DATE_TO != null)
                {
                    results = results.Where(m => m.PURCHASE_DATE >= criteria.PURCHASE_DATE_FROM && m.PURCHASE_DATE <= criteria.PURCHASE_DATE_TO).ToList();
                }

                if (!String.IsNullOrEmpty(criteria.PURCHASE_NO))
                {
                    results = results.Where(m => (m.PURCHASE_NO ?? "").Contains(criteria.PURCHASE_NO.ToUpper())).ToList();
                }

                if (!String.IsNullOrEmpty(criteria.SUPPLIER))
                {
                    results = results.Where(m => m.VND_ACC_NUM_VENDOR == criteria.SUPPLIER).ToList();
                }
            }
            return results;
        }

    }
}
