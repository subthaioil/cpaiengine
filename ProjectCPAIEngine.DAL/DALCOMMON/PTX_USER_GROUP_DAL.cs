﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectCPAIEngine.DAL.DALCOMMON
{
    public class PTX_USER_GROUP_DAL
    {
        public string GetUserGroup(string username, string system)
        {
            username = username ?? "";
            username = username.Trim();
            EntityCPAIEngine context = new EntityCPAIEngine();
            string query = "SELECT cug.USG_USER_GROUP FROM CPAI_USER_GROUP cug INNER JOIN USERS u ON UPPER(cug.USG_FK_USERS) = UPPER(u .USR_ROW_ID) WHERE UPPER(u .USR_LOGIN) = UPPER(:username) AND UPPER(cug.USG_USER_SYSTEM) = UPPER(:system) AND UPPER(cug.USG_STATUS) = 'ACTIVE'";
            string usergroup = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("username", username), new OracleParameter("system", system) }).FirstOrDefault();
            return usergroup ?? "";
        }

    }
}
