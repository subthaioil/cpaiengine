﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_ASSAY_DAL
    {
        public void Save(COO_ASSAY cooAssay, EntityCPAIEngine context)
        {
            try
            {
                context.COO_ASSAY.Add(cooAssay);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_ASSAY data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_ASSAY.Find(data.COAS_ROW_ID);
                #region Set Value
                _search.COAS_FILE_PATH = data.COAS_FILE_PATH;
                _search.COAS_UPDATED_BY = data.COAS_UPDATED_BY;
                _search.COAS_UPDATED = data.COAS_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public COO_ASSAY GetLatestByID(string CODA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_ASSAY.Where(x => x.COAS_FK_COO_DATA.ToUpper() == CODA_ROW_ID.ToUpper()).OrderByDescending(x => x.COAS_CREATED).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
