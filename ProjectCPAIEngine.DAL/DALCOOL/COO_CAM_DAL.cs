﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_CAM_DAL
    {
        public void Save(COO_CAM cooCam, EntityCPAIEngine context)
        {
            try
            {
                context.COO_CAM.Add(cooCam);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_CAM data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_CAM.Find(data.COCA_ROW_ID);
                #region Set Value
                _search.COCA_FILE_PATH = data.COCA_FILE_PATH;
                _search.COCA_JSON = data.COCA_JSON;
                _search.COCA_UPDATED_BY = data.COCA_UPDATED_BY;
                _search.COCA_UPDATED = data.COCA_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public COO_CAM GetLatestByID(string CODA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_CAM.Where(x => x.COCA_FK_COO_DATA.ToUpper() == CODA_ROW_ID.ToUpper()).OrderByDescending(x => x.COCA_CREATED).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //for migration data
        public void UpdateFilePath(COO_CAM data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.COO_CAM.Find(data.COCA_ROW_ID);
                #region Set Value
                _search.COCA_FILE_PATH = data.COCA_FILE_PATH;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
