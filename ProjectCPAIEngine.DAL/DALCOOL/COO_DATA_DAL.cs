﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_DATA_DAL
    {
        public void Save(COO_DATA cooData, EntityCPAIEngine context)
        {
            try
            {
                context.COO_DATA.Add(cooData);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_DATA WHERE CODA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_DATA.Find(data.CODA_ROW_ID);
                #region Set Value
                if (data.CODA_DRAFT_APPROVE_DATE != null)
                    _search.CODA_DRAFT_APPROVE_DATE = data.CODA_DRAFT_APPROVE_DATE;
                if (data.CODA_DRAFT_SUBMIT_DATE != null)
                    _search.CODA_DRAFT_SUBMIT_DATE = data.CODA_DRAFT_SUBMIT_DATE;
                if (data.CODA_DRAFT_REJECT_DATE != null)
                    _search.CODA_DRAFT_REJECT_DATE = data.CODA_DRAFT_REJECT_DATE;
                if (data.CODA_DRAFT_APPROVED_BY != null)
                    _search.CODA_DRAFT_APPROVED_BY = data.CODA_DRAFT_APPROVED_BY;
                if (data.CODA_FINAL_APPROVE_DATE != null)
                    _search.CODA_FINAL_APPROVE_DATE = data.CODA_FINAL_APPROVE_DATE;
                if (data.CODA_FINAL_SUBMIT_DATE != null)
                    _search.CODA_FINAL_SUBMIT_DATE = data.CODA_FINAL_SUBMIT_DATE;
                if (data.CODA_FINAL_REJECT_DATE != null)
                    _search.CODA_FINAL_REJECT_DATE = data.CODA_FINAL_REJECT_DATE;
                if (data.CODA_FINAL_APPROVED_BY != null)
                    _search.CODA_FINAL_APPROVED_BY = data.CODA_FINAL_APPROVED_BY;
                _search.CODA_COMMENT_DRAFT_CAM = data.CODA_COMMENT_DRAFT_CAM;
                _search.CODA_COMMENT_FINAL_CAM = data.CODA_COMMENT_FINAL_CAM;
                _search.CODA_DRAFT_CAM_PATH = data.CODA_DRAFT_CAM_PATH;
                _search.CODA_FINAL_CAM_PATH = data.CODA_FINAL_CAM_PATH;
                _search.CODA_NOTE = data.CODA_NOTE;
                _search.CODA_STATUS = data.CODA_STATUS;
                _search.CODA_UPDATED_BY = data.CODA_UPDATED_BY;
                _search.CODA_UPDATED = data.CODA_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(COO_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_DATA.Find(data.CODA_ROW_ID);
                #region Set Value
                _search.CODA_STATUS = data.CODA_STATUS;
                _search.CODA_UPDATED_BY = data.CODA_UPDATED_BY;
                _search.CODA_UPDATED = data.CODA_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePath(COO_DATA data)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var _search = context.COO_DATA.Find(data.CODA_ROW_ID);
                    #region Set Value                
                    if (data.CODA_DRAFT_CAM_PATH != null)
                        _search.CODA_DRAFT_CAM_PATH = data.CODA_DRAFT_CAM_PATH;
                    if (data.CODA_FINAL_CAM_PATH != null)
                        _search.CODA_FINAL_CAM_PATH = data.CODA_FINAL_CAM_PATH;
                    _search.CODA_UPDATED_BY = data.CODA_UPDATED_BY;
                    _search.CODA_UPDATED = data.CODA_UPDATED;
                    #endregion
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }

        public COO_DATA GetByID(string CODA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_DATA.Where(x => x.CODA_ROW_ID.ToUpper() == CODA_ROW_ID.ToUpper() && x.CODA_STATUS != "CANCEL").FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
 
        public static COO_DATA GetByAssayReferenceNo(string CODA_ASSAY_REF_NO)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_DATA.Where(x => x.CODA_ASSAY_REF_NO.ToUpper() == CODA_ASSAY_REF_NO.ToUpper() && x.CODA_STATUS == "APPROVED").OrderByDescending(y => y.CODA_CREATED).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public COO_DATA GetByCrudeReference(string CODA_CRUDE_REFERENCE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_DATA.Where(x => x.CODA_ASSAY_REF_NO.ToUpper() == CODA_CRUDE_REFERENCE.ToUpper() && x.CODA_STATUS == "APPROVED").FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_DATA> GetByDateRange(DateTime from, DateTime to)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_DATA.Where(x => x.CODA_CREATED >= from && x.CODA_CREATED <= to && x.CODA_STATUS == "APPROVED").ToList();
                    if (_Data != null && _Data.Count > 0)
                    {
                        _Data = _Data.OrderBy(x => x.CODA_CREATED).ToList();
                    }
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_DATA> GetByStatus(string CODA_STATUS = "APPROVED", string CODA_UPDATED_BY = "")
        {
            List<COO_DATA> listData = new List<COO_DATA>();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_DATA.Where(x => x.CODA_STATUS == CODA_STATUS && x.CODA_UPDATED_BY != CODA_UPDATED_BY);
                    if (_Data != null)
                    {
                        listData = _Data.ToList();
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listData;
        }

        public List<COO_DATA> GetBySpec(string MSP_ROW_ID, string CODA_STATUS = "APPROVED")
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = (from co in context.COO_DATA
                                 join cc in context.COO_CAM on co.CODA_ROW_ID equals cc.COCA_FK_MT_SPEC
                                 join ms in context.MT_SPEC on cc.COCA_FK_MT_SPEC equals ms.MSP_ROW_ID
                                 where ms.MSP_ROW_ID == MSP_ROW_ID 
                                 && co.CODA_STATUS == CODA_STATUS
                                 select co).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ChkDucomentInProgress()
        {
            using(var context = new EntityCPAIEngine())
            {

                var query = context.COO_DATA.Where(c => c.CODA_STATUS != "APPROVED" || c.CODA_STATUS != "CANCEL");

                if (query ==null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string getLatestFinalCAM(string crude, string country)
        {
            using (var context = new EntityCPAIEngine())
            {

                COO_DATA query = context.COO_DATA.Where(x => x.CODA_CRUDE_NAME == crude && x.CODA_COUNTRY == country && x.CODA_STATUS == "APPROVED").OrderByDescending(x => x.CODA_CREATED).FirstOrDefault();
                if (query != null)
                {
                    return query.CODA_FINAL_CAM_PATH;
                }
                return "";
            }
        }

        //for migration data
        public string getKeyByAssayRef(string assay_ref)
        {
            using (var context = new EntityCPAIEngine())
            {

                COO_DATA query = context.COO_DATA.Where(x => x.CODA_ASSAY_REF_NO == assay_ref).OrderByDescending(x => x.CODA_CREATED).FirstOrDefault();
                if (query != null)
                {
                    return query.CODA_ROW_ID;
                }
                return "";
            }
        }

    }
}
