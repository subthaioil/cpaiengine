﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_DOC_MANAGE_DAL
    {
        public void Save(COO_DOC_MANAGE cooDoc, EntityCPAIEngine context)
        {
            try
            {
                context.COO_DOC_MANAGE.Add(cooDoc);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string crude_name, string country, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_DOC_MANAGE WHERE CODM_CRUDE_NAME = '" + crude_name + "' AND CODM_COUNTRY = '" + country + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_DOC_MANAGE data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_DOC_MANAGE.Find(data.CODM_ROW_ID);
                #region Set Value
                _search.CODM_CRUDE_NAME = data.CODM_CRUDE_NAME;
                //_search.CODM_FILE_NAME = data.CODM_FILE_NAME;
                _search.CODM_FILE_PATH = data.CODM_FILE_PATH;
                _search.CODM_UPDATED_BY = data.CODM_UPDATED_BY;
                _search.CODM_UPDATED = data.CODM_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
