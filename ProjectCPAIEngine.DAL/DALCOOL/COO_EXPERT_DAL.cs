﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_EXPERT_DAL
    {
        public void Save(COO_EXPERT cooExpert, EntityCPAIEngine context)
        {
            try
            {
                context.COO_EXPERT.Add(cooExpert);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_EXPERT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.COO_EXPERT.Find(data.COEX_ROW_ID);
                #region Set Value

                _search.COEX_ACTIVATION_STATUS = data.COEX_ACTIVATION_STATUS;
                _search.COEX_STATUS = data.COEX_STATUS;
                _search.COEX_UPDATED_BY = data.COEX_UPDATED_BY;
                _search.COEX_UPDATED = data.COEX_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateExpert(COO_EXPERT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.COO_EXPERT.Find(data.COEX_ROW_ID);
                #region Set Value
                _search.COEX_AGREED_SCORE = data.COEX_AGREED_SCORE;
                _search.COEX_AVERAGE_SCORE = data.COEX_AVERAGE_SCORE;
                _search.COEX_RECOMMENDED_SCORE = data.COEX_RECOMMENDED_SCORE;
                _search.COEX_SCORE_FLAG = data.COEX_SCORE_FLAG;
                _search.COEX_SCORE_REASON = data.COEX_SCORE_REASON;
                _search.COEX_PROCESSING_FLAG = data.COEX_PROCESSING_FLAG;
                _search.COEX_PROCESSING_NOTE = data.COEX_PROCESSING_NOTE;
                _search.COEX_COMMENT_EXPERT = data.COEX_COMMENT_EXPERT;
                _search.COEX_EXPERT_SELECTED = data.COEX_EXPERT_SELECTED;
                _search.COEX_ATTACHED_FILE = data.COEX_ATTACHED_FILE;
                _search.COEX_STATUS = data.COEX_STATUS;
                _search.COEX_SUBMIT_DATE = data.COEX_SUBMIT_DATE;
                _search.COEX_SUBMITTED_BY = data.COEX_SUBMITTED_BY;
                _search.COEX_UPDATED_BY = data.COEX_UPDATED_BY;
                _search.COEX_UPDATED = data.COEX_UPDATED;
                #endregion
                context.SaveChanges();
            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateExpertSH(COO_EXPERT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.COO_EXPERT.Find(data.COEX_ROW_ID);
                #region Set Value
                _search.COEX_AGREED_SCORE = data.COEX_AGREED_SCORE;
                _search.COEX_AVERAGE_SCORE = data.COEX_AVERAGE_SCORE;
                _search.COEX_RECOMMENDED_SCORE = data.COEX_RECOMMENDED_SCORE;
                _search.COEX_SCORE_FLAG = data.COEX_SCORE_FLAG;
                _search.COEX_SCORE_REASON = data.COEX_SCORE_REASON;
                _search.COEX_PROCESSING_FLAG = data.COEX_PROCESSING_FLAG;
                _search.COEX_PROCESSING_NOTE = data.COEX_PROCESSING_NOTE;
                _search.COEX_COMMENT_EXPERT = data.COEX_COMMENT_EXPERT;
                _search.COEX_EXPERT_SELECTED = data.COEX_EXPERT_SELECTED;
                _search.COEX_EXPERT_SH_SELECTED = data.COEX_EXPERT_SH_SELECTED;
                _search.COEX_ATTACHED_FILE = data.COEX_ATTACHED_FILE;
                _search.COEX_APPROVE_DATE = data.COEX_APPROVE_DATE;
                _search.COEX_APPROVED_BY = data.COEX_APPROVED_BY;
                _search.COEX_STATUS = data.COEX_STATUS;
                _search.COEX_UPDATED_BY = data.COEX_UPDATED_BY;
                _search.COEX_UPDATED = data.COEX_UPDATED;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public COO_EXPERT GetByID(string COEX_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_EXPERT.Where(x => x.COEX_ROW_ID.ToUpper() == COEX_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public COO_EXPERT GetByAreaAndUnit(string COEX_FK_COO_DATA, string area, string unit)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_EXPERT.Where(x => x.COEX_FK_COO_DATA.ToUpper() == COEX_FK_COO_DATA.ToUpper() && x.COEX_AREA.ToUpper() == area.ToUpper() && x.COEX_UNIT.ToUpper() == unit.ToUpper() && x.COEX_ACTIVATION_STATUS == "ACTIVE").FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_EXPERT> GetByArea(string COEX_FK_COO_DATA, string area)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    List<COO_EXPERT> _Data = context.COO_EXPERT.Where(x => x.COEX_FK_COO_DATA.ToUpper() == COEX_FK_COO_DATA.ToUpper() && x.COEX_AREA.ToUpper() == area.ToUpper() && x.COEX_ACTIVATION_STATUS == "ACTIVE").ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_EXPERT> GetAllByID(string CODA_ROW_ID, bool isActive = false)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    if (!isActive)
                    {
                        var _Data = context.COO_EXPERT.Where(x => x.COEX_FK_COO_DATA.ToUpper() == CODA_ROW_ID.ToUpper()).ToList();
                        return _Data;
                    }
                    else
                    {
                        var _Data = context.COO_EXPERT.Where(x => x.COEX_FK_COO_DATA.ToUpper() == CODA_ROW_ID.ToUpper() && x.COEX_ACTIVATION_STATUS == "ACTIVE").ToList();
                        return _Data;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
