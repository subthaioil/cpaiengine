﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_SPIRAL_DAL
    {
        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_SPIRAL WHERE COSP_FK_COO_CAM = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(COO_SPIRAL cooSpiral, EntityCPAIEngine context)
        {
            try
            {
                context.COO_SPIRAL.Add(cooSpiral);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_SPIRAL data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_SPIRAL.Where(x => x.COSP_HEADER == data.COSP_HEADER && x.COSP_KEY == data.COSP_KEY).FirstOrDefault();
                #region Set Value
                if (_search != null)
                {
                    _search.COSP_VALUE = data.COSP_VALUE;
                    _search.COSP_UPDATED_BY = data.COSP_UPDATED_BY;
                    _search.COSP_UPDATED = data.COSP_UPDATED;
                    context.SaveChanges();
                }
                #endregion

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_SPIRAL> GetAllSpiralsByCAMID(string COCA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_SPIRAL.Where(x => x.COSP_FK_COO_CAM.ToUpper() == COCA_ROW_ID.ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
