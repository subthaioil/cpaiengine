﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_SUP_DOC_DAL
    {
        public void Save(COO_SUP_DOC cooSupDoc, EntityCPAIEngine context)
        {
            try
            {
                context.COO_SUP_DOC.Add(cooSupDoc);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_SUP_DOC data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_SUP_DOC.Find(data.COSD_ROW_ID);
                #region Set Value
                _search.COSD_FILE_PATH = data.COSD_FILE_PATH;
                _search.COSD_FILE_TYPE = data.COSD_FILE_TYPE;
                _search.COSD_UPDATED_BY = data.COSD_UPDATED_BY;
                _search.COSD_UPDATED = data.COSD_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_SUP_DOC> GetAllByID(string CODA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_SUP_DOC.Where(x => x.COSD_FK_COO_DATA.ToUpper() == CODA_ROW_ID.ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_SUP_DOC WHERE COSD_FK_COO_DATA = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
