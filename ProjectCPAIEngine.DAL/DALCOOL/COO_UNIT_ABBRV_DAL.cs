﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_UNIT_ABBRV_DAL
    {
        public void Save(COO_UNIT_ABBV cooUnit, EntityCPAIEngine context)
        {
            try
            {
                context.COO_UNIT_ABBV.Add(cooUnit);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_UNIT_ABBV WHERE CUA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAll(EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_UNIT_ABBV");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_UNIT_ABBV data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_UNIT_ABBV.Find(data.CUA_ROW_ID);
                #region Set Value
                _search.CUA_UNIT = data.CUA_UNIT;
                _search.CUA_CRUDE_NAME = data.CUA_CRUDE_NAME;
                _search.CUA_CRUDE_ABBRV = data.CUA_CRUDE_ABBRV;
                _search.CUA_UPDATED = data.CUA_UPDATED;
                _search.CUA_UPDATED_BY = data.CUA_UPDATED_BY;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
