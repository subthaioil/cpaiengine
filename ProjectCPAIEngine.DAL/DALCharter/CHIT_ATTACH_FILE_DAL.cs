﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHIT_ATTACH_FILE_DAL
    {
        public void Save(CHIT_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_ATTACH_FILE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHIT_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_ATTACH_FILE> GetFileByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiFile = context.CHIT_ATTACH_FILE.Where(x => x.ITAF_FK_CHIT_DATA.ToUpper() == CHIID.ToUpper()).ToList();
                    return _chiFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_ATTACH_FILE> GetFileByDataID(string CHIID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiFile = context.CHIT_ATTACH_FILE.Where(x => x.ITAF_FK_CHIT_DATA.ToUpper() == CHIID.ToUpper()).ToList();
                    if(string.IsNullOrEmpty(TYPE) != true)
                    {
                        _chiFile = _chiFile.Where(x => x.ITAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _chiFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
