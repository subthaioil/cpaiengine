﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHIT_CARGO_DAL
    {
        public void Save(CHIT_CARGO data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_CARGO.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHIT_CARGO data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_CARGO.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_CARGO> GetCargoByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiPort = context.CHIT_CARGO.Where(x => x.CHTC_FK_CHIT_DATA.ToUpper() == CHIID.ToUpper()).OrderBy(x => x.CHTC_ORDER).ToList();
                    return _chiPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
