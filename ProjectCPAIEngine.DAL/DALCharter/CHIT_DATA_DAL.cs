﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHIT_DATA_DAL
    {
        public void Save(CHIT_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHIT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CHIT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHIT_DATA.Find(data.IDAT_ROW_ID);
                #region Set Value
                _search.IDAT_REASON = data.IDAT_REASON;
                //if (data.IDAT_EXPLANATION != null)
                //{
                //    _search.IDAT_EXPLANATION = data.IDAT_EXPLANATION;
                //}
                //if (data.IDAT_REMARK != null)
                //{
                //    _search.IDAT_REMARK = data.IDAT_REMARK;
                //}
                _search.IDAT_STATUS = data.IDAT_STATUS;

                //if (!string.IsNullOrEmpty(data.IDAT_REMARK))
                //{
                //    _search.IDAT_REMARK = data.IDAT_REMARK;
                //}
                //if (!string.IsNullOrEmpty(data.IDAT_EXPLANATION))
                //{
                //    _search.IDAT_EXPLANATION = data.IDAT_EXPLANATION;
                //}
                if (_search.IDAT_STATUS == "WAITING_CONFIRM")
                {
                    _search.IDAT_SELECT_CHARTER_FOR = data.IDAT_SELECT_CHARTER_FOR;
                }
                _search.IDAT_UPDATED_BY = data.IDAT_UPDATED_BY;
                _search.IDAT_UPDATED = data.IDAT_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSelect(CHIT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHIT_DATA.Find(data.IDAT_ROW_ID);
                #region Set Value  
                if (_search != null)
                {
                    _search.IDAT_SELECT_CHARTER_FOR = data.IDAT_SELECT_CHARTER_FOR;
                    _search.IDAT_UPDATED_BY = data.IDAT_UPDATED_BY;
                    _search.IDAT_UPDATED = data.IDAT_UPDATED;
                    #endregion
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNote(CHIT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHIT_DATA.Find(data.IDAT_ROW_ID);
                #region Set Value  
                if (_search != null)
                {
                    _search.IDAT_CHARTER_FOR_CMMENT = data.IDAT_CHARTER_FOR_CMMENT;
                    _search.IDAT_UPDATED_BY = data.IDAT_UPDATED_BY;
                    _search.IDAT_UPDATED = data.IDAT_UPDATED;
                    #endregion
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNoteCMMT(CHIT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHIT_DATA.Find(data.IDAT_ROW_ID);
                #region Set Value  
                if (_search != null)
                {
                    _search.IDAT_EXPLANATION = data.IDAT_EXPLANATION;
                    _search.IDAT_UPDATED_BY = data.IDAT_UPDATED_BY;
                    _search.IDAT_UPDATED = data.IDAT_UPDATED;
                    _search.IDAT_REMARK = data.IDAT_REMARK;
                    #endregion
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CHIT_DATA WHERE IDAT_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {

            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CHIT_DATA WHERE IDAT_ROW_ID = '" + TransID + "'");
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<CHIT_DATA> GetCHIDataStatusApprove(bool EditMode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    List<CHIT_DATA> lstReturn = new List<CHIT_DATA>();
                    var lstSubmitTCE = context.CPAI_TCE_WS.Where(x => x.CTW_STATUS == "SUBMIT").ToList();
                    var lstCHI = context.CHIT_DATA.Where(x => x.IDAT_STATUS.ToUpper() == "APPROVED" && x.IDAT_D_FREIGHT == "WS").ToList();//&& x.IDAT_VESSEL_SIZE == "VLCC"
                    if (lstCHI != null)
                    {
                        for (int ii = 0; ii < lstCHI.Count; ii++)
                        {
                            if (EditMode)
                            {
                                if (lstSubmitTCE.Where(x => x.CTW_FK_CHI_DATA == lstCHI[ii].IDAT_ROW_ID).ToList().Count > 0) continue;
                            }
                            string PurchaseNO = lstCHI[ii].IDAT_PURCHASE_NO;
                            var lstOffer = (from chi in context.CHIT_DATA
                                            join chi_off in context.CHIT_OFFER_ITEMS on chi.IDAT_ROW_ID equals chi_off.ITOI_FK_CHIT_DATA
                                            where chi.IDAT_PURCHASE_NO == PurchaseNO// && chi_off.IOI_FINAL_FLAG == "Y"
                                            select chi_off).ToList();

                            if (lstOffer != null)
                            {
                                for (int i = 0; i < lstOffer.Count; i++)
                                {
                                    string IOI_ROW_ID = lstOffer[i].ITOI_ROW_ID;
                                    var lstRItem = (from chi_rounI in context.CHIT_ROUND_ITEMS where chi_rounI.ITRI_FK_OFFER_ITEMS == IOI_ROW_ID select chi_rounI).ToList();

                                    if (lstRItem != null)
                                    {
                                        for (int ir = 0; ir < lstRItem.Count; ir++)
                                        {
                                            string IRI_ROW_ID = lstRItem[ir].ITRI_ROW_ID;
                                            var lstRound = (from chi_roun in context.CHIT_ROUND where chi_roun.ITIR_FK_ROUND_ITEMS == IRI_ROW_ID select chi_roun).ToList();
                                            for (int r = 0; r < lstRound.Count; r++)
                                            {
                                                lstRItem[ir].CHIT_ROUND.Add(lstRound[r]);
                                            }
                                            lstOffer[i].CHIT_ROUND_ITEMS.Add(lstRItem[ir]);
                                        }
                                    }
                                    lstCHI[ii].CHIT_OFFER_ITEMS.Add(lstOffer[i]);
                                }

                            }
                            lstReturn.Add(lstCHI[ii]);

                        }
                    }
                    return lstReturn;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHIT_DATA GetCHIDataByPn(string PurchaseNO)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var dataReturn = context.CHIT_DATA.Where(x => x.IDAT_PURCHASE_NO.ToUpper() == PurchaseNO.ToUpper()).FirstOrDefault();
                    var lstOffer = (from chi in context.CHIT_DATA
                                    join chi_off in context.CHIT_OFFER_ITEMS on chi.IDAT_ROW_ID equals chi_off.ITOI_FK_CHIT_DATA
                                    where chi.IDAT_PURCHASE_NO == PurchaseNO && chi_off.ITOI_FINAL_FLAG == "Y"
                                    select chi_off).ToList();

                    if (lstOffer != null)
                    {
                        for (int i = 0; i < lstOffer.Count; i++)
                        {
                            string IOI_ROW_ID = lstOffer[i].ITOI_ROW_ID;
                            var lstRItem = (from chi_rounI in context.CHIT_ROUND_ITEMS where chi_rounI.ITRI_FK_OFFER_ITEMS == IOI_ROW_ID select chi_rounI).ToList();

                            if (lstRItem != null)
                            {
                                for (int ir = 0; ir < lstRItem.Count; ir++)
                                {
                                    string IRI_ROW_ID = lstRItem[ir].ITRI_ROW_ID;
                                    var lstRound = (from chi_roun in context.CHIT_ROUND where chi_roun.ITIR_FK_ROUND_ITEMS == IRI_ROW_ID select chi_roun).ToList();
                                    for (int r = 0; r < lstRound.Count; r++)
                                    {
                                        lstRItem[ir].CHIT_ROUND.Add(lstRound[r]);
                                    }
                                    lstOffer[i].CHIT_ROUND_ITEMS.Add(lstRItem[ir]);
                                }
                            }
                            dataReturn.CHIT_OFFER_ITEMS.Add(lstOffer[i]);
                        }

                    }

                    return dataReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_DATA> GetCHIDataStatusApprove(object sTATUS_APPROVED)
        {
            throw new NotImplementedException();
        }

        public string GetCHINOByID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiData = context.CHIT_DATA.Where(x => x.IDAT_ROW_ID.ToUpper() == CHIID.ToUpper()).ToList();
                    if (_chiData.Count > 0) return _chiData[0].IDAT_PURCHASE_NO;
                    else return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHIT_DATA GetCHIDATAByID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiData = context.CHIT_DATA.Where(x => x.IDAT_ROW_ID.ToUpper() == CHIID.ToUpper()).FirstOrDefault();
                    return _chiData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHIT_DATA GetCHIDATAByPurchaseNo(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiData = context.CHIT_DATA.Where(x => x.IDAT_PURCHASE_NO.ToUpper() == CHIID.ToUpper()).FirstOrDefault();
                    return _chiData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
