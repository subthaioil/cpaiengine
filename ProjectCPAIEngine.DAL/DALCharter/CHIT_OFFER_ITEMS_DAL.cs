﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
   public class CHIT_OFFER_ITEMS_DAL
    {
        public void Save(CHIT_OFFER_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_OFFER_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHIT_OFFER_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_OFFER_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_OFFER_ITEMS> GetOfferItemByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiOffer = context.CHIT_OFFER_ITEMS.Where(x => x.ITOI_FK_CHIT_DATA.ToUpper() == CHIID.ToUpper()).OrderBy(o => o.ITOI_ORDER).ToList();
                    return _chiOffer;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
