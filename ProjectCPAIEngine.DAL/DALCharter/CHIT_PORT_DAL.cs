﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHIT_PORT_DAL
    {
        public void Save(CHIT_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHIT_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_PORT> GetPortByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiPort = context.CHIT_PORT.Where(x => x.CHTP_FK_CHIT_DATA.ToUpper() == CHIID.ToUpper()).OrderBy(x => x.CHTP_ORDER_PORT).ToList();
                    return _chiPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
