﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHI_DATA_DAL
    {
        public void Save(CHI_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHI_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNote(CHI_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHI_DATA.Find(data.IDA_ROW_ID);
                if (_search != null)
                {
                    if (_search.IDA_STATUS == "WAITING CERTIFIED" || _search.IDA_STATUS == "WAITING APPROVE")
                    {
                        _search.IDA_REMARK = data.IDA_REMARK;
                        _search.IDA_EXPLANATION = data.IDA_EXPLANATION;
                    }
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHI_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.CHI_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CHI_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.CHI_DATA.Find(data.IDA_ROW_ID);
                #region Set Value
                _search.IDA_REASON = data.IDA_REASON;
                //if (data.IDA_EXPLANATION != null)
                //{
                    //_search.IDA_EXPLANATION = data.IDA_EXPLANATION;
                //}
                //if (data.IDA_REASON != null)
                //{
                    //_search.IDA_REASON = data.IDA_REASON;
                //}
                _search.IDA_STATUS = data.IDA_STATUS;
                _search.IDA_UPDATED_BY = data.IDA_UPDATED_BY;
                _search.IDA_UPDATED = data.IDA_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CHI_DATA WHERE IDA_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {

            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CHI_DATA WHERE IDA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //public List<CHI_DATA> GetCHIDataStatusApprove(bool EditMode)
        //{
        //    try
        //    {
        //        using (var context = new EntityCPAIEngine())
        //        {
        //            List<CHI_DATA> lstReturn = new List<CHI_DATA>();
        //            var lstSubmitTCE = context.CPAI_TCE_WS.Where(x => x.CTW_STATUS == "SUBMIT").ToList();
        //            var lstCHI = context.CHI_DATA.Where(x => x.IDA_STATUS.ToUpper() == "APPROVED").ToList();
        //            if (lstCHI != null)
        //            {
        //                for (int ii = 0; ii < lstCHI.Count; ii++)
        //                {
        //                    if (EditMode)
        //                    {
        //                        if (lstSubmitTCE.Where(x => x.CTW_FK_CHI_DATA == lstCHI[ii].IDA_ROW_ID).ToList().Count > 0) continue;
        //                    }
        //                    string PurchaseNO = lstCHI[ii].IDA_PURCHASE_NO;
        //                    var lstOffer = (from chi in context.CHI_DATA
        //                                    join chi_off in context.CHI_OFFER_ITEMS on chi.IDA_ROW_ID equals chi_off.IOI_FK_CHI_DATA
        //                                    where chi.IDA_PURCHASE_NO == PurchaseNO// && chi_off.IOI_FINAL_FLAG == "Y"
        //                                    select chi_off).ToList();

        //                    if (lstOffer != null)
        //                    {
        //                        for (int i = 0; i < lstOffer.Count; i++)
        //                        {
        //                            string IOI_ROW_ID = lstOffer[i].IOI_ROW_ID;
        //                            var lstRItem = (from chi_rounI in context.CHI_ROUND_ITEMS where chi_rounI.IRI_FK_OFFER_ITEMS == IOI_ROW_ID select chi_rounI).ToList();

        //                            if (lstRItem != null)
        //                            {
        //                                for (int ir = 0; ir < lstRItem.Count; ir++)
        //                                {
        //                                    string IRI_ROW_ID = lstRItem[ir].IRI_ROW_ID;
        //                                    var lstRound = (from chi_roun in context.CHI_ROUND where chi_roun.IIR_FK_ROUND_ITEMS == IRI_ROW_ID select chi_roun).ToList();
        //                                    for (int r = 0; r < lstRound.Count; r++)
        //                                    {
        //                                        lstRItem[ir].CHI_ROUND.Add(lstRound[r]);
        //                                    }
        //                                    lstOffer[i].CHI_ROUND_ITEMS.Add(lstRItem[ir]);
        //                                }
        //                            }
        //                            lstCHI[ii].CHI_OFFER_ITEMS.Add(lstOffer[i]);
        //                        }

        //                    }
        //                    lstReturn.Add(lstCHI[ii]);

        //                }
        //            }
        //            return lstReturn;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public List<CHI_DATA> GetCHIDataStatusApprove(bool EditMode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    List<CHI_DATA> lstReturn = new List<CHI_DATA>();
                    var lstSubmitTCE = context.CPAI_TCE_WS.Where(x => x.CTW_STATUS == "SUBMIT").ToList();
                    var lstCHI = context.CHI_DATA.Where(x => x.IDA_STATUS.ToUpper() == "APPROVED" && x.IDA_FREIGHT == "WS" && x.IDA_VESSEL_SIZE == "VLCC").ToList();
                    if (lstCHI != null)
                    {
                        for (int ii = 0; ii < lstCHI.Count; ii++)
                        {
                            if (EditMode)
                            {
                                if (lstSubmitTCE.Where(x => x.CTW_FK_CHI_DATA == lstCHI[ii].IDA_ROW_ID).ToList().Count > 0) continue;
                            }
                            string PurchaseNO = lstCHI[ii].IDA_PURCHASE_NO;
                            var lstOffer = (from chi in context.CHI_DATA
                                            join chi_off in context.CHI_OFFER_ITEMS on chi.IDA_ROW_ID equals chi_off.IOI_FK_CHI_DATA
                                            where chi.IDA_PURCHASE_NO == PurchaseNO// && chi_off.IOI_FINAL_FLAG == "Y"
                                            select chi_off).ToList();

                            if (lstOffer != null)
                            {
                                for (int i = 0; i < lstOffer.Count; i++)
                                {
                                    string IOI_ROW_ID = lstOffer[i].IOI_ROW_ID;
                                    var lstRItem = (from chi_rounI in context.CHI_ROUND_ITEMS where chi_rounI.IRI_FK_OFFER_ITEMS == IOI_ROW_ID select chi_rounI).ToList();

                                    if (lstRItem != null)
                                    {
                                        for (int ir = 0; ir < lstRItem.Count; ir++)
                                        {
                                            string IRI_ROW_ID = lstRItem[ir].IRI_ROW_ID;
                                            var lstRound = (from chi_roun in context.CHI_ROUND where chi_roun.IIR_FK_ROUND_ITEMS == IRI_ROW_ID select chi_roun).ToList();
                                            for (int r = 0; r < lstRound.Count; r++)
                                            {
                                                lstRItem[ir].CHI_ROUND.Add(lstRound[r]);
                                            }
                                            lstOffer[i].CHI_ROUND_ITEMS.Add(lstRItem[ir]);
                                        }
                                    }
                                    lstCHI[ii].CHI_OFFER_ITEMS.Add(lstOffer[i]);
                                }

                            }
                            lstReturn.Add(lstCHI[ii]);

                        }
                    }
                    return lstReturn;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHI_DATA GetCHIDataByPn(string PurchaseNO)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var dataReturn = context.CHI_DATA.Where(x => x.IDA_PURCHASE_NO.ToUpper() == PurchaseNO.ToUpper()).FirstOrDefault();
                    var lstOffer = (from chi in context.CHI_DATA
                                    join chi_off in context.CHI_OFFER_ITEMS on chi.IDA_ROW_ID equals chi_off.IOI_FK_CHI_DATA
                                    where chi.IDA_PURCHASE_NO == PurchaseNO && chi_off.IOI_FINAL_FLAG == "Y"
                                    select chi_off).ToList();

                    if (lstOffer != null)
                    {
                        for (int i = 0; i < lstOffer.Count; i++)
                        {
                            string IOI_ROW_ID = lstOffer[i].IOI_ROW_ID;
                            var lstRItem = (from chi_rounI in context.CHI_ROUND_ITEMS where chi_rounI.IRI_FK_OFFER_ITEMS == IOI_ROW_ID select chi_rounI).ToList();

                            if (lstRItem != null)
                            {
                                for (int ir = 0; ir < lstRItem.Count; ir++)
                                {
                                    string IRI_ROW_ID = lstRItem[ir].IRI_ROW_ID;
                                    var lstRound = (from chi_roun in context.CHI_ROUND where chi_roun.IIR_FK_ROUND_ITEMS == IRI_ROW_ID select chi_roun).ToList();
                                    for (int r = 0; r < lstRound.Count; r++)
                                    {
                                        lstRItem[ir].CHI_ROUND.Add(lstRound[r]);
                                    }
                                    lstOffer[i].CHI_ROUND_ITEMS.Add(lstRItem[ir]);
                                }
                            }
                            dataReturn.CHI_OFFER_ITEMS.Add(lstOffer[i]);
                        }

                    }

                    return dataReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHI_DATA> GetCHIDataStatusApprove(object sTATUS_APPROVED)
        {
            throw new NotImplementedException();
        }

        public string GetCHINOByID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiData = context.CHI_DATA.Where(x => x.IDA_ROW_ID.ToUpper() == CHIID.ToUpper()).ToList();
                    if (_chiData.Count > 0) return _chiData[0].IDA_PURCHASE_NO;
                    else return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHI_DATA GetCHIDATAByID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiData = context.CHI_DATA.Where(x => x.IDA_ROW_ID.ToUpper() == CHIID.ToUpper()).FirstOrDefault();
                    return _chiData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHI_DATA GetCHIDATAByPurchaseNo(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiData = context.CHI_DATA.Where(x => x.IDA_PURCHASE_NO.ToUpper() == CHIID.ToUpper()).FirstOrDefault();
                    return _chiData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
