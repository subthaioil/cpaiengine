﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHI_DISCHARGE_PORT_DAL
    {
        public void Save(CHI_DISCHARGE_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHI_DISCHARGE_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHI_DISCHARGE_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CHI_DISCHARGE_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHI_DISCHARGE_PORT> GetPortByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiPort = context.CHI_DISCHARGE_PORT.Where(x => x.IDP_FK_CHI_DATA.ToUpper() == CHIID.ToUpper()).OrderBy(x => x.IDP_ORDER_PORT).ToList();
                    return _chiPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
