﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHI_LOAD_PORT_DAL
    {
        public void Save(CHI_LOAD_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHI_LOAD_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHI_LOAD_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CHI_LOAD_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHI_LOAD_PORT> GetPortByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiPort = context.CHI_LOAD_PORT.Where(x => x.IPT_FK_CHI_DATA.ToUpper() == CHIID.ToUpper()).OrderBy(x => x.IPT_ORDER_PORT).ToList();
                    return _chiPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
