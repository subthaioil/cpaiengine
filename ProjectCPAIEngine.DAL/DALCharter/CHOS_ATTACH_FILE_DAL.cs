﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_ATTACH_FILE_DAL
    {
        public void Save(CHOS_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_ATTACH_FILE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOS_ATTACH_FILE> GetFileByDataID(string CHOSID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CHOS_ATTACH_FILE.Where(x => x.OSAF_FK_CHOS_DATA.ToUpper() == CHOSID.ToUpper()).ToList();
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOS_ATTACH_FILE> GetFileByDataID(string CHOSID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CHOS_ATTACH_FILE.Where(x => x.OSAF_FK_CHOS_DATA.ToUpper() == CHOSID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _choFile = _choFile.Where(x => x.OSAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
