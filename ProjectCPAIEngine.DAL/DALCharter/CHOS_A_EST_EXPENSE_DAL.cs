﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_A_EST_EXPENSE_DAL
    {
        
        public void Save(CHOS_A_EST_EXPENSE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_A_EST_EXPENSE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_A_EST_EXPENSE data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_A_EST_EXPENSE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOS_A_EST_EXPENSE> GetByDataID(string CHOSID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.CHOS_A_EST_EXPENSE.Where(x => x.OSEE_FK_CHOS_CASE_A.ToUpper() == CHOSID.ToUpper()).ToList();
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
