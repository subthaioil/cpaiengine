﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_B_ONE_EST_INCOME_DAL
    {
       
        public void Save(CHOS_B_ONE_EST_INCOME data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_B_ONE_EST_INCOME.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_B_ONE_EST_INCOME data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_B_ONE_EST_INCOME.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOS_B_ONE_EST_INCOME> GetByDataID(string CHOS_CASE_B_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.CHOS_B_ONE_EST_INCOME.Where(x => x.OSEI_FK_CHOS_CASE_B.ToUpper() == CHOS_CASE_B_ID.ToUpper()).ToList();
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
