﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_B_TWO_COST_OF_MT_DAL
    {
        public void Save(CHOS_B_TWO_COST_OF_MT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_B_TWO_COST_OF_MT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_B_TWO_COST_OF_MT data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_B_TWO_COST_OF_MT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHOS_B_TWO_COST_OF_MT GetByDataID(string CHOS_CASE_B_TWO_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.CHOS_B_TWO_COST_OF_MT.Where(x => x.OSCM_FK_CHOS_CASE_B.ToUpper() == CHOS_CASE_B_TWO_ID.ToUpper()).FirstOrDefault();
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
