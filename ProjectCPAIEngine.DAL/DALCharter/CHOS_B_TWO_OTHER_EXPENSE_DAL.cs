﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_B_TWO_OTHER_EXPENSE_DAL
    {
        public void Save(CHOS_B_TWO_OTHER_EXPENSE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_B_TWO_OTHER_EXPENSE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_B_TWO_OTHER_EXPENSE data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_B_TWO_OTHER_EXPENSE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOS_B_TWO_OTHER_EXPENSE> GetByDataID(string CHOS_CASE_B_TWO_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.CHOS_B_TWO_OTHER_EXPENSE.Where(x => x.OSNE_FK_CHOS_B_TWO.ToUpper() == CHOS_CASE_B_TWO_ID.ToUpper()).ToList();
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
