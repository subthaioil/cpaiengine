﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_CASE_B_DAL
    {
        public void Save(CHOS_CASE_B data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_CASE_B.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_CASE_B data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_CASE_B.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHOS_CASE_B GetByDataID(string CHOSID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.CHOS_CASE_B.Where(x => x.OSBO_FK_CHOS_DATA.ToUpper() == CHOSID.ToUpper()).FirstOrDefault();
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
