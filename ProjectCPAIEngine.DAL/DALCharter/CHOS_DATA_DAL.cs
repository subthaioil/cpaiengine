﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOS_DATA_DAL
    {
        public void Save(CHOS_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOS_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOS_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOS_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CHOS_DATA data, EntityCPAIEngine context)
        {
            try
            {              
                var _search = context.CHOS_DATA.Find(data.OSDA_ROW_ID);
                #region Set Value
                _search.OSDA_REASON = data.OSDA_REASON;
                //if (!string.IsNullOrEmpty(data.OSDA_REMARK))
                //{
                //    _search.OSDA_REMARK = data.OSDA_REMARK;
                //}

                //if (!string.IsNullOrEmpty(data.OSDA_EXPLANATION))
                //{
                //    _search.OSDA_EXPLANATION = data.OSDA_EXPLANATION;
                //}

                //_search.OSDA_REMARK = (string.IsNullOrEmpty(data.OSDA_REMARK)) ? "" : data.OSDA_REMARK;
                //_search.OSDA_EXPLANATION = (string.IsNullOrEmpty(data.OSDA_EXPLANATION)) ? "" : data.OSDA_EXPLANATION;
                _search.OSDA_STATUS = data.OSDA_STATUS;
                _search.OSDA_UPDATED_BY = data.OSDA_UPDATED_BY;
                _search.OSDA_UPDATED = data.OSDA_UPDATED;
                #endregion
                context.SaveChanges();                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CHOS_DATA WHERE OSDA_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CHOS_DATA WHERE OSDA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHOS_DATA GetCHOSDATAByID(string CHOSID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chosData = context.CHOS_DATA.Where(x => x.OSDA_ROW_ID.ToUpper() == CHOSID.ToUpper()).FirstOrDefault();
                    return _chosData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNote(CHOS_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHOS_DATA.Find(data.OSDA_ROW_ID);
                if (_search != null)
                {
                    _search.OSDA_REMARK = data.OSDA_REMARK;
                    _search.OSDA_EXPLANATION = data.OSDA_EXPLANATION;
                    _search.OSDA_UPDATED = data.OSDA_UPDATED;
                    _search.OSDA_UPDATED_BY = data.OSDA_UPDATED_BY;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
