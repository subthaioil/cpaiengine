﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_ATTACH_FILE_DAL
    {
        public void Save(CHOT_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_ATTACH_FILE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_ATTACH_FILE> GetFileByDataID(string CHOTID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CHOT_ATTACH_FILE.Where(x => x.OTAF_FK_CHI_DATA.ToUpper() == CHOTID.ToUpper()).ToList();
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_ATTACH_FILE> GetFileByDataID(string CHOTID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CHOT_ATTACH_FILE.Where(x => x.OTAF_FK_CHI_DATA.ToUpper() == CHOTID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _choFile = _choFile.Where(x => x.OTAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
