﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_A_OTHER_COSTS_DAL
    {
        public void Save(CHOT_A_OTHER_COSTS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_A_OTHER_COSTS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_A_OTHER_COSTS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_A_OTHER_COSTS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_A_OTHER_COSTS> GetByDataID(string sID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.CHOT_A_OTHER_COSTS.Where(x => x.OMAO_FK_CHOT_DATA.ToUpper() == sID.ToUpper()).ToList();
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
