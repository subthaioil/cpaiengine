﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_DATA_DAL
    {
        public void Save(CHOT_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNote(CHOT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CHOT_DATA.Find(data.OTDA_ROW_ID);
                if (_search != null)
                {
                    _search.OTDA_NOTE = data.OTDA_NOTE;
                    _search.OTDA_EXPLANATION = data.OTDA_EXPLANATION;
                    _search.OTDA_UPDATED = data.OTDA_UPDATED;
                    _search.OTDA_UPDATED_BY = data.OTDA_UPDATED_BY;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CHOT_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.CHOT_DATA.Find(data.OTDA_ROW_ID);
                #region Set Value
                _search.OTDA_REASON = data.OTDA_REASON;
                if (!string.IsNullOrEmpty(data.OTDA_NOTE))
                {
                    _search.OTDA_NOTE = data.OTDA_NOTE;
                }
                if (!string.IsNullOrEmpty(data.OTDA_EXPLANATION))
                {
                    _search.OTDA_EXPLANATION = data.OTDA_EXPLANATION;
                }
                _search.OTDA_STATUS = data.OTDA_STATUS;
                _search.OTDA_UPDATED_BY = data.OTDA_UPDATED_BY;
                _search.OTDA_UPDATED = data.OTDA_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CHOT_DATA WHERE OTDA_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CHOT_DATA WHERE OTDA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CHOT_DATA GetCHOTDATAByID(string CHOTID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choData = context.CHOT_DATA.Where(x => x.OTDA_ROW_ID.ToUpper() == CHOTID.ToUpper()).FirstOrDefault();
                    return _choData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
