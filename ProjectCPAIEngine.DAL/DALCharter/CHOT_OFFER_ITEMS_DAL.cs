﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_OFFER_ITEMS_DAL
    {
        public void Save(CHOT_OFFER_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_OFFER_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_OFFER_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_OFFER_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_OFFER_ITEMS> GetOfferItemByDataID(string CHOTID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiOffer = context.CHOT_OFFER_ITEMS.Where(x => x.OTOI_FK_CHOT_DATA.ToUpper() == CHOTID.ToUpper()).ToList();
                    return _chiOffer;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
