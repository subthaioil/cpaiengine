﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_ROUND_DAL
    {
        public void Save(CHOT_ROUND data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_ROUND.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_ROUND data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_ROUND.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_ROUND> GetRoundByRoundItemID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choRound = context.CHOT_ROUND.Where(x => x.OTIR_FK_ROUND_ITEMS.ToUpper() == ITEMID.ToUpper()).ToList();
                    return _choRound;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
