﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_ROUND_ITEMS_DAL
    {
        public void Save(CHOT_ROUND_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_ROUND_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_ROUND_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_ROUND_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_ROUND_ITEMS> GetRoundByOfferItemID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choItems = context.CHOT_ROUND_ITEMS.Where(x => x.OTRI_FK_OFFER_ITEMS.ToUpper() == ITEMID.ToUpper()).ToList();
                    return _choItems;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
