using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.DAL.DALCrudePurchase
{
    public static class CrudePurchase_DAL
    {
        public static System.Collections.IEnumerable DynamicSqlQuery(this Database database, string sql, params object[] parameters)
        {
            TypeBuilder builder = createTypeBuilder(
                    "MyDynamicAssembly", "MyDynamicModule", "MyDynamicType");

            using (System.Data.IDbCommand command = database.Connection.CreateCommand())
            {
                try
                {
                    database.Connection.Open();
                    command.CommandText = sql;
                    command.CommandTimeout = command.Connection.ConnectionTimeout;
                    foreach (var param in parameters)
                    {
                        command.Parameters.Add(param);
                    }

                    using (System.Data.IDataReader reader = command.ExecuteReader())
                    {
                        var schema = reader.GetSchemaTable();

                        foreach (System.Data.DataRow row in schema.Rows)
                        {
                            string name = (string)row["ColumnName"];
                            //var a=row.ItemArray.Select(d=>d.)
                            Type type = (Type)row["DataType"];
                            if (type != typeof(string) && (bool)row.ItemArray[schema.Columns.IndexOf("AllowDbNull")])
                            {
                                type = typeof(Nullable<>).MakeGenericType(type);
                            }
                            createAutoImplementedProperty(builder, name, type);
                        }
                    }
                }
                finally
                {
                    database.Connection.Close();
                    command.Parameters.Clear();
                }
            }

            Type resultType = builder.CreateType();

            return database.SqlQuery(resultType, sql, parameters);
        }
        private static TypeBuilder createTypeBuilder(
            string assemblyName, string moduleName, string typeName)
        {
            TypeBuilder typeBuilder = AppDomain
                .CurrentDomain
                .DefineDynamicAssembly(new AssemblyName(assemblyName),
                                       AssemblyBuilderAccess.Run)
                .DefineDynamicModule(moduleName)
                .DefineType(typeName, TypeAttributes.Public);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            return typeBuilder;
        }
        private static void createAutoImplementedProperty(
            TypeBuilder builder, string propertyName, Type propertyType)
        {
            const string PrivateFieldPrefix = "m_";
            const string GetterPrefix = "get_";
            const string SetterPrefix = "set_";

            // Generate the field.
            FieldBuilder fieldBuilder = builder.DefineField(
                string.Concat(PrivateFieldPrefix, propertyName),
                              propertyType, FieldAttributes.Private);

            // Generate the property
            PropertyBuilder propertyBuilder = builder.DefineProperty(
                propertyName, System.Reflection.PropertyAttributes.HasDefault, propertyType, null);

            // Property getter and setter attributes.
            MethodAttributes propertyMethodAttributes =
                MethodAttributes.Public | MethodAttributes.SpecialName |
                MethodAttributes.HideBySig;

            // Define the getter method.
            MethodBuilder getterMethod = builder.DefineMethod(
                string.Concat(GetterPrefix, propertyName),
                propertyMethodAttributes, propertyType, Type.EmptyTypes);

            // Emit the IL code.
            // ldarg.0
            // ldfld,_field
            // ret
            ILGenerator getterILCode = getterMethod.GetILGenerator();
            getterILCode.Emit(OpCodes.Ldarg_0);
            getterILCode.Emit(OpCodes.Ldfld, fieldBuilder);
            getterILCode.Emit(OpCodes.Ret);

            // Define the setter method.
            MethodBuilder setterMethod = builder.DefineMethod(
                string.Concat(SetterPrefix, propertyName),
                propertyMethodAttributes, null, new Type[] { propertyType });

            // Emit the IL code.
            // ldarg.0
            // ldarg.1
            // stfld,_field
            // ret
            ILGenerator setterILCode = setterMethod.GetILGenerator();
            setterILCode.Emit(OpCodes.Ldarg_0);
            setterILCode.Emit(OpCodes.Ldarg_1);
            setterILCode.Emit(OpCodes.Stfld, fieldBuilder);
            setterILCode.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getterMethod);
            propertyBuilder.SetSetMethod(setterMethod);
        }

        public static EntityCPAIEngine context = new EntityCPAIEngine();
        

        public static object GetFilterCrudePurchase_Customer()
        {
            var query = DynamicSqlQuery(context.Database, "SELECT MCD_ROW_ID, MCD_NAME_1 || ' ' || MCD_NAME_2 as MCD_NAME" +
                " FROM MT_CUST_CONTROL" +
                " LEFT JOIN MT_CUST_DETAIL ON MCR_FK_CUST_CONTROL_ID = MCD_ROW_ID" +
                " WHERE MCR_SYSTEM = 'CDS'", new[] { new OracleParameter("M", 1) });
            return query;
        }
        public static object GetFilterCrudePurchase_CrudeName()
        {
            var query = DynamicSqlQuery(context.Database, "SELECT DISTINCT MET_MAT_DES_ENGLISH" +
                " FROM MT_MATERIALS" +
                " WHERE MET_CREATE_TYPE = 'COOL' AND MET_MAT_DES_ENGLISH IS NOT NULL" +
                " ORDER BY MET_MAT_DES_ENGLISH", new[] { new OracleParameter("M", 1) });
            return query;
        }
        public static object GetFilterCrudePurchase_LR()
        {
            var query = DynamicSqlQuery(context.Database, "SELECT DISTINCT CBI_MATERIAL_NAME_OTHER" +
                " FROM VW_CRUDE_SALE_REF" +
                " WHERE CBI_MATERIAL_NAME_OTHER IS NOT NULL", new[] { new OracleParameter("M", 1) });
            return query;
        }
        public static object GetSearchCrudePurchase_CrudeRef_Detail(string SaleNo)
        {
            var query = DynamicSqlQuery(context.Database, "SELECT *" +
                " FROM VW_CRUDE_SALE_REF" +
                " WHERE SALE_NO = '" + SaleNo + "'" +
                " ORDER BY SALE_NO DESC", new[] { new OracleParameter("M", 1) });
            return query;
        }
        public static object GetSearchCrudePurchase_CrudeRef(String json)
        {
            CrudeSale_Ref_Filter Filter = (CrudeSale_Ref_Filter)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(CrudeSale_Ref_Filter));
            var where_Filter = "";
            if (Filter.SaleNo != "" && Filter.SaleNo != null)
            {
                where_Filter += " WHERE SALE_NO = '" + Filter.SaleNo + "'";
            }
            if (where_Filter == ""
                && Filter.SaleDateStart != "" && Filter.SaleDateStart != null
                && Filter.SaleDateEnd != "" && Filter.SaleDateEnd != null)
            {
                var dateFrom = "";
                var dateTo = "";
                dateFrom = Filter.SaleDateStart.Replace("-", "/");
                dateTo = (Convert.ToDateTime(Filter.SaleDateEnd).Date.ToString("yyyy/MM/dd")).Replace("-", "/");
                where_Filter += " WHERE (SALE_DATE >= TO_DATE('" + dateFrom + "', 'yyyy/mm/dd') AND SALE_DATE <= TO_DATE('" + dateTo + "','yyyy/mm/dd'))";
            }
            else if (where_Filter != ""
                && Filter.SaleDateStart != "" && Filter.SaleDateStart != null
                && Filter.SaleDateEnd != "" &&Filter.SaleDateEnd != null)
            {
                var dateFrom = "";
                var dateTo = "";
                dateFrom = Filter.SaleDateStart.Replace("-", "/");
                dateTo = (Convert.ToDateTime(Filter.SaleDateEnd).Date.ToString("yyyy/MM/dd")).Replace("-", "/");
                where_Filter += " AND (SALE_DATE >= TO_DATE('" + dateFrom + "', 'yyyy/mm/dd') AND SALE_DATE <= TO_DATE('" + dateTo + "','yyyy/mm/dd'))";
            }
            if (where_Filter == ""
                && Filter.Customer != "" && Filter.Customer != null)
            {
                where_Filter += " WHERE CUSTOMER_NAME = '" + Filter.Customer + "'";
            }
            else if (where_Filter != ""
                && Filter.Customer != "" && Filter.Customer != null)
            {
                where_Filter += " AND CUSTOMER_NAME = '" + Filter.Customer + "'";
            }
            if (where_Filter == ""
                && Filter.Feedstock != "ALL")
            {
                where_Filter += " WHERE FEEDSTOCK = '" + Filter.Feedstock + "'";
                if (Filter.CrudeName != "" && Filter.CrudeName != null)
                {
                    if (Filter.Feedstock == "CRUDE")
                    {
                        where_Filter += " AND CRUDE_NAME = '" + Filter.CrudeName + "'";
                    }
                    else
                    {
                        where_Filter += " AND CBI_MATERIAL_NAME_OTHER = '" + Filter.CrudeName + "'";
                    }
                }
            }
            else if (where_Filter != ""
                && Filter.Feedstock != "ALL")
            {
                where_Filter += " AND FEEDSTOCK = '" + Filter.Feedstock + "'";
                if (Filter.CrudeName != "" && Filter.CrudeName != null)
                {
                    if (Filter.Feedstock == "CRUDE")
                    {
                        where_Filter += " AND CRUDE_NAME = '" + Filter.CrudeName + "'";
                    }
                    else
                    {
                        where_Filter += " AND CBI_MATERIAL_NAME_OTHER = '" + Filter.CrudeName + "'";
                    }
                }
            }
            
            var query = JsonConvert.SerializeObject(DynamicSqlQuery(context.Database, "SELECT SALE_NO" +
                " FROM VW_CRUDE_SALE_REF" +
                where_Filter +
                " ORDER BY SALE_NO DESC", new[] { new OracleParameter("M", 1) }));
            JArray list = (JArray)JsonConvert.DeserializeObject(query);
            if(list.Count() > 0)
            {
                var where_Sale_No = "";
                for (int i = 0; i < list.Count(); i++)
                {
                    if (i == 0)
                    {
                        where_Sale_No = " WHERE SALE_NO = '" + list.ElementAt(i)["SALE_NO"] + "'";
                    }
                    else
                    {
                        where_Sale_No += " OR SALE_NO = '" + list.ElementAt(i)["SALE_NO"] + "'";
                    }
                }
                var query2 = DynamicSqlQuery(context.Database, "SELECT *" +
                    " FROM VW_CRUDE_SALE_REF" +
                    where_Sale_No +
                    " ORDER BY SALE_NO DESC", new[] { new OracleParameter("M", 1) });
                return query2;
            }
            else
            {
                return query;
            }
            
        }
        public class CrudeSale_Ref_Filter
        {
            public string SaleNo { get; set; }
            public string SaleDateStart { get; set; }
            public string SaleDateEnd { get; set; }
            public string Customer { get; set; }
            public string Feedstock { get; set; }
            public string CrudeName { get; set; }
        }
        public static object GetSearchCrudePurchase_CrudeRef_OPSetup(string taskID)
        {
            var query = DynamicSqlQuery(context.Database, "SELECT *" +
                " FROM CDP_DATA" +
                " LEFT JOIN CDO_OPTIMIZATION_DETAIL ON CDO_OPTIMIZATION_DETAIL.COO_FK_CDP_DATA = CDP_DATA.CDA_ROW_ID" +
                " LEFT JOIN VW_CRUDE_SALE_REF ON VW_CRUDE_SALE_REF.CDO_ROW_ID = CDO_OPTIMIZATION_DETAIL.COO_FK_CDO_DATA" +
                " WHERE CDP_DATA.CDA_ROW_ID = '" + taskID + "'", new[] { new OracleParameter("M", 1) });
            return query;
        }
    }
}
