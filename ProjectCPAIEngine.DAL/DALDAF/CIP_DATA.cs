﻿using ProjectCPAIEngine.DAL.DALPAF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class CIP_DATA
    {
        public string TRIP_NO { get; set; }
        public string VESSEL_CODE { get; set; }
        public string VESSEL_NAME { get; set; }
        public Nullable<Decimal> PDPR { get; set; }
        public string CREATED_BY { get; set; }
        public string MAT_TYPE { get; set; }

        public Nullable<DateTime> ACTUAL_ETA_FROM { get; set; }
        public Nullable<DateTime> ACTUAL_ETA_TO { get; set; }

        public string SHOW_ACTUAL_ETA
        {
            get
            {
                if (this.ACTUAL_ETA_FROM != null && this.ACTUAL_ETA_TO != null)
                {
                    return PAFHelper.ToOnlyDateString(this.ACTUAL_ETA_FROM) + " - " + PAFHelper.ToOnlyDateString(this.ACTUAL_ETA_TO);
                }
                return null;
            }
        }

        public string GT_C
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    return this.CIP_MATERIALS[0].GT_C;
                }

                return "-";
            }
        }
        public string INCOTERMS
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    return this.CIP_MATERIALS[0].INCOTERMS;
                }

                return "-";
            }
        }
        public string CRUDE_NAMES
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    List<string> crude_name = this.CIP_MATERIALS.Select(m => m.MET_NAME).Distinct().ToList();
                    return String.Join(", ", crude_name);
                }

                return "-";
            }
        }


        public Nullable<DateTime> DISCHARGE_LAYCAN_FROM
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    return this.CIP_MATERIALS[0].DISCHARGE_LAYCAN_FROM;
                }
                return null;
            }
        }

        public Nullable<DateTime> DISCHARGE_LAYCAN_TO
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    return this.CIP_MATERIALS[0].DISCHARGE_LAYCAN_TO;
                }
                return null;
            }
        }

        public Nullable<DateTime> LOADING_DATE_FROM
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    return this.CIP_MATERIALS[0].LOADING_DATE_FROM;
                }
                return null;
            }
        }

        public Nullable<DateTime> LOADING_DATE_TO
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0)
                {
                    return this.CIP_MATERIALS[0].LOADING_DATE_TO;
                }
                return null;
            }
        }

        public string SHOW_DISCHARGE_LAYCAN
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0 && this.CIP_MATERIALS[0] != null)
                {
                    return PAFHelper.ToOnlyDateString(this.CIP_MATERIALS[0].DISCHARGE_LAYCAN_FROM) + " - " + PAFHelper.ToOnlyDateString(this.CIP_MATERIALS[0].DISCHARGE_LAYCAN_TO);
                }
                return null;
            }
        }

        public string SHOW_LOADING_LAYCAN
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0 && this.CIP_MATERIALS[0] != null)
                {
                    return PAFHelper.ToOnlyDateString(this.CIP_MATERIALS[0].LOADING_DATE_FROM) + " - " + PAFHelper.ToOnlyDateString(this.CIP_MATERIALS[0].LOADING_DATE_TO);
                }
                return null;
            }
        }

        public string SUPPLIER_MAT_CODE
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0 && this.CIP_MATERIALS[0] != null)
                {
                    return this.CIP_MATERIALS[0].SUPPLIER;
                }
                return null;
            }
        }

        public string SUPPLIER_MAT_NAME
        {
            get
            {
                if (this.CIP_MATERIALS != null && this.CIP_MATERIALS.Count() > 0 && this.CIP_MATERIALS[0] != null)
                {
                    return this.CIP_MATERIALS[0].SUPPLIER_NAME;
                }
                return null;
            }
        }

        public string SUPPLIER_CODE
        {
            get
            {
                if (this.CIP_FREIGHT != null)
                {
                    return this.CIP_FREIGHT.SUPPLIER_CODE;
                }
                return null;
            }
        }

        public string SUPPLIER_NAME
        {
            get
            {
                if (this.CIP_FREIGHT != null)
                {
                    return this.CIP_FREIGHT.SUPPLIER_NAME;
                }
                return null;
            }
        }

        public string BROKER_CODE
        {
            get
            {
                if (this.CIP_FREIGHT != null)
                {
                    return this.CIP_FREIGHT.BROKER_CODE;
                }
                return null;
            }
        }

        public string BROKER_NAME
        {
            get
            {
                if (this.CIP_FREIGHT != null)
                {
                    return this.CIP_FREIGHT.BROKER_NAME;
                }
                return null;
            }
        }


        public List<CIP_MATERIALS> CIP_MATERIALS { get; set; }
        public CIP_FREIGHT CIP_FREIGHT { get; set; }
        public Nullable<decimal> LAYTIME
        {
            get
            {
                if(this.CIP_FREIGHT != null)
                {
                    return this.CIP_FREIGHT.LAYTIME_VALUE;
                }
                return null;
            }
        }
        public Nullable<decimal> DEMURRAGE
        {
            get
            {
                if (this.CIP_FREIGHT != null)
                {
                    return this.CIP_FREIGHT.DEMURAGE;
                }
                return null;
            }
        }
    }

    public class CIP_MATERIALS
    {
        public string TRIP_NO { get; set; }
        public string INCOTERMS { get; set; }
        public string MET_NUM { get; set; }
        public string MET_NAME { get; set; }
        public string GT_C { get; set; }

        public Nullable<DateTime> BL_DATE { get; set; }
        public Nullable<DateTime> DISCHARGE_LAYCAN_FROM { get; set; }
        public Nullable<DateTime> DISCHARGE_LAYCAN_TO { get; set; }
        public Nullable<DateTime> LOADING_DATE_FROM { get; set; }
        public Nullable<DateTime> LOADING_DATE_TO { get; set; }
        public string BL_STATUS { get; set; }
        public string SUPPLIER { get; set; }
        public string SUPPLIER_NAME { get; set; }
    }

    public class CIP_FREIGHT
    {
        public string TRIP_NO { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string BROKER_CODE { get; set; }
        public string BROKER_NAME { get; set; }
        public string LAYTIME { get; set; }
        public Nullable<decimal> LAYTIME_VALUE
        {
            get
            {
                if(LAYTIME == null)
                {
                    return null;
                }
                decimal a;
                Decimal.TryParse(LAYTIME, out a);
                return a;
            }
        }
        public Nullable<decimal> DEMURAGE { get; set; }
    }



    #region --- deprecated
    public class CIP_DATA_OLD
    {
        public string TRIP_NO { get; set; }
        public string SHIP_OWNER { get; set; }

        public string BROKER_CODE { get; set; }
        public string BROKER_NAME { get; set; }

        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }

        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }


        public string VESSEL_CODE { get; set; }
        public string VESSEL_NAME { get; set; }

        public Nullable<DateTime> CP_DATE { get; set; }
        public Nullable<DateTime> LOADING_DATE_FROM { get; set; }
        public Nullable<DateTime> LOADING_DATE_TO { get; set; }
        public Nullable<DateTime> DISCHARG_DATE_FROM { get; set; }
        public Nullable<DateTime> DISCHARG_DATE_TO { get; set; }
        public Nullable<DateTime> CONTRACT_DATE_FROM { get; set; }
        public Nullable<DateTime> CONTRACT_DATE_TO { get; set; }
        public Nullable<DateTime> LAYCAN_FROM { get; set; }
        public Nullable<DateTime> LAYCAN_TO { get; set; }
        public string LOADING_PORT { get; set; }
        public string DISCHARGING_PORT { get; set; }
        public string AGREED_LAYCAN { get; set; }
        public string CRUDE { get; set; }
        public string CRUDE_NAME { get; set; }
        
        public string PRODUCT { get; set; }
        public Nullable<DateTime> BL_DATE { get; set; }
        public Nullable<DateTime> ARRIVAL_DATE { get; set; }
        public string INCOTERMS { get; set; }
        public string SHOW_ARRIVAL_DATE
        {
            get
            {
                if (this.ARRIVAL_DATE != null)
                {
                    return PAFHelper.ToOnlyDateString(this.ARRIVAL_DATE);
                }

                return "-";
            }
        }
        public string SHOW_CONTRACT_DATE
        {
            get
            {
                if (this.CONTRACT_DATE_FROM != null && this.CONTRACT_DATE_TO != null)
                {
                    return PAFHelper.ToOnlyDateString(this.CONTRACT_DATE_FROM) + " - " + PAFHelper.ToOnlyDateString(this.CONTRACT_DATE_TO);
                }

                return "-";
            }
        }
        public string SHOW_LOADING_DATE
        {
            get
            {
                if (this.LOADING_DATE_FROM != null && this.LOADING_DATE_TO != null)
                {
                    return PAFHelper.ToOnlyDateString(this.LOADING_DATE_FROM) + " - " + PAFHelper.ToOnlyDateString(this.LOADING_DATE_TO);
                }

                return "-";
            }
        }
        public string SHOW_LAYCAN_DATE
        {
            get
            {
                if (this.LAYCAN_FROM != null && this.LAYCAN_TO != null)
                {
                    return PAFHelper.ToOnlyDateString(this.LAYCAN_FROM) + " - " + PAFHelper.ToOnlyDateString(this.LAYCAN_TO);
                }

                return "-";
            }
        }

        public string CREATED_BY { get; set; }
    }
    #endregion

}
