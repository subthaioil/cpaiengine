﻿using com.pttict.engine.dal.Service;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAF_ACTION_MESSAGE_DAL
    {
        public List<CPAI_ACTION_MESSAGE> findUserGroupMail(string action, string system, string type, string funciton, string current_status)
        {
            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                (
                    x.AMS_CURRENT_STATUS != null &&
                    (
                        x.AMS_CURRENT_STATUS.Contains(current_status) ||
                        x.AMS_CURRENT_STATUS == "ALL"
                    )
                ) &&
                x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }
        public List<CPAI_ACTION_MESSAGE> findUserGroupNoti(string action, string system, string type, string funciton, string ex_group)
        {
            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                x.AMS_NOTI_FLAG.ToUpper() == "Y".ToUpper() &&
                //x.AMS_USR_GROUP.ToUpper().Contains(ex_group) &&
                x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    var grouped = from p in results
                                  group p by p.AMS_USR_GROUP into g
                                  select g;

                    var realResult = new List<CPAI_ACTION_MESSAGE>();
                    for (int i = 0; i < grouped.Count(); i++)
                    {
                        var eachGroup = grouped.ElementAt(i);
                        if (eachGroup.Count() == 1)
                        {
                            realResult.Add(eachGroup.ElementAt(0));
                        }
                        else
                        {
                            List<CPAI_ACTION_MESSAGE> SortedList = eachGroup.OrderByDescending(o => o.AMS_FK_USER).ToList();
                            for (int j = 0; j < SortedList.Count; j++)
                            {
                                if (SortedList.ElementAt(j).AMS_FK_USER != null)
                                {
                                    realResult.Add(SortedList.ElementAt(j));
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                    return realResult;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }
    }
}
