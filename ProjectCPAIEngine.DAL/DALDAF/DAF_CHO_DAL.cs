﻿using com.pttict.engine.dal.Entity;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAF_CHO_DAL
    {
        EntityCPAIEngine db = new EntityCPAIEngine();
        EntitiesEngine entity = new EntitiesEngine();
        public List<CHO_DATA> getCHOList(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            string query = @"SELECT * FROM VW_CHO_DAF_HEADER";
            List<CHO_DATA> data = db.Database.SqlQuery<CHO_DATA>(query).ToList();
            foreach(CHO_DATA elem in data)
            {
                elem.CHO_CARGOS = this.GetCHOCargoes(elem.TRIP_NO);
                elem.CHO_PORTS = this.GetCHOPorts(elem.TRIP_NO);
                elem.CHO_PORTS_D = elem.CHO_PORTS.Where(m => m.PORT_TYPE == "D").ToList();
                elem.CHO_PORTS_L = elem.CHO_PORTS.Where(m => m.PORT_TYPE == "L").ToList();
            }
            if ( criteria != null)
            {
                if(!string.IsNullOrEmpty(criteria.DOC_NO))
                {
                    data = data.Where(m => (m.TRIP_NO ?? "").Contains(criteria.DOC_NO)).ToList();
                }

                if (!string.IsNullOrEmpty(criteria.VESSEL_NAME))
                {
                    data = data.Where(m => (m.VESSEL_NAME ?? "").ToUpper().Contains(criteria.VESSEL_NAME.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(criteria.CHARTERER))
                {
                    data = data.Where(m => (m.CUSTOMER_NAME ?? "").ToUpper().Contains(criteria.CHARTERER.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(criteria.CREATED_BY))
                {
                    data = data.Where(m => (m.CREATED_BY ?? "").ToUpper().Contains(criteria.CREATED_BY.ToUpper())).ToList();
                }

                if (criteria.LAYCAN_FROM != null && criteria.LAYCAN_TO != null)
                {
                    data = data.Where(m => m.LAYCAN_FROM >= criteria.LAYCAN_FROM && m.LAYCAN_TO <= criteria.LAYCAN_TO).ToList();
                }
            }
            return data;
        }

        public List<CHO_CARGOS> GetCHOCargoes(string TRIP_NO)
        {
            string query = @"SELECT * FROM VW_CHO_DAF_CARGOES WHERE TRIP_NO = :TRIP_NO";
            List<CHO_CARGOS> data = db.Database.SqlQuery<CHO_CARGOS>(query, new[] { new OracleParameter("TRIP_NO", TRIP_NO) }).ToList();
            return data;
        }

        public List<CHO_PORTS> GetCHOPorts(string TRIP_NO)
        {
            string query = @"SELECT * FROM VW_CHO_DAF_PORTS WHERE TRIP_NO = :TRIP_NO";
            List<CHO_PORTS> data = db.Database.SqlQuery<CHO_PORTS>(query, new[] { new OracleParameter("TRIP_NO", TRIP_NO) }).ToList();
            return data;
        }
    }
}
