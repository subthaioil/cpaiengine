﻿using com.pttict.engine.dal.Entity;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAF_CIP_DAL
    {
        EntityCPAIEngine db = new EntityCPAIEngine();
        EntitiesEngine entity = new EntitiesEngine();

        public List<CIP_DATA> getCIPCrudeList(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            List<CIP_DATA> data = this.getCIPList(criteria);
            data = data.Where(m => m.MAT_TYPE == "CRD").ToList();
            return data;
        }

        public List<CIP_DATA> getCIPProductList(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            List<CIP_DATA> data = this.getCIPList(criteria);
            data = data.Where(m => m.MAT_TYPE == "PRD").ToList();
            return data;
        }

        public List<CIP_DATA> getCIPList(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            string query = @"SELECT * FROM VW_PCF_DAF_HEADER";
            List<CIP_DATA> data = db.Database.SqlQuery<CIP_DATA>(query).ToList();
            data = data.OrderByDescending(m => m.TRIP_NO).ToList();
            foreach (CIP_DATA elem in data)
            {
                elem.CIP_MATERIALS = this.GetMaterials(elem.TRIP_NO);
                elem.CIP_FREIGHT = this.GetFreight(elem.TRIP_NO);
            }
            if (criteria != null)
            {
                if (!string.IsNullOrEmpty(criteria.DOC_NO))
                {
                    data = data.Where(m => (m.TRIP_NO ?? "").Contains(criteria.DOC_NO)).ToList();
                }

                if (!string.IsNullOrEmpty(criteria.VESSEL_NAME))
                {
                    data = data.Where(m => (m.VESSEL_NAME ?? "").ToUpper().Contains(criteria.VESSEL_NAME.ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(criteria.CREATED_BY))
                {
                    data = data.Where(m => (m.CREATED_BY ?? "").ToUpper().Contains(criteria.CREATED_BY.ToUpper())).ToList();
                }

                if(criteria.DISCHARGE_FROM != null && criteria.DISCHARGE_TO != null)
                {
                    data = data.Where(m => m.DISCHARGE_LAYCAN_FROM >= criteria.DISCHARGE_FROM && m.DISCHARGE_LAYCAN_TO <= criteria.DISCHARGE_TO).ToList();
                }

                if (criteria.LOADING_FROM != null && criteria.LOADING_TO != null)
                {
                    data = data.Where(m => m.DISCHARGE_LAYCAN_FROM >= criteria.LOADING_FROM && m.DISCHARGE_LAYCAN_TO <= criteria.LOADING_TO).ToList();
                }

            }
            data = data.Where(m => m.CIP_MATERIALS != null && m.CIP_MATERIALS.Count() > 0).ToList();

            return data;
        }


        public List<CIP_MATERIALS> GetMaterials(string TRIP_NO)
        {
            string query = @"SELECT * FROM VW_PCF_DAF_MATERIALS WHERE TRIP_NO = :TRIP_NO";
            List<CIP_MATERIALS> data = db.Database.SqlQuery<CIP_MATERIALS>(query, new[] { new OracleParameter("TRIP_NO", TRIP_NO) }).ToList();
            return data;
        }

        public CIP_FREIGHT GetFreight(string TRIP_NO)
        {
            string query = @"SELECT * FROM VW_PCF_DAF_FREIGHT WHERE TRIP_NO = :TRIP_NO";
            CIP_FREIGHT data = db.Database.SqlQuery<CIP_FREIGHT>(query, new[] { new OracleParameter("TRIP_NO", TRIP_NO) }).FirstOrDefault();
            return data;
        }
    }
}
