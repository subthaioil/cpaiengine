﻿using ProjectCPAIEngine.DAL.DALPAF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Entity
{
    public partial class DAF_DATA
    {
        public string REQ_TRAN_ID { get; set; }
        public string TRAN_ID { get; set; }

        public string USER_GROUP { get; set; }

        public string SHOW_DDA_CONTRACT_DATE {
            get
            {
                var date = this.DDA_CONTRACT_DATE;
                try
                {
                    return ((DateTime)date).ToString("dd/MMM/yyyy");
                }
                catch
                {
                    return "";
                }
            }
        }
    }

}
