﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAFButtonAction
    {
        public string name { get; set; }
        public string page_url { get; set; }
        public string call_xml { get; set; }

    }

    public class DAF_DATA_WRAPPER
    {
        #region attribute
        public string DDA_ROW_ID { get; set; }
        public string DDA_FORM_ID { get; set; }
        public string DDA_FORM_TYPE { get; set; }
        public string DDA_USER_GROUP { get; set; }
        public Nullable<System.DateTime> DDA_DOC_DATE { get; set; }
        public string DDA_FOR_COMPANY { get; set; }
        public string DDA_TYPE_OF_TRANSECTION { get; set; }
        public string DDA_INCOTERM { get; set; }
        public string DDA_DEMURAGE_TYPE { get; set; }
        public string DDA_REF_DOC_NO { get; set; }
        public string DDA_COUNTERPARTY_TYPE { get; set; }
        public string DDA_SHIP_OWNER { get; set; }
        public string DDA_BROKER { get; set; }
        public string DDA_SUPPLIER { get; set; }
        public string DDA_CUSTOMER { get; set; }
        public string DDA_GT_C { get; set; }
        public Nullable<decimal> DDA_LAYTIME_CONTRACT_HRS { get; set; }
        public string DDA_LAYTIME_CONTRACT_TEXT { get; set; }
        public string DDA_VESSEL { get; set; }
        public Nullable<System.DateTime> DDA_CONTRACT_DATE { get; set; }
        public string DDA_CURRENCY_UNIT { get; set; }
        public Nullable<System.DateTime> DDA_AGREED_LOADING_LAYCAN_FROM { get; set; }
        public Nullable<System.DateTime> DDA_AGREED_LOADING_LAYCAN_TO { get; set; }
        public Nullable<decimal> DDA_DEMURAGE_RATE { get; set; }
        public Nullable<System.DateTime> DDA_AGREED_DC_LAYCAN_FROM { get; set; }
        public Nullable<System.DateTime> DDA_AGREED_DC_LAYCAN_TO { get; set; }
        public string DDA_SAILING_ROUTE { get; set; }
        public Nullable<decimal> DDA_TRT_ORIGIN_CLAIM_HRS { get; set; }
        public string DDA_TRT_ORIGIN_CLAIM_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TRT_TOP_REVIEWED_HRS { get; set; }
        public string DDA_TRT_TOP_REVIEWED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TRT_SETTLED_HRS { get; set; }
        public string DDA_TRT_SETTLED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TRT_SAVING_HRS { get; set; }
        public string DDA_TRT_SAVING_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TDT_ORIGIN_CLAIM_HRS { get; set; }
        public string DDA_TDT_ORIGIN_CLAIM_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TDT_TOP_REVIEWED_HRS { get; set; }
        public string DDA_TDT_TOP_REVIEWED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TDT_SETTLED_HRS { get; set; }
        public string DDA_TDT_SETTLED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TDT_SAVING_HRS { get; set; }
        public string DDA_TDT_SAVING_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_NTS_ORIGIN_CLAIM_HRS { get; set; }
        public string DDA_NTS_ORIGIN_CLAIM_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_NTS_TOP_REVIEWED_HRS { get; set; }
        public string DDA_NTS_TOP_REVIEWED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_NTS_SETTLED_HRS { get; set; }
        public string DDA_NTS_SETTLED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_NTS_SAVING_HRS { get; set; }
        public string DDA_NTS_SAVING_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_LY_ORIGIN_CLAIM_HRS { get; set; }
        public string DDA_LY_ORIGIN_CLAIM_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_LY_TOP_REVIEWED_HRS { get; set; }
        public string DDA_LY_TOP_REVIEWED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_LY_SETTLED_HRS { get; set; }
        public string DDA_LY_SETTLED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_LY_SAVING_HRS { get; set; }
        public string DDA_LY_SAVING_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TOD_ORIGIN_CLAIM_HRS { get; set; }
        public string DDA_TOD_ORIGIN_CLAIM_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TOD_TOP_REVIEWED_HRS { get; set; }
        public string DDA_TOD_TOP_REVIEWED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TOD_SETTLED_HRS { get; set; }
        public string DDA_TOD_SETTLED_TIME_TEXT { get; set; }
        public Nullable<decimal> DDA_TOD_SAVING_HRS { get; set; }
        public string DDA_TOD_SAVING_TIME_TEXT { get; set; }
        public string DDA_IS_ADDRESS_COMMISSION { get; set; }
        public Nullable<decimal> DDA_ADDRESS_COMMISSION_PERCEN { get; set; }
        public string DDA_IS_BROKER_COMMISSION { get; set; }
        public Nullable<decimal> DDA_BROKER_COMMISSION_PERCEN { get; set; }
        public string DDA_IS_SHARING_TO_TOP { get; set; }
        public Nullable<decimal> DDA_SHARING_TO_TOP_PERCEN { get; set; }
        public Nullable<decimal> DDA_DECIMAL { get; set; }
        public Nullable<decimal> DDA_GD_ORIGIN_CLAIM_VALUE { get; set; }
        public Nullable<decimal> DDA_GD_TOP_REVIEWED_VALUE { get; set; }
        public Nullable<decimal> DDA_GD_SETTLED_VALUE { get; set; }
        public Nullable<decimal> DDA_GD_SAVING_VALUE { get; set; }
        public Nullable<decimal> DDA_AC_ORIGIN_CLAIM_VALUE { get; set; }
        public Nullable<decimal> DDA_AC_TOP_REVIEWED_VALUE { get; set; }
        public Nullable<decimal> DDA_AC_SETTLED_VALUE { get; set; }
        public Nullable<decimal> DDA_AC_SAVING_VALUE { get; set; }
        public Nullable<decimal> DDA_STD_ORIGIN_CLAIM_VALUE { get; set; }
        public Nullable<decimal> DDA_STD_TOP_REVIEWED_VALUE { get; set; }
        public Nullable<decimal> DDA_STD_SETTLED_VALUE { get; set; }
        public Nullable<decimal> DDA_STD_SAVING_VALUE { get; set; }
        public Nullable<decimal> DDA_DST_ORIGIN_CLAIM_VALUE { get; set; }
        public Nullable<decimal> DDA_DST_TOP_REVIEWED_VALUE { get; set; }
        public Nullable<decimal> DDA_DST_SETTLED_VALUE { get; set; }
        public Nullable<decimal> DDA_DST_SAVING_VALUE { get; set; }
        public Nullable<decimal> DDA_ND_ORIGIN_CLAIM_VALUE { get; set; }
        public Nullable<decimal> DDA_ND_TOP_REVIEWED_VALUE { get; set; }
        public Nullable<decimal> DDA_ND_SETTLED_VALUE { get; set; }
        public Nullable<decimal> DDA_ND_SAVING_VALUE { get; set; }
        public string DDA_EXPLANATION { get; set; }
        public string DDA_TIME_ON_DEM_TEXT { get; set; }
        public Nullable<decimal> DDA_TIME_ON_DEM_HRS { get; set; }
        public Nullable<decimal> DDA_NET_PAYMENT_DEM_VALUE { get; set; }
        public Nullable<decimal> DDA_BROKER_COMMISSION_VALUE { get; set; }
        public Nullable<decimal> DDA_EXCHANGE_RATE { get; set; }
        public string DDA_STATUS { get; set; }
        public Nullable<System.DateTime> DDA_CREATED { get; set; }
        public string DDA_CREATED_BY { get; set; }
        public Nullable<System.DateTime> DDA_UPDATED { get; set; }
        public string DDA_UPDATED_BY { get; set; }
        public Nullable<System.DateTime> DDA_AGREED_LAYCAN_FROM { get; set; }
        public Nullable<System.DateTime> DDA_AGREED_LAYCAN_TO { get; set; }
        public string DDA_MATERIAL_TYPE { get; set; }
        public string DDA_REASON { get; set; }
        public string DDA_CUSTOMER_BROKER { get; set; }

        public Nullable<decimal> DDA_SETTLEMENT_AMOUNT_THB { get; set; }
        public string DDA_IS_OVERRIDE_NET_PAYMENT { get; set; }
        public Nullable<decimal> DDA_NET_PAYMENT_DEM_VALUE_OV { get; set; }


        public List<DAF_ATTACH_FILE> DAF_ATTACH_FILE { get; set; }
        public MT_INCOTERMS MT_INCOTERMS { get; set; }
        public MT_VENDOR MT_VENDOR { get; set; }
        public MT_VENDOR MT_VENDOR1 { get; set; }
        public MT_CUST MT_CUST { get; set; }
        public MT_VEHICLE MT_VEHICLE { get; set; }
        public List<DAF_MATERIAL> DAF_MATERIAL { get; set; }
        public List<DAF_PORT> DAF_PORT { get; set; }
        public Nullable<decimal> DDA_LAYTIME_QUANTITY_RATE { get; set; }
        public Nullable<decimal> DDA_LAYTIME_QUANTITY { get; set; }
        #endregion

        #region extra attrivute
        //Extra
        public bool IS_DISABLED { get; set; }
        public bool IS_DISABLED_NOTE { get; set; }
        public List<DAFButtonAction> Buttons { get; set; }
        #endregion


        public DAF_DATA_WRAPPER()
        {

        }

        public DAF_DATA_WRAPPER(DAF_DATA source)
        {
            #region set attribute
            this.DDA_ROW_ID = source.DDA_ROW_ID;
            this.DDA_FORM_ID = source.DDA_FORM_ID;
            this.DDA_FORM_TYPE = source.DDA_FORM_TYPE;
            this.DDA_USER_GROUP = source.DDA_USER_GROUP;
            this.DDA_DOC_DATE = source.DDA_DOC_DATE;
            this.DDA_FOR_COMPANY = source.DDA_FOR_COMPANY;
            this.DDA_TYPE_OF_TRANSECTION = source.DDA_TYPE_OF_TRANSECTION;
            this.DDA_INCOTERM = source.DDA_INCOTERM;
            this.DDA_DEMURAGE_TYPE = source.DDA_DEMURAGE_TYPE;
            this.DDA_REF_DOC_NO = source.DDA_REF_DOC_NO;
            this.DDA_COUNTERPARTY_TYPE = source.DDA_COUNTERPARTY_TYPE;
            this.DDA_SHIP_OWNER = source.DDA_SHIP_OWNER;
            this.DDA_BROKER = source.DDA_BROKER;
            this.DDA_SUPPLIER = source.DDA_SUPPLIER;
            this.DDA_CUSTOMER = source.DDA_CUSTOMER;
            this.DDA_GT_C = source.DDA_GT_C;
            this.DDA_LAYTIME_CONTRACT_HRS = source.DDA_LAYTIME_CONTRACT_HRS;
            this.DDA_LAYTIME_CONTRACT_TEXT = source.DDA_LAYTIME_CONTRACT_TEXT;
            this.DDA_VESSEL = source.DDA_VESSEL;
            this.DDA_CONTRACT_DATE = source.DDA_CONTRACT_DATE;
            this.DDA_CURRENCY_UNIT = source.DDA_CURRENCY_UNIT;
            this.DDA_AGREED_LOADING_LAYCAN_FROM = source.DDA_AGREED_LOADING_LAYCAN_FROM;
            this.DDA_AGREED_LOADING_LAYCAN_TO = source.DDA_AGREED_LOADING_LAYCAN_TO;
            this.DDA_DEMURAGE_RATE = source.DDA_DEMURAGE_RATE;
            this.DDA_AGREED_DC_LAYCAN_FROM = source.DDA_AGREED_DC_LAYCAN_FROM;
            this.DDA_AGREED_DC_LAYCAN_TO = source.DDA_AGREED_DC_LAYCAN_TO;
            this.DDA_SAILING_ROUTE = source.DDA_SAILING_ROUTE;
            this.DDA_TRT_ORIGIN_CLAIM_HRS = source.DDA_TRT_ORIGIN_CLAIM_HRS;
            this.DDA_TRT_ORIGIN_CLAIM_TIME_TEXT = source.DDA_TRT_ORIGIN_CLAIM_TIME_TEXT;
            this.DDA_TRT_TOP_REVIEWED_HRS = source.DDA_TRT_TOP_REVIEWED_HRS;
            this.DDA_TRT_TOP_REVIEWED_TIME_TEXT = source.DDA_TRT_TOP_REVIEWED_TIME_TEXT;
            this.DDA_TRT_SETTLED_HRS = source.DDA_TRT_SETTLED_HRS;
            this.DDA_TRT_SETTLED_TIME_TEXT = source.DDA_TRT_SETTLED_TIME_TEXT;
            this.DDA_TRT_SAVING_HRS = source.DDA_TRT_SAVING_HRS;
            this.DDA_TRT_SAVING_TIME_TEXT = source.DDA_TRT_SAVING_TIME_TEXT;
            this.DDA_TDT_ORIGIN_CLAIM_HRS = source.DDA_TDT_ORIGIN_CLAIM_HRS;
            this.DDA_TDT_ORIGIN_CLAIM_TIME_TEXT = source.DDA_TDT_ORIGIN_CLAIM_TIME_TEXT;
            this.DDA_TDT_TOP_REVIEWED_HRS = source.DDA_TDT_TOP_REVIEWED_HRS;
            this.DDA_TDT_TOP_REVIEWED_TIME_TEXT = source.DDA_TDT_TOP_REVIEWED_TIME_TEXT;
            this.DDA_TDT_SETTLED_HRS = source.DDA_TDT_SETTLED_HRS;
            this.DDA_TDT_SETTLED_TIME_TEXT = source.DDA_TDT_SETTLED_TIME_TEXT;
            this.DDA_TDT_SAVING_HRS = source.DDA_TDT_SAVING_HRS;
            this.DDA_TDT_SAVING_TIME_TEXT = source.DDA_TDT_SAVING_TIME_TEXT;
            this.DDA_NTS_ORIGIN_CLAIM_HRS = source.DDA_NTS_ORIGIN_CLAIM_HRS;
            this.DDA_NTS_ORIGIN_CLAIM_TIME_TEXT = source.DDA_NTS_ORIGIN_CLAIM_TIME_TEXT;
            this.DDA_NTS_TOP_REVIEWED_HRS = source.DDA_NTS_TOP_REVIEWED_HRS;
            this.DDA_NTS_TOP_REVIEWED_TIME_TEXT = source.DDA_NTS_TOP_REVIEWED_TIME_TEXT;
            this.DDA_NTS_SETTLED_HRS = source.DDA_NTS_SETTLED_HRS;
            this.DDA_NTS_SETTLED_TIME_TEXT = source.DDA_NTS_SETTLED_TIME_TEXT;
            this.DDA_NTS_SAVING_HRS = source.DDA_NTS_SAVING_HRS;
            this.DDA_NTS_SAVING_TIME_TEXT = source.DDA_NTS_SAVING_TIME_TEXT;
            this.DDA_LY_ORIGIN_CLAIM_HRS = source.DDA_LY_ORIGIN_CLAIM_HRS;
            this.DDA_LY_ORIGIN_CLAIM_TIME_TEXT = source.DDA_LY_ORIGIN_CLAIM_TIME_TEXT;
            this.DDA_LY_TOP_REVIEWED_HRS = source.DDA_LY_TOP_REVIEWED_HRS;
            this.DDA_LY_TOP_REVIEWED_TIME_TEXT = source.DDA_LY_TOP_REVIEWED_TIME_TEXT;
            this.DDA_LY_SETTLED_HRS = source.DDA_LY_SETTLED_HRS;
            this.DDA_LY_SETTLED_TIME_TEXT = source.DDA_LY_SETTLED_TIME_TEXT;
            this.DDA_LY_SAVING_HRS = source.DDA_LY_SAVING_HRS;
            this.DDA_LY_SAVING_TIME_TEXT = source.DDA_LY_SAVING_TIME_TEXT;
            this.DDA_TOD_ORIGIN_CLAIM_HRS = source.DDA_TOD_ORIGIN_CLAIM_HRS;
            this.DDA_TOD_ORIGIN_CLAIM_TIME_TEXT = source.DDA_TOD_ORIGIN_CLAIM_TIME_TEXT;
            this.DDA_TOD_TOP_REVIEWED_HRS = source.DDA_TOD_TOP_REVIEWED_HRS;
            this.DDA_TOD_TOP_REVIEWED_TIME_TEXT = source.DDA_TOD_TOP_REVIEWED_TIME_TEXT;
            this.DDA_TOD_SETTLED_HRS = source.DDA_TOD_SETTLED_HRS;
            this.DDA_TOD_SETTLED_TIME_TEXT = source.DDA_TOD_SETTLED_TIME_TEXT;
            this.DDA_TOD_SAVING_HRS = source.DDA_TOD_SAVING_HRS;
            this.DDA_TOD_SAVING_TIME_TEXT = source.DDA_TOD_SAVING_TIME_TEXT;
            this.DDA_IS_ADDRESS_COMMISSION = source.DDA_IS_ADDRESS_COMMISSION;
            this.DDA_ADDRESS_COMMISSION_PERCEN = source.DDA_ADDRESS_COMMISSION_PERCEN;
            this.DDA_IS_BROKER_COMMISSION = source.DDA_IS_BROKER_COMMISSION;
            this.DDA_BROKER_COMMISSION_PERCEN = source.DDA_BROKER_COMMISSION_PERCEN;
            this.DDA_IS_SHARING_TO_TOP = source.DDA_IS_SHARING_TO_TOP;
            this.DDA_SHARING_TO_TOP_PERCEN = source.DDA_SHARING_TO_TOP_PERCEN;
            this.DDA_DECIMAL = source.DDA_DECIMAL;
            this.DDA_GD_ORIGIN_CLAIM_VALUE = source.DDA_GD_ORIGIN_CLAIM_VALUE;
            this.DDA_GD_TOP_REVIEWED_VALUE = source.DDA_GD_TOP_REVIEWED_VALUE;
            this.DDA_GD_SETTLED_VALUE = source.DDA_GD_SETTLED_VALUE;
            this.DDA_GD_SAVING_VALUE = source.DDA_GD_SAVING_VALUE;
            this.DDA_AC_ORIGIN_CLAIM_VALUE = source.DDA_AC_ORIGIN_CLAIM_VALUE;
            this.DDA_AC_TOP_REVIEWED_VALUE = source.DDA_AC_TOP_REVIEWED_VALUE;
            this.DDA_AC_SETTLED_VALUE = source.DDA_AC_SETTLED_VALUE;
            this.DDA_AC_SAVING_VALUE = source.DDA_AC_SAVING_VALUE;
            this.DDA_STD_ORIGIN_CLAIM_VALUE = source.DDA_STD_ORIGIN_CLAIM_VALUE;
            this.DDA_STD_TOP_REVIEWED_VALUE = source.DDA_STD_TOP_REVIEWED_VALUE;
            this.DDA_STD_SETTLED_VALUE = source.DDA_STD_SETTLED_VALUE;
            this.DDA_STD_SAVING_VALUE = source.DDA_STD_SAVING_VALUE;
            this.DDA_DST_ORIGIN_CLAIM_VALUE = source.DDA_DST_ORIGIN_CLAIM_VALUE;
            this.DDA_DST_TOP_REVIEWED_VALUE = source.DDA_DST_TOP_REVIEWED_VALUE;
            this.DDA_DST_SETTLED_VALUE = source.DDA_DST_SETTLED_VALUE;
            this.DDA_DST_SAVING_VALUE = source.DDA_DST_SAVING_VALUE;
            this.DDA_ND_ORIGIN_CLAIM_VALUE = source.DDA_ND_ORIGIN_CLAIM_VALUE;
            this.DDA_ND_TOP_REVIEWED_VALUE = source.DDA_ND_TOP_REVIEWED_VALUE;
            this.DDA_ND_SETTLED_VALUE = source.DDA_ND_SETTLED_VALUE;
            this.DDA_ND_SAVING_VALUE = source.DDA_ND_SAVING_VALUE;
            this.DDA_EXPLANATION = source.DDA_EXPLANATION;
            this.DDA_TIME_ON_DEM_TEXT = source.DDA_TIME_ON_DEM_TEXT;
            this.DDA_TIME_ON_DEM_HRS = source.DDA_TIME_ON_DEM_HRS;
            this.DDA_NET_PAYMENT_DEM_VALUE = source.DDA_NET_PAYMENT_DEM_VALUE;
            this.DDA_BROKER_COMMISSION_VALUE = source.DDA_BROKER_COMMISSION_VALUE;
            this.DDA_EXCHANGE_RATE = source.DDA_EXCHANGE_RATE;
            this.DDA_STATUS = source.DDA_STATUS;
            this.DDA_CREATED = source.DDA_CREATED;
            this.DDA_CREATED_BY = source.DDA_CREATED_BY;
            this.DDA_UPDATED = source.DDA_UPDATED;
            this.DDA_UPDATED_BY = source.DDA_UPDATED_BY;
            this.DDA_AGREED_LAYCAN_FROM = source.DDA_AGREED_LAYCAN_FROM;
            this.DDA_AGREED_LAYCAN_TO = source.DDA_AGREED_LAYCAN_TO;
            this.DDA_MATERIAL_TYPE = source.DDA_MATERIAL_TYPE;
            this.DDA_REASON = source.DDA_REASON;
            this.DDA_SETTLEMENT_AMOUNT_THB = source.DDA_SETTLEMENT_AMOUNT_THB;
            this.DDA_IS_OVERRIDE_NET_PAYMENT = source.DDA_IS_OVERRIDE_NET_PAYMENT;
            this.DDA_CUSTOMER_BROKER = source.DDA_CUSTOMER_BROKER;
            this.DDA_NET_PAYMENT_DEM_VALUE_OV = source.DDA_NET_PAYMENT_DEM_VALUE_OV;
            this.DDA_LAYTIME_QUANTITY = source.DDA_LAYTIME_QUANTITY;
            this.DDA_LAYTIME_QUANTITY_RATE = source.DDA_LAYTIME_QUANTITY_RATE;

            #endregion

            this.IS_DISABLED = source.DDA_STATUS != "DRAFT";
            this.IS_DISABLED_NOTE = (source.DDA_STATUS == "APPROVED" || source.DDA_STATUS == "CANCEL");

            EntityCPAIEngine db = new EntityCPAIEngine();
            db.Configuration.ProxyCreationEnabled = false;

            this.DAF_ATTACH_FILE = db.DAF_ATTACH_FILE.Where(x => x.DAT_FK_DAF_DATA == source.DDA_ROW_ID).OrderBy(x => x.DAT_ROW_ID).ToList();
            this.DAF_MATERIAL = db.DAF_MATERIAL.Where(x => x.DMT_FK_DAF_DATA == source.DDA_ROW_ID).OrderBy(x => x.DMT_ROW_ID).ToList();
            this.DAF_PORT = db.DAF_PORT.Where(x => x.DPT_FK_DAF_DATA == source.DDA_ROW_ID).OrderBy(x => x.DPT_ROW_ID).ToList();
            
            foreach(DAF_PORT e in this.DAF_PORT)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                e.DAF_DEDUCTION_ACTIVITY = db2.DAF_DEDUCTION_ACTIVITY.Where(x => x.DDD_FK_DAF_PORT == e.DPT_ROW_ID).OrderBy(x => x.DDD_ROW_ID).ToList();
            }
        }
    }
}
