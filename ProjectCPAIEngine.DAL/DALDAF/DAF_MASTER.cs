﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class SELECT_CRITERIA
    {
        public string ID { get; set; }
        public string VALUE { get; set; }
    }

    public class DAF_CUSTOMER_CRITERIA : SELECT_CRITERIA
    {
        public string COMPANY_CODE { get; set; }
        public string PAYMENT_TERM_DETAIL { get; set; }
    }

    public class DAF_MASTER_CRITERIA : DAF_CUSTOMER_CRITERIA
    {
        public string MET_PRODUCT_DENSITY { get; set; }
        public string NAME { get; set; }
        public string GROUP { get; set; }
        public string DETAIL { get; set; }
        public string FK_MT_CUST { get; set; }
        public bool INCLUDED_FREIGHT { get; set; }
    }


    public class DAF_INCOTERM_CRITERIA : SELECT_CRITERIA
    {
        public bool INCLUDED_FREIGHT { get; set; }
    }

    public class DAF_JETTY_CRITERIA : SELECT_CRITERIA
    {
        public string CREATE_TYPE { get; set; }
    }

    class DAF_MASTER
    {
    }
}
