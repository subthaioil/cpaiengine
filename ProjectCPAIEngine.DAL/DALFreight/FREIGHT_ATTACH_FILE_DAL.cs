﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALFreight
{
    public class FREIGHT_ATTACH_FILE_DAL
    {
        public void Save(CPAI_FREIGHT_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_FREIGHT_ATTACH_FILE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_FREIGHT_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_FREIGHT_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_FREIGHT_ATTACH_FILE> GetFileByDataID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CPAI_FREIGHT_ATTACH_FILE.Where(x => x.FDAF_ROW_ID.ToUpper() == ID.ToUpper()).ToList();
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_FREIGHT_ATTACH_FILE> GetFileByDataID(string ID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CPAI_FREIGHT_ATTACH_FILE.Where(x => x.FDAF_ROW_ID.ToUpper() == ID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _choFile = _choFile.Where(x => x.FDAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
