﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALFreight
{
    public class FREIGHT_BUNKER_DAL
    {
        public void Save(CPAI_FREIGHT_BUNKER data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_FREIGHT_BUNKER.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_FREIGHT_BUNKER data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_FREIGHT_BUNKER.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CPAI_FREIGHT_BUNKER GetFBCDATAByID(string FCLID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _fclData = context.CPAI_FREIGHT_BUNKER.Where(x => x.FBC_FK_FREIGHT_DATA.ToUpper() == FCLID.ToUpper()).FirstOrDefault();
                    return _fclData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
