﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALFreight
{
    public class FREIGHT_CARGO_DAL
    {
        public void Save(CPAI_FREIGHT_CARGO data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_FREIGHT_CARGO.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_FREIGHT_CARGO data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_FREIGHT_CARGO.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_FREIGHT_CARGO> GetFBCDATAByID(string FCLID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _fclData = context.CPAI_FREIGHT_CARGO.Where(x => x.FDC_FK_FREIGHT_DATA.ToUpper() == FCLID.ToUpper()).ToList();
                    return _fclData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
