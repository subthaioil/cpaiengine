﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALFreight
{
    public class FREIGHT_PORT_DAL
    {
        public void Save(CPAI_FREIGHT_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_FREIGHT_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_FREIGHT_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_FREIGHT_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_FREIGHT_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.CPAI_FREIGHT_PORT.Find(data.FDP_ROW_ID);
                    #region Set Value
                    _search.FDP_PORT_TYPE = data.FDP_PORT_TYPE;
                    _search.FDP_VALUE = data.FDP_VALUE;
                    _search.FDP_UPDATED = data.FDP_UPDATED;
                    _search.FDP_UPDATED_BY = data.FDP_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_FREIGHT_PORT WHERE FDP_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_FREIGHT_PORT> GetFDPDATAByFCLID(string FCLID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _fclData = context.CPAI_FREIGHT_PORT.Where(x => x.FDP_FK_FREIGHT_DATA.ToUpper() == FCLID.ToUpper()).ToList();
                    return _fclData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Data CPAI_FREIGHT_DATA by Status
        /// </summary>
        /// <param name="Status">status ("NEW","SUBMIT") set string emply or nul get all</param>
        /// <returns></returns>
        //public static List<CPAI_FREIGHT_DATA> GetFreightData(string Status)
        //{

        //    using (EntityCPAIEngine context = new EntityCPAIEngine())
        //    {
        //        if (String.IsNullOrEmpty(Status) == false)
        //        {
        //            var query = (from f in context.CPAI_FREIGHT_DATA
        //                         where f.FDA_STATUS.ToUpper() == Status.ToUpper()
        //                         select f).Distinct();
        //            return query.ToList();

        //        }
        //        else
        //        {
        //            var query = (from f in context.CPAI_FREIGHT_DATA
        //                         select f).Distinct();
        //            return query.ToList();
        //        }

        //    }
        //}
    }
}
