﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_ANNUAL_FW_DAL
    {
        public void Save(HEDG_ANNUAL_FW data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_ANNUAL_FW.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_ANNUAL_FW data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_ANNUAL_FW.SingleOrDefault(p => p.HAF_ROW_ID == data.HAF_ROW_ID);
                #region Set Value
                _search.HAF_FK_MT_COMPANY = data.HAF_FK_MT_COMPANY;
                _search.HAF_APPROVED_DATE = data.HAF_APPROVED_DATE;
                _search.HAF_VERSION = data.HAF_VERSION;
                _search.HAF_NOTE = data.HAF_NOTE;
                _search.HAF_STATUS = data.HAF_STATUS;

                _search.HAF_UPDATED_BY = data.HAF_UPDATED_BY;
                _search.HAF_UPDATED_DATE = data.HAF_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_ANNUAL_FW WHERE HAF_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_ANNUAL_FW GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW.Where(x => x.HAF_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsLatestVersionByID(string id)
        {
            bool _LatestVersion = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW.Where(x => x.HAF_HISTORY_REF.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    if (_Data == null)
                    {
                        _LatestVersion = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _LatestVersion;
        }
    }
}
