﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_ANNUAL_FW_DETAIL_DAL
    {
        public void Save(HEDG_ANNUAL_FW_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_ANNUAL_FW_DETAIL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_ANNUAL_FW_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_ANNUAL_FW_DETAIL.SingleOrDefault(p => p.HAFD_ROW_ID == data.HAFD_ROW_ID);
                #region Set Value

                _search.HAFD_FK_ANNUAL_HEDGE_TYPE = data.HAFD_FK_ANNUAL_HEDGE_TYPE;
                _search.HAFD_FK_HEDG_PRODUCT_FRONT = data.HAFD_FK_HEDG_PRODUCT_FRONT;
                _search.HAFD_FK_HEDG_PRODUCT_BACK = data.HAFD_FK_HEDG_PRODUCT_BACK;
                _search.HAFD_VERSION = data.HAFD_VERSION;
                _search.HAFD_TITLE = data.HAFD_TITLE;
                _search.HAFD_UNIT_PRICE = data.HAFD_UNIT_PRICE;
                _search.HAFD_UNIT_VOLUME = data.HAFD_UNIT_VOLUME;
                _search.HAFD_YEAR = data.HAFD_YEAR;
                _search.HAFD_PRICE_YEAR = data.HAFD_PRICE_YEAR;
                _search.HAFD_PRICE_Q1 = data.HAFD_PRICE_Q1;
                _search.HAFD_PRICE_Q2 = data.HAFD_PRICE_Q2;
                _search.HAFD_PRICE_Q3 = data.HAFD_PRICE_Q3;
                _search.HAFD_PRICE_Q4 = data.HAFD_PRICE_Q4;
                _search.HAFD_PRODUCTION_VOLUME = data.HAFD_PRODUCTION_VOLUME;
                _search.HAFD_LIMIT_VOLUME = data.HAFD_LIMIT_VOLUME;
                _search.HAFD_APPROVED_VOLUME = data.HAFD_APPROVED_VOLUME;
                _search.HAFD_NOTE = data.HAFD_NOTE;
                _search.HAFD_MIN_MAX = data.HAFD_MIN_MAX;
                _search.HAFD_APPROVED_DATE = data.HAFD_APPROVED_DATE;

                _search.HAFD_UPDATED_BY = data.HAFD_UPDATED_BY;
                _search.HAFD_UPDATED_DATE = data.HAFD_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateHedgeType(HEDG_ANNUAL_FW_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_ANNUAL_FW_DETAIL.SingleOrDefault(p => p.HAFD_ROW_ID == data.HAFD_ROW_ID);
                #region Set Value

                _search.HAFD_FK_ANNUAL_HEDGE_TYPE = data.HAFD_FK_ANNUAL_HEDGE_TYPE;

                _search.HAFD_UPDATED_BY = data.HAFD_UPDATED_BY;
                _search.HAFD_UPDATED_DATE = data.HAFD_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_ANNUAL_FW_DETAIL WHERE HAFD_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveByID(string hedgTypeID, List<string> pk, EntityCPAIEngine context)
        {
            try
            {
                List<HEDG_ANNUAL_FW_DETAIL> details = context.HEDG_ANNUAL_FW_DETAIL.Where(x => x.HAFD_FK_ANNUAL_HEDGE_TYPE == hedgTypeID).ToList();
                if (details != null && details.Count > 0)
                {
                    for (int i = 0; i < details.Count; i++)
                    {
                        if (!pk.Contains(details[i].HAFD_ROW_ID))
                        {
                            HEDG_ANNUAL_FW_DETAIL detail = context.HEDG_ANNUAL_FW_DETAIL.Where(x => x.HAFD_ROW_ID == details[i].HAFD_ROW_ID).FirstOrDefault();
                            if (detail != null)
                                context.HEDG_ANNUAL_FW_DETAIL.Remove(detail);
                        }
                    }
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_ANNUAL_FW_DETAIL GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW_DETAIL.Where(x => x.HAFD_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_ANNUAL_FW_DETAIL> GetByHedgeTypeID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW_DETAIL.Where(x => x.HAFD_FK_ANNUAL_HEDGE_TYPE.Trim().ToUpper() == id.Trim().ToUpper()).OrderBy(x => x.HAFD_ORDER).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_ANNUAL_FW GetAnnualParentByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = (from d in context.HEDG_ANNUAL_FW_DETAIL where d.HAFD_ROW_ID == id 
                                join t in context.HEDG_ANNUAL_FW_HEDGE_TYPE
                                on d.HAFD_FK_ANNUAL_HEDGE_TYPE equals t.HAFH_ROW_ID
                                join h in context.HEDG_ANNUAL_FW
                                on t.HAFH_FK_HEDG_ANNUAL_FW equals h.HAF_ROW_ID select h).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsLatestVersionByID(string id)
        {
            bool _LatestVersion = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW_DETAIL.Where(x => x.HAFD_HISTORY_REF.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    if (_Data == null)
                    {
                        _LatestVersion = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _LatestVersion;
        }

    }
}
