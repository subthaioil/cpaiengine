﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_ANNUAL_FW_HEDGE_TYPE_DAL
    {
        public void Save(HEDG_ANNUAL_FW_HEDGE_TYPE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_ANNUAL_FW_HEDGE_TYPE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_ANNUAL_FW_HEDGE_TYPE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_ANNUAL_FW_HEDGE_TYPE.SingleOrDefault(p => p.HAFH_ROW_ID == data.HAFH_ROW_ID);
                #region Set Value
                _search.HAFH_ORDER = data.HAFH_ORDER;
                _search.HAFH_FK_HEDG_ANNUAL_FW = data.HAFH_FK_HEDG_ANNUAL_FW;
                _search.HAFH_FK_HEDG_MT_HEDGE_TYPE = data.HAFH_FK_HEDG_MT_HEDGE_TYPE;
                _search.HAFH_HEDGE_TYPE_NAME = data.HAFH_HEDGE_TYPE_NAME;

                _search.HAFH_UPDATED_BY = data.HAFH_UPDATED_BY;
                _search.HAFH_UPDATED_DATE = data.HAFH_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_ANNUAL_FW_HEDGE_TYPE WHERE HAFH_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_ANNUAL_FW_HEDGE_TYPE GetByAnnualAndTypeID(string annual_id, string type_id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW_HEDGE_TYPE.Where(x => x.HAFH_FK_HEDG_ANNUAL_FW.Trim().ToUpper() == annual_id.Trim().ToUpper() && x.HAFH_FK_HEDG_MT_HEDGE_TYPE.Trim().ToUpper() == type_id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_ANNUAL_FW_HEDGE_TYPE> GetByAnnualID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_FW_HEDGE_TYPE.Where(x => x.HAFH_FK_HEDG_ANNUAL_FW.Trim().ToUpper() == id.Trim().ToUpper()).OrderBy(x => x.HAFH_ORDER).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
