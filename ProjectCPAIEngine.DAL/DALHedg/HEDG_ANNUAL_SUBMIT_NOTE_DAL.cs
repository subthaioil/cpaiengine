﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_ANNUAL_SUBMIT_NOTE_DAL
    {
        public void Save(HEDG_ANNUAL_SUBMIT_NOTE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_ANNUAL_SUBMIT_NOTE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_ANNUAL_SUBMIT_NOTE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_ANNUAL_SUBMIT_NOTE.SingleOrDefault(p => p.HASN_ROW_ID == data.HASN_ROW_ID);
                #region Set Value
                _search.HASN_SUBJECT = data.HASN_SUBJECT;
                _search.HASN_APPROVED_DATE = data.HASN_APPROVED_DATE;
                _search.HASN_REF_NO = data.HASN_REF_NO;
                _search.HASN_NOTE = data.HASN_NOTE;

                _search.HASN_UPDATED_BY = data.HASN_UPDATED_BY;
                _search.HASN_UPDATED_DATE = data.HASN_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_ANNUAL_SUBMIT_NOTE WHERE HASN_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_ANNUAL_SUBMIT_NOTE GetByAnnualID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_ANNUAL_SUBMIT_NOTE.Where(x => x.HASN_FK_HEDG_ANNUAL.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
