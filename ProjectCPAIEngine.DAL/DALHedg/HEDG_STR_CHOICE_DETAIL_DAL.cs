﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CHOICE_DETAIL_DAL
    {
        public void Save(HEDG_STR_CHOICE_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CHOICE_DETAIL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_CHOICE_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_CHOICE_DETAIL.SingleOrDefault(p => p.HSCD_ROW_ID == data.HSCD_ROW_ID);
                #region Set Value
                _search.HSCD_FK_HEDG_STR_CHOICE_TYPE = data.HSCD_FK_HEDG_STR_CHOICE_TYPE;
                _search.HSCD_CHOICE_NAME = data.HSCD_CHOICE_NAME;
                _search.HSCD_PACKAGE_NAME = data.HSCD_PACKAGE_NAME;
                _search.HSCD_OPERATION = data.HSCD_OPERATION;
                _search.HSCD_NET_AF_PRICE = data.HSCD_NET_AF_PRICE;

                _search.HSCD_UPDATED_BY = data.HSCD_UPDATED_BY;
                _search.HSCD_UPDATED_DATE = data.HSCD_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CHOICE_DETAIL WHERE HSCD_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_CHOICE_DETAIL GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_DETAIL.Where(x => x.HSCD_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_CHOICE_DETAIL> GetBySTRCTID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_DETAIL.Where(x => x.HSCD_FK_HEDG_STR_CHOICE_TYPE.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
