﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CHOICE_PRODUCT_DAL
    {
        public void Save(HEDG_STR_CHOICE_PRODUCT data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CHOICE_PRODUCT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_CHOICE_PRODUCT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_CHOICE_PRODUCT.SingleOrDefault(p => p.HSCP_ROW_ID == data.HSCP_ROW_ID);
                #region Set Value
                _search.HSCP_FK_HEDG_STR_CHOICE_ROLE = data.HSCP_FK_HEDG_STR_CHOICE_ROLE;
                _search.HSCP_FK_HEDG_PRODUCT_FRONT = data.HSCP_FK_HEDG_PRODUCT_FRONT;
                _search.HSCP_FK_HEDG_PRODUCT_BACK = data.HSCP_FK_HEDG_PRODUCT_BACK;
                _search.HSCP_ORDER = data.HSCP_ORDER;
                _search.HSCP_AF_FRAME = data.HSCP_AF_FRAME;
                _search.HSCP_AF_PRICE = data.HSCP_AF_PRICE;
                _search.HSCP_UNIT_PRICE = data.HSCP_UNIT_PRICE;
                _search.HSCP_WEIGHT_PERCENT = data.HSCP_WEIGHT_PERCENT;
                _search.HSCP_WEIGHT_VOLUME = data.HSCP_WEIGHT_VOLUME;
                _search.HSCP_UNIT_VOLUME = data.HSCP_UNIT_VOLUME;
                //_search.HSCP_TYPE = data.HSCP_TYPE;

                _search.HSCP_UPDATED_BY = data.HSCP_UPDATED_BY;
                _search.HSCP_UPDATED_DATE = data.HSCP_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CHOICE_PRODUCT WHERE HSCP_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_CHOICE_PRODUCT GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_PRODUCT.Where(x => x.HSCP_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_CHOICE_PRODUCT> GetBySTRCRID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_PRODUCT.Where(x => x.HSCP_FK_HEDG_STR_CHOICE_ROLE.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
