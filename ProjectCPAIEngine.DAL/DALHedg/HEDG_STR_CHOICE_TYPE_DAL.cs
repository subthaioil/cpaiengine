﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CHOICE_TYPE_DAL
    {
        public void Save(HEDG_STR_CHOICE_TYPE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CHOICE_TYPE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_CHOICE_TYPE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_CHOICE_TYPE.SingleOrDefault(p => p.HSCT_ROW_ID == data.HSCT_ROW_ID);
                #region Set Value
                _search.HSCT_FK_HEDG_STR_CMT_FW = data.HSCT_FK_HEDG_STR_CMT_FW;
                _search.HSCT_TYPE = data.HSCT_TYPE;
                _search.HSCT_VOLUME = data.HSCT_VOLUME;

                _search.HSCT_UPDATED_BY = data.HSCT_UPDATED_BY;
                _search.HSCT_UPDATED_DATE = data.HSCT_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CHOICE_TYPE WHERE HSCT_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_CHOICE_TYPE GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_TYPE.Where(x => x.HSCT_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_CHOICE_TYPE> GetBySTRCMTID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_TYPE.Where(x => x.HSCT_FK_HEDG_STR_CMT_FW.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
