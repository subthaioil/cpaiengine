﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CMT_FW_DAL
    {
        public void Save(HEDG_STR_CMT_FW data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CMT_FW.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_CMT_FW data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_CMT_FW.SingleOrDefault(p => p.HSCF_ROW_ID == data.HSCF_ROW_ID);
                #region Set Value
                _search.HSCF_FK_MT_COMPANY = data.HSCF_FK_MT_COMPANY;
                _search.HSCF_FK_HEDG_MT_HEDGE_TYPE = data.HSCF_FK_HEDG_MT_HEDGE_TYPE;
                _search.HSCF_APPROVED_DATE = data.HSCF_APPROVED_DATE;
                _search.HSCF_CONSECUTIVE_FLAG = data.HSCF_CONSECUTIVE_FLAG;
                _search.HSCF_EXCEED_FLAG = data.HSCF_EXCEED_FLAG;
                _search.HSCF_HISTORY_REF = data.HSCF_HISTORY_REF;
                _search.HSCF_INSTRUMENT = data.HSCF_INSTRUMENT;
                _search.HSCF_M1_M_FLAG = data.HSCF_M1_M_FLAG;
                _search.HSCF_NOTE = data.HSCF_NOTE;
                _search.HSCF_REF_NO = data.HSCF_REF_NO;
                _search.HSCF_STATUS = data.HSCF_STATUS;
                _search.HSCF_TEMPLATE = data.HSCF_TEMPLATE;
                //_search.HSCF_TENOR_PERIOD_FROM = data.HSCF_TENOR_PERIOD_FROM;
                //_search.HSCF_TENOR_PERIOD_TO = data.HSCF_TENOR_PERIOD_TO;
                _search.HSCF_TIME_SPREAD_FLAG = data.HSCF_TIME_SPREAD_FLAG;
                _search.HSCF_VERSION = data.HSCF_VERSION;

                _search.HSCF_UPDATED_BY = data.HSCF_UPDATED_BY;
                _search.HSCF_UPDATED_DATE = data.HSCF_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CMT_FW WHERE HSCF_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_CMT_FW GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CMT_FW.Where(x => x.HSCF_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
