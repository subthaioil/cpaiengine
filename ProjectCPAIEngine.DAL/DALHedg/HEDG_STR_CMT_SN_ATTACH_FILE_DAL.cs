﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CMT_SN_ATTACH_FILE_DAL
    {
        public void Save(HEDG_STR_CMT_SN_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CMT_SN_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CMT_SN_ATTACH_FILE WHERE HSSF_FK_HEDG_STR_CMT_SN = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_CMT_SN_ATTACH_FILE> GetFileByDataID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _hedgFile = context.HEDG_STR_CMT_SN_ATTACH_FILE.Where(x => x.HSSF_FK_HEDG_STR_CMT_SN.ToUpper() == ID.ToUpper()).ToList();
                    return _hedgFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
