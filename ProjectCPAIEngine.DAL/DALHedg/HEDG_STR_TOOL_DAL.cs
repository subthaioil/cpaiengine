﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_TOOL_DAL
    {
        public void Save(HEDG_STR_TOOL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_TOOL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_TOOL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_TOOL.SingleOrDefault(p => p.HST_ROW_ID == data.HST_ROW_ID);
                #region Set Value
                //_search.HST_FK_HEDG_STR_CMT_FW = data.HST_FK_HEDG_STR_CMT_FW;
                _search.HST_ORDER = data.HST_ORDER;
                _search.HST_NAME = data.HST_NAME;
                _search.HST_USE_NET_PREMIUM = data.HST_USE_NET_PREMIUM;
                _search.HST_USE_FORMULA = data.HST_USE_FORMULA;
                //_search.HST_NET_PREMIUM_TYPE = data.HST_NET_PREMIUM_TYPE;
                //_search.HST_NET_PREMIUM_VOLUME = data.HST_NET_PREMIUM_VOLUME;

                _search.HST_UPDATED_BY = data.HST_UPDATED_BY;
                _search.HST_UPDATED_DATE = data.HST_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_TOOL WHERE HST_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_TOOL GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL.Where(x => x.HST_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_TOOL> GetBySTRCMTID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL/*.Where(x => x.HST_FK_HEDG_STR_CMT_FW.Trim().ToUpper() == id.Trim().ToUpper())*/.ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
