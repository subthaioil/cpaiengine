﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_TOOL_FORMULA_DAL
    {
        public void Save(HEDG_STR_TOOL_FORMULA data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_TOOL_FORMULA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_TOOL_FORMULA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_TOOL_FORMULA.SingleOrDefault(p => p.HSTF_ROW_ID == data.HSTF_ROW_ID);
                #region Set Value
                _search.HSTF_FK_HEDG_STR_TOOL = data.HSTF_FK_HEDG_STR_TOOL;
                _search.HSTF_ORDER = data.HSTF_ORDER;
                _search.HSTF_TYPE = data.HSTF_TYPE;
                _search.HSTF_VALUE = data.HSTF_VALUE;

                _search.HSTF_UPDATED_BY = data.HSTF_UPDATED_BY;
                _search.HSTF_UPDATED_DATE = data.HSTF_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_TOOL_FORMULA WHERE HSTF_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_TOOL_FORMULA GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_FORMULA.Where(x => x.HSTF_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_TOOL_FORMULA> GetBySTRToolID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_FORMULA.Where(x => x.HSTF_FK_HEDG_STR_TOOL.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
