﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_TOOL_KIO_OPTION_DAL
    {
        public void Save(HEDG_STR_TOOL_KIO_OPTION data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_TOOL_KIO_OPTION.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_TOOL_KIO_OPTION data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_TOOL_KIO_OPTION.SingleOrDefault(p => p.HSTK_ROW_ID == data.HSTK_ROW_ID);
                #region Set Value
                _search.HSTK_FK_HEDG_STR_TOOL_OPTION = data.HSTK_FK_HEDG_STR_TOOL_OPTION;
                _search.HSTK_OPTION = data.HSTK_OPTION;
                _search.HSTK_ACTIVATED_OPTION = data.HSTK_ACTIVATED_OPTION;
                _search.HSTK_ACTIVATED_OPERATOR = data.HSTK_ACTIVATED_OPERATOR;
                _search.HSTK_HEDGE_PRICE = data.HSTK_HEDGE_PRICE;

                _search.HSTK_UPDATED_BY = data.HSTK_UPDATED_BY;
                _search.HSTK_UPDATED_DATE = data.HSTK_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_TOOL_KIO_OPTION WHERE HSTK_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_TOOL_KIO_OPTION GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_KIO_OPTION.Where(x => x.HSTK_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_TOOL_KIO_OPTION> GetBySTRTOID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_KIO_OPTION.Where(x => x.HSTK_FK_HEDG_STR_TOOL_OPTION.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
