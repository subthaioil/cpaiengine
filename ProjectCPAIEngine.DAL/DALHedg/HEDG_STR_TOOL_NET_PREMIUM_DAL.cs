﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_TOOL_NET_PREMIUM_DAL
    {
        public void Save(HEDG_STR_TOOL_NET_PREMIUM data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_TOOL_NET_PREMIUM.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_TOOL_NET_PREMIUM data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_TOOL_NET_PREMIUM.SingleOrDefault(p => p.HSTN_ROW_ID == data.HSTN_ROW_ID);
                #region Set Value
                _search.HSTN_FK_HEDG_STR_TOOL = data.HSTN_FK_HEDG_STR_TOOL;
                _search.HSTN_ORDER = data.HSTN_ORDER;
                _search.HSTN_OPTION = data.HSTN_OPTION;

                _search.HSTN_UPDATED_BY = data.HSTN_UPDATED_BY;
                _search.HSTN_UPDATED_DATE = data.HSTN_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_TOOL_NET_PREMIUM WHERE HSTN_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_TOOL_NET_PREMIUM GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_NET_PREMIUM.Where(x => x.HSTN_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_TOOL_NET_PREMIUM> GetBySTRToolID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_NET_PREMIUM.Where(x => x.HSTN_FK_HEDG_STR_TOOL.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
