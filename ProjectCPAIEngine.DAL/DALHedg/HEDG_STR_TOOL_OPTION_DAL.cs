﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_TOOL_OPTION_DAL
    {
        public void Save(HEDG_STR_TOOL_OPTION data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_TOOL_OPTION.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_TOOL_OPTION data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_TOOL_OPTION.SingleOrDefault(p => p.HSTO_ROW_ID == data.HSTO_ROW_ID);
                #region Set Value
                _search.HSTO_FK_HEDG_STR_TOOL = data.HSTO_FK_HEDG_STR_TOOL;
                _search.HSTO_FK_HEDG_MT_PROD = data.HSTO_FK_HEDG_MT_PROD;
                //_search.HSTO_CAPPED_VALUE = data.HSTO_CAPPED_VALUE;
                _search.HSTO_EXCERCISE_DATE = data.HSTO_EXCERCISE_DATE;
                //_search.HSTO_GAIN_LOSS = data.HSTO_GAIN_LOSS;
                _search.HSTO_KNOCK_IN_OUT_BY = data.HSTO_KNOCK_IN_OUT_BY;
                _search.HSTO_VALUE_PER_UNIT = data.HSTO_VALUE_PER_UNIT;

                _search.HSTO_UPDATED_BY = data.HSTO_UPDATED_BY;
                _search.HSTO_UPDATED_DATE = data.HSTO_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_TOOL_OPTION WHERE HSTO_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_TOOL_OPTION GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_OPTION.Where(x => x.HSTO_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_TOOL_OPTION> GetBySTRToolID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_OPTION.Where(x => x.HSTO_FK_HEDG_STR_TOOL.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
