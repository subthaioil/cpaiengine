﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class CPAI_DELEGATE_AUTH_DAL
    {
        public static List<string> GetDelegateSystem(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.CPAI_DELEGATE_AUTH
                            where v.DLG_STATUS == "ACTIVE" && v.DLG_FK_USERS == id
                            select v.DLG_USER_SYSTEM;
                return query.Distinct().ToList(); 
            }
        }

        public static List<string> GetDelegateGroup(string id, string system)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.CPAI_USER_GROUP
                            where v.USG_STATUS == "ACTIVE" && v.USG_FK_USERS == id && v.USG_USER_SYSTEM == system
                            select v.USG_USER_GROUP;
                return query.ToList(); 
            }
        }
    }
}
