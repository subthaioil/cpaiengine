﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class CPAI_USER_GROUP_DAL
    {
        public void Save(CPAI_USER_GROUP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_USER_GROUP.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_USER_GROUP data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_USER_GROUP.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_USER_GROUP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.CPAI_USER_GROUP.Find(data.USG_ROW_ID);
                    #region Set Value
                    _search.USG_USER_GROUP = data.USG_USER_GROUP;
                    _search.USG_USER_SYSTEM = data.USG_USER_SYSTEM;
                    _search.USG_STATUS = data.USG_STATUS;
                    _search.USG_STATUS = data.USG_STATUS;
                    _search.USG_UPDATED_BY = data.USG_UPDATED_BY;
                    _search.USG_UPDATED_DATE = data.USG_UPDATED_DATE;
                    _search.USG_AFFECT_DATE = data.USG_AFFECT_DATE;
                    _search.USG_END_DATE = data.USG_END_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_USER_GROUP data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.CPAI_USER_GROUP.Find(data.USG_ROW_ID);
                #region Set Value
                _search.USG_USER_GROUP = data.USG_USER_GROUP;
                _search.USG_USER_SYSTEM = data.USG_USER_SYSTEM;
                _search.USG_STATUS = data.USG_STATUS;
                _search.USG_STATUS = data.USG_STATUS;
                _search.USG_UPDATED_BY = data.USG_UPDATED_BY;
                _search.USG_UPDATED_DATE = data.USG_UPDATED_DATE;
                _search.USG_AFFECT_DATE = data.USG_AFFECT_DATE;
                _search.USG_END_DATE = data.USG_END_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_USER_GROUP WHERE USG_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM CPAI_USER_GROUP WHERE USG_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByUserID(string UserID, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM CPAI_USER_GROUP WHERE USG_FK_USERS = '" + UserID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByUserIDAndSystemGroup(string UserID, string System, string Group, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM CPAI_USER_GROUP WHERE USG_FK_USERS = '" + UserID + "' AND USG_USER_SYSTEM = '" + System + "' AND USG_DELEGATE_FLAG = 'Y'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
