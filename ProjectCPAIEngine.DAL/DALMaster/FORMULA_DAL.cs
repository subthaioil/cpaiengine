﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class FORMULA_DAL
    {
        public static List<FORMULA> GetFormula()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.FORMULA
                            join vc in context.MT_MATERIALS on v.FOR_MET_NUM equals vc.MET_NUM
                            where v.FOR_PRICE_STATUS.ToUpper() == "FINAL"
                            select v;
                return query.ToList();
            }
        }

        public static FORMULA GetFormula(string met_num)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.FORMULA
                            join vc in context.MT_MATERIALS on v.FOR_MET_NUM equals vc.MET_NUM
                            where v.FOR_PRICE_STATUS.ToUpper() == "FINAL" && vc.MET_NUM == met_num
                            select v;
                return query.FirstOrDefault();
            }
        }
    }
}
