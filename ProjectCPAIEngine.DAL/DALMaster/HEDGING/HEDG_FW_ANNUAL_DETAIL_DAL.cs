﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_FW_ANNUAL_DETAIL_DAL
    {
        public void Save(HEDG_FW_ANNUAL_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_FW_ANNUAL_DETAIL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_FW_ANNUAL_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_FW_ANNUAL_DETAIL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_ANNUAL_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_FW_ANNUAL_DETAIL.SingleOrDefault(p => p.HFTD_ROW_ID == data.HFTD_ROW_ID && p.HFTD_FK_FW_ANNUAL == data.HFTD_FK_FW_ANNUAL);
                    #region Set Value
                    _search.HFTD_ORDER = data.HFTD_ORDER;
                    _search.HFTD_HEDGE_TYPE = data.HFTD_HEDGE_TYPE;
                    _search.HFTD_TYPE = data.HFTD_TYPE;
                    _search.HFTD_TITLE = data.HFTD_TITLE;
                    _search.HFTD_VALUE_TYPE = data.HFTD_VALUE_TYPE;
                    _search.HFTD_VOLUME = data.HFTD_VOLUME;
                    _search.HFTD_VOLUME_UNIT = data.HFTD_VOLUME_UNIT;
                    _search.HFTD_PRICE1 = data.HFTD_PRICE1;
                    _search.HFTD_PRICE1_REMARK = data.HFTD_PRICE1_REMARK;
                    _search.HFTD_PRICE2 = data.HFTD_PRICE2;
                    _search.HFTD_PRICE2_REMARK = data.HFTD_PRICE2_REMARK;
                    _search.HFTD_PRICE3 = data.HFTD_PRICE3;
                    _search.HFTD_PRICE3_REMARK = data.HFTD_PRICE3_REMARK;
                    _search.HFTD_PRICE_UNIT = data.HFTD_PRICE_UNIT;
                    _search.HFTD_UPDATED_BY = data.HFTD_UPDATED_BY;
                    _search.HFTD_UPDATED_DATE = data.HFTD_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_ANNUAL_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_FW_ANNUAL_DETAIL.SingleOrDefault(p => p.HFTD_ROW_ID == data.HFTD_ROW_ID && p.HFTD_FK_FW_ANNUAL == data.HFTD_FK_FW_ANNUAL);
                #region Set Value
                _search.HFTD_ORDER = data.HFTD_ORDER;
                _search.HFTD_HEDGE_TYPE = data.HFTD_HEDGE_TYPE;
                _search.HFTD_TYPE = data.HFTD_TYPE;
                _search.HFTD_TITLE = data.HFTD_TITLE;
                _search.HFTD_VALUE_TYPE = data.HFTD_VALUE_TYPE;
                _search.HFTD_VOLUME = data.HFTD_VOLUME;
                _search.HFTD_VOLUME_UNIT = data.HFTD_VOLUME_UNIT;
                _search.HFTD_PRICE1 = data.HFTD_PRICE1;
                _search.HFTD_PRICE1_REMARK = data.HFTD_PRICE1_REMARK;
                _search.HFTD_PRICE2 = data.HFTD_PRICE2;
                _search.HFTD_PRICE2_REMARK = data.HFTD_PRICE2_REMARK;
                _search.HFTD_PRICE3 = data.HFTD_PRICE3;
                _search.HFTD_PRICE3_REMARK = data.HFTD_PRICE3_REMARK;
                _search.HFTD_PRICE_UNIT = data.HFTD_PRICE_UNIT;
                _search.HFTD_UPDATED_BY = data.HFTD_UPDATED_BY;
                _search.HFTD_UPDATED_DATE = data.HFTD_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_FW_ANNUAL_DETAIL WHERE HFTD_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByAnnualID(string HFTD_FK_FW_ANNUAL, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_FW_ANNUAL_DETAIL WHERE HFTD_FK_FW_ANNUAL = '" + HFTD_FK_FW_ANNUAL + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
