﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_FW_COMT_DAL
    {
        public void Save(HEDG_FW_COMT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_FW_COMT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_FW_COMT data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_FW_COMT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_COMT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_FW_COMT.Find(data.HFC_ROW_ID);
                    #region Set Value                    
                    //_search.HFC_ACTIVEDATE_START = data.HFC_ACTIVEDATE_START;
                    //_search.HFC_ACTIVEDATE_END = data.HFC_ACTIVEDATE_END;
                    //_search.HFC_FK_UNDERLYING = data.HFC_FK_UNDERLYING;
                    //_search.HFC_CROP_PLAN = data.HFC_CROP_PLAN;
                    _search.HFC_PERCENT_PRD = data.HFC_PERCENT_PRD;
                    //_search.HFC_UNIT_PRICE = data.HFC_UNIT_PRICE;
                    //_search.HFC_UNIT_VOLUME = data.HFC_UNIT_VOLUME;
                    _search.HFC_UPDATED_BY = data.HFC_UPDATED_BY;
                    _search.HFC_UPDATED_DATE = data.HFC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_COMT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_FW_COMT.Find(data.HFC_ROW_ID);
                #region Set Value                
                //_search.HFC_ACTIVEDATE_START = data.HFC_ACTIVEDATE_START;
                //_search.HFC_ACTIVEDATE_END = data.HFC_ACTIVEDATE_END;
                //_search.HFC_FK_UNDERLYING = data.HFC_FK_UNDERLYING;
                //_search.HFC_CROP_PLAN = data.HFC_CROP_PLAN;
                _search.HFC_PERCENT_PRD = data.HFC_PERCENT_PRD;
                //_search.HFC_UNIT_PRICE = data.HFC_UNIT_PRICE;
                //_search.HFC_UNIT_VOLUME = data.HFC_UNIT_VOLUME;
                _search.HFC_UPDATED_BY = data.HFC_UPDATED_BY;
                _search.HFC_UPDATED_DATE = data.HFC_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
