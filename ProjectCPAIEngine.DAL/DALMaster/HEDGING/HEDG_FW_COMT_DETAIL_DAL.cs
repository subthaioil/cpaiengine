﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_FW_COMT_DETAIL_DAL
    {
        public void Save(HEDG_FW_COMT_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_FW_COMT_DETAIL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_FW_COMT_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_FW_COMT_DETAIL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_COMT_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_FW_COMT_DETAIL.SingleOrDefault(p => p.HFCD_ROW_ID == data.HFCD_ROW_ID && p.HFCD_FK_COMMITTEE == data.HFCD_FK_COMMITTEE);
                    #region Set Value                    
                    _search.HFCD_ORDER = data.HFCD_ORDER;
                    _search.HFCD_PRICE = data.HFCD_PRICE;
                    _search.HFCD_VOLUME = data.HFCD_VOLUME;
                    _search.HFCD_UPDATED_BY = data.HFCD_UPDATED_BY;
                    _search.HFCD_UPDATED_DATE = data.HFCD_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_COMT_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_FW_COMT_DETAIL.SingleOrDefault(p => p.HFCD_ROW_ID == data.HFCD_ROW_ID && p.HFCD_FK_COMMITTEE == data.HFCD_FK_COMMITTEE);
                #region Set Value
                _search.HFCD_ORDER = data.HFCD_ORDER;
                _search.HFCD_PRICE = data.HFCD_PRICE;
                _search.HFCD_VOLUME = data.HFCD_VOLUME;
                _search.HFCD_UPDATED_BY = data.HFCD_UPDATED_BY;
                _search.HFCD_UPDATED_DATE = data.HFCD_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(HEDG_FW_COMT_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_FW_COMT_DETAIL.SingleOrDefault(p => p.HFCD_ROW_ID == data.HFCD_ROW_ID && p.HFCD_FK_COMMITTEE == data.HFCD_FK_COMMITTEE);
                    if (_search != null)
                    {
                        context.HEDG_FW_COMT_DETAIL.Remove(_search);
                        context.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(HEDG_FW_COMT_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_FW_COMT_DETAIL.SingleOrDefault(p => p.HFCD_ROW_ID == data.HFCD_ROW_ID && p.HFCD_FK_COMMITTEE == data.HFCD_FK_COMMITTEE);
                if (_search != null)
                {
                    context.HEDG_FW_COMT_DETAIL.Remove(_search);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }  

        public void DeleteByFK(string HFCD_FK_COMMITTEE, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_FW_COMT_DETAIL WHERE HFCD_FK_COMMITTEE = '" + HFCD_FK_COMMITTEE + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
