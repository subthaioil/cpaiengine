﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_HIS_CP_CDS_DAL
    {
        public void Save(HEDG_HIS_CP_CDS data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_HIS_CP_CDS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_HIS_CP_CDS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_HIS_CP_CDS.Find(data.HICD_ROW_ID);
                #region Set Value
                _search.HICD_FK_HEDG_CP = data.HICD_FK_HEDG_CP;
                _search.HICD_FK_HEDG_CDS_FILE = data.HICD_FK_HEDG_CDS_FILE;
                _search.HICD_DATE = data.HICD_DATE;
                _search.HICD_CDS = data.HICD_CDS;
                _search.HICD_STATUS = data.HICD_STATUS;
                _search.HICD_CREATED_DATE = data.HICD_CREATED_DATE;
                _search.HICD_CREATED_BY = data.HICD_CREATED_BY;
                _search.HICD_UPDATED_DATE = data.HICD_UPDATED_DATE;
                _search.HICD_UPDATED_BY = data.HICD_UPDATED_BY;
                _search.HICD_HISTORY_REASON = data.HICD_HISTORY_REASON;
                _search.HICD_FK_HEDG_CDS = data.HICD_FK_HEDG_CDS;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_HIS_CP_CDS WHERE HICD_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_HIS_CP_CDS where r.HICD_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_HIS_CP_CDS.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
