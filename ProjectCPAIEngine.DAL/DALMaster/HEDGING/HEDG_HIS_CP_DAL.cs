﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_HIS_CP_DAL
    {
        public void Save(HEDG_HIS_CP data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_HIS_CP.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_HIS_CP data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_HIS_CP.Find(data.HIC_ROW_ID);
                #region Set Value
                _search.HIC_FK_VENDOR = data.HIC_FK_VENDOR;
                _search.HIC_FK_HEDG_CDS_FILE = data.HIC_FK_HEDG_CDS_FILE;
                _search.HIC_CREDIT_RATING = data.HIC_CREDIT_RATING;
                _search.HIC_CREDIT_LIMIT = data.HIC_CREDIT_LIMIT;
                _search.HIC_STATUS = data.HIC_STATUS;
                _search.HIC_REASON = data.HIC_REASON;
                _search.HIC_CREATED_DATE = data.HIC_CREATED_DATE;
                _search.HIC_CREATED_BY = data.HIC_CREATED_BY;
                _search.HIC_UPDATED_DATE = data.HIC_UPDATED_DATE;
                _search.HIC_UPDATED_BY = data.HIC_UPDATED_BY;
                _search.HIC_HISTORY_REASON = data.HIC_HISTORY_REASON;
                _search.HIC_FK_HEDG_CP = data.HIC_FK_HEDG_CP;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_HIS_CP WHERE HMF_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_HIS_CP where r.HIC_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_HIS_CP.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
