﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_HIS_CP_LIMIT_DAL
    {
        public void Save(HEDG_HIS_CP_LIMIT data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_HIS_CP_LIMIT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_HIS_CP_LIMIT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_HIS_CP_LIMIT.Find(data.HICL_ROW_ID);
                #region Set Value
                _search.HICL_FK_HEDG_CP = data.HICL_FK_HEDG_CP;
                _search.HICL_DATE_START = data.HICL_DATE_START;
                _search.HICL_DATE_END = data.HICL_DATE_END;
                _search.HICL_CREDIT_LIMIT = data.HICL_CREDIT_LIMIT;
                _search.HICL_STATUS = data.HICL_STATUS;
                _search.HICL_CREATED_DATE = data.HICL_CREATED_DATE;
                _search.HICL_CREATED_BY = data.HICL_CREATED_BY;
                _search.HICL_UPDATED_DATE = data.HICL_UPDATED_DATE;
                _search.HICL_UPDATED_BY = data.HICL_UPDATED_BY;
                _search.HICL_HISTORY_REASON = data.HICL_HISTORY_REASON;
                _search.HICL_FK_HEDG_CP_LIMIT = data.HICL_FK_HEDG_CP_LIMIT;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_HIS_CP_LIMIT WHERE HICL_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_HIS_CP_LIMIT where r.HICL_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_HIS_CP_LIMIT.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
