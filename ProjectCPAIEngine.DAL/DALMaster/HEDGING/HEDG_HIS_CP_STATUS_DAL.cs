﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_HIS_CP_STATUS_DAL
    {
        public void Save(HEDG_HIS_CP_STATUS data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_HIS_CP_STATUS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_HIS_CP_STATUS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_HIS_CP_STATUS.Find(data.HICS_ROW_ID);
                #region Set Value
                _search.HICS_FK_HEDG_CP = data.HICS_FK_HEDG_CP;
                _search.HICS_STATUS = data.HICS_STATUS;
                _search.HICS_REASON = data.HICS_REASON;
                _search.HICS_CREATED_DATE = data.HICS_CREATED_DATE;
                _search.HICS_CREATED_BY = data.HICS_CREATED_BY;
                _search.HICS_UPDATED_DATE = data.HICS_UPDATED_DATE;
                _search.HICS_UPDATED_BY = data.HICS_UPDATED_BY;
                _search.HICS_HISTORY_REASON = data.HICS_HISTORY_REASON;
                _search.HICS_FK_HEDG_CP_STATUS = data.HICS_FK_HEDG_CP_STATUS;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_HIS_CP_STATUS WHERE HICS_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_HIS_CP_STATUS where r.HICS_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_HIS_CP_STATUS.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
