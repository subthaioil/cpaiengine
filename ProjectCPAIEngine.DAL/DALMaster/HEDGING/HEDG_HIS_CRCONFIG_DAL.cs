﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_HIS_CRCONFIG_DAL
    {
        public void Save(HEDG_HIS_CRCONFIG data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_HIS_CRCONFIG.Add(data);
                    context.SaveChanges();
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_HIS_CRCONFIG data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_HIS_CRCONFIG.Add(data);
                context.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}
