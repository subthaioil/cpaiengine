﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CDS_FILE_DAL
    {
        public void Save(HEDG_MT_CDS_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CDS_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CDS_FILE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CDS_FILE.Find(data.HMF_ROW_ID);
                #region Set Value
                _search.HMF_FILE_NAME = data.HMF_FILE_NAME;
                _search.HMF_FILE_PATH = data.HMF_FILE_PATH;
                _search.HMF_CREATED_DATE = data.HMF_CREATED_DATE;
                _search.HMF_CREATED_BY = data.HMF_CREATED_BY;
                _search.HMF_UPDATED_DATE = data.HMF_UPDATED_DATE;
                _search.HMF_UPDATED_BY = data.HMF_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CDS_FILE WHERE HMF_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CDS_FILE where r.HMF_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CDS_FILE.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
