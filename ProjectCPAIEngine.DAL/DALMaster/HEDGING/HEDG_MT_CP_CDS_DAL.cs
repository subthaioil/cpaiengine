﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_CDS_DAL
    {
        public void Save(HEDG_MT_CP_CDS data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP_CDS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP_CDS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP_CDS.Find(data.HMCD_ROW_ID);
                #region Set Value
                _search.HMCD_FK_HEDG_CP = data.HMCD_FK_HEDG_CP;
                _search.HMCD_FK_HEDG_CDS_FILE = data.HMCD_FK_HEDG_CDS_FILE;
                _search.HMCD_DATE = data.HMCD_DATE;
                _search.HMCD_CDS = data.HMCD_CDS;
                _search.HMCD_STATUS = data.HMCD_STATUS;
                _search.HMCD_UPDATED_DATE = data.HMCD_UPDATED_DATE;
                _search.HMCD_UPDATED_BY = data.HMCD_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP_CDS WHERE HMCD_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP_CDS where r.HMCD_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP_CDS.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //1. check CDS1_IN | CDS1_IN_DAY2 >> unlock 100%                 >> CDS <= CDS1_IN | CDS1_IN_DAY2
        //2. check CDS1_IN | CDS1_IN_DAY1 >> unlock 50%                  >> CDS <= CDS1_IN | CDS1_IN_DAY1
        public static Boolean CheckUnlock(string HEDG_CP,int CDS_IN_DAY, Decimal CDS_IN)
        {
            Boolean resp = false;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP_CDS
                                 where v.HMCD_FK_HEDG_CP.ToUpper() == HEDG_CP.ToUpper()
                                 && v.HMCD_STATUS == "ACTIVE"
                                 orderby v.HMCD_DATE descending
                                 select v).Take(CDS_IN_DAY);
                    var result = query.ToList();
                    int count = (from a in result
                                 where Convert.ToDecimal(a.HMCD_CDS) <= CDS_IN
                                 select a).Count();
                    if (count == CDS_IN_DAY)
                    {
                        resp = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }

        //1. check CDS2_OUT | CDS2_OUT_DAY >> lock                       >> CDS > CDS2_OUT | CDS2_OUT_DAY
        //2. check CDS1_OUT | CDS1_OUT_DAY >> send Notice to TRCR        >> CDS > CDS1_OUT | CDS1_OUT_DAY
        public static Boolean CheckLock(string HEDG_CP, int CDS_OUT_DAY, Decimal CDS_OUT)
        {
            Boolean resp = false;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP_CDS
                                 where v.HMCD_FK_HEDG_CP.ToUpper() == HEDG_CP.ToUpper()
                                 && v.HMCD_STATUS == "ACTIVE"
                                 orderby v.HMCD_DATE descending
                                 select v).Take(CDS_OUT_DAY);
                    var result = query.ToList();
                    int count = (from a in result
                                 where Convert.ToDecimal(a.HMCD_CDS) > CDS_OUT
                                 select a).Count();
                    if (count == CDS_OUT_DAY)
                    {
                        resp = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }



    }
}
