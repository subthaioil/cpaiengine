﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_DAL
    {
        public void Save(HEDG_MT_CP data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP.Find(data.HMC_ROW_ID);
                #region Set Value
                _search.HMC_FK_VENDOR = data.HMC_FK_VENDOR;
                _search.HMC_FK_HEDG_CDS_FILE = data.HMC_FK_HEDG_CDS_FILE;
                _search.HMC_CREDIT_RATING = data.HMC_CREDIT_RATING;
                _search.HMC_CREDIT_LIMIT = data.HMC_CREDIT_LIMIT;
                _search.HMC_STATUS = data.HMC_STATUS;
                _search.HMC_REASON = data.HMC_REASON;
                _search.HMC_UPDATED_DATE = data.HMC_UPDATED_DATE;
                _search.HMC_UPDATED_BY = data.HMC_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP WHERE HMF_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP where r.HMC_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static HEDG_MT_CP GetMtCp(string vendor)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_MT_CP
                            where v.HMC_FK_VENDOR.ToUpper() == vendor.ToUpper()
                            select v).FirstOrDefault();
                return query;
            }
        }
    }
}
