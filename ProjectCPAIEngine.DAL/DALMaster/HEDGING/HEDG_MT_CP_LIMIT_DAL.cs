﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_LIMIT_DAL
    {
        public void Save(HEDG_MT_CP_LIMIT data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP_LIMIT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP_LIMIT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP_LIMIT.Find(data.HMCL_ROW_ID);
                #region Set Value
                _search.HMCL_FK_HEDG_CP = data.HMCL_FK_HEDG_CP;
                _search.HMCL_DATE_START = data.HMCL_DATE_START;
                _search.HMCL_DATE_END = data.HMCL_DATE_END;
                _search.HMCL_CREDIT_LIMIT = data.HMCL_CREDIT_LIMIT;
                _search.HMCL_STATUS = data.HMCL_STATUS;
                _search.HMCL_UPDATED_DATE = data.HMCL_UPDATED_DATE;
                _search.HMCL_UPDATED_BY = data.HMCL_UPDATED_BY;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP_LIMIT WHERE HMCL_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP_LIMIT where r.HMCL_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP_LIMIT.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
