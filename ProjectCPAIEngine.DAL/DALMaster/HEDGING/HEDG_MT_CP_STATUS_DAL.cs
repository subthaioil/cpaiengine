﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_STATUS_DAL
    {

        public void Save(HEDG_MT_CP_STATUS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_MT_CP_STATUS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_MT_CP_STATUS data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP_STATUS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_MT_CP_STATUS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_MT_CP_STATUS.Find(data.HMCS_ROW_ID);
                    #region Set Value                    
                    _search.HMCS_FK_HEDG_CP = data.HMCS_FK_HEDG_CP;
                    _search.HMCS_STATUS = data.HMCS_STATUS;
                    _search.HMCS_REASON = data.HMCS_REASON;
                    _search.HMCS_UPDATED_BY = data.HMCS_UPDATED_BY;
                    _search.HMCS_UPDATED_DATE = data.HMCS_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP_STATUS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP_STATUS.Find(data.HMCS_ROW_ID);
                #region Set Value                
                _search.HMCS_FK_HEDG_CP = data.HMCS_FK_HEDG_CP;
                _search.HMCS_STATUS = data.HMCS_STATUS;
                _search.HMCS_REASON = data.HMCS_REASON;
                _search.HMCS_UPDATED_BY = data.HMCS_UPDATED_BY;
                _search.HMCS_UPDATED_DATE = data.HMCS_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
