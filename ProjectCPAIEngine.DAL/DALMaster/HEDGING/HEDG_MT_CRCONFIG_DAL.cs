﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CRCONFIG_DAL
    {
        public void Save(HEDG_MT_CRCONFIG data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_MT_CRCONFIG.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_MT_CRCONFIG data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CRCONFIG.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CRCONFIG data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_MT_CRCONFIG.Find(data.HMCC_ROW_ID);
                    #region Set Value                    
                    _search.HMCC_NAME = data.HMCC_NAME;
                    _search.HMCC_DESC = data.HMCC_DESC;
                    _search.HMCC_VALUE = data.HMCC_VALUE;
                    _search.HMCC_UPDATED_BY = data.HMCC_UPDATED_BY;
                    _search.HMCC_UPDATED_DATE = data.HMCC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CRCONFIG data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CRCONFIG.Find(data.HMCC_ROW_ID);
                #region Set Value                
                _search.HMCC_NAME = data.HMCC_NAME;
                _search.HMCC_DESC = data.HMCC_DESC;
                _search.HMCC_VALUE = data.HMCC_VALUE;
                _search.HMCC_UPDATED_BY = data.HMCC_UPDATED_BY;
                _search.HMCC_UPDATED_DATE = data.HMCC_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static HEDG_MT_CRCONFIG GetByKey(string key)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_MT_CRCONFIG
                             where v.HMCC_NAME.ToUpper() == key.ToUpper()
                             select v).FirstOrDefault();
                return query;
            }
        }
    }
}
