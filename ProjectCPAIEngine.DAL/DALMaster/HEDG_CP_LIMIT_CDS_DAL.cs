﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_CP_LIMIT_CDS_DAL
    {
        public void Save(HEDG_CP_LIMIT_CDS data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_CP_LIMIT_CDS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string FKID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_CP_LIMIT_CDS WHERE HCLC_FK_CP_LIMIT = '" + FKID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HEDG_CP_LIMIT_CDS> GetCdsConfig()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<HEDG_CP_LIMIT_CDS> result = new List<HEDG_CP_LIMIT_CDS>();

                var queryOut = (from v in context.HEDG_CP_LIMIT_CDS
                                where v.HCLC_TYPE == "CDS OUT"
                                select v);
                result = queryOut.OrderByDescending(x => x.HCLC_VALUE).ThenBy(x => x.HCLC_DAY).ToList();

                var queryIn = (from v in context.HEDG_CP_LIMIT_CDS
                               where v.HCLC_TYPE == "CDS IN"
                               select v);
                result.AddRange(queryIn.OrderBy(x => x.HCLC_VALUE).ThenBy(x => x.HCLC_DAY).ToList());

                return result;
            }
        }
    }
}
