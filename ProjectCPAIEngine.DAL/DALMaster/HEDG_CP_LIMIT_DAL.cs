﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_CP_LIMIT_DAL
    {
        public void Save(HEDG_CP_LIMIT data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_CP_LIMIT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_CP_LIMIT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_CP_LIMIT.Find(data.HCPL_ROW_ID);
                #region Set Value
                //_search.HCPL_FK_CREDIT_RATING = data.HCPL_FK_CREDIT_RATING;
                //_search.HCPL_CREDIT_RATING_DESC = data.HCPL_CREDIT_RATING_DESC;
                ////_search.HCPL_SPTRADE_PERCENT = data.HCPL_SPTRADE_PERCENT;
                ////_search.HCPL_SPTRADE_PERCENT_DESC = data.HCPL_SPTRADE_PERCENT_DESC;
                //_search.HCPL_LIMIT_OVER_PERCENT = data.HCPL_LIMIT_OVER_PERCENT;
                //_search.HCPL_LIMIT_OVER_PERCENT_DESC = data.HCPL_LIMIT_OVER_PERCENT_DESC;
                //_search.HCPL_LIMIT_OVER_PERCENT_EMAIL = data.HCPL_LIMIT_OVER_PERCENT_EMAIL;
                _search.HCPL_UPDATED_DATE = data.HCPL_UPDATED_DATE;
                _search.HCPL_UPDATED_BY = data.HCPL_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
