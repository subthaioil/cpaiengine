﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_DAL
    {
        public void Save(HEDG_MT_CP data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Save(HEDG_MT_CP data)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    context.HEDG_MT_CP.Add(data);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }


        public void Update(HEDG_MT_CP data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP.Find(data.HMCP_ROW_ID);
                #region Set Value
                _search.HMCP_FK_VENDOR = data.HMCP_FK_VENDOR;
                _search.HMCP_NAME = data.HMCP_NAME;
                _search.HMCP_FULL_NAME = data.HMCP_FULL_NAME;
                _search.HMCP_MAPPING_CDS_CP = data.HMCP_MAPPING_CDS_CP;
                _search.HMCP_LAST_CDS_DATE = data.HMCP_LAST_CDS_DATE;
                _search.HMCP_LAST_CDS_VALUE = data.HMCP_LAST_CDS_VALUE;
                _search.HMCP_CREDIT_RATING = data.HMCP_CREDIT_RATING;
                _search.HMCP_CREDIT_LIMIT = data.HMCP_CREDIT_LIMIT;
                _search.HMCP_CREDIT_LIMIT_UPDATE = data.HMCP_CREDIT_LIMIT_UPDATE;
                _search.HMCP_PAYMENT_DAY = data.HMCP_PAYMENT_DAY;
                _search.HMCP_PAYMENT_DAY_TYPE = data.HMCP_PAYMENT_DAY_TYPE;
                _search.HMCP_UNIT_ROUND_DECIMAL = data.HMCP_UNIT_ROUND_DECIMAL;
                _search.HMCP_UNIT_ROUND_TYPE = data.HMCP_UNIT_ROUND_TYPE;
                _search.HMCP_DESC = data.HMCP_DESC;
                _search.HMCP_REASON = data.HMCP_REASON;
                _search.HMCP_STATUS = data.HMCP_STATUS;
                _search.HMCP_UPDATED_DATE = data.HMCP_UPDATED_DATE;
                _search.HMCP_UPDATED_BY = data.HMCP_UPDATED_BY;
                _search.HMCP_STATUS_TRADE = data.HMCP_STATUS_TRADE;
                _search.HMCP_VERSION = data.HMCP_VERSION;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFromExcel(HEDG_MT_CP data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP.Find(data.HMCP_ROW_ID);
                #region Set Value
                _search.HMCP_LAST_CDS_DATE = data.HMCP_LAST_CDS_DATE;
                _search.HMCP_LAST_CDS_VALUE = data.HMCP_LAST_CDS_VALUE;

                if (string.IsNullOrEmpty(data.HMCP_CREDIT_RATING))
                {
                    _search.HMCP_CREDIT_RATING_REF_FLAG = "Y";
                }
                else
                {
                    _search.HMCP_CREDIT_RATING_REF_FLAG = "N";
                    _search.HMCP_CREDIT_RATING = data.HMCP_CREDIT_RATING;
                }

                if (data.HMCP_CREDIT_LIMIT_UPDATE == "N")
                {
                    _search.HMCP_CREDIT_LIMIT = data.HMCP_CREDIT_LIMIT;
                }

                _search.HMCP_VERSION = (Convert.ToInt32(_search.HMCP_VERSION) + 1).ToString();
                _search.HMCP_UPDATED_DATE = data.HMCP_UPDATED_DATE;
                _search.HMCP_UPDATED_BY = data.HMCP_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFromCDSService(HEDG_MT_CP data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP.Find(data.HMCP_ROW_ID);
                #region Set Value
                //_search.HMCP_FK_VENDOR = data.HMCP_FK_VENDOR;
                //_search.HMCP_NAME = data.HMCP_NAME;
                //_search.HMCP_FULL_NAME = data.HMCP_FULL_NAME;
                ////_search.HMCP_MAPPING_CDS_CP = data.HMCP_MAPPING_CDS_CP;
                //_search.HMCP_LAST_CDS_DATE = data.HMCP_LAST_CDS_DATE;
                //_search.HMCP_LAST_CDS_VALUE = data.HMCP_LAST_CDS_VALUE;
                //_search.HMCP_CREDIT_RATING = data.HMCP_CREDIT_RATING;
                //_search.HMCP_CREDIT_RATING_NA = data.HMCP_CREDIT_RATING_NA;
                //_search.HMCP_CREDIT_LIMIT = data.HMCP_CREDIT_LIMIT;
                //_search.HMCP_CREDIT_LIMIT_UPDATE = data.HMCP_CREDIT_LIMIT_UPDATE;
                //_search.HMCP_PAYMENT_DAY = data.HMCP_PAYMENT_DAY;
                //_search.HMCP_PAYMENT_DAY_TYPE = data.HMCP_PAYMENT_DAY_TYPE;
                //_search.HMCP_ROUND_DECIMAL = data.HMCP_ROUND_DECIMAL;
                //_search.HMCP_ROUND_TYPE = data.HMCP_ROUND_TYPE;
                //_search.HMCP_DESC = data.HMCP_DESC;
                //_search.HMCP_REASON = data.HMCP_REASON;
                //_search.HMCP_STATUS = data.HMCP_STATUS;
                _search.HMCP_UPDATED_DATE = data.HMCP_UPDATED_DATE;
                _search.HMCP_UPDATED_BY = data.HMCP_UPDATED_BY;
                _search.HMCP_STATUS_TRADE = data.HMCP_STATUS_TRADE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP WHERE HMCP_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP where r.HMCP_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static HEDG_MT_CP GetMtCp(string vendor)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_MT_CP
                             where v.HMCP_FK_VENDOR.ToUpper() == vendor.ToUpper()
                             select v).FirstOrDefault();
                return query;
            }
        }

        public bool ChkByCountryParty(string counterParty)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var chk = context.HEDG_MT_CP.Where(c => c.HMCP_FK_VENDOR == counterParty).Any();
                return chk;
            }

        }


        public bool ChkMappingCDS(string mappingCDS)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var chk = context.HEDG_MT_CP.Where(c => c.HMCP_MAPPING_CDS_CP == mappingCDS).Any();
                return chk;
            }
        }



        public HEDG_MT_CP GetMtById(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_MT_CP
                             where v.HMCP_ROW_ID.ToUpper() == id.ToUpper()
                             select v).FirstOrDefault();
                return query;
            }
        }
    }
}
