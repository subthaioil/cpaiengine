﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_LIMIT_DAL
    {
        public void Save(HEDG_MT_CP_LIMIT data)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    context.HEDG_MT_CP_LIMIT.Add(data);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public void Update(HEDG_MT_CP_LIMIT data)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var _search = context.HEDG_MT_CP_LIMIT.Find(data.HMCL_ROW_ID);
                try
                {
                    #region Set Value
                    _search.HMCL_DATE_START = data.HMCL_DATE_START;
                    _search.HMCL_DATE_END = data.HMCL_DATE_END;
                    _search.HMCL_CREDIT_LIMIT_AMT = data.HMCL_CREDIT_LIMIT_AMT;
                    _search.HMCL_NOTE = data.HMCL_NOTE;
                    _search.HMCL_STATUS = data.HMCL_STATUS;
                    _search.HMCL_UPDATED_BY = data.HMCL_UPDATED_BY;
                    _search.HMCL_UPDATED_DATE = data.HMCL_UPDATED_DATE;             
                    #endregion                    
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
    }
}
