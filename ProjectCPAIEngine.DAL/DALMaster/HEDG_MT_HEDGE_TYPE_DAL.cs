﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_HEDGE_TYPE_DAL
    {
        public void Save(HEDG_MT_HEDGE_TYPE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_MT_HEDGE_TYPE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string getLastID()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.HEDG_MT_HEDGE_TYPE.OrderByDescending(c=>c.HMHT_ROW_ID).FirstOrDefault().HMHT_ROW_ID;
                return data;
            }
        }


        public void Update(HEDG_MT_HEDGE_TYPE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_MT_HEDGE_TYPE.Find(data.HMHT_ROW_ID);
                    #region Set Value
                    _search.HMHT_ROW_ID = data.HMHT_ROW_ID;
                    _search.HMHT_NAME = data.HMHT_NAME;
                    _search.HMHT_DESC = data.HMHT_DESC;
                    _search.HMHT_STATUS = data.HMHT_STATUS;
                    _search.HMHT_INVENTORY_FLAG = data.HMHT_INVENTORY_FLAG;
                    _search.HMHT_UPDATED_DATE = data.HMHT_UPDATED_DATE;
                    _search.HMHT_UPDATED_BY = data.HMHT_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
