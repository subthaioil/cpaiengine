﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_PROD_DAL
    {
        public void Save(HEDG_MT_PROD data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_PROD.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveMappingMarketPrice(HEDG_MT_PROD_MKT data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_PROD_MKT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveMappingForward(HEDG_MT_PROD_MKT_FW data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_PROD_MKT_FW.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_PROD data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_PROD.SingleOrDefault(p => p.HMP_ROW_ID == data.HMP_ROW_ID);
                if (_search != null)
                {
                    _search.HMP_STATUS = data.HMP_STATUS;
                    _search.HMP_NAME = data.HMP_NAME;
                    _search.HMP_FULL_NAME = data.HMP_FULL_NAME;
                    _search.HMP_DESC = data.HMP_DESC;
                    _search.HMP_UPDATED_DATE = data.HMP_UPDATED_DATE;
                    _search.HMP_UPDATED_BY = data.HMP_UPDATED_BY;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMappingMarketPrice(HEDG_MT_PROD_MKT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_PROD_MKT.SingleOrDefault(p => p.HMPM_ROW_ID == data.HMPM_ROW_ID && p.HMPM_FK_PROD == data.HMPM_FK_PROD);
                if (_search != null)
                { 
                    _search.HMPM_ORDER = data.HMPM_ORDER;
                    _search.HMPM_FK_MKT_PROD_F = data.HMPM_FK_MKT_PROD_F;
                    _search.HMPM_MKT_PROD_F_NAME = data.HMPM_MKT_PROD_F_NAME;
                    _search.HMPM_MKT_PROD_F_UNIT = data.HMPM_MKT_PROD_F_UNIT;
                    _search.HMPM_FK_MKT_PROD_S = data.HMPM_FK_MKT_PROD_S;
                    _search.HMPM_MKT_PROD_S_NAME = data.HMPM_MKT_PROD_S_NAME;
                    _search.HMPM_MKT_PROD_S_UNIT = data.HMPM_MKT_PROD_S_UNIT;
                    _search.HMPM_DISPLAY_NAME = data.HMPM_DISPLAY_NAME;
                    _search.HMPM_UNIT = data.HMPM_UNIT;
                    _search.HMPM_CF = data.HMPM_CF;
                    _search.HMPM_SHOW_FLAG = data.HMPM_SHOW_FLAG;
                    _search.HMPM_UPDATED_DATE = data.HMPM_UPDATED_DATE;
                    _search.HMPM_UPDATED_BY = data.HMPM_UPDATED_BY;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMappingForward(HEDG_MT_PROD_MKT_FW data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_PROD_MKT_FW.SingleOrDefault(p => p.HMPF_ROW_ID == data.HMPF_ROW_ID && p.HMPF_FK_PROD_MKT == data.HMPF_FK_PROD_MKT);
                if (_search != null)
                {
                    _search.HMPF_ORDER = data.HMPF_ORDER;
                    _search.HMPF_MKT_NAME = data.HMPF_MKT_NAME;
                    _search.HMPF_FK_MKT_PROD = data.HMPF_FK_MKT_PROD;
                    _search.HMPF_MKT_PROD_NAME = data.HMPF_MKT_PROD_NAME;
                    _search.HMPF_MKT_PROD_UNIT = data.HMPF_MKT_PROD_UNIT;
                    _search.HMPF_UPDATED_DATE = data.HMPF_UPDATED_DATE;
                    _search.HMPF_UPDATED_BY = data.HMPF_UPDATED_BY;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(HEDG_MT_PROD data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_PROD.SingleOrDefault(p => p.HMP_ROW_ID == data.HMP_ROW_ID);
                if (_search != null)
                {
                    context.HEDG_MT_PROD.Remove(_search);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteMappingMarketPrice(HEDG_MT_PROD_MKT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_PROD_MKT.SingleOrDefault(p => p.HMPM_ROW_ID == data.HMPM_ROW_ID && p.HMPM_FK_PROD == data.HMPM_FK_PROD);
                if (_search != null)
                {
                    context.HEDG_MT_PROD_MKT.Remove(_search);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteMappingForward(HEDG_MT_PROD_MKT_FW data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_PROD_MKT_FW.SingleOrDefault(p => p.HMPF_ROW_ID == data.HMPF_ROW_ID && p.HMPF_FK_PROD_MKT == data.HMPF_FK_PROD_MKT);
                if (_search != null)
                {
                    context.HEDG_MT_PROD_MKT_FW.Remove(_search);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
