﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_TOOL_CONDITION_DAL
    {
        public void Save(HEDG_MT_TOOL_CONDITION data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_TOOL_CONDITION.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_TOOL_CONDITION data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_TOOL_CONDITION.Find(data.HMTC_ROW_ID);
                #region Set Value
                _search.HMTC_ORDER = data.HMTC_ORDER;
                _search.HMTC_VALUE_A = data.HMTC_VALUE_A;
                _search.HMTC_VALUE_B = data.HMTC_VALUE_B;
                _search.HMTC_OPERATOR = data.HMTC_OPERATOR;
                _search.HMTC_EXPRESSION = data.HMTC_EXPRESSION;
                _search.HMTC_RETURN_FLAG = data.HMTC_RETURN_FLAG;
                _search.HMTC_PREMIUM_FLAG = data.HMTC_PREMIUM_FLAG;
                _search.HMTC_UPDATED_DATE = data.HMTC_UPDATED_DATE;
                _search.HMTC_UPDATED_BY = data.HMTC_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL_CONDITION WHERE HMTC_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAll(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL_CONDITION WHERE HMTC_FK_HEDG_TOOL_OPTION = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_MT_TOOL_CONDITION> GetDataByFkID(string sID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var result = context.HEDG_MT_TOOL_CONDITION.Where(x => x.HMTC_FK_HEDG_TOOL_OPTION.ToUpper() == sID.ToUpper()).OrderBy(x => x.HMTC_ORDER).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
