﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_TOOL_DAL
    {
        public void Save(HEDG_MT_TOOL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_TOOL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_TOOL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_TOOL.Find(data.HMT_ROW_ID);
                #region Set Value
                _search.HMT_REASON = data.HMT_REASON;
                _search.HMT_STATUS = data.HMT_STATUS;
                _search.HMT_UPDATED_DATE = data.HMT_UPDATED_DATE;
                _search.HMT_UPDATED_BY = data.HMT_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL WHERE HMT_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void Update(HEDG_MT_TOOL data, EntityCPAIEngine context)
        //{
        //    try
        //    {
        //        var _search = context.HEDG_MT_TOOL.Find(data.HMT_ROW_ID);
        //        #region Set Value
        //        _search.HMT_NAME = data.HMT_NAME;
        //        _search.HMT_DESC = data.HMT_DESC;
        //        _search.HMT_FEATURE_TARGET_FLAG = data.HMT_FEATURE_TARGET_FLAG;
        //        _search.HMT_FEATURE_SWAP_FLAG = data.HMT_FEATURE_SWAP_FLAG;
        //        _search.HMT_FEATURE_EXTEND_FLAG = data.HMT_FEATURE_EXTEND_FLAG;
        //        _search.HMT_FEATURE_CAPPED_FLAG = data.HMT_FEATURE_CAPPED_FLAG;
        //        _search.HMT_VERSION = data.HMT_VERSION;
        //        _search.HMT_REASON = data.HMT_REASON;
        //        _search.HMT_STATUS = data.HMT_STATUS;
        //        _search.HMT_UPDATED_DATE = data.HMT_UPDATED_DATE;
        //        _search.HMT_UPDATED_BY = data.HMT_UPDATED_BY;
        //        #endregion
        //        context.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
