﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_TOOL_OPTION_DAL
    {
        public void Save(HEDG_MT_TOOL_OPTION data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_TOOL_OPTION.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_TOOL_OPTION data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_TOOL_OPTION.Find(data.HMTO_ROW_ID);
                #region Set Value
                _search.HMTO_ORDER = data.HMTO_ORDER;
                _search.HMTO_OPTION = data.HMTO_OPTION;
                _search.HMTO_UPDATED_DATE = data.HMTO_UPDATED_DATE;
                _search.HMTO_UPDATED_BY = data.HMTO_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL_OPTION WHERE HMTO_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAll(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL_OPTION WHERE HMTO_FK_HEDG_MT_TOOL = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_MT_TOOL_OPTION> GetDataByFkID(string sID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var result = context.HEDG_MT_TOOL_OPTION.Where(x => x.HMTO_FK_HEDG_MT_TOOL.ToUpper() == sID.ToUpper()).OrderBy(x => x.HMTO_ORDER).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
