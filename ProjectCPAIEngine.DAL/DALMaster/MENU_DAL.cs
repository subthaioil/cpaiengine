﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MENU_DAL
    {
        public void Save(MENU data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MENU.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MENU data, EntityCPAIEngine context)
        {
            try
            {
                context.MENU.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MENU data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MENU.Find(data.MEU_ROW_ID);
                    #region Set Value
                    _search.MEU_GROUP_MENU = data.MEU_GROUP_MENU;
                    _search.MEU_PARENT_ID = data.MEU_PARENT_ID;
                    _search.LNG_DESCRIPTION = data.LNG_DESCRIPTION;
                    _search.MEU_URL = data.MEU_URL;
                    _search.MEU_IMG = data.MEU_IMG;
                    _search.MEU_LEVEL = data.MEU_LEVEL;
                    _search.MEU_LIST_NO = data.MEU_LIST_NO;
                    _search.MEU_CONTROL_TYPE = data.MEU_CONTROL_TYPE;
                    _search.MEU_ACTIVE = data.MEU_ACTIVE;
                    _search.MEU_UPDATED_BY = data.MEU_UPDATED_BY;
                    _search.MEU_UPDATED_DATE = data.MEU_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MENU data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MENU.Find(data.MEU_ROW_ID);
                #region Set Value
                _search.MEU_GROUP_MENU = data.MEU_GROUP_MENU;
                _search.MEU_PARENT_ID = data.MEU_PARENT_ID;
                _search.LNG_DESCRIPTION = data.LNG_DESCRIPTION;
                _search.MEU_URL = data.MEU_URL;
                _search.MEU_IMG = data.MEU_IMG;
                _search.MEU_LEVEL = data.MEU_LEVEL;
                _search.MEU_LIST_NO = data.MEU_LIST_NO;
                _search.MEU_CONTROL_TYPE = data.MEU_CONTROL_TYPE;
                _search.MEU_ACTIVE = data.MEU_ACTIVE;
                _search.MEU_UPDATED_BY = data.MEU_UPDATED_BY;
                _search.MEU_UPDATED_DATE = data.MEU_UPDATED_DATE;
                _search.MEU_URL_DIRECT = data.MEU_URL_DIRECT;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateListNo(string MenuRowID, string ListNo, string UpdateBy, DateTime UpdateDate, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MENU.Find(MenuRowID);
                #region Set Value
                _search.MEU_LIST_NO = ListNo;
                _search.MEU_UPDATED_BY = UpdateBy;
                _search.MEU_UPDATED_DATE = UpdateDate;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MENU WHERE MEU_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MENU WHERE MEU_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
