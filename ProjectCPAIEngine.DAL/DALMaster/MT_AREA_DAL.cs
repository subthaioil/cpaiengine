﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_AREA_DAL
    {
       
        public static List<MT_AREA> GetArea(string status = "ACTIVE")
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_AREA
                             orderby v.MAR_ORDER
                             where v.MAR_STATUS == status
                             select v).AsEnumerable().ToList();
                List<MT_AREA> areas = query.Select(area => new MT_AREA()
                {
                    MAR_ROW_ID = area.MAR_ROW_ID,
                    MAR_NAME = area.MAR_NAME,
                    MAR_ORDER = area.MAR_ORDER,
                    MAR_STATUS = area.MAR_STATUS,
                    MAR_CREATED_BY = area.MAR_CREATED_BY,
                    MAR_CREATED_DATE = area.MAR_CREATED_DATE,
                    MAR_UPDATED_BY = area.MAR_UPDATED_BY,
                    MAR_UPDATED_DATE = area.MAR_UPDATED_DATE,
                    MT_UNIT = area.MT_UNIT
                }).ToList();
                return areas;
            }
        }



        public bool ChkAreaByName(string name)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var aresName = context.MT_AREA.Where(c => c.MAR_NAME == name).Any();
                if (!aresName)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
                
        }



        public void Save(MT_AREA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_AREA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void Update(MT_AREA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_AREA.Find(data.MAR_ROW_ID);

                    _search.MAR_NAME = data.MAR_NAME != null ? data.MAR_NAME : _search.MAR_NAME;
                    _search.MAR_ORDER = data.MAR_ORDER != null ? data.MAR_ORDER : _search.MAR_ORDER;
                    _search.MAR_STATUS = data.MAR_STATUS != null ? data.MAR_STATUS : _search.MAR_STATUS;
                    _search.MAR_UPDATED_BY = data.MAR_UPDATED_BY;
                    _search.MAR_UPDATED_DATE = data.MAR_UPDATED_DATE;

                    context.SaveChanges();
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        

        public bool ChkAreaName(string areaName, string newName)
        {
            using(EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var oldName = context.MT_AREA.Where(c => c.MAR_ROW_ID == areaName).SingleOrDefault().MAR_NAME;
                var name = context.MT_AREA.Where(a => a.MAR_NAME == newName && a.MAR_NAME != oldName).Any();
                if (!name)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
