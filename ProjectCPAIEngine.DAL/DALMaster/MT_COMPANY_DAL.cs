﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_COMPANY_DAL
    {
        public void Save(MT_COMPANY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_COMPANY.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_COMPANY data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_COMPANY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_COMPANY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_COMPANY.Find(data.MCO_COMPANY_CODE);
                    #region Set Value
                    _search.MCO_COMPANY_CODE = data.MCO_COMPANY_CODE;
                    _search.MCO_SHORT_NAME = data.MCO_SHORT_NAME;
                    _search.MCO_FULL_NAME = data.MCO_FULL_NAME;
                    _search.MCO_DESCRIPTION = data.MCO_DESCRIPTION;
                    _search.MCO_ACC_NUM_PAY = data.MCO_ACC_NUM_PAY;
                    _search.MCO_ACC_NUM_RECEIVE = data.MCO_ACC_NUM_RECEIVE;
                    _search.MCO_STATUS = data.MCO_STATUS;
                    _search.MCO_UPDATED_BY = data.MCO_UPDATED_BY;
                    _search.MCO_UPDATED_DATE = data.MCO_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_COMPANY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_COMPANY.Find(data.MCO_COMPANY_CODE);
                #region Set Value
                _search.MCO_COMPANY_CODE = data.MCO_COMPANY_CODE;
                _search.MCO_SHORT_NAME = data.MCO_SHORT_NAME;
                _search.MCO_STATUS = data.MCO_STATUS;
                _search.MCO_UPDATED_BY = data.MCO_UPDATED_BY;
                _search.MCO_UPDATED_DATE = data.MCO_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_COMPANY WHERE MCO_COMPANY_CODE = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_COMPANY WHERE MCO_COMPANY_CODE = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ChkRowId(string RowId)
        {
            using (var context = new EntityCPAIEngine())
            {
               var chkRowId = context.MT_COMPANY.Where(c => c.MCO_COMPANY_CODE == RowId).Any();
                return chkRowId;
            }
        }
    }
}
