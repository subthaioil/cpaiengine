﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_COUNTRY_DAL
    {
        public void Save(MT_COUNTRY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_COUNTRY.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_COUNTRY data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_COUNTRY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_COUNTRY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_COUNTRY.Find(data.MCT_ROW_ID);
                    #region Set Value
                    _search.MCT_CLIENT = data.MCT_CLIENT;
                    _search.MCT_LAND1 = data.MCT_LAND1;
                    _search.MCT_LANDX = data.MCT_LANDX;
                    _search.MCT_LANDX50 = data.MCT_LANDX50;
                    _search.MCT_LANGUAGE = data.MCT_LANGUAGE;
                    _search.MCT_NATIO = data.MCT_NATIO;
                    _search.MCT_STATUS = data.MCT_STATUS;
                    _search.MCT_UPDATED_BY = data.MCT_UPDATED_BY;
                    _search.MCT_UPDATED_DATE = data.MCT_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_COUNTRY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_COUNTRY.Find(data.MCT_ROW_ID);
                #region Set Value
                _search.MCT_CLIENT = data.MCT_CLIENT;
                _search.MCT_LAND1 = data.MCT_LAND1;
                _search.MCT_LANDX = data.MCT_LANDX;
                _search.MCT_LANDX50 = data.MCT_LANDX50;
                _search.MCT_LANGUAGE = data.MCT_LANGUAGE;
                _search.MCT_NATIO = data.MCT_NATIO;
                _search.MCT_STATUS = data.MCT_STATUS;
                _search.MCT_UPDATED_BY = data.MCT_UPDATED_BY;
                _search.MCT_UPDATED_DATE = data.MCT_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_COUNTRY WHERE MCT_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_COUNTRY WHERE MCT_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MT_COUNTRY> GetCountry(string status = "ACTIVE")
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_COUNTRY
                            where v.MCT_STATUS == status && v.MCT_CLIENT == "900"
                            select v;
                return query.ToList();
            }
        }
    }
}
