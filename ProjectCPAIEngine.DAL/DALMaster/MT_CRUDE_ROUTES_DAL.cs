﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CRUDE_ROUTES_DAL
    {
        public void Save(MT_CRUDE_ROUTES data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CRUDE_ROUTES.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CRUDE_ROUTES data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CRUDE_ROUTES.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CRUDE_ROUTES data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CRUDE_ROUTES.Find(data.MCR_DATE);
                    #region Set Value
                    _search.MCR_TD2 = data.MCR_TD2;
                    _search.MCR_TD3 = data.MCR_TD3;
                    _search.MCR_TD15 = data.MCR_TD15;
                    _search.MCR_STATUS = data.MCR_STATUS;
                    _search.MCR_UPDATED_BY = data.MCR_UPDATED_BY;
                    _search.MCR_UPDATED_DATE = data.MCR_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CRUDE_ROUTES data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CRUDE_ROUTES.Find(data.MCR_DATE);
                #region Set Value
                _search.MCR_TD2 = data.MCR_TD2;
                _search.MCR_TD3 = data.MCR_TD3;
                _search.MCR_TD15 = data.MCR_TD15;
                _search.MCR_STATUS = data.MCR_STATUS;
                _search.MCR_UPDATED_BY = data.MCR_UPDATED_BY;
                _search.MCR_UPDATED_DATE = data.MCR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(DateTime dDate)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CRUDE_ROUTES WHERE to_char(MCR_DATE,'dd/MM/yyyy') = '" + dDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(DateTime dDate, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CRUDE_ROUTES WHERE to_char(MCR_DATE,'dd/MM/yyyy') = '" + dDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
