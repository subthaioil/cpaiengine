﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CUST_BROKER_DAL
    {
        public void Save(MT_CUST_BROKER data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CUST_BROKER.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CUST_BROKER data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CUST_BROKER.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_BROKER data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CUST_BROKER.Find(data.MCB_ROW_ID);
                    #region Set Value
                    _search.MCB_FK_MT_CUST = data.MCB_FK_MT_CUST;
                    _search.MCB_FK_MT_VENDOR_BROKER = data.MCB_FK_MT_VENDOR_BROKER;
                    _search.MCB_ORDER = data.MCB_ORDER;
                    _search.MCB_STATUS = data.MCB_STATUS;
                    _search.MCB_DEFAULT = data.MCB_DEFAULT;
                    _search.MCB_UPDATED_DATE = data.MCB_UPDATED_DATE;
                    _search.MCB_UPDATED_BY = data.MCB_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_BROKER data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CUST_BROKER.Find(data.MCB_ROW_ID);
                #region Set Value
                _search.MCB_FK_MT_CUST = data.MCB_FK_MT_CUST;
                _search.MCB_FK_MT_VENDOR_BROKER = data.MCB_FK_MT_VENDOR_BROKER;
                _search.MCB_ORDER = data.MCB_ORDER;
                _search.MCB_STATUS = data.MCB_STATUS;
                _search.MCB_DEFAULT = data.MCB_DEFAULT;
                _search.MCB_UPDATED_DATE = data.MCB_UPDATED_DATE;
                _search.MCB_UPDATED_BY = data.MCB_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_BROKER WHERE MCB_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_BROKER WHERE MCB_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustBrokerID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_CUST_BROKER where r.MCB_FK_MT_VENDOR_BROKER == CustBrokerID select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_CUST_BROKER.Remove(p);
                                }
                                context.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    var result = from r in context.MT_CUST_BROKER where r.MCB_FK_MT_CUST == CustID select r;
                    //check if there is a record with this id

                    if (result.Count() > 0)
                    {

                        foreach (var p in result)
                        {
                            context.MT_CUST_BROKER.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
