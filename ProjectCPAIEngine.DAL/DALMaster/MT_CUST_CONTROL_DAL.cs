﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CUST_CONTROL_DAL
    {
        public void Save(MT_CUST_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CUST_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CUST_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CUST_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CUST_CONTROL.Find(data.MCR_ROW_ID);
                    #region Set Value
                    _search.MCR_FK_CUST_CONTROL_ID = data.MCR_FK_CUST_CONTROL_ID;
                    _search.MCR_SYSTEM = data.MCR_SYSTEM;
                    _search.MCR_TYPE = data.MCR_TYPE;
                    _search.MCR_COLOR_CODE = data.MCR_COLOR_CODE;
                    _search.MCR_STATUS = data.MCR_STATUS;
                    _search.MCR_UPDATED_BY = data.MCR_UPDATED_BY;
                    _search.MCR_UPDATED_DATE = data.MCR_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CUST_CONTROL.Find(data.MCR_ROW_ID);
                #region Set Value
                _search.MCR_FK_CUST_CONTROL_ID = data.MCR_FK_CUST_CONTROL_ID;
                _search.MCR_SYSTEM = data.MCR_SYSTEM;
                _search.MCR_TYPE = data.MCR_TYPE;
                _search.MCR_COLOR_CODE = data.MCR_COLOR_CODE;
                _search.MCR_STATUS = data.MCR_STATUS;
                _search.MCR_UPDATED_BY = data.MCR_UPDATED_BY;
                _search.MCR_UPDATED_DATE = data.MCR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_CONTROL WHERE MCR_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_CONTROL WHERE MCR_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustControlID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_CUST_CONTROL where r.MCR_FK_CUST_CONTROL_ID == CustControlID select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_CUST_CONTROL.Remove(p);
                                }
                                context.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustControlID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    var result = from r in context.MT_CUST_CONTROL where r.MCR_FK_CUST_CONTROL_ID == CustControlID select r;
                    //check if there is a record with this id

                    if (result.Count() > 0)
                    {

                        foreach (var p in result)
                        {
                            context.MT_CUST_CONTROL.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetColor(string Cust_ID)
        {
            using (var context = new EntityCPAIEngine())
            {
                var color = context.MT_CUST_CONTROL.Where(c => c.MCR_FK_CUST_CONTROL_ID == Cust_ID && c.MCR_TYPE == "CHR_FRE" && c.MCR_STATUS.ToUpper() == "ACTIVE").FirstOrDefault();

                if (color != null)
                {
                    var c = color.MCR_COLOR_CODE;
                    return c;
                }
                else
                {
                    return null;
                }
            }
        }

        public string GetName(string Cust_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    // var a = context.MT_CUST_CONTROL.Where(c => c.MCR_FK_CUST_CONTROL_ID == Cust_ID).SingleOrDefault();
                    var color = context.MT_CUST_DETAIL.Where(c => c.MCD_ROW_ID == Cust_ID).SingleOrDefault();
                    if (color != null)
                    {                        
                        return color.MCD_NAME_1;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetShotName(string Cust_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    // var a = context.MT_CUST_CONTROL.Where(c => c.MCR_FK_CUST_CONTROL_ID == Cust_ID).SingleOrDefault();
                    var color = context.MT_CUST_DETAIL.Where(c => c.MCD_ROW_ID == Cust_ID).SingleOrDefault();
                    if (color != null)
                    {
                        return color.MCD_SORT_FIELD;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
