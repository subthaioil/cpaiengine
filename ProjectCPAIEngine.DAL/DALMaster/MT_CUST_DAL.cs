﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CUST_DAL
    {
        public void Save(MT_CUST data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CUST.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CUST data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CUST.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CUST.Find(data.MCT_CUST_NUM);
                    #region Set Value
                    _search.MCT_STATUS = data.MCT_STATUS;
                    _search.MCT_CREATE_TYPE = data.MCT_CREATE_TYPE;
                    _search.MCT_UPDATED_BY = data.MCT_UPDATED_BY;
                    _search.MCT_UPDATED_DATE = data.MCT_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CUST.Find(data.MCT_CUST_NUM);
                #region Set Value
                _search.MCT_STATUS = data.MCT_STATUS;
                _search.MCT_CREATE_TYPE = data.MCT_CREATE_TYPE;
                _search.MCT_UPDATED_BY = data.MCT_UPDATED_BY;
                _search.MCT_UPDATED_DATE = data.MCT_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST WHERE MCT_CUST_NUM = '" + Num + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST WHERE VND_ACC_NUM_VENDOR = '" + Num + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MT_CUST_DATA> GetCUSTData()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_CUST_DATA
                            //where v.DLG_STATUS == "ACTIVE" && v.DLG_FK_USERS == id && v.DLG_USER_SYSTEM == system
                            select v;
                return query.ToList();
            }
        }

        public static List<MT_CUST_DETAIL> GetCUSTDetail()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_CUST_DETAIL
                                //where v.DLG_STATUS == "ACTIVE" && v.DLG_FK_USERS == id && v.DLG_USER_SYSTEM == system
                            where v.MCD_NATION =="I"
                            select v;
                return query.ToList();
            }
        }
    }
}
