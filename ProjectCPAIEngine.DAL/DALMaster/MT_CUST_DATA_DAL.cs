﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CUST_DATA_DAL
    {
        public void Save(MT_CUST_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CUST_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CUST_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CUST_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CUST_DATA.Find(data.MCS_ROW_ID);
                    #region Set Value
                    _search.MCS_STATUS = data.MCS_STATUS;
                    _search.MCS_UPDATED_BY = data.MCS_UPDATED_BY;
                    _search.MCS_UPDATED_DATE = data.MCS_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CUST_DATA.Find(data.MCS_ROW_ID);
                #region Set Value
                _search.MCS_STATUS = data.MCS_STATUS;
                _search.MCS_UPDATED_BY = data.MCS_UPDATED_BY;
                _search.MCS_UPDATED_DATE = data.MCS_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_DATA WHERE MCS_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_DATA WHERE MCS_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
