﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_JETTY_DAL
    {
        public void Save(MT_JETTY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_JETTY.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_JETTY data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_JETTY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_JETTY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_JETTY.Find(data.MTJ_ROW_ID);
                    #region Set Value
                    _search.MTJ_PORT_TYPE = data.MTJ_PORT_TYPE;
                    _search.MTJ_MT_COUNTRY = data.MTJ_MT_COUNTRY;
                    _search.MTJ_LOCATION = data.MTJ_LOCATION;
                    _search.MTJ_JETTY_NAME = data.MTJ_JETTY_NAME;
                    _search.MTJ_STATUS = data.MTJ_STATUS;
                    _search.MTJ_CREATE_TYPE = data.MTJ_CREATE_TYPE;
                    _search.MTJ_UPDATED_DATE = data.MTJ_UPDATED_DATE;
                    _search.MTJ_UPDATED_BY = data.MTJ_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_JETTY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_JETTY.Find(data.MTJ_ROW_ID);
                #region Set Value
                _search.MTJ_PORT_TYPE = data.MTJ_PORT_TYPE;
                _search.MTJ_MT_COUNTRY = data.MTJ_MT_COUNTRY;
                _search.MTJ_LOCATION = data.MTJ_LOCATION;
                _search.MTJ_JETTY_NAME = data.MTJ_JETTY_NAME;
                _search.MTJ_STATUS = data.MTJ_STATUS;
                _search.MTJ_CREATE_TYPE = data.MTJ_CREATE_TYPE;
                _search.MTJ_UPDATED_DATE = data.MTJ_UPDATED_DATE;
                _search.MTJ_UPDATED_BY = data.MTJ_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(MT_JETTY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_JETTY.Find(data.MTJ_ROW_ID);
                #region Set Value
                _search.MTJ_STATUS = data.MTJ_STATUS;
                _search.MTJ_UPDATED_DATE = data.MTJ_UPDATED_DATE;
                _search.MTJ_UPDATED_BY = data.MTJ_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_JETTY WHERE MTJ_ROW_ID = '" + Num + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_JETTY WHERE MTJ_ROW_ID = '" + Num + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
