﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_OPER_CONDITION_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();

        public void Save(MT_OPER_CONDITION data)
        {
            try
            {
                context.MT_OPER_CONDITION.Add(data);
                try
                {
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                    throw innerException;
                }
            }
            catch (Exception ex)
            {
                Exception innerEx = ex.InnerException != null ? ex.InnerException : ex;
                throw innerEx;
            }
        }


        public void Delete(string unitRowId)
        {
            try
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand("DELETE FROM MT_OPER_CONDITION WHERE MOC_FK_MT_OPER_UNIT = '" + unitRowId + "'");
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;
                        dbContextTransaction.Rollback();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
