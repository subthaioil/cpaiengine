﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_OPER_UNIT_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();


        public void Save(MT_OPER_UNIT data)
        {
            try
            {
                context.MT_OPER_UNIT.Add(data);
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                    throw innerException;
                }
            }
            catch (Exception ex)
            {
                Exception innerEx = ex.InnerException != null ? ex.InnerException : ex;
                throw ex;
            }
        }


        public void Update(MT_OPER_UNIT data)
        {
            try
            {
                var _search = context.MT_OPER_UNIT.Find(data.MOU_ROW_ID);
                if (_search != null)
                {
                    _search.MOU_UNIT = data.MOU_UNIT != null ? data.MOU_UNIT : _search.MOU_UNIT;
                    _search.MOU_UPDATED_BY = data.MOU_UPDATED_BY;
                    _search.MOU_UPDATED_DATE = data.MOU_UPDATED_DATE;
                    try
                    {
                        context.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                        Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                        throw innerException;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Exception innerEx = ex.InnerException != null ? ex.InnerException : ex;
                throw innerEx;
            }
        }




        public void Delete(string unitRowId)
        {
            try
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand("DELETE FROM MT_OPER_UNIT WHERE MOU_ROW_ID = '" + unitRowId + "'");
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;
                        dbContextTransaction.Rollback();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
