﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_PLANT_DAL
    {
        public void Save(MT_PLANT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_PLANT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_PLANT data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_PLANT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PLANT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_PLANT.Find(data.MPL_PLANT);
                    #region Set Value
                    _search.MPL_COMPANY_CODE = data.MPL_COMPANY_CODE;
                    _search.MPL_SHORT_NAME = data.MPL_SHORT_NAME;
                    _search.MPL_DESC = data.MPL_DESC;
                    _search.MPL_CREATE_TYPE = data.MPL_CREATE_TYPE;
                    _search.MPL_STATUS = data.MPL_STATUS;
                    _search.MPL_UPDATED_BY = data.MPL_UPDATED_BY;
                    _search.MPL_UPDATED_DATE = data.MPL_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PLANT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_PLANT.Find(data.MPL_PLANT);
                #region Set Value
                _search.MPL_COMPANY_CODE = data.MPL_COMPANY_CODE;
                _search.MPL_SHORT_NAME = data.MPL_SHORT_NAME;
                _search.MPL_DESC = data.MPL_DESC;
                _search.MPL_CREATE_TYPE = data.MPL_CREATE_TYPE;
                _search.MPL_STATUS = data.MPL_STATUS;
                _search.MPL_UPDATED_BY = data.MPL_UPDATED_BY;
                _search.MPL_UPDATED_DATE = data.MPL_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(MT_PLANT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_PLANT.Find(data.MPL_PLANT);
                #region Set Value
                _search.MPL_STATUS = data.MPL_STATUS;
                _search.MPL_UPDATED_BY = data.MPL_UPDATED_BY;
                _search.MPL_UPDATED_DATE = data.MPL_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_PLANT WHERE MPL_PLANT = '" + Num + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      

        public void Delete(string Num, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_PLANT WHERE MPL_PLANT = '" + Num + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MT_PLANT> GetPlant(string company_code = "", string status = "ACTIVE")
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_PLANT
                            where v.MPL_COMPANY_CODE.ToUpper().Contains(company_code.ToUpper()) && v.MPL_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        //public static List<MT_PORT> GetPortData(string sSystem, string sID, string sName, string sCreateType, string sStatus)
        //{

        //    using (EntityCPAIEngine context = new EntityCPAIEngine())
        //    {
        //        // Select All
        //        var query = from v in context.MT_PORT
        //                    join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT
        //                    where vc.MMP_SYSTEM.ToUpper() == sSystem
        //                    select v;

        //        if (!string.IsNullOrEmpty(sID))
        //            query = query.Where(p => p.MLP_LOADING_PORT_ID.ToString().Contains(sID));

        //        if (!string.IsNullOrEmpty(sName))
        //            query = query.Where(p => p.MLP_LOADING_PORT_NAME.Contains(sName));

        //        if (!string.IsNullOrEmpty(sCreateType))
        //            query = query.Where(p => p.MLP_CREATE_TYPE.Contains(sCreateType));

        //        if (!string.IsNullOrEmpty(sStatus))
        //            query = query.Where(p => p.MLP_STATUS.Contains(sStatus));

        //        if (query != null)
        //            return query.ToList();
        //        else
        //            return null;

        //    }
        //}


    }
}
