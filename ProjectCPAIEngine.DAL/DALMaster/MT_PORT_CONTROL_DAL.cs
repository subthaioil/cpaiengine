﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_PORT_CONTROL_DAL
    {
        public void Save(MT_PORT_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_PORT_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_PORT_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_PORT_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PORT_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_PORT_CONTROL.Find(data.MMP_ROW_ID);
                    #region Set Value
                    _search.MMP_FK_PORT = data.MMP_FK_PORT;
                    _search.MMP_SYSTEM = data.MMP_SYSTEM;
                    _search.MMP_STATUS = data.MMP_STATUS;
                    _search.MMP_UPDATED_BY = data.MMP_UPDATED_BY;
                    _search.MMP_UPDATED_DATE = data.MMP_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PORT_CONTROL data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.MT_PORT_CONTROL.Find(data.MMP_ROW_ID);
                #region Set Value
                _search.MMP_FK_PORT = data.MMP_FK_PORT;
                _search.MMP_SYSTEM = data.MMP_SYSTEM;
                _search.MMP_STATUS = data.MMP_STATUS;
                _search.MMP_UPDATED_BY = data.MMP_UPDATED_BY;
                _search.MMP_UPDATED_DATE = data.MMP_UPDATED_DATE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_PORT_CONTROL WHERE MMP_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_PORT_CONTROL WHERE MMP_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByPortID(string PortID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_PORT_CONTROL where r.MMP_FK_PORT == Convert.ToDecimal(PortID) select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_PORT_CONTROL.Remove(p);
                                }
                                context.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByPortID(string PortID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    var result = from r in context.MT_PORT_CONTROL where r.MMP_FK_PORT == Convert.ToDecimal(PortID) select r;
                    //check if there is a record with this id

                    if (result.Count() > 0)
                    {

                        foreach (var p in result)
                        {
                            context.MT_PORT_CONTROL.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
