﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_PORT_DAL
    {
        public void Save(MT_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_PORT.Find(data.MLP_LOADING_PORT_ID);
                    #region Set Value
                    _search.MLP_LOADING_PORT_NAME = data.MLP_LOADING_PORT_NAME;
                    _search.MLP_CREATE_TYPE = data.MLP_CREATE_TYPE;
                    _search.MLP_STATUS = data.MLP_STATUS;
                    _search.MLP_UPDATED_BY = data.MLP_UPDATED_BY;
                    _search.MLP_UPDATED_DATE = data.MLP_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PORT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_PORT.Find(data.MLP_LOADING_PORT_ID);
                #region Set Value
                _search.MLP_LOADING_PORT_NAME = data.MLP_LOADING_PORT_NAME;
                _search.MLP_CREATE_TYPE = data.MLP_CREATE_TYPE;
                _search.MLP_REMARK = data.MLP_REMARK;
                _search.MLP_STATUS = data.MLP_STATUS;
                _search.MLP_UPDATED_BY = data.MLP_UPDATED_BY;
                _search.MLP_UPDATED_DATE = data.MLP_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_PORT WHERE MLP_LOADING_PORT_ID = '" + Num + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(MT_PORT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_PORT.Find(data.MLP_LOADING_PORT_ID);
                #region Set Value
                _search.MLP_LOADING_PORT_NAME = data.MLP_LOADING_PORT_NAME;
                _search.MLP_STATUS = data.MLP_STATUS;
                _search.MLP_REMARK = data.MLP_REMARK;
                _search.MLP_UPDATED_BY = data.MLP_UPDATED_BY;
                _search.MLP_UPDATED_DATE = data.MLP_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_PORT WHERE MLP_LOADING_PORT_ID = '" + Num + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Data MT_PORT by Status
        /// </summary>
        /// <param name="Status">status ("ACTIVE","INACTIVE") set string emply or nul get all</param>
        /// <returns></returns>
        public static List<MT_PORT> GetPortData(string sSystem, string sID, string sName, string sCreateType, string sStatus)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                // Select All
                var query = from v in context.MT_PORT
                            join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT
                            where vc.MMP_SYSTEM.ToUpper() == sSystem
                            select v;

                if (!string.IsNullOrEmpty(sID))
                    query = query.Where(p => p.MLP_LOADING_PORT_ID.ToString().Contains(sID));

                if (!string.IsNullOrEmpty(sName))
                    query = query.Where(p => p.MLP_LOADING_PORT_NAME.Contains(sName));

                if (!string.IsNullOrEmpty(sCreateType))
                    query = query.Where(p => p.MLP_CREATE_TYPE.Contains(sCreateType));

                if (!string.IsNullOrEmpty(sStatus))
                    query = query.Where(p => p.MLP_STATUS.Contains(sStatus));

                if (query != null)
                    return query.ToList();
                else
                    return null;

            }
        }


    }
}
