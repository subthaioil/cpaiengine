﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_SPEC_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();
        public void Save(MT_SPEC data)
        {
            try
            {
                    context.MT_SPEC.Add(data);
                    context.SaveChanges();
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }


        public void Update(MT_SPEC data)
        {
            try
            {
                var _search = context.MT_SPEC.Find(data.MSP_ROW_ID);
                if (_search != null)
                {
                    
                    //var rowVersion = _search.MSP_VERSION;
                   // var version = Convert.ToInt32(rowVersion) +1;
                    //rowVersion = version.ToString();
                    //_search.MSP_FK_MATERIALS = data.MSP_FK_MATERIALS != null ? data.MSP_FK_MATERIALS : _search.MSP_FK_MATERIALS;
                    //_search.MSP_FK_COUNTRY = data.MSP_FK_COUNTRY != null ? data.MSP_FK_COUNTRY : _search.MSP_FK_COUNTRY;
                    _search.MSP_REASON = data.MSP_REASON;
                    _search.MSP_UPDATED_BY = data.MSP_UPDATED_BY;
                    _search.MSP_UPDATED_DATE = data.MSP_UPDATED_DATE;
                    context.SaveChanges();
                }
            }catch(Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }

        public void Delete(MT_SPEC data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    // CHECK TRANSACTION DOES NOT USED BEFORE // 
                    if (!context.COO_CAM.Any(i => i.COCA_FK_MT_SPEC == data.MSP_ROW_ID))
                    {
                        var _search = context.MT_SPEC.Find(data.MSP_ROW_ID);
                        if (_search != null)
                        {
                            var _searchNote = context.MT_SPEC_NOTE.Where(i => i.MSPN_FK_MT_SPEC == data.MSP_ROW_ID).ToList();
                            if (_searchNote != null)
                            {
                                context.MT_SPEC_NOTE.RemoveRange(_searchNote);
                            }

                            var _searchData = context.MT_SPEC_DATA.Where(i => i.MSPD_FK_MT_SPEC == data.MSP_ROW_ID).ToList();
                            if (_searchData != null)
                            {
                                context.MT_SPEC_DATA.RemoveRange(_searchData);
                            }

                            context.MT_SPEC.Remove(_search);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        throw new ArgumentException("System cannot delete, this transaction has been used.");
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static MT_SPEC GetSpec(string material, string country)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_SPEC
                             //where v.MSP_FK_MATERIALS == material && v.MSP_FK_COUNTRY == country
                             orderby v.MSP_CREATED_DATE descending
                             select v).FirstOrDefault();
                return query;
            }        
        }



        public static int GetMaxVersion()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_SPEC                            
                             select v).ToList()
                             .Select(i=> int.Parse(i.MSP_VERSION)).OrderByDescending(i=>i).FirstOrDefault();
                return query != 0 ? query : 0;
            }
        }
    }
}
