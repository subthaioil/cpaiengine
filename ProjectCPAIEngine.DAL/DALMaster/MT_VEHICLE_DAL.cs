﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_VEHICLE_DAL
    {
        public void Save(MT_VEHICLE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_VEHICLE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_VEHICLE data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_VEHICLE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VEHICLE data)
        {
            try
            {
                
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_VEHICLE.Find(data.VEH_ID);

                    #region Set Value
                    _search.VEH_VEHICLE = data.VEH_VEHICLE;
                    _search.VEH_TYPE = data.VEH_TYPE;
                    _search.VEH_NRTUS = data.VEH_NRTUS;
                    _search.VEH_VOL_UOM = data.VEH_VOL_UOM;
                    _search.VEH_WGT_UOM = data.VEH_WGT_UOM;
                    _search.VEH_DIM_UOM = data.VEH_DIM_UOM;
                    _search.VEH_MAXWGT = data.VEH_MAXWGT;
                    _search.VEH_UNLWGT = data.VEH_UNLWGT;
                    _search.VEH_MAXVOL = data.VEH_MAXVOL;
                    _search.VEH_HEIGHT = data.VEH_HEIGHT;
                    _search.VEH_WIDTH = data.VEH_WIDTH;
                    _search.VEH_LENGTH = data.VEH_LENGTH;
                    _search.VEH_DEPOT = data.VEH_DEPOT;
                    _search.VEH_NAME_EFF_DATE = data.VEH_NAME_EFF_DATE;
                    _search.VEH_CLF_EFDT = data.VEH_CLF_EFDT;
                    _search.VEH_REG_CNTRY = data.VEH_REG_CNTRY;
                    _search.VEH_REG_DATE = data.VEH_REG_DATE;
                    _search.VEH_OWNER = data.VEH_OWNER;
                    _search.VEH_REG_OWNER = data.VEH_REG_OWNER;
                    _search.VEH_OWN_FLAG = data.VEH_OWN_FLAG;
                    _search.VEH_BALLCAP = data.VEH_BALLCAP;
                    _search.VEH_FL_BNK_CAPA = data.VEH_FL_BNK_CAPA;
                    _search.VEH_DSL_BNK_CAPA = data.VEH_DSL_BNK_CAPA;
                    _search.VEH_CLASS_GRP = data.VEH_CLASS_GRP;
                    _search.VEH_DRAFT = data.VEH_DRAFT;
                    _search.VEH_POINT = data.VEH_POINT;
                    _search.VEH_CRE_DATE = data.VEH_CRE_DATE;
                    _search.VEH_CRE_NAME = data.VEH_CRE_NAME;
                    _search.VEH_CHA_DATE = data.VEH_CHA_DATE;
                    _search.VEH_CHA_NAME = data.VEH_CHA_NAME;
                    _search.VEH_IMO_NUMBER = data.VEH_IMO_NUMBER;
                    _search.VEH_CLASS_SOCIETY = data.VEH_CLASS_SOCIETY;
                    _search.VEH_INSERT_DATE = data.VEH_INSERT_DATE;
                    _search.VEH_STATUS = data.VEH_STATUS;
                    //_search.VEH_CREATED_DATE = data.VEH_CREATED_DATE;
                    //_search.VEH_CREATED_BY = data.VEH_CREATED_BY;
                    _search.VEH_UPDATED_DATE = data.VEH_UPDATED_DATE;
                    _search.VEH_UPDATED_BY = data.VEH_UPDATED_BY;
                    _search.VEH_LANGUAGE = data.VEH_LANGUAGE;
                    _search.VEH_VEH_TEXT = data.VEH_VEH_TEXT;
                    _search.VEH_GROUP = data.VEH_GROUP;
                    _search.VEH_VESSEL_CATEGORY = data.VEH_VESSEL_CATEGORY;
                    _search.VEH_BUILT = data.VEH_BUILT;
                    _search.VEH_AGE = data.VEH_AGE;
                    _search.VHE_DWT = data.VHE_DWT;
                    _search.VHE_CAPACITY = data.VHE_CAPACITY;
                    _search.VHE_MAX_DRAFT = data.VHE_MAX_DRAFT;
                    _search.VHE_COATING = data.VHE_COATING;
                    _search.VHE_FULL_BALLAST = data.VHE_FULL_BALLAST;
                    _search.VHE_FULL_LADEN = data.VHE_FULL_LADEN;
                    _search.VHE_ECO_BALLAST = data.VHE_ECO_BALLAST;
                    _search.VHE_ECO_LADEN = data.VHE_ECO_LADEN;
                    _search.VHE_CREATE_TYPE = data.VHE_CREATE_TYPE;

                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VEHICLE data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.MT_VEHICLE.Find(data.VEH_ID);
                #region Set Value
                _search.VEH_VEHICLE = data.VEH_VEHICLE;
                _search.VEH_TYPE = data.VEH_TYPE;
                _search.VEH_NRTUS = data.VEH_NRTUS;
                _search.VEH_VOL_UOM = data.VEH_VOL_UOM;
                _search.VEH_WGT_UOM = data.VEH_WGT_UOM;
                _search.VEH_DIM_UOM = data.VEH_DIM_UOM;
                _search.VEH_MAXWGT = data.VEH_MAXWGT;
                _search.VEH_UNLWGT = data.VEH_UNLWGT;
                _search.VEH_MAXVOL = data.VEH_MAXVOL;
                _search.VEH_HEIGHT = data.VEH_HEIGHT;
                _search.VEH_WIDTH = data.VEH_WIDTH;
                _search.VEH_LENGTH = data.VEH_LENGTH;
                _search.VEH_DEPOT = data.VEH_DEPOT;
                _search.VEH_NAME_EFF_DATE = data.VEH_NAME_EFF_DATE;
                _search.VEH_CLF_EFDT = data.VEH_CLF_EFDT;
                _search.VEH_REG_CNTRY = data.VEH_REG_CNTRY;
                _search.VEH_REG_DATE = data.VEH_REG_DATE;
                _search.VEH_OWNER = data.VEH_OWNER;
                _search.VEH_REG_OWNER = data.VEH_REG_OWNER;
                _search.VEH_OWN_FLAG = data.VEH_OWN_FLAG;
                _search.VEH_BALLCAP = data.VEH_BALLCAP;
                _search.VEH_FL_BNK_CAPA = data.VEH_FL_BNK_CAPA;
                _search.VEH_DSL_BNK_CAPA = data.VEH_DSL_BNK_CAPA;
                _search.VEH_CLASS_GRP = data.VEH_CLASS_GRP;
                _search.VEH_DRAFT = data.VEH_DRAFT;
                _search.VEH_POINT = data.VEH_POINT;
                _search.VEH_CRE_DATE = data.VEH_CRE_DATE;
                _search.VEH_CRE_NAME = data.VEH_CRE_NAME;
                _search.VEH_CHA_DATE = data.VEH_CHA_DATE;
                _search.VEH_CHA_NAME = data.VEH_CHA_NAME;
                _search.VEH_IMO_NUMBER = data.VEH_IMO_NUMBER;
                _search.VEH_CLASS_SOCIETY = data.VEH_CLASS_SOCIETY;
                _search.VEH_INSERT_DATE = data.VEH_INSERT_DATE;
                _search.VEH_STATUS = data.VEH_STATUS;
                //_search.VEH_CREATED_DATE = data.VEH_CREATED_DATE;
                //_search.VEH_CREATED_BY = data.VEH_CREATED_BY;
                _search.VEH_UPDATED_DATE = data.VEH_UPDATED_DATE;
                _search.VEH_UPDATED_BY = data.VEH_UPDATED_BY;
                _search.VEH_LANGUAGE = data.VEH_LANGUAGE;
                _search.VEH_VEH_TEXT = data.VEH_VEH_TEXT;
                _search.VEH_GROUP = data.VEH_GROUP;
                _search.VEH_VESSEL_CATEGORY = data.VEH_VESSEL_CATEGORY;
                _search.VEH_BUILT = data.VEH_BUILT;
                _search.VEH_AGE = data.VEH_AGE;
                _search.VHE_DWT = data.VHE_DWT;
                _search.VHE_CAPACITY = data.VHE_CAPACITY;
                _search.VHE_MAX_DRAFT = data.VHE_MAX_DRAFT;
                _search.VHE_COATING = data.VHE_COATING;
                _search.VHE_FULL_BALLAST = data.VHE_FULL_BALLAST;
                _search.VHE_FULL_LADEN = data.VHE_FULL_LADEN;
                _search.VHE_ECO_BALLAST = data.VHE_ECO_BALLAST;
                _search.VHE_ECO_LADEN = data.VHE_ECO_LADEN;
                _search.VHE_CREATE_TYPE = data.VHE_CREATE_TYPE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string VEH_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VEHICLE WHERE VEH_ID = '" + VEH_ID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string VEH_ID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_VEHICLE WHERE VEH_ID = '" + VEH_ID + "'");
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
