﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_VENDOR_DAL
    {
        public void Save(MT_VENDOR data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_VENDOR.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_VENDOR data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_VENDOR.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VENDOR data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_VENDOR.Find(data.VND_ACC_NUM_VENDOR);
                    #region Set Value
                    _search.VND_NAME1 = data.VND_NAME1;
                    _search.VND_BROKER_COMMISSION = data.VND_BROKER_COMMISSION;
                    _search.VND_COUNTRY_KEY = data.VND_COUNTRY_KEY;
                    _search.VND_CREATE_TYPE = data.VND_CREATE_TYPE;
                    _search.VND_STATUS = data.VND_STATUS;
                    _search.VND_UPDATED_BY = data.VND_UPDATED_BY;
                    _search.VND_UPDATED_DATE = data.VND_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VENDOR data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_VENDOR.Find(data.VND_ACC_NUM_VENDOR);
                #region Set Value
                _search.VND_NAME1 = data.VND_NAME1;
                _search.VND_BROKER_COMMISSION = data.VND_BROKER_COMMISSION;
                _search.VND_COUNTRY_KEY = data.VND_COUNTRY_KEY;
                _search.VND_CREATE_TYPE = data.VND_CREATE_TYPE;
                _search.VND_STATUS = data.VND_STATUS;
                _search.VND_CONTRACT = data.VND_CONTRACT;
                _search.VND_UPDATED_BY = data.VND_UPDATED_BY;
                _search.VND_UPDATED_DATE = data.VND_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(MT_VENDOR data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_VENDOR.Find(data.VND_ACC_NUM_VENDOR);
                #region Set Value
                _search.VND_STATUS = data.VND_STATUS;
                _search.VND_UPDATED_BY = data.VND_UPDATED_BY;
                _search.VND_UPDATED_DATE = data.VND_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string VendorCode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VENDOR WHERE VND_ACC_NUM_VENDOR = '" + VendorCode + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string VendorCode, EntityCPAIEngine context)
        {
            try
            {
                   
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VENDOR WHERE VND_ACC_NUM_VENDOR = '" + VendorCode + "'");
                            context.SaveChanges();  
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                        }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Data MT_VENDOR by Status
        /// </summary>
        /// <param name="Status">status ("ACTIVE","INACTIVE") set string emply or nul get all</param>
        /// <returns></returns>
        public static List<MT_VENDOR> GetVerndorData(string sSystem, string sCode, string sName, string sCountry, string sCreateType, string sStatus)
        {
          
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                // Select All
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == sSystem
                            && vc.MVC_TYPE.Contains(sCode)
                            select v;

                if (!string.IsNullOrEmpty(sCode))
                    query = query.Where(p => p.VND_ACC_NUM_VENDOR.Contains(sCode));
                if (!string.IsNullOrEmpty(sName))
                    query = query.Where(p => p.VND_NAME1.Contains(sName));

                if (!string.IsNullOrEmpty(sCountry))
                    query = query.Where(p => p.VND_COUNTRY_KEY.Contains(sCountry));

                if (!string.IsNullOrEmpty(sCreateType))
                    query = query.Where(p => p.VND_CREATE_TYPE.Contains(sCreateType));

                if (!string.IsNullOrEmpty(sStatus))
                    query = query.Where(p => p.VND_STATUS.Contains(sStatus));

                if (query != null)
                    return query.ToList();
                else
                    return null;
            }
        }

        public static string GetNameShort(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = from v in context.MT_VENDOR
                                where v.VND_ACC_NUM_VENDOR.ToUpper() == id.ToUpper()
                                select v;

                    return query.FirstOrDefault().VND_SORT_FIELD;
                }
            } else
            {
                return "";
            }
            
        }
    }
}
