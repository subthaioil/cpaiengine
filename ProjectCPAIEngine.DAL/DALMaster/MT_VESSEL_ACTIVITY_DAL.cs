﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_VESSEL_ACTIVITY_DAL
    {
        public void Save(MT_VESSEL_ACTIVITY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_VESSEL_ACTIVITY.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_VESSEL_ACTIVITY data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_VESSEL_ACTIVITY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VESSEL_ACTIVITY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_VESSEL_ACTIVITY.Find(data.MVA_ROW_ID);
                    #region Set Value
                    _search.MVA_CODE = data.MVA_CODE;
                    _search.MVA_STANDARD_PHRASE_EN = data.MVA_STANDARD_PHRASE_EN;
                    _search.MVA_STANDARD_PHRASE_TH = data.MVA_STANDARD_PHRASE_TH;
                    _search.MVA_KEY = data.MVA_KEY;
                    _search.MVA_STD_TIME = data.MVA_STD_TIME;
                    _search.MVA_MIN_USE = data.MVA_MIN_USE;
                    _search.MVA_REF = data.MVA_REF;

                    _search.MVA_UPDATED_BY = data.MVA_UPDATED_BY;
                    _search.MVA_UPDATED_DATE = data.MVA_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VESSEL_ACTIVITY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_VESSEL_ACTIVITY.Find(data.MVA_ROW_ID);
                #region Set Value
                _search.MVA_CODE = data.MVA_CODE;
                _search.MVA_STANDARD_PHRASE_EN = data.MVA_STANDARD_PHRASE_EN;
                _search.MVA_STANDARD_PHRASE_TH = data.MVA_STANDARD_PHRASE_TH;
                _search.MVA_KEY = data.MVA_KEY;
                _search.MVA_STD_TIME = data.MVA_STD_TIME;
                _search.MVA_MIN_USE = data.MVA_MIN_USE;
                _search.MVA_REF = data.MVA_REF;
                _search.MVA_UPDATED_BY = data.MVA_UPDATED_BY;
                _search.MVA_UPDATED_DATE = data.MVA_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VESSEL_ACTIVITY WHERE MVA_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                   
                        try
                        {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_VESSEL_ACTIVITY WHERE MVA_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();  
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                        }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///// <summary>
        ///// Get Data MT_VESSEL_ACTIVITY by Status
        ///// </summary>
        ///// <param name="Status">status ("ACTIVE","INACTIVE") set string emply or nul get all</param>
        ///// <returns></returns>
        //public static List<MT_VESSEL_ACTIVITY> GetVerndorData(string sSystem, string sCode, string sName, string sCountry, string sCreateType, string sStatus)
        //{
          
        //    using (EntityCPAIEngine context = new EntityCPAIEngine())
        //    {
        //        // Select All
        //        var query = from v in context.MT_VESSEL_ACTIVITY
        //                    join vc in context.MT_VESSEL_ACTIVITY_CONTROL on v.MVA_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
        //                    where vc.MVC_SYSTEM.ToUpper() == sSystem
        //                    select v;

        //        if (!string.IsNullOrEmpty(sCode))
        //            query = query.Where(p => p.MVA_ACC_NUM_VENDOR.Contains(sCode));

        //        if (!string.IsNullOrEmpty(sName))
        //            query = query.Where(p => p.MVA_NAME1.Contains(sName));

        //        if (!string.IsNullOrEmpty(sCountry))
        //            query = query.Where(p => p.MVA_COUNTRY_KEY.Contains(sCountry));

        //        if (!string.IsNullOrEmpty(sCreateType))
        //            query = query.Where(p => p.MVA_CREATE_TYPE.Contains(sCreateType));

        //        if (!string.IsNullOrEmpty(sStatus))
        //            query = query.Where(p => p.MVA_STATUS.Contains(sStatus));

        //        if (query != null)
        //            return query.ToList();
        //        else
        //            return null;

        //    }
        //}

    }
}
