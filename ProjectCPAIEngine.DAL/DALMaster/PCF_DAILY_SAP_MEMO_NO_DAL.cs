﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PCF_DAILY_SAP_MEMO_NO_DAL
    {
        public void Save(PCF_DAILY_SAP_MEMO_NO data)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    context.PCF_DAILY_SAP_MEMO_NO.Add(data);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(PCF_DAILY_SAP_MEMO_NO data)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var _search = context.PCF_DAILY_SAP_MEMO_NO.Find(data.MEMO_NO);
                    _search.TRIP_NO = data.TRIP_NO;
                    _search.LOADING_DATE = data.LOADING_DATE;
                    _search.MAT_ITEM_NO = data.MAT_ITEM_NO;
                    _search.UPDATED_DATE = data.UPDATED_DATE;
                    _search.UPDATED_BY = data.UPDATED_BY;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMemoFreight(string tripno, string itemno, string memono)
        {

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sql = "";
                    sql = "UPDATE PCF_FREIGHT SET ";
                    sql += "PFR_MEMO_NO='" + memono + "' ";
                    sql += " WHERE PFR_TRIP_NO='" + tripno + "' and PFR_ITEM_NO = " + itemno;

                    context.Database.ExecuteSqlCommand(sql);

                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertMemoCrude(string tripno, string itemno, string memono, string username, string loadingdate)
        {

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sql = "";
                    sql = "INSERT INTO PCF_DAILY_SAP_MEMO_NO (MEMO_NO, TRIP_NO, MAT_ITEM_NO, LOADING_DATE, CREATED_BY, UPDATED_BY, CREATED_DATE, UPDATED_DATE) VALUES ";
                    sql += "('" + memono + "', '" + tripno + "', " + itemno + ", TO_DATE('" + loadingdate + "', 'yyyy-mm-dd'), '" + username + "', '" + username + "', sysdate, sysdate )";

                    context.Database.ExecuteSqlCommand(sql);

                    sql = "UPDATE PCF_MATERIAL SET ";
                    sql += " PMA_MEMO_NO='" + memono + "'";
                    sql += " WHERE PMA_TRIP_NO='" + tripno + "' and PMA_ITEM_NO= " + itemno;

                    context.Database.ExecuteSqlCommand(sql);



                    context.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMemoCrude(string tripno, string itemno, string memono, string username, string loadingdate, string isdel)
        {

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sql = "";
                    sql = "UPDATE PCF_DAILY_SAP_MEMO_NO SET ";
                    sql += "  TRIP_NO = '" + tripno + "' ";
                    sql += " , MAT_ITEM_NO = '" + itemno + "' ";
                    sql += " , IS_DELETE= '" + isdel + "' ";

                    if (loadingdate != "")
                        sql += " , LOADING_DATE = TO_DATE('" + loadingdate + "', 'yyyy-mm-dd') ";

                    //sql += " , CREATED_BY = '" + username + "' ";
                    sql += " , UPDATED_BY = '" + username + "' ";
                    //sql += " , CREATED_DATE = sysdate ";
                    sql += " , UPDATED_DATE = sysdate ";
                    sql += " WHERE MEMO_NO = '" + memono + "' ";

                    context.Database.ExecuteSqlCommand(sql);

                    sql = "UPDATE PCF_MATERIAL SET ";
                    if (isdel.Equals("X"))
                        sql += " PMA_MEMO_NO=''";
                    else
                        sql += " PMA_MEMO_NO='" + memono + "'";

                    sql += " WHERE PMA_TRIP_NO='" + tripno + "' and PMA_ITEM_NO= " + itemno;

                    context.Database.ExecuteSqlCommand(sql);

                    context.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMemoOther(string tripno, string itemno, string memono)
        {

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sql = "";
                    sql = "UPDATE PCF_OTHER_COST SET ";
                    sql += " POC_MEMO_NO='" + memono + "' ";
                    sql += " WHERE POC_TRIP_NO='" + tripno + "' and POC_ITEM_NO = " + itemno;

                    context.Database.ExecuteSqlCommand(sql);

                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
