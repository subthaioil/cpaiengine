﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PCF_PURCHASE_ENTRY_DAILY_DAL
    {
        public void Save(PCF_PURCHASE_ENTRY_DAILY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PCF_PURCHASE_ENTRY_DAILY.Find(data.TRIP_NO, data.MAT_ITEM_NO, data.ITEM_NO);
                    if (search == null) //Add new record
                    {
                        context.PCF_PURCHASE_ENTRY_DAILY.Add(data);
                        context.SaveChanges();
                    }
                    else //Update the existing record
                    {
                        search.BL_VOLUME_BBL = data.BL_VOLUME_BBL;
                        search.BL_VOLUME_MT = data.BL_VOLUME_MT;
                        search.BL_VOLUME_L30 = data.BL_VOLUME_L30;
                        search.GR_VOLUME_BBL = data.GR_VOLUME_BBL;
                        search.GR_VOLUME_MT = data.GR_VOLUME_MT;
                        search.GR_VOLUME_L30 = data.GR_VOLUME_L30;
                        search.FOB_AMOUNT_USD = data.FOB_AMOUNT_USD;
                        search.FOB_AMOUNT_THB = data.FOB_AMOUNT_THB;
                        search.FRT_AMOUNT_USD = data.FRT_AMOUNT_USD;
                        search.FRT_AMOUNT_THB = data.FRT_AMOUNT_THB;
                        search.CF_AMOUNT_USD = data.CF_AMOUNT_USD;
                        search.CF_AMOUNT_THB = data.CF_AMOUNT_THB;
                        search.CF_TOTAL_USD = data.CF_TOTAL_USD;
                        search.CF_TOTAL_THB = data.CF_TOTAL_THB;
                        search.CF_VAT = data.CF_VAT;
                        search.CAL_INVOICE_FIGURE = data.CAL_INVOICE_FIGURE;
                        search.CAL_PRICE_UNIT = data.CAL_PRICE_UNIT;
                        search.CAL_VOLUME_UNIT = data.CAL_VOLUME_UNIT;
                        search.FOB_PRICE = data.FOB_PRICE;
                        search.FRT_PRICE = data.FRT_PRICE;
                        search.CF_PRICE = data.CF_PRICE;
                        search.ROE = data.ROE;

                        context.SaveChanges();
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void SaveReversedFiDoc(PCF_DAILY_REVERSED_FI_DOC data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PCF_DAILY_REVERSED_FI_DOC.Find(data.TRIP_NO, data.MAT_ITEM_NO, data.ITEM_NO);
                    if (search == null) //Add new record
                    {
                        context.PCF_DAILY_REVERSED_FI_DOC.Add(data);
                        context.SaveChanges();
                    }
                    else //Update the existing record
                    {

                        search.ITEM_NO = data.ITEM_NO;
                        search.MAT_ITEM_NO = data.MAT_ITEM_NO;
                        search.PO_DATE = data.PO_DATE;
                        search.REVERSAL_REASON = data.REVERSAL_REASON;
                        search.SAP_FI_DOC_NO = data.SAP_FI_DOC_NO;
                        search.SAP_REVERSED_DOC_NO = data.SAP_REVERSED_DOC_NO;
                        search.TRIP_NO = data.TRIP_NO;
                        search.UPDATED_BY = data.UPDATED_BY;
                        search.UPDATED_DATE = data.UPDATED_DATE;
                        search.CREATED_BY = data.CREATED_BY;
                        search.CREATED_DATE = data.CREATED_DATE;

                        context.SaveChanges();
                    }

                    var pcf_daily = context.PCF_PURCHASE_ENTRY_DAILY.Find(data.TRIP_NO, data.MAT_ITEM_NO, data.ITEM_NO);
                    if (pcf_daily != null)
                    {
                        pcf_daily.SAP_FI_DOC_NO = "";
                        pcf_daily.SAP_FI_DOC_STATUS = "";
                        pcf_daily.UPDATED_BY = data.UPDATED_BY;
                        pcf_daily.UPDATED_DATE = data.UPDATED_DATE;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Remove(PCF_PURCHASE_ENTRY_DAILY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PCF_PURCHASE_ENTRY_DAILY.Find(data.TRIP_NO, data.MAT_ITEM_NO, data.ITEM_NO);
                    if (search != null)
                    {
                        context.PCF_PURCHASE_ENTRY_DAILY.Remove(search);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSapFiDocNo(string tripNo, string matItemNo, string itemNo, string sapFiDocNo, string userName, string type)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PCF_PURCHASE_ENTRY_DAILY.Find(tripNo, Convert.ToDecimal(matItemNo), Convert.ToDecimal(itemNo));
                    if (search != null) //Add new record
                    {
                        string status = "A";
                        if (type == "C")
                        {
                            sapFiDocNo = sapFiDocNo.Substring(0, sapFiDocNo.Length - 8);
                            status = "C";
                        }
                        
                        search.SAP_FI_DOC_NO = sapFiDocNo;
                        search.SAP_FI_DOC_STATUS = status;
                        //search.SAP_FI_DOC_STATUS = "Created";
                        search.UPDATED_DATE = DateTime.Now;
                        search.UPDATED_BY = userName;
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("Cannot find Trip No = " + tripNo + ", MAT Item No = " + matItemNo + ", ITEM-NO = " + itemNo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
