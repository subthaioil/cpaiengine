﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PCF_VAT_REVERSED_FI_DOC_DAL
    {
        public void Save(PCF_VAT_REVERSED_FI_DOC data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PCF_VAT_REVERSED_FI_DOC.Add(data);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
