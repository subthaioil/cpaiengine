﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PIT_SAP_MEMO_NO_DAL
    {
        public void Save(PIT_SAP_MEMO_NO data)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    context.PIT_SAP_MEMO_NO.Add(data);
                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
