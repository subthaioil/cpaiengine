﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PRICING_TO_SAP_DAL
    {
        public void Save(PRICING_TO_SAP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PRICING_TO_SAP.Add(data);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PRICING_TO_SAP data, EntityCPAIEngine context)
        {
            try
            {
                context.PRICING_TO_SAP.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
