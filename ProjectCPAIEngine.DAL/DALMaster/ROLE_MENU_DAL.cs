﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class ROLE_MENU_DAL
    {
        public void Save(ROLE_MENU data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.ROLE_MENU.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(ROLE_MENU data, EntityCPAIEngine context)
        {
            try
            {
                context.ROLE_MENU.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ROLE_MENU data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.ROLE_MENU.Find(data.RMN_ROW_ID);
                    #region Set Value
                    _search.RMN_FK_ROLE = data.RMN_FK_ROLE;
                    _search.RMN_FK_MENU = data.RMN_FK_MENU;
                    _search.RMN_UPDATED_BY = data.RMN_UPDATED_BY;
                    _search.RMN_UPDATED_DATE = data.RMN_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ROLE_MENU data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.ROLE_MENU.Find(data.RMN_ROW_ID);
                #region Set Value
                _search.RMN_FK_ROLE = data.RMN_FK_ROLE;
                _search.RMN_FK_MENU = data.RMN_FK_MENU;
                _search.RMN_UPDATED_BY = data.RMN_UPDATED_BY;
                _search.RMN_UPDATED_DATE = data.RMN_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM ROLE_MENU WHERE RMN_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM ROLE_MENU WHERE RMN_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAll(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM ROLE_MENU WHERE RMN_FK_ROLE = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAll(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM ROLE_MENU WHERE RMN_FK_ROLE = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
