﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
   public class USERS_DAL
    {
        public USERS GetUserByUserName(string pUserName) {
            USERS rslt = null;
            using (var context = new EntityCPAIEngine()) {
                if (!string.IsNullOrEmpty(pUserName))
                    rslt = context.USERS.Single(p => p.USR_LOGIN.ToUpper().Equals(pUserName.ToUpper()));
            }
            return rslt;
        }

        public ROLE GetUserRoleByUserName(string pUserName)
        {
            ROLE rslt = null;
            using (var context = new EntityCPAIEngine())
            {
                if (!string.IsNullOrEmpty(pUserName))
                {
                    USERS user_rslt = context.USERS.Single(p => p.USR_LOGIN.ToUpper().Equals(pUserName.ToUpper()));
                    if (user_rslt != null)
                    {
                        var role = context.USER_ROLE.Where(x=>x.URO_FK_USER == user_rslt.USR_ROW_ID).FirstOrDefault();
                        if (role != null)
                        {
                            rslt = context.ROLE.Where(y => y.ROL_ROW_ID == role.URO_FK_ROLE).FirstOrDefault();
                        }
                    }
                }
            }
            return rslt;
        }

        public static string GetWebMenuByUserName(string pUserName)
        {
            string web_menu = "";
            using (var context = new EntityCPAIEngine())
            {
                if (!string.IsNullOrEmpty(pUserName))
                {
                    USERS rslt = null;
                    rslt = context.USERS.Where(p => p.USR_LOGIN.ToUpper().Equals(pUserName.ToUpper())).FirstOrDefault();
                    if (rslt != null)
                    {
                        web_menu = rslt.USR_WEB_MENU;
                    }
                }
            }
            return web_menu;
        }

        public void Save(USERS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.USERS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(USERS data, EntityCPAIEngine context)
        {
            try
            {
                context.USERS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(USERS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.USERS.Find(data.USR_ROW_ID);
                    #region Set Value
                    _search.USR_TITLE_EN = data.USR_TITLE_EN;
                    _search.USR_TITLE_COUNTRY = data.USR_TITLE_COUNTRY;
                    _search.USR_FIRST_NAME_EN = data.USR_FIRST_NAME_EN;
                    _search.USR_FIRST_NAME_COUNTRY = data.USR_FIRST_NAME_COUNTRY;
                    _search.USR_LAST_NAME_EN = data.USR_LAST_NAME_EN;
                    _search.USR_LAST_NAME_COUNTRY = data.USR_LAST_NAME_COUNTRY;
                    _search.USR_LOGIN = data.USR_LOGIN;
                    _search.USR_PASSWORD = data.USR_PASSWORD;
                    _search.USR_AD_LOGIN_FLAG = data.USR_AD_LOGIN_FLAG;
                    _search.USR_COMPANY = data.USR_COMPANY;
                    _search.USR_EMPLOYEE_ID = data.USR_EMPLOYEE_ID;
                    _search.USR_EMAIL = data.USR_EMAIL;
                    _search.USR_NOTI_ID = data.USR_NOTI_ID;
                    _search.USR_DEPARTMENT = data.USR_DEPARTMENT;
                    _search.USR_DIVISION = data.USR_DIVISION;
                    _search.USR_SECTION  = data.USR_SECTION;
                    _search.USR_FUCNTION = data.USR_FUCNTION;
                    _search.USR_UNIT = data.USR_UNIT;
                    _search.USR_SYSTEM = data.USR_SYSTEM;
                    _search.USR_STATUS = data.USR_STATUS;
                    _search.USR_SIGNATURE_PATH = data.USR_SIGNATURE_PATH;
                    _search.USR_SECTION_HEAD = data.USR_SECTION_HEAD;
                    _search.USR_SIGNATURE_PATH = data.USR_SIGNATURE_PATH;
                    _search.USR_OS = data.USR_OS;
                    _search.USR_NOTI_TOPICS = data.USR_NOTI_TOPICS;
                    _search.USR_APP_VERSION = data.USR_APP_VERSION;
                    _search.USR_MOBILE_FLAG = data.USR_MOBILE_FLAG;
                    _search.USR_NOTI_FLAG = data.USR_NOTI_FLAG;
                    _search.USR_EMAIL_FLAG = data.USR_EMAIL_FLAG;
                    _search.USR_SMS_FLAG = data.USR_SMS_FLAG;
                    _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                    _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;

                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(USERS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USERS.Find(data.USR_ROW_ID);
                #region Set Value
                _search.USR_TITLE_EN = data.USR_TITLE_EN;
                _search.USR_TITLE_COUNTRY = data.USR_TITLE_COUNTRY;
                _search.USR_FIRST_NAME_EN = data.USR_FIRST_NAME_EN;
                _search.USR_FIRST_NAME_COUNTRY = data.USR_FIRST_NAME_COUNTRY;
                _search.USR_LAST_NAME_EN = data.USR_LAST_NAME_EN;
                _search.USR_LAST_NAME_COUNTRY = data.USR_LAST_NAME_COUNTRY;
                _search.USR_LOGIN = data.USR_LOGIN;
                _search.USR_PASSWORD = data.USR_PASSWORD;
                _search.USR_AD_LOGIN_FLAG = data.USR_AD_LOGIN_FLAG;
                _search.USR_COMPANY = data.USR_COMPANY;
                _search.USR_EMPLOYEE_ID = data.USR_EMPLOYEE_ID;
                _search.USR_EMAIL = data.USR_EMAIL;
                _search.USR_NOTI_ID = data.USR_NOTI_ID;
                _search.USR_DEPARTMENT = data.USR_DEPARTMENT;
                _search.USR_DIVISION = data.USR_DIVISION;
                _search.USR_SECTION = data.USR_SECTION;
                _search.USR_FUCNTION = data.USR_FUCNTION;
                _search.USR_UNIT = data.USR_UNIT;
                _search.USR_SYSTEM = data.USR_SYSTEM;
                _search.USR_STATUS = data.USR_STATUS;
                _search.USR_SIGNATURE_PATH = data.USR_SIGNATURE_PATH;
                _search.USR_SECTION_HEAD = data.USR_SECTION_HEAD;
                _search.USR_SIGNATURE_PATH = data.USR_SIGNATURE_PATH;
                _search.USR_OS = data.USR_OS;
                _search.USR_NOTI_TOPICS = data.USR_NOTI_TOPICS;
                _search.USR_APP_VERSION = data.USR_APP_VERSION;
                _search.USR_MOBILE_FLAG = data.USR_MOBILE_FLAG;
                _search.USR_NOTI_FLAG = data.USR_NOTI_FLAG;
                _search.USR_EMAIL_FLAG = data.USR_EMAIL_FLAG;
                _search.USR_SMS_FLAG = data.USR_SMS_FLAG;
                _search.USR_MOBILE_MENU = data.USR_MOBILE_MENU;
                _search.USR_WEB_MENU = data.USR_WEB_MENU;
                _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateStatus(USERS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USERS.Find(data.USR_ROW_ID);
                #region Set Value
                _search.USR_STATUS = data.USR_STATUS;
                _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEmailFlag(USERS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USERS.Find(data.USR_ROW_ID);
                #region Set Value
                _search.USR_EMAIL_FLAG = data.USR_EMAIL_FLAG;
                _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSMSFlag(USERS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USERS.Find(data.USR_ROW_ID);
                #region Set Value
                _search.USR_SMS_FLAG= data.USR_SMS_FLAG;
                _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateNotiFlag(USERS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USERS.Find(data.USR_ROW_ID);
                #region Set Value
                _search.USR_NOTI_FLAG = data.USR_NOTI_FLAG;
                _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMobileFlag(USERS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USERS.Find(data.USR_ROW_ID);
                #region Set Value
                _search.USR_MOBILE_FLAG = data.USR_MOBILE_FLAG;
                _search.USR_UPDATED_BY = data.USR_UPDATED_BY;
                _search.USR_UPDATED_DATE = data.USR_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM USERS WHERE USR_ROW_ID = '" + id + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string id, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM USERS WHERE USR_ROW_ID = '" + id + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Data MT_VENDOR by Status
        /// </summary>
        /// <param name="Status">status ("ACTIVE","INACTIVE") set string emply or nul get all</param>
        /// <returns></returns>
        
    }
}
