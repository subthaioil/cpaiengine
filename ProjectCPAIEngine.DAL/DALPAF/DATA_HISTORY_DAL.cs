﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class DATA_HISTORY_DAL
    {
        public CPAI_DATA_HISTORY findByDthRowId(String dthRowId)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_ROW_ID == dthRowId).ToList();
                if (results != null && (results.Count() == 1))
                {
                    return results[0];
                }
                else
                {
                    return null;
                }
            }
        }

        public List<CPAI_DATA_HISTORY> findByDthTxnRef(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }

        public List<CPAI_DATA_HISTORY> findApproveByDthTxnRef(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef && x.DTH_REJECT_FLAG == "N").ToList();
                if (results != null)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }
        
        public void UpdateRejectFlag(string txRef, string updateBy, string flag)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    DateTime now = DateTime.Now;

                    string sql = "UPDATE CPAI_DATA_HISTORY SET DTH_REJECT_FLAG = :Flag, DTH_UPDATED_BY = :UpdateBy, DTH_UPDATED_DATE = TO_DATE ('" + DateTime.Now.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')  WHERE DTH_TXN_REF = :TxRef";
                    sql = "UPDATE CPAI_DATA_HISTORY SET DTH_REJECT_FLAG = :DTH_REJECT_FLAG, DTH_UPDATED_BY = :DTH_UPDATED_BY, DTH_UPDATED_DATE = SYSDATE WHERE DTH_TXN_REF = :DTH_TXN_REF";
                    var result = context.Database.ExecuteSqlCommand(
                       sql,
                       new OracleParameter("DTH_TXN_REF", flag),
                       new OracleParameter("DTH_UPDATED_BY", updateBy),
                       new OracleParameter("DTH_REJECT_FLAG", txRef));

                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_DATA_HISTORY dataHistory)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_DATA_HISTORY.Add(dataHistory); //   .AddObject(dataHistory);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
