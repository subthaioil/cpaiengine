﻿using com.pttict.engine.dal.Entity;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public static class linq_ex
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }

    public class FLOW_MANAGEMENT_DAL : FunctionTransactionDAL
    {
        private const string APPROVAL_FORM_FUNCTION_CODE = "44";


        public List<FunctionTransaction> findCPAIApprovalFormTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate, List<string> units, List<string> areas, string username)
        {
            EntitiesEngine db = new EntitiesEngine();
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            //List<FUNCTION_TRANSACTION> list = db.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == APPROVAL_FORM_FUNCTION_CODE.ToUpper()).ToList();
            string user_group = PAFHelper.GetUserGroup(username, "PAF");
            string query = "SELECT FTX_ROW_ID, FTX_FK_FUNCTION, FTX_TRANS_ID, FTX_APP_ID, FTX_REQ_TRANS, FTX_RETURN_STATUS, FTX_RETURN_NAME_SPACE, FTX_RETURN_CODE, FTX_RETURN_DESC, FTX_STATE_ACTION, FTX_CURRENT_NAME_SPACE, FTX_CURRENT_CODE, FTX_CURRENT_DESC, FTX_REQUEST_DATE, FTX_RESPONSE_DATE, FTX_ACCUM_RETRY, FTX_RETRY_COUNT, FTX_CURRENT_STATE, FTX_NEXT_STATE, FTX_WAKEUP, FTX_FWD_RESP_CODE_FLAG, FTX_CREATED, FTX_CREATED_BY, FTX_UPDATED, FTX_UPDATED_BY, FTX_RETURN_MESSAGE, FTX_PARENT_TRX_ID, FTX_PARENT_ACT_ID, FTX_INDEX1, FTX_INDEX2, FTX_INDEX3, FTX_INDEX4, FTX_INDEX5, FTX_INDEX6, FTX_INDEX7, FTX_INDEX8, FTX_INDEX9, FTX_INDEX10, FTX_INDEX11, FTX_INDEX12, FTX_INDEX13, FTX_INDEX14, FTX_INDEX15, FTX_INDEX16, FTX_INDEX17, FTX_INDEX18, FTX_INDEX19, FTX_INDEX20 FROM FUNCTION_TRANSACTION FT INNER JOIN PAF_DATA PD ON FT.FTX_TRANS_ID = PD.PDA_ROW_ID LEFT JOIN USERS U ON UPPER(PD.PDA_CREATED_BY) = UPPER(U .USR_LOGIN) JOIN CPAI_USER_GROUP cug ON cug.USG_FK_USERS = U .USR_ROW_ID WHERE UPPER(FT.FTX_FK_FUNCTION) = UPPER('44') AND UPPER(SUBSTR(cug.USG_USER_GROUP, 0, :bound)) = UPPER(SUBSTR(:user_group, 0, :bound))";
            //List<FUNCTION_TRANSACTION> list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("user_group", user_group) }).ToList();


            List<FUNCTION_TRANSACTION> list = new List<FUNCTION_TRANSACTION>();
            if (user_group.ToUpper() == "CMVP" || user_group.ToUpper() == "EVPC")
            {
                list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 2), new OracleParameter("user_group", "CM") }).ToList();
            }
            else if (user_group.ToUpper().Contains("CMPS_INTER") || user_group.ToUpper().Contains("CMPS_DOM"))
            {
                list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 11), new OracleParameter("user_group", user_group) }).ToList();
            }
            else
            {
                list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 3), new OracleParameter("user_group", user_group) }).ToList();
            }
            //List<FUNCTION_TRANSACTION> child_list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == COOL_EXPERT_FUNCTION_CODE.ToUpper()
            //    && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW" && x.FTX_PARENT_TRX_ID != null).ToList();

            // find name by index id
            results = findName(list, ACF_FK_FUNCTION);
            
            if (results != null && results.Count() > 0)
            {
                if (lstStatus != null && lstStatus.Count > 0)
                {
                    results = results.Where(x => lstStatus.Contains(x.STATUS)).ToList();
                }
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return results;
                }
                else
                {
                    DateTime fDate = parseInputDate(fromDate);
                    DateTime tDate = parseInputDate(toDate);
                    var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_CREATED != null && (a.FUNCTION_TRANSACTION.FTX_CREATED >= fDate) && (a.FUNCTION_TRANSACTION.FTX_CREATED <= tDate) select a;
                    return realResult.ToList();
                }
            }
            else
            {
                return new List<FunctionTransaction>();
            }
        }

        private List<FunctionTransaction> findName(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                IEnumerable<FunctionTransaction> temp = from ftx in list
                                                        join b in cpaiEntity.PAF_DATA on ftx.FTX_TRANS_ID equals b.PDA_ROW_ID
                                                        select new FunctionTransaction
                                                        {
                                                            FUNCTION_TRANSACTION = ftx,
                                                            //UNIT_STATUS = e != null ? e.COEX_ACTIVATION_STATUS : "",
                                                            //REQUESTER_NAME = d != null ? d.CODA_REQUESTED_BY : "",
                                                            //DRAFT_APPROVER_NAME = d != null ? d.CODA_DRAFT_APPROVED_BY : "",
                                                            //FINAL_APPROVER_NAME = d != null ? d.CODA_FINAL_APPROVED_BY : "",
                                                            //DRAFT_CAM_FILE = d != null ? d.CODA_DRAFT_CAM_PATH : "",
                                                            //FINAL_CAM_FILE = d != null ? d.CODA_FINAL_CAM_PATH : "",
                                                            //DRAFT_SUBMIT_DATE = d != null ? d.CODA_DRAFT_SUBMIT_DATE.HasValue ? d.CODA_DRAFT_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //DRAFT_APPROVE_DATE = d != null ? d.CODA_DRAFT_APPROVE_DATE.HasValue ? d.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_SUBMIT_DATE = d != null ? d.CODA_FINAL_SUBMIT_DATE.HasValue ? d.CODA_FINAL_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_APPROVE_DATE = d != null ? d.CODA_FINAL_APPROVE_DATE.HasValue ? d.CODA_FINAL_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            STATUS = b != null ? b.PDA_STATUS : "",
                                                            NOTE = b != null ? b.PDA_NOTE : "",
                                                            UPDATED_DATE = b != null ? b.PDA_UPDATED.ToString() : "",
                                                            UPDATED_BY = b != null ? b.PDA_UPDATED_BY : "",
                                                            CREATED_DATE = b != null ? b.PDA_CREATED.ToString() : "",
                                                        };

                results = temp.DistinctBy(m => m.FUNCTION_TRANSACTION.FTX_REQ_TRANS).ToList();
                return results;
            }
        }

        public List<FunctionTransaction> findCPAIDAFTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate, string create_by, string username)
        {
            EntitiesEngine db = new EntitiesEngine();
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            //List<FUNCTION_TRANSACTION> list = db.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == APPROVAL_FORM_FUNCTION_CODE.ToUpper()).ToList();
            string user_group = PAFHelper.GetUserGroup(username, "DAF");
            string query = "SELECT FTX_ROW_ID, FTX_FK_FUNCTION, FTX_TRANS_ID, FTX_APP_ID, FTX_REQ_TRANS, FTX_RETURN_STATUS, FTX_RETURN_NAME_SPACE, FTX_RETURN_CODE, FTX_RETURN_DESC, FTX_STATE_ACTION, FTX_CURRENT_NAME_SPACE, FTX_CURRENT_CODE, FTX_CURRENT_DESC, FTX_REQUEST_DATE, FTX_RESPONSE_DATE, FTX_ACCUM_RETRY, FTX_RETRY_COUNT, FTX_CURRENT_STATE, FTX_NEXT_STATE, FTX_WAKEUP, FTX_FWD_RESP_CODE_FLAG, FTX_CREATED, FTX_CREATED_BY, FTX_UPDATED, FTX_UPDATED_BY, FTX_RETURN_MESSAGE, FTX_PARENT_TRX_ID, FTX_PARENT_ACT_ID, FTX_INDEX1, FTX_INDEX2, FTX_INDEX3, FTX_INDEX4, FTX_INDEX5, FTX_INDEX6, FTX_INDEX7, FTX_INDEX8, FTX_INDEX9, FTX_INDEX10, FTX_INDEX11, FTX_INDEX12, FTX_INDEX13, FTX_INDEX14, FTX_INDEX15, FTX_INDEX16, FTX_INDEX17, FTX_INDEX18, FTX_INDEX19, FTX_INDEX20 FROM FUNCTION_TRANSACTION FT INNER JOIN DAF_DATA DD ON FT.FTX_TRANS_ID = DD.DDA_ROW_ID WHERE UPPER(FT.FTX_FK_FUNCTION) = UPPER('80') AND UPPER(SUBSTR(DD.DDA_USER_GROUP, 0, :bound)) = UPPER(SUBSTR(:user_group, 0, :bound))";
            //List<FUNCTION_TRANSACTION> list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("user_group", user_group) }).ToList();

            List<FUNCTION_TRANSACTION> list = new List<FUNCTION_TRANSACTION>();
            if (user_group.ToUpper() == "CMVP" || user_group.ToUpper() == "EVPC")
            {
                list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 2), new OracleParameter("user_group", "CM") }).ToList();
            }
            else
            {
                list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 3), new OracleParameter("user_group", user_group) }).ToList();
            }
            //List<FUNCTION_TRANSACTION> child_list = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_FK_FUNCTION.ToUpper() == COOL_EXPERT_FUNCTION_CODE.ToUpper()
            //    && x.FTX_INDEX4 != null && x.FTX_INDEX4.ToUpper() != "NEW" && x.FTX_PARENT_TRX_ID != null).ToList();

            // find name by index id
            results = findDAFName(list, ACF_FK_FUNCTION);

            if (results != null && results.Count() > 0)
            {
                if (lstStatus != null && lstStatus.Count > 0)
                {
                    results = results.Where(x => lstStatus.Contains(x.STATUS)).ToList();
                }
                if (!string.IsNullOrEmpty(create_by))
                {
                    results = results.Where(x => create_by.Contains(x.CREATED_BY)).ToList();
                }
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return results;
                }
                else
                {
                    DateTime fDate = parseInputDate(fromDate);
                    DateTime tDate = parseInputDate(toDate);
                    var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_CREATED != null && (a.FUNCTION_TRANSACTION.FTX_CREATED >= fDate) && (a.FUNCTION_TRANSACTION.FTX_CREATED <= tDate) select a;
                    return realResult.ToList();
                }
            }
            else
            {
                return new List<FunctionTransaction>();
            }
        }

        private List<FunctionTransaction> findDAFName(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                IEnumerable<FunctionTransaction> temp = from ftx in list
                                                        join b in cpaiEntity.DAF_DATA on ftx.FTX_TRANS_ID equals b.DDA_ROW_ID
                                                        select new FunctionTransaction
                                                        {
                                                            FUNCTION_TRANSACTION = ftx,
                                                            //UNIT_STATUS = e != null ? e.COEX_ACTIVATION_STATUS : "",
                                                            //REQUESTER_NAME = d != null ? d.CODA_REQUESTED_BY : "",
                                                            //DRAFT_APPROVER_NAME = d != null ? d.CODA_DRAFT_APPROVED_BY : "",
                                                            //FINAL_APPROVER_NAME = d != null ? d.CODA_FINAL_APPROVED_BY : "",
                                                            //DRAFT_CAM_FILE = d != null ? d.CODA_DRAFT_CAM_PATH : "",
                                                            //FINAL_CAM_FILE = d != null ? d.CODA_FINAL_CAM_PATH : "",
                                                            //DRAFT_SUBMIT_DATE = d != null ? d.CODA_DRAFT_SUBMIT_DATE.HasValue ? d.CODA_DRAFT_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //DRAFT_APPROVE_DATE = d != null ? d.CODA_DRAFT_APPROVE_DATE.HasValue ? d.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_SUBMIT_DATE = d != null ? d.CODA_FINAL_SUBMIT_DATE.HasValue ? d.CODA_FINAL_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_APPROVE_DATE = d != null ? d.CODA_FINAL_APPROVE_DATE.HasValue ? d.CODA_FINAL_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            STATUS = b != null ? b.DDA_STATUS : "",
                                                            NOTE = b != null ? b.DDA_EXPLANATION : "",
                                                            UPDATED_DATE = b != null ? b.DDA_UPDATED.ToString() : "",
                                                            UPDATED_BY = b != null ? b.DDA_UPDATED_BY : "",
                                                            CREATED_BY = b != null ? b.DDA_CREATED_BY : "",
                                                            CREATED_DATE = b != null ? b.DDA_CREATED.ToString() : "",
                                                        };

                results = temp.DistinctBy(m => m.FUNCTION_TRANSACTION.FTX_REQ_TRANS).ToList();
                return results;
            }
        }

        public List<FunctionTransaction> findCPAICDSTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate, string create_by, string username)
        {
            EntitiesEngine db = new EntitiesEngine();
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            string user_group = PAFHelper.GetUserGroup(username, "CDS");
            string query = "SELECT FTX_ROW_ID, FTX_FK_FUNCTION, FTX_TRANS_ID, FTX_APP_ID, FTX_REQ_TRANS, FTX_RETURN_STATUS, FTX_RETURN_NAME_SPACE, FTX_RETURN_CODE, FTX_RETURN_DESC, FTX_STATE_ACTION, FTX_CURRENT_NAME_SPACE, FTX_CURRENT_CODE, FTX_CURRENT_DESC, FTX_REQUEST_DATE, FTX_RESPONSE_DATE, FTX_ACCUM_RETRY, FTX_RETRY_COUNT, FTX_CURRENT_STATE, FTX_NEXT_STATE, FTX_WAKEUP, FTX_FWD_RESP_CODE_FLAG, FTX_CREATED, FTX_CREATED_BY, FTX_UPDATED, FTX_UPDATED_BY, FTX_RETURN_MESSAGE, FTX_PARENT_TRX_ID, FTX_PARENT_ACT_ID, FTX_INDEX1, FTX_INDEX2, FTX_INDEX3, FTX_INDEX4, FTX_INDEX5, FTX_INDEX6, FTX_INDEX7, FTX_INDEX8, FTX_INDEX9, FTX_INDEX10, FTX_INDEX11, FTX_INDEX12, FTX_INDEX13, FTX_INDEX14, FTX_INDEX15, FTX_INDEX16, FTX_INDEX17, FTX_INDEX18, FTX_INDEX19, FTX_INDEX20 FROM FUNCTION_TRANSACTION FT INNER JOIN CDS_DATA DD ON FT.FTX_TRANS_ID = DD.CDA_ROW_ID WHERE UPPER(FT.FTX_FK_FUNCTION) = UPPER('87')";

            List<FUNCTION_TRANSACTION> list = new List<FUNCTION_TRANSACTION>();
            if (user_group.ToUpper() == "CMVP" || user_group.ToUpper() == "EVPC")
            {
                //list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 2), new OracleParameter("user_group", "CM") }).ToList();
            }
            else
            {
                //list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 3), new OracleParameter("user_group", user_group) }).ToList();
            }
            list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query).ToList();

            // find name by index id
            results = findCDSName(list, ACF_FK_FUNCTION);

            if (results != null && results.Count() > 0)
            {
                if (lstStatus != null && lstStatus.Count > 0)
                {
                    results = results.Where(x => lstStatus.Contains(x.STATUS)).ToList();
                }
                if (!string.IsNullOrEmpty(create_by))
                {
                    results = results.Where(x => create_by.Contains(x.CREATED_BY)).ToList();
                }
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return results;
                }
                else
                {
                    DateTime fDate = parseInputDate(fromDate);
                    DateTime tDate = parseInputDate(toDate);
                    var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_CREATED != null && (a.FUNCTION_TRANSACTION.FTX_CREATED >= fDate) && (a.FUNCTION_TRANSACTION.FTX_CREATED <= tDate) select a;
                    return realResult.ToList();
                }
            }
            else
            {
                return new List<FunctionTransaction>();
            }
        }

        private List<FunctionTransaction> findCDSName(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                IEnumerable<FunctionTransaction> temp = from ftx in list
                                                        join b in cpaiEntity.CDS_DATA on ftx.FTX_TRANS_ID equals b.CDA_ROW_ID
                                                        select new FunctionTransaction
                                                        {
                                                            FUNCTION_TRANSACTION = ftx,
                                                            //UNIT_STATUS = e != null ? e.COEX_ACTIVATION_STATUS : "",
                                                            //REQUESTER_NAME = d != null ? d.CODA_REQUESTED_BY : "",
                                                            //DRAFT_APPROVER_NAME = d != null ? d.CODA_DRAFT_APPROVED_BY : "",
                                                            //FINAL_APPROVER_NAME = d != null ? d.CODA_FINAL_APPROVED_BY : "",
                                                            //DRAFT_CAM_FILE = d != null ? d.CODA_DRAFT_CAM_PATH : "",
                                                            //FINAL_CAM_FILE = d != null ? d.CODA_FINAL_CAM_PATH : "",
                                                            //DRAFT_SUBMIT_DATE = d != null ? d.CODA_DRAFT_SUBMIT_DATE.HasValue ? d.CODA_DRAFT_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //DRAFT_APPROVE_DATE = d != null ? d.CODA_DRAFT_APPROVE_DATE.HasValue ? d.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_SUBMIT_DATE = d != null ? d.CODA_FINAL_SUBMIT_DATE.HasValue ? d.CODA_FINAL_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_APPROVE_DATE = d != null ? d.CODA_FINAL_APPROVE_DATE.HasValue ? d.CODA_FINAL_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            STATUS = b != null ? b.CDA_STATUS : "",
                                                            NOTE = b != null ? b.CDA_NOTE : "",
                                                            UPDATED_DATE = b != null ? b.CDA_UPDATED.ToString() : "",
                                                            UPDATED_BY = b != null ? b.CDA_UPDATED_BY : "",
                                                            CREATED_BY = b != null ? b.CDA_CREATED_BY : "",
                                                            CREATED_DATE = b != null ? b.CDA_CREATED.ToString() : "",
                                                        };

                results = temp.DistinctBy(m => m.FUNCTION_TRANSACTION.FTX_REQ_TRANS).ToList();
                return results;
            }
        }

        public List<FunctionTransaction> findCPAICDOTransaction(string ACF_FK_FUNCTION, List<String> lstStatus, List<string> lstIndexValues, string fromDate, string toDate, string create_by, string username)
        {
            EntitiesEngine db = new EntitiesEngine();
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            string user_group = PAFHelper.GetUserGroup(username, "CDO");
            string query = "SELECT FTX_ROW_ID, FTX_FK_FUNCTION, FTX_TRANS_ID, FTX_APP_ID, FTX_REQ_TRANS, FTX_RETURN_STATUS, FTX_RETURN_NAME_SPACE, FTX_RETURN_CODE, FTX_RETURN_DESC, FTX_STATE_ACTION, FTX_CURRENT_NAME_SPACE, FTX_CURRENT_CODE, FTX_CURRENT_DESC, FTX_REQUEST_DATE, FTX_RESPONSE_DATE, FTX_ACCUM_RETRY, FTX_RETRY_COUNT, FTX_CURRENT_STATE, FTX_NEXT_STATE, FTX_WAKEUP, FTX_FWD_RESP_CODE_FLAG, FTX_CREATED, FTX_CREATED_BY, FTX_UPDATED, FTX_UPDATED_BY, FTX_RETURN_MESSAGE, FTX_PARENT_TRX_ID, FTX_PARENT_ACT_ID, FTX_INDEX1, FTX_INDEX2, FTX_INDEX3, FTX_INDEX4, FTX_INDEX5, FTX_INDEX6, FTX_INDEX7, FTX_INDEX8, FTX_INDEX9, FTX_INDEX10, FTX_INDEX11, FTX_INDEX12, FTX_INDEX13, FTX_INDEX14, FTX_INDEX15, FTX_INDEX16, FTX_INDEX17, FTX_INDEX18, FTX_INDEX19, FTX_INDEX20 FROM FUNCTION_TRANSACTION FT INNER JOIN CDO_DATA DD ON FT.FTX_TRANS_ID = DD.CDA_ROW_ID WHERE UPPER(FT.FTX_FK_FUNCTION) = UPPER('87')";

            List<FUNCTION_TRANSACTION> list = new List<FUNCTION_TRANSACTION>();
            if (user_group.ToUpper() == "CMVP" || user_group.ToUpper() == "EVPC")
            {
                //list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 2), new OracleParameter("user_group", "CM") }).ToList();
            }
            else
            {
                //list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query, new[] { new OracleParameter("bound", 3), new OracleParameter("user_group", user_group) }).ToList();
            }
            list = db.Database.SqlQuery<FUNCTION_TRANSACTION>(query).ToList();

            // find name by index id
            results = findCDOName(list, ACF_FK_FUNCTION);

            if (results != null && results.Count() > 0)
            {
                if (lstStatus != null && lstStatus.Count > 0)
                {
                    results = results.Where(x => lstStatus.Contains(x.STATUS)).ToList();
                }
                if (!string.IsNullOrEmpty(create_by))
                {
                    results = results.Where(x => create_by.Contains(x.CREATED_BY)).ToList();
                }
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate))
                {
                    return results;
                }
                else
                {
                    DateTime fDate = parseInputDate(fromDate);
                    DateTime tDate = parseInputDate(toDate);
                    var realResult = from a in results where a.FUNCTION_TRANSACTION.FTX_CREATED != null && (a.FUNCTION_TRANSACTION.FTX_CREATED >= fDate) && (a.FUNCTION_TRANSACTION.FTX_CREATED <= tDate) select a;
                    return realResult.ToList();
                }
            }
            else
            {
                return new List<FunctionTransaction>();
            }
        }

        private List<FunctionTransaction> findCDOName(List<FUNCTION_TRANSACTION> list, string ACF_FK_FUNCTION)
        {
            List<FunctionTransaction> results = new List<FunctionTransaction>();
            using (EntityCPAIEngine cpaiEntity = new EntityCPAIEngine())
            {
                IEnumerable<FunctionTransaction> temp = from ftx in list
                                                        join b in cpaiEntity.CDO_DATA on ftx.FTX_TRANS_ID equals b.CDO_ROW_ID
                                                        select new FunctionTransaction
                                                        {
                                                            FUNCTION_TRANSACTION = ftx,
                                                            //UNIT_STATUS = e != null ? e.COEX_ACTIVATION_STATUS : "",
                                                            //REQUESTER_NAME = d != null ? d.CODA_REQUESTED_BY : "",
                                                            //DRAFT_APPROVER_NAME = d != null ? d.CODA_DRAFT_APPROVED_BY : "",
                                                            //FINAL_APPROVER_NAME = d != null ? d.CODA_FINAL_APPROVED_BY : "",
                                                            //DRAFT_CAM_FILE = d != null ? d.CODA_DRAFT_CAM_PATH : "",
                                                            //FINAL_CAM_FILE = d != null ? d.CODA_FINAL_CAM_PATH : "",
                                                            //DRAFT_SUBMIT_DATE = d != null ? d.CODA_DRAFT_SUBMIT_DATE.HasValue ? d.CODA_DRAFT_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //DRAFT_APPROVE_DATE = d != null ? d.CODA_DRAFT_APPROVE_DATE.HasValue ? d.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_SUBMIT_DATE = d != null ? d.CODA_FINAL_SUBMIT_DATE.HasValue ? d.CODA_FINAL_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            //FINAL_APPROVE_DATE = d != null ? d.CODA_FINAL_APPROVE_DATE.HasValue ? d.CODA_FINAL_APPROVE_DATE.Value.ToString("dd-MMM-yyyy HH.mm") : "" : "",
                                                            STATUS = b != null ? b.CDS_DATA.CDA_STATUS : "",
                                                            NOTE = b != null ? b.CDS_DATA.CDA_NOTE : "",
                                                            UPDATED_DATE = b != null ? b.CDS_DATA.CDA_UPDATED.ToString() : "",
                                                            UPDATED_BY = b != null ? b.CDS_DATA.CDA_UPDATED_BY : "",
                                                            CREATED_BY = b != null ? b.CDS_DATA.CDA_CREATED_BY : "",
                                                            CREATED_DATE = b != null ? b.CDS_DATA.CDA_CREATED.ToString() : "",
                                                        };

                results = temp.DistinctBy(m => m.FUNCTION_TRANSACTION.FTX_REQ_TRANS).ToList();
                return results;
            }
        }

    }
}
