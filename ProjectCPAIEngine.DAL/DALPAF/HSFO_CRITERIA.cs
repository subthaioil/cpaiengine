﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class HSFO_FX_CRITERIA
    {
        public DateTime DATE_FROM { get; set; }
        public DateTime DATE_TO { get; set; }
    }
}
