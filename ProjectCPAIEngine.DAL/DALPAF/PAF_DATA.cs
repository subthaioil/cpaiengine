﻿using ProjectCPAIEngine.DAL.DALPAF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Entity
{
    public partial class PAF_DATA
    {
        public string REQ_TRAN_ID { get; set; }
        public string TRAN_ID { get; set; }

        public string PDA_CONTRACT_DATE()
        {
            if (PDA_CONTRACT_DATE_FROM != null && PDA_CONTRACT_DATE_TO != null)
            {
                return PAFHelper.ToOnlyDateString(PDA_CONTRACT_DATE_FROM) + " - " + PAFHelper.ToOnlyDateString(PDA_CONTRACT_DATE_TO);
            }

            return "-";
        }

        public string PDA_LOADING_DATE()
        {
            if (PDA_LOADING_DATE_FROM != null && PDA_LOADING_DATE_TO != null)
            {
                return PAFHelper.ToOnlyDateString(PDA_LOADING_DATE_FROM) + " - " + PAFHelper.ToOnlyDateString(PDA_LOADING_DATE_TO);
            }

            return "-";
        }

        public string PDA_DISCHARGING_DATE()
        {
            if (PDA_DISCHARGING_DATE_FROM != null && PDA_DISCHARGING_DATE_TO != null)
            {
                return PAFHelper.ToOnlyDateString(PDA_DISCHARGING_DATE_FROM) + " - " + PAFHelper.ToOnlyDateString(PDA_DISCHARGING_DATE_TO);
            }

            return "-";
        }
        


    }

}
