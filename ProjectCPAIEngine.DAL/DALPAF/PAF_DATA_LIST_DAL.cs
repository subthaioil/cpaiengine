﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class PAF_DATA_LIST_DAL
    {
        private FLOW_MANAGEMENT_DAL FlowManagement = new FLOW_MANAGEMENT_DAL();
        private USER_GROUP_DAL UserGroupManagement = new USER_GROUP_DAL();
        private EntityCPAIEngine db = new EntityCPAIEngine();

        public List<PAF_MASTER_CRITERIA> GetCreatedList(string user)
        {
            string GROUP = UserGroupManagement.GetGroupByUsernameAndSystem(user, "PAF");
            List<PAF_MASTER_CRITERIA> result = new List<PAF_MASTER_CRITERIA>();
            string query = @"SELECT DISTINCT PAF_DATA.PDA_CREATED_BY as ID, PAF_DATA.PDA_CREATED_BY as VALUE FROM PAF_DATA ";
            if(GROUP == "CMLA" || GROUP == "CMPS")
            {
                result = db.Database.SqlQuery<PAF_MASTER_CRITERIA>(query).ToList();
            } else
            {
                query += "WHERE UPPER(SUBSTR(PAF_DATA.PDA_TEMPLATE, 0, 3)) = UPPER(SUBSTR(:GROUP, 0, 3))";
                result = db.Database.SqlQuery<PAF_MASTER_CRITERIA>(query, new[] { new OracleParameter("GROUP", GROUP.Trim()) }).ToList();
            }
            return result;
        }

        public List<PAF_MASTER_CRITERIA> GetCustomerList(string user)
        {
            string GROUP = UserGroupManagement.GetGroupByUsernameAndSystem(user, "PAF");
            List<PAF_MASTER_CRITERIA> result = new List<PAF_MASTER_CRITERIA>();
            string query = @"SELECT DISTINCT MT_CUST_DETAIL.MCD_FK_CUS AS ID, MT_CUST_DETAIL.MCD_NAME_1 || ' ' || MT_CUST_DETAIL.MCD_NAME_2 AS VALUE FROM PAF_PAYMENT_ITEMS JOIN PAF_DATA ON PAF_PAYMENT_ITEMS.PPI_FK_PAF_DATA = PAF_DATA.PDA_ROW_ID JOIN MT_CUST_DETAIL ON MT_CUST_DETAIL.MCD_FK_CUS = PAF_PAYMENT_ITEMS.PPI_FK_CUSTOMER WHERE MT_CUST_DETAIL.MCD_NATION = 'I'";
            if (GROUP == "CMLA" || GROUP == "CMPS")
            {
                result = db.Database.SqlQuery<PAF_MASTER_CRITERIA>(query).ToList();
            }
            else
            {
                query += " AND UPPER( SUBSTR(PAF_DATA.PDA_TEMPLATE, 0, 3)) = UPPER(SUBSTR(:GROUP, 0, 3))";
                result = db.Database.SqlQuery<PAF_MASTER_CRITERIA>(query, new[] { new OracleParameter("GROUP", GROUP.Trim()) }).ToList();
            }
            return result;
        }

        public List<PAF_MASTER_CRITERIA> GetVendorList(string user)
        {
            string GROUP = UserGroupManagement.GetGroupByUsernameAndSystem(user, "PAF");
            List<PAF_MASTER_CRITERIA> result = new List<PAF_MASTER_CRITERIA>();
            string query = @"SELECT DISTINCT MT_VENDOR.VND_ACC_NUM_VENDOR as ID, MT_VENDOR.VND_NAME1 || ' ' || MT_VENDOR.VND_NAME2 as VALUE FROM PAF_PAYMENT_ITEMS JOIN PAF_DATA ON PAF_PAYMENT_ITEMS.PPI_FK_PAF_DATA = PAF_DATA.PDA_ROW_ID JOIN MT_VENDOR ON MT_VENDOR.VND_ACC_NUM_VENDOR = PAF_PAYMENT_ITEMS.PPI_FK_VENDOR ";
            if (GROUP == "CMLA" || GROUP == "CMPS")
            {
                result = db.Database.SqlQuery<PAF_MASTER_CRITERIA>(query).ToList();
            }
            else
            {
                query += "WHERE UPPER(SUBSTR(PAF_DATA.PDA_TEMPLATE, 0, 3)) = UPPER(SUBSTR(:GROUP, 0, 3))";
                result = db.Database.SqlQuery<PAF_MASTER_CRITERIA>(query, new[] { new OracleParameter("GROUP", GROUP.Trim()) }).ToList();
            }
            return result;
        }
    }
}
