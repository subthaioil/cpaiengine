﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class USER_GROUP_DAL
    {
        private EntityCPAIEngine db = new EntityCPAIEngine();
        public string GetGroupByUsernameAndSystem(string username, string system)
        {
            CPAI_USER_GROUP ug = FindByUserAndSystem(username, system);
            if(ug == null)
            {
                return "";
            }

            return ug.USG_USER_GROUP;
        }

        public CPAI_USER_GROUP FindByUserAndSystem(string username, string system)
        {
            CPAI_USER_GROUP usergroup = db.CPAI_USER_GROUP.Where(x => x.USERS.USR_LOGIN.ToUpper() == username.ToUpper()
                                                && x.USG_USER_SYSTEM.ToUpper() == system.ToUpper()
                                                //&& x.USG_USED_FLAG.ToUpper() == "Y"
                                                //&& x.USG_STATUS.ToUpper() == "ACTIVE"
                                        ).FirstOrDefault();

            return usergroup;
        }
    }
}
