﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Entity
{
    public partial class VW_PAF_IMPORT
    {
        public string REF_ID { get; set; }
        public string CRUDE_CODE { get; set; }
        public string CRUDE_NAME { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string COM_CODE { get; set; }
        public string TOLERANCE { get; set; }
        public Nullable<System.DateTime> DISCHARGING_DATE_FROM { get; set; }
        public Nullable<System.DateTime> DISCHARGING_DATE_TO { get; set; }
        public string ORIGIN_CODE { get; set; }
        public string ORIGIN { get; set; }
        public string GT_C { get; set; }
        public string PAYMENT_TERM { get; set; }
        public Nullable<decimal> BENCHMARK_CODE { get; set; }
        public string BENCHMARK { get; set; }
        public Nullable<System.DateTime> LOADING_DATE_FROM { get; set; }
        public Nullable<System.DateTime> LOADING_DATE_TO { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string PRICING_PERIOD { get; set; }
        public string OFFER_UNIT { get; set; }
        public Nullable<decimal> OFFER { get; set; }
        public string INCOTERM { get; set; }
        public Nullable<decimal> QUANTITY_MIN { get; set; }
        public Nullable<decimal> QUANTITY_MAX { get; set; }
        public string QUANTITY_UNIT { get; set; }
        public string FORM_ID { get; set; }
    }
}
