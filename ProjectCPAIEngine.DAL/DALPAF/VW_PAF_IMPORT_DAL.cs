﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class VW_PAF_IMPORT_DAL
    {
        public static List<VW_PAF_IMPORT> GetData()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            List<VW_PAF_IMPORT> result = context.Database.SqlQuery<VW_PAF_IMPORT>("SELECT * FROM VW_PAF_IMPORT").ToList();
            return result;
        }
    }
}
