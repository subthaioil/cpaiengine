﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class BORROW_DAL
    {
        public void SaveBorrowCrude(BORROW_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.BORROW_CRUDE.Add(data);                  
                    context.SaveChanges();
                };
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }

        public void SaveBorrowCrude(BORROW_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                context.BORROW_CRUDE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBorrowCrude(BORROW_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _BorrrowCrude = context.BORROW_CRUDE.Where(x => x.BRC_TRIP_NO.ToUpper().Equals(data.BRC_TRIP_NO.ToUpper())
                                                                      && x.BRC_CRUDE_TYPE_ID.ToUpper().Equals(data.BRC_CRUDE_TYPE_ID.ToUpper())
                                                                      && x.BRC_TYPE.ToUpper().Equals(data.BRC_TYPE.ToUpper())                                                                      
                                                                        ).ToList().FirstOrDefault();
                    //var _BorrrowCrude = context.BORROW_CRUDE.Where(x => x.BRC_TRIP_NO.ToUpper().Equals(data.BRC_TRIP_NO.ToUpper())
                    //                                                  && x.BRC_TYPE.ToUpper().Equals(data.BRC_TYPE.ToUpper())
                    //                                                    ).ToList().FirstOrDefault();

                    if (_BorrrowCrude != null)
                    {
                        _BorrrowCrude.BRC_TRIP_NO = data.BRC_TRIP_NO;
                        _BorrrowCrude.BRC_PO_NO = data.BRC_PO_NO;
                        _BorrrowCrude.BRC_CUST_NUM = data.BRC_CUST_NUM;
                        _BorrrowCrude.BRC_DELIVERY_DATE = data.BRC_DELIVERY_DATE;
                        _BorrrowCrude.BRC_DUE_DATE = data.BRC_DUE_DATE;
                        _BorrrowCrude.BRC_PLANT = data.BRC_PLANT;
                        _BorrrowCrude.BRC_COMPANY = data.BRC_COMPANY;
                        _BorrrowCrude.BRC_CRUDE_TYPE_ID = data.BRC_CRUDE_TYPE_ID;
                        _BorrrowCrude.BRC_VOLUME_BBL = data.BRC_VOLUME_BBL;
                        _BorrrowCrude.BRC_VOLUME_MT = data.BRC_VOLUME_MT;
                        _BorrrowCrude.BRC_VOLUME_LITE = data.BRC_VOLUME_LITE;
                        _BorrrowCrude.BRC_OUTTURN_BBL = data.BRC_OUTTURN_BBL;
                        _BorrrowCrude.BRC_OUTTURN_LITE = data.BRC_OUTTURN_LITE;
                        _BorrrowCrude.BRC_OUTTURN_MT = data.BRC_OUTTURN_MT;
                        _BorrrowCrude.BRC_PRICE_PER = data.BRC_PRICE_PER;
                        _BorrrowCrude.BRC_PRICE = data.BRC_PRICE;
                        _BorrrowCrude.BRC_UNIT_ID = data.BRC_UNIT_ID;
                        _BorrrowCrude.BRC_EXCHANGE_RATE = data.BRC_EXCHANGE_RATE;
                        _BorrrowCrude.BRC_TYPE = data.BRC_TYPE;
                        _BorrrowCrude.BRC_CREATE_DATE = data.BRC_CREATE_DATE;
                        _BorrrowCrude.BRC_CREATE_BY = data.BRC_CREATE_BY;
                        _BorrrowCrude.BRC_LAST_MODIFY_DATE = data.BRC_LAST_MODIFY_DATE;
                        _BorrrowCrude.BRC_LAST_MODIFY_BY = data.BRC_LAST_MODIFY_BY;
                        _BorrrowCrude.BRC_STATUS = data.BRC_STATUS;
                        _BorrrowCrude.BRC_CREATE_TIME = data.BRC_CREATE_TIME;
                        _BorrrowCrude.BRC_LAST_MODIFY_TIME = data.BRC_LAST_MODIFY_TIME;
                        _BorrrowCrude.BRC_RELEASE = data.BRC_RELEASE;
                        _BorrrowCrude.BRC_TANK = data.BRC_TANK;
                        _BorrrowCrude.BRC_SALEORDER = data.BRC_SALEORDER;
                        _BorrrowCrude.BRC_PRICE_RELEASE = data.BRC_PRICE_RELEASE;
                        _BorrrowCrude.BRC_REMARK = data.BRC_REMARK;
                        _BorrrowCrude.BRC_DISTRI_CHANN = data.BRC_DISTRI_CHANN;
                        _BorrrowCrude.BRC_TABLE_NAME = data.BRC_TABLE_NAME;
                        _BorrrowCrude.BRC_TOTAL = data.BRC_TOTAL;
                        _BorrrowCrude.BRC_MEMO = data.BRC_MEMO;
                        _BorrrowCrude.BRC_DATE_SAP = data.BRC_DATE_SAP;
                        _BorrrowCrude.BRC_FI_DOC = data.BRC_FI_DOC;
                        _BorrrowCrude.BRC_DO_NO = data.BRC_DO_NO;
                        _BorrrowCrude.BRC_STATUS_TO_SAP = data.BRC_STATUS_TO_SAP;
                        _BorrrowCrude.BRC_VESSEL_ID = data.BRC_VESSEL_ID;
                        _BorrrowCrude.BRC_UNIT_TOTAL = data.BRC_UNIT_TOTAL;
                        _BorrrowCrude.BRC_FI_DOC_REVERSE = data.BRC_FI_DOC_REVERSE;
                        _BorrrowCrude.BRC_INCOTERM_TYPE = data.BRC_INCOTERM_TYPE;
                        _BorrrowCrude.BRC_TEMP = data.BRC_TEMP;
                        _BorrrowCrude.BRC_DENSITY = data.BRC_DENSITY;
                        _BorrrowCrude.BRC_STORAGE_LOCATION = data.BRC_STORAGE_LOCATION;
                        _BorrrowCrude.BRC_UPDATE_DO = data.BRC_UPDATE_DO;
                        _BorrrowCrude.BRC_UPDATE_GI = data.BRC_UPDATE_GI;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBorrowCrude(BORROW_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                var _BorrrowCrude = context.BORROW_CRUDE.Where(x => x.BRC_TRIP_NO.ToUpper().Equals(data.BRC_TRIP_NO.ToUpper())
                                                                      && x.BRC_CRUDE_TYPE_ID.ToUpper().Equals(data.BRC_CRUDE_TYPE_ID.ToUpper())
                                                                      && x.BRC_TYPE.ToUpper().Equals(data.BRC_TYPE.ToUpper())                                                                      
                                                                        ).ToList().FirstOrDefault();
                //var _BorrrowCrude = context.BORROW_CRUDE.Where(x => x.BRC_TRIP_NO.ToUpper().Equals(data.BRC_TRIP_NO.ToUpper())                                                                      
                //                                                      && x.BRC_TYPE.ToUpper().Equals(data.BRC_TYPE.ToUpper())                                                                     
                //                                                        ).ToList().FirstOrDefault();

                if (_BorrrowCrude != null)
                {
                    _BorrrowCrude.BRC_TRIP_NO = data.BRC_TRIP_NO;
                    _BorrrowCrude.BRC_PO_NO = data.BRC_PO_NO;
                    _BorrrowCrude.BRC_CUST_NUM = data.BRC_CUST_NUM;
                    _BorrrowCrude.BRC_DELIVERY_DATE = data.BRC_DELIVERY_DATE;
                    _BorrrowCrude.BRC_DUE_DATE = data.BRC_DUE_DATE;
                    _BorrrowCrude.BRC_PLANT = data.BRC_PLANT;
                    _BorrrowCrude.BRC_COMPANY = data.BRC_COMPANY;
                    _BorrrowCrude.BRC_CRUDE_TYPE_ID = data.BRC_CRUDE_TYPE_ID;
                    _BorrrowCrude.BRC_VOLUME_BBL = data.BRC_VOLUME_BBL;
                    _BorrrowCrude.BRC_VOLUME_MT = data.BRC_VOLUME_MT;
                    _BorrrowCrude.BRC_VOLUME_LITE = data.BRC_VOLUME_LITE;
                    _BorrrowCrude.BRC_OUTTURN_BBL = data.BRC_OUTTURN_BBL;
                    _BorrrowCrude.BRC_OUTTURN_LITE = data.BRC_OUTTURN_LITE;
                    _BorrrowCrude.BRC_OUTTURN_MT = data.BRC_OUTTURN_MT;
                    _BorrrowCrude.BRC_PRICE_PER = data.BRC_PRICE_PER;
                    _BorrrowCrude.BRC_PRICE = data.BRC_PRICE;
                    _BorrrowCrude.BRC_UNIT_ID = data.BRC_UNIT_ID;
                     _BorrrowCrude.BRC_EXCHANGE_RATE = data.BRC_EXCHANGE_RATE;
                     _BorrrowCrude.BRC_TYPE = data.BRC_TYPE;
                     _BorrrowCrude.BRC_CREATE_DATE = data.BRC_CREATE_DATE;
                     _BorrrowCrude.BRC_CREATE_BY = data.BRC_CREATE_BY;
                     _BorrrowCrude.BRC_LAST_MODIFY_DATE = data.BRC_LAST_MODIFY_DATE;
                     _BorrrowCrude.BRC_LAST_MODIFY_BY = data.BRC_LAST_MODIFY_BY;
                     _BorrrowCrude.BRC_STATUS = data.BRC_STATUS;
                     _BorrrowCrude.BRC_CREATE_TIME = data.BRC_CREATE_TIME;
                     _BorrrowCrude.BRC_LAST_MODIFY_TIME = data.BRC_LAST_MODIFY_TIME;
                     _BorrrowCrude.BRC_RELEASE = data.BRC_RELEASE;
                     _BorrrowCrude.BRC_TANK = data.BRC_TANK;
                     _BorrrowCrude.BRC_SALEORDER = data.BRC_SALEORDER;
                     _BorrrowCrude.BRC_PRICE_RELEASE = data.BRC_PRICE_RELEASE;
                     _BorrrowCrude.BRC_REMARK = data.BRC_REMARK;
                     _BorrrowCrude.BRC_DISTRI_CHANN = data.BRC_DISTRI_CHANN;
                     _BorrrowCrude.BRC_TABLE_NAME = data.BRC_TABLE_NAME;
                     _BorrrowCrude.BRC_TOTAL = data.BRC_TOTAL;
                     _BorrrowCrude.BRC_MEMO = data.BRC_MEMO;
                     _BorrrowCrude.BRC_DATE_SAP = data.BRC_DATE_SAP;
                     _BorrrowCrude.BRC_FI_DOC = data.BRC_FI_DOC;
                     _BorrrowCrude.BRC_DO_NO = data.BRC_DO_NO;
                     _BorrrowCrude.BRC_STATUS_TO_SAP = data.BRC_STATUS_TO_SAP;
                     _BorrrowCrude.BRC_VESSEL_ID = data.BRC_VESSEL_ID;
                     _BorrrowCrude.BRC_UNIT_TOTAL = data.BRC_UNIT_TOTAL;
                     _BorrrowCrude.BRC_FI_DOC_REVERSE = data.BRC_FI_DOC_REVERSE;
                     _BorrrowCrude.BRC_INCOTERM_TYPE = data.BRC_INCOTERM_TYPE;
                     _BorrrowCrude.BRC_TEMP = data.BRC_TEMP;
                     _BorrrowCrude.BRC_DENSITY = data.BRC_DENSITY;
                     _BorrrowCrude.BRC_STORAGE_LOCATION = data.BRC_STORAGE_LOCATION;
                     _BorrrowCrude.BRC_UPDATE_DO = data.BRC_UPDATE_DO;
                    _BorrrowCrude.BRC_UPDATE_GI = data.BRC_UPDATE_GI;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveBorrowPrice(BORROW_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.BORROW_PRICE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveBorrowPrice(BORROW_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                context.BORROW_PRICE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBorrowPrice(BORROW_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _BorrowPrice = context.BORROW_PRICE.Where(x=>x.BRP_TRIP_NO.ToUpper().Equals(data.BRP_TRIP_NO.ToUpper())                                                                                                                                            
                                                                      && x.BRP_NUM.Equals(data.BRP_NUM)).ToList().FirstOrDefault();
                    if (_BorrowPrice != null)
                    {
                        _BorrowPrice.BRP_TRIP_NO = data.BRP_TRIP_NO;
                        _BorrowPrice.BRP_PRICE = data.BRP_PRICE;
                        _BorrowPrice.BRP_DATE_PRICE = data.BRP_DATE_PRICE;
                        _BorrowPrice.BRP_NUM = data.BRP_NUM;
                        _BorrowPrice.BRP_UNIT_ID = data.BRP_UNIT_ID;
                        _BorrowPrice.BRP_RELEASE = data.BRP_RELEASE;
                        _BorrowPrice.BRP_INVOICE = data.BRP_INVOICE;
                        _BorrowPrice.BRP_MEMO = data.BRP_MEMO;
                        _BorrowPrice.BRP_UNIT_INV = data.BRP_UNIT_INV;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBorrowPrice(BORROW_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                var _BorrowPrice = context.BORROW_PRICE.Where(x => x.BRP_TRIP_NO.ToUpper().Equals(data.BRP_TRIP_NO.ToUpper())
                                                                      && x.BRP_NUM.Equals(data.BRP_NUM)).ToList().FirstOrDefault();
                if (_BorrowPrice != null)
                {
                    _BorrowPrice.BRP_TRIP_NO = data.BRP_TRIP_NO;
                    _BorrowPrice.BRP_PRICE = data.BRP_PRICE;
                    _BorrowPrice.BRP_DATE_PRICE = data.BRP_DATE_PRICE;
                    _BorrowPrice.BRP_NUM = data.BRP_NUM;
                    _BorrowPrice.BRP_UNIT_ID = data.BRP_UNIT_ID;
                    _BorrowPrice.BRP_RELEASE = data.BRP_RELEASE;
                    _BorrowPrice.BRP_INVOICE = data.BRP_INVOICE;
                    _BorrowPrice.BRP_MEMO = data.BRP_MEMO;
                    _BorrowPrice.BRP_UNIT_INV = data.BRP_UNIT_INV;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
