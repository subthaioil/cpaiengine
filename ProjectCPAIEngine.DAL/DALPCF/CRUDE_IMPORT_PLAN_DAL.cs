﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;


namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class CRUDE_IMPORT_PLAN_DAL
    {
        public void UpdatePOResult(string tripno, string matItemno, string pono, string ItemNoCrude, string ItemNoFreight
            , string ItemNoInsur, string ItemNoCustoms, string GITNo, EntityCPAIEngine context)
        {
            try
            {
                string sql = "";
                sql = "UPDATE PCF_MATERIAL SET ";
                sql += "PMA_PO_NO='" + pono + "' ";
                sql += ", PMA_PO_MAT_ITEM=" + (string.IsNullOrEmpty(ItemNoCrude) ? "0" : ItemNoCrude);
                sql += ", PMA_PO_FREIGHT_ITEM=" + (string.IsNullOrEmpty(ItemNoFreight) ? "0" : ItemNoFreight);
                sql += ", PMA_PO_INSURANCE_ITEM=" + (string.IsNullOrEmpty(ItemNoInsur) ? "0" : ItemNoInsur);
                sql += ", PMA_PO_CUSTOMS_ITEM=" + (string.IsNullOrEmpty(ItemNoCustoms) ? "0" : ItemNoCustoms);
                sql += ", PMA_GIT_NO=" + (string.IsNullOrEmpty(GITNo) ? "0" : GITNo);
                sql += " WHERE PMA_TRIP_NO='" + tripno + "' and PMA_ITEM_NO = " + matItemno;


                context.Database.ExecuteSqlCommand(sql);

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateCancelGITResult(string tripno, string matItemno, string GITNo, EntityCPAIEngine context)
        {
            try
            {
                string sql = "";
                sql = "UPDATE PCF_MATERIAL SET ";
                sql += "PMA_GIT_NO=" + (string.IsNullOrEmpty(GITNo) ? "0" : GITNo);
                sql += " WHERE PMA_TRIP_NO='" + tripno + "' and PMA_ITEM_NO = " + matItemno;


                context.Database.ExecuteSqlCommand(sql);

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateChangePOResult(string tripno, string matItemno, string ItemNoCrude, string ItemNoFreight
          , string ItemNoInsur, string ItemNoCustoms, EntityCPAIEngine context, string pono)
        {
            try
            {
                string sItemNoCrude = (string.IsNullOrEmpty(ItemNoCrude) ? "0" : ItemNoCrude);
                string sItemNoFreight = (string.IsNullOrEmpty(ItemNoFreight) ? "0" : ItemNoFreight); ;
                string sItemNoInsur = (string.IsNullOrEmpty(ItemNoInsur) ? "0" : ItemNoInsur);
                string sItemNoCustoms = (string.IsNullOrEmpty(ItemNoCustoms) ? "0" : ItemNoCustoms);

                string sql = "";
                sql = "UPDATE PCF_MATERIAL SET ";
                sql += " PMA_PO_MAT_ITEM=" + sItemNoCrude;
                sql += ", PMA_PO_FREIGHT_ITEM=" + sItemNoFreight;
                sql += ", PMA_PO_INSURANCE_ITEM=" + sItemNoInsur;
                sql += ", PMA_PO_CUSTOMS_ITEM=" + sItemNoCustoms;
                sql += ", PMA_PO_NO= '" + pono + "'";
                sql += " WHERE PMA_TRIP_NO='" + tripno + "' and PMA_ITEM_NO = " + matItemno;


                context.Database.ExecuteSqlCommand(sql);

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSendToBooking(string tripno, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("UPDATE PCF_HEADER SET PHE_PLANNING_RELEASE='Y'  WHERE PHE_TRIP_NO = '" + tripno + "'");

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSendToAccounting(string tripno, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("UPDATE PCF_HEADER SET PHE_BOOKING_RELEASE='Y'  WHERE PHE_TRIP_NO = '" + tripno + "'");

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getLastTripNo(string TripPrefix)
        {
            string ret = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.PCF_HEADER
                             where v.PHE_TRIP_NO.StartsWith(TripPrefix)
                             orderby v.PHE_TRIP_NO descending
                             select v.PHE_TRIP_NO).ToList();


                if (query != null && query.Count() > 0)
                {
                    ret = query[0];
                    if (ret != "")
                        ret = ret.Replace(TripPrefix, "");
                }
            }

            //where v.PHE_TRIP_NO.Contains("/" + TripPrefix + "/")
            return ret;
        }

        public string getLastTripNo(string TripPrefix, EntityCPAIEngine context)
        {
            string ret = "";

            var query = (from v in context.PCF_HEADER
                         where v.PHE_TRIP_NO.StartsWith(TripPrefix)
                         orderby v.PHE_TRIP_NO descending
                         select v.PHE_TRIP_NO).ToList();


            if (query != null && query.Count() > 0)
            {
                ret = query[0];
                if (ret != "")
                    ret = ret.Replace(TripPrefix, "");
            }

            //where v.PHE_TRIP_NO.Contains("/" + TripPrefix + "/")
            return ret;
        }

        public void Save(PCF_FREIGHT_MATERIAL data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_FREIGHT_MATERIAL.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_ATTACH_FILE.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_MATERIAL_PRICE_DAILY data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_MATERIAL_PRICE_DAILY.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void SaveAttachFile(string sql, EntityCPAIEngine context)
        {
            try
            {

                context.Database.ExecuteSqlCommand(sql);
                //context.PCF_ATTACH_FILE.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_MATERIAL data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_MATERIAL.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PRICING_TO_SAP data, EntityCPAIEngine context)
        {
            try
            {
                context.PRICING_TO_SAP.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_MATERIAL_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_MATERIAL_PRICE.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_MATERIAL_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PCF_MATERIAL_PRICE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_FREIGHT data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_FREIGHT.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_PO_SURVEYOR data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_PO_SURVEYOR.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_PO_SURVEYOR_MATERIAL data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_PO_SURVEYOR_MATERIAL.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_OTHER_COST data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_OTHER_COST.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveCustomsVAT(PCF_CUSTOMS_VAT data, EntityCPAIEngine context)
        {
            try
            {
                if (data == null)
                    return;

                string sDate = "null";
                if (data.BL_DATE != null)
                {
                    sDate = " to_date('" + Convert.ToDateTime(data.BL_DATE).ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US")) + "', 'DD/MM/YYYY') ";
                }

                string sql = "insert into PCF_CUSTOMS_VAT (TRIP_NO, MAT_ITEM_NO, CUSTOMS_PRICE, FREIGHT_AMOUNT, INSURANCE_AMOUNT";
                sql += " , INSURANCE_RATE, INVOICE_STATUS, BL_DATE)  ";
                sql += " select '" + data.TRIP_NO + "', '" + data.MAT_ITEM_NO + "' ";
                sql += " , " + ((data.CUSTOMS_PRICE == null) ? "0.0" : data.CUSTOMS_PRICE.ToString());
                sql += " , " + ((data.FREIGHT_AMOUNT == null) ? "0.0" : data.FREIGHT_AMOUNT.ToString());
                sql += " , " + ((data.INSURANCE_AMOUNT == null) ? "0.0" : data.INSURANCE_AMOUNT.ToString());
                sql += " , " + ((data.INSURANCE_RATE == null) ? "0.0" : data.INSURANCE_RATE.ToString());
                sql += " , '" + data.INVOICE_STATUS + "' ";
                sql += " , " + sDate + " ";
                sql += " from dual ";
                sql += " where not exists(select * ";
                sql += " from PCF_CUSTOMS_VAT ";
                sql += " where (TRIP_NO = '" + data.TRIP_NO + "' and MAT_ITEM_NO = " + data.MAT_ITEM_NO.ToString() + " and SAP_FI_DOC IS NOT NULL)) ";

                context.Database.ExecuteSqlCommand(sql);

            }
            catch (Exception ex) {
                throw ex;
            }
        }
        public void Save(PCF_CUSTOMS_VAT data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_CUSTOMS_VAT.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_HEADER data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_HEADER.Add(data);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PCF_MATERIAL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.PCF_MATERIAL.Find(data.PMA_TRIP_NO);
                    #region Set Value
                    //_search.MCO_COMPANY_CODE = data.MCO_COMPANY_CODE;
                    //_search.MCO_SHORT_NAME = data.MCO_SHORT_NAME;
                    //_search.MCO_STATUS = data.MCO_STATUS;
                    //_search.MCO_UPDATED_BY = data.MCO_UPDATED_BY;
                    //_search.MCO_UPDATED_DATE = data.MCO_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PCF_MATERIAL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.PCF_MATERIAL.Find(data.PMA_TRIP_NO);
                #region Set Value
                //_search.MCO_COMPANY_CODE = data.MCO_COMPANY_CODE;
                //_search.MCO_SHORT_NAME = data.MCO_SHORT_NAME;
                //_search.MCO_STATUS = data.MCO_STATUS;
                //_search.MCO_UPDATED_BY = data.MCO_UPDATED_BY;
                //_search.MCO_UPDATED_DATE = data.MCO_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePrice0(string tripNo)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("CALL P_PCF_INSERT_PRICE_0('" + tripNo + "')");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePrice0(string tripNo, string sDateTime, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("CALL P_PCF_INSERT_PRICE_0('" + tripNo + "', '" + sDateTime + "')");
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertVesselPrice(string tripNo, string matnum, string datafrom, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("CALL P_PCF_VESSEL_INSERT_PRICE('" + tripNo + "', '" + matnum + "', '"+ datafrom + "')");
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePrice0Only(string tripNo)
        {
            try
            {
                DateTime createDate = DateTime.Now;
                string sDateTime = createDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")) + ' ' + createDate.ToString("HH:mm:ss", new System.Globalization.CultureInfo("th-TH"));

                using (var context = new EntityCPAIEngine())
                {
                    context.Database.ExecuteSqlCommand("CALL P_PCF_INSERT_PRICE_0('" + tripNo + "', '" + sDateTime + "')");
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean CheckUpdatePricing(string tripno, string user_group)
        {
            Boolean ret = true;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    try
                    {

                        if (user_group.Equals("plan"))
                        {
                            var query = (from p in context.PRICING_TO_SAP
                                         where p.TRIP_NO.Equals(tripno) && (p.DATA_FROM.Equals("BOOKING") || p.DATA_FROM.Equals("ACCOUNTING"))
                                         select p).Count();

                            if (query > 0)
                                ret = false;

                        }
                        else if (user_group.Equals("book"))
                        {
                            var query = (from p in context.PRICING_TO_SAP
                                         where p.TRIP_NO.Equals(tripno) && (p.DATA_FROM.Equals("ACCOUNTING"))
                                         select p).Count();

                            if (query > 0)
                                ret = false;

                        }
                        else if (user_group.Equals("all"))
                        {

                            var query = (from p in context.PRICING_TO_SAP
                                         where p.TRIP_NO.Equals(tripno) && (p.DATA_FROM.Equals("ACCOUNTING"))
                                         select p).Count();

                            if (query > 0)
                                ret = false;
                        }

                    }
                    catch (Exception ex)
                    {
                        ret = false;

                    }
                };
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public void Delete(string tripno)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_HEADER WHERE PHE_TRIP_NO = '" + tripno + "'");
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_MATERIAL WHERE PMA_TRIP_NO = '" + tripno + "'");
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_MATERIAL_PRICE WHERE PMP_TRIP_NO = '" + tripno + "'");
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_FREIGHT WHERE PFR_TRIP_NO = '" + tripno + "'");
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_CUSTOMS_VAT WHERE TRIP_NO = '" + tripno + "'");
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_PO_SURVEYOR WHERE PPS_TRIP_NO = '" + tripno + "'");
                            context.Database.ExecuteSqlCommand("DELETE FROM PCF_OTHER_COST WHERE POC_TRIP_NO = '" + tripno + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSurveyorResult(string tripno, string itemno, string sapno, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("UPDATE PCF_PO_SURVEYOR SET PPS_PO_NO='" + sapno + "' WHERE PPS_TRIP_NO = '" + tripno + "' AND PPS_ITEM_NO='" + itemno + "'");

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteSurveyorResult(string tripno, string itemno, string sapno, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_PO_SURVEYOR WHERE PPS_TRIP_NO = '" + tripno + "' AND PPS_ITEM_NO='" + itemno + "'");

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string tripno, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_MATERIAL_PRICE_DAILY WHERE TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_ATTACH_FILE WHERE PAF_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_OTHER_COST WHERE POC_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_PO_SURVEYOR_MATERIAL WHERE PSM_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_PO_SURVEYOR WHERE PPS_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_CUSTOMS_VAT WHERE TRIP_NO = '" + tripno + "' and SAP_FI_DOC IS NULL ");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_FREIGHT_MATERIAL WHERE PFM_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_FREIGHT WHERE PFR_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_MATERIAL_PRICE WHERE PMP_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_MATERIAL WHERE PMA_TRIP_NO = '" + tripno + "'");
                    context.Database.ExecuteSqlCommand("DELETE FROM PCF_HEADER WHERE PHE_TRIP_NO = '" + tripno + "'");

                    //DateTime createDate = DateTime.Now;
                    //String sDateTime = createDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")) + ' ' + createDate.ToString("HH:mm:ss", new System.Globalization.CultureInfo("th-TH"));
                    ////TO_DATE('2012-07-18 13:27:18', 'YYYY-MM-DD HH24:MI:SS')
                    //dataDAL.UpdatePrice0(tripNo, sDateTime, context);

                    //context.Database.ExecuteSqlCommand("CALL P_PCF_INSERT_PRICE_0('" + tripno + "', '" + sDateTime + "')");

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
