﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class PURCHASE_ENTRY_VESSEL_DAL
    {
        public void InsertPPE(PCF_PURCHASE_ENTRY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.PCF_PURCHASE_ENTRY.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPPE(PCF_PURCHASE_ENTRY data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_PURCHASE_ENTRY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePPE(PCF_PURCHASE_ENTRY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _PurchaseEntryVessel = context.PCF_PURCHASE_ENTRY.Find(data.PPE_TRIP_NO, data.PPE_MET_NUM,data.PPE_ITEM_NO);
                    if (_PurchaseEntryVessel != null)
                    {
                        _PurchaseEntryVessel.PPE_TRIP_NO = data.PPE_TRIP_NO;
                        _PurchaseEntryVessel.PPE_MET_NUM = data.PPE_MET_NUM;
                        _PurchaseEntryVessel.PPE_ITEM_NO = data.PPE_ITEM_NO;
                        _PurchaseEntryVessel.PPE_PARTLY = data.PPE_PARTLY;
                        _PurchaseEntryVessel.PPE_PARTLY_COMPLETE = data.PPE_PARTLY_COMPLETE;
                        _PurchaseEntryVessel.PPE_BL_DATE = data.PPE_BL_DATE;
                        _PurchaseEntryVessel.PPE_BL_VOLUME_BBL = data.PPE_BL_VOLUME_BBL;
                        _PurchaseEntryVessel.PPE_BL_VOLUME_MT = data.PPE_BL_VOLUME_MT;
                        _PurchaseEntryVessel.PPE_BL_VOLUME_ML = data.PPE_BL_VOLUME_ML;
                        _PurchaseEntryVessel.PPE_GR_DATE = data.PPE_GR_DATE;
                        _PurchaseEntryVessel.PPE_GR_VOLUME_BBL = data.PPE_GR_VOLUME_BBL;
                        _PurchaseEntryVessel.PPE_GR_VOLUME_MT= data.PPE_GR_VOLUME_MT;
                        _PurchaseEntryVessel.PPE_GR_VOLUME_ML = data.PPE_GR_VOLUME_ML;
                        _PurchaseEntryVessel.PPE_FOB_PRICE_USD = data.PPE_FOB_PRICE_USD;
                        _PurchaseEntryVessel.PPE_FOB_SUPPLIER = data.PPE_FOB_SUPPLIER;
                        _PurchaseEntryVessel.PPE_FOB_TOTAL_USD = data.PPE_FOB_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_FOB_ROE = data.PPE_FOB_ROE;
                        _PurchaseEntryVessel.PPE_FOB_TOTAL_THB = data.PPE_FOB_TOTAL_THB;
                        _PurchaseEntryVessel.PPE_FRT_PRICE_USD = data.PPE_FRT_PRICE_USD;
                        _PurchaseEntryVessel.PPE_FRT_TOTAL_USD = data.PPE_FRT_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_FRT_ROE = data.PPE_FRT_ROE;
                        _PurchaseEntryVessel.PPE_FRT_TOTAL_THB = data.PPE_FRT_TOTAL_THB;
                        _PurchaseEntryVessel.PPE_FOB_FRT_TOTAL_USD= data.PPE_FOB_FRT_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_FOB_FRT_TOTAL_THB = data.PPE_FOB_FRT_TOTAL_THB;
                        _PurchaseEntryVessel.PPE_INS_PRICE_USD = data.PPE_INS_PRICE_USD;
                        _PurchaseEntryVessel.PPE_INS_TOTAL_USD = data.PPE_INS_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_INS_RATE = data.PPE_INS_RATE;
                        _PurchaseEntryVessel.PPE_STAMP_DUTY_TOTAL_USD = data.PPE_STAMP_DUTY_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_INS_STAMP_DUTY_RATE = data.PPE_INS_STAMP_DUTY_RATE;
                        _PurchaseEntryVessel.PPE_LOSS_GAIN = data.PPE_LOSS_GAIN;
                        _PurchaseEntryVessel.PPE_MRC_INS_TOTAL_USD = data.PPE_MRC_INS_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_MRC_INS_TOTAL_THB = data.PPE_MRC_INS_TOTAL_THB;
                        _PurchaseEntryVessel.PPE_MRC_INS_ROE = data.PPE_MRC_INS_ROE;
                        _PurchaseEntryVessel.PPE_TOTAL_CIF_PRICE_USD = data.PPE_TOTAL_CIF_PRICE_USD;
                        _PurchaseEntryVessel.PPE_TOTAL_CIF_TOTAL_USD = data.PPE_TOTAL_CIF_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_TOTAL_CIF_TOTAL_THB = data.PPE_TOTAL_CIF_TOTAL_THB;
                        _PurchaseEntryVessel.PPE_REMARK_FOB_USD = data.PPE_REMARK_FOB_USD;
                        _PurchaseEntryVessel.PPE_REMARK_FOB_THB = data.PPE_REMARK_FOB_THB;
                        _PurchaseEntryVessel.PPE_REMARK_FRT_USD = data.PPE_REMARK_FRT_USD;
                        _PurchaseEntryVessel.PPE_REMARK_FRT_THB = data.PPE_REMARK_FRT_THB;
                        _PurchaseEntryVessel.PPE_REMARK_INS_USD = data.PPE_REMARK_INS_USD;
                        _PurchaseEntryVessel.PPE_REMARK_INS_THB = data.PPE_REMARK_INS_THB;
                        _PurchaseEntryVessel.PPE_REMARK_IMPORT_DUTY_USD= data.PPE_REMARK_IMPORT_DUTY_USD;
                        _PurchaseEntryVessel.PPE_REMARK_IMPORT_DUTY_THB = data.PPE_REMARK_IMPORT_DUTY_THB;
                        _PurchaseEntryVessel.PPE_REMARK_IMPORT_DUTY_ROE = data.PPE_REMARK_IMPORT_DUTY_ROE;
                        _PurchaseEntryVessel.PPE_REMARK_TOTAL_USD = data.PPE_REMARK_TOTAL_USD;
                        _PurchaseEntryVessel.PPE_REMARK_TOTAL_THB = data.PPE_REMARK_TOTAL_THB;
                        _PurchaseEntryVessel.PPE_CAL_VOLUME_UNIT = data.PPE_CAL_VOLUME_UNIT;
                        _PurchaseEntryVessel.PPE_CAL_INVOICE_FIGURE = data.PPE_CAL_INVOICE_FIGURE;
                        _PurchaseEntryVessel.PPE_DOCUMENT_DATE = data.PPE_DOCUMENT_DATE;
                        _PurchaseEntryVessel.PPE_POSTING_DATE = data.PPE_POSTING_DATE;                        
                        _PurchaseEntryVessel.PPE_ACCRUAL_TYPE = data.PPE_ACCRUAL_TYPE;
                        _PurchaseEntryVessel.PPE_REVERSE_DATE_FOR_TYPE_A = data.PPE_REVERSE_DATE_FOR_TYPE_A;
                        _PurchaseEntryVessel.PPE_SAP_FI_DOC_NO = data.PPE_SAP_FI_DOC_NO;
                        _PurchaseEntryVessel.PPE_SAP_FI_DOC_STATUS = data.PPE_SAP_FI_DOC_STATUS;
                        _PurchaseEntryVessel.PPE_SAP_ERROR_MESSAGE = data.PPE_SAP_ERROR_MESSAGE;
                        _PurchaseEntryVessel.PPE_CREATED_DATE = data.PPE_CREATED_DATE;
                        _PurchaseEntryVessel.PPE_CREATED_BY = data.PPE_CREATED_BY;
                        _PurchaseEntryVessel.PPE_UPDATED_DATE = data.PPE_UPDATED_DATE;
                        _PurchaseEntryVessel.PPE_UPDATED_BY = data.PPE_UPDATED_BY;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePPE(PCF_PURCHASE_ENTRY data, EntityCPAIEngine context)
        {
            try
            {
                var _PurchaseEntryVessel = context.PCF_PURCHASE_ENTRY.Find(data.PPE_TRIP_NO, data.PPE_MET_NUM, data.PPE_ITEM_NO);
                if (_PurchaseEntryVessel != null)
                {
                    _PurchaseEntryVessel.PPE_TRIP_NO = data.PPE_TRIP_NO;
                    _PurchaseEntryVessel.PPE_MET_NUM = data.PPE_MET_NUM;
                    _PurchaseEntryVessel.PPE_ITEM_NO = data.PPE_ITEM_NO;
                    _PurchaseEntryVessel.PPE_PARTLY = data.PPE_PARTLY;
                    _PurchaseEntryVessel.PPE_PARTLY_COMPLETE = data.PPE_PARTLY_COMPLETE;
                    _PurchaseEntryVessel.PPE_BL_DATE = data.PPE_BL_DATE;
                    _PurchaseEntryVessel.PPE_BL_VOLUME_BBL = data.PPE_BL_VOLUME_BBL;
                    _PurchaseEntryVessel.PPE_BL_VOLUME_MT = data.PPE_BL_VOLUME_MT;
                    _PurchaseEntryVessel.PPE_BL_VOLUME_ML = data.PPE_BL_VOLUME_ML;
                    _PurchaseEntryVessel.PPE_GR_DATE = data.PPE_GR_DATE;
                    _PurchaseEntryVessel.PPE_GR_VOLUME_BBL = data.PPE_GR_VOLUME_BBL;
                    _PurchaseEntryVessel.PPE_GR_VOLUME_MT = data.PPE_GR_VOLUME_MT;
                    _PurchaseEntryVessel.PPE_GR_VOLUME_ML = data.PPE_GR_VOLUME_ML;
                    _PurchaseEntryVessel.PPE_FOB_PRICE_USD = data.PPE_FOB_PRICE_USD;
                    _PurchaseEntryVessel.PPE_FOB_SUPPLIER = data.PPE_FOB_SUPPLIER;
                    _PurchaseEntryVessel.PPE_FOB_TOTAL_USD = data.PPE_FOB_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_FOB_ROE = data.PPE_FOB_ROE;
                    _PurchaseEntryVessel.PPE_FOB_TOTAL_THB = data.PPE_FOB_TOTAL_THB;
                    _PurchaseEntryVessel.PPE_FRT_PRICE_USD = data.PPE_FRT_PRICE_USD;
                    _PurchaseEntryVessel.PPE_FRT_TOTAL_USD = data.PPE_FRT_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_FRT_ROE = data.PPE_FRT_ROE;
                    _PurchaseEntryVessel.PPE_FRT_TOTAL_THB = data.PPE_FRT_TOTAL_THB;
                    _PurchaseEntryVessel.PPE_FOB_FRT_TOTAL_USD = data.PPE_FOB_FRT_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_FOB_FRT_TOTAL_THB = data.PPE_FOB_FRT_TOTAL_THB;
                    _PurchaseEntryVessel.PPE_INS_PRICE_USD = data.PPE_INS_PRICE_USD;
                    _PurchaseEntryVessel.PPE_INS_TOTAL_USD = data.PPE_INS_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_INS_RATE = data.PPE_INS_RATE;
                    _PurchaseEntryVessel.PPE_STAMP_DUTY_TOTAL_USD = data.PPE_STAMP_DUTY_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_INS_STAMP_DUTY_RATE = data.PPE_INS_STAMP_DUTY_RATE;
                    _PurchaseEntryVessel.PPE_LOSS_GAIN = data.PPE_LOSS_GAIN;
                    _PurchaseEntryVessel.PPE_MRC_INS_TOTAL_USD = data.PPE_MRC_INS_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_MRC_INS_TOTAL_THB = data.PPE_MRC_INS_TOTAL_THB;
                    _PurchaseEntryVessel.PPE_MRC_INS_ROE = data.PPE_MRC_INS_ROE;
                    _PurchaseEntryVessel.PPE_TOTAL_CIF_PRICE_USD = data.PPE_TOTAL_CIF_PRICE_USD;
                    _PurchaseEntryVessel.PPE_TOTAL_CIF_TOTAL_USD = data.PPE_TOTAL_CIF_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_TOTAL_CIF_TOTAL_THB = data.PPE_TOTAL_CIF_TOTAL_THB;
                    _PurchaseEntryVessel.PPE_REMARK_FOB_USD = data.PPE_REMARK_FOB_USD;
                    _PurchaseEntryVessel.PPE_REMARK_FOB_THB = data.PPE_REMARK_FOB_THB;
                    _PurchaseEntryVessel.PPE_REMARK_FRT_USD = data.PPE_REMARK_FRT_USD;
                    _PurchaseEntryVessel.PPE_REMARK_FRT_THB = data.PPE_REMARK_FRT_THB;
                    _PurchaseEntryVessel.PPE_REMARK_INS_USD = data.PPE_REMARK_INS_USD;
                    _PurchaseEntryVessel.PPE_REMARK_INS_THB = data.PPE_REMARK_INS_THB;
                    _PurchaseEntryVessel.PPE_REMARK_IMPORT_DUTY_USD = data.PPE_REMARK_IMPORT_DUTY_USD;
                    _PurchaseEntryVessel.PPE_REMARK_IMPORT_DUTY_THB = data.PPE_REMARK_IMPORT_DUTY_THB;
                    _PurchaseEntryVessel.PPE_REMARK_IMPORT_DUTY_ROE = data.PPE_REMARK_IMPORT_DUTY_ROE;
                    _PurchaseEntryVessel.PPE_REMARK_TOTAL_USD = data.PPE_REMARK_TOTAL_USD;
                    _PurchaseEntryVessel.PPE_REMARK_TOTAL_THB = data.PPE_REMARK_TOTAL_THB;
                    _PurchaseEntryVessel.PPE_CAL_VOLUME_UNIT = data.PPE_CAL_VOLUME_UNIT;
                    _PurchaseEntryVessel.PPE_CAL_INVOICE_FIGURE = data.PPE_CAL_INVOICE_FIGURE;
                    _PurchaseEntryVessel.PPE_DOCUMENT_DATE = data.PPE_DOCUMENT_DATE;
                    _PurchaseEntryVessel.PPE_POSTING_DATE = data.PPE_POSTING_DATE;
                    _PurchaseEntryVessel.PPE_ACCRUAL_TYPE = data.PPE_ACCRUAL_TYPE;
                    _PurchaseEntryVessel.PPE_REVERSE_DATE_FOR_TYPE_A = data.PPE_REVERSE_DATE_FOR_TYPE_A;
                    _PurchaseEntryVessel.PPE_SAP_FI_DOC_NO = data.PPE_SAP_FI_DOC_NO;
                    _PurchaseEntryVessel.PPE_SAP_FI_DOC_STATUS = data.PPE_SAP_FI_DOC_STATUS;
                    _PurchaseEntryVessel.PPE_SAP_ERROR_MESSAGE = data.PPE_SAP_ERROR_MESSAGE;
                    _PurchaseEntryVessel.PPE_CREATED_DATE = data.PPE_CREATED_DATE;
                    _PurchaseEntryVessel.PPE_CREATED_BY = data.PPE_CREATED_BY;
                    _PurchaseEntryVessel.PPE_UPDATED_DATE = data.PPE_UPDATED_DATE;
                    _PurchaseEntryVessel.PPE_UPDATED_BY = data.PPE_UPDATED_BY;

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPPF(PCF_PURCHASE_ENTRY_FREIGHT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.PCF_PURCHASE_ENTRY_FREIGHT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPPF(PCF_PURCHASE_ENTRY_FREIGHT data,EntityCPAIEngine context)
        {
            try
            {
                context.PCF_PURCHASE_ENTRY_FREIGHT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePPF(PCF_PURCHASE_ENTRY_FREIGHT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _PurchaseEntryVesselFreight = context.PCF_PURCHASE_ENTRY_FREIGHT.Find(data.TRIP_NO, data.MET_NUM, data.ITEM_NO, data.FREIGHT_ITEM_NO);
                    if (_PurchaseEntryVesselFreight != null)
                    {
                        _PurchaseEntryVesselFreight.TRIP_NO = data.TRIP_NO;
                        _PurchaseEntryVesselFreight.MET_NUM = data.MET_NUM;
                        _PurchaseEntryVesselFreight.ITEM_NO = data.ITEM_NO;
                        _PurchaseEntryVesselFreight.FREIGHT_ITEM_NO = data.FREIGHT_ITEM_NO;
                        _PurchaseEntryVesselFreight.FRT_ROE = data.FRT_ROE;
                        _PurchaseEntryVesselFreight.FRT_TOTAL_THB = data.FRT_TOTAL_THB;
                        _PurchaseEntryVesselFreight.FRT_TOTAL_USD = data.FRT_TOTAL_USD;

                        context.SaveChanges();
                    }
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePPF(PCF_PURCHASE_ENTRY_FREIGHT data, EntityCPAIEngine context)
        {
            try
            {
                var _PurchaseEntryVesselFreight = context.PCF_PURCHASE_ENTRY_FREIGHT.Find(data.TRIP_NO);
                if (_PurchaseEntryVesselFreight != null)
                {
                    _PurchaseEntryVesselFreight.TRIP_NO = data.TRIP_NO;
                    _PurchaseEntryVesselFreight.MET_NUM = data.MET_NUM;
                    _PurchaseEntryVesselFreight.ITEM_NO = data.ITEM_NO;
                    _PurchaseEntryVesselFreight.FREIGHT_ITEM_NO = data.FREIGHT_ITEM_NO;
                    _PurchaseEntryVesselFreight.FRT_ROE = data.FRT_ROE;
                    _PurchaseEntryVesselFreight.FRT_TOTAL_THB = data.FRT_TOTAL_THB;
                    _PurchaseEntryVesselFreight.FRT_TOTAL_USD = data.FRT_TOTAL_USD;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPPI(PCF_PURCHASE_ENTRY_ITEM data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.PCF_PURCHASE_ENTRY_ITEM.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPPI(PCF_PURCHASE_ENTRY_ITEM data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_PURCHASE_ENTRY_ITEM.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePPI(PCF_PURCHASE_ENTRY_ITEM data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _PurchaseEntryVesselItem = context.PCF_PURCHASE_ENTRY_ITEM.Find(data.PPI_TRIP_NO, data.PPI_MET_NUM, data.PPI_ENTRY_ITEM_NO,data.PPI_MAT_ITEM_NO);
                    if (_PurchaseEntryVesselItem != null)
                    {
                        _PurchaseEntryVesselItem.PPI_TRIP_NO = data.PPI_TRIP_NO;
                        _PurchaseEntryVesselItem.PPI_MET_NUM = data.PPI_MET_NUM;
                        _PurchaseEntryVesselItem.PPI_ENTRY_ITEM_NO = data.PPI_ENTRY_ITEM_NO;
                        _PurchaseEntryVesselItem.PPI_MAT_ITEM_NO = data.PPI_MAT_ITEM_NO;
                        _PurchaseEntryVesselItem.PPI_VOLUME_BBL = data.PPI_VOLUME_BBL;
                        _PurchaseEntryVesselItem.PPI_UNIT_PRICE = data.PPI_UNIT_PRICE;
                        _PurchaseEntryVesselItem.PPI_TOTAL_USD = data.PPI_TOTAL_USD;
                        _PurchaseEntryVesselItem.PPI_ROE = data.PPI_ROE;
                        _PurchaseEntryVesselItem.PPI_TOTAL_THB = data.PPI_TOTAL_THB;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePPI(PCF_PURCHASE_ENTRY_ITEM data, EntityCPAIEngine context)
        {
            try
            {
                var _PurchaseEntryVesselItem = context.PCF_PURCHASE_ENTRY_ITEM.Find(data.PPI_TRIP_NO, data.PPI_MET_NUM, data.PPI_ENTRY_ITEM_NO, data.PPI_MAT_ITEM_NO);
                if (_PurchaseEntryVesselItem != null)
                {
                    _PurchaseEntryVesselItem.PPI_TRIP_NO = data.PPI_TRIP_NO;
                    _PurchaseEntryVesselItem.PPI_MET_NUM = data.PPI_MET_NUM;
                    _PurchaseEntryVesselItem.PPI_ENTRY_ITEM_NO = data.PPI_ENTRY_ITEM_NO;
                    _PurchaseEntryVesselItem.PPI_MAT_ITEM_NO = data.PPI_MAT_ITEM_NO;
                    _PurchaseEntryVesselItem.PPI_VOLUME_BBL = data.PPI_VOLUME_BBL;
                    _PurchaseEntryVesselItem.PPI_UNIT_PRICE = data.PPI_UNIT_PRICE;
                    _PurchaseEntryVesselItem.PPI_TOTAL_USD = data.PPI_TOTAL_USD;
                    _PurchaseEntryVesselItem.PPI_ROE = data.PPI_ROE;
                    _PurchaseEntryVesselItem.PPI_TOTAL_THB = data.PPI_TOTAL_THB;

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPricingToSAP(PRICING_TO_SAP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.PRICING_TO_SAP.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPricingToSAP(PRICING_TO_SAP data, EntityCPAIEngine context)
        {
            try
            {
                context.PRICING_TO_SAP.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePricingToSAP(PRICING_TO_SAP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _PricingToSAP = context.PRICING_TO_SAP.Find(data.CODE);
                    if (_PricingToSAP != null)
                    {
                        _PricingToSAP.TRANS_TYPE = data.TRANS_TYPE;
                        _PricingToSAP.COM_CODE = data.COM_CODE;
                        _PricingToSAP.TRIP_NO = data.TRIP_NO;
                        _PricingToSAP.CREATE_DATE = data.CREATE_DATE;
                        _PricingToSAP.CMCS_ETA = data.CMCS_ETA;
                        _PricingToSAP.MAT_NUM = data.MAT_NUM;
                        _PricingToSAP.PARTLY_ORDER = data.PARTLY_ORDER;
                        _PricingToSAP.VENDOR_NO = data.VENDOR_NO;
                        _PricingToSAP.PLANT = data.PLANT;
                        _PricingToSAP.GROUP_LOCATION = data.GROUP_LOCATION;
                        _PricingToSAP.EXCHANGE_RATE = data.EXCHANGE_RATE;
                        _PricingToSAP.BASE_PRICE = data.BASE_PRICE;
                        _PricingToSAP.PREMIUM_OSP = data.PREMIUM_OSP;
                        _PricingToSAP.PREMIUM_MARKET = data.PREMIUM_MARKET;
                        _PricingToSAP.OTHER_COST = data.OTHER_COST;
                        _PricingToSAP.FREIGHT_PRICE = data.FREIGHT_PRICE;
                        _PricingToSAP.INSURANCE_PRICE = data.INSURANCE_PRICE;
                        _PricingToSAP.TOTAL_AMOUNT_THB = data.TOTAL_AMOUNT_THB;
                        _PricingToSAP.TOTAL_AMOUNT_USD = data.TOTAL_AMOUNT_USD;
                        _PricingToSAP.QTY = data.QTY;
                        _PricingToSAP.QTY_UNIT = data.QTY_UNIT;
                        _PricingToSAP.CURRENCY = data.CURRENCY;
                        _PricingToSAP.SAP_RECEIVED_DATE = data.SAP_RECEIVED_DATE;
                        _PricingToSAP.STATUS = data.STATUS;
                        _PricingToSAP.GROUP_DATA = data.GROUP_DATA;
                        _PricingToSAP.FLAG_PLANNING = data.FLAG_PLANNING;
                        _PricingToSAP.DUE_DATE = data.DUE_DATE;
                        _PricingToSAP.REMARK = data.REMARK;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePricingToSAP(PRICING_TO_SAP data, EntityCPAIEngine context)
        {
            try
            {
                var _PricingToSAP = context.PRICING_TO_SAP.Find(data.CODE);
                if (_PricingToSAP != null)
                {
                    _PricingToSAP.TRANS_TYPE = data.TRANS_TYPE;
                    _PricingToSAP.COM_CODE = data.COM_CODE;
                    _PricingToSAP.TRIP_NO = data.TRIP_NO;
                    _PricingToSAP.CREATE_DATE = data.CREATE_DATE;
                    _PricingToSAP.CMCS_ETA = data.CMCS_ETA;
                    _PricingToSAP.MAT_NUM = data.MAT_NUM;
                    _PricingToSAP.PARTLY_ORDER = data.PARTLY_ORDER;
                    _PricingToSAP.VENDOR_NO = data.VENDOR_NO;
                    _PricingToSAP.PLANT = data.PLANT;
                    _PricingToSAP.GROUP_LOCATION = data.GROUP_LOCATION;
                    _PricingToSAP.EXCHANGE_RATE = data.EXCHANGE_RATE;
                    _PricingToSAP.BASE_PRICE = data.BASE_PRICE;
                    _PricingToSAP.PREMIUM_OSP = data.PREMIUM_OSP;
                    _PricingToSAP.PREMIUM_MARKET = data.PREMIUM_MARKET;
                    _PricingToSAP.OTHER_COST = data.OTHER_COST;
                    _PricingToSAP.FREIGHT_PRICE = data.FREIGHT_PRICE;
                    _PricingToSAP.INSURANCE_PRICE = data.INSURANCE_PRICE;
                    _PricingToSAP.TOTAL_AMOUNT_THB = data.TOTAL_AMOUNT_THB;
                    _PricingToSAP.TOTAL_AMOUNT_USD = data.TOTAL_AMOUNT_USD;
                    _PricingToSAP.QTY = data.QTY;
                    _PricingToSAP.QTY_UNIT = data.QTY_UNIT;
                    _PricingToSAP.CURRENCY = data.CURRENCY;
                    _PricingToSAP.SAP_RECEIVED_DATE = data.SAP_RECEIVED_DATE;
                    _PricingToSAP.STATUS = data.STATUS;
                    _PricingToSAP.GROUP_DATA = data.GROUP_DATA;
                    _PricingToSAP.FLAG_PLANNING = data.FLAG_PLANNING;
                    _PricingToSAP.DUE_DATE = data.DUE_DATE;
                    _PricingToSAP.REMARK = data.REMARK;

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertReversedFiDoc(PCF_REVERSED_FI_DOC data)
        {
            using (var context = new EntityCPAIEngine())
            {
                try
                {
                    context.PCF_REVERSED_FI_DOC.Add(data);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void InsertReversedFiDoc(PCF_REVERSED_FI_DOC data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_REVERSED_FI_DOC.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveReversedFiDoc(PCF_REVERSED_FI_DOC data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PCF_REVERSED_FI_DOC.Find(data.TRIP_NO, data.MET_NUM, data.ITEM_NO);
                    if (search == null) //Add new record
                    {
                        context.PCF_REVERSED_FI_DOC.Add(data);
                        context.SaveChanges();
                    }
                    else //Update the existing record
                    {

                        search.ITEM_NO = data.ITEM_NO;
                        search.MET_NUM = data.MET_NUM;
                        search.REVERSAL_REASON = data.REVERSAL_REASON;
                        search.SAP_FI_DOC_NO = data.SAP_FI_DOC_NO;
                        search.SAP_REVERSED_DOC_NO = data.SAP_REVERSED_DOC_NO;
                        search.TRIP_NO = data.TRIP_NO;
                        search.UPDATED_BY = data.UPDATED_BY;
                        search.UPDATED_DATE = data.UPDATED_DATE;

                        context.SaveChanges();
                    }
                    PCF_PURCHASE_ENTRY temp = new PCF_PURCHASE_ENTRY();
                    temp.PPE_SAP_FI_DOC_NO = data.SAP_FI_DOC_NO;
                    var pcf_daily = context.PCF_PURCHASE_ENTRY.Where(p=>p.PPE_SAP_FI_DOC_NO == temp.PPE_SAP_FI_DOC_NO).FirstOrDefault();
                    if (pcf_daily != null)
                    {
                        pcf_daily.PPE_SAP_FI_DOC_NO = "";
                        pcf_daily.PPE_SAP_FI_DOC_STATUS = "";
                        pcf_daily.PPE_UPDATED_BY = data.UPDATED_BY;
                        pcf_daily.PPE_UPDATED_DATE = data.UPDATED_DATE;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
