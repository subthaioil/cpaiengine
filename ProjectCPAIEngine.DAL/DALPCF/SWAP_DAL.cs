﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class SWAP_CRUDE_DAL
    {
        public SWAP_CRUDE Get(SWAP_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                           && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWC_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                    if (_data != null)
                    {
                        return _data;
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public SWAP_CRUDE Get(SWAP_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                       && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWC_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                if (_data != null)
                {
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public void Save(SWAP_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.SWAP_CRUDE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(SWAP_CRUDE data, EntityCPAIEngine context)
        {
            try
            {                
                context.SWAP_CRUDE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SWAP_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                           && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWC_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                    if (_data != null)
                    {
                        _data.SWC_TRIP_NO = data.SWC_TRIP_NO;
                        _data.SWC_ITEM_NO = data.SWC_ITEM_NO;
                        _data.SWC_PO_NO = data.SWC_PO_NO;
                        _data.SWC_VESSEL_ID = data.SWC_VESSEL_ID;
                        _data.SWC_SOLDTO = data.SWC_SOLDTO;
                        _data.SWC_SHIPTO = data.SWC_SHIPTO;
                        _data.SWC_DELIVERY_DATE = data.SWC_DELIVERY_DATE;
                        _data.SWC_PLANT = data.SWC_PLANT;
                        _data.SWC_CRUDE_TYPE_ID = data.SWC_CRUDE_TYPE_ID;
                        _data.SWC_VOLUME_BBL = data.SWC_VOLUME_BBL;
                        _data.SWC_VOLUME_MT = data.SWC_VOLUME_MT;
                        _data.SWC_VOLUME_LITE = data.SWC_VOLUME_LITE;
                        _data.SWC_OUTTURN_BBL = data.SWC_OUTTURN_BBL;
                        _data.SWC_OUTTURN_LITE = data.SWC_OUTTURN_LITE;
                        _data.SWC_OUTTURN_MT = data.SWC_OUTTURN_MT;
                        _data.SWC_PRICE_PER = data.SWC_PRICE_PER;
                        _data.SWC_PRICE = data.SWC_PRICE;
                        _data.SWC_UNIT_ID = data.SWC_UNIT_ID;
                        _data.SWC_EXCHANGE_RATE = data.SWC_EXCHANGE_RATE;
                        _data.SWC_TOTAL = data.SWC_TOTAL;
                        _data.SWC_UNIT_TOTAL = data.SWC_UNIT_TOTAL;
                        _data.SWC_TYPE = data.SWC_TYPE;
                        _data.SWC_CREATE_DATE = data.SWC_CREATE_DATE;
                        _data.SWC_CREATE_BY = data.SWC_CREATE_BY;
                        _data.SWC_LAST_MODIFY_DATE = data.SWC_LAST_MODIFY_DATE;
                        _data.SWC_LAST_MODIFY_BY = data.SWC_LAST_MODIFY_BY;
                        _data.SWC_STATUS = data.SWC_STATUS;
                        _data.SWC_CREATE_TIME = data.SWC_CREATE_TIME;
                        _data.SWC_LAST_MODIFY_TIME = data.SWC_LAST_MODIFY_TIME;
                        _data.SWC_RELEASE = data.SWC_RELEASE;
                        _data.SWC_TANK = data.SWC_TANK;
                        _data.SWC_SALEORDER = data.SWC_SALEORDER;
                        _data.SWC_PRICE_RELEASE = data.SWC_PRICE_RELEASE;
                        _data.SWC_REMARK = data.SWC_REMARK;
                        _data.SWC_DISTRI_CHANN = data.SWC_DISTRI_CHANN;
                        _data.SWC_TABLE_NAME = data.SWC_TABLE_NAME;
                        _data.SWC_MEMO = data.SWC_MEMO;
                        _data.SWC_DATE_SAP = data.SWC_DATE_SAP;
                        _data.SWC_FI_DOC = data.SWC_FI_DOC;
                        _data.SWC_DO_NO = data.SWC_DO_NO;
                        _data.SWC_STATUS_TO_SAP = data.SWC_STATUS_TO_SAP;
                        _data.SWC_SEQ = data.SWC_SEQ;
                        _data.SWC_INCOTERM_TYPE = data.SWC_INCOTERM_TYPE;
                        _data.SWC_TEMP = data.SWC_TEMP;
                        _data.SWC_DENSITY = data.SWC_DENSITY;
                        _data.SWC_STORAGE_LOCATION = data.SWC_STORAGE_LOCATION;
                        _data.SWC_SUPPLIER = data.SWC_SUPPLIER;
                        _data.SWC_DISTRI_CHANN_SAP = data.SWC_DISTRI_CHANN_SAP;
                        _data.SWC_VAT_NONVAT = data.SWC_VAT_NONVAT;
                        _data.SWC_UPDATE_DO = data.SWC_UPDATE_DO;
                        _data.SWC_UPDATE_GI = data.SWC_UPDATE_GI;
                        _data.SWC_FI_DOC_REVERSE = data.SWC_FI_DOC_REVERSE;                        
                        context.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SWAP_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                           && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWC_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                if (_data != null)
                {
                    _data.SWC_TRIP_NO = data.SWC_TRIP_NO;
                    _data.SWC_ITEM_NO = data.SWC_ITEM_NO;
                    _data.SWC_PO_NO = data.SWC_PO_NO;
                    _data.SWC_VESSEL_ID = data.SWC_VESSEL_ID;
                    _data.SWC_SOLDTO = data.SWC_SOLDTO;
                    _data.SWC_SHIPTO = data.SWC_SHIPTO;
                    _data.SWC_DELIVERY_DATE = data.SWC_DELIVERY_DATE;
                    _data.SWC_PLANT = data.SWC_PLANT;
                    _data.SWC_CRUDE_TYPE_ID = data.SWC_CRUDE_TYPE_ID;
                    _data.SWC_VOLUME_BBL = data.SWC_VOLUME_BBL;
                    _data.SWC_VOLUME_MT = data.SWC_VOLUME_MT;
                    _data.SWC_VOLUME_LITE = data.SWC_VOLUME_LITE;
                    _data.SWC_OUTTURN_BBL = data.SWC_OUTTURN_BBL;
                    _data.SWC_OUTTURN_LITE = data.SWC_OUTTURN_LITE;
                    _data.SWC_OUTTURN_MT = data.SWC_OUTTURN_MT;
                    _data.SWC_PRICE_PER = data.SWC_PRICE_PER;
                    _data.SWC_PRICE = data.SWC_PRICE;
                    _data.SWC_UNIT_ID = data.SWC_UNIT_ID;
                    _data.SWC_EXCHANGE_RATE = data.SWC_EXCHANGE_RATE;
                    _data.SWC_TOTAL = data.SWC_TOTAL;
                    _data.SWC_UNIT_TOTAL = data.SWC_UNIT_TOTAL;
                    _data.SWC_TYPE = data.SWC_TYPE;
                    _data.SWC_LAST_MODIFY_DATE = data.SWC_LAST_MODIFY_DATE;
                    _data.SWC_LAST_MODIFY_BY = data.SWC_LAST_MODIFY_BY;
                    _data.SWC_STATUS = data.SWC_STATUS;
                    _data.SWC_LAST_MODIFY_TIME = data.SWC_LAST_MODIFY_TIME;
                    _data.SWC_RELEASE = data.SWC_RELEASE;
                    _data.SWC_TANK = data.SWC_TANK;
                    _data.SWC_SALEORDER = data.SWC_SALEORDER;
                    _data.SWC_PRICE_RELEASE = data.SWC_PRICE_RELEASE;
                    _data.SWC_REMARK = data.SWC_REMARK;
                    _data.SWC_DISTRI_CHANN = data.SWC_DISTRI_CHANN;
                    _data.SWC_TABLE_NAME = data.SWC_TABLE_NAME;
                    _data.SWC_MEMO = data.SWC_MEMO;
                    _data.SWC_DATE_SAP = data.SWC_DATE_SAP;
                    _data.SWC_FI_DOC = data.SWC_FI_DOC;
                    _data.SWC_DO_NO = data.SWC_DO_NO;
                    _data.SWC_STATUS_TO_SAP = data.SWC_STATUS_TO_SAP;
                    _data.SWC_SEQ = data.SWC_SEQ;
                    _data.SWC_INCOTERM_TYPE = data.SWC_INCOTERM_TYPE;
                    _data.SWC_TEMP = data.SWC_TEMP;
                    _data.SWC_DENSITY = data.SWC_DENSITY;
                    _data.SWC_STORAGE_LOCATION = data.SWC_STORAGE_LOCATION;
                    _data.SWC_SUPPLIER = data.SWC_SUPPLIER;
                    _data.SWC_DISTRI_CHANN_SAP = data.SWC_DISTRI_CHANN_SAP;
                    _data.SWC_VAT_NONVAT = data.SWC_VAT_NONVAT;
                    _data.SWC_UPDATE_DO = data.SWC_UPDATE_DO;
                    _data.SWC_UPDATE_GI = data.SWC_UPDATE_GI;
                    _data.SWC_FI_DOC_REVERSE = data.SWC_FI_DOC_REVERSE;                    
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Load(ref SWAP_CRUDE data, ref List<SWAP_PRICE> data_details)
        {
            var SWC_TRIP_NO = data.SWC_TRIP_NO;
            var SWC_CRUDE_TYPE_ID = data.SWC_CRUDE_TYPE_ID;
            var SWC_ITEM_NO = data.SWC_ITEM_NO;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(SWC_TRIP_NO.ToUpper())
                                                       && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(SWC_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWC_ITEM_NO.ToUpper().Equals(SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                    if (_data != null)
                    {
                        var _data_details = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(SWC_TRIP_NO.ToUpper())
                                                               && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(SWC_CRUDE_TYPE_ID.ToUpper())
                                                               && x.SWP_ITEM_NO.ToUpper().Equals(SWC_ITEM_NO.ToUpper())).ToList();
                        if (_data_details != null)
                        {
                            data_details = _data_details;
                        }
                        data = _data;
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Load(ref SWAP_CRUDE data, ref List<SWAP_PRICE> data_details, EntityCPAIEngine context)
        {
            var SWC_TRIP_NO = data.SWC_TRIP_NO;
            var SWC_CRUDE_TYPE_ID = data.SWC_CRUDE_TYPE_ID;
            var SWC_ITEM_NO = data.SWC_ITEM_NO;
            try
            {
                var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(SWC_TRIP_NO.ToUpper())
                                                       && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(SWC_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWC_ITEM_NO.ToUpper().Equals(SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                if (_data != null)
                {
                    var _data_details = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(SWC_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(SWC_ITEM_NO.ToUpper())).ToList();
                    if (_data_details != null)
                    {
                        data_details = _data_details;
                    }
                    data = _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(SWAP_CRUDE data, List<SWAP_PRICE> data_details)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data_details = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                       && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWP_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList();
                    if (_data_details.Any())
                    {
                        context.SWAP_PRICE.RemoveRange(_data_details);
                    }

                    context.SWAP_CRUDE.Add(data);
                    context.SWAP_PRICE.AddRange(data_details);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(SWAP_CRUDE data, List<SWAP_PRICE> data_details, EntityCPAIEngine context)
        {
            try
            {
                var _data_details = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                       && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWP_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList();
                if (_data_details.Any())
                {
                    context.SWAP_PRICE.RemoveRange(_data_details);
                }

                context.SWAP_CRUDE.Add(data);
                context.SWAP_PRICE.AddRange(data_details);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public void Update(SWAP_CRUDE data, List<SWAP_PRICE> data_details)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                           && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWC_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                    if (_data != null)
                    {
                        _data.SWC_TRIP_NO = data.SWC_TRIP_NO;
                        _data.SWC_ITEM_NO = data.SWC_ITEM_NO;
                        _data.SWC_PO_NO = data.SWC_PO_NO;
                        _data.SWC_VESSEL_ID = data.SWC_VESSEL_ID;
                        _data.SWC_SOLDTO = data.SWC_SOLDTO;
                        _data.SWC_SHIPTO = data.SWC_SHIPTO;
                        _data.SWC_DELIVERY_DATE = data.SWC_DELIVERY_DATE;
                        _data.SWC_PLANT = data.SWC_PLANT;
                        _data.SWC_CRUDE_TYPE_ID = data.SWC_CRUDE_TYPE_ID;
                        _data.SWC_VOLUME_BBL = data.SWC_VOLUME_BBL;
                        _data.SWC_VOLUME_MT = data.SWC_VOLUME_MT;
                        _data.SWC_VOLUME_LITE = data.SWC_VOLUME_LITE;
                        _data.SWC_OUTTURN_BBL = data.SWC_OUTTURN_BBL;
                        _data.SWC_OUTTURN_LITE = data.SWC_OUTTURN_LITE;
                        _data.SWC_OUTTURN_MT = data.SWC_OUTTURN_MT;
                        _data.SWC_PRICE_PER = data.SWC_PRICE_PER;
                        _data.SWC_PRICE = data.SWC_PRICE;
                        _data.SWC_UNIT_ID = data.SWC_UNIT_ID;
                        _data.SWC_EXCHANGE_RATE = data.SWC_EXCHANGE_RATE;
                        _data.SWC_TOTAL = data.SWC_TOTAL;
                        _data.SWC_UNIT_TOTAL = data.SWC_UNIT_TOTAL;
                        _data.SWC_TYPE = data.SWC_TYPE;
                        _data.SWC_CREATE_DATE = data.SWC_CREATE_DATE;
                        _data.SWC_CREATE_BY = data.SWC_CREATE_BY;
                        _data.SWC_LAST_MODIFY_DATE = data.SWC_LAST_MODIFY_DATE;
                        _data.SWC_LAST_MODIFY_BY = data.SWC_LAST_MODIFY_BY;
                        _data.SWC_STATUS = data.SWC_STATUS;
                        _data.SWC_CREATE_TIME = data.SWC_CREATE_TIME;
                        _data.SWC_LAST_MODIFY_TIME = data.SWC_LAST_MODIFY_TIME;
                        _data.SWC_RELEASE = data.SWC_RELEASE;
                        _data.SWC_TANK = data.SWC_TANK;
                        _data.SWC_SALEORDER = data.SWC_SALEORDER;
                        _data.SWC_PRICE_RELEASE = data.SWC_PRICE_RELEASE;
                        _data.SWC_REMARK = data.SWC_REMARK;
                        _data.SWC_DISTRI_CHANN = data.SWC_DISTRI_CHANN;
                        _data.SWC_TABLE_NAME = data.SWC_TABLE_NAME;
                        _data.SWC_MEMO = data.SWC_MEMO;
                        _data.SWC_DATE_SAP = data.SWC_DATE_SAP;
                        _data.SWC_FI_DOC = data.SWC_FI_DOC;
                        _data.SWC_DO_NO = data.SWC_DO_NO;
                        _data.SWC_STATUS_TO_SAP = data.SWC_STATUS_TO_SAP;
                        _data.SWC_SEQ = data.SWC_SEQ;
                        _data.SWC_INCOTERM_TYPE = data.SWC_INCOTERM_TYPE;
                        _data.SWC_TEMP = data.SWC_TEMP;
                        _data.SWC_DENSITY = data.SWC_DENSITY;
                        _data.SWC_STORAGE_LOCATION = data.SWC_STORAGE_LOCATION;
                        _data.SWC_SUPPLIER = data.SWC_SUPPLIER;
                        _data.SWC_DISTRI_CHANN_SAP = data.SWC_DISTRI_CHANN_SAP;
                        _data.SWC_VAT_NONVAT = data.SWC_VAT_NONVAT;
                        _data.SWC_UPDATE_DO = data.SWC_UPDATE_DO;
                        _data.SWC_UPDATE_GI = data.SWC_UPDATE_GI;
                        _data.SWC_FI_DOC_REVERSE = data.SWC_FI_DOC_REVERSE;

                        foreach (var data_detail in data_details)
                        {
                            var _data_detail = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data_detail.SWP_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data_detail.SWP_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(data_detail.SWP_ITEM_NO.ToUpper())
                                                           && x.SWP_NUM.Equals(data_detail.SWP_NUM)).ToList().FirstOrDefault();
                            if (_data_detail != null)
                            {
                                _data_detail.SWP_TRIP_NO = data_detail.SWP_TRIP_NO;
                                _data_detail.SWP_ITEM_NO = data_detail.SWP_ITEM_NO;
                                _data_detail.SWP_CRUDE_TYPE_ID = data_detail.SWP_CRUDE_TYPE_ID;
                                _data_detail.SWP_NUM = data_detail.SWP_NUM;
                                _data_detail.SWP_TYPE = data_detail.SWP_TYPE;
                                _data_detail.SWP_PRICE = data_detail.SWP_PRICE;
                                _data_detail.SWP_DATE_PRICE = data_detail.SWP_DATE_PRICE;
                                _data_detail.SWP_UNIT_ID = data_detail.SWP_UNIT_ID;
                                _data_detail.SWP_RELEASE = data_detail.SWP_RELEASE;
                                _data_detail.SWP_INVOICE = data_detail.SWP_INVOICE;
                                _data_detail.SWP_MEMO = data_detail.SWP_MEMO;
                                _data_detail.SWP_UNIT_INV = data_detail.SWP_UNIT_INV;
                            }
                            else
                            {
                                context.SWAP_PRICE.Add(data_detail);
                            }
                        }
                        context.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SWAP_CRUDE data, List<SWAP_PRICE> data_details, EntityCPAIEngine context)
        {
            try
            {
                var _data = context.SWAP_CRUDE.Where(x => x.SWC_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                           && x.SWC_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWC_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList().FirstOrDefault();
                if (_data != null)
                {
                    _data.SWC_TRIP_NO = data.SWC_TRIP_NO;
                    _data.SWC_ITEM_NO = data.SWC_ITEM_NO;
                    _data.SWC_PO_NO = data.SWC_PO_NO;
                    _data.SWC_VESSEL_ID = data.SWC_VESSEL_ID;
                    _data.SWC_SOLDTO = data.SWC_SOLDTO;
                    _data.SWC_SHIPTO = data.SWC_SHIPTO;
                    _data.SWC_DELIVERY_DATE = data.SWC_DELIVERY_DATE;
                    _data.SWC_PLANT = data.SWC_PLANT;
                    _data.SWC_CRUDE_TYPE_ID = data.SWC_CRUDE_TYPE_ID;
                    _data.SWC_VOLUME_BBL = data.SWC_VOLUME_BBL;
                    _data.SWC_VOLUME_MT = data.SWC_VOLUME_MT;
                    _data.SWC_VOLUME_LITE = data.SWC_VOLUME_LITE;
                    _data.SWC_OUTTURN_BBL = data.SWC_OUTTURN_BBL;
                    _data.SWC_OUTTURN_LITE = data.SWC_OUTTURN_LITE;
                    _data.SWC_OUTTURN_MT = data.SWC_OUTTURN_MT;
                    _data.SWC_PRICE_PER = data.SWC_PRICE_PER;
                    _data.SWC_PRICE = data.SWC_PRICE;
                    _data.SWC_UNIT_ID = data.SWC_UNIT_ID;
                    _data.SWC_EXCHANGE_RATE = data.SWC_EXCHANGE_RATE;
                    _data.SWC_TOTAL = data.SWC_TOTAL;
                    _data.SWC_UNIT_TOTAL = data.SWC_UNIT_TOTAL;
                    _data.SWC_TYPE = data.SWC_TYPE;
                    _data.SWC_LAST_MODIFY_DATE = data.SWC_LAST_MODIFY_DATE;
                    _data.SWC_LAST_MODIFY_BY = data.SWC_LAST_MODIFY_BY;
                    _data.SWC_STATUS = data.SWC_STATUS;
                    _data.SWC_LAST_MODIFY_TIME = data.SWC_LAST_MODIFY_TIME;
                    _data.SWC_RELEASE = data.SWC_RELEASE;
                    _data.SWC_TANK = data.SWC_TANK;
                    _data.SWC_SALEORDER = data.SWC_SALEORDER;
                    _data.SWC_PRICE_RELEASE = data.SWC_PRICE_RELEASE;
                    _data.SWC_REMARK = data.SWC_REMARK;
                    _data.SWC_DISTRI_CHANN = data.SWC_DISTRI_CHANN;
                    _data.SWC_TABLE_NAME = data.SWC_TABLE_NAME;
                    _data.SWC_MEMO = data.SWC_MEMO;
                    _data.SWC_DATE_SAP = data.SWC_DATE_SAP;
                    _data.SWC_FI_DOC = data.SWC_FI_DOC;
                    _data.SWC_DO_NO = data.SWC_DO_NO;
                    _data.SWC_STATUS_TO_SAP = data.SWC_STATUS_TO_SAP;
                    _data.SWC_SEQ = data.SWC_SEQ;
                    _data.SWC_INCOTERM_TYPE = data.SWC_INCOTERM_TYPE;
                    _data.SWC_TEMP = data.SWC_TEMP;
                    _data.SWC_DENSITY = data.SWC_DENSITY;
                    _data.SWC_STORAGE_LOCATION = data.SWC_STORAGE_LOCATION;
                    _data.SWC_SUPPLIER = data.SWC_SUPPLIER;
                    _data.SWC_DISTRI_CHANN_SAP = data.SWC_DISTRI_CHANN_SAP;
                    _data.SWC_VAT_NONVAT = data.SWC_VAT_NONVAT;
                    _data.SWC_UPDATE_DO = data.SWC_UPDATE_DO;
                    _data.SWC_UPDATE_GI = data.SWC_UPDATE_GI;
                    _data.SWC_FI_DOC_REVERSE = data.SWC_FI_DOC_REVERSE;

                    foreach (var data_detail in data_details)
                    {
                        var _data_detail = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data_detail.SWP_TRIP_NO.ToUpper())
                                                       && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data_detail.SWP_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWP_ITEM_NO.ToUpper().Equals(data_detail.SWP_ITEM_NO.ToUpper())
                                                       && x.SWP_NUM.Equals(data_detail.SWP_NUM)).ToList().FirstOrDefault();
                        if (_data_detail != null)
                        {
                            _data_detail.SWP_TRIP_NO = data_detail.SWP_TRIP_NO;
                            _data_detail.SWP_ITEM_NO = data_detail.SWP_ITEM_NO;
                            _data_detail.SWP_CRUDE_TYPE_ID = data_detail.SWP_CRUDE_TYPE_ID;
                            _data_detail.SWP_NUM = data_detail.SWP_NUM;
                            _data_detail.SWP_TYPE = data_detail.SWP_TYPE;
                            _data_detail.SWP_PRICE = data_detail.SWP_PRICE;
                            _data_detail.SWP_DATE_PRICE = data_detail.SWP_DATE_PRICE;
                            _data_detail.SWP_UNIT_ID = data_detail.SWP_UNIT_ID;
                            _data_detail.SWP_RELEASE = data_detail.SWP_RELEASE;
                            _data_detail.SWP_INVOICE = data_detail.SWP_INVOICE;
                            _data_detail.SWP_MEMO = data_detail.SWP_MEMO;
                            _data_detail.SWP_UNIT_INV = data_detail.SWP_UNIT_INV;
                        }
                        else
                        {
                            context.SWAP_PRICE.Add(data_detail);
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        
    }

    public class SWAP_PRICE_DAL
    {
        public SWAP_PRICE Get(SWAP_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWP_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWP_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(data.SWP_ITEM_NO.ToUpper())
                                                           && x.SWP_NUM.Equals(data.SWP_NUM)).ToList().FirstOrDefault();
                    if (_data != null)
                    {
                        return _data;
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public SWAP_PRICE Get(SWAP_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                var _data = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWP_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWP_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(data.SWP_ITEM_NO.ToUpper())
                                                           && x.SWP_NUM.Equals(data.SWP_NUM)).ToList().FirstOrDefault();
                if (_data != null)
                {
                    return _data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public void Save(SWAP_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.SWAP_PRICE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(SWAP_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                context.SWAP_PRICE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SWAP_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWP_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWP_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(data.SWP_ITEM_NO.ToUpper())
                                                           && x.SWP_NUM.Equals(data.SWP_NUM)).ToList().FirstOrDefault();
                    if (_data != null)
                    {
                        _data.SWP_TRIP_NO = data.SWP_TRIP_NO;
                        _data.SWP_ITEM_NO = data.SWP_ITEM_NO;
                        _data.SWP_CRUDE_TYPE_ID = data.SWP_CRUDE_TYPE_ID;
                        _data.SWP_NUM = data.SWP_NUM;
                        _data.SWP_TYPE = data.SWP_TYPE;
                        _data.SWP_PRICE = data.SWP_PRICE;
                        _data.SWP_DATE_PRICE = data.SWP_DATE_PRICE;
                        _data.SWP_UNIT_ID = data.SWP_UNIT_ID;
                        _data.SWP_RELEASE = data.SWP_RELEASE;
                        _data.SWP_INVOICE = data.SWP_INVOICE;
                        _data.SWP_MEMO = data.SWP_MEMO;
                        _data.SWP_UNIT_INV = data.SWP_UNIT_INV;
                        context.SaveChanges();
                    }
                    else
                    {
                        context.SWAP_PRICE.Add(data);
                        context.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SWAP_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                var _data = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWP_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWP_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(data.SWP_ITEM_NO.ToUpper())
                                                           && x.SWP_NUM.Equals(data.SWP_NUM)).ToList().FirstOrDefault();

                if (_data != null)
                {
                    _data.SWP_TRIP_NO = data.SWP_TRIP_NO;
                    _data.SWP_ITEM_NO = data.SWP_ITEM_NO;
                    _data.SWP_CRUDE_TYPE_ID = data.SWP_CRUDE_TYPE_ID;
                    _data.SWP_NUM = data.SWP_NUM;
                    _data.SWP_TYPE = data.SWP_TYPE;
                    _data.SWP_PRICE = data.SWP_PRICE;
                    _data.SWP_DATE_PRICE = data.SWP_DATE_PRICE;
                    _data.SWP_UNIT_ID = data.SWP_UNIT_ID;
                    _data.SWP_RELEASE = data.SWP_RELEASE;
                    _data.SWP_INVOICE = data.SWP_INVOICE;
                    _data.SWP_MEMO = data.SWP_MEMO;
                    _data.SWP_UNIT_INV = data.SWP_UNIT_INV;
                    context.SaveChanges();
                }
                else
                {
                    context.SWAP_PRICE.Add(data);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(SWAP_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _data = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                           && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                           && x.SWP_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList();
                    if (_data.Any())
                    {
                        context.SWAP_PRICE.RemoveRange(_data);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(SWAP_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                var _data = context.SWAP_PRICE.Where(x => x.SWP_TRIP_NO.ToUpper().Equals(data.SWC_TRIP_NO.ToUpper())
                                                       && x.SWP_CRUDE_TYPE_ID.ToUpper().Equals(data.SWC_CRUDE_TYPE_ID.ToUpper())
                                                       && x.SWP_ITEM_NO.ToUpper().Equals(data.SWC_ITEM_NO.ToUpper())).ToList();
                if (_data.Any())
                {
                    context.SWAP_PRICE.RemoveRange(_data);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
