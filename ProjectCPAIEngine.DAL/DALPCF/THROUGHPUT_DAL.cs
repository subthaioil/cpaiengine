﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class THROUGHPUT_DAL
    {
        public void Save(TRP_HEADER data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.TRP_HEADER.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(TRP_HEADER data, EntityCPAIEngine context)
        {
            try
            {
                context.TRP_HEADER.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveDetail(TRP_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.TRP_PRICE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveDetail(TRP_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                context.TRP_PRICE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(TRP_HEADER data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _TRP_HEADER = context.TRP_HEADER.SingleOrDefault(a => a.TRIP_NO == data.TRIP_NO);
                    if (_TRP_HEADER != null)
                    {
                        _TRP_HEADER.TRIP_NO = data.TRIP_NO;
                        _TRP_HEADER.SALES_ORG = data.SALES_ORG;
                        _TRP_HEADER.VESSEL = data.VESSEL;
                        _TRP_HEADER.CUSTOMER = data.CUSTOMER;
                        _TRP_HEADER.SHIP_TO = data.SHIP_TO;
                        _TRP_HEADER.MET_NUM = data.MET_NUM;
                        _TRP_HEADER.COMPLETED_DATE_TIME = data.COMPLETED_DATE_TIME;
                        _TRP_HEADER.DUE_DATE = data.DUE_DATE;
                        _TRP_HEADER.SALES_UNIT = data.SALES_UNIT;
                        _TRP_HEADER.DISTRIBUTION_CHANNEL = data.DISTRIBUTION_CHANNEL;
                        _TRP_HEADER.PLANT = data.PLANT;
                        _TRP_HEADER.SO_NO = data.SO_NO;
                        _TRP_HEADER.SO_CREATED_DATE = data.SO_CREATED_DATE;
                        _TRP_HEADER.SO_CREATED_BY = data.SO_CREATED_BY;
                        _TRP_HEADER.SO_UPDATED_DATE = data.SO_UPDATED_DATE;
                        _TRP_HEADER.SO_UPDATED_BY = data.SO_UPDATED_BY;
                        _TRP_HEADER.CREATED_DATE = data.CREATED_DATE;
                        _TRP_HEADER.CREATED_BY = data.CREATED_BY;
                        _TRP_HEADER.UPDATED_DATE = data.UPDATED_DATE;
                        _TRP_HEADER.UPDATED_BY = data.UPDATED_BY;
                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(TRP_HEADER data, EntityCPAIEngine context)
        {
            try
            {
                var _TRP_HEADER = context.TRP_HEADER.SingleOrDefault(a => a.TRIP_NO == data.TRIP_NO);
                if (_TRP_HEADER != null)
                {
                    _TRP_HEADER.TRIP_NO = data.TRIP_NO;
                    _TRP_HEADER.SALES_ORG = data.SALES_ORG;
                    _TRP_HEADER.VESSEL = data.VESSEL;
                    _TRP_HEADER.CUSTOMER = data.CUSTOMER;
                    _TRP_HEADER.SHIP_TO = data.SHIP_TO;
                    _TRP_HEADER.MET_NUM = data.MET_NUM;
                    _TRP_HEADER.COMPLETED_DATE_TIME = data.COMPLETED_DATE_TIME;
                    _TRP_HEADER.DUE_DATE = data.DUE_DATE;
                    _TRP_HEADER.SALES_UNIT = data.SALES_UNIT;
                    _TRP_HEADER.DISTRIBUTION_CHANNEL = data.DISTRIBUTION_CHANNEL;
                    _TRP_HEADER.PLANT = data.PLANT;
                    _TRP_HEADER.SO_NO = data.SO_NO;
                    _TRP_HEADER.SO_CREATED_DATE = data.SO_CREATED_DATE;
                    _TRP_HEADER.SO_CREATED_BY = data.SO_CREATED_BY;
                    _TRP_HEADER.SO_UPDATED_DATE = data.SO_UPDATED_DATE;
                    _TRP_HEADER.SO_UPDATED_BY = data.SO_UPDATED_BY;
                    _TRP_HEADER.CREATED_DATE = data.CREATED_DATE;
                    _TRP_HEADER.CREATED_BY = data.CREATED_BY;
                    _TRP_HEADER.UPDATED_DATE = data.UPDATED_DATE;
                    _TRP_HEADER.UPDATED_BY = data.UPDATED_BY;
                    context.SaveChanges();
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateDetail(TRP_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _TRP_PRICE = context.TRP_PRICE.SingleOrDefault(a => a.TRIP_NO == data.TRIP_NO);
                    if (_TRP_PRICE != null)
                    {
                        _TRP_PRICE.TRIP_NO = data.TRIP_NO;
                        _TRP_PRICE.ITEM_NO = data.ITEM_NO;
                        _TRP_PRICE.PRICE_STATUS = data.PRICE_STATUS;
                        _TRP_PRICE.QTY_BBL = data.QTY_BBL;
                        _TRP_PRICE.QTY_ML = data.QTY_ML;
                        _TRP_PRICE.QTY_MT = data.QTY_MT;
                        _TRP_PRICE.FIX_RATE = data.FIX_RATE;
                        _TRP_PRICE.TOTAL_AMOUNT_USD = data.TOTAL_AMOUNT_USD;
                        _TRP_PRICE.TOTAL_AMOUNT_THB = data.TOTAL_AMOUNT_THB;
                        _TRP_PRICE.CREATED_DATE = data.CREATED_DATE;
                        _TRP_PRICE.CREATED_BY = data.CREATED_BY;
                        _TRP_PRICE.UPDATED_DATE = data.UPDATED_DATE;
                        _TRP_PRICE.UPDATED_BY = data.UPDATED_BY;
                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateDetail(TRP_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                var _TRP_PRICE = context.TRP_PRICE.SingleOrDefault(a => a.TRIP_NO == data.TRIP_NO);
                if (_TRP_PRICE != null)
                {
                    _TRP_PRICE.TRIP_NO = data.TRIP_NO;
                    _TRP_PRICE.ITEM_NO = data.ITEM_NO;
                    _TRP_PRICE.PRICE_STATUS = data.PRICE_STATUS;
                    _TRP_PRICE.QTY_BBL = data.QTY_BBL;
                    _TRP_PRICE.QTY_ML = data.QTY_ML;
                    _TRP_PRICE.QTY_MT = data.QTY_MT;
                    _TRP_PRICE.FIX_RATE = data.FIX_RATE;
                    _TRP_PRICE.TOTAL_AMOUNT_USD = data.TOTAL_AMOUNT_USD;
                    _TRP_PRICE.TOTAL_AMOUNT_THB = data.TOTAL_AMOUNT_THB;
                    _TRP_PRICE.CREATED_DATE = data.CREATED_DATE;
                    _TRP_PRICE.CREATED_BY = data.CREATED_BY;
                    _TRP_PRICE.UPDATED_DATE = data.UPDATED_DATE;
                    _TRP_PRICE.UPDATED_BY = data.UPDATED_BY;
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
