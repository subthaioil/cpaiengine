﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class THROUGHPUT_FEE_DAL
    {
        public void SaveThroughputFee(THROUGHPUT_FEE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.THROUGHPUT_FEE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveThroughputFee(THROUGHPUT_FEE data, EntityCPAIEngine context)
        {
            try
            {
                context.THROUGHPUT_FEE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateThroughputFee(THROUGHPUT_FEE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _ThroughputFee = context.THROUGHPUT_FEE.SingleOrDefault(a =>a.TPF_TRIP_NO == data.TPF_TRIP_NO);
                    if (_ThroughputFee != null)
                    {
                        //_ThroughputFee.TPF_TRIP_NO = data.TPF_TRIP_NO;
                        _ThroughputFee.TPF_CUST_NUM = data.TPF_CUST_NUM;
                        _ThroughputFee.TPF_DELIVERY_DATE = data.TPF_DELIVERY_DATE;
                        _ThroughputFee.TPF_PLANT = data.TPF_PLANT;
                        _ThroughputFee.TPF_CRUDE_TYPE_ID = data.TPF_CRUDE_TYPE_ID;
                        //_ThroughputFee.TPF_CREATE_DATE = data.TPF_CREATE_DATE;
                        //_ThroughputFee.TPF_CREATE_BY = data.TPF_CREATE_BY;
                        _ThroughputFee.TPF_LAST_MODIFY_DATE = data.TPF_LAST_MODIFY_DATE;
                        _ThroughputFee.TPF_LAST_MODIFY_BY = data.TPF_LAST_MODIFY_BY;
                        _ThroughputFee.TPF_STATUS = data.TPF_STATUS;
                        //_ThroughputFee.TPF_CREATE_TIME = data.TPF_CREATE_TIME;
                        _ThroughputFee.TPF_LAST_MODIFY_TIME = data.TPF_LAST_MODIFY_TIME;
                        _ThroughputFee.TPF_RELEASE = data.TPF_RELEASE;
                        _ThroughputFee.TPF_SALEORDER = data.TPF_SALEORDER;
                        _ThroughputFee.TPF_DISTRI_CHANN = data.TPF_DISTRI_CHANN;
                        _ThroughputFee.TPF_REMARK = data.TPF_REMARK;
                        _ThroughputFee.TPF_DO_NO = data.TPF_DO_NO;
                        _ThroughputFee.TPF_FI_DOC = data.TPF_FI_DOC;
                        _ThroughputFee.TPF_INCOTERM_TYPE = data.TPF_INCOTERM_TYPE;
                        _ThroughputFee.TPF_TEMP = data.TPF_TEMP;
                        _ThroughputFee.TPF_DENSITY = data.TPF_DENSITY;
                        _ThroughputFee.TPF_STORAGE_LOCATION = data.TPF_STORAGE_LOCATION;
                        _ThroughputFee.TPF_DISTRI_CHANN_SAP = data.TPF_DISTRI_CHANN_SAP;
                        _ThroughputFee.TPF_UPDATE_DO = data.TPF_UPDATE_DO;
                        _ThroughputFee.TPF_UPDATE_GI = data.TPF_UPDATE_GI;
                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateThroughputFee(THROUGHPUT_FEE data, EntityCPAIEngine context)
        {
            try
            {
                var _ThroughputFee = context.THROUGHPUT_FEE.Find(data.TPF_TRIP_NO);
                if (_ThroughputFee != null)
                {
                    _ThroughputFee.TPF_TRIP_NO = data.TPF_TRIP_NO;
                    _ThroughputFee.TPF_CUST_NUM = data.TPF_CUST_NUM;
                    _ThroughputFee.TPF_DELIVERY_DATE = data.TPF_DELIVERY_DATE;
                    _ThroughputFee.TPF_PLANT = data.TPF_PLANT;
                    _ThroughputFee.TPF_CRUDE_TYPE_ID = data.TPF_CRUDE_TYPE_ID;
                    _ThroughputFee.TPF_CREATE_DATE = data.TPF_CREATE_DATE;
                    _ThroughputFee.TPF_CREATE_BY = data.TPF_CREATE_BY;
                    _ThroughputFee.TPF_LAST_MODIFY_DATE = data.TPF_LAST_MODIFY_DATE;
                    _ThroughputFee.TPF_LAST_MODIFY_BY = data.TPF_LAST_MODIFY_BY;
                    _ThroughputFee.TPF_STATUS = data.TPF_STATUS;
                    _ThroughputFee.TPF_CREATE_TIME = data.TPF_CREATE_TIME;
                    _ThroughputFee.TPF_LAST_MODIFY_TIME = data.TPF_LAST_MODIFY_TIME;
                    _ThroughputFee.TPF_RELEASE = data.TPF_RELEASE;
                    _ThroughputFee.TPF_SALEORDER = data.TPF_SALEORDER;
                    _ThroughputFee.TPF_DISTRI_CHANN = data.TPF_DISTRI_CHANN;
                    _ThroughputFee.TPF_REMARK = data.TPF_REMARK;
                    _ThroughputFee.TPF_DO_NO = data.TPF_DO_NO;
                    _ThroughputFee.TPF_FI_DOC = data.TPF_FI_DOC;
                    _ThroughputFee.TPF_INCOTERM_TYPE = data.TPF_INCOTERM_TYPE;
                    _ThroughputFee.TPF_TEMP = data.TPF_TEMP;
                    _ThroughputFee.TPF_DENSITY = data.TPF_DENSITY;
                    _ThroughputFee.TPF_STORAGE_LOCATION = data.TPF_STORAGE_LOCATION;
                    _ThroughputFee.TPF_DISTRI_CHANN_SAP = data.TPF_DISTRI_CHANN_SAP;
                    _ThroughputFee.TPF_UPDATE_DO = data.TPF_UPDATE_DO;
                    _ThroughputFee.TPF_UPDATE_GI = data.TPF_UPDATE_GI;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveThroughputDetail(THROUGHPUT_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.THROUGHPUT_DETAIL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveThroughputDetail(THROUGHPUT_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                context.THROUGHPUT_DETAIL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateThroughputDetail(THROUGHPUT_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _ThroughputDetail = context.THROUGHPUT_DETAIL.SingleOrDefault(a =>a.TPD_TRIP_NO == data.TPD_TRIP_NO && a.TPD_SEQ == data.TPD_SEQ);
                    if (_ThroughputDetail != null)
                    {
                        //_ThroughputDetail.TPD_TRIP_NO = data.TPD_TRIP_NO;
                        _ThroughputDetail.TPD_CRUDE_TYPE_ID = data.TPD_CRUDE_TYPE_ID;
                        _ThroughputDetail.TPD_VESSEL_ID = data.TPD_VESSEL_ID;
                        _ThroughputDetail.TPD_OUTTURN_BBL = data.TPD_OUTTURN_BBL;
                        _ThroughputDetail.TPD_PRICE_PER = data.TPD_PRICE_PER;
                        _ThroughputDetail.TPD_PRICE = data.TPD_PRICE;
                        _ThroughputDetail.TPD_UNIT_ID = data.TPD_UNIT_ID;
                        _ThroughputDetail.TPD_EXCHANGE_RATE = data.TPD_EXCHANGE_RATE;
                        _ThroughputDetail.TPD_TOTAL = data.TPD_TOTAL;
                        _ThroughputDetail.TPD_UNIT_TOTAL = data.TPD_UNIT_TOTAL;
                        _ThroughputDetail.TPD_TYPE = data.TPD_TYPE;
                        //_ThroughputDetail.TPD_CREATE_DATE = data.TPD_CREATE_DATE;
                        //_ThroughputDetail.TPD_CREATE_BY = data.TPD_CREATE_BY;
                        _ThroughputDetail.TPD_LAST_MODIFY_DATE = data.TPD_LAST_MODIFY_DATE;
                        _ThroughputDetail.TPD_LAST_MODIFY_BY = data.TPD_LAST_MODIFY_BY;
                        _ThroughputDetail.TPD_STATUS = data.TPD_STATUS;
                        //_ThroughputDetail.TPD_CREATE_TIME = data.TPD_CREATE_TIME;
                        _ThroughputDetail.TPD_LAST_MODIFY_TIME = data.TPD_LAST_MODIFY_TIME;
                        _ThroughputDetail.TPD_RELEASE = data.TPD_RELEASE;
                        //_ThroughputDetail.TPD_SEQ = data.TPD_SEQ;
                        _ThroughputDetail.TPD_SALEORDER = data.TPD_SALEORDER;
                        _ThroughputDetail.TPD_STATUS_TO_SAP = data.TPD_STATUS_TO_SAP;

                        context.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateThroughputDetail(THROUGHPUT_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _ThroughputDetail = context.THROUGHPUT_DETAIL.Find(data.TPD_SEQ);
                if (_ThroughputDetail != null)
                {
                    _ThroughputDetail.TPD_TRIP_NO = data.TPD_TRIP_NO;
                    _ThroughputDetail.TPD_CRUDE_TYPE_ID = data.TPD_CRUDE_TYPE_ID;
                    _ThroughputDetail.TPD_VESSEL_ID = data.TPD_VESSEL_ID;
                    _ThroughputDetail.TPD_OUTTURN_BBL = data.TPD_OUTTURN_BBL;
                    _ThroughputDetail.TPD_PRICE_PER = data.TPD_PRICE_PER;
                    _ThroughputDetail.TPD_PRICE = data.TPD_PRICE;
                    _ThroughputDetail.TPD_UNIT_ID = data.TPD_UNIT_ID;
                    _ThroughputDetail.TPD_EXCHANGE_RATE = data.TPD_EXCHANGE_RATE;
                    _ThroughputDetail.TPD_TOTAL = data.TPD_TOTAL;
                    _ThroughputDetail.TPD_UNIT_TOTAL = data.TPD_UNIT_TOTAL;
                    _ThroughputDetail.TPD_TYPE = data.TPD_TYPE;
                    _ThroughputDetail.TPD_CREATE_DATE = data.TPD_CREATE_DATE;
                    _ThroughputDetail.TPD_CREATE_BY = data.TPD_CREATE_BY;
                    _ThroughputDetail.TPD_LAST_MODIFY_DATE = data.TPD_LAST_MODIFY_DATE;
                    _ThroughputDetail.TPD_LAST_MODIFY_BY = data.TPD_LAST_MODIFY_BY;
                    _ThroughputDetail.TPD_STATUS = data.TPD_STATUS;
                    _ThroughputDetail.TPD_CREATE_TIME = data.TPD_CREATE_TIME;
                    _ThroughputDetail.TPD_LAST_MODIFY_TIME = data.TPD_LAST_MODIFY_TIME;
                    _ThroughputDetail.TPD_RELEASE = data.TPD_RELEASE;
                    _ThroughputDetail.TPD_SEQ = data.TPD_SEQ;
                    _ThroughputDetail.TPD_SALEORDER = data.TPD_SALEORDER;
                    _ThroughputDetail.TPD_STATUS_TO_SAP = data.TPD_STATUS_TO_SAP;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
