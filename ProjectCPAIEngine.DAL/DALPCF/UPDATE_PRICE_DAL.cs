﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;
using System.Data.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class UPDATE_PRICE_DAL
    {
        public void Save(PIT_PO_PRICE data)
        {
            try 
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PIT_PO_PRICE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PIT_PO_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                context.PIT_PO_PRICE.Add(data);
                //PIT_PO_PRICE tempValue = new PIT_PO_PRICE();
                //tempValue.ADJUST_PRICE_USD

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PIT_PO_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.PIT_PO_PRICE.Find(data.PO_NO);
                    #region Set Value
                    //_search. = data.MCO_COMPANY_CODE;
                    //_search.MCO_SHORT_NAME = data.MCO_SHORT_NAME;
                    //_search.MCO_STATUS = data.MCO_STATUS;
                    //_search.MCO_UPDATED_BY = data.MCO_UPDATED_BY;
                    //_search.MCO_UPDATED_DATE = data.MCO_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PIT_PO_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.PIT_PO_PRICE.Find(data.PO_NO);
                #region Set Value
                //_search.MCO_COMPANY_CODE = data.MCO_COMPANY_CODE;
                //_search.MCO_SHORT_NAME = data.MCO_SHORT_NAME;
                //_search.MCO_STATUS = data.MCO_STATUS;
                //_search.MCO_UPDATED_BY = data.MCO_UPDATED_BY;
                //_search.MCO_UPDATED_DATE = data.MCO_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUnitPrice(string po_no, string invoiceStatus, string unitPrice)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand(
                                "UPDATE PIT_PO_PRICE SET UNIT_PRICE = " +unitPrice+ " WHERE PO_NO = '" + po_no + "' AND invoice_status = '" + invoiceStatus + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string po_no, string invoiceStatus)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM PIT_PO_PRICE WHERE PO_NO = '" + po_no + "' AND INVOICE_STATUS = '"+ invoiceStatus + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string po_no, string invoiceStatus, string startDate, string endDate, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM PIT_PO_PRICE WHERE PO_NO = '" + po_no + "' AND INVOICE_STATUS = '" + invoiceStatus + "' AND PO_DATE between to_date('"+ startDate + "','dd/MM/yyyy') and to_date('"+ endDate + "','dd/MM/yyyy')");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
