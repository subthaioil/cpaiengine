﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALTce
{
    public class CPAI_TCE_MK_DAL
    {
        public static CPAI_TCE_MK GetTCE(string transactionID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_MK
                             where v.CTM_ROW_ID == transactionID
                            select v).Distinct();
                return query.ToList().FirstOrDefault();
            }
        }

        public static bool isDuplicate(string vessel, string month, string status)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_MK

                             where v.CTM_FK_MT_VEHICLE == vessel && v.CTM_MONTH == month && v.CTM_STATUS == status

                             select v).Distinct().ToList();
                if (query.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Save(CPAI_TCE_MK data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_TCE_MK.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_TCE_MK data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_TCE_MK.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_TCE_MK data, EntityCPAIEngine context)
        {
            try
            {
                
                    var _search = context.CPAI_TCE_MK.Find(data.CTM_ROW_ID);
                    #region Set Value
                    _search.CTM_STATUS = data.CTM_STATUS;
                    _search.CTM_UPDATED_BY = data.CTM_UPDATED_BY;
                    _search.CTM_UPDATED_DATE = data.CTM_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_TCE_MK WHERE CTM_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_TCE_MK WHERE CTM_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<CPAI_TCE_MK> GetTCELst(List<string> lsttransactionID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_MK
                             where lsttransactionID.Contains( v.CTM_ROW_ID)
                             select v).Distinct();
                return query.ToList();
            }
        }

        public static List<CPAI_TCE_MK_PORT> GetTCEPortLst(List<string> lsttransactionID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_MK_PORT
                             where lsttransactionID.Contains(v.TMP_FK_TCE_MK)
                             select v).Distinct();
                return query.ToList();
            }
        }

        public CPAI_TCE_MK GetByID(string CTM_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CPAI_TCE_MK.Where(x => x.CTM_ROW_ID.ToUpper() == CTM_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Calculate

        public double? CalEverageWS(DateTime TargetDate, string TD, string HolidayType)
        {
       
            int count = 0;
            double? iSum = 0;
            List<MT_CRUDE_ROUTES> list;

            List<DateTime> ListDateTD = new List<DateTime>();

            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                // CalTargetDate MK
                //ListDateTD = CalTargetDate(TargetDate, HolidayType);    // Get date in month with out holiday
                
                // Change to CalTargetDate WS
                ListDateTD = CalTargetDateWS(TargetDate, HolidayType);    // Get date in month with out holiday

                foreach (DateTime dtTD in ListDateTD)
                {

                    list = entity.MT_CRUDE_ROUTES.Where(x => DbFunctions.TruncateTime(dtTD) == DbFunctions.TruncateTime(x.MCR_DATE) && x.MCR_STATUS == "ACTIVE").ToList();

                    if (list.Count == 0)
                    {
                        iSum = null;
                        break;
                    }
                    foreach (var item in list)
                    {
                        if (TD.Equals("TD2"))
                        {
                            if (String.IsNullOrEmpty(item.MCR_TD2) || Convert.ToString(item.MCR_TD2) == "NA")
                            {
                                iSum = null;
                                break;
                            }

                            iSum += Convert.ToDouble(item.MCR_TD2);

                        }

                        else if (TD.Equals("TD3"))
                        {
                            if (String.IsNullOrEmpty(item.MCR_TD3) || Convert.ToString(item.MCR_TD3) == "NA")
                            {
                                iSum = null;
                                break;
                            }

                            iSum += Convert.ToDouble(item.MCR_TD3);

                        }
                        else
                        {
                            if (String.IsNullOrEmpty(item.MCR_TD15) || Convert.ToString(item.MCR_TD15) == "NA")
                            {
                                iSum = null;
                                break;
                            }

                            iSum += Convert.ToDouble(item.MCR_TD15);
                        }

                    }

                    count++;
                }
            }

            if (iSum != null)
                iSum = Math.Round(Convert.ToDouble(iSum) / count, 2);

            return iSum;

        }

        // Calculate Date MK
        public List<DateTime> CalTargetDate(DateTime TargetDate, string HolidayType)
        {

            DateTime DateBack1Month = TargetDate.AddMonths(-1); // Back 1 month

            var firstDayOfMonth = new DateTime(DateBack1Month.Year, DateBack1Month.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            List<DateTime> rtnDate = new List<DateTime>();

            DateTime DateTemp = firstDayOfMonth;
            do
            {
                
                if (isHoliday(DateTemp, HolidayType) == false)
                {
                    rtnDate.Add(DateTemp);
                }

                DateTemp = DateTemp.AddDays(1);

            } while (DateTemp.Date < lastDayOfMonth.Date);


            return rtnDate;
        }

        // Calculate Date WS
        public DateTime CalTargetDateWS(DateTime LaycanLoadFrom)
        {
            DateTime TargetDate = LaycanLoadFrom.AddDays(-20);

            return TargetDate;
        }
        public List<DateTime> CalTargetDateWS(DateTime LaycanLoadFrom, string HolidayType)
        {
            DateTime DateBack20 = CalTargetDateWS(LaycanLoadFrom);
            List<DateTime> rtnDate = new List<DateTime>();

            DateTime DateTemp = new DateTime();


            if (isHoliday(DateBack20, HolidayType) == false)
            {

                DateTemp = DateBack20;

                rtnDate.Add(DateTemp);
                DateTemp = DateTemp.AddDays(-1);

                int intBack1Day = 1;
                do
                {
                    // Date Back 1 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intBack1Day -= 1;
                    }

                    DateTemp = DateTemp.AddDays(-1);

                } while (intBack1Day > 0);

                DateTemp = DateBack20.AddDays(1);
                int intForword1Day = 1;
                do
                {
                    // Date Forword 1 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intForword1Day -= 1;
                    }
                    DateTemp = DateTemp.AddDays(1);

                } while (intForword1Day > 0);

                // Forword 3 Day
                //DateTemp = DateBack20;
                //int intForwoard3Day = 3;
                //do
                //{
                //    // Date Back 3 Day
                //    if (isHoliday(DateTemp, HolidayType) == false)
                //    {
                //       rtnDate.Add(DateTemp);
                //        intForwoard3Day -= 1;
                //    }

                //    DateTemp = DateTemp.AddDays(1);

                //} while (intForwoard3Day > 0);
            }
            else
            {
                DateTemp = DateBack20;
                int intBack2Day = 2;
                do
                {
                    // Date Back 2 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intBack2Day -= 1;

                    }

                    DateTemp = DateTemp.AddDays(-1);

                } while (intBack2Day > 0);

                DateTemp = DateBack20;
                int intForword1Day = 1;
                do
                {
                    // Date Forword 1 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intForword1Day -= 1;
                    }
                    DateTemp = DateTemp.AddDays(1);

                } while (intForword1Day > 0);
            }

            return rtnDate;
        }

        // Check is Holiday
        public bool isHoliday(DateTime checkDate, string HolidayType)
        {


            if (isHolidayDB(checkDate, HolidayType) == true)
            {
                return true;    // is holiday
            }
            else if (checkDate.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;    // is holiday
            }
            else if (checkDate.DayOfWeek == DayOfWeek.Saturday)
            {
                return true;    // is holiday
            }
            else if (isTD_NA(checkDate) == true)
            {
                return true;    // is TD NA
            }
            else
            {
                // Not Holiday
                return false;
            }

        }

        // Check is Holiday in DB
        public bool isHolidayDB(DateTime checkDate, string HolidayType)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                var holiday = entity.MT_HOLIDAY.Where(x => x.MH_HOL_TYPE.ToUpper().Equals(HolidayType.ToUpper()) && DbFunctions.TruncateTime(x.MH_HOL_DATE.Value) == DbFunctions.TruncateTime(checkDate));
                if (holiday.ToList().Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        // Check is Holiday in DB
        public bool isTD_NA(DateTime checkDate)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                var td = entity.MT_CRUDE_ROUTES.Where(x => DbFunctions.TruncateTime(x.MCR_DATE) == DbFunctions.TruncateTime(checkDate) && x.MCR_TD2.Equals("NA") && x.MCR_TD3.Equals("NA") && x.MCR_TD15.Equals("NA") && x.MCR_STATUS.Equals("ACTIVE"));
                if (td.ToList().Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //public double CalEverageWS(DateTime TargetDate, string TD)
        //{
        //    double iSum = 0;
        //    List<MT_CRUDE_ROUTES> list;
        //    int month = TargetDate.Month - 1;
        //    int year = TargetDate.Year;
        //    int count = 0;
        //    using (EntityCPAIEngine entity = new EntityCPAIEngine())
        //    {
        //        list = entity.MT_CRUDE_ROUTES.Where(x => x.MCR_DATE.Month == month && x.MCR_DATE.Year == year).ToList();
        //        foreach (MT_CRUDE_ROUTES crude in list)
        //        {
        //            if (TD.Equals("TD2"))
        //            {
        //                foreach (var item in list)
        //                {
        //                    iSum += Convert.ToDouble(item.MCR_TD2);
        //                }
        //                count = list.Count;
        //            }
        //            else if (TD.Equals("TD3"))
        //            {
        //                foreach (var item in list)
        //                {
        //                    iSum += Convert.ToDouble(item.MCR_TD3);
        //                }
        //                count = list.Count;
        //            }
        //            else
        //            {
        //                foreach (var item in list)
        //                {
        //                    iSum += Convert.ToDouble(item.MCR_TD15);
        //                }
        //                count = list.Count;
        //            }
        //        }
        //    }

        //    iSum = Math.Round(iSum / count, 2);

        //    return iSum;
        //}
        #endregion

    }
}
