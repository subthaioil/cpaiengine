﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALTce
{
    public class CPAI_TCE_MK_PORT_DAL
    {
        public static CPAI_TCE_MK_PORT GetTceMkPort(string transactionID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_MK_PORT
                             where v.TMP_FK_TCE_MK == transactionID
                            select v).Distinct();
                return query.ToList().FirstOrDefault();
            }
        }

        public void Save(CPAI_TCE_MK_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_TCE_MK_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_TCE_MK_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_TCE_MK_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_TCE_MK_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.CPAI_TCE_MK_PORT.Find(data.TMP_ROW_ID);
                    #region Set Value
                    //_search.CTW_STATUS = data.CTW_STATUS;
                    _search.TMP_UPDATED_BY = data.TMP_UPDATED_BY;
                    _search.TMP_UPDATED = data.TMP_UPDATED;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_TCE_MK_PORT WHERE TMP_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_TCE_MK_PORT> GetByID(string CTM_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CPAI_TCE_MK_PORT.Where(x => x.TMP_FK_TCE_MK.ToUpper() == CTM_ROW_ID.ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       

    }
}
