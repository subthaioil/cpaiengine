﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVCOOL
{
    public class VCO_COMMENT_DAL
    {
        public void Save(VCO_COMMENT vcoData, EntityCPAIEngine context)
        {
            try
            {
                context.VCO_COMMENT.Add(vcoData);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM VCO_COMMENT WHERE VCCO_FK_VCO_DATA = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(VCO_COMMENT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.VCO_COMMENT.Find(data.VCCO_ROW_ID);
                #region Set Value
                _search.VCCO_CMCS_REQUEST = data.VCCO_CMCS_REQUEST;
                _search.VCCO_CMCS_PROPOSE_DISCHARGE = data.VCCO_CMCS_PROPOSE_DISCHARGE;
                _search.VCCO_CMCS_PROPOSE_FINAL = data.VCCO_CMCS_PROPOSE_FINAL;
                _search.VCCO_REASON_FOR_PURCHASING = data.VCCO_REASON_FOR_PURCHASING;
                _search.VCCO_LP_RUN_NOTE = data.VCCO_LP_RUN_NOTE;
                _search.VCCO_LP_RUN_SUMMARY = data.VCCO_LP_RUN_SUMMARY;
                _search.VCCO_LP_RUN_SUMMARY_MOBILE = data.VCCO_LP_RUN_SUMMARY_MOBILE;
                _search.VCCO_LP_RUN_ATTACH_FILE = data.VCCO_LP_RUN_ATTACH_FILE;
                _search.VCCO_SCEP = data.VCCO_SCEP;
                _search.VCCO_SCSC = data.VCCO_SCSC;
                _search.VCCO_TN = data.VCCO_TN;
                _search.VCCO_SCVP = data.VCCO_SCVP;
                _search.VCCO_EVPC = data.VCCO_EVPC;
                _search.VCCO_TNVP = data.VCCO_TNVP;
                _search.VCCO_CMVP = data.VCCO_CMVP;
                _search.VCCO_CMCS_NOTE = data.VCCO_CMCS_NOTE;
                _search.VCCO_SCSC_AGREE_FLAG = data.VCCO_SCSC_AGREE_FLAG;
                _search.VCCO_TNVP_FLAG = data.VCCO_TNVP_FLAG;
                _search.VCCO_SCVP_FLAG = data.VCCO_SCVP_FLAG;
                _search.VCCO_CMVP_FLAG = data.VCCO_CMVP_FLAG;
                _search.VCCO_UPDATED_BY = data.VCCO_UPDATED_BY;
                _search.VCCO_UPDATED = data.VCCO_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VCO_COMMENT GetByDataID(string VCDA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.VCO_COMMENT.Where(x => x.VCCO_FK_VCO_DATA.ToUpper() == VCDA_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VCO_COMMENT FirstOrDefault(System.Linq.Expressions.Expression<Func<VCO_COMMENT,bool>> predicate)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.VCO_COMMENT.FirstOrDefault(predicate);
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
