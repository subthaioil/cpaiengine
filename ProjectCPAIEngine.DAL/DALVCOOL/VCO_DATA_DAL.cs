﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVCOOL
{
    public class VCO_DATA_DAL
    {
        public void Save(VCO_DATA vcoData, EntityCPAIEngine context)
        {
            try
            {
                context.VCO_DATA.Add(vcoData);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM VCO_DATA WHERE VCDA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(VCO_DATA data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.VCO_DATA.Find(data.VCDA_ROW_ID);
                #region Set Value
                _search.VCDA_PREMIUM = data.VCDA_PREMIUM;
                _search.VCDA_DISCHARGING_DATE_FROM = data.VCDA_DISCHARGING_DATE_FROM;
                _search.VCDA_DISCHARGING_DATE_TO = data.VCDA_DISCHARGING_DATE_TO;
                _search.VCDA_PROCESSING_DATE_FROM = data.VCDA_PROCESSING_DATE_FROM;
                _search.VCDA_PROCESSING_DATE_TO = data.VCDA_PROCESSING_DATE_TO;
                _search.VCDA_BENCHMARK_PRICE = data.VCDA_BENCHMARK_PRICE;
                _search.VCDA_PURCHASE_TYPE = data.VCDA_PURCHASE_TYPE;
                _search.VCDA_SUPPLIER = data.VCDA_SUPPLIER;
                _search.VCDA_FK_SUPPLIER = data.VCDA_FK_SUPPLIER;
                _search.VCDA_INCOTERM = data.VCDA_INCOTERM;
                _search.VCDA_STATUS = data.VCDA_STATUS;
                _search.VCDA_UPDATED_BY = data.VCDA_UPDATED_BY;
                _search.VCDA_UPDATED = data.VCDA_UPDATED;
                _search.VCDA_PREMIUM_MAXIMUM = data.VCDA_PREMIUM_MAXIMUM;
                _search.VCDA_PURCHASE_RESULT = data.VCDA_PURCHASE_RESULT;
                _search.VCDA_PURCHASE_DATE = data.VCDA_PURCHASE_DATE; 
                _search.VCDA_COMPARISON_JSON = data.VCDA_COMPARISON_JSON;
                _search.VCDA_FINAL_PREMIUM = data.VCDA_FINAL_PREMIUM;
                _search.VCDA_FINAL_BENCHMARK_PRICE = data.VCDA_FINAL_BENCHMARK_PRICE;
                _search.VCDA_FINAL_INCOTERM = data.VCDA_FINAL_INCOTERM;
                _search.VCDA_SCSC_SH_NOTE = data.VCDA_SCSC_SH_NOTE;
                _search.VCDA_TNVP_NOTE = data.VCDA_TNVP_NOTE;
                _search.VCDA_SCVP_NOTE = data.VCDA_SCVP_NOTE;
                _search.VCDA_REASON_SCEP_SH = data.VCDA_REASON_SCEP_SH;
                _search.VCDA_REASON_CMCS_SH = data.VCDA_REASON_CMCS_SH;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSCStatus(VCO_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.VCO_DATA.Find(data.VCDA_ROW_ID);
                #region Set Value
                _search.VCDA_SC_STATUS = data.VCDA_SC_STATUS;
                _search.VCDA_UPDATED_BY = data.VCDA_UPDATED_BY;
                _search.VCDA_UPDATED = data.VCDA_UPDATED;
                if (data.VCDA_DISCHARGING_DATE_FROM != null && data.VCDA_DISCHARGING_DATE_TO != null)
                {
                    _search.VCDA_DISCHARGING_DATE_FROM = data.VCDA_DISCHARGING_DATE_FROM;
                    _search.VCDA_DISCHARGING_DATE_TO = data.VCDA_DISCHARGING_DATE_TO;
                }

                _search.VCDA_SCSC_REVISED_DATE_FROM = data.VCDA_SCSC_REVISED_DATE_FROM;
                _search.VCDA_SCSC_REVISED_DATE_TO = data.VCDA_SCSC_REVISED_DATE_TO;
                _search.VCDA_SCSC_SH_NOTE = data.VCDA_SCSC_SH_NOTE;
                _search.VCDA_SCVP_NOTE = data.VCDA_SCVP_NOTE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTNStatus(VCO_DATA data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.VCO_DATA.Find(data.VCDA_ROW_ID);
                #region Set Value
                _search.VCDA_TN_STATUS = data.VCDA_TN_STATUS;
                _search.VCDA_UPDATED_BY = data.VCDA_UPDATED_BY;
                _search.VCDA_UPDATED = data.VCDA_UPDATED; 
                _search.VCDA_TNVP_NOTE = data.VCDA_TNVP_NOTE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VCO_DATA GetByID(string VCDA_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.VCO_DATA.Where(x => x.VCDA_ROW_ID.ToUpper() == VCDA_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VCO_DATA GetFormerByCrudeAndCountry(string crude, string country, DateTime created_date)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.VCO_DATA.Where(x => x.VCDA_CRUDE_NAME.ToUpper() == crude && x.VCDA_ORIGIN.ToUpper() == country && x.VCDA_CREATED < created_date).OrderByDescending(x => x.VCDA_CREATED).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<VCO_DATA> SearchVCODataExpression(System.Linq.Expressions.Expression<Func<VCO_DATA, bool>> predicate)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    if (predicate == null)
                    {
                        return context.VCO_DATA.ToList();
                    }
                    else
                    {
                        var result = context.VCO_DATA.Where(predicate).ToList();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static List<VCO_DATA> GetAllCrude()
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.VCO_DATA.OrderBy(x => x.VCDA_CRUDE_NAME).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public VCO_DATA GetByCrudePurchaseID(string id)
        {
            using (var context = new EntityCPAIEngine())
            {

                VCO_DATA data = context.VCO_DATA.Where(x => x.VCDA_FK_CDP_DATA == id).FirstOrDefault();
                return data;
            }
        }

        public void RemoveCrudePurchaseFK(string id, EntityCPAIEngine context)
        {
            try
            {
                context.VCO_DATA.Where(x => x.VCDA_FK_CDP_DATA == id).ToList().ForEach(x => x.VCDA_FK_CDP_DATA = "");
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
