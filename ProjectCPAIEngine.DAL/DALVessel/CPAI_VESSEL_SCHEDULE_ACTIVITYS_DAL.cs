﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_ACTIVITYS_DAL
    {
        public void Save(CPAI_VESSEL_SCHEDULE_ACTIVITYS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCHEDULE_ACTIVITYS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCHEDULE_ACTIVITYS data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE_ACTIVITYS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_VESSEL_SCHEDULE_ACTIVITYS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.CPAI_VESSEL_SCHEDULE_ACTIVITYS.Find(data.VAS_ROW_ID);
                    #region Set Value
                    _search.VSA_UPDATED = data.VSA_UPDATED;
                    _search.VSA_UPDATED_BY = data.VSA_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCHEDULE_ACTIVITYS WHERE VAS_FK_VESSEL_SCHEDULE = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCHEDULE_ACTIVITYS WHERE VAS_FK_VESSEL_SCHEDULE = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCHEDULE_ACTIVITYS> GetSchedulerActivityByDataID(string VAS_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _schs = context.CPAI_VESSEL_SCHEDULE_ACTIVITYS.Where(x => x.VAS_FK_VESSEL_SCHEDULE.ToUpper() == VAS_ROW_ID.ToUpper()).ToList();
                    //var _schs = (from a in context.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                    //             join b in context.MT_VESSEL_ACTIVITY on a.VAS_FK_VESSEL_ACTIVITY equals b.MVA_ROW_ID
                    //             join c in context.MT_VESSEL_ACTIVITY_CONTROL on a.VAS_FK_VESSEL_ACTIVITY equals c.MAC_FK_VESSEL_ACTIVITY
                    //             where a.VAS_FK_VESSEL_SCHEDULE == VAS_ROW_ID.ToUpper()
                    //             select new
                    //             {
                    //                 VAS_ROW_ID = a.VAS_ROW_ID,
                    //                 VAS_FK_VESSEL_ACTIVITY = a.VAS_FK_VESSEL_ACTIVITY,
                    //                 VAS_ORDER = a.VAS_ORDER,
                    //                 VAS_START_DATE = a.VAS_START_DATE,
                    //                 VAS_END_DATE = a.VAS_END_DATE,
                    //                 VAS_ACTIVITY_OTHER = a.VAS_ACTIVITY_OTHER,
                    //                 VAS_NOTE = a.VAS_NOTE,
                    //                 MAC_COLOR_CODE = c.MAC_COLOR_CODE,
                    //             }).ToList();

                    return _schs;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
