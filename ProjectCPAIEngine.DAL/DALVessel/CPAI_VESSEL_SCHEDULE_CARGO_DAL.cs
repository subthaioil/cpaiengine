﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_CARGO_DAL
    {
        public void Save(CPAI_VESSEL_SCHEDULE_CARGO data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE_CARGO.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCHEDULE_CARGO> GetCargoByDataID(string SCHID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _schCargo = context.CPAI_VESSEL_SCHEDULE_CARGO.Where(x => x.VSCG_FK_VESSEL_SCHEDULE.ToUpper() == SCHID.ToUpper()).OrderBy(o => o.VSCG_ORDER).ToList();
                    return _schCargo;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
