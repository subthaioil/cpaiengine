﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_DAL
    {
        public void Save(CPAI_VESSEL_SCHEDULE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCHEDULE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCHEDULE data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_VESSEL_SCHEDULE data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.CPAI_VESSEL_SCHEDULE.Find(data.VSD_ROW_ID);
                #region Set Value
                _search.VSD_STATUS = data.VSD_STATUS;
                _search.VSD_UPDATED_BY = data.VSD_UPDATED_BY;
                _search.VSD_UPDATED_DATE = data.VSD_UPDATED_DATE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCHEDULE WHERE VSD_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCHEDULE WHERE VSD_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CPAI_VESSEL_SCHEDULE GetByID(string VSD_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CPAI_VESSEL_SCHEDULE.Where(x => x.VSD_ROW_ID.ToUpper() == VSD_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //status = "SUBMIT" , date format = "dd/MM/yyyy HH:mm:ss"
        public bool checkValidSchedule(string txn_id, string vessel, string status, string fromDate, string toDate)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                DateTime fDate = parseInputDate(fromDate);
                DateTime tDate = parseInputDate(toDate);

                /*List<CPAI_VESSEL_SCHEDULE> results = entity.CPAI_VESSEL_SCHEDULE
                                                    .Where(x => x.VSD_FK_VEHICLE.ToUpper() == vessel.ToUpper()
                                                           && ((fDate >= x.VSD_DATE_START && fDate <= x.VSD_DATE_END )
                                                                || (tDate >= x.VSD_DATE_START && tDate <=  x.VSD_DATE_END))
                                                            && x.VSD_STATUS.ToUpper() == status.ToUpper()).ToList();*/
                /*
                --
                select * from CPAI_VESSEL_SCHEDULE
                where 
                VSD_FK_CUST = '1000000021'
                and VSD_STATUS = 'SUBMIT'
                and 
                (
                    (VSD_DATE_START >= TO_DATE('2017-04-01 00:00:00','YYYY-MM-DD HH24:MI:SS') and  VSD_DATE_START <= TO_DATE('2017-04-24 00:00:00','YYYY-MM-DD HH24:MI:SS'))
                    or 
                    (VSD_DATE_END >= TO_DATE('2017-04-01 00:00:00','YYYY-MM-DD HH24:MI:SS') and  VSD_DATE_END <= TO_DATE('2017-04-24 00:00:00','YYYY-MM-DD HH24:MI:SS'))
                )

                if not found do this 

                select * from CPAI_VESSEL_SCHEDULE
                where 
                VSD_FK_CUST = '1000000021'
                and VSD_STATUS = 'SUBMIT'
                and 
                (
                    (TO_DATE('2017-04-01 00:00:00','YYYY-MM-DD HH24:MI:SS') >= VSD_DATE_START and TO_DATE('2017-04-24 00:00:00','YYYY-MM-DD HH24:MI:SS') <= VSD_DATE_END)
                      or
                    (TO_DATE('2017-04-01 00:00:00','YYYY-MM-DD HH24:MI:SS') <= VSD_DATE_START and TO_DATE('2017-04-24 00:00:00','YYYY-MM-DD HH24:MI:SS') >= VSD_DATE_END)
                )
                --
                */
                List<CPAI_VESSEL_SCHEDULE> results = entity.CPAI_VESSEL_SCHEDULE
                                    .Where(x => x.VSD_FK_VEHICLE.ToUpper() == vessel.ToUpper()
                                                     && x.VSD_ROW_ID != txn_id
                                                     && x.VSD_STATUS.ToUpper() == status.ToUpper()
                                                     && ((DbFunctions.TruncateTime(x.VSD_DATE_START) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(x.VSD_DATE_START) <= DbFunctions.TruncateTime(tDate))
                                                          || (DbFunctions.TruncateTime(x.VSD_DATE_END) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(x.VSD_DATE_END) <= DbFunctions.TruncateTime(tDate)))
                                                     ).ToList();
                if (results != null && results.Count() > 0)
                {
                    return false;
                }
                else
                {
                    List<CPAI_VESSEL_SCHEDULE> results1 = entity.CPAI_VESSEL_SCHEDULE
                                    .Where(x => x.VSD_FK_VEHICLE.ToUpper() == vessel.ToUpper()
                                                     && x.VSD_ROW_ID != txn_id
                                                     && x.VSD_STATUS.ToUpper() == status.ToUpper()
                                                     && ((DbFunctions.TruncateTime(fDate) >= DbFunctions.TruncateTime(x.VSD_DATE_START) && DbFunctions.TruncateTime(tDate) <= DbFunctions.TruncateTime(x.VSD_DATE_END))
                                                          || (DbFunctions.TruncateTime(fDate) <= DbFunctions.TruncateTime(x.VSD_DATE_START) && DbFunctions.TruncateTime(tDate) >= DbFunctions.TruncateTime(x.VSD_DATE_END)))
                                                     ).ToList();
                    if (results1 != null && results1.Count() > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        public bool checkValidActivity(string txn_id)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                /*select* from CPAI_VESSEL_SCHEDULE
                where  VSD_ROW_ID = '1'
                       and TRUNC(VSD_DATE_START) <=
                       (select TRUNC(VAS_DATE) from CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                where VAS_ORDER =
                                                (select min(VAS_ORDER) from CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                                where VAS_FK_VESSEL_SCHEDULE = '1')
                                and VAS_FK_VESSEL_SCHEDULE = '1')
                       and TRUNC(VSD_DATE_END) >=
                        (select TRUNC(VAS_DATE) from CPAI_VESSEL_SCHEDULE_ACTIVITYS
                               where VAS_ORDER =
                                                        (select max(VAS_ORDER) from CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                                        where VAS_FK_VESSEL_SCHEDULE = '1')
                              and VAS_FK_VESSEL_SCHEDULE = '1')*/

                var act = from a in entity.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                          where a.VAS_FK_VESSEL_SCHEDULE == txn_id
                          select new { a };
                if (act != null && act.Count() > 0)
                {

                    var results = from e in entity.CPAI_VESSEL_SCHEDULE
                                  where e.VSD_ROW_ID == txn_id
                                  && DbFunctions.TruncateTime(e.VSD_DATE_START) <= (from x in entity.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                                                                    where x.VAS_ORDER == (from y in entity.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                                                                                          where y.VAS_FK_VESSEL_SCHEDULE == txn_id
                                                                                                          select y.VAS_ORDER).Min()
                                                                                    && x.VAS_FK_VESSEL_SCHEDULE == txn_id
                                                                                    select DbFunctions.TruncateTime(x.VAS_END_DATE)).FirstOrDefault()
                                  && DbFunctions.TruncateTime(e.VSD_DATE_END) >= (from v in entity.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                                                                  where v.VAS_ORDER == (from n in entity.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                                                                                        where n.VAS_FK_VESSEL_SCHEDULE == txn_id
                                                                                                        select n.VAS_ORDER).Max()
                                                                                  && v.VAS_FK_VESSEL_SCHEDULE == txn_id
                                                                                  select DbFunctions.TruncateTime(v.VAS_END_DATE)).FirstOrDefault()
                                  select new { e };
                    if (results != null && results.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public DateTime parsePurchaseDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //dd/MM/yyyy HH:mm:ss
        public DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<CPAI_VESSEL_SCHEDULE> GetDataForJsonExcel(DateTime? dateFrom, DateTime? dateTo) // 01/09/2017 - 30/09/2017
        {            
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    //var _Data = context.CPAI_VESSEL_SCHEDULE.Where((x => DbFunctions.TruncateTime(x.VSD_DATE_START) >= DbFunctions.TruncateTime(monthYearFrom) && DbFunctions.TruncateTime(x.VSD_DATE_END) <= DbFunctions.TruncateTime(monthYearTo))).ToList();

                    var _Data = context.CPAI_VESSEL_SCHEDULE.Where(x => x.VSD_STATUS == "SUBMIT"
                                                               && ((DbFunctions.TruncateTime(x.VSD_DATE_START) >= DbFunctions.TruncateTime(dateFrom)) && (DbFunctions.TruncateTime(x.VSD_DATE_END) <= DbFunctions.TruncateTime(dateTo))
                                                               || ((DbFunctions.TruncateTime(x.VSD_DATE_START) <= DbFunctions.TruncateTime(dateFrom)) && (DbFunctions.TruncateTime(x.VSD_DATE_END) >= DbFunctions.TruncateTime(dateTo))
                                                               || ((DbFunctions.TruncateTime(x.VSD_DATE_START) <= DbFunctions.TruncateTime(dateFrom)) && ((DbFunctions.TruncateTime(x.VSD_DATE_END) >= DbFunctions.TruncateTime(dateFrom) && (DbFunctions.TruncateTime(x.VSD_DATE_END) <= DbFunctions.TruncateTime(dateTo))))
                                                               || (((DbFunctions.TruncateTime(x.VSD_DATE_START) >= DbFunctions.TruncateTime(dateFrom)) && (DbFunctions.TruncateTime(x.VSD_DATE_START) <= DbFunctions.TruncateTime(dateTo)))) //***
                                                               )
                                                               )
                                                               )
                                                               ).ToList();

                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}