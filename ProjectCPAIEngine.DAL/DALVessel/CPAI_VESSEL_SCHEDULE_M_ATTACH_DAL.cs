﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL
    {
        public List<CPAI_VESSEL_SCHEDULE_M_ATTACH> GetFileByDataID(string ACTID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _actFile = context.CPAI_VESSEL_SCHEDULE_M_ATTACH.Where(x => x.VSMF_FK_VESSEL_SCHEDULE.ToUpper() == ACTID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _actFile = _actFile.Where(x => x.VSMF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _actFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCHEDULE_M_ATTACH data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE_M_ATTACH.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCHEDULE_M_ATTACH> GetFileByDataID(string CHOTID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CPAI_VESSEL_SCHEDULE_M_ATTACH.Where(x => x.VSMF_FK_VESSEL_SCHEDULE.ToUpper() == CHOTID.ToUpper()).ToList();
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCHEDULE_M_ATTACH> GetFileByDataTypeID(string CHOTID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choFile = context.CPAI_VESSEL_SCHEDULE_M_ATTACH.Where(x => x.VSMF_FK_VESSEL_SCHEDULE.ToUpper() == CHOTID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _choFile = _choFile.Where(x => x.VSMF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _choFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
