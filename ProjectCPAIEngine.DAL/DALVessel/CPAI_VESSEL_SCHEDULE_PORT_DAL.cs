﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_PORT_DAL
    {
        public void Save(CPAI_VESSEL_SCHEDULE_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCHEDULE_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCHEDULE_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCHEDULE_PORT> GetPortByDataID(string SCHID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _schPort = context.CPAI_VESSEL_SCHEDULE_PORT.Where(x => x.VSP_FK_VESSEL_SCHEDULE.ToUpper() == SCHID.ToUpper()).OrderBy(o => o.VSP_ORDER_PORT).ToList();
                    return _schPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
