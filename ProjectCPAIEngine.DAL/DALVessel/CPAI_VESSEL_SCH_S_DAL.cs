﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCH_S_DAL
    {
        public void Save(CPAI_VESSEL_SCH_S data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCH_S.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCH_S data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCH_S.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_VESSEL_SCH_S data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.CPAI_VESSEL_SCH_S.Find(data.VSDS_ROW_ID);
                #region Set Value
                _search.VSDS_STATUS = data.VSDS_STATUS;
                _search.VSDS_UPDATED_BY = data.VSDS_UPDATED_BY;
                _search.VSDS_UPDATED_DATE = data.VSDS_UPDATED_DATE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCH_S WHERE VSDS_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCH_S WHERE VSDS_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //status = "SUBMIT" , date format = "dd/MM/yyyy HH:mm:ss"
        public bool checkValidSchedule(string txn_id, string vessel, string status, string fromDate, string toDate)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                DateTime fDate = parseInputDate(fromDate, "dd/MM/yyyy HH:mm");
                DateTime tDate = parseInputDate(toDate, "dd/MM/yyyy HH:mm");

                List<CPAI_VESSEL_SCH_S> results = entity.CPAI_VESSEL_SCH_S
                                    .Where(x => x.VSDS_FK_VEHICLE.ToUpper() == vessel.ToUpper()
                                                     && x.VSDS_ROW_ID != txn_id
                                                     && x.VSDS_STATUS.ToUpper() == status.ToUpper()
                                                     && ((DbFunctions.TruncateTime(x.VSDS_DATE_START) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(x.VSDS_DATE_START) <= DbFunctions.TruncateTime(tDate))
                                                          || (DbFunctions.TruncateTime(x.VSDS_DATE_END) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(x.VSDS_DATE_END) <= DbFunctions.TruncateTime(tDate)))
                                                     ).ToList();
                if (results != null && results.Count() > 0)
                {
                    return false;
                }
                else
                {
                    List<CPAI_VESSEL_SCH_S> results1 = entity.CPAI_VESSEL_SCH_S
                                    .Where(x => x.VSDS_FK_VEHICLE.ToUpper() == vessel.ToUpper()
                                                     && x.VSDS_ROW_ID != txn_id
                                                     && x.VSDS_STATUS.ToUpper() == status.ToUpper()
                                                     && ((DbFunctions.TruncateTime(fDate) >= DbFunctions.TruncateTime(x.VSDS_DATE_START) && DbFunctions.TruncateTime(tDate) <= DbFunctions.TruncateTime(x.VSDS_DATE_END))
                                                          || (DbFunctions.TruncateTime(fDate) <= DbFunctions.TruncateTime(x.VSDS_DATE_START) && DbFunctions.TruncateTime(tDate) >= DbFunctions.TruncateTime(x.VSDS_DATE_END)))
                                                     ).ToList();
                    if (results1 != null && results1.Count() > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        public bool checkValidActivity(string txn_id)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
               
                var act = from a in entity.CPAI_VESSEL_SCH_S_ACTIVITYS
                          where a.VASS_FK_VESSEL_SCH_S == txn_id
                          select new { a };
                if (act != null && act.Count() > 0)
                {

                    var results = from e in entity.CPAI_VESSEL_SCH_S
                                  where e.VSDS_ROW_ID == txn_id
                                  //&& DbFunctions.TruncateTime(e.VSDS_DATE_START) <= (from x in entity.CPAI_VESSEL_SCH_S_ACTIVITYS
                                  //                                                  where x.VASS_ORDER == (from y in entity.CPAI_VESSEL_SCH_S_ACTIVITYS
                                  //                                                                        where y.VASS_FK_VESSEL_SCH_S == txn_id
                                  //                                                                        select y.VASS_ORDER).Min()
                                  //                                                  && x.VASS_FK_VESSEL_SCH_S == txn_id
                                  //                                                  select DbFunctions.TruncateTime(x.VASS_DATE)).FirstOrDefault()
                                  //&& DbFunctions.TruncateTime(e.VSDS_DATE_END) >= (from v in entity.CPAI_VESSEL_SCH_S_ACTIVITYS
                                  //                                                 where v.VASS_ORDER == (from n in entity.CPAI_VESSEL_SCH_S_ACTIVITYS
                                  //                                                                       where n.VASS_FK_VESSEL_SCH_S == txn_id
                                  //                                                                      select n.VASS_ORDER).Max()
                                  //                                                && v.VASS_FK_VESSEL_SCH_S == txn_id
                                  //                                                select DbFunctions.TruncateTime(v.VASS_DATE)).FirstOrDefault()
                                  select new { e };
                    if (results != null && results.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
        }

        public DateTime parsePurchaseDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //dd/MM/yyyy HH:mm:ss
        public DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime parseInputDate(string date, string format)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, format, CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CPAI_VESSEL_SCH_S GetSchedulerByDataID(string SCOS_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _schs = context.CPAI_VESSEL_SCH_S.Where(x => x.VSDS_ROW_ID.ToUpper() == SCOS_ID.ToUpper()).FirstOrDefault();
                    return _schs;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
