﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCH_S_PORT_DAL
    {
        public void Save(CPAI_VESSEL_SCH_S_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCH_S_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCH_S_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCH_S_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCH_S_PORT> GetPortByDataID(string SCHSID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choPort = context.CPAI_VESSEL_SCH_S_PORT.Where(x => x.VSPS_FK_VESSEL_SCH_S.ToUpper() == SCHSID.ToUpper()).OrderBy(o => o.VSPS_ORDER_PORT).ToList();
                    return _choPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
