//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CDP_ROUND_COMPET_ITEMS
    {
        public CDP_ROUND_COMPET_ITEMS()
        {
            this.CDP_ROUND_COMPET = new HashSet<CDP_ROUND_COMPET>();
        }
    
        public string CIC_ROW_ID { get; set; }
        public string CIC_FK_OFFER_COMPET_ITEMS { get; set; }
        public string CIC_ROUND_NO { get; set; }
        public System.DateTime CIC_CREATED { get; set; }
        public string CIC_CREATED_BY { get; set; }
        public System.DateTime CIC_UPDATED { get; set; }
        public string CIC_UPDATED_BY { get; set; }
    
        public virtual ICollection<CDP_ROUND_COMPET> CDP_ROUND_COMPET { get; set; }
        public virtual CDP_OFFER_COMPET_ITEMS CDP_OFFER_COMPET_ITEMS { get; set; }
    }
}
