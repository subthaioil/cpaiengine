//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHI_DISCHARGE_PORT
    {
        public string IDP_ROW_ID { get; set; }
        public string IDP_FK_CHI_DATA { get; set; }
        public string IDP_ORDER_PORT { get; set; }
        public string IDP_PORT { get; set; }
        public System.DateTime IDP_CREATED { get; set; }
        public string IDP_CREATED_BY { get; set; }
        public System.DateTime IDP_UPDATED { get; set; }
        public string IDP_UPDATED_BY { get; set; }
    
        public virtual CHI_DATA CHI_DATA { get; set; }
    }
}
