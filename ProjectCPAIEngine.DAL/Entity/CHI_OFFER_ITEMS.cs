//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHI_OFFER_ITEMS
    {
        public CHI_OFFER_ITEMS()
        {
            this.CHI_ROUND_ITEMS = new HashSet<CHI_ROUND_ITEMS>();
        }
    
        public string IOI_ROW_ID { get; set; }
        public string IOI_FK_CHI_DATA { get; set; }
        public string IOI_FK_VENDOR { get; set; }
        public string IOI_FK_VEHICLE { get; set; }
        public string IOI_YEAR { get; set; }
        public string IOI_KDWT { get; set; }
        public string IOI_OWNER { get; set; }
        public string IOI_FINAL_FLAG { get; set; }
        public string IOI_FINAL_DEAL { get; set; }
        public System.DateTime IOI_CREATED { get; set; }
        public string IOI_CREATED_BY { get; set; }
        public System.DateTime IOI_UPDATED { get; set; }
        public string IOI_UPDATED_BY { get; set; }
        public string IOI_TYPE { get; set; }
    
        public virtual ICollection<CHI_ROUND_ITEMS> CHI_ROUND_ITEMS { get; set; }
        public virtual MT_VEHICLE MT_VEHICLE { get; set; }
        public virtual MT_VENDOR MT_VENDOR { get; set; }
        public virtual CHI_DATA CHI_DATA { get; set; }
    }
}
