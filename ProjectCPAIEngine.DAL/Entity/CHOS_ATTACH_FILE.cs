//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHOS_ATTACH_FILE
    {
        public string OSAF_ROW_ID { get; set; }
        public string OSAF_FK_CHOS_DATA { get; set; }
        public string OSAF_TYPE { get; set; }
        public string OSAF_PATH { get; set; }
        public string OSAF_INFO { get; set; }
        public System.DateTime OSAF_CREATED { get; set; }
        public string OSAF_CREATED_BY { get; set; }
        public System.DateTime OSAF_UPDATED { get; set; }
        public string OSAF_UPDATED_BY { get; set; }
    
        public virtual CHOS_DATA CHOS_DATA { get; set; }
    }
}
