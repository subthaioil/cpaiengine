//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHOT_OFFER_ITEMS
    {
        public CHOT_OFFER_ITEMS()
        {
            this.CHOT_ROUND_ITEMS = new HashSet<CHOT_ROUND_ITEMS>();
        }
    
        public string OTOI_ROW_ID { get; set; }
        public string OTOI_FK_CHOT_DATA { get; set; }
        public string OTOI_FK_VENDOR_BROKER { get; set; }
        public string OTOI_FK_CUST_CHARTERER { get; set; }
        public string OTOI_FINAL_FLAG { get; set; }
        public string OTOI_FINAL_DEAL { get; set; }
        public System.DateTime OTOI_CREATED { get; set; }
        public string OTOI_CREATED_BY { get; set; }
        public System.DateTime OTOI_UPDATED { get; set; }
        public string OTOI_UPDATED_BY { get; set; }
    
        public virtual ICollection<CHOT_ROUND_ITEMS> CHOT_ROUND_ITEMS { get; set; }
        public virtual MT_CUST_DETAIL MT_CUST_DETAIL { get; set; }
        public virtual MT_VENDOR MT_VENDOR { get; set; }
        public virtual CHOT_DATA CHOT_DATA { get; set; }
    }
}
