//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CPAI_TCE_MK
    {
        public CPAI_TCE_MK()
        {
            this.CPAI_TCE_MK_PORT = new HashSet<CPAI_TCE_MK_PORT>();
        }
    
        public string CTM_ROW_ID { get; set; }
        public Nullable<System.DateTime> CTM_DATE_FROM { get; set; }
        public Nullable<System.DateTime> CTM_DATE_TO { get; set; }
        public string CTM_TD { get; set; }
        public string CTM_FK_MT_VEHICLE { get; set; }
        public string CTM_TASK_NO { get; set; }
        public string CTM_MONTH { get; set; }
        public string CTM_TC_RATE { get; set; }
        public string CTM_OPERATING_DAYS { get; set; }
        public string CTM_TIME_CHARTER_COST { get; set; }
        public string CTM_TOTAL_PORT_COST { get; set; }
        public string CTM_BUNKER_FO { get; set; }
        public string CTM_BUNKER_GO { get; set; }
        public string CTM_TOTAL_BUNKER_COST { get; set; }
        public string CTM_OTHER_COST { get; set; }
        public string CTM_TOTAL_COST { get; set; }
        public string CTM_FLAT_RATE { get; set; }
        public string CTM_WS_BITR_TD { get; set; }
        public string CTM_MINLOAD { get; set; }
        public string CTM_THEORITICAL_DAYS { get; set; }
        public string CTM_TOTAL_MARKET_COST { get; set; }
        public string CTM_SAVING_LOSS_MARKET { get; set; }
        public string CTM_SPOT_VARIABLE_COST { get; set; }
        public string CTM_TCE_BENEFIT_DAY { get; set; }
        public string CTM_TCE_BENEFIT_VOY { get; set; }
        public string CTM_TCE_DAY { get; set; }
        public Nullable<System.DateTime> CTM_CREATED_DATE { get; set; }
        public string CTM_CREATED_BY { get; set; }
        public Nullable<System.DateTime> CTM_UPDATED_DATE { get; set; }
        public string CTM_UPDATED_BY { get; set; }
        public string CTM_FO_MT { get; set; }
        public string CTM_GO_MT { get; set; }
        public string CTM_DEMURRAGE { get; set; }
        public string CTM_STATUS { get; set; }
        public string CTM_TC_NUM { get; set; }
        public string CTM_FO_UNIT_PRICE { get; set; }
        public string CTM_GO_UNIT_PRICE { get; set; }
        public Nullable<System.DateTime> CTM_LOADING_DATE_FROM { get; set; }
        public Nullable<System.DateTime> CTM_LOADING_DATE_TO { get; set; }
    
        public virtual ICollection<CPAI_TCE_MK_PORT> CPAI_TCE_MK_PORT { get; set; }
        public virtual MT_VEHICLE MT_VEHICLE { get; set; }
    }
}
