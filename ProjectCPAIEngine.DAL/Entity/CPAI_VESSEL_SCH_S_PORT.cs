//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CPAI_VESSEL_SCH_S_PORT
    {
        public CPAI_VESSEL_SCH_S_PORT()
        {
            this.CPAI_VESSEL_SCH_S_PRODUCTS = new HashSet<CPAI_VESSEL_SCH_S_PRODUCTS>();
        }
    
        public string VSPS_ROW_ID { get; set; }
        public string VSPS_FK_VESSEL_SCH_S { get; set; }
        public string VSPS_ORDER_PORT { get; set; }
        public Nullable<decimal> VSPS_FK_PORT { get; set; }
        public string VSPS_PORT_TYPE { get; set; }
        public System.DateTime VSPS_CREATED { get; set; }
        public string VSPS_CREATED_BY { get; set; }
        public System.DateTime VSPS_UPDATED { get; set; }
        public string VSPS_UPDATED_BY { get; set; }
    
        public virtual CPAI_VESSEL_SCH_S CPAI_VESSEL_SCH_S { get; set; }
        public virtual ICollection<CPAI_VESSEL_SCH_S_PRODUCTS> CPAI_VESSEL_SCH_S_PRODUCTS { get; set; }
        public virtual MT_PORT MT_PORT { get; set; }
    }
}
