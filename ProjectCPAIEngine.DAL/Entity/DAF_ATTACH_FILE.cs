//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class DAF_ATTACH_FILE
    {
        public string DAT_ROW_ID { get; set; }
        public string DAT_FK_DAF_DATA { get; set; }
        public string DAT_PATH { get; set; }
        public string DAT_INFO { get; set; }
        public Nullable<System.DateTime> DAT_CREATED { get; set; }
        public string DAT_CREATED_BY { get; set; }
        public Nullable<System.DateTime> DAT_UPDATED { get; set; }
        public string DAT_UPDATED_BY { get; set; }
        public string DAT_CAPTION { get; set; }
    
        public virtual DAF_DATA DAF_DATA { get; set; }
    }
}
