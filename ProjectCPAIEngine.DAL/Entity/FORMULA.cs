//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class FORMULA
    {
        public FORMULA()
        {
            this.FORMULA_SAP_PRICING = new HashSet<FORMULA_SAP_PRICING>();
        }
    
        public string FOR_CODE { get; set; }
        public string FOR_MET_NUM { get; set; }
        public string FOR_PRICE_STATUS { get; set; }
        public string FOR_NAME { get; set; }
        public Nullable<System.DateTime> FOR_CREATED_DATE { get; set; }
        public string FOR_CREATED_BY { get; set; }
        public Nullable<System.DateTime> FOR_UPDATED_DATE { get; set; }
        public string FOR_UPDATED_BY { get; set; }
        public string FOR_STATUS { get; set; }
    
        public virtual ICollection<FORMULA_SAP_PRICING> FORMULA_SAP_PRICING { get; set; }
    }
}
