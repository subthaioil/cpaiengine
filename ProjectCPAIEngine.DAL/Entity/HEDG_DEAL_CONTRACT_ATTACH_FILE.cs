//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_DEAL_CONTRACT_ATTACH_FILE
    {
        public string HDCA_ROW_ID { get; set; }
        public string HDCA_FK_HEDG_DEAL_HEADER { get; set; }
        public string HDCA_TYPE { get; set; }
        public string HDCA_PATH { get; set; }
        public string HDCA_REMARKS { get; set; }
        public string HDCA_STATUS { get; set; }
        public System.DateTime HDCA_CREATED_DATE { get; set; }
        public string HDCA_CREATED_BY { get; set; }
        public System.DateTime HDCA_UPDATED_DATE { get; set; }
        public string HDCA_UPDATED_BY { get; set; }
        public string HDCA_ORDER { get; set; }
        public string HDCA_CONTRACT_NO { get; set; }
        public string HDCA_CONTRACT_DETAIL { get; set; }
        public string HDCA_FK_HEDG_DEAL_CONT_TEMP { get; set; }
        public string HDCA_MAIL_SENDER { get; set; }
        public string HDCA_MAIL_SEND_TO { get; set; }
        public string HDCA_MAIL_CC { get; set; }
        public string HDCA_MAIL_BODY { get; set; }
        public string HDCA_MAIL_SUBJECT { get; set; }
        public string HDCA_MAIL_SENDER_NAME { get; set; }
    
        public virtual HEDG_DEAL_CONTRACT_TEMP HEDG_DEAL_CONTRACT_TEMP { get; set; }
        public virtual HEDG_DEAL_HEADER HEDG_DEAL_HEADER { get; set; }
    }
}
