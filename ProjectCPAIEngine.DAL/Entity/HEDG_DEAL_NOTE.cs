//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_DEAL_NOTE
    {
        public string HDN_ROW_ID { get; set; }
        public string HDN_NOTE_TYPE { get; set; }
        public string HDN_NOTE { get; set; }
        public System.DateTime HDN_CREATED_DATE { get; set; }
        public string HDN_CREATED_BY { get; set; }
        public System.DateTime HDN_UPDATED_DATE { get; set; }
        public string HDB_UPDATED_BY { get; set; }
        public string HDN_FK_DEAL_HISTORY_LOG { get; set; }
    
        public virtual HEDG_DEAL_HISTORY_LOG HEDG_DEAL_HISTORY_LOG { get; set; }
    }
}
