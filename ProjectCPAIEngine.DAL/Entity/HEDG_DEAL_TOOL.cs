//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_DEAL_TOOL
    {
        public HEDG_DEAL_TOOL()
        {
            this.HEDG_DEAL_TOOL_FORMULA = new HashSet<HEDG_DEAL_TOOL_FORMULA>();
            this.HEDG_DEAL_TOOL_NET_PREMIUM = new HashSet<HEDG_DEAL_TOOL_NET_PREMIUM>();
            this.HEDG_DEAL_TOOL_OPTION = new HashSet<HEDG_DEAL_TOOL_OPTION>();
        }
    
        public string HDT_ROW_ID { get; set; }
        public string HDT_FK_HEDG_DEAL_CHOICE_TYPE { get; set; }
        public string HDT_ORDER { get; set; }
        public string HDT_NAME { get; set; }
        public string HDT_TYPE { get; set; }
        public string HDT_EXTEND_VOLUME { get; set; }
        public string HDT_NET_PREMIUM_FLAG { get; set; }
        public string HDT_FORMULA_FLAG { get; set; }
        public System.DateTime HDT_CREATED_DATE { get; set; }
        public string HDT_CREATED_BY { get; set; }
        public System.DateTime HDT_UPDATED_DATE { get; set; }
        public string HDT_UPDATED_BY { get; set; }
        public string HDT_EXPRESSION { get; set; }
        public string HDT_VALUE { get; set; }
        public string HDT_VALUE_UNIT { get; set; }
        public string HDT_FK_HEDG_STR_TOOL { get; set; }
        public string HDT_IS_SELECTED { get; set; }
        public string HDT_FK_HEDG_MT_TOOL { get; set; }
        public string HDT_NET_PREMIUM_VALUE { get; set; }
        public string HDT_NET_PREMIUM_UNIT { get; set; }
    
        public virtual HEDG_DEAL_CHOICE_TYPE HEDG_DEAL_CHOICE_TYPE { get; set; }
        public virtual ICollection<HEDG_DEAL_TOOL_FORMULA> HEDG_DEAL_TOOL_FORMULA { get; set; }
        public virtual ICollection<HEDG_DEAL_TOOL_NET_PREMIUM> HEDG_DEAL_TOOL_NET_PREMIUM { get; set; }
        public virtual ICollection<HEDG_DEAL_TOOL_OPTION> HEDG_DEAL_TOOL_OPTION { get; set; }
        public virtual HEDG_STR_TOOL HEDG_STR_TOOL { get; set; }
        public virtual HEDG_MT_TOOL HEDG_MT_TOOL { get; set; }
    }
}
