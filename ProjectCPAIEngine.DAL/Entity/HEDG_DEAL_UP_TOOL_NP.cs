//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_DEAL_UP_TOOL_NP
    {
        public HEDG_DEAL_UP_TOOL_NP()
        {
            this.HEDG_DEAL_UP_TOOL_NP_ITEM = new HashSet<HEDG_DEAL_UP_TOOL_NP_ITEM>();
        }
    
        public string HDUN_ROW_ID { get; set; }
        public string HDUN_FK_HEDG_DEAL_DETAIL { get; set; }
        public string HDUN_ORDER { get; set; }
        public string HDUN_OPTION { get; set; }
        public System.DateTime HDUN_CREATED_DATE { get; set; }
        public string HDUN_CREATED_BY { get; set; }
        public System.DateTime HDUN_UPDATED_DATE { get; set; }
        public string HDUN_UPDATED_BY { get; set; }
    
        public virtual ICollection<HEDG_DEAL_UP_TOOL_NP_ITEM> HEDG_DEAL_UP_TOOL_NP_ITEM { get; set; }
        public virtual HEDG_DEAL_DETAIL HEDG_DEAL_DETAIL { get; set; }
    }
}
