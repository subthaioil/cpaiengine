//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_MT_PACKAGE_UNDERLYING
    {
        public HEDG_MT_PACKAGE_UNDERLYING()
        {
            this.HEDG_MT_PACKAGE_FW = new HashSet<HEDG_MT_PACKAGE_FW>();
        }
    
        public string HMU_ROW_ID { get; set; }
        public string HMU_ORDER { get; set; }
        public string HMU_AF_FRAME { get; set; }
        public string HMU_FK_HEDG_PRODUCT_FRONT { get; set; }
        public string HMU_FK_HEDG_PRODUCT_BACK { get; set; }
        public string HMU_WEIGHT_PERCENT { get; set; }
        public string HMU_WEIGHT_VOLUME { get; set; }
        public string HMU_AF_PRICE { get; set; }
        public string HMU_PRICE_UNIT { get; set; }
        public string HMU_BUY_SELL_TYPE { get; set; }
        public string HMU_C_F_FRONT { get; set; }
        public string HMU_WEIGHT_UNIT { get; set; }
        public System.DateTime HMU_CREATED_DATE { get; set; }
        public string HMU_CREATED_BY { get; set; }
        public System.DateTime HMU_UPDATED_DATE { get; set; }
        public string HMU_UPDATED_BY { get; set; }
        public string HMU_FK_HEDG_PACKAGE_ROLE { get; set; }
        public string HMU_C_F_BACK { get; set; }
        public string HMU_MAP_SPREAD_FLAG { get; set; }
    
        public virtual ICollection<HEDG_MT_PACKAGE_FW> HEDG_MT_PACKAGE_FW { get; set; }
        public virtual HEDG_MT_PACKAGE_ROLE HEDG_MT_PACKAGE_ROLE { get; set; }
    }
}
