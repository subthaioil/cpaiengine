//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_SETTLE_TOOL_OPTION
    {
        public HEDG_SETTLE_TOOL_OPTION()
        {
            this.HEDG_SETTLE_TOOL_KIO_OPTION = new HashSet<HEDG_SETTLE_TOOL_KIO_OPTION>();
        }
    
        public string HSTO_ROW_ID { get; set; }
        public string HSTO_FK_HEDG_SETTLE_DETAIL { get; set; }
        public string HSTO_GAIN { get; set; }
        public string HSTO_LOSS { get; set; }
        public string HSTO_GAIN_REMAINING { get; set; }
        public string HSTO_LOSS_REMAINING { get; set; }
        public string HSTO_CAPPED_MIN { get; set; }
        public string HSTO_CAPPED_MAX { get; set; }
        public string HSTO_CAPPED_PRICE { get; set; }
        public string HSTO_KNOCK_IN_OUT_BY { get; set; }
        public string HSTO_FK_HEDG_MT_PROD { get; set; }
        public string HSTO_PRODUCT_NAME { get; set; }
        public string HSTO_GAIN_FLAG { get; set; }
        public string HSTO_LOSS_FLAG { get; set; }
        public string HSTO_CAPPED_MIN_FLAG { get; set; }
        public string HSTO_CAPPED_MAX_FLAG { get; set; }
        public string HSTO_KNOCK_IN_OUT_FLAG { get; set; }
        public System.DateTime HSTO_CREATED_DATE { get; set; }
        public string HSTO_CREATED_BY { get; set; }
        public System.DateTime HSTO_UPDATED_DATE { get; set; }
        public string HSTO_UPDATED_BY { get; set; }
    
        public virtual HEDG_MT_PROD HEDG_MT_PROD { get; set; }
        public virtual ICollection<HEDG_SETTLE_TOOL_KIO_OPTION> HEDG_SETTLE_TOOL_KIO_OPTION { get; set; }
        public virtual HEDG_SETTLE_DETAIL HEDG_SETTLE_DETAIL { get; set; }
    }
}
