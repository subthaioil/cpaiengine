//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_STR_TOOL
    {
        public HEDG_STR_TOOL()
        {
            this.HEDG_STR_TOOL_FORMULA = new HashSet<HEDG_STR_TOOL_FORMULA>();
            this.HEDG_STR_TOOL_NET_PREMIUM = new HashSet<HEDG_STR_TOOL_NET_PREMIUM>();
            this.HEDG_STR_TOOL_OPTION = new HashSet<HEDG_STR_TOOL_OPTION>();
            this.HEDG_DEAL_TOOL = new HashSet<HEDG_DEAL_TOOL>();
        }
    
        public string HST_ROW_ID { get; set; }
        public string HST_FK_HEDG_STR_CHOICE_TYPE { get; set; }
        public string HST_ORDER { get; set; }
        public string HST_NAME { get; set; }
        public string HST_NET_PREMIUM_FLAG { get; set; }
        public string HST_USE_FORMULA { get; set; }
        public System.DateTime HST_CREATED_DATE { get; set; }
        public string HST_CREATED_BY { get; set; }
        public System.DateTime HST_UPDATED_DATE { get; set; }
        public string HST_UPDATED_BY { get; set; }
        public string HST_EXPRESSION { get; set; }
        public string HST_VALUE { get; set; }
        public string HST_FK_HEDG_MT_TOOL { get; set; }
        public string HST_FEATURE_TARGET_FLAG { get; set; }
        public string HST_FEATURE_SWAP_FLAG { get; set; }
        public string HST_FEATURE_CAPPED_FLAG { get; set; }
        public string HST_FEATURE_KNOCK_IN_OUT_FLAG { get; set; }
        public string HST_CODE { get; set; }
        public string HST_VALUE_UNIT_VOLUME { get; set; }
        public string HST_VALUE_UNIT_PRICE { get; set; }
        public string HST_USE_NET_PREMIUM { get; set; }
    
        public virtual HEDG_MT_TOOL HEDG_MT_TOOL { get; set; }
        public virtual HEDG_STR_CHOICE_TYPE HEDG_STR_CHOICE_TYPE { get; set; }
        public virtual ICollection<HEDG_STR_TOOL_FORMULA> HEDG_STR_TOOL_FORMULA { get; set; }
        public virtual ICollection<HEDG_STR_TOOL_NET_PREMIUM> HEDG_STR_TOOL_NET_PREMIUM { get; set; }
        public virtual ICollection<HEDG_STR_TOOL_OPTION> HEDG_STR_TOOL_OPTION { get; set; }
        public virtual ICollection<HEDG_DEAL_TOOL> HEDG_DEAL_TOOL { get; set; }
    }
}
