//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_STR_TOOL_NET_PREMIUM
    {
        public HEDG_STR_TOOL_NET_PREMIUM()
        {
            this.HEDG_STR_TOOL_NP_ITEM = new HashSet<HEDG_STR_TOOL_NP_ITEM>();
        }
    
        public string HSTN_ROW_ID { get; set; }
        public string HSTN_FK_HEDG_STR_TOOL { get; set; }
        public string HSTN_ORDER { get; set; }
        public string HSTN_OPTION { get; set; }
        public System.DateTime HSTN_CREATED_DATE { get; set; }
        public string HSTN_CREATED_BY { get; set; }
        public System.DateTime HSTN_UPDATED_DATE { get; set; }
        public string HSTN_UPDATED_BY { get; set; }
    
        public virtual ICollection<HEDG_STR_TOOL_NP_ITEM> HEDG_STR_TOOL_NP_ITEM { get; set; }
        public virtual HEDG_STR_TOOL HEDG_STR_TOOL { get; set; }
    }
}
