//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_AREA
    {
        public MT_AREA()
        {
            this.MT_UNIT = new HashSet<MT_UNIT>();
        }
    
        public string MAR_ROW_ID { get; set; }
        public string MAR_NAME { get; set; }
        public System.DateTime MAR_CREATED_DATE { get; set; }
        public string MAR_CREATED_BY { get; set; }
        public System.DateTime MAR_UPDATED_DATE { get; set; }
        public string MAR_UPDATED_BY { get; set; }
        public string MAR_STATUS { get; set; }
        public string MAR_ORDER { get; set; }
    
        public virtual ICollection<MT_UNIT> MT_UNIT { get; set; }
    }
}
