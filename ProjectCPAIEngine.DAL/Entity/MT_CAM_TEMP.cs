//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_CAM_TEMP
    {
        public MT_CAM_TEMP()
        {
            this.COO_CAM = new HashSet<COO_CAM>();
        }
    
        public string COCT_ROW_ID { get; set; }
        public string COCT_JSON { get; set; }
        public System.DateTime COCT_CREATED { get; set; }
        public string COCT_CREATED_BY { get; set; }
        public System.DateTime COCT_UPDATED { get; set; }
        public string COCT_UPDATED_BY { get; set; }
        public string COCT_VERSION { get; set; }
        public string COCT_REASON { get; set; }
    
        public virtual ICollection<COO_CAM> COO_CAM { get; set; }
    }
}
