//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_CRUDE_ROUTES
    {
        public System.DateTime MCR_DATE { get; set; }
        public string MCR_TD2 { get; set; }
        public string MCR_TD3 { get; set; }
        public string MCR_TD15 { get; set; }
        public string MCR_STATUS { get; set; }
        public Nullable<System.DateTime> MCR_CREATED_DATE { get; set; }
        public string MCR_CREATED_BY { get; set; }
        public Nullable<System.DateTime> MCR_UPDATED_DATE { get; set; }
        public string MCR_UPDATED_BY { get; set; }
    }
}
