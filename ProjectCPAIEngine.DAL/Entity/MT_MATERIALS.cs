//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_MATERIALS
    {
        public MT_MATERIALS()
        {
            this.CDP_OFFER_COMPET_ITEMS = new HashSet<CDP_OFFER_COMPET_ITEMS>();
            this.CHI_LOAD_PORT = new HashSet<CHI_LOAD_PORT>();
            this.CHIT_CARGO = new HashSet<CHIT_CARGO>();
            this.COO_DATA = new HashSet<COO_DATA>();
            this.CPAI_FREIGHT_CARGO = new HashSet<CPAI_FREIGHT_CARGO>();
            this.CPAI_VESSEL_SCH_S_PRODUCTS = new HashSet<CPAI_VESSEL_SCH_S_PRODUCTS>();
            this.CPAI_VESSEL_SCHEDULE_CARGO = new HashSet<CPAI_VESSEL_SCHEDULE_CARGO>();
            this.DAF_MATERIAL = new HashSet<DAF_MATERIAL>();
            this.FORMULA_PRICE = new HashSet<FORMULA_PRICE>();
            this.FORMULA_TIER = new HashSet<FORMULA_TIER>();
            this.MT_MATERIALS_CONTROL = new HashSet<MT_MATERIALS_CONTROL>();
            this.MT_SAP_MEMO_MAPPING = new HashSet<MT_SAP_MEMO_MAPPING>();
            this.PAF_BTM_GRADE_ITEMS = new HashSet<PAF_BTM_GRADE_ITEMS>();
            this.PAF_REVIEW_ITEMS = new HashSet<PAF_REVIEW_ITEMS>();
            this.PBC_MT_FREIGHT_PRICE = new HashSet<PBC_MT_FREIGHT_PRICE>();
            this.TRP_HEADER = new HashSet<TRP_HEADER>();
            this.SWP_MATERIAL = new HashSet<SWP_MATERIAL>();
            this.PCF_FREIGHT_MATERIAL = new HashSet<PCF_FREIGHT_MATERIAL>();
            this.PCF_PO_SURVEYOR_MATERIAL = new HashSet<PCF_PO_SURVEYOR_MATERIAL>();
            this.PAF_BIDDING_ITEMS = new HashSet<PAF_BIDDING_ITEMS>();
            this.PCF_MATERIAL = new HashSet<PCF_MATERIAL>();
            this.CLR_HEADER = new HashSet<CLR_HEADER>();
            this.BOR_SALES_HEADER = new HashSet<BOR_SALES_HEADER>();
            this.BOR_PURCHASE_HEADER = new HashSet<BOR_PURCHASE_HEADER>();
            this.CDP_DATA = new HashSet<CDP_DATA>();
            this.CDS_BIDDING_ITEMS = new HashSet<CDS_BIDDING_ITEMS>();
            this.CDS_PROPOSAL_ITEMS = new HashSet<CDS_PROPOSAL_ITEMS>();
        }
    
        public string MET_NUM { get; set; }
        public string MET_DIVISION { get; set; }
        public Nullable<System.DateTime> MET_CRE_DATE { get; set; }
        public string MET_NAME_PER_CRE { get; set; }
        public Nullable<System.DateTime> MET_DATE_LAST { get; set; }
        public string MET_MAT_TYPE { get; set; }
        public string MET_MAT_GROUP { get; set; }
        public string MET_OLD_MAT_NUM { get; set; }
        public string MET_BASE_MEA { get; set; }
        public string MET_LAB_DESIGN_OFFICE { get; set; }
        public string MET_FLA_MAT_DELETION { get; set; }
        public string MET_MAT_DES_THAI { get; set; }
        public string MET_MAT_DES_ENGLISH { get; set; }
        public string MET_CRUDE_TYPE { get; set; }
        public string MET_CRUDE_SRC { get; set; }
        public string MET_LOAD_PORT { get; set; }
        public string MET_ORIG_CRTY { get; set; }
        public string MET_GRP_CODE { get; set; }
        public string MET_PRD_TYPE { get; set; }
        public string MET_BL_SRC { get; set; }
        public string MET_FK_PRD { get; set; }
        public string MET_DEN_CONV { get; set; }
        public string MET_VFC_CONV { get; set; }
        public string MET_IO { get; set; }
        public string MET_MAT_ADD { get; set; }
        public string MET_LORRY { get; set; }
        public string MET_SHIP { get; set; }
        public string MET_THAPP { get; set; }
        public string MET_PIPE { get; set; }
        public string MET_MAT_GROP_TYPE { get; set; }
        public string MET_ZONE { get; set; }
        public string MET_PORT { get; set; }
        public Nullable<System.DateTime> MET_CREATED_DATE { get; set; }
        public string MET_CREATED_BY { get; set; }
        public Nullable<System.DateTime> MET_UPDATED_DATE { get; set; }
        public string MET_UPDATED_BY { get; set; }
        public string MET_IO_TLB { get; set; }
        public string MET_IO_TPX { get; set; }
        public string MET_DOEB_CODE { get; set; }
        public string MET_IO_LABIX { get; set; }
        public string MET_EXCISE_CODE { get; set; }
        public string MET_EXCISE_UOM { get; set; }
        public string MET_PRODUCT_DENSITY { get; set; }
        public string MET_GL_ACCOUNT { get; set; }
        public string MET_CREATE_TYPE { get; set; }
        public string MET_TYPE { get; set; }
    
        public virtual ICollection<CDP_OFFER_COMPET_ITEMS> CDP_OFFER_COMPET_ITEMS { get; set; }
        public virtual ICollection<CHI_LOAD_PORT> CHI_LOAD_PORT { get; set; }
        public virtual ICollection<CHIT_CARGO> CHIT_CARGO { get; set; }
        public virtual ICollection<COO_DATA> COO_DATA { get; set; }
        public virtual ICollection<CPAI_FREIGHT_CARGO> CPAI_FREIGHT_CARGO { get; set; }
        public virtual ICollection<CPAI_VESSEL_SCH_S_PRODUCTS> CPAI_VESSEL_SCH_S_PRODUCTS { get; set; }
        public virtual ICollection<CPAI_VESSEL_SCHEDULE_CARGO> CPAI_VESSEL_SCHEDULE_CARGO { get; set; }
        public virtual ICollection<DAF_MATERIAL> DAF_MATERIAL { get; set; }
        public virtual ICollection<FORMULA_PRICE> FORMULA_PRICE { get; set; }
        public virtual ICollection<FORMULA_TIER> FORMULA_TIER { get; set; }
        public virtual ICollection<MT_MATERIALS_CONTROL> MT_MATERIALS_CONTROL { get; set; }
        public virtual ICollection<MT_SAP_MEMO_MAPPING> MT_SAP_MEMO_MAPPING { get; set; }
        public virtual ICollection<PAF_BTM_GRADE_ITEMS> PAF_BTM_GRADE_ITEMS { get; set; }
        public virtual ICollection<PAF_REVIEW_ITEMS> PAF_REVIEW_ITEMS { get; set; }
        public virtual ICollection<PBC_MT_FREIGHT_PRICE> PBC_MT_FREIGHT_PRICE { get; set; }
        public virtual ICollection<TRP_HEADER> TRP_HEADER { get; set; }
        public virtual ICollection<SWP_MATERIAL> SWP_MATERIAL { get; set; }
        public virtual ICollection<PCF_FREIGHT_MATERIAL> PCF_FREIGHT_MATERIAL { get; set; }
        public virtual ICollection<PCF_PO_SURVEYOR_MATERIAL> PCF_PO_SURVEYOR_MATERIAL { get; set; }
        public virtual ICollection<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS { get; set; }
        public virtual ICollection<PCF_MATERIAL> PCF_MATERIAL { get; set; }
        public virtual ICollection<CLR_HEADER> CLR_HEADER { get; set; }
        public virtual ICollection<BOR_SALES_HEADER> BOR_SALES_HEADER { get; set; }
        public virtual ICollection<BOR_PURCHASE_HEADER> BOR_PURCHASE_HEADER { get; set; }
        public virtual ICollection<CDP_DATA> CDP_DATA { get; set; }
        public virtual ICollection<CDS_BIDDING_ITEMS> CDS_BIDDING_ITEMS { get; set; }
        public virtual ICollection<CDS_PROPOSAL_ITEMS> CDS_PROPOSAL_ITEMS { get; set; }
    }
}
