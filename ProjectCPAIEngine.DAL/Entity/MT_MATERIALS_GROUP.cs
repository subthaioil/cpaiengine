//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_MATERIALS_GROUP
    {
        public string MMT_ROW_ID { get; set; }
        public string MMT_FK_COMPANY_CODE { get; set; }
        public string MMT_GROUP_NAME { get; set; }
        public string MMT_GROUP_CODE { get; set; }
        public string MMT_NUM { get; set; }
        public string MMT_TRAN_ID { get; set; }
        public string MMT_STATUS { get; set; }
        public System.DateTime MMT_CREATED { get; set; }
        public string MMT_CREATED_BY { get; set; }
        public System.DateTime MMT_UPDATED { get; set; }
        public string MMT_UPDATED_BY { get; set; }
    
        public virtual MT_COMPANY MT_COMPANY { get; set; }
    }
}
