//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_SAP_MEMO_MAPPING
    {
        public string SMM_ROW_ID { get; set; }
        public string SMM_COMPANY_CODE { get; set; }
        public string SMM_COST_NAME { get; set; }
        public string SMM_ACC_NUM_VENDOR { get; set; }
        public string SMM_MET_NUM { get; set; }
        public string SMM_MAT_GROUP { get; set; }
        public string SMM_PLANNING_TYPE { get; set; }
        public string SMM_PLANNING_GROUP { get; set; }
        public System.DateTime SMM_CREATED_DATE { get; set; }
        public string SMM_CREATED_BY { get; set; }
        public System.DateTime SMM_UPDATED_DATE { get; set; }
        public string SMM_UPDATED_BY { get; set; }
    
        public virtual MT_VENDOR MT_VENDOR { get; set; }
        public virtual MT_MATERIALS MT_MATERIALS { get; set; }
    }
}
