//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAF_PROPOSAL_PRODUCT_ITEMS
    {
        public string PSP_ROW_ID { get; set; }
        public string PSP_FK_PAF_PROPOSAL_ITEMS { get; set; }
        public string PSP_FK_MATERIAL { get; set; }
        public Nullable<decimal> PSP_QUANTITY_MIN { get; set; }
        public Nullable<decimal> PSP_QUANTITY_MAX { get; set; }
        public string PSP_QUANTITY_UNIT { get; set; }
        public Nullable<System.DateTime> PSP_CREATED { get; set; }
        public string PSP_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PSP_UPDATED { get; set; }
        public string PSP_UPDATED_BY { get; set; }
    
        public virtual PAF_PROPOSAL_ITEMS PAF_PROPOSAL_ITEMS { get; set; }
    }
}
