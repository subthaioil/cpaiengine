//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAF_ROUND
    {
        public PAF_ROUND()
        {
            this.PAF_ROUND_ITEMS = new HashSet<PAF_ROUND_ITEMS>();
        }
    
        public string PRN_ROW_ID { get; set; }
        public Nullable<decimal> PRN_COUNT { get; set; }
        public Nullable<System.DateTime> PRN_CREATED { get; set; }
        public string PRN_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PRN_UPDATED { get; set; }
        public string PRN_UPDATED_BY { get; set; }
        public string PRN_FK_PAF_BIDDING_ITEMS { get; set; }
        public string PRN_FLOAT { get; set; }
        public Nullable<decimal> PRN_FIXED { get; set; }
    
        public virtual PAF_BIDDING_ITEMS PAF_BIDDING_ITEMS { get; set; }
        public virtual ICollection<PAF_ROUND_ITEMS> PAF_ROUND_ITEMS { get; set; }
    }
}
