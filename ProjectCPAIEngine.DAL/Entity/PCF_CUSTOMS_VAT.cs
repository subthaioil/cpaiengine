//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCF_CUSTOMS_VAT
    {
        public string TRIP_NO { get; set; }
        public decimal MAT_ITEM_NO { get; set; }
        public Nullable<decimal> CUSTOMS_PRICE { get; set; }
        public Nullable<decimal> FREIGHT_AMOUNT { get; set; }
        public Nullable<decimal> INSURANCE_AMOUNT { get; set; }
        public Nullable<decimal> INSURANCE_RATE { get; set; }
        public Nullable<decimal> ROE { get; set; }
        public Nullable<decimal> FOB { get; set; }
        public Nullable<decimal> FRT { get; set; }
        public Nullable<decimal> CFR { get; set; }
        public Nullable<decimal> INS { get; set; }
        public Nullable<decimal> CIF_SUM_INS { get; set; }
        public Nullable<decimal> PREMIUM { get; set; }
        public Nullable<decimal> TOTAL_USD_100 { get; set; }
        public Nullable<decimal> BAHT_100 { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> TAX_BASE { get; set; }
        public Nullable<decimal> CORRECT_VAT { get; set; }
        public Nullable<decimal> IMPORT_DUTY { get; set; }
        public Nullable<decimal> EXCISE_TAX { get; set; }
        public Nullable<decimal> MUNICIPAL_TAX { get; set; }
        public Nullable<decimal> OIL_FUEL_FUND { get; set; }
        public Nullable<decimal> ENERY_OIL_FUEL_FUND { get; set; }
        public Nullable<decimal> DEPOSIT_OF_EXCISE_TAX { get; set; }
        public Nullable<decimal> DEPOSIT_OF_MUNICIPAL_TAX { get; set; }
        public Nullable<decimal> DEPOSIT_OF_IMPORT_DUTY { get; set; }
        public string CAL_VOLUME_UNIT { get; set; }
        public Nullable<System.DateTime> DOCUMENT_DATE { get; set; }
        public Nullable<System.DateTime> POSTING_DATE { get; set; }
        public string SAP_FI_DOC { get; set; }
        public string SAP_FI_DOC_STATUS { get; set; }
        public string SAP_FI_DOC_ERROR { get; set; }
        public string SAP_INVOICE_DOC { get; set; }
        public string SAP_INVOICE_DOC_STATUS { get; set; }
        public string SAP_INVOICE_DOC_ERROR { get; set; }
        public string SAP_FI_DOC_INVOICE { get; set; }
        public string SAP_FI_DOC_INVOICE_STATUS { get; set; }
        public string SAP_FI_DOC_INVOICE_ERROR { get; set; }
        public Nullable<decimal> BAHT_105 { get; set; }
        public Nullable<decimal> PRICE0_STS { get; set; }
        public string INVOICE_STATUS { get; set; }
        public Nullable<System.DateTime> BL_DATE { get; set; }
        public Nullable<System.DateTime> IMPORT_DATE { get; set; }
    }
}
