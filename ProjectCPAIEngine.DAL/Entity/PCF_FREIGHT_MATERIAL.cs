//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCF_FREIGHT_MATERIAL
    {
        public string PFM_TRIP_NO { get; set; }
        public decimal PFM_ITEM_NO { get; set; }
        public string PFM_MET_NUM { get; set; }
        public Nullable<System.DateTime> PFM_CREATED_DATE { get; set; }
        public string PFM_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PFM_UPDATED_DATE { get; set; }
        public string PFM_UPDATED_BY { get; set; }
    
        public virtual MT_MATERIALS MT_MATERIALS { get; set; }
    }
}
