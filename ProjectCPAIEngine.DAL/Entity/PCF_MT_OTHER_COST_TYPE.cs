//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PCF_MT_OTHER_COST_TYPE
    {
        public decimal PMO_CODE { get; set; }
        public string PMO_NAME { get; set; }
        public string PMO_DATA_FROM_FREIGHT { get; set; }
        public string PMO_MATERIAL { get; set; }
        public string PMO_MATERIAL_DISPLAY { get; set; }
        public string PMO_SUPPLIER { get; set; }
        public string PMO_SUPPLIER_DISPLAY { get; set; }
        public string PMO_PORT { get; set; }
        public string PMO_PORT_DISPLAY { get; set; }
        public string PMO_PERIOD_FROM_TO { get; set; }
        public string PMO_PERIOD_FROM_TO_DISPLAY { get; set; }
        public string PMO_PRICE { get; set; }
        public string PMO_PRICE_DISPLAY { get; set; }
        public string PMO_HOURS { get; set; }
        public string PMO_HOURS_DISPLAY { get; set; }
        public string PMO_TOTAL { get; set; }
        public string PMO_TOTAL_DISPLAY { get; set; }
        public string PMO_DUE_DATE { get; set; }
        public string PMO_DUE_DATE_DISPLAY { get; set; }
        public string PMO_CERTIFIED_DATE { get; set; }
        public string PMO_CERTIFIED_DATE_DISPLAY { get; set; }
        public string PMO_APPROVED_DATE { get; set; }
        public string PMO_APPROVED_DATE_DISPLAY { get; set; }
        public string PMO_REMARK { get; set; }
        public string PMO_REMARK_DISPLAY { get; set; }
        public Nullable<System.DateTime> PMO_CREATE_DATE { get; set; }
        public string PMO_CREATE_BY { get; set; }
        public Nullable<System.DateTime> PMO_UPDATED_DATE { get; set; }
        public string PMO_UPDATED_BY { get; set; }
        public string PMO_PAYMENTTERM { get; set; }
        public string PMO_PAYMENTTERM_DISPLAY { get; set; }
        public string PMO_PRODUCT { get; set; }
        public string PMO_PRODUCT_DISPLAY { get; set; }
        public string PMO_QTY { get; set; }
        public string PMO_QTY_DISPLAY { get; set; }
        public string PMO_PURCHASE_TYPE { get; set; }
        public string PMO_PURCHASE_TYPE_DISPLAY { get; set; }
        public string PMO_BENCHMARK { get; set; }
        public string PMO_BENCHMARK_DISPLAY { get; set; }
        public string PMO_PRICE_STATUS { get; set; }
        public string PMO_PRICE_STATUS_DISPLAY { get; set; }
        public string PMO_PRICING_PERIOD { get; set; }
        public string PMO_PRICING_PERIOD_DISPLAY { get; set; }
        public string PMO_BASE_PRICE { get; set; }
        public string PMO_BASE_PRICE_DISPLAY { get; set; }
        public string PMO_OSP_PREMIUM { get; set; }
        public string PMO_OSP_PREMIUM_DISPLAY { get; set; }
        public string PMO_TRADING_PREMIUM { get; set; }
        public string PMO_TRADING_PREMIUM_DISPLAY { get; set; }
        public string PMO_OTHER_COST { get; set; }
        public string PMO_OTHER_COST_DISPLAY { get; set; }
    }
}
