﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Model
{
    public class CustColor
    {
        public MT_CUST_DETAIL CUSTOMER { get; set; }
        public string CUSTOMER_COLOR_CODE { get; set; }
    }
}
