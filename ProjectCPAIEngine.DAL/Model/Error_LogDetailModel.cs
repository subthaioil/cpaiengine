﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class Error_LogDetailModel
    {
        public int ID { get; set; }
        public string Module { get; set; }
        public string Function { get; set; }
        public string EmpID_Action { get; set; }
        public int? ERROR_NUMBER { get; set; }
        public int? ERROR_SEVERITY { get; set; }
        public int? ERROR_STATE { get; set; }
        public int? ERROR_LINE { get; set; }
        public string ERROR_PROCEDURE { get; set; }
        public string ERROR_MESSAGE { get; set; }
        public string TIME_STAMP { get; set; }
    }
}