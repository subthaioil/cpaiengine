﻿using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Model
{
    public class FunctionTransaction
    {
        public FUNCTION_TRANSACTION FUNCTION_TRANSACTION { get; set; }
        public string VESSEL_NAME { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string BROKER_NAME { get; set; }
        public string CHARTERER_NAME { get; set; }
        public string CARGO_NAME { get; set; }
        public string LOAD_PORT_NAME { get; set; }
        public string DISCHARGE_PORT_NAME { get; set; }
        public string DELIVERY_DATE_FROM { get;  set; }
        public string DELIVERY_DATE_TO { get; set; }
        public string UNIT_PRICE_TYPE { get; set; }
        public string UNIT_PRICE_VALUE { get; set; }
        public string UNIT_PRICE_ORDER { get; set; }
        public string EXTEN_COST { get; set; }
        public string EST_FREIGHT { get; set; }
        public string CURRENCY_SYMBOL { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string UNDERLYING_NAME { get; set; }

        public string LOADING_PERIOD_FROM { get; set; }
        public string LOADING_PERIOD_TO { get; set; }
        public string DISCHARGING_PERIOD_FROM { get; set; }
        public string DISCHARGING_PERIODE_TO { get; set; }

        public string ASSAY_FILE { get; set; }
        public string ASSAY_FROM { get; set; }
        public string CAM_FILE { get; set; }
        public string DENSITY { get; set; }
        public string SULPHUR { get; set; }
        public string ACID { get; set; }
        public string SUPPORT_FILE { get; set; }
        public string UNIT { get; set; }
        public string UNIT_STATUS { get; set; }
        public string REQUESTER_NAME { get; set; }
        public string DRAFT_CAM_FILE { get; set; }
        public string FINAL_CAM_FILE { get; set; }
        public string DRAFT_CAM_STATUS { get; set; }
        public string FINAL_CAM_STATUS { get; set; }
        public string DRAFT_APPROVER_NAME { get; set; }
        public string DRAFT_SUBMIT_DATE { get; set; }
        public string DRAFT_APPROVE_DATE { get; set; }
        public string FINAL_APPROVER_NAME { get; set; }
        public string FINAL_SUBMIT_DATE { get; set; }
        public string FINAL_APPROVE_DATE { get; set; }
        public string STATUS { get; set; }
        public string SYSTEM { get; set; }
        public string COO_FK_COO_CAM { get; set; }
        public string NOTE { get; set; }
        public string UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public string COO_EXPERT { get; set; }
        public string COUNTRY { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string FINAL_PRICE { get; set; }
        public string MAXIMUM_BUYING_PRICE { get; set; }
        public string VOLUMN_KBBL_MIN { get; set; }
        public string VOLUMN_KBBL_MAX { get; set; }
        public string VOLUMN_KT_MIN { get; set; }
        public string VOLUMN_KT_MAX { get; set; }
        public string LP_RESULT { get; set; }
        public string TPC_MONTH { get; set; }
        public string TPC_YEAR { get; set; }
        public string TN_STATUS { get; set; }
        public string SC_STATUS { get; set; }

        public string COMPANY { get; set; }
        public string YEAR { get; set; }

        public string EXCEED_FLAG { get; set; }
        public string DESCRIPTION { get; set; }

    }
}