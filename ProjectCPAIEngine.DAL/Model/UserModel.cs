﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class UserModel
    {
        private List<MenuPermission> _MenuPermission { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string RoleType { get; set; }
        public string UserGroup { get; set; }
        public string UserWebMenu { get; set; }
        public List<MenuPermission> MenuPermission
        {
            get
            {
                if (_MenuPermission == null) return new List<Model.MenuPermission>();
                else return _MenuPermission;
            }
            set
            {
                if (_MenuPermission == null) _MenuPermission = new List<Model.MenuPermission>();
                _MenuPermission = value;
            }
        }


        public bool GetEventPermission(string ActionName, string MenuLink)
        {
            bool Action = false;
            if (MenuPermission != null)
            {
                MenuPermission Menupermission = FindMenuByMenulink(MenuPermission, MenuLink);
                if (Menupermission != null)
                {
                    foreach (var _Action in Menupermission.MENU_Event)
                    {
                        if (_Action.LNG_DESCRIPTION.ToUpper() == ActionName.ToUpper())
                        {
                            Action = true; break;
                        }
                    }
                }
            }
            return Action;

        }

        private MenuPermission FindMenuByMenulink(List<MenuPermission> lstMenu, string Menulink)
        {
            MenuPermission Menupermission = null;
            foreach (var ItemMenu in lstMenu)
            {
                if (ItemMenu.MEU_URL.ToUpper().IndexOf(Menulink.ToUpper()) >= 0)
                {
                    Menupermission = ItemMenu;
                    break;
                }
                else
                {
                    if (ItemMenu.MENU_CHILD.Count > 0)
                    {
                        Menupermission = FindMenuByMenulink(ItemMenu.MENU_CHILD, Menulink);
                        if (Menupermission != null) break;
                    }
                }
            }
            return Menupermission;
        }



        public string UserPassword { get; set; }

    }

    public class MenuPermission
    {
        private string _LNG_DESCRIPTION = "";
        private string _MEU_URL = "";
        private string _MEU_ROWID = "";
        private string _MEU_URL_DIRECT = "";
        private string _MEU_IMG = "";
        private string _MEU_LEVEL = "";
        private string _MEU_CONTROL_TYPE = "";
        private string _MEU_LIST_NO = "";
        private List<MenuPermission> _MENU_CHILD { get; set; }
        private List<MenuPermission> _MENU_Event { get; set; }
        public string LNG_DESCRIPTION { get { return _LNG_DESCRIPTION; } set { _LNG_DESCRIPTION = value; } }
        public string MEU_ROWID { get { return _MEU_ROWID; } set { _MEU_ROWID = value; } }
        public string MEU_URL { get { return _MEU_URL; } set { _MEU_URL = value; } }
        public string MEU_IMG { get { return _MEU_IMG; } set { _MEU_IMG = value; } }
        public string MEU_LEVEL { get { return _MEU_LEVEL; } set { _MEU_LEVEL = value; } }
        public string MEU_CONTROL_TYPE { get { return _MEU_CONTROL_TYPE; } set { _MEU_CONTROL_TYPE = value; } }
        public string MEU_URL_DIRECT { get { return _MEU_URL_DIRECT; } set { _MEU_URL_DIRECT = value; } }
        public string MEU_LIST_NO { get { return _MEU_LIST_NO; } set { _MEU_LIST_NO = value; } }
        public List<MenuPermission> MENU_CHILD
        {
            get
            {
                if (_MENU_CHILD == null) return new List<MenuPermission>();
                else return _MENU_CHILD;
            }
            set {if (_MENU_CHILD == null) _MENU_CHILD = new List<MenuPermission>();
                 _MENU_CHILD = value; }
        }
        public List<MenuPermission> MENU_Event
        {
            get
            {
                if (_MENU_Event == null) return new List<MenuPermission>();
                else return _MENU_Event;
            }
            set
            {
                if (_MENU_Event == null) _MENU_Event = new List<MenuPermission>();
                _MENU_Event = value;
            }
        }
    }

    public class userApprove
    {
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string TransactionID { get; set; }
    }
}