﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.Model
{
  
    public class VendorColor
    {
        public MT_VENDOR VENDOR { get; set; }
        public string VENDOR_COLOR_CODE { get; set; }
    }
}
