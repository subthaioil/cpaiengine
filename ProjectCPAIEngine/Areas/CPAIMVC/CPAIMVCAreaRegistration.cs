﻿using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC
{
    public class CPAIMVCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CPAIMVC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CPAIMVC_default",
                "CPAIMVC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}