﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using System.Web.Routing;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class AreaController : BaseController
    {
        // GET: CPAIMVC/Area
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Area");
        }

        [HttpGet]
        public ActionResult Search()
        {
            AreaViewModel model = initialModel();
            AreaServiceModel serviceModel = new AreaServiceModel();
            AreaViewModel_Search searchModel = new AreaViewModel_Search();
            serviceModel.Search(ref searchModel);
            model.area_Search = searchModel;
            return View(model);
        }


        public ActionResult Search(AreaViewModel postModel)
        {
            AreaViewModel model = initialModel();
            AreaServiceModel serviceModel = new AreaServiceModel();
            AreaViewModel_Search searchModel = new AreaViewModel_Search();

            searchModel = postModel.area_Search;
            serviceModel.Search(ref searchModel);
            model.area_Search = searchModel;
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {

            if (ViewBag.action_create)
            {
                AreaViewModel model = initialModel();              
                model.area_Detail.AreaCode = "";              
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Area" }));
            }
           
        }


        [HttpPost]
        public ActionResult Create(FormCollection frm, AreaViewModel viewModel)
        {
            if (ViewBag.action_create)
            {
                AreaServiceModel serviceModel = new AreaServiceModel();
                ReturnValue rtn = new ReturnValue();
                viewModel.area_Detail.AreaStatus = viewModel.area_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Add(viewModel.area_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                AreaViewModel model = initialModel();
                viewModel.ddl_FreeUnit = DropdownServiceModel.getFreeUnit();
                viewModel.ddl_Unit = DropdownServiceModel.getUnit();
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Area" }));
            }
        }


        [HttpGet]
        public ActionResult Edit(string areaCode)
        {
            if (ViewBag.action_edit)
            {
                AreaServiceModel serviceModel = new AreaServiceModel();

                AreaViewModel model = initialModel();
                model.area_Detail = serviceModel.Get(areaCode);                
                TempData["AreaCode"] = areaCode;
                model.ddl_Unit = DropdownServiceModel.getUnitByArea(areaCode);
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Area" }));
            }
        }


        [HttpPost]
        public ActionResult Edit(string areaCode, AreaViewModel model)
        {
            if (ViewBag.action_edit)
            {
                AreaServiceModel serviceModel = new AreaServiceModel();

                ReturnValue rtn = new ReturnValue();
                model.area_Detail.AreaStatus = model.area_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                model.area_Detail.AreaCode = areaCode;

               
                rtn = serviceModel.Edit(model.area_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["AreaCode"] = areaCode;
                AreaViewModel initModel = initialModel();
                model.ddl_Unit = DropdownServiceModel.getUnitByArea(areaCode);
                model.ddl_FreeUnit = DropdownServiceModel.getFreeUnit();
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Area" }));
            }

        }



        [HttpGet]
        public ActionResult View(string areaCode)
        {
            if (ViewBag.action_view)
            {
                AreaServiceModel serviceModel = new AreaServiceModel();

                AreaViewModel model = initialModel();
                model.area_Detail = serviceModel.Get(areaCode);
                TempData["AreaCode"] = areaCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Area" }));
            }

        }


        public AreaViewModel initialModel()
        {
            AreaServiceModel serviceModel = new AreaServiceModel();
            AreaViewModel model = new AreaViewModel();

            model.area_Search = new AreaViewModel_Search();
            model.area_Search.sSearchData = new List<AreaViewModel_SeachData>();

            model.area_Detail = new AreaViewModel_Detail();
            model.area_Detail.BoolStatus = true;
            model.ddl_areaStatus = DropdownServiceModel.getMasterStatus();
            model.ddl_Unit = DropdownServiceModel.getUnit();
            model.ddl_FreeUnit = DropdownServiceModel.getFreeUnit();
           // model.ddl_Unit = DropdownServiceModel.getUnit();

            return model;
        }

    }
}