﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using ProjectCPAIEngine.Areas.CPAIMVC;
using System.IO;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class BaseController : Controller
    {
        //เอาไว้ใช้สร้างเมนู
        public StringBuilder HTMLMENU = new StringBuilder();
        DateTime _Dt = DateTime.Now;
        public string lbUserName = "";
        public string lbUserID = "";
        public string lbHeader = "";
        public string lbHeaderDetail = "";
        public string lbHeaderDay = "";
        public string lbHeaderDate = "";
        public string lbHeaderTime = "";
        public string empImage = "";
        //จบส่วน

        //// string format (MENU)
        //private string pstrMainMenuFormat = "<li><a href=\"{0}\" {1} class=\"dropdown-toggle\" data-toggle=\"dropdown\">{2}<span class=\"caret\"></span></a></li>";
        //private string pstrMainMenuWithSubMenuFormat = "<li><a href=\"{0}\" {1} class=\"dropdown-toggle\" data-toggle=\"dropdown\">{2}<span class=\"caret\"></span></a><ul class=\"dropdown-menu\">{3}</ul></li>";
        //private string pstrSubMenuFormat = "<li><a href=\"{0}\" {1}>{2}</a></li>";
        //private string pstrSubMenuWithSubMenuFormat = "<li class=\"dropdown-submenu\"><a href=\"{0}\" {1}>{2}</a><ul class=\"dropdown-menu\">{3}</ul>";

        public BaseController()
        {
            LoadUserInfo();
            GenMenu();
            ViewData["lbUserName"] = lbUserName;
            ViewBag.UserName = lbUserName;
            ViewData["empImage"] = empImage;
            ViewData["lbHeaderTime"] = lbHeaderTime;
            ViewData["lbHeaderDay"] = lbHeaderDay;
            ViewData["lbHeaderDate"] = lbHeaderDate;
            ViewData["HTMLMENU"] = HTMLMENU;

            string controllerName = GetType().Name.Replace("Controller", "");

            GetActionMenu();


        }

        ShareFn _FNx = new ShareFn();
        ShareFn _FN = new ShareFn();
        private void LoadUserInfo()
        {
            if (Const.User != null && Const.User.UserName != null)
            {
                _Dt = DateTime.Now;
                lbUserName = Const.User.Name;
                lbUserID = Const.User.UserName;
                empImage = _FNx.GetPathofPersonalImg(Const.User.UserName);
                lbHeaderDay = _Dt.DayOfWeek.ToString();
                lbHeaderDate = string.Format("{0} {1}, {2}", _Dt.ToString("MMM"), _Dt.Day.ToString(), _Dt.ToString("yyyy"));
                lbHeaderTime = _Dt.ToString("hh:mm tt");
            }

        }

        private void GetActionMenu()
        {
            ViewBag.action_view = false; //getPermissionActionMenu("view", "cpaimvc/" + controllerName);
            ViewBag.action_create = false; //getPermissionActionMenu("create", "cpaimvc/" + controllerName);
            ViewBag.action_edit = false; //getPermissionActionMenu("edit", "cpaimvc/" + controllerName);
            ViewBag.action_search = false; //getPermissionActionMenu("search", "cpaimvc/" + controllerName);
            ViewBag.action_delete = false; //getPermissionActionMenu("delete", "cpaimvc/" + controllerName);

            try
            {

                string action_menu = Const.UserActionMenu;
                foreach (var item in action_menu.Split('|'))
                {
                    if (item.ToLower() == "view")
                        ViewBag.action_view = true;

                    if (item.ToLower() == "create")
                        ViewBag.action_create = true;

                    if (item.ToLower() == "edit")
                        ViewBag.action_edit = true;

                    if (item.ToLower() == "search")
                        ViewBag.action_search = true;

                    if (item.ToLower() == "delete")
                        ViewBag.action_delete = true;

                }
            }
            catch
            {

            }
        }

        //private void GenMenu()
        //{
        //    if (Const.User != null)
        //    {
        //        if (Const.User.MenuPermission.Count > 0)
        //        {
        //            StringBuilder _html = new StringBuilder();
        //            List<MenuPermission> _lstMenu = Const.User.MenuPermission.OrderBy(x => x.MEU_LIST_NO).ToList();
        //            foreach (var _mainMenu in _lstMenu)
        //            {
        //                LoadMenuPermission(1, _mainMenu, ref _html);
        //            }
        //            HTMLMENU = _html;
        //        }
        //    }
        //}

        //private void LoadMenuPermission(int LevelMenu, MenuPermission _Menu, ref StringBuilder _html)
        //{
        //    if (_Menu.MENU_CHILD.Count > 0)
        //    {
        //        string parentURL = "#";
        //        if (_Menu.MEU_URL != "#")
        //        {
        //            parentURL = _FNx.GetSiteRootUrl(_Menu.MEU_URL);
        //        }
        //        _html.Append(string.Format("<li class=\"dropdown{0}\" style=\"font-size: 22px;\">", LevelMenu));
        //        if (LevelMenu == 2)
        //        {
        //            _html.Append(string.Format("<a href=\"{1}\">{0}</a>", _Menu.LNG_DESCRIPTION, parentURL));
        //        }
        //        else
        //        {
        //            _html.Append(string.Format("<a href=\"{0}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-size: 22px;\">{1}<span class=\"caret\"></span></a>", parentURL, _Menu.LNG_DESCRIPTION));
        //        }

        //        _html.Append(string.Format("<ul class=\"dropdown-menu\">", LevelMenu));
        //        LevelMenu++;
        //        foreach (var _item in _Menu.MENU_CHILD.OrderBy(x => x.MEU_LIST_NO).ToList())
        //        {
        //            LoadMenuPermission(LevelMenu, _item, ref _html);
        //        }
        //        _html.Append("</ul></li>");

        //    }
        //    else
        //    {
        //        if ((String.IsNullOrEmpty(_Menu.MEU_URL_DIRECT) != true || Convert.ToString(_Menu.MEU_URL_DIRECT) != "#") && Convert.ToString(_Menu.MEU_URL) == "REDIRECT")
        //        {
        //            // New windows go to MEU_URL_DIRECT
        //            _html.Append(string.Format(
        //          "<li style=\"font-size: 22px;\"> " +
        //          "<a href=\"{1}\" target=\"_blank\">" +
        //          "{0}</a>" +
        //          "</li>", _Menu.LNG_DESCRIPTION, _Menu.MEU_URL_DIRECT));

        //        }
        //        else
        //        {
        //            _html.Append(string.Format(
        //             "<li style=\"font-size: 22px;\"> " +
        //             "<a href=\"{1}\">" +
        //             "{0}</a>" +
        //             "</li>", _Menu.LNG_DESCRIPTION, _FNx.GetSiteRootUrl(_Menu.MEU_URL)));
        //        }


        //    }


        //}

        //Sample Function Get ActionMenu
        //actionName = "create"
        //url = "cpaimvc/vendor"

        public static bool getPermissionActionMenu(string actionName, string url)
        {
            bool actionMenu = Const.User.GetEventPermission(actionName, url);
            return actionMenu;
        }


        // Before Edit GenMenu by menuID
        //private void GenMenu()
        //{
        //    if (Const.User != null)
        //    {
        //        if (Const.User.MenuPermission.Count > 0)
        //        {
        //            StringBuilder _html = new StringBuilder();
        //            List<MenuPermission> _lstMenu = Const.User.MenuPermission;

        //            var mainMenu = _lstMenu.Where(item => int.Parse(item.MEU_LEVEL).Equals(1) && item.MEU_CONTROL_TYPE.Equals("MENU")).OrderBy(x => int.Parse(x.MEU_LIST_NO));

        //            if (mainMenu != null)
        //            {
        //                foreach (var item in mainMenu)
        //                {
        //                    string parentURL = GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
        //                    string direct = checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";
        //                    if (item.MENU_CHILD.Count > 0)
        //                    {
        //                        var subMenu = GetSubMenu(item.MENU_CHILD);
        //                        _html.Append(string.Format(pstrMainMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
        //                    }
        //                    else
        //                    {
        //                        _html.Append(string.Format(pstrMainMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
        //                    }
        //                }
        //            }

        //            HTMLMENU = _html;
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect("../web/login.aspx");
        //    }
        //}

        //public string GetSubMenu(List<MenuPermission> menuLst)
        //{
        //    StringBuilder _html = new StringBuilder();

        //    try
        //    {
        //        if (menuLst != null)
        //        {
        //            var query = menuLst.OrderBy(s => int.Parse(s.MEU_LIST_NO));

        //            foreach (var item in query)
        //            {
        //                //string parentURL = GetURL(iSub.MEU_URL, iSub.MEU_URL_DIRECT, iSub.LNG_DESCRIPTION);
        //                string parentURL = GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
        //                string direct = checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";

        //                if (item.MENU_CHILD.Count > 0)
        //                {
        //                    var subMenu = GetSubMenu(item.MENU_CHILD);
        //                    _html.Append(string.Format(pstrSubMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
        //                }
        //                else
        //                {
        //                    _html.Append(string.Format(pstrSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _html.Append("");
        //        }


        //        return _html.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        private void GenMenu()
        {
            if (Const.User != null)
            {
                //if (Const.User.MenuPermission.Count > 0)
                //{
                //    StringBuilder _html = new StringBuilder();
                //    List<MenuPermission> _lstMenu = Const.User.MenuPermission;

                //    var mainMenu = _lstMenu.Where(item => int.Parse(item.MEU_LEVEL).Equals(1) && item.MEU_CONTROL_TYPE.Equals("MENU")).OrderBy(x => int.Parse(x.MEU_LIST_NO));

                //    if (mainMenu != null)
                //    {
                //        foreach (var item in mainMenu)
                //        {
                //            string parentURL = GetMenuURL(item.MEU_ROWID, item.MEU_URL); //GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
                //            string direct = ""; //checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";
                //            if (item.MENU_CHILD.Count > 0)
                //            {
                //                var subMenu = GetSubMenu(item.MENU_CHILD);
                //                _html.Append(string.Format(pstrMainMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
                //            }
                //            else
                //            {
                //                _html.Append(string.Format(pstrMainMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
                //            }
                //        }
                //    }

                //    HTMLMENU = _html;
                //}

                HTMLMENU.Append(Models.MenuServiceModel.GenMenu(Const.User));
            }
            else
            {
                if (Response !=null)
                {
                    Response.Redirect("../web/login.aspx");
                }               
            }
        }

        //public string GetSubMenu(List<MenuPermission> menuLst)
        //{
        //    StringBuilder _html = new StringBuilder();

        //    try
        //    {
        //        if (menuLst != null)
        //        {
        //            var query = menuLst.OrderBy(s => int.Parse(s.MEU_LIST_NO));

        //            foreach (var item in query)
        //            {
        //                //string parentURL = GetURL(iSub.MEU_URL, iSub.MEU_URL_DIRECT, iSub.LNG_DESCRIPTION);
        //                string parentURL = GetMenuURL(item.MEU_ROWID, item.MEU_URL); //GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
        //                string direct = "";// checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";

        //                if (item.MENU_CHILD.Count > 0)
        //                {
        //                    var subMenu = GetSubMenu(item.MENU_CHILD);
        //                    _html.Append(string.Format(pstrSubMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
        //                }
        //                else
        //                {
        //                    _html.Append(string.Format(pstrSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _html.Append("");
        //        }


        //        return _html.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        //private bool checkURLDirect(string MEU_URL, string MEU_URL_DIRECT)
        //{
        //    var check = false;
        //    if ((String.IsNullOrEmpty(MEU_URL_DIRECT) != true || Convert.ToString(MEU_URL_DIRECT) != "#") && Convert.ToString(MEU_URL) == "REDIRECT")
        //    {
        //        // New windows go to MEU_URL_DIRECT
        //        check = true;

        //    }
        //    return check;
        //}

        //private string GetURL(string strURL, string strURLDirect)
        //{

        //    string parentURL = "#";

        //    if (checkURLDirect(strURL, strURLDirect) == true)
        //    {
        //        // New windows go to MEU_URL_DIRECT
        //        if (strURLDirect != "#" && !string.IsNullOrEmpty(strURLDirect))
        //        {
        //            parentURL = strURLDirect;
        //        }

        //    }
        //    else
        //    {
        //        if (strURL != "#")
        //        {
        //            parentURL = _FN.GetSiteRootUrl(strURL);
        //        }
        //    }
        //    return parentURL;
        //}

        //private string GetMenuURL(string pMenuID, string pMenuURL)
        //{

        //    string URL = "#";

        //    if (pMenuURL != "#" && !string.IsNullOrEmpty(pMenuURL))
        //        URL = _FN.GetSiteRootUrl("CPAIMVC/Menu?menuID=" + pMenuID);

        //    return URL;
        //}

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}