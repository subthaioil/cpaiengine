﻿using com.pttict.downstream.common.utilities;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using com.pttict.sap.Interface.Service;
using com.pttict.sap.Interface.Model;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class BillingInfoController : BaseController
    {
        string CIP_BILLING_INFO = "CIP BILLING INFO";
        public ActionResult index()
        {
            return RedirectToAction("Create", "BillingInfo");
        }

        [HttpGet]
        public ActionResult Create()
        {
            BillingInfoViewModel model = new BillingInfoViewModel();
            model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(BillingInfoViewModel model)
        {
            try
            {
                string checkBlank = (String.IsNullOrEmpty(model.BillingDate) ? "" : model.BillingDate) +
                                (String.IsNullOrEmpty(model.CreatedDate) ? "" : model.CreatedDate);
                if(checkBlank == "")
                {
                    ViewBag.Error = true;                
                    ViewBag.ErrorMessage = "Please input at lease 1 date type";
                    return View();
                }

                string billingDateFrom = "";
                string billingDateTo = "";
                string createdDateFrom = "";
                string createdDateTo = "";
                string changedDateFrom = "";
                string changedDateTo = "";

                BillingInfoSendServiceConnectorImpl BIService = new BillingInfoSendServiceConnectorImpl();
                BillingInfoService service = new BillingInfoService();
                string jsonBilling = "";
                try
                {
                    DateTime fromDate = DateTime.Now.AddDays(-6);
                    //billingDateFrom = fromDate.Day.ToString() + "/" + fromDate.Month.ToString() + "/" + fromDate.Year.ToString();
                    //billingDateTo = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                    createdDateFrom = fromDate.Day.ToString() + "/" + fromDate.Month.ToString() + "/" + fromDate.Year.ToString();
                    createdDateTo = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                    //changedDateFrom = fromDate.Day.ToString() + "/" + fromDate.Month.ToString() + "/" + fromDate.Year.ToString();
                    //changedDateTo = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                }
                catch
                {
                    ViewBag.Error = true;
                    return View();
                }

                //ConfigModel config = new ConfigModel { sap_user = "ZPI_IF_CIP", sap_pass = "1@zpi_if_cip" };
                ConfigManagement configManagement = new ConfigManagement();
                string jsonConfig = configManagement.getDownstreamConfig("CIP_BILLINGINFO_SEND");
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                //string jsonConfig = javaScriptSerializer.Serialize(config);

                jsonBilling += "{ \"SALES_ORG\": [";
                if (model.Company == "0000")
                {
                    jsonBilling += "{ \"SALES_CODE\": \"1100\", \"NAME\": \"TOP\", \"SHIPPING_POINT\": \"12PI\" }, { \"SALES_CODE\": \"1400\", \"NAME\": \"TLB\", \"SHIPPING_POINT\": \"42PI\" }],";
                }
                else if (model.Company == "1100")
                {
                    jsonBilling += "{ \"SALES_CODE\": \"1100\", \"NAME\": \"TOP\", \"SHIPPING_POINT\": \"12PI\" }],";
                }
                else if (model.Company == "1400")
                {
                    jsonBilling += "{ \"SALES_CODE\": \"1400\", \"NAME\": \"TLB\", \"SHIPPING_POINT\": \"42PI\" }],";
                }

                jsonBilling += "\"DISTRIBUTION_CHANNEL\": \"12\", \"SHIPPING_POINT\": \"*2PI\",";
                jsonBilling += "\"RUNFROMWEB\": \"true\",";

                if ((model.BillingDate != null) && (model.BillingDate.Contains("to")))
                {
                    billingDateFrom = model.BillingDate.Replace(" to ", "|").Split('|')[0];
                    billingDateTo = model.BillingDate.Replace(" to ", "|").Split('|')[1];
                    jsonBilling += "\"BILLING_DATE\": [ { \"FROM\": \"" + billingDateFrom + "\", \"TO\": \"" + billingDateTo + "\" } ],";
                }
                else
                {
                    jsonBilling += "\"BILLING_DATE\": \"\",";
                }

                jsonBilling += "\"CHANGED_DATE\": \"\",";                

                if ((model.CreatedDate != null) && (model.CreatedDate.Contains("to")))
                {
                    createdDateFrom = model.CreatedDate.Replace(" to ", "|").Split('|')[0];
                    createdDateTo = model.CreatedDate.Replace(" to ", "|").Split('|')[1];
                    jsonBilling += "\"CREATED_DATE\": [ { \"FROM\": \"" + createdDateFrom + "\", \"TO\": \"" + createdDateTo + "\" } ]}";
                }
                else
                {
                    jsonBilling += "\"CREATED_DATE\": \"\"}";
                }

                BIService.connect(jsonConfig, jsonBilling);
            }
            catch (Exception ex)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }

            ViewBag.SendToSapCompleted = true;
            return View();
        }

        public BillingInfoViewModel initialModel()
        {
            BillingInfoViewModel model = new BillingInfoViewModel();

            string JsonD = MasterData.GetJsonMasterSetting(CIP_BILLING_INFO);

            return model;
        }


    }
}