﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class BorrowController : BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "Borrow");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            serviceModel.Search(ref pModel);
            BorrowViewModel model = initialModel();
            model.Borrow_Search = pModel.Borrow_Search;
            return View(model);
        }
        
        [HttpGet]
        public ActionResult Search()
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel model = new BorrowViewModel();
            model = initialModel();
            return View("Search", model);
            
        }

        [HttpGet]
        public ActionResult AddEdit(string pTripNo)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel model = initialModel();
            if (String.IsNullOrEmpty(pTripNo))
            {
                //Add new
                model.sBorrowDetail.dTripNo = "";
                model.sBorrowDetail.dPriceStatus = "";
                //Fix
                model.pBorrowDetail.dTripNo = "";
                model.pBorrowDetail.dPriceStatus = "";
                serviceModel.Add(ref model);
            }
            else
            {
                //Edit
                model.sBorrowDetail.dTripNo = pTripNo;
                model.sBorrowDetail.dPriceStatus = "";
                model.pBorrowDetail.dTripNo = pTripNo;
                model.pBorrowDetail.dPriceStatus = "";
                serviceModel.Edit(ref model);
            }

            serviceModel.setInitail_ddl(ref model);
            return View("AddEdit", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddEdit")]
       // public ActionResult AddEdit(FormCollection frm, BorrowViewModel pModel)
        public ActionResult AddEdit()
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel model = initialModel();
            //Add new 
            serviceModel.Add(ref model);
            return View("AddEdit", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Save(FormCollection frm, BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            ReturnValue rtn = new ReturnValue();

            rtn = serviceModel.SaveData(ref pModel);

            if (rtn != null)
            {
                TempData["Borrow"] = pModel;
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
            }
            else
            {
                TempData["Borrow"] = pModel;
                TempData["ReponseMessage"] = null;
                TempData["ReponseStatus"] = null;
            }
            return RedirectToAction("Save", "Borrow");

        }

        [HttpGet]
        public ActionResult Save()
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel model = initialModel();

            if (TempData["Borrow"] != null)
            {
                BorrowViewModel temp_model = (BorrowViewModel)TempData["Borrow"];
                model.pBorrowDetail = temp_model.pBorrowDetail;

                BorrowViewModel temp_models = (BorrowViewModel)TempData["Borrow"];
                model.sBorrowDetail = temp_model.sBorrowDetail;

                TempData.Remove("Borrow");
            }

            return View("AddEdit", model);
        }

        #region "Create SO"

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "CreateChangeSO")]
        public ActionResult CreateChangeSO(FormCollection frm, BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel smodel = new BorrowViewModel();

            //if (!string.IsNullOrEmpty(pModel.Borrow_Search.sTripNo_Select))
            //{
            //    smodel.sBorrowDetail.dTripNo = pModel.Borrow_Search.sTripNo_Select;
            //    smodel.pBorrowDetail.dTripNo = pModel.Borrow_Search.sTripNo_Select;
            //}
            serviceModel.CreateUpateSO(ref pModel);

            serviceModel.setInitail_ddl(ref pModel);

            if (TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
            TempData["tempBorrow"] = pModel;

            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "CancelSO")]
        public ActionResult CancelSO(FormCollection frm, BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel smodel = new BorrowViewModel();

            if (!string.IsNullOrEmpty(pModel.Borrow_Search.sTripNo_Select))
            {
                smodel.sBorrowDetail.dTripNo = pModel.Borrow_Search.sTripNo_Select;
                smodel.pBorrowDetail.dTripNo = pModel.Borrow_Search.sTripNo_Select;
                serviceModel.Edit(ref smodel);
            }
            if (!String.IsNullOrEmpty(smodel.sBorrowDetail.dFIDoc))
            {
                serviceModel.CancelSO(ref pModel);
            }
            serviceModel.setInitail_ddl(ref pModel);

            if (TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
            TempData["tempBorrow"] = pModel;

            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action",Argument = "CreateChangeMEMO")]
        public ActionResult CreateChangeMEMO(FormCollection frm, BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();

            serviceModel.CreateUpdateMEMO(ref pModel);

            serviceModel.setInitail_ddl(ref pModel);

            if(TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
            TempData["tempBorrow"] = pModel;

            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name ="action",Argument ="CancelMEMO")]
        public ActionResult CancelMEMO(FormCollection frm,BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();

            serviceModel.CancelMEMO(ref pModel);

            serviceModel.setInitail_ddl(ref pModel);

            if(TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
            TempData["tempBorrow"] = pModel;

            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name ="action",Argument ="CreateChangePO")]
        public ActionResult CreateChangePO(FormCollection frm,BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();

            serviceModel.CreateUpdatePO(ref pModel);

            serviceModel.setInitail_ddl(ref pModel);

            if(TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
            TempData["tempBorrow"] = pModel;

            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "CancelPO")]
        public ActionResult CancelPO(FormCollection frm,BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();

            serviceModel.CancelPO(ref pModel);

            // BEGIN: FIX MERGE BRANCH CONFIG //
            // serviceModel.CreateSO(ref pModel);
            // END: FIX MERGE BRANCH CONFIG //
            serviceModel.setInitail_ddl(ref pModel);

            if (TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
            TempData["tempBorrow"] = pModel;

            return View("Search", pModel);
        }

        #endregion

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Create")]
        //public ActionResult Create(FormCollection frm, BorrowViewModel pModel)
        //{
        //    BorrowServiceModel serviceModel = new BorrowServiceModel();
        //    if (!string.IsNullOrEmpty(pModel.Borrow_Search.sTripNo_Select))
        //    {
        //        //pModel.selectedTripNo = pModel.SOClearLine_Search.sTripNo_Select;
        //    }

        //    serviceModel.CreateSO(ref pModel);

        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempBorrow"] != null) { TempData.Remove("tempBorrow"); }
        //    TempData["tempBorrow"] = pModel;

        //    return View("Search", pModel);
        //}

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Update")]
        //public ActionResult Update(FormCollection frm, BorrowViewModel pModel)
        //{
        //    BorrowServiceModel serviceModel = new BorrowServiceModel();

        //    var selectedTripNo = frm["SelectedTripNo"].Split(',');
        //    for (var k = 0; k < selectedTripNo.Length - 1; k++)
        //    {
        //        if (!String.IsNullOrEmpty(selectedTripNo[k]))
        //        {
        //            //pModel.selectedTripNo = selectedTripNo[k];
        //            break;
        //        }
        //    }

        //    #region "For Use"
        //    //if (!String.IsNullOrEmpty(pModel.selectedTripNo))
        //    //{
        //    //    if (pModel.SOClearLine_Search.SearchData != null)
        //    //    {
        //    //        var qry = pModel.SOClearLine_Search.SearchData.Where(z => z.dTripNo.ToUpper().Equals(pModel.selectedTripNo.ToUpper()));
        //    //        if (qry != null && !String.IsNullOrEmpty(qry.ToList()[0].dSaleOrder))
        //    //        {
        //    //            serviceModel.UpdateSO(ref pModel);
        //    //        }
        //    //    }
        //    //}
        //    #endregion

        //    //For Test
        //    serviceModel.UpdateSO(ref pModel);


        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Cancel")]
        //public ActionResult Cancel(FormCollection frm, ClearLineViewModel pModel)
        //{
        //    ClearLineServiceModel serviceModel = new ClearLineServiceModel();

        //    var selectedTripNo = frm["SelectedTripNo"].Split(',');
        //    for (var k = 0; k < selectedTripNo.Length - 1; k++)
        //    {
        //        if (!String.IsNullOrEmpty(selectedTripNo[k]))
        //        {
        //            //pModel.selectedTripNo = selectedTripNo[k];
        //            break;
        //        }
        //    }
        //    #region "For Use"
        //    //if (!String.IsNullOrEmpty(pModel.selectedTripNo))
        //    //{
        //    //    if (pModel.SOClearLine_Search.SearchData != null)
        //    //    {
        //    //        var qry = pModel.SOClearLine_Search.SearchData.Where(z => z.dTripNo.ToUpper().Equals(pModel.selectedTripNo.ToUpper()));
        //    //        if (qry != null && !String.IsNullOrEmpty(qry.ToList()[0].dSaleOrder))
        //    //        {
        //    //            serviceModel.CancelSO(ref pModel);
        //    //        }
        //    //    }
        //    //}
        //    #endregion

        //    //For Test
        //    serviceModel.CancelSO(ref pModel);

        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        public BorrowViewModel initialModel()
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel model = new BorrowViewModel();
            try
            {
                model.Borrow_Search = new BorrowViewModel_Search();
                model.Borrow_Search.SearchData = new List<BorrowViewModel_SearchData>();
              
                model.Borrow_Search.sTripNo = "";
                model.pBorrowDetail = new BorrowDetailViewModel();
                model.pBorrowDetail.dTripNo = "";
                model.pBorrowDetail.dPriceStatus = "";

                //Fixsale
                model.sBorrowDetail = new BorrowDetailViewModel();
                model.sBorrowDetail.dTripNo = "";
                model.sBorrowDetail.dPriceStatus = "";

                model.ddl_Crude = BorrowServiceModel.getMaterial();
                model.json_Crude = BorrowServiceModel.GetDataToJSON_TextValue(model.ddl_Crude);

                model.ddl_Customer = BorrowServiceModel.getCustomer();
                model.json_Customer = BorrowServiceModel.GetDataToJSON_TextValue(model.ddl_Customer);

                model.ddl_Vender = BorrowServiceModel.getVendor("CPAI", "CPAI");
                model.json_Vendor = BorrowServiceModel.GetDataToJSON_TextValue(model.ddl_Vender);

                model.ddl_SaleOrg = new List<SelectListItem>();
                model.ddl_SaleOrg = BorrowServiceModel.getCompany();

                model.ddl_Plant = new List<SelectListItem>();
                model.ddl_Plant = BorrowServiceModel.getPlant("1100,1400");

                model.ddl_Unit = new List<SelectListItem>();
                model.ddl_Unit = BorrowServiceModel.getUnit();

                model.ddl_UnitPrice = new List<SelectListItem>();
                model.ddl_UnitPrice = BorrowServiceModel.getUnitPrice();

                model.ddl_UnitTotal = new List<SelectListItem>();
                model.ddl_UnitTotal = BorrowServiceModel.getUnitTotal();

                model.ddl_PriceStatus = new List<SelectListItem>();
                model.ddl_PriceStatus = ClearLineServiceModel.getPriceStatus();

                model.ddl_UnitINV = new List<SelectListItem>();
                model.ddl_UnitINV = BorrowServiceModel.getUnitINV();
               
                


            }
            catch (Exception ex)
            {

            }
          

            return model;
        }

        [HttpGet]
        public ActionResult SearchReverse()
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            BorrowViewModel_Search SearchModel = new BorrowViewModel_Search();
            BorrowViewModel model = new BorrowViewModel();
            model = initialModel();
            return View("SearchReverse", model);
        }

        [HttpPost]
        public ActionResult SearchReverse(BorrowViewModel pModel)
        {
            BorrowServiceModel serviceModel = new BorrowServiceModel();
            pModel.Borrow_Search.SearchData = new List<BorrowViewModel_SearchData>();
            serviceModel.SearchFIDoc(ref pModel);
            List<BorrowViewModel_SearchData> resultGroup = new List<BorrowViewModel_SearchData>();
            foreach (var item in pModel.Borrow_Search.SearchData)
            {
                if (item != null)
                {
                    var trip_no = item.aTripNo;
                    var crude_type = item.aCrudeType;
                    if (!resultGroup.Where(i => i.aTripNo == trip_no && i.aCrudeType == crude_type).Any())
                    {
                        resultGroup.Add(item);
                    }
                }
                else
                {
                    resultGroup = null;
                }                
            }
            pModel.Borrow_Search.SearchData = resultGroup;
            BorrowViewModel model = initialModel();
            model.Borrow_Search = pModel.Borrow_Search;

            return View(model);
        }
    }
}
