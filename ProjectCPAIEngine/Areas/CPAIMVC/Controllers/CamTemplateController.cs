﻿using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CamTemplateController : BaseController
    {
        // GET: CPAIMVC/CamTemplate
        public ActionResult Index()
        {
            return RedirectToAction("Search", "CamTemplate");
        }

        [HttpGet]
        public ActionResult Search()
        {
            CamTemplateViewModel model = initialModel();
            return View(model);
        }
        
        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SearchResult")]
        public ActionResult SearchResult(CamTemplateViewModel postModel)
        {
            CamTemplateViewModel model = initialModel();

            CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();
            CamTemplateViewModel_Search searchModel = new CamTemplateViewModel_Search();

            searchModel = postModel.CamTemplate_Search;
            serviceModel.Search(ref searchModel);
            model.CamTemplate_Search = searchModel;
            return View("Search", model);
        }

        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "DeleteResult")]
        public ActionResult DeleteResult(CamTemplateViewModel viewModel)
        {
            if (ViewBag.action_edit)
            {
                CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();

                ReturnValue rtn = new ReturnValue();
                rtn = serviceModel.Delete(viewModel.CamTemplate_Detail, lbUserName);

                CamTemplateViewModel model = initialModel();
                CamTemplateViewModel_Search searchModel = new CamTemplateViewModel_Search();

                searchModel = viewModel.CamTemplate_Search;
                serviceModel.Search(ref searchModel);
                model.CamTemplate_Search = searchModel;

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                return View("Search", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                CamTemplateViewModel model = initialModel();
                model.CamTemplate_Detail.CamCode = "";
               // model.ddl_Assay = DropdownServiceModel.getAllApprovedCrudeReference();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }

        }


        [HttpPost, ValidateInput(false)]
        public ActionResult Create(FormCollection frm, CamTemplateViewModel viewModel)
        {
            if (ViewBag.action_create)
            {
                CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Add(viewModel.CamTemplate_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                CamTemplateViewModel modelDropdownSet = initialModel();
                modelDropdownSet.CamTemplate_Detail = viewModel.CamTemplate_Detail;
                return View(modelDropdownSet);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }
        }


        [HttpGet]
        public ActionResult Edit(string CamCode)
        {
            if (ViewBag.action_edit)
            {
                CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();

                CamTemplateViewModel model = initialModel();
                model.CamTemplate_Detail = serviceModel.Get(CamCode);

                if (model.CamTemplate_Detail.JSON.Compared_Ranking == null)
                    model.CamTemplate_Detail.JSON.Compared_Ranking = new CamTemplateViewModel_Compared_Spec();


               
                 model.ddl_Assay = DropdownServiceModel.getAllApprovedCrudeReference();

                TempData["CamCode"] = CamCode;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }
        }


        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(string CamCode, CamTemplateViewModel viewModel)
        {
            if (ViewBag.action_edit)
            {
                CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();

                ReturnValue rtn = new ReturnValue();

                if (serviceModel.IsUsed(CamCode))
                {
                    rtn = serviceModel.Add(viewModel.CamTemplate_Detail, lbUserName);
                    TempData["CamCode"] = CamCode;
                }
                else
                {
                    rtn = serviceModel.Edit(viewModel.CamTemplate_Detail, lbUserName);
                    TempData["CamCode"] = rtn.newID;
                }
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                CamTemplateViewModel model = initialModel();
                model.CamTemplate_Detail = viewModel.CamTemplate_Detail;
                model.CamTemplate_Detail.CamCode = string.IsNullOrEmpty(rtn.newID) ? CamCode : rtn.newID;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }

        }

        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Delete")]
        public ActionResult Delete(string CamCode, CamTemplateViewModel viewModel)
        {
            if (ViewBag.action_edit)
            {
                CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();

                ReturnValue rtn = new ReturnValue();
                rtn = serviceModel.Delete(viewModel.CamTemplate_Detail, lbUserName);
                if (serviceModel.IsUsed(CamCode))
                {
                    TempData["CamCode"] = CamCode;
                }
                else
                {
                    TempData["CamCode"] = rtn.newID;
                }
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                CamTemplateViewModel model = initialModel();
                model.CamTemplate_Detail = viewModel.CamTemplate_Detail;
                model.CamTemplate_Detail.CamCode = string.IsNullOrEmpty(rtn.newID) ? CamCode : rtn.newID;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }

        }


        public ActionResult ViewDetail(string CamCode)
        {
            if (ViewBag.action_view)
            {
                CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();

                CamTemplateViewModel model = initialModel();
                model.CamTemplate_Detail = serviceModel.Get(CamCode);
                TempData["CamCode"] = CamCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }

        }


        public CamTemplateViewModel initialModel()
        {
            CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();
            CamTemplateViewModel model = new CamTemplateViewModel();

            model.CamTemplate_Search = new CamTemplateViewModel_Search();
            model.CamTemplate_Search.sSearchData = new List<CamTemplateViewModel_SearchData>();

            //model.ddl_country = DropdownServiceModel.getCountryCrudePurchase();
            //model.ddl_crude = MaterialsServiceModel.getMaterialDDL();
            model.ddl_country = DropdownServiceModel.getCountryByCrudePurchase();
            model.ddl_crude = MaterialsServiceModel.getMaterialsDDL();
            model.ddl_Assay = DropdownServiceModel.getCrudeReference();
            model.ddl_data_field = new List<SelectListItem>();
            model.ddl_data_field.Add(new SelectListItem { Text = "Crude Name", Value = "Crude Name" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Country", Value = "Country" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Assay Reference", Value = "Assay Reference" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Categories", Value = "Categories" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Kerogen", Value = "Kerogen" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Characteristic", Value = "Characteristic" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Maturity", Value = "Maturity" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Fouling Possibility", Value = "Fouling Possibility" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Value from Spiral", Value = "Value from Spiral" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Value from Excel", Value = "Value from Excel" });
            model.ddl_data_field.Add(new SelectListItem { Text = "Formula from Excel", Value = "Formula from Excel" });

            model.CamTemplate_Detail = new CamTemplateViewModel_Detail();
            model.CamTemplate_Detail.JSON = new CamTemplateViewModel_JSON();
            model.CamTemplate_Detail.JSON.Config = new List<CamTemplateViewModel_Config>();
            model.CamTemplate_Detail.JSON.Config.Add(new CamTemplateViewModel_Config());
            model.CamTemplate_Detail.JSON.ColumnWidth = new List<CamTemplateViewModel_ColumnWidth>();
            model.CamTemplate_Detail.JSON.ColumnWidth.Add(new CamTemplateViewModel_ColumnWidth());
            model.CamTemplate_Detail.JSON.Reference = new List<CamTemplateViewModel_Ref>();
            model.CamTemplate_Detail.JSON.Reference.Add(new CamTemplateViewModel_Ref());
            model.CamTemplate_Detail.JSON.Fix = new List<CamTemplateViewModel_Fix>();
            model.CamTemplate_Detail.JSON.Fix.Add(new CamTemplateViewModel_Fix());
            model.CamTemplate_Detail.JSON.Spiral = new List<CamTemplateViewModel_Spiral>();
            //model.CamTemplate_Detail.JSON.Spiral.Add(new CamTemplateViewModel_Spiral());

            model.CamTemplate_Detail.JSON.Spiral.Add(new CamTemplateViewModel_Spiral() { ObjProperty = new objProperty() { Property = new List<Property>() } });

            model.CamTemplate_Detail.JSON.Ranking = new CamTemplateViewModel_Spec();
            model.CamTemplate_Detail.JSON.Compared_Ranking = new CamTemplateViewModel_Compared_Spec();
            model.CamTemplate_Detail.JSON.Data = new List<CamTemplateViewModel_Data>();
            model.CamTemplate_Detail.JSON.Data.Add(new CamTemplateViewModel_Data());
            return model;
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Preview")]
        public ActionResult Preview(FormCollection form, CamTemplateViewModel model)
        {
            var fileName = PreviewCampTemp(model);
            return View("Preview", "", fileName);
        }

        //public ActionResult Preview(string campTempID)
        //{
        //    var fileName = PreviewCampTemp(campTempID);
        //    return View("Preview","",fileName);
        //}

        public string PreviewCampTemp(CamTemplateViewModel campTemp, bool isDownload = true)
        {

            string file_name = string.Format("CAM_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "COOL", file_name);
            CamTemplateServiceModel serviceModel = new CamTemplateServiceModel();
            List<CAMTemplate> templates = serviceModel.getCAMTemplate(campTemp.CamTemplate_Detail);
            if (templates != null)
            {
                FileInfo excelFile = new FileInfo(file_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("CAM Template");


                    foreach (var template in templates)
                    {
                        // SET PANEL FREEZE
                        if (!string.IsNullOrEmpty(template.freeze_col) && !string.IsNullOrEmpty(template.freeze_row))
                        {
                            int rowNumber = int.Parse(template.freeze_row);
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.freeze_col);
                            ws.View.FreezePanes(rowNumber, columnNumber);
                        }

                        //SET COLUMN STYLES
                        if (template.column != null && template.row == null)
                        {
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.column);

                            // SET COLUMN WIDTH
                            if (!string.IsNullOrEmpty(template.width))
                            {
                                double columnWidth = double.Parse(template.width);
                                ws.Column(columnNumber).Width = columnWidth;
                            }

                            // SET COLUMN HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Column(columnNumber).Hidden = template.hidden == "Y" ? true : false;
                            }
                        }
                        //SET ROW STYLES
                        else if (template.column == null && template.row != null)
                        {
                            int rowNumber = int.Parse(template.row);

                            // SET ROW HEIGHT 
                            if (!string.IsNullOrEmpty(template.row))
                            {
                                double rowHeight = 21; //double.Parse(template.height);
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }

                            // SET ROW HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                //ws.Row(rowNumber).Hidden = template.hidden == "Y" ? true : false;
                                double rowHeight = 0;
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }
                        }
                        else if (template.column != null && template.row != null)
                        {
                            string[] Columns = template.column.Split('|');
                            string[] Rows = template.row.Split('|');
                            string Cell = Columns.First() + Rows.First() + ':' + Columns.Last() + Rows.Last();
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(Columns.First());
                            int rowNumber = int.Parse(Rows.First());

                            #region
                            if (!string.IsNullOrEmpty(template.value))
                            {
                                ws.Cells[Cell].Value = template.value;
                            }
                            // SET CELL MERGE // 
                            if (!string.IsNullOrEmpty(template.merge)) //if ((!string.IsNullOrEmpty(Columns.Last()) || !string.IsNullOrEmpty(Rows.Last())) && !string.IsNullOrEmpty(template.merge))
                            {
                                ws.Cells[Cell].Merge = template.merge == "Y" ? true : false;
                            }

                            // SET CELL BACKGROUND
                            if (!string.IsNullOrEmpty(template.bg_color))
                            {
                                ws.Cells[Cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells[Cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(template.bg_color));
                            }

                            // SET CELL BORDER
                            if (!string.IsNullOrEmpty(template.border_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                if (!string.IsNullOrEmpty(template.border_color) && template.border_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                }

                            }
                            // SET CELL TEXT ROTATION //
                            if (!string.IsNullOrEmpty(template.alignment))
                            {
                                if (template.alignment == "V")
                                    ws.Cells[Cell].Style.TextRotation = 90;
                            }

                            // SET CELL FONT BOLD
                            if (!string.IsNullOrEmpty(template.font_bold))
                            {
                                ws.Cells[Cell].Style.Font.Bold = template.font_bold == "Y" ? true : false;
                            }

                            // SET CELL FONT COLOR
                            if (!string.IsNullOrEmpty(template.font_color))
                            {
                                ws.Cells[Cell].Style.Font.Color.SetColor(ColorTranslator.FromHtml(template.font_color));
                            }

                            // SET CELL FONT SIZE
                            if (!string.IsNullOrEmpty(template.font_size))
                            {
                                ws.Cells[Cell].Style.Font.Size = float.Parse(template.font_size);
                            }

                            // SET CELL NUMBER FORMAT //
                            if (!string.IsNullOrEmpty(template.number_format))
                            {
                                ws.Cells[Cell].Style.Numberformat.Format = template.number_format;
                            }

                            // SET CELL INDENT //
                            if (!string.IsNullOrEmpty(template.indent))
                            {
                                if (template.indent == "Y")
                                    ws.Cells[Cell].Value = "   -" + ws.Cells[Cell].Value;
                            }

                            // SET CELL CENTER
                            if (!string.IsNullOrEmpty(template.center))
                            {
                                if (template.center == "Y")
                                {
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }

                            // SET CELL FONT ITALIC
                            if (!string.IsNullOrEmpty(template.font_italic))
                            {
                                ws.Cells[Cell].Style.Font.Italic = template.font_italic == "Y" ? true : false;
                            }

                            // SET CELL FONT UNDERLINE
                            if (!string.IsNullOrEmpty(template.font_underline))
                            {
                                ws.Cells[Cell].Style.Font.UnderLine = template.font_underline == "Y" ? true : false;
                            }

                            // SET CELL FONT TEXTWRAP
                            if (!string.IsNullOrEmpty(template.font_textwrap))
                            {
                                ws.Cells[Cell].Style.WrapText = template.font_textwrap == "Y" ? true : false;
                            }

                            // SET CELL FONT HORIZONTAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_horizontal_alignment))
                            {
                                if (template.font_horizontal_alignment == "L")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                else if (template.font_horizontal_alignment == "C")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                else if (template.font_horizontal_alignment == "R")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            }

                            // SET CELL FONT VERTICAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_vertical_alignment))
                            {
                                if (template.font_vertical_alignment == "T")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                else if (template.font_vertical_alignment == "M")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                else if (template.font_vertical_alignment == "B")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                            }

                            // SET CELL BORDER TOP STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_top_style))
                            {
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_top_style);
                                if (!string.IsNullOrEmpty(template.border_top_color) && template.border_top_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_top_color));
                                }
                            }

                            // SET CELL BORDER BOTTOM STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_bottom_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_bottom_style);
                                if (!string.IsNullOrEmpty(template.border_bottom_color) && template.border_bottom_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_bottom_color));
                                }
                            }

                            // SET CELL BORDER LEFT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_left_style))
                            {
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_left_style);
                                if (!string.IsNullOrEmpty(template.border_left_color) && template.border_left_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_left_color));
                                }
                            }

                            // SET CELL BORDER RIGHT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_right_style))
                            {
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_right_style);
                                if (!string.IsNullOrEmpty(template.border_right_color) && template.border_right_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_right_color));
                                }
                            }
                            #endregion
                        }


                    }
                    package.Save();
                    //if (isDownload)
                    //{
                    //    Response.Clear();
                    //    Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
                    //    Response.AddHeader("content-type", "application/Excel");
                    //    Response.WriteFile(file_path);
                    //    Response.End();
                    //}

                    return file_name;

                }
            }
            return "";
        }

        [HttpGet]
        public JsonResult GetAssayRef(string crude, string country,string assayref)
        {
            List<SelectListItem> obgcity = DropdownServiceModel.getAllApprovedCrudeReference(country, crude, assayref);
            return Json(obgcity, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult GetCountryRef(string crude)
        {
            List<SelectListItem> objcountry = DropdownServiceModel.getCountryByCrudePurchase(crude);
            return Json(objcountry, JsonRequestBehavior.AllowGet);
        }
    }
}