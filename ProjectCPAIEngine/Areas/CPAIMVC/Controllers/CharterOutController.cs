﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using Newtonsoft.Json;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CharterOutController : BaseController
    {
        public List<ButtonAction> ButtonListCharterOut
        {
            get
            {
                if (Session["ButtonListCharterOut"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListCharterOut"];
            }
            set { Session["ButtonListCharterOut"] = value; }
        }
        
        ShareFn _FN = new ShareFn();
        public string MCCTypeSS
        {
            get
            {
                string MCC = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
                return MCC;
            }
            set
            {
                Session["Charter_Type"] = value;
            }
        }
        const string returnPage = "../web/MainBoards.aspx";

        public ActionResult Index()
        {
            CharterOutViewModel model;
            if (TempData["CharterOut_Model"] == null)
            {
                model = new CharterOutViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                MCCTypeSS = ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt(); //CMCS FOR SURE
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                    MCCTypeSS = Request.QueryString["Type"].ToString();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCharterOut(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }
                    //if (Request.QueryString["PURNO"] != null && Request.QueryString["isDup"] == null)
                    //{
                    //    ViewBag.PURNO = Request.QueryString["PURNO"].ToString().Decrypt();
                    //}
                    if (Request.QueryString["Reason"] != null)
                    {
                        model.chos_reason = Request.QueryString["Reason"].ToString().Decrypt();
                    }

                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = returnPage;
                    }
                }
            } else
            {
                model = TempData["CharterOut_Model"] as CharterOutViewModel;
                model.chos_reason = TempData["CharterOut_Reason"] as string;
                TempData["CharterOut_Model"] = null;
                TempData["CharterOut_Reason"] = null;
            }

            initializeDropDownList(ref model);

            return View(model);
        }

        [HttpGet]
        public ActionResult CharterOutCMMT_Old()
        {
            CharterOutViewModel model;
            if (TempData["CharterOut_Model"] == null)
            {
                model = new CharterOutViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                MCCTypeSS = ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt(); //CMMT FOR SURE
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                    MCCTypeSS = Request.QueryString["Type"].ToString();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCharterOut(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }
                    //if (Request.QueryString["PURNO"] != null && Request.QueryString["isDup"] == null)
                    //{
                    //    ViewBag.PURNO = Request.QueryString["PURNO"].ToString().Decrypt();
                    //}

                    if (Request.QueryString["Reason"] != null)
                    {
                        if (Request.QueryString["Reason"] != null)
                        {
                            model.chot_reason = Request.QueryString["Reason"].ToString().Decrypt();
                        }
                    }

                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = "../" + returnPage;
                    }
                }
                else
                {
                    model.chot_proposed_for_approve = new ChotProposedForApprove();
                    model.chot_proposed_for_approve.charter_party_form = "Shell Voy 5";
                    model.chot_proposed_for_approve.laytime = "120 hrs";
                }
            } else
            {
                model = TempData["CharterOut_Model"] as CharterOutViewModel;
                model.chot_reason = TempData["CharterOut_Reason"] as string;
                TempData["CharterOut_Model"] = null;
                TempData["CharterOut_Reason"] = null;
            }

            initializeDropDownList(ref model);

            return View(model);
        }

        [HttpGet]
        public ActionResult CharterOutCMMT()
        {
            CharterOutViewModel model;
            if (TempData["CharterOut_Model"] == null)
            {
                model = new CharterOutViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                MCCTypeSS = ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt(); //CMMT FOR SURE
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                    MCCTypeSS = Request.QueryString["Type"].ToString();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCharterOut(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    } else
                    {
                        if (!CharterOutServiceModel.getDupFreight(tranID))
                        {
                            model.freight_id = "";
                            //model.chos_charter_out_case_b = null;
                            model.chos_charter_out_case_b = new ChosCharterOutCaseB();
                            model.chot_charter_out_case_b.chot_est_income_b_one = new ChotEstIncomeBOne();
                            model.chot_charter_out_case_b.chot_est_expense_b_two = new ChotEstExpenseBTwo();
                            model.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one = new ChotEstExpenseBTwoPointOne();
                            model.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two = new ChotEstExpenseBTwoPointTwo();

                            model.chot_summary = new ChotSummary();
                            model.chot_result = new ChotResult();
                            //model.chot_result.chot_d_result = new ChotDResult();

                            model.chot_result.chot_saving = new ChotSaving();
                            model.chot_result.chot_profit = new ChotProfit();
                        }
                    }

                    //if (Request.QueryString["Reason"] != null)
                    //{
                    //    model.chot_reason = Request.QueryString["Reason"].ToString().Decrypt();
                    //}

                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = "../" + returnPage;
                    }
                }
                else
                {
                    if (Request.QueryString["FreightNo"] != null)
                    {
                        model.freight_id = Request.QueryString["FreightNo"].ToString().Decrypt();
                    }
                    model.chot_no_charter_out_case_a = new ChotNoCharterOutCaseA();
                    model.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs = new List<ChotCharterOutCaseAOtherCost>();
                    model.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs.Add(new ChotCharterOutCaseAOtherCost());
                    model.chot_charter_out_case_b = new ChotCharterOutCaseB();
                    model.chot_charter_out_case_b.chot_est_income_b_one = new ChotEstIncomeBOne();
                    model.chot_charter_out_case_b.chot_est_expense_b_two = new ChotEstExpenseBTwo();
                    model.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one = new ChotEstExpenseBTwoPointOne();
                    model.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two = new ChotEstExpenseBTwoPointTwo();
                    model.chot_summary = new ChotSummary();
                    model.chot_result = new ChotResult();
                    //model.chot_result.chot_d_result = new ChotDResult();
                    model.chot_result.chot_profit = new ChotProfit();
                    model.chot_result.chot_saving = new ChotSaving();
                }
            }
            else
            {
                model = TempData["CharterOut_Model"] as CharterOutViewModel;
                model.chot_reason = TempData["CharterOut_Reason"] as string;
                TempData["CharterOut_Model"] = null;
                TempData["CharterOut_Reason"] = null;
            }
            initializeDropDownList(ref model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, CharterOutViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt();

            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            string attached_explain = form["hdfExplanFileUpload"];
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            this.updateModel(form, model);

            ResponseData resData;
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE)
            {
                resData = SaveDataCharterOut(_status, model, note, json_uploadFile, attached_explain);
            }
            else
            {
                resData = SaveDataCharterOutCMMT(_status, model, note, json_uploadFile, attached_explain);
            }

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);
            if (resData != null && (resData.result_code == "1" || resData.result_code == "2"))
            {
                string tranID = resData.transaction_id.Encrypt();
                string reqID = resData.req_transaction_id.Encrypt();
                //string purno = resData.resp_parameters[0].v.Encrypt();
                string reason = MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE ? model.chos_reason.Encrypt() : model.chot_reason.Encrypt();
                string path = string.Format("~/CPAIMVC/CharterOut?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) path = string.Format("~/CPAIMVC/CharterOut/CharterOutCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);

                return Redirect(path);
            }
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                return View("CharterOutCMMT", model);
            }
            else
            {
                return View("Index", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, CharterOutViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt();

            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            string attached_explain = form["hdfExplanFileUpload"];
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            this.updateModel(form, model);

            ResponseData resData;
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE)
            {
                resData = SaveDataCharterOut(_status, model, note, json_uploadFile, attached_explain);
            }
            else
            {
                resData = SaveDataCharterOutCMMT(_status, model, note, json_uploadFile, attached_explain);
            }

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null && (resData.result_code == "1" || resData.result_code == "2"))
            {
                string tranID = resData.transaction_id.Encrypt();
                string reqID = resData.req_transaction_id.Encrypt();
                //string purno = resData.resp_parameters[0].v.Encrypt();
                string reason = MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE ? model.chos_reason.Encrypt() : model.chot_reason.Encrypt();
                string path = string.Format("~/CPAIMVC/CharterOut?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) path = string.Format("~/CPAIMVC/CharterOut/CharterOutCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);


                return Redirect(path);
            }
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                return View("CharterOutCMMT", model);
            }
            else
            {
                return View("Index", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenPDF")]
        public ActionResult GenPDF(FormCollection form, CharterOutViewModel model)
        {
            string path = "";
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt();
            if (type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                path = "~/Web/Report/CharteringOutCMMTReport.aspx";
            }
            else
            {
                path = "~/Web/Report/CharteringOutReport.aspx";
            }
            if (Request.QueryString["TranID"] != null && Request.QueryString["isDup"] == null)
            {
                string tranID = Request.QueryString["TranID"].ToString();
                //LoadDataCharterOut(tranID, ref model);
                path += "?TranID=" + tranID;
            } else
            {
                this.updateModel(form, model);

                if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                {
                    ChotObjectRoot obj = new ChotObjectRoot();
                    obj.vessel_id = model.chot_vessel;
                    obj.vessel_name = model.vessel_name;
                    obj.freight_id = model.freight_id;
                    obj.freight_no = model.freight_no;
                    obj.chot_date = model.chot_date;
                    obj.chot_reason = model.chot_reason;
                    obj.chot_no_charter_out_case_a = model.chot_no_charter_out_case_a;
                    obj.chot_charter_out_case_b = model.chot_charter_out_case_b;
                    obj.chot_summary = model.chot_summary;
                    obj.chot_result = model.chot_result;
                    obj.chot_note = model.chot_note;
                    obj.purchase_no = model.pur_no;
                    obj.chot_reason_explanation = model.chot_reason_explanation;
                    obj.approve_items = model.approve_items;
                    obj.explanationAttach = form["hdfExplanFileUpload"].ToString();
                    //var json = new JavaScriptSerializer().Serialize(obj);
                    Session["chot_object_root"] = obj;
                }
                else
                {
                    ChosRootObject obj = new ChosRootObject();
                    obj.chos_benefit = model.chos_benefit;
                    obj.chos_chartering_method = model.chos_chartering_method;
                    obj.chos_charter_out_case_b = model.chos_charter_out_case_b;
                    obj.chos_evaluating = model.chos_evaluating;
                    obj.chos_no_charter_out_case_a = model.chos_no_charter_out_case_a;
                    obj.chos_proposed_for_approve = model.chos_proposed_for_approve;
                    obj.chos_reason = model.chos_reason;
                    obj.chos_note = model.chos_note;
                    obj.approve_items = model.approve_items;
                    obj.explanationAttach = form["hdfExplanFileUpload"].ToString();
                    //var json = new JavaScriptSerializer().Serialize(obj);
                    Session["ChosRootObject"] = obj;
                }
            }

            return Redirect(path);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, CharterOutViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
            this.updateModel(form, model);
            if (ButtonListCharterOut != null)
            {
                var _lstButton = ButtonListCharterOut.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    //string note = _lstButton[0].name.ToUpper() == "REJECT" || _lstButton[0].name.ToUpper() == "CANCEL" ? form["hdfNoteAction"] : "-";
                    string temp = form["hdfNoteAction"];
                    string note = string.IsNullOrEmpty(temp) ? "-" : temp;

                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    model.explanationAttach = form["hdfExplanFileUpload"];
                    string json;
                    if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                    {
                        ChotObjectRoot obj = new ChotObjectRoot();
                        obj.vessel_id = model.chot_vessel;
                        obj.vessel_name = VehicleServiceModel.GetName(model.chot_vessel);
                        obj.freight_id = model.freight_id;
                        obj.freight_no = model.chot_fcl_no;
                        obj.chot_date = model.chot_date;
                        obj.chot_reason = model.chot_reason;
                        obj.chot_note = model.chot_note;
                        obj.chot_no_charter_out_case_a = model.chot_no_charter_out_case_a;
                        obj.chot_charter_out_case_b = model.chot_charter_out_case_b;
                        obj.chot_summary = model.chot_summary;
                        obj.chot_summary.charterer_name = MT_CUST_DETAIL_DAL.GetCUSTDetailName(model.chot_summary.charterer);
                        obj.chot_result = model.chot_result;
                        obj.purchase_no = model.pur_no;
                        obj.chot_reason_explanation = model.chot_reason_explanation;
                        obj.approve_items = model.approve_items;
                        obj.explanationAttach = model.explanationAttach;
                        json = new JavaScriptSerializer().Serialize(obj);

                        ReturnValue rtn = new ReturnValue();
                        CharterOutServiceModel charterOutService = new CharterOutServiceModel();
                        rtn = charterOutService.AddCharterNoteCMMT(model, lbUserName, model.taskID);
                        if (rtn.Status == false)
                        {
                            TempData["res_message"] = rtn.Message;
                            initializeDropDownList(ref model);
                            TempData["CharterOut_Model"] = model;
                            return RedirectToAction("CharterOutCMMT", "CharterOut");
                        }
                    }
                    else
                    {
                        ChosRootObject obj = new ChosRootObject();
                        obj.chos_benefit = model.chos_benefit;
                        obj.chos_chartering_method = model.chos_chartering_method;
                        obj.chos_charter_out_case_b = model.chos_charter_out_case_b;
                        obj.chos_evaluating = model.chos_evaluating;
                        obj.chos_no_charter_out_case_a = model.chos_no_charter_out_case_a;
                        obj.chos_proposed_for_approve = model.chos_proposed_for_approve;
                        obj.chos_reason = model.chos_reason;
                        obj.chos_note = model.chos_note;
                        obj.approve_items = model.approve_items;
                        obj.explanationAttach = model.explanationAttach;
                        json = new JavaScriptSerializer().Serialize(obj);

                        ReturnValue rtn = new ReturnValue();
                        CharterOutServiceModel charterOutService = new CharterOutServiceModel();
                        rtn = charterOutService.AddCharterNoteCMCS(model, lbUserName, model.taskID);
                        if (rtn.Status == false)
                        {
                            TempData["res_message"] = rtn.Message;
                            initializeDropDownList(ref model);
                            if (model.chot_evaluating != null)
                            {
                                if (!string.IsNullOrEmpty(model.chot_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chot_proposed_for_approve.laycan_to))
                                {
                                    model.ev_tempLaycan = model.chot_proposed_for_approve.laycan_from + " to " + model.chot_proposed_for_approve.laycan_to;
                                }
                            }
                            if (model.chos_evaluating != null)
                            {
                                if (!string.IsNullOrEmpty(model.chos_evaluating.period_date_from) && !string.IsNullOrEmpty(model.chos_evaluating.period_date_to))
                                {
                                    model.chos_evaluating.period_date_from = model.chos_evaluating.period_date_from + " to " + model.chos_evaluating.period_date_to;
                                }
                            }

                            //Assign A, B1, B2 order and value
                            if (model.chos_no_charter_out_case_a != null)
                            {
                                //A
                                if (model.chos_no_charter_out_case_a.chos_est_expense != null)
                                {
                                    for (int i = 0; i < model.chos_no_charter_out_case_a.chos_est_expense.Count; i++)
                                    {
                                        if (!string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from) && !string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to))
                                        {
                                            model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from = model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from + " to " + model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to;
                                        }
                                    }
                                }
                            }
                            if (model.chos_charter_out_case_b != null)
                            {
                                //B1
                                if (model.chos_charter_out_case_b.chos_charter_out_case_b_one != null)
                                {
                                    for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_one.Count; i++)
                                    {
                                        if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to))
                                        {
                                            model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to;
                                        }
                                    }
                                }
                                //B2
                                if (model.chos_charter_out_case_b.chos_charter_out_case_b_two != null)
                                {
                                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt != null)
                                    {
                                        if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to))
                                        {
                                            model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to;
                                        }

                                        for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel.Count(); i++)
                                        {

                                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to))
                                            {
                                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to;
                                            }
                                        }
                                    }


                                }
                            }

                            if (model.chos_proposed_for_approve != null)
                            {
                                if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.laycan_to))
                                {
                                    model.pa_tempLaycan = model.chos_proposed_for_approve.laycan_from + " to " + model.chos_proposed_for_approve.laycan_to;
                                }

                                if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_to))
                                {
                                    model.chos_proposed_for_approve.a_date_from = model.chos_proposed_for_approve.a_date_from + " to " + model.chos_proposed_for_approve.a_date_to;
                                }
                                if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_to))
                                {
                                    model.chos_proposed_for_approve.b_date_from = model.chos_proposed_for_approve.b_date_from + " to " + model.chos_proposed_for_approve.b_date_to;
                                }
                            }
                            TempData["CharterOut_Model"] = model;
                            return RedirectToAction("Index", "CharterOut");
                        }
                    }
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            //string purno = resData.resp_parameters[0].v.Encrypt();
                            string reason = MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE ? model.chos_reason.Encrypt() : model.chot_reason.Encrypt();
                            string path = string.Format("~/CPAIMVC/CharterOut?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                            {
                                path = string.Format("~/CPAIMVC/CharterOut/CharterOutCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                            }

                            return Redirect(path);
                        }
                    }

                    //if (resData != null)
                    //{
                    //    if (resData.result_code == "1" || resData.result_code == "2")
                    //    {
                    //        string tranID = resData.transaction_id.Encrypt();
                    //        string reqID = resData.req_transaction_id.Encrypt();
                    //        string purno = resData.resp_parameters[0].v.Encrypt();
                    //        string reason = model.chos_reason.Encrypt();
                    //        string path = string.Format("~/CPAIMVC/CharterOut?TranID={0}&Tran_Req_ID={1}&Reason={2}", tranID, reqID, reason);
                    //        // string path = string.Format("~/CPAIMVC/CharterOut?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Reason={3}", tranID, reqID, purno, reason);
                    //        return Redirect(path);
                    //    }
                    //}


                }
            } else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            initializeDropDownList(ref model);
            if (type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                TempData["CharterOut_Model"] = model;
                return RedirectToAction("CharterOutCMMT", "CharterOut");
            }
            else
            {
                if (model.chot_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chot_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chot_proposed_for_approve.laycan_to))
                    {
                        model.ev_tempLaycan = model.chot_proposed_for_approve.laycan_from + " to " + model.chot_proposed_for_approve.laycan_to;
                    }
                }
                if (model.chos_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chos_evaluating.period_date_from) && !string.IsNullOrEmpty(model.chos_evaluating.period_date_to))
                    {
                        model.chos_evaluating.period_date_from = model.chos_evaluating.period_date_from + " to " + model.chos_evaluating.period_date_to;
                    }
                }

                //Assign A, B1, B2 order and value
                if (model.chos_no_charter_out_case_a != null)
                {
                    //A
                    if (model.chos_no_charter_out_case_a.chos_est_expense != null)
                    {
                        for (int i = 0; i < model.chos_no_charter_out_case_a.chos_est_expense.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from) && !string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to))
                            {
                                model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from = model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from + " to " + model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to;
                            }
                        }
                    }
                }
                if (model.chos_charter_out_case_b != null)
                {
                    //B1
                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_one != null)
                    {
                        for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_one.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to))
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to;
                            }
                        }
                    }
                    //B2
                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_two != null)
                    {
                        if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt != null)
                        {
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to))
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to;
                            }

                            for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel.Count(); i++) {

                                if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to))
                                {
                                    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to;
                                }
                            }
                        }

                        
                    }
                }

                if (model.chos_proposed_for_approve != null)
                {
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chos_proposed_for_approve.laycan_from + " to " + model.chos_proposed_for_approve.laycan_to;
                    }

                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_to))
                    {
                        model.chos_proposed_for_approve.a_date_from = model.chos_proposed_for_approve.a_date_from + " to " + model.chos_proposed_for_approve.a_date_to;
                    }
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_to))
                    {
                        model.chos_proposed_for_approve.b_date_from = model.chos_proposed_for_approve.b_date_from + " to " + model.chos_proposed_for_approve.b_date_to;
                    }
                }
                TempData["CharterOut_Model"] = model;
                return RedirectToAction("Index", "CharterOut");
            }
        }

        [HttpPost]
        public JsonResult getPaymentTermDetailByCharterer(string broker)
        {
            EstimateFreightServiceModel ef = new EstimateFreightServiceModel();
            MT_CUST_PAYMENT_TERM payment = ef.GetPaymentByCharterer(broker);
            return Json(new
            {
                PAYMENT_TERM = payment.MCP_PAYMENT_TERM ?? "",
                ADDRESS_COMMISSION = payment.MCP_ADDRESS_COMMISSION ?? "",
                WITHHOLDING_TAX = payment.MCP_WITHOLDING_TAX ?? ""
            });
        }

        [HttpPost]
        public string getVenderCommission(string id)
        {
            return VendorDAL.GetVendorCommission(id);
        }

        [HttpPost]
        public string getBrokerByCharterer(string id)
        {
            return CustDAL.getBrokerByCharterer(id, "CHOBROMT");
        }

        [HttpPost]
        public string getBrokerByChartererCMMT(string id)
        {
            return CustDAL.getBrokerByCharterer(id, "BRO_FRE");
        }

        private void updateModel(FormCollection form, CharterOutViewModel model)
        {
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                if (!string.IsNullOrEmpty(model.chot_no_charter_out_case_a.period))
                {
                    string[] pa_data = model.chot_no_charter_out_case_a.period.Split(new[] { " to " }, StringSplitOptions.None);
                    model.chot_no_charter_out_case_a.period_from = pa_data[0];
                    model.chot_no_charter_out_case_a.period_to = pa_data[1];
                }
                if (!string.IsNullOrEmpty(model.chot_charter_out_case_b.chot_est_income_b_one.period))
                {
                    string[] pa_data = model.chot_charter_out_case_b.chot_est_income_b_one.period.Split(new[] { " to " }, StringSplitOptions.None);
                    model.chot_charter_out_case_b.chot_est_income_b_one.period_from = pa_data[0];
                    model.chot_charter_out_case_b.chot_est_income_b_one.period_to = pa_data[1];
                }
                if (!string.IsNullOrEmpty(model.chot_charter_out_case_b.chot_est_expense_b_two.period))
                {
                    string[] pa_data = model.chot_charter_out_case_b.chot_est_expense_b_two.period.Split(new[] { " to " }, StringSplitOptions.None);
                    model.chot_charter_out_case_b.chot_est_expense_b_two.period_from = pa_data[0];
                    model.chot_charter_out_case_b.chot_est_expense_b_two.period_to = pa_data[1];
                }

                if (model.chot_summary.laycan != null)
                {
                    if (!string.IsNullOrEmpty(model.chot_summary.laycan))
                    {
                        string[] pa_data = model.chot_summary.laycan.Split(new[] { " to " }, StringSplitOptions.None);
                        model.chot_summary.laycan_from = pa_data[0];
                        model.chot_summary.laycan_to = pa_data[1];
                    }
                }

                //Assign Period, Laycan
                    if (!string.IsNullOrEmpty(model.ev_tempLaycan))
                {
                    string[] ev_laycan = model.ev_tempLaycan.Split(' ');
                    model.chot_proposed_for_approve.laycan_from = ev_laycan[0];
                    model.chot_proposed_for_approve.laycan_to = ev_laycan[2];
                }

                //Assign loadport order and discharge port order

                if (model.chot_detail != null)
                {
                    if (model.chot_detail.chot_load_ports != null)
                    {
                        for (int i = 0; i < model.chot_detail.chot_load_ports.Count; i++)
                        {
                            model.chot_detail.chot_load_ports[i].order = (i + 1) + "";
                        }
                        for (int i = 0; i < model.chot_detail.chot_dis_ports.Count; i++)
                        {
                            model.chot_detail.chot_dis_ports[i].order = (i + 1) + "";
                        }
                    }
                }
                
                model.vessel_list = DropdownServiceModel.getVehicle("CHOVESMT");
                if (model.chos_evaluating != null)
                {
                    model.chos_evaluating.vessel_name = model.vessel_list.Where(x => x.Value == model.chos_evaluating.vessel_id).FirstOrDefault().Text;
                }

                model.broker_list = DropdownServiceModel.getVendorFreight("CHOBROMT");
                model.customer = DropdownServiceModel.getCustomer("CHOCUSMT");
                //Assign round_no, order no., order type and final flag
                if (model.chot_negotiation_summary != null && form["negotiation_summary_offers_items_final_flag"] != null)
                {
                    for (int i = 0; i < model.chot_negotiation_summary.chot_offers_items.Count; i++)
                    {
                        if (form["negotiation_summary_offers_items_final_flag"].ToString() == i + "")
                        {
                            model.chot_negotiation_summary.chot_offers_items[i].final_flag = "Y";
                        }
                        else
                        {
                            model.chot_negotiation_summary.chot_offers_items[i].final_flag = "N";
                        }
                        model.chot_negotiation_summary.chot_offers_items[i].broker_name = model.broker_list.Where(x => x.Value == model.chot_negotiation_summary.chot_offers_items[i].broker_id).FirstOrDefault().Text;
                        model.chot_negotiation_summary.chot_offers_items[i].charterer_name = model.customer.Where(x => x.Value == model.chot_negotiation_summary.chot_offers_items[i].charterer_id).FirstOrDefault().Text;
                        if (model.chot_negotiation_summary.chot_offers_items[i].chot_round_items != null)
                        {
                            for (int j = 0; j < model.chot_negotiation_summary.chot_offers_items[i].chot_round_items.Count; j++)
                            {
                                model.chot_negotiation_summary.chot_offers_items[i].chot_round_items[j].round_no = (j + 1) + "";
                                for (int k = 0; k < model.chot_negotiation_summary.chot_offers_items[i].chot_round_items[j].chot_round_info.Count; k++)
                                {
                                    model.chot_negotiation_summary.chot_offers_items[i].chot_round_items[j].chot_round_info[k].order = (k + 1) + "";
                                    if (j != 0)
                                    {
                                        model.chot_negotiation_summary.chot_offers_items[i].chot_round_items[j].chot_round_info[k].type = model.chot_negotiation_summary.chot_offers_items[i].chot_round_items[0].chot_round_info[k].type;
                                    }
                                }
                            }
                        }
                    }
                }

                //Assign reason
                TempData["CharterOut_Reason"] = model.chot_reason;
                //model.chot_reason = (form["hdfNoteAction"] != null) ? form["hdfNoteAction"].ToString() : (Request.QueryString["Reason"] != null) ? Request.QueryString["Reason"].ToString().Decrypt() : "-";

                using (var context = new EntityCPAIEngine())
                {
                    var sql_freight = context.CPAI_FREIGHT_DATA.Where(f => f.FDA_ROW_ID == model.freight_id).FirstOrDefault();
                    if (sql_freight != null)
                    {
                        model.chot_summary.demurrage_unit = sql_freight.FDA_UNIT;
                    }
                }

            } else
            {
                //Assign Laycan
                if (!string.IsNullOrEmpty(model.pa_tempLaycan))
                {
                    string[] pa_laycan = model.pa_tempLaycan.Split(' ');
                    model.chos_proposed_for_approve.laycan_from = pa_laycan[0];
                    model.chos_proposed_for_approve.laycan_to = pa_laycan[2];
                }

                if (!string.IsNullOrEmpty(model.chos_evaluating.period_date_from))
                {
                    string[] period = model.chos_evaluating.period_date_from.Split(' ');
                    model.chos_evaluating.period_date_from = period[0];// + " " + period[1];
                    model.chos_evaluating.period_date_to = period[2];// + " " + period[4];
                }

                //Assign chartering method type, reason
                if (model.chos_chartering_method == null)
                {
                    model.chos_chartering_method = new ChosCharteringMethod();
                }

                model.chos_chartering_method.chos_type_of_fixings = new List<ChosTypeOfFixing>();
                string type = form["chos_chartering_method_chos_type_of_fixings"].ToString();
                if (type == "M")
                {
                    model.chos_chartering_method.chos_type_of_fixings.Add(new ChosTypeOfFixing { type_value = "Marketing Order", type_flag = "Y" });
                    model.chos_chartering_method.chos_type_of_fixings.Add(new ChosTypeOfFixing { type_value = "Private Order", type_flag = "N" });

                }
                else
                {
                    model.chos_chartering_method.chos_type_of_fixings.Add(new ChosTypeOfFixing { type_value = "Marketing Order", type_flag = "N" });
                    model.chos_chartering_method.chos_type_of_fixings.Add(new ChosTypeOfFixing { type_value = "Private Order", type_flag = "Y" });
                }

                model.vessel_list = DropdownServiceModel.getVehicle("CHOVESCS");
                model.broker_list = DropdownServiceModel.getVendorFreight("CHOBROCS");
                model.charterer_broker_list = DropdownServiceModel.getVendorFreight("CHOCBROCS");
                model.owner_broker_list = DropdownServiceModel.getVendorFreight("CHOOBROCS");
                //Assign Evaluating vessel and broker name
                if (model.chos_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chos_evaluating.vessel_id) && model.vessel_list != null)
                    {
                        model.chos_evaluating.vessel_name = model.vessel_list.SingleOrDefault(x => x.Value == model.chos_evaluating.vessel_id) == null ? "" : model.vessel_list.SingleOrDefault(x => x.Value == model.chos_evaluating.vessel_id).Text;
                    }
                    if (!string.IsNullOrEmpty(model.chos_evaluating.broker_id) && model.broker_list != null)
                    {
                        model.chos_evaluating.broker_name = model.broker_list.SingleOrDefault(x => x.Value == model.chos_evaluating.broker_id) == null ? "" : model.broker_list.SingleOrDefault(x => x.Value == model.chos_evaluating.broker_id).Text;
                    }
                }

                //Assign A, B1, B2 order and value
                if (model.chos_no_charter_out_case_a != null)
                {
                    //A
                    if (model.chos_no_charter_out_case_a.chos_est_expense != null)
                    {
                        for (int i = 0; i < model.chos_no_charter_out_case_a.chos_est_expense.Count; i++)
                        {
                            model.chos_no_charter_out_case_a.chos_est_expense[i].order = (i + 1) + "";
                            if (!string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from))
                            {
                                string[] date = model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from.Split(' ');
                                model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from = date[0];// + " " + date[1];
                                model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to = date[2];// + " " + date[4];
                            }
                        }
                    }
                }
                if (model.chos_charter_out_case_b != null)
                {
                    //B1
                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_one != null)
                    {
                        for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_one.Count; i++)
                        {
                            model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].order = (i + 1) + "";
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from))
                            {
                                string[] date = model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from.Split(' ');
                                model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from = date[0];// + " " + date[1];
                                model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to = date[2];// + " " + date[4];
                            }
                            //Warning. you have to update chos_evaluating vessel id and name first.
                            model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].vessel_id = model.chos_evaluating.vessel_id;
                            model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].vessel_name = model.chos_evaluating.vessel_name;
                            if (model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].other_expense!=null) {
                                for (int j = 0; j < model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].other_expense.Count; j++)
                                {
                                    model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].other_expense[j].order = (j + 1) + "";
                                }
                            }
                            
                        }
                    }
                    //B2
                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_two != null)
                    {
                        if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt != null)
                        {
                            model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.order = 1 + "";
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from))
                            {
                                string[] date = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from.Split(' ');
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from = date[0];// + " " + date[1];
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to = date[2];// + " " + date[4];
                            }
                        }
                        if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel != null)
                        {
                            for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel.Count; i++)
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].order = (i + 1) + "";
                                if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].vessel_id) && model.vessel_list != null)
                                {
                                    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].vessel_name = model.vessel_list.SingleOrDefault(x => x.Value == model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].vessel_id) == null ? "" : model.vessel_list.SingleOrDefault(x => x.Value == model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].vessel_id).Text;
                                }

                                if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from))
                                {
                                    string[] date = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from.Split(' ');
                                    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from = date[0];// + " " + date[1];
                                    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to = date[2];// + " " + date[4];
                                }
                            }
                        }
                        

                        if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_other_expense != null)
                        {
                            for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_other_expense.Count; i++)
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_other_expense[i].order = (i + 1) + "";
                            }
                        }
                    }
                }

                model.customer = DropdownServiceModel.getCustomer("CHOCUSCS");
                //Assign proposed for approve
                if (model.chos_proposed_for_approve != null)
                {
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_from))
                    {
                        string[] date = model.chos_proposed_for_approve.a_date_from.Split(' ');
                        model.chos_proposed_for_approve.a_date_from = date[0] + " " + date[1];
                        model.chos_proposed_for_approve.a_date_to = date[3] + " " + date[4];
                    }
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_from))
                    {
                        string[] date = model.chos_proposed_for_approve.b_date_from.Split(' ');
                        model.chos_proposed_for_approve.b_date_from = date[0] + " " + date[1];
                        model.chos_proposed_for_approve.b_date_to = date[3] + " " + date[4];
                    }

                    if (model.broker_list != null)
                    {
                        if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.charterer_broker))
                        {
                            model.chos_proposed_for_approve.charterer_broker_name = model.charterer_broker_list.SingleOrDefault(x => x.Value == model.chos_proposed_for_approve.charterer_broker) == null ? "" : model.charterer_broker_list.SingleOrDefault(x => x.Value == model.chos_proposed_for_approve.charterer_broker).Text;
                        }
                        if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.owner_broker))
                        {
                            model.chos_proposed_for_approve.owner_broker_name = model.owner_broker_list.SingleOrDefault(x => x.Value == model.chos_proposed_for_approve.owner_broker) == null ? "" : model.owner_broker_list.SingleOrDefault(x => x.Value == model.chos_proposed_for_approve.owner_broker).Text;
                        }
                    }
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.charterer_id) && model.customer != null)
                    {
                        model.chos_proposed_for_approve.charterer_name = model.customer.SingleOrDefault(x => x.Value == model.chos_proposed_for_approve.charterer_id) == null ? "" : model.customer.SingleOrDefault(x => x.Value == model.chos_proposed_for_approve.charterer_id).Text;
                    }
                }

                //Assign reason
                TempData["CharterOut_Reason"] = model.chos_reason;
                model.chos_reason = (form["hdfNoteAction"] != null) ? form["hdfNoteAction"].ToString() : (Request.QueryString["Reason"] != null) ? Request.QueryString["Reason"].ToString().Decrypt() : "-";
            }
        }

        private void initializeDropDownList(ref CharterOutViewModel model)
        {
            model.vesselSize = new List<VesselSize>();
            model.pricesTerm = getPricesTerm();
            model.vessel = getVesselSize();
            model.freight = getFreight();
            model.quantity_list = getTypeOfVessel();
            model.bss_list = getBSS();
            model.vessel_list = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOVESMT" : "CHOVESCS");
            model.broker_list = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOBROMT" : "CHOBROCS");
            model.charterer_broker_list = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOCBROMT" : "CHOCBROCS");
            model.owner_broker_list = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOOBROMT" : "CHOOBROCS");
            model.product_list = DropdownServiceModel.getMaterial(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "MAT_CMMT" : "MAT_CMCS");
            model.customer = DropdownServiceModel.getCustomer((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOCUSMT" : "CHOCUSCS");
            model.freight_list = DropdownServiceModel.getFreight();
            model.year_list = DropdownServiceModel.getYearList();
            model.deduct_action_list = DropdownServiceModel.getDeductActionList();

            model.charterer_list = DropdownServiceModel.getCustomer("CHR_FRE");
            model.cargo_list = DropdownServiceModel.getMaterial(false, "", "MAT_CMMT");
        }

        private List<SelectListItem> getVesselSize(CharterOutViewModel model = null)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_OUT_CRUDE" : "JSON_CHARTER_OUT_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> vesselSize = new List<SelectListItem>();
            if(setting.vessel_size!=null)
            {
                for (int i = 0; i < setting.vessel_size.Count; i++)
                {
                    vesselSize.Add(new SelectListItem { Text = setting.vessel_size[i].key, Value = setting.vessel_size[i].key });
                }
                if (model != null)
                {
                    model.vesselSize = setting.vessel_size;
                }
            } else
            {
                vesselSize.Add(new SelectListItem { Text = "No Data", Value = "" });
            }
            return vesselSize;
        }

        private List<SelectListItem> getTypeOfVessel(CharterOutViewModel model = null)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_OUT_CRUDE" : "JSON_CHARTER_OUT_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> list = new List<SelectListItem>();
            if(setting.type_of_vessel == null)
            {
                list.Add(new SelectListItem { Text = "No data not found", Value = "No data not found" });
            } else
            {
                for (int i = 0; i < setting.type_of_vessel.Count; i++)
                {
                    list.Add(new SelectListItem { Text = setting.type_of_vessel[i], Value = setting.type_of_vessel[i] });
                }
            }
            return list;
        }

        private List<SelectListItem> getBSS()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //get built from the first key of vehicle.
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    selectList.Add(new SelectListItem { Text = "" + i + ":" + j, Value = i + ":" + j });
                }
            }
            return selectList;
        }

        private List<SelectListItem> getFreight()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_OUT_CRUDE" : "JSON_CHARTER_OUT_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> freight = new List<SelectListItem>();
            for (int i = 0; i < setting.freight.Count; i++)
            {
                freight.Add(new SelectListItem { Text = setting.freight[i], Value = setting.freight[i] });
            }
            return freight;
        }

        private List<SelectListItem> getPricesTerm()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_OUT_CRUDE" : "JSON_CHARTER_OUT_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> pricesTerm = new List<SelectListItem>();
            for (int i = 0; i < setting.prices_term.Count; i++)
            {
                pricesTerm.Add(new SelectListItem { Text = setting.prices_term[i], Value = setting.prices_term[i] });
            }
            return pricesTerm;
        }
               
        private ResponseData SaveDataCharterOut(DocumentActionStatus _status, CharterOutViewModel model, string note, string json_fileUpload, string attached_explain = "")
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_2; NextState = ConstantPrm.ACTION.WAITING_CERTIFIED; }
            model.explanationAttach = attached_explain;

            ChosRootObject obj = new ChosRootObject();
            obj.chos_benefit = model.chos_benefit;
            obj.chos_chartering_method = model.chos_chartering_method;
            obj.chos_charter_out_case_b = model.chos_charter_out_case_b;
            obj.chos_evaluating = model.chos_evaluating;
            obj.chos_no_charter_out_case_a = model.chos_no_charter_out_case_a;
            obj.chos_proposed_for_approve = model.chos_proposed_for_approve;
            obj.chos_reason = model.chos_reason;
            obj.chos_note = model.chos_note;
            obj.explanationAttach = model.explanationAttach;
            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000026;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private ResponseData SaveDataCharterOutCMMT(DocumentActionStatus _status, CharterOutViewModel model, string note, string json_fileUpload, string attached_explain = "")
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }
            model.explanationAttach = attached_explain;

            ChotObjectRoot obj = new ChotObjectRoot();
            obj.vessel_id = model.chot_vessel;
            obj.freight_id = model.freight_id;
            obj.chot_date = model.chot_date;
            obj.chot_reason_explanation = model.chot_reason_explanation;
            obj.chot_reason = model.chot_reason;
            obj.chot_note = model.chot_note;
            obj.chot_no_charter_out_case_a = model.chot_no_charter_out_case_a;
            obj.chot_charter_out_case_b = model.chot_charter_out_case_b;
            obj.chot_summary = model.chot_summary;
            obj.chot_result = model.chot_result;
            obj.explanationAttach = model.explanationAttach;
            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000007;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.VESSEL });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }

        private ResponseData LoadDataCharterOut(string TransactionID, ref CharterOutViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CHARTERING });
            req.Req_parameters.P.Add(new P { K = "type", V = type.Decrypt() });
            req.Extra_xml = "";
            MCCTypeSS = type;

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }
            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CharterOutViewModel>(_model.data_detail);
                model.chot_date = model.chot_date;
                model.chot_note = model.chot_note;
                model.chot_reason = model.chot_reason;
                //List<ButtonAction> btn = new List<ButtonAction>();
                //if (model.approve_items[0].approve_action == ConstantPrm.ACTION.APPROVE_1)
                //{
                //    string[] arr_btn = { "CANCEL", "VERIFIED", "REJECT" };
                //    for (int idx_btn = 0; idx_btn < arr_btn.Length; idx_btn++)
                //    {
                //        ButtonAction arr_btn_act = new ButtonAction();
                //        arr_btn_act = _model.buttonDetail.Button.Where(i => i.name == arr_btn[idx_btn]).FirstOrDefault();
                //        if (arr_btn_act != null)
                //        {
                //            btn.Add(arr_btn_act);
                //        }
                //    }
                //    _model.buttonDetail.Button = btn;
                //}

                if (model.chot_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chot_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chot_proposed_for_approve.laycan_to))
                    {
                        model.ev_tempLaycan = model.chot_proposed_for_approve.laycan_from + " to " + model.chot_proposed_for_approve.laycan_to;
                    }
                    //if (!string.IsNullOrEmpty(model.chot_proposed_for_approve.chot_final_fixing_detail.laycan_from) && !string.IsNullOrEmpty(model.chot_proposed_for_approve.chot_final_fixing_detail.laycan_to))
                    //{
                    //    model.pa_tempLaycan = model.chot_proposed_for_approve.chot_final_fixing_detail.laycan_from + " to " + model.chot_proposed_for_approve.chot_final_fixing_detail.laycan_to;
                    //}
                }
                if (model.chos_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chos_evaluating.period_date_from) && !string.IsNullOrEmpty(model.chos_evaluating.period_date_to))
                    {
                        model.chos_evaluating.period_date_from = model.chos_evaluating.period_date_from + " to " + model.chos_evaluating.period_date_to;
                    }
                }

                //Assign A, B1, B2 order and value
                if (model.chos_no_charter_out_case_a != null)
                {
                    //A
                    if (model.chos_no_charter_out_case_a.chos_est_expense != null)
                    {
                        for (int i = 0; i < model.chos_no_charter_out_case_a.chos_est_expense.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from) && !string.IsNullOrEmpty(model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to))
                            {
                                model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from = model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_from + " to " + model.chos_no_charter_out_case_a.chos_est_expense[i].chos_period_date_to;
                            }
                        }
                    }
                }
                if (model.chos_charter_out_case_b != null)
                {
                    //B1
                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_one != null)
                    {
                        for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_one.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to))
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_one[i].chos_period_date_to;
                            }
                        }
                    }
                    //B2
                    if (model.chos_charter_out_case_b.chos_charter_out_case_b_two != null)
                    {
                        if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt != null)
                        {
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to))
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to;
                            }

                            //if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mtchos_spot_in_vessel[i].loading_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to))
                            //{
                            //    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to;
                            //}
                        }


                        if (model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt != null)
                        {
                            if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to))
                            {
                                model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_period_date_to;
                            }

                            for (int i = 0; i < model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel.Count(); i++)
                            {

                                if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from) && !string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to))
                                {
                                    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from + " to " + model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to;
                                }
                            }
                        }


                        //if (!string.IsNullOrEmpty(model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from))
                        //{
                        //    string[] date = model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from.Split(' ');
                        //    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_from = date[0] + " " + date[1];
                        //    model.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel[i].loading_date_to = date[3] + " " + date[4];
                        //}
                    }
                }

                if (model.chos_proposed_for_approve != null)
                {
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chos_proposed_for_approve.laycan_from + " to " + model.chos_proposed_for_approve.laycan_to;
                    }

                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.a_date_to))
                    {
                        model.chos_proposed_for_approve.a_date_from = model.chos_proposed_for_approve.a_date_from + " to " + model.chos_proposed_for_approve.a_date_to;
                    }
                    if (!string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_from) && !string.IsNullOrEmpty(model.chos_proposed_for_approve.b_date_to))
                    {
                        model.chos_proposed_for_approve.b_date_from = model.chos_proposed_for_approve.b_date_from + " to " + model.chos_proposed_for_approve.b_date_to;
                    }
                }
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
                if (_model.buttonDetail.Button.Where(x => x.name.ToUpper() == "VERIFIED").Any())
                {
                    TempData["edit_note"] = true;
                }
                else
                {
                    TempData["edit_note"] = false;
                }
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }
            string _btn = "<input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            model.buttonCode += _btn;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.pur_no = resData.resp_parameters[0].v;
            }

            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref CharterOutViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.VERIFIED).ToList().Count > 0 || Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.CERTIFIED).ToList().Count > 0)
                {
                    model.pageMode = "EDIT_REASON";
                }
                foreach (ButtonAction _button in Button)
                {
                    if (_button.name == "CERTIFIED")
                    {
                        _button.name = "ENDORSED";
                    }
                    string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                    model.buttonCode += _btn;
                }
                model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListCharterOut = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        public ActionResult Search()
        {
            CharteringSearchViewModel CharteringModel = new CharteringSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CHARTERING);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CHARTERING);
            msdata = msdata.OrderBy(x => x).ToList();

            CharteringModel.createdByUser = findUserByGroup;

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            if (Request.QueryString["Type"] != null)
                MCCTypeSS = Request.QueryString["Type"];
            ViewBag.getVessel = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOVESMT" : "CHOVESCS");
            ViewBag.getBroker = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOBROMT" : "CHOBROCS");
            ViewBag.getChaterer = DropdownServiceModel.getCustomer((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOCUSMT" : "CHOCUSCS");
            ViewBag.getUsers = userList;
            ViewBag.TypeDecrypt = MCCTypeSS.Decrypt();

            return View(CharteringModel);
        }
        [HttpPost]
        public ActionResult SearchResult(FormCollection fm, CharteringSearchViewModel model)
        {
            CharteringSearchViewModel CharteringModel = new CharteringSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, "CHARTERING");
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);

            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CHARTERING);
            msdata = msdata.OrderBy(x => x).ToList();

            CharteringModel.createdByUser = findUserByGroup;

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            ViewBag.getVessel = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOVESMT" : "CHOVESCS");
            ViewBag.getBroker = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOBROMT" : "CHOBROCS");
            ViewBag.getChaterer = DropdownServiceModel.getCustomer((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHOCUSMT" : "CHOCUSCS");
            ViewBag.getUsers = userList;
            string _type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
            ViewBag.TypeDecrypt = _type.Decrypt();
            string datePurchase = fm["sDate"].ToString();
            string stDate = "";
            string enDate = "";
            string vessel = fm["vessel"].ToString();
            //string flatRate = fm["flatRate"].ToString();
            string laycan = fm["sLaycan"].ToString();
            string laycan_start = "";
            string laycan_end = "";
            string broker = fm["broker"].ToString();
            string charterer = fm["charterer"].ToString();
            string owner = fm["owner"].ToString();
            string userlist = "";
            if (fm["userList"] != null)
            {
                userlist = fm["userList"].ToString().Replace(",", "|");
            }

            if (!datePurchase.Equals(""))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                enDate = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            if (!laycan.Equals(""))
            {
                string[] s = laycan.Split(new[] { " to " }, StringSplitOptions.None);
                laycan_start = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                laycan_end = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            List_Chartertrx res = SearchCharterOutData(stDate, enDate, vessel, datePurchase, userlist, laycan, broker, charterer, owner, laycan_start, laycan_end);
            CharteringModel.CharteringTransaction = res.CharteringTransaction.OrderByDescending(x => x.transaction_id).ToList();

            for(int i = 0; i < res.CharteringTransaction.Count; i++)
            {
                CharteringModel.CharteringTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(CharteringModel.CharteringTransaction[i].date_purchase, "MMMM dd yyyy");
                CharteringModel.CharteringTransaction[i].Transaction_id_Encrypted = CharteringModel.CharteringTransaction[i].transaction_id.Encrypt();
                CharteringModel.CharteringTransaction[i].Req_transaction_id_Encrypted = CharteringModel.CharteringTransaction[i].req_transaction_id.Encrypt();
                CharteringModel.CharteringTransaction[i].Purchase_no_Encrypted = CharteringModel.CharteringTransaction[i].purchase_no.Encrypt();
                CharteringModel.CharteringTransaction[i].reason_Encrypted = CharteringModel.CharteringTransaction[i].reason.Encrypt();
                if (Session["Charter_Type"] != null)
                    CharteringModel.CharteringTransaction[i].type_Encrypted = Session["Charter_Type"].ToString();
            }

            CharteringModel.sDate = datePurchase;
            CharteringModel.sVessel = vessel;
            CharteringModel.sCharterer = charterer;
            CharteringModel.sLaycan = laycan;
            CharteringModel.sCreateBySelected = userlist;
            CharteringModel.sCreateBy = userlist;

            return View("Search", CharteringModel);
        }
        private List_Chartertrx SearchCharterOutData(string sDate, string eDate, string vessel, string date_purchase, string create_by, string laycan, string broker, string charterer, string owner, string laycan_start, string laycan_end)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000004;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CHARTERING });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "function_code", V = MCCTypeSS.Decrypt() == ConstantPrm.SYSTEMTYPE.VESSEL ? CHARTER_OUT_CMMT_FUNCTION_CODE : CHARTER_OUT_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_5", V = vessel });
            //req.Req_parameters.P.Add(new P { K = "index_7", V = flat_rate });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = broker });
            req.Req_parameters.P.Add(new P { K = "index_11", V = charterer });
            req.Req_parameters.P.Add(new P { K = "index_12", V = owner });
            req.Req_parameters.P.Add(new P { K = "index_19", V = laycan_start });
            req.Req_parameters.P.Add(new P { K = "index_20", V = laycan_end });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_Chartertrx _model = ShareFunction.DeserializeXMLFileToObject<List_Chartertrx>(_DataJson);
            if (_model == null) _model = new List_Chartertrx();
            if (_model.CharteringTransaction == null) _model.CharteringTransaction = new List<CharteringEncrypt>();
            return _model;
        }

        public ActionResult ApproveCharterOut()
        {
            //ForTest
            //http://localhost:50131/CPAIMVC/CharterOut/ApproveCharterOut?token=89b7e01a8af24dc98102e6be1d3470a3

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }
        
        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        //if (item.UserType.Equals("CRUDE"))
                        //{
                        //    urlPage = "BunkerPreDuePurchase.aspx";
                        //}
                        //else
                        //{
                        //    urlPage = "BunkerPreDuePurchaseCMMT.aspx";
                        //}
                        Session["Charter_Type"] = item.UserType.Encrypt();
                        urlPage = item.UserType == ConstantPrm.SYSTEMTYPE.CRUDE ? "CharterOut" : "CharterOut/CharterOutCMMT";
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                            }
                            strTransID = item.TransactionID;
                        }

                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&PURNO=" + strPurNo.Encrypt();
                    }
                }
            }
            return path;
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        [HttpPost]
        public string getFreightDataList(string datestr, string vessel, string charterer, string laycan, string laytime, string cargo)
        {
            DateTime? date = string.IsNullOrEmpty(datestr) ? null : ShareFn.ConvertStringDateFormatToDatetime(datestr);
            DateTime? laycan_from = null; DateTime? laycan_to = null;
            if (!string.IsNullOrEmpty(laycan))
            {
                string[] arr_laycan = laycan.SplitWord(" to ");
                laycan_from = ShareFn.ConvertStringDateFormatToDatetime(arr_laycan[0]);
                laycan_to = ShareFn.ConvertStringDateFormatToDatetime(arr_laycan[1]);
            }
           
            return CharterOutServiceModel.getFreightDataList(date, vessel, charterer, laycan_from, laycan_to, laytime, cargo);
        }

        [HttpPost]
        public string getFreightData(string freight_row_id)
        {
            return new JavaScriptSerializer().Serialize(CharterOutServiceModel.getFreightData(freight_row_id));
        }

        [HttpPost]
        public string getChartererData(string row_id)
        {
            return CharterOutServiceModel.getCharterer(row_id);
        }

        #region ---- Call Estimated Freight Calculation ----
        [HttpGet]
        public ActionResult FreightCalAjax(string DOCNO)
        {
            string result = CharterOutServiceModel.getFreightCalculationByDocNo(DOCNO);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}