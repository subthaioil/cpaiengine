﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CompanyController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "Company");
        }

        [HttpPost]
        public ActionResult Search(CompanyViewModel tempModel)
        {

            CompanyViewModel model = initialModel();

            CompanyServiceModel serviceModel = new CompanyServiceModel();
            CompanyViewModel_Seach viewModelSearch = new CompanyViewModel_Seach();

            viewModelSearch = tempModel.cmp_Search;
            serviceModel.Search(ref viewModelSearch);
            model.cmp_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            CompanyViewModel model = initialModel();
            return View(model);
        }

        public static CompanyViewModel SearchQuery(CompanyViewModel CompanyModel)
        {
            CompanyViewModel_Seach viewModelSearch = new CompanyViewModel_Seach();
            CompanyServiceModel serviceModel = new CompanyServiceModel();

            CompanyModel.ddl_CompanyCode = DropdownServiceModel.GetCompanyCode();
            CompanyModel.ddl_ShortName = DropdownServiceModel.GetCompanyShortName();
            CompanyModel.ddl_Status = DropdownServiceModel.getMasterStatus();
            CompanyModel.ddl_CreateType = DropdownServiceModel.getCreateType();

            viewModelSearch = CompanyModel.cmp_Search;
            serviceModel.Search(ref viewModelSearch);
            CompanyModel.cmp_Search = viewModelSearch;

            return CompanyModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, CompanyViewModel model)
        {
            if (ViewBag.action_create)
            {
                CompanyServiceModel serviceModel = new CompanyServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.cmp_Detail.Status = model.cmp_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Add(model.cmp_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                //CompanyViewModel modelDropdownSet = initialModel();

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Company" }));
            }
            
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                CompanyViewModel model = initialModel();
                if (model.cmp_Detail.CreateType == "" || model.cmp_Detail.CreateType == null)
                {
                    model.cmp_Detail.CreateType = "CPAI";
                    model.cmp_Detail.CompanyCode = "";
                }
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Company" }));
            }
            
        }

        [HttpPost]
        public ActionResult Edit(string CompanyCode, CompanyViewModel model)
        {
            if (ViewBag.action_edit)
            {
                CompanyServiceModel serviceModel = new CompanyServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.cmp_Detail.Status = model.cmp_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                rtn = serviceModel.Edit(model.cmp_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["CompanyCode"] = CompanyCode;

                CompanyViewModel modelDropdownSet = initialModel();
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Company" }));
            }
            
        }

        [HttpGet]
        public ActionResult Edit(string CompanyCode)
        {
            if (ViewBag.action_edit)
            {
                CompanyServiceModel serviceModel = new CompanyServiceModel();

                CompanyViewModel model = initialModel();
                model.cmp_Detail = serviceModel.Get(CompanyCode);
                TempData["CompanyCode"] = CompanyCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Company" }));
            }

        }

        [HttpGet]
        public ActionResult View(string CompanyCode)
        {
            if (ViewBag.action_view)
            {
                CompanyServiceModel serviceModel = new CompanyServiceModel();

                CompanyViewModel model = initialModel();
                model.cmp_Detail = serviceModel.Get(CompanyCode);
                TempData["CompanyCode"] = CompanyCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Company" }));
            }

        }

        public CompanyViewModel initialModel()
        {
            CompanyServiceModel serviceModel = new CompanyServiceModel();
            CompanyViewModel model = new CompanyViewModel();

            model.cmp_Search = new CompanyViewModel_Seach();
            model.cmp_Search.sSearchData = new List<CompanyViewModel_SeachData>();

            model.cmp_Detail = new CompanyViewModel_Detail();
            model.cmp_Detail.BoolStatus = true;

            model.ddl_CompanyCode = DropdownServiceModel.GetCompanyCode();
            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_ShortName = DropdownServiceModel.GetCompanyShortName();

            model.companyCodeJSON = CompanyServiceModel.GetCompanyCodeJSON();
            model.companyNameJSON = CompanyServiceModel.GetCompanyNameJSON();


            return model;
        }
    }
}