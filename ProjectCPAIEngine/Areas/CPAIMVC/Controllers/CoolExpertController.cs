﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.DAL.Entity;
using System.IO;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using OfficeOpenXml;
using Excel;
using System.Data;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Xml.Linq;
using System.Xml;
using System.Net;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public partial class CoolController : BaseController
    {
        public ActionResult CoolExpert()
        {
            CoolViewModel model;
            if (TempData["CoolViewModel"] == null)
            {
                model = new CoolViewModel();
                model.pageMode = "EDIT"; //PageMode is EDIT, EDIT_REASON or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    temp_tranID = tranID;
                    ResponseData resData = LoadDataCoolExpert(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }

                    if (resData != null)
                    {
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            else
                            {
                                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            }
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
                else
                {
                    string path = string.Format("~/CPAIMVC/Cool/Search");
                    return Redirect(path);
                }
            }
            else
            {
                model = TempData["CoolViewModel"] as CoolViewModel;
                //List<string> userGroups = CoolServiceModel.getUserGroup(lbUserID);
                //if (!(userGroups.Contains("EXPERT_SH") || model.areas.area.Where(x => userGroups.Contains(x.name.ToUpper()) || x.unit.Where(z => z.next_status == ConstantPrm.ACTION.APPROVE_3).Any()).Any()))
                //{
                //    model.buttonCode = null;
                //    model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL_Expert' />";
                //    string _btnPrint = "<div class='col-md-6' style='margin-top: 10px; width:50%'><input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenExpertPDF' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" style='width:100%' /></div>";
                //    string _btn1 = "<div class='col-md-6' style='margin-top: 10px; width:50%'><input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("btnBack") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" style='width:100%' /></div>";
                //    model.buttonCode += _btnPrint;
                //    model.buttonCode += _btn1;
                //}
                TempData["CoolViewModel"] = null;
            }
            model.ddlFileSupportingDocumentType = getFileType();
            List<string> userGroup = CoolServiceModel.getUserGroup(lbUserID);
            TempData["isExpert"] = userGroup.Contains("EXPERT");
            TempData["isExpert_SH"] = userGroup.Contains("EXPERT_SH");
            TempData["isShowAllArea"] = (bool)TempData["isExpert"] || (bool)TempData["isExpert_SH"];
            CoolServiceModel.updateModelFromGroup(lbUserID, ref model);

            return View(model);
        }

        private ResponseData LoadDataCoolExpert(string TransactionID, ref CoolViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000043;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CoolViewModel>(_model.data_detail);
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermissionExpert(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL_Expert' />";
            //string _btnPrint = "<input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            string _btnPrint = "<div class='col-md-6' style='margin-top: 10px; width:50%'><input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenExpertPDF' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" style='width:100%' /></div>";
            string _btn1 = "<div class='col-md-6' style='margin-top: 10px; width:50%'><input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("btnBack") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" style='width:100%' /></div>";
            model.buttonCode += _btnPrint;
            model.buttonCode += _btn1;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.purno = resData.resp_parameters[0].v;
            }
            return resData;
        }

        private ResponseData SaveDataCoolExpert(DocumentActionStatus _status, CoolViewModel model, string note = "", string json_fileUpload = "")
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }


            CoexRootObject obj = new CoexRootObject();

            obj.areas = model.areas;
            obj.approve_items = model.approve_items;
            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000041;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.COOL });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            resData = service.CallService(req);
            return resData;
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL_Expert")]
        public ActionResult _btn_Click_Expert(FormCollection form, CoolViewModel model)
        {
            if (ButtonListCool != null)
            {
                var _lstButton = ButtonListCool.Where(x => x.name == form["hdfEventClick"]).ToList();
                CooRootObject obj = new CooRootObject();
                obj.areas = model.areas;
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    COO_DATA_DAL dataDal = new COO_DATA_DAL();
                    COO_DATA data = dataDal.GetByID(model.taskID);
                    if (model.data == null)
                    {
                        obj.data = new CooData();
                    }
                    else
                    {
                        obj.data = model.data;
                    }
                    if (data != null)
                    {
                        obj.data.approval_date = (data.CODA_COMPLETE_DATE == DateTime.MinValue || data.CODA_COMPLETE_DATE == null) ? "" : Convert.ToDateTime(data.CODA_COMPLETE_DATE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                        obj.data.assay_ref = data.CODA_ASSAY_REF_NO;
                        obj.data.country = data.CODA_COUNTRY;
                        obj.data.crude_name = data.CODA_CRUDE_NAME;
                    }

                    obj.comment_draft_cam = model.comment_draft_cam;
                    obj.comment_final_cam = model.comment_final_cam;

                    // GENERATE DRAFT CAM: EXPERT AND EXPERT SECTION HEAD SUBMIT
                    if (_lstButton[0].name.ToUpper() == ConstantPrm.ACTION.SUBMIT)
                    {
                        obj.data.draft_cam_file = GenerateExcel(model, isOnlyPath: true);
                    }

                    string _xml = _lstButton[0].call_xml;
                    string note = _lstButton[0].name.ToUpper() == "REJECT" || _lstButton[0].name.ToUpper() == "CANCEL" ? form["hdfNoteAction"] : "-";

                    string json_fileUpload = string.Empty;
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    var json = new JavaScriptSerializer().Serialize(obj);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = json_fileUpload });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            //real success
                            if (resData.resp_parameters != null && resData.resp_parameters.Where(x => x.k == "update_flag").FirstOrDefault() != null)
                            {
                                // RE-GENERATE DRAFT CAM: EXPERT AND EXPERT SECTION HEAD SUBMIT
                                if (_lstButton[0].name.ToUpper() == ConstantPrm.ACTION.SUBMIT)
                                {
                                    GenerateExcel(model: model);
                                }
                                string tranID;
                                if (resData.transaction_id != "-")
                                    tranID = resData.transaction_id.Encrypt();
                                else
                                    tranID = temp_tranID.Encrypt();
                                string reqID = resData.req_transaction_id.Encrypt();
                                string path = string.Format("~/CPAIMVC/Cool/CoolExpert?TranID={0}&Tran_Req_ID={1}", tranID, reqID);
                                return Redirect(path);
                            }
                            //else
                            //{
                            //    TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            //}
                        }
                        //else if (resData.result_code == "10000033")
                        //{
                        //    TempData["res_message"] = "This document has been changed by others. Please check it again.";
                        //}
                    }
                }
            }
            else
            {
                TempData["res_message"] = "An error occurred on the system, Please contact the system administrator.";
            }

            TempData["res_message"] = "This document has been changed by others. Please check and submit it again.";
            TempData["CoolViewModel"] = model;
            return RedirectToAction("CoolExpert", "Cool");
        }

        private void BindButtonPermissionExpert(List<ButtonAction> Button, ref CoolViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.APPROVE).ToList().Count > 0)
                {
                    model.pageMode = "EDIT_REASON";
                }

                List<string> userGroup = CoolServiceModel.getUserGroup(lbUserID);
                if (userGroup.Contains("EXPERT_SH") && model.areas.area.Where(x => userGroup.Contains(x.name.ToUpper()) && x.unit.Where(z => z.next_status == ConstantPrm.ACTION.APPROVE_3).Any()).Any())
                {
                    foreach (ButtonAction _button in Button)
                    {
                        string _btn = "<div class='col-md-6' style='margin-top: 10px; width:50%'><input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' style='width:100%'/></div>";
                        model.buttonCode += _btn;
                    }
                }
                else
                {
                    foreach (ButtonAction _button in Button)
                    {
                        string _btn = "<div class='col-md-6' style='margin-top: 10px; width:50%'><input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' style='width:100%'/></div>";
                        if (model != null && model.areas != null)
                        {
                            if (_button.name == "CALL BACK")
                            {
                                if (userGroup.Contains("EXPERT_SH"))
                                {
                                    model.buttonCode += _btn;
                                }
                                else if (userGroup.Contains("EXPERT") && model.areas.area.Where(x => x.unit.Where(z => z.next_status == ConstantPrm.ACTION.APPROVE_3 && userGroup.Contains(z.name.ToUpper())).Any()).Any())
                                {
                                    model.buttonCode += _btn;
                                }
                            }
                            else if (model.areas.area.Where(i => i.unit.Where(x => (x.status == ConstantPrm.ACTION.APPROVE_2 || x.status == ConstantPrm.ACTION.REJECT) && userGroup.Contains(x.name.ToUpper())).Any()).Any()) // && model.areas.area.Where(a => a.unit.Where(u=> userGroup.Contains(u.name)).Any()).Any())
                            {
                                model.buttonCode += _btn;
                            }
                            else if (userGroup.Contains("EXPERT_SH") && userGroup.Contains("EXPERT") && model.areas.area.Where(x => userGroup.Contains(x.name.ToUpper()) && x.unit.Where(y => y.status == ConstantPrm.ACTION.APPROVE_2 || y.status == ConstantPrm.ACTION.REJECT || y.next_status == ConstantPrm.ACTION.APPROVE_3).Any()).Any())
                            {
                                model.buttonCode += _btn;
                            }
                        }

                    }
                }
                ButtonListCool = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraftExpert")]
        public ActionResult SaveDraftExpert(FormCollection form, CoolViewModel model)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            ResponseData resData = SaveDataCoolExpert(_status: _status, model: model);
            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            //initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string path = string.Format("~/CPAIMVC/Cool/CoolExpert?TranID={0}&Tran_Req_ID={1}", tranID, reqID);
                    return Redirect(path);
                }
            }
            ModelState.Clear();
            return View("Search", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenExpertPDF")]
        public ActionResult GenExpertPDF(FormCollection form, CoolViewModel model)
        {
            GenerateExcel(model: model, isDownload: true);
            //initializeDropDownList(ref model);
            model.ddlFileSupportingDocumentType = getFileType();
            List<string> userGroup = CoolServiceModel.getUserGroup(lbUserID);
            TempData["isExpert"] = userGroup.Contains("EXPERT");
            TempData["isExpert_SH"] = userGroup.Contains("EXPERT_SH");
            TempData["isShowAllArea"] = (bool)TempData["isExpert"] || (bool)TempData["isExpert_SH"];
            //CoolServiceModel.updateModelFromGroup(lbUserID, ref model);

            return View("CoolExpert", model);
        }

        public ActionResult ExtendExcel (CoolViewModel model)
        {
            string tempFinalCAM = string.Empty;
            if (Request.QueryString["tempExtendExcel"] != null)
            {
                tempFinalCAM = Request.QueryString["tempExtendExcel"].ToString();
            }
            return View("ExtendExcel","", tempFinalCAM);
        }
    }
}