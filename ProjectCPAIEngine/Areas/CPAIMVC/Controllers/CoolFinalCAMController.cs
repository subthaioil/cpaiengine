﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public partial class CoolController : BaseController
    {
        public ActionResult CoolFinalCAM()
        {
            CoolViewModel model;
            if(TempData["CoolViewModel"] == null)
            {
                model = new CoolViewModel();
                model.pageMode = "EDIT";
                model.buttonMode = "AUTO";
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    temp_tranID = tranID;
                    ResponseData resData = LoadDataCoolFinal(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }

                    if (resData != null)
                    {
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
            }
            else
            {
                model = TempData["CoolViewModel"] as CoolViewModel;
                TempData["CoolViewModel"] = null;
            }

            List<string> userGroup = CoolServiceModel.getUserGroup(lbUserID);
            TempData["isExpert"] = userGroup.Contains("EXPERT");
            TempData["isExpert_SH"] = userGroup.Contains("EXPERT_SH");
            TempData["isSCEP"] = userGroup.Contains("SCEP");
            TempData["SCEP_SH"] = userGroup.Contains("SCEP_SH");

            model.comment_final_cam = model.comment_final_cam ?? model.comment_draft_cam;

            return View(model);
        }

        private ResponseData LoadDataCoolFinal(string TransactionID, ref CoolViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000043;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CoolViewModel>(_model.data_detail);
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermissionFinal(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL_final' />";
            string _btn1 = "<div class='col-md-6' style='margin-top: 10px;width:50%'><input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("btnBack") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" style='width:100%' /></div>";
            model.buttonCode += _btn1;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.purno = resData.resp_parameters[0].v;
            }
            return resData;
        }

        private void BindButtonPermissionFinal(List<ButtonAction> Button, ref CoolViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.APPROVE).ToList().Count > 0 || Button.Where(x => x.name.ToUpper().Contains(ConstantPrm.ACTION.EDIT)).ToList().Count > 0)
                {
                    model.pageMode = "EDIT_REASON";
                }
                foreach (ButtonAction _button in Button)
                {
                    if (_button.name == "EDIT COMMENT" && model.pageMode != "EDIT_REASON") continue;
                    string _btn = "";
                    if (_button.name == "EDIT COMMENT")
                    {
                        if (!string.IsNullOrEmpty( model.data.created_by) && model.data.created_by.ToLower() != "migrate")
                        {
                            _btn = "<div class='col-md-6' style='margin-top: 10px;width:50%'><input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' style='width:100%;padding-left:0px;padding-right:0px;' /></div>";
                        }
                    }
                    else
                    {
                        _btn = "<div class='col-md-6' style='margin-top: 10px;width:50%'><input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' style='width:100%' /></div>";
                    }
                    model.buttonCode += _btn;
                }
                ButtonListCool = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL_final")]
        public ActionResult _btn_Click_final(FormCollection form, CoolViewModel model)
        {
            if (ButtonListCool != null)
            {
                model.note = form["hdfNoteAction"] != null ? form["hdfNoteAction"].ToString() : model.note;
                var _lstButton = ButtonListCool.Where(x => x.name == form["hdfEventClick"]).ToList();
                CooRootObject obj = new CooRootObject();
                obj.areas = model.areas;
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    ResponseData resFile = SaveFileCool(form: ref form, model: ref model);
                    if (model.data == null)
                    {
                        COO_DATA_DAL dataDal = new COO_DATA_DAL();
                        COO_DATA data = dataDal.GetByID(model.taskID);
                        if (data != null)
                        {
                            obj.data = new CooData();
                            obj.data.approval_date = (data.CODA_COMPLETE_DATE == DateTime.MinValue || data.CODA_COMPLETE_DATE == null) ? "" : Convert.ToDateTime(data.CODA_COMPLETE_DATE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            obj.data.assay_ref = data.CODA_ASSAY_REF_NO;
                            obj.data.country = data.CODA_COUNTRY;
                            obj.data.crude_name = data.CODA_CRUDE_NAME;
                        }
                    }
                    else
                    {
                        obj.data = model.data;
                    }
                    obj.comment_draft_cam = model.comment_draft_cam;
                    obj.comment_final_cam = model.comment_final_cam;
                    obj.note = model.note;

                    // GENERATE FINAL CAM: SCEP SETION HEAD APPROVED
                    if (_lstButton[0].name.ToUpper() == ConstantPrm.ACTION.APPROVE || _lstButton[0].name.ToUpper().Contains(ConstantPrm.ACTION.EDIT))
                    {
                        obj.data.draft_cam_file = GenerateExcel(model: model, isOnlyPath: true);
                        obj.data.final_cam_file = GenerateExcel(model: model, isOnlyPath: true, isFinal: true);
                    }

                    string _xml = _lstButton[0].call_xml;
                    string json_fileUpload = string.Empty;
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    var json = new JavaScriptSerializer().Serialize(obj);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = "-" });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = json_fileUpload });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2" || resData.result_code == "10000033")
                        {
                            // RE-GENERATE FINAL CAM: SCEP SETION HEAD APPROVED
                            if (_lstButton[0].name.ToUpper() == ConstantPrm.ACTION.APPROVE || _lstButton[0].name.ToUpper().Contains(ConstantPrm.ACTION.EDIT))
                            {
                                GenerateExcel(model: model);
                                GenerateExcel(model: model, isFinal: true);
                            }
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string purno = resData.resp_parameters[0].v.Encrypt();
                            string path = string.Format("~/CPAIMVC/Cool/CoolFinalCAM?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "An error occurred on the system, Please contact the system administrator.";
            }

            TempData["CoolViewModel"] = model;
            return RedirectToAction("CoolFinalCAM", "Cool");
        }
    }
}