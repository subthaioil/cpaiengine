﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Utilities;

using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;

using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CrudeImportPlanController : BaseController
    {
        CIP_Config _CIPConfig = null;
        public ActionResult index()
        {
            return RedirectToAction("Create", "CrudeImportPlan");
        }

        [HttpPost]
        public ActionResult SaveCrude(FormCollection frm, CrudeImportPlanViewModel model)
        {
            return View(model);
        }

        //[HttpPost]
        //public ActionResult Create(FormCollection frm, CrudeImportPlanViewModel model)
        //{
        //    CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
        //    service.SaveCrudeImportPlan(ref model);

        //    TempData["CrudeDataModel"] = model;

        //    return RedirectToAction("Create", "CrudeImportPlan");

        //}

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendBooking")]
        public ActionResult SendBooking(FormCollection form, CrudeImportPlanViewModel model)
        {
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendToBookingService(ref model);

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "PriceTabClick")]
        public ActionResult PriceTabClick(FormCollection form, CrudeImportPlanViewModel model)
        {
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            if (!string.IsNullOrEmpty(model.AvalivalYearMonth))
            {

                Decimal ArrivalMonth = DateTime.ParseExact(model.AvalivalYearMonth.Substring(0, 3), "MMM", System.Globalization.CultureInfo.CurrentCulture).Month;

                Decimal ArrivalYear = Convert.ToDecimal(("20" + model.AvalivalYearMonth.Substring(3, 2)));

                List<string> MOPSPrdCode = new List<string>();
                service.SearchCrudeImportPlan("Y", "", ArrivalMonth, ArrivalYear, _CIPConfig, ref model, ref MOPSPrdCode);

                Session["MOPSPrdCode"] = MOPSPrdCode;
            }

        
            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendAccount")]
        public ActionResult SendAccount(FormCollection form, CrudeImportPlanViewModel model)
        {
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendToAccountService(ref model);

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendMemo")]
        public ActionResult SendMemo(FormCollection form, CrudeImportPlanViewModel model)
        {
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendMemoToSap(ref model);

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCreatePOAndGIT")]
        public ActionResult SendCreatePOAndGIT(FormCollection form, CrudeImportPlanViewModel model)
        {

            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendCreatePOAndGIT(ref model, "PO", "", "");

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCreateGIT")]
        public ActionResult SendCreateGIT(FormCollection form, CrudeImportPlanViewModel model)
        {

            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendCreatePOAndGIT(ref model, "GIT", "", "");

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCancelPOAndGIT")]
        public ActionResult SendCancelPOAndGIT(FormCollection form, CrudeImportPlanViewModel model)
        {

            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendCancelPOAndGIT(ref model, "", "");

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "cancelPO")]
        public ActionResult cancelPO(FormCollection form, CrudeImportPlanViewModel model)
        {

            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            //service.SendPOIntransitToSap(ref model);

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCancelGIT")]
        public ActionResult SendCancelGIT(FormCollection form, CrudeImportPlanViewModel model)
        {

            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendCancelGIT(ref model, "", "");

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");

        }




        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendSurveyor")]
        public ActionResult SendSurveyor(FormCollection form, CrudeImportPlanViewModel model)
        {
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            service.initialModel(ref model);

            service.setIsCheckHeader(ref model);

            service.SendSurveyorToSap(ref model);

            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }



        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendSurveyorPopup")]
        public ActionResult SendSurveyorPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PSU";
                TempData["CrudeDataModel"] = model;
            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else {

                    service.initialModel(ref model);

                    service.setIsCheckHeader(ref model);

                    service.SendSurveyorPopupToSap(ref model, tripno);

                    if (model.crude_approve_detail != null && model.crude_approve_detail.dDetail_Surveyor != null)
                    {
                        var list = model.crude_approve_detail.dDetail_Surveyor.Where(p => p.Sur_TripNo.Equals(tripno));
                        if (list != null)
                        {
                            foreach (var item in list)
                            {
                                item.i_IS_CHECK = false;
                            }
                        }
                    }
                }

                model.auto_showPopup = "PSU";
                TempData["CrudeDataModel"] = model;
            }
            
            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AutoSaveCustomPopup")]
        public ActionResult AutoSaveCustomPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PCU";
                TempData["CrudeDataModel"] = model;

            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    model.MsgAlert = "save completed";
                     
                    service.setIsCheckHeader(ref model);

                }

                service.initialModel(ref model);

                model.auto_showPopup = "PCU";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AutoSaveSurveyorPopup")]
        public ActionResult AutoSaveSurveyorPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PSU";
                TempData["CrudeDataModel"] = model;

            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    model.MsgAlert = "save completed";
                     

                    service.setIsCheckHeader(ref model);
                     
                }

                service.initialModel(ref model);
                model.auto_showPopup = "PSU";
                TempData["CrudeDataModel"] = model;
            }
             
            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AutoSaveOtherPopup")]
        public ActionResult AutoSaveOtherPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "POT";
                TempData["CrudeDataModel"] = model;

            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    model.MsgAlert = "save completed";


                    service.setIsCheckHeader(ref model);

                }

                service.initialModel(ref model);
                model.auto_showPopup = "POT";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AutoSaveFreightPopup")]
        public ActionResult AutoSaveFreightPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PFR";
                TempData["CrudeDataModel"] = model;
            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    model.MsgAlert = "save completed";
                     
                    service.setIsCheckHeader(ref model);

                }

                service.initialModel(ref model);

                model.auto_showPopup = "PFR";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AutoSaveCrudePopup")]
        public ActionResult AutoSaveCrudePopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    model.MsgAlert = "save completed";
                     
                    service.setIsCheckHeader(ref model);

                }

                service.initialModel(ref model);

                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AutoSaveDailyPricePopup")]
        public ActionResult AutoSaveDailyPricePopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PPH";
                TempData["CrudeDataModel"] = model;
            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    model.MsgAlert = "save completed";

                    service.setIsCheckHeader(ref model);

                }

                service.initialModel(ref model);

                model.auto_showPopup = "PPH";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }


        //SendGITPopup
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendGITPopup")]
        public ActionResult SendGITPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            string itemno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (form.GetValue("Popup_ItemNoSelect") != null)
                itemno = form.GetValue("Popup_ItemNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }
            else
            {

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                     
                    service.setIsCheckHeader(ref model);

                    service.SendCreatePOAndGIT(ref model, "GIT", tripno, itemno);

                }

                service.initialModel(ref model);

                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCancelGITPopup")]
        public ActionResult SendCancelGITPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            string itemno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (form.GetValue("Popup_ItemNoSelect") != null)
                itemno = form.GetValue("Popup_ItemNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }
            else
            {
                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                    
                    service.setIsCheckHeader(ref model);

                    service.SendCancelGIT(ref model, tripno, itemno);
                }

                service.initialModel(ref model);

                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendPOCrudePopup")]
        public ActionResult SendPOCrudePopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            string itemno = "";
            if (form.GetValue("Popup_TripNoSelect") != null) 
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (form.GetValue("Popup_ItemNoSelect") != null)
                itemno = form.GetValue("Popup_ItemNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }
            else
            {
                 
                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                      
                    service.setIsCheckHeader(ref model);

                    service.SendCreatePOAndGIT(ref model, "PO", tripno, itemno);
                     
                }

                service.initialModel(ref model);

                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }
            
            return RedirectToAction("Create", "CrudeImportPlan");

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCancelPOPopup")]
        public ActionResult SendCancelPOPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            string itemno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (form.GetValue("Popup_ItemNoSelect") != null)
                itemno = form.GetValue("Popup_ItemNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }
            else
            {
                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {
                     
                    service.setIsCheckHeader(ref model);

                    service.SendCancelPOAndGIT(ref model, tripno, itemno);
                }

                service.initialModel(ref model);

                model.auto_showPopup = "PCR";
                TempData["CrudeDataModel"] = model;
            }

            return RedirectToAction("Create", "CrudeImportPlan");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SendCancelSurveyorPopup")]
        public ActionResult SendCancelSurveyorPopup(FormCollection form, CrudeImportPlanViewModel model)
        {
            string rootPath = Server.MapPath("../../");

            string tripno = "";
            if (form.GetValue("Popup_TripNoSelect") != null)
                tripno = form.GetValue("Popup_TripNoSelect").AttemptedValue;

            if (tripno == "")
            {
                model.auto_showPopup = "PSU";
                TempData["CrudeDataModel"] = model;
            }
            else
            {

                if (model.crude_approve_detail != null && model.crude_approve_detail.dDetail_Surveyor != null)
                {
                    var list = model.crude_approve_detail.dDetail_Surveyor.Where(p => p.Sur_TripNo.Equals(tripno));
                    if (list != null) {
                        foreach (var item in list)
                        {
                            item.IS_DELETE = "X";
                        }
                    }
                }

                CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                string msg = "";
                Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), tripno, rootPath, ref msg);
                if (result == false)
                {
                    model.MsgAlert = msg;
                }
                else
                {

                   

                    service.setIsCheckHeader(ref model);

                    service.SendSurveyorPopupToSap(ref model, tripno, "cancel");

                    if (model.crude_approve_detail != null && model.crude_approve_detail.dDetail_Surveyor != null)
                    {
                        var list = model.crude_approve_detail.dDetail_Surveyor.Where(p => p.Sur_TripNo.Equals(tripno));
                        if (list != null)
                        {
                            foreach (var item in list)
                            {
                                item.i_IS_CHECK = false;
                            }
                        }
                    }
                    
                }

                service.initialModel(ref model);

                model.auto_showPopup = "PSU";
                TempData["CrudeDataModel"] = model;
            }




            return RedirectToAction("Create", "CrudeImportPlan");
            //?form.GetValue("Popup_TripNoSelect").AttemptedValue
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, CrudeImportPlanViewModel model)
        {
                        
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();

            string rootPath = Server.MapPath("../../");
            
            string msg = "";
            Boolean result = service.SaveCrudeImportPlan(ref model, getMatType(), Session["menu_action"].ToString(), "", rootPath, ref msg);
            //if (result == false)

            model.MsgAlert = msg;

            service.initialModel(ref model);
             
            TempData["CrudeDataModel"] = model;

            return RedirectToAction("Create", "CrudeImportPlan");
             
        }


        public ActionResult Create_plan()
        {
            Session["menu_action"] = "plan";
            //ViewBag.menu_action = "create_Plan";
            return RedirectToAction("Create", "CrudeImportPlan");
        }

        public ActionResult Create_book()
        {
            Session["menu_action"] = "book";
            //ViewBag.menu_action = "create_Plan";
            return RedirectToAction("Create", "CrudeImportPlan");
        }

        public ActionResult Create_all()
        {
            Session["menu_action"] = "all";
            //ViewBag.menu_action = "create_Plan";
            return RedirectToAction("Create", "CrudeImportPlan");
        }

        [HttpGet]
        public ActionResult CreateResult(CrudeImportPlanViewModel model)
        {
            //if (Session["menu_action"] == null)
            //{
            //    Session["menu_action"] = "plan";
            //}
            //CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            //model = initialModel();
            if (model == null)
            {
                model = new CrudeImportPlanViewModel();
                model = initialModel();
            }
            else
            {
                if (model.crude_approve_detail == null)
                {
                    model = new CrudeImportPlanViewModel();
                    model = initialModel();
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            CrudeImportPlanViewModel model;

            if (TempData["CrudeDataModel"] != null)
            {
                model = (CrudeImportPlanViewModel)TempData["CrudeDataModel"];
                TempData.Remove("CrudeDataModel");
            }
            else
            {

                var trip = Request.QueryString["t"];
                if (string.IsNullOrEmpty(trip))
                {
                    model = new CrudeImportPlanViewModel();
                    model = initialModel();

                    if (Session["menu_action"] != null && (Session["menu_action"].ToString() == "book"))
                    {
                        CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                        Decimal ArrivalMonth = DateTime.Now.Month;
                        Decimal ArrivalYear = Convert.ToDecimal(DateTime.Now.ToString("yyyy", new System.Globalization.CultureInfo("en-US")));

                        List<string> MOPSPrdCode = new List<string>();
                        service.SearchCrudeImportPlan("Y", "", ArrivalMonth, ArrivalYear, _CIPConfig, ref model, ref MOPSPrdCode);

                        Session["MOPSPrdCode"] = MOPSPrdCode;
                    }
                    
                }
                else
                {
                    model = initialModel();
                    CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
                    List<string> MOPSPrdCode = new List<string>();
                    service.SearchCrudeImportPlan("", trip, 0, 0, _CIPConfig, ref model, ref MOPSPrdCode);

                    Session["MOPSPrdCode"] = MOPSPrdCode;
                }

            }

            if (Session["menu_action"] == null)
            {
                Session["menu_action"] = "plan";
            }

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Search(CrudeImportPlanViewModel tempModel)
        {

            CrudeImportPlanViewModel model = initialModel();
            CrudeImportPlanServiceModel serviceModel = new CrudeImportPlanServiceModel();
            CrudeImportPlanViewModel_Seach viewModelSearch = new CrudeImportPlanViewModel_Seach();

            viewModelSearch = tempModel.crude_Search;
            serviceModel.Search(ref viewModelSearch, getMatType(), tempModel.crude_Search.sExportSTS);
            model.crude_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public string DeleteFile(string filename = "")
        {
            string ret = "0";
            try
            {
                string rootPath = Request.PhysicalApplicationPath;
                //rootPath = rootPath + "Web/FileUpload/";

                if (!System.IO.Directory.Exists(rootPath))
                    return ret;

                string filepath = rootPath + filename;

                if (!System.IO.File.Exists(filepath))
                    return ret;

                System.IO.File.Delete(filepath);

                ret = "1";

            }
            catch (Exception ex) {
                ret = "0";
            }

            return ret;
        }

        [HttpGet]
        public string TestPost(CrudeImportPlanViewModel tempModel)
        {

            //CrudeImportPlanViewModel model = initialModel();
            //CrudeImportPlanServiceModel serviceModel = new CrudeImportPlanServiceModel();
            //CrudeImportPlanViewModel_Seach viewModelSearch = new CrudeImportPlanViewModel_Seach();

            //viewModelSearch = tempModel.crude_Search;
            //serviceModel.Search(ref viewModelSearch, getMatType());
            //model.crude_Search = viewModelSearch;

            //return View(model);

            return "jate";
        }

        private string getMatType()
        {
            string ret = "CRD";
            if (Session["menu_action"] != null)
            {
                if (Session["menu_action"].ToString() == "all")
                {
                    ret = "PRD";
                }
            }
            return ret;
        }

        public ActionResult Search()
        {
            if (Session["menu_action"] == null)
            {
                Session["menu_action"] = "plan";
            }


            CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            model = initialModel();
            return View(model);
        }

        public ActionResult Create_Swap()
        {
            if (Session["menu_action"] == null)
            {
                Session["menu_action"] = "plan";
            }


            CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            model = initialModel();
            return View(model);
        }

        public ActionResult Search_plan()
        {
            //CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            //model = initialModel();
            //return View(model);

            Session["menu_action"] = "plan";

            return RedirectToAction("Search", "CrudeImportPlan");
        }

        public ActionResult Search_book()
        {
            //CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            //model = initialModel();
            //return View(model);

            Session["menu_action"] = "book";

            return RedirectToAction("Create_book", "CrudeImportPlan");
        }

        public ActionResult Search_all()
        {
            //CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            //model = initialModel();
            //return View(model);

            Session["menu_action"] = "all";

            return RedirectToAction("Search", "CrudeImportPlan");
        }

        public CrudeImportPlanViewModel initialModel()
        {
            //CrudeImportPlanServiceModel serviceModel = new CrudeImportPlanServiceModel();
            CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            //List<SelectListItem> temp = CrudeImportPlanServiceModel.getGlobalConfig();
            model.crude_Search = new CrudeImportPlanViewModel_Seach();
            model.crude_Search.sSearchData = new List<CrudeImportPlanViewModel_Detail>();

            model.crude_approve_detail = new CrudeApproveFormViewModel_Detail();
            model.crude_approve_detail.dDetail = new List<CrudeImportPlanViewModel_Detail>();
            model.crude_approve_detail.dDetail_FreightFile = new List<FileViewModel>();

            model.ddl_CrudeName = CrudeImportPlanServiceModel.getMaterial();
            model.json_Crude = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_CrudeName);
            model.json_CrudeAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(model.ddl_CrudeName);

            
            model.ddl_Supplier = CrudeImportPlanServiceModel.getVendorAll(FEED_TYPE);
            model.json_Supplier = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Supplier);
            model.json_SupplierAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(model.ddl_Supplier);

            List<SelectListItem> list = CrudeImportPlanServiceModel.getVendor("PCF", "CRUDE");
            model.json_VendorCrude = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(list);

            list = CrudeImportPlanServiceModel.getVendor("PCF", "FREIGHT");
            model.json_VendorFreight = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(list);

            list = CrudeImportPlanServiceModel.getVendor("PCF", "SURVEYOR");
            model.json_VendorSurveyor = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(list);

            list = CrudeImportPlanServiceModel.getVendor("PCF", "OTHER");
            model.json_VendorOther = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(list);

            list = CrudeImportPlanServiceModel.getVendor("PCF", "BROKER");
            model.json_VendorBroker = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(list);

            model.ddl_Incoterm = CrudeImportPlanServiceModel.getMTIncoterms();
            model.json_Incoterm = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Incoterm);

            // vessel
            model.ddl_Vehicle = CrudeImportPlanServiceModel.getVehicle("PCF", true, "Select Vessel");
            model.ddl_Vehicle.Insert(0, new SelectListItem
            {
                Value = "SPOT",
                Text = "SPOT",
            });
            model.json_Vehicle = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Vehicle);
            model.json_VehicleAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(model.ddl_Vehicle);

            // loading port
            model.ddl_LoadingPort = CrudeImportPlanServiceModel.getPortName("PORT_PCF", true, "select", "VESSEL");
            model.json_LoadingPort = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_LoadingPort);
            model.json_LoadingPortAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(model.ddl_LoadingPort);

            model.json_AuthorizePage = CrudeImportPlanServiceModel.getGlobalConfig("PCF_AUTHORIZE_PAGE");

            _CIPConfig = CrudeImportPlanServiceModel.getCIPConfig("CIP_IMPORT_PLAN");
            if (_CIPConfig != null)
            {
                model.ddl_Month = CrudeImportPlanServiceModel.getMonth(_CIPConfig.PCF_MONTH);
                model.json_month = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Month);

                model.ddl_PriceStatus = CrudeImportPlanServiceModel.getPriceStatus(_CIPConfig.PCF_MT_PRICE_STATUS);
                model.json_PriceStatus = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_PriceStatus);

                //model.ddl_PurchaseType = CrudeImportPlanServiceModel.getPurchaseType(CIPConfig.PCF_MT_PURCHASE_TYPE);
                //model.json_PurchaseType = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_PurchaseType); 
                JavaScriptSerializer js = new JavaScriptSerializer();
                model.json_PurchaseType = js.Serialize(_CIPConfig.PCF_MT_PURCHASE_TYPE);
                model.json_SearchType = js.Serialize(_CIPConfig.PCF_SEARCH_TYPE);
                model.json_freight_prod = js.Serialize(_CIPConfig.PCF_FREIGHT_PRODUCT);

                model.json_purgroup = js.Serialize(_CIPConfig.PCF_PO_SURVEYOR_PUR_GROUP);

                model.ddl_Channel = CrudeImportPlanServiceModel.getChannel(_CIPConfig.PCF_CHANNEL);
                model.json_Channel = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Channel);

                model.json_InsuranceRate = js.Serialize(_CIPConfig.PCF_MT_INSURANCE_RATE);

                model.ddl_UnitCal = CrudeImportPlanServiceModel.getUnitCal(_CIPConfig.PCF_UNIT_CAL);

                model.json_UnitCal = js.Serialize(_CIPConfig.PCF_UNIT_CAL);

                //model.ddl_StorageLocation = CrudeImportPlanServiceModel.getStorageLocation(CIPConfig.PCF_MT_STORAGE_LOCATION);
                //model.json_StorageLocation = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_StorageLocation); 
                model.json_StorageLocation = js.Serialize(_CIPConfig.PCF_MT_STORAGE_LOCATION);

                model.json_CharteringType = js.Serialize(_CIPConfig.PCF_MT_CHARTERING_TYPE);
            }

            model.ddl_BLStatus = CrudeImportPlanServiceModel.getBLStatus();
            model.json_BLStatus = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_BLStatus);

            model.ddl_Year = CrudeImportPlanServiceModel.getYear();
            model.json_year = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Year);

            model.ddl_Currency = CrudeImportPlanServiceModel.getCurrencyConfig();
            model.json_Currency = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_Currency);


            model.ddl_SurveyorType = CrudeImportPlanServiceModel.GetSurveyorType();
            model.json_SurveyorType = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_SurveyorType);
            model.json_SurveyorConfig = CrudeImportPlanServiceModel.GetSurveyorConfigToJSON();

            model.ddl_OtherCostType = CrudeImportPlanServiceModel.GetOtherCostType();
            model.json_OtherCostType = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_OtherCostType);
            model.json_OtherCostTypeConfig = CrudeImportPlanServiceModel.GetOtherCostConfigToJSON();

            model.ddl_FreightType = CrudeImportPlanServiceModel.GetFreightType();
            model.json_FreightType = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_FreightType);
            model.json_FreightTypeConfig = CrudeImportPlanServiceModel.GetFreightConfigToJSON();

            model.ddl_HolidayType = CrudeImportPlanServiceModel.GetHolidayType();
            model.json_HolidayType = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_HolidayType);

            List<SelectListItem> ddlCountry = DropdownServiceModel.getCountryCrudePurchase();
            model.json_Country = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(ddlCountry);

            model.crude_Search.sExportSTS = "";

            return model;
        }

        [HttpGet]
        public string getPricePeriod(string cda_id = "")
        {
            return CrudeImportPlanServiceModel.getPricePeriod(cda_id);
        }

        [HttpGet]
        public string getFIDocSTS(string tripno = "")
        {
            return CrudeImportPlanServiceModel.getFIDocSTS(tripno);
        }


        [HttpGet]
        public string getCrudeApprove(string ArriavalMonth = "", string LoadingMonth = "", string CrudeName = "", string Supplier = "", string Incoterm = "", string SearchType = "")
        {             

            string user_group ="";
            if (Session["menu_action"] != null)
                user_group = Session["menu_action"].ToString();

            if (user_group == "all")
                SearchType = "PRD";

            return JSonConvertUtil.modelToJson(CrudeImportPlanServiceModel.getCrudeApprove(ArriavalMonth, LoadingMonth, CrudeName, Supplier, Incoterm, SearchType));

        }

        [HttpGet]
        public string CalPrice(string benchmark = "", string met_num = "", string price_status = "", string date_from_to = "", string crudePhet_Vol = "")
        {
           
            return JSonConvertUtil.modelToJson(CrudeImportPlanServiceModel.CalPriceFormula(benchmark, met_num, price_status, date_from_to, crudePhet_Vol));
        }

        [HttpGet]
        public string CalPriceUpdateJson(string sfixedData, string benchmark = "", string met_num = "", string price_status = "", string date_from_to = "", string crudePhet_Vol = "")
        {
            System.Collections.ArrayList fixedData = new System.Collections.ArrayList();
            if (!string.IsNullOrEmpty(sfixedData)) {
                string[] list = sfixedData.Split('|');
                foreach (string p in list) {
                    if (string.IsNullOrEmpty(p))
                        continue;

                    string[] arr = p.Split(',');
                    if (arr.Length < 1)
                        continue;

                    if (string.IsNullOrEmpty(arr[1].Trim()))
                        continue;

                    fixedData.Add(new[] { arr[0], arr[1] });

                }
            }


            return CrudeImportPlanServiceModel.CalPriceFormula_RetJson(benchmark, met_num, price_status, date_from_to, crudePhet_Vol, fixedData);
        }

        [HttpGet]
        public string CalPriceJson(string benchmark = "", string met_num = "", string price_status = "", string date_from_to = "", string crudePhet_Vol = "")
        {
            return CrudeImportPlanServiceModel.CalPriceFormula_RetJson(benchmark, met_num, price_status, date_from_to, crudePhet_Vol);
        }

        [HttpGet]
        public string getVesselMaster()
        {
            List<SelectListItem> Vessel = CrudeImportPlanServiceModel.getVehicle("PCF", true, "Select Vessel");
            string json_Vehicle = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(Vessel);
            string json_VehicleAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(Vessel);


            // loading port
            List<SelectListItem> Port = CrudeImportPlanServiceModel.getPortName("PORT_PCF", true, "select", "VESSEL");
            string json_LoadingPort = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(Port);
            string json_LoadingPortAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(Port);

            return json_Vehicle + "||" + json_VehicleAuto + "||" + json_LoadingPort + "||" + json_LoadingPortAuto;
        }

        [HttpGet]
        public string getDailyPrice(string tripno = "", string matnum = "", string benchmark = "",  string pro_pricePeriod = ""
                                    , string pro_crudePhet_Vol = "", string final_pricePeriod = "", string final_crudePhet_Vol = ""
                                    , string bl_date = "", string completeTime = "", string recal = "0")
        {

            string ret = "";
            
            try
            {
               
                List<DailyPriceViewModel> outturn = null;
                outturn = CrudeImportPlanServiceModel.getDailyPrice(tripno, matnum, benchmark, pro_pricePeriod, pro_crudePhet_Vol, final_pricePeriod, final_crudePhet_Vol, bl_date, completeTime, recal);

                    // for test
                    //priceResult = priceResult + Convert.ToDecimal(item.rowIndex);

                    //item.retsts = "S";
                    //item.retmsg = "";
                    //item.priceVol = priceResult.ToString();
             

                JavaScriptSerializer js = new JavaScriptSerializer();
                ret = js.Serialize(outturn);

            }
            catch (Exception ex)
            {

            }

            return ret;
        }

        [HttpGet]
        public string CalPriceAll(string jsonModelDetail = "")
        {
            string ret = "";

            if (jsonModelDetail == "")
                return "";
            

            try
            {
                CalPricingAllViewModel priceData = null;
                priceData = (CalPricingAllViewModel)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonModelDetail, typeof(CalPricingAllViewModel));

                if (priceData == null)
                    return "";
                              
                
                if (priceData.PricingDataViewModel.Count == 0)
                    return "";

                foreach (var item in priceData.PricingDataViewModel)
                {
                    Decimal priceResult = 0;
                    string benchMark = item.benchMark;
                    string matNum = item.matNum;
                    string priceStatus = item.priceStatus;
                    string pricePeriod = item.pricePeriod;
                    string crudePhetVol = item.crudePhetVol;

                    priceResult = CrudeImportPlanServiceModel.CalPriceFormula(benchMark, matNum, priceStatus, pricePeriod, crudePhetVol);

                    // for test
                    //priceResult = priceResult + Convert.ToDecimal(item.rowIndex);

                    item.retsts = "S";
                    item.retmsg = "";
                    item.priceVol = priceResult.ToString();
                }


                JavaScriptSerializer js = new JavaScriptSerializer();
                ret = js.Serialize(priceData);
            }
            catch (Exception ex)
            {

            }


            return ret;

        }

       

        [HttpGet]        
        public string CalPriceFormulaManual(string periodFrom = "", string periodTo = "")
        {
            CrudeImportPlanServiceModel service = new CrudeImportPlanServiceModel();
            
            List<string> MOPSPrdCode = new List<string>();
            if (Session["MOPSPrdCode"] != null) {
                MOPSPrdCode = (List<string>)Session["MOPSPrdCode"];
            }

            string result = service.getPriceFormulaManual(MOPSPrdCode, periodFrom, periodTo);
           
            return result;
        }

        [HttpGet]
        public string ExchangeReteCalculate(string bl_date = "", string completeTime = "", string exchange_type = "", string transport_type = "")
        {
            FormulaPricingServiceModel service = new FormulaPricingServiceModel();
            string result = service.ExchangeReteCalculatePCF(bl_date, completeTime, exchange_type, transport_type);
            if (!string.IsNullOrEmpty(result))
            {
                result = Convert.ToDecimal(result).ToString("#,##0.###0");
            }
            return result;
        }

        [HttpGet]
        public string ExchangeReteCalculateAll(string bl_date = "", string completeTime = "", string transport_type = "", string cal_type = "", string fx_date_period = "" )
        {
            string ret = "";
            // vessel
            if (cal_type.Equals("all"))
            {
                if (string.IsNullOrEmpty(fx_date_period))
                    return ret;
            }
            else
            {
                if (string.IsNullOrEmpty(bl_date))
                    return ret;
            }

            try
            {
                FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                string result = "";
                if (cal_type.Equals("all"))
                    result = service.ExchangeReteCalculatePCF_V2(fx_date_period, completeTime, "A", transport_type);
                else
                    result = service.ExchangeReteCalculatePCF(bl_date, completeTime, "A", transport_type);

                if (!string.IsNullOrEmpty(result))
                {
                    result = Convert.ToDecimal(result).ToString("#,##0.###0");
                }

                ret = result;
                if (cal_type.Equals("all"))
                    result = service.ExchangeReteCalculatePCF_V2(fx_date_period, completeTime, "B", transport_type);
                else
                    result = service.ExchangeReteCalculatePCF(bl_date, completeTime, "B", transport_type);
                 

                if (!string.IsNullOrEmpty(result))
                {
                    result = Convert.ToDecimal(result).ToString("#,##0.###0");
                }

                ret += "|" + result;
            }
            catch (Exception ex)
            {

            }


            return ret;
        }

        [HttpGet]
        public string CalDueDate(string bl_date = "", string paymenterm = "", string matcode = "", string bankType = "")
        {
            string ret = "";
            // vessel
            if (string.IsNullOrEmpty(bl_date))
            {
                return ret;
            }

            DateTime d_BLDate = DateTime.ParseExact(bl_date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            int n_paymenterm = 0;

            n_paymenterm = string.IsNullOrEmpty(paymenterm) ? 0 : Convert.ToInt32(paymenterm);

            DateTime DueDate = d_BLDate.AddDays(n_paymenterm);
            
            //ret = CrudeImportPlanServiceModel.CalDueDateService(DueDate, matcode);

           ResultModel result = CrudeImportPlanServiceModel.CalDueDateService(DueDate, bankType, matcode);
            if (result != null) {
                JavaScriptSerializer js = new JavaScriptSerializer();
                ret = js.Serialize(result);
            }

            return ret;
        }

        [HttpGet]
        public string CalLumpsumChartering(string type = "")
        {
            string ret = "0";

            return ret;
        }

        [HttpGet]
        public string checkSupplierInSAP(string code = "")
        {
            string ret = "0";
            ret = CrudeImportPlanServiceModel.checkSupplierInSAP(code);
            return ret;
        }

        [HttpGet]
        public string checkChartering(string tripnoList = "")
        {
            string ret = "0";
            try
            {

                string tripno = "";
                if (tripnoList == "")
                    return ret;

                string[] arrTrip = tripnoList.Split('|');
                foreach (var d in arrTrip) {
                    if (d != "") {
                        if (tripno != "") tripno += ", ";

                        tripno += "'"+ d +"'";
                    }
                }

                if (tripno == "")
                    return ret;

                tripno = " UPPER(TRIP_NO)  in ( " + tripno + " )";
                FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                List<CharteringOutbound> result = service.getChartering(tripno);

                if (result != null && result.Count() > 0)
                    ret = "1";

            }
            catch(Exception ex)
            {

            }
            return ret;
        }

        [HttpGet]
        public string getChartering(string tripno = "", string pono = "")
        {
            string ret = "";
            try
            {
                FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                string sqlCondi = "";
                if (!string.IsNullOrEmpty(tripno))
                {
                    sqlCondi = " UPPER(TRIP_NO)  = '" + tripno + "' ";
                }
                else if (!string.IsNullOrEmpty(pono))
                {
                    sqlCondi = " UPPER(ida_purchase_no)  = '" + pono + "' ";
                }


                List<CharteringOutbound> result = service.getChartering(sqlCondi);
                WSDataModel mOut = null;
                if (result != null && result.Count() > 0)
                {
                    mOut = new WSDataModel();
                    mOut.VesselCode = result[0].VesselCode ?? "";
                    mOut.VesselName = result[0].VesselName ?? "";
                    mOut.FlatRate = result[0].FlatRate ?? "";
                    mOut.Laytime = result[0].LayTime ?? "";
                    mOut.FreightType = result[0].FreightType ?? "";

                    foreach (var it in result)
                    {

                        if (it.RoundType.Equals("Min loaded"))
                            mOut.MinLoad = it.RoundVol ?? "";
                        else if (it.RoundType.Equals("Dem"))
                            mOut.Dem = it.RoundVol ?? "";
                        else if (it.RoundType.Equals("WS"))
                            mOut.WS = it.RoundVol ?? "";

                        if (!string.IsNullOrEmpty(pono))
                        {
                            if (it.RoundType.Equals("WS"))
                                mOut.WS = it.RoundVol ?? "";

                            if (it.RoundType.Equals("Lumpsum"))
                                mOut.Total = it.Total ?? "";
                        }

                    }

                }

                if (mOut != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ret = js.Serialize(mOut);
                } 

            }
            catch (Exception ex)
            {

            }

            return ret;

        }

        [HttpGet]
        public string SearchChartering(string pono = "")
        {
            string ret = "";
            try
            {
                FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                string sqlCondi = "";
                sqlCondi = " UPPER(ida_purchase_no)  LIKE  '%" + pono.ToUpper() + "%' ";


                List<CharteringOutbound> result = service.searchChartering(sqlCondi);
                List<WSDataModel> mOut = new List<WSDataModel>();
                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result) {
                        mOut.Add(new WSDataModel
                        {
                            VesselCode = item.VesselCode ?? "",
                            VesselName = item.VesselName ?? "",
                            VendorName = item.VendorName ?? "",
                            PurchaseNo = item.PurchaseNo ?? "",
                            FreightType = item.FreightType ?? "",
                        });
                    }
                }

                if (mOut != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    ret = js.Serialize(mOut);
                }

            }
            catch (Exception ex)
            {

            }

            return ret;

        }


        public ActionResult ADAuthen()
        {
            var trip = Request.QueryString["t"] == null ? "" : Request.QueryString["t"];
            var comcode = Request.QueryString["comcode"] == null ? "" : Request.QueryString["comcode"];
            comcode = (comcode == "" ? "1100" : comcode);

            var ym = Request.QueryString["ym"] == null ? "" : Request.QueryString["ym"];

            // return Redirect("../../web/DIRECTPAGE.aspx?MEUROWID=PCF0102&t=" + trip + "&ym=" + ym);
            return Redirect("../../web/DirectActionPage.aspx?MEUROWID=PCF0102&t=" + trip + "&ym=" + ym);
        }



    }
}