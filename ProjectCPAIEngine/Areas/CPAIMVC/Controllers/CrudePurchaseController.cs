﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.DAL.DALBunker;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Model;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL;
using System.Drawing;
using System.IO;
using OfficeOpenXml;
using Spire.Xls;
using OfficeOpenXml.Style;
using ProjectCPAIEngine.DAL.DALCrudePurchase;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API;
using Newtonsoft.Json;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{

    public class CrudePurchaseController : BaseController
    {
        public List<ButtonAction> ButtonListCrudePurchase
        {
            get
            {
                if (Session["ButtonListCrudePurchase"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListCrudePurchase"];
            }
            set { Session["ButtonListCrudePurchase"] = value; }
        }
        ShareFn _FN = new ShareFn();
        const string returnPage = "../web/MainBoards.aspx";
        const string JSON_CRUDE_PURCHASE = "JSON_CRUDE_PURCHASE";

        public ActionResult Index()
        {
            CrudePurchaseViewModel model;
            if (TempData["CrudePurchase_Model"] == null)
            {
                model = new CrudePurchaseViewModel();
                model.pageMode = "EDIT"; //PageMode is EDIT, EDIT_REASON or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCrudePurchase(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }

                    if (Request.QueryString["Reason"] != null)
                    {
                        model.reason = Request.QueryString["Reason"].ToString().Decrypt();
                    }
                    if (model.proposal == null)
                    {
                        model.proposal = new CdpProposal();
                    }
                    if (resData != null)
                    {
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            TempData["returnPage"] = returnPage;
                        }
                    }
                }
                else
                {
                    model.nrdoDay = "DayOne";
                    model.payment_terms = new CdpPaymentTerms();
                    model.payment_terms.payment_term = getPaymentTerms()[1].Text;
                    model.payment_terms.payment_term_detail = "30";
                    if (model.proposal == null)
                    {
                        model.proposal = new CdpProposal();
                    }
                    model.requested_by = GetRequesterFullName(Const.User.UserName);
                }
            }
            else
            {
                model = TempData["CrudePurchase_Model"] as CrudePurchaseViewModel;
                model.reason = TempData["Crude_Reason"] as string;
                TempData["CrudePurchase_Model"] = null;
                TempData["Crude_Reason"] = null;
            }
            initializeDropDownList(ref model);
            var findPtt = model.ddlSupplier.SingleOrDefault(a => a.Text == "PTT Public Company Limited");
            if (findPtt != null)
            {
                model.ptt_id = findPtt.Value;
            }
            string sql = "select * from CTR.VIEW_CTR_BOND_MASTER";
            System.Data.DataSet ds = new System.Data.DataSet();
            var strCon = System.Configuration.ConfigurationManager.ConnectionStrings["EntitiesEngine"].ConnectionString;
            int startIndex = strCon.LastIndexOf("DATA SOURCE");
            int endIndex = strCon.Length - 1;
            int length = endIndex - startIndex;
            strCon = strCon.Substring(startIndex, length);
            var con = new Oracle.ManagedDataAccess.Client.OracleConnection(strCon);
            Oracle.ManagedDataAccess.Client.OracleDataAdapter ad = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(sql, strCon);
            ad.Fill(ds, "PERFORMANCE BOND");
            con.Close();
            model.ListPERFORMANCEBOND = new List<PERFORMANCEBOND>();
            if (ds.Tables != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string code = ds.Tables[0].Rows[i]["SUPPLIERCODE"].ToString();
                    string bond = ds.Tables[0].Rows[i]["PERFORMANCEBOND"].ToString();
                    model.ListPERFORMANCEBOND.Add(new PERFORMANCEBOND { SUPPLIERCODE = code, BOND = bond });
                }
            }
            if (Request.QueryString["PDF"] != null)
            {
                model.saveAndPreview = "PDF";
            }
            //if(model.offers_items != null)
            //{
            //    for(var i =0; i < model.offers_items.Count();i++)
            //    {
            //        if(model.offers_items[i].final_flag == "Y")
            //        {
            //            model.select_New_Name = (i +1).ToString();
            //        }
            //    }
            //}

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, CrudePurchaseViewModel model)
        {
            model.offered_by_ptt = model.offered_by_ptt == null ? "N" : model.offered_by_ptt;

            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            string attached_explain = form["hdfExplanFileUpload"];
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            this.updateModel(form, model);
            ResponseData resData = SaveDataCrudePurchase(_status, model, note, json_uploadFile, attached_explain);
            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);
            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string purno = resData.resp_parameters[0].v.Encrypt();
                    string reason = model.reason.Encrypt();
                    string path = string.Format("~/CPAIMVC/CrudePurchase?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Reason={3}", tranID, reqID, purno, reason);
                    return Redirect(path);
                }
            }
            string sql = "select * from CTR.VIEW_CTR_BOND_MASTER";
            System.Data.DataSet ds = new System.Data.DataSet();
            var strCon = System.Configuration.ConfigurationManager.ConnectionStrings["EntitiesEngine"].ConnectionString;
            int startIndex = strCon.LastIndexOf("DATA SOURCE");
            int endIndex = strCon.Length - 1;
            int length = endIndex - startIndex;
            strCon = strCon.Substring(startIndex, length);
            var con = new Oracle.ManagedDataAccess.Client.OracleConnection(strCon);
            Oracle.ManagedDataAccess.Client.OracleDataAdapter ad = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(sql, strCon);
            ad.Fill(ds, "PERFORMANCE BOND");
            con.Close();
            model.ListPERFORMANCEBOND = new List<PERFORMANCEBOND>();
            if (ds.Tables != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string code = ds.Tables[0].Rows[i]["SUPPLIERCODE"].ToString();
                    string bond = ds.Tables[0].Rows[i]["PERFORMANCEBOND"].ToString();
                    model.ListPERFORMANCEBOND.Add(new PERFORMANCEBOND { SUPPLIERCODE = code, BOND = bond });
                }
            }
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, CrudePurchaseViewModel model)
        {
            if (model.offers_items != null)
            {
                if (model.offers_items[0].supplier == "")
                {
                    model.ddlSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
                    var newSuppName = model.ddlSupplier.SingleOrDefault(a => a.Text == model.supplier_New_Name);
                    if (newSuppName != null)
                    {
                        model.supplier_New_Name = newSuppName.Value;
                    }
                    model.offers_items[0].supplier = model.supplier_New_Name;
                }
            }

            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            string attached_explain = form["hdfExplanFileUpload"];
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            this.updateModel(form, model);

            ResponseData resData = SaveDataCrudePurchase(_status, model, note, json_uploadFile, attached_explain);
            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);
            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    if (model.saveAndPreview == "PDF")
                    {
                        string tranID = resData.transaction_id.Encrypt();
                        string reqID = resData.req_transaction_id.Encrypt();
                        string purno = resData.resp_parameters[0].v.Encrypt();
                        string reason = model.reason.Encrypt();
                        string pdf = "PDF";
                        string path = string.Format("~/CPAIMVC/CrudePurchase?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Reason={3}&PDF={4}", tranID, reqID, purno, reason, pdf);
                        return Redirect(path);
                    }
                    else
                    {
                        string tranID = resData.transaction_id.Encrypt();
                        string reqID = resData.req_transaction_id.Encrypt();
                        string purno = resData.resp_parameters[0].v.Encrypt();
                        string reason = model.reason.Encrypt();
                        string path = string.Format("~/CPAIMVC/CrudePurchase?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Reason={3}", tranID, reqID, purno, reason);
                        return Redirect(path);
                    }
                }
            }
            string sql = "select * from CTR.VIEW_CTR_BOND_MASTER";
            System.Data.DataSet ds = new System.Data.DataSet();
            var strCon = System.Configuration.ConfigurationManager.ConnectionStrings["EntitiesEngine"].ConnectionString;
            int startIndex = strCon.LastIndexOf("DATA SOURCE");
            int endIndex = strCon.Length - 1;
            int length = endIndex - startIndex;
            strCon = strCon.Substring(startIndex, length);
            var con = new Oracle.ManagedDataAccess.Client.OracleConnection(strCon);
            Oracle.ManagedDataAccess.Client.OracleDataAdapter ad = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(sql, strCon);
            ad.Fill(ds, "PERFORMANCE BOND");
            con.Close();
            model.ListPERFORMANCEBOND = new List<PERFORMANCEBOND>();
            if (ds.Tables != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string code = ds.Tables[0].Rows[i]["SUPPLIERCODE"].ToString();
                    string bond = ds.Tables[0].Rows[i]["PERFORMANCEBOND"].ToString();
                    model.ListPERFORMANCEBOND.Add(new PERFORMANCEBOND { SUPPLIERCODE = code, BOND = bond });
                }
            }
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenPDF")]
        public ActionResult GenPDF(FormCollection form, CrudePurchaseViewModel model)
        {
            string path = "~/Web/Report/CrudePurchaseReport.aspx";
            if (Request.QueryString["TranID"] != null && Request.QueryString["isDup"] == null)
            {
                string tranID = Request.QueryString["TranID"].ToString();
                //LoadDataCharterIn(tranID, ref model);
                path += "?TranID=" + tranID;
            }
            else
            {
                this.updateModel(form, model, true);

                CdpRootObject obj = new CdpRootObject();
                obj.approve_items = model.approve_items;
                obj.compet_offers_items = model.compet_offers_items;
                obj.contract_period = model.contract_period;
                obj.crude_id = model.crude_id;
                obj.crude_name = model.crude_name;
                obj.crude_name_others = model.crude_name_others;
                obj.date_purchase = model.date_purchase;
                obj.plan_month = model.plan_month;
                obj.plan_year = model.plan_year;
                obj.discharging_period = model.discharging_period;
                obj.explanation = model.explanation;
                obj.explanationAttach = string.IsNullOrEmpty(model.explanationAttach) ? form["hdfExplanFileUpload"] : model.explanationAttach;
                obj.feedstock = model.feedstock;
                obj.feedstock_others = model.feedstock_others;
                obj.for_feedStock = model.for_feedstock;
                obj.loading_period = model.loading_period;
                obj.notes = model.notes;
                obj.offers_items = model.offers_items;
                obj.origin = model.origin;
                obj.origin_id = model.origin_id;
                obj.other_condition = model.other_condition;
                obj.payment_terms = model.payment_terms;
                obj.proposal = model.proposal;
                obj.purchase = model.purchase;
                obj.reason = model.reason;
                obj.term = model.term;
                obj.term_others = model.term_others;
                obj.gtc = model.gtc;

                if (model.feedstock != "CRUDE")
                {
                    obj.crude_name = obj.crude_name_others;
                }

                //var json = new JavaScriptSerializer().Serialize(obj);
                Session["CdpRootObject"] = obj;
            }

            return Redirect(path);
        }



        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, CrudePurchaseViewModel model)
        {
            this.updateModel(form, model);
            if (ButtonListCrudePurchase != null)
            {
                var _lstButton = ButtonListCrudePurchase.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    //string note = _lstButton[0].name.ToUpper() == "REJECT" || _lstButton[0].name.ToUpper() == "CANCEL" ? form["hdfNoteAction"] : "-";
                    string temp = form["hdfNoteAction"];
                    string note = string.IsNullOrEmpty(temp) ? "-" : temp;

                    CrudePurchaseServiceModel crudeService = new CrudePurchaseServiceModel();
                    ReturnValue rtn = new ReturnValue();
                    rtn = crudeService.AddCrudeNote(model, lbUserName, model.taskID);
                    if (rtn.Status == false)
                    {
                        TempData["res_message"] = rtn.Message;
                        TempData["CrudePurchase_Model"] = model;
                        return RedirectToAction("Index", "CrudePurchase");
                    }

                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    model.explanationAttach = form["hdfExplanFileUpload"];
                    var json = new JavaScriptSerializer().Serialize(model);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string purno = resData.resp_parameters[0].v.Encrypt();
                            string reason = model.reason.Encrypt();
                            string path = string.Format("~/CPAIMVC/CrudePurchase?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Reason={3}", tranID, reqID, purno, reason);
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            initializeDropDownList(ref model);
            string sql = "select * from CTR.VIEW_CTR_BOND_MASTER";
            System.Data.DataSet ds = new System.Data.DataSet();
            var strCon = System.Configuration.ConfigurationManager.ConnectionStrings["EntitiesEngine"].ConnectionString;
            int startIndex = strCon.LastIndexOf("DATA SOURCE");
            int endIndex = strCon.Length - 1;
            int length = endIndex - startIndex;
            strCon = strCon.Substring(startIndex, length);
            var con = new Oracle.ManagedDataAccess.Client.OracleConnection(strCon);
            Oracle.ManagedDataAccess.Client.OracleDataAdapter ad = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(sql, strCon);
            ad.Fill(ds, "PERFORMANCE BOND");
            con.Close();
            model.ListPERFORMANCEBOND = new List<PERFORMANCEBOND>();
            if (ds.Tables != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string code = ds.Tables[0].Rows[i]["SUPPLIERCODE"].ToString();
                    string bond = ds.Tables[0].Rows[i]["PERFORMANCEBOND"].ToString();
                    model.ListPERFORMANCEBOND.Add(new PERFORMANCEBOND { SUPPLIERCODE = code, BOND = bond });
                }
            }
            TempData["CrudePurchase_Model"] = model;
            return RedirectToAction("Index", "CrudePurchase");
        }

        public ActionResult Search()
        {
            CrudePurchaseSearchViewModel CrudePurchaseModel = new CrudePurchaseSearchViewModel();
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getCrude = MaterialsServiceModel.getMaterialsDDL();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();


            return View(CrudePurchaseModel);
        }

        public ActionResult Search_New()
        {
            CrudePurchaseSearchViewModel CrudePurchaseModel = new CrudePurchaseSearchViewModel();
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getCrude = MaterialsServiceModel.getMaterialsDDL();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();


            return View(CrudePurchaseModel);
        }


        public ActionResult Search_Bond()
        {
            CrudePurchaseSearchBondViewModel CrudePurchaseModel = new CrudePurchaseSearchBondViewModel();
            CrudePurchaseModel.CrudePurchases = new List<CrudePurchaseViewModel_SearchBond>();
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            ViewBag.getStatuses = new List<SelectListItem>() { new SelectListItem() { Text = "WAITING BOND DOCS", Value = "WAITING BOND DOCS" }, new SelectListItem() { Text = "WAIVED", Value = "WAIVED" }, new SelectListItem() { Text = "COMPLETED", Value = "COMPLETED" } };
            //ViewBag.getCrudes = DropdownServiceModel.getMaterial();
            ViewBag.getCrudes = MaterialsServiceModel.getMaterialsDDL();

            ViewBag.getSuppliers = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();


            return View(CrudePurchaseModel);
        }

        [HttpGet]
        public ActionResult Report()
        {
            CrudePurchaseReportViewModel model = initialReportModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Report(CrudePurchaseReportViewModel pModel)
        {

            //CrudePurchaseReportViewModel model = initialReportModel();

            CrudePurchaseServiceModel serviceModel = new CrudePurchaseServiceModel();
            CrudePurchaseReportViewModel_Search viewModelSearch = new CrudePurchaseReportViewModel_Search();

            viewModelSearch = pModel.cdp_Search;
            serviceModel.Search(ref viewModelSearch);
            pModel.cdp_Search = viewModelSearch;

            if (!String.IsNullOrEmpty(pModel.ExportTo))
                pModel.ActionMSG = GenarateFile(pModel);


            return View(pModel);
        }


        [HttpPost]
        public ActionResult SearchResult(CrudePurchaseSearchViewModel CrudePurchaseModel)
        {
            //CrudePurchaseSearchViewModel CrudePurchaseModel = new CrudePurchaseSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            //userList = userList.Distinct().ToArray();

            userList = userList.Distinct().ToList();
            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getCrude = MaterialsServiceModel.getMaterialsDDL();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();

            string datePurchase = CrudePurchaseModel.search_DatePurchase;
            string stDate = "";
            string enDate = "";
            //string feedstock = fm["feedstock"].ToString();
            //string product = fm["product"].ToString();
            //string supplier = fm["supplier"].ToString();
            string userlist = "";
            if (CrudePurchaseModel.search_User != null)
            {
                userlist = CrudePurchaseModel.search_User.ToString().Replace(",", "|");
            }

            if (!String.IsNullOrEmpty(datePurchase))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                enDate = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            List_CrudePurchasetrx res = SearchCrudePurchaseData(stDate, enDate, CrudePurchaseModel.search_Stock, CrudePurchaseModel.search_Product, CrudePurchaseModel.search_Supplier, userlist);
            CrudePurchaseModel.CrudePurchaseTransaction = res.CrudePurchaseTransaction.OrderByDescending(x => x.transaction_id).ToList();

            for (int i = 0; i < res.CrudePurchaseTransaction.Count; i++)
            {
                CrudePurchaseModel.CrudePurchaseTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(CrudePurchaseModel.CrudePurchaseTransaction[i].date_purchase, "MMMM dd yyyy");
                CrudePurchaseModel.CrudePurchaseTransaction[i].Transaction_id_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].transaction_id.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].Req_transaction_id_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].req_transaction_id.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].Purchase_no_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].purchase_no.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].reason_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].reason.Encrypt();
            }

            return View("Search", CrudePurchaseModel);
        }

        [HttpPost]
        public ActionResult SearchResult_New(FormCollection fm)
        {
            CrudePurchaseSearchViewModel CrudePurchaseModel = new CrudePurchaseSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            //userList = userList.Distinct().ToArray();

            userList = userList.Distinct().ToList();
            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getCrude = MaterialsServiceModel.getMaterialsDDL();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();

            string datePurchase = fm["datePurchaseField"].ToString();
            string stDate = "";
            string enDate = "";
            string feedstock = fm["feedstock"].ToString();
            string product = fm["product"].ToString();
            string supplier = fm["supplier"].ToString();
            string userlist = "";
            if (fm["userList"] != null)
            {
                userlist = fm["userList"].ToString().Replace(",", "|");
            }

            if (!datePurchase.Equals(""))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                enDate = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            List_CrudePurchasetrx res = SearchCrudePurchaseData(stDate, enDate, feedstock, product, supplier, userlist);
            CrudePurchaseModel.CrudePurchaseTransaction = res.CrudePurchaseTransaction.OrderByDescending(x => x.transaction_id).ToList();

            for (int i = 0; i < res.CrudePurchaseTransaction.Count; i++)
            {
                CrudePurchaseModel.CrudePurchaseTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(CrudePurchaseModel.CrudePurchaseTransaction[i].date_purchase, "MMMM dd yyyy");
                CrudePurchaseModel.CrudePurchaseTransaction[i].Transaction_id_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].transaction_id.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].Req_transaction_id_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].req_transaction_id.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].Purchase_no_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].purchase_no.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].reason_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].reason.Encrypt();
            }
            return View("Search_New", CrudePurchaseModel);
        }

        [HttpPost]
        public ActionResult Search_Bond_Doc(CrudePurchaseSearchBondViewModel model)
        {
            CrudePurchaseServiceModel service = new CrudePurchaseServiceModel();
            ReturnValue rtn = new ReturnValue();
            //model = service.GetCrudePurchasesWithBond(model.Search_datePurchase,model.Search_statuses,model.Search_crudes,model.Search_suppliers,model.Search_createdByUser);
            rtn = service.GetCrudePurchasesWithBond_New(ref model);


            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            userList = userList.Distinct().ToList();
            ViewBag.getStatuses = new List<SelectListItem>() { new SelectListItem() { Text = "Waiting Bond Doc", Value = "WAITING" }, new SelectListItem() { Text = "Waived", Value = "WAIVED" }, new SelectListItem() { Text = "Completed", Value = "Completed" } };
            ViewBag.getCrudes = DropdownServiceModel.getMaterial();
            ViewBag.getSuppliers = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();

            return View("Search_Bond", model);
        }

        [HttpPost]
        public ActionResult searchResult_Bond(FormCollection fm)
        {
            CrudePurchaseSearchBondViewModel CrudePurchaseModel = new CrudePurchaseServiceModel().GetCrudePurchasesWithBond("", "", "", "", "");
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            userList = userList.Distinct().ToList();
            ViewBag.getStatuses = new List<SelectListItem>() { new SelectListItem() { Text = "WAITING BOND DOCS", Value = "WAITING" }, new SelectListItem() { Text = "WAIVED", Value = "WAIVED" }, new SelectListItem() { Text = "COMPLETED", Value = "COMPLETED" } };
            ViewBag.getCrudes = DropdownServiceModel.getMaterial();
            ViewBag.getSuppliers = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            ViewBag.getUsers = userList.Distinct().ToList();

            return View("Search_Bond", CrudePurchaseModel);
        }

        [HttpPost]
        public string DeleteFileUploaded(string tranId, string filename)
        {
            string rslt = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        var oldfile = context.CDP_ATTACH_FILE.Where(a => a.CAF_FK_CDP_DATA == tranId && a.CAF_TYPE == "BND").ToList();
                        string newPath = "";
                        foreach (var item in oldfile)
                        {
                            String[] fileData = item.CAF_PATH.Split(new Char[] { '|' });
                            if (fileData.Count() == 1)
                            {
                                context.CDP_ATTACH_FILE.Remove(item);
                            }
                            else
                            {
                                var i = 0;
                                foreach (var z in fileData)
                                {
                                    if (z != filename)
                                    {
                                        var countData = fileData.Count();
                                        if ((fileData.Count() - 1) == 1)
                                        {
                                            newPath = z;
                                        }
                                        else
                                        {
                                            if (i == (countData - 2))
                                            {
                                                newPath += z;
                                            }
                                            else
                                            {
                                                newPath += z + "|";
                                            }
                                        }
                                        i++;
                                    }
                                }
                                item.CAF_PATH = newPath;
                            }
                        }
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return rslt;
        }

        [HttpPost]
        public string searchResult_BondJson(string pDates, string pStatus, string pCrude, string pSupplier, string pUsers)
        {
            string rslt = "";

            CrudePurchaseSearchBondViewModel CrudePurchaseModel = new CrudePurchaseServiceModel()
                .GetCrudePurchasesWithBond(pDates, pStatus, pCrude, pSupplier, pUsers);

            rslt = new JavaScriptSerializer().Serialize(CrudePurchaseModel.CrudePurchases);
            return rslt;
        }

        [HttpPost]
        public string UpdateBondDocFilesUploaded(string pCrudePurchaseRowId, string pBondDocFiles, string pStateName)
        {
            CrudePurchaseViewModel model = new CrudePurchaseViewModel();
            ResponseData rspModelData = LoadDataCrudePurchase(pCrudePurchaseRowId, ref model);

            CdpRootObject obj = new CdpRootObject();
            obj.approve_items = model.approve_items;
            obj.compet_offers_items = model.compet_offers_items;
            obj.contract_period = model.contract_period;
            obj.crude_id = model.crude_id;
            obj.crude_name = model.crude_name;
            obj.crude_name_others = model.crude_name_others;
            obj.date_purchase = model.date_purchase;
            obj.plan_month = model.plan_month;
            obj.plan_year = model.plan_year;
            obj.discharging_period = model.discharging_period;
            obj.explanation = model.explanation;
            obj.explanationAttach = model.explanationAttach;
            obj.feedstock = model.feedstock;
            obj.feedstock_others = model.feedstock_others;
            obj.for_feedStock = model.for_feedstock;
            obj.loading_period = model.loading_period;
            obj.notes = model.notes;
            obj.offers_items = model.offers_items;
            obj.origin = model.origin;
            obj.origin_id = model.origin_id;
            obj.other_condition = model.other_condition;
            obj.payment_terms = model.payment_terms;
            obj.proposal = model.proposal;
            obj.purchase = model.purchase;
            obj.reason = model.reason;
            obj.term = model.term;
            obj.term_others = model.term_others;
            obj.gtc = model.gtc;
            obj.bond_documents = pBondDocFiles;
            obj.performance_bond = model.proposal.performance_bond;

            obj.CrudeSale_Optimization = model.CrudeSale_Optimization;
            obj.CrudeSale_Ref = model.CrudeSale_Ref;

            var json = new JavaScriptSerializer().Serialize(obj);

            string tReqTranId = new FunctionTransactionDAL().GetReqTransIDByTransactionId(pCrudePurchaseRowId);

            string rslt = "";

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000023;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = tReqTranId;

            req.state_name = pStateName;
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            if (req.state_name.Equals("CPAICDPHSetBondStatusState"))
            {
                req.req_parameters.p.Add(new p { k = "current_action", v = "APPROVE_4" });
                req.req_parameters.p.Add(new p { k = "next_status", v = "WAITING BOND DOC" });
            }
            else
            {
                req.req_parameters.p.Add(new p { k = "current_action", v = "UPLOAD" });
                req.req_parameters.p.Add(new p { k = "next_status", v = "COMPLETED" });
                //req.req_parameters.p.Add(new p { k = "current_action", v = "WAIVE" });
                //req.req_parameters.p.Add(new p { k = "next_status", v = "WAIVED" });
            }

            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();


            resData = service.CallService(req);
            AjaxRespons ajr = new AjaxRespons();
            ajr.Status = resData.result_desc.Contains("Success") ? "Success" : "Error";
            if (ajr.Status.Equals("Success"))
            {
                if (pStateName.Equals("CPAIVerifyCDPHRequireInputContinueState"))
                {
                    ajr.Data = new object[3];
                    string tTripNo = "";
                    ajr.Data[0] = new CrudePurchaseServiceModel().GetCrudePurchasesBondDocFilesPath(pCrudePurchaseRowId, ref tTripNo);
                    ajr.Data[1] = pCrudePurchaseRowId;
                    ajr.Data[2] = tTripNo;
                }
                else
                {
                    ajr.Data = new object[1];
                    ajr.Data[0] = resData.result_code + ":" + resData.response_message;
                }

            }
            ajr.ErrorMessage = resData.result_desc;
            rslt = new JavaScriptSerializer().Serialize(ajr);
            return rslt;
        }



        //private string [] UpdateBondDocFilesUploaded_GetUploadedFilePath(string pCrudePurchaseRowId, string pTripNo)
        //{
        //    string[] rslt = new string[] { };

        //    return rslt;
        //}

        private void updateModel(FormCollection form, CrudePurchaseViewModel model, bool isTextMode = false)
        {
            //Assign round_no, order no., order type and final flag | form["offers_items_final_flag"] != null
            List<SelectListItem> suplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            List<SelectListItem> crude = DropdownServiceModel.getMaterial();
            if (model.offers_items != null)
            {
                for (int i = 0; i < model.offers_items.Count; i++)
                {
                    if (form["offers_items_final_flag"] != null)
                    {
                        if (form["offers_items_final_flag"].ToString() == i + "")
                        {
                            model.offers_items[i].final_flag = "Y";
                        }
                        else
                        {
                            model.offers_items[i].final_flag = "N";
                        }
                    }
                    else
                    {
                        model.offers_items[i].final_flag = "N";
                    }

                    //if (model.select_New_Name != null)
                    //{
                    //    var indexSelectName = int.Parse(model.select_New_Name);
                    //    if ((indexSelectName - 1) == i)
                    //    {
                    //        model.offers_items[i].final_flag = "Y";
                    //    }
                    //    else
                    //    {
                    //        model.offers_items[i].final_flag = "N";
                    //    }
                    //    //model.offers_items[indexSelectName - 1].final_flag = "Y";
                    //}

                    model.offers_items[i].supplier_name = suplier.Where(x => x.Value == model.offers_items[i].supplier).FirstOrDefault() == null ? "" : suplier.Where(x => x.Value == model.offers_items[i].supplier).FirstOrDefault().Text;

                    for (int j = 0; j < model.offers_items[i].round_items.Count; j++)
                    {
                        model.offers_items[i].round_items[j].round_no = (j + 1).ToString();
                    }

                    model.offers_items[i].quantity_unit = getFeedStock(true).Where(x => x.Text == model.feedstock).Select(x => x.Value).FirstOrDefault();
                    model.offers_items[i].margin_unit = "$/bbl";
                }
            }

            if (model.compet_offers_items != null)
            {
                for (int i = 0; i < model.compet_offers_items.Count; i++)
                {
                    model.compet_offers_items[i].crude_name = crude.Where(x => x.Value == model.compet_offers_items[i].crude_id).FirstOrDefault() == null ? "" : crude.Where(x => x.Value == model.compet_offers_items[i].crude_id).FirstOrDefault().Text;
                    model.compet_offers_items[i].supplier_name = suplier.Where(x => x.Value == model.compet_offers_items[i].supplier).FirstOrDefault() == null ? "" : suplier.Where(x => x.Value == model.compet_offers_items[i].supplier).FirstOrDefault().Text;

                    for (int j = 0; j < model.compet_offers_items[i].round_items.Count; j++)
                    {
                        model.compet_offers_items[i].round_items[j].round_no = (j + 1).ToString();
                    }

                    model.compet_offers_items[i].quantity_unit = getFeedStock(true).Where(x => x.Text == model.feedstock).Select(x => x.Value).FirstOrDefault();
                    model.compet_offers_items[i].margin_unit = "$/bbl";
                }
            }

            model.contract_period = new CdpContractPeriod();
            model.loading_period = new CdpLoadingPeriod();
            model.discharging_period = new CdpDischargingPeriod();

            if (!string.IsNullOrEmpty(model.contract))
            {
                string[] pa_contract = model.contract.Split(' ');
                model.contract_period.date_from = pa_contract[0];
                model.contract_period.date_to = pa_contract[2];
            }

            if (!string.IsNullOrEmpty(model.loading))
            {
                string[] pa_loading = model.loading.Split(' ');
                model.loading_period.date_from = pa_loading[0];
                model.loading_period.date_to = pa_loading[2];
            }

            if (!string.IsNullOrEmpty(model.discharging))
            {
                string[] pa_discharging = model.discharging.Split(' ');
                model.discharging_period.date_from = pa_discharging[0];
                model.discharging_period.date_to = pa_discharging[2];
            }

            if (!string.IsNullOrEmpty(model.price_period))
            {
                string[] price_period = model.price_period.Split(' ');
                model.proposal.price_period_from = price_period[0];
                model.proposal.price_period_to = price_period[2];
            }

            model.payment_terms.sub_payment_term = model.nrdoDay;

            if (!model.purchase.Equals("Private negotiation"))
            {
                model.supply_source = null;
            }

            //Assign reason
            model.reason = (form["hdfNoteAction"] != null) ? form["hdfNoteAction"].ToString() : (Request.QueryString["Reason"] != null) ? Request.QueryString["Reason"].ToString().Decrypt() : "";
        }

        private void initializeDropDownList(ref CrudePurchaseViewModel model)
        {
            model.ddlForFeedstock = getForFeedStock();
            model.ddlFeedstock = getFeedStock();
            model.ddlHiddenFeedstock = getFeedStock(true);
            model.ddlTerm = getTerm();
            model.ddlPurchase = getPurchase();
            model.ddlIncoterms = getIncoterms();
            model.ddlBenchmarkPrice = getBenchmarkPrice();
            model.ddlPaymentTerms = getPaymentTerms();
            model.ddlProposalReason = getProposalReason();
            model.ddlCrude = MaterialsServiceModel.getMaterialsDDL();
            model.ddlCountry = DropdownServiceModel.getCountryByCrudePurchase();
            model.ddlSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            model.ddlFormula_incoterms = DropdownServiceModel.getIncoterms();

            model.ddlCrudeCountry = DropdownServiceModel.getMaterialCountry();
            model.ddlToleranceOption = getToleranceOption();
            model.ddlToleranceType = getToleranceType();
            model.ddlSupplySource = getSupplySources();
            //model.supplier_bond_rates = getSupplierBondRates();

            model.ddlOfferVia = new List<SelectListItem>() { new SelectListItem() { Text = "Phone", Value = "Phone" }, new SelectListItem() { Text = "Email", Value = "Email" }, new SelectListItem() { Text = "Other", Value = "Other" } };

        }

        public CrudePurchaseReportViewModel initialReportModel()
        {
            CrudePurchaseServiceModel serviceModel = new CrudePurchaseServiceModel();
            CrudePurchaseReportViewModel model = new CrudePurchaseReportViewModel();

            model.ddlCrude = getCrudeForDDL();
            model.ddlFeedStock = getFeedStock();
            model.CrudeJSON = getCrudeJSON();
            model.FeedStockJSON = getFeedStockJSON();
            model.SupplierJSON = getSupplierJSON();
            model.cdp_Search = new CrudePurchaseReportViewModel_Search();
            model.cdp_Search.sSearchData = new List<CrudePurchaseReportViewModel_SearchData>();
            model.cdp_Search.dMarginTotal = "0.00";
            return model;
        }

        public static List<SelectListItem> getCrudeForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.CDP_DATA select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    join o in context.CDP_OFFER_ITEMS on d.CDA_ROW_ID equals o.COI_FK_CDP_DATA into viewOffer
                                    from lvo in viewOffer.DefaultIfEmpty()
                                    where lvo.COI_FINAL_FLAG.Equals("Y")
                                     && d.CDA_STATUS.Equals("APPROVED")
                                    select new { CrudeId = d.CDA_FK_MATERIALS, CrudeName = d.MT_MATERIALS.MET_MAT_DES_ENGLISH, CrudeOther = d.CDA_MATERIALS_OTHERS }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {
                            if (string.IsNullOrEmpty(item.CrudeId))
                            {
                                selectList.Add(new SelectListItem { Text = item.CrudeOther, Value = item.CrudeOther });
                            }
                            else
                            {
                                selectList.Add(new SelectListItem { Text = item.CrudeName, Value = item.CrudeName });
                            }

                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static string getCrudeJSON()
        {
            string json = "";
            try
            {
                List<SelectListItem> data = getCrudeForDDL();
                var query = (from v in data
                             select new { value = v.Text });


                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(qDis.OrderBy(p => p.value));
                }



                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> getFeedStockForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.CDP_DATA select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    join o in context.CDP_OFFER_ITEMS on d.CDA_ROW_ID equals o.COI_FK_CDP_DATA into viewOffer
                                    from lvo in viewOffer.DefaultIfEmpty()
                                    where lvo.COI_FINAL_FLAG.Equals("Y")
                                     && d.CDA_STATUS.Equals("APPROVED")
                                    select new { FeedStock = d.CDA_FEED_STOCK, FeedStockOther = d.CDA_FEED_STOCK_OTHERS }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {
                            string feedstock = "";
                            if (string.IsNullOrEmpty(item.FeedStockOther))
                                feedstock = item.FeedStock;
                            else
                                feedstock = item.FeedStockOther;

                            selectList.Add(new SelectListItem { Text = feedstock, Value = feedstock });

                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static string getFeedStockJSON()
        {
            string json = "";
            try
            {
                List<SelectListItem> data = getFeedStockForDDL();
                var query = (from v in data
                             select new { value = v.Text });


                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(qDis.OrderBy(p => p.value));
                }



                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> getSupplierForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.CDP_DATA select v);

                    if (query != null)
                    {

                        var qDis = (from d in query
                                    join o in context.CDP_OFFER_ITEMS on d.CDA_ROW_ID equals o.COI_FK_CDP_DATA into viewOffer
                                    from lvo in viewOffer.DefaultIfEmpty()
                                    where lvo.COI_FINAL_FLAG.Equals("Y")
                                     && d.CDA_STATUS.Equals("APPROVED")
                                    select new { Supplier = d.CDA_PROPOSAL_SUPPLIER }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {

                            selectList.Add(new SelectListItem { Text = item.Supplier, Value = item.Supplier });

                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static string getSupplierJSON()
        {
            string json = "";
            try
            {
                List<SelectListItem> data = getSupplierForDDL();
                var query = (from v in data
                             select new { value = v.Text });


                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(qDis.OrderBy(p => p.value));
                }



                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        private List<SelectListItem> getForFeedStock()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.for_feedstock.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.for_feedstock[i], Value = setting.for_feedstock[i] });
            }
            return list;
        }

        private List<SelectListItem> getFeedStock(bool isHidden = false)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.feedstock.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.feedstock[i].key, Value = isHidden ? setting.feedstock[i].unit : setting.feedstock[i].key });
            }
            return list;
        }

        private List<SelectListItem> getTerm()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.term.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.term[i], Value = setting.term[i] });
            }
            return list;
        }

        private List<SelectListItem> getPurchase()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.purchase.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.purchase[i], Value = setting.purchase[i] });
            }
            return list;
        }

        private List<SelectListItem> getSupplySources()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.supply_source.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.supply_source[i], Value = setting.supply_source[i] });
            }
            return list;
        }

        private List<SelectListItem> getIncoterms()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.incoterms.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.incoterms[i], Value = setting.incoterms[i] });
            }
            return list;
        }

        private List<SelectListItem> getBenchmarkPrice()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.bechmark_price.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.bechmark_price[i], Value = setting.bechmark_price[i] });
            }
            return list;
        }

        private List<SelectListItem> getPaymentTerms()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.payment_terms.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.payment_terms[i].type, Value = setting.payment_terms[i].type });
            }
            return list;
        }

        private List<SelectListItem> getProposalReason()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.proposal_reason.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.proposal_reason[i], Value = setting.proposal_reason[i] });
            }
            return list;
        }

        private List<SelectListItem> getToleranceOption()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.toterance_option.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.toterance_option[i], Value = setting.toterance_option[i] });
            }
            return list;
        }

        private List<SelectListItem> getToleranceType()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.toterance_type.Count; i++)
            {
                if (setting.toterance_type[i] == "+/-")
                {
                    list.Insert(0, new SelectListItem { Text = setting.toterance_type[i], Value = setting.toterance_type[i] });
                }
                else
                {
                    list.Add(new SelectListItem { Text = setting.toterance_type[i], Value = setting.toterance_type[i] });
                }
            }
            return list;
        }

        private string getSupplierBondRates()
        {
            string rslt = "";
            var context = new EntityCPAIEngine();
            var items = context.Database.SqlQuery<SUPPLIER_BOND_RATE>("SELECT * FROM CTR.VIEW_CTR_BOND_MASTER").ToList<SUPPLIER_BOND_RATE>();
            foreach (SUPPLIER_BOND_RATE item in items)
            {
                rslt += rslt == "" ? "" : "|";
                rslt += item.SUPPLIERCODE + ":" + item.PERFORMANCEBOND.ToString();
            }
            return rslt;
        }

        private class SUPPLIER_BOND_RATE
        {
            public string SUPPLIERCODE { get; set; }
            public decimal PERFORMANCEBOND { get; set; }
        }

        private ResponseData SaveDataCrudePurchase(DocumentActionStatus _status, CrudePurchaseViewModel model, string note, string json_fileUpload, string attached_explain = "")
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_2; NextState = ConstantPrm.ACTION.WAITING_CERTIFIED; }
            model.explanationAttach = attached_explain;

            CdpRootObject obj = new CdpRootObject();
            obj.approve_items = model.approve_items;
            obj.compet_offers_items = model.compet_offers_items;
            obj.contract_period = model.contract_period;
            obj.crude_id = model.crude_id;
            obj.crude_name = model.crude_name;
            obj.crude_name_others = model.crude_name_others;
            obj.date_purchase = model.date_purchase;
            obj.plan_month = model.plan_month;
            obj.plan_year = model.plan_year;
            obj.discharging_period = model.discharging_period;
            obj.explanation = model.explanation;
            obj.explanationAttach = model.explanationAttach;
            obj.feedstock = model.feedstock;
            obj.feedstock_others = model.feedstock_others;
            obj.for_feedStock = model.for_feedstock;
            obj.loading_period = model.loading_period;
            obj.notes = model.notes;
            obj.offers_items = model.offers_items;
            obj.origin = model.origin;
            obj.origin_id = model.origin_id;
            obj.other_condition = model.other_condition;
            obj.payment_terms = model.payment_terms;
            obj.proposal = model.proposal;
            obj.purchase = model.purchase;
            if (obj.purchase.Equals("Private negotiation"))
            {
                obj.supply_source = model.supply_source;
            }
            obj.proposal.performance_bond = model.proposal.performance_bond;
            obj.reason = model.reason;
            obj.term = model.term;
            obj.term_others = model.term_others;
            obj.gtc = model.gtc;

            obj.CrudeSale_Optimization = model.CrudeSale_Optimization;
            obj.CrudeSale_Ref = model.CrudeSale_Ref;

            //Hide PTT Offer
            obj.offered_by_ptt = model.offered_by_ptt;
            obj.reasonPTToffer = model.reasonPTToffer;
            obj.offerVia = model.offerVia;
            obj.reasonOfferVia = model.reasonOfferVia;
            //if(!String.IsNullOrEmpty(model.offerDate))
            //{
            //    var dateOfferDate = Convert.ToDateTime(model.offerDate);
            //    var newOfferDate = dateOfferDate.ToString("dd-MMM-yyyy");
            //    obj.offerDate = newOfferDate;
            //}
            //else
            //{
            //    obj.offerDate = model.offerDate;
            //}           
            obj.offerDate = model.offerDate;
            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000023;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }

        private ResponseData LoadDataCrudePurchase(string TransactionID, ref CrudePurchaseViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }
            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CrudePurchaseViewModel>(_model.data_detail);
                if (!string.IsNullOrEmpty(model.contract_period.date_from) && !string.IsNullOrEmpty(model.contract_period.date_to))
                {
                    model.contract = model.contract_period.date_from + " to " + model.contract_period.date_to;
                }
                if (!string.IsNullOrEmpty(model.loading_period.date_from) && !string.IsNullOrEmpty(model.loading_period.date_to))
                {
                    model.loading = model.loading_period.date_from + " to " + model.loading_period.date_to;
                }
                if (!string.IsNullOrEmpty(model.discharging_period.date_from) && !string.IsNullOrEmpty(model.discharging_period.date_to))
                {
                    model.discharging = model.discharging_period.date_from + " to " + model.discharging_period.date_to;
                }
                if (!string.IsNullOrEmpty(model.proposal.price_period_from) && !string.IsNullOrEmpty(model.proposal.price_period_to))
                {
                    model.price_period = model.proposal.price_period_from + " to " + model.proposal.price_period_to;
                }
                if (!string.IsNullOrEmpty(model.payment_terms.sub_payment_term))
                {
                    model.nrdoDay = model.payment_terms.sub_payment_term;
                }
                if (!string.IsNullOrEmpty(model.requested_by))
                {
                    model.requested_by = GetRequesterFullName(model.requested_by);
                }
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }
            string _btn = "<input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            model.buttonCode += _btn;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.purno = resData.resp_parameters[0].v;
            }

            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref CrudePurchaseViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.VERIFIED).ToList().Count > 0 || Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.CERTIFIED).ToList().Count > 0)
                {
                    model.pageMode = "EDIT_REASON";
                }
                foreach (ButtonAction _button in Button)
                {
                    if (_button.name == "CERTIFIED")
                    {
                        _button.name = "ENDORSED";
                    }
                    string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                    model.buttonCode += _btn;
                }
                model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListCrudePurchase = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        private List_CrudePurchasetrx SearchCrudePurchaseData(string sDate, string eDate, string feedstock, string product, string supplier, string create_by)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000004;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "function_code", V = CRUDE_PURCHASE_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_7", V = feedstock });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = product });
            req.Req_parameters.P.Add(new P { K = "index_12", V = supplier });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_CrudePurchasetrx _model = ShareFunction.DeserializeXMLFileToObject<List_CrudePurchasetrx>(_DataJson);
            if (_model == null) _model = new List_CrudePurchasetrx();
            if (_model.CrudePurchaseTransaction == null) _model.CrudePurchaseTransaction = new List<CrudePurchaseEncrypt>();
            return _model;
        }

        public ActionResult ApproveCrudePurchase()
        {
            //ForTest
            //http://localhost:50131/CHR/CPAIMVC/CrudePurchase/ApproveCrudePurchase?token=89b7e01a8af24dc98102e6be1d3470a3

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }

        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        urlPage = "CrudePurchase";
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&PURNO=" + strPurNo.Encrypt();
                    }
                }
            }
            return path;
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        /// <summary>
        /// GenarateFile 
        /// </summary>
        /// <param name="model">Data</param>
        /// <param name="FileType">PDF,EXCEL</param>
        /// <returns>path file for download</returns>
        private string GenarateFile(CrudePurchaseReportViewModel model)
        {
            try
            {
                Color _BorderColor = Color.Black;
                string _Time = DateTime.Now.ToString("yyyyMMddHHmmssffff");

                string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("CrudePurchaseReport{0}.xlsx", _Time));
                string _urlPath = "";
                FileInfo excelFile = new FileInfo(file_path);

                #region -- Gen Data excel

                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Crude Purchase Reprot");

                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.Orientation = eOrientation.Landscape;
                    ws.PrinterSettings.Scale = 68;

                    //Initialize columns
                    int columnNo = 1;

                    // Loading period
                    ws.Column(columnNo).Width = 16;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    // Discharging period
                    ws.Column(columnNo).Width = 16;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    // Crude name
                    ws.Column(columnNo).Width = 13;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Column(columnNo).Style.WrapText = true;
                    columnNo++;
                    // Origin
                    ws.Column(columnNo).Width = 8;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    // Feed stock
                    ws.Column(columnNo).Width = 9;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    // For
                    ws.Column(columnNo).Width = 8;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    // Supplier
                    ws.Column(columnNo).Width = 14;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Column(columnNo).Style.WrapText = true;
                    columnNo++;
                    /// Contact person
                    ws.Column(columnNo).Width = 11;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Column(columnNo).Style.WrapText = true;
                    columnNo++;
                    /// Quantity
                    ws.Column(columnNo).Width = 9;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    /// Incoterms
                    ws.Column(columnNo).Width = 9;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    /// Offer
                    ws.Column(columnNo).Width = 9;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    /// Market/Source
                    ws.Column(columnNo).Width = 11;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    /// Latest LP plan price
                    ws.Column(columnNo).Width = 14;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Column(columnNo).Style.WrapText = true;
                    columnNo++;
                    /// Bechmark price
                    ws.Column(columnNo).Width = 11;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Column(columnNo).Style.WrapText = true;
                    columnNo++;
                    /// Rank/Round
                    ws.Column(columnNo).Width = 9;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;
                    /// Margin vs LP
                    ws.Column(columnNo).Width = 9;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Column(columnNo).Style.WrapText = true;
                    columnNo++;
                    /// Margin
                    ws.Column(columnNo).Width = 12;
                    ws.Column(columnNo).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(columnNo).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    columnNo++;


                    ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 9));

                    ws.Cells["A1:Q1"].Merge = true;
                    ws.Cells["A1:Q1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ws.Cells[1, 1].Value = "Crude Purchase Reprot";
                    ws.Cells[1, 1].Style.Font.Size = 11;
                    ws.Cells[1, 1].Style.Font.Bold = true;

                    ws.Cells["A2:Q2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A2:Q2"].Style.Font.Bold = true;
                    ws.Cells["A2:Q2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells["A2:Q2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    //ws.Cells["A2:Q2"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));

                    columnNo = 1;
                    ws.Cells[2, columnNo].Value = "Loading period"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Discharging period"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Crude name"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Origin"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Feed stock"; columnNo++;
                    ws.Cells[2, columnNo].Value = "For"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Supplier"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Contact person"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Quantity"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Incoterms"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Offer"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Market/Source"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Latest LP\r\nplan price"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Bechmark\r\nprice"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Rank/Round"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Margin\r\nvs LP"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Margin"; columnNo++;

                    int rowNo = 3;
                    foreach (var _item in model.cdp_Search.sSearchData)
                    {
                        columnNo = 1;
                        ws.Cells[rowNo, columnNo].Value = _item.dLoadingPeriodDate; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dDischargingPeriodDate; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dCrudeName; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dOrigin; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dFeedStock; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dFor; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dSupplier; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dContactPerson; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dQuantity; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dIncoterms; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dOffer; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dMarketSource; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dLatestLP; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dBechmarkPrice; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dRankRound; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.dMarginLP; columnNo++; ;
                        ws.Cells[rowNo, columnNo].Value = _item.dMargin; columnNo++;

                        rowNo++;
                    }

                    var lblTotalRow = String.Format("A{0}:P{0}", rowNo.ToString());
                    ws.Cells[lblTotalRow].Merge = true;
                    ws.Cells[lblTotalRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //ws.Cells[lblTotalRow].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                    ws.Cells[lblTotalRow].Style.Font.Bold = true;
                    ws.Cells[lblTotalRow].Value = "Total";

                    var lblTotalMarginRow = String.Format("Q{0}:Q{0}", rowNo.ToString());
                    ws.Cells[lblTotalMarginRow].Value = model.cdp_Search.dMarginTotal;
                    ws.Cells[lblTotalMarginRow].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                    // ws.Cells[lblTotalMarginRow].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 217, 217));
                    ws.Cells[lblTotalMarginRow].Style.Font.Bold = true;

                    var lblTotalMarginRowBorder = String.Format("A{0}:Q{0}", rowNo.ToString());
                    ws.Cells[lblTotalMarginRowBorder].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[lblTotalMarginRowBorder].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    package.Save();

                    #endregion

                    if (model.ExportTo == "PDF")
                    {

                        ReturnValue rtn = SaveAsPdf(file_path);
                        if (rtn.Status)
                            _urlPath = _FN.GetSiteRootUrl(Path.Combine("Web", "Report", "TmpFile", string.Format("CrudePurchaseReport{0}.pdf", _Time)));
                        else
                            throw new Exception("GenarateExcel >>" + rtn.Message);
                    }
                    else // EXCEL
                    {
                        _urlPath = _FN.GetSiteRootUrl(Path.Combine("Web", "Report", "TmpFile", string.Format("CrudePurchaseReport{0}.xlsx", _Time)));
                    }

                }
                return _urlPath.Replace(@"\", "/");
            }
            catch (Exception ex)
            {
                throw new Exception("GenarateExcel >>" + ex.Message);
            }

        }


        private ReturnValue SaveAsPdf(string saveAsLocation)
        {
            ReturnValue rtn = new ReturnValue();
            string saveas = (saveAsLocation.Split('.')[0]) + ".pdf";
            try
            {
                Workbook workbook = new Workbook();
                workbook.LoadFromFile(saveAsLocation);

                //Save the document in PDF format
                workbook.SaveToFile(saveas, Spire.Xls.FileFormat.PDF);
                rtn.Status = true;
                rtn.Message = saveas;
                return rtn;
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
                return rtn;
            }
        }

        public string GetRequesterFullName(string pUserName)
        {
            string rslt = "";
            USERS_DAL userDal = new USERS_DAL();
            USERS tUser = userDal.GetUserByUserName(pUserName);
            if (tUser != null)
            {
                rslt = tUser.USR_FIRST_NAME_EN + " " + tUser.USR_LAST_NAME_EN;
            }
            return rslt;
        }

        public struct AjaxRespons
        {
            public string Status;
            public string ErrorMessage;
            public object[] Data;
        }























        public ActionResult SendJson(dynamic input)
        {
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(input, microsoftDateFormatSettings);
            return Content(json, "application/json");
            //return Json(input, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFilterCrudePurchase_Customer()
        {
            var query = SendJson(CrudePurchase_DAL.GetFilterCrudePurchase_Customer());
            return query;
        }
        public ActionResult GetFilterCrudePurchase_CrudeName()
        {
            var query = SendJson(CrudePurchase_DAL.GetFilterCrudePurchase_CrudeName());
            return query;
        }
        public ActionResult GetFilterCrudePurchase_LR()
        {
            var query = SendJson(CrudePurchase_DAL.GetFilterCrudePurchase_LR());
            return query;
        }
        public ActionResult GetSearchCrudePurchase_CrudeRef(String json)
        {
            var query = SendJson(CrudePurchase_DAL.GetSearchCrudePurchase_CrudeRef(json));
            return query;
        }
        public ActionResult GetSearchCrudePurchase_CrudeRef_Detail(string SaleNo)
        {
            var query = SendJson(CrudePurchase_DAL.GetSearchCrudePurchase_CrudeRef_Detail(SaleNo));
            return query;
        }
        public ActionResult GetSearchCrudePurchase_CrudeRef_OPSetup(string taskID)
        {
            var query = SendJson(CrudePurchase_DAL.GetSearchCrudePurchase_CrudeRef_OPSetup(taskID));
            return query;
        }

    }
    //public class CrudePurchaseLoader: CrudePurchaseController
    //{
    //    public ResponseData LoadDataCrudePurchase(string TransactionID, ref CrudePurchaseViewModel model) {
    //        //return new CrudePurchaseController().LoadDataCrudePurchase(TransactionId,model);
    //        return this.LoadDataCrudePurchase(TransactionId, ref model);
    //    }
    //}
}