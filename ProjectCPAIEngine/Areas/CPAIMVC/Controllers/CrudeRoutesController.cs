﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CrudeRoutesController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "CrudeRoutes");
        }

        [HttpPost]
        public ActionResult Search(CrudeRoutesViewModel tempModel)
        {

            CrudeRoutesViewModel model = initialModel();

            CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();
            CrudeRoutesViewModel_Seach viewModelSearch = new CrudeRoutesViewModel_Seach();

            viewModelSearch = tempModel.crude_Search;
            serviceModel.Search(ref viewModelSearch);
            model.crude_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            CrudeRoutesViewModel model = initialModel();
            return View(model);
        }

        public static CrudeRoutesViewModel SearchQuery(CrudeRoutesViewModel CrudeRoutesModel)
        {
            CrudeRoutesViewModel_Seach viewModelSearch = new CrudeRoutesViewModel_Seach();
            CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();

            CrudeRoutesModel.ddl_Status = DropdownServiceModel.getMasterStatus();

            viewModelSearch = CrudeRoutesModel.crude_Search;
            serviceModel.Search(ref viewModelSearch);
            CrudeRoutesModel.crude_Search = viewModelSearch;

            return CrudeRoutesModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, CrudeRoutesViewModel model)
        {
            if (ViewBag.action_create)
            {
                CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.crude_Detail.Status = model.crude_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Add(model.crude_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                if (String.IsNullOrEmpty(model.crude_Detail.Date))
                    model.crude_Detail.Date = "";
                if (String.IsNullOrEmpty(model.crude_Detail.Status))
                    model.crude_Detail.Status = "ACTIVE";
                model.ddl_Status = DropdownServiceModel.getMasterStatus();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "CrudeRoutes" }));
            }
            
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                CrudeRoutesViewModel model = initialModel();
                if (String.IsNullOrEmpty(model.crude_Detail.Date))
                    model.crude_Detail.Date = "";
                if (String.IsNullOrEmpty(model.crude_Detail.Status))
                    model.crude_Detail.Status = "ACTIVE";
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "CrudeRoutes" }));
            }
            
        }

        [HttpPost]
        public ActionResult Edit(string date, CrudeRoutesViewModel model)
        {
            if (ViewBag.action_edit)
            {

                CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();
                ReturnValue rtn = new ReturnValue();
                
                model.crude_Detail.Status = model.crude_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Edit(model.crude_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["CrudeRoutesDate"] = date;

                if (model.crude_Detail == null)
                {
                    model.crude_Detail = new CrudeRoutesViewModel_Detail();
                    if (String.IsNullOrEmpty(model.crude_Detail.Date))
                        model.crude_Detail.Date = "";
                    if (String.IsNullOrEmpty(model.crude_Detail.Status))
                        model.crude_Detail.Status = "ACTIVE";
                    model.ddl_Status = DropdownServiceModel.getMasterStatus();
                }
                if (model.ddl_Status == null)
                {
                    model.ddl_Status = DropdownServiceModel.getMasterStatus();
                }

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "CrudeRoutes" }));
            }

        }

        [HttpGet]
        public ActionResult Edit(string CrudeRoutesDate)
        {
            if (ViewBag.action_edit)
            {
                CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();

                CrudeRoutesViewModel model = initialModel();
                model.crude_Detail = serviceModel.Get(CrudeRoutesDate);
                TempData["CrudeRoutesCode"] = CrudeRoutesDate;
                if (model.crude_Detail == null)
                {
                    model.crude_Detail = new CrudeRoutesViewModel_Detail();
                    if (String.IsNullOrEmpty(model.crude_Detail.Date))
                        model.crude_Detail.Date = "";
                    if (String.IsNullOrEmpty(model.crude_Detail.Status))
                        model.crude_Detail.Status = "ACTIVE";
                    model.ddl_Status = DropdownServiceModel.getMasterStatus();
                }

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "CrudeRoutes" }));
            }
            
        }

        [HttpGet]
        public ActionResult View(string CrudeRoutesDate)
        {
            if (ViewBag.action_view)
            {
                CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();

                CrudeRoutesViewModel model = initialModel();
                model.crude_Detail = serviceModel.Get(CrudeRoutesDate);
                TempData["CrudeRoutesCode"] = CrudeRoutesDate;
                if (model.crude_Detail == null)
                {
                    model.crude_Detail = new CrudeRoutesViewModel_Detail();
                    if (String.IsNullOrEmpty(model.crude_Detail.Date))
                        model.crude_Detail.Date = "";
                    if (String.IsNullOrEmpty(model.crude_Detail.Status))
                        model.crude_Detail.Status = "ACTIVE";
                    model.ddl_Status = DropdownServiceModel.getMasterStatus();
                }

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CrudeRoutes" }));
            }
            
        }

        public CrudeRoutesViewModel initialModel()
        {
            CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();
            CrudeRoutesViewModel model = new CrudeRoutesViewModel();

            model.crude_Search = new CrudeRoutesViewModel_Seach();
            model.crude_Search.sSearchData = new List<CrudeRoutesViewModel_SeachData>();

            model.crude_Detail = new CrudeRoutesViewModel_Detail();
            model.crude_Detail.BoolStatus = true;

            model.ddl_Status = DropdownServiceModel.getMasterStatus();
          
            return model;
        }

        [HttpPost]
        public JsonResult GetExcelSheet()
        {
            var ExcelFile = new List<SelectListItem>();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Web/FileUpload"), fileName);
                    file.SaveAs(path);
                    ExcelFile = CrudeRoutesServiceModel.getDataFromExcel(path);
                }
            }
            return Json(ExcelFile, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ImportExcelFile(string wooksheet)
        {
            CrudeRoutesServiceModel serviceModel = new CrudeRoutesServiceModel();
            ReturnValue rtn = new ReturnValue();

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Web/FileUpload"), fileName);
                    file.SaveAs(path);
                    rtn = serviceModel.ImportDataFromExcel(path, wooksheet, lbUserName);
                }
            }
            return Json(rtn, JsonRequestBehavior.AllowGet);
        }
    }
}