﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CustomVatController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "CustomVat");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(CustomVatViewModel pModel)
        {

            //CustomVatViewModel model = initialModel();

            CustomVatServiceModel serviceModel = new CustomVatServiceModel();
            CustomVatViewModel_Search viewModelSearch = new CustomVatViewModel_Search();

            viewModelSearch = pModel.CustomVat_Search;
            serviceModel.Search(ref viewModelSearch);
            pModel.CustomVat_Search = viewModelSearch;

            if (TempData["tempCustomsVat"] != null) { TempData.Remove("tempCustomsVat"); }

            TempData["tempCustomsVat"] = pModel;

            return View(pModel);
        }

        [HttpGet]
        public ActionResult Search()
        {
            //CustomVatViewModel model = initialModel();
            CustomVatViewModel model = new CustomVatViewModel();
            if (TempData["tempCustomsVat"] != null)
            {
                model = (CustomVatViewModel)TempData["tempCustomsVat"];

                TempData.Remove("tempCustomsVat");
            }
            else
            {
                model = initialModel();
            }

            return View("index",model);
        }

        [HttpGet]
        public string GetSavedData(string p)
        {
            string rslt = "{}";
            return rslt;
        }

        [HttpPost]
        public string GetSavedData(string[] p) {
            string rslt = "";
            AjaxRespons rsltObj = new AjaxRespons();
            try
            {
                rsltObj.Data = new object[1];
                List<CustomVatSavedData> tList = CustomVatServiceModel.getSavedData(p);
                rsltObj.Status = "Success";
                rsltObj.Data[0] = tList;
                rslt = JSonConvertUtil.modelToJson(rsltObj);
            }
            catch (Exception ex)
            {
                rsltObj.Status = "Error";
                rsltObj.Data = null;
                rsltObj.ErrorMessage = ex.Message;
                rslt = JSonConvertUtil.modelToJson(rsltObj);
            }
            return rslt;
        }

        [HttpGet]
        public string getCustomVatSearch(string BLDate = "", string DueDate = "", string TripNo = "", string Vessel = "", string Crude = "", string Supplier = "")
        {
            string rslt = "";
            AjaxRespons rsltObj = new AjaxRespons();
            if (Session["User"] != null) {
                rsltObj.Data = new object[1];
                rsltObj.Status = "Success";
                rsltObj.Data[0] = CustomVatServiceModel.getCustomVatSearch(BLDate, DueDate, TripNo.Trim(), Vessel, Crude, Supplier);
            }
            else
            {
                rsltObj.Status = "Error";
                rsltObj.Data = null;
                rsltObj.ErrorMessage = "SESSION_EXPIRED";
            }
            rslt = JSonConvertUtil.modelToJson(rsltObj);
            return rslt;
        }

        [HttpGet]
        public string GetEmptyCustomVatViewModelInJson()
        {
            CustomVatViewModel tModel = new CustomVatViewModel() ;
            CustomVatViewModel_Calc tModelCalc = new CustomVatViewModel_Calc();
            string [] rslt =  new string[] { JSonConvertUtil.modelToJson(tModel), JSonConvertUtil.modelToJson(tModelCalc) };
            return JSonConvertUtil.modelToJson(rslt);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(FormCollection frm, CustomVatViewModel model)
        {
            CustomVatServiceModel serviceModel = new CustomVatServiceModel();
            
            ReturnValue rtn = new ReturnValue();
            TempData["tempCustomsVat"] = model;
            rtn = serviceModel.SaveData(ref model);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;

            return RedirectToAction("Search", "CustomVat");

        }

        [HttpPost]
        public String Create( string pModelJson)
        {
            AjaxRespons ajr = new AjaxRespons();
            if (Session["User"] != null)
            {
                CustomVatServiceModel serviceModel = new CustomVatServiceModel();
                CustomVatViewModel model = JSonConvertUtil.jsonToModel<CustomVatViewModel>(pModelJson);
                ReturnValue rtn = new ReturnValue();
                TempData["tempCustomsVat"] = model;
                rtn = serviceModel.SaveData(ref model);
            
                ajr.Status = rtn.Status == true ? "Success" : "Error";
            
                if (rtn.Status.Equals(true))
                {
                    ajr.Data = new object[1];
                    ajr.Data[0] = "<div><span style='color:green;top:4px;' class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><span>&nbsp;&nbsp;" + rtn.Message + "</span></div>";
                }
                else {
                    ajr.ErrorMessage = "<div style='color:red;'><span style='top:4px;' class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span>&nbsp;&nbsp;" + rtn.Message + "</span></div>";
                }
            }else
            {
                ajr.Status = "Error";
                ajr.Data = null;
                ajr.ErrorMessage = "SESSION_EXPIRED";
            }
            return JSonConvertUtil.modelToJson(ajr);
        }

        [HttpGet]
        public ActionResult Create()
        {
            //CustomVatViewModel model = initialModel();
            CustomVatViewModel model = new CustomVatViewModel();
            if (TempData["tempCustomsVat"] != null)
            {
                model = (CustomVatViewModel)TempData["tempCustomsVat"];
                
                TempData.Remove("tempCustomsVat");
            }else
            {
                model = initialModel();
            }

            return View("index", model);
            //return RedirectToAction("Search", model);

        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "SendToSAP")]
        public ActionResult SendToSAP(FormCollection frm, CustomVatViewModel model)
        {
            CustomVatServiceModel serviceModel = new CustomVatServiceModel();

            ReturnValue rtn = new ReturnValue();

            
            TempData["tempCustomsVat"] = model;
            rtn = serviceModel.SendToSAP(ref model);

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;

            return RedirectToAction("Search", "CustomVat");

        }

        [HttpPost]
        public string SendToSAP( string pModelJson)
        {
            AjaxRespons ajr = new AjaxRespons();
            if (Session["User"] != null)
            {
                CustomVatServiceModel serviceModel = new CustomVatServiceModel();
                CustomVatViewModel model = JSonConvertUtil.jsonToModel<CustomVatViewModel>(pModelJson);
           //     model.CustomVat_Search
                ReturnValue rtn = new ReturnValue();
              
                rtn = serviceModel.SendToSAP(ref model);

                ajr.Status = rtn.Status == true ? "Success" : "Error";
                if (rtn.Status.Equals(true))
                {
                    ajr.Data = new object[2];
                    ajr.Data[0] = "<div><span style='color:green;top:4px;' class='glyphicon glyphicon-ok-sign' aria-hidden='true'></span><span>&nbsp;&nbsp;" + rtn.Message + "</span></div>";
                    string[] tFiDocs = new string[model.CustomVat_Calc.Count];
                    for (int i = 0; i < model.CustomVat_Calc.Count; i++)
                    {
                        tFiDocs[i] = model.CustomVat_Calc[i].TripNo + model.CustomVat_Calc[i].MatItemNo + "|" + model.CustomVat_Calc[i].FIDocVAT;
                    }
                    ajr.Data[1] = tFiDocs;
                }
                else
                {
                    ajr.ErrorMessage = "<div style='color:red;'><span style='top:4px;' class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span><span>&nbsp;&nbsp;" + rtn.Message + "</span></div>";
                }
            }else
            {
                ajr.Status = "Error";
                ajr.Data = null;
                ajr.ErrorMessage = "SESSION_EXPIRED";
            }

            return JSonConvertUtil.modelToJson(ajr);
        }

        public CustomVatViewModel initialModel()
        {
            CustomVatServiceModel serviceModel = new CustomVatServiceModel();
            CustomVatViewModel model = new CustomVatViewModel();

            model.CustomVat_Search = new CustomVatViewModel_Search();
            model.CustomVat_Search.sSearchData = new List<CustomVatViewModel_SearchData>();

            model.ddl_Vessel = CustomVatServiceModel.getVehicle("PRODUCT", true, "select"); 
            model.json_Vessel = CustomVatServiceModel.GetDataToJSON_TextValue(model.ddl_Vessel);
            model.ddl_Crude = CustomVatServiceModel.getMaterial();
            model.json_Crude = CustomVatServiceModel.GetDataToJSON_TextValue(model.ddl_Crude);
            model.ddl_Supplier = CustomVatServiceModel.getVendor();
            model.json_Supplier = CustomVatServiceModel.GetDataToJSON_TextValue(model.ddl_Supplier);

            model.sInsuranceRate = CustomVatServiceModel.getLastedInsuranceRate();

            model.GLAccountVAT = CustomVatServiceModel.getGLAccountVAT();

            model.ddl_InsuranceRate = new List<SelectListItem>();
            model.ddl_InsuranceRate = CustomVatServiceModel.getCIPConfigInsuranceRate();
            //model.ddl_InsuranceRate.Add(new SelectListItem { Text = "0.00001805", Value = "0.00001805" });
            //model.ddl_InsuranceRate.Add(new SelectListItem { Text = "0.00001083", Value = "0.00001083" });

            //model.sInsuranceRate = "0.00001805";

            model.VATFactorList = CustomVatServiceModel.getVATFactor();

            model.nVatPercentages = new VatPercent();

            return model;
        }
    }

    public struct AjaxRespons {
        public string Status;
        public string ErrorMessage;
        public object[] Data;
    }
}