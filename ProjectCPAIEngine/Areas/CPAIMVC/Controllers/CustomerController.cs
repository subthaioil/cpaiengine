﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CustomerController : BaseController
    {
        // GET: CPAIMVC/Customer
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Customer");
        }

        [HttpGet]
        public ActionResult Search()
        {
            CustomerViewModel model = initialModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(CustomerViewModel postModel)
        {
            CustomerViewModel model = initialModel();

            CustomerServiceModel serviceModel = new CustomerServiceModel();
            CustomerViewModel_Seach searchModel = new CustomerViewModel_Seach();

            searchModel = postModel.cust_Search;
            serviceModel.Search(ref searchModel);
            model.cust_Search = searchModel;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                CustomerViewModel model = initialModel();

                if (model.cust_Detail.CreateType == null || model.cust_Detail.CreateType == "")
                {
                    model.cust_Detail.CreateType = "CPAI";
                }
                model.cust_Detail.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "1", System = "", Type = "", Color = "", Status = "ACTIVE" });

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Customer" }));
            }
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, CustomerViewModel viewModel)
        {
            if (ViewBag.action_create)
            {
                CustomerServiceModel serviceModel = new CustomerServiceModel();
                ReturnValue rtn = new ReturnValue();

                CustomerViewModel model = initialModel();

                if (frm["custSystem"] != null)
                {
                    var custSystem = frm["custSystem"].Split(',');
                    var custSystemType = frm["custSystemType"].Split(',');
                    var custSystemColor = frm["custSystemColor"].Split(',');
                    var custSystemStatus = frm["custSystemStatus"].Split(',');

                    for (int i = 0; i < custSystem.Count(); i++)
                    {
                        viewModel.cust_Detail.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "", System = custSystem[i], Type = custSystemType[i], Color = custSystemColor[i], Status = custSystemStatus[i] });
                        model.cust_Detail = viewModel.cust_Detail;
                    }
                }

                CustomerViewModel_Detail modelDetail = new CustomerViewModel_Detail();
                modelDetail = viewModel.cust_Detail;
                rtn = serviceModel.Add(ref modelDetail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                CustomerViewModel modelDropdownSet = initialModel();
                modelDropdownSet.cust_Detail = modelDetail;
                return View(modelDropdownSet);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Customer" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string customerCode)
        {
            if (ViewBag.action_edit)
            {
                CustomerServiceModel serviceModel = new CustomerServiceModel();

                CustomerViewModel model = initialModel();
                model.cust_Detail = serviceModel.Get(customerCode);
                if (model.cust_Detail.Control.Count < 1)
                {
                    model.cust_Detail.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "1", System = "", Color = "" });
                }

                TempData["customerCode"] = customerCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Customer" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string customerCode, CustomerViewModel viewModel)
        {
            if (ViewBag.action_edit)
            {
                CustomerServiceModel serviceModel = new CustomerServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(viewModel.cust_Detail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["customerCode"] = customerCode;

                CustomerViewModel model = initialModel();
                model.cust_Detail = viewModel.cust_Detail;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Customer" }));
            }
        }

        public ActionResult ViewDetail(string customerCode)
        {
            if (ViewBag.action_view)
            {
                CustomerServiceModel serviceModel = new CustomerServiceModel();
                CustomerViewModel model = initialModel();
                model.cust_Detail = serviceModel.Get(customerCode);
                if (model.cust_Detail.Control.Count < 1)
                {
                    model.cust_Detail.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "1", System = "", Color = "" });
                }
                TempData["vendorCode"] = customerCode;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Customer" }));
            }
        }

        public CustomerViewModel initialModel()
        {
            CustomerServiceModel serviceModel = new CustomerServiceModel();
            CustomerViewModel model = new CustomerViewModel();

            model.cust_Detail = new CustomerViewModel_Detail();
            model.cust_Detail.Control = new List<CustomerViewModel_Control>();
            model.cust_Detail.Broker = new List<CustomerViewModel_Broker>();

            model.ddl_Country = DropdownServiceModel.getCountry();
            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_Company = DropdownServiceModel.getCompany();
            model.ddl_Nation = DropdownServiceModel.getNation();
            model.ddl_System = DropdownServiceModel.getCustomerCtrlSystem();
            model.ddl_Type = DropdownServiceModel.getCustomerCtrlType();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_Color = DropdownServiceModel.getColorPicker();

            model.ddl_Broker = DropdownServiceModel.getVendor("CPAI", "BROKER", "ACTIVE");

            model.cust_SystemJSON = CustomerServiceModel.GetSystemJSON();
            model.cust_TypeJSON = CustomerServiceModel.GetTypeJSON();

            return model;
        }
    }
}