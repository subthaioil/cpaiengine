﻿using ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API;
using ProjectCPAIEngine.DAL.DALDAF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.DAF.API
{
    public class DAFMasterController : BaseApiController
    {
        DAF_MASTER_DAL master_mnt = new DAF_MASTER_DAL();
        DAF_CIP_DAL cip_man = new DAF_CIP_DAL();
        DAF_CHO_DAL cho_man = new DAF_CHO_DAL();

        // GET: CPAIMVC/DAFMaster
        public ActionResult Index()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            result["DAF_CURRENCY_UNIT"] = master_mnt.GetCurrency();
            result["PORTS"] = master_mnt.GetPortList(lbUserName, "DAF");
            result["JETTIES_D"] = master_mnt.GetJettyDishargeList();
            result["JETTIES_L"] = master_mnt.GetJettyLoadList();
            result["VESSELS"] = master_mnt.GetVesselList();
            result["CUSTOMERS"] = master_mnt.GetCustomerList();
            result["BROKERS"] = master_mnt.GetBrokerList();
            result["COMPANY"] = master_mnt.GetCompany();
            result["INCOTERMS"] = master_mnt.GetIncoterms();
            result["MATERIALS"] = master_mnt.GetMatrials();
            return SendJson(result);
        }

        public ActionResult GetCustomers()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            result["CUSTOMERS"] = master_mnt.GetCustomerList();
            return SendJson(result);
        }

        public ActionResult GetCIPCrudeData(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            var cip_data = cip_man.getCIPCrudeList(criteria);
            return SendJson(cip_data);
        }

        public ActionResult GetCIPProductData(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            var cip_data = cip_man.getCIPProductList(criteria);
            return SendJson(cip_data);
        }

        public ActionResult GetCHOData(DAF_SEARCH_DATA_CRITERIA criteria)
        {
            var data = cho_man.getCHOList(criteria);
            return SendJson(data);
        }
    }
}