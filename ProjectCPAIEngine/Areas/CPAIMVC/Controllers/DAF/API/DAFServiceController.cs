﻿using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API;
using ProjectCPAIEngine.DAL.DALDAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.DAF.API
{
    public class DAFServiceController : BaseApiController
    {
        // GET: CPAIMVC/DAFService
        public ActionResult Index()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            return SendJson(result);
        }

        public ActionResult SaveDraft(DAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            ResponseData result = new ResponseData();
            if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            {
                //List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
                result = SaveData(_status, md, "");
            }
            else
            {
                result = ButtonClickDAF("SAVE DRAFT", md);
                //result = _btn_Click("SAVE DRAFT", md);
                //md.PDA_FORM_ID = result.resp_parameters[0].v;
                //md.PDA_CREATED = md.PDA_CREATED ?? DateTime.Now;
                //md.PDA_CREATED_BY = lbUserID;
                //md.PDA_STATUS = ACTION.DRAFT;
                ////PAF_DATA_DAL.InsertOrUpdate(md);
            }
            return SendJson(result);
        }
        public ActionResult Submit(DAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            md.DDA_UPDATED = DateTime.Now;
            md.DDA_UPDATED_BY = lbUserID;
            result = ButtonClickDAF("SUBMIT", md);
            return SendJson(result);
        }
        public ActionResult Verify(DAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            md.DDA_UPDATED = DateTime.Now;
            md.DDA_UPDATED_BY = lbUserID;
            result = ButtonClickDAF("VERIFY", md);
            return SendJson(result);
        }
        public ActionResult Endorse(DAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.DDA_UPDATED = DateTime.Now;
            md.DDA_UPDATED_BY = lbUserID;
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = ButtonClickDAF("ENDORSE", md);
            //}
            return SendJson(result);
        }
        public ActionResult Cancel(DAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Cancel;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.DDA_UPDATED = DateTime.Now;
            md.DDA_UPDATED_BY = lbUserID;
            result = ButtonClickDAF("CANCEL", md);
            //}
            return SendJson(result);
        }
        public ActionResult Reject(DAF_DATA md)
        {
            //DocumentActionStatus _status = DocumentActionStatus.Reject;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.DDA_UPDATED = DateTime.Now;
            md.DDA_UPDATED_BY = lbUserID;
            result = ButtonClickDAF("REJECT", md);
            //}
            return SendJson(result);
        }
        public ActionResult Approve(DAF_DATA md)
        {
            ResponseData result = new ResponseData();
            md.DDA_UPDATED = DateTime.Now;
            md.DDA_UPDATED_BY = lbUserID;
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = ButtonClickDAF("APPROVE", md);

            return SendJson(result);
        }


        public ActionResult GetByTransactionId(DAF_DATA model)
        {
            return FindDafButtons(model);
        }

        public ActionResult FindDafButtons(DAF_DATA model)
        {
            DAF_DATA_WRAPPER result = LoadDAFData(model.TRAN_ID);
            return SendJson(result);
        }

        public ActionResult FileUpload()
        {
            DAF_ATTACH_FILE result = new DAF_ATTACH_FILE();
            try
            {
                if (Request.QueryString["PAT_TYPE"] == null) throw new Exception("Invalid Type.");
                if (Request.QueryString["Type"] == null) throw new Exception("Invalid Type.");
                string[] _Param = Request.QueryString["Type"].ToString().Split('|');
                if (_Param.Length >= 2)
                {
                    string Type = _Param[0];
                    string FNID = _Param[1];
                    string rootPath = Request.PhysicalApplicationPath;
                    System.Web.HttpFileCollection upload_file = System.Web.HttpContext.Current.Request.Files;
                    HttpPostedFile file = upload_file[0];
                    string file_name = Path.GetFileName(file.FileName);
                    if (file_name != string.Empty)
                    {
                        file_name = string.Format("{0}_{2}_{1}_{3}", DateTime.Now.ToString("yyyyMMddHHmmss"), Type, FNID, Path.GetFileName(file.FileName));
                        if (!Directory.Exists(rootPath + "Web/FileUpload/"))
                            Directory.CreateDirectory(rootPath + "Web/FileUpload/");
                        file.SaveAs(rootPath + "Web/FileUpload/" + file_name);
                    }

                    //result.PAT_TYPE = Request.QueryString["PAT_TYPE"];
                    result.DAT_INFO = file_name;
                    result.DAT_PATH = "Web/FileUpload/" + file_name;
                    result.DAT_CREATED = DateTime.Now;
                    result.DAT_CREATED_BY = lbUserID;
                    result.DAT_UPDATED = DateTime.Now;
                    result.DAT_UPDATED_BY = lbUserID;
                }
                else
                {
                    throw new Exception("Invalid Type.");
                }
            }
            catch (Exception ex)
            {
                //output.result = new OutputResult();
                //output.result.Status = "ERROR";
                //output.result.Description = ex.Message.Replace("\r\n", " ").Replace("\"", "'");
            }
            return SendJson(result);
        }

        public ActionResult GenExcel(string id)
        {
            DAFExcelExport exMan = new DAFExcelExport();
            DAF_DATA_DAL dafMan = new DAF_DATA_DAL();

            DAF_DATA data = dafMan.Get(id);
            string file_name = string.Format("Demurrage-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_path = exMan.GenExcel(data, file_name);
            return SendJson(file_path);
        }

        public ActionResult GeneratePDF(string id)
        {
            DAF_DATA_DAL dafMan = new DAF_DATA_DAL();
            DAF_DATA data = dafMan.Get(id);
            string pdf_file = DAFGenerator.genPDF(data);
            return SendJson(pdf_file);
        }
    }
}