﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class DisChannelController : BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "DisChannel");
        }

        [HttpPost]
        public ActionResult Search(DisChannelViewModel tempModel)
        {

            DisChannelViewModel model = initialModel();

            DisChannelServiceModel serviceModel = new DisChannelServiceModel();
            DisChannelViewModel_Search viewModelSearch = new DisChannelViewModel_Search();

            viewModelSearch = tempModel.disChannel_Search;
            serviceModel.Search(ref viewModelSearch);
            model.disChannel_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            DisChannelViewModel model = initialModel();
            return View(model);
        }

        public static DisChannelViewModel SearchQuery(DisChannelViewModel dischannelModel)
        {
            DisChannelViewModel_Search viewModelSearch = new DisChannelViewModel_Search();
            DisChannelServiceModel serviceModel = new DisChannelServiceModel();

            viewModelSearch = dischannelModel.disChannel_Search;
            serviceModel.Search(ref viewModelSearch);
            dischannelModel.disChannel_Search = viewModelSearch;

            return dischannelModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, DisChannelViewModel model)
        {
            if (ViewBag.action_create)
            {
                DisChannelServiceModel serviceModel = new DisChannelServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.disChannel_Detail.MDCStatus = model.disChannel_Detail.boolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Add(model.disChannel_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                //CompanyViewModel modelDropdownSet = initialModel();

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "DisChannel" }));
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                DisChannelViewModel model = initialModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "DisChannel" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string CompanyCode, DisChannelViewModel model)
        {
            if (ViewBag.action_edit)
            {
                DisChannelServiceModel serviceModel = new DisChannelServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.disChannel_Detail.MDCStatus = model.disChannel_Detail.boolStatus == true ? "ACTIVE" : "INACTIVE";
                rtn = serviceModel.Edit(model.disChannel_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["CompanyCode"] = CompanyCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "DisChannel" }));
            }

        }

        [HttpGet]
        public ActionResult Edit(string MDC_DC)
        {
            if (ViewBag.action_edit)
            {
                DisChannelServiceModel serviceModel = new DisChannelServiceModel();

                DisChannelViewModel model = initialModel();
                model.disChannel_Detail = serviceModel.Get(MDC_DC);
                TempData["MDC_DC"] = MDC_DC;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "DisChannel" }));
            }

        }

        [HttpGet]
        public ActionResult View(string MDC_DC)
        {
            if (ViewBag.action_view)
            {
                DisChannelServiceModel serviceModel = new DisChannelServiceModel();

                DisChannelViewModel model = initialModel();
                model.disChannel_Detail = serviceModel.Get(MDC_DC);
                TempData["MDC_DC"] = MDC_DC;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "DisChannel" }));
            }

        }

        public DisChannelViewModel initialModel()
        {
            DisChannelViewModel model = new DisChannelViewModel();

            model.disChannel_Search = new DisChannelViewModel_Search();
            model.disChannel_Search.sSearchData = new List<DisChannelViewModel_SearchData>();
            model.disChannel_Detail = new DisChannelViewModel_Detail();
            model.disChannel_Detail.boolStatus = true;

            model.ddl_Status = DropdownServiceModel.getMasterStatus();

            model.disChannelIDJSON = DisChannelServiceModel.GetDisChannelIDJSON();
            model.disChannelZoneJSON = DisChannelServiceModel.GetDisChannelZoneJSON();

            return model;
        }
    }
}