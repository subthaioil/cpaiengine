﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class DocumentManagementController : BaseController
    {
        // GET: CPAIMVC/DocumentManagement
        public ActionResult Index()
        {
            SearchDocumentManagemantViewModel model;
            if (TempData["DocumentManagemantViewModel"] == null)
            {
                model = new SearchDocumentManagemantViewModel();
                model.ddlFileSupportingDocumentCountry = DropdownServiceModel.getCountryByCrudePurchase();
                model.ddlCrudeName = DocumentManagementServiceModel.getMaterialsDDL();
                DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
                serviceModel.SearchSupportingDocument(ref model);
                serviceModel.SearchDocument(ref model);
                serviceModel.getAbbreviation(ref model);
            }
            else
            {
                model = TempData["DocumentManagemantViewModel"] as SearchDocumentManagemantViewModel;
                TempData["DocumentManagemantViewModel"] = null;
            }
            return View(model);
        }

        public ActionResult Create()
        {
            DocumentManagemantViewModel model = new DocumentManagemantViewModel();
            model.ddlContry = DropdownServiceModel.getCountryByCrudePurchase();
            model.ddlCrudeName = DocumentManagementServiceModel.getMaterialsDDL();
            model.ddlFileType = getFileType();
            return View(model);
        }

        public ActionResult Edit()
        {
            SupportingDocumentViewModel model = new SupportingDocumentViewModel();
            DocumentManagementServiceModel service = new DocumentManagementServiceModel();
            model.crude_name = Request.QueryString["crudename"].ToString();
            model.country = Request.QueryString["country"].ToString();

            service.getTranID(ref model);
            service.getFileSupport(ref model);

            model.ddlFileSupportingDocumentType = getFileType();

            return View(model);
        }

        public ActionResult ViewFile()
        {
            DocumentManagemantViewDetailModel model = new DocumentManagemantViewDetailModel();
            DocumentManagementServiceModel service = new DocumentManagementServiceModel();
            model.crudeName = Request.QueryString["crudename"].ToString();
            model.country = Request.QueryString["country"].ToString();
            model.ddlFileType = getFileType();
            service.getDetailFile(ref model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "UpdateFileSupporting")]
        public ActionResult UpdateFileSupporting(FormCollection form, SupportingDocumentViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
            rtn = serviceModel.saveFileSupport(model, lbUserName);
            TempData["res_message"] = rtn.Message;
            string path = string.Format("~/CPAIMVC/DocumentManagement/Edit?crudename="+model.crude_name+"&country="+model.country);
            return RedirectToAction("Index", "DocumentManagement");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "UpdateDocument")]
        public ActionResult UpdateDocument(FormCollection form, DocumentManagemantViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
            if (!string.IsNullOrEmpty(form["fileSupportingDocument"]))
            {
                model.filePath = form["fileSupportingDocument"];
            }
            rtn = serviceModel.updateFileDocument(model, lbUserName);
            TempData["res_message"] = rtn.Message;
            return RedirectToAction("Index", "DocumentManagement");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDocument")]
        public ActionResult SaveDocument(FormCollection form, DocumentManagemantViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
            if (!string.IsNullOrEmpty(form["fileSupportingDocument"]))
            {
                model.filePath = form["fileSupportingDocument"];
            }
            rtn = serviceModel.saveFileDocManagement(model, lbUserName);
            TempData["res_message"] = rtn.Message;
            return RedirectToAction("Index", "DocumentManagement");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveCPU")]
        public ActionResult SaveCPU(FormCollection form, SearchDocumentManagemantViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Web/FileUpload/COOL"), fileName);
                    file.SaveAs(path);
                    model.CPU_Path = path;
                }
            }
            if (!string.IsNullOrEmpty(form["Crude_abbreviation"]))
            {
                model.cpu_name = form["Crude_abbreviation"];
            }
            rtn = serviceModel.saveFileCPU(model, lbUserName);
            TempData["res_message"] = rtn.Message;
            return RedirectToAction("Index", "DocumentManagement");
        }

        [HttpPost]
        public ActionResult SearchDocumentManagement(FormCollection form, DocumentManagemantViewModel pModel)
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchSupportingDocument(FormCollection form, SearchDocumentManagemantViewModel postModel)
        {
            SearchDocumentManagemantViewModel model = new SearchDocumentManagemantViewModel();
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
            model = postModel;
            model.ddlFileSupportingDocumentCountry = DropdownServiceModel.getCountryByCrudePurchase();
            model.ddlCrudeName = DocumentManagementServiceModel.getMaterialsDDL();
            serviceModel.SearchSupportingDocument(ref model);
            serviceModel.getAbbreviation(ref model);
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult SearchDocument(FormCollection form, SearchDocumentManagemantViewModel postModel)
        {
            SearchDocumentManagemantViewModel model = new SearchDocumentManagemantViewModel();
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
            model = postModel;
            model.ddlFileSupportingDocumentCountry = DropdownServiceModel.getCountryByCrudePurchase();
            model.ddlCrudeName = DocumentManagementServiceModel.getMaterialsDDL();
            serviceModel.SearchDocument(ref model);
            serviceModel.getAbbreviation(ref model);
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form)
        {
            return RedirectToAction("Index", "DocumentManagement");
        }

        private List<SelectListItem> getFileType()
        {
            Setting setting = JSONSetting.getSetting("JSON_COOL_CRUDE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.file_type.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.file_type[i], Value = setting.file_type[i] });
            }
            return list;
        }


        [HttpGet]
        public JsonResult GetCountryRef(string crude)
        {
            List<SelectListItem> objcountry = DropdownServiceModel.getCountryByCrudePurchase(crude);
            return Json(objcountry, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string CheckFileFormat()
        {
            DocumentManagementServiceModel serviceModel = new DocumentManagementServiceModel();
            string filepath = "";
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Web/FileUpload/COOL"), fileName);
                    file.SaveAs(path);
                    filepath = path;
                    return serviceModel.CheckFileFormat(filepath);
                }
            }
            return "0";
        }
    }
}