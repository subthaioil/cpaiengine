﻿using Excel;
using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class DownloadAppController : BaseController
    {
        // GET: CPAIMVC/DownloadApp
        public ActionResult Index(FormCollection form)
        {
            //genExcel(form);
            genExcel2(form);
            return View();
        }

        //public void genExcel(FormCollection form)
        //{
        //    if (form["btnExcel"] != null && form["btnExcel"].ToString().Equals("Y"))
        //    {
        //        #region Upload Excel
        //        try
        //        {
        //            if (Request.Files.Count > 0)
        //            {
        //                HttpPostedFileBase upload = Request.Files[0];
        //                string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", upload.FileName);
        //                FileInfo excelFile = new FileInfo(file_path);
        //                if (upload.ContentLength > 0)
        //                {
        //                    var package = new ExcelPackage(upload.InputStream);

        //                    ExcelWorksheet ws = package.Workbook.Worksheets.First();
        //                    //clear value
        //                    ws.Cells["A2:A" + ws.Dimension.End.Row].Value = "";
        //                    ws.Cells["B2:B" + ws.Dimension.End.Row].Value = "";
        //                    ws.Cells["E2:E" + ws.Dimension.End.Row].Value = "";


        //                    List<SelectListItem> counter_party = DropdownServiceModel.getCounterParty(false, "", "CPALL");
        //                    Random rnd = new Random();
        //                    for (int i = 0; i < counter_party.Count; i++)
        //                    {
        //                        int cRow = i + 2;
        //                        int cp_approved = rnd.Next(1, 20);
        //                        int prefix = i % 2 == 0 ? 1 : -1;
        //                        double gain_loss = prefix * rnd.NextDouble();
        //                        ws.Cells["A" + cRow].Value = counter_party[i].Text;
        //                        ws.Cells["B" + cRow].Value = cp_approved;
        //                        ws.Cells["E" + cRow].Value = gain_loss;
        //                        ws.Cells["F" + cRow].Formula = "IF(E" + cRow + ">=0,B" + cRow + "-E" + cRow + ",B" + cRow + ")";
        //                        ws.Cells["G" + cRow].Formula = "E" + cRow + "/B" + cRow;
        //                        ws.Cells["H" + cRow].Formula = "F" + cRow + "/B" + cRow;
        //                    }

        //                    package.SaveAs(excelFile);

        //                    Response.Clear();
        //                    Response.AddHeader("content-disposition", "attachment; filename=" + upload.FileName);
        //                    Response.AddHeader("content-type", "application/Excel");
        //                    Response.WriteFile(file_path);
        //                    Response.End();
        //                }
        //                else
        //                {
        //                    ModelState.AddModelError("File", "Please Upload Your file");
        //                }

        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            //throw;
        //        }
        //        #endregion
        //    }
        //}

        public void genExcel2(FormCollection form)
        {
            if (form["btnSpiral"] != null && form["btnSpiral"].ToString().Equals("Y"))
            {
                #region Upload Excel
                if (Request != null)
                {
                    HttpPostedFileBase file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        string type = "CRUDE";
                        string FNID = "F10000040";
                        string rootPath = Request.PhysicalApplicationPath;
                        string subPath = "Web/FileUpload/COOL/SPIRAL";
                        fileName = string.Format("{0}_{1}_{2}_{3}", type.ToUpper(), DateTime.Now.ToString("yyyyMMddHHmmss"), FNID, Path.GetFileName(file.FileName));
                        if (!Directory.Exists(rootPath + subPath))
                            Directory.CreateDirectory(rootPath + subPath);
                        var path = Path.Combine(Server.MapPath("~/" + subPath), fileName);
                        file.SaveAs(path);

                        IExcelDataReader reader = null;

                        if (fileName.EndsWith(".xls"))
                        {
                            reader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                        }
                        else if (fileName.EndsWith(".xlsx"))
                        {
                            reader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
                        }

                        reader.IsFirstRowAsColumnNames = true;

                        DataSet ds = reader.AsDataSet();
                        reader.Close();
                        if (ds.Tables.Count > 0)
                        {
                            DataTable dt = ds.Tables[0];
                            COO_DATA_DAL dataDAL = new COO_DATA_DAL();
                            string key = dataDAL.getKeyByAssayRef(dt.Rows[4][1].ToString());

                            if (!string.IsNullOrEmpty(key))
                            {
                                COO_CAM_DAL camDAL = new COO_CAM_DAL();
                                COO_CAM cam = camDAL.GetLatestByID(key);
                                cam.COCA_FILE_PATH = fileName;
                                camDAL.UpdateFilePath(cam, new EntityCPAIEngine());

                                if (cam != null)
                                {
                                    COO_SPIRAL_DAL spiralDAL = new COO_SPIRAL_DAL();
                                    spiralDAL.Delete(key, new EntityCPAIEngine());

                                    int pointer = 0;
                                    string header = "";
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        if (dt.Rows[i][0].ToString() == "" && pointer == 0)
                                        {
                                            pointer = 1;
                                            continue;
                                        }
                                        if (pointer == 1)
                                        {
                                            header = dt.Rows[i][0].ToString();
                                            pointer = 2;
                                        }
                                        else
                                        {
                                            if (dt.Rows[i][1].ToString() != "")
                                            {
                                                COO_SPIRAL spiral = new COO_SPIRAL();

                                                spiral = new COO_SPIRAL();
                                                spiral.COSP_ROW_ID = Guid.NewGuid().ToString("N");
                                                spiral.COSP_FK_COO_CAM = cam.COCA_ROW_ID;
                                                spiral.COSP_HEADER = header;
                                                spiral.COSP_KEY = dt.Rows[i][0].ToString();
                                                spiral.COSP_VALUE = dt.Rows[i][1].ToString();
                                                spiral.COSP_EXCEL_POSITION = "B" + (i + 2);
                                                spiral.COSP_CREATED = DateTime.Now;
                                                spiral.COSP_CREATED_BY = "migrate";
                                                spiral.COSP_UPDATED = DateTime.Now;
                                                spiral.COSP_UPDATED_BY = "migrate";
                                                spiralDAL.Save(spiral, new EntityCPAIEngine());
                                            }
                                            pointer = 0;
                                        }
                                    }
                                }
                                
                            }

                        }
                    }

                }
            #endregion
            }

        }
    }
}