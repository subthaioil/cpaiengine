﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using Excel;
using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class EstimateFreightCalculationController : BaseController
    {
        public List<ButtonAction> ButtonListFreightCalculation
        {
            get
            {
                if (Session["ButtonListFreightCalculation"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListFreightCalculation"];
            }
            set { Session["ButtonListFreightCalculation"] = value; }
        }
        ShareFn _FN = new ShareFn();
        const string returnPage = "Search";
        EstimateFreightServiceModel df;

        public ActionResult Index(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            initializeDropDownList(ref model);
            return View(model);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            this.updateModel(form, model);
            if (model.fcl_results.offer_freight.offer_unit.ToUpper() == "LUMPSUM")
            {
                if (!String.IsNullOrEmpty(model.FreightOfferUSD) && !String.IsNullOrEmpty(model.FreightOfferTHB))
                {
                    var usd = Convert.ToDecimal(model.FreightOfferUSD);
                    var baht = Convert.ToDecimal(model.FreightOfferTHB);
                    model.fcl_results.offer_freight.usd = usd.ToString();
                    model.fcl_results.offer_freight.baht = baht.ToString();
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(model.FreightOfferUSDprice) && !String.IsNullOrEmpty(model.FreightOfferTHBprice))
                {
                    var usd = Convert.ToDecimal(model.FreightOfferUSDprice);
                    var baht = Convert.ToDecimal(model.FreightOfferTHBprice);
                    model.fcl_results.offer_freight.usd = usd.ToString();
                    model.fcl_results.offer_freight.baht = baht.ToString();
                }
            }
            HttpPostedFileBase upload = Request.Files.Count > 0 ? Request.Files[0] : null;
            var image = upload;
            string uploadImageName = "";
            if (upload.ContentLength > 0)
            {
                string type = ConstantPrm.SYSTEM.FREIGHT;
                string FNID = ConstantPrm.FUNCTION.F10000008;
                string typename = Path.GetExtension(image.FileName).ToLower();
                uploadImageName = string.Format("{0}_{2}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, FNID);
                model.fcl_pic_path = uploadImageName + typename;
                var uploadPicRes = UploadFileImage(upload, "Web/FileUpload/FREIGHT/IMAGE", uploadImageName + typename);
            }

            ResponseData resData = SaveDataFreightCalculation(_status, model, note, getFileUploadJson(form["hdfFileUpload"]));

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    //string purno = resData.resp_parameters[0].v.Encrypt();
                    string reason = "";
                    string path = string.Format("~/CPAIMVC/EstimateFreightCalculation/Create?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt());
                    return Redirect(path);
                }
            }

            return View("Create", model);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            this.updateModel(form, model);
            if (model.fcl_results.offer_freight.offer_unit.ToUpper() == "LUMPSUM")
            {
                if (!String.IsNullOrEmpty(model.FreightOfferUSD) && !String.IsNullOrEmpty(model.FreightOfferTHB))
                {
                    var usd = Convert.ToDecimal(model.FreightOfferUSD);
                    var baht = Convert.ToDecimal(model.FreightOfferTHB);
                    model.fcl_results.offer_freight.usd = usd.ToString();
                    model.fcl_results.offer_freight.baht = baht.ToString();
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(model.FreightOfferUSDprice) && !String.IsNullOrEmpty(model.FreightOfferTHBprice))
                {
                    var usd = Convert.ToDecimal(model.FreightOfferUSDprice);
                    var baht = Convert.ToDecimal(model.FreightOfferTHBprice);
                    model.fcl_results.offer_freight.usd = usd.ToString();
                    model.fcl_results.offer_freight.baht = baht.ToString();
                }
            }
            HttpPostedFileBase upload = Request.Files.Count > 0 ? Request.Files[0] : null;
            var image = upload;
            string uploadImageName = "";
            if (upload.ContentLength > 0)
            {
                string type = ConstantPrm.SYSTEM.FREIGHT;
                string FNID = ConstantPrm.FUNCTION.F10000008;
                string typename = Path.GetExtension(image.FileName).ToLower();
                uploadImageName = string.Format("{0}_{2}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, FNID);
                model.fcl_pic_path = uploadImageName + typename;
                var uploadPicRes = UploadFileImage(upload, "Web/FileUpload/FREIGHT/IMAGE", uploadImageName + typename);
            }

            ResponseData resData = SaveDataFreightCalculation(_status, model, note, getFileUploadJson(form["hdfFileUpload"]));

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    //string purno = resData.resp_parameters[0].v.Encrypt();
                    string reason = "";
                    string path = string.Format("~/CPAIMVC/EstimateFreightCalculation/Create?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt());
                    return Redirect(path);
                }
            }
            return View("Create", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenPDF")]
        public ActionResult GenPDF(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            this.updateModel(form, model, true);

            FclRootObject obj = new FclRootObject();
            obj.fcl_excel_file_name = model.fcl_excel_file_name;
            obj.fcl_vessel_particular = model.fcl_vessel_particular;
            obj.fcl_payment_terms = model.fcl_payment_terms;
            obj.fcl_time_for_operation = model.fcl_time_for_operation;
            obj.total_bunker_cost = model.total_bunker_cost;
            obj.total_port_charge = model.total_port_charge;
            obj.fcl_vessel_speed = model.fcl_vessel_speed;
            obj.fcl_latest_bunker_price = model.fcl_latest_bunker_price;
            obj.fcl_bunker_consumption = model.fcl_bunker_consumption;
            obj.fcl_pic_path = model.fcl_pic_path;
            obj.fcl_remark = model.fcl_remark;
            obj.fcl_bunker_cost = model.fcl_bunker_cost;
            obj.fcl_other_cost = model.fcl_other_cost;
            obj.fcl_results = model.fcl_results;
            obj.fcl_exchange_rate = model.fcl_exchange_rate;
            obj.fcl_exchange_date = model.fcl_exchange_date;
            obj.fcl_button_event = model.buttonEvent;
            //var json = new JavaScriptSerializer().Serialize(obj);
            Session["FclRootObject"] = obj;

            return Redirect("~/Web/Report/FreightCalculationReport.aspx");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenExcel")]
        public void GenExcel(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            GenExcelFile(form, model);
        }

        System.Drawing.Color BLUE = System.Drawing.Color.DodgerBlue;
        System.Drawing.Color RED = System.Drawing.Color.Red;
        System.Drawing.Color WHITE = System.Drawing.Color.White;
        [HttpPost]
        [ValidateInput(false)]
        //[MultipleButton(Name = "action", Argument = "GenExcel")]
        public ActionResult GenExcelFile(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            string year = "";
            if (string.IsNullOrEmpty(model.fcl_vessel_particular.date_time))
            {
                year = DateTime.Now.ToString("yyyy");
            }
            else
            {
                year = model.fcl_vessel_particular.date_time.SplitWord("/")[2];
            }

            string vessel = "VESSEL";
            if (!string.IsNullOrEmpty(model.fcl_vessel_particular.vessel_name))
            {
                vessel = VehicleServiceModel.GetNameInitial(model.fcl_vessel_particular.vessel_name);
            }
            string file_name = string.Format("Freight{0}-{1} ({2}) [{3}].xlsx", year, vessel, form["pur_no"], DateTime.Now.ToString("yyyyMMddHHmmss"));

            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "FREIGHT", file_name);

            if (model.fcl_bunker_cost != null)
            {
                if (model.fcl_bunker_cost.Count == 0)
                {
                    model.fcl_bunker_cost = new List<FclBunkerCost>();
                    model.fcl_bunker_cost.Add(new FclBunkerCost());
                }
            }
            else
            {
                model.fcl_bunker_cost = new List<FclBunkerCost>();
                model.fcl_bunker_cost.Add(new FclBunkerCost());
            }

            updateModel(form, model, true);

            df = new EstimateFreightServiceModel();
            FileInfo excelFile = new FileInfo(file_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Estimated Freight Calculation");

                //Add an image.
                string img_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web/FileUpload/FREIGHT/IMAGE", model.fcl_pic_path == null ? "" : model.fcl_pic_path);
                if (System.IO.File.Exists(img_path))
                {
                    AddImage(ws, 50, 0, img_path);
                }
                string _stagement = "";

                //Initialize columns
                ws.Column(1).Width = 32;
                ws.Column(2).Width = 25;
                ws.Column(3).Width = 32;
                ws.Column(4).Width = 25;
                ws.Column(5).Width = 24;
                ws.Column(6).Width = 12;
                ws.Column(7).Width = 12;
                ws.Column(8).Width = 12;
                ws.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Column(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 9));

                ws.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                int indexRunning = 1;
                int indexTotalCommission = 1;
                int indexOfferfreight = 1;

                ws.Cells["A1:H1"].Merge = true;
                ws.Cells["A1:H1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                #region SET Vessel Particular

                #region Set Font And Background color
                ws.Cells["B3:B7"].Style.Font.Bold = true;
                //ws.Cells["B3:B7"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["B3:B7"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["B3:B7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //ws.Cells["B5:B7"].Style.Font.Color.SetColor(RED);
                ws.Cells["D3:D7"].Style.Font.Bold = true;
                //ws.Cells["D3:D7"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["D3:D7"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["D3:D7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //ws.Cells["D3:D7"].Style.Font.Color.SetColor(RED);
                ws.Cells["F3:H4"].Style.Font.Bold = true;
                //ws.Cells["F3:H4"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["F3:H4"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["F3:H4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //ws.Cells["F3:H4"].Style.Font.Color.SetColor(RED);
                ws.Cells["F6:H8"].Style.Font.Bold = true;
                //ws.Cells["F6:H8"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["F6:H8"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["F6:H8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["F6:H7"].Style.Font.Color.SetColor(RED);

                ws.Cells["G3:G4"].Style.Numberformat.Format = "#,##0.0000";
                ws.Cells["H3:H4"].Style.Numberformat.Format = "#,##0.0000";
                ws.Cells["H6:H7"].Style.Numberformat.Format = "#,##0.0000";
                ws.Cells["D3:D6"].Style.Numberformat.Format = "#,##0.0000";
                #endregion

                #region Set Value
                ws.Cells[1, 1].Value = "Estimated Freight Calculation";
                ws.Cells[1, 1].Style.Font.Size = 10;
                ws.Cells[1, 1].Style.Font.Bold = true;
                //ws.Cells[1, 1].Style.Font.Color.SetColor(BLUE);

                ws.Cells["A2:D2"].Merge = true;
                ws.Cells[2, 1].Value = "Vessel Particular";
                ws.Cells[2, 5].Value = "Vessel speed (Knots)";
                ws.Cells[2, 6].Value = "Select";
                ws.Cells[2, 7].Value = "Ballast";
                ws.Cells[2, 8].Value = "Laden";
                ws.Cells["A2:H2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["A2:H2"].Style.Font.Bold = true;
                ws.Cells["A2:H2"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells["A2:H2"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));

                ws.Cells[3, 1].Value = "Calculation Date";
                string date_year = "";
                if (string.IsNullOrEmpty(model.fcl_vessel_particular.date_time))
                {
                    model.fcl_vessel_particular.date_time = "";
                }
                else
                {
                    string[] date = model.fcl_vessel_particular.date_time.Split('/');
                    if (date.Count() > 0)
                    {
                        date_year = date[2];
                        model.fcl_vessel_particular.date_time = date[0] + "/" + date[1] + "/" + date[2];
                    }
                }
                ws.Cells[3, 2].Style.Numberformat.Format = "@";
                ws.Cells[3, 2].Value = model.fcl_vessel_particular.date_time;
                ws.Cells[3, 3].Value = "DWT";
                ws.Cells[3, 4].Formula = model.fcl_vessel_particular.dwt;
                ws.Cells[3, 5].Value = "Full";
                if (model.fcl_vessel_speed.fcl_full.flag != "")
                    ws.Cells[3, 6].Value = "x";
                ws.Cells[3, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells[3, 7].Formula = model.fcl_vessel_speed.fcl_full.ballast;
                ws.Cells[3, 8].Formula = model.fcl_vessel_speed.fcl_full.laden;

                ws.Cells[4, 1].Value = "Vessel Name";
                ws.Cells[4, 2].Value = model.fcl_vessel_particular.vessel_name;
                ws.Cells[4, 3].Value = "Displacement (MT)";
                ws.Cells[4, 4].Formula = model.fcl_vessel_particular.displacement;
                ws.Cells[4, 5].Value = "ECO";
                if (model.fcl_vessel_speed.fcl_eco.flag != "")
                    ws.Cells[4, 6].Value = "x";
                ws.Cells[4, 6].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells[4, 7].Formula = model.fcl_vessel_speed.fcl_eco.ballast;
                ws.Cells[4, 8].Formula = model.fcl_vessel_speed.fcl_eco.laden;

                ws.Cells[5, 1].Value = "Type";
                ws.Cells[5, 2].Value = model.fcl_vessel_particular.type;
                ws.Cells[5, 2].Style.WrapText = true;
                ws.Cells[5, 3].Value = "Capacity 98% (m3)";
                ws.Cells[5, 4].Formula = model.fcl_vessel_particular.capacity;
                ws.Cells["F5:H5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["E5:H5"].Style.Font.Bold = true;
                ws.Cells["E5:H5"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells["E5:H5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                //ws.Cells["E5:H5"].Style.Font.Color.SetColor(BLUE);
                ws.Cells[5, 5].Value = "Latest bunker price info";
                ws.Cells[5, 6].Value = "Date";
                ws.Cells[5, 7].Value = "Place";
                ws.Cells[5, 8].Value = "Price(USD/MT)";

                ws.Cells[6, 1].Value = "Built";
                string built_year = "";
                if (string.IsNullOrEmpty(model.fcl_vessel_particular.built))
                {
                    model.fcl_vessel_particular.built = "";
                }
                else
                {
                    string[] date = model.fcl_vessel_particular.built.Split('/');
                    if (date.Count() > 0)
                    {
                        built_year = date[2];
                        model.fcl_vessel_particular.built = date[0] + "/" + date[1] + "/" + date[2];
                    }
                }
                ws.Cells[6, 2].Style.Numberformat.Format = "@";
                ws.Cells[6, 2].Value = model.fcl_vessel_particular.built;
                ws.Cells[6, 3].Value = "Max Draft (M)";
                ws.Cells[6, 4].Formula = model.fcl_vessel_particular.max_draft;
                ws.Cells[6, 5].Value = "Grade FO";
                if (model.mode == "INDEX")
                {
                    model.fcl_latest_bunker_price = df.GetLastBunker(model.vessel_id, null);
                }
                ws.Cells[6, 6].Style.Numberformat.Format = "@";
                ws.Cells[6, 6].Value = model.fcl_latest_bunker_price.fcl_grade_fo.date;
                ws.Cells[6, 7].Value = model.fcl_latest_bunker_price.fcl_grade_fo.place;
                ws.Cells[6, 7].Style.WrapText = true;
                ws.Cells[6, 8].Formula = EmtrytoZero(model.fcl_latest_bunker_price.fcl_grade_fo.price);

                ws.Cells[7, 1].Value = "Age";
                if (date_year != "" && built_year != "")
                    ws.Cells[7, 2].Value = (int.Parse(date_year) - int.Parse(built_year)).ToString();
                ws.Cells[7, 3].Value = "Cargo Tanks Coating";
                ws.Cells[7, 4].Value = model.fcl_vessel_particular.coating;
                ws.Cells[7, 4].Style.WrapText = true;
                ws.Cells[7, 5].Value = "Grade MGO";
                ws.Cells[7, 6].Style.Numberformat.Format = "@";
                ws.Cells[7, 6].Value = model.fcl_latest_bunker_price.fcl_grade_mgo.date;
                ws.Cells[7, 7].Value = model.fcl_latest_bunker_price.fcl_grade_mgo.place;
                ws.Cells[7, 7].Style.WrapText = true;
                ws.Cells[7, 8].Formula = EmtrytoZero(model.fcl_latest_bunker_price.fcl_grade_mgo.price);
                #endregion

                #endregion

                #region Set Main Terms
                int startValueIndex = 9;

                ws.Cells["A8:H8"].Style.Font.Bold = true;
                //ws.Cells["A8:H8"].Style.Font.Color.SetColor(BLUE);
                ws.Cells["A8:D8"].Merge = true;
                ws.Cells["A8:D8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells[8, 1].Value = "Main Terms";
                int indexExchangeRate = 8;
                ws.Cells[8, 5].Value = "Exchange rate";
                //ws.Cells["F8:H8"].Merge = true;
                ws.Cells["F8:F8"].Style.Font.Color.SetColor(RED);
                ws.Cells[8, 6].Formula = model.mode == "INDEX" ? df.GetExchangeRate(model.fcl_vessel_particular.date_time) : model.fcl_exchange_rate;
                ws.Cells[8, 6].Style.Numberformat.Format = "#,##0.0000";

                ws.Cells[8, 7].Value = "As of";
                ws.Cells[8, 8].Value = model.fcl_exchange_date;

                ws.Cells["A8:E8"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells["A8:E8"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                ws.Cells["G8:G8"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells["G8:G8"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));

                ws.Cells["F10:F16"].Style.Numberformat.Format = "#,##0.0000";
                ws.Cells["G10:G16"].Style.Numberformat.Format = "#,##0.0000";

                ws.Cells[9, 1].Value = "Charterer";
                ws.Cells[9, 2].Value = model.fcl_payment_terms.charterer;
                ws.Cells[9, 2].Style.WrapText = true;
                if (!string.IsNullOrEmpty(model.fcl_payment_terms.dem_unit))
                {
                    ws.Cells[9, 3].Value = "Demurrage (" + model.fcl_payment_terms.dem_unit + ")";
                }
                else
                {
                    ws.Cells[9, 3].Value = "Demurrage (PDPR)";
                }

                ws.Cells[9, 4].Formula = model.fcl_payment_terms.dem;
                ws.Cells[9, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                //ws.Cells[9, 3].Value = "Unit";
                //ws.Cells[9, 4].Value = model.fcl_payment_terms.unit;
                ws.Cells["F9:H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["E9:H9"].Style.Font.Bold = true;
                //ws.Cells["E9:H9"].Style.Font.Color.SetColor(BLUE);
                ws.Cells[9, 5].Value = "Bunker consumption (MT/Day)";
                ws.Cells[9, 6].Value = "FO";
                ws.Cells[9, 7].Value = "MGO";
                ws.Cells[9, 8].Value = "";
                ws.Cells["E9:G9"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells["E9:G9"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));

                //-------------------------------------------------Generate Cargoes
                int indexStart = 10;//------------------------Default index
                indexRunning = indexStart;

                string indexSumQuantityMT = "";

                #region Set Value column A to B Gen Cargoes
                //string unit = string.IsNullOrEmpty(model.fcl_payment_terms.unit) ? "MT" : model.fcl_payment_terms.unit;
                if (model.fcl_payment_terms.Cargoes != null)
                {
                    int index = 0;
                    if (model.fcl_payment_terms.Cargoes.Count > 0)
                    {
                        foreach (var val in model.fcl_payment_terms.Cargoes)
                        {
                            index += 1;

                            string cargoName = "";
                            if (val.cargo != null && val.cargo != "")
                            { cargoName = model.cargo.Where(x => x.Value == val.cargo).FirstOrDefault().Text; }

                            if(model.fcl_payment_terms.Cargoes.Count == 1)
                            {
                                ws.Cells[indexRunning, 1].Value = "Cargo ";
                            }
                            else
                            {
                                ws.Cells[indexRunning, 1].Value = "Cargo " + index;
                            }
                            
                            ws.Cells[indexRunning, 2].Value = cargoName;
                            ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                            ws.Cells[indexRunning, 2].Style.WrapText = true;
                            indexRunning += 1;

                            string cargoQtyMT = "0";
                            if (val.quantity != null && val.quantity != "")
                            { cargoQtyMT = val.quantity; }
                            if (model.fcl_payment_terms.Cargoes.Count == 1)
                            {
                                ws.Cells[indexRunning, 1].Value = "Quantity " + " (" + (val.unit == "OTHER" ? val.unit_other : val.unit) + ")";
                            }
                            else
                            {
                                ws.Cells[indexRunning, 1].Value = "Quantity " + index + " (" + (val.unit == "OTHER" ? val.unit_other : val.unit) + ")";
                            }
                            
                            ws.Cells[indexRunning, 2].Formula = cargoQtyMT;
                            ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                            if (model.fcl_payment_terms.Cargoes.Count == 1)
                            {
                                indexSumQuantityMT = indexSumQuantityMT + "+B" + indexRunning;
                            }
                            indexRunning += 1;
                            if (model.fcl_payment_terms.Cargoes.Count == 1)
                            {
                                ws.Cells[indexRunning, 1].Value = "Tolerance " + " (+/-) (%)";
                            }
                            else
                            {
                                ws.Cells[indexRunning, 1].Value = "Tolerance " + index + " (+/-) (%)";
                            }
                           
                            ws.Cells[indexRunning, 2].Formula = string.IsNullOrEmpty(val.tolerance) ? "0" : val.tolerance;
                            ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                            indexRunning += 1;
                        }
                    }
                    else
                    {
                        ws.Cells[indexRunning, 1].Value = "Cargo";
                        ws.Cells[indexRunning, 2].Value = "";
                        ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                        ws.Cells[indexRunning, 2].Style.WrapText = true;
                        indexRunning += 1;
                        ws.Cells[indexRunning, 1].Value = "Quantity (MT)";
                        ws.Cells[indexRunning, 2].Formula = "0";
                        ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                        indexSumQuantityMT = indexSumQuantityMT + "+B" + indexRunning;
                        indexRunning += 1;
                        ws.Cells[indexRunning, 1].Value = "Tolerance (+/-) (%)";
                        ws.Cells[indexRunning, 2].Formula = "0";
                        ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                        indexRunning += 1;
                    }
                }
                else
                {
                    ws.Cells[indexRunning, 1].Value = "Cargo";
                    ws.Cells[indexRunning, 2].Value = "";
                    ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                    ws.Cells[indexRunning, 2].Style.WrapText = true;
                    indexRunning += 1;
                    ws.Cells[indexRunning, 1].Value = "Quantity (MT)";
                    ws.Cells[indexRunning, 2].Formula = "0";
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    indexRunning += 1;
                    ws.Cells[indexRunning, 1].Value = "Tolerance (+/-) (%)";
                    ws.Cells[indexRunning, 2].Formula = "0";
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    indexRunning += 1;
                }
                #endregion

                ws.Cells[indexRunning, 1].Value = "Laycan";
                if (!string.IsNullOrEmpty(model.fcl_payment_terms.laycan_from))
                    ws.Cells[indexRunning, 2].Value = model.fcl_payment_terms.laycan_from + " to " + model.fcl_payment_terms.laycan_to;
                indexRunning += 1;

                int indexLaytime = indexRunning;
                ws.Cells[indexRunning, 1].Value = "Laytime (Hrs)";
                ws.Cells[indexRunning, 2].Formula = !string.IsNullOrEmpty(model.fcl_payment_terms.laytime) ? model.fcl_payment_terms.laytime : "0";
                ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                indexRunning += 1;

                ws.Cells[indexRunning, 1].Value = "BSS";
                ws.Cells[indexRunning, 2].Value = model.fcl_payment_terms.bss;
                indexRunning += 1;

                #region Set Value column A to B Gen Port and Discharge
                if (model.fcl_payment_terms.ControlPort != null)
                {
                    int index = 0;
                    if (model.fcl_payment_terms.ControlPort.Count > 0)
                    {
                        foreach (var val in model.fcl_payment_terms.ControlPort)
                        {
                            index += 1;
                            if(model.fcl_payment_terms.ControlPort.Count == 1)
                            {
                                ws.Cells[indexRunning, 1].Value = "Load Port ";
                            }
                            else
                            {
                                ws.Cells[indexRunning, 1].Value = "Load Port " + index;
                            }
                            
                            string ttt = "";
                            if (!string.IsNullOrEmpty(val.PortOtherDesc))
                            {
                                ttt = val.PortOtherDesc + val.PortFullName.Replace("OTHER", "");
                            }
                            else
                            {
                                ttt = val.PortFullName.Replace("OTHER,", "").Trim();
                            }
                            //ws.Cells[indexRunning, 2].Value = !string.IsNullOrEmpty(val.PortOtherDesc) ? val.PortFullName.Replace("OTHER", "OTHER (" + val.PortOtherDesc + ")") : val.PortFullName;
                            ws.Cells[indexRunning, 2].Value = ttt;
                            ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                            ws.Cells[indexRunning, 2].Style.WrapText = true;
                            ws.Cells[indexRunning, 9].Value = val.JettyPortKey;
                            ws.Cells[indexRunning, 9].Style.Font.Color.SetColor(WHITE);
                            ws.Cells[indexRunning, 10].Value = val.PortOtherDesc;
                            ws.Cells[indexRunning, 10].Style.Font.Color.SetColor(WHITE);
                            indexRunning += 1;
                        }
                    }
                    else
                    {
                        ws.Cells[indexRunning, 1].Value = "Load Port";
                        ws.Cells[indexRunning, 2].Value = "";
                        ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                        ws.Cells[indexRunning, 2].Style.WrapText = true;
                        indexRunning += 1;
                    }
                }
                else
                {
                    ws.Cells[indexRunning, 1].Value = "Load Port";
                    ws.Cells[indexRunning, 2].Value = "";
                    ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                    ws.Cells[indexRunning, 2].Style.WrapText = true;
                    indexRunning += 1;
                }

                if (model.fcl_payment_terms.ControlDischarge != null)
                {
                    int index = 0;
                    if (model.fcl_payment_terms.ControlDischarge.Count > 0)
                    {
                        foreach (var val in model.fcl_payment_terms.ControlDischarge)
                        {
                            index += 1;
                            if(model.fcl_payment_terms.ControlDischarge.Count == 1)
                            {
                                ws.Cells[indexRunning, 1].Value = "Discharge Port ";
                            }
                            else
                            {
                                ws.Cells[indexRunning, 1].Value = "Discharge Port " + index;
                            }
                            
                            string ttt = "";
                            if (!string.IsNullOrEmpty(val.PortOtherDesc))
                            {
                                ttt = val.PortOtherDesc + val.DischargeFullName.Replace("OTHER", "");
                            }
                            else
                            {
                                ttt = val.DischargeFullName.Replace("OTHER,", "").Trim();
                            }
                            ws.Cells[indexRunning, 2].Value = ttt;
                            //ws.Cells[indexRunning, 2].Value = !string.IsNullOrEmpty(val.PortOtherDesc) ? val.DischargeFullName.Replace("OTHER", "OTHER (" + val.PortOtherDesc + ")") : val.DischargeFullName;
                            ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                            ws.Cells[indexRunning, 2].Style.WrapText = true;
                            ws.Cells[indexRunning, 9].Value = val.JettyPortKey;
                            ws.Cells[indexRunning, 9].Style.Font.Color.SetColor(WHITE);
                            ws.Cells[indexRunning, 10].Value = val.PortOtherDesc;
                            ws.Cells[indexRunning, 10].Style.Font.Color.SetColor(WHITE);
                            indexRunning += 1;
                        }
                    }
                    else
                    {
                        ws.Cells[indexRunning, 1].Value = "Discharge Port";
                        ws.Cells[indexRunning, 2].Value = "";
                        ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                        ws.Cells[indexRunning, 2].Style.WrapText = true;
                        indexRunning += 1;
                    }
                }
                else
                {
                    ws.Cells[indexRunning, 1].Value = "Discharge Port";
                    ws.Cells[indexRunning, 2].Value = "";
                    ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                    ws.Cells[indexRunning, 2].Style.WrapText = true;
                    indexRunning += 1;
                }

                //int indexLaytime = indexRunning;
                //ws.Cells[indexRunning, 1].Value = "Laytime (Hrs)";
                //ws.Cells[indexRunning, 2].Formula = model.fcl_payment_terms.laytime;
                //ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";

                int endValueIndex = indexRunning;
                indexRunning += 1;

                #endregion

                #region Set Value column C to H

                //ws.Cells[indexStart, 3].Value = "Demurrage (PDPR)";
                //ws.Cells[indexStart, 4].Formula = EmtrytoZero(model.fcl_payment_terms.dem);               
                //ws.Cells[indexStart, 4].Style.WrapText = true;
                ws.Cells[indexStart, 3].Value = "Payment Term";
                ws.Cells[indexStart, 4].Style.Font.Color.SetColor(RED);
                ws.Cells[indexStart, 4].Value = model.fcl_payment_terms.payment_term;
                ws.Cells[indexStart, 4].Style.WrapText = true;
                ws.Cells[indexStart, 5].Value = "Load";
                ws.Cells[indexStart, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_load.fo);
                ws.Cells[indexStart, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_load.mgo);
                ws.Cells[indexStart, 8].Value = "";

                //ws.Cells[indexStart + 1, 3].Value = "Payment Term";
                //ws.Cells[indexStart + 1, 4].Style.Font.Color.SetColor(RED);
                //ws.Cells[indexStart + 1, 4].Value = model.fcl_payment_terms.payment_term;
                //ws.Cells[indexStart + 1, 4].Style.WrapText = true;
                ws.Cells[indexStart + 1, 3].Value = "Transit Loss (%)";
                ws.Cells[indexStart + 1, 4].Formula = EmtrytoZero(model.fcl_payment_terms.transit_loss);
                ws.Cells[indexStart + 1, 4].Style.WrapText = true;
                ws.Cells[indexStart + 1, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Cells[indexStart + 1, 5].Value = "Discharge";
                ws.Cells[indexStart + 1, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_discharge.fo);
                ws.Cells[indexStart + 1, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_discharge.mgo);
                ws.Cells[indexStart + 1, 8].Value = "";

                //ws.Cells[indexStart + 2, 3].Value = "Transit Loss (%)";
                //ws.Cells[indexStart + 2, 4].Formula = EmtrytoZero(model.fcl_payment_terms.transit_loss);
                //ws.Cells[indexStart + 2, 4].Style.WrapText = true;
                ws.Cells[indexStart + 2, 3].Value = "Broker Name";
                ws.Cells[indexStart + 2, 4].Value = model.fcl_payment_terms.broker;
                ws.Cells[indexStart + 2, 4].Style.WrapText = true;
                ws.Cells[indexStart + 2, 5].Value = "Steaming Ballast";
                ws.Cells[indexStart + 2, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_steaming_ballast.fo);
                ws.Cells[indexStart + 2, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_steaming_ballast.mgo);
                ws.Cells[indexStart + 2, 8].Value = "";

                ws.Cells["D9"].Style.Numberformat.Format = "#,##0.0000";
                ws.Cells["D11"].Style.Numberformat.Format = "#,##0.0000";
                ws.Cells["D13:D16"].Style.Numberformat.Format = "#,##0.0000";

                //ws.Cells[indexStart + 3, 3].Value = "Broker Name";
                //ws.Cells[indexStart + 3, 4].Value = model.fcl_payment_terms.broker;
                //ws.Cells[indexStart + 3, 4].Style.WrapText = true;
                ws.Cells[indexStart + 3, 3].Value = "Broker Commission (%)";
                ws.Cells[indexStart + 3, 4].Formula = EmtrytoZero(model.fcl_payment_terms.broker_comm);
                ws.Cells[indexStart + 3, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Cells[indexStart + 3, 5].Value = "Steaming Laden";
                ws.Cells[indexStart + 3, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_steaming_laden.fo);
                ws.Cells[indexStart + 3, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_steaming_laden.mgo);
                ws.Cells[indexStart + 3, 8].Value = "";

                //ws.Cells[indexStart + 4, 3].Value = "Broker Commission (%)";
                //ws.Cells[indexStart + 4, 4].Formula = EmtrytoZero(model.fcl_payment_terms.broker_comm);
                ws.Cells[indexStart + 4, 3].Value = "Address Commission (%)";
                ws.Cells[indexStart + 4, 4].Formula = EmtrytoZero(model.fcl_payment_terms.add_comm);
                ws.Cells[indexStart + 4, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Cells[indexStart + 4, 5].Value = "Idle";
                ws.Cells[indexStart + 4, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_idle.fo);
                ws.Cells[indexStart + 4, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_idle.mgo);
                ws.Cells[indexStart + 4, 8].Value = "";

                //ws.Cells[indexStart + 5, 3].Value = "Address Commission (%)";
                //ws.Cells[indexStart + 5, 4].Formula = EmtrytoZero(model.fcl_payment_terms.add_comm);
                _stagement = "ROUND(D" + (indexStart + 3) + ", 4)+ROUND(D" + (indexStart + 4) + ", 4)";
                ws.Cells[indexStart + 5, 3].Value = "Total Commission (%)";
                if (model.mode == "CREATE")
                {
                    ws.Cells[indexStart + 5, 4].Formula = EmtrytoZero(model.fcl_payment_terms.total_comm);
                }
                else
                {
                    ws.Cells[indexStart + 5, 4].Formula = _stagement;
                }
                ws.Cells[indexStart + 5, 3].Style.Font.Bold = true;
                ws.Cells[indexStart + 5, 4].Style.Font.Bold = true;
                ws.Cells[indexStart + 5, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                indexTotalCommission = indexStart + 6;
                ws.Cells[indexStart + 5, 5].Value = "Tank cleaning";
                ws.Cells[indexStart + 5, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_tack_cleaning.fo);
                ws.Cells[indexStart + 5, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_tack_cleaning.mgo);
                ws.Cells[indexStart + 5, 8].Value = "";

                //_stagement = "ROUND(D" + (indexStart + 4) + ", 4)+ROUND(D" + (indexStart + 5) + ", 4)";
                //ws.Cells[indexStart + 6, 3].Value = "Total Commission (%)";                
                //if (model.mode == "CREATE")
                //{
                //    ws.Cells[indexStart + 6, 4].Formula = EmtrytoZero(model.fcl_payment_terms.total_comm);
                //}
                //else
                //{
                //    ws.Cells[indexStart + 6, 4].Formula = _stagement;
                //}
                //ws.Cells[indexStart + 6, 3].Style.Font.Bold = true;
                //ws.Cells[indexStart + 6, 4].Style.Font.Bold = true;
                //indexTotalCommission = indexStart + 6;
                ws.Cells[indexStart + 6, 3].Value = "Witholding Tax (%)";
                ws.Cells[indexStart + 6, 4].Formula = EmtrytoZero(model.fcl_payment_terms.withold_tax);
                ws.Cells[indexStart + 6, 4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ws.Cells[indexStart + 6, 5].Value = "Heating";
                ws.Cells[indexStart + 6, 6].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_heating.fo);
                ws.Cells[indexStart + 6, 7].Formula = EmtrytoZero(model.fcl_bunker_consumption.fcl_heating.mgo);
                ws.Cells[indexStart + 6, 8].Value = "";

                int sumValue = (model.fcl_payment_terms.ControlPort != null ? model.fcl_payment_terms.ControlPort.Count() : 0) + (model.fcl_payment_terms.ControlDischarge != null ? model.fcl_payment_terms.ControlDischarge.Count() : 0);
                int portDischargeAdd = sumValue > 2 ? sumValue - 2 : 0;
                for (int i = 1; i <= portDischargeAdd; i++)
                {
                    ws.Cells[indexStart + 6 + i, 3].Value = "";
                    ws.Cells[indexStart + 6 + i, 4].Value = "";
                    ws.Cells[indexStart + 6 + i, 5].Value = "";
                    ws.Cells[indexStart + 6 + i, 6].Value = "";
                    ws.Cells[indexStart + 6 + i, 7].Value = "";
                    ws.Cells[indexStart + 6 + i, 8].Value = "";
                }

                int indexWitholdingTaxPercent = indexStart + 6;
                int endValueIndex_D = indexRunning - 1;
                if ((model.fcl_payment_terms.Cargoes == null || model.fcl_payment_terms.Cargoes.Count == 1) &&
                    (model.fcl_payment_terms.bss == "1:1" || model.fcl_payment_terms.bss == ""))
                {
                    //indexRunning++;
                    endValueIndex_D = indexRunning;
                }
                //ws.Cells[indexStart + 7, 3].Value = "Witholding Tax (%)";
                //ws.Cells[indexStart + 7, 4].Formula = EmtrytoZero(model.fcl_payment_terms.withold_tax);

                #endregion

                #region Set Color 
                //ws.Cells["B"+ startValueIndex+":B"+ endValueIndex].Style.Font.Bold = true;
                //ws.Cells["B" + startValueIndex + ":B" + endValueIndex].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["B" + startValueIndex + ":B" + endValueIndex].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["B" + startValueIndex + ":B" + endValueIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //ws.Cells["B"+ startValueIndex+":B"+ endValueIndex].Style.Font.Color.SetColor(RED);

                //ws.Cells["D" + startValueIndex + ":D" + endValueIndex_D].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["D" + startValueIndex + ":D" + endValueIndex_D].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["D" + startValueIndex + ":D" + (endValueIndex_D - 1)].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                //ws.Cells["F" + (startValueIndex + 1) + ":G" + endValueIndex].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //ws.Cells["F" + (startValueIndex + 1) + ":G" + endValueIndex].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                ws.Cells["F" + (startValueIndex + 1) + ":G" + endValueIndex].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                #endregion

                #endregion

                if (1 == 1)
                {
                    int indexTimeForOprationStart = indexRunning;
                    int indexTimeForOprationEnd = indexRunning;
                    int indexBunkerCostStart = indexRunning;
                    int indexBunkerCostEnd = indexRunning;

                    #region SET Time for operation (Days)
                    ws.Cells["A" + indexRunning + ":H" + indexRunning].Style.Font.Bold = true;
                    //ws.Cells["A" + indexRunning + ":H" + indexRunning].Style.Font.Color.SetColor(BLUE);
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Merge = true;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    ws.Cells[indexRunning, 1].Value = "Time for operation(Days)";
                    indexRunning += 1;

                    //add row 1
                    ws.Cells[indexRunning, 1].Value = "Time spending at loading port";
                    _stagement = "(ROUND(B" + indexLaytime + ", 4)/2+6)/24";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_time_for_operation.loading_port;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 3].Value = "Distance (Laden) NM";
                    ws.Cells[indexRunning, 4].Formula = EmtrytoZero(model.fcl_time_for_operation.distance_laden);
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    indexRunning += 1;

                    //add row 2
                    ws.Cells[indexRunning, 1].Value = "Time spending at discharging port";
                    ws.Cells[indexRunning, 1].Style.WrapText = true;
                    _stagement = "(ROUND(B" + indexLaytime + ", 4)/2+6)/24";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_time_for_operation.discharge_port;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 3].Value = "Time steaming (Laden)";
                    _stagement = "D" + (indexRunning - 1) + "/24/IF(F3 = \"x\", ROUND(H3, 4), ROUND(H4, 4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 4].Formula = model.fcl_time_for_operation.laden;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 4].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    indexRunning += 1;

                    //add row 3
                    ws.Cells[indexRunning, 1].Value = "Distance (Ballast) NM";
                    ws.Cells[indexRunning, 2].Formula = EmtrytoZero(model.fcl_time_for_operation.distance_ballast);
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 3].Value = "Time for tank cleaning";
                    _stagement = "IF(1.5 - ROUND(B" + (indexRunning + 1) + ", 4) < 0, 0, 1.5 - ROUND(B" + (indexRunning + 1) + ", 4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 4].Formula = model.fcl_time_for_operation.tank_cleaning;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 4].Formula = _stagement;// model.fcl_time_for_operation.total_duration;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    //ws.Cells[indexRunning, 4].Style.Font.Bold = true;
                    indexTimeForOprationEnd = indexRunning;
                    indexRunning += 1;

                    //add row 4
                    ws.Cells[indexRunning, 1].Value = "Time steaming (Ballast)";
                    _stagement = "B" + (indexRunning - 1) + "/24/IF(F3 = \"x\", ROUND(G3, 4), ROUND(G4, 4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_time_for_operation.ballast;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;// model.fcl_time_for_operation.ballast;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 3].Value = "Total duration per voyage";
                    ws.Cells[indexRunning, 3].Style.Font.Bold = true;
                    _stagement = "ROUND(SUM(B" + (indexRunning - 3) + ":B" + (indexRunning - 2) + ", B" + indexRunning + ", D" + (indexRunning - 2) + ":D" + (indexRunning - 1) + "),4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 4].Formula = model.fcl_time_for_operation.total_duration;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 4].Formula = _stagement;// model.fcl_time_for_operation.total_duration;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 4].Style.Font.Bold = true;
                    indexTimeForOprationEnd = indexRunning;
                    indexRunning += 1;

                    #region Set Style for Time for operation (Days)
                    //ws.Cells["B" + (indexTimeForOprationStart + 1) + ":B" + indexTimeForOprationEnd].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["B" + (indexTimeForOprationStart + 1) + ":B" + indexTimeForOprationEnd].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["B" + (indexTimeForOprationStart + 1) + ":B" + indexTimeForOprationEnd].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                    //ws.Cells["D" + (indexTimeForOprationStart + 1) + ":D" + indexTimeForOprationEnd].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["D" + (indexTimeForOprationStart + 1) + ":D" + indexTimeForOprationEnd].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["D" + (indexTimeForOprationStart + 1) + ":D" + indexTimeForOprationEnd].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    #endregion
                    #endregion

                    #region SET Bunker Cost (USD)

                    if (model.fcl_bunker_cost.Count == 0)
                    {
                        model.fcl_bunker_cost.Add(new FclBunkerCost());
                    }

                    int indexBunkerRunning = indexBunkerCostStart;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Merge = true;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker Cost (USD)";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker price";
                    ws.Cells[indexBunkerRunning, 6].Value = "FO";
                    ws.Cells[indexBunkerRunning, 7].Value = "MGO";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Unit Price (USD/MT)";
                    ws.Cells[indexBunkerRunning, 6].Formula = "=H6";
                    ws.Cells[indexBunkerRunning, 7].Formula = "=H7";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    ws.Cells["F" + indexBunkerRunning + ":G" + indexBunkerRunning].Style.Font.Bold = true;
                    ws.Cells["F" + indexBunkerRunning + ":G" + indexBunkerRunning].Style.Font.Color.SetColor(RED);

                    indexBunkerRunning += 1;

                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Merge = true;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Font.Bold = true;
                    //ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Font.Color.SetColor(BLUE);
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["E" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker Cost (USD)";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker at loading";
                    _stagement = "(ROUND(B" + (indexBunkerCostStart + 1) + ",4)*ROUND(F10,4)*ROUND(F" + (indexRunning - 3) + ",4)) + (ROUND(B" + (indexBunkerCostStart + 1) + ",4)*ROUND(G10,4)*ROUND(G" + (indexRunning - 3) + ",4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = model.fcl_bunker_cost[0].loading;
                    }
                    else
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = _stagement;
                    }
                    ws.Cells[indexBunkerRunning, 7].Value = "";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker at discharge port";
                    _stagement = "(ROUND(B" + (indexBunkerCostStart + 2) + ",4)*ROUND(F11,4)*ROUND(F" + (indexRunning - 3) + ",4)) + (ROUND(B" + (indexBunkerCostStart + 2) + ",4)*ROUND(G11,4)*ROUND(G" + (indexRunning - 3) + ",4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = model.fcl_bunker_cost[0].discharge_port;
                    }
                    else
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = _stagement;
                    }
                    ws.Cells[indexBunkerRunning, 7].Value = "";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker at steaming ballast";
                    _stagement = "(ROUND(B" + (indexBunkerCostStart + 4) + ",4)*ROUND(F12,4)*ROUND(F" + (indexRunning - 3) + ",4)) + (ROUND(B" + (indexBunkerCostStart + 4) + ",4)*ROUND(G12,4)*ROUND(G" + (indexRunning - 3) + ",4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = model.fcl_bunker_cost[0].steaming_ballast;
                    }
                    else
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = _stagement;
                    }
                    ws.Cells[indexBunkerRunning, 7].Value = "";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker at steaming laden";
                    _stagement = "(ROUND(D" + (indexBunkerCostStart + 2) + ",4)*ROUND(F13,4)*ROUND(F" + (indexRunning - 3) + ",4)) + (ROUND(D" + (indexBunkerCostStart + 2) + ",4)*ROUND(G13,4)*ROUND(G" + (indexRunning - 3) + ",4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = model.fcl_bunker_cost[0].steaming_laden;
                    }
                    else
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = _stagement;
                    }
                    ws.Cells[indexBunkerRunning, 7].Value = "";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    indexBunkerRunning += 1;

                    ws.Cells[indexBunkerRunning, 5].Value = "Bunker at tank cleaning";
                    _stagement = "(ROUND(D" + (indexBunkerCostStart + 3) + ",4)*ROUND(F15,4)*ROUND(F" + (indexRunning - 3) + ",4)) + (ROUND(D" + (indexBunkerCostStart + 3) + ",4)*ROUND(G15,4)*ROUND(G" + (indexRunning - 3) + ",4))";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = model.fcl_bunker_cost[0].tank_cleaning;
                    }
                    else
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = _stagement;
                    }
                    ws.Cells[indexBunkerRunning, 7].Value = "";
                    ws.Cells[indexBunkerRunning, 8].Value = "";
                    indexBunkerCostEnd = indexBunkerRunning;
                    indexBunkerRunning += 1;

                    _stagement = "ROUND(SUM(F" + (indexBunkerCostStart + 4) + ":F" + indexBunkerCostEnd + "),4)";
                    int indexTotalBunkerCost = indexBunkerRunning;
                    ws.Cells[indexBunkerRunning, 5].Value = "Total Bunker Cost";
                    ws.Cells[indexBunkerRunning, 5].Style.Font.Bold = true;
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = model.total_bunker_cost;
                    }
                    else
                    {
                        ws.Cells[indexBunkerRunning, 6].Formula = _stagement;
                    }
                    ws.Cells[indexBunkerRunning, 6].Style.Font.Bold = true;
                    ws.Cells[indexBunkerRunning, 7].Value = "";
                    ws.Cells[indexBunkerRunning, 8].Value = "";

                    #region Set Style for Bunker Cost
                    //ws.Cells["F" + (indexBunkerCostStart + 2) + ":F" + indexBunkerRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["F" + (indexBunkerCostStart + 2) + ":F" + indexBunkerRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["F" + (indexBunkerCostStart + 2) + ":F" + indexBunkerRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Cells["F" + (indexBunkerCostStart + 4) + ":F" + indexBunkerRunning].Style.Numberformat.Format = "#,##0.0000";

                    //ws.Cells[indexBunkerCostStart + 2, 7].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells[indexBunkerCostStart + 2, 7].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells[indexBunkerCostStart + 2, 7].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                    ws.Cells["F" + (indexBunkerCostStart + 1) + ":G" + (indexBunkerCostStart + 1)].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["F" + (indexBunkerCostStart + 1) + ":G" + (indexBunkerCostStart + 1)].Style.Font.Bold = true;
                    //ws.Cells["F" + (indexBunkerCostStart + 1) + ":G" + (indexBunkerCostStart + 1)].Style.Font.Color.SetColor(BLUE);
                    ws.Cells["F" + (indexBunkerCostStart + 2) + ":G" + (indexBunkerCostStart + 2)].Style.Numberformat.Format = "#,##0.0000";
                    #endregion

                    indexBunkerRunning += 1;

                    #endregion

                    #region SET Port Charge
                    int indexPortChargeStart = indexRunning;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Merge = true;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Font.Bold = true;
                    //ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Font.Color.SetColor(BLUE);
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    ws.Cells[indexRunning, 1].Value = "Port Charge (USD)";
                    indexRunning += 1;

                    if (model.fcl_payment_terms.ControlPort != null)
                    {
                        int index = 0;
                        if (model.fcl_payment_terms.ControlPort.Count > 0)
                        {
                            foreach (var val in model.fcl_payment_terms.ControlPort)
                            {
                                index += 1;
                                string tPort = "";
                                if (val.PortName == "OTHER")
                                {
                                    if (val.PortOtherDesc == "")
                                    {
                                        tPort = val.PortFullName.SplitWord("OTHER, ")[1];
                                    }
                                    else
                                    {
                                        tPort = val.PortOtherDesc + ", " + val.PortFullName.SplitWord("OTHER, ")[1];
                                    }                                    
                                }
                                else
                                {
                                    tPort = val.PortFullName;
                                }
                                if(model.fcl_payment_terms.ControlPort.Count == 1)
                                {
                                    ws.Cells[indexRunning, 1].Value = "Port disbursement at load port " + "\n(" + tPort + ")";
                                }
                                else
                                {
                                    ws.Cells[indexRunning, 1].Value = "Port disbursement at load port " + index + "\n(" + tPort + ")";
                                }
                                
                                ws.Cells[indexRunning, 1].Style.WrapText = true;
                                ws.Cells[indexRunning, 2].Formula = val.PortValue;
                                ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                                ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                                ws.Cells[indexRunning, 3].Value = "";
                                ws.Cells[indexRunning, 4].Value = "";
                                ws.Cells[indexRunning, 9].Value = val.JettyPortKey;
                                ws.Cells[indexRunning, 9].Style.Font.Color.SetColor(WHITE);
                                indexRunning += 1;

                            }
                        }
                        else
                        {
                            ws.Cells[indexRunning, 1].Value = "Port disbursement at load port";
                            ws.Cells[indexRunning, 2].Value = "";
                            ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                            ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                            ws.Cells[indexRunning, 3].Value = "";
                            ws.Cells[indexRunning, 4].Value = "";
                            indexRunning += 1;
                        }
                    }
                    else
                    {
                        ws.Cells[indexRunning, 1].Value = "Port disbursement at load port";
                        ws.Cells[indexRunning, 2].Value = "";
                        ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                        ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                        ws.Cells[indexRunning, 3].Value = "";
                        ws.Cells[indexRunning, 4].Value = "";
                        indexRunning += 1;
                    }

                    if (model.fcl_payment_terms.ControlDischarge != null)
                    {
                        int index = 0;
                        if (model.fcl_payment_terms.ControlDischarge.Count > 0)
                        {
                            foreach (var val in model.fcl_payment_terms.ControlDischarge)
                            {
                                index += 1;

                                string tPort = "";
                                if (val.DischargeName == "OTHER")
                                {
                                    if (val.PortOtherDesc == "")
                                    {
                                        tPort = val.DischargeFullName.SplitWord("OTHER, ")[1];
                                    }
                                    else
                                    {
                                        tPort = val.PortOtherDesc + ", " + val.DischargeFullName.SplitWord("OTHER, ")[1];
                                    }
                                }
                                else
                                {
                                    tPort = val.DischargeFullName;
                                }

                                if(model.fcl_payment_terms.ControlDischarge.Count == 1)
                                {
                                    ws.Cells[indexRunning, 1].Value = "Port disbursement at discharging port " + "\n(" + tPort + ")";
                                }
                                else
                                {
                                    ws.Cells[indexRunning, 1].Value = "Port disbursement at discharging port " + index + "\n(" + tPort + ")";
                                }
                               
                                ws.Cells[indexRunning, 1].Style.WrapText = true;
                                ws.Cells[indexRunning, 2].Formula = val.DischargeValue;
                                ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                                ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                                ws.Cells[indexRunning, 3].Value = "";
                                ws.Cells[indexRunning, 4].Value = "";
                                ws.Cells[indexRunning, 9].Value = val.JettyPortKey;
                                ws.Cells[indexRunning, 9].Style.Font.Color.SetColor(WHITE);
                                indexRunning += 1;
                            }
                        }
                        else
                        {
                            ws.Cells[indexRunning, 1].Value = "Port disbursement at discharging port";
                            ws.Cells[indexRunning, 2].Value = "";
                            ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                            ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                            ws.Cells[indexRunning, 3].Value = "";
                            ws.Cells[indexRunning, 4].Value = "";
                            indexRunning += 1;
                        }
                    }
                    else
                    {
                        ws.Cells[indexRunning, 1].Value = "Port disbursement at discharging port";
                        ws.Cells[indexRunning, 2].Value = "";
                        ws.Cells[indexRunning, 2].Style.Font.Color.SetColor(RED);
                        ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                        ws.Cells[indexRunning, 3].Value = "";
                        ws.Cells[indexRunning, 4].Value = "";
                        indexRunning += 1;
                    }

                    //_stagement = "SUM(B" + (indexPortChargeStart + 1) + ":B" + (indexRunning - 1) + ")";
                    _stagement = "ROUND(SUM(B" + (indexPortChargeStart + 1) + ":B" + (indexRunning - 1) + "),4)";
                    int indexTotalPortCharge = indexRunning;
                    ws.Cells[indexRunning, 1].Value = "Total Port Charge";
                    ws.Cells[indexRunning, 1].Style.Font.Bold = true;
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.total_port_charge;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 2].Style.Font.Bold = true;
                    ws.Cells[indexRunning, 3].Value = "";
                    ws.Cells[indexRunning, 4].Value = "";
                    indexRunning += 1;

                    #region Set Style for Port Charge
                    //ws.Cells["B" + (indexPortChargeStart + 1) + ":B" + indexTotalPortCharge].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["B" + (indexPortChargeStart + 1) + ":B" + indexTotalPortCharge].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["B" + (indexPortChargeStart + 1) + ":B" + indexTotalPortCharge].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    #endregion

                    #endregion

                    #region SET Other costs (USD)
                    int indexOtherCostStart = indexRunning;

                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Merge = true;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Font.Bold = true;
                    //ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Font.Color.SetColor(BLUE);
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["A" + indexRunning + ":D" + indexRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    ws.Cells[indexRunning, 1].Value = "Total costs (USD)";
                    indexRunning += 1;

                    ws.Cells[indexRunning, 1].Value = "Fixed cost per day (USD/Day)";
                    ws.Cells[indexRunning, 2].Formula = EmtrytoZero(model.fcl_other_cost.fix_cost);
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 3].Value = "Others";
                    ws.Cells[indexRunning, 4].Value = EmtrytoZero(model.fcl_other_cost.others);
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    indexRunning += 1;

                    _stagement = "ROUND(B" + (indexRunning - 1) + ", 4)*ROUND(D" + (indexTimeForOprationEnd) + ", 4)";
                    int indexTotalFixCostPerDay = indexRunning;
                    ws.Cells[indexRunning, 1].Value = "Total Fixed cost";
                    ws.Cells[indexRunning, 1].Style.Font.Bold = true;
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_other_cost.total_fix_cost_per_day;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;//model.fcl_other_cost.total_fix_cost_per_day;
                    }
                    ws.Cells[indexRunning, 2].Style.Font.Bold = true;

                    ws.Cells[indexRunning, 3].Value = "Total Costs";
                    ws.Cells[indexRunning, 3].Style.Font.Bold = true;
                    _stagement = "ROUND(B" + indexTotalPortCharge + ",4)+ROUND(F" + indexTotalBunkerCost + ",4)+ROUND(B" + indexTotalFixCostPerDay + ",4)+ROUND(B" + (indexRunning + 1) + ",4)+ROUND(D" + (indexTotalFixCostPerDay - 1) + ",4)+ROUND(B" + (indexRunning + 2) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 4].Formula = model.fcl_other_cost.total_expenses;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 4].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 4].Style.Font.Bold = true;
                    indexRunning += 1;



                    //ws.Cells[indexRunning, 3].Value = "Suggested adding margin (%)";
                    //ws.Cells[indexRunning, 4].Formula = EmtrytoZero(model.fcl_other_cost.suggested_margin);
                    //ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    //indexRunning += 1;

                    int indexTotalCommessionDeduction = indexRunning;
                    ws.Cells[indexRunning, 1].Value = "Total commission deduction";
                    ws.Cells[indexRunning, 1].Style.Font.Bold = true;
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_other_cost.total_commession;
                    }
                    ws.Cells[indexRunning, 2].Style.Font.Bold = true;
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";

                    ws.Cells[indexRunning, 3].Value = "Suggested adding margin (%)";
                    ws.Cells[indexRunning, 4].Formula = EmtrytoZero(model.fcl_other_cost.suggested_margin);
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    indexRunning += 1;

                    //ws.Cells[indexRunning, 3].Value = "Suggested Freight after adding margin";
                    //ws.Cells[indexRunning, 4].Formula = "=IF(D" + (indexRunning - 1) + " = 0, 0, ROUND(B" + (indexRunning + 7) + "+((D" + (indexRunning - 1) + "*B" + (indexRunning + 7) + ")/ 100),4))";
                    //ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    //indexRunning += 1;

                    int indexWitholdingTax = indexRunning;
                    ws.Cells[indexRunning, 1].Value = "Withholding Tax";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_other_cost.withold_tax;
                    }

                    ws.Cells[indexRunning, 3].Value = "Suggested Freight after adding margin";
                    ws.Cells[indexRunning, 4].Formula = "=IF(D" + (indexRunning - 1) + " = 0, 0, ROUND(B" + (indexRunning + 6) + "+((D" + (indexRunning - 1) + "*B" + (indexRunning + 6) + ")/ 100),4))";
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";

                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 4].Formula = model.fcl_other_cost.suggested_margin_value;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 4].Formula = _stagement;
                    }
                    indexRunning += 1;

                    //ws.Cells[indexRunning, 3].Value = "Total Costs";
                    //ws.Cells[indexRunning, 3].Style.Font.Bold = true;
                    //_stagement = "ROUND(B" + indexTotalPortCharge + ",4)+ROUND(F" + indexTotalBunkerCost + ",4)+ROUND(B" + indexTotalFixCostPerDay + ",4)+ROUND(B" + indexTotalCommessionDeduction + ",4)+ROUND(D" + (indexTotalFixCostPerDay - 1) + ",4)+ROUND(B" + (indexTotalCommessionDeduction + 1) + ",4)";
                    //if (model.mode == "CREATE")
                    //{
                    //    ws.Cells[indexRunning, 4].Formula = model.fcl_other_cost.total_expenses;
                    //}
                    //else
                    //{
                    //    ws.Cells[indexRunning, 4].Formula = _stagement;
                    //}
                    //ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    //ws.Cells[indexRunning, 4].Style.Font.Bold = true;
                    //indexRunning += 1;

                    #region Set Style for Other costs (USD)
                    //ws.Cells["B" + (indexOtherCostStart + 1) + ":B" + indexWitholdingTax].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["B" + (indexOtherCostStart + 1) + ":B" + indexWitholdingTax].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["B" + (indexOtherCostStart + 1) + ":B" + indexWitholdingTax].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                    //ws.Cells["D" + (indexWitholdingTax - 2) + ":D" + indexWitholdingTax].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["D" + (indexWitholdingTax - 2) + ":D" + indexWitholdingTax].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["D" + (indexWitholdingTax - 2) + ":D" + indexWitholdingTax].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    #endregion

                    #endregion

                    #region SET Results,    USD,    BAHT,   USD/MT,   %
                    string unit_res = string.IsNullOrEmpty(model.fcl_payment_terms.unit) ? "USD/MT" : model.fcl_payment_terms.unit;
                    int indexResultstart = indexRunning;
                    ws.Cells["A" + indexRunning + ":E" + indexRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A" + indexRunning + ":E" + indexRunning].Style.Font.Bold = true;
                    ws.Cells["A" + indexRunning + ":E" + indexRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["A" + indexRunning + ":E" + indexRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    //ws.Cells["A" + indexRunning + ":E" + indexRunning].Style.Font.Color.SetColor(BLUE);
                    ws.Cells[indexRunning, 1].Value = "Results";
                    ws.Cells[indexRunning, 2].Value = "USD";
                    ws.Cells[indexRunning, 3].Value = "THB";
                    ws.Cells[indexRunning, 4].Value = unit_res;
                    ws.Cells[indexRunning, 5].Value = "%";
                    indexRunning += 1;

                    indexOfferfreight = indexRunning;
                    ws.Cells[indexRunning, 1].Value = "Offer freight";
                    ws.Cells["A" + indexRunning].Style.Font.Bold = true;
                    ws.Cells[indexRunning, 2].Formula = EmtrytoZero(model.fcl_results.offer_freight.usd);
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexOfferfreight + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.offer_freight.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    if (model.fcl_payment_terms.Cargoes.Count == 1)
                    {
                        _stagement = "ROUND(B" + indexOfferfreight + ",4)/ROUND(0+B11,4)";
                    }
                    else
                    {
                        _stagement = "0";
                    }

                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 4].Formula = model.fcl_results.offer_freight.mt;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 4].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    if (model.fcl_results.offer_freight.offer_by == "USD" && model.fcl_results.offer_freight.offer_unit == "LUMPSUM")
                    {
                        ws.Cells["B" + indexRunning].Style.Font.Bold = true;
                    }
                    else if (model.fcl_results.offer_freight.offer_by == "THB" && model.fcl_results.offer_freight.offer_unit == "LUMPSUM")
                    {
                        ws.Cells["C" + indexRunning].Style.Font.Bold = true;
                    }
                    else if (model.fcl_results.offer_freight.offer_unit == "UNIT PRICE")
                    {
                        ws.Cells["D" + indexRunning].Style.Font.Bold = true;
                    }
                    indexRunning += 1;


                    #region Update formular of Other cost
                    _stagement = "ROUND(D" + (indexTotalCommission - 1).ToString() + "/100, 4)*ROUND(B" + (indexOfferfreight) + ",4)";
                    if (model.mode == "INDEX")
                    {
                        ws.Cells[indexTotalCommessionDeduction, 2].Formula = _stagement;
                    }
                    ws.Cells[indexTotalCommessionDeduction, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "IF(D" + indexWitholdingTaxPercent + " = 0, 0, ((ROUND(D" + indexWitholdingTaxPercent + ",4)*ROUND(B" + (indexOfferfreight) + ",4))/100))";
                    if (model.mode == "INDEX")
                    {
                        ws.Cells[indexWitholdingTax, 2].Formula = _stagement;
                    }
                    ws.Cells[indexWitholdingTax, 2].Style.Numberformat.Format = "#,##0.0000";
                    #endregion

                    ws.Cells[indexRunning, 1].Value = "Total variable cost\n(Bunker + Port charge + Comm + Tax)";
                    ws.Cells[indexRunning, 1].Style.WrapText = true;
                    _stagement = "ROUND(B" + indexTotalPortCharge + ",4)+ROUND(F" + indexTotalBunkerCost + ",4)+ROUND(B" + indexTotalCommessionDeduction + ",4)+ROUND(B" + indexWitholdingTax + ",4)+ROUND(D" + (indexWitholdingTax - 3) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_results.total_variable_cost.usd;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexRunning + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.total_variable_cost.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 4].Formula = "";
                    indexRunning += 1;

                    ws.Cells["A" + indexRunning].Style.Font.Bold = true;
                    ws.Cells[indexRunning, 1].Value = "Profit over variable cost";
                    _stagement = "ROUND(B" + (indexRunning - 2) + ",4)-ROUND(B" + (indexRunning - 1) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_results.profit_over.usd;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexRunning + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.profit_over.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "(ROUND(B" + indexRunning + ",4)*100)/ROUND(B" + (indexRunning - 2) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 5].Formula = model.fcl_results.profit_over.percent;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 5].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    if (model.fcl_results.offer_freight.offer_by == "USD" && model.fcl_results.offer_freight.offer_unit == "LUMPSUM")
                    {
                        ws.Cells["B" + indexRunning].Style.Font.Bold = true;
                    }
                    else if (model.fcl_results.offer_freight.offer_by == "THB" && model.fcl_results.offer_freight.offer_unit == "LUMPSUM")
                    {
                        ws.Cells["C" + indexRunning].Style.Font.Bold = true;
                    }
                    indexRunning += 1;

                    ws.Cells[indexRunning, 1].Value = "Total fixed cost";
                    _stagement = "ROUND(B" + indexTotalFixCostPerDay + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_results.total_fix_cost.usd;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexRunning + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.total_fix_cost.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 4].Value = "";
                    indexRunning += 1;

                    ws.Cells[indexRunning, 1].Value = "Total costs (Variable + Fixed)";
                    _stagement = "ROUND(B" + (indexRunning - 3) + ",4)+ROUND(B" + (indexRunning - 1) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_results.total_cost.usd;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexRunning + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.total_cost.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 4].Value = "";
                    indexRunning += 1;

                    ws.Cells["A" + indexRunning].Style.Font.Bold = true;
                    ws.Cells[indexRunning, 1].Value = "NET Profit over total cost";
                    _stagement = "ROUND(B" + (indexRunning - 5) + ",4)-ROUND(B" + (indexRunning - 1) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 2].Formula = model.fcl_results.profit_over_total.usd;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 2].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexRunning + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.profit_over_total.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND((B" + (indexRunning) + "*100)/B" + (indexRunning - 5) + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 5].Formula = model.fcl_results.profit_over_total.percent;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 5].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 4].Style.Numberformat.Format = "#,##0.0000";
                    if (model.fcl_results.offer_freight.offer_by == "USD" && model.fcl_results.offer_freight.offer_unit == "LUMPSUM")
                    {
                        ws.Cells["B" + indexRunning].Style.Font.Bold = true;
                    }
                    else if (model.fcl_results.offer_freight.offer_by == "THB" && model.fcl_results.offer_freight.offer_unit == "LUMPSUM")
                    {
                        ws.Cells["C" + indexRunning].Style.Font.Bold = true;
                    }
                    indexRunning += 1;

                    ws.Cells[indexRunning, 1].Value = "Market freight reference";
                    ws.Cells[indexRunning, 2].Formula = EmtrytoZero(model.fcl_results.market_ref.usd);
                    ws.Cells[indexRunning, 2].Style.Numberformat.Format = "#,##0.0000";
                    _stagement = "ROUND(B" + indexRunning + ",4)*ROUND(F" + indexExchangeRate + ",4)";
                    if (model.mode == "CREATE")
                    {
                        ws.Cells[indexRunning, 3].Formula = model.fcl_results.market_ref.baht;
                    }
                    else
                    {
                        ws.Cells[indexRunning, 3].Formula = _stagement;
                    }
                    ws.Cells[indexRunning, 3].Style.Numberformat.Format = "#,##0.0000";
                    ws.Cells[indexRunning, 4].Value = "";

                    #region Set Style for Other costs (USD)
                    //ws.Cells["B" + (indexResultstart + 1) + ":E" + indexRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["B" + (indexResultstart + 1) + ":E" + indexRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    ws.Cells["B" + (indexResultstart + 1) + ":E" + indexRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

                    #endregion

                    //------------------------------------Add Remarks
                    indexBunkerRunning = indexRunning - 7;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexBunkerRunning].Merge = true;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //ws.Cells["F" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Font.Color.SetColor(BLUE);
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Font.Bold = true;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexBunkerRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(169, 169, 169));
                    ws.Cells[indexBunkerRunning, 6].Value = "Remarks";

                    indexBunkerRunning += 1;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexRunning].Merge = true;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexRunning].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells["F" + indexBunkerRunning + ":H" + indexRunning].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    //ws.Cells["F" + indexBunkerRunning + ":H" + indexRunning].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //ws.Cells["F" + indexBunkerRunning + ":H" + indexRunning].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(218, 238, 243));
                    //ws.Cells[indexBunkerRunning, 6].Value = model.fcl_remark;
                    HtmlRemoval _htmlRemove = new HtmlRemoval();
                    List<htmlTagList> list = _htmlRemove.SplitHTMLTag(string.IsNullOrEmpty(model.fcl_remark) ? "<p></p>" : model.fcl_remark);
                    //List<htmlTagList> list = _htmlRemove.RearrangeHtmlTagList(Lst);                    
                    string[] arr_remark = list[0].TextString.SplitWord("\r\n");
                    string rs_remark = "";
                    for (int count_remark = 0; count_remark < arr_remark.Length; count_remark++)
                    {
                        if (!string.IsNullOrEmpty(arr_remark[count_remark]))
                        {
                            rs_remark += arr_remark[count_remark] + "\n";
                        }
                    }
                    ws.Cells[indexBunkerRunning, 6].Value = rs_remark;
                    ws.Cells[indexBunkerRunning, 6].Style.WrapText = true;
                    //------------------------------------End Add Remarks
                    AllBorders(ws.Cells["A1:H" + indexRunning].Style.Border);

                    #region Old
                    if (model.fcl_bunker_cost.Count >= 3)
                    {
                        int i = 38;
                        for (int j = 2; j < model.fcl_bunker_cost.Count; j++)
                        {
                            ws.Cells["E" + i + ":H" + i].Style.Font.Bold = true;
                            ws.Cells["E" + i + ":H" + i].Style.Font.Color.SetColor(BLUE);
                            ws.Cells["E" + i + ":H" + i].Merge = true;
                            ws.Cells["E" + i + ":H" + i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            ws.Cells[i++, 5].Value = "Bunker Cost " + (j + 1) + " (USD)";
                            //ws.Cells[i++, 5].Value = "Bunker Cost (USD)";

                            ws.Cells["F" + i + ":H" + i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            ws.Cells["E" + i + ":H" + i].Style.Font.Bold = true;
                            ws.Cells["E" + i + ":H" + i].Style.Font.Color.SetColor(BLUE);
                            ws.Cells[i, 5].Value = "Bunk price";
                            ws.Cells[i, 6].Value = "FO";
                            ws.Cells[i, 7].Value = "MGO";
                            ws.Cells[i++, 8].Value = "";

                            ws.Cells[i, 5].Value = "Bunk price (USD/MT)";
                            ws.Cells[i, 6].Formula = model.fcl_bunker_cost[j].fcl_price.fo;
                            ws.Cells[i, 7].Formula = model.fcl_bunker_cost[j].fcl_price.mgo;
                            ws.Cells[i++, 8].Value = "";

                            ws.Cells["E" + i + ":H" + i].Style.Font.Bold = true;
                            ws.Cells["E" + i + ":H" + i].Style.Font.Color.SetColor(BLUE);
                            ws.Cells["E" + i + ":H" + i].Merge = true;
                            ws.Cells["E" + i + ":H" + i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            ws.Cells[i++, 5].Value = "Bunker Cost";

                            ws.Cells[i, 5].Value = "Bunker at loading";
                            ws.Cells[i, 6].Formula = model.fcl_bunker_cost[j].loading;
                            ws.Cells[i, 7].Value = "";
                            ws.Cells[i++, 8].Value = "";

                            ws.Cells[i, 5].Value = "Bunker at discharge port";
                            ws.Cells[i, 6].Formula = model.fcl_bunker_cost[j].discharge_port;
                            ws.Cells[i, 7].Value = "";
                            ws.Cells[i++, 8].Value = "";

                            ws.Cells[i, 5].Value = "Bunker at steaming ballast";
                            ws.Cells[i, 6].Formula = model.fcl_bunker_cost[j].steaming_ballast;
                            ws.Cells[i, 7].Value = "";
                            ws.Cells[i++, 8].Value = "";

                            ws.Cells[i, 5].Value = "Bunker at steaming laden";
                            ws.Cells[i, 6].Formula = model.fcl_bunker_cost[j].steaming_laden;
                            ws.Cells[i, 7].Value = "";
                            ws.Cells[i++, 8].Value = "";

                            ws.Cells[i, 5].Value = "Bunker at tank cleaning";
                            ws.Cells[i, 6].Formula = model.fcl_bunker_cost[j].tank_cleaning;
                            ws.Cells[i, 7].Value = "";
                            ws.Cells[i++, 8].Value = "";
                            AllBorders(ws.Cells["E" + (i - 8) + ":H" + i].Style.Border);
                        }
                    }
                    #endregion
                    //Add image
                    //string img_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web/FileUpload/FREIGHT/IMAGE", model.fcl_pic_path == null ? "" : model.fcl_pic_path);
                    //if (System.IO.File.Exists(img_path))
                    //{
                    //    AddImage(ws, 38, 0, img_path);
                    //}
                    #endregion
                }
                //save excel
                package.Save();
            }

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
            Response.AddHeader("content-type", "application/Excel");
            Response.WriteFile(file_path);
            Response.End();

            initializeDropDownList(ref model);
            return View("Index", model);
        }

        private void AllBorders(OfficeOpenXml.Style.Border _borders)
        {
            _borders.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        }

        private void AddImage(ExcelWorksheet oSheet, int rowIndex, int colIndex, string imagePath)
        {
            System.Drawing.Bitmap image = new System.Drawing.Bitmap(imagePath);
            if (image != null)
            {
                var excelImage = oSheet.Drawings.AddPicture("Attached_Image", image);
                excelImage.From.Column = colIndex;
                excelImage.From.Row = rowIndex;
                excelImage.SetSize(400, 300);
                // 2x2 px space for better alignment
                excelImage.From.ColumnOff = Pixel2MTU(2);
                excelImage.From.RowOff = Pixel2MTU(2);
            }
        }

        public int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, EstimateFreightCalculationViewModel model)
        {
            this.updateModel(form, model);
            if(model.fcl_results.offer_freight.offer_unit.ToUpper() == "LUMPSUM")
            {
                if (!String.IsNullOrEmpty(model.FreightOfferUSD) && !String.IsNullOrEmpty(model.FreightOfferTHB))
                {
                    var usd = Convert.ToDecimal(model.FreightOfferUSD);
                    var baht = Convert.ToDecimal(model.FreightOfferTHB);
                    model.fcl_results.offer_freight.usd = usd.ToString();
                    model.fcl_results.offer_freight.baht = baht.ToString();
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(model.FreightOfferUSDprice) && !String.IsNullOrEmpty(model.FreightOfferTHBprice))
                {
                    var usd = Convert.ToDecimal(model.FreightOfferUSDprice);
                    var baht = Convert.ToDecimal(model.FreightOfferTHBprice);
                    model.fcl_results.offer_freight.usd = usd.ToString();
                    model.fcl_results.offer_freight.baht = baht.ToString();
                }
            }
           
            HttpPostedFileBase upload = Request.Files.Count > 0 ? Request.Files[0] : null;
            var image = upload;
            string uploadImageName = "";
            if (ButtonListFreightCalculation != null)
            {
                var _lstButton = ButtonListFreightCalculation.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    if (upload != null)
                    {
                        if (upload.ContentLength > 0)
                        {
                            string type = ConstantPrm.SYSTEM.FREIGHT;
                            string FNID = ConstantPrm.FUNCTION.F10000008;
                            string typename = Path.GetExtension(image.FileName).ToLower();
                            uploadImageName = string.Format("{0}_{2}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, FNID);
                            model.fcl_pic_path = uploadImageName + typename;
                            var uploadPicRes = UploadFileImage(upload, "Web/FileUpload/FREIGHT/IMAGE", uploadImageName + typename);
                        }
                    }
                    string _xml = _lstButton[0].call_xml;
                    string note = _lstButton[0].name.ToUpper() == "REJECT" || _lstButton[0].name.ToUpper() == "CANCEL" ? form["hdfNoteAction"] : "-";
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"].ToString();
                    FclRootObject temp = new FclRootObject();
                    temp.fcl_bunker_consumption = model.fcl_bunker_consumption;
                    temp.fcl_bunker_cost = model.fcl_bunker_cost;
                    temp.fcl_excel_file_name = model.fcl_excel_file_name;
                    temp.fcl_exchange_rate = model.fcl_exchange_rate;
                    temp.fcl_exchange_date = model.fcl_exchange_date;
                    temp.fcl_latest_bunker_price = model.fcl_latest_bunker_price;
                    temp.fcl_other_cost = model.fcl_other_cost;
                    temp.fcl_payment_terms = model.fcl_payment_terms;
                    temp.fcl_pic_path = model.fcl_pic_path;
                    temp.total_bunker_cost = model.total_bunker_cost;
                    temp.total_port_charge = model.total_port_charge;
                    temp.fcl_remark = model.fcl_remark;
                    temp.fcl_results = model.fcl_results;
                    temp.fcl_time_for_operation = model.fcl_time_for_operation;
                    temp.fcl_vessel_particular = model.fcl_vessel_particular;
                    temp.fcl_vessel_speed = model.fcl_vessel_speed;
                    var json = new JavaScriptSerializer().Serialize(temp);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            //string purno = resData.resp_parameters[0].v.Encrypt();
                            string path = string.Format("~/CPAIMVC/EstimateFreightCalculation/Create?TranID={0}&Tran_Req_ID={1}", tranID, reqID);
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            initializeDropDownList(ref model);

            TempData["Freight_Model"] = model;
            return RedirectToAction("Create", "EstimateFreightCalculation");
        }

        private void initializeDropDownList(ref EstimateFreightCalculationViewModel model)
        {
            model.broker = getBroker();
            model.vehicle = getVehicle();
            model.portTypeLoad = getPortList("l", "", "", "", "");
            model.portTypeDisCharge = getPortList("d", "", "", "", "");

            List<SelectListItem> pName = new List<SelectListItem>();
            foreach (var i in model.portTypeLoad)
            {
                SelectListItem port = new SelectListItem();
                port.Text = i.Text;
                port.Value = i.Value;
                pName.Add(port);
            }

            model.portName = pName;
            model.customer = getCharterer();
            model.cargo = getCargo();
            //please initialize customer first before initializing cust payment terms
            model.custPaymentTerms = getCustPaymentTerms(model);
            //please initialize vehivle first before initializing type and built
            model.type = getType(model);
            model.built = getBuilt(model);
            model.bss = getBSS();
            model.portFullData = getPortAllData();
            if (model.fcl_payment_terms.ControlPort == null)
            {
                model.fcl_payment_terms.ControlPort = new List<Port_Control>();
            }
            if (model.fcl_payment_terms.ControlDischarge == null)
            {
                model.fcl_payment_terms.ControlDischarge = new List<Discharge_Control>();
            }
            model.unit = getUnit();
            model.unit_offer = getOfferUnit();
            model.by_offer = getOfferBy();
            model.unit_demurrage = getDemurrageUnit();
        }

        public List<MT_JETTY> getPortAllData()
        {
            return PortDAL.GetPortData();
        }

        [HttpGet]
        public string getPortListString(string fromType = "", string portType = "", string country = "", string location = "", string name = "")
        {
            return JSonConvertUtil.modelToJson(EstimateFreightServiceModel.getPortJetty(fromType, portType, country, location, name));
        }

        public List<ListDrowDownKey> getPortList(string fromType = "", string portType = "", string country = "", string location = "", string name = "")
        {
            return EstimateFreightServiceModel.getPortJetty(fromType, portType, country, location, name);
        }

        [HttpGet]
        public string getJettyPortById(string jetty)
        {
            return JSonConvertUtil.modelToJson(EstimateFreightServiceModel.getPortJettyById(jetty));
        }

        public Jetty_Port getJettyPortByIdServiceModel(string jetty)
        {
            return EstimateFreightServiceModel.getPortJettyById(jetty);
        }

        //[HttpGet]
        //public string getPortLocation(string country, string fromType)
        //{
        //    return JSonConvertUtil.modelToJson(DropdownServiceModel.getPortLocationFromJetty(country, fromType));
        //}

        //[HttpGet]
        //public string getPortCountry(string porttype, string fromType)
        //{
        //    return JSonConvertUtil.modelToJson(DropdownServiceModel.getPortCountryFromJetty(porttype, fromType));
        //}

        //public List<SelectListItem> getPortType(string fromType)
        //{
        //    return DropdownServiceModel.getPortTypeFromJetty(fromType);
        //}


        public List<SelectListItem> getVehicle()
        {
            return DropdownServiceModel.getVehicle("CHOVESMT");
        }

        public List<SelectListItem> getBroker()
        {
            return DropdownServiceModel.getVendorFreight("CHIBROMT");
        }

        private List<SelectListItem> getCharterer()
        {
            return DropdownServiceModel.getCustomer("CHR_FRE");
        }

        private List<SelectListItem> getCargo()
        {
            return DropdownServiceModel.getMaterial(false, "", "MAT_CMMT");
        }

        private List<SelectListItem> getUnit()
        {
            Setting setting = JSONSetting.getSetting("JSON_FREIGHT");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_unit.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_unit[i], Value = setting.fcl_unit[i] });
            }
            return list;
        }

        private List<SelectListItem> getOfferUnit()
        {
            Setting setting = JSONSetting.getSetting("JSON_FREIGHT");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_offer_unit.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_offer_unit[i], Value = setting.fcl_offer_unit[i] });
            }
            return list;
        }

        private List<SelectListItem> getDemurrageUnit()
        {
            Setting setting = JSONSetting.getSetting("JSON_FREIGHT");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_demurrage_unit.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_demurrage_unit[i], Value = setting.fcl_demurrage_unit[i] });
            }
            return list;
        }

        private List<SelectListItem> getOfferBy()
        {
            Setting setting = JSONSetting.getSetting("JSON_FREIGHT");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_offer_by.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_offer_by[i], Value = setting.fcl_offer_by[i] });
            }
            return list;
        }

        private List<SelectListItem> getCustPaymentTerms(EstimateFreightCalculationViewModel model)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //get cust data from the first key of customer.
            if (model.customer.Count > 0)
            {
                List<MT_CUST_DATA> mt_cust_data = CustDAL.GetCustPaymentTerms(model.customer[0].Value);
                for (int i = 0; i < mt_cust_data.Count; i++)
                {
                    selectList.Add(new SelectListItem { Text = mt_cust_data[i].MCS_TERMS_PAYMENT_KEY, Value = mt_cust_data[i].MCS_TERMS_PAYMENT_KEY });
                }
            }
            else
            {
                selectList.Add(new SelectListItem { Text = "xx", Value = "xx" });
            }
            return selectList;
        }

        private List<SelectListItem> getType(EstimateFreightCalculationViewModel model)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //get type from the first key of vehicle.
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetDetailById(PROJECT_NAME_SPACE, ACTIVE, CRUDE_TYPE, model.vehicle[0].Value);
            for (int i = 0; i < mt_vehicle.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = mt_vehicle[i].VEH_TYPE, Value = mt_vehicle[i].VEH_TYPE });
            }
            return selectList;
        }

        private List<SelectListItem> getBuilt(EstimateFreightCalculationViewModel model)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //get built from the first key of vehicle.
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetDetailById(PROJECT_NAME_SPACE, ACTIVE, CRUDE_TYPE, model.vehicle[0].Value);
            for (int i = 0; i < mt_vehicle.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = mt_vehicle[i].VEH_BUILT, Value = mt_vehicle[i].VEH_BUILT });
            }
            return selectList;
        }

        private List<SelectListItem> getBSS()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //get built from the first key of vehicle.
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    selectList.Add(new SelectListItem { Text = "" + i + ":" + j, Value = i + ":" + j });
                }
            }
            return selectList;
        }

        private List<SelectListItem> getStatus()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            string[] arr_status = { "DRAFT", "SUBMIT", "CANCEL" };
            for (int i = 0; i < arr_status.Length; i++)
            {
                list.Add(new SelectListItem { Text = arr_status[i], Value = arr_status[i] });
            }
            return list;
        }

        //Get Vessel Name by id
        [HttpPost]
        public JsonResult getVehicleById(string id)
        {
            MT_VEHICLE vehicle = VehicleDAL.GetVehicleById(id);
            return Json(new
            {
                TYPE = vehicle.VHE_PRODUCT_TYPE,
                CAPACITY = vehicle.VHE_CAPACITY,
                BUILT = vehicle.VEH_BUILT,
                MAX_DRAFT = vehicle.VHE_MAX_DRAFT,
                AGE = vehicle.VEH_AGE,
                COATING = vehicle.VHE_COATING,
                DATE = ShareFn.ConvertDateTimeToDateStringFormat(vehicle.VEH_CRE_DATE, "dd/MM/yyyy"),
                DWT = vehicle.VHE_DWT,
                DISPLACEMENT = vehicle.VHE_DISPLACEMENT,
                FULL_FLAG = (String.IsNullOrEmpty(vehicle.VHE_ECO_BALLAST) ? "Y" : ""),
                ECO_BALLAST = vehicle.VHE_ECO_BALLAST,
                ECO_LADEN = vehicle.VHE_ECO_LADEN,
                FULL_BALLAST = vehicle.VHE_FULL_BALLAST,
                FULL_LADEN = vehicle.VHE_FULL_LADEN,

                FO_LOAD = vehicle.VHE_FO_LOAD,
                FO_DISCHARGE = vehicle.VHE_FO_DISCHARGE,
                FO_HEATING = vehicle.VHE_FO_HEATING,
                FO_IDLE = vehicle.VHE_FO_IDLE,
                FO_TANK_CLEANING = vehicle.VHE_FO_TANK_CLEANING,
                FO_STEAMING_BALLAST = vehicle.VHE_FO_STEAMING_BALLAST,
                FO_STEAMING_LADEN = vehicle.VHE_FO_STEAMING_LADEN,

                MGO_LOAD = vehicle.VHE_MGO_LOAD,
                MGO_DISCHARGE = vehicle.VHE_MGO_DISCHARGE,
                MGO_HEATING = vehicle.VHE_MGO_HEATING,
                MGO_IDLE = vehicle.VHE_MGO_IDLE,
                MGO_TANK_CLEANING = vehicle.VHE_MGO_TANK_CLEANING,
                MGO_STEAMING_BALLAST = vehicle.VHE_MGO_STEAMING_BALLAST,
                MGO_STEAMING_LADEN = vehicle.VHE_MGO_STEAMING_LADEN
            });
        }

        [HttpPost]
        public JsonResult getBrokerCommission(string broker)
        {
            string tCommission = new EstimateFreightServiceModel().getBokerCommission(broker);
            return Json(new { COMMISSION = tCommission ?? "" });
        }

        [HttpPost]
        public JsonResult getLastExchangeRate(string date)
        {
            string[] arr_date = date.SplitWord("/");
            string tValue = new EstimateFreightServiceModel().getLastExchangeRate(arr_date[2] + arr_date[1] + arr_date[0]);
            return Json(new
            {
                EXCHANGE_RATE = tValue.SplitWord("|")[0] ?? "",
                EXCHANGE_DATE = tValue.SplitWord("|")[1] ?? ""
            });
        }

        [HttpPost]
        public JsonResult getPaymentTermDetailByCharterer(string broker)
        {
            EstimateFreightServiceModel ef = new EstimateFreightServiceModel();
            MT_CUST_PAYMENT_TERM payment = ef.GetPaymentByCharterer(broker);
            return Json(new
            {
                PAYMENT_TERM = payment.MCP_PAYMENT_TERM ?? "",
                ADDRESS_COMMISSION = payment.MCP_ADDRESS_COMMISSION ?? "",
                WITHHOLDING_TAX = payment.MCP_WITHOLDING_TAX ?? ""
            });
        }

        [HttpPost]
        public string getVenderCommission(string id)
        {
            return VendorDAL.GetVendorCommission(id);
        }

        //Get Payment term from Charterer by id
        [HttpPost]
        public JsonResult getCustPaymentTerms(string id)
        {
            List<MT_CUST_DATA> cust = CustDAL.GetCustPaymentTerms(id);
            List<string> cust_payment = new List<string>();
            for (int i = 0; i < cust.Count; i++)
            {
                cust_payment.Add(cust[i].MCS_TERMS_PAYMENT_KEY);
            }
            return Json(cust_payment);
        }

        [HttpPost]
        public string getPaymentTermByCharterer(string id)
        {
            return CustDAL.GetPaymentTerm(id);
        }

        [HttpPost]
        public string getBrokerByCharterer(string id)
        {
            return CustDAL.getBrokerByCharterer(id, "BRO_FRE");
        }

        [HttpPost]
        public string getExchangeRateByDate(string date)
        {
            df = new EstimateFreightServiceModel();
            return df.GetExchangeRate(date);
        }

        [HttpPost]
        public string getPortCharge(string portId, string vesselId)
        {
            df = new EstimateFreightServiceModel();
            return df.GetPortCharge(portId, vesselId);
        }

        [HttpPost]
        public JsonResult getBunkerPrice(string id, string dateStr)
        {
            df = new EstimateFreightServiceModel();
            DateTime? date = string.IsNullOrEmpty(dateStr) ? null : ShareFn.ConvertStringDateFormatToDatetime(dateStr);
            FclLatestBunkerPrice price = df.GetLastBunkerByDate(id, date);

            //FclLatestBunkerPrice price = df.GetLastBunker(id, date);
            return Json(new
            {
                FO_DATE = price.fcl_grade_fo.date ?? "",
                FO_PLACE = price.fcl_grade_fo.place ?? "",
                FO_PRICE = price.fcl_grade_fo.price ?? "",
                MGO_DATE = price.fcl_grade_mgo.date ?? "",
                MGO_PLACE = price.fcl_grade_mgo.place ?? "",
                MGO_PRICE = price.fcl_grade_mgo.price ?? "",
                FO_BUNKER_ID = price.fcl_grade_fo.bunker_id ?? "",
                FO_BUNKER_PURNO = price.fcl_grade_fo.bunker_purno ?? "",
                MGO_BUNKER_ID = price.fcl_grade_mgo.bunker_id ?? "",
                MGO_BUNKER_PURNO = price.fcl_grade_mgo.bunker_purno ?? ""
            });
        }

        [HttpPost]
        public JsonResult getBunkerConsumptionSpeed(string id, string type)
        {
            df = new EstimateFreightServiceModel();
            FclBunkerConsumption data = df.getBunkerConsumptionSpeed(id, type); ;
            return Json(new
            {
                FO_LOAD = data.fcl_load.fo ?? "",
                MGO_LOAD = data.fcl_load.mgo ?? "",
                FO_DISCHARGE = data.fcl_discharge.fo,
                MGO_DISCHARGE = data.fcl_discharge.mgo,
                FO_BALLAST = data.fcl_steaming_ballast.fo,
                MGO_BALLAST = data.fcl_steaming_ballast.mgo,
                FO_LADEN = data.fcl_steaming_laden.fo,
                MGO_LADEN = data.fcl_steaming_laden.mgo,
                FO_IDLE = data.fcl_idle.fo,
                MGO_IDLE = data.fcl_idle.mgo,
                FO_TANK_CLEANING = data.fcl_tack_cleaning.fo,
                MGO_TANK_CLEANING = data.fcl_tack_cleaning.mgo,
                FO_HEATING = data.fcl_heating.fo,
                MGO_HEATING = data.fcl_heating.mgo
            });
        }

        private ResponseData SaveDataFreightCalculation(DocumentActionStatus _status, EstimateFreightCalculationViewModel model, string note, string json_fileUpload)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }

            FclRootObject obj = new FclRootObject();
            obj.fcl_excel_file_name = model.fcl_excel_file_name;
            obj.fcl_vessel_particular = model.fcl_vessel_particular;
            obj.fcl_payment_terms = model.fcl_payment_terms;
            obj.fcl_time_for_operation = model.fcl_time_for_operation;
            obj.total_bunker_cost = model.total_bunker_cost;
            obj.total_port_charge = model.total_port_charge;
            obj.fcl_vessel_speed = model.fcl_vessel_speed;
            obj.fcl_latest_bunker_price = model.fcl_latest_bunker_price;
            obj.fcl_bunker_consumption = model.fcl_bunker_consumption;
            obj.fcl_pic_path = model.fcl_pic_path;
            obj.fcl_remark = model.fcl_remark;
            obj.fcl_bunker_cost = model.fcl_bunker_cost;
            obj.fcl_other_cost = model.fcl_other_cost;
            obj.fcl_results = model.fcl_results;
            obj.fcl_exchange_rate = model.fcl_exchange_rate;
            obj.fcl_exchange_date = model.fcl_exchange_date;
            var json = new JavaScriptSerializer().Serialize(obj);
            //_param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000008;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.FREIGHT });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.VESSEL });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private ResponseData LoadDataFreightCalculation(string TransactionID, ref EstimateFreightCalculationViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.FREIGHT });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.VESSEL });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null && _model.attachitems != "#attach_items")
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }
            List<ButtonAction> obj_btn = new List<ButtonAction>();
            if (_model.data_detail != null)
            {
                string[] arr_btn = { "SAVE DRAFT", "SUBMIT", "CANCEL", "REJECT" };
                for (int i = 0; i < arr_btn.Length; i++)
                {
                    ButtonAction btn = _model.buttonDetail.Button.Where(m => m.name == arr_btn[i]).FirstOrDefault();
                    if (btn != null)
                    {
                        obj_btn.Add(btn);
                    }
                }
                _model.buttonDetail.Button = obj_btn;

                model = new JavaScriptSerializer().Deserialize<EstimateFreightCalculationViewModel>(_model.data_detail);
                if (model.fcl_payment_terms != null)
                {
                    if (!string.IsNullOrEmpty(model.fcl_payment_terms.laycan_from) && !string.IsNullOrEmpty(model.fcl_payment_terms.laycan_to))
                    {
                        model.laycan = model.fcl_payment_terms.laycan_from + " to " + model.fcl_payment_terms.laycan_to;
                    }
                }

                if (model.fcl_payment_terms != null)
                {
                    if (model.fcl_payment_terms.ControlPort != null && model.fcl_payment_terms.ControlPort.Count() > 0)
                    {
                        foreach (var i in model.fcl_payment_terms.ControlPort)
                        {
                            if (!string.IsNullOrEmpty(i.PortName) && !string.IsNullOrEmpty(i.PortLocation) &&
                                !string.IsNullOrEmpty(i.PortCountry))
                            {
                                if (i.PortName.ToUpper() == "OTHER")
                                {
                                    if (!string.IsNullOrEmpty(i.PortOtherDesc))
                                    {
                                        i.PortFullName = i.PortName + ", " + i.PortLocation + ", " + i.PortCountry;
                                    }
                                    else
                                    {
                                        i.PortFullName = i.PortName + "(" + i.PortOtherDesc + "), " + i.PortLocation + ", " + i.PortCountry;
                                    }
                                }
                                else
                                {
                                    i.PortFullName = i.PortName + ", " + i.PortLocation + ", " + i.PortCountry;
                                }
                            }
                        }
                        model.fcl_payment_terms.json_port_control = new JavaScriptSerializer().Serialize(model.fcl_payment_terms.ControlPort);
                    }
                    if (model.fcl_payment_terms.ControlDischarge != null && model.fcl_payment_terms.ControlDischarge.Count() > 0)
                    {
                        foreach (var i in model.fcl_payment_terms.ControlDischarge)
                        {
                            if (!string.IsNullOrEmpty(i.DischargeName) && !string.IsNullOrEmpty(i.PortLocation) &&
                                !string.IsNullOrEmpty(i.PortCountry))
                            {
                                if (i.DischargeName.ToUpper() == "OTHER")
                                {
                                    if (string.IsNullOrEmpty(i.PortOtherDesc))
                                    {
                                        i.DischargeFullName = i.DischargeName + ", " + i.PortLocation + ", " + i.PortCountry;
                                    }
                                    else
                                    {
                                        i.DischargeFullName = i.DischargeName + "(" + i.PortOtherDesc + "), " + i.PortLocation + ", " + i.PortCountry;
                                    }
                                }
                                else
                                {
                                    i.DischargeFullName = i.DischargeName + ", " + i.PortLocation + ", " + i.PortCountry;
                                }
                            }
                        }
                        model.fcl_payment_terms.json_discharge_control = new JavaScriptSerializer().Serialize(model.fcl_payment_terms.ControlDischarge);
                    }
                }
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = returnPage;
            }
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                model.pageMode = "EDIT";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.pageMode = "EDIT";
                }
            }
            string _btn = "<input type='submit' id='btnPrint' value='PRINT PDF' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            model.buttonCode += _btn;
            _btn = "<input type='submit' id='btnPrintExcel' value='PRINT EXCEL' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenExcel' onclick=\"printExcel();\" />";
            model.buttonCode += _btn;
            
            if (obj_btn.Count > 0)
            {
                if (model.pageMode != "EDIT")
                {
                    if (EstimateFreightServiceModel.isUseFreightCharterOut(TransactionID))
                    {
                        _btn = "<input type='button' id='btn_to_chot' value='CHARTERING OUT' class='" + _FN.GetDefaultButtonCSS("REQUEST") + "' name='btn_to_chot' onclick=\"onSetToChot('" + TransactionID.Encrypt() + "');\" />";
                        model.buttonCode += _btn;
                    }                    
                }                
            }


            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.pur_no = resData.resp_parameters[0].v;
            }

            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref EstimateFreightCalculationViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                foreach (ButtonAction _button in Button)
                {
                    string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(this.className, \"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                    model.buttonCode += _btn;
                }
                model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListFreightCalculation = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }

        private void updateModel(FormCollection form, EstimateFreightCalculationViewModel model, bool isTextMode = false)
        {
            model.fcl_vessel_particular.purchase_no = form["pur_no"];

            if (!string.IsNullOrEmpty(model.laycan))
            {
                string[] pt_laycan = model.laycan.Split(' ');
                if (pt_laycan.Count() >= 3)
                {
                    model.fcl_payment_terms.laycan_from = pt_laycan[0];
                    model.fcl_payment_terms.laycan_to = pt_laycan[2];
                }
            }

            initializeDropDownList(ref model);

            if (isTextMode)
            {
                model.fcl_vessel_particular.vessel_id = model.fcl_vessel_particular.vessel_name;
                if (!string.IsNullOrEmpty(model.fcl_vessel_particular.vessel_name))
                    model.fcl_vessel_particular.vessel_name = model.vehicle.Where(x => x.Value == model.fcl_vessel_particular.vessel_name).FirstOrDefault().Text;
                if (!string.IsNullOrEmpty(model.fcl_payment_terms.charterer))
                    model.fcl_payment_terms.charterer = model.customer.Where(x => x.Value == model.fcl_payment_terms.charterer).FirstOrDefault().Text;
                if (!string.IsNullOrEmpty(model.fcl_payment_terms.broker))
                    model.fcl_payment_terms.broker = model.broker.Where(x => x.Value == model.fcl_payment_terms.broker).FirstOrDefault().Text;
                //if (!string.IsNullOrEmpty(model.fcl_payment_terms.cargo))
                //    model.fcl_payment_terms.cargo = model.cargo.Where(x => x.Value == model.fcl_payment_terms.cargo).FirstOrDefault().Text;
            }

            if (model.fcl_payment_terms.Cargoes != null && model.fcl_payment_terms.Cargoes.Count > 0)
            {
                for (int i = 0; i < model.fcl_payment_terms.Cargoes.Count; i++)
                {
                    model.fcl_payment_terms.Cargoes[i].order = (i + 1).ToString();
                }
            }

            if (model.fcl_payment_terms.ControlPort != null && model.fcl_payment_terms.ControlPort.Count > 0)
            {
                List<Port_Control> portList = new List<Port_Control>();
                for (int i = 0; i < model.fcl_payment_terms.ControlPort.Count; i++)
                {
                    //var port = model.fcl_payment_terms.ControlPort[i].PortName.Split(',');
                    if (model.fcl_payment_terms.ControlPort[i].JettyPortKey != "")
                    {
                        var temp = model.portFullData.Where(p => p.MTJ_ROW_ID.Trim().ToUpper() == model.fcl_payment_terms.ControlPort[i].JettyPortKey).ToList();
                        if (temp != null && temp.Count() > 0)
                        {
                            EstimateFreightServiceModel sv = new EstimateFreightServiceModel();
                            var portCharge = model.fcl_payment_terms.ControlPort[i].PortValue ?? "";
                            if (portCharge == "") { portCharge = sv.GetPortCharge(temp[0].MTJ_ROW_ID); }
                            var otherDesc = model.fcl_payment_terms.ControlPort[i].PortOtherDesc ?? "";
                            Port_Control tempLoad = new Port_Control();
                            tempLoad.JettyPortKey = temp[0].MTJ_ROW_ID;
                            tempLoad.PortType = temp[0].MTJ_CREATE_TYPE;
                            tempLoad.PortCountry = temp[0].MTJ_MT_COUNTRY;
                            tempLoad.PortLocation = temp[0].MTJ_LOCATION;
                            tempLoad.PortName = temp[0].MTJ_JETTY_NAME;
                            tempLoad.PortFullName = temp[0].MTJ_JETTY_NAME + ", " + temp[0].MTJ_LOCATION + ", " + temp[0].MTJ_MT_COUNTRY;
                            tempLoad.PortValue = portCharge;
                            tempLoad.PortOrder = (i + 1).ToString();
                            tempLoad.PortOtherDesc = otherDesc;
                            portList.Add(tempLoad);
                        }
                    }
                    else if (!string.IsNullOrEmpty(model.fcl_payment_terms.ControlPort[i].PortName) && !string.IsNullOrEmpty(model.fcl_payment_terms.ControlPort[i].PortLocation) && !string.IsNullOrEmpty(model.fcl_payment_terms.ControlPort[i].PortCountry))
                    {
                        var fullData = model.portFullData.Where(p => p.MTJ_JETTY_NAME.Trim().ToUpper() == model.fcl_payment_terms.ControlPort[i].PortName.Trim().ToUpper()
                        && p.MTJ_LOCATION.Trim().ToUpper() == model.fcl_payment_terms.ControlPort[i].PortLocation.Trim().ToUpper() && p.MTJ_MT_COUNTRY.Trim().ToUpper() == model.fcl_payment_terms.ControlPort[i].PortCountry.Trim().ToUpper()).ToList();
                        if (fullData != null && fullData.Count() > 0)
                        {
                            EstimateFreightServiceModel sv = new EstimateFreightServiceModel();
                            var portCharge = model.fcl_payment_terms.ControlPort[i].PortValue ?? "";
                            var otherDesc = model.fcl_payment_terms.ControlPort[i].PortOtherDesc ?? "";
                            Port_Control tempLoad = new Port_Control();
                            tempLoad.JettyPortKey = fullData[0].MTJ_ROW_ID;
                            tempLoad.PortType = fullData[0].MTJ_CREATE_TYPE;
                            tempLoad.PortCountry = model.fcl_payment_terms.ControlPort[i].PortCountry;
                            tempLoad.PortLocation = fullData[0].MTJ_LOCATION;
                            tempLoad.PortName = fullData[0].MTJ_JETTY_NAME;
                            tempLoad.PortFullName = fullData[0].MTJ_JETTY_NAME + ", " + fullData[0].MTJ_LOCATION + ", " + model.fcl_payment_terms.ControlPort[i].PortCountry;
                            tempLoad.PortValue = portCharge;
                            tempLoad.PortOrder = (i + 1).ToString();
                            tempLoad.PortOtherDesc = otherDesc;
                            portList.Add(tempLoad);
                        }
                    }
                }
                if (portList.Count() > 0)
                {
                    model.fcl_payment_terms.ControlPort = portList;
                }
            }
            else
            {
                model.fcl_payment_terms.ControlPort = new List<Port_Control>();
            }

            if (model.fcl_payment_terms.ControlDischarge != null && model.fcl_payment_terms.ControlDischarge.Count > 0)
            {
                List<Discharge_Control> portList = new List<Discharge_Control>();
                for (int i = 0; i < model.fcl_payment_terms.ControlDischarge.Count; i++)
                {
                    //var port = model.fcl_payment_terms.ControlDischarge[i].DischargeName.Split(',');
                    if (model.fcl_payment_terms.ControlDischarge[i].JettyPortKey != "")
                    {
                        var temp = model.portFullData.Where(p => p.MTJ_ROW_ID.Trim().ToUpper() == model.fcl_payment_terms.ControlDischarge[i].JettyPortKey).ToList();
                        if (temp != null && temp.Count() > 0)
                        {
                            EstimateFreightServiceModel sv = new EstimateFreightServiceModel();
                            var portCharge = model.fcl_payment_terms.ControlDischarge[i].DischargeValue ?? "";
                            if (portCharge == "") { portCharge = sv.GetPortCharge(temp[0].MTJ_ROW_ID); }
                            var otherDesc = model.fcl_payment_terms.ControlDischarge[i].PortOtherDesc ?? "";
                            Discharge_Control tempLoad = new Discharge_Control();
                            tempLoad.JettyPortKey = temp[0].MTJ_ROW_ID;
                            tempLoad.PortType = temp[0].MTJ_CREATE_TYPE;
                            tempLoad.PortCountry = temp[0].MTJ_MT_COUNTRY;
                            tempLoad.PortLocation = temp[0].MTJ_LOCATION;
                            tempLoad.DischargeName = temp[0].MTJ_JETTY_NAME;
                            tempLoad.DischargeFullName = temp[0].MTJ_JETTY_NAME + ", " + temp[0].MTJ_LOCATION + ", " + temp[0].MTJ_MT_COUNTRY;
                            tempLoad.DischargeValue = portCharge;
                            tempLoad.PortOrder = (i + 1).ToString();
                            tempLoad.PortOtherDesc = otherDesc;
                            portList.Add(tempLoad);
                        }
                    }
                    else if (!string.IsNullOrEmpty(model.fcl_payment_terms.ControlDischarge[i].DischargeName) && !string.IsNullOrEmpty(model.fcl_payment_terms.ControlDischarge[i].PortLocation) && !string.IsNullOrEmpty(model.fcl_payment_terms.ControlDischarge[i].PortCountry))
                    {
                        var fullData = model.portFullData.Where(p => p.MTJ_JETTY_NAME.Trim().ToUpper() == model.fcl_payment_terms.ControlDischarge[i].DischargeName.Trim().ToUpper()
                        && p.MTJ_LOCATION.Trim().ToUpper() == model.fcl_payment_terms.ControlDischarge[i].PortLocation.Trim().ToUpper() && p.MTJ_MT_COUNTRY.Trim().ToUpper() == model.fcl_payment_terms.ControlDischarge[i].PortCountry.Trim().ToUpper()).ToList();
                        if (fullData != null && fullData.Count() > 0)
                        {
                            EstimateFreightServiceModel sv = new EstimateFreightServiceModel();
                            var portCharge = model.fcl_payment_terms.ControlDischarge[i].DischargeValue ?? "";
                            var otherDesc = model.fcl_payment_terms.ControlDischarge[i].PortOtherDesc ?? "";
                            Discharge_Control tempLoad = new Discharge_Control();
                            tempLoad.JettyPortKey = fullData[0].MTJ_ROW_ID;
                            tempLoad.PortType = fullData[0].MTJ_CREATE_TYPE;
                            tempLoad.PortCountry = model.fcl_payment_terms.ControlDischarge[i].PortCountry;
                            tempLoad.PortLocation = fullData[0].MTJ_LOCATION;
                            tempLoad.DischargeName = fullData[0].MTJ_JETTY_NAME;
                            tempLoad.DischargeFullName = fullData[0].MTJ_JETTY_NAME + ", " + fullData[0].MTJ_LOCATION + ", " + model.fcl_payment_terms.ControlDischarge[i].PortCountry;
                            tempLoad.DischargeValue = model.fcl_payment_terms.ControlDischarge[i].DischargeValue;
                            tempLoad.PortOrder = (i + 1).ToString();
                            tempLoad.PortOtherDesc = otherDesc;
                            portList.Add(tempLoad);
                        }
                    }
                }
                if (portList.Count() > 0)
                {
                    model.fcl_payment_terms.ControlDischarge = portList;
                }
            }
            else
            {
                model.fcl_payment_terms.ControlDischarge = new List<Discharge_Control>();
            }

            //if (model.fcl_results.offer_freight != null)
            //{
            //    string tFreightBy = model.fcl_results.offer_freight.offer_by;
            //    string tFrehghtUnit = model.fcl_results.offer_freight.offer_unit;
            //    model.fcl_results.offer_freight.offer_by = tFreightBy + "|" + tFrehghtUnit;
            //}      
        }

        public ActionResult Search()
        {
            EstimateFreightCalculationViewModel Model = new EstimateFreightCalculationViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.FREIGHT);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.FREIGHT);

            Model.createByUser = findUserByGroup;

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            ViewBag.getVessel = getVehicle();
            ViewBag.getBroker = getBroker();
            ViewBag.getUsers = userList;
            ViewBag.getChaterer = getCharterer();
            ViewBag.getCargo = getCargo();
            ViewBag.getStatus = getStatus();
            //ViewBag.getPort = DropdownServiceModel.getPortName();
            ViewBag.getPortLoad = DropdownServiceModel.getPortJettyList("l");
            ViewBag.getPortDischarge = DropdownServiceModel.getPortJettyList("d");

            //this.updateModel(null, Model);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SearchResult(FormCollection fm)
        {
            EstimateFreightCalculationViewModel Model = new EstimateFreightCalculationViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.FREIGHT);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);

            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.FREIGHT);

            Model.createByUser = findUserByGroup;

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }

            ViewBag.getVessel = getVehicle();
            ViewBag.getBroker = getBroker();
            ViewBag.getUsers = userList.Distinct().ToList();
            ViewBag.getChaterer = getCharterer();
            ViewBag.getCargo = getCargo();
            ViewBag.getStatus = getStatus();
            //ViewBag.getPort = DropdownServiceModel.getPortName();
            ViewBag.getPortLoad = DropdownServiceModel.getPortJettyList("l");
            ViewBag.getPortDischarge = DropdownServiceModel.getPortJettyList("d");

            string datePurchase = fm["search_date"].ToString();
            string stDate = "";
            string enDate = "";
            string vessel = fm["search_vessel"].ToString();
            string charterer = fm["search_customer"].ToString();
            string cargo = fm["search_cargo"].ToString();
            //string broker = fm["broker"].ToString();
            //string load_port = fm["loadPort"].ToString();
            //string discharge_port = fm["dischargePort"].ToString();
            string load_port = "";/*fm["loadPort"].ToString();*/
            string discharge_port = "";/*fm["dischargePort"].ToString();*/
            string laytime = fm["search_laytime"].ToString();

            string laycan = fm["search_laycan"].ToString();
            string laycan_start = "";
            string laycan_end = "";
            string status = fm["search_status"].ToString();

            string userlist = "";
            if (fm["userList"] != null)
            {
                userlist = fm["userList"].ToString().Replace(",", "|");
            }

            if (!datePurchase.Equals(""))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                enDate = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            if (!laycan.Equals(""))
            {
                string[] s = laycan.Split(new[] { " to " }, StringSplitOptions.None);
                laycan_start = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                laycan_end = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            List_Freighttrx res = SearchFreightInData(stDate, enDate, vessel, charterer, cargo, "", load_port, discharge_port, laytime, laycan_start, laycan_end, status);
            Model.FreightTransaction = res.FreightTransaction;

            int i = 0;
            foreach (var s in res.FreightTransaction)
            {
                Model.FreightTransaction[i].Transaction_id_Encrypted = s.transaction_id.Encrypt();
                Model.FreightTransaction[i].Req_transaction_id_Encrypted = s.req_transaction_id.Encrypt();
                Model.FreightTransaction[i].Purchase_no_Encrypted = s.purchase_no.Encrypt();
                Model.FreightTransaction[i].reason_Encrypted = s.reason.Encrypt();

                i++;
            }

            Model.search_date = datePurchase;
            Model.search_laycan = laycan;
            Model.search_laytime = laytime;

            Model.search_vessel = vessel;
            Model.search_customer = charterer;
            Model.search_cargo = cargo;
            Model.search_status = status;

            //this.updateModel(fm, Model);
            return View("Search", Model);
        }

        private List_Freighttrx SearchFreightInData(string sDate, string eDate, string vessel, string charterer, string cargo, string broker, string load_port, string discharge_port, string laytime, string laycan_start, string laycan_end, string stauts)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000004;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            //req.Req_parameters.P.Add(new P { K = "user", V = "ORNRAWEE" });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.FREIGHT });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = stauts });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "function_code", V = FREIGHT_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_5", V = vessel });
            req.Req_parameters.P.Add(new P { K = "index_10", V = charterer });
            req.Req_parameters.P.Add(new P { K = "index_11", V = cargo });
            req.Req_parameters.P.Add(new P { K = "index_12", V = broker });
            req.Req_parameters.P.Add(new P { K = "index_13", V = load_port });
            req.Req_parameters.P.Add(new P { K = "index_14", V = discharge_port });
            req.Req_parameters.P.Add(new P { K = "index_15", V = laytime });
            req.Req_parameters.P.Add(new P { K = "index_19", V = laycan_start });
            req.Req_parameters.P.Add(new P { K = "index_20", V = laycan_end });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_Freighttrx _model = ShareFunction.DeserializeXMLFileToObject<List_Freighttrx>(_DataJson);
            return _model;
        }
        
        public ActionResult Create(FormCollection frm, EstimateFreightCalculationViewModel model)
        {
            if (TempData["CharterIn_Model"] == null)
            {
                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL

                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataFreightCalculation(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }

                    if (resData != null && !(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = returnPage;
                    }
                }

                if (model.portFullData == null)
                {
                    model.portFullData = getPortAllData();
                }

                if (Request.HttpMethod == "POST")
                {
                    ResponseData resData = new ResponseData();
                    this.updateModel(frm, model);

                    HttpPostedFileBase upload = Request.Files[0];
                    var image = upload;
                    string uploadImageName = "";

                    if (model.fcl_vessel_particular.date_time == "" || model.fcl_vessel_particular.date_time == null)
                    {
                        DateTime localDate = DateTime.UtcNow.Date;
                        model.fcl_vessel_particular.date_time = localDate.ToString("dd/MM/yyyy");
                    }

                    if (upload != null)
                    {
                        if (upload.ContentLength > 0)
                        {
                            string type = ConstantPrm.SYSTEM.FREIGHT;
                            string FNID = ConstantPrm.FUNCTION.F10000008;
                            string typename = Path.GetExtension(image.FileName).ToLower();
                            uploadImageName = string.Format("{0}_{2}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, FNID);
                            model.fcl_pic_path = uploadImageName + typename;
                            var uploadPicRes = UploadFileImage(upload, "Web/FileUpload/FREIGHT/IMAGE", uploadImageName + typename);
                        }
                    }

                    if (frm["createFromExellBtn"] != null && frm["createFromExellBtn"].ToString().Equals("Y"))
                    {
                        #region Upload Excel
                        try
                        {
                            if (Request.Files.Count > 0)
                            {

                                var fileName = Request.Files[0];
                                if (upload.ContentLength > 0)
                                {
                                    var package = new ExcelPackage(upload.InputStream);

                                    ExcelWorksheet ws = package.Workbook.Worksheets.First();
                                    DataTable dt = new DataTable();

                                    for (int i = 0; i < ws.Dimension.End.Column; i++)
                                    {
                                        //dummy column
                                        dt.Columns.Add("");
                                    }

                                    for (int rowNum = 2; rowNum <= ws.Dimension.End.Row; rowNum++)
                                    {
                                        var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                        DataRow row = dt.Rows.Add();
                                        foreach (var cell in wsRow)
                                        {
                                            if (!string.IsNullOrEmpty(cell.Formula))
                                            {
                                                cell.Formula = cell.Formula.Substring(0, 1).Equals("=") ? ShareFn.ReplaceFirst(cell.Formula, "=", "") : cell.Formula;
                                                cell.Calculate();
                                            }
                                            if (cell.Value == null)
                                                cell.Value = "";
                                            cell.Value = cell.Value.ToString() == ExcelErrorValue.Values.Value || cell.Value.ToString() == ExcelErrorValue.Values.Name ? "" : cell.Value;
                                            row[cell.Start.Column - 1] = cell.Value;
                                        }
                                    }
                                    model.fcl_excel_file_name = upload.FileName;
                                    model = MapFieldtoModel(dt);
                                    model.mode = "INDEX";
                                }
                                else
                                {
                                    ModelState.AddModelError("File", "Please Upload Your file");
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                            //throw;
                        }
                        #endregion
                    }

                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            TempData["returnPage"] = returnPage;
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        }
                    }
                }
            }
            else
            {
                model = TempData["Freight_Model"] as EstimateFreightCalculationViewModel;
                TempData["Freight_Model"] = null;
            }

            initializeDropDownList(ref model);
            return View(model);
        }

        public bool UploadFileImage(HttpPostedFileBase requestFile, string uploadPath, string uploadImageName)
        {
            if (Request != null)
            {
                var file = requestFile; //Request.Files["UploadedFile"];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string rootPath = Request.PhysicalApplicationPath;
                    string subPath = uploadPath;

                    fileName = uploadImageName;
                    if (!Directory.Exists(rootPath + subPath))
                        Directory.CreateDirectory(rootPath + subPath);
                    var path = Path.Combine(Server.MapPath("~/" + uploadPath), fileName);
                    file.SaveAs(path);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }       

        public bool UploadFile(HttpPostedFileBase requestFile)
        {
            if (Request != null)
            {
                var file = requestFile; //Request.Files["UploadedFile"];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string type = "FREIGHT";
                    string FNID = "F10000008";
                    string rootPath = Request.PhysicalApplicationPath;
                    string subPath = "Web/FileUpload/FREIGHT";

                    fileName = string.Format("{0}_{2}_{1}_{3}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, FNID, Path.GetFileName(file.FileName));
                    if (!Directory.Exists(rootPath + subPath))
                        Directory.CreateDirectory(rootPath + subPath);
                    var path = Path.Combine(Server.MapPath("~/web/fileupload/freight"), fileName);
                    file.SaveAs(path);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private EstimateFreightCalculationViewModel MapFieldtoModel(DataTable dt)
        {
            EstimateFreightCalculationViewModel modelF = new EstimateFreightCalculationViewModel();
            try
            {
                if (dt.Rows.Count > 0)
                {

                    #region Vessel Particle
                    //modelF.fcl_vessel_particular.date_time = string.IsNullOrEmpty(dt.Rows[1][1].ToString()) ? "" : Convert.ToDateTime(dt.Rows[1][1]).ToString("dd/MM/yyyy");
                    modelF.fcl_vessel_particular.date_time = dt.Rows[1][1].ToString();
                    modelF.fcl_vessel_particular.dwt = dt.Rows[1][3].ToString();
                    modelF.fcl_vessel_particular.displacement = dt.Rows[2][3].ToString();
                    string valDDLID = dt.Rows[2][1].ToString();
                    if (valDDLID.Trim() != "")
                    {
                        var valData = getVehicle();
                        valData = valData.Where(p => p.Text == valDDLID).ToList();
                        if (valData != null)
                        {
                            foreach (var i in valData)
                            {
                                valDDLID = i.Value;
                            }
                        }
                    }
                    modelF.fcl_vessel_particular.vessel_name = valDDLID;
                    modelF.fcl_vessel_particular.capacity = dt.Rows[3][3].ToString();
                    modelF.fcl_vessel_particular.type = dt.Rows[3][1].ToString();
                    modelF.fcl_vessel_particular.max_draft = dt.Rows[4][3].ToString();
                    //modelF.fcl_vessel_particular.built = string.IsNullOrEmpty(dt.Rows[4][1].ToString()) ? "" : Convert.ToDateTime(dt.Rows[4][1]).ToString("dd/MM/yyyy");
                    modelF.fcl_vessel_particular.built = dt.Rows[4][1].ToString();
                    modelF.fcl_vessel_particular.coating = dt.Rows[5][3].ToString();
                    modelF.fcl_vessel_particular.age = dt.Rows[5][1].ToString();
                    #endregion

                    #region Payment Terms
                    valDDLID = dt.Rows[7][1].ToString();
                    if (valDDLID.Trim() != "")
                    {
                        var valData = getCharterer();
                        valData = valData.Where(p => p.Text == valDDLID).ToList();
                        if (valData != null)
                        {
                            foreach (var i in valData)
                            {
                                valDDLID = i.Value;
                            }
                        }
                    }
                    modelF.fcl_payment_terms.charterer = valDDLID;
                    //modelF.fcl_payment_terms.dem = dt.Rows[8][3].ToString();

                    valDDLID = dt.Rows[8][1].ToString();
                    if (valDDLID.Trim() != "")
                    {
                        var valData = getCargo();
                        valData = valData.Where(p => p.Text == valDDLID).ToList();
                        if (valData != null)
                        {
                            foreach (var i in valData)
                            {
                                valDDLID = i.Value;
                            }
                        }
                    }

                    modelF.fcl_payment_terms.Cargoes = new List<FclCargo>();
                    int n_cargo = 0;
                    bool end_cargo = true;
                    while (end_cargo)
                    {
                        //string tt = dt.Rows[8 + n_cargo][0].ToString().ToUpper();
                        if (dt.Rows[8 + n_cargo][0].ToString().ToUpper().Contains("CARGO") && dt.Rows[8 + (n_cargo + 1)][0].ToString().ToUpper().Contains("QUANTITY"))
                        {
                            valDDLID = dt.Rows[8 + n_cargo][1].ToString();
                            if (valDDLID.Trim() != "")
                            {
                                var valData = getCargo();
                                valData = valData.Where(p => p.Text == valDDLID).ToList();
                                if (valData != null)
                                {
                                    foreach (var i in valData)
                                    {
                                        valDDLID = i.Value;
                                    }
                                }
                            }
                            string tUnit = dt.Rows[8 + (n_cargo + 1)][0].ToString().SplitWord("(")[1].SplitWord(")")[0];
                            string unit_other = "";
                            if (tUnit.Trim() != "")
                            {
                                var valData = getUnit();
                                valData = valData.Where(p => p.Text == tUnit).ToList();
                                if (valData != null)
                                {
                                    if (valData.Count() > 0)
                                    {
                                        foreach (var v in valData)
                                        {
                                            tUnit = v.Value;
                                        }
                                    }
                                    else
                                    {
                                        tUnit = "OTHER";
                                        unit_other = dt.Rows[8 + (n_cargo + 1)][0].ToString().SplitWord("(")[1].SplitWord(")")[0];
                                    }
                                }
                                else
                                {
                                    tUnit = "OTHER";
                                    unit_other = dt.Rows[8 + (n_cargo + 1)][0].ToString().SplitWord("(")[1].SplitWord(")")[0];
                                }
                            }
                            string tTolerance = dt.Rows[8 + (n_cargo + 2)][1].ToString();
                            modelF.fcl_payment_terms.Cargoes.Add(new FclCargo()
                            {
                                cargo = valDDLID,
                                quantity = dt.Rows[8 + n_cargo + 1][1].ToString(),
                                unit = tUnit,
                                unit_other = unit_other,
                                tolerance = tTolerance
                            });
                            n_cargo += 3;
                        }
                        else
                        {
                            end_cargo = false;
                        }
                    }

                    //modelF.fcl_payment_terms.cargo = valDDLID;
                    //modelF.fcl_payment_terms.unit = dt.Rows[7][3].ToString();
                    modelF.fcl_payment_terms.dem = dt.Rows[7][3].ToString();
                    modelF.fcl_payment_terms.payment_term = dt.Rows[8][3].ToString();
                    //modelF.fcl_payment_terms.quantity = dt.Rows[9][1].ToString();
                    modelF.fcl_payment_terms.transit_loss = dt.Rows[9][3].ToString();
                    modelF.laycan = dt.Rows[n_cargo + 8][1].ToString();
                    valDDLID = dt.Rows[10][3].ToString();
                    if (valDDLID.Trim() != "")
                    {
                        var valData = getBroker();
                        valData = valData.Where(p => p.Text == valDDLID).ToList();
                        if (valData != null)
                        {
                            foreach (var i in valData)
                            {
                                valDDLID = i.Value;
                            }
                        }
                    }
                    modelF.fcl_payment_terms.broker = valDDLID;
                    modelF.fcl_payment_terms.broker_comm = dt.Rows[11][3].ToString();
                    modelF.fcl_payment_terms.withold_tax = dt.Rows[14][3].ToString();


                    modelF.fcl_payment_terms.ControlPort = new List<Port_Control>();
                    modelF.fcl_payment_terms.ControlDischarge = new List<Discharge_Control>();
                    int n = 0;
                    bool endPort = false;
                    while (endPort == false)
                    {
                        if (dt.Rows[n_cargo + 11 + n][0].ToString().ToUpper().Contains("PORT"))
                        {
                            valDDLID = dt.Rows[n_cargo + 11 + n][1].ToString();
                            Jetty_Port jetty_port = new Jetty_Port();

                            if (dt.Rows[n_cargo + 11 + n].ItemArray.Length >= 9)
                            {
                                jetty_port = EstimateFreightServiceModel.getPortJettyById(dt.Rows[n_cargo + 11 + n][8].ToString());
                            }

                            if (valDDLID.Trim() != "")
                            {
                                string[] port = valDDLID.Split(',');
                                int idx_port = port.Length > 2 ? 0 : 1;
                                if (port.Count() > 0)
                                {
                                    if (dt.Rows[n_cargo + 11 + n][0].ToString().ToUpper().Contains("LOAD"))
                                    {
                                        Port_Control temp = new Port_Control();
                                        temp.PortCountry = port[2 - idx_port].Trim();
                                        temp.PortLocation = port[1 - idx_port].Trim();
                                        temp.PortName = jetty_port.JettyPortName == "OTHER" ? "OTHER" : port[0];
                                        temp.PortFullName = valDDLID;
                                        temp.PortOrder = (modelF.fcl_payment_terms.ControlPort.Count() + 1).ToString();
                                        //temp.JettyPortKey = dt.Rows[n_cargo + 9 + n][8].ToString();
                                        //temp.PortOtherDesc = dt.Rows[n_cargo + 9 + n][9].ToString();
                                        //temp.PortValue = valDDLID;
                                        modelF.fcl_payment_terms.ControlPort.Add(temp);
                                    }
                                    else
                                    {
                                        Discharge_Control temp = new Discharge_Control();
                                        temp.PortCountry = port[2 - idx_port].Trim();
                                        temp.PortLocation = port[1 - idx_port].Trim();
                                        temp.DischargeName = jetty_port.JettyPortName == "OTHER" ? "OTHER" : port[0];
                                        temp.DischargeFullName = valDDLID;
                                        temp.PortOrder = (modelF.fcl_payment_terms.ControlDischarge.Count() + 1).ToString();
                                        //temp.JettyPortKey = dt.Rows[n_cargo + 9 + n][8].ToString();
                                        //temp.PortOtherDesc = dt.Rows[n_cargo + 9 + n][9].ToString();
                                        //temp.DischargeValue = valDDLID;
                                        modelF.fcl_payment_terms.ControlDischarge.Add(temp);
                                    }

                                }
                            }
                            n++;
                        }
                        else
                        {
                            endPort = true;
                            n += -2;
                        }
                    }

                    modelF.fcl_payment_terms.add_comm = dt.Rows[12][3].ToString();
                    modelF.fcl_payment_terms.total_comm = dt.Rows[13][3].ToString();

                    modelF.fcl_payment_terms.laytime = dt.Rows[n_cargo + 9][1].ToString();
                    modelF.fcl_payment_terms.bss = dt.Rows[n_cargo + 10][1].ToString();
                    #endregion

                    #region Time for operation (Days)

                    n_cargo++;

                    modelF.fcl_time_for_operation.loading_port = string.IsNullOrEmpty(dt.Rows[n_cargo + 14 + n][1].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 14 + n][1].ToString()), 4).ToString();
                    modelF.fcl_time_for_operation.discharge_port = string.IsNullOrEmpty(dt.Rows[n_cargo + 15 + n][1].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 15 + n][1].ToString()), 4).ToString();
                    modelF.fcl_time_for_operation.distance_ballast = string.IsNullOrEmpty(dt.Rows[n_cargo + 16 + n][1].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 16 + n][1].ToString()), 4).ToString();
                    modelF.fcl_time_for_operation.ballast = string.IsNullOrEmpty(dt.Rows[n_cargo + 17 + n][1].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 17 + n][1].ToString()), 4).ToString();

                    modelF.fcl_time_for_operation.distance_laden = string.IsNullOrEmpty(dt.Rows[n_cargo + 14 + n][3].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 14 + n][3].ToString()), 4).ToString();
                    modelF.fcl_time_for_operation.laden = string.IsNullOrEmpty(dt.Rows[n_cargo + 15 + n][3].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 15 + n][3].ToString()), 4).ToString();
                    modelF.fcl_time_for_operation.tank_cleaning = string.IsNullOrEmpty(dt.Rows[n_cargo + 16 + n][3].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 16 + n][3].ToString()), 4).ToString();
                    modelF.fcl_time_for_operation.total_duration = string.IsNullOrEmpty(dt.Rows[n_cargo + 17 + n][3].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[n_cargo + 17 + n][3].ToString()), 4).ToString();
                    #endregion

                    #region Port Charge (USD)
                    //modelF.fcl_port_charge.load_port = dt.Rows[20][1].ToString();
                    //modelF.fcl_port_charge.discharge_port = dt.Rows[21][1].ToString();
                    int loadIndex = 0;
                    int dischargeIndex = 0;
                    endPort = false;
                    while (endPort == false)
                    {
                        if (dt.Rows[n_cargo + 19 + n][0].ToString().ToUpper().Contains("PORT DISBURSEMENT"))
                        {
                            valDDLID = dt.Rows[n_cargo + 19 + n][1].ToString();
                            if (valDDLID.Trim() != "")
                            {
                                if (dt.Rows[n_cargo + 19 + n][0].ToString().ToUpper().Contains("DISBURSEMENT AT LOAD PORT"))
                                {
                                    if (modelF.fcl_payment_terms.ControlPort != null && loadIndex < modelF.fcl_payment_terms.ControlPort.Count())
                                    {
                                        modelF.fcl_payment_terms.ControlPort[loadIndex].PortValue = valDDLID;
                                        string portKey = dt.Rows[n_cargo + 19 + n][8].ToString();
                                        modelF.fcl_payment_terms.ControlPort[loadIndex].JettyPortKey = portKey;
                                        modelF.fcl_payment_terms.ControlPort[loadIndex].PortType = getJettyPortByIdServiceModel(portKey).PortType;
                                        if (modelF.fcl_payment_terms.ControlPort[loadIndex].PortName.Contains("OTHER"))
                                        {
                                            string[] arr_port = modelF.fcl_payment_terms.ControlPort[loadIndex].PortFullName.SplitWord(", ");
                                            if (arr_port.Length == 2)
                                            {
                                                modelF.fcl_payment_terms.ControlPort[loadIndex].PortFullName = "OTHER, " + modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeFullName;
                                                modelF.fcl_payment_terms.ControlPort[loadIndex].PortOtherDesc = "";
                                            }
                                            else if (arr_port.Length == 3)
                                            {
                                                modelF.fcl_payment_terms.ControlPort[loadIndex].PortFullName = "OTHER(" + arr_port[0] + "), " + arr_port[1] + ", " + arr_port[2];
                                                modelF.fcl_payment_terms.ControlPort[loadIndex].PortOtherDesc = arr_port[0];
                                            }
                                            //modelF.fcl_payment_terms.ControlPort[loadIndex].PortOtherDesc = modelF.fcl_payment_terms.ControlPort[loadIndex].PortName.SplitWord("(")[1].SplitWord(")")[0];
                                            //modelF.fcl_payment_terms.ControlPort[loadIndex].PortOtherDesc = modelF.fcl_payment_terms.ControlPort[loadIndex].PortName;
                                        }
                                        loadIndex += 1;
                                    }

                                    //Port_Control temp = new Port_Control();
                                    //temp.PortValue = valDDLID;
                                    //modelF.fcl_payment_terms.ControlPort.Add(temp);
                                }
                                else
                                {
                                    if (modelF.fcl_payment_terms.ControlDischarge != null && dischargeIndex < modelF.fcl_payment_terms.ControlDischarge.Count())
                                    {
                                        modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeValue = valDDLID;
                                        string portKey = dt.Rows[n_cargo + 19 + n][8].ToString();
                                        modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].JettyPortKey = portKey;
                                        modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].PortType = getJettyPortByIdServiceModel(portKey).PortType;
                                        if (modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeName.Contains("OTHER"))
                                        {
                                            string[] arr_port = modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeFullName.SplitWord(", ");
                                            if (arr_port.Length == 2)
                                            {
                                                modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeFullName = "OTHER, " + modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeFullName;
                                                modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].PortOtherDesc = "";
                                            }
                                            else if (arr_port.Length == 3)
                                            {
                                                modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeFullName = "OTHER(" + arr_port[0] + "), " + arr_port[1] + ", " + arr_port[2];
                                                modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].PortOtherDesc = arr_port[0];
                                            }

                                            //modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].PortOtherDesc = modelF.fcl_payment_terms.ControlDischarge[dischargeIndex].DischargeName.SplitWord("(")[1].SplitWord(")")[0];
                                        }
                                        dischargeIndex += 1;
                                    }
                                    //Discharge_Control temp = new Discharge_Control();
                                    //temp.DischargeValue = valDDLID;
                                    //modelF.fcl_payment_terms.ControlDischarge.Add(temp);
                                }

                            }
                            n++;
                        }
                        else
                        {
                            //valDDLID = dt.Rows[n_cargo + 19 + n][1].ToString();
                            modelF.total_port_charge = dt.Rows[n_cargo + 19 + n][1].ToString();
                            endPort = true;
                            n += -2;
                        }
                    }

                    //set Json Port
                    modelF.fcl_payment_terms.json_port_control = new JavaScriptSerializer().Serialize(modelF.fcl_payment_terms.ControlPort);
                    modelF.fcl_payment_terms.json_discharge_control = new JavaScriptSerializer().Serialize(modelF.fcl_payment_terms.ControlDischarge);

                    #endregion

                    #region Other costs (USD)
                    //if ((n_cargo + 21 + n) < 26)
                    //{
                    //    n_cargo += 2;
                    //}
                    //n_cargo += (loadIndex + dischargeIndex);
                    modelF.fcl_other_cost.fix_cost = dt.Rows[n_cargo + 23 + n][1].ToString();
                    modelF.fcl_other_cost.total_fix_cost_per_day = dt.Rows[n_cargo + 24 + n][1].ToString();
                    modelF.fcl_other_cost.total_commession = dt.Rows[n_cargo + 25 + n][1].ToString();
                    modelF.fcl_other_cost.withold_tax = dt.Rows[n_cargo + 26 + n][1].ToString();

                    modelF.fcl_other_cost.others = dt.Rows[n_cargo + 23 + n][3].ToString();
                    modelF.fcl_other_cost.total_expenses = dt.Rows[n_cargo + 24 + n][3].ToString();
                    modelF.fcl_other_cost.suggested_margin = dt.Rows[n_cargo + 25 + n][3].ToString();
                    modelF.fcl_other_cost.suggested_margin_value = dt.Rows[n_cargo + 26 + n][3].ToString();
                    #endregion

                    #region Results
                    modelF.fcl_results.offer_freight.usd = dt.Rows[n_cargo + 28 + n][1].ToString();
                    modelF.fcl_results.offer_freight.baht = dt.Rows[n_cargo + 28 + n][2].ToString();
                    modelF.fcl_results.offer_freight.mt = dt.Rows[n_cargo + 28 + n][3].ToString();
                    modelF.fcl_results.total_variable_cost.usd = dt.Rows[n_cargo + 29 + n][1].ToString();
                    modelF.fcl_results.total_variable_cost.baht = dt.Rows[n_cargo + 29 + n][2].ToString();
                    modelF.fcl_results.profit_over.usd = dt.Rows[n_cargo + 30 + n][1].ToString();
                    modelF.fcl_results.profit_over.baht = dt.Rows[n_cargo + 30 + n][2].ToString();
                    modelF.fcl_results.profit_over.percent = dt.Rows[n_cargo + 30 + n][4].ToString();
                    modelF.fcl_results.total_fix_cost.usd = dt.Rows[n_cargo + 31 + n][1].ToString();
                    modelF.fcl_results.total_fix_cost.baht = dt.Rows[n_cargo + 31 + n][2].ToString();
                    modelF.fcl_results.total_cost.usd = dt.Rows[n_cargo + 32 + n][1].ToString();
                    modelF.fcl_results.total_cost.baht = dt.Rows[n_cargo + 32 + n][2].ToString();
                    modelF.fcl_results.profit_over_total.usd = dt.Rows[n_cargo + 33 + n][1].ToString();
                    modelF.fcl_results.profit_over_total.baht = dt.Rows[n_cargo + 33 + n][2].ToString();
                    modelF.fcl_results.profit_over_total.percent = dt.Rows[n_cargo + 33 + n][4].ToString();
                    modelF.fcl_results.market_ref.usd = dt.Rows[n_cargo + 34 + n][1].ToString();
                    modelF.fcl_results.market_ref.baht = dt.Rows[n_cargo + 34 + n][2].ToString();
                    #endregion

                    #region Vessel speed (Knots)
                    modelF.fcl_vessel_speed.fcl_full.flag = dt.Rows[1][5].ToString() != "" ? "Y" : "";
                    modelF.fcl_vessel_speed.fcl_full.ballast = dt.Rows[1][6].ToString();
                    modelF.fcl_vessel_speed.fcl_full.laden = dt.Rows[1][7].ToString();
                    modelF.fcl_vessel_speed.fcl_eco.flag = dt.Rows[2][5].ToString() != "" ? "Y" : "";
                    modelF.fcl_vessel_speed.fcl_eco.ballast = dt.Rows[2][6].ToString();
                    modelF.fcl_vessel_speed.fcl_eco.laden = dt.Rows[2][7].ToString();
                    #endregion

                    #region Latest bunker price info
                    //modelF.fcl_latest_bunker_price.fcl_grade_fo.date = string.IsNullOrEmpty(dt.Rows[4][5].ToString()) ? "" : Convert.ToDateTime(dt.Rows[4][5]).ToString("dd/MM/yyyy");
                    modelF.fcl_latest_bunker_price.fcl_grade_fo.date = dt.Rows[4][5].ToString();
                    modelF.fcl_latest_bunker_price.fcl_grade_fo.place = dt.Rows[4][6].ToString();
                    modelF.fcl_latest_bunker_price.fcl_grade_fo.price = dt.Rows[4][7].ToString();
                    //modelF.fcl_latest_bunker_price.fcl_grade_mgo.date = string.IsNullOrEmpty(dt.Rows[5][5].ToString()) ? "" : Convert.ToDateTime(dt.Rows[5][5]).ToString("dd/MM/yyyy");
                    modelF.fcl_latest_bunker_price.fcl_grade_mgo.date = dt.Rows[5][5].ToString();
                    modelF.fcl_latest_bunker_price.fcl_grade_mgo.place = dt.Rows[5][6].ToString();
                    modelF.fcl_latest_bunker_price.fcl_grade_mgo.price = dt.Rows[5][7].ToString();
                    #endregion

                    #region Exchange rate
                    modelF.fcl_exchange_rate = dt.Rows[6][5].ToString();

                    #endregion

                    #region Bunker consumption (Ton/Day)
                    modelF.fcl_bunker_consumption.fcl_load.fo = dt.Rows[8][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_load.mgo = dt.Rows[8][6].ToString();
                    modelF.fcl_bunker_consumption.fcl_discharge.fo = dt.Rows[9][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_discharge.mgo = dt.Rows[9][6].ToString();
                    modelF.fcl_bunker_consumption.fcl_steaming_ballast.fo = dt.Rows[10][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_steaming_ballast.mgo = dt.Rows[10][6].ToString();
                    modelF.fcl_bunker_consumption.fcl_steaming_laden.fo = dt.Rows[11][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_steaming_laden.mgo = dt.Rows[11][6].ToString();
                    modelF.fcl_bunker_consumption.fcl_idle.fo = dt.Rows[12][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_idle.mgo = dt.Rows[12][6].ToString();
                    modelF.fcl_bunker_consumption.fcl_tack_cleaning.fo = dt.Rows[13][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_tack_cleaning.mgo = dt.Rows[13][6].ToString();
                    modelF.fcl_bunker_consumption.fcl_heating.fo = dt.Rows[14][5].ToString();
                    modelF.fcl_bunker_consumption.fcl_heating.mgo = dt.Rows[14][6].ToString();
                    #endregion

                    #region Bunker Cost 1 (USD)
                    modelF.fcl_bunker_cost = new List<FclBunkerCost>();
                    FclBunkerCost tmpCost;
                    if (n_cargo == 4)
                    {
                        n_cargo -= 1;
                    }

                    int iBC = n_cargo + 14 + loadIndex + dischargeIndex;
                    if (!string.IsNullOrEmpty(dt.Rows[iBC][4].ToString()))
                    {
                        tmpCost = new FclBunkerCost();
                        string fcl_price_fo_temp = string.IsNullOrEmpty(dt.Rows[iBC][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC][5].ToString()), 4).ToString();
                        string fcl_price_mgo_temp = string.IsNullOrEmpty(dt.Rows[iBC][6].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC][6].ToString()), 4).ToString();

                        string fcl_price_fo = string.IsNullOrEmpty(fcl_price_fo_temp) ? dt.Rows[4][7].ToString() : fcl_price_fo_temp;
                        string fcl_price_mgo = string.IsNullOrEmpty(fcl_price_mgo_temp) ? dt.Rows[5][7].ToString() : fcl_price_mgo_temp;

                        tmpCost.fcl_price.fo = fcl_price_fo;
                        tmpCost.fcl_price.mgo = fcl_price_mgo;

                        //tmpCost.fcl_price.fo = string.IsNullOrEmpty(dt.Rows[iBC][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC][5].ToString()), 4).ToString();
                        //tmpCost.fcl_price.mgo = string.IsNullOrEmpty(dt.Rows[iBC][6].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC][6].ToString()), 4).ToString();

                        //iBC += 2;
                        tmpCost.loading = string.IsNullOrEmpty(dt.Rows[iBC + 2][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC + 2][5].ToString()), 4).ToString();
                        tmpCost.discharge_port = string.IsNullOrEmpty(dt.Rows[iBC + 3][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC + 3][5].ToString()), 4).ToString();
                        tmpCost.steaming_ballast = string.IsNullOrEmpty(dt.Rows[iBC + 4][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC + 4][5].ToString()), 4).ToString();
                        tmpCost.steaming_laden = string.IsNullOrEmpty(dt.Rows[iBC + 5][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC + 5][5].ToString()), 4).ToString();
                        tmpCost.tank_cleaning = string.IsNullOrEmpty(dt.Rows[iBC + 6][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC + 6][5].ToString()), 4).ToString();
                        modelF.total_bunker_cost = string.IsNullOrEmpty(dt.Rows[iBC + 7][5].ToString()) ? "" : Math.Round(Convert.ToDouble(dt.Rows[iBC + 7][5].ToString()), 4).ToString();
                        modelF.fcl_bunker_cost.Add(tmpCost);
                    } else
                    {
                        modelF.fcl_bunker_cost.Add(new FclBunkerCost());
                    }
                    //iBC += 6;

                    #endregion

                    #region Remark
                    //modelF.fcl_remark = dt.Rows[iBC + 11][5].ToString();
                    modelF.fcl_remark = dt.Rows[n_cargo + 28 + n][5].ToString();

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelF;
        }

        private static string EmtrytoZero(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value;
            }
            else
            {
                return "0";
            }
        }
    }
}