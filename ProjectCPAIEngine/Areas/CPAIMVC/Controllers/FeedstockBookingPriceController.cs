﻿using com.pttict.downstream.common.utilities;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using System.Globalization;
using System.Drawing;
using OfficeOpenXml.Style;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class FeedstockBookingPriceController : BaseController
    {
        private CultureInfo provider = new CultureInfo("en-US");
        private string format = "dd/MM/yyyy";
        const string JSON_CRUDE_PURCHASE = "JSON_CRUDE_PURCHASE";
        public ActionResult index()
        {
            return RedirectToAction("Create", "FeedstockBookingPrice");
        }

        [HttpGet]
        public ActionResult Create()
        {
            FeedstockBookingPriceViewModel model = new FeedstockBookingPriceViewModel();
            model = initialModel();

            return View(model);
        }


        public FeedstockBookingPriceViewModel initialModel()
        {
            FeedstockBookingPriceServiceModel serviceModel = new FeedstockBookingPriceServiceModel();
            FeedstockBookingPriceViewModel model = new FeedstockBookingPriceViewModel();
            model.data_Search = new FeedStockBookingPriceSearch();
            model.data_Search.sPriceDetail = new UpdatePriceDetail();
            model.data_Search.vAC_1ST_select = true;
            model.data_Search.vAC_2SD_select = true;

            return model;
        }

        private List<SelectListItem> getForFeedStock()
        {
            //LoadMaster From JSON
            PurchaseOrderController item = new PurchaseOrderController();
            return item.GetFeedStockPIT();
        }
        private List<SelectListItem> getFeedStock(bool isHidden = false)
        {
            //LoadMaster From JSON
            PurchaseOrderController item = new PurchaseOrderController();
            return item.GetFeedStockPIT();
        }

        public void getDropDownList(ref FeedstockBookingPriceViewModel model)
        {
            List<SelectListItem> companyList = new List<SelectListItem>();
            var ddlCompany = DropdownServiceModel.getCompany();
            var companyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").MT_COMPANY;
            foreach (var company in ddlCompany)
            {
                var result = companyGlobalConfig.Where(x => x.CODE == company.Value).ToList();
                if (result.Count != 0)
                {
                    companyList.Add(company);
                }
            }
            model.ddl_Company = companyList;
            model.ddl_Supplier = DropdownServiceModel.getVendorPIT();
            model.ddl_Feedstock = getFeedStock();

            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Provisional", Value = "PRO" });
            list.Add(new SelectListItem { Text = "Final", Value = "FINAL" });
            model.ddl_Invoice = list;

        }


        [HttpGet]
        public ActionResult Search()
        {
            FeedstockBookingPriceViewModel model = new FeedstockBookingPriceViewModel();
            if (TempData["tempPrice"] != null)
            {
                model = (FeedstockBookingPriceViewModel)TempData["tempPrice"];
                model.data_Search.vFocus = "div_search";
                TempData.Remove("tempPrice");
            }

            return View("Create", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Search")]
        //public ActionResult Seacrh(FormCollection form, FeedstockBookingPriceViewModel model)
        public ActionResult Seacrh(FeedstockBookingPriceViewModel model)
        {
            getDropDownList(ref model);
            FeedstockBookingPriceServiceModel serviceModel = new FeedstockBookingPriceServiceModel();
            model.data_Search.sPriceDetail = new UpdatePriceDetail();
            model.data_Search.sPriceDetail.dQualityList = new List<QualityProductDetail>();
            model.data_Search.sPriceDetail.dProductList = new List<ProductDetail>();
            serviceModel.Search(ref model);

            TempData["tempPrice"] = model;
            return RedirectToAction("Search", "FeedstockBookingPrice");
        }

        [HttpGet]
        public ActionResult ChangeListPrice()
        {
            FeedstockBookingPriceViewModel model = new FeedstockBookingPriceViewModel();
            if (TempData["ChangeListPrice"] != null)
            {
                model = (FeedstockBookingPriceViewModel)TempData["ChangeListPrice"];
                model.data_Search.vFocus = "div_change";
                getDropDownList(ref model);
                TempData.Remove("ChangeListPrice");
            }

            return View("Create", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "ChangeListPrice")]
        public ActionResult ChangeListPrice(FormCollection form, FeedstockBookingPriceViewModel model)
        {
            FeedstockBookingPriceServiceModel service = new FeedstockBookingPriceServiceModel();
            
            service.getProductQualityLastFinal(ref model);
            
            //return View("Create", model);
            TempData["ChangeListPrice"] = model;
            return RedirectToAction("ChangeListPrice", "FeedstockBookingPrice");
        }

        [HttpGet]
        public ActionResult CalculatePrice()
        {
            FeedstockBookingPriceViewModel model = new FeedstockBookingPriceViewModel();
            if (TempData["CalculatePrice"] != null)
            {
                model = (FeedstockBookingPriceViewModel)TempData["CalculatePrice"];
                model.data_Search.vFocus = "btn_callculate";
                getDropDownList(ref model);
                TempData.Remove("CalculatePrice");
            }

            return View("Create", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "CalculatePrice")]
        public ActionResult CalculatePrice(FormCollection form, FeedstockBookingPriceViewModel model)
        {
            FeedstockBookingPriceServiceModel service = new FeedstockBookingPriceServiceModel();
            
            service.mainCalculatePrice(ref model);
            //return View("Create", model);
            TempData["CalculatePrice"] = model;
            return RedirectToAction("CalculatePrice", "FeedstockBookingPrice");
        }

        [HttpGet]
        public ActionResult SavePrice()
        {
            FeedstockBookingPriceViewModel model = new FeedstockBookingPriceViewModel();
            if (TempData["SavePrice"] != null)
            {
                model = (FeedstockBookingPriceViewModel)TempData["SavePrice"];
                model.data_Search.vFocus = "tbProduct";
                getDropDownList(ref model);
                TempData.Remove("SavePrice");
                TempData["ReponseMessage"] = "Save success.";
                TempData["ReponseStatus"] = "FALSE";
            }
            
            return View("Create", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SavePrice")]
        public ActionResult SavePrice(FormCollection form, FeedstockBookingPriceViewModel model)
        {
            FeedstockBookingPriceServiceModel service = new FeedstockBookingPriceServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = service.Save(model, lbUserName);
            
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            TempData["SavePrice"] = model;
            //return View("Create", model);

            return RedirectToAction("SavePrice", "FeedstockBookingPrice");
        }

        [HttpGet]
        public string getJsonPONOlist(string company = "", string supplier = "", string fromto = "")
        {
            //return JSonConvertUtil.modelToJson(EstimateFreightServiceModel.getPortJetty("", "", "", "", ""));
            return JSonConvertUtil.modelToJson(FeedstockBookingPriceServiceModel.getPONOlist_DDL(company, supplier, fromto));
        }

        [HttpGet]
        public string getProductQuality(string po_no = "", string feedstock = "", string fromTo = "", string tripNo = "", string invoiceNOStatus = "", string supplier= "")
        {
            return JSonConvertUtil.modelToJson(FeedstockBookingPriceServiceModel.getProductQuality(po_no, feedstock, fromTo, invoiceNOStatus, tripNo, supplier));

        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenExcel")]
        public void GenExcel(FormCollection form, FeedstockBookingPriceViewModel model)
        {
            GenExcel(model);
        }

        System.Drawing.Color BLUE = System.Drawing.Color.DodgerBlue;
        System.Drawing.Color RED = System.Drawing.Color.Red;
        System.Drawing.Color WHITE = System.Drawing.Color.White;
        //[HttpPost]
        //[ValidateInput(false)]
        //[MultipleButton(Name = "action", Argument = "GenExcel")]
        public ActionResult GenExcel(FeedstockBookingPriceViewModel model)
        {
            try
            {
                if (model != null)
                {
                    string file_name = string.Format("FeedStock_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                    string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", file_name);


                    FileInfo excelFile = new FileInfo(file_path);
                    using (ExcelPackage package = new ExcelPackage(excelFile))
                    {
                        ExcelWorksheet ws = package.Workbook.Worksheets.Add("New");
                        #region Title
                        //Add an image.
                        //string img_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web/FileUpload/FREIGHT/IMAGE", model.fcl_pic_path == null ? "" : model.fcl_pic_path);
                        //if (System.IO.File.Exists(img_path))
                        //{
                        //    AddImage(ws, 38, 0, img_path);
                        //}
                        //Initialize columns
                        ws.Column(1).Width = 20;
                        ws.Column(2).Width = 20;
                        ws.Column(3).Width = 20;
                        ws.Column(4).Width = 20;
                        ws.Column(5).Width = 20;
                        ws.Column(6).Width = 20;
                        ws.Column(7).Width = 20;
                        ws.Column(8).Width = 20;
                        ws.Column(9).Width = 20;
                        ws.Column(10).Width = 20;
                        ws.Column(11).Width = 20;
                        ws.Column(12).Width = 20;
                        ws.Column(13).Width = 20;
                        ws.Column(14).Width = 20;
                        ws.Column(15).Width = 22;
                        ws.Column(16).Width = 22;
                        ws.Column(17).Width = 20;
                        ws.Column(18).Width = 20;
                        ws.Column(19).Width = 20;
                        ws.Column(20).Width = 20;
                        //ws.Column(21).Width = 15;
                        //ws.Column(22).Width = 15;
                        //ws.Column(23).Width = 15;
                        //ws.Column(24).Width = 15;
                        //ws.Column(25).Width = 15;
                        //ws.Column(26).Width = 15;
                        //ws.Column(27).Width = 15;
                        //ws.Column(28).Width = 15;
                        //ws.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //ws.Column(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //ws.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //ws.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //ws.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        //ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 9));
                        
                        string product = "";
                        string company = "";
                        bool isFinal = true;

                        var modelTable = ws.Cells["A4:R" + (5 + model.data_Search.sPriceDetail.dQualityList.Count-1)];

                        if (model.data_Search.sPriceDetail.dQualityList.Count > 0)
                        {
                            product = model.data_Search.sPriceDetail.dQualityList[0].Product;
                            if(model.data_Search.sPriceDetail.dQualityList[0].InvoiceNoStatus.Trim().ToUpper() == "PRO")
                            {
                                isFinal = false;
                            }
                            if (product.Trim().ToUpper().Contains("BLENDED KEROSENE")
                            || product.Trim().ToUpper().Contains("PEP AROMATICS AND HAB")
                            || product.Trim().ToUpper().Contains("STRIPPER OVHD LIQUID")
                            || product.Trim().ToUpper().Contains("HYDROCARBON")
                            || product.Trim().ToUpper().Contains("A9") || product.Trim().ToUpper().Contains("C9")
                            || product.Trim().ToUpper().Contains("MX") || product.Trim().ToUpper().Contains("C5")
                            || product.Trim().ToUpper().Contains("C4")
                            || product.Trim().ToUpper().Contains("TOLUENE")
                            || product.ToUpper().Contains("HEAVIES") || product.ToUpper().Contains("C10")
                            || product.ToUpper().Contains("RAFFINATE"))
                            {
                                modelTable = ws.Cells["A4:I" + (5 + model.data_Search.sPriceDetail.dQualityList.Count-1)];
                                ws.Cells["A4:I5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                                // Assign borders
                                modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                company = "TPX/LABIX";
                                ws.Cells["A1:I1"].Merge = true;
                                ws.Cells["A1:I1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                                //ws.Cells["A4:AA5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                ws.Cells["A4:I5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A4:I5"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells["A4:I5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));
                            }
                            else
                            {
                                modelTable = ws.Cells["A4:R" + (6 + model.data_Search.sPriceDetail.dQualityList.Count-1)];
                                ws.Cells["A4:R5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                                // Assign borders
                                modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                ws.Cells["A1:R1"].Merge = true;
                                ws.Cells["A1:R1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                                //ws.Cells["A4:AA5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                ws.Cells["A4:R5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A4:R5"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells["A4:R5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));
                            }
                        }
                        else
                        {
                            modelTable = ws.Cells["A4:R" + (5 + model.data_Search.sPriceDetail.dQualityList.Count-1)];
                            ws.Cells["A4:R5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            // Assign borders
                            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            ws.Cells["A1:R1"].Merge = true;
                            ws.Cells["A1:R1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                            //ws.Cells["A4:AA5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                            ws.Cells["A4:R5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            ws.Cells["A4:R5"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ws.Cells["A4:R5"].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));
                        }
                        
                        string headTitle = "Final/Pro.Payment";
                        if (model.data_Search != null)
                        {
                            if (!string.IsNullOrEmpty(model.data_Search.vInvoice))
                            {
                                if (model.data_Search.vInvoice == "FINAL")
                                {
                                    headTitle = "Final Payment";
                                }
                                else
                                {
                                    headTitle = "Pro Payment";
                                }
                            }
                            else
                            {
                                headTitle = "Pro Payment";
                            }
                        }
                        ws.Cells[1, 1].Value = headTitle;
                        ws.Cells[1, 1].Style.Font.Size = 16;
                        ws.Cells[1, 1].Style.Font.Bold = true;
                        
                        if (model.data_Search.sPriceDetail.dQualityList.Count > 0)
                        {//vSearchPeriod
                            var sDateFrom = (!isFinal ? "01" : "16") + DateTime.ParseExact(model.data_Search.sPriceDetail.dQualityList[0].PODate,format,provider).ToString("/MM/yyyy", provider);
                            var sDateTO = (!isFinal ? "15" : "30") + DateTime.ParseExact(model.data_Search.sPriceDetail.dQualityList[0].PODate, format, provider).ToString("/MM/yyyy", provider);
                            DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);

                            DateTime dateTo = DateTime.ParseExact(sDateTO, format, provider);
                            if (isFinal)
                            {
                                dateTo = DateTime.ParseExact(("01/" + sDateFrom.Substring(3, 7)), format, provider).AddMonths(1).AddDays(-1);
                            }
                            
                            
                            string tmpDate = "";
                            tmpDate = "Period: " + dateFrom.ToString("dd MMM yyyy", provider) + "-" + dateTo.ToString("dd MMM yyyy", provider);
                            ws.Cells[2, 1].Value = tmpDate;
                            ws.Cells[2, 1].Style.Font.Size = 12;
                            ws.Cells[2, 1].Style.Font.Bold = true;
                        }
                        #endregion
                        //--------------------------------Head Table
                        #region Head Table

                        if (company == "TPX/LABIX")
                        {
                            ws.Cells["A4:A5"].Merge = true;
                            //ws.Cells["A4:A5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 1].Value = "Date";
                            ws.Cells[4, 1].Style.Font.Size = 10;
                            ws.Cells[4, 1].Style.Font.Bold = true;

                            ws.Cells["B4:B5"].Merge = true;
                            //ws.Cells["B4:C4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 2].Value = "Invoice No.";
                            ws.Cells[4, 2].Style.Font.Size = 10;
                            ws.Cells[4, 2].Style.Font.Bold = true;
                            
                            //ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[5, 3].Value = "Status";
                            //ws.Cells[5, 3].Style.Font.Size = 10;
                            //ws.Cells[5, 3].Style.Font.Bold = true;

                            ws.Cells["C4:C5"].Merge = true;
                            //ws.Cells["D4:D5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 3].Value = "Trip No.";
                            ws.Cells[4, 3].Style.Font.Size = 10;
                            ws.Cells[4, 3].Style.Font.Bold = true;

                            ws.Cells["D4:D5"].Merge = true;
                            //ws.Cells["E4:E5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 4].Value = "Product";
                            ws.Cells[4, 4].Style.Font.Size = 10;
                            ws.Cells[4, 4].Style.Font.Bold = true;

                            //ws.Cells["F4:F5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 5].Value = "Volume";
                            ws.Cells[4, 5].Style.Font.Size = 10;
                            ws.Cells[4, 5].Style.Font.Bold = true;
                            ws.Cells[5, 5].Value = "(mts)";
                            ws.Cells[5, 5].Style.Font.Size = 10;
                            ws.Cells[5, 5].Style.Font.Bold = true;
                            
                            ws.Cells["E4:E4"].Merge = true;
                            //ws.Cells["O4:P4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 6].Value = "Unit Price";
                            ws.Cells[4, 6].Style.Font.Size = 10;
                            ws.Cells[4, 6].Style.Font.Bold = true;

                            ws.Cells[5, 6].Value = "New Formular (Baht/mt)";
                            //ws.Cells[5, 15].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 6].Style.Font.Size = 10;
                            ws.Cells[5, 6].Style.Font.Bold = true;

                            //ws.Cells[5, 7].Value = "Old Formula (Baht/mt)";
                            ////ws.Cells[5, 16].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[5, 7].Style.Font.Size = 10;
                            //ws.Cells[5, 7].Style.Font.Bold = true;

                            ws.Cells["G4:I4"].Merge = true;
                            //ws.Cells["Q4:R4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 7].Value = "Price";
                            ws.Cells[4, 7].Style.Font.Size = 10;
                            ws.Cells[4, 7].Style.Font.Bold = true;

                            ws.Cells[5, 7].Value = "Price(Baht)";
                            //ws.Cells[5, 17].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 7].Style.Font.Size = 10;
                            ws.Cells[5, 7].Style.Font.Bold = true;

                            //ws.Cells[5, 18].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 8].Value = "Vat(Baht)";
                            ws.Cells[5, 8].Style.Font.Size = 10;
                            ws.Cells[5, 8].Style.Font.Bold = true;

                            //ws.Cells[5, 19].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 9].Value = "Total(Inc.Vat.)";
                            ws.Cells[5, 9].Style.Font.Size = 10;
                            ws.Cells[5, 9].Style.Font.Bold = true;

                            //ws.Cells[4, 23].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[4, 11].Value = "Price(Include VAT)";
                            //ws.Cells[4, 11].Style.Font.Size = 10;
                            //ws.Cells[4, 11].Style.Font.Bold = true;

                            ////ws.Cells[5, 23].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[5, 11].Value = "(Baht)";
                            //ws.Cells[5, 11].Style.Font.Size = 10;
                            //ws.Cells[5, 11].Style.Font.Bold = true;
                        }
                        else
                        {
                            ws.Cells["A4:A5"].Merge = true;
                            //ws.Cells["A4:A5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 1].Value = "Date";
                            ws.Cells[4, 1].Style.Font.Size = 10;
                            ws.Cells[4, 1].Style.Font.Bold = true;

                            //ws.Cells["B4:C4"].Merge = true;
                            //ws.Cells["B4:C4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells["B4:B5"].Merge = true;
                            //ws.Cells["B4:C4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 2].Value = "Invoice No.";
                            ws.Cells[4, 2].Style.Font.Size = 10;
                            ws.Cells[4, 2].Style.Font.Bold = true;

                            //ws.Cells[5, 3].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[5, 3].Value = "Status";
                            //ws.Cells[5, 3].Style.Font.Size = 10;
                            //ws.Cells[5, 3].Style.Font.Bold = true;

                            ws.Cells["C4:C5"].Merge = true;
                            //ws.Cells["D4:D5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 3].Value = "Trip No.";
                            ws.Cells[4, 3].Style.Font.Size = 10;
                            ws.Cells[4, 3].Style.Font.Bold = true;

                            ws.Cells["D4:D5"].Merge = true;
                            //ws.Cells["E4:E5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 4].Value = "Product";
                            ws.Cells[4, 4].Style.Font.Size = 10;
                            ws.Cells[4, 4].Style.Font.Bold = true;

                            //ws.Cells["F4:F5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 5].Value = "Volume";
                            ws.Cells[4, 5].Style.Font.Size = 10;
                            ws.Cells[4, 5].Style.Font.Bold = true;
                            ws.Cells[5, 5].Value = "(mts)";
                            ws.Cells[5, 5].Style.Font.Size = 10;
                            ws.Cells[5, 5].Style.Font.Bold = true;

                            ws.Cells["F4:H4"].Merge = true;
                            //ws.Cells["G4:I4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 6].Value = "Quality";
                            ws.Cells[4, 6].Style.Font.Size = 10;
                            ws.Cells[4, 6].Style.Font.Bold = true;

                            //ws.Cells[5, 7].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 6].Value = "Den@15C";
                            ws.Cells[5, 6].Style.Font.Size = 10;
                            ws.Cells[5, 6].Style.Font.Bold = true;
                            //ws.Cells[5, 8].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 7].Value = "Visc, cst@50 C";
                            ws.Cells[5, 7].Style.Font.Size = 10;
                            ws.Cells[5, 7].Style.Font.Bold = true;
                            //ws.Cells[5, 9].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 8].Value = "Sulfur(%wt)";
                            ws.Cells[5, 8].Style.Font.Size = 10;
                            ws.Cells[5, 8].Style.Font.Bold = true;

                            ws.Cells["I4:I4"].Merge = true;
                            //ws.Cells["J4:K4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 9].Value = "Unit Price";
                            ws.Cells[4, 9].Style.Font.Size = 10;
                            ws.Cells[4, 9].Style.Font.Bold = true;

                            //ws.Cells[5, 10].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 9].Value = "New Formula ($/mt)";
                            ws.Cells[5, 9].Style.Font.Size = 10;
                            ws.Cells[5, 9].Style.Font.Bold = true;

                            //ws.Cells[5, 11].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 10].Value = "Old Formular ($/mt)";
                            ws.Cells[5, 10].Style.Font.Size = 10;
                            ws.Cells[5, 10].Style.Font.Bold = true;

                            ws.Cells["K4:K5"].Merge = true;
                            //ws.Cells["L4:L5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 11].Value = "Adjust Price";
                            ws.Cells[4, 11].Style.Font.Size = 10;
                            ws.Cells[4, 11].Style.Font.Bold = true;

                            ws.Cells["L4:L5"].Merge = true;
                            //ws.Cells["M4:M5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 12].Value = "Exchange Rate New";
                            ws.Cells[4, 12].Style.Font.Size = 10;
                            ws.Cells[4, 12].Style.Font.Bold = true;

                            ws.Cells["M4:M5"].Merge = true;
                            //ws.Cells["N4:N5"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 13].Value = "Exchange Rate Old";
                            ws.Cells[4, 13].Style.Font.Size = 10;
                            ws.Cells[4, 13].Style.Font.Bold = true;

                            ws.Cells["N4:N4"].Merge = true;
                            //ws.Cells["O4:P4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 14].Value = "Unit Price";
                            ws.Cells[4, 14].Style.Font.Size = 10;
                            ws.Cells[4, 14].Style.Font.Bold = true;

                            ws.Cells[5, 14].Value = "New Formular (Baht/mt)";
                            //ws.Cells[5, 15].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 14].Style.Font.Size = 10;
                            ws.Cells[5, 14].Style.Font.Bold = true;

                            ws.Cells[5, 15].Value = "Old Formula (Baht/mt)";
                            //ws.Cells[5, 16].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 15].Style.Font.Size = 10;
                            ws.Cells[5, 15].Style.Font.Bold = true;

                            ws.Cells["P4:R4"].Merge = true;
                            //ws.Cells["Q4:R4"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[4, 16].Value = "Price";
                            ws.Cells[4, 16].Style.Font.Size = 10;
                            ws.Cells[4, 16].Style.Font.Bold = true;

                            ws.Cells[5, 16].Value = "Price(Baht)";
                            //ws.Cells[5, 17].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 16].Style.Font.Size = 10;
                            ws.Cells[5, 16].Style.Font.Bold = true;

                            //ws.Cells[5, 18].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 17].Value = "Vat(Baht)";
                            ws.Cells[5, 17].Style.Font.Size = 10;
                            ws.Cells[5, 17].Style.Font.Bold = true;

                            //ws.Cells[5, 19].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            ws.Cells[5, 18].Value = "Total(Inc.Vat.)";
                            ws.Cells[5, 18].Style.Font.Size = 10;
                            ws.Cells[5, 18].Style.Font.Bold = true;

                            //ws.Cells[4, 23].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[4, 19].Value = "Price(Include VAT)";
                            //ws.Cells[4, 19].Style.Font.Size = 10;
                            //ws.Cells[4, 19].Style.Font.Bold = true;

                            ////ws.Cells[5, 23].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            //ws.Cells[5, 19].Value = "(Baht)";
                            //ws.Cells[5, 19].Style.Font.Size = 10;
                            //ws.Cells[5, 19].Style.Font.Bold = true;
                        }
                        
                        #endregion

                        #region Body Detail
                        //5 + model.data_Search.sPriceDetail.dQualityList.Count
                        int detail_num = 5;
                        
                        foreach (QualityProductDetail i in model.data_Search.sPriceDetail.dQualityList)
                        {
                            detail_num++;
                            int z = 1;
                            if (!string.IsNullOrEmpty(i.PODate))
                            {
                                ws.Cells[detail_num, z].Value = i.PODate;
                            }
                            ws.Cells[detail_num, z].Style.Font.Size = 10;
                            if (i.NO == "99")
                            {
                                ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                            }

                            z++;
                            if (!string.IsNullOrEmpty(i.InvoiceNo))
                            {
                                ws.Cells[detail_num, z].Value = i.InvoiceNo;
                            }
                            ws.Cells[detail_num, z].Style.Font.Size = 10;
                            if (i.NO == "99")
                            {
                                ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                            }

                            //z++;
                            //ws.Cells[detail_num, z].Value = i.InvoiceNoStatus;
                            //ws.Cells[detail_num, z].Style.Font.Size = 10;

                            z++;
                            //if (!string.IsNullOrEmpty(i.TripNo))
                            //{
                            //    ws.Cells[detail_num, z].Value = i.TripNo;
                            //}
                            if (!string.IsNullOrEmpty(i.CargoNo))
                            {
                                ws.Cells[detail_num, z].Value = i.CargoNo;
                            }
                            ws.Cells[detail_num, z].Style.Font.Size = 10;
                            if (i.NO == "99")
                            {
                                ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                            }

                            z++;
                            if (!string.IsNullOrEmpty(i.Product))
                            {
                                ws.Cells[detail_num, z].Value = i.Product;
                            }
                            ws.Cells[detail_num, z].Style.Font.Size = 10;
                            if (i.NO == "99")
                            {
                                ws.Cells[detail_num, z].Value = "";
                                ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                            }

                            z++;
                            if (!string.IsNullOrEmpty(i.VolumnMT))
                            {
                                ws.Cells[detail_num, z].Value = i.VolumnMT;
                            }
                            ws.Cells[detail_num, z].Style.Font.Size = 10;
                            ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                            if (i.NO == "99")
                            {
                                ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                            }

                            if ((i.Product ?? "").Trim().ToUpper().Contains("BLENDED KEROSENE")
                            || (i.Product ?? "").Trim().ToUpper().Contains("PEP AROMATICS AND HAB")
                            || (i.Product ?? "").Trim().ToUpper().Contains("STRIPPER OVHD LIQUID")
                            || (i.Product ?? "").Trim().ToUpper().Contains("HYDROCARBON")
                            || (i.Product ?? "").Trim().ToUpper().Contains("A9") || (i.Product ?? "").Trim().ToUpper().Contains("C9")
                            || (i.Product ?? "").Trim().ToUpper().Contains("MX") || (i.Product ?? "").Trim().ToUpper().Contains("C5")
                            || (i.Product ?? "").Trim().ToUpper().Contains("C4")
                            || (i.Product ?? "").Trim().ToUpper().Contains("TOLUENE")
                            || (i.Product ?? "").ToUpper().Contains("HEAVIES") || (i.Product ?? "").ToUpper().Contains("C10")
                            || (i.Product ?? "").ToUpper().Contains("RAFFINATE"))
                            {
                                z++;
                                if (!string.IsNullOrEmpty(i.UnitPriceTHBNew))
                                {
                                    ws.Cells[detail_num, z].Value = i.UnitPriceTHBNew;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }
                                z++;
                                if (!string.IsNullOrEmpty(i.Price))
                                {
                                    ws.Cells[detail_num, z].Value = i.Price;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.Vat))
                                {
                                    ws.Cells[detail_num, z].Value = i.Vat;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.Total))
                                {
                                    ws.Cells[detail_num, z].Value = i.Total;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                                }
                            }
                            else
                            {
                                
                                z++;
                                if (!string.IsNullOrEmpty(i.Den_15))
                                {
                                    ws.Cells[detail_num, z].Value = i.Den_15;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.Visc_cst_50))
                                {
                                    ws.Cells[detail_num, z].Value = i.Visc_cst_50;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.Sulfur))
                                {
                                    ws.Cells[detail_num, z].Value = i.Sulfur;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.UnitPriceUSDNew))
                                {
                                    ws.Cells[detail_num, z].Value = i.UnitPriceUSDNew;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.UnitPriceUSDOld))
                                {
                                    ws.Cells[detail_num, z].Value = i.UnitPriceUSDOld;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.AdjustPrice))
                                {
                                    ws.Cells[detail_num, z].Value = i.AdjustPrice;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.ExchangeRateNew))
                                {
                                    ws.Cells[detail_num, z].Value = i.ExchangeRateNew;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.ExchangeRateOld))
                                {
                                    ws.Cells[detail_num, z].Value = i.ExchangeRateOld;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.UnitPriceTHBNew))
                                {
                                    ws.Cells[detail_num, z].Value = i.UnitPriceTHBNew;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.UnitPriceTHBOld))
                                {
                                    ws.Cells[detail_num, z].Value = i.UnitPriceTHBOld;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                                }
                                z++;
                                if (!string.IsNullOrEmpty(i.Price))
                                {
                                    ws.Cells[detail_num, z].Value = i.Price;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.Vat))
                                {
                                    ws.Cells[detail_num, z].Value = i.Vat;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                                }

                                z++;
                                if (!string.IsNullOrEmpty(i.Total))
                                {
                                    ws.Cells[detail_num, z].Value = i.Total;
                                }
                                ws.Cells[detail_num, z].Style.Font.Size = 10;
                                ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                                if (i.NO == "99")
                                {
                                    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                                    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                                }
                            }

                            

                            //z++;
                            //if (!string.IsNullOrEmpty(i.invoice_amount))
                            //{
                            //    ws.Cells[detail_num, z].Value = i.invoice_amount;
                            //}
                            //ws.Cells[detail_num, z].Style.Font.Size = 10;
                            //ws.Cells[detail_num, z].Style.Numberformat.Format = "#,##0.0000";
                            //if (i.NO == "99")
                            //{
                            //    ws.Cells[detail_num, z].Style.Border.Left.Style = ExcelBorderStyle.None;
                            //    ws.Cells[detail_num, z].Style.Border.Right.Style = ExcelBorderStyle.None;
                            //    ws.Cells[detail_num, z].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                            //}

                        }
                        #endregion

                        string productName = model.data_Search.sPriceDetail.dQualityList[0].Product;
                        if (productName.Trim().ToUpper().Contains("BLENDED KEROSENE")
                            || productName.Trim().ToUpper().Contains("PEP AROMATICS AND HAB")
                            || productName.Trim().ToUpper().Contains("STRIPPER OVHD LIQUID")
                            || productName.ToUpper().Contains("HYDROCARBON"))
                        {
                            var sDateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                            var sDateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                            DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                            DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);
                            
                            #region Product Price LABIX
                            if (model.data_Search.sPriceDetail != null)
                            {
                                detail_num += 2;
                                
                                string tmpDate = DateTime.ParseExact(model.data_Search.sFXDetail[0].d_Date, "yyyyMMdd", provider).ToString("MMM yyyy", provider);
                                tmpDate = "Period: " + dateFrom.ToString("dd MMM yyyy", provider) + " - " + dateTo.ToString("dd MMM yyyy", provider) ;
                                ws.Cells[detail_num, 1].Value = tmpDate;
                                ws.Cells[detail_num, 1].Style.Font.Size = 12;
                                ws.Cells[detail_num, 1].Style.Font.Bold = true;

                                detail_num++;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));

                                var modelProduct = ws.Cells["A" + detail_num + ":I" + (detail_num + 3)];
                                ws.Cells["A" + detail_num + ":I" + (detail_num + 3)].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                                // Assign borders
                                modelProduct.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                ws.Cells[detail_num, 1].Value = "Reference Prices ($/mt)";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = "KERO";
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = "GASOIL";
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = "PROPANE";
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;
                                ws.Cells[detail_num, 5].Value = "BUTANE";
                                ws.Cells[detail_num, 5].Style.Font.Size = 10;
                                ws.Cells[detail_num, 6].Value = "LPG";
                                ws.Cells[detail_num, 6].Style.Font.Size = 10;
                                ws.Cells[detail_num, 7].Value = "FO380";
                                ws.Cells[detail_num, 7].Style.Font.Size = 10;
                                ws.Cells[detail_num, 8].Value = "ULG92";
                                ws.Cells[detail_num, 8].Style.Font.Size = 10;
                                ws.Cells[detail_num, 9].Value = "NAPHTA";
                                ws.Cells[detail_num, 9].Style.Font.Size = 10;
                                ws.Cells[detail_num, 10].Value = "";
                                ws.Cells[detail_num, 10].Style.Font.Size = 10;

                                detail_num++;
                                ws.Cells[detail_num, 1].Value = "Conversion Factor (bbl/mt)";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.USD_LABIX_KERO;
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = model.data_Search.sPriceDetail.USD_LABIX_GASOIL;
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = model.data_Search.sPriceDetail.USD_LABIX_PROPANE;
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;
                                ws.Cells[detail_num, 5].Value = model.data_Search.sPriceDetail.USD_LABIX_BUTANE;
                                ws.Cells[detail_num, 5].Style.Font.Size = 10;
                                ws.Cells[detail_num, 6].Value = model.data_Search.sPriceDetail.USD_LABIX_LPG;
                                ws.Cells[detail_num, 6].Style.Font.Size = 10;
                                ws.Cells[detail_num, 7].Value = model.data_Search.sPriceDetail.USD_LABIX_FO380;
                                ws.Cells[detail_num, 7].Style.Font.Size = 10;
                                ws.Cells[detail_num, 8].Value = model.data_Search.sPriceDetail.USD_LABIX_ULG92;
                                ws.Cells[detail_num, 8].Style.Font.Size = 10;
                                ws.Cells[detail_num, 9].Value = model.data_Search.sPriceDetail.USD_LABIX_NAPHTA;
                                ws.Cells[detail_num, 9].Style.Font.Size = 10;
                                ws.Cells[detail_num, 10].Value = "";
                                ws.Cells[detail_num, 10].Style.Font.Size = 10;

                                detail_num++;
                                ws.Cells[detail_num, 1].Value = "";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = "";
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = "";
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = "";
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;


                            }
                            #endregion
                        }
                        else if (productName.Trim().ToUpper().Contains("A9") || productName.Trim().ToUpper().Contains("C9")
                            || productName.Trim().ToUpper().Contains("MX") || productName.Trim().ToUpper().Contains("C5")
                            || productName.Trim().ToUpper().Contains("C4")
                            || productName.Trim().ToUpper().Contains("TOLUENE")
                            || productName.ToUpper().Contains("HEAVIES") || productName.ToUpper().Contains("C10")
                            || productName.ToUpper().Contains("RAFFINATE")
                            )
                        {
                            var sDateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                            var sDateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                            DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                            DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);
                            #region Product Price TPX
                            if (model.data_Search.sPriceDetail != null)
                            {
                                detail_num += 2;
                                
                                string tmpDate = tmpDate = "Period: " + dateFrom.ToString("dd MMM yyyy", provider) + " - " + dateTo.ToString("dd MMM yyyy", provider);
                                ws.Cells[detail_num, 1].Value = tmpDate;
                                ws.Cells[detail_num, 1].Style.Font.Size = 12;
                                ws.Cells[detail_num, 1].Style.Font.Bold = true;

                                detail_num++;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));

                                var modelProduct = ws.Cells["A" + detail_num + ":I" + (detail_num + 3)];
                                ws.Cells["A" + detail_num + ":I" + (detail_num + 3)].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                                // Assign borders
                                modelProduct.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                ws.Cells[detail_num, 1].Value = "Reference Prices ($/mt)";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = "ULG95";
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = "KERO";
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = "GASOIL";
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;
                                ws.Cells[detail_num, 5].Value = "NAPHTA";
                                ws.Cells[detail_num, 5].Style.Font.Size = 10;
                                ws.Cells[detail_num, 6].Value = "";
                                ws.Cells[detail_num, 6].Style.Font.Size = 10;
                                ws.Cells[detail_num, 7].Value = "";
                                ws.Cells[detail_num, 7].Style.Font.Size = 10;
                                ws.Cells[detail_num, 8].Value = "";
                                ws.Cells[detail_num, 8].Style.Font.Size = 10;
                                ws.Cells[detail_num, 9].Value = "";
                                ws.Cells[detail_num, 9].Style.Font.Size = 10;
                                ws.Cells[detail_num, 10].Value = "";
                                ws.Cells[detail_num, 10].Style.Font.Size = 10;

                                detail_num++;
                                ws.Cells[detail_num, 1].Value = "Conversion Factor (bbl/mt)";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.USD_TPX_ULG95;
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = model.data_Search.sPriceDetail.USD_TPX_KERO;
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = model.data_Search.sPriceDetail.USD_TPX_GASOIL;
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;
                                ws.Cells[detail_num, 5].Value = model.data_Search.sPriceDetail.USD_TPX_NAPHTA;
                                ws.Cells[detail_num, 5].Style.Font.Size = 10;
                                ws.Cells[detail_num, 6].Value = "";
                                ws.Cells[detail_num, 6].Style.Font.Size = 10;
                                ws.Cells[detail_num, 7].Value = "";
                                ws.Cells[detail_num, 7].Style.Font.Size = 10;
                                ws.Cells[detail_num, 8].Value = "";
                                ws.Cells[detail_num, 8].Style.Font.Size = 10;
                                ws.Cells[detail_num, 9].Value = "";
                                ws.Cells[detail_num, 9].Style.Font.Size = 10;
                                ws.Cells[detail_num, 10].Value = "";
                                ws.Cells[detail_num, 10].Style.Font.Size = 10;

                                detail_num++;
                                ws.Cells[detail_num, 1].Value = "";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = "";
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = "";
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = "";
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;


                            }
                            #endregion
                        }
                        else
                        {
                            #region Product Price TLB / TPX
                            if (model.data_Search.sPriceDetail != null)
                            {
                                detail_num += 2;

                                //string sDate = model.data_Search.sFXDetail[0].d_Date.Substring(6, 2);
                                //string eDate = model.data_Search.sFXDetail[model.data_Search.sFXDetail.Count - 1].d_Date.Substring(6, 2);
                                //string tmpDate = DateTime.ParseExact(model.data_Search.sFXDetail[0].d_Date, "yyyyMMdd", provider).ToString("MMM yyyy", provider);
                                var sDateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                                var sDateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                                string tmpDate = "";
                                tmpDate = "Period: " + dateFrom.ToString("dd MMM yyyy", provider) + " - " + dateTo.ToString("dd MMM yyyy", provider) ;
                                
                                ws.Cells[detail_num, 1].Value = tmpDate;
                                ws.Cells[detail_num, 1].Style.Font.Size = 12;
                                ws.Cells[detail_num, 1].Style.Font.Bold = true;

                                detail_num++;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells["A" + detail_num + ":I" + detail_num].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));

                                var modelProduct = ws.Cells["A" + detail_num + ":I" + (detail_num + 3)];
                                ws.Cells["A" + detail_num + ":I" + (detail_num + 3)].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                                // Assign borders
                                modelProduct.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                modelProduct.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                ws.Cells[detail_num, 1].Value = "Reference Prices ($/mt)";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = "ULG95";
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = "KERO";
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = "GASOIL";
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;
                                ws.Cells[detail_num, 5].Value = "LSFO";
                                ws.Cells[detail_num, 5].Style.Font.Size = 10;
                                ws.Cells[detail_num, 6].Value = "HSFO";
                                ws.Cells[detail_num, 6].Style.Font.Size = 10;
                                ws.Cells[detail_num, 7].Value = "H2";
                                ws.Cells[detail_num, 7].Style.Font.Size = 10;
                                ws.Cells[detail_num, 8].Value = "LPG";
                                ws.Cells[detail_num, 8].Style.Font.Size = 10;
                                ws.Cells[detail_num, 9].Value = "ULG92";
                                ws.Cells[detail_num, 9].Style.Font.Size = 10;
                                ws.Cells[detail_num, 10].Value = "NAPHTA";
                                ws.Cells[detail_num, 10].Style.Font.Size = 10;

                                detail_num++;
                                ws.Cells[detail_num, 1].Value = "Conversion Factor (bbl/mt)";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.USD_ULG95;
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = model.data_Search.sPriceDetail.USD_KERO;
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = model.data_Search.sPriceDetail.USD_GASOIL;
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;
                                ws.Cells[detail_num, 5].Value = model.data_Search.sPriceDetail.USD_LSFO;
                                ws.Cells[detail_num, 5].Style.Font.Size = 10;
                                ws.Cells[detail_num, 6].Value = model.data_Search.sPriceDetail.USD_HSFO;
                                ws.Cells[detail_num, 6].Style.Font.Size = 10;
                                ws.Cells[detail_num, 7].Value = model.data_Search.sPriceDetail.USD_H2;
                                ws.Cells[detail_num, 7].Style.Font.Size = 10;
                                ws.Cells[detail_num, 8].Value = model.data_Search.sPriceDetail.USD_LPG;
                                ws.Cells[detail_num, 8].Style.Font.Size = 10;
                                ws.Cells[detail_num, 9].Value = model.data_Search.sPriceDetail.USD_ULG92;
                                ws.Cells[detail_num, 9].Style.Font.Size = 10;
                                ws.Cells[detail_num, 10].Value = model.data_Search.sPriceDetail.USD_NAPHTA;
                                ws.Cells[detail_num, 10].Style.Font.Size = 10;

                                detail_num++;
                                ws.Cells[detail_num, 1].Value = "";
                                ws.Cells[detail_num, 1].Style.Font.Size = 10;
                                ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.BBL_ULG95;
                                ws.Cells[detail_num, 2].Style.Font.Size = 10;
                                ws.Cells[detail_num, 3].Value = model.data_Search.sPriceDetail.BBL_KERO;
                                ws.Cells[detail_num, 3].Style.Font.Size = 10;
                                ws.Cells[detail_num, 4].Value = model.data_Search.sPriceDetail.BBL_GASOIL;
                                ws.Cells[detail_num, 4].Style.Font.Size = 10;


                            }
                            #endregion
                        }

                        #region Exchange Rate
                        if (model.data_Search.sPriceDetail.EXC_AVERAGE != null)
                        {
                            detail_num += 3;

                            //string sDate = model.data_Search.sFXDetail[0].d_Date.Substring(6, 2);
                            //string eDate = model.data_Search.sFXDetail[model.data_Search.sFXDetail.Count - 1].d_Date.Substring(6, 2);
                            //string tmpDate = DateTime.ParseExact(model.data_Search.sFXDetail[0].d_Date, "yyyyMMdd", provider).ToString("MMM yyyy", provider);
                            var sDateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                            var sDateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                            DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                            DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                            string tmpDate = "";
                            tmpDate = "EXCHANGE RATE (" + dateFrom.ToString("dd MMM yyyy", provider) + "-" + dateTo.ToString("dd MMM yyyy", provider) + " )";
        
                            ws.Cells[detail_num, 1].Value = tmpDate;
                            ws.Cells[detail_num, 1].Style.Font.Size = 12;
                            ws.Cells[detail_num, 1].Style.Font.Bold = true;

                            detail_num++;

                            ws.Cells["A" + detail_num + ":B" + detail_num].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            ws.Cells["A" + detail_num + ":B" + detail_num].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ws.Cells["A" + detail_num + ":B" + detail_num].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 255, 153));

                            var modelExchange = ws.Cells["A" + detail_num + ":B" + (detail_num + 3)];
                            ws.Cells["A" + detail_num + ":B" + (detail_num + 3)].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.Black);
                            // Assign borders
                            modelExchange.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            modelExchange.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            modelExchange.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            modelExchange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                            ws.Cells["A" + detail_num + ":B" + detail_num].Merge = true;
                            ws.Cells[detail_num, 1].Value = "Exchange Rate :";
                            ws.Cells[detail_num, 1].Style.Font.Size = 10;
                            //sPriceDetail.EXC_SELLING

                            detail_num++;
                            ws.Cells[detail_num, 1].Value = "Buying";
                            ws.Cells[detail_num, 1].Style.Font.Size = 10;
                            ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.EXC_BUYING;
                            ws.Cells[detail_num, 2].Style.Font.Size = 10;

                            detail_num++;
                            ws.Cells[detail_num, 1].Value = "Selling";
                            ws.Cells[detail_num, 1].Style.Font.Size = 10;
                            ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.EXC_SELLING;
                            ws.Cells[detail_num, 2].Style.Font.Size = 10;

                            detail_num++;
                            ws.Cells[detail_num, 1].Value = "Average";
                            ws.Cells[detail_num, 1].Style.Font.Size = 10;
                            ws.Cells[detail_num, 2].Value = model.data_Search.sPriceDetail.EXC_AVERAGE;
                            ws.Cells[detail_num, 2].Style.Font.Size = 10;


                        }
                        #endregion

                        package.Save();
                    }

                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
                    Response.AddHeader("content-type", "application/Excel");
                    Response.WriteFile(file_path);
                    Response.End();
                }

            }
            catch(Exception ex)
            {

            }

            FeedstockBookingPriceServiceModel service = new FeedstockBookingPriceServiceModel();
            service.getProductList(ref model);
            return View("Create", model);
        }
    }
}