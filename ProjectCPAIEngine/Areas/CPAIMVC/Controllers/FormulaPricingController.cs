﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using System.Web.Routing;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class FormulaPricingController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Search", "FormulaPricing");
        }

        [HttpGet]
        public ActionResult Search()
        {
            FormulaPricingViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(FormulaPricingViewModel tempModel)
        {

            FormulaPricingViewModel model = initialModel();

            FormulaPricingServiceModel serviceModel = new FormulaPricingServiceModel();
            FormulaPricingViewModel_Search viewModelSearch = new FormulaPricingViewModel_Search();

            viewModelSearch = tempModel.for_Search;
            serviceModel.Search(ref viewModelSearch);
            model.for_Search = viewModelSearch;

            return View(model);
        }

        public static FormulaPricingViewModel SearchQuery(FormulaPricingViewModel FormulaModel)
        {
            FormulaPricingViewModel_Search viewModelSearch = new FormulaPricingViewModel_Search();
            FormulaPricingServiceModel serviceModel = new FormulaPricingServiceModel();

            //FormulaModel.ddl_PriceType = DropdownServiceModel.getPricingType();
            //FormulaModel.ddl_MopsCode = DropdownServiceModel.getMOPSCode();
            //FormulaModel.ddl_PriceStatus = DropdownServiceModel.getPricingStatus();
            //FormulaModel.ddl_PriceStructure = DropdownServiceModel.getPricingStructure();
            //FormulaModel.ddl_Product = DropdownServiceModel.getPricingProduct();
            //FormulaModel.ddl_PriceVariable = DropdownServiceModel.getPricingVariable();

            viewModelSearch = FormulaModel.for_Search;
            serviceModel.Search(ref viewModelSearch);
            FormulaModel.for_Search = viewModelSearch;

            return FormulaModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, FormulaPricingViewModel model)
        {
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            //if (ViewBag.action_create)
            //{
            //    FormulaPricingViewModel model = initialModel();
            //    //if (model.cmp_Detail.CreateType == "" || model.cmp_Detail.CreateType == null)
            //    //{
            //    //    model.cmp_Detail.CreateType = "CPAI";
            //    //    model.cmp_Detail.CompanyCode = "";
            //    //}
            //    return View(model);
            //}
            //else
            //{
            //    return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Company" }));
            //}

            FormulaPricingViewModel model = initialModel();
            return View(model);

        }

        [HttpGet]
        public ActionResult ShowFormulaVariable()
        {
            //return RedirectToAction("ShowFormulaVariable", "FormulaPricing", new RouteValueDictionary(new { f = "FormulaPricing" }));

            FormulaPricingViewModel model = initialModel();
            return View("pFormulaVariable", model);
        }


        [HttpPost]
        public ActionResult Edit(string CompanyCode, FormulaPricingViewModel model)
        {
            //if (ViewBag.action_edit)
            //{
            CompanyServiceModel serviceModel = new CompanyServiceModel();
            ReturnValue rtn = new ReturnValue();
            //model.cmp_Detail.Status = model.cmp_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
            //rtn = serviceModel.Edit(model.cmp_Detail, lbUserName);
            //TempData["ReponseMessage"] = rtn.Message;
            //TempData["ReponseStatus"] = rtn.Status;
            //TempData["CompanyCode"] = CompanyCode;

            FormulaPricingViewModel modelDropdownSet = initialModel();
            model.ddl_Material = DropdownServiceModel.getPricingMaterial();
            model.ddl_PriceStatus = DropdownServiceModel.getPricingStatus();
            model.ddl_PriceStructure = DropdownServiceModel.getPricingStructure();
            model.ddl_PriceVariable = DropdownServiceModel.getPricingVariable();
            model.ddl_PriceType = DropdownServiceModel.getPricingType();
            model.ddl_PriceMOPS = DropdownServiceModel.getPricingMOPS();
            return View("Create", model);
            //}
            //else
            //{
            //    return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Company" }));
            //}

        }

        [HttpGet]
        public ActionResult Edit(string FOVCode)
        {
            //if (ViewBag.action_edit)
            //{
            //    FormulaPricingServiceModel serviceModel = new FormulaPricingServiceModel();

            //    FormulaPricingViewModel model = initialModel();
            //    model.fov_Detail = serviceModel.Get(FOVCode);
            //    TempData["FOVCode"] = FOVCode;

            //    return View("Create", model);
            //}
            //else
            //{
            //    return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Company" }));
            //}
            FormulaPricingServiceModel serviceModel = new FormulaPricingServiceModel();
            FormulaPricingViewModel model = initialModel();
            model.for_Detail = serviceModel.Get(FOVCode);
            TempData["FOVCode"] = FOVCode;

            replaceVariableDesc(ref model);

            return View("Create", model);

        }

        private void replaceVariableDesc(ref FormulaPricingViewModel model)
        {
            if (model.for_Detail.dDetailVariable == null)
                return;

            int vcount = model.for_Detail.dDetailVariable.Count;
            if (vcount == 0)
                return;

            int mcount = model.ddl_PriceVariable.Count;
            if (mcount == 0)
                return;
            
            for (int i = 0; i < vcount; i++)
            {
                var variableValue = model.for_Detail.dDetailVariable[i].VariableCode;
                var variableDesc = variableValue;
                string variableDescKey = "";
                variableDescKey = variableValue.Replace(' ', ';');

                for (int j = 0; j < mcount; j++)
                {
                    variableDesc = variableDesc.Replace(model.ddl_PriceVariable[j].Value, model.ddl_PriceVariable[j].Text);
                    variableDescKey = variableDescKey.Replace(model.ddl_PriceVariable[j].Value, model.ddl_PriceVariable[j].Text);
                }

                model.for_Detail.dDetailVariable[i].VariableDesc = variableDesc;
                model.for_Detail.dDetailVariable[i].VariableDescKey = variableDescKey;
            }


        }

        [HttpGet]
        public ActionResult View(string FOVCode)
        {
            //if (ViewBag.action_view)
            //{
            FormulaPricingServiceModel serviceModel = new FormulaPricingServiceModel();

            FormulaPricingViewModel model = initialModel();
            model.for_Detail = serviceModel.Get(FOVCode);
            TempData["FOVCode"] = FOVCode;



            return View("Create", model);
            //}
            //else
            //{
            //    return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Company" }));
            //}

        }

        public FormulaPricingViewModel initialModel()
        {
            FormulaPricingServiceModel serviceModel = new FormulaPricingServiceModel();
            FormulaPricingViewModel model = new FormulaPricingViewModel();
            model.for_Search = new FormulaPricingViewModel_Search();
            model.for_Search.sSearchData = new List<FormulaPricingViewModel_SearchData>();

            model.for_Detail = new FormulaPricingViewModel_Detail();
            model.ddl_Material = DropdownServiceModel.getPricingMaterial();
            model.ddl_PriceStatus = DropdownServiceModel.getPricingStatus();
            model.ddl_PriceStructure = DropdownServiceModel.getPricingStructure();
            model.ddl_PriceVariable = DropdownServiceModel.getPricingVariable();
            model.ddl_PriceType = DropdownServiceModel.getPricingType();
            model.ddl_PriceMOPS = DropdownServiceModel.getPricingMOPS();

            return model;
        }

    }
}