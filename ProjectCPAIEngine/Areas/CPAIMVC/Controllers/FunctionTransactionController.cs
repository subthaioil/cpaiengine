﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class FunctionTransactionController : BaseController
    {
        FunctionTransactionDAL FnTranDAL = new FunctionTransactionDAL();
        ShareFn _FN = new ShareFn();
        private List<FUNCTIONS> LstFNName
        {
            get { return (Session["LstFNName"] != null) ? (List<FUNCTIONS>)Session["LstFNName"] : new List<FUNCTIONS>(); }
            set { Session["LstFNName"] = value; }
        }
        // GET: CPAIMVC/FunctionTransaction
        public ActionResult Index()
        {
            FnTranViewModel _FNView = new ViewModels.FnTranViewModel();
            _FNView.FunctionTransaction = new List<FN_TRANSACTION>();
            LstFNName = FnTranDAL.GetFNNamelst();
            initializeDropDownListFixture(ref _FNView);
            return View(_FNView);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(FormCollection form, FnTranViewModel model)
        {
            initializeDropDownListFixture(ref model);
            Dictionary<string, string> _Dic = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(model.CAppID)) _Dic.Add("AppID".ToUpper(), model.CAppID);
            if (!string.IsNullOrEmpty(model.CReqTranID)) _Dic.Add("ReqTranID".ToUpper(), model.CReqTranID);
            if (!string.IsNullOrEmpty(model.CTranID)) _Dic.Add("TranID".ToUpper(), model.CTranID);
            if (!string.IsNullOrEmpty(model.CFromDate)) _Dic.Add("FromDate".ToUpper(), model.CFromDate);
            if (!string.IsNullOrEmpty(model.CToDate)) _Dic.Add("ToDate".ToUpper(),model.CToDate);
            if (!string.IsNullOrEmpty(model.CStatus)) _Dic.Add("Status".ToUpper(), model.CStatus);
            if (!string.IsNullOrEmpty(model.CFunctionID)) _Dic.Add("FunctionID".ToUpper(), model.CFunctionID);

            if (!string.IsNullOrEmpty(model.CIndex1)) _Dic.Add("INDEX1".ToUpper(), model.CIndex1);
            if (!string.IsNullOrEmpty(model.CIndex2)) _Dic.Add("INDEX2".ToUpper(), model.CIndex2);
            if (!string.IsNullOrEmpty(model.CIndex3)) _Dic.Add("INDEX3".ToUpper(), model.CIndex3);
            if (!string.IsNullOrEmpty(model.CIndex4)) _Dic.Add("INDEX4".ToUpper(), model.CIndex4);
            if (!string.IsNullOrEmpty(model.CIndex5)) _Dic.Add("INDEX5".ToUpper(), model.CIndex5);
            if (!string.IsNullOrEmpty(model.CIndex6)) _Dic.Add("INDEX6".ToUpper(), model.CIndex6);
            if (!string.IsNullOrEmpty(model.CIndex7)) _Dic.Add("INDEX7".ToUpper(), model.CIndex7);
            if (!string.IsNullOrEmpty(model.CIndex8)) _Dic.Add("INDEX8".ToUpper(), model.CIndex8);
            if (!string.IsNullOrEmpty(model.CIndex9)) _Dic.Add("INDEX9".ToUpper(), model.CIndex9);
            if (!string.IsNullOrEmpty(model.CIndex10)) _Dic.Add("INDEX10".ToUpper(), model.CIndex10);
            if (!string.IsNullOrEmpty(model.CIndex11)) _Dic.Add("INDEX11".ToUpper(), model.CIndex11);
            if (!string.IsNullOrEmpty(model.CIndex12)) _Dic.Add("INDEX12".ToUpper(), model.CIndex12);
            if (!string.IsNullOrEmpty(model.CIndex13)) _Dic.Add("INDEX13".ToUpper(), model.CIndex13);
            if (!string.IsNullOrEmpty(model.CIndex14)) _Dic.Add("INDEX14".ToUpper(), model.CIndex14);
            if (!string.IsNullOrEmpty(model.CIndex15)) _Dic.Add("INDEX15".ToUpper(), model.CIndex15);
            if (!string.IsNullOrEmpty(model.CIndex16)) _Dic.Add("INDEX16".ToUpper(), model.CIndex16);
            if (!string.IsNullOrEmpty(model.CIndex17)) _Dic.Add("INDEX17".ToUpper(), model.CIndex17);
            if (!string.IsNullOrEmpty(model.CIndex18)) _Dic.Add("INDEX18".ToUpper(), model.CIndex18);
            if (!string.IsNullOrEmpty(model.CIndex19)) _Dic.Add("INDEX19".ToUpper(), model.CIndex19);
            if (!string.IsNullOrEmpty(model.CIndex20)) _Dic.Add("INDEX20".ToUpper(), model.CIndex20);
            model.FunctionTransaction = MapFunctiontoFN(FnTranDAL.GetFunctionTransaction(_Dic).Take(1000).ToList());
            return View("Index", model);
        }

        private string GetFNName(string FnID)
        {
            string fnName = "";
            if (LstFNName != null)
            {
                fnName = LstFNName.Where(x => x.FNC_ROW_ID == FnID).FirstOrDefault().FNC_DESCRIPTION;
            }
            return fnName;
        }

        private List<FN_TRANSACTION> MapFunctiontoFN(List<FUNCTION_TRANSACTION> lstFunctionTran)
        {
            List<FN_TRANSACTION> lstFNTran = new List<ViewModels.FN_TRANSACTION>();
            foreach (var _item in lstFunctionTran)
            {
                FN_TRANSACTION _Fn = new FN_TRANSACTION();
                _Fn.FTX_ACCUM_RETRY = _item.FTX_ACCUM_RETRY;
                _Fn.FTX_APP_ID = _item.FTX_APP_ID;
                _Fn.FTX_CREATED = _item.FTX_CREATED.ToString("dd/MM/yyyy");
                _Fn.FTX_CREATED_BY = _item.FTX_CREATED_BY;
                _Fn.FTX_CURRENT_CODE = _item.FTX_CURRENT_CODE;
                _Fn.FTX_CURRENT_DESC = _item.FTX_CURRENT_DESC;
                _Fn.FTX_CURRENT_NAME_SPACE = _item.FTX_CURRENT_NAME_SPACE;
                _Fn.FTX_CURRENT_STATE = _item.FTX_CURRENT_STATE;
                _Fn.FTX_FK_FUNCTION = _item.FTX_FK_FUNCTION;
                _Fn.FTX_FK_FUNCTIONNAME = GetFNName(_item.FTX_FK_FUNCTION);
                _Fn.FTX_FWD_RESP_CODE_FLAG = _item.FTX_FWD_RESP_CODE_FLAG;
                _Fn.FTX_INDEX1 = _item.FTX_INDEX1;
                _Fn.FTX_INDEX10 = _item.FTX_INDEX10;
                _Fn.FTX_INDEX11 = _item.FTX_INDEX11;
                _Fn.FTX_INDEX12 = _item.FTX_INDEX12;
                _Fn.FTX_INDEX13 = _item.FTX_INDEX13;
                _Fn.FTX_INDEX14 = _item.FTX_INDEX14;
                _Fn.FTX_INDEX15 = _item.FTX_INDEX15;
                _Fn.FTX_INDEX16 = _item.FTX_INDEX16;
                _Fn.FTX_INDEX17 = _item.FTX_INDEX17;
                _Fn.FTX_INDEX18 = _item.FTX_INDEX18;
                _Fn.FTX_INDEX19 = _item.FTX_INDEX19;
                _Fn.FTX_INDEX2 = _item.FTX_INDEX2;
                _Fn.FTX_INDEX20 = _item.FTX_INDEX20;
                _Fn.FTX_INDEX3 = _item.FTX_INDEX3;
                _Fn.FTX_INDEX4 = _item.FTX_INDEX4;
                _Fn.FTX_INDEX5 = _item.FTX_INDEX5;
                _Fn.FTX_INDEX6 = _item.FTX_INDEX6;
                _Fn.FTX_INDEX7 = _item.FTX_INDEX7;
                _Fn.FTX_INDEX8 = _item.FTX_INDEX8;
                _Fn.FTX_INDEX9 = _item.FTX_INDEX9;
                _Fn.FTX_NEXT_STATE = _item.FTX_NEXT_STATE;
                _Fn.FTX_PARENT_ACT_ID = _item.FTX_PARENT_ACT_ID;
                _Fn.FTX_PARENT_TRX_ID = _item.FTX_PARENT_TRX_ID;
                _Fn.FTX_REQUEST_DATE = _item.FTX_REQUEST_DATE;
                _Fn.FTX_REQ_TRANS = _item.FTX_REQ_TRANS;
                _Fn.FTX_RESPONSE_DATE = _item.FTX_RESPONSE_DATE;
                _Fn.FTX_RETRY_COUNT = _item.FTX_RETRY_COUNT;
                _Fn.FTX_RETURN_CODE = _item.FTX_RETURN_CODE;
                _Fn.FTX_RETURN_DESC = _item.FTX_RETURN_DESC;
                _Fn.FTX_RETURN_MESSAGE = _item.FTX_RETURN_MESSAGE;
                _Fn.FTX_RETURN_NAME_SPACE = _item.FTX_RETURN_NAME_SPACE;
                _Fn.FTX_RETURN_STATUS = _item.FTX_RETURN_STATUS;
                _Fn.FTX_ROW_ID = _item.FTX_ROW_ID;
                _Fn.FTX_STATE_ACTION = _item.FTX_STATE_ACTION;
                _Fn.FTX_TRANS_ID = _item.FTX_TRANS_ID;
                _Fn.FTX_UPDATED = _item.FTX_UPDATED;
                _Fn.FTX_UPDATED_BY = _item.FTX_UPDATED_BY;
                _Fn.FTX_WAKEUP = _item.FTX_WAKEUP;
                lstFNTran.Add(_Fn);
            }
            return lstFNTran;
        }

        #region------------------Page Event----------------------------
        public ActionResult GetDetailFnTran(string TranID)
        {
            FnTranDetail ViewModel = new FnTranDetail();

            try
            {
                ViewModel.ExtenTran = new List<ExthenVal>();
                ViewModel.ActivityTran = new List<FN_Activity>();
                ViewModel.FnTranItemDetail = new FN_TRANSACTION();
                ViewModel.TranLog = new List<FN_LOG>();
                ViewModel.EventMSG = "";
                FUNCTION_TRANSACTION FNTran = FnTranDAL.GetFnTranDetail(TranID);
                if (FNTran != null)
                {
                    var lstExthenTran = FnTranDAL.GetFnTranExthen(FNTran.FTX_ROW_ID);
                    foreach (var _item in lstExthenTran)
                    {
                        ViewModel.ExtenTran.Add(new ViewModels.ExthenVal() { ExtenParam = _item.ETX_KEY, ExtenVal = _item.ETX_VALUE });
                    }

                    var lstActivity = FnTranDAL.GetFnTranActivity(FNTran.FTX_ROW_ID);
                    foreach (var _item in lstActivity)
                    {
                        FN_Activity fnAct = new ViewModels.FN_Activity();
                        fnAct.FAC_CLASS = _item.FAC_CLASS;
                        fnAct.FAC_CREATED = _item.FAC_CREATED.ToString("dd/MM/yyyy HH:mm:ss");
                        fnAct.FAC_CREATED_BY = _item.FAC_CREATED_BY;
                        fnAct.FAC_FK_FUNC_TRX = _item.FAC_FK_FUNC_TRX;
                        fnAct.FAC_NAME_SPACE = _item.FAC_NAME_SPACE;
                        fnAct.FAC_NO_REPORT_FLAG = _item.FAC_NO_REPORT_FLAG;
                        fnAct.FAC_PARALLEL_FLAG = _item.FAC_PARALLEL_FLAG;
                        fnAct.FAC_RECEIVE_DATE = _item.FAC_RECEIVE_DATE.ToString("dd/MM/yyyy HH:mm:ss");
                        fnAct.FAC_RETURN_CODE = _item.FAC_RETURN_CODE;
                        fnAct.FAC_RETURN_DESC = _item.FAC_RETURN_DESC;
                        fnAct.FAC_ROW_ID = _item.FAC_ROW_ID;
                        fnAct.FAC_SEND_DATE = _item.FAC_SEND_DATE.ToString("dd/MM/yyyy HH:mm:ss");
                        fnAct.FAC_STATE_NAME = _item.FAC_STATE_NAME;
                        fnAct.FAC_UPDATED = _item.FAC_UPDATED.ToString("dd/MM/yyyy HH:mm:ss");
                        fnAct.FAC_UPDATED_BY = _item.FAC_UPDATED_BY;
                        ViewModel.ActivityTran.Add(fnAct);
                    }
                    var lstLog = FnTranDAL.GetFnTranLog(FNTran.FTX_ROW_ID,FNTran.FTX_TRANS_ID);
                    foreach (var _item in lstLog)
                    {
                        FN_LOG fnLog = new FN_LOG();
                        fnLog.TXL_CREATED = _item.TXL_CREATED.ToString("dd/MM/yyyy HH:mm:ss");
                        fnLog.TXL_CREATED_BY = _item.TXL_CREATED_BY;
                        fnLog.TXL_DATA = _item.TXL_DATA;
                        fnLog.TXL_DATA_DATE = _item.TXL_DATA_DATE.ToString("dd/MM/yyyy HH:mm:ss");
                        fnLog.TXL_DATA_TYPE = _item.TXL_DATA_TYPE;
                        fnLog.TXL_ENCRYPT_FLAG = _item.TXL_ENCRYPT_FLAG;
                        fnLog.TXL_GROUP = _item.TXL_GROUP;
                        fnLog.TXL_ROW_ID = _item.TXL_ROW_ID;
                        fnLog.TXL_TABLE = _item.TXL_TABLE;
                        fnLog.TXL_TABLE_ROW_ID = _item.TXL_TABLE_ROW_ID;
                        fnLog.TXL_UPDATED = _item.TXL_UPDATED.ToString("dd/MM/yyyy HH:mm:ss");
                        fnLog.TXL_UPDATED_BY = _item.TXL_UPDATED_BY;
                        ViewModel.TranLog.Add(fnLog);
                    }

                }
                if (FNTran != null)
                {
                    var lst = new List<FUNCTION_TRANSACTION>();
                    lst.Add(FNTran);
                    ViewModel.FnTranItemDetail = MapFunctiontoFN(lst)[0];
                    
                }
            }
            catch (Exception ex)
            {
                ViewModel.EventMSG = ex.Message;
            }
            return Json(ViewModel, JsonRequestBehavior.AllowGet);

        }
        #endregion-----------------------------------------------------

        #region------------------Get Master Data-----------------------
        private void initializeDropDownListFixture(ref FnTranViewModel _FNView)
        {
            _FNView.lstlstStatus = getlstStatus();
            _FNView.lstFunction = getlstFunction();
        }

        private List<SelectListItem> getlstStatus()
        {
            List<SelectListItem> lstItem = new List<SelectListItem>();
            var lstStatus = FnTranDAL.GetStatuslst();
            lstItem.Add(new SelectListItem { Text = "---Select---", Value = "" });
            foreach (var _item in lstStatus)
            {
                lstItem.Add(new SelectListItem { Text = _item, Value = _item });
            }
            return lstItem;
        }

        private List<SelectListItem> getlstFunction()
        {
            List<SelectListItem> lstItem = new List<SelectListItem>();
            lstItem.Add(new SelectListItem { Text = "---Select---", Value = "" });
            foreach (var _item in LstFNName)
            {
                lstItem.Add(new SelectListItem { Text=_item.FNC_DESCRIPTION,Value=_item.FNC_ROW_ID});
            }
            return lstItem;
        }

        #endregion---------------------------------------------------------

        
    }
}