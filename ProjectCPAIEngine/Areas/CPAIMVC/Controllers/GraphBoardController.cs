﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.CPAI;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.Model;
using com.pttict.engine.utility;
using OfficeOpenXml;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System.Globalization;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class GraphBoardController : BaseController
    {
        ShareFn _FN = new ShareFn();
        DALGraphBoard _dalGraphBoard = new DALGraphBoard();
        CPAI_TCE_WS_DAL _dalTCEWS = new CPAI_TCE_WS_DAL();
        string returnPage =  "/CPAIMVC/graphboard";
        
        public string UserTypeSS
        {
            get
            {
                string UserTypeSS = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["UserTypeSS"] != null ? Session["UserTypeSS"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
                return UserTypeSS;
            }
            set
            {
                Session["UserTypeSS"] = value;
            }
        }
        private List<Transaction> SSVoyCharterIn
        {
            get
            {
                if (Session["SSVoyCharterIn"] == null) return new List<Transaction>();
                else return (List<Transaction>)Session["SSVoyCharterIn"];
            }
            set
            {
                Session["SSVoyCharterIn"] = value;
            }
        }
        private List<CHI_DATA> SSCHIData
        {
            get
            {
                if (Session["SSCHIData"] == null) return null;
                else return (List<CHI_DATA>)Session["SSCHIData"];
            }
            set
            {
                Session["SSCHIData"] = value;
            }
        }
        private bool SSEditMode
        {
            get
            {
                if (Session["SSEditMode"] == null) return false;
                else return (bool)Session["SSEditMode"];
            }
            set
            {
                Session["SSEditMode"] = value;
            }
        }
        public ActionResult Index()
        {
            ViewModels.GraphBoardViewModel _Vmodel = new ViewModels.GraphBoardViewModel();
            _Vmodel.lstTD = GetMasterTD();
            _Vmodel.lstSTATUS = GetMasterSTATUS();
            //_Vmodel.lstVessel = DropdownServiceModel.getVehicle(true, "", (UserTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? MCCT_CHI_CMMT : MCCT_CHI_CMCS);
            _Vmodel.lstVoyNo = new List<SelectListItem>();
            _Vmodel.lstCharterIn = new List<SelectListItem>();
            List<TDData> _trData = new List<TDData>();

            // Get current month (Past 1 month, Next 1 month)
            var currentDate = _dalTCEWS.GetLastFixtureDate(); //DateTime.Now;
            var _from = new DateTime(currentDate.AddMonths(-3).Year, currentDate.AddMonths(-3).Month, 1);               // Start date Past 3 month
            var _to = currentDate; //new DateTime(currentDate.AddMonths(1).Year, currentDate.AddMonths(1).Month, 1).AddDays(-1);                                 // End date current month

            var startDateFormat = ConvertDateTimeToDateStringFormat(_from, "dd/MM/yyyy");
            var endDateFormat = ConvertDateTimeToDateStringFormat(_to, "dd/MM/yyyy");
            _Vmodel.datePurchaseField = startDateFormat + " to " + endDateFormat;
            List<string> lstTD = new List<string>();
            lstTD.Add("TD2");
            lstTD.Add("TD3");
            _Vmodel.selectTD = "TD2,TD3";
            SSVoyCharterIn = GetTransaction(_from, _to, null, lstTD, null, null);
            //MathRoundTDData(ref _trData);
            _Vmodel.GraphDate = MakeGraph(_from, _to, null, lstTD, null, ref _trData);
            _Vmodel.lstCharterIn = GetPurchCharterIn(SSVoyCharterIn);
            _Vmodel.lstVessel = GetVesselCharterIn(SSVoyCharterIn);
            _Vmodel.MinYAxis = getMinYAxis(_Vmodel.GraphDate);
            //_Vmodel.TDData = _trData.OrderBy(x => x.DateOrder).ToList();            
            _Vmodel.TDData = MakeTDData(_trData, null);
            _Vmodel.TotalAvg = getTotalAVG(_trData, null);
            _Vmodel.TotalSum = getTotalSUM(_trData, null);
            return View(_Vmodel);
        }
        public ActionResult AddFixture()
        {
            CHI_DATA_DAL chiDataDAL = new CHI_DATA_DAL();
            FixtureViewModel _fixViewModel = new FixtureViewModel();

            string chiID = "";
            string chiNO = "";
            
            if (Request.QueryString["transaction_id"] != null)
            {
                _fixViewModel.TceDetail = new TceDetail();
                _fixViewModel.Action = (Request.QueryString["status"] != null) ? Request.QueryString["status"].Decrypt() : "";
                SSEditMode = false;
                LoadDataFixture(Request.QueryString["transaction_id"].Decrypt(), ref _fixViewModel);
                initializeDropDownListFixture(ref _fixViewModel);
                _fixViewModel.TceDetail.voy_no = (Request.QueryString["purchase_no"]!=null)?Request.QueryString["purchase_no"].Decrypt():"";
                _fixViewModel.chi_data_no = chiDataDAL.GetCHINOByID(_fixViewModel.TceDetail.chi_data_id);
                _fixViewModel.FixtureStatus = _fixViewModel.Action =="CANCEL"?"CANCEL": _fixViewModel.Action;
                return View(_fixViewModel);
            }
            else if (Request.QueryString["CharterinTranID"] != null)
            {
                chiID = Request.QueryString["CharterinTranID"].Decrypt();
                string transID = TceServiceModel.getTransactionIDByCherterinID(chiID);
                FunctionTransactionDAL fnDAL = new FunctionTransactionDAL();
                string reqTransID = fnDAL.GetReqTransIDByTransactionId(transID);
                if(string.IsNullOrEmpty(reqTransID)==false)
                    Session["tcews_reqID"] = reqTransID.Encrypt();

                if (string.IsNullOrEmpty(transID))
                {
                    //chiNO = chiDataDAL.GetCHINOByID(chiID);
                    goto AddTce;
                }

                SSEditMode = false;
                _fixViewModel.TceDetail = new TceDetail();
                _fixViewModel.Action = "APPROVED";
                LoadDataFixture(transID, ref _fixViewModel);
                _fixViewModel.TceDetail.voy_no = _fixViewModel.TceDetail.voy_no;
                _fixViewModel.chi_data_no = chiDataDAL.GetCHINOByID(_fixViewModel.TceDetail.chi_data_id);
                _fixViewModel.FixtureStatus = _fixViewModel.Action == "CANCEL" ? "CANCEL" : _fixViewModel.Action;

                initializeDropDownListFixture(ref _fixViewModel);

                return View(_fixViewModel);
            }
           
            AddTce:

            SSCHIData = null;
            SSEditMode = true;
            _fixViewModel.Action = "ADD";
            _fixViewModel.TceDetail = new TceDetail();
            _fixViewModel.TceDetail.ship_name = "";
            _fixViewModel.TceDetail.ship_broker = "";
            _fixViewModel.TceDetail.date_fixture = ""; // DateTime.Now.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //_fixViewModel.TceDetail.top_fixture_value = "64";
            _fixViewModel.PageMode = true;
            _fixViewModel.FixtureStatus = "NEW";

            if (Request.QueryString["OnSubDate"] != null)
            {
                string _OnSubDate = Request.QueryString["OnSubDate"] != null ? HttpUtility.UrlDecode(Request.QueryString["OnSubDate"]) : "";
                _fixViewModel.sOnSubjectDate = _OnSubDate;
                initializeDropDownListFixture(ref _fixViewModel, _OnSubDate);
                return View("AddFixture", _fixViewModel);
            }
            else
            {
                if(!string.IsNullOrEmpty(chiID))
                {
                    SSCHIData = chiDataDAL.GetCHIDataStatusApprove(SSEditMode);
                    _fixViewModel.TceDetail = getChiDetail(chiID);
                }
                initializeDropDownListFixture(ref _fixViewModel);
                return View(_fixViewModel);
            }
        }
        public ActionResult ApproveAddFixture()
        {
            //ForTest
            //http://localhost:50131/CPAIMVC/GraphBoard/ApproveAddFixture?token=5db58cca9fc347378ae34085bd118641

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }

        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string strStatus = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        urlPage = "GraphBoard/AddFixture";
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                                strStatus = itemFunc.FTX_INDEX3;
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;
                        //"?transaction_id=" + _item.transaction_id.Encrypt() + "&Tran_Req_ID=" + _item.req_transaction_id.Encrypt() + "&purchase_no=" + _item.purchase_no.Encrypt() + "&status=" + _item.status.Encrypt();
                        path = "~/CPAIMVC/" + urlPage + "?transaction_id=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&purchase_no=" + strPurNo.Encrypt() + "&status=" + strStatus.Encrypt();
                    }
                }
            }
            return path;
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        //public ActionResult AddFixtureSearch(string OnSubDate = "")
        //{
        //    CHI_DATA_DAL chiDataDAL = new CHI_DATA_DAL();
        //    FixtureViewModel _fixViewModel = new FixtureViewModel();


        //    SSCHIData = null;
        //    SSEditMode = true;
        //    _fixViewModel.Action = "ADD";
        //    _fixViewModel.TceDetail = new TceDetail();
        //    _fixViewModel.TceDetail.ship_name = "";
        //    _fixViewModel.TceDetail.ship_broker = "";
        //    _fixViewModel.TceDetail.date_fixture = ""; // DateTime.Now.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        //    //_fixViewModel.TceDetail.top_fixture_value = "64";
        //    _fixViewModel.PageMode = true;

        //    string _OnSubDate = Request.QueryString["OnSubDate"] != null ? HttpUtility.UrlDecode(Request.QueryString["OnSubDate"]) : "";
        //    _fixViewModel.sOnSubjectDate = _OnSubDate;

        //    if (!string.IsNullOrEmpty(_OnSubDate))
        //    {
        //        initializeDropDownListFixture(ref _fixViewModel, OnSubDate);
        //    }
        //    else
        //    {
        //        initializeDropDownListFixture(ref _fixViewModel);
        //    }


        //    return View("AddFixture", _fixViewModel);

        //}

        public List<ButtonAction> ButtonListAddFixture
        {
            get
            {
                if (Session["ButtonListAddFixture"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListAddFixture"];
            }
            set { Session["ButtonListAddFixture"] = value; }
        }
        private void initializeDropDownListFixture(ref FixtureViewModel _fixViewModel)
        {
            _fixViewModel.lstVoyNo = getVoyForFixture();
            _fixViewModel.lstShipName = DropdownServiceModel.getVehicle((UserTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIVESMT" : "CHIVESCS", true);
            _fixViewModel.lstShipBroker = DropdownServiceModel.getVendorFreight((UserTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIBROMT" : "CHIBROCS", true);
            _fixViewModel.lstTD = GetMasterTD();
        }
        private void initializeDropDownListFixture(ref FixtureViewModel _fixViewModel, string _OnSubDate)
        {
            _fixViewModel.lstVoyNo = getVoyForFixture(_OnSubDate);
            _fixViewModel.lstShipName = DropdownServiceModel.getVehicle((UserTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIVESMT" : "CHIVESCS", true);
            _fixViewModel.lstShipBroker = DropdownServiceModel.getVendorFreight((UserTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIBROMT" : "CHIBROCS", true);
            _fixViewModel.lstTD = GetMasterTD();
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, FixtureViewModel model)
        {
            UpdateModel(form, model);
            initializeDropDownListFixture(ref model);
            SaveData(DocumentActionStatus.Draft, ref model, form["hdfNoteAction"]);
            return View("AddFixture", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, FixtureViewModel model)
        {
            UpdateModel(form, model);
            initializeDropDownListFixture(ref model);
            SaveData(DocumentActionStatus.Submit, ref model, form["hdfNoteAction"]);
            return View("AddFixture", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form, FixtureViewModel model)
        {
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, FixtureViewModel model)
        {            
            if (ButtonListAddFixture != null)
            {
                var _lstButton = ButtonListAddFixture.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    string note = form["hdfNoteAction"];
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] != null ? Request.QueryString["Tran_Req_ID"].ToString() : Session["tcews_reqID"] != null ? Session["tcews_reqID"].ToString() : "";
                    var json = new JavaScriptSerializer().Serialize(new TceRootObject() { tce_detail = MakeModelTosave(model.TceDetail) });
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    if (resData != null && resData.result_code == "1")
                    {
                        string tranID = Request.QueryString["transaction_id"];
                        string reqID = TranReqID != null ? TranReqID : resData.req_transaction_id;
                        string currUrl = string.Format("/CPAIMVC/GraphBoard/AddFixture?transaction_id={0}&Tran_Req_ID={1}", tranID, reqID);

                        if (_lstButton[0].name == ConstantPrm.ACTION.SAVE_DRAFT)
                        {
                            TempData["res_message"] = resData.response_message;
                            TempData["returnPage"] = (ConstantPrm.AppName!="")? "/"+ConstantPrm.AppName + currUrl: currUrl;
                        }
                        else
                        {
                            TempData["res_message"] = resData.response_message;
                            TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
                        }
                    }
                    else
                    {
                        TempData["res_message"] = resData.response_message;
                        //TempData["returnPage"] = null;
                    }

                    Session.Remove("tcews_reqID");
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
                //TempData["returnPage"] = null;
            }

            initializeDropDownListFixture(ref model);

            return View("AddFixture", model);
        }

        #region-------------LoadMaster--------------------

        private List<SelectListItem> getVoyForFixture(string _OnSubDate = "")
        {
            CHI_DATA_DAL chiDataDAL = new CHI_DATA_DAL();
            List<SelectListItem> selectList = new List<SelectListItem>();
             SSCHIData = chiDataDAL.GetCHIDataStatusApprove(SSEditMode);
            
            if (!String.IsNullOrEmpty(_OnSubDate) && SSCHIData.Count > 0)
            {
                DateTime? _from = null; DateTime? _to = null;
                string[] Delivaeryrange = _OnSubDate.SplitWord(" to ");
                if (Delivaeryrange.Length >= 2)
                {
                    _from = _FN.ConvertDateFormatBackFormat(Delivaeryrange[0]);
                    _to = _FN.ConvertDateFormatBackFormat(Delivaeryrange[1]);
                }
                else
                {
                    _to = null;
                    _from = null;

                    // Get current month (Past 1 month, Next 1 month)
                    var currentDate = _dalTCEWS.GetLastFixtureDate(); //DateTime.Now;
                    if (_from == null) _from = new DateTime(currentDate.AddMonths(-3).Year, currentDate.AddMonths(-3).Month, 1);               // Start date Past 3 month
                    if (_to == null) _to = currentDate;

                }

                var filter = SSCHIData.Where(p => (p.IDA_DATE_PURCHASE >= _from && p.IDA_DATE_PURCHASE <= _to));
                if(filter != null)
                    SSCHIData = filter.ToList();
            }
            selectList.Add(new SelectListItem { Text = "Please select", Value = "" });
            for (int i = 0; i < SSCHIData.Count; i++)
            {
                var vessel_name = VehicleServiceModel.GetName(SSCHIData[i].IDA_P_FK_VESSEL_NAME);
                selectList.Add(new SelectListItem { Text = SSCHIData[i].IDA_PURCHASE_NO + " : " + vessel_name, Value = SSCHIData[i].IDA_ROW_ID });
            }
            return selectList;
        }

        private List<SelectListItem> getBroker()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(PROJECT_NAME_SPACE,BROKER_TYPE, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            selectList.Add(new SelectListItem { Text = "Please select", Value = "" });
            for (int i = 0; i < mt_vendor.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = mt_vendor[i].VND_NAME1, Value = mt_vendor[i].VND_ACC_NUM_VENDOR });
            }
            return selectList;
        }

        private List<SelectListItem> GetMasterTD()
        {
            List<SelectListItem> lstTD = new List<SelectListItem>();
            List<string> lstdalTD = _dalGraphBoard.GetTD();
            foreach (string _item in lstdalTD)
            {
                lstTD.Add(new SelectListItem() { Text = _item, Value = _item });
            }

            return lstTD;
        }

        private List<SelectListItem> GetMasterSTATUS()
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            List<string> lstdal = _dalGraphBoard.GetSTATUS();
            foreach (string _item in lstdal)
            {
                if(_item == "SUBMIT")
                {
                    lst.Add(new SelectListItem() { Text = "APPROVED", Value = _item });
                }
                else
                {
                    lst.Add(new SelectListItem() { Text = _item, Value = _item });
                }
                
            }

            return lst;
        }

        //private List<SelectListItem> GetVoyCharterIn(DateTime _from, DateTime _To,List<string> lstVessel, string SelectItem = "")
        //{

        //    List<SelectListItem> lst = new List<SelectListItem>();
        //    SSVoyCharterIn = GetTransaction(_from, _To,lstVessel);           
        //    if ( SSVoyCharterIn != null)
        //    {
        //        foreach (var item in SSVoyCharterIn.Where(x => x.status.ToUpper() == ConstantPrm.ACTION.SUBMIT).OrderBy(x => x.purchase_no).ToList())
        //        {
        //            lst.Add(new SelectListItem() { Text=item.purchase_no,Value=item.purchase_no});
        //        }
        //    }

        //    return lst;
        //}

        private List<SelectListItem> GetPurchCharterIn(DateTime _from, DateTime _To, List<string> lstVessel, string SelectItem = "")
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            SSVoyCharterIn = GetTransaction(_from, _To, lstVessel, null, null, null);
            if (SSVoyCharterIn != null)
            {
                foreach (var item in SSVoyCharterIn.Where(x => x.status.ToUpper() == "APPROVED").OrderByDescending(x => x.charter_in).ToList())
                {
                    if (!string.IsNullOrEmpty(item.charter_in) && lst.Where(x => x.Value == item.charter_in).ToArray().Length<=0)
                    {
                        lst.Add(new SelectListItem() { Text = item.charter_in, Value = item.charter_in });
                    }
                }
            }

            return lst;
        }

        private List<SelectListItem> GetPurchCharterIn(List<Transaction> lstTransaction)
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            
            if (lstTransaction != null)
            {
                foreach (var item in lstTransaction.Where(x => x.status.ToUpper() == "APPROVED").OrderByDescending(x => x.charter_in).ToList())
                {
                    if (!string.IsNullOrEmpty(item.charter_in) && lst.Where(x => x.Value == item.charter_in).ToArray().Length <= 0)
                    {
                        lst.Add(new SelectListItem() { Text = item.charter_in, Value = item.charter_in });
                    }
                }
            }

            return lst;
        }

        private List<SelectListItem> GetVesselCharterIn(List<Transaction> lstTransaction)
        {

            List<SelectListItem> lst = new List<SelectListItem>();

            if (lstTransaction != null)
            {
                foreach (var item in lstTransaction.Where(x => x.status.ToUpper() == "APPROVED").OrderBy(x => x.vessel).ToList())
                {
                    if (!string.IsNullOrEmpty(item.vessel_id) && lst.Where(x => x.Value == item.vessel_id).ToArray().Length <= 0)
                    {
                        lst.Add(new SelectListItem() { Text = item.vessel, Value = item.vessel_id });
                    }
                }
            }

            return lst;
        }

        #endregion----------------------------------------

        #region-------------PageEvent---------------------

        //public ActionResult getVoyCharterInEvt(string DateSearch,List<string> ShipName)
        //{
        //    DateTime? _from = null; DateTime? _to = null;
        //    string[] Delivaeryrange = DateSearch.SplitWord(" to ");
        //    if (Delivaeryrange.Length >= 2)
        //    {
        //        _from = _FN.ConvertDateFormatBackFormat(Delivaeryrange[0]);
        //        _to = _FN.ConvertDateFormatBackFormat(Delivaeryrange[1]);
        //    }
        //    else
        //    {
        //        _to = null;
        //        _from = null;
        //    }
        //    if (_from != null)
        //    {
        //        return Json(GetVoyCharterIn(Convert.ToDateTime(_from), Convert.ToDateTime(_to), ShipName), JsonRequestBehavior.AllowGet);
        //    }
        //    else if (ShipName != null && ShipName.Count > 0)
        //    {
        //        _from = DateTime.Now.AddDays(-90);
        //        _to = DateTime.Now;
        //        return Json(GetVoyCharterIn(Convert.ToDateTime(_from), Convert.ToDateTime(_to), ShipName), JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {

        //        return Json(new List<SelectListItem>(), JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult getPurchCharterInEvt(string DateSearch, List<string> ShipName)
        {
            DateTime? _from = null; DateTime? _to = null;
            string[] Delivaeryrange = DateSearch.SplitWord(" to ");
            if (Delivaeryrange.Length >= 2)
            {
                _from = _FN.ConvertDateFormatBackFormat(Delivaeryrange[0]);
                _to = _FN.ConvertDateFormatBackFormat(Delivaeryrange[1]);
            }
            else
            {
                _to = null;
                _from = null;
            }
            if (_from != null)
            {
                return Json(GetPurchCharterIn(Convert.ToDateTime(_from), Convert.ToDateTime(_to), ShipName), JsonRequestBehavior.AllowGet);
            }
            else if (ShipName != null && ShipName.Count > 0)
            {
                _from = DateTime.Now.AddDays(-90);
                _to = DateTime.Now;
                return Json(GetPurchCharterIn(Convert.ToDateTime(_from), Convert.ToDateTime(_to), ShipName), JsonRequestBehavior.AllowGet);
            }
            else
            {

                return Json(new List<SelectListItem>(), JsonRequestBehavior.AllowGet);
            }
            //return Json(GetPurchCharterIn());            
        }

        public ActionResult getGraphData(string DateSearch, string VoyCharter, string TD, List<string> ShipName, string CharterIn, string Status, bool GenExcel = false)
        {
            DateTime? _from = null; DateTime? _to = null;
            string[] Delivaeryrange = DateSearch.SplitWord(" to ");
            if (Delivaeryrange.Length >= 2)
            {
                _from = _FN.ConvertDateFormatBackFormat(Delivaeryrange[0]);
                _to = _FN.ConvertDateFormatBackFormat(Delivaeryrange[1]);
            }
            //else if (ShipName != null && ShipName.Count > 0)
            //{
            //    _from = DateTime.Now.AddDays(-90);
            //    _to = DateTime.Now;
            //}
            else
            {
                _to = null;
                _from = null;

                // Get current month (Past 1 month, Next 1 month)
                var currentDate = _dalTCEWS.GetLastFixtureDate(); //DateTime.Now;
                if (_from == null) _from = new DateTime(currentDate.AddMonths(-3).Year, currentDate.AddMonths(-3).Month, 1);               // Start date Past 3 month
                if (_to == null) _to = currentDate;

            }


            List<string> lstVoy = String.IsNullOrEmpty(VoyCharter) ? null : VoyCharter.Split('|').ToList();
            List<string> lstTD = String.IsNullOrEmpty(TD) ? null : TD.Split('|').ToList();
            List<string> lstSTATUS = String.IsNullOrEmpty(Status) ? null : Status.Split('|').ToList();
            List<string> lstCharterIn= String.IsNullOrEmpty(CharterIn) ? null : CharterIn.Split('|').ToList();
         
            GraphBoardViewModel ViewModel = new GraphBoardViewModel();
            ViewModel.ActionMSG = "";
            List<TDData> lstTDData = new List<TDData>();
            try
            {
                SSVoyCharterIn= GetTransaction(_from, _to, ShipName, lstTD, lstCharterIn, lstSTATUS);
                
                ViewModel.GraphDate = MakeGraph(Convert.ToDateTime(_from), Convert.ToDateTime(_to), lstVoy, lstTD, lstCharterIn, ref lstTDData);
                ViewModel.lstCharterIn = GetPurchCharterIn(SSVoyCharterIn);
                ViewModel.lstVessel = GetVesselCharterIn(SSVoyCharterIn);
                ViewModel.MinYAxis = getMinYAxis(ViewModel.GraphDate);
                ViewModel.TDData = MakeTDData(lstTDData, lstSTATUS);
                ViewModel.TotalAvg = getTotalAVG(lstTDData, lstSTATUS);
                ViewModel.TotalSum = getTotalSUM(lstTDData, lstSTATUS);
                //if (GenExcel) ViewModel.ActionMSG = GenarateExcel(lstTDData.Where(x => x.Status == "" || x.Status == "SUBMIT").OrderBy(x => x.DateOrder).ToList(), lstVoy, lstTD);
                if (GenExcel) ViewModel.ActionMSG = GenarateExcel(ViewModel.TDData, lstVoy, lstTD);
            }
            catch (Exception ex)
            {
                ViewModel.ActionMSG = ex.Message;
            }
            return Json(ViewModel, JsonRequestBehavior.AllowGet);


        }     

        public ActionResult getCHIDataInfo(string CHINo)
        {
            TceDetail _Detail = new Model.TceDetail();
            _Detail = getChiDetail(CHINo);
            
            return Json(_Detail, JsonRequestBehavior.AllowGet);
           
        }

        public TceDetail getChiDetail(string CHINo)
        {
            TceDetail _Detail = new Model.TceDetail();
            var CHIData = SSCHIData.Where(x => x.IDA_ROW_ID.ToUpper() == CHINo.ToUpper()).ToList();
            if (CHIData != null && CHIData.Count > 0)
            {//CHIData[0]
                _Detail.chi_data_id = CHIData[0].IDA_ROW_ID;
                //_Detail.chi_data_no = CHIData[0].IDA_PURCHASE_NO;
                _Detail.date_fixture = (CHIData[0].IDA_DATE_PURCHASE == null) ? "" : Convert.ToDateTime(CHIData[0].IDA_DATE_PURCHASE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                _Detail.extra_cost = CHIData[0].IDA_P_EXTEN_COST;
                _Detail.flat_rate = CHIData[0].IDA_P_FLAT_RATE;
                _Detail.td = string.IsNullOrEmpty(CHIData[0].IDA_TD_MARKET) ? "TD2" : CHIData[0].IDA_TD_MARKET;
                _Detail.laycan_load_from = ((CHIData[0].IDA_P_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(CHIData[0].IDA_P_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)) + " to " + ((CHIData[0].IDA_P_LAYCAN_TO == null) ? "" : Convert.ToDateTime(CHIData[0].IDA_P_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                var OfferFinal = CHIData[0].CHI_OFFER_ITEMS.Count > 0 ? CHIData[0].CHI_OFFER_ITEMS.Where(x => x.IOI_FINAL_FLAG == "Y").ToList() : null;
                if (OfferFinal != null && OfferFinal.Count > 0)
                {
                    _Detail.ship_name = OfferFinal[0].IOI_FK_VEHICLE;
                    _Detail.ship_broker = OfferFinal[0].IOI_FK_VENDOR;
                    _Detail.ship_owner = OfferFinal[0].IOI_OWNER;
                    var RoundItem = OfferFinal[0].CHI_ROUND_ITEMS.OrderByDescending(x => x.IRI_ROUND_NO).ToList();

                    _Detail.dem = RoundItem[0].CHI_ROUND.Where(x => x.IIR_ROUND_TYPE.ToUpper() == "DEM").ToList().Select(x => x.IIR_ROUND_OFFER).FirstOrDefault();
                    _Detail.min_load = RoundItem[0].CHI_ROUND.Where(x => x.IIR_ROUND_TYPE.ToUpper() == "MIN LOADED").ToList().Select(x => x.IIR_ROUND_OFFER).FirstOrDefault();
                    _Detail.top_fixture_value = RoundItem[0].CHI_ROUND.Where(x => x.IIR_ROUND_TYPE.ToUpper() == "WS").ToList().Select(x => x.IIR_ROUND_OFFER).FirstOrDefault();

                }
            }
            
            return _Detail;
           
        }
        #endregion----------------------------------------

        private List<ViewModels.GraphBoardFromDate> MakeGraph(DateTime _from, DateTime _to, List<string> lstVoyNo, List<string> lstTD, List<string> lstCharterIn, ref List<TDData> LstTRData)
        {

            //Generate Route
            #region--------------------Route--------------------

            List<List<double?>> _lstTD2 = new List<List<double?>>();
            List<List<double?>> _lstTD3 = new List<List<double?>>();
            List<List<double?>> _lstTD15 = new List<List<double?>>();

            // Get CRUDE ROUTE from date
            var CR = _dalGraphBoard.Get_MT_CRUDE_ROUTE(_from, _to);
            // Distinct Date in CRUDE ROUTE
            var Dtlst = CR.Select(x => new { x.MCR_DATE }).ToList().Distinct().OrderBy(x => x.MCR_DATE).ToList();
            
            foreach (var item in Dtlst)
            {
                if (item != null)
                {
                    TDData _trData = new ViewModels.TDData();

                    var _lstitemFromDate = CR.Where(x => x.MCR_DATE == item.MCR_DATE).ToList();
                    if (_lstitemFromDate != null && _lstitemFromDate.Count > 0)
                    {
                            _trData.DateTD = _lstitemFromDate[0].MCR_DATE.ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            _trData.DateOrder = _lstitemFromDate[0].MCR_DATE.ToString("yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture) +"_"+_lstitemFromDate[0].MCR_STATUS;
                            if (string.IsNullOrEmpty(_lstitemFromDate[0].MCR_TD2)) { _lstTD2.Add(null); _trData.TD2 = null; }
                            else
                            {
                                List<double?> str = new List<double?>();
                                str.Add(_FN.ToUnixTimestamp(_lstitemFromDate[0].MCR_DATE));
                                str.Add(MapDoubleVal(_lstitemFromDate[0].MCR_TD2));
                                _trData.TD2 = _lstitemFromDate[0].MCR_TD2;
                                _lstTD2.Add(str);
                            }
                            if (string.IsNullOrEmpty(_lstitemFromDate[0].MCR_TD3)) { _lstTD3.Add(null); _trData.TD3 = null; }
                            else
                            {
                                List<double?> str = new List<double?>();
                                str.Add(_FN.ToUnixTimestamp(_lstitemFromDate[0].MCR_DATE));
                                str.Add(MapDoubleVal(_lstitemFromDate[0].MCR_TD3));
                                _trData.TD3 = _lstitemFromDate[0].MCR_TD3;
                                _lstTD3.Add(str);
                            }
                            if (string.IsNullOrEmpty(_lstitemFromDate[0].MCR_TD15)) { _lstTD15.Add(null); _trData.TD15 = null; }
                            else
                            {
                                List<double?> str = new List<double?>();
                                str.Add(_FN.ToUnixTimestamp(_lstitemFromDate[0].MCR_DATE));
                                str.Add(MapDoubleVal(_lstitemFromDate[0].MCR_TD15));
                                _trData.TD15 = _lstitemFromDate[0].MCR_TD15;
                                _lstTD15.Add(str);
                            }
                        }
                        _trData.Status = (string.IsNullOrEmpty(_trData.Status)) ? "" : _trData.Status;
                        _trData.chi_data_id = (string.IsNullOrEmpty(_trData.chi_data_id)) ? "" : _trData.chi_data_id;
                        LstTRData.Add(_trData);
                    
                }
            }

            foreach (var _item in SSVoyCharterIn)
            {
                var _trData = LstTRData.Where(x => x.DateTD == _FN.ConvertDateFormatBackFormat(_item.date_fixture, "dd-MMM-yyyy")).ToList();

                string ws_saving = String.IsNullOrEmpty(_item.ws_saving) ? "0.00" : Convert.ToDecimal(_item.ws_saving).ToString("#,##0.00");
                string saving_in = String.IsNullOrEmpty(_item.saving_in) ? "0.00" : Math.Round(Convert.ToDecimal(_item.saving_in)).ToString("#,##0");
                string flat_rate = String.IsNullOrEmpty(_item.flat_rate) ? "0.00" : Convert.ToDecimal(_item.flat_rate).ToString("#,##0.00");
                string avg_ws = String.IsNullOrEmpty(_item.avg_ws) ? "0.00" : Convert.ToDecimal(_item.avg_ws).ToString("#,##0.00");
                string status = String.IsNullOrEmpty(_item.status) ? "" : _item.status;
                string charter_in = String.IsNullOrEmpty(_item.charter_in) ? "" : _item.charter_in;

                string LaycanLoad = _FN.ConvertDateFormatBackFormat(_item.laycan_load_from, "dd-MMM-yyyy") + " to " + _FN.ConvertDateFormatBackFormat(_item.laycan_load_to, "dd-MMM-yyyy");
                string target_days = _FN.ConvertDateFormatBackFormat(_item.target_days, "dd-MMM-yyyy");
                if (_trData.Count > 0)
                {

                    if ((_trData.Count == 1 && String.IsNullOrEmpty( _trData[0].chi_data_id)) || _trData.Where(x => x.chi_data_id == _item.purchase_no).ToList().Count > 0)
                    {
                        _trData[0].Status = status;
                        _trData[0].chi_data_id = charter_in;
                        _trData[0].WSSaveing = ws_saving;
                        _trData[0].TOPFix = _item.top_fixture_value;
                        _trData[0].Target20Day = target_days;
                        _trData[0].ShipName = _item.vessel;
                        _trData[0].ShipOwner = _item.owner;
                        _trData[0].ShipBroker = _item.broker;
                        _trData[0].SaveIn = saving_in;
                        _trData[0].LaycanLoad = LaycanLoad; //_item.laycan_load_from + " to " + _item.laycan_load_to;
                        _trData[0].Flatrate = flat_rate;
                        _trData[0].Deviation = _item.extra_cost;
                        _trData[0].Dem = _item.dem;
                        _trData[0].AvgWS3day = avg_ws;
                        _trData[0].ParamVal = "?transaction_id=" + _item.transaction_id.Encrypt() + "&Tran_Req_ID=" + _item.req_transaction_id.Encrypt() + "&purchase_no=" + _item.purchase_no.Encrypt() + "&status=" + _item.status.Encrypt();
                        _trData[0].TD = _item.td;
                    }
                    else
                    {
                        TDData _tdDataItem = new TDData();
                        _tdDataItem.Status = status;
                        _tdDataItem.chi_data_id = charter_in;
                        _tdDataItem.DateTD = _FN.ConvertDateFormatBackFormat(_item.date_fixture, "dd-MMM-yyyy");
                        _tdDataItem.DateOrder = _FN.ConvertDateFormatBackFormat(_item.date_fixture, "yyyyMMdd") + "_" + _item.status;
                        _tdDataItem.WSSaveing = ws_saving;
                        _tdDataItem.TOPFix = _item.top_fixture_value;
                        _tdDataItem.Target20Day = target_days;
                        _tdDataItem.ShipName = _item.vessel;
                        _tdDataItem.ShipOwner = _item.owner;
                        _tdDataItem.ShipBroker = _item.broker;
                        _tdDataItem.SaveIn = saving_in;
                        _tdDataItem.LaycanLoad = LaycanLoad; // _item.laycan_load_from + " to " + _item.laycan_load_to;
                        _tdDataItem.Flatrate = flat_rate;
                        _tdDataItem.Deviation = _item.extra_cost;
                        _tdDataItem.Dem = _item.dem;
                        _tdDataItem.AvgWS3day = avg_ws;
                        _tdDataItem.ParamVal = "?transaction_id=" + _item.transaction_id.Encrypt() + "&Tran_Req_ID=" + _item.req_transaction_id.Encrypt() + "&purchase_no=" + _item.purchase_no.Encrypt() + "&status=" + _item.status.Encrypt();
                        _tdDataItem.TD2 = _trData[0].TD2;
                        _tdDataItem.TD3 = _trData[0].TD3;
                        _tdDataItem.TD15 = _trData[0].TD15;
                        _tdDataItem.TD = _item.td;
                        LstTRData.Add(_tdDataItem);
                    }
                }
                else
                {
                    TDData _tdDataItem = new TDData();
                    _tdDataItem.Status = status;
                    _tdDataItem.chi_data_id = charter_in;
                    _tdDataItem.DateTD = _FN.ConvertDateFormatBackFormat(_item.date_fixture, "dd-MMM-yyyy");
                    _tdDataItem.DateOrder = _FN.ConvertDateFormatBackFormat(_item.date_fixture, "yyyyMMdd") + "_" + _tdDataItem.Status;
                    _tdDataItem.WSSaveing = ws_saving;
                    _tdDataItem.TOPFix = _item.top_fixture_value;
                    _tdDataItem.Target20Day = target_days;
                    _tdDataItem.ShipName = _item.vessel;
                    _tdDataItem.ShipOwner = _item.owner;
                    _tdDataItem.ShipBroker = _item.broker;
                    _tdDataItem.SaveIn = saving_in;
                    _tdDataItem.LaycanLoad = LaycanLoad; // _item.laycan_load_from + " to " + _item.laycan_load_to;
                    _tdDataItem.Flatrate = flat_rate;
                    _tdDataItem.Deviation = _item.extra_cost;
                    _tdDataItem.Dem = _item.dem;
                    _tdDataItem.AvgWS3day = avg_ws;
                    _tdDataItem.ParamVal = "?transaction_id=" + _item.transaction_id.Encrypt() + "&Tran_Req_ID=" + _item.req_transaction_id.Encrypt() + "&purchase_no=" + _item.purchase_no.Encrypt() + "&status=" + _item.status.Encrypt();
                    _tdDataItem.TD = _item.td;
                    LstTRData.Add(_tdDataItem);
                }
            }

            #endregion

            #region--------------------Generate graph --------------------

            GraphBoardFromDate grphTD2 = new ViewModels.GraphBoardFromDate();
            GraphBoardFromDate grphTD3 = new ViewModels.GraphBoardFromDate();
            GraphBoardFromDate grphTD15 = new ViewModels.GraphBoardFromDate();

            string colorTD2 = "#0172b8";
            string colorTD3 = "#f8a81d";
            string colorTD15 = "#05994d";

            grphTD2.showInLegend = true;
            grphTD2.visible = true;
            grphTD2.name = "TD2";
            grphTD2.color = colorTD2;
            grphTD2.marker = new Marker() { symbol = "diamond", enabled = false}; 
            grphTD2.data = _lstTD2;

            grphTD3.showInLegend = true;
            grphTD3.visible = true;
            grphTD3.name = "TD3";
            grphTD3.color = colorTD3;
            grphTD3.marker = new Marker() { symbol = "diamond", enabled = false }; 
            grphTD3.data = _lstTD3;

            grphTD15.showInLegend = true;
            grphTD15.visible = false;
            grphTD15.name = "TD15";
            grphTD15.color = colorTD15;
            grphTD15.marker = new Marker() { symbol = "diamond", enabled = false };
            grphTD15.data = _lstTD15;
            var GraphLine = new List<ViewModels.GraphBoardFromDate>();
            //if (lstTD == null || lstTD.Where(x => x == "TD2").ToList().Count > 0)
                GraphLine.Add(grphTD2);
            //if (lstTD == null || lstTD.Where(x => x == "TD3").ToList().Count > 0)
                GraphLine.Add(grphTD3);
            //if (lstTD == null || lstTD.Where(x => x == "TD15").ToList().Count > 0)
                GraphLine.Add(grphTD15);

            #endregion------------------------------------------

            //Generate Fix Point SUBMIT
            #region--------------------Fix Point----------------
            var filterSSVoyCharterIn = SSVoyCharterIn.Where(x => x.status.ToUpper() == "APPROVED").ToList();
            //if(lstVoyNo != null && filterSSVoyCharterIn != null)
            //  filterSSVoyCharterIn = filterSSVoyCharterIn.Where(x => lstVoyNo.Contains(x.purchase_no)).ToList();
            if (lstCharterIn != null && filterSSVoyCharterIn != null)
                filterSSVoyCharterIn = filterSSVoyCharterIn.Where(x => lstCharterIn.Contains(x.charter_in)).ToList();
            

            foreach (var _item in filterSSVoyCharterIn)
            {
               
                string ws_saving = String.IsNullOrEmpty(_item.ws_saving) ? "0.00" : Convert.ToDecimal(_item.ws_saving).ToString("#,##0.00");
                string saving_in = String.IsNullOrEmpty(_item.saving_in) ? "0" : Math.Round(Convert.ToDecimal(_item.saving_in)).ToString("#,##0");

                //if (lstTD != null && lstTD.Where(x => x == _item.td).ToList().Count <= 0) continue;
                GraphBoardFromDate _graphLine = new ViewModels.GraphBoardFromDate();
                List<List<double?>> _lstFix = new List<List<double?>>();
                List<double?> str = new List<double?>();
                str.Add(_FN.ToUnixTimestamp(Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(_item.date_fixture))));
                //str.Add(_FN.ToUnixTimestamp(Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(_item.target_days))));
                double outFix = 0; double.TryParse(_item.top_fixture_value, out outFix);
                str.Add(outFix);///
                _lstFix.Add(str);
                _graphLine.showInLegend = false;
                _graphLine.visible = true;
                _graphLine.name = _item.charter_in;
                //_graphLine.marker = new Marker() { symbol = "url(../Content/images/sun.png)", enabled = true };
                //_graphLine.marker = new Marker() { symbol = "url(http://icons.iconarchive.com/icons/unclebob/spanish-travel/32/ship-icon.png)", enabled = true };

                if(_item.td == "TD2")
                {
                    _graphLine.marker = new Marker() { symbol = "circle", enabled = true, fillColor = colorTD2, lineWidth = 2, lineColor = colorTD2 };
                }
                else if(_item.td == "TD3")
                {
                    _graphLine.marker = new Marker() { symbol = "circle", enabled = true, fillColor = colorTD3, lineWidth = 2, lineColor = colorTD3 };
                }
                else if(_item.td == "TD15")
                {
                    _graphLine.marker = new Marker() { symbol = "circle", enabled = true, fillColor = colorTD15, lineWidth = 2, lineColor = colorTD15 };
                }

                //_graphLine.tooltip = new ViewModels.Tooltip()
                //{
                //    headerFormat = string.Format("<b>Charter In :{0} </b><br>", _item.charter_in)
                //    ,
                //    pointFormat = string.Format("-Status : {5}<br>-TOP Fixture value : {0}<br>-TD : {1} <br>-Vessel: {2} <br>-WS saving : {3} <br>-Saving in $ : {4}",
                //        _item.top_fixture_value,
                //        _item.td,
                //        _item.vessel,
                //        ws_saving,
                //        saving_in,
                //        _item.status)
                //};

                _graphLine.tooltip = new ViewModels.Tooltip()
                {
                    headerFormat = string.Format("<b>Charter In :{0} </b><br/>", _item.charter_in)
                    ,
                    pointFormat = string.Format("- Ship name : {0} <br/>- Market Ref. : {1} <br/>- TOP Fixture value : {2} WS<br/>- WS saving : {3} <br/>- Saving ($) : {4}",
                        _item.vessel,
                        _item.td,
                        _item.top_fixture_value, 
                        ws_saving,
                        saving_in)
                };
                _graphLine.data = _lstFix;
                GraphLine.Add(_graphLine);
                 
            }


            #endregion------------------------------------------

            //Generate Fix Point CANCEL
            #region--------------------Fix Point----------------
            //var filterSSVoyCharterInCancel = SSVoyCharterIn.Where(x => x.status.ToUpper() == ConstantPrm.ACTION.CANCEL).ToList();
            //if (lstCharterIn != null && filterSSVoyCharterInCancel != null)
            //    filterSSVoyCharterInCancel = filterSSVoyCharterInCancel.Where(x => lstCharterIn.Contains(x.charter_in)).ToList();


            //foreach (var _item in filterSSVoyCharterInCancel)
            //{

            //    string ws_saving = String.IsNullOrEmpty(_item.ws_saving) ? "0.00" : Convert.ToDecimal(_item.ws_saving).ToString("#,##0.00");
            //    string saving_in = String.IsNullOrEmpty(_item.saving_in) ? "0.00" : Convert.ToDecimal(_item.saving_in).ToString("#,##0.00");

            //    //if (lstTD != null && lstTD.Where(x => x == _item.td).ToList().Count <= 0) continue;
            //    GraphBoardFromDate _graphLine = new ViewModels.GraphBoardFromDate();
            //    List<List<double?>> _lstFix = new List<List<double?>>();
            //    List<double?> str = new List<double?>();
            //    str.Add(_FN.ToUnixTimestamp(Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(_item.date_fixture))));
            //    double outFix = 0; double.TryParse(_item.top_fixture_value, out outFix);
            //    str.Add(outFix);///
            //    _lstFix.Add(str);
            //    _graphLine.showInLegend = false;
            //    _graphLine.name = _item.charter_in;

            //    string strResonReject = "";
            //    using (var context = new EntityCPAIEngine())
            //    {
            //        var CHIData = context.CHI_DATA.Where(x => x.IDA_PURCHASE_NO.ToUpper() == _item.charter_in.ToUpper() && x.IDA_FREIGHT.ToUpper() == "WS").ToList();

            //        if (CHIData != null && CHIData.Count > 0)
            //        {
            //            strResonReject = CHIData[0].IDA_REASON;
            //        }
            //    }

            //    _graphLine.marker = new Marker() { symbol = "circle", enabled = true, fillColor = "#EB410A", lineWidth = 2, lineColor = "#EB410A" };//_FN.getRandColor() };
            //    _graphLine.tooltip = new ViewModels.Tooltip()
            //    {
            //        headerFormat = string.Format("<b>Charter In :{0} </b><br>", _item.charter_in)
            //        ,
            //        pointFormat = string.Format("-Status : {5}<br>-Reason : {6}<br>-TOP Fixture value : {0}<br>-TD : {1} <br>-Vessel: {2} <br>-WS saving : {3} <br>-Saving in $ : {4}",
            //        _item.top_fixture_value,
            //        _item.td,
            //        _item.vessel,
            //        ws_saving,
            //        saving_in,
            //        _item.status,
            //        strResonReject)
            //    };
            //    _graphLine.data = _lstFix;
            //    GraphLine.Add(_graphLine);

            //}

            //foreach (var _item in SSVoyCharterIn)
            //{
            //    if ((lstVoyNo == null || lstVoyNo.Where(x => x == _item.purchase_no).ToList().Count > 0) && (lstCharterIn == null || lstCharterIn.Where(x => x == _item.charter_in).ToList().Count > 0))
            //    {
            //        if (_item.status.ToUpper() == ConstantPrm.ACTION.SUBMIT)
            //        {
            //            string ws_saving = String.IsNullOrEmpty(_item.ws_saving) ? "0.00" : Convert.ToDecimal(_item.ws_saving).ToString("#,##0.00");
            //            string saving_in = String.IsNullOrEmpty(_item.saving_in) ? "0.00" : Convert.ToDecimal(_item.saving_in).ToString("#,##0.00");

            //            if (lstTD != null && lstTD.Where(x => x == _item.td).ToList().Count <= 0) continue;
            //            GraphBoardFromDate _graphLine = new ViewModels.GraphBoardFromDate();
            //            List<List<double?>> _lstFix = new List<List<double?>>();
            //            List<double?> str = new List<double?>();
            //            str.Add(_FN.ToUnixTimestamp(Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(_item.date_fixture))));
            //            double outFix = 0; double.TryParse(_item.top_fixture_value, out outFix);
            //            str.Add(outFix);///
            //            _lstFix.Add(str);
            //            _graphLine.name = _item.purchase_no;
            //            //_graphLine.marker = new Marker() { symbol = "url(../Content/images/sun.png)", enabled = true };
            //            //_graphLine.marker = new Marker() { symbol = "url(http://icons.iconarchive.com/icons/unclebob/spanish-travel/32/ship-icon.png)", enabled = true };

            //            _graphLine.marker = new Marker() { symbol = "circle", enabled = true, fillColor = "#EB410A", lineWidth = 2, lineColor = "#EB410A" };//_FN.getRandColor() };
            //            _graphLine.tooltip = new ViewModels.Tooltip()
            //            {
            //                headerFormat = string.Format("<b>Voy No :{0} </b><br>", _item.purchase_no)
            //                ,
            //                pointFormat = string.Format("-Charter In : {5}<br>-TOP Fixture value : {0}<br>-TD : {1} <br>-Vessel: {2} <br>-WS saving : {3} <br>-Saving in $ : {4}",
            //                _item.top_fixture_value,
            //                _item.td,
            //                _item.vessel,
            //                ws_saving,
            //                saving_in,
            //                _item.charter_in)
            //            };
            //            _graphLine.data = _lstFix;
            //            GraphLine.Add(_graphLine);
            //        }
            //    }
            //}
            #endregion------------------------------------------



            return GraphLine;
        }

        private double? MapDoubleVal(string _dVal)
        {
            double _dbVal = 0;
            if (double.TryParse(_dVal, out _dbVal))
            {
                return _dbVal;
            }
            else
            {
                return null;
            }
            
        }

        private string GenarateExcel(List<TDData> LstTRData, List<string> lstVoy, List<string> lstTD)
        {

            try
            {
                Color _BorderColor = Color.Black;
                string _Time = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                //string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Areas", "CPAIMVC", "Content", "TmpFile", string.Format("Graphboard{0}.xlsx", _Time));
                string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("Graphboard{0}.xlsx", _Time));
                string _urlPath = "";
                FileInfo excelFile = new FileInfo(file_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Freight Calculation");


                    //Initialize columns
                    int columnNo = 1;
                    ws.Column(columnNo).Width = 10; columnNo++;
                    //if (lstTD==null ||lstTD.Where(x => x == "TD2").ToList().Count > 0) { ws.Column(columnNo).Width = 9; columnNo++; }
                    //if (lstTD == null || lstTD.Where(x => x == "TD3").ToList().Count > 0) { ws.Column(columnNo).Width = 9; columnNo++; }
                    //if (lstTD == null || lstTD.Where(x => x == "TD15").ToList().Count > 0) { ws.Column(columnNo).Width = 9; columnNo++; }
                    ws.Column(columnNo).Width = 9; columnNo++; //TD2
                    ws.Column(columnNo).Width = 9; columnNo++; //TD3
                    ws.Column(columnNo).Width = 9; columnNo++; //TD15
                    ws.Column(columnNo).Width = 10; columnNo++; // Charter No
                    ws.Column(columnNo).Width = 10; columnNo++; // Status
                    ws.Column(columnNo).Width = 10; columnNo++;
                    ws.Column(columnNo).Width = 12; columnNo++;
                    ws.Column(columnNo).Width = 12; columnNo++;
                    ws.Column(columnNo).Width = 12; columnNo++;
                    ws.Column(columnNo).Width = 12; columnNo++;
                    ws.Column(columnNo).Width = 12; columnNo++;
                    ws.Column(columnNo).Width = 30; columnNo++;
                    ws.Column(columnNo).Width = 15; columnNo++;
                    ws.Column(columnNo).Width = 40; columnNo++;
                    ws.Column(columnNo).Width = 35; columnNo++;
                    ws.Column(columnNo).Width = 20; columnNo++;
                    ws.Column(columnNo).Width = 15; columnNo++;
                    ws.Column(columnNo).Width = 10; columnNo++;
                    ws.Column(2).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(4).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Column(8).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 9));

                    ws.Cells["A1:P1"].Merge = true;
                    ws.Cells["A1:P1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ws.Cells[1, 1].Value = "Spot VLCC Chartering VS Market";
                    ws.Cells[1, 1].Style.Font.Size = 10;
                    ws.Cells[1, 1].Style.Font.Bold = true;


                    ws.Cells["A2:P2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A2:P2"].Style.Font.Bold = true;
                    //ws.Cells["A2:P2"].Style.Border..Color = System.Drawing.ColorTranslator.ToOle(_BorderColor);
                    columnNo = 1;
                    ws.Cells[2, columnNo].Value = "Date"; columnNo++;
                    //if (lstTD == null || lstTD.Where(x => x == "TD2").ToList().Count > 0) { ws.Cells[2, columnNo].Value = "BITR TD2"; columnNo++; }
                    //if (lstTD == null || lstTD.Where(x => x == "TD3").ToList().Count > 0) { ws.Cells[2, columnNo].Value = "BITR TD3"; columnNo++; }
                    //if (lstTD == null || lstTD.Where(x => x == "TD15").ToList().Count > 0) { ws.Cells[2, columnNo].Value = "BITR TD15"; columnNo++;}
                    ws.Cells[2, columnNo].Value = "BITR TD2"; columnNo++; 
                    ws.Cells[2, columnNo].Value = "BITR TD3"; columnNo++; 
                    ws.Cells[2, columnNo].Value = "BITR TD15"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Marker Ref."; columnNo++;
                    ws.Cells[2, columnNo].Value = "Charter In"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Status"; columnNo++;
                    ws.Cells[2, columnNo].Value = "TOP Fixture (A)"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Ship Name"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Ship Broker"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Ship Owner"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Dem ($/d)"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Flat Rate"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Diviation/ WRPM/ Armguard"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Laycan Load"; columnNo++;
                    ws.Cells[2, columnNo].Value = "(B) Target 20 days before 1st date laycan"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Avg WS 3 day around (B)"; columnNo++;
                    ws.Cells[2, columnNo].Value = "WS Saving (B)-(A)"; columnNo++;
                    ws.Cells[2, columnNo].Value = "Saving in $"; columnNo++;                    
                    int rowNo = 3;
                    foreach (var _item in LstTRData)
                    {
                        //if (_item.Status != ConstantPrm.ACTION.SUBMIT) continue;
                        columnNo = 1;
                        ws.Cells[rowNo, columnNo].Value = _item.DateTD; columnNo++;
                        //if (lstTD == null || lstTD.Where(x => x == "TD2").ToList().Count > 0) { ws.Cells[rowNo, columnNo].Value = _item.TD2; columnNo++; }
                        //if (lstTD == null || lstTD.Where(x => x == "TD3").ToList().Count > 0) { ws.Cells[rowNo, columnNo].Value = _item.TD3; columnNo++; }
                        //if (lstTD == null || lstTD.Where(x => x == "TD15").ToList().Count > 0) { ws.Cells[rowNo, columnNo].Value = _item.TD15; columnNo++; }
                        ws.Cells[rowNo, columnNo].Value = _item.TD2; columnNo++; 
                        ws.Cells[rowNo, columnNo].Value = _item.TD3; columnNo++; 
                        ws.Cells[rowNo, columnNo].Value = _item.TD15; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.TD; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.chi_data_id; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.Status; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.TOPFix; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.ShipName; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.ShipBroker; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.ShipOwner; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.Dem; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.Flatrate; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.Deviation; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.LaycanLoad; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.Target20Day; columnNo++; ;
                        ws.Cells[rowNo, columnNo].Value = _item.AvgWS3day; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.WSSaveing; columnNo++;
                        ws.Cells[rowNo, columnNo].Value = _item.SaveIn; columnNo++;                        
                        //ws.get_Range("A" + rowNo, "P" + rowNo).Cells.Borders.Color = System.Drawing.ColorTranslator.ToOle(_BorderColor);
                        rowNo++;
                    }
                    package.Save();
                    //_urlPath = _FN.GetSiteRootUrl(Path.Combine("Areas", "CPAIMVC", "Content", "TmpFile", string.Format("Graphboard{0}.xlsx", _Time)));
                    _urlPath = _FN.GetSiteRootUrl(Path.Combine("Web", "Report", "TmpFile", string.Format("Graphboard{0}.xlsx", _Time)));
                }
                return _urlPath;
            }
            catch (Exception ex)
            {
                throw new Exception("GenarateExcel >>" + ex.Message);
            }

        }

        private void updateModel(FormCollection form, FixtureViewModel model, bool isTextMode = false)
        {

        }        

        private void BindButtonPermission(List<ButtonAction> Button)
        {
            foreach (ButtonAction _button in Button)
            {
                string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                ViewBag.ButtonCode += _btn;
            }
            ViewBag.ButtonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            ButtonListAddFixture = Button;
        }

        private void SaveData(DocumentActionStatus _status, ref FixtureViewModel _Vmodel, string NoteAction)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }
            var json = new JavaScriptSerializer().Serialize(new TceRootObject() { tce_detail = MakeModelTosave( _Vmodel.TceDetail )});
            //add json to class for convert to xml.
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000012;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "current_action", V = CurrentAction });
            req.Req_parameters.P.Add(new P { K = "next_status", V = NextState });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_WS });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "note", V = (NoteAction == "") ? "-" : NoteAction });
            req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            if (resData != null && resData.result_code == "1")
            {
                if (DocumentActionStatus.Draft == _status && resData.resp_parameters != null && resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList().Count > 0)
                {

                    string currUrl = string.Format("/CPAIMVC/GraphBoard/AddFixture?TranID={1}&Tran_Req_ID={0}&PURNO={2}", resData.req_transaction_id.Encrypt(), resData.transaction_id.Encrypt(), resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList()[0].v.Encrypt());
                    
                    TempData["res_message"] = resData.response_message;
                    TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + currUrl : currUrl;

                }
                else
                {
                    TempData["res_message"] = (resData.response_message==null)? resData.result_desc : resData.response_message;
                    TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
                }
            }
            else
            {
                TempData["res_message"] = (resData.response_message == null) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
                //TempData["returnPage"] = null;
            }
            
        }

        private void UpdateModel(FormCollection from, FixtureViewModel model)
        {

        }

        private TceDetail MakeModelTosave(TceDetail _model)
        {
            if (_model == null) _model = new Model.TceDetail();
            if (_model.chi_data_id == null) _model.chi_data_id = "";
            //if (_model.chi_data_no == null) _model.chi_data_no = "";
            if (_model.ship_name == null) _model.ship_name = "";
            if (_model.ship_broker == null) _model.ship_broker = "";
            if (_model.ship_owner == null) _model.ship_owner = "";
            if (_model.dem == null) _model.dem = "";
            if (_model.flat_rate == null) _model.flat_rate = "";
            if (_model.extra_cost == null) _model.extra_cost = "";
            if (_model.laycan_load_from == null) _model.laycan_load_from = "";
            if (_model.laycan_load_to == null) _model.laycan_load_to = "";
            if (_model.top_fixture_value == null) _model.top_fixture_value = "";
            if (_model.date_fixture == null) _model.date_fixture = "";
            if (_model.td == null) _model.td = "";
            if (_model.min_load == null) _model.min_load = "";

            if (_model.laycan_load_from != "")
            {
                string[] splitDateLaycan = _model.laycan_load_from.SplitWord("to");
                if (splitDateLaycan.Length > 1)
                {
                    _model.laycan_load_from = splitDateLaycan[0].Trim();
                    _model.laycan_load_to = splitDateLaycan[1].Trim();
                }
                else
                {
                    _model.laycan_load_from = "";
                    _model.laycan_load_to = "";
                }
            }
            
            return _model;
        }

        private ResponseData LoadDataFixture(string FixtureID, ref FixtureViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000014;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = FixtureID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_WS });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            
            model.PageMode = true;//false;ของเดิม Check อยู่แต่ Edit ก็สามารถเชคได้รอ Confirm ว่าต้องดักอะไรบ้าง
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            model.GenButton = true;

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                if (_model.buttonDetail.Button.Where(x => x.name.ToUpper().IndexOf(ConstantPrm.ACTION.SAVE_DRAFT) >= 0).ToList().Count > 0)
                {
                    model.PageMode = true;
                    model.Action = "DRAFT";
                    SSEditMode = true;
                }
                if (_model.buttonDetail.Button.Where(x => x.name.ToUpper().IndexOf(ConstantPrm.ACTION.EDIT) >= 0).ToList().Count > 0)
                {
                    model.PageMode = true;
                    model.Action = "SUBMIT";
                    SSEditMode = false;
                }
                BindButtonPermission(_model.buttonDetail.Button);               
            }
            else
            {
                model.Action = "CANCEL";
                model.PageMode = false;
            }
            if (_model.data_detail != null)
            {
                model.TceDetail = new JavaScriptSerializer().Deserialize<TceRootObject>(_model.data_detail).tce_detail;
                model.TceDetail.laycan_load_from = model.TceDetail.laycan_load_from+ " to "+ model.TceDetail.laycan_load_to;
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
            }
            return resData;
        }

        private List<Transaction> GetTransaction(DateTime? _from, DateTime? _To, List<string> lstVessel, List<string> lstTD, List<string> lstCharterIn, List<string> lstStatus)
        {
            List<Transaction> _transaction = new List<Flow.Model.Transaction>();
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000013;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_WS });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });

            if (_from != null)
            {
                string _strfrom = "";
                string _strTo = "";
                _strfrom = Convert.ToDateTime(_from).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                _strTo = Convert.ToDateTime(_To).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                req.Req_parameters.P.Add(new P { K = "index_18", V = (_strfrom != "" && _strTo != "") ? _strfrom + "|" + _strTo : "" });
            }
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_TCEtrx _model = ShareFunction.DeserializeXMLFileToObject<List_TCEtrx>(_DataJson);
            if (_model != null && _model.TCETransaction != null && _model.TCETransaction.Count > 0)
            {
                if (_model.TCETransaction.Count > 0)
                {
                    _transaction = _model.TCETransaction;
                  
                    CHI_DATA_DAL _chicls = new CHI_DATA_DAL();
                    foreach (var _item in _transaction)
                    {
                        _item.charter_in = _chicls.GetCHINOByID(_item.charter_in);    // Replace Charter In RowID with Charter In NO 
                        if(string.IsNullOrEmpty(_item.status) == false)
                        {
                           if (_item.status == "SUBMIT")
                                _item.status = "APPROVED" ;
                        }
                        
                    }

                    if (lstVessel != null && lstVessel.Count > 0)
                    {
                        _transaction = _transaction.Where(x => lstVessel.Contains(x.vessel_id)).ToList();
                    }

                    if (lstTD != null && lstTD.Count > 0)
                    {
                        _transaction = _transaction.Where(x => lstTD.Contains(x.td)).ToList();
                    }

                    if (lstCharterIn != null && lstCharterIn.Count > 0)
                    {
                        _transaction = _transaction.Where(x => lstCharterIn.Contains(x.charter_in)).ToList();
                    }

                    if (lstStatus != null && lstStatus.Count > 0)
                    {
                        _transaction = _transaction.Where(x => lstStatus.Contains(x.status)).ToList();
                    }
                }
            }
            

            return _transaction;
        }

        private string getMinYAxis(List<GraphBoardFromDate> model)
        {
            string rtn = "0";

            try
            {
                List<decimal> lstMinValue = new List<decimal>();
                if (model != null && model.Count > 0)
                {
                    foreach (var item in model)
                    {
                        foreach (var itemData in item.data)
                        {
                            foreach (var itemDataL in itemData)
                            {
                                if(itemDataL != null)
                                {
                                    lstMinValue.Add(Convert.ToDecimal(itemDataL.Value));
                                }
                                    
                            }
                        }
                    }

                    string minY = (from p in lstMinValue
                                   select p).Min().ToString();
                    decimal dMin = Convert.ToDecimal(minY);
                    var min = ((int)Math.Floor(dMin / 10) * 10);
                    rtn = min.ToString();
                }
            }
            catch
            {
                rtn = "0";
            }


            return rtn;

        }

        public string ConvertDateTimeToDateStringFormat(DateTime pDate, string DateStringFormat = "yyyy-MM-dd")
        {
            return pDate.ToString(DateStringFormat, CultureInfo.InvariantCulture);
        }   
        
        private void MathRoundTDData(ref List<TDData> LstTRData)
        {
            Decimal decimalSaveIn;
            if (LstTRData != null)
            {
                for (int i = 0; i < LstTRData.Count; i++)
                {
                    if (!String.IsNullOrEmpty(LstTRData[i].SaveIn))
                    {
                        decimalSaveIn = Convert.ToDecimal(LstTRData[i].SaveIn);
                        LstTRData[i].SaveIn = Math.Round(decimalSaveIn).ToString();
                    }
                }
            }
        }

        private List<TDData> MakeTDData(List<TDData> LstTRData, List<string> lstStatus)
        {
            List<TDData> rtn;
            Decimal decimalSaveIn;

            if (lstStatus != null && lstStatus.Count > 0)
            {
                rtn = LstTRData.Where(x => lstStatus.Contains(x.Status)).OrderBy(x => x.DateOrder).ToList();
            }
            else
            {
                rtn = LstTRData.OrderBy(x => x.DateOrder).ToList();
            }
            if (rtn != null)
            {
                for (int i = 0; i < rtn.Count; i++)
                {
                    if (!String.IsNullOrEmpty(rtn[i].SaveIn))
                    {
                        decimalSaveIn = Convert.ToDecimal(rtn[i].SaveIn);
                        rtn[i].SaveIn = Math.Round(decimalSaveIn).ToString("#,##0");
                    }
                }
            }

            return rtn;
        }

        private string getTotalAVG(List<TDData> LstTRData, List<string> lstStatus)
        {
            List<TDData> data = new List<TDData>();
            string rtn = "";
            decimal total = 0;
            decimal avg = 0;
            int count = 0;

            if (lstStatus != null && lstStatus.Count > 0)
            {
                data = LstTRData.Where(x => lstStatus.Contains(x.Status)).OrderBy(x => x.DateOrder).ToList();
            }
            else
            {
                data = LstTRData.OrderBy(x => x.DateOrder).ToList();
            }

            if(data != null)
            {
                foreach(var item in data)
                {
                    if(!String.IsNullOrEmpty(item.WSSaveing))
                    {
                        total += Convert.ToDecimal(item.WSSaveing);
                        count++;
                    }
                }
                if(count > 0)
                {
                    avg = total / count;
                    rtn = avg.ToString("#,##0.00");
                }
                
            }
            return rtn;
        }

        private string getTotalSUM(List<TDData> LstTRData, List<string> lstStatus)
        {
            List<TDData> data;
            string rtn = "";

            decimal total = 0;
            

            if (lstStatus != null && lstStatus.Count > 0)
            {
                data = LstTRData.Where(x => lstStatus.Contains(x.Status)).OrderBy(x => x.DateOrder).ToList();
            }
            else
            {
                data = LstTRData.OrderBy(x => x.DateOrder).ToList();
            }

           
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        if (!String.IsNullOrEmpty(item.SaveIn))
                        {
                            total += Convert.ToDecimal(item.SaveIn);
                        }
                    }
                    if (total != 0)
                    {
                        rtn = total.ToString("#,##0");
                    }

                }
            
            return rtn;
        }
    }
}