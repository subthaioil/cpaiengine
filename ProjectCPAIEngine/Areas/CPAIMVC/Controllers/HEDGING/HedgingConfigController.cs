﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingConfigController : BaseController
    {
        // GET: CPAIMVC/HedgingConfig
        [HttpGet]
        public ActionResult Index()
        {
            HedgingConfigViewModel model = initialModel();
            HedgingConfigServiceModel service = new HedgingConfigServiceModel();
            model = service.GetDetail();
            model.ddlCreditRate = DropdownServiceModel.getCreditRate();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(HedgingConfigViewModel resultmodel)
        {
            HedgingConfigServiceModel service = new HedgingConfigServiceModel();
            ReturnValue rtn = new ReturnValue();
            HedgingConfigViewModel model = initialModel();
            rtn = service.Update(ref resultmodel,lbUserName);

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            model = resultmodel;
            model.ddlCreditRate = DropdownServiceModel.getCreditRate();
            return View("Index",model);
        }

        public HedgingConfigViewModel initialModel()
        {
            HedgingConfigViewModel model = new HedgingConfigViewModel();
            model.HedgingConfig_NewData = new List<HedgingConfig_NewData>();
            model.HedgingConfig_OldData = new List<HedgingConfig_OldData>();
            model.ddlCreditRate = new List<SelectListItem>();
            return model;
        }
    }
}