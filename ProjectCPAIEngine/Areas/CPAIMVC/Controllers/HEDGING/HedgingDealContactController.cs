﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingDealContactController : BaseController
    {
        #region Action   
        [HttpGet]
        public ActionResult Search()
        {
            return RedirectToAction("Index", "HedgingDealContact", new { searchTicket = false});
        }

        [HttpGet]
        public ActionResult Index(bool searchTicket)
        {
            HedgingDealContactViewModel model = initialModel();
            model.searchTicket = searchTicket;
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgingDealContactViewModel postModel)
        {
            HedgingDealContactViewModel model = initialModel();
            return View(model);
        }
        #endregion

        #region Data
        private HedgingDealContactViewModel initialModel()
        {
            HedgingDealContactViewModel model = new HedgingDealContactViewModel();
            //model.Search = new HedgingDealContactViewModel_Search();
            //model.Search.sSearchData = new List<HedgingDealContactViewModel_SearchData>();

            model.ddlCounterParty = new List<SelectListItem>();

            return model;
        }
        #endregion
    }
}