﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingDealTicketController : BaseController
    {         
        [HttpGet]
        public ActionResult Search()
        {
            return RedirectToAction("Index", "HedgingDealContact", new { searchTicket = true });            
        }
    }
}