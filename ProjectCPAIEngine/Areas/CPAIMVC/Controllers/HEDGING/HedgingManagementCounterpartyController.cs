﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingManagementCounterpartyController : BaseController
    {
        #region Action        
        public ActionResult Index()
        {
            HedgingManagementCounterpartyViewModel model = initialModelKeyin();
            model.pageMode = "NEW";
            model.HMC_Detail.DetailCreditLimit = new List<HedgingManagementCounterpartyViewModel_DetailCreditLimit>();
            model.HMC_Detail.DetailCreditLimit.Add(new HedgingManagementCounterpartyViewModel_DetailCreditLimit { dDate = "", CreditLimit = "", Status = "ACTIVE" });
            return View(model);
        }

        public ActionResult ViewDetail(string cpID)
        {
            HedgingManagementCounterpartyViewModel model = initialModel();
            HedgingManagementCounterpartyServiceModel servicemodel = new HedgingManagementCounterpartyServiceModel();
            model.ddlActive = new List<SelectListItem>();
            model.ddlCounterParty = new List<SelectListItem>();
            model.ddlCreditRate = new List<SelectListItem>();
            model.HMC_Detail = new HedgingManagementCounterpartyViewModel_Detail();
            model.ddlCounterParty = DropdownServiceModel.getCounterParty(false, "", "CPALL");
            model.ddlCreditRating = DropdownServiceModel.getCreditRate();
            model.ddlActive = DropdownActive();
            model.pageMode = "EDIT";
            model.HMC_Detail = servicemodel.GetDetail(cpID);
            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            HedgingManagementCounterpartyViewModel model = initialModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgingManagementCounterpartyViewModel postModel)
        {
            HedgingManagementCounterpartyViewModel model = initialModel();
            HedgingManagementCounterpartyViewModel_Search search = new HedgingManagementCounterpartyViewModel_Search();
            HedgingManagementCounterpartyServiceModel service = new HedgingManagementCounterpartyServiceModel();

            search = postModel.Search;
            service.Search(ref search);
            model.Search = search;

            return View(model);
        }

        //public ActionResult Draft(string funcID)
        //{
        //    HedgingManagementCounterpartyViewModel model = new HedgingManagementCounterpartyViewModel();

        //    return RedirectToAction("Search", "HedgingManagementCounterparty");
        //}

        [HttpGet]
        public ActionResult UploadExcel()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Save(HedgingManagementCounterpartyViewModel resultmodel)
        {
            HedgingManagementCounterpartyViewModel model = initialModelKeyin();
            HedgingManagementCounterpartyServiceModel service = new HedgingManagementCounterpartyServiceModel();
            ReturnValue rtn = new ReturnValue();
            HedgingManagementCounterpartyViewModel_Detail modeldetail = new HedgingManagementCounterpartyViewModel_Detail();
            modeldetail = resultmodel.HMC_Detail;
            if (resultmodel.pageMode == "NEW")
            {
                rtn = service.Add(ref modeldetail, lbUserName);
                if (rtn.Status)
                {
                    model.HMC_Detail = modeldetail;
                }
            }
            else if (resultmodel.pageMode == "EDIT")
            {
                rtn = service.Edit(modeldetail, lbUserName);
            }

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            if (rtn.Status)
            {
                TempData["RowID"] = modeldetail.HMC_ROW_ID;
            }

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult SearchDate(string Date, string Counterparty)
        {
            HedgingManagementCounterpartyViewModel model = new HedgingManagementCounterpartyViewModel();
            HedgingManagementCounterpartyServiceModel service = new HedgingManagementCounterpartyServiceModel();
            var rData = service.GetDetailCds(Date, Counterparty);
            model.HMC_Detail = new HedgingManagementCounterpartyViewModel_Detail();
            model.HMC_Detail.DetailCds = new List<HedgingManagementCounterpartyViewModel_DetailCds>();
            model.HMC_Detail.DetailCds = rData;
            return Json(model);
        }

        [HttpPost]
        public ActionResult Filter(string filter, string Counterparty)
        {
            HedgingManagementCounterpartyViewModel model = new HedgingManagementCounterpartyViewModel();
            model.HMC_Detail = new HedgingManagementCounterpartyViewModel_Detail();
            model.HMC_Detail.DetailCreditLimit = new List<HedgingManagementCounterpartyViewModel_DetailCreditLimit>();
            HedgingManagementCounterpartyServiceModel service = new HedgingManagementCounterpartyServiceModel();
            var rData = service.GetDetailCreditLimit(filter, Counterparty);//Add
            model.HMC_Detail.DetailCreditLimit = rData;
            return Json(model);
        }

        [HttpPost]
        public ActionResult AddDate(string Date, string Counterparty)
        {
            HedgingManagementCounterpartyServiceModel serviceModel = new HedgingManagementCounterpartyServiceModel();
            string rDate = serviceModel.genCPDateForAddControl(Date, Counterparty);

            HedgingManagementCounterpartyViewModel model = new HedgingManagementCounterpartyViewModel();
            model.HMC_Detail = new HedgingManagementCounterpartyViewModel_Detail();
            model.HMC_Detail.DetailCds = new List<HedgingManagementCounterpartyViewModel_DetailCds>();
            model.HMC_Detail.DetailCds.Add(new HedgingManagementCounterpartyViewModel_DetailCds { Date = rDate, Cds = "1" });
            return Json(model);
        }

        public ActionResult Upload(FormCollection form)
        {
            HedgingManagementCounterpartyServiceModel serviceModel = new HedgingManagementCounterpartyServiceModel();
            HttpPostedFileBase upload = Request.Files.Count > 0 ? Request.Files[0] : null;
            string fileName = "";
            string filePath = "";
            if (upload.ContentLength > 0)
            {
                string type = ConstantPrm.SYSTEM.FREIGHT;
                string FNID = ConstantPrm.FUNCTION.F10000008;
                string typename = Path.GetExtension(upload.FileName).ToLower();
                fileName = string.Format("{0}_{2}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, FNID);
                filePath = UploadFile(upload, "Web/FileUpload/HedgingManagementCounterparty", fileName + typename);

                if (upload.ContentLength > 0)
                {
                    Thread thread = new Thread(() => serviceModel.UploadExcel(fileName, filePath, lbUserName));
                    thread.Start();
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }

            return RedirectToAction("UploadExcel", "HedgingManagementCounterparty");
        }
        #endregion

        #region Data
        public string UploadFile(HttpPostedFileBase requestFile, string uploadPath, string uploadFileName)
        {
            string path = "";
            if (Request != null)
            {
                var file = requestFile;

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string rootPath = Request.PhysicalApplicationPath;
                    string subPath = uploadPath;

                    fileName = uploadFileName;
                    if (!Directory.Exists(rootPath + subPath))
                        Directory.CreateDirectory(rootPath + subPath);
                    path = Path.Combine(Server.MapPath("~/" + uploadPath), fileName);
                    file.SaveAs(path);
                    return path;
                }
                else
                {
                    return path;
                }
            }
            else
            {
                return path;
            }
        }

        private HedgingManagementCounterpartyViewModel initialModel()
        {
            HedgingManagementCounterpartyViewModel model = new HedgingManagementCounterpartyViewModel();

            model.ddlCounterParty = DropdownServiceModel.getCounterParty(false, "", "CPALL");
            model.ddlCreditRating = DropdownServiceModel.getCreditRate(false, "");
            model.ddlStatus = DropdownServiceModel.getStatusManagementCounterparty(false, "");

            return model;
        }

        private HedgingManagementCounterpartyViewModel initialModelKeyin()
        {
            HedgingManagementCounterpartyViewModel model = new HedgingManagementCounterpartyViewModel();
            model.ddlActive = new List<SelectListItem>();
            model.ddlCounterParty = new List<SelectListItem>();
            model.ddlCreditRate = new List<SelectListItem>();
            model.HMC_Detail = new HedgingManagementCounterpartyViewModel_Detail();
            model.ddlCounterParty = DropdownServiceModel.getCounterParty(false, "", "CP");
            model.ddlCreditRating = DropdownServiceModel.getCreditRate();
            model.ddlActive = DropdownActive();
            return model;
        }

        public List<SelectListItem> DropdownActive()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "ACTIVE", Text = "ACTIVE" });
            data.Add(new DropdownServiceModel_Data { Value = "INACTIVE", Text = "INACTIVE" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }
        #endregion

    }
}