﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingPreDealController : BaseController
    {
        #region Action
        public string MCCTypeSS
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : string.Empty; }
            set { Session["Type"] = value; }
        }

        public ActionResult Index()
        {
            HedgingPreDealViewModel model = initialModel(MCCTypeSS.Decrypt());
            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            HedgingPreDealViewModel model = initialModel(MCCTypeSS.Decrypt());
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgingPreDealViewModel postModel)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingPreDealViewModel model = initialModel(postModel.systemType);
            HedgingPreDealViewModel_Search search = new HedgingPreDealViewModel_Search();
            HedgingPreDealServiceModel service = new HedgingPreDealServiceModel();

            search = postModel.HedgingPreDeal_Search;
            search.systemType = postModel.systemType;
            rtn = service.Search(ref search);
            model.HedgingPreDeal_Search = search;
            modifyField(ref model);

            TempData["ReponseStatus"] = rtn.Status;
            TempData["ReponseMessage"] = rtn.Message;
            return View(model);
        }
        #endregion

        #region Data
        public HedgingPreDealViewModel initialModel(string systemType)
        {
            HedgingPreDealViewModel resultModel = new HedgingPreDealViewModel();
            resultModel.HedgingPreDeal_Search = new HedgingPreDealViewModel_Search();
            resultModel.HedgingPreDeal_Search.sSearchData = new List<HedgingPreDealViewModel_SearchData>();

            resultModel.systemType = systemType;
            resultModel.ddlStatus = DropdownServiceModel.getStatus(false, string.Empty, DropdownServiceModel.HedgeStatus.PreDeal);
            resultModel.ddlHedged_Type = DropdownServiceModel.getHedgeType(false, string.Empty);
            resultModel.ddlTool = DropdownServiceModel.getTool(false, string.Empty); 
            resultModel.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            resultModel.ddlVS = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            resultModel.ddlCompany = DropdownServiceModel.getCompanyForDeal(false, string.Empty, "HG_CP_TOP");
            resultModel.ddlCounterParty = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, "HG_CP_OTH");
            resultModel.ddlTrade_For = DropdownServiceModel.getTradeFor(false, string.Empty);            

            return resultModel;
        }

        private void modifyField(ref HedgingPreDealViewModel model)
        {
            if (model.HedgingPreDeal_Search.sSearchData != null)
            {
                foreach (var item in model.HedgingPreDeal_Search.sSearchData)
                {
                    var nameTool = model.ddlTool.SingleOrDefault(a => a.Value == item.dTool);
                    var nameUnderly = model.ddlUnderlying.SingleOrDefault(a => a.Value == item.dUnderlying);
                    var nameHedged = model.ddlHedged_Type.SingleOrDefault(a => a.Value == item.dHedged_Type);
                    var nameCompany = model.ddlCompany.SingleOrDefault(a => a.Value == item.dSeller);
                    var nameCounterparty = model.ddlCounterParty.SingleOrDefault(a => a.Value == item.dBuyer);
                    var nameTradeFor = model.ddlTrade_For.SingleOrDefault(a => a.Value == item.dTrade_For);

                    if (nameTool != null)
                        item.dTool = nameTool.Text;
                    if (nameUnderly != null)
                        item.dUnderlying = nameUnderly.Text;
                    if (nameHedged != null)
                        item.dHedged_Type = nameHedged.Text;
                    if (nameCompany != null)
                        item.dSeller = nameCompany.Text;
                    if (nameCounterparty != null)
                        item.dBuyer = nameCounterparty.Text;
                    if (nameTradeFor != null)
                        item.dTrade_For = nameTradeFor.Text;
                }
            }
        }
        #endregion
    }
}