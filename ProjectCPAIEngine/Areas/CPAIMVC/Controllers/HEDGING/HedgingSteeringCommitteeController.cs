﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingSteeringCommitteeController : BaseController
    {
        #region Action
        public ActionResult Index()
        {
            return RedirectToAction("Search", "HedgingSteeringCommittee");
        }

        [HttpGet]
        public ActionResult Search()
        {
            HedgingSteeringCommitteeViewModel model = initialModel(string.Empty);

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgingSteeringCommitteeViewModel postModel)
        {
            HedgingSteeringCommitteeViewModel model = initialModel(string.Empty);            
            HedgingSteeringCommitteeViewModel_Search search = new HedgingSteeringCommitteeViewModel_Search();
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();

            search = postModel.search;
            service.Search(ref search);
            model.search = search;

            return View(model);
        }        

        [HttpGet]
        public ActionResult Create()
        {
            HedgingSteeringCommitteeViewModel model = initialModel("Create");                   
            DateTime dt = DateTime.Now;
            
            model.detailList = new HedgingSteeringCommitteeViewModel_DetailList();
            model.detailList._pageMode = "Create";
            model.detailList.Underlying = "";
            model.detailList.CropPlan = dt.ToString("yyyy");
            model.detailList.detail = new List<HedgingSteeringCommitteeViewModel_Detail>();     
            addDetail(model.detailList);

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(HedgingSteeringCommitteeViewModel postModel)
        {
            HedgingSteeringCommitteeViewModel model = initialModel(postModel.detailList._pageMode);
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();
            ReturnValue rtn = new ReturnValue();

            model.detailList = postModel.detailList;
            if (model.detailList._funcID != null)
            {
                if (model.detailList._funcID.Equals("0"))
                {
                    deleteDetailData(model.detailList._rowID, model.detailList);
                }
                else if (model.detailList._funcID.Equals("1"))
                {
                    if (string.IsNullOrEmpty(model.detailList._rowID))
                        addDetail(model.detailList);
                    else
                        addDetailData(Convert.ToInt32(model.detailList._rowID), model.detailList);
                }
                else if (model.detailList._funcID.Equals("2"))
                {                    
                    rtn = service.Update(model.detailList, lbUserName);
                    TempData["ReponseMessage"] = rtn.Message;
                    TempData["ReponseStatus"] = rtn.Status;
                    if (rtn.Status)
                    {
                        return RedirectToAction("Edit", "HedgingSteeringCommittee", new { underlying = model.detailList.Underlying, cropPlan = model.detailList.CropPlan });                        
                    }
                }
            }

            return View("Create", model);
        }        

        [HttpGet]
        public ActionResult Edit(string underlying, string cropPlan)
        {
            HedgingSteeringCommitteeViewModel model = initialModel("Edit");
            HedgingSteeringCommitteeViewModel_DetailList detailList = new HedgingSteeringCommitteeViewModel_DetailList();
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();
                        
            detailList = service.GetDetail(underlying, cropPlan);
            detailList._pageMode = "Edit";
            model.detailList = detailList;

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Draft(string funcID, HedgingSteeringCommitteeViewModel_DetailList jsonModel)
        {
            HedgingSteeringCommitteeViewModel model = new HedgingSteeringCommitteeViewModel();
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();
            ReturnValue rtn = new ReturnValue();

            model.detailList = jsonModel;//(HedgingSteeringCommitteeViewModel_DetailList)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonModel, typeof(HedgingSteeringCommitteeViewModel_DetailList));            
            rtn = service.Update(model.detailList, lbUserName);

            return Json(model);
        }
        #endregion

        #region Data
        private HedgingSteeringCommitteeViewModel initialModel(string pageMode)
        {
            HedgingSteeringCommitteeViewModel model = new HedgingSteeringCommitteeViewModel();
            List<SelectListItem> underlyingNew = new List<SelectListItem>();
            List<string> underlyingCommittee = new HedgingSteeringCommitteeServiceModel().getUnderlyingCommittee();            

            model.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            model.ddlCropPlan = DropdownServiceModel.getYearCrop(false, "", "", 5, 5);
            model.ddlActiveDate_FromMonth = DropdownServiceModel.getMonth(false, "", true, true);
            model.ddlActiveDate_FromYear = DropdownServiceModel.getYearCrop(false, "", "", 5, 5);
            model.ddlActiveDate_ToMonth = DropdownServiceModel.getMonth(false, "", true, true);
            model.ddlActiveDate_ToYear = DropdownServiceModel.getYearCrop(false, "", "", 5, 5);

            if (pageMode.Trim().ToLower().IndexOf("create") > -1)
            {
                foreach (var item in model.ddlUnderlying)
                {
                    if (!underlyingCommittee.Contains(item.Value.Trim()))
                        underlyingNew.Add(new SelectListItem { Text = item.Text, Value = item.Value });
                }
                model.ddlUnderlying = underlyingNew;
            }

            return model;
        }        

        private void addDetail(HedgingSteeringCommitteeViewModel_DetailList detailList)
        {
            HedgingSteeringCommitteeViewModel_Detail detail;
            List<HedgingSteeringCommitteeViewModel_DetailData> detailData = new List<HedgingSteeringCommitteeViewModel_DetailData>();
            DateTime dt = DateTime.Now;
            
            detailData.Add(new HedgingSteeringCommitteeViewModel_DetailData
            {
                OrderID = "",
                RowID = "",
                Order = "1",
                Price = "",
                Volume = ""
            });
            detail = new HedgingSteeringCommitteeViewModel_Detail
            {                
                ActiveDate_FromMonth = dt.ToString("MM"),
                ActiveDate_FromYear = dt.ToString("yyyy"),
                ActiveDate_ToMonth = dt.ToString("MM"),
                ActiveDate_ToYear = dt.ToString("yyyy"),
                DetailData = detailData
            };

            detailList.detail.Add(detail);
        }

        private void addDetailData(int rowID, HedgingSteeringCommitteeViewModel_DetailList detailList)
        {
            detailList.detail[rowID].DetailData.Add(new HedgingSteeringCommitteeViewModel_DetailData
            {
                OrderID = detailList.detail[rowID].OrderID,
                RowID = "",
                Order = (detailList.detail[rowID].DetailData.Count + 1).ToString(),
                Price = "",
                Volume = ""
            });
        }

        private void deleteDetailData(string rowID, HedgingSteeringCommitteeViewModel_DetailList detailList)
        {
            int indexDetail = Convert.ToInt32(rowID);
            int indexDetailData = detailList.detail[Convert.ToInt32(rowID)].DetailData.Count - 1;

            if (indexDetailData > 0)
            {                
                detailList.detail[Convert.ToInt32(rowID)].DetailData.RemoveAt(indexDetailData);                
            }
            else
            {
                if ((indexDetail > 0) || (detailList.detail.Count - 1) > 0)
                    detailList.detail.RemoveAt(indexDetail);
            }
        }
        #endregion
    }
}