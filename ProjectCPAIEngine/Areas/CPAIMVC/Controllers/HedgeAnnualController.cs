﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Model;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;
using System.Globalization;
using ProjectCPAIEngine.DAL;
using com.pttict.engine.dal.Entity;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

using DevExpress.Spreadsheet;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeAnnualController : BaseController
    {
        public HedgeAnnualSearchViewModel modelSearch
        {
            get { return TempData["HedgeAnnualSearchViewModel"] != null ? (HedgeAnnualSearchViewModel)TempData["HedgeAnnualSearchViewModel"] : new HedgeAnnualSearchViewModel(); }
            set { TempData["HedgeAnnualSearchViewModel"] = value; }
        }

        public HedgeAnnualViewModel modelView
        {
            get { return TempData["HedgeAnnualViewModel"] as HedgeAnnualViewModel; }
            set { TempData["HedgeAnnualViewModel"] = value; }
        }

        public List<ButtonAction> ButtonListHedgeAnnual
        {
            get
            {
                if (Session["ButtonListHedgeAnnual"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListHedgeAnnual"];
            }
            set { Session["ButtonListHedgeAnnual"] = value; }
        }

        ShareFn _FN = new ShareFn();
        const string returnPage = "/web/MainBoards.aspx";
        const string JSON_HEDG = "JSON_HEDG";

        // GET: CPAIMVC/HedgingFramework
        public ActionResult Index()
        {
            return RedirectToAction("Search");
        }

        public ActionResult Search()
        {
            HedgeAnnualSearchViewModel model;
            model = modelSearch;

            initializeDropDownList(ref model);
            return View(model);
        }


        public ActionResult Create()
        {
            HedgeAnnualViewModel model = new HedgeAnnualViewModel();
            if (modelView != null)
            {
                model = modelView;
                modelView = null;
            }
            model.pageMode = "EDIT";
            model.buttonMode = "AUTO";
            initializeDropDownList(ref model);
            return View(model);
        }

        public ActionResult Edit()
        {
            HedgeAnnualViewModel model = new HedgeAnnualViewModel();
            if (modelView == null)
            {
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    string reqID = Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
                    ResponseData resData = LoadDataHedgeAnnual(tranID, ref model);
                    if (resData != null)
                    {
                        model.taskID = tranID;
                        model.reqID = reqID;
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
            }
            else
            {
                model = modelView;
                modelView = null;
            }
            model.pageMode = "EDIT";
            model.buttonMode = "MANUAL";
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        public ActionResult History()
        {
            HedgeAnnualViewModel model = new HedgeAnnualViewModel();

            if (modelView == null)
            {
                model = new HedgeAnnualViewModel();
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    string reqID = Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
                    ResponseData resData = LoadDataHedgeAnnual(tranID, ref model);
                    if (resData != null)
                    {
                        model.taskID = tranID;
                        model.reqID = reqID;
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
            }
            else
            {
                model = modelView;
                modelView = null;
            }
            model.pageMode = "READ";
            model.buttonMode = "AUTO";
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        public ActionResult SearchResult(FormCollection fm, HedgeAnnualSearchViewModel model)
        {
            string approved_date_start = DateSplit(model.approved_date, true);
            string appruved_date_end = DateSplit(model.approved_date, false);
            string update_date_start = DateSplit(model.update_date, true);
            string update_date_end = DateSplit(model.update_date, false);

            var activationddl = getActivationStatus(isOnlyVisibility: true);
            List<string> sort_activation = new List<string>();
            foreach (var item in activationddl)
            {
                sort_activation.Add(item.Value);
            }

            string approved_date = !string.IsNullOrEmpty(approved_date_start) ? approved_date_start + "|" + appruved_date_end : "";
            string update_date = !string.IsNullOrEmpty(update_date_start) ? update_date_start + "|" + update_date_end : "";
            List_HedgeAnnualtrx res = SearchHedgeAnnualData(
                        company: model.trading_book,
                        approved_date: approved_date,
                        updated_date: update_date,
                        updated_by: model.update_by,
                        status: model.status
                );
            model.HedgeAnnualTransaction = res.HedgeAnnualTransaction.Where(x => sort_activation.Where(a => a == x.activation_status).Any()).OrderByDescending(y => y.transaction_id).ToList();
            foreach (var item in model.HedgeAnnualTransaction)
            {
                item.Transaction_id_Encrypted = item.transaction_id.Encrypt();
                item.Req_transaction_id_Encrypted = item.req_transaction_id.Encrypt();
            }

            modelSearch = model;
            return RedirectToAction("Search");
        }


        private List_HedgeAnnualtrx SearchHedgeAnnualData(string system = "", string type = "", string action = "", string status = "",
                                                          string create_by = "", string company = "", string version = "", string approved_date = "",
                                                          string activation_status = "", string updated_date = "", string updated_by = "")
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000068;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();

            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = HEDGE_ANNUAL_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = company });
            req.Req_parameters.P.Add(new P { K = "index_11", V = version });
            req.Req_parameters.P.Add(new P { K = "index_12", V = approved_date });
            req.Req_parameters.P.Add(new P { K = "index_13", V = status });
            req.Req_parameters.P.Add(new P { K = "index_14", V = updated_date });
            req.Req_parameters.P.Add(new P { K = "index_15", V = updated_by });

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_HedgeAnnualtrx _model = ShareFunction.DeserializeXMLFileToObject<List_HedgeAnnualtrx>(_DataJson);
            if (_model == null)
            {
                _model = new List_HedgeAnnualtrx();
            }

            if (_model.HedgeAnnualTransaction == null)
            {
                _model.HedgeAnnualTransaction = new List<HedgeAnnualEncrypt>();
            }
            else
            {
                foreach (var HedgeAnnualTransaction in _model.HedgeAnnualTransaction)
                {
                    HedgeAnnualTransaction.latest_version = HedgeAnnualServiceModel.GetHedgeAnnualLatestVersion(HedgeAnnualTransaction.transaction_id);
                }
            }

            return _model;
        }


        public string DateSplit(string date, bool start = true)
        {
            string dateSplit = string.Empty;
            if (!string.IsNullOrEmpty(date))
            {
                string[] s = date.Split(new[] { " to " }, StringSplitOptions.None);
                if (start)
                {
                    dateSplit = s[0];
                }
                else
                {
                    dateSplit = s[1];
                }
            }
            return dateSplit;
        }

        private List<SelectListItem> getUnitPrice()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unit_price.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.unit_price[i].value, Value = setting.unit_price[i].key });
            }
            return list;
        }

        private List<SelectListItem> getUnitVolume()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 0; i < setting.unit_volume.Count; i++)
            {
                var unit_price = setting.unit_price.Any(x => x.mapping == setting.unit_volume[i].key) ? setting.unit_price.Where(x => x.mapping == setting.unit_volume[i].key).FirstOrDefault() : new Unit_Price();
                list.Add(new SelectListItem { Text = setting.unit_volume[i].value, Value = unit_price.key });
            }
            return list;
        }

        private List<SelectListItem> getMinMax()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.min_max.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.min_max[i].value, Value = setting.min_max[i].key });
            }
            return list;
        }

        private List<SelectListItem> getActivationStatus(bool isOnlyVisibility = false)
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.activation_status.Count; i++)
            {
                if (isOnlyVisibility)
                {
                    if (setting.activation_status[i].visibility == "Y")
                    {
                        list.Add(new SelectListItem { Text = setting.activation_status[i].value, Value = setting.activation_status[i].key });
                    }
                }
                else
                {
                    list.Add(new SelectListItem { Text = setting.activation_status[i].value, Value = setting.activation_status[i].key });
                }

            }
            return list;
        }

        public void initializeDropDownList(ref HedgeAnnualSearchViewModel model)
        {
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            model.ddlTrading = service.GetHedgeCompany();
            model.ddlStatus = getActivationStatus(isOnlyVisibility: true);
        }

        public void initializeDropDownList(ref HedgeAnnualViewModel model)
        {
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            model.ddlTrading = service.GetHedgeCompany(model.trading_book);
            model.ddlHedgeType = service.GetHedgeType();
            model.ddlHedgeProduct = service.GetHedgeProduct();
            model.ddlHedgeUnitPrice = getUnitPrice();
            model.ddlHedgeUnitVolume = getUnitVolume();
            model.ddlHedgeMinMax = getMinMax();
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref HedgeAnnualViewModel model)
        {
            model.buttonMode = "MANUAL";
            model.pageMode = "READ";
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count > 0)
            {
                model.pageMode = "EDIT";
            }
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SUBMIT).ToList().Count > 0)
            {
                model.pageMode = "EDIT";
            }
            foreach (ButtonAction _button in Button)
            {
                string _btn = "";
                if (_button.name.ToUpper() == ConstantPrm.ACTION.SUBMIT)
                {
                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='button-control btn btn-success validate-button' style='margin-right: -350px;background:#0a7ab1;' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                }
                else if (_button.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT)
                {
                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='button-control btn btn-success validate-button' style='background:#0a7ab1;' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                }
                else
                {
                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                }
                model.buttonCode += _btn;
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            ButtonListHedgeAnnual = Button;
        }

        private ResponseData SaveDataHedgeAnnual(DocumentActionStatus _status, HedgeAnnualViewModel model, string note = "", string json_fileUpload = "")
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }

            HedgingRootObject obj = new HedgingRootObject()
            {
                hedg_annual_data = new HedgAnnualData()
                {
                    trading_book = model.trading_book,
                    approved_date = model.approved_date,
                    version = model.version
                },
                hedg_annual_submit_note = new HedgAnnualSubmitNote()
                {
                    subject = model.hedge_annual_submit_note.subject,
                    approved_date = model.hedge_annual_submit_note.approved_date,
                    ref_no = model.hedge_annual_submit_note.ref_no,
                    note = model.hedge_annual_submit_note.note,
                    attach_file = model.hedge_annual_submit_note.attach_file
                },
                hedg_annual_type = new HedgAnnualType()
                {
                    hedg_type_data = model.hedge_annual_types.Select(hedge_annual_type => new HedgTypeData()
                    {
                        hedg_type_key = hedge_annual_type.hedge_type,
                        hedg_type_detail = hedge_annual_type.hedge_annual_type_details.Select(hedge_annual_type_detail => new HedgTypeDetail()
                        {
                            key = hedge_annual_type_detail.key,
                            order = hedge_annual_type_detail.order,
                            product_front = hedge_annual_type_detail.product_front,
                            product_back = hedge_annual_type_detail.product_back,
                            title = hedge_annual_type_detail.title,
                            approved_date = hedge_annual_type_detail.approved_date,
                            unit_price = hedge_annual_type_detail.unit_price,
                            unit_volume = hedge_annual_type_detail.unit_volume,
                            year = hedge_annual_type_detail.year,
                            min_max = hedge_annual_type_detail.min_max,
                            price_year = hedge_annual_type_detail.price_year,
                            price_Q1 = hedge_annual_type_detail.price_Q1,
                            price_Q2 = hedge_annual_type_detail.price_Q2,
                            price_Q3 = hedge_annual_type_detail.price_Q3,
                            price_Q4 = hedge_annual_type_detail.price_Q4,
                            prod_volume = hedge_annual_type_detail.prod_volume,
                            limit_volume = hedge_annual_type_detail.limit_volume,
                            approved_volume = hedge_annual_type_detail.approved_volume,
                            note = hedge_annual_type_detail.note
                        }).ToList()
                    }).ToList()
                },
                hedg_annual_note = model.note,
                attach_file = model.attach_file
            };

            //Assign value here

            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000070;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.HEDG });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEM.HEDG });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = "N" });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            resData = service.CallService(req);
            return resData;
        }

        private ResponseData LoadDataHedgeAnnual(string TransactionID, ref HedgeAnnualViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000069;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEM.HEDG });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                HedgingRootObject data_model = new JavaScriptSerializer().Deserialize<HedgingRootObject>(_model.data_detail);
                HedgeAnnualServiceModel serviceModel = new HedgeAnnualServiceModel();
                model = new HedgeAnnualViewModel()
                {
                    trading_book = data_model.hedg_annual_data.trading_book,
                    approved_date = data_model.hedg_annual_data.approved_date,
                    note = data_model.hedg_annual_note,
                    version = data_model.hedg_annual_data.version,
                    status = data_model.hedg_annual_data.status,
                    attach_file = data_model.attach_file,

                    hedge_annual_submit_note = new HedgAnnualSubmitNote()
                    {
                        subject = data_model.hedg_annual_submit_note.subject,
                        approved_date = data_model.hedg_annual_submit_note.approved_date,
                        note = data_model.hedg_annual_submit_note.note,
                        attach_file = data_model.hedg_annual_submit_note.attach_file,
                        ref_no = data_model.hedg_annual_submit_note.ref_no
                    },
                    hedge_type = data_model.hedg_annual_type.hedg_type_data.Any() ? data_model.hedg_annual_type.hedg_type_data.FirstOrDefault().hedg_type_key : string.Empty,
                    hedge_annual_types = data_model.hedg_annual_type.hedg_type_data.Select(hedg_type_data => new HedgeAnnualTypeViewModel()
                    {
                        hedge_type = hedg_type_data.hedg_type_key,
                        hedge_type_name = serviceModel.GetHedgeType().Any(x => x.Value == hedg_type_data.hedg_type_key) ? serviceModel.GetHedgeType().Where(x => x.Value == hedg_type_data.hedg_type_key).FirstOrDefault().Text : string.Empty,
                        hedge_annual_type_details = hedg_type_data.hedg_type_detail.OrderBy(hedg_type_detail => hedg_type_detail.order).Select(hedg_type_detail => new HedgeAnnualTypeDetailViewModel()
                        {
                            action = ConstantPrm.ACTION.VIEW,
                            key = hedg_type_detail.key,
                            order = hedg_type_detail.order,
                            product_front = hedg_type_detail.product_front,
                            product_back = hedg_type_detail.product_back,
                            title = hedg_type_detail.title,
                            approved_date = hedg_type_detail.approved_date,
                            unit_price = hedg_type_detail.unit_price,
                            unit_volume = hedg_type_detail.unit_volume,
                            year = hedg_type_detail.year,
                            min_max = hedg_type_detail.min_max,
                            price_year = hedg_type_detail.price_year,
                            price_Q1 = hedg_type_detail.price_Q1,
                            price_Q2 = hedg_type_detail.price_Q2,
                            price_Q3 = hedg_type_detail.price_Q3,
                            price_Q4 = hedg_type_detail.price_Q4,
                            prod_volume = hedg_type_detail.prod_volume,
                            limit_volume = hedg_type_detail.limit_volume,
                            approved_volume = hedg_type_detail.approved_volume,
                            note = hedg_type_detail.note
                        }).ToList()
                    }).ToList()
                };
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                model.buttonMode = "MANUAL";
                model.pageMode = "READ";
            }

            //string _btnBack = "<input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("BACK") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" />";
            //model.buttonCode += _btnBack;

            return resData;
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form, HedgeAnnualViewModel model)
        {
            return RedirectToAction("Search");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, HedgeAnnualViewModel model)
        {
            if (ButtonListHedgeAnnual != null)
            {
                var _lstButton = ButtonListHedgeAnnual.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    string name = _lstButton[0].name;

                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = model.reqID; //Request.QueryString["Tran_Req_ID"] == null ? "" : Request.QueryString["Tran_Req_ID"].ToString();
                    string TranID = model.taskID; //Request.QueryString["TranID"] == null ? "" : Request.QueryString["TranID"].ToString();


                    HedgingRootObject data_model = new HedgingRootObject()
                    {
                        hedg_annual_data = new HedgAnnualData()
                        {
                            trading_book = model.trading_book,
                            approved_date = model.approved_date,
                            version = model.version,
                            status = model.status
                        },
                        hedg_annual_submit_note = new HedgAnnualSubmitNote()
                        {
                            subject = model.hedge_annual_submit_note.subject,
                            approved_date = model.hedge_annual_submit_note.approved_date,
                            ref_no = model.hedge_annual_submit_note.ref_no,
                            note = model.hedge_annual_submit_note.note,
                            attach_file = model.hedge_annual_submit_note.attach_file
                        },
                        hedg_annual_type = new HedgAnnualType()
                        {
                            hedg_type_data = model.hedge_annual_types.Select(hedge_annual_type => new HedgTypeData()
                            {
                                hedg_type_key = hedge_annual_type.hedge_type,
                                hedg_type_detail = hedge_annual_type.hedge_annual_type_details.Select(hedge_annual_type_detail => new HedgTypeDetail()
                                {
                                    key = hedge_annual_type_detail.key,
                                    order = hedge_annual_type_detail.order,
                                    product_front = hedge_annual_type_detail.product_front,
                                    product_back = hedge_annual_type_detail.product_back,
                                    title = hedge_annual_type_detail.title,
                                    approved_date = hedge_annual_type_detail.approved_date,
                                    unit_price = hedge_annual_type_detail.unit_price,
                                    unit_volume = hedge_annual_type_detail.unit_volume,
                                    year = hedge_annual_type_detail.year,
                                    min_max = hedge_annual_type_detail.min_max,
                                    price_year = hedge_annual_type_detail.price_year,
                                    price_Q1 = hedge_annual_type_detail.price_Q1,
                                    price_Q2 = hedge_annual_type_detail.price_Q2,
                                    price_Q3 = hedge_annual_type_detail.price_Q3,
                                    price_Q4 = hedge_annual_type_detail.price_Q4,
                                    prod_volume = hedge_annual_type_detail.prod_volume,
                                    limit_volume = hedge_annual_type_detail.limit_volume,
                                    approved_volume = hedge_annual_type_detail.approved_volume,
                                    note = hedge_annual_type_detail.note
                                }).ToList()
                            }).ToList()
                        },
                        hedg_annual_note = model.note,
                        attach_file = model.attach_file
                    };

                    string json = new JavaScriptSerializer().Serialize(data_model);

                    if (model.status == "ACTIVE")
                    {
                        //generate a new transaction and attach this tran id in note.
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = TranID });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = "" });
                        _param.Add(new ReplaceParam() { ParamName = "CPAIVerifyRequireInputContinueState", ParamVal = "" });
                    }
                    else
                    {
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = TranID });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = "" });
                    }

                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);

                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string path = string.Format("~/CPAIMVC/HedgeAnnual/Edit?TranID={0}&Tran_Req_ID={1}", tranID, reqID);
                            return Redirect(path);
                        }
                        else
                        {
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            modelView = model;
            return RedirectToAction("Edit");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "AddHedgeAnnualType")]
        public ActionResult AddHedgeAnnualType(FormCollection form, HedgeAnnualViewModel model)
        {
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            if (!model.hedge_annual_types.Any(i => i.hedge_type == model.hedge_type))
            {
                var hedge_type = service.GetHedgeType().Where(i => i.Value == model.hedge_type).FirstOrDefault();
                if (hedge_type != null)
                {
                    var value = hedge_type.Value;
                    var name = hedge_type.Text;
                    var order = string.Format("{0}", (model.hedge_annual_types.Count + 1));
                    HedgeAnnualTypeViewModel hedge_annual_type = new HedgeAnnualTypeViewModel()
                    {
                        hedge_annual_type_details = new List<HedgeAnnualTypeDetailViewModel>() { new HedgeAnnualTypeDetailViewModel() { action = ConstantPrm.ACTION.EDIT } },
                        hedge_type = value,
                        hedge_type_name = name,
                        hedge_type_order = order
                    };
                    model.hedge_annual_types.Add(hedge_annual_type);
                }
            }
            initializeDropDownList(ref model);
            //model.ddlHedgeType = model.ddlHedgeType.Where(a => model.hedge_annual_types.Where(x => x.hedge_type_name != a.Text).Any());
            return View("Create", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "DeleteHedgeAnnualType")]
        public ActionResult DeleteHedgeAnnualType(FormCollection form, HedgeAnnualViewModel model)
        {
            if (model.hedge_type != null)
            {
                model.hedge_annual_types.RemoveAll(i => i.hedge_type == model.hedge_type);
                model.hedge_annual_types = model.hedge_annual_types.OrderBy(o => Convert.ToDecimal(o.hedge_type_order)).ToList();
                for (int i = 0; i < model.hedge_annual_types.Count; i++)
                {
                    model.hedge_annual_types[i].hedge_type_order = string.Format("{0}", (i + 1));
                }
                if (model.hedge_annual_types.Any())
                {
                    model.hedge_type = model.hedge_annual_types.FirstOrDefault().hedge_type;
                }
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveHedgeAnnual")]
        public ActionResult SaveHedgeAnnual(FormCollection form, HedgeAnnualViewModel model)
        {
            DocumentActionStatus status = new DocumentActionStatus();
            if (model.action == "draft")
            {
                status = DocumentActionStatus.Draft;
            }
            else if (model.action == "submit")
            {
                status = DocumentActionStatus.Submit;
            }
            ResponseData resData = SaveDataHedgeAnnual(status, model);
            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string path = string.Format("~/CPAIMVC/HedgeAnnual/Edit?TranID={0}&Tran_Req_ID={1}", tranID, reqID);
                    return Redirect(path);
                }
            }
            modelView = model;
            return RedirectToAction("Create");
        }


        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenerateExcel")]
        public ActionResult GenerateExcel(HedgeAnnualViewModel model)
        {
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            ExportExcel(model, "GenExcel");
            model.ddlTrading = service.GetHedgeCompany();
            return View("Create", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GeneratePdf")]
        public ActionResult GeneratePdf(HedgeAnnualViewModel model)
        {
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            ExportExcel(model, "GenPdf");

            model.ddlTrading = service.GetHedgeCompany();
            return View("Create", model);
        }




        public void ExportExcel(HedgeAnnualViewModel model, string type)
        {
            string physical_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "HedgeCDSImport", "Hedge_Annual\\");

            if (!Directory.Exists(physical_path))
            {
                DirectoryInfo di = Directory.CreateDirectory(physical_path);
            }
            String file_Name = "Annual FW Excel Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            String xlsTemp = physical_path + file_Name;
            FileInfo current_file = new FileInfo(xlsTemp);
            string Trading_bookName = "";
            string Approved_date = "";
            string Version = "";
           
            int rowidx = 9;
            string hedge_type_name = "";
            string Trading_bookCode = model.trading_book;
            string Note = "";
            string AttachFile = "";
            DateTime approved_date = DateTime.ParseExact(model.approved_date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            Approved_date = approved_date.ToString("MMMM  dd  yyyy");
            Version = model.version;
            Note = model.note;
            AttachFile = model.attach_file;
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();

            Trading_bookName = service.GetHedgeCompanyName(Trading_bookCode);
            if (!current_file.Exists)
            {
                current_file.Delete();
            }
            FileInfo excelFile = new FileInfo(xlsTemp);
            ExcelPackage package = new ExcelPackage(excelFile);

            ExcelWorksheet ws = package.Workbook.Worksheets.Add("Layout1");
            ws.View.ShowGridLines = false;
            ws.Cells[1, 1].Value = "Annual Hedging Framework";
            ws.Cells["A3"].Value = "Trading Book*:";
            ws.Cells["A4"].Value = "Approved Date:";
            ws.Cells["A5"].Value = "Version *:";
            //ws.Cells["A28"].Value = "Note:";
            //ws.Cells["A30"].Value = "AttachFile:";
            ws.Cells["D3"].Value = Trading_bookName;
            ws.Cells["D4"].Value = Approved_date;
            ws.Cells["D5"].Value = Version;
            //ws.Cells["S1"].Value = string.Format(Page_number, 1, 1);
            //ws.Cells["C28"].Value = Note;
            //ws.Cells["C30"].Value = AttachFile;
            cellHeader(7, ref ws);


            List<HedgeAnnualTypeViewModel> hedge_annual_types = model.hedge_annual_types;
            List<HedgeAnnualTypeDetailViewModel> hedge_annual_type_details = new List<HedgeAnnualTypeDetailViewModel>();
            foreach (var hedge_types in hedge_annual_types)
            {
                hedge_annual_type_details = hedge_types.hedge_annual_type_details;
                hedge_type_name = hedge_types.hedge_type_name;
                int rowcount = hedge_annual_type_details.Count;
                foreach (var item in hedge_annual_type_details)
                {
                    #region countcell
                    //ws.Cells["A" + rowidx + ""].Value = "I= "+ i;
                    //ws.Cells["C" + rowidx + ""].Value = "I= " + i;
                    //ws.Cells["D" + rowidx + ""].Value = "I= " + i;
                    //ws.Cells["E" + rowidx + ""].Value = "I= " + i;
                    //ws.Cells["E" + rowidx + ""].Value = "I= " + i;
                    //ws.Cells["F" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["G" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["H" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["I" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["J" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["K" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["L" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["M" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["N" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["O" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["P" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["Q" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["R" + rowidx + ""].Value = "rowidx" + rowidx;
                    //ws.Cells["S" + rowidx + ""].Value =  "rowidx"+rowidx;

                    #endregion
                    var product_front = service.GetHedgeProductname(item.product_front);
                    var product_back = service.GetHedgeProductname(item.product_back);

                    ws.Cells["A" + rowidx + ""].Value = product_front;
                    ws.Cells["A" + rowidx + ""].Style.WrapText = true;
                    ws.Cells["C" + rowidx + ""].Value = product_back;
                    ws.Cells["D" + rowidx + ""].Value = item.title;
                    ws.Cells["E" + rowidx + ""].Value = item.approved_date;
                    ws.Cells["E" + rowidx + ""].Value = item.approved_date;
                    ws.Cells["F" + rowidx + ""].Value = hedge_type_name;
                    ws.Cells["G" + rowidx + ""].Value = item.unit_price;
                    ws.Cells["H" + rowidx + ""].Value = item.unit_volume;
                    ws.Cells["I" + rowidx + ""].Value = item.year;
                    ws.Cells["J" + rowidx + ""].Value = item.min_max;
                    ws.Cells["K" + rowidx + ""].Value = item.price_year;
                    ws.Cells["L" + rowidx + ""].Value = item.price_Q1;
                    ws.Cells["M" + rowidx + ""].Value = item.price_Q2;
                    ws.Cells["N" + rowidx + ""].Value = item.price_Q3;
                    ws.Cells["O" + rowidx + ""].Value = item.price_Q4;
                    ws.Cells["P" + rowidx + ""].Value = item.prod_volume;
                    ws.Cells["Q" + rowidx + ""].Value = item.limit_volume;
                    ws.Cells["R" + rowidx + ""].Value = item.approved_volume;
                    ws.Cells["S" + rowidx + ""].Value = item.note;
                    var modelCells = ws.Cells["A" + rowidx + ":S" + rowidx + ""];

                    modelCells.Style.WrapText = true;
                    modelCells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    modelCells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    modelCells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    modelCells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                    //if (i == 18)
                    //{
                    //    PageNum++;
                    //    rowidx = rowidx + 10;
                    //    cellHeader(rowidx, ref ws);
                    //    rowidx = rowidx + 1;
                    //}
                    //else if (i == 45)
                    //{
                    //    PageNum++;
                    //    rowidx = rowidx + 5;
                    //    cellHeader(rowidx, ref ws);
                    //    rowidx = rowidx + 1;

                    //}
                    //else if (i == 72)
                    //{
                    //    PageNum++;
                    //    rowidx = rowidx + 5;
                    //    cellHeader(rowidx, ref ws);
                    //    rowidx = rowidx + 1;
                    //}
                    //else if (i == 99)
                    //{
                    //    PageNum++;
                    //    rowidx = rowidx + 5;
                    //    cellHeader(rowidx, ref ws);
                    //    rowidx = rowidx + 1;
                    //}

                    //i++;
                    rowidx++;
                }
            }
             rowidx= rowidx + 2;
            int rowidxn = rowidx + 1;
            ws.Cells["A" + rowidx + ""].Value = "Note:";
             ws.Cells["C" + rowidx + ":S" + rowidxn + ""].Merge=true;
            ws.Cells["C" + rowidx + ":S" + rowidxn + ""].Value = Note;
            ws.Cells["C" + rowidx + ":S" + rowidxn + ""].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
            ws.Cells["C" + rowidx + ":S" + rowidxn + ""].Style.WrapText = true;
            // ws.Cells["C" + rowidx + ""].Value = Note;
            rowidx = rowidx + 2;
            ws.Cells["A" + rowidx + ""].Value = "Attach File:";
            if (AttachFile != null)
            {
                ws.Cells["C" + rowidx + ""].Value = AttachFile.Replace("Web/FileUpload/", "");
            }
            //rowidx_end = rowidx - 1;


            ws.HeaderFooter.OddHeader.RightAlignedText =
   string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);

            ws.PrinterSettings.Orientation = eOrientation.Landscape;
            ws.PrinterSettings.PaperSize = ePaperSize.A4;
            ws.PrinterSettings.RepeatRows = new ExcelAddress("$1:$8");
            ws.PrinterSettings.LeftMargin = 0.45m;
            ws.PrinterSettings.RightMargin = 0.30m;
            ws.PrinterSettings.Scale = 58;
      
            package.Save();



            //Response.End();
            if (type == "GenPdf")
            {
                ReturnValue rtn = SaveAsPdf(xlsTemp, physical_path);

            }
            else if (type == "GenExcel")
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + file_Name);
                Response.AddHeader("content-type", "application/Excel");
                Response.WriteFile(xlsTemp);
                Response.End();
            }

        }

        public ReturnValue SaveAsPdf(string saveAsLocation, string physical_path)
        {
            ReturnValue rtn = new ReturnValue();
            string saveas = (saveAsLocation.Split('.')[0]) + ".pdf";
            string filename = saveas.Replace(physical_path, "");
            try
            {
                Workbook workbook = new Workbook();
                workbook.LoadDocument(saveAsLocation, DocumentFormat.OpenXml);

                using (FileStream pdfFileStream = new FileStream(saveas, FileMode.Create))
                {
                    foreach (var worksheet in workbook.Worksheets)
                    {
                        //worksheet.ActiveView.ShowHeadings = true;
                        worksheet.ActiveView.Orientation = PageOrientation.Landscape;
                        worksheet.ActiveView.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        worksheet.ActiveView.Margins.Left = 100;
                        worksheet.ActiveView.Margins.Right = 125;
                        worksheet.ActiveView.Margins.Bottom = 100;
                        worksheet.ActiveView.Margins.Top = 100;
                        worksheet.PrintOptions.PrintTitles.SetColumns(0, 2);
                        worksheet.PrintOptions.Scale = 65;
                        //worksheet.HorizontalPageBreaks.Add(3);
                        //worksheet.PrintOptions.PrintTitles.Clear();
                        //worksheet.PrintOptions.FitToPage = true;
                    }
                    workbook.ExportToPdf(pdfFileStream);
                }
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                Response.AddHeader("content-type", "application/Excel");
                Response.WriteFile(saveas);
                Response.End();
                rtn.Status = true;
                rtn.Message = saveas;
                return rtn;
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
                return rtn;
            }
        }

        public void cellHeader(int row, ref ExcelWorksheet ws)
        {
            int rowIdx = row;
            ws.Cells["A" + rowIdx + ":C" + rowIdx + ""].Merge = true;
            ws.Cells["A" + rowIdx + ":C" + rowIdx + ""].Value = "Product";
            // ws.Column(1).Width = 24;
            rowIdx = rowIdx + 1;
            ws.Cells["A" + rowIdx + ""].Value = "Front";
            ws.Cells["B" + rowIdx + ""].Value = "vs";
            ws.Cells["C" + rowIdx + ""].Value = "Back";
            ws.Cells["D" + row + ":D" + rowIdx + ""].Merge = true;
            ws.Cells["D" + row + ":D" + rowIdx + ""].Value = "Title";
            ws.Cells["E" + row + ":E" + rowIdx + ""].Merge = true;
            ws.Cells["E" + row + ":E" + rowIdx + ""].Value = "Approved date";

            ws.Cells["F" + row + ":F" + rowIdx + ""].Merge = true;
            ws.Cells["F" + row + ":F" + rowIdx + ""].Value = "Hedge Type";
            ws.Cells["G" + row + ":G" + rowIdx + ""].Merge = true;
            ws.Cells["G" + row + ":G" + rowIdx + ""].Value = "Unit(Price)";
            ws.Cells["H" + row + ":H" + rowIdx + ""].Merge = true;
            ws.Cells["H" + row + ":H" + rowIdx + ""].Value = "Unit(Vol)";
            ws.Cells["I" + row + ":I" + rowIdx + ""].Merge = true;
            ws.Cells["I" + row + ":I" + rowIdx + ""].Value = "Year Plan";
            ws.Cells["J" + row + ":J" + rowIdx + ""].Merge = true;
            ws.Cells["J" + row + ":J" + rowIdx + ""].Value = "Buy/Sell";
            ws.Cells["K" + row + ":O" + row + ""].Merge = true;
            ws.Cells["K" + row + ":O" + row + ""].Value = "Price";
            ws.Cells["K" + rowIdx + ""].Value = "Year";
            ws.Cells["L" + rowIdx + ""].Value = "Q1";
            ws.Cells["M" + rowIdx + ""].Value = "Q2";
            ws.Cells["N" + rowIdx + ""].Value = "Q3";
            ws.Cells["O" + rowIdx + ""].Value = "Q4";
            ws.Cells["P" + row + ":P" + rowIdx + ""].Merge = true;
            ws.Cells["P" + row + ":P" + rowIdx + ""].Value = "Prod Vol";
            ws.Cells["Q" + row + ":Q" + rowIdx + ""].Merge = true;
            ws.Cells["Q" + row + ":Q" + rowIdx + ""].Value = "Limit Vol (%)";
            ws.Cells["R" + row + ":R" + rowIdx + ""].Merge = true;
            ws.Cells["R" + row + ":R" + rowIdx + ""].Value = "Approved Vol";
            ws.Cells["S" + row + ":S" + rowIdx + ""].Merge = true;
            ws.Cells["S" + row + ":S" + rowIdx + ""].Value = "Note";
            ws.Cells["A" + row + ":S" + rowIdx + ""].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            ws.Cells["A" + row + ":S" + rowIdx + ""].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            var modelCells = ws.Cells["A" + row + ":S" + rowIdx + ""];
            // Assign borders
            modelCells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelCells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelCells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelCells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            ws.Column(1).Width = 11;
            ws.Column(2).Width = 3;
            ws.Column(3).Width = 11;
            ws.Column(4).Width = 11;
            ws.Column(5).Width = 16;
            ws.Column(6).Width = 16;
            ws.Column(7).Width = 12;
            ws.Column(8).Width = 12;
            ws.Column(9).Width = 11;
            ws.Column(10).Width = 12;
            ws.Column(11).Width = 11;
            ws.Column(12).Width = 11;
            ws.Column(13).Width = 11;
            ws.Column(14).Width = 11;
            ws.Column(15).Width = 11;
            ws.Column(16).Width = 11;
            ws.Column(17).Width = 16;
            ws.Column(18).Width = 16;
            ws.Column(19).Width = 16;

        }




        [HttpGet]
        public JsonResult GetHistory(string tranID)
        {
            string tran_id = tranID.Decrypt();
            List<HedgeAnnualHistoryViewModel> history = new List<HedgeAnnualHistoryViewModel>();
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            history = service.getHedgeAnnualHistory(tran_id);
            foreach (var item in history)
            {
                item.tran_id = item.tran_id.Encrypt();
            }
            return Json(history, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProductHistory(string tranID)
        {
            List<HedgeAnnualProductHistoryViewModel> history = new List<HedgeAnnualProductHistoryViewModel>();
            HedgeAnnualServiceModel service = new HedgeAnnualServiceModel();
            history = service.getHedgeAnnualProductHistory(tranID);
            return Json(history, JsonRequestBehavior.AllowGet);
        }
    }
}