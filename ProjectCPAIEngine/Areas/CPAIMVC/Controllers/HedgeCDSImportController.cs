﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeCDSImportController : BaseController
    {
        // GET: CPAIMVC/HedgeCDSImport
        public ActionResult Index(HedgeCDSImportViewModel viewModel)
        {
            HedgeCDSImportViewModel model = new HedgeCDSImportViewModel();
            HedgeCDSImportServiceModel serviceModel = new HedgeCDSImportServiceModel();
            HedgeCDSImportViewModel_Search viewModelSearch = new HedgeCDSImportViewModel_Search();

            //if (viewModel.cds_search == null)
            //    viewModelSearch.sLimitRow = 5;
            //else
            //    viewModelSearch = viewModel.cds_search;

            serviceModel.Search(ref viewModelSearch);
            model.cds_search = viewModelSearch;

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(FormCollection form, HedgeCDSImportViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            HedgeCDSImportServiceModel serviceModel = new HedgeCDSImportServiceModel();
            HttpPostedFileBase upload = Request.Files.Count > 0 ? Request.Files[0] : null;
            string fileName = "";
            string filePath = "";
            string reason = "";
            string note = "";
            if (upload.ContentLength > 0)
            {
                string type = ConstantPrm.SYSTEM.HEDG;
                string typename = Path.GetExtension(upload.FileName).ToLower();
                fileName = string.Format("{0}_{2}_{1}", DateTime.Now.ToString("yyyyMMddHHmmss"), type, "ImportCDS");
                filePath = UploadFile(upload, "Web/FileUpload/HedgeCDSImport", fileName + typename);
                Thread thread = new Thread(() => serviceModel.UploadExcel(fileName, filePath, reason, note, lbUserName));
                thread.Start();
                //serviceModel.UploadExcel(fileName, filePath, reason, note, lbUserName);
                rtn.Message = "Upload file success";
                rtn.Status = true;
            }
            else
            {
                rtn.Message = "Please Upload Your file";
            }

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            return View(model);
        }

        [HttpPost]
        public JsonResult getLogData(string sLimit)
        {
            HedgeCDSImportServiceModel service = new HedgeCDSImportServiceModel();
            List<HedgeCDSImport_SearchData> logList = service.getCdsLogList(sLimit);
            return Json(logList, JsonRequestBehavior.AllowGet);
        }

        public string UploadFile(HttpPostedFileBase requestFile, string uploadPath, string uploadFileName)
        {
            string path = "";
            if (Request != null)
            {
                var file = requestFile;

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string rootPath = Request.PhysicalApplicationPath;
                    string subPath = uploadPath;

                    fileName = uploadFileName;
                    if (!Directory.Exists(rootPath + subPath))
                        Directory.CreateDirectory(rootPath + subPath);
                    path = Path.Combine(Server.MapPath("~/" + uploadPath), fileName);
                    file.SaveAs(path);
                    return path;
                }
                else
                {
                    return path;
                }
            }
            else
            {
                return path;
            }
        }

    }
}