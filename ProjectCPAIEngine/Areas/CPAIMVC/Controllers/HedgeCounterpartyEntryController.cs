﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System.Web.Routing;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeCounterpartyEntryController : BaseController
    {
        // GET: CPAIMVC/HedgeCounterpartyEntry
        public ActionResult Index()
        {
            return RedirectToAction("Search", "HedgeCounterpartyEntry");
        }



        [HttpGet]
        public ActionResult Search()
        {
            HedgeCounterpartyEntryViewModel model = initialModel();
            return View(model);
        }


        public ActionResult Search(HedgeCounterpartyEntryViewModel postModel)
        {
            HedgeCounterpartyEntryViewModel model = initialModel();
            HedgeCounterpartyEntryServiceModel serviceModel = new HedgeCounterpartyEntryServiceModel();
            HedgeCounterpartyEntry_Search searchModel = new HedgeCounterpartyEntry_Search();

            searchModel = postModel.cpe_search;
            serviceModel.Search(ref searchModel);
            model.cpe_search = searchModel;
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                HedgeCounterpartyEntryViewModel model = initialModel();
                model.cpe_Detail.pageMode = "ADD";
                ViewBag.Version = "1";
                ViewBag.User = lbUserName;
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
   new { f = "HedgeCounterpartyEntry" }));
            }
           
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, HedgeCounterpartyEntryViewModel model)
        {
            if (model.cpe_Detail.pageMode == "ADD")
            {

                HedgeCounterpartyEntryServiceModel serviceModel = new HedgeCounterpartyEntryServiceModel();
                ReturnValue rtn = new ReturnValue();
                rtn = serviceModel.Add(model, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                model.ddl_counterparty = DropdownServiceModel.getVendor();
                model.ddl_creditRating = DropdownServiceModel.getCreditRating();
                model.ddl_countDate = DropdownServiceModel.getSelectCountDate();
                model.ddl_MasterDDL = DropdownServiceModel.getMasterStatus();
                model.ddl_CounterpartyMapping = DropdownServiceModel.getVendorMapped();
                return View(model);
            }
            else if (model.cpe_Detail.pageMode == "EDIT")
            {
                HedgeCounterpartyEntryServiceModel serviceModel = new HedgeCounterpartyEntryServiceModel();
                ReturnValue rtn = new ReturnValue();
               // model.cpe_Detail.row_id = EntryCode;
                rtn = serviceModel.Edit(model, lbUserName);
                model.ddl_counterparty = DropdownServiceModel.getVendor();
                model.ddl_creditRating = DropdownServiceModel.getCreditRating();
                model.ddl_countDate = DropdownServiceModel.getSelectCountDate();
                model.ddl_MasterDDL = DropdownServiceModel.getMasterStatus();
                model.ddl_CounterpartyMapping = DropdownServiceModel.getVendorMapped();
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["EntryCode"] = model.cpe_Detail.row_id;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
                   new { f = "HedgeCounterpartyEntry" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string EntryCode)
        {
            if (ViewBag.action_edit)
            {
                HedgeCounterpartyEntryServiceModel serviceModel = new HedgeCounterpartyEntryServiceModel();

                HedgeCounterpartyEntryViewModel model = initialModel();
                model.cpe_Detail = serviceModel.Get(EntryCode);
                TempData["EntryCode"] = EntryCode;
                model.cpe_Detail.pageMode = "EDIT";
                ViewBag.Version = model.cpe_Detail.version;
                ViewBag.User = lbUserName;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeCompany" }));
            }
        }

        //[HttpPost]
        //public ActionResult Edit(string EntryCode, HedgeCounterpartyEntryViewModel model)
        //{

        //    if (ViewBag.action_edit)
        //    {
        //        HedgeCounterpartyEntryServiceModel serviceModel = new HedgeCounterpartyEntryServiceModel();
        //        ReturnValue rtn = new ReturnValue();
        //        model.cpe_Detail.row_id = EntryCode;
        //        rtn = serviceModel.Edit(model, lbUserName);
        //        model.ddl_counterparty = DropdownServiceModel.getVendor();
        //        model.ddl_creditRating = DropdownServiceModel.getCreditRating();
        //        model.ddl_countDate = DropdownServiceModel.getSelectCountDate();
        //        model.ddl_MasterDDL = DropdownServiceModel.getMasterStatus();
        //        model.ddl_CounterpartyMapping = DropdownServiceModel.getVendorMapped();
        //        TempData["ReponseMessage"] = rtn.Message;
        //        TempData["ReponseStatus"] = rtn.Status;
        //        TempData["AreaCode"] = EntryCode;         
        //        return View("Create", model);
        //    }
        //    else
        //    {
        //        return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Area" }));
        //    }

        //}


        public HedgeCounterpartyEntryViewModel initialModel()
        {
            HedgeCounterpartyEntryViewModel model = new HedgeCounterpartyEntryViewModel();
            model.cpe_Detail = new HedgeCounterpartyEntry_Detail();
            model.cpe_search = new HedgeCounterpartyEntry_Search();
            model.cpe_search.searchData = new List<HedgeCounterpartyEntry_SearchData>();
            model.cpe_History = new History();
            model.cpe_History.history_log = new List<History_Log>();            
            model.ddl_counterparty = DropdownServiceModel.getVendor();
            model.ddl_creditRating = DropdownServiceModel.getCreditRating();
            model.ddl_countDate = DropdownServiceModel.getSelectCountDate();
            model.ddl_MasterDDL = DropdownServiceModel.getMasterStatus();
            model.ddl_CounterpartyMapping = DropdownServiceModel.getVendorMapped();
            return model;
        }

        [HttpGet]
        public JsonResult GetCreditLimit(string creditRating)
        {
            CreditRatingServiceModel service = new CreditRatingServiceModel();
            string credit_rating = service.GetCreditRating(creditRating);
            return Json(credit_rating, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetHistoryLog(string id)
        {
            HedgeCounterpartyEntryServiceModel service = new HedgeCounterpartyEntryServiceModel();
            List<History_Log> logList = service.GetHistoryLog(id);
            return Json(logList, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetRefCreditLimit(string refcreditRating)
        {
            HedgeCounterpartyEntryServiceModel service = new HedgeCounterpartyEntryServiceModel();
            string credit_rating = service.GetRefCreditLimit(refcreditRating);
            return Json(credit_rating, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public JsonResult SearchCDSByDate(string date,string entryId) 
        {           
            HedgeCounterpartyEntryServiceModel service = new HedgeCounterpartyEntryServiceModel();
            List<CDS> dataCDS = service.GetCDS(date, entryId);
            return Json(dataCDS, JsonRequestBehavior.AllowGet);
        }
    }
}