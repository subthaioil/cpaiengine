﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeCounterpartyLimitController : BaseController
    {
        // GET: CPAIMVC/HedgeCounterPartyLimit
        public ActionResult Index()
        {
            HedgeCounterpartyLimitViewModel model = new HedgeCounterpartyLimitViewModel();
            HedgeCounterpartyLimitServiceModel serviceModel = new HedgeCounterpartyLimitServiceModel();
            model = serviceModel.GetData();
            if (model.CounterpartyCDS == null)
            {
                model.CounterpartyCDS = new List<CounterpartyCDS>();
            }
            initializeDropDownList(ref model);
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Index(HedgeCounterpartyLimitViewModel model)
        {
            HedgeCounterpartyLimitServiceModel serviceModel = new HedgeCounterpartyLimitServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = serviceModel.Edit(model, lbUserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            initializeDropDownList(ref model);
            return View("Index", model);
        }

        private void initializeDropDownList(ref HedgeCounterpartyLimitViewModel model)
        {
            model.ddlCreditRating = DropdownServiceModel.getCreditRating();
            model.ddlCdsType = new List<SelectListItem>();
            model.ddlCdsType.Add(new SelectListItem { Text = "CDS IN", Value = "CDS IN" });
            model.ddlCdsType.Add(new SelectListItem { Text = "CDS OUT", Value = "CDS OUT" });
            model.ddlCdsAction = new List<SelectListItem>();
            model.ddlCdsAction.Add(new SelectListItem { Text = "NOTICE", Value = "NOTICE" });
            model.ddlCdsAction.Add(new SelectListItem { Text = "SUSPEND", Value = "SUSPEND" });
            model.ddlCdsAction.Add(new SelectListItem { Text = "SPECIAL", Value = "SPECIAL" });
            model.ddlCdsAction.Add(new SelectListItem { Text = "NORMAL", Value = "NORMAL" });
        }

        [HttpPost]
        public ActionResult SearchEmailList(string userGroup, string userSystem, string fullNameEN, string fullNameTH, string[] oldProduct)
        {
            HedgeCounterpartyLimitViewModel model = new HedgeCounterpartyLimitViewModel();
            try
            {
                HedgeCounterpartyLimitServiceModel serviceModel = new HedgeCounterpartyLimitServiceModel();
                serviceModel.SearchEmail(ref model, userGroup, userSystem, fullNameEN, fullNameTH, oldProduct);
            }
            catch (Exception ex)
            {

            }
            return Json(model);
        }


        public ActionResult SearchEmail()
        {
            HedgeCounterpartyLimitViewModel vm = new HedgeCounterpartyLimitViewModel();
            try
            {
                vm.oldSelectList = Session["oldSelectList"] as List<string>;
                if (vm.oldSelectList == null)
                {
                    vm.oldSelectList = new List<string>();
                }

                vm.ddlUserGroup = DropdownServiceModel.getUserGroup();
                vm.ddlSystem = DropdownServiceModel.getUserGroup();
            }
            catch (Exception ex)
            {

            }
            return PartialView("_SearchEmail", vm);
        }
    }
}