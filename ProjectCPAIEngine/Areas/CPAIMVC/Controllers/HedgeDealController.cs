﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeDealController : BaseController
    {
        public ActionResult Summary()
        {
            HedgeDealSummaryViewModel model = new HedgeDealSummaryViewModel();
            model.Companies = DropdownServiceModel.GetHEDGCompany();
            model.TradingBooks = DropdownServiceModel.GetHEDGCompany();
            model.Status = DropdownServiceModel.getStatus(false,"");
            //  model.Counterparties = DropdownServiceModel.Counterparties();
            if (TempData.ContainsKey("HedgeDealSummaryViewModel") && TempData["HedgeDealSummaryViewModel"] != null)
            {
                model = TempData["HedgeDealSummaryViewModel"] as HedgeDealSummaryViewModel;
            }
            else
            {
                //model = new HedgeDealSummaryViewModel
                //{
                //};
            }
            TempData["HedgeDealSummaryViewModel"] = null;
            return View(model);
        }

        public ActionResult SummaryResult(HedgeDealSummaryViewModel model)
        {
            try
            {
                var criteria = model.Criteria.Clone();
                model.HedgeDealSummaryResults = new HedgeDealServiceModel().GetHedgeDealSummary(criteria);
                TempData["HedgeDealSummaryViewModel"] = model;
            }
            catch (Exception ex)
            {
                if (ModelState.IsValid)
                {
                    model.ErrorMessage = GetErrorHtmlMessage(ex);
                    TempData["HedgeDealSummaryViewModel"] = model;
                }
                else
                {
                    TempData["HedgeDealSummaryViewModel"] = new VCoolReportViewModel { ErrorMessage = ex.Message };
                }
            }
            return RedirectToAction("Summary", "HedgeDeal");
        }

        public ActionResult DealKeyin()
        {
            var model = HedgeDealKeyinMainViewModel.GetMockup();

            model.Header.Companies = DropdownServiceModel.GetHEDGCompany();
            model.Header.TradingBooks = DropdownServiceModel.GetHEDGCompany();
            //model.Header.UnitPrices = DropdownServiceModel.getUnitPrice();
            return View(model);
        }
        
        private string GetErrorHtmlMessage(Exception ex)
        {
            var inner = ex.FromHierarchy(c => c.InnerException).LastOrDefault();
            var sb = new System.Text.StringBuilder();
            sb.Append("<h3>Error has occurred.</h3>");
            sb.AppendFormat("<span>Source : {0}</span><br/>", inner?.Source);
            sb.AppendFormat("<span>HResult: {0}</span><br/>", inner?.HResult);
            sb.AppendFormat("<span>Message: {0}</span><br/>", inner?.Message);
            return sb.ToString();
        }
    }
}