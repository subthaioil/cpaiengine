﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeFormularController : BaseController
    {
        // GET: CPAIMVC/HedgeFormular
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult Formular()
        {
            HedgeFormularViewModel model = initialModel();
            HedgeFormularServiceModel serviceModel = new HedgeFormularServiceModel();
            model.Formular_Detail = serviceModel.GetAllFormular();
            return PartialView("_Formular", model);
        }

        [HttpPost]
        public ActionResult SaveData(HedgeFormularViewModel model)
        {
            HedgeFormularServiceModel serviceModel = new HedgeFormularServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = serviceModel.Save(model,lbUserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            model.Formular_Detail = new List<Formular>();
            model.ddl_MasterDDL = DropdownServiceModel.getMasterStatus();
            model.ddl_Operation = DropdownServiceModel.getToolFormularOprator();
            model.ddl_Variable = DropdownServiceModel.getToolConditionVariable();
            ViewBag.HedgeFormular = "HedgeFormular";
            return Json(rtn, JsonRequestBehavior.AllowGet);
        } 


        public HedgeFormularViewModel initialModel()
        {
            HedgeFormularViewModel model = new HedgeFormularViewModel();
            model.Formular_Detail = new List<Formular>();
            model.ddl_MasterDDL = DropdownServiceModel.getMasterStatus();
            model.ddl_Operation = DropdownServiceModel.getToolFormularOprator();
            model.ddl_Variable = DropdownServiceModel.getToolConditionVariable();
            return model;
        }


        public ActionResult DeleteFormular(string id)
        {
            HedgeFormularServiceModel serviceModel = new HedgeFormularServiceModel();
            var data = serviceModel.DeleteFormular(id);
            return Json(data, JsonRequestBehavior.AllowGet);
        }


    }
}