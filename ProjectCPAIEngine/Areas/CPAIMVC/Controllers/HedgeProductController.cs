﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeProductController : BaseController
    {

        public ActionResult Search()
        {
            HedgeProductViewModel vm = new HedgeProductViewModel();
            try
            {
                var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
                msdata = msdata.OrderBy(x => x).ToList();
                List<SelectListItem> userList = new List<SelectListItem>();
                foreach (var a in msdata)
                {
                    userList.Add(new SelectListItem { Text = a, Value = a });
                }
                vm.ddlUser = userList.Distinct().ToList();
                vm.ddlStatus = new List<SelectListItem>() { new SelectListItem() { Text = "ACTIVE", Value = "ACTIVE" }, new SelectListItem() { Text = "INACTIVE", Value = "INACTIVE" } };
            }
            catch (Exception ex)
            {

            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult Search(HedgeProductViewModel vm)
        {
            try
            {
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                serviceModel.SearchMarket(ref vm);
                var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
                msdata = msdata.OrderBy(x => x).ToList();
                List<SelectListItem> userList = new List<SelectListItem>();
                foreach (var a in msdata)
                {
                    userList.Add(new SelectListItem { Text = a, Value = a });
                }
                vm.ddlUser = userList.Distinct().ToList();
                vm.ddlStatus = new List<SelectListItem>() { new SelectListItem() { Text = "ACTIVE", Value = "ACTIVE" }, new SelectListItem() { Text = "INACTIVE", Value = "INACTIVE" } };
            }
            catch (Exception ex)
            {

            }
            return View(vm);
        }

        public ActionResult Create()
        {
            HedgeProductViewModel vm = new HedgeProductViewModel();
            try
            {
                vm.pageMode = "NEW";
                Session["oldSelectList"] = null;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from a in context.MKT_MST_MOPS_PRODUCT
                                 join b in context.MKT_MST_MOPS_MARKET on a.M_MPR_MKTID_FK equals b.M_MKT_ID
                                 join c in context.HEDG_MT_PROD_MKT on b.M_MKT_ID equals Convert.ToInt32(c.HMPM_ORDER)
                                 select new
                                 {
                                     MarketID = b.M_MKT_ID,
                                     MarketName = b.M_MKT_MKTNAME,
                                     ProductID = a.M_MPR_ID,
                                     ProductName = a.M_MPR_ACTPRODNAME,
                                     Unit = c.HMPM_UNIT,
                                 });

                    if (query != null)
                    {
                        foreach (var item in query.Distinct().ToList())
                        {
                                vm.MMMPRODUCT_List.Add(new MKT_MST_MOPS_PRODUCT_List
                                {
                                    Id = item.ProductID.ToString(),
                                    MKT_MST_MOPS_MARKET_ID = item.MarketID.ToString(),
                                    Name = item.ProductName,
                                    Unit = item.Unit,
                                });                          
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddDLYMKT(HedgeProductViewModel vm)
        {
            try
            {
                if(vm.MappingMarketPriceList != null)
                {
                    for(var i =0;i<vm.MappingMarketPriceList.Count();i++)
                    {
                        if(vm.MappingMarketPriceList[i].MappingForwardList != null)
                        {
                            for(var k=0;k< vm.MappingMarketPriceList[i].MappingForwardList.Count();k++)
                            {
                                vm.MappingMarketPriceList[i].MappingForwardList[k].Name = vm.MappingMarketPriceList[0].MappingForwardList[k].Name;
                            }
                        }
                    }
                }
                vm.oldSelectList = Session["oldSelectList"] as List<string>;
                if (vm.oldSelectList == null)
                {
                    vm.oldSelectList = new List<string>();
                    if(vm.MappingMarketPriceList != null)
                    {
                        foreach(var item in vm.MappingMarketPriceList)
                        {
                            vm.oldSelectList.Add(item.ProductId_F);
                        }
                    }
                }
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from a in context.MKT_MST_MOPS_MARKET
                                 select new
                                 {
                                     MarketID = a.M_MKT_ID,
                                     MarketName = a.M_MKT_MKTNAME,
                                 });
                    if (query != null)
                    {
                        vm.ddlMarketName = new List<SelectListItem>();
                        foreach (var item in query.ToList())
                        {
                            vm.ddlMarketName.Add(new SelectListItem { Text = item.MarketName, Value = item.MarketID.ToString() });
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("_AddDLYMKT", vm);
        }

        [HttpPost]
        public ActionResult SearchMarket(string marketId, string marketName, string productId, string productName, string[] oldProduct)
        {
            HedgeProductViewModel model = new HedgeProductViewModel();
            try
            {
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                serviceModel.SearchMarket(ref model, marketId, marketName, productId, productName, oldProduct);
            }
            catch (Exception ex)
            {

            }
            return Json(model);
        }

        [HttpPost]
        public ActionResult AddMarketPrice(HedgeProductViewModel vm)
        {
            try
            {
                if (vm.MappingMarketPriceList != null)
                {
                    for (var i = 0; i < vm.MappingMarketPriceList.Count(); i++)
                    {
                        if (vm.MappingMarketPriceList[i].MappingForwardList != null)
                        {
                            for (var k = 0; k < vm.MappingMarketPriceList[i].MappingForwardList.Count(); k++)
                            {
                                vm.MappingMarketPriceList[i].MappingForwardList[k].Name = vm.MappingMarketPriceList[0].MappingForwardList[k].Name;
                            }
                        }
                    }
                }
                string[] market_no = vm.SelectedProduct.Split(',');
                market_no = market_no.Where(i => i != "").ToArray();
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                serviceModel.AddMarketPrice(ref vm, market_no);
                Session["oldSelectList"] = vm.oldSelectList;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryProductUnit = (from a in context.HEDG_MT_PROD_MKT
                                            select new
                                            {
                                                ProductUnit = a.HMPM_UNIT
                                            }).Distinct();
                    if (queryProductUnit != null)
                    {
                        vm.ddlProductUnit = new List<SelectListItem>();
                        foreach (var item in queryProductUnit.ToList())
                        {
                            if (item.ProductUnit != null)
                            {
                                vm.ddlProductUnit.Add(new SelectListItem { Value = item.ProductUnit, Text = item.ProductUnit });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return PartialView("_MappingMarketPrice", vm);
        }

        [HttpPost]
        public ActionResult DeleteMarketPrice(HedgeProductViewModel vm)
        {
            try
            {
                if (vm.MappingMarketPriceList != null)
                {
                    for (var j = 0; j < vm.MappingMarketPriceList.Count(); j++)
                    {
                        if (vm.MappingMarketPriceList[j].MappingForwardList != null)
                        {
                            for (var k = 0; k < vm.MappingMarketPriceList[j].MappingForwardList.Count(); k++)
                            {
                                vm.MappingMarketPriceList[j].MappingForwardList[k].Name = vm.MappingMarketPriceList[0].MappingForwardList[k].Name;
                            }
                        }
                    }
                }
                ModelState.Clear();
                vm.oldSelectList = new List<string>();
                var newMappingList = new List<MappingMarketPrice>();
                var i = 1;
                foreach (var item in vm.MappingMarketPriceList.OrderBy(a => a.Number))
                {
                    if (item.Delete != "TRUE")
                    {
                        item.Number = i.ToString();
                        newMappingList.Add(item);
                        vm.oldSelectList.Add(item.ProductId_F);
                        i++;
                    }
                    else
                    {
                        item.Number = null;
                        newMappingList.Add(item);
                    }
                }
                vm.MappingMarketPriceList = newMappingList;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryProductUnit = (from a in context.HEDG_MT_PROD_MKT
                                            select new
                                            {
                                                ProductUnit = a.HMPM_UNIT
                                            }).Distinct();
                    if (queryProductUnit != null)
                    {
                        vm.ddlProductUnit = new List<SelectListItem>();
                        foreach (var item in queryProductUnit.ToList())
                        {
                            if (item.ProductUnit != null)
                            {
                                vm.ddlProductUnit.Add(new SelectListItem { Value = item.ProductUnit, Text = item.ProductUnit });
                            }
                        }
                    }
                    var query = (from a in context.MKT_MST_MOPS_PRODUCT
                                 join b in context.MKT_MST_MOPS_MARKET on a.M_MPR_MKTID_FK equals b.M_MKT_ID
                                 select new
                                 {
                                     MarketID = b.M_MKT_ID,
                                     MarketName = b.M_MKT_MKTNAME,
                                     ProductID = a.M_MPR_ID,
                                     ProductName = a.M_MPR_ACTPRODNAME,
                                     Unit = a.M_MPR_UNIT,
                                 });
                    if (query != null)
                    {
                        foreach (var item in query.Distinct().ToList())
                        {
                            vm.MMMPRODUCT_List.Add(new MKT_MST_MOPS_PRODUCT_List
                            {
                                Id = item.ProductID.ToString(),
                                MKT_MST_MOPS_MARKET_ID = item.MarketID.ToString(),
                                Name = item.ProductName,
                                Unit = item.Unit,
                            });
                        }
                    }
                }
                Session["oldSelectList"] = vm.oldSelectList;
            }
            catch (Exception ex)
            {

            }
            return PartialView("_MappingMarketPrice", vm);
        }

        [HttpPost]
        public ActionResult SaveProduct(HedgeProductViewModel vm)
        {
            try
            {
                ReturnValue rtn = new ReturnValue();
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                if (vm.pageMode == "NEW")
                {
                    rtn = serviceModel.SaveDATA(ref vm, lbUserName);
                }
                else
                {
                    rtn = serviceModel.UpdateDATA(ref vm, lbUserName);
                }
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
                msdata = msdata.OrderBy(x => x).ToList();
                List<SelectListItem> userList = new List<SelectListItem>();
                foreach (var a in msdata)
                {
                    userList.Add(new SelectListItem { Text = a, Value = a });
                }
                vm.ddlUser = userList.Distinct().ToList();
                vm.ddlStatus = new List<SelectListItem>() { new SelectListItem() { Text = "ACTIVE", Value = "ACTIVE" }, new SelectListItem() { Text = "INACTIVE", Value = "INACTIVE" } };
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("Search");
            //return View("Search", vm);
        }


        public ActionResult ViewData(string rowId)
        {
            HedgeProductViewModel vm = new HedgeProductViewModel();
            try
            {
                rowId = rowId.Decrypt();
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                serviceModel.GetDATA(ref vm, rowId);
                vm.pageMode = "VIEW";
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryProductUnit = (from a in context.HEDG_MT_PROD_MKT
                                            select new
                                            {
                                                ProductUnit = a.HMPM_UNIT
                                            }).Distinct();
                    if (queryProductUnit != null)
                    {
                        vm.ddlProductUnit = new List<SelectListItem>();
                        foreach (var item in queryProductUnit.ToList())
                        {
                            if (item.ProductUnit != null)
                            {
                                vm.ddlProductUnit.Add(new SelectListItem { Value = item.ProductUnit, Text = item.ProductUnit });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return View("Create", vm);
        }

        public ActionResult EditData(string rowId)
        {
            HedgeProductViewModel vm = new HedgeProductViewModel();
            try
            {
                Session["oldSelectList"] = null;
                rowId = rowId.Decrypt();
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                serviceModel.GetDATA(ref vm, rowId);
                vm.pageMode = "EDIT";
                vm.rowId = rowId;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //var queryProductUnit = (from a in context.MKT_MST_MOPS_PRODUCT
                    //                        select new
                    //                        {
                    //                            ProductUnit = a.M_MPR_UNIT
                    //                        }).Distinct();
                    var queryProductUnit = (from a in context.HEDG_MT_PROD_MKT
                                            select new
                                            {
                                                ProductUnit = a.HMPM_UNIT
                                            }).Distinct();

                    if (queryProductUnit != null)
                    {
                        vm.ddlProductUnit = new List<SelectListItem>();
                        foreach (var item in queryProductUnit.ToList())
                        {
                            if (item.ProductUnit != null)
                            {
                                vm.ddlProductUnit.Add(new SelectListItem { Value = item.ProductUnit, Text = item.ProductUnit });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return View("Create", vm);
        }

        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "DeleteResult")]
        public ActionResult DeleteResult(HedgeProductViewModel vm)
        {
            try
            {
                var rowId = vm.deleteRowId.Decrypt();
                HedgeProductServiceModel serviceModel = new HedgeProductServiceModel();
                serviceModel.DeleteDATA(ref vm, rowId);
                var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
                msdata = msdata.OrderBy(x => x).ToList();
                List<SelectListItem> userList = new List<SelectListItem>();
                foreach (var a in msdata)
                {
                    userList.Add(new SelectListItem { Text = a, Value = a });
                }
                vm.ddlUser = userList.Distinct().ToList();
                vm.ddlStatus = new List<SelectListItem>() { new SelectListItem() { Text = "ACTIVE", Value = "ACTIVE" }, new SelectListItem() { Text = "INACTIVE", Value = "INACTIVE" } };
            }
            catch (Exception ex)
            {

            }
            return View("Search", vm);
        }

        public ActionResult Autocomplete(string term)
        {
            List<string> items = new List<string>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from a in context.MKT_MST_MOPS_PRODUCT
                             join b in context.MKT_MST_MOPS_MARKET on a.M_MPR_MKTID_FK equals b.M_MKT_ID
                             select new
                             {
                                 MarketID = b.M_MKT_ID,
                                 MarketName = b.M_MKT_MKTNAME,
                                 ProductID = a.M_MPR_ID,
                                 ProductName = a.M_MPR_ACTPRODNAME,
                                 Unit = a.M_MPR_UNIT,
                             });
                if (query != null)
                {
                    foreach (var item in query.Distinct().ToList())
                    {
                        items.Add(item.ProductName);
                    }
                }
            }
            var filteredItems = items.Where(item => item.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) >= 0);
            return Json(filteredItems, JsonRequestBehavior.AllowGet);
        }
    }
}