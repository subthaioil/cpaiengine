﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Model;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;
using System.Globalization;
using ProjectCPAIEngine.DAL;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeSteeringCommitteeController : BaseController
    {
        #region MODELS AND CONSTRUCTOR PROPERTIES 

        public HedgeSteeringCommitteeSearchViewModel modelSearch
        {
            get { return TempData["HedgeSteeringCommitteeSearchViewModel"] != null ? (HedgeSteeringCommitteeSearchViewModel)TempData["HedgeSteeringCommitteeSearchViewModel"] : new HedgeSteeringCommitteeSearchViewModel(); }
            set { TempData["HedgeSteeringCommitteeSearchViewModel"] = value; }
        }

        public HedgeSteeringCommitteeViewModel modelView
        {
            get { return TempData["HedgeSteeringCommitteeViewModel"] as HedgeSteeringCommitteeViewModel; }
            set { TempData["HedgeSteeringCommitteeViewModel"] = value; }
        }

        public List<ButtonAction> ButtonListHedgeSteeringCommittee
        {
            get
            {
                if (Session["ButtonListHedgeSteeringCommittee"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListHedgeSteeringCommittee"];
            }
            set { Session["ButtonListHedgeSteeringCommittee"] = value; }
        }

        ShareFn _FN = new ShareFn();
        const string returnPage = "/web/MainBoards.aspx";
        const string JSON_HEDG = "JSON_HEDG";

        #endregion

        #region ACTIONS: HEDGE STEERING COMMITTEE
        public ActionResult Index()
        {
            return RedirectToAction("Search");
        }

        public ActionResult Create()
        {
            HedgeSteeringCommitteeViewModel model;
            model = modelView;

            if (model == null)
                model = new HedgeSteeringCommitteeViewModel();

            model.buttonMode = "AUTO";
            model.pageMode = "EDIT";
            initializeDropDownList(ref model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            return RedirectToAction("Search");
        }
        #endregion

        #region ACTIONS: SEARCH HEDGE STEERING COMMITTEE 
        public ActionResult Search()
        {
            HedgeSteeringCommitteeSearchViewModel model;
            model = modelSearch;

            initializeDropDownList(ref model);
            return View(model);
        }

        public ActionResult SearchResult(FormCollection fm, HedgeSteeringCommitteeSearchViewModel model)
        {
            string approved_date_start = getDateSplit(model.approved_date, true);
            string approved_date_end = getDateSplit(model.approved_date, false);
            string update_date_start = getDateSplit(model.update_date, true);
            string update_date_end = getDateSplit(model.update_date, false);

            List_HedgeSteeringCommitteetrx res = SearchHedgeSteeringCommitteeData(
                        company: model.trading_book,
                        approved_date: approved_date_start,
                        updated_date: update_date_start,
                        updated_by: model.update_by,
                        status: model.status
                );

            model.HedgeSteeringCommitteeTransaction = res.HedgeSteeringCommitteeTransaction.OrderByDescending(x => x.transaction_id).ToList();

            foreach (var item in model.HedgeSteeringCommitteeTransaction)
            {
                item.Transaction_id_Encrypted = item.transaction_id.Encrypt();
                item.Req_transaction_id_Encrypted = item.req_transaction_id.Encrypt();
            }

            modelSearch = model;
            return RedirectToAction("Search");
        }

        private List_HedgeSteeringCommitteetrx SearchHedgeSteeringCommitteeData(string system = "", string type = "", string action = "", string status = "",
                                                  string create_by = "", string company = "", string version = "", string approved_date = "",
                                                  string activation_status = "", string updated_date = "", string updated_by = "")
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000071;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();

            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = HEDGE_STR_CMT_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = company });
            req.Req_parameters.P.Add(new P { K = "index_11", V = version });
            req.Req_parameters.P.Add(new P { K = "index_12", V = approved_date });
            req.Req_parameters.P.Add(new P { K = "index_13", V = activation_status });
            req.Req_parameters.P.Add(new P { K = "index_14", V = updated_date });
            req.Req_parameters.P.Add(new P { K = "index_15", V = updated_by });

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_HedgeSteeringCommitteetrx _model = ShareFunction.DeserializeXMLFileToObject<List_HedgeSteeringCommitteetrx>(_DataJson);
            if (_model == null)
            {
                _model = new List_HedgeSteeringCommitteetrx();
            }

            if (_model.HedgeSteeringCommitteeTransaction == null)
            {
                _model.HedgeSteeringCommitteeTransaction = new List<HedgeSteeringCommitteeEncrypt>();
            }
            else
            {
                //foreach (var HedgeAnnualTransaction in _model.HedgeAnnualTransaction)
                //{
                //    HedgeAnnualTransaction.status_description = VCoolServiceModel.getWorkflowStatusDescription(HedgeAnnualTransaction.status);
                //}
            }

            return _model;
        }
        #endregion

        #region UTILITY METHODS
        public void initializeDropDownList(ref HedgeSteeringCommitteeSearchViewModel model)
        {
            HedgeSteeringCommitteeServiceModel service = new HedgeSteeringCommitteeServiceModel();
            model.ddlTrading = service.GetHedgeCompany();
            model.ddlHedgeType = service.GetHedgeType();
        }

        public void initializeDropDownList(ref HedgeSteeringCommitteeViewModel model)
        {
            HedgeSteeringCommitteeServiceModel service = new HedgeSteeringCommitteeServiceModel();
            model.ddlTrading = service.GetHedgeCompany(model.trading_book);
            model.ddlHedgeType = service.GetHedgeType();
            model.hedge_steering_committee_main_choice.ddlHedgeTitle = service.GetHedgeTitle();
            model.hedge_steering_committee_main_choice.ddlHedgeProduct = service.GetHedgeProduct();
            model.hedge_steering_committee_main_choice.ddlHedgeUnitPrice = getUnitPrice();
            model.hedge_steering_committee_main_choice.ddlHedgeUnitVolume = getUnitVolume();
            model.hedge_steering_committee_main_choice.ddlHedgeMinMax = getMinMax();
            model.hedge_steering_committee_extend_choice.ddlHedgeTitle = service.GetHedgeTitle();
            model.hedge_steering_committee_extend_choice.ddlHedgeProduct = service.GetHedgeProduct();
            model.hedge_steering_committee_extend_choice.ddlHedgeUnitPrice = getUnitPrice();
            model.hedge_steering_committee_extend_choice.ddlHedgeUnitVolume = getUnitVolume();
            model.hedge_steering_committee_extend_choice.ddlHedgeMinMax = getMinMax();
        }

        public string getDateSplit(string date, bool start = true)
        {
            string dateSplit = string.Empty;
            if (!string.IsNullOrEmpty(date))
            {
                string[] s = date.Split(new[] { " to " }, StringSplitOptions.None);
                if (start)
                {
                    dateSplit = s[0];
                }
                else
                {
                    dateSplit = s[1];
                }
            }
            return dateSplit;
        }

        private List<SelectListItem> getUnitPrice()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unit_price.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.unit_price[i].value, Value = setting.unit_price[i].key });
            }
            return list;
        }

        private List<SelectListItem> getUnitVolume()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 0; i < setting.unit_volume.Count; i++)
            {
                var unit_price = setting.unit_price.Any(x => x.mapping == setting.unit_volume[i].key) ? setting.unit_price.Where(x => x.mapping == setting.unit_volume[i].key).FirstOrDefault() : new Unit_Price();
                list.Add(new SelectListItem { Text = setting.unit_volume[i].value, Value = unit_price.key });
            }
            return list;
        }

        private List<SelectListItem> getMinMax()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.min_max.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.min_max[i].value, Value = setting.min_max[i].key });
            }
            return list;
        }
        #endregion

        #region ACTION: HEDGE STEERING COMMITTEE CHOICE
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddHedgeSteeringCommitteeMainChoice")]
        public ActionResult AddHedgeSteeringCommitteeMainChoice(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (!model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.Any(i => i.name == model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_detail_name))
            {
                var hedge_steering_committee_choice_detail_name = model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_detail_name;
                var hedge_steering_committee_choice_detail_order = string.Format("{0}",model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.Count + 1);
                HedgeSteeringCommitteeChoiceDetailViewModel hedge_steering_committee_choice_detail = new HedgeSteeringCommitteeChoiceDetailViewModel()
                {
                    hedge_steering_committee_choice_underlying_primaries = new List<HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel> { new HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel() { action = ConstantPrm.ACTION.EDIT } },
                    name = hedge_steering_committee_choice_detail_name,
                    order = hedge_steering_committee_choice_detail_order
                };
                model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.Add(hedge_steering_committee_choice_detail);
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DeleteHedgeSteeringCommitteeMainChoice")]
        public ActionResult DeleteHedgeSteeringCommitteeMainChoice(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_detail_name != null)
            {
                model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.RemoveAll(i => i.name == model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_detail_name);
                model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details = model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.OrderBy(o => Convert.ToDecimal(o.order)).ToList();
                for (int i = 0; i < model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.Count; i++)
                {
                    model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details[i].order = string.Format("{0}", (i + 1));
                }
                if (model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.Any())
                {
                    model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_detail_name = model.hedge_steering_committee_main_choice.hedge_steering_committee_choice_details.FirstOrDefault().name;
                }
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddHedgeSteeringCommitteeExtendChoice")]
        public ActionResult AddHedgeSteeringCommitteeExtendChoice(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (!model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.Any(i => i.name == model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_detail_name))
            {
                var hedge_steering_committee_choice_detail_name = model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_detail_name;
                var hedge_steering_committee_choice_detail_order = string.Format("{0}", model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.Count + 1);
                HedgeSteeringCommitteeChoiceDetailViewModel hedge_steering_committee_choice_detail = new HedgeSteeringCommitteeChoiceDetailViewModel()
                {
                    hedge_steering_committee_choice_underlying_primaries = new List<HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel> { new HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel() { action = ConstantPrm.ACTION.EDIT } },
                    name = hedge_steering_committee_choice_detail_name,
                    order = hedge_steering_committee_choice_detail_order
                };
                model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.Add(hedge_steering_committee_choice_detail);
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DeleteHedgeSteeringCommitteeExtendChoice")]
        public ActionResult DeleteHedgeSteeringCommitteeExtendChoice(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_detail_name != null)
            {
                model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.RemoveAll(i => i.name == model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_detail_name);
                model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details = model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.OrderBy(o => Convert.ToDecimal(o.order)).ToList();
                for (int i = 0; i < model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.Count; i++)
                {
                    model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details[i].order = string.Format("{0}", (i + 1));
                }
                if (model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.Any())
                {
                    model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_detail_name = model.hedge_steering_committee_extend_choice.hedge_steering_committee_choice_details.FirstOrDefault().name;
                }
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        public JsonResult GetHedgeSteeringCommitteeChoiceDetail(string row_id)
        {
            HedgeAnnualTypeDetailViewModel model = new HedgeAnnualTypeDetailViewModel();
            HedgeSteeringCommitteeServiceModel service = new HedgeSteeringCommitteeServiceModel();
            model = service.GetHedgeAnnualProductDetail(row_id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ACTION: HEDGE STEERING COMMITTEE TOOL
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddHedgeSteeringCommitteeTool")]
        public ActionResult AddHedgeSteeringCommitteeTool(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (!model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.Any(i => i.hedge_tool_detail == model.hedge_steering_committee_tool.hedge_steering_committee_tool_detail))
            {
                HedgeToolServiceModel hedge_tool_service = new HedgeToolServiceModel();
                HedgeToolViewModel_Detail hedge_tool_view = hedge_tool_service.Get(model.hedge_steering_committee_tool.hedge_steering_committee_tool_detail);
                if (hedge_tool_view != null)
                {
                    var row_id = hedge_tool_view.HMT_ROW_ID;
                    var code = hedge_tool_view.Code;
                    var name = hedge_tool_view.Name;
                    var order = string.Format("{0}", model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.Count + 1);

                    var flag_capped = hedge_tool_view.FeatureCappedFlag;
                    var flag_target_redemption = hedge_tool_view.FeatureTargetFlag;
                    var flag_extendible = hedge_tool_view.FeatureExtendFlag;
                    var flag_knock_in_out = hedge_tool_view.FeatureKnockInOutFlag;
                    var flag_swaption = hedge_tool_view.FeatureKnockInOutFlag;
                    HedgeSteeringCommitteeToolDetailViewModel hedge_steering_committee_tool_detail = new HedgeSteeringCommitteeToolDetailViewModel()
                    {
                        hedge_tool_detail = row_id,
                        hedge_tool_detail_code = code,
                        hedge_tool_detail_name = name,
                        hedge_tool_detail_order = order,
                        feature_capped = flag_capped,
                        feature_target_redemption = flag_target_redemption,
                        feature_extendible = flag_extendible,
                        feature_knock_in_out = flag_knock_in_out,
                        feature_swaption = flag_swaption                        
                    };
                    model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.Add(hedge_steering_committee_tool_detail);
                }
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "DeleteHedgeSteeringCommitteeTool")]
        public ActionResult DeleteHedgeSteeringCommitteeTool(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (model.hedge_steering_committee_tool.hedge_steering_committee_tool_detail != null)
            {
                model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.RemoveAll(i => i.hedge_tool_detail == model.hedge_steering_committee_tool.hedge_steering_committee_tool_detail);

                model.hedge_steering_committee_tool.hedge_steering_committee_tool_details = model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.OrderBy(o => Convert.ToDecimal(o.hedge_tool_detail_order)).ToList();
                for (int i = 0; i < model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.Count; i++)
                {
                    model.hedge_steering_committee_tool.hedge_steering_committee_tool_details[i].hedge_tool_detail_order = string.Format("{0}", (i + 1));
                }
                if (model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.Any())
                {
                    model.hedge_steering_committee_tool.hedge_steering_committee_tool_detail = model.hedge_steering_committee_tool.hedge_steering_committee_tool_details.FirstOrDefault().hedge_tool_detail;
                }
            }
            initializeDropDownList(ref model);
            return View("Create", model);
        }

        [HttpPost]
        public JsonResult SearchHedgeTool(string ToolName = "", string Desc = "", string Option = "")
        {
            HedgeToolServiceModel service = new HedgeToolServiceModel();
            HedgeToolViewModel model = new HedgeToolViewModel();
            HedgeToolViewModel_Search viewModelSearch = new HedgeToolViewModel_Search();
            viewModelSearch.sName = ToolName;
            viewModelSearch.sDesc = Desc;
            viewModelSearch.sOption = Option;
            viewModelSearch.sStatus = ConstantPrm.ACTION.ACTIVE;
            service.Search(ref viewModelSearch);
            model.tool_Search = viewModelSearch;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion


        private ResponseData SaveDataHedgeSteeringCommittee(DocumentActionStatus _status, HedgeSteeringCommitteeViewModel model, string note = "", string json_fileUpload = "")
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }

            HedgingRootObject obj = new HedgingRootObject();

            //Assign value here

            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000073;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.HEDG });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEM.HEDG });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = "N" });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            resData = service.CallService(req);
            return resData;
        }

        private ResponseData LoadDataHedgeSteeringCommittee(string TransactionID, ref HedgeSteeringCommitteeViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000072;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEM.HEDG });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                HedgingRootObject data_model = new JavaScriptSerializer().Deserialize<HedgingRootObject>(_model.data_detail);
                HedgeSteeringCommitteeServiceModel serviceModel = new HedgeSteeringCommitteeServiceModel();
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                model.buttonMode = "MANUAL";
                model.pageMode = "READ";
            }

            //string _btnBack = "<input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("BACK") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" />";
            //model.buttonCode += _btnBack;

            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref HedgeSteeringCommitteeViewModel model)
        {
            model.buttonMode = "MANUAL";
            model.pageMode = "READ";
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count > 0)
            {
                model.pageMode = "EDIT";
            }
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SUBMIT).ToList().Count > 0)
            {
                model.pageMode = "EDIT";
            }
            foreach (ButtonAction _button in Button)
            {
                string _btn = "";
                if (_button.name.ToUpper() == ConstantPrm.ACTION.SUBMIT)
                {
                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='button-control btn btn-success validate-button' style='margin-right: -350px;background:#0a7ab1;' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                }
                else if (_button.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT)
                {
                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='button-control btn btn-success validate-button' style='background:#0a7ab1;' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                }
                else
                {
                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                }
                model.buttonCode += _btn;
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            ButtonListHedgeSteeringCommittee = Button;
        }


        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, HedgeSteeringCommitteeViewModel model)
        {
            if (ButtonListHedgeSteeringCommittee != null)
            {
                var _lstButton = ButtonListHedgeSteeringCommittee.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    string name = _lstButton[0].name;

                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = model.reqID;
                    string TranID = model.taskID;

                    //assign value here
                    HedgingRootObject data_model = new HedgingRootObject();

                    string json = new JavaScriptSerializer().Serialize(data_model);

                    if (model.status == "ACTIVE")
                    {
                        //generate a new transaction and attach this tran id in note.
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = TranID });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = "" });
                        _param.Add(new ReplaceParam() { ParamName = "CPAIVerifyRequireInputContinueState", ParamVal = "" });
                    }
                    else
                    {
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = TranID });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = "" });
                    }

                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);

                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string path = string.Format("~/CPAIMVC/HedgeSteeringCommittee/Edit?TranID={0}&Tran_Req_ID={1}", tranID, reqID);
                            return Redirect(path);
                        }
                        else
                        {
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            modelView = model;
            return RedirectToAction("Edit");
        }
    }
}