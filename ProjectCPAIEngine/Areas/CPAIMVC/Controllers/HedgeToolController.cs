﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeToolController : BaseController
    {

        public ActionResult Index()
        {
            HedgeToolViewModel model = new HedgeToolViewModel();
            model.pageMode = "CREATE";
            model.ddlToolSwitch = DropdownServiceModel.getToolKnock();
            model.ddlToolOption = DropdownServiceModel.getToolOption();
            model.ddlToolConditionOperand = DropdownServiceModel.getToolConditionOperand();
            model.ddlToolConditionOperator = DropdownServiceModel.getToolConditionOperator();
            model.ddlToolFormular = DropdownServiceModel.getFormulaByTool();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(FormCollection form, HedgeToolViewModel model)
        {
            if (ViewBag.action_create)
            {
                HedgeToolServiceModel serviceModel = new HedgeToolServiceModel();
                ReturnValue rtn = new ReturnValue();
                updateModel(form, model);
                rtn = serviceModel.Add(model.tool_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                HedgeToolViewModel toolmodel = initialModel();
                toolmodel.tool_Detail = toolmodel.tool_Detail;

                return View(toolmodel);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeTool" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string HedgeToolID)
        {
            if (ViewBag.action_edit)
            {
                HedgeToolServiceModel serviceModel = new HedgeToolServiceModel();

                HedgeToolViewModel model = initialModel();
                model.tool_Detail = serviceModel.Get(HedgeToolID);
                TempData["HedgeToolID"] = HedgeToolID;

                return View("Index", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeTool" }));
            }

        }

        [HttpPost]
        public ActionResult Edit(string HedgeToolID, HedgeToolViewModel model)
        {
            if (ViewBag.action_edit)
            {
                HedgeToolServiceModel serviceModel = new HedgeToolServiceModel();
                ReturnValue rtn = new ReturnValue();
                updateModel(null, model);
                //model.tool_Detail.Status = model.tool_Detail.Status == true ? "ACTIVE" : "INACTIVE";
                rtn = serviceModel.Edit(model.tool_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["HedgeToolID"] = HedgeToolID;

                model.ddlToolSwitch = DropdownServiceModel.getToolKnock();
                model.ddlToolOption = DropdownServiceModel.getToolOption();
                model.ddlToolConditionOperand = DropdownServiceModel.getToolConditionOperand();
                model.ddlToolConditionOperator = DropdownServiceModel.getToolConditionOperator();
                model.ddlToolFormular = DropdownServiceModel.getFormulaByTool();
                return View("Index", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeTool" }));
            }

        }

        public ActionResult Search()
        {
            HedgeToolViewModel model = initialModel();
            model.tool_Search = new HedgeToolViewModel_Search();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgeToolViewModel tempModel)
        {
            HedgeToolViewModel model = initialModel();

            HedgeToolServiceModel serviceModel = new HedgeToolServiceModel();
            HedgeToolViewModel_Search viewModelSearch = new HedgeToolViewModel_Search();

            viewModelSearch = tempModel.tool_Search;
            serviceModel.Search(ref viewModelSearch);
            model.tool_Search = viewModelSearch;
            return View(model);
        }

        [HttpGet]
        public ActionResult View(string HedgeToolID)
        {
            if (ViewBag.action_view)
            {
                HedgeToolServiceModel serviceModel = new HedgeToolServiceModel();

                HedgeToolViewModel model = initialModel();
                model.tool_Detail = serviceModel.Get(HedgeToolID);
                TempData["HedgeToolID"] = HedgeToolID;

                return View("Index", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeTool" }));
            }
        }

        public HedgeToolViewModel initialModel()
        {
            HedgeToolViewModel model = new HedgeToolViewModel();
            model.tool_Search = new HedgeToolViewModel_Search();
            model.tool_Search.sSearchData = new List<HedgeToolViewModel_SearchData>();
            model.tool_Detail = new HedgeToolViewModel_Detail();
            model.ddlStatus = DropdownServiceModel.getMasterStatus();

            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
            msdata = msdata.OrderBy(x => x).ToList();
            List<SelectListItem> userList = new List<SelectListItem>();
            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            model.ddlUser = userList.Distinct().ToList();

            model.ddlToolSwitch = DropdownServiceModel.getToolKnock();
            model.ddlToolOption = DropdownServiceModel.getToolOption();
            model.ddlToolConditionOperand = DropdownServiceModel.getToolConditionOperand();
            model.ddlToolConditionOperator = DropdownServiceModel.getToolConditionOperator();
            model.ddlToolFormular = DropdownServiceModel.getFormulaByTool();

            return model;
        }

        private void updateModel(FormCollection frm, HedgeToolViewModel model)
        {
            if (model.tool_Detail != null)
            {
                if (model.tool_Detail.ToolSwitch != null)
                {
                    for (int i = 0; i < model.tool_Detail.ToolSwitch.Count; i++)
                    {
                        model.tool_Detail.ToolSwitch[i].Order = (i + 1) + "";
                    }
                }

                if (model.tool_Detail.ToolOption != null)
                {
                    for (int i = 0; i < model.tool_Detail.ToolOption.Count; i++)
                    {
                        model.tool_Detail.ToolOption[i].Order = (i + 1) + "";
                        for (int j = 0; j < model.tool_Detail.ToolOption[i].ToolCondition.Count; j++)
                        {
                            model.tool_Detail.ToolOption[i].ToolCondition[j].Order = (i + 1) + "";


                            string[] sData = model.tool_Detail.ToolOption[i].ToolCondition[j].Formula.Split('|');
                            model.tool_Detail.ToolOption[i].ToolCondition[j].Formula = sData[0];
                            model.tool_Detail.ToolOption[i].ToolCondition[j].Expression = sData[1];
                        }
                    }
                }
            }


            //string type = frm["tool_Detail.Status"].ToString();
            //if (type == "M")
            //{
            //    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Marketing Order", type_flag = "Y" });
            //    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Private Order", type_flag = "N" });

            //}
            //else
            //{
            //    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Marketing Order", type_flag = "N" });
            //    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Private Order", type_flag = "Y" });
            //}
        }
    }
}