﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeTypeController : BaseController
    {
        // GET: CPAIMVC/HedgeType
        public ActionResult Index()
        {
            return RedirectToAction("Search", "HedgeType");
        }

        [HttpGet]
        public ActionResult Search()
        {
            HedgeTypeViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgeTypeViewModel tempModel)
        {
            HedgeTypeViewModel model = initialModel();
            HedgeTypeServiceModel serviceModel = new HedgeTypeServiceModel();
            HedgeTypeViewModel_Seach viewModelSearch = new HedgeTypeViewModel_Seach();
            viewModelSearch = tempModel.ht_Search;
            serviceModel.Search(ref viewModelSearch);
            model.ht_Search = viewModelSearch;
            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                HedgeTypeViewModel model = initialModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeType" }));
            }
        }


        [HttpPost]
        public ActionResult Create(FormCollection frm,HedgeTypeViewModel model)
        {
            if (ViewBag.action_create)
            {
                HedgeTypeServiceModel serviceModel = new HedgeTypeServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.ht_Detail.HedgeTypeStatus = model.ht_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                model.ht_Detail.HedgeTypeInventory = model.ht_Detail.BoolInventory == true ? "Y" : "N";
                rtn = serviceModel.Add(model.ht_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                model.ddl_Status = DropdownServiceModel.getMasterStatus();
                model.ddl_Inventory = DropdownServiceModel.getMasterADLogin();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeType" }));
            }
        }


        [HttpGet]
        public ActionResult Edit(string TypeID)
        {
            if (ViewBag.action_edit)
            {
                HedgeTypeServiceModel serviceModel = new HedgeTypeServiceModel();

                HedgeTypeViewModel model = initialModel();
                model.ht_Detail = serviceModel.Get(TypeID);
                TempData["CompanyCode"] = TypeID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeCompany" }));
            }
        }


        [HttpPost]
        public ActionResult Edit(string TypeID, HedgeTypeViewModel model)
        {
            if (ViewBag.action_edit)
            {
                HedgeTypeServiceModel serviceModel = new HedgeTypeServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.ht_Detail.HedgeTypeStatus = model.ht_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                model.ht_Detail.HedgeTypeInventory = model.ht_Detail.BoolInventory == true ? "Y" : "N";
                rtn = serviceModel.Edit(model.ht_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                model.ddl_Status = DropdownServiceModel.getMasterStatus();
                model.ddl_Inventory = DropdownServiceModel.getMasterADLogin();
                return View("Create", model);          
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeCompany" }));
            }

        }


        [HttpGet]
        public ActionResult ViewDetail(string TypeID)
        {
            if (ViewBag.action_view)
            {
                HedgeTypeServiceModel serviceModel = new HedgeTypeServiceModel();

                HedgeTypeViewModel model = initialModel();
                model.ht_Detail = serviceModel.Get(TypeID);
                TempData["CompanyCode"] = TypeID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "HedgeCompany" }));
            }

        }


        public HedgeTypeViewModel initialModel()
        {
            HedgeTypeViewModel model = new HedgeTypeViewModel();
            model.ht_Search = new HedgeTypeViewModel_Seach();
            model.ht_Search.sSearchData = new List<HedgeTypeViewModel_SeachData>();
            model.ht_Detail = new HedgeTypeViewModel_Detail();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_Inventory = DropdownServiceModel.getMasterADLogin();
            return model;
        }
    }
}