﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HolidayController : BaseController
    {
        // GET: CPAIMVC/Holiday
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Holiday");
        }

        [HttpGet]
        public ActionResult Search()
        {
            HolidayViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HolidayViewModel viewModel)
        {
            HolidayViewModel model = initialModel();
            HolidayServiceModel serviceModel = new HolidayServiceModel();
            HolidayViewModel_Search viewModelSearch = new HolidayViewModel_Search();

            viewModelSearch = viewModel.holiday_Search;
            serviceModel.Search(ref viewModelSearch);
            model.holiday_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                HolidayViewModel model = initialModel();

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Holiday" }));
            }
        }

        [HttpPost]
        public ActionResult Create(HolidayViewModel pModel)
        {
            if (ViewBag.action_create)
            {
                HolidayServiceModel serviceModel = new HolidayServiceModel();
                ReturnValue rtn = new ReturnValue();

                HolidayViewModel_Detail modelDetail = new HolidayViewModel_Detail();
                modelDetail = pModel.holiday_Detail;
                rtn = serviceModel.Add(ref modelDetail, lbUserName);
                pModel.holiday_Detail = modelDetail;

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                HolidayViewModel model = initialModel();
                model.holiday_Detail = pModel.holiday_Detail;
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Holiday" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string HolidayID)
        {
            if (ViewBag.action_edit)
            {
                HolidayServiceModel serviceModel = new HolidayServiceModel();

                HolidayViewModel model = initialModel();
                model.holiday_Detail = serviceModel.Get(HolidayID);
              
                TempData["HolidayID"] = HolidayID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Holiday" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string HolidayID, HolidayViewModel pModel)
        {
            if (ViewBag.action_edit)
            {
                HolidayServiceModel serviceModel = new HolidayServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(pModel.holiday_Detail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["HolidayID"] = HolidayID;

                HolidayViewModel model = initialModel();
                model.holiday_Detail = pModel.holiday_Detail;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Holiday" }));
            }
        }

        [HttpGet]
        public ActionResult Delete(string HolidayID)
        {
            if (ViewBag.action_delete)
            {
                HolidayServiceModel serviceModel = new HolidayServiceModel();

                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Delete(HolidayID);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["HolidayID"] = HolidayID;

                HolidayViewModel model = initialModel();
                return View("search",model);
                
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Holiday" }));
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(string HolidayID)
        {
            if (ViewBag.action_view)
            {
                HolidayServiceModel serviceModel = new HolidayServiceModel();

                HolidayViewModel model = initialModel();
                model.holiday_Detail = serviceModel.Get(HolidayID);

                TempData["HolidayID"] = HolidayID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Holiday" }));
            }
        }

        public HolidayViewModel initialModel()
        {
            HolidayViewModel model = new HolidayViewModel();
            model.holiday_Detail = new HolidayViewModel_Detail();
            
            model.ddl_HolidayType = DropdownServiceModel.getHolidayType();
           
            return model;
        }
    }
}