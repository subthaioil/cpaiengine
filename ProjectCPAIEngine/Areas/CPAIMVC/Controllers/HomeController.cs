﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HomeController : Controller
    {
        public Setting setting
        {
            get
            {
                object o = Session["setting"];
                if (o == null)
                {
                    Setting s = new Setting();
                    return s;
                }
                else
                {
                    return (Setting)o;
                }

            }
            set
            {
                Session["setting"] = value;
            }

        }
        ShareFn _FN = new ShareFn();
        // GET: CPAIMVC/Home

        private void LoadMaster()
        {
            //LoadMaster From JSON
            setting = JSONSetting.getSetting("JSON_BUNKER_CRUDE");
        }

        private void LoadDataBunker(string TransactionID)
        {

            RequestCPAI req =
                new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.BUNKER });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            

        }

        public void testXML()
        {
            StreamReader _sr = new StreamReader(@"C:\Test\Test.txt");
            string Line = _sr.ReadToEnd();
            //CharteringModel _modelBunker = new JavaScriptSerializer().Deserialize<CharteringModel>(Line);

        }
               

        public ActionResult Index()
        {
            testXML();
            //LoadMaster();
            //LoadDataBunker("");
            return View();
        }

        public ActionResult Index2()
        {
            return View();
        }

        public string GetReport()
        {
            StringBuilder _str = new StringBuilder();


            return "";
        }

        public ActionResult toLogout()
        {
            Const.User = null;
            return Redirect("../../web/login.aspx");

           // return Redirect("~/web/login.aspx");
        }

    }
}