﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class JettyController : BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "Jetty");
        }

        [HttpPost]
        public ActionResult Search(JettyViewModel tempModel)
        {

            JettyViewModel model = initialModel();

            JettyServiceModel serviceModel = new JettyServiceModel();
            JettyViewModel_Search viewModelSearch = new JettyViewModel_Search();

            viewModelSearch = tempModel.jetty_Search;
            serviceModel.Search(ref viewModelSearch);
            model.jetty_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            JettyViewModel model = initialModel();
            return View(model);
        }

        public static JettyViewModel SearchQuery(JettyViewModel JettyModel)
        {
            JettyViewModel_Search viewModelSearch = new JettyViewModel_Search();
            JettyServiceModel serviceModel = new JettyServiceModel();

            viewModelSearch = JettyModel.jetty_Search;
            serviceModel.Search(ref viewModelSearch);
            JettyModel.jetty_Search = viewModelSearch;

            return JettyModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, JettyViewModel model)
        {
            if (ViewBag.action_create)
            {
                JettyServiceModel serviceModel = new JettyServiceModel();
                ReturnValue rtn = new ReturnValue();
                //model.jetty_Detail.Status = model.jetty_Detail.boolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Add(model.jetty_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                JettyViewModel jettymodel = initialModel();
                jettymodel.jetty_Detail = model.jetty_Detail;

                return View(jettymodel);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Jetty" }));
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                JettyViewModel model = initialModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Jetty" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string JettyID, JettyViewModel model)
        {
            if (ViewBag.action_edit)
            {
                JettyServiceModel serviceModel = new JettyServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.jetty_Detail.Status = model.jetty_Detail.boolStatus == true ? "ACTIVE" : "INACTIVE";
                rtn = serviceModel.Edit(model.jetty_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["JettyID"] = JettyID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Jetty" }));
            }

        }

        [HttpGet]
        public ActionResult Edit(string JettyID)
        {
            if (ViewBag.action_edit)
            {
                JettyServiceModel serviceModel = new JettyServiceModel();

                JettyViewModel model = initialModel();
                model.jetty_Detail = serviceModel.Get(JettyID);
                TempData["JettyID"] = JettyID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Jetty" }));
            }

        }

        [HttpGet]
        public ActionResult View(string JettyID)
        {
            if (ViewBag.action_view)
            {
                JettyServiceModel serviceModel = new JettyServiceModel();

                JettyViewModel model = initialModel();
                model.jetty_Detail = serviceModel.Get(JettyID);
                TempData["JettyID"] = JettyID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Jetty" }));
            }

        }

        public JettyViewModel initialModel()
        {
            JettyViewModel model = new JettyViewModel();

            model.jetty_Search = new JettyViewModel_Search();
            model.jetty_Search.sSearchData = new List<JettyViewModel_SearchData>();
            model.jetty_Detail = new JettyViewModel_Detail();
            model.jetty_Detail.boolStatus = true;

            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            List<SelectListItem> crudeVehicle = DropdownServiceModel.getVehicle();
            List<SelectListItem> productVehicle = DropdownServiceModel.getVehicle("PRODUCT");
            crudeVehicle.AddRange(productVehicle);
            model.ddl_Vehicle = crudeVehicle.OrderBy(x => x.Text).ToList();
            model.ddl_Country = DropdownServiceModel.getCountry();
            model.ddl_CreateType = DropdownServiceModel.getJettyCreateType();

            model.jettyIDJSON = JettyServiceModel.GetJettyIDJSON();

            return model;
        }
    }
}