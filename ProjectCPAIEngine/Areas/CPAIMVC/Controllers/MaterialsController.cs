﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class MaterialsController : BaseController
    {
        public MaterialsViewModel initialModel()
        {
            MaterialsServiceModel serviceModel = new MaterialsServiceModel();
            //HedgingFrameworkServiceModel hedgingFrameWorkModel = new HedgingFrameworkServiceModel();
            MaterialsViewModel model = new MaterialsViewModel();

            model.met_Search = new MaterialsViewModel_Search();
            model.met_Search.sSearchData = new List<MaterialsViewModel_SearchData>();

            model.met_Detail = new MaterialsViewModel_Detail();
            model.met_Detail.Control = new List<MaterialsViewModel_Control>();

            
            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();

            model.ddl_CtrlSystem = DropdownServiceModel.getMaterialsCtrlSystem();
            model.ddl_CtrlUnitPrice = DropdownServiceModel.getUnitPrice();


            model.met_CtrlSystemJSON = MaterialsServiceModel.GetCtrlSystemJSON();           
            //model.met_CtrlUnitVolumeJSON = Newtonsoft.Json.JsonConvert.SerializeObject(HedgingFrameworkServiceModel.getUnitPrice());
            //model.met_CtrlUnitPriceJSON = Newtonsoft.Json.JsonConvert.SerializeObject(DropdownServiceModel.getUnitPrice()); 

            model.met_DivisionJSON = MaterialsServiceModel.GetDivisionJSON();
            model.met_FlaMatDeletionJSON = MaterialsServiceModel.GetFlaMatDeletionJSON();
            model.met_MatTypeJSON = MaterialsServiceModel.GetMatTypeJSON();
            
            return model;
        }

        // GET: CPAIMVC/Materials
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Materials");
        }

        [HttpGet]
        public ActionResult Search()
        {
            MaterialsViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(MaterialsViewModel pModel)
        {

            MaterialsViewModel model = initialModel();

            MaterialsServiceModel serviceModel = new MaterialsServiceModel();
            MaterialsViewModel_Search viewModelSearch = new MaterialsViewModel_Search();

            viewModelSearch = pModel.met_Search;
            serviceModel.Search(ref viewModelSearch);
            model.met_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(string MaterialsNum)
        {
            if (ViewBag.action_edit)
            {
                MaterialsServiceModel serviceModel = new MaterialsServiceModel();

                MaterialsViewModel model = initialModel();
                model.met_Detail = serviceModel.Get(MaterialsNum);
                //if (model.met_Detail.Control.Count < 1)
                //{
                //    model.met_Detail.Control.Add(new MaterialsViewModel_Control { RowID = "", System = "" });
                //}

                TempData["MaterialsNum"] = MaterialsNum;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Materials" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string MaterialsNum, MaterialsViewModel model)
        {
            if (ViewBag.action_edit)
            {
                MaterialsServiceModel serviceModel = new MaterialsServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(model.met_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["MaterialsNum"] = MaterialsNum;

                MaterialsViewModel modelDropdownSet = initialModel();
                modelDropdownSet.met_Detail = model.met_Detail;

                return View("Create", modelDropdownSet);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Materials" }));
            }
        }

        
        [HttpGet]
        public ActionResult ViewDetail(string MaterialsNum)
        {
            if (ViewBag.action_view)
            {
                MaterialsServiceModel serviceModel = new MaterialsServiceModel();

                MaterialsViewModel model = initialModel();
                model.met_Detail = serviceModel.Get(MaterialsNum);
                if (model.met_Detail.Control.Count < 1)
                {
                    model.met_Detail.Control.Add(new MaterialsViewModel_Control { RowID = "", System = "" });
                }

                TempData["MaterialsNum"] = MaterialsNum;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Materials" }));
            }
        }
    }
}