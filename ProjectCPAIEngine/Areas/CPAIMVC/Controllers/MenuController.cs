﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class MenuController : BaseController
    {
        // GET: CPAIMVC/Menu
        [HttpGet]
        public ActionResult Index()
        {
            MenuViewModel model = initialModel();
            return View(model);
        }

        // Add
        [HttpPost]
        public ActionResult Index(MenuViewModel viewModel)
        {
            MenuViewModel model = initialModel();
            MenuServiceModel serviceModel = new MenuServiceModel();

            ReturnValue rtn = new ReturnValue();

            rtn = serviceModel.Add(viewModel.menu_Detail, lbUserName);

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;

            model.menu_Detail = viewModel.menu_Detail;

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(string menuID)
        {
            MenuViewModel model = initialModel();
            MenuServiceModel serviceModel = new MenuServiceModel();

            model.menu_Detail = serviceModel.Get(menuID);
            model.ddl_ListNo = MenuServiceModel.GetDDListNo(model.menu_Detail.MEU_PARENT_ID);
            model.menu_Detail.Menu_ParentDetail = MenuServiceModel.GetParentDetail(model.menu_Detail.MEU_PARENT_ID);

            //Response.Write("xxx");

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(string menuID, MenuViewModel viewModel)
        {
            MenuViewModel model = initialModel();
            MenuServiceModel serviceModel = new MenuServiceModel();
            ReturnValue rtn = new ReturnValue();

            rtn = serviceModel.Edit(viewModel.menu_Detail, lbUserName);

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;

            model.menu_Detail = viewModel.menu_Detail;
            model.ddl_ListNo = MenuServiceModel.GetDDListNo(model.menu_Detail.MEU_PARENT_ID);
            model.menu_Detail.Menu_ParentDetail = MenuServiceModel.GetParentDetail(model.menu_Detail.MEU_PARENT_ID);

            return View(model);

        }

        public MenuViewModel initialModel()
        {
            MenuViewModel model = new MenuViewModel();

            MenuViewModel_Detail modelDetail = new MenuViewModel_Detail();
            modelDetail.MEU_PARENT_ID = "#";
            modelDetail.MEU_LEVEL = "1";
            modelDetail.MEU_URL_DIRECT = "#";

            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_ControlType = DropdownServiceModel.getMasterMenuControlType();

            model.menuJSON = MenuServiceModel.GetAllMenu();
            modelDetail.MEU_IMG = "#";
            model.menu_Detail = modelDetail;


            return model;
        }

        [HttpGet]
        public JsonResult GetListNoInParent(string menuParentID)
        {
         
            MenuServiceModel serviceModel = new MenuServiceModel();
            List<SelectListItem> ListNo = new List<SelectListItem>();

            ListNo = MenuServiceModel.GetDDListNo(menuParentID);
            
            return Json(ListNo, JsonRequestBehavior.AllowGet);
        }

        // Redirect Menu
        [HttpGet]
        public ActionResult MenuURL(string menuID)
        {
            //var MenuPermission = from p in Const.User.MenuPermission
            //                     where p.MEU_ROWID.Equals(menuID) 
            //                     select p;

            List<string> lstAction = MenuServiceModel.GetActionByMenuID(menuID);

            MenuServiceModel objMenu = new MenuServiceModel();
            ShareFn _FN = new ShareFn();
            MenuViewModel_Detail menu = objMenu.Get(menuID);

            string action_menu = "";
           

            var URL = "";
            if(menu != null)
            {
                if (lstAction.Count > 0)
                {
                    int i = 0;
                    foreach (var item in lstAction)
                    {
                        if(i == 0)
                            action_menu += item;
                        else
                            action_menu += "|" + item;

                        i++;
                    }
                    Const.UserActionMenu= action_menu;
                }
                
                URL = _FN.GetSiteRootUrl(menu.MEU_URL);
                return Redirect(URL);
            }
            else
            {
                URL = _FN.GetSiteRootUrl("web/login.aspx");
                return Redirect(URL);
            }
           
            
        }
    }
}