﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class OperationalParameterController : BaseController
    {
        // GET: CPAIMVC/OperationalParameter
        public ActionResult Index()
        {
            return RedirectToAction("Search", "OperationalParameter");
        }

        [HttpGet]
        public ActionResult Search()
        {
            OperatParamViewModel model = initialModel();
            model.ddl_Unit = DropdownServiceModel.getUnit();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(OperatParamViewModel postModel)
        {
            OperatParamViewModel model = initialModel();

            OperatParamServiceModel serviceModel = new OperatParamServiceModel();
            OperatParamViewModel_Search searchModel = new OperatParamViewModel_Search();
            model.ddl_Unit = DropdownServiceModel.getUnit();
            searchModel = postModel.OperaParam_Search;
            serviceModel.Search(ref searchModel);
            model.OperaParam_Search = searchModel;
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                OperatParamViewModel model = initialModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "OperationalParameter" }));
            }

        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, OperatParamViewModel viewModel)
        {
            if (ViewBag.action_create)
            {
                OperatParamServiceModel serviceModel = new OperatParamServiceModel();
                ReturnValue rtn = new ReturnValue();
                
                rtn = serviceModel.Add(viewModel.OperatParam_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                OperatParamViewModel model = initialModel();
                model.ddl_Unit = DropdownServiceModel.getUnit();
                model.OperatParam_Detail.RowId = viewModel.OperatParam_Detail.RowId;
                return View("Create", model);
                //return RedirectToAction("Edit", model.OperatParam_Detail.RowId);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "OperationalParameter" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string rowId)
        {
            if (ViewBag.action_edit)
            {
                OperatParamServiceModel serviceModel = new OperatParamServiceModel();

                OperatParamViewModel model = initialModel();
                model.ddl_Unit = DropdownServiceModel.getFreeUnitFromOper2(rowId);
                model.OperatParam_Detail = serviceModel.GetOperParamDetail(rowId);
                TempData["rowId"] = rowId;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "OperationalParameter" }));
            }
        }

        [HttpPost]
        public ActionResult Edit (FormCollection frm, OperatParamViewModel viewModel)
        {
            if (ViewBag.action_edit)
            {
                OperatParamServiceModel serviceModel = new OperatParamServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(viewModel.OperatParam_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                OperatParamViewModel model = initialModel();
                //viewModel.ddl_Unit = DropdownServiceModel.getUnit();
                model.OperatParam_Detail.RowId = viewModel.OperatParam_Detail.RowId;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "OperationalParameter" }));
            }
        }


        [HttpGet]
        public ActionResult View(string rowId)
        {
            if (ViewBag.action_view)
            {
                OperatParamServiceModel serviceModel = new OperatParamServiceModel();

                OperatParamViewModel model = initialModel();
                model.ddl_Unit = DropdownServiceModel.getFreeUnitFromOper2(rowId);
                model.OperatParam_Detail = serviceModel.GetOperParamDetail(rowId);
                TempData["rowId"] = rowId;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "OperationalParameter" }));
            }

        }

        public OperatParamViewModel initialModel()
        {
            OperatParamViewModel model = new OperatParamViewModel();

            model.OperaParam_Search = new OperatParamViewModel_Search();
            model.OperaParam_Search.sSearchData = new List<OperatParamViewModel_SearchData>();


            model.OperatParam_Detail = new OperatParamViewModel_Detail();
            // model.ddl_Unit = DropdownServiceModel.getUnit();
            model.ddl_Unit = DropdownServiceModel.getFreeUnitFromOper();



            #region custom  select 

            model.ddl_Data_Operation = new List<SelectListItem>();
            model.ddl_Data_Operation.Add(new SelectListItem { Text = "=", Value = "==" });
            model.ddl_Data_Operation.Add(new SelectListItem { Text = "≠", Value = "!=" });
            model.ddl_Data_Operation.Add(new SelectListItem { Text = ">", Value = ">" });
            model.ddl_Data_Operation.Add(new SelectListItem { Text = "<", Value = "<" });
            model.ddl_Data_Operation.Add(new SelectListItem { Text = ">=", Value = ">=" });
            model.ddl_Data_Operation.Add(new SelectListItem { Text = "<=", Value = "<=" });



            model.ddl_Data_AndOr = new List<SelectListItem>();
            model.ddl_Data_AndOr.Add(new SelectListItem { Text = " and ", Value = "&&" });
            model.ddl_Data_AndOr.Add(new SelectListItem { Text = " or ", Value = "||" });
            #endregion



            return model;
        }
    }
}