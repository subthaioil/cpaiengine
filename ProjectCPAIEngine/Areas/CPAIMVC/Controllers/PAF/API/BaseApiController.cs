using com.pttict.engine.dal.Entity;
using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCDO;
using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.DALDAF;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.ConstantPrm;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API
{
    [AllowAnonymous]
    public class BaseApiController : BaseController
    {
        DAF_DATA_DAL daf_dal = new DAF_DATA_DAL();
        CDS_DATA_DAL cds_dal = new CDS_DATA_DAL();
        CDO_DATA_DAL cdo_dal = new CDO_DATA_DAL();
        
        [NonAction]
        public ActionResult SendJson(dynamic input)
        {
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(input, microsoftDateFormatSettings);
            return Content(json, "application/json");
            //return Json(input, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public bool MatchButton(string status, string button_name)
        {
            string[] draft_btns = { "SAVE DRAFT", "SUBMIT", "CANCEL", };
            string[] waiting_verify_btns = { "VERIFY", "REJECT", "CANCEL", };
            string[] waiting_endorse_btns = { "ENDORSE", "REJECT", "CANCEL", };
            string[] waiting_approve_btns = { "APPROVE", "REJECT", "CANCEL", };
            string[] approved_btns = { "REJECT", "CANCEL", };
            string[] cancel_btns = { };

            if (status == ConstantPrm.ACTION.DRAFT)
            {
                if(draft_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if(status == ConstantPrm.ACTION.WAITING_VERIFY)
            {
                if (waiting_verify_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_ENDORSE)
            {
                if (waiting_endorse_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_APPROVE)
            {
                if (waiting_approve_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.CANCEL)
            {
                if (cancel_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.APPROVE)
            {
                if (approved_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else
            {

            }
            return false;
        }

        [NonAction]
        public PAF_DATA_WRAPPER LoadApprovalData(string transaction_id)
        {
            PAF_DATA_WRAPPER result = PAF_DATA_DAL.Get(transaction_id);
            result.Buttons = new List<PAFButtonAction>();
            string CurrentAction = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000046;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = transaction_id });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.PAF });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.OTHER });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                foreach (ButtonAction b in _model.buttonDetail.Button)
                {
                    PAFButtonAction ba = new PAFButtonAction();
                    ba.name = b.name;
                    //bool is_show_button = MatchButton(result.PDA_STATUS, b.name);
                    //if (is_show_button) {
                        result.Buttons.Add(ba);
                    //}
                }
            }

            return result;
        }

        [NonAction]
        public DAF_DATA_WRAPPER LoadDAFData(string transaction_id)
        {
            DAF_DATA_WRAPPER result = daf_dal.GetWrap(transaction_id);
            result.Buttons = new List<DAFButtonAction>();
            string CurrentAction = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000082;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = transaction_id });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.DAF });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.OTHER });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                foreach (ButtonAction b in _model.buttonDetail.Button)
                {
                    DAFButtonAction ba = new DAFButtonAction();
                    ba.name = b.name;
                    result.Buttons.Add(ba);
                }
            }

            return result;
        }

        [NonAction]
        public CDS_DATA LoadCDSData(string transaction_id)
        {
            CDS_DATA result = cds_dal.GetWrap(transaction_id);
            result.Buttons = new List<CDSButtonAction>();
            string CurrentAction = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000089;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = transaction_id });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CDS });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.OTHER });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                foreach (ButtonAction b in _model.buttonDetail.Button)
                {
                    CDSButtonAction ba = new CDSButtonAction();
                    ba.name = b.name;
                    result.Buttons.Add(ba);
                }
            }

            return result;
        }

        [NonAction]
        public CDO_DATA LoadCDOData(string transaction_id)
        {
            CDO_DATA result = cdo_dal.GetWrap(transaction_id);
            result.Buttons = new List<CDOButtonAction>();
            string CurrentAction = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000092;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = transaction_id });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CDS });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.OTHER });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                foreach (ButtonAction b in _model.buttonDetail.Button)
                {
                    CDOButtonAction ba = new CDOButtonAction();
                    ba.name = b.name;
                    result.Buttons.Add(ba);
                }
            }

            return result;
        }

        [NonAction]
        public List_PAFtrx GetSearchResult(PAF_SEARCH_CRITERIA criteria)
        {
            string CurrentAction = "";
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000045;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.PAF });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = "44" });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            List_PAFtrx _model = ShareFunction.DeserializeXMLFileToObject<List_PAFtrx>(_DataJson);
            if (_model == null) _model = new List_PAFtrx();
            if (_model.PAFTransaction == null) _model.PAFTransaction = new List<PAFEncrypt>();
            return _model;
        }

        [NonAction]
        public ResponseData SaveData(dynamic _status, PAF_DATA model, string note, string json_fileUpload = "", string attached_explain = "")
        {

            List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
            //List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons("zjaturongt");
            PAFHelper.LogUserInput(model);
            var _lstButton = button_list.Where(x => x.ABT_BUTTON_NAME == "SAVE DRAFT").ToList();

            if (_lstButton == null || _lstButton.Count == 0)
            {
                throw new Exception("Can't find " + "SAVE DRAFT" + "");
            }


            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }
            // model.explanationAttach = attached_explain;

            //var json = new JavaScriptSerializer().Serialize(model);
            string json = JsonConvert.SerializeObject(model);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000044;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            if (!String.IsNullOrEmpty(model.PDA_ROW_ID))
            {
                req.parent_trx_id = model.PDA_ROW_ID;
            }
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "user_group", v = Const.User.UserGroup });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.PAF });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.OTHER });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);
            model.PDA_ROW_ID = resData.transaction_id;
            model.PDA_FORM_ID = resData.resp_parameters[0].v;
            model.PDA_CREATED = model.PDA_CREATED ?? DateTime.Now;
            model.PDA_CREATED_BY = lbUserID;
            model.PDA_STATUS = ACTION.DRAFT;
            PAF_DATA_DAL.InsertOrUpdate(model);
            return resData;
        }

        [NonAction]
        public ResponseData SaveData(dynamic _status, DAF_DATA model, string note, string json_fileUpload = "", string attached_explain = "")
        {

            List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
            PAFHelper.LogUserInput(model);

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }
            // model.explanationAttach = attached_explain;

            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000080;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            //if (!String.IsNullOrEmpty(model.PDA_ROW_ID))
            //{
            //    req.parent_trx_id = model.PDA_ROW_ID;
            //}
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "user_group", v = Const.User.UserGroup });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.DAF });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.OTHER });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);
            DAL.DALDAF.USER_GROUP_DAL ugd = new DAL.DALDAF.USER_GROUP_DAL();
            string user_group = ugd.findByUserAndSystems(lbUserName, ConstantPrm.SYSTEM.DAF);
            resData.result_code = user_group;
            //model.PDA_ROW_ID = resData.transaction_id;
            //model.PDA_FORM_ID = resData.resp_parameters[0].v;
            //model.PDA_CREATED = model.PDA_CREATED ?? DateTime.Now;
            //model.PDA_CREATED_BY = lbUserID;
            //model.PDA_STATUS = ACTION.DRAFT;
            //PAF_DATA_DAL.InsertOrUpdate(model);
            return resData;
        }

        [NonAction]
        public ResponseData SaveData(dynamic _status, CDS_DATA model, string note, string json_fileUpload = "", string attached_explain = "")
        {

            List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
            PAFHelper.LogUserInput(model);

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }
            // model.explanationAttach = attached_explain;

            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000087;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            //if (!String.IsNullOrEmpty(model.PDA_ROW_ID))
            //{
            //    req.parent_trx_id = model.PDA_ROW_ID;
            //}
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "user_group", v = Const.User.UserGroup });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CDS });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.OTHER });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);
            DAL.DALCDS.USER_GROUP_DAL ugd = new DAL.DALCDS.USER_GROUP_DAL();
            string user_group = ugd.findByUserAndSystems(lbUserName, ConstantPrm.SYSTEM.CDS);
            resData.result_code = user_group;
            //model.PDA_ROW_ID = resData.transaction_id;
            //model.PDA_FORM_ID = resData.resp_parameters[0].v;
            //model.PDA_CREATED = model.PDA_CREATED ?? DateTime.Now;
            //model.PDA_CREATED_BY = lbUserID;
            //model.PDA_STATUS = ACTION.DRAFT;
            //PAF_DATA_DAL.InsertOrUpdate(model);
            return resData;
        }

        [NonAction]
        public ResponseData SaveData(dynamic _status, CDO_DATA model, string note, string json_fileUpload = "", string attached_explain = "")
        {

            List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
            PAFHelper.LogUserInput(model);

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }
            // model.explanationAttach = attached_explain;

            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000087;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            //if (!String.IsNullOrEmpty(model.PDA_ROW_ID))
            //{
            //    req.parent_trx_id = model.PDA_ROW_ID;
            //}
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "user_group", v = Const.User.UserGroup });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CDS });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.OTHER });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);
            DAL.DALCDS.USER_GROUP_DAL ugd = new DAL.DALCDS.USER_GROUP_DAL();
            string user_group = ugd.findByUserAndSystems(lbUserName, ConstantPrm.SYSTEM.CDS);
            resData.result_code = user_group;
            //model.PDA_ROW_ID = resData.transaction_id;
            //model.PDA_FORM_ID = resData.resp_parameters[0].v;
            //model.PDA_CREATED = model.PDA_CREATED ?? DateTime.Now;
            //model.PDA_CREATED_BY = lbUserID;
            //model.PDA_STATUS = ACTION.DRAFT;
            //PAF_DATA_DAL.InsertOrUpdate(model);
            return resData;
        }

        [NonAction]
        public string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }

        [NonAction]
        public ResponseData _btn_Click(string action_name, PAF_DATA model)
        {
            ShareFn _FN = new ShareFn();
            //List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(Const.User.UserName);
            List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
            //List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons("zjaturongt");

            var _lstButton = button_list.Where(x => x.ABT_BUTTON_NAME == action_name).ToList();

            if (_lstButton == null || _lstButton.Count == 0)
            {
                throw new Exception("Can't find " + action_name + "");
            }

            string _xml = _lstButton[0].ABT_XML;
            string note = string.IsNullOrEmpty(model.PDA_REASON) ? "-" : model.PDA_REASON;

            List<ReplaceParam> _param = new List<ReplaceParam>();
            string TranReqID = model.REQ_TRAN_ID ?? ConstantPrm.EnginGetEngineID() ;
            PAFHelper.LogUserInput(model);
            //model.explanationAttach = form["hdfExplanFileUpload"];
            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);
            PAF_DATA data = JsonConvert.DeserializeObject<PAF_DATA>(json);
            _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
            _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
            _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
            _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
            _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson("")) });
            _xml = _FN.RaplceParamXML(_param, _xml);
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            RequestData _req = new RequestData();
            ResponseData resData = new ResponseData();
            _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
            resData = service.CallService(_req);
            return resData;
        }

        [NonAction]
        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        [NonAction]
        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {

                        urlPage = "~/CPAIMVC/ApprovalForm/#/";
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        PAF_DATA _item = PAF_DATA_DAL.GetNoWrap(strTransID);
                        switch (_item.PDA_TEMPLATE)
                        {
                            case "CMPS_DOM_SALE":
                                urlPage += "cmps/dom/" + strReqID + "/" + strTransID + "/edit";
                                break;
                            case "CMPS_INTER_SALE":
                                urlPage += "cmps/inter/" + strReqID + "/" + strTransID + "/edit";
                                break;
                            case "CMPS_IMPORT":
                                urlPage += "cmps/import/" + strReqID + "/" + strTransID + "/edit";
                                break;
                            case "CMLA_FORM_1":
                                urlPage += "cmla/form-1/" + strReqID + "/" + strTransID + "/edit";
                                break;
                            case "CMLA_FORM_2":
                                urlPage += "cmla/form-2/" + strReqID + "/" + strTransID + "/edit";
                                break;
                            case "CMLA_IMPORT":
                                urlPage += "cmla/import/" + strReqID + "/" + strTransID + "/edit";
                                break;
                            case "CMLA_BITUMEN":
                                urlPage += "cmla/bitumen/" + strReqID + "/" + strTransID + "/edit";
                                break;
                        }
                        path = urlPage;
                        //path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&PURNO=" + strPurNo.Encrypt();
                        //path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&PURNO=" + strPurNo.Encrypt();
                    }
                }
            }
            return path;
        }


        [NonAction]
        public ResponseData ButtonClickDAF(string action_name, DAF_DATA model)
        {
            DAF_DATA_DAL dafMan = new DAF_DATA_DAL();
            ShareFn _FN = new ShareFn();
            List<CPAI_ACTION_BUTTON> button_list = dafMan.GetButtons(lbUserID);
            PAFHelper.LogUserInput(model);
            var _lstButton = button_list.Where(x => x.ABT_BUTTON_NAME == action_name).ToList();
            if (_lstButton == null || _lstButton.Count == 0)
            {
                throw new Exception("Can't find " + action_name + "");
            }

            string _xml = _lstButton[0].ABT_XML;
            string note = string.IsNullOrEmpty(model.DDA_REASON) ? "-" : model.DDA_REASON;

            List<ReplaceParam> _param = new List<ReplaceParam>();
            string TranReqID = model.REQ_TRAN_ID ?? ConstantPrm.EnginGetEngineID();
            //model.explanationAttach = form["hdfExplanFileUpload"];
            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);
            _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
            _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
            _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
            _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
            _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson("")) });
            _xml = _FN.RaplceParamXML(_param, _xml);
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            RequestData _req = new RequestData();
            ResponseData resData = new ResponseData();
            _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
            resData = service.CallService(_req);

            DAL.DALDAF.USER_GROUP_DAL ugd = new DAL.DALDAF.USER_GROUP_DAL();
            string user_group = ugd.findByUserAndSystems(lbUserName, ConstantPrm.SYSTEM.DAF);
            resData.result_code = user_group.Substring(0, 4);
            return resData;
        }

        [NonAction]
        public ResponseData ButtonClickCDS(string action_name, CDS_DATA model)
        {
            CDS_DATA_DAL cdsMan = new CDS_DATA_DAL();
            ShareFn _FN = new ShareFn();
            List<CPAI_ACTION_BUTTON> button_list = cdsMan.GetButtons(lbUserID);
            PAFHelper.LogUserInput(model);
            var _lstButton = button_list.Where(x => x.ABT_BUTTON_NAME == action_name).ToList();
            if (_lstButton == null || _lstButton.Count == 0)
            {
                throw new Exception("Can't find " + action_name + "");
            }

            string _xml = _lstButton[0].ABT_XML;
            string note = string.IsNullOrEmpty(model.CDA_REASON) ? "-" : model.CDA_REASON;

            List<ReplaceParam> _param = new List<ReplaceParam>();
            string TranReqID = model.REQ_TRAN_ID ?? ConstantPrm.EnginGetEngineID();
            //model.explanationAttach = form["hdfExplanFileUpload"];
            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);
            _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
            _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
            _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
            _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
            _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson("")) });
            _xml = _FN.RaplceParamXML(_param, _xml);
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            RequestData _req = new RequestData();
            ResponseData resData = new ResponseData();
            _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
            resData = service.CallService(_req);

            DAL.DALCDS.USER_GROUP_DAL ugd = new DAL.DALCDS.USER_GROUP_DAL();
            string user_group = ugd.findByUserAndSystems(lbUserName, ConstantPrm.SYSTEM.CDS);
            resData.result_code = user_group.Substring(0, 4);
            return resData;
        }

        [NonAction]
        public ResponseData ButtonClickCDO(string action_name, CDO_DATA model)
        {
            CDO_DATA_DAL cdsMan = new CDO_DATA_DAL();
            ShareFn _FN = new ShareFn();
            List<CPAI_ACTION_BUTTON> button_list = cdsMan.GetButtons(lbUserID);
            PAFHelper.LogUserInput(model);
            var _lstButton = button_list.Where(x => x.ABT_BUTTON_NAME == action_name).ToList();
            if (_lstButton == null || _lstButton.Count == 0)
            {
                throw new Exception("Can't find " + action_name + "");
            }

            string _xml = _lstButton[0].ABT_XML;
            string note = string.IsNullOrEmpty(model.CDS_DATA.CDA_REASON) ? "-" : model.CDS_DATA.CDA_REASON;

            List<ReplaceParam> _param = new List<ReplaceParam>();
            string TranReqID = model.REQ_TRAN_ID ?? ConstantPrm.EnginGetEngineID();
            //model.explanationAttach = form["hdfExplanFileUpload"];
            //var json = new JavaScriptSerializer().Serialize(model);
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            string json = JsonConvert.SerializeObject(model, microsoftDateFormatSettings);
            _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
            _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
            _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
            _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
            _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson("")) });
            _xml = _FN.RaplceParamXML(_param, _xml);
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            RequestData _req = new RequestData();
            ResponseData resData = new ResponseData();
            _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
            resData = service.CallService(_req);

            DAL.DALCDS.USER_GROUP_DAL ugd = new DAL.DALCDS.USER_GROUP_DAL();
            string user_group = ugd.findByUserAndSystems(lbUserName, ConstantPrm.SYSTEM.CDS);
            resData.result_code = user_group.Substring(0, 4);
            return resData;
        }

    }
}
