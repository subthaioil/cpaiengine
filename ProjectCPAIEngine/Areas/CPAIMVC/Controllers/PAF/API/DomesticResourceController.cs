﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API
{
    [AllowAnonymous]
    public class DomesticResourceController : BaseApiController
    {
        public ActionResult GetButtons(string id)
        {
            return SendJson(PAF_DATA_DAL.GetButtons(id));
        }
        //    //public ActionResult GetList()
        //    //{
        //    //    //string CurrentAction = "";
        //    //    //RequestCPAI req = new RequestCPAI();
        //    //    //req.Function_id = ConstantPrm.FUNCTION.F10000045;
        //    //    //req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
        //    //    //req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
        //    //    //req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
        //    //    //req.State_name = "";
        //    //    //req.Req_parameters = new Req_parameters();
        //    //    //req.Req_parameters.P = new List<P>();
        //    //    //req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
        //    //    //req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
        //    //    //req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.PAF });
        //    //    //req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
        //    //    //req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
        //    //    //req.Req_parameters.P.Add(new P { K = "status", V = "" });
        //    //    //req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
        //    //    //req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
        //    //    //req.Req_parameters.P.Add(new P { K = "function_code", V = "44" });
        //    //    //req.Extra_xml = "";

        //    //    //ResponseData resData = new ResponseData();
        //    //    //RequestData reqData = new RequestData();
        //    //    //ServiceProvider.ProjService service = new ServiceProvider.ProjService();

        //    //    //var xml = ShareFunction.XMLSerialize(req);
        //    //    //reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
        //    //    //resData = service.CallService(reqData);

        //    //    //var a = PAF_DATA_DAL.GetList();

        //    //    ////return resData;
        //    //    //return Json(a, JsonRequestBehavior.AllowGet);
        //    //    return SendJson(null);
        //    //}

        //    //public ActionResult Get()
        //    //{
        //    //    var routeValues = Url.RequestContext.RouteData.Values;
        //    //    var paramName = "id";
        //    //    var id = routeValues.ContainsKey(paramName) ?
        //    //             routeValues[paramName] :
        //    //             Request.QueryString[paramName];
        //    //    string transaction_id = id.ToString();
        //    //    var a = PAF_DATA_DAL.Get(transaction_id);
        //    //    return Json(a, JsonRequestBehavior.AllowGet);
        //    //}

        //    public ActionResult GetByTransactionId(PAF_DATA model)
        //    {
        //        string transaction_id = model.REQ_TRAN_ID;
        //        PAF_DATA_WRAPPER ss = PAF_DATA_DAL.Get(transaction_id);
        //        return Json(ss, JsonRequestBehavior.AllowGet);
        //    }

        //    public ActionResult GetButtons()
        //    {
        //        var routeValues = Url.RequestContext.RouteData.Values;
        //        //var paramName = "id";
        //        //var id = routeValues.ContainsKey(paramName) ?
        //        //         routeValues[paramName] :
        //        //         Request.QueryString[paramName];
        //        //string transaction_id = id.ToString();
        //        //lbUserID = "zThaweesakC";
        //        //lbUserID = "zparint";
        //        //lbUserID = "10000024";
        //        var a = PAF_DATA_DAL.GetButtons(Const.User.UserName);
        //        //return Json(a, JsonRequestBehavior.AllowGet);
        //        Dictionary<string, dynamic> ss = new Dictionary<string, dynamic>();
        //        ss["user"] = Const.User.UserName;
        //        ss["btn"] = a;
        //        return Json(ss, JsonRequestBehavior.AllowGet);
        //    }


        //    public ActionResult SaveDraft(PAF_DATA md)
        //    {
        //        DocumentActionStatus _status = DocumentActionStatus.Draft;
        //        ResponseData result = new ResponseData();
        //        if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
        //        {
        //            result = SaveData(_status, md, "");
        //        }
        //        else
        //        {
        //            result = _btn_Click("SAVE DRAFT", md);
        //        }
        //        return SendJson(result);
        //    }

        //    public ActionResult Submit(PAF_DATA md)
        //    {
        //        DocumentActionStatus _status = DocumentActionStatus.Submit;
        //        ResponseData result = new ResponseData();
        //        //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
        //        //{
        //        //    result = SaveData(_status, md, "");
        //        //}
        //        //else
        //        //{
        //            result = _btn_Click("SUBMIT", md);
        //        //}
        //        return SendJson(result);
        //    }

        //    public ActionResult Verify(PAF_DATA md)
        //    {
        //        DocumentActionStatus _status = DocumentActionStatus.Submit;
        //        ResponseData result = new ResponseData();
        //        result = _btn_Click("VERIFY", md);
        //        return SendJson(result);
        //    }

        //    public ActionResult Endorse(PAF_DATA md)
        //    {
        //        DocumentActionStatus _status = DocumentActionStatus.Submit;
        //        ResponseData result = new ResponseData();
        //        result = _btn_Click("ENDORSE", md);
        //        return SendJson(result);
        //    }

        //    public ActionResult GenPdf(PAF_DATA md)
        //    {
        //        PAF_DATA g = PAF_DATA_DAL.GetNoWrap("201707221422110222995");
        //        PAFGenerator.genPDF(g);
        //        return null;
        //    }
    }
}