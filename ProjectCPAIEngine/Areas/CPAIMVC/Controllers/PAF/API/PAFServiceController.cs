﻿using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectCPAIEngine.Utilities.ConstantPrm;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API
{
    public class PAFServiceController : BaseApiController
    {


        public ActionResult GetTransactionList()
        {
            PAF_SEARCH_CRITERIA criteria = new PAF_SEARCH_CRITERIA();
            var resData = GetSearchResult(criteria);
            return SendJson(resData);
        }

        public ActionResult GetSearchCriteria()
        {
            PAF_DATA_LIST_DAL pdl = new PAF_DATA_LIST_DAL();
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            result.Add("CREATED_LIST", pdl.GetCreatedList(Const.User.UserName));
            result.Add("CUSTOMER_LIST", pdl.GetCustomerList(Const.User.UserName));
            result.Add("VENDOR_LIST", pdl.GetVendorList(Const.User.UserName));
            //var resData = GetSearchResult();
            return SendJson(result);
        }

        public ActionResult SearchTransaction(PAF_SEARCH_CRITERIA criteria)
        {
            var resData = GetSearchResult(criteria);
            return SendJson(resData);
        }

        public ActionResult Copy(COPPY_CRITERIA criteria)
        {

            var resData = PAF_DATA_DAL.Copy(criteria);
            return SendJson(resData);
        }

        public ActionResult GetByTransactionId(PAF_DATA model)
        {
            return FindButtons(model);
        }

        public ActionResult GetButtonRaws()
        {
            var routeValues = Url.RequestContext.RouteData.Values;
            var a = PAF_DATA_DAL.GetButtons(Const.User.UserName);
            Dictionary<string, dynamic> ss = new Dictionary<string, dynamic>();
            ss["user"] = Const.User.UserName;
            ss["btn"] = a;
            return Json(ss, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FindButtons(PAF_DATA md)
        {
            PAF_DATA_WRAPPER result = LoadApprovalData(md.TRAN_ID);
            return SendJson(result);
        }

        public ActionResult SaveDraft(PAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            ResponseData result = new ResponseData();
            if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            {
                List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
                result = SaveData(_status, md, "");
            }
            else
            {
                result = _btn_Click("SAVE DRAFT", md);
                md.PDA_FORM_ID = result.resp_parameters[0].v;
                md.PDA_CREATED = md.PDA_CREATED ?? DateTime.Now;
                md.PDA_CREATED_BY = lbUserID;
                md.PDA_STATUS = ACTION.DRAFT;
                PAF_DATA_DAL.InsertOrUpdate(md);
            }
            return SendJson(result);
        }

        public ActionResult Submit(PAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.PDA_UPDATED = DateTime.Now;
            md.PDA_UPDATED_BY = lbUserID;
            result = _btn_Click("SUBMIT", md);
            //}
            return SendJson(result);
        }

        public ActionResult Verify(PAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            md.PDA_UPDATED = DateTime.Now;
            md.PDA_UPDATED_BY = lbUserID;
            result = _btn_Click("VERIFY", md);
            //}
            return SendJson(result);
        }

        public ActionResult Endorse(PAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.PDA_UPDATED = DateTime.Now;
            md.PDA_UPDATED_BY = lbUserID;
            PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = _btn_Click("ENDORSE", md);
            //}
            return SendJson(result);
        }

        public ActionResult Cancel(PAF_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Cancel;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.PDA_UPDATED = DateTime.Now;
            md.PDA_UPDATED_BY = lbUserID;
            result = _btn_Click("CANCEL", md);
            //}
            return SendJson(result);
        }

        public ActionResult Reject(PAF_DATA md)
        {
            //DocumentActionStatus _status = DocumentActionStatus.Reject;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            //md.PDA_UPDATED = DateTime.Now;
            md.PDA_UPDATED_BY = lbUserID;
            result = _btn_Click("REJECT", md);
            //}
            return SendJson(result);
        }

        public ActionResult Approve(PAF_DATA md)
        {
            ResponseData result = new ResponseData();
            md.PDA_UPDATED = DateTime.Now;
            md.PDA_UPDATED_BY = lbUserID;
            PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = _btn_Click("APPROVE", md);
            return SendJson(result);
        }

        public ActionResult GenPdf(PAF_DATA md)
        {
            PAF_DATA g = PAF_DATA_DAL.GetNoWrap(md.TRAN_ID);
            PAFGenerator.genPDF(g);
            return null;
        }

        public ActionResult GeneratePdf(string id)
        {
            PAF_DATA g = PAF_DATA_DAL.GetNoWrap(id);
            string pdf_file = PAFGenerator.genPDF(g);
            return SendJson(pdf_file);
        }

        public ActionResult FileUpload()
        {
            PAF_ATTACH_FILE result = new PAF_ATTACH_FILE();
            try
            {
                if (Request.QueryString["PAT_TYPE"] == null) throw new Exception("Invalid Type.");
                if (Request.QueryString["Type"] == null) throw new Exception("Invalid Type.");
                string[] _Param = Request.QueryString["Type"].ToString().Split('|');
                if (_Param.Length >= 2)
                {
                    string Type = _Param[0];
                    string FNID = _Param[1];
                    string rootPath = Request.PhysicalApplicationPath;
                    System.Web.HttpFileCollection upload_file = System.Web.HttpContext.Current.Request.Files;
                    HttpPostedFile file = upload_file[0];
                    string file_name = Path.GetFileName(file.FileName);
                    if (file_name != string.Empty)
                    {
                        file_name = string.Format("{0}_{2}_{1}_{3}", DateTime.Now.ToString("yyyyMMddHHmmss"), Type, FNID, Path.GetFileName(file.FileName));
                        if (!Directory.Exists(rootPath + "Web/FileUpload/"))
                            Directory.CreateDirectory(rootPath + "Web/FileUpload/");
                        file.SaveAs(rootPath + "Web/FileUpload/" + file_name);
                    }

                    result.PAT_TYPE = Request.QueryString["PAT_TYPE"];
                    result.PAT_INFO = file_name;
                    result.PAT_PATH = "Web/FileUpload/" + file_name;
                    result.PAT_CREATED = DateTime.Now;
                    result.PAT_UPDATED_BY = lbUserID;
                    result.PAT_UPDATED = DateTime.Now;
                    result.PAT_UPDATED_BY = lbUserID;
                }
                else
                {
                    throw new Exception("Invalid Type.");
                }
            }
            catch (Exception ex)
            {
                //output.result = new OutputResult();
                //output.result.Status = "ERROR";
                //output.result.Description = ex.Message.Replace("\r\n", " ").Replace("\"", "'");
            }
            return SendJson(result);
        }

        public ActionResult ApproveEndpoint()
        {
            //ForTest
            //http://localhost:50131/CHR/CPAIMVC/CrudePurchase/ApproveCrudePurchase?token=89b7e01a8af24dc98102e6be1d3470a3

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }
    }
}