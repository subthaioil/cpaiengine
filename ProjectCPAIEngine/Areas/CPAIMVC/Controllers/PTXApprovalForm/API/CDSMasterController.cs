﻿using ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.DALCOMMON;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PTXApprovalForm.API
{
    public class CDSMasterController : BaseApiController
    {
        private string system = "CDS";
        PTX_MASTER_DAL master_mnt = new PTX_MASTER_DAL();

        // GET: CPAIMVC/CDSMaster
        public ActionResult Index()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            //result["CDS_CURRENCY_UNIT"] = master_mnt.GetCurrency();
            result["CUSTOMERS"] = master_mnt.GetCustomerList(system);
            result["MATERIALS"] = master_mnt.GetMatrials(system);
            result["INCOTERMS"] = master_mnt.GetIncoterms();
            result["SALE_TYPE"] = master_mnt.GetSaleType();
            result["SALE_METHOD"] = master_mnt.GetSaleMethod();
            result["FEED_STOCK"] = master_mnt.GetFeedStock();
            result["PAYMENT_TERM"] = master_mnt.GetPaymentTerm();
            result["PRICE_PER_UNIT"] = master_mnt.GetPricePerQuantityUnit();
            result["PROPOSAL_REASON"] = master_mnt.GetProposalReason();
            result["TOLERANCE_OPTION"] = master_mnt.GetToleranceOption();
            result["BENCHMARKS"] = master_mnt.GetBenchmarkPrice();
            result["TOLERANCE"] = master_mnt.GetTolerance();
            result["QUANTITY_UNIT"] = master_mnt.GetQuantityUnit();

            result["LR"] = master_mnt.GetLR();
            result["VGO"] = master_mnt.GetVGO();
            result["SUPPLIER"] = master_mnt.GetSupplier("CPAI");
            return SendJson(result);
        }

        public ActionResult GetCustomers()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            result["CUSTOMERS"] = master_mnt.GetCustomerList(system);
            return SendJson(result);
        }

        public ActionResult GetCrudePurchase(CDS_CRUDE_REF_CRITERIA criteria)
        {
            List<CDS_CRUDE_PURCHASE_DETAIL_EXT> master = master_mnt.SearchCrudeRefs(criteria);
            return SendJson(master);
        }


        //public ActionResult GetCIPCrudeData(CDS_SEARCH_DATA_CRITERIA criteria)
        //{
        //    var cip_data = cip_man.getCIPCrudeList(criteria);
        //    return SendJson(cip_data);
        //}

        //public ActionResult GetCIPProductData(CDS_SEARCH_DATA_CRITERIA criteria)
        //{
        //    var cip_data = cip_man.getCIPProductList(criteria);
        //    return SendJson(cip_data);
        //}

        //public ActionResult GetCHOData(CDS_SEARCH_DATA_CRITERIA criteria)
        //{
        //    var data = cho_man.getCHOList(criteria);
        //    return SendJson(data);
        //}
    }
}