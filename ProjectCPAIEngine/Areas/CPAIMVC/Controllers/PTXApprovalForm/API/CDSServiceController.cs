﻿using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API;
using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PTXApprovalForm.API
{
    public class CDSServiceController : BaseApiController
    {
        // GET: CPAIMVC/CDSService
        public ActionResult Index()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            return SendJson(result);
        }

        public ActionResult SaveDraft(CDS_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            ResponseData result = new ResponseData();
            if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            {
                //List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
                result = SaveData(_status, md, "");
            }
            else
            {
                result = ButtonClickCDS("SAVE DRAFT", md);
                //result = _btn_Click("SAVE DRAFT", md);
                //md.PDA_FORM_ID = result.resp_parameters[0].v;
                //md.PDA_CREATED = md.PDA_CREATED ?? DateTime.Now;
                //md.PDA_CREATED_BY = lbUserID;
                //md.PDA_STATUS = ACTION.DRAFT;
                ////PAF_DATA_DAL.InsertOrUpdate(md);
            }
            return SendJson(result);
        }
        public ActionResult Submit(CDS_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            md.CDA_UPDATED = DateTime.Now;
            md.CDA_UPDATED_BY = lbUserID;
            result = ButtonClickCDS("SUBMIT", md);
            return SendJson(result);
        }

        public ActionResult Verify(CDS_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            md.CDA_UPDATED = DateTime.Now;
            md.CDA_UPDATED_BY = lbUserID;
            result = ButtonClickCDS("VERIFY", md);
            return SendJson(result);
        }
        public ActionResult Endorse(CDS_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.CDA_UPDATED = DateTime.Now;
            md.CDA_UPDATED_BY = lbUserID;
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = ButtonClickCDS("ENDORSE", md);
            //}
            return SendJson(result);
        }
        public ActionResult Cancel(CDS_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Cancel;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.CDA_UPDATED = DateTime.Now;
            md.CDA_UPDATED_BY = lbUserID;
            result = ButtonClickCDS("CANCEL", md);
            //}
            return SendJson(result);
        }
        public ActionResult Reject(CDS_DATA md)
        {
            //DocumentActionStatus _status = DocumentActionStatus.Reject;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.CDA_UPDATED = DateTime.Now;
            md.CDA_UPDATED_BY = lbUserID;
            result = ButtonClickCDS("REJECT", md);
            //}
            return SendJson(result);
        }
        public ActionResult Approve(CDS_DATA md)
        {
            ResponseData result = new ResponseData();
            md.CDA_UPDATED = DateTime.Now;
            md.CDA_UPDATED_BY = lbUserID;
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = ButtonClickCDS("APPROVE", md);
            return SendJson(result);
        }

        public ActionResult GetByTransactionId(CDS_DATA model)
        {
            return FindDafButtons(model);
        }
        public ActionResult FindDafButtons(CDS_DATA model)
        {
            CDS_DATA result = LoadCDSData(model.TRAN_ID);
            return SendJson(result);
        }

        public ActionResult FileUpload()
        {
            CDS_ATTACH_FILE result = new CDS_ATTACH_FILE();
            try
            {
                if (Request.QueryString["CAT_TYPE"] == null) throw new Exception("Invalid Type.");
                if (Request.QueryString["Type"] == null) throw new Exception("Invalid Type.");
                string[] _Param = Request.QueryString["Type"].ToString().Split('|');
                if (_Param.Length >= 2)
                {
                    string Type = _Param[0];
                    string FNID = _Param[1];
                    string rootPath = Request.PhysicalApplicationPath;
                    System.Web.HttpFileCollection upload_file = System.Web.HttpContext.Current.Request.Files;
                    HttpPostedFile file = upload_file[0];
                    string file_name = Path.GetFileName(file.FileName);
                    if (file_name != string.Empty)
                    {
                        file_name = string.Format("{0}_{2}_{1}_{3}", DateTime.Now.ToString("yyyyMMddHHmmss"), Type, FNID, Path.GetFileName(file.FileName));
                        if (!Directory.Exists(rootPath + "Web/FileUpload/"))
                            Directory.CreateDirectory(rootPath + "Web/FileUpload/");
                        file.SaveAs(rootPath + "Web/FileUpload/" + file_name);
                    }

                    result.CAT_TYPE = Request.QueryString["CAT_TYPE"];
                    result.CAT_INFO = file_name;
                    result.CAT_PATH = "Web/FileUpload/" + file_name;
                    result.CAT_CREATED = DateTime.Now;
                    result.CAT_CREATED_BY = lbUserID;
                    result.CAT_UPDATED = DateTime.Now;
                    result.CAT_UPDATED_BY = lbUserID;
                }
                else
                {
                    throw new Exception("Invalid Type.");
                }
            }
            catch (Exception ex)
            {
                //output.result = new OutputResult();
                //output.result.Status = "ERROR";
                //output.result.Description = ex.Message.Replace("\r\n", " ").Replace("\"", "'");
            }
            return SendJson(result);
        }

        //public ActionResult GenExcel(string id)
        //{
        //    CDSExcelExport exMan = new CDSExcelExport();
        //    CDS_DATA_DAL dafMan = new CDS_DATA_DAL();

        //    CDS_DATA data = dafMan.Get(id);
        //    string file_name = string.Format("Demurrage-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
        //    string file_path = exMan.GenExcel(data, file_name);
        //    return SendJson(file_path);
        //}

        //public ActionResult GeneratePDF(string id)
        //{
        //    CDS_DATA_DAL dafMan = new CDS_DATA_DAL();
        //    CDS_DATA data = dafMan.Get(id);
        //    string pdf_file = CDSGenerator.genPDF(data);
        //    return SendJson(pdf_file);
        //}
    }
}
