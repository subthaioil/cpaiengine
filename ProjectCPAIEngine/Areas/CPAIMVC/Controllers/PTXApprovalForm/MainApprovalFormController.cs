﻿using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PTXApprovalForm
{
    public class MainApprovalFormController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}