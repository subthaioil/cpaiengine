﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class PlantController : BaseController
    {
        // GET: CPAIMVC/Plant
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Plant");
        }

        [HttpGet]
        public ActionResult Search()
        {
            PlantViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(PlantViewModel PlantViewModel)
        {
            PlantViewModel model = initialModel();
            PlantServiceModel serviceModel = new PlantServiceModel();
            PlantViewModel_Search viewModelSearch = new PlantViewModel_Search();

            viewModelSearch = PlantViewModel.plant_Search;
            serviceModel.Search(ref viewModelSearch);
            model.plant_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                PlantViewModel model = initialModel();

                if (model.plant_Detail.CreateType == null || model.plant_Detail.CreateType == "")
                {
                    model.plant_Detail.CreateType = "CPAI";
                }
                model.plant_Detail.Control.Add(new PlantViewModel_Control { RowID = "", Order = "1", System = "", Status = "ACTIVE" });

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Plant" }));
            }
        }

        [HttpPost]
        public ActionResult Create(PlantViewModel pModel)
        {
            if (ViewBag.action_create)
            {
                PlantServiceModel serviceModel = new PlantServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Add(pModel.plant_Detail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                PlantViewModel model = initialModel();
                model.plant_Detail = pModel.plant_Detail;
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Plant" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string PlantID)
        {
            if (ViewBag.action_edit)
            {
                PlantServiceModel serviceModel = new PlantServiceModel();

                PlantViewModel model = initialModel();
                model.plant_Detail = serviceModel.Get(PlantID);
                if (model.plant_Detail.CreateType == null || model.plant_Detail.CreateType == "")
                {
                    model.plant_Detail.CreateType = "SAP";
                }
                if (model.plant_Detail.Control.Count < 1)
                {
                    model.plant_Detail.Control.Add(new PlantViewModel_Control { RowID = "", Order = "1", System = "", Status = "ACTIVE" });
                }

                TempData["PlantID"] = PlantID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Plant" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string PlantID, PlantViewModel PlantViewModel)
        {
            if (ViewBag.action_edit)
            {
                PlantServiceModel serviceModel = new PlantServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(PlantViewModel.plant_Detail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["PlantID"] = PlantID;

                PlantViewModel model = initialModel();
                model.plant_Detail = PlantViewModel.plant_Detail;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Plant" }));
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(string PlantID)
        {
            if (ViewBag.action_view)
            {
                PlantServiceModel serviceModel = new PlantServiceModel();

                PlantViewModel model = initialModel();
                model.plant_Detail = serviceModel.Get(PlantID);
                if (model.plant_Detail.Control.Count < 1)
                {
                    model.plant_Detail.Control.Add(new PlantViewModel_Control { RowID = "", Order = "1", System = "", Status = "ACTIVE" });
                }

                TempData["PlantID"] = PlantID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Plant" }));
            }
        }

        public PlantViewModel initialModel()
        {
            PlantViewModel model = new PlantViewModel();
            model.plant_Detail = new PlantViewModel_Detail();
            model.plant_Detail.Control = new List<PlantViewModel_Control>();
            
            model.ddl_PlantCompany = DropdownServiceModel.getPlantCompany();
            model.ddl_Company = DropdownServiceModel.getCompany();
            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_System = DropdownServiceModel.getPlantCtrlSystem();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();

            model.plant_SystemJSON = PlantServiceModel.GetSystemJSON();
            model.plantIdJSON = PlantServiceModel.GetPlantIdJSON();
            model.plantNameJSON = PlantServiceModel.GetPlantNameJSON();
            return model;
        }
    }
}