﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.engine.utility;
using com.pttict.sap.Interface.Model;
using System.Web.UI;
using ProjectCPAIEngine.DAL.DALMaster;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class PurchaseEntryController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction("Search", "PurchaseEntry");
        }

        [HttpGet]
        public ActionResult Search()
        {
            PurchaseEntryViewModel model = initialModel();
            if (Session["CurrentSearchModel"] != null)
            {
                model = (PurchaseEntryViewModel)Session["CurrentSearchModel"];
                Session["CurrentSearchModel"] = null;
            }

            return View(model);                        
        }

        [HttpPost]
        public ActionResult Search(PurchaseEntryViewModel tempModel)
        {
            if (Session["CurrentSearchModel"] != null)
            {
                PurchaseEntryViewModel blankModel = initialModel();
                Session["CurrentSearchModel"] = null;
                //return View(blankModel);
            }
            
            PurchaseEntryViewModel model = initialModel();
            PurchaseEntryServiceModel servicemodel = new PurchaseEntryServiceModel();
            PurchaseEntryViewModel_Search viewModelSearch = new PurchaseEntryViewModel_Search();

            viewModelSearch.sSearchData = new List<PurchaseEntryViewModel_SearchData>();
            viewModelSearch = tempModel.PurchaseEntry_Search;
            servicemodel.Search(ref viewModelSearch);
            model.PurchaseEntry_Search = viewModelSearch;

            Session["CurrentSearchModel"] = model;
            Session["CurrentSearchModelBackup"] = model;

            return View(model);
        }

        public PurchaseEntryViewModel initialModel()
        {
            PurchaseEntryServiceModel serviceModel = new PurchaseEntryServiceModel();

            PurchaseEntryViewModel model = new PurchaseEntryViewModel();
            model.PurchaseEntry_Search = new PurchaseEntryViewModel_Search();
            model.PurchaseEntry_Search.sSearchData = new List<PurchaseEntryViewModel_SearchData>();

            model.ddl_Company = serviceModel.GetCompany();
            model.ddl_Product = serviceModel.GetProduct();
           
            model.ddl_Supplier = DropdownServiceModel.getVendorPIT();

            return model;
        }

        [HttpGet]
        public ActionResult BackToSearch()
        {
            return RedirectToAction("Search", true);
        }

        [HttpGet]
        public ActionResult ChangeData(PurchaseEntryViewModel_SearchData model)
        {
            try
            {
                ModelState.Clear();
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                var blDate = Request.QueryString["txtBlDate"];
                if (blDate.EndsWith(","))
                {
                    blDate = blDate.Substring(0, blDate.Length - 1);
                }
                var simulateDataName = Request.QueryString["SimulateDataName"];
                var simulateValue = Request.QueryString["SimulateValue"];
                PurchaseEntryServiceModel servicemodel = new PurchaseEntryServiceModel();
                if (Session["CurrentModel"] == null)
                {
                    servicemodel.GetOutturnData(model);
                }
                else
                {
                    model = (PurchaseEntryViewModel_SearchData)Session["CurrentModel"];
                }
                
                if (blDate != "" && simulateDataName != null)
                {
                    blDate = blDate.Replace(" to ", "|");
                    var fromDate = DateTime.ParseExact(blDate.Split('|')[0], "dd/MM/yyyy", cultureinfo);
                    var toDate = DateTime.ParseExact(blDate.Split('|')[1], "dd/MM/yyyy", cultureinfo);

                    decimal n;
                    bool isSimulateValueNumeric = decimal.TryParse(simulateValue, out n);

                    if (fromDate == null || toDate == null || !isSimulateValueNumeric)
                    {
                        return View("Change", model);
                    }

                    foreach (var item in model.sOutturn)
                    {
                        if (item.Valid && (DateTime.ParseExact(item.RunningDate, "dd/MM/yyyy", cultureinfo) >= fromDate && DateTime.ParseExact(item.RunningDate, "dd/MM/yyyy", cultureinfo) <= toDate))
                        {
                            if (simulateDataName == "FOB Price")
                            {
                                item.Price = Convert.ToDecimal(simulateValue).ToString("#,#00.0000");
                            }
                            else
                            {
                                item.ROE = Convert.ToDecimal(simulateValue).ToString("#,#00.0000");
                            }
                            decimal? price = Math.Round(Convert.ToDecimal(item.Price), 4);
                            decimal? roe = Convert.ToDecimal(item.ROE);

                            #region Realtime calculate the new Price/ROE (Now disabled)

                            //decimal? amountUSD = 0;
                            //decimal? amountTHB = 0;
                            //decimal? vat7 = 0;
                            //decimal? totalAmountUSD = 0;
                            //decimal? totalAmountTHB = 0;

                            //if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "bbl")
                            //{
                            //    if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            //    {
                            //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                            //            amountUSD = (Convert.ToDecimal(item.BlQtyBbl) * price) / roe;
                            //        else
                            //            amountUSD = Convert.ToDecimal(item.BlQtyBbl) * price;
                            //    }
                            //    else
                            //    {
                            //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                            //        {
                            //            amountUSD = (Convert.ToDecimal(item.QtyBbl) * price) / roe;
                            //        }
                            //        else
                            //        {
                            //            amountUSD = Convert.ToDecimal(item.QtyBbl) * price;
                            //        }
                            //    }
                            //}
                            //else if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "mt")
                            //{
                            //    if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            //    {
                            //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                            //        {
                            //            amountUSD = (Convert.ToDecimal(item.BlQtyMt) * price) / roe;
                            //        }
                            //        else
                            //        {
                            //            amountUSD = Convert.ToDecimal(item.BlQtyMt) * price;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                            //        {
                            //            amountUSD = (Convert.ToDecimal(item.QtyMt) * price) / roe;
                            //        }
                            //        else
                            //        {
                            //            amountUSD = Convert.ToDecimal(item.QtyMt) * price;
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            //    {
                            //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                            //        {
                            //            amountUSD = (Convert.ToDecimal(item.BlQtyMl) * price) / roe;
                            //        }
                            //        else
                            //        {
                            //            amountUSD = Convert.ToDecimal(item.BlQtyMl) * price;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                            //        {
                            //            amountUSD = (Convert.ToDecimal(item.QtyMl) * price) / roe;
                            //        }
                            //        else
                            //        {
                            //            amountUSD = Convert.ToDecimal(item.QtyMl) * price;
                            //        }
                            //    }
                            //}
                            roe = Math.Round(Convert.ToDecimal(roe), 4);
                            //amountTHB = amountUSD * Convert.ToDecimal(roe);
                            //amountUSD = Math.Round(Convert.ToDecimal(amountUSD), 4);
                            //amountTHB = Math.Round(Convert.ToDecimal(amountTHB), 4);
                            //vat7 = amountTHB * Convert.ToDecimal(0.07);
                            //totalAmountUSD = amountUSD;
                            //totalAmountTHB = amountTHB + vat7;

                            //vat7 = Math.Round(Convert.ToDecimal(vat7), 4);
                            //totalAmountUSD = Math.Round(Convert.ToDecimal(totalAmountUSD), 4);
                            //totalAmountTHB = Math.Round(Convert.ToDecimal(totalAmountTHB), 4);

                            //item.AmountUSD = amountUSD.Value.ToString("#,##0.0000");
                            //item.AmountTHB = amountTHB.Value.ToString("#,##0.0000");
                            //item.Vat7 = vat7.Value.ToString("#,##0.0000");
                            //item.TotalAmountUSD = totalAmountUSD.Value.ToString("#,##0.0000");
                            //item.TotalAmountTHB = totalAmountTHB.Value.ToString("#,##0.0000");

                            #endregion
                        }
                    }
                }

                var fromDateForDisplay = DateTime.ParseExact(blDate.Split('|')[0], "dd/MM/yyyy", cultureinfo);
                var toDateForDisplay = DateTime.ParseExact(blDate.Split('|')[1], "dd/MM/yyyy", cultureinfo);

                model.txtBlDate = fromDateForDisplay.Day.ToString("00") + "/" + fromDateForDisplay.Month.ToString("00") + "/" + fromDateForDisplay.Year + " to " +
                                  toDateForDisplay.Day.ToString("00") + "/" + toDateForDisplay.Month.ToString("00") + "/" + toDateForDisplay.Year;
                model.SimulateDataName = simulateDataName;
                model.SimulateValue = simulateValue;

                Session["CurrentModel"] = model;
                TempData["CurrentModel"] = model;
                return View("Change", model);
            }
            catch(Exception ex)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = "Error: " + ex.Message;
                return View("Change", model);
            }
        }

        [HttpGet]
        public ActionResult Change(PurchaseEntryViewModel_SearchData model)
        {
            ModelState.Clear();
            PurchaseEntryServiceModel servicemodel = new PurchaseEntryServiceModel();
            servicemodel.Calculate(model);
            Session["CurrentSearchModel"] = Session["CurrentSearchModelBackup"];            
            servicemodel.GetOutturnData(model);

            Session["CurrentModel"] = model;
            TempData["CurrentModel"] = model;

            var debugMode = MasterData.GetJsonMasterSetting("PIT_PURCHASE_ENTRY_SEARCH_DEBUG");
            if (debugMode == "true")
            {
                ViewBag.DebugMode = true;
            }
            else
            {
                ViewBag.DebugMode = false;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult ManagePE(PurchaseEntryViewModel_SearchData model, string command)
        {
            try
            {
                if (command == "Calculate")
                {
                    PurchaseEntryServiceModel serviceModel = new PurchaseEntryServiceModel();
                    ReturnValue rtn = new ReturnValue();

                    rtn = serviceModel.Calculate(model);
                    TempData["CurrentModel"] = model;
                    Session["CurrentModel"] = model;
                    ViewBag.FromCalculate = true;
                }
                else if (command == "Save")
                {
                    PurchaseEntryServiceModel serviceModel = new PurchaseEntryServiceModel();
                    ReturnValue rtn = new ReturnValue();
                    rtn = serviceModel.Save(model);
                    ViewBag.SaveMessage = rtn.Message;

                    //serviceModel.GetOutturnData(model);
                    serviceModel.Calculate(model);
                    TempData["CurrentModel"] = model;
                    Session["CurrentModel"] = model;
                    ViewBag.FromCalculate = true;


                    return View("Change", model);
                }
                else if (command == "Report")
                {
                    return View("Change", model);
                }
                else if (command == "Send to SAP")
                {
                    PurchaseEntryServiceModel serviceModel = new PurchaseEntryServiceModel();
                    serviceModel.Calculate(model);
                    ReturnValue rtn = new ReturnValue();
                    var accrualType = Request["AccrualType"];
                    var reverseDate = Request["txtReverseDate"];
                    model.SupplierNo = Convert.ToInt32(model.SupplierNo).ToString();

                    ConfigManagement configManagement = new ConfigManagement();
                    string configFromDownStream = "";
                    if (accrualType.ToUpper() == "A")
                    {
                        configFromDownStream = configManagement.getDownstreamConfig("CIP_PE_SEND_TYPE_A");
                    }
                    else // Type C
                    {
                        configFromDownStream = configManagement.getDownstreamConfig("CIP_PE_SEND");
                    }

                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    model = serviceModel.CleansingModel(model);

                    rtn = serviceModel.SendToSAP(configFromDownStream, model, accrualType, reverseDate);

                    ViewBag.SendToSapCompleted = true;
                    ViewBag.SendToSapMessage = rtn.Message;

                    serviceModel.GetOutturnData(model);
                    TempData["CurrentModel"] = model;
                    ViewBag.FromCalculate = true;
                    return View("Change", model);
                }
                else if (command == "Back")
                {
                    return RedirectToAction("Search");
                }

                var debugMode = MasterData.GetJsonMasterSetting("PIT_PURCHASE_ENTRY_SEARCH_DEBUG");
                if (debugMode == "true")
                {
                    ViewBag.DebugMode = true;
                }
                else
                {
                    ViewBag.DebugMode = false;
                }

                return View("Change", model);
            }
            catch(Exception ex)
            {
                ViewBag.Error = true;

                return View("Change", model);
            }
        }

        [HttpGet]
        public ActionResult Report()
        {
            PurchaseEntryViewModel_SearchData model = null;
            PurchaseEntryServiceModel serviceModel = new PurchaseEntryServiceModel();

            if (TempData["CurrentModel"] != null)
            {
                model = (PurchaseEntryViewModel_SearchData)TempData["CurrentModel"];
                serviceModel.GetPitPurchaseEntry(model);                
                model = serviceModel.FilterOnlySavedRecord(model);
            }
            else if(Session["CurrentModel"] != null)
            {
                model = (PurchaseEntryViewModel_SearchData)Session["CurrentModel"];
                serviceModel.GetPitPurchaseEntry(model);
                model = serviceModel.FilterOnlySavedRecord(model);
            }
            else
            {
                model = serviceModel.FilterOnlySavedRecord(model);
                serviceModel.GetPitPurchaseEntry(model);
                model = serviceModel.FilterOnlySavedRecord(model);
                return View("Report", model);                
            }

            return View("Report", model);
        }

        [HttpPost]
        public ActionResult ExportToExcel()
        {
            PurchaseEntryViewModel_SearchData model = null;
            try
            {
                PurchaseEntryServiceModel serviceModel = new PurchaseEntryServiceModel();

                if (TempData["CurrentModel"] != null)
                {
                    model = (PurchaseEntryViewModel_SearchData)TempData["CurrentModel"];
                }
                else if (Session["CurrentModel"] != null)
                {
                    model = (PurchaseEntryViewModel_SearchData)Session["CurrentModel"];
                }

                string result = serviceModel.ExportToExcel(model);

                if (!result.StartsWith("error"))
                {
                    return File(result, "application /excel", System.IO.Path.GetFileName(result));
                }
                return null;
            }
            catch(Exception ex)
            {
                return View("Report", model);
            }
        }
    }
}