﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class PurchaseEntryVesselController : BaseController
    {

        public ActionResult index(string TripNo , string MetNum )
        {
            PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();
            PurchaseEntryVesselViewModel model = new PurchaseEntryVesselViewModel();
            ReturnValue rtn = new ReturnValue();

            model = initialModel();
            if (!String.IsNullOrEmpty(TripNo) && !String.IsNullOrEmpty(MetNum))
            {
                model.ddl_ItemNo = PurchaseEntryVesselServiceModel.getItemNo(TripNo, MetNum);
                rtn = serviceModel.getMaterialsVessel(ref model, TripNo, MetNum);
                if(model.PriceList.Count != 0)
                {
                    model.ddl_InsuranceRate = PurchaseEntryVesselServiceModel.getCIPConfigInsuranceRate(model);
                }
            }
            else
            {
                serviceModel.setInitialModel(ref model);

                rtn = null;
            }

            if (rtn != null)
            {
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
            }
            else
            {
                TempData["ReponseMessage"] = null;
                TempData["ReponseStatus"] = null;
            }
            return View(model);
        }
        
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(FormCollection frm, PurchaseEntryVesselViewModel model)
        {
            PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();

            ReturnValue rtn = new ReturnValue();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string test1 = model.PriceList[0].sTripNo;
                string test2 = model.PriceList[0].sMetNum;
                var queryif = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO == test1 && z.PPE_MET_NUM == test2).Select(z => z.PPE_SAP_FI_DOC_NO).FirstOrDefault();
                if (String.IsNullOrEmpty(queryif))
                {
                    rtn = serviceModel.SaveData(ref model, lbUserName);

                    if (rtn.Status)
                    {
                        foreach (var item in model.PriceList)
                        {
                            if (item.FreightList == null)
                            {
                                item.FreightList = new List<PEV_Freight>();
                            }
                        }
                    }

                }
                else
                {
                    rtn.Message = "This Purcahse Entry Has Been Send To Sap";
                    rtn.Status = false;
                }
                model.IsSave = "Saved";

            }
                

            model.isEdit = false;
            if (TempData["tempPurchaseEntryVessel"] != null) { TempData.Remove("tempPurchaseEntryVessel"); }
            TempData["tempPurchaseEntryVessel"] = model;

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            if (model.ddl_InsuranceRate == null)
            {
                model.ddl_InsuranceRate = PurchaseEntryVesselServiceModel.getCIPConfigInsuranceRate(model);
            }

            return View("Index",model);

        }

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "GetDataForEdit")]
        //public ActionResult GetDataForEdit(FormCollection frm, PurchaseEntryVesselViewModel model)
        //{
        //    ReturnValue rtn = new ReturnValue();

        //    if (!String.IsNullOrEmpty(model.sItemNo))
        //    {
        //        PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();
        //        rtn = serviceModel.getDataForEdit(ref model);

        //        if (TempData["tempPurchaseEntryVessel"] != null) { TempData.Remove("tempPurchaseEntryVessel"); }
        //        TempData["tempPurchaseEntryVessel"] = model;

        //        if (rtn != null)
        //        {
        //            TempData["ReponseMessage"] = rtn.Message;
        //            TempData["ReponseStatus"] = rtn.Status;
        //        }
        //        else
        //        {
        //            TempData["ReponseMessage"] = null;
        //            TempData["ReponseStatus"] = null;
        //        }

        //        serviceModel.setInitialModel(ref model);
        //    }

        //    return RedirectToAction("Index", "PurchaseEntryVessel", new { TripNo = model.sTripNo, MetNum = model.sMetNum });
        //}

        //[HttpPost]
        //public ActionResult ExportToExcel()
        //{
        //    PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();

        //    if(TempData["tempPurchaseEntryVessel"] != null)
        //    {
        //        PurchaseEntryVesselViewModel model = (PurchaseEntryVesselViewModel)TempData["tempPurchaseEntryVessel"];

        //        string result = serviceModel.ExportToExcel(model);

        //        if (!result.StartsWith("error"))
        //        {
        //            return File(result, "application /excel", System.IO.Path.GetFileName(result));
        //        }
        //    }
            
        //    return null;
        //}

        [HttpPost] 
        public ActionResult Report(PurchaseEntryVesselViewModel model)
        {
            PurchaseEntryVesselReport pModel = new PurchaseEntryVesselReport();
            int num = !model.bPartly ? 0 : Convert.ToInt32(model.tabPartly);
            pModel.sTripNo = model.PriceList[num].sTripNo;
            pModel.sBLDate = model.PriceList[num].sBLDate;
            pModel.sMetNum = model.PriceList[num].sTxtMet;
            pModel.sDueDate = model.PriceList[num].sDueDate;
            pModel.sVesselName = model.PriceList[num].sTxtVesselName;
            pModel.sPostingDate = model.PriceList[num].sPostingDate;
            pModel.QuantityList = model.PriceList[num].QuantityList;
            pModel.PriceSupplierList = model.PriceList[num].PriceSupplierList;
            if (model.PriceList[num].FreightList != null)
            {
                pModel.FreightList = model.PriceList[num].FreightList;
            }
            else
            {
                pModel.FreightList = new List<PEV_Freight>();
            }
            
            pModel.PriceList = model.PriceList[num];
            return View("Report", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "SendToSAP")]
        public ActionResult SendToSAP(FormCollection frm, PurchaseEntryVesselViewModel model)
        {
            PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();
            ReturnValue rtn = new ReturnValue();
            model.SupplierNo = Convert.ToInt32(model.SupplierNo).ToString();
            var accrualType = model.SAPAccuralType;
            var reverseDate = model.SAPReverseDateForTypeA;
            bool Notpass = false;
            bool IsNotSave = false;
            int num = !model.bPartly ? 0 : Convert.ToInt32(model.tabPartly);

            model.PEV_Price = model.PriceList[num];

            string sTripNo = model.PEV_Price.sTripNo;
            string sMetNum = model.PEV_Price.sMetNum;
            decimal sItemNo = Decimal.Parse(model.PEV_Price.sItemNo);
            string configFromDownStream = "";
            ConfigManagement configManagement = new ConfigManagement();
            using (var context = new EntityCPAIEngine())
            {
                var querycheck = context.PCF_PURCHASE_ENTRY.Where(a => a.PPE_TRIP_NO == sTripNo && a.PPE_MET_NUM == sMetNum && a.PPE_ITEM_NO == sItemNo).SingleOrDefault();
                if(querycheck != null)
                {
                    if(!String.IsNullOrEmpty(querycheck.PPE_ACCRUAL_TYPE))
                    {
                        if(querycheck.PPE_ACCRUAL_TYPE == "C")
                        {
                            Notpass = true;
                            rtn.Message = "This Document has been send to sap";
                            rtn.Status = false;
                        }
                        else
                        {
                            if(accrualType != "C")
                            {
                                Notpass = true;
                                rtn.Message = "This Document has been send to sap";
                                rtn.Status = false;
                            }
                        }
                    }
                }
                else
                {
                    IsNotSave = true;
                    rtn.Message = "This Document hasn't been save";
                    rtn.Status = false;
                }
            }
            if (accrualType == "C")
            {
                configFromDownStream = configManagement.getDownstreamConfig("CIP_PE_SEND");
            }
            else
            {
                configFromDownStream = configManagement.getDownstreamConfig("CIP_PE_SEND_TYPE_A");
            }
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            if (!IsNotSave)
            {
                if (!Notpass)
                {
                    rtn = serviceModel.SendToSAP(configFromDownStream, model, accrualType, reverseDate, lbUserName, num);

                }
            }
            foreach (var item in model.PriceList)
            {
                if (item.FreightList == null)
                {
                    item.FreightList = new List<PEV_Freight>();
                }
            }

            if (model.ddl_InsuranceRate == null)
            {
                model.ddl_InsuranceRate = PurchaseEntryVesselServiceModel.getCIPConfigInsuranceRate(model);
            }
            model.IsSave = "Saved";
            if (rtn != null)
            {
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
            }
            else
            {
                TempData["ReponseMessage"] = null;
                TempData["ReponseStatus"] = null;
            }

            return View("Index", model);
        }

        public PurchaseEntryVesselViewModel initialModel()
        {
            PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();
            PurchaseEntryVesselViewModel model = new PurchaseEntryVesselViewModel();

            model.PriceList = new List<PEV_Price>();
            model.PEV_Price = new PEV_Price();
            model.ddl_InsuranceRate = new List<SelectListItem>();
            model.tabPartly = "";

            return model;
        }


    }
}