﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.sap.Interface;
using com.pttict.sap.Interface.Service;
using com.pttict.sap.Interface.Model;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.sap.po.change;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class PurchaseOrderController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "PurchaseOrder");
        }

        [HttpGet]
        public ActionResult Search()
        {
            PurchaseOrderViewModel model = initialModel();
            if(Session["SavedModel"] != null)
            {
                model = (PurchaseOrderViewModel)Session["SavedModel"];
               //Session["SavedModel"] = null;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(PurchaseOrderViewModel tempModel)
        {
            PurchaseOrderViewModel model = initialModel();
            PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
            PurchaseOrderViewModel_Search viewModelSearch = new PurchaseOrderViewModel_Search();

            viewModelSearch.sSearchData = new List<PurchaseOrderViewModel_SearchData>();
            viewModelSearch = tempModel.PurchaseOrder_Search;
            serviceModel.Search(ref viewModelSearch);
            model.PurchaseOrder_Search = viewModelSearch;

            Session["SavedModel"] = model;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                PurchaseOrderViewModel model = initialModel();
                model.PurchaseOrder_Detail.Control.Add(new PurchaseOrderViewModel_Control());

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "PurchaseOrder" }));
            }
        }

        [HttpPost]
        public ActionResult Create(PurchaseOrderViewModel purchaseOrderViewModel)
        {
            if (ViewBag.action_create)
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
                ReturnValue rtn = new ReturnValue();
                PurchaseOrderViewModel_Detail poModel = new PurchaseOrderViewModel_Detail();
                poModel = purchaseOrderViewModel.PurchaseOrder_Detail;
                rtn = serviceModel.Add(ref poModel, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                PurchaseOrderViewModel model = initialModel();
                model.PurchaseOrder_Detail = poModel;
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "PurchaseOrder" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string poNo)
        {
            if (ViewBag.action_edit)
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();

                PurchaseOrderViewModel model = initialModel();
                model.PurchaseOrder_Detail = serviceModel.Get(poNo);

                TempData["PoNo"] = poNo;
                TempData["CurrentModel"] = model;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "PurchaseOrder" }));
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(string PoNo, PurchaseOrderViewModel purchaseOrderViewModel)
        {
            if (ViewBag.action_edit)
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(purchaseOrderViewModel.PurchaseOrder_Detail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["PoNo"] = PoNo;
                if(rtn.Message == "Successfully!")
                {
                    ViewBag.EditPOResult = true;
                }
                else
                {
                    ViewBag.EditPOResult = false;
                }

                PurchaseOrderViewModel model = initialModel();
                model.PurchaseOrder_Detail = purchaseOrderViewModel.PurchaseOrder_Detail;
                if(model.PurchaseOrder_Detail.Control.Count() > 0)
                {
                    model.PurchaseOrder_Detail.Control[0].TotalAmount = (Convert.ToDecimal(model.PurchaseOrder_Detail.Control[0].Volume) * Convert.ToDecimal(model.PurchaseOrder_Detail.Control[0].UnitPrice)).ToString();
                }

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "PurchaseOrder" }));
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(string PoNo)
        {
            if (ViewBag.action_view)
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();

                PurchaseOrderViewModel model = initialModel();
                model.PurchaseOrder_Detail = serviceModel.Get(PoNo);

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "PurchaseOrder" }));
            }
        }

        public PurchasingChangeModel CreateModel(PurchaseOrderViewModel poViewModel, bool sendCancel)
        {
            PurchasingChangeModel model = new PurchasingChangeModel();            

            //Purchase Order No.
            model.PURCHASEORDER = poViewModel.PurchaseOrder_Detail.PoNo;

            //ITEMS
            model.POITEM = new List<BAPIMEPOITEM>();
            model.POITEMX = new List<BAPIMEPOITEMX>();

            if (poViewModel.PurchaseOrder_Detail.Control != null)
            {
                foreach (var item in poViewModel.PurchaseOrder_Detail.Control)
                {
                    //ITEM
                    BAPIMEPOITEM poItem = new BAPIMEPOITEM()
                    {
                        PO_ITEM = String.IsNullOrEmpty(item.PoItem) ? "" : item.PoItem,
                        SHORT_TEXT = String.IsNullOrEmpty(item.ShortText) ? "" : item.ShortText,
                        PLANT = String.IsNullOrEmpty(item.Plant) ? "" : item.Plant,
                        PO_UNIT = String.IsNullOrEmpty(item.VolumeUnit) ? "" : item.VolumeUnit,
                        NET_PRICE = Convert.ToDecimal(item.UnitPrice),
                        NET_PRICESpecified = true,
                        QUANTITY = Math.Round(Convert.ToDecimal(item.Volume), 3),
                        QUANTITYSpecified = true
                    };
                    if (sendCancel)
                    {
                        poItem.DELETE_IND = "X";
                    }
                    model.POITEM.Add(poItem);

                    //ITEMX
                    BAPIMEPOITEMX poItemx = new BAPIMEPOITEMX()
                    {
                        PO_ITEM = item.PoItem,
                        PO_ITEMX = "X",
                        SHORT_TEXT = "X",
                        PLANT = "X",       
                        STGE_LOC = "X",
                        MATL_GROUP = "X",
                        QUANTITY = "X",
                        PO_UNIT = "X",
                        NET_PRICE = "X",
                        NO_MORE_GR = "X"
                    };
                    if (sendCancel)
                    {
                        poItemx.DELETE_IND = "X";
                    }
                    model.POITEMX.Add(poItemx);
                }
            }


            model.ALLVERSIONS = new List<BAPIMEDCM_ALLVERSIONS>();
            model.EXTENSIONIN = new List<BAPIPAREX>();
            model.EXTENSIONOUT = new List<BAPIPAREX>();
            model.INVPLANHEADER = new List<BAPI_INVOICE_PLAN_HEADER>();
            model.INVPLANHEADERX = new List<BAPI_INVOICE_PLAN_HEADERX>();
            model.INVPLANITEM = new List<BAPI_INVOICE_PLAN_ITEM>();
            model.INVPLANITEMX = new List<BAPI_INVOICE_PLAN_ITEMX>();
            model.POACCOUNT = new List<BAPIMEPOACCOUNT>();
            model.POACCOUNTPROFITSEGMENT = new List<BAPIMEPOACCOUNTPROFITSEGMENT>();
            model.POACCOUNTX = new List<BAPIMEPOACCOUNTX>();
            model.POADDRDELIVERY = new List<BAPIMEPOADDRDELIVERY>();
            model.POCOMPONENTS = new List<BAPIMEPOCOMPONENT>();
            model.POCOMPONENTSX = new List<BAPIMEPOCOMPONENTX>();
            model.POCOND = new List<BAPIMEPOCOND>();
            model.POCONDHEADER = new List<BAPIMEPOCONDHEADER>();
            model.POCONDHEADERX = new List<BAPIMEPOCONDHEADERX>();
            model.POCONDX = new List<BAPIMEPOCONDX>();
            model.POCONFIRMATION = new List<BAPIEKES>();
            model.POCONTRACTLIMITS = new List<BAPIESUCC>();
            model.POEXPIMPITEM = new List<BAPIEIPO>();
            model.POEXPIMPITEMX = new List<BAPIEIPOX>();
            model.POHISTORY = new List<BAPIEKBE>();
            model.POHISTORY_MA = new List<BAPIEKBE_MA>();
            model.POHISTORY_TOTALS = new List<BAPIEKBES>();
            model.POLIMITS = new List<BAPIESUHC>();
            model.POPARTNER = new List<BAPIEKKOP>();
            model.POSCHEDULE = new List<BAPIMEPOSCHEDULE>();
            model.POSCHEDULEX = new List<BAPIMEPOSCHEDULX>();
            model.POSERVICES = new List<BAPIESLLC>();
            model.POSERVICESTEXT = new List<BAPIESLLTX>();
            model.POSHIPPING = new List<BAPIITEMSHIP>();
            model.POSHIPPINGEXP = new List<BAPIMEPOSHIPPEXP>();
            model.POSHIPPINGX = new List<BAPIITEMSHIPX>();
            model.POSRVACCESSVALUES = new List<BAPIESKLC>();
            model.POTEXTHEADER = new List<BAPIMEPOTEXTHEADER>();
            model.POTEXTITEM = new List<BAPIMEPOTEXT>();
            model.SERIALNUMBER = new List<BAPIMEPOSERIALNO>();
            model.SERIALNUMBERX = new List<BAPIMEPOSERIALNOX>();
            model.EXPPOEXPIMPHEADER = new BAPIEIKP();

            // Set value at RETURN
            model.RETURN = new List<BAPIRET2>();
            model.RETURN.Add(new BAPIRET2()
            {
                FIELD = ""
            });

            return model;
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Change")]
        public ActionResult Change(PurchaseOrderViewModel poViewModel)
        {
            PurchasingChangeModel model = CreateModel(poViewModel, false);
            var json = new JavaScriptSerializer().Serialize(model);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000030;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
            req.req_parameters.p.Add(new p { k = "system", v = "PurchaseOrder" });
            req.req_parameters.p.Add(new p { k = "po_no", v = poViewModel.PurchaseOrder_Detail.PoNo });
            req.req_parameters.p.Add(new p { k = "status", v = "Change" });
            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();            

            resData = service.CallService(req);

            ViewBag.ChangePOCompleted = true;
            ViewBag.ChangePOResult = ((resData.result_status.ToLower() == "fail") || (resData.result_status == "-")) ? false : true;
            ViewBag.ChangePOResutDesc = resData.result_desc;

            var currencyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").CURRENCY.ToList();
            poViewModel.ddl_Currency = new List<SelectListItem>();
            foreach (var currency in currencyGlobalConfig)
            {
                poViewModel.ddl_Currency.Add(new SelectListItem { Value = currency.CODE, Text = currency.CODE });
            }

            return View("Create", poViewModel);            
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Cancel")]
        public ActionResult Cancel(PurchaseOrderViewModel poViewModel)
        {
            PurchasingChangeModel model = CreateModel(poViewModel, true);
            var json = new JavaScriptSerializer().Serialize(model);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000030;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
            req.req_parameters.p.Add(new p { k = "system", v = "PurchaseOrder" });
            req.req_parameters.p.Add(new p { k = "po_no", v = poViewModel.PurchaseOrder_Detail.PoNo });
            req.req_parameters.p.Add(new p { k = "status", v = "Cancel" });
            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            //TempData["ChangePOCompleted"] = true;
            //TempData["ChangePOResult"] = (resData.result_status.ToLower() == "fail") ? false : true;
            //TempData["ChangePOResutDesc"] = resData.result_desc;

            if ((resData.result_status.ToLower() == "fail") ? false : true)
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
                serviceModel.MarkForDeletion(poViewModel.PurchaseOrder_Detail.PoNo);

                PurchaseOrderViewModel defaultModel = initialModel();
                return View("Search", defaultModel);
            }

            var currencyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").CURRENCY.ToList();
            poViewModel.ddl_Currency = new List<SelectListItem>();
            foreach (var currency in currencyGlobalConfig)
            {
                poViewModel.ddl_Currency.Add(new SelectListItem { Value = currency.CODE, Text = currency.CODE });
            }

            return View("Create", poViewModel);
        }

        public PurchaseOrderViewModel initialModel()
        {
            PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
            PurchaseOrderViewModel model = new PurchaseOrderViewModel();

            model.PurchaseOrder_Search = new PurchaseOrderViewModel_Search();
            model.PurchaseOrder_Search.sSearchData = new List<PurchaseOrderViewModel_SearchData>();

            model.PurchaseOrder_Detail = new PurchaseOrderViewModel_Detail();
            model.PurchaseOrder_Detail.Control = new List<PurchaseOrderViewModel_Control>();
            
            List<SelectListItem> tempList = new List<SelectListItem>();
            var ddlCompany = DropdownServiceModel.getCompany();
            var companyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").MT_COMPANY;
            foreach (var company in ddlCompany)
            {
                var result = companyGlobalConfig.Where(x => x.CODE == company.Value).ToList();
                if (result.Count != 0)
                {
                    tempList.Add(company);
                }
            }

            var currencyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").CURRENCY.ToList();
            model.ddl_Currency = new List<SelectListItem>();
            foreach (var currency in currencyGlobalConfig)
            {
                model.ddl_Currency.Add(new SelectListItem { Value = currency.CODE, Text = currency.CODE });
            }
            
            model.ddl_Company = tempList;
            model.ddl_Vendor = DropdownServiceModel.getVendorPIT();
            model.ddl_feedStock = GetFeedStockPIT();
            ViewBag.ddl_Currency = model.ddl_Currency;

            return model;
        }

        const string JSON_CRUDE_PURCHASE = "JSON_CRUDE_PURCHASE";

        private List<SelectListItem> getFeedStock(bool isHidden = false)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.feedstock.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.feedstock[i].key, Value = isHidden ? setting.feedstock[i].unit : setting.feedstock[i].key });
            }
            return list;
        }

        public List<SelectListItem> GetFeedStockPIT()
        {
            PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
            return serviceModel.GetFeedStockPIT();
        }

        [HttpPost]
        public ActionResult AccrualPost()
        {
            ConfigModel config = new ConfigModel
            {
                sap_user = "ZPI_IF_CIP",
                sap_pass = "1@zpi_if_cip",
                //sap_url = "http://sappi-dev.thaioilgroup.com:8021/XISOAPAdapter/MessageServlet?senderParty==BC_CIP===PurchasingChangeRequest_Sync_Out_SI=http:%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fpurchasing_change%2F",
                sap_url = "http://sappi-dev.thaioilgroup.com:8021/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=PurchasingChangeRequest_Sync_Out_SI&interfaceNamespace=http:%2F%2Fthaioilgroup.com%2F_i_purchaseorder%2Fpurchasing%2Fpurchasing_change%2F",
                connect_time_out = "120000"
            };


            return View("Create");
        }

    }
}