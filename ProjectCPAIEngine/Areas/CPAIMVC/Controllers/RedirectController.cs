﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class RedirectController : Controller
    {
        // GET: CPAIMVC/Redirect
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PermissionDenied()
        {
            return View();
        }
    }
}