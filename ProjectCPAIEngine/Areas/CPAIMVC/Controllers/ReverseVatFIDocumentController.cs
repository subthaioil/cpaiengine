﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using com.pttict.sap.Interface.Service;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class ReverseVatFIDocumentController : Controller
    {        
        public ActionResult Index()
        {
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            ReverseFIDocumentServiceModel service = new ReverseFIDocumentServiceModel();
            
            model.ddl_Company = service.GetCompany();

            return View(model);
        }

        public ActionResult Search(ReverseFIDocumentViewModel model)
        {
            CustomVatViewModel modelResult = new CustomVatViewModel();
            ReverseVatFIDocumentServiceModel service = new ReverseVatFIDocumentServiceModel();

            modelResult = service.SearchCustomVat(model.Document.Trim());
            modelResult.GLAccountVAT = CustomVatServiceModel.getGLAccountVAT();
            Session["FIModel"] = model;

            return View("Search", modelResult);
        }

        public ActionResult Result()
        {
            return View();
        }

        public ActionResult Send(CustomVatViewModel data)
        {
            ReverseFIDocumentSendResult resultPost = new ReverseFIDocumentSendResult();
            ReverseFIDocumentResult result = new ReverseFIDocumentResult();
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            ReverseFIDocumentServiceModel serviceReverse = new ReverseFIDocumentServiceModel();
            ReverseVatFIDocumentServiceModel serviceReverseVat = new ReverseVatFIDocumentServiceModel();

            if (Session["FIModel"] != null)
            {
                model = (ReverseFIDocumentViewModel)Session["FIModel"];
                resultPost = serviceReverse.Post(model.Year, model.ComCode, model.Document);
            }
            else
            {
                model.ddl_Company = serviceReverse.GetCompany();
                return View("Index", model);
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            result.Pass = resultPost.Pass;            
            result.SAPReverseDocNo = resultPost.ReturnDocumentNo;
            result.ResultMessage = resultPost.ResultText;
            result.CompanyCode = model.ComCode;
            if (result.Pass)
            {
                serviceReverseVat.UpdateTables(data, model.Document, result.SAPReverseDocNo, result);
            }

            return View("Result", result);
        }
    }
}