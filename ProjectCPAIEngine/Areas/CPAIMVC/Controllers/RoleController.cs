﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class RoleController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "Role");
        }

        [HttpPost]
        public ActionResult Search(RoleViewModel tempModel)
        {

            RoleViewModel model = initialModel();

            RoleServiceModel serviceModel = new RoleServiceModel();
            RoleViewModel_Seach viewModelSearch = new RoleViewModel_Seach();

            viewModelSearch = tempModel.role_Search;
            serviceModel.Search(ref viewModelSearch);
            model.role_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            RoleViewModel model = initialModel();
            return View(model);
        }

        public static RoleViewModel SearchQuery(RoleViewModel RoleModel)
        {
            RoleViewModel_Seach viewModelSearch = new RoleViewModel_Seach();
            RoleServiceModel serviceModel = new RoleServiceModel();

            RoleModel.ddl_RoleName = DropdownServiceModel.getRoleName();
            RoleModel.ddl_RoleDesc = DropdownServiceModel.getRoleDesc();
            RoleModel.ddl_Status = DropdownServiceModel.getMasterStatus();

            viewModelSearch = RoleModel.role_Search;
            serviceModel.Search(ref viewModelSearch);
            RoleModel.role_Search = viewModelSearch;

            return RoleModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, RoleViewModel model)
        {
            if (ViewBag.action_create)
            {
                RoleServiceModel serviceModel = new RoleServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.role_Detail.RolStatus = model.role_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";

                rtn = serviceModel.Add(model.role_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                //RoleViewModel modelDropdownSet = initialModel();

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Role" }));
            }
            
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                RoleViewModel model = initialModel();
                RoleServiceModel serviceModel = new RoleServiceModel();
                model.role_Detail = serviceModel.Get("Template");
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Role" }));
            }
            
        }

        [HttpPost]
        public ActionResult Edit(string roleID, RoleViewModel model)
        {
            if (ViewBag.action_edit)
            {
                RoleServiceModel serviceModel = new RoleServiceModel();
                ReturnValue rtn = new ReturnValue();
                model.role_Detail.RolStatus = model.role_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                rtn = serviceModel.Edit(model.role_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["RoleID"] = roleID;

                RoleViewModel modelDropdownSet = initialModel();
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Role" }));
            }
            
        }

        [HttpGet]
        public ActionResult Edit(string roleID)
        {
            if (ViewBag.action_edit)
            {
                RoleServiceModel serviceModel = new RoleServiceModel();

                RoleViewModel model = initialModel();
                model.role_Detail = serviceModel.Get(roleID);
                TempData["RoleID"] = roleID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Role" }));
            }

        }

        [HttpGet]
        public ActionResult View(string RoleID)
        {
            if (ViewBag.action_view)
            {
                RoleServiceModel serviceModel = new RoleServiceModel();

                RoleViewModel model = initialModel();
                model.role_Detail = serviceModel.Get(RoleID);
                TempData["RoleCode"] = RoleID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Role" }));
            }

        }

        public RoleViewModel initialModel()
        {
            RoleServiceModel serviceModel = new RoleServiceModel();
            RoleViewModel model = new RoleViewModel();

            model.role_Search = new RoleViewModel_Seach();
            model.role_Search.sSearchData = new List<RoleViewModel_SeachData>();

            model.role_Detail = new RoleViewModel_Detail();
            model.role_Detail.BoolStatus = true;

            model.ddl_RoleName = DropdownServiceModel.getRoleName();
            model.ddl_RoleDesc = DropdownServiceModel.getRoleDesc();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
          
            return model;
        }
    }
}