﻿using com.pttict.engine.utility;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class SchedulerCMCSController : BaseController
    {
        ShareFn _FN = new ShareFn();
        const string returnPage = "../web/MainBoards.aspx";

        private void initializeDropDownList(ref SchedulerCMCSViewModel model, string pType)
        {
            //model.vessel = DropdownServiceModel.getVehicle(pType == ConstantPrm.SYSTEMTYPE.CRUDE ? "SCH_C" : "SCH_P");
            //model.product = DropdownServiceModel.getProduct(pType == ConstantPrm.SYSTEMTYPE.CRUDE ? "MAT_CMCS" : "MAT_CMMT");
            //model.portName = DropdownServiceModel.getPortName(false, "", pType == ConstantPrm.SYSTEMTYPE.CRUDE ? "PORT_CMCS" : "PORT_CMMT");
            model.vessel = DropdownServiceModel.getVehicle("SCH_C");
            model.product = DropdownServiceModel.getProduct("MAT_CMCS");
            model.portName = DropdownServiceModel.getPortName(false, "", "PORT_CMCS");
            model.vesselActivity = DropdownServiceModel.getVesselActivity(pType, true, "--- Please Select ---");
        }

        public List<ButtonAction> ButtonListScheduler
        {
            get
            {
                if (Session["ButtonListScheduler"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListScheduler"];
            }
            set { Session["ButtonListScheduler"] = value; }
        }

        private void BindButtonPermission(List<ButtonAction> Button)
        {
            if (Request.QueryString["isDup"] == null)
            {
                ViewBag.ButtonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    ViewBag.PageMode = "READ";
                }
                foreach (ButtonAction _button in Button)
                {
                    string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                    ViewBag.ButtonCode += _btn;
                }
                ViewBag.ButtonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListScheduler = Button;
            }
            else
            {
                ViewBag.ButtonMode = "AUTO";
                ViewBag.PageMode = "EDIT";
            }
        }

        private void updateModel(FormCollection form, ref SchedulerCMCSViewModel model, bool isTextMode = false)
        {
            //Voyage Period
            if (!string.IsNullOrEmpty(model.schs_tempDateFrom) || !string.IsNullOrEmpty(model.schs_tempDateTo))
            {
                //model.schs_detail.date_start = model.schs_tempDateFrom;
                //model.schs_detail.date_end = model.schs_tempDateTo;
                model.schs_detail.date_start = model.schs_tempDateFrom.SplitWord(" ")[0] + " " + model.schs_tempDateFrom.SplitWord(" ")[1];
                model.schs_detail.date_end = model.schs_tempDateFrom.SplitWord(" ")[3] + " " + model.schs_tempDateFrom.SplitWord(" ")[4];
            }

            //Load Port
            if (model.schs_load_ports != null)
            {
                int i = 0;
                List<Model.SchsLoadPorts> schs_load_ports_list = new List<Model.SchsLoadPorts>();
                foreach (var v1 in model.schs_load_ports)
                {
                    Model.SchsLoadPorts schs_load_ports = new Model.SchsLoadPorts();
                    schs_load_ports.order = (i + 1).ToString();
                    schs_load_ports.port = v1.port;
                    List<Model.SchsCrude> schs_crude_list = new List<Model.SchsCrude>();
                    int j = 0;
                    foreach (var v2 in model.schs_load_ports[i].schs_crude)
                    {
                        Model.SchsCrude schs_crude = new Model.SchsCrude();
                        schs_crude.crude = v2.crude;
                        schs_crude.qty = v2.qty;
                        if (v2.nominate_date != null)
                        {
                            schs_crude.nominate_date_start = v2.nominate_date.SplitWord(" ")[0];
                            schs_crude.nominate_date_end = v2.nominate_date.SplitWord(" ")[2];

                        }
                        else
                        {
                            schs_crude.nominate_date_start = null;
                            schs_crude.nominate_date_end = null;
                        }
                        if (v2.received_date != null)
                        {
                            schs_crude.received_date_start = v2.received_date.SplitWord(" ")[0];
                            schs_crude.received_date_end = v2.received_date.SplitWord(" ")[2];
                        }
                        else
                        {
                            schs_crude.received_date_start = null;
                            schs_crude.received_date_end = null;
                        }

                        schs_crude_list.Add(schs_crude);
                        j++;
                    }
                    schs_load_ports.schs_crude = schs_crude_list;
                    schs_load_ports_list.Add(schs_load_ports);
                    i++;
                }
                model.schs_detail.schs_load_ports = schs_load_ports_list;
            }

            //DisPort
            if (model.schs_dis_ports != null)
            {
                List<Model.SchsDisPorts> schs_dis_ports_list = new List<Model.SchsDisPorts>();
                for (int i = 0; i < model.schs_dis_ports.Count; i++)
                {
                    Model.SchsDisPorts schs_dis_ports = new Model.SchsDisPorts();
                    schs_dis_ports.order = (i + 1).ToString();
                    schs_dis_ports.port = model.schs_dis_ports[i].port;
                    schs_dis_ports_list.Add(schs_dis_ports);
                }
                model.schs_detail.schs_dis_ports = schs_dis_ports_list;
            }
        }

        // GET: CPAIMVC/SchedulerCMCS
        public ActionResult Index()
        {
            Session["PageAction"] = "SCH"; // For _btn_Click
            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            SchedulerCMCSViewModel model = new SchedulerCMCSViewModel();
            //initializeDropDownList(ref model, null);

            if (TranID != null && Status != null)
            {
                // Get Data by TranID
                ResponseData resData = LoadDataSchedulerDetail(TranID, Type, ref model);
                // Set DDL
                initializeDropDownList(ref model, Type);

                // Set Alert Message
                if (resData != null && resData.result_code != "1")
                {
                    TempData["res_message"] = resData.response_message;
                }

                // Check is not Duplicate
                if (Request.QueryString["isDup"] == null)
                {
                    ViewBag.TaskID = TranID;    // Set Transaction ID

                    if (Status.ToUpper() == "DRAFT")
                    {
                        ViewBag.Title = "Edit schedule (DRAFT)";
                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                    }
                    else if (Status.ToUpper() == "SUBMIT")
                    {
                        ViewBag.Title = "Edit schedule (SUBMIT)";
                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                    }
                    else // CANCEL or Other
                    {
                        ViewBag.Title = "View schedule (CANCEL)";
                        ViewBag.PageMode = "READ";       //PageMode is EDIT or READ
                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                    }

                    // Set parameter not Encrypt
                    model.schs_Transaction_id = String.IsNullOrEmpty(TranID) ? null : TranID;
                    model.schs_Status = String.IsNullOrEmpty(Status) ? null : Status; // Status (DRAFT,SUBMIT,CANCEL)
                    model.schs_Type = String.IsNullOrEmpty(Type) ? null : Type;
                    model.schs_Doc_no = String.IsNullOrEmpty(DocNO) ? null : DocNO;
                    model.schs_Req_transaction_id = String.IsNullOrEmpty(Tran_Req_ID) ? null : Tran_Req_ID;
                }
                else
                {
                    // Add
                    ViewBag.Title = "Add new schedule (Duplicate)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL
                }
                return View("Index", model);
            }
            else
            {
                // New Schedule
                ViewBag.Title = "Add new schedule";
                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or 
                initializeDropDownList(ref model, Type);
                return View("Index", model);
            }
        }

        // GET: CPAIMVC/SchedulerCMCS
        public ActionResult Search(string Type = null, string StartDate = null, string EndDate = null, string MultiVesselID = null)
        {
            Session.Remove("PageAction");
            string sessTypeEncrypt = "";

            if (Type != null && sessTypeEncrypt == "")
                Session["Schedule_Type"] = Type;

            if (Session["Schedule_Type"] != null)
                sessTypeEncrypt = Convert.ToString(Session["Schedule_Type"]);

            // Get parameter from URL QueryString
            string pType = String.IsNullOrEmpty(Type) ? sessTypeEncrypt.Decrypt() : Type.Decrypt();
            string pStartDate = String.IsNullOrEmpty(StartDate) ? null : StartDate;
            string pEndDate = String.IsNullOrEmpty(EndDate) ? null : EndDate;
            string pMultiVesselID = String.IsNullOrEmpty(MultiVesselID) ? null : MultiVesselID;

            SchedulerCMCSViewModel model = new SchedulerCMCSViewModel();

            var startDateFormat = ""; var endDateFormat = "";
            if (pStartDate == null || pEndDate == null)
            {
                var currentDate = DateTime.Now;
                var startDate = new DateTime(currentDate.AddMonths(-1).Year, currentDate.AddMonths(-1).Month, 1);               // Start date Past 1 month
                var endDate = new DateTime(currentDate.AddMonths(3).Year, currentDate.AddMonths(3).Month, 1).AddDays(-1);       // End date Next 2 month

                startDateFormat = ConvertDateTimeToDateStringFormat(startDate, "dd/MM/yyyy");
                endDateFormat = ConvertDateTimeToDateStringFormat(endDate, "dd/MM/yyyy");
            }
            else
            {
                startDateFormat = pStartDate;
                endDateFormat = pEndDate;
            }

            model.schs_searchStartEndDate = startDateFormat + " to " + endDateFormat;
            var multiVesselID = pMultiVesselID;
            SearchScheduleTransData(startDateFormat, endDateFormat, multiVesselID, pType, ref model);

            initializeDropDownList(ref model, pType);
            return View(model);
        }

        private bool SearchScheduleTransData(string sDate, string eDate, string multiVesselID, string pType, ref SchedulerCMCSViewModel _model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000035;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user_group", V = ConstantPrm.SYSTEMTYPE.CMCS });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.SCHEDULE });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = _FN.ConvertDateFormat(sDate, true).Replace("-", "/") });
            req.Req_parameters.P.Add(new P { K = "to_date", V = _FN.ConvertDateFormat(eDate, true).Replace("-", "/").SplitWord(" ")[0] + " 23:59:59" });

            if (String.IsNullOrEmpty(multiVesselID) == false)
                req.Req_parameters.P.Add(new P { K = "index_5", V = multiVesselID });

            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            if (_DataJson != "")
            {
                List_Scheduletrx scheduletrx = new List_Scheduletrx();
                scheduletrx = ShareFunction.DeserializeXMLFileToObject<List_Scheduletrx>(_DataJson);

                if (scheduletrx.ScheduleTransaction != null)
                {
                    // Config Data for Grid
                    _model.schs_cmsc_trans_grid_view_model = getSchTransForGridView(scheduletrx.ScheduleTransaction);

                    // Config Data for Timeline

                    List<SchTransVesselViewModel> schList_temp = new List<SchTransVesselViewModel>();
                    _model.schs_cmsc_trans_vessel_json = getSchTransForVesselTimeLine(scheduletrx.ScheduleTransaction, ref schList_temp);
                    List<SchsActivityTimelineListViewModel> activity_info = new List<SchsActivityTimelineListViewModel>();
                    _model.schs_cmsc_trans_timeline_json = getSchTransForTimeLine(scheduletrx.ScheduleTransaction, schList_temp, sDate, eDate, ref activity_info);

                    foreach (var z in activity_info)
                    {
                        _model.activity_info.Add(z);
                    }
                }
            }
            else
            {
                // Set Alert Message
                if (resData != null && resData.result_code != "1")
                {
                    TempData["res_message"] = resData.response_message;
                    TempData["returnPage"] = Url.Action("Search", "SchedulerCMCS", new { Type = pType.Encrypt() }); // returnPage;

                }
                else
                {
                    TempData["res_message"] = "Data Not Found";
                    TempData["returnPage"] = Url.Action("Search", "SchedulerCMCS", new { Type = pType.Encrypt() }); //returnPage;
                }
                return false;
            }
            return false;
        }

        private List<SchCMCSTransGridViewModel> getSchTransForGridView(List<ScheduleTransaction> pSchTrans)
        {
            List<SchCMCSTransGridViewModel> schList = new List<SchCMCSTransGridViewModel>();

            // Fillter status not equals SUBMIT
            var filterList = from v in pSchTrans
                             where v.Status != "SUBMIT"
                             select v;

            foreach (ScheduleTransaction item in filterList.ToList())
            {
                schList.Add(new SchCMCSTransGridViewModel
                {
                    Transaction_id_Encrypted = item.Transaction_id.Encrypt()
                    ,
                    Req_transaction_id_Encrypted = item.Req_transaction_id.Encrypt()
                    ,
                    Doc_no_Encrypted = item.Purchase_no.Encrypt()
                    ,
                    Type_Encrypted = item.Type.Encrypt()
                    ,
                    scht_Datetime = string.Format("{0} - {1}", item.Date_start.SplitWord(" ")[0], item.Date_end.SplitWord(" ")[0])
                    ,
                    scht_Doc_no = item.Purchase_no
                    ,
                    scht_vasselName = item.Vessel
                    ,
                    scht_tc = item.TC
                    ,
                    scht_note = item.Remark
                    ,
                    scht_reson = item.Reason
                    ,
                    scht_status = item.Status
                    ,
                    scht_type = item.Type
                    ,
                    scht_status_Encrypted = item.Status.Encrypt()
                });
            }
            return schList;
        }

        private string getSchTransForVesselTimeLine(List<ScheduleTransaction> pSchTrans, ref List<SchTransVesselViewModel> section)
        {
            List<SchTransVesselViewModel> schList_temp = new List<SchTransVesselViewModel>();
            // Fillter status not equals SUBMIT
            var filterList = from v in pSchTrans
                             where v.Status == "SUBMIT"
                             select v;
            var data_group = filterList.ToList().GroupBy(v => v.Vessel_id);
            int sectionid = 1;
            foreach (var item in data_group.ToList())
            {
                SchTransVesselViewModel schs = new SchTransVesselViewModel();
                schs.key = sectionid.ToString();
                schs.label = VehicleServiceModel.GetName(item.Key);
                schList_temp.Add(schs);
                sectionid++;
            }
            section = schList_temp;
            return new JavaScriptSerializer().Serialize(schList_temp); ;
        }

        private string getSchTransForTimeLine(List<ScheduleTransaction> pSchTrans, List<SchTransVesselViewModel> section, string startDate, string endDate, ref List<SchsActivityTimelineListViewModel> timelineList)
        {
            // Fillter status not equals SUBMIT
            var filterList = from v in pSchTrans
                             where v.Status == "SUBMIT"
                             select v;
            string date_search = "";
            if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            {
                date_search = "&DateSearch=" + startDate + "_" + endDate;
            }
            int sectionid = 1;

            string result = "";

            foreach (ScheduleTransaction item in filterList.ToList())
            {
                result += sectionid > 1 ? "," : "";
                string ttt = section.First(s => s.label == item.Vessel).key;

                SchsActivityTimelineViewModel main_timeline = new SchsActivityTimelineViewModel();
                //main_timeline.text = string.Format("TC {0}({1})", item.TC, item.Reason);
                main_timeline.text = string.Format("TC {0}", item.TC);
                main_timeline.start_date = convertDateToTimeline(item.Date_start);
                main_timeline.end_date = convertDateToTimeline(item.Date_end);
                main_timeline.color = "#bfbfbf";
                //main_timeline.section_id = sectionid.ToString();
                main_timeline.section_id = section.First(s => s.label == item.Vessel).key;
                main_timeline.href_edit = Url.Action("Index", "SchedulerCMCS").ToString() + "?TranID=" + item.Transaction_id.Encrypt() + "&Tran_Req_ID=" + item.Req_transaction_id.Encrypt() + "&DocNO=" + item.Purchase_no.Encrypt() + "&Status=" + item.Status.Encrypt() + date_search;
                main_timeline.href_copy = Url.Action("Index", "SchedulerCMCS").ToString() + "?TranID=" + item.Transaction_id.Encrypt() + "&Tran_Req_ID=" + item.Req_transaction_id.Encrypt() + "&Status=" + item.Status.Encrypt() + "&isDup=true" + date_search;
                main_timeline.href_view = Url.Action("Activity", "SchedulerCMCS").ToString() + "?TranId=" + item.Transaction_id.Encrypt() + "&Tran_Req_ID=" + item.Req_transaction_id.Encrypt() + "&Status=" + item.Status.Encrypt() + date_search;
                SchsObjectViewModel obj_act = new JavaScriptSerializer().Deserialize<SchsObjectViewModel>(item.Vessel_Activity);
                string json_act_timeline_list = "";

                if (obj_act.schs_activitys != null && obj_act.schs_activitys.Count != 0)
                {
                    List<SchsActivityTimelineInfoViewModel> activity_info_list = new List<SchsActivityTimelineInfoViewModel>();
                    foreach (var v in obj_act.schs_activitys)
                    {
                        SchsActivityTimelineInfoViewModel activity_info = new SchsActivityTimelineInfoViewModel();
                        activity_info.activity = VesselActivityServiceModel.GetName(v.activity, ConstantPrm.SYSTEMTYPE.CRUDE);
                        activity_info.date_start = v.date_start.SplitWord(" ")[0];
                        if(v.date_end != null)
                        {
                            activity_info.date_end = v.date_end.SplitWord(" ")[0];
                        } 
                        activity_info.activity_start = v.date_start.SplitWord(" ")[1];
                        activity_info.activity_end = (Int32.Parse(v.date_start.SplitWord(" ")[1].SplitWord(":")[0]) + 1).ToString().PadLeft(2, '0') + ":00";
                        activity_info.activity_color = v.activity_color;
                        activity_info_list.Add(activity_info);
                    }
                    main_timeline.activity_info = activity_info_list;

                    List<SchsActivityTimelineListViewModel> act_timeline_list = new List<SchsActivityTimelineListViewModel>();
                    for (int i = 0; i < obj_act.schs_activitys.Count; i++)
                    {
                        SchsActivityTimelineListViewModel activity_info = new SchsActivityTimelineListViewModel();
                        activity_info.activity = "true";
                        activity_info.start_date = convertDateToTimeline(obj_act.schs_activitys[i].date_start);

                        if (i > obj_act.schs_activitys.Count)
                        {
                            activity_info.end_date = obj_act.schs_activitys[i + 1].date_end;
                        }
                        else
                        {
                            activity_info.end_date = convertDateToTimeline(item.Date_end);
                        }

                        activity_info.section_id = section.First(s => s.label == item.Vessel).key;
                        activity_info.color = obj_act.schs_activitys[i].activity_color;
                        act_timeline_list.Add(activity_info);
                    }
                    json_act_timeline_list = "," + new JavaScriptSerializer().Serialize(act_timeline_list).ReplaceFirst("[", "").ReplaceLast("]", "");
                }
                result += new JavaScriptSerializer().Serialize(main_timeline) + json_act_timeline_list;

                sectionid++;

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var activityList = context.CPAI_VESSEL_SCH_S_ACTIVITYS.Where(a => a.VASS_FK_VESSEL_SCH_S == item.Transaction_id).ToList();
                    foreach (var z in activityList)
                    {
                        SchsActivityTimelineListViewModel sch = new SchsActivityTimelineListViewModel();
                        sch.activity = VesselActivityServiceModel.GetName(z.VASS_FK_VESSEL_ACTIVITY, ConstantPrm.SYSTEMTYPE.CRUDE);
                        sch.tcNumber = main_timeline.text;
                        //sch.activity = z.VASS_FK_VESSEL_ACTIVITY;
                        sch.start_date = z.VASS_DATE_START.Value.ToString("dd/MM/yyy HH:mm");

                        if (z.VASS_DATE_END != null)
                        {
                            sch.end_date = z.VASS_DATE_END.Value.ToString("dd/MM/yyy HH:mm");
                        }
                        timelineList.Add(sch);
                    }
                }

            }
            return "[" + result + "]";
        }

        private string convertDateToTimeline(string date)
        {
            string newDate = "";
            if (!string.IsNullOrEmpty(date))
            {
                newDate = DateTime.ParseExact(date, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm");
            }

            return newDate;
        }

        // GET: CPAIMVC/SchedulerCMCS
        public ActionResult Activity()
        {
            Session["PageAction"] = "ACT"; // For _btn_Click
            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();

            SchedulerCMCSViewModel model = new SchedulerCMCSViewModel();

            // Check TranId And Status is not null
            if (TranID != null && Status != null)
            {
                // Get Data by TranID
                ResponseData resData = LoadDataSchedulerActivity(TranID, Type, ref model);

                // Set Alert Message
                if (resData != null && resData.result_code != "1")
                {
                    TempData["res_message"] = resData.response_message;
                    TempData["returnPage"] = Url.Action("Search", "SchedulerCMCS", new { Type = Type.Encrypt() }); // returnPage;//returnPage;
                }

                ViewBag.TaskID = TranID;    // Set Transaction ID

                if (Status.ToUpper() == "SUBMIT")
                {
                    ViewBag.Title = "Edit activity (SUBMIT)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                }

                model.schs_Transaction_id = String.IsNullOrEmpty(TranID) ? null : TranID;
                model.schs_Status = String.IsNullOrEmpty(Status) ? null : Status; // Status (DRAFT,SUBMIT,CANCEL)
                model.schs_Type = String.IsNullOrEmpty(Type) ? null : Type;
                model.schs_Doc_no = String.IsNullOrEmpty(DocNO) ? null : DocNO;
                model.schs_Req_transaction_id = String.IsNullOrEmpty(Tran_Req_ID) ? null : Tran_Req_ID;

                // Set DDL
                initializeDropDownList(ref model, Type);

                return View("Activity", model);
            }
            else
            {
                return View("Activity", model);
            }
        }

        private ResponseData LoadDataSchedulerActivity(string TransactionID, string userType, ref SchedulerCMCSViewModel model)
        {
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();

            ExtraXML_ACT _model = new ExtraXML_ACT();

            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            string _DataJson = "";

            RequestCPAI req = new RequestCPAI();

            // Get Activity
            req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000036;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.SCHEDULE });
            req.Req_parameters.P.Add(new P { K = "type", V = userType });
            req.Req_parameters.P.Add(new P { K = "button_type", V = "ACT" });
            req.Extra_xml = "";

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            _DataJson = resData.extra_xml;

            _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML_ACT>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                ViewBag.ButtonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button);
            }
            else
            {
                ViewBag.ButtonMode = "AUTO";
                ViewBag.PageMode = "EDIT";
            }

            string _btn = "<input type = 'button' class='btn btn-default' id='btnBack' onclick=\"location.href='" + Url.Action("Search", "SchedulerCMCS") + "?Type=" + userType.Encrypt() + "'\" value='BACK' />";
            ViewBag.ButtonCode += _btn;

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<SchedulerCMCSViewModel>(_model.data_detail);
                SchTransVesselViewModel schs_vessel = new SchTransVesselViewModel();
                schs_vessel.key = "1";
                schs_vessel.label = VehicleServiceModel.GetName(model.schs_detail.vessel);
                model.schs_cmsc_trans_vessel_json = "[" + new JavaScriptSerializer().Serialize(schs_vessel) + "]";

                SchsRootObject modelACT = new SchsRootObject();
                modelACT = new JavaScriptSerializer().Deserialize<SchsRootObject>(_model.data_detail_act);

                // Set Schedule
                model.schs_activitys_model = new SchsActivityViewModel();
                model.schs_activitys_model.TranID = TransactionID;
                model.schs_activitys_model.Type = userType;
                model.schs_activitys_model.date = model.schs_detail.date_start.SplitWord(" ")[0];

                SchsActivityTimelineViewModel obj_json = new SchsActivityTimelineViewModel();
                //obj_json.text = string.Format("TC {0} ({1})", model.schs_detail.tc, model.schs_detail.note);
                obj_json.text = string.Format("TC {0}", model.schs_detail.tc);
                obj_json.start_date = convertDateToTimeline(model.schs_detail.date_start);
                obj_json.end_date = convertDateToTimeline(model.schs_detail.date_end);
                obj_json.section_id = "1";
                obj_json.color = "#bfbfbf";

                List<SchsActivityTimelineInfoViewModel> activity_info_list = new List<SchsActivityTimelineInfoViewModel>();
                string json_act_timeline_list = "";
                if (modelACT.schs_activitys != null)
                {

                    foreach (var item in modelACT.schs_activitys)
                    {
                        //201712011530430325557
                        //using (EntityCPAIEngine context = new EntityCPAIEngine())
                        //{
                        //    var c =context.CPAI_VESSEL_SCHEDULE_ACTIVITYS.Where(w=>w.)
                        //}

                        SchsActivityTimelineInfoViewModel activity_info = new SchsActivityTimelineInfoViewModel();
                        activity_info.activity = VesselActivityServiceModel.GetName(item.activity, userType);
                        activity_info.date_start = item.date_start.SplitWord(" ")[0];
                        if (!String.IsNullOrEmpty(item.date_end))
                        {
                            activity_info.date_end = item.date_end.SplitWord(" ")[0];
                            activity_info.activity_end = item.date_end.SplitWord(" ")[1];
                        }
                        activity_info.activity_start = item.date_start.SplitWord(" ")[1];
                        activity_info.activity_color = item.activity_color;
                        activity_info_list.Add(activity_info);
                    }

                    List<SchsActivityTimelineListViewModel> act_timeline_list = new List<SchsActivityTimelineListViewModel>();

                    for (int i = 0; i < modelACT.schs_activitys.Count; i++)
                    {
                        SchsActivityTimelineListViewModel activity_info = new SchsActivityTimelineListViewModel();
                        activity_info.activity = "true";
                        activity_info.start_date = convertDateToTimeline(modelACT.schs_activitys[i].date_start);

                        if (i > modelACT.schs_activitys.Count)
                        {
                            activity_info.end_date = modelACT.schs_activitys[i + 1].date_end;
                        }
                        else
                        {
                            if(modelACT.schs_activitys[i].date_end != null)
                            {
                                activity_info.end_date = convertDateToTimeline(modelACT.schs_activitys[i].date_end);
                            } 
                        }

                        activity_info.section_id = "1";
                        activity_info.color = modelACT.schs_activitys[i].activity_color;
                        act_timeline_list.Add(activity_info);
                    }

                    json_act_timeline_list = "," + new JavaScriptSerializer().Serialize(act_timeline_list).ReplaceFirst("[", "").ReplaceLast("]", "");

                    List<SchsActivityViewModel_GridValues> schs_activity_grid_model_list = new List<SchsActivityViewModel_GridValues>();
                    foreach (var item in modelACT.schs_activitys)
                    {
                        SchsActivityViewModel_GridValues schs_activity_grid_model = new SchsActivityViewModel_GridValues();
                        schs_activity_grid_model.act_order = item.order;
                        schs_activity_grid_model.act_code = VesselActivityServiceModel.GetStatus_Color_RowID(item.activity, userType);
                        schs_activity_grid_model.act_color = item.activity_color;
                        schs_activity_grid_model.act_date_start = item.date_start;
                        schs_activity_grid_model.act_date_end = item.date_end;
                        schs_activity_grid_model.act_portName = item.portName;
                        //schs_activity_grid_model.act_time = item.date.SplitWord(" ")[1];
                        schs_activity_grid_model.act_ref = item.@ref;
                        schs_activity_grid_model.act_ref_status = VesselActivityServiceModel.GetStatus_Color_RowID(item.activity, userType).Split('|')[0];
                        schs_activity_grid_model_list.Add(schs_activity_grid_model);
                        SchsActivityTimelineListViewModel act = new SchsActivityTimelineListViewModel();
                        act.activity = item.activity;
                        act.start_date = item.date_start;
                        act.end_date = item.date_end;
                        model.activity_info.Add(act);
                    }
                    model.schs_activity_grid_model = schs_activity_grid_model_list;
                }

                obj_json.activity_info = activity_info_list;
                string json_act_timeline = new JavaScriptSerializer().Serialize(obj_json);
                model.schs_activitys_model.sch_activity_timeline_model_json = "[" + json_act_timeline + json_act_timeline_list + "]";
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = returnPage;
            }
            return resData;
        }

        public string ConvertDateTimeToDateStringFormat(DateTime pDate, string DateStringFormat = "yyyy-MM-dd")
        {
            return pDate.ToString(DateStringFormat, CultureInfo.InvariantCulture);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, SchedulerCMCSViewModel model)
        {
            string note = form["hdfNoteAction"];

            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            DocumentActionStatus _status = DocumentActionStatus.Draft;
            this.updateModel(form, ref model);
            string typeDecrypt = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";

            ResponseData resData = SaveDataSchedule(_status, model, note);

            ViewBag.TaskID = resData.transaction_id;    // Set Transaction ID

            TempData["res_message"] = resData.response_message;

            if (resData != null && resData.result_code == "1")
            {
                string tranID = resData.transaction_id.Encrypt();
                string reqID = resData.req_transaction_id.Encrypt();
                string docno = resData.resp_parameters[0].v.Encrypt();
                string status = "DRAFT".Encrypt();
                string type = typeDecrypt.Encrypt();

                ViewBag.Title = "Edit schedule (DRAFT)";
                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL

                string path = string.Format("~/CPAIMVC/SchedulerCMCS?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Status={3}&Type={4}", tranID, reqID, docno, status, type);
                return Redirect(path);
            }
            else
            {
                // Check is not Duplicate
                if (isDup == null)
                {
                    ViewBag.Title = "Add new schedule";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or 
                }
                else
                {
                    // Add
                    ViewBag.Title = "Add new schedule (Duplicate)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL
                }
            }

            initializeDropDownList(ref model, typeDecrypt);
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, SchedulerCMCSViewModel model)
        {
            string note = form["hdfNoteAction"];

            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            DocumentActionStatus _status = DocumentActionStatus.Submit;
            this.updateModel(form, ref model);

            string typeDecrypt = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";

            ResponseData resData = SaveDataSchedule(_status, model, note);

            TempData["res_message"] = resData.response_message;

            if (resData != null && resData.result_code == "1")
            {
                string tranID = resData.transaction_id.Encrypt();
                string reqID = resData.req_transaction_id.Encrypt();
                string docno = resData.resp_parameters[0].v.Encrypt();
                string status = "SUBMIT".Encrypt();
                string type = typeDecrypt.Encrypt();

                ViewBag.Title = "Edit schedule (SUBMIT)";
                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL

                string path = string.Format("~/CPAIMVC/SchedulerCMCS?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Type={3}&Status={4}", tranID, reqID, docno, type, status);
                return Redirect(path);
            }
            else
            {
                // Check is not Duplicate
                if (isDup == null)
                {
                    ViewBag.Title = "Add new schedule";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or 
                }
                else
                {
                    // Add
                    ViewBag.Title = "Add new schedule (Duplicate)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL

                }
            }
            initializeDropDownList(ref model, typeDecrypt);
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, SchedulerCMCSViewModel model)
        {
            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? model.schs_Transaction_id : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? model.schs_Status : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? model.schs_Doc_no : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? model.schs_Req_transaction_id : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            string PageAction = Convert.ToString(Session["PageAction"]);
            string buttonCode = model.ButtonCode;

            SchedulerCMCSViewModel modelTemp = model;   // Set model to temp for error message

            if (PageAction == "SCH")
            {
                // Save Schedule
                string type = "";
                this.updateModel(form, ref model);
                if (ButtonListScheduler != null)
                {
                    var _lstButton = ButtonListScheduler.Where(x => x.name == form["hdfEventClick"]).ToList();
                    if (_lstButton != null && _lstButton.Count > 0)
                    {
                        string _xml = _lstButton[0].call_xml;
                        string note = form["hdfNoteAction"];
                        List<ReplaceParam> _param = new List<ReplaceParam>();
                        string TranReqID = model.schs_Req_transaction_id;
                        SchsRootObject obj = new SchsRootObject();
                        obj.schs_detail = model.schs_detail;
                        var json = new JavaScriptSerializer().Serialize(obj);
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _xml = _FN.RaplceParamXML(_param, _xml);
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                        RequestData _req = new RequestData();
                        ResponseData resData = new ResponseData();
                        _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                        resData = service.CallService(_req);

                        var btnName = _lstButton[0].name.ToUpper();
                        ViewBag.TaskID = resData.transaction_id;    // Set Transaction ID

                        string tranID = resData.transaction_id.Encrypt();
                        string reqID = resData.req_transaction_id.Encrypt();
                        string docno = resData.resp_parameters[0].v.Encrypt();

                        string status = "";
                        if (btnName == "SAVE DRAFT")
                        {
                            status = "DRAFT".Encrypt();
                        }
                        else if (btnName == "CANCEL")
                        {
                            status = "CANCEL".Encrypt();
                        }
                        else if (btnName == "SUBMIT" || btnName == "EDIT SCHEDULE")
                        {
                            status = "SUBMIT".Encrypt();
                        }

                        if (resData != null && resData.result_code == "1")
                        {
                            type = !string.IsNullOrEmpty(model.schs_Type) ? model.schs_Type.Encrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString() : "";

                            //string path = string.Format("~/CPAIMVC/SchedulerCMCS/Index?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Type={3}&Status={4}", tranID, reqID, docno, type, status);
                            string path = string.Format("~/CPAIMVC/SchedulerCMCS?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Status={3}", tranID, reqID, docno, status);
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            return Redirect(path);
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            string path = string.Format("~/CPAIMVC/SchedulerCMCS?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Status={3}", tranID, reqID, docno, status);
                            // Get Data by TranID
                            ResponseData resDataLoad = LoadDataSchedulerDetail(TranID, Type, ref model);

                            // Set Alert Message
                            if (resDataLoad != null && resDataLoad.result_code == "1")
                            {
                                // Check is not Duplicate
                                if (isDup == null)
                                {
                                    ViewBag.TaskID = TranID;    // Set Transaction ID

                                    if (Status == "DRAFT")
                                    {
                                        ViewBag.Title = "Edit schedule (DRAFT)";
                                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                    }
                                    else if (Status == "SUBMIT")
                                    {
                                        ViewBag.Title = "Edit schedule (SUBMIT)";
                                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                    }
                                    else // CANCEL or Other
                                    {
                                        ViewBag.Title = "View schedule (CANCEL)";
                                        ViewBag.PageMode = "READ";       //PageMode is EDIT or READ
                                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                    }
                                    // set model temp
                                    model = modelTemp;
                                    return Redirect(path);
                                }
                                else
                                {
                                    // Add
                                    ViewBag.Title = "Add new schedule (Duplicate)";
                                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL
                                }
                            }
                            else
                            {
                                TempData["res_message"] = resDataLoad.response_message;
                            }
                        }
                    }
                }
                else
                {
                    TempData["res_message"] = "ERROR";
                }
                initializeDropDownList(ref model, type.Decrypt());
                return View("Index", model);
            }
            else if (PageAction == "ACT")
            {
                // Save Activity
                Session["PageAction"] = "ACT"; // For _btn_Click
                string type = "";
                if (ButtonListScheduler != null)
                {
                    var _lstButton = ButtonListScheduler.Where(x => x.name == form["hdfEventClick"]).ToList();
                    if (_lstButton != null && _lstButton.Count > 0)
                    {
                        string _xml = _lstButton[0].call_xml;
                        string note = form["hdfNoteAction"];
                        List<ReplaceParam> _param = new List<ReplaceParam>();
                        string TranReqID = model.schs_Req_transaction_id;

                        SchsRootObject obj = new SchsRootObject();
                        obj.schs_activitys = new List<SchsActivity>();

                        foreach (var item in model.schs_activity_grid_model)
                        {
                            var actcode = item.act_code.Split('|');
                            obj.schs_activitys.Add(new SchsActivity
                            {
                                order = item.act_order
                                ,
                                activity = actcode[2]
                                ,
                                activity_color = actcode[1]
                                //, date = string.Format("{0} {1}", item.act_date, item.act_time)
                                ,
                                date_start = item.act_date_start
                                ,
                                date_end = item.act_date_end
                                ,
                                @ref = item.act_ref
                                ,
                                portName = item.act_portName
                            }
                            );
                        }

                        var json = new JavaScriptSerializer().Serialize(obj);
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _xml = _FN.RaplceParamXML(_param, _xml);
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                        RequestData _req = new RequestData();
                        ResponseData resData = new ResponseData();
                        _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                        resData = service.CallService(_req);

                        var btnName = _lstButton[0].name.ToUpper();
                        ViewBag.TaskID = resData.transaction_id;    // Set Transaction ID
                        if (resData != null && resData.result_code == "1")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string docno = resData.resp_parameters[0].v.Encrypt();
                            type = !string.IsNullOrEmpty(model.schs_Type) ? model.schs_Type.Encrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString() : "";
                            string status = "";// btnName == "CANCEL" ? "CANCEL".Encrypt() : model.schd_Status.Encrypt();

                            if (btnName == "EDIT ACTIVITY")
                            {
                                status = "SUBMIT".Encrypt();
                            }

                            string path = string.Format("~/CPAIMVC/SchedulerCMCS/Activity?TranID={0}&Type={1}&Tran_Req_ID={2}&DocNO={3}&Status={4}", tranID, type, reqID, docno, status);

                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            return Redirect(path);
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;

                            // Get Data by TranID
                            ResponseData resDataLoad = LoadDataSchedulerActivity(TranID, Type, ref model);

                            // Set Alert Message
                            if (resDataLoad != null && resDataLoad.result_code == "1")
                            {

                                if (Status.ToUpper() == "SUBMIT")
                                {
                                    ViewBag.Title = "Edit activity (SUBMIT)";
                                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                    ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                }

                                // set model temp
                                model.schs_activitys_model.schs_activity_grid_model = modelTemp.schs_activitys_model.schs_activity_grid_model;
                            }
                            else
                            {
                                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            }

                            model.schs_Transaction_id = String.IsNullOrEmpty(TranID) ? null : TranID;
                            model.schs_Status = String.IsNullOrEmpty(Status) ? null : Status; // Status (DRAFT,SUBMIT,CANCEL)
                            model.schs_Type = String.IsNullOrEmpty(Type) ? null : Type;
                            model.schs_Doc_no = String.IsNullOrEmpty(DocNO) ? null : DocNO;
                            model.schs_Req_transaction_id = String.IsNullOrEmpty(Tran_Req_ID) ? null : Tran_Req_ID;

                            initializeDropDownList(ref model, Type);
                        }
                    }
                }
                else
                {
                    TempData["res_message"] = "เกิดข้อผิดพลาด";
                }
                return View("Activity", model);
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
                string sType = !string.IsNullOrEmpty(model.schs_Type) ? model.schs_Type.Encrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString() : "";
                return RedirectToAction("Search", "SchedulerCMCS", new { Type = sType.Encrypt() });
            }
        }

        // Function for Save Data Schedule To Engine Service (F10000009)
        private ResponseData SaveDataSchedule(DocumentActionStatus _status, SchedulerCMCSViewModel model, string note)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }
            string type = !string.IsNullOrEmpty(model.schs_Type) ? model.schs_Type : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";

            SchsRootObject obj = new SchsRootObject();
            obj.schs_detail = model.schs_detail;

            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000034;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.SCHEDULE });
            req.req_parameters.p.Add(new p { k = "data_detail_input_sch", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user_group", v = ConstantPrm.SYSTEMTYPE.CMCS });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);
            return resData;
        }

        private ResponseData LoadDataSchedulerDetail(string TransactionID, string userType, ref SchedulerCMCSViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000036;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.SCHEDULE });
            req.Req_parameters.P.Add(new P { K = "type", V = userType });
            req.Req_parameters.P.Add(new P { K = "button_type", V = "SCH" });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                ViewBag.ButtonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    ViewBag.ButtonMode = "MANUAL";
                    ViewBag.PageMode = "READ";
                }
                else
                {
                    ViewBag.ButtonMode = "AUTO";
                    ViewBag.PageMode = "EDIT";
                }
            }

            string url = Url.Action("Search", "SchedulerCMCS", new { Type = userType.Encrypt() });
            string _btn = "<input type = 'button' class='btn btn-default' id='btnBack' onclick=\"location.href='" + url + "'\" value='BACK' />";

            ViewBag.ButtonCode += _btn;

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<SchedulerCMCSViewModel>(_model.data_detail);
                if (model.schs_detail != null)
                {
                    if (!string.IsNullOrEmpty(model.schs_detail.date_start) && !string.IsNullOrEmpty(model.schs_detail.date_end))
                    {
                        model.schs_tempDateFrom = model.schs_detail.date_start + " to " + model.schs_detail.date_end;
                        //model.schs_tempDateTo = model.schs_detail.date_end;
                    }

                    if (model.schs_detail.schs_load_ports.Count > 0)
                    {
                        List<ViewModels.SchsLoadPorts> schs_load_ports_list = new List<ViewModels.SchsLoadPorts>();
                        for (int i = 0; i < model.schs_detail.schs_load_ports.Count; i++)
                        {
                            ViewModels.SchsLoadPorts schs_load_ports = new ViewModels.SchsLoadPorts();
                            schs_load_ports.order = model.schs_detail.schs_load_ports[i].order;
                            schs_load_ports.port = model.schs_detail.schs_load_ports[i].port;

                            List<ViewModels.SchsCrudeDetail> schs_crude_list = new List<ViewModels.SchsCrudeDetail>();
                            for (int j = 0; j < model.schs_detail.schs_load_ports[i].schs_crude.Count; j++)
                            {
                                ViewModels.SchsCrudeDetail schs_crude = new ViewModels.SchsCrudeDetail();
                                schs_crude.crude = model.schs_detail.schs_load_ports[i].schs_crude[j].crude;
                                schs_crude.qty = model.schs_detail.schs_load_ports[i].schs_crude[j].qty;
                                if (model.schs_detail.schs_load_ports[i].schs_crude[j].nominate_date_start != null && model.schs_detail.schs_load_ports[i].schs_crude[j].nominate_date_end != null)
                                {
                                    schs_crude.nominate_date = string.Format("{0} to {1}", model.schs_detail.schs_load_ports[i].schs_crude[j].nominate_date_start, model.schs_detail.schs_load_ports[i].schs_crude[j].nominate_date_end);
                                }
                                if (model.schs_detail.schs_load_ports[i].schs_crude[j].received_date_start != null && model.schs_detail.schs_load_ports[i].schs_crude[j].received_date_end != null)
                                {
                                    schs_crude.received_date = string.Format("{0} to {1}", model.schs_detail.schs_load_ports[i].schs_crude[j].received_date_start, model.schs_detail.schs_load_ports[i].schs_crude[j].received_date_end);
                                }

                                schs_crude_list.Add(schs_crude);
                            }
                            schs_load_ports.schs_crude = schs_crude_list;
                            schs_load_ports_list.Add(schs_load_ports);
                        }
                        model.schs_load_ports = schs_load_ports_list;
                    }

                    if (model.schs_detail.schs_dis_ports.Count > 0)
                    {
                        List<ViewModels.SchsDisPorts> schs_dis_ports_list = new List<ViewModels.SchsDisPorts>();
                        for (int i = 0; i < model.schs_detail.schs_dis_ports.Count; i++)
                        {
                            ViewModels.SchsDisPorts schs_dis_ports = new ViewModels.SchsDisPorts();
                            schs_dis_ports.order = model.schs_detail.schs_dis_ports[i].order;
                            schs_dis_ports.port = model.schs_detail.schs_dis_ports[i].port;
                            schs_dis_ports_list.Add(schs_dis_ports);
                        }
                        model.schs_dis_ports = schs_dis_ports_list;
                    }
                }
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = returnPage;
            }

            model.ButtonCode = Convert.ToString(ViewBag.ButtonCode); // set button code to model
            return resData;
        }
        #region GenExcel
        public Vessel GenExcel_Tc(Search Search)
        {
            Vessel Vessel = new Vessel();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var MT_VEHICLE = context.MT_VEHICLE.Where(w => w.VEH_ID == Search.sVessel).Select(s => new { s.VEH_ID, s.VEH_VEHICLE }).FirstOrDefault();
                var CPAI_VESSEL_SCH_S = (from cvs_s in context.CPAI_VESSEL_SCH_S
                                         join cvs_s_a in context.CPAI_VESSEL_SCH_S_ACTIVITYS on cvs_s.VSDS_ROW_ID equals cvs_s_a.VASS_FK_VESSEL_SCH_S
                                         where cvs_s.VSDS_FK_VEHICLE == MT_VEHICLE.VEH_ID && cvs_s.VSDS_STATUS == "SUBMIT"
                                         orderby cvs_s.VSDS_DATE_START
                                         select new
                                         {
                                             cvs_s.VSDS_ROW_ID,
                                             voyage_s = cvs_s.VSDS_DATE_START,
                                             voyage_e = cvs_s.VSDS_DATE_END,
                                             activity_s = cvs_s_a.VASS_DATE_START,
                                             activity_e = cvs_s_a.VASS_DATE_END,
                                             tc = cvs_s.VSDS_TC,
                                             cvs_s.VSDS_FK_VEHICLE,
                                             cvs_s_a.VASS_FK_VESSEL_ACTIVITY
                                         }).ToList();
                var AcricityList = CPAI_VESSEL_SCH_S.Where(w => Convert.ToInt16(w.tc) >= Convert.ToInt16(Search.VoyageFrom) && Convert.ToInt16(w.tc) <= Convert.ToInt16(Search.VoyageTo)).GroupBy(g => g.tc).OrderBy(o => o.Key).ToList();
                Vessel.Vessel_Name = MT_VEHICLE.VEH_VEHICLE;
                Vessel.Operating_Year = new List<Operating_Year>();
                Operating_Year Operating_Year = new Operating_Year();
                Operating_Year.Activity = new List<Operating_Activity>();
                Operating_Year.Qtr = new List<Operating_Qtr>();
                foreach (var VASS_FK_VESSEL_ACTIVITY in AcricityList)
                {
                    foreach (var item in VASS_FK_VESSEL_ACTIVITY)
                    {
                        Operating_Activity Operating_Activity = new Operating_Activity();
                        Operating_Activity.Qtr = new List<Operating_Qtr>();
                        var MT_VESSEL_ACTIVITY = (from M_VESSEL_ACTIVITY in context.MT_VESSEL_ACTIVITY
                                                  join M_VESSEL_ACTIVITY_C in context.MT_VESSEL_ACTIVITY_CONTROL on M_VESSEL_ACTIVITY.MVA_ROW_ID equals M_VESSEL_ACTIVITY_C.MAC_FK_VESSEL_ACTIVITY
                                                  where M_VESSEL_ACTIVITY.MVA_ROW_ID == item.VASS_FK_VESSEL_ACTIVITY
                                                  select new
                                                  {
                                                      M_VESSEL_ACTIVITY.MVA_ROW_ID,
                                                      M_VESSEL_ACTIVITY.MVA_CODE,
                                                      M_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN,
                                                      M_VESSEL_ACTIVITY_C.MAC_COLOR_CODE
                                                  }).FirstOrDefault();
                        Operating_Year.Year = item.activity_s.Value.Year.ToString();
                        Operating_Activity.Activity_id = MT_VESSEL_ACTIVITY.MVA_ROW_ID;
                        Operating_Activity.Activity_Name = MT_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN;
                        foreach (var tc in AcricityList)
                        {
                            Operating_Qtr qtrs_g = new Operating_Qtr();
                            qtrs_g.Qtr_Name_Year = "TC " + tc.Key;
                            qtrs_g.Qtr_Vul = 0;
                            Operating_Activity.Qtr.Add(qtrs_g);
                        }
                        var tc_y = "TC " + item.tc;
                        if (Operating_Year.Activity.Count != 0)
                        {
                            var checkActivity = Operating_Year.Activity.Where(w => w.Activity_id == Convert.ToString(MT_VESSEL_ACTIVITY.MVA_ROW_ID)).FirstOrDefault();
                            if (checkActivity != null)
                            {
                                var setdata = checkActivity.Qtr.Where(w => w.Qtr_Name_Year.Contains(tc_y)).FirstOrDefault();
                                TimeSpan times = new TimeSpan();
                                decimal timeOfday = 0;
                                if (item.activity_e != null)
                                {
                                    timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_e.Value.TimeOfDay.Hours);
                                }
                                else
                                {
                                    timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_s.Value.TimeOfDay.Hours);
                                }                                
                                times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                                if (item.activity_e != null)
                                {
                                    var date_vul = ((item.activity_e.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                    setdata.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##")); 
                                }
                                else
                                {
                                    var date_vul = ((item.activity_s.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                    setdata.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                                }
                            }
                            else
                            {
                                var setdata = Operating_Activity.Qtr.Where(w => w.Qtr_Name_Year.Contains(tc_y)).FirstOrDefault();
                                TimeSpan times = new TimeSpan();
                                decimal timeOfday = 0;
                                if(item.activity_e != null)
                                {
                                    timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_e.Value.TimeOfDay.Hours);
                                }
                                else
                                { 
                                    timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_s.Value.TimeOfDay.Hours);
                                }                                
                                times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                                if(item.activity_e != null)
                                {
                                    var date_vul = ((item.activity_e.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                    setdata.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                                }
                                else
                                {
                                    var date_vul = ((item.activity_s.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                    setdata.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                                }                                
                                Operating_Year.Activity.Add(Operating_Activity);
                            }
                        }
                        else
                        {
                            var setdata = Operating_Activity.Qtr.Where(w => w.Qtr_Name_Year.Contains(tc_y)).FirstOrDefault();
                            TimeSpan times = new TimeSpan();
                            decimal timeOfday = 0;
                            if(item.activity_e != null)
                            {
                                timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_e.Value.TimeOfDay.Hours);
                            }
                            else
                            {
                                timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_s.Value.TimeOfDay.Hours);
                            }                            
                            times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                            if(item.activity_e != null)
                            {
                                var date_vul = ((item.activity_e.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                setdata.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                            }
                            else
                            {
                                var date_vul = ((item.activity_s.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                setdata.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                            }                            
                            Operating_Year.Activity.Add(Operating_Activity);
                        }
                    }
                }
                Operating_Year Operating_Tc = new Operating_Year();
                Operating_Tc.Qtr = new System.Collections.Generic.List<Operating_Qtr>();
                foreach (var Tc in AcricityList)
                {
                    Operating_Qtr Operating_Qtr = new Operating_Qtr();
                    //Operating_Qtr.Qtr_Name_Year = "TC " + Tc.Key + " " + item.voyage_s.Value.Year;
                    List<string> newListDate = new List<string>();
                    foreach (var item in Tc)
                    {
                        Operating_Qtr.Qtr_Name_Year = "TC " + Tc.Key;
                        if(item.activity_e != null)
                        {
                            var timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_e.Value.TimeOfDay.Hours);
                            TimeSpan times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                            var date_vul = item.voyage_e.Value.DayOfYear - item.voyage_s.Value.DayOfYear + Convert.ToDecimal(times.TotalDays);
                            newListDate.Add(date_vul.ToString("#.##"));
                        }
                        else
                        {
                            var timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_s.Value.TimeOfDay.Hours);
                            TimeSpan times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                            var date_vul = item.voyage_e.Value.DayOfYear - item.voyage_s.Value.DayOfYear + Convert.ToDecimal(times.TotalDays);
                            newListDate.Add(date_vul.ToString("#.##"));
                        }
                        //var date_vul = item.voyage_e.Value.DayOfYear - item.voyage_s.Value.DayOfYear;
                        //Operating_Qtr.Qtr_Vul = Convert.ToDecimal(date_vul.ToString("#.##"));
                        
                    }
                    if(newListDate != null)
                    {
                        if(newListDate.Count != 0)
                        {
                            Operating_Qtr.Qtr_Vul = Convert.ToDecimal(newListDate[0]);
                        }
                    }                   
                    Operating_Tc.Qtr.Add(Operating_Qtr);
                    Operating_Year.Qtr = Operating_Tc.Qtr;
                }
                Vessel.Operating_Year.Add(Operating_Year);
            }
            return Vessel;
        }
        public Vessel GenExcel_YearToYear(Search Search)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var MT_VEHICLE = context.MT_VEHICLE.Where(w => w.VEH_ID == Search.sVessel).Select(s => new { s.VEH_ID, s.VEH_VEHICLE }).FirstOrDefault();
                var CPAI_VESSEL_SCH_S = (from cvs_s in context.CPAI_VESSEL_SCH_S
                                         join cvs_s_a in context.CPAI_VESSEL_SCH_S_ACTIVITYS on cvs_s.VSDS_ROW_ID equals cvs_s_a.VASS_FK_VESSEL_SCH_S
                                         where cvs_s.VSDS_FK_VEHICLE == MT_VEHICLE.VEH_ID && cvs_s.VSDS_STATUS == "SUBMIT"
                                         orderby cvs_s.VSDS_DATE_START
                                         select new
                                         {
                                             cvs_s.VSDS_ROW_ID,
                                             voyage_s = cvs_s.VSDS_DATE_START,
                                             voyage_e = cvs_s.VSDS_DATE_END,
                                             activity_s = cvs_s_a.VASS_DATE_START,
                                             activity_e = cvs_s_a.VASS_DATE_END,
                                             tc = cvs_s.VSDS_TC,
                                             cvs_s.VSDS_FK_VEHICLE,
                                             cvs_s_a.VASS_FK_VESSEL_ACTIVITY
                                         }).ToList();
                var ListYear = Convert.ToInt16(Search.sYearlyComparsion2) - Convert.ToInt16(Search.sYearlyComparsion1);
                DateTime sYearlyComparsion1_dt1 = new DateTime(Convert.ToInt16(Search.sYearlyComparsion1), 1, 1);
                DateTime sYearlyComparsion2_dt2 = new DateTime(Convert.ToInt16(Search.sYearlyComparsion2), 12, 12);
                CPAI_VESSEL_SCH_S = CPAI_VESSEL_SCH_S.Where(w => w.voyage_s >= sYearlyComparsion1_dt1 && w.voyage_e <= sYearlyComparsion2_dt2).ToList();
                var AcricityList = CPAI_VESSEL_SCH_S.GroupBy(g => g.VASS_FK_VESSEL_ACTIVITY).ToList();
                ///get data
                Vessel Vessel = new Vessel();
                Vessel.Vessel_Name = MT_VEHICLE.VEH_VEHICLE;
                Vessel.Operating_Year = new List<Operating_Year>();
                //set data vul=0;
                #region set data vul=0;
                Operating_Year Operating_Year = new Operating_Year();
                Operating_Year.Activity = new List<Operating_Activity>();
                Operating_Year.Qtr = new List<Operating_Qtr>();
                foreach (var item in AcricityList)
                {
                    Operating_Activity Operating_Activity = new Operating_Activity();
                    Operating_Activity.Qtr = new List<Operating_Qtr>();
                    var MT_VESSEL_ACTIVITY = (from M_VESSEL_ACTIVITY in context.MT_VESSEL_ACTIVITY
                                              join M_VESSEL_ACTIVITY_C in context.MT_VESSEL_ACTIVITY_CONTROL on M_VESSEL_ACTIVITY.MVA_ROW_ID equals M_VESSEL_ACTIVITY_C.MAC_FK_VESSEL_ACTIVITY
                                              where M_VESSEL_ACTIVITY.MVA_ROW_ID == item.Key
                                              select new
                                              {
                                                  M_VESSEL_ACTIVITY.MVA_ROW_ID,
                                                  M_VESSEL_ACTIVITY.MVA_CODE,
                                                  M_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN,
                                                  M_VESSEL_ACTIVITY_C.MAC_COLOR_CODE
                                              }).FirstOrDefault();
                    Operating_Activity.Activity_id = MT_VESSEL_ACTIVITY.MVA_ROW_ID;
                    Operating_Activity.Activity_Name = MT_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN;
                    Operating_Year.Year = Search.sYearlyComparsion1 + " - " + Search.sYearlyComparsion2;
                    for (int i = 0; i <= ListYear; i++)
                    {
                        var year_count = Convert.ToInt16(Search.sYearlyComparsion1) + i;
                        Operating_Qtr qtrs_g = new Operating_Qtr();
                        qtrs_g.Qtr_Name_Year = year_count.ToString();
                        qtrs_g.Qtr_Vul = 0;
                        Operating_Activity.Qtr.Add(qtrs_g);
                    }

                    Operating_Year.Activity.Add(Operating_Activity);

                }
                for (int i = 0; i <= ListYear; i++)
                {
                    var year_count = Convert.ToInt16(Search.sYearlyComparsion1) + i;
                    Operating_Qtr Operating_Qtr = new Operating_Qtr();
                    var dayofyear = new DateTime(year_count, 12, 31);
                    Operating_Qtr.Qtr_Name_Year = year_count.ToString();
                    Operating_Qtr.Qtr_Vul = dayofyear.DayOfYear;
                    Operating_Year.Qtr.Add(Operating_Qtr);
                }

                Vessel.Operating_Year.Add(Operating_Year);

                #endregion
                //set vul
                for (int y = 0; y <= ListYear; y++)
                {
                    sYearlyComparsion1_dt1 = new DateTime(Convert.ToInt16(Search.sYearlyComparsion1) + y, 1, 1);
                    sYearlyComparsion2_dt2 = new DateTime(Convert.ToInt16(Search.sYearlyComparsion1) + y, 12, 12);
                    var setCPAI_VESSEL_SCH_S = CPAI_VESSEL_SCH_S.Where(w => w.voyage_s >= sYearlyComparsion1_dt1 && w.voyage_e <= sYearlyComparsion2_dt2).ToList();
                    var year_count = Convert.ToInt16(Search.sYearlyComparsion1) + y;

                    // setActivity.Qtr_Vul +=
                    foreach (var VASS_FK_VESSEL_ACTIVITY in setCPAI_VESSEL_SCH_S.GroupBy(g => g.VASS_FK_VESSEL_ACTIVITY))
                    {
                        var MT_VESSEL_ACTIVITY = (from M_VESSEL_ACTIVITY in context.MT_VESSEL_ACTIVITY
                                                  join M_VESSEL_ACTIVITY_C in context.MT_VESSEL_ACTIVITY_CONTROL on M_VESSEL_ACTIVITY.MVA_ROW_ID equals M_VESSEL_ACTIVITY_C.MAC_FK_VESSEL_ACTIVITY
                                                  where M_VESSEL_ACTIVITY.MVA_ROW_ID == VASS_FK_VESSEL_ACTIVITY.Key
                                                  select new
                                                  {
                                                      M_VESSEL_ACTIVITY.MVA_ROW_ID,
                                                      M_VESSEL_ACTIVITY.MVA_CODE,
                                                      M_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN,
                                                      M_VESSEL_ACTIVITY_C.MAC_COLOR_CODE
                                                  }).FirstOrDefault();
                        var Vessel_year = Vessel.Operating_Year.FirstOrDefault();
                        var getActivity_year = Vessel_year.Activity.Where(w => w.Activity_id.Contains(MT_VESSEL_ACTIVITY.MVA_ROW_ID)).FirstOrDefault();
                        var get = getActivity_year.Qtr.Where(w => w.Qtr_Name_Year.Contains(year_count.ToString())).FirstOrDefault();
                        foreach (var item in VASS_FK_VESSEL_ACTIVITY)
                        {
                            decimal timeOfday = 0;
                            TimeSpan times = new TimeSpan();
                            if(item.activity_e!= null)
                            {
                                timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_e.Value.TimeOfDay.Hours);
                                times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                                var date_vul = ((item.activity_e.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                get.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                            } else
                            {
                                timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_s.Value.TimeOfDay.Hours);
                                times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                                var date_vul = ((item.activity_e.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                                get.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                            }
                        }
                    }
                }
                return Vessel;
            }

        }
        public Vessel GenExcel_MonthtoMonth(Search Search)
        {
            var ListYear = 0;
            var typeSearch = ""; var Month_Count = 0; var ECFNE = "";//ColumnFromNumber
            var MonthList = DropdownServiceModel.getMonth(false, "test", true, true);
            Vessel Vessel = new Vessel();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var MT_VEHICLE = context.MT_VEHICLE.Where(w => w.VEH_ID == Search.sVessel).Select(s => new { s.VEH_ID, s.VEH_VEHICLE }).FirstOrDefault();
                var CPAI_VESSEL_SCH_S = (from cvs_s in context.CPAI_VESSEL_SCH_S
                                         join cvs_s_a in context.CPAI_VESSEL_SCH_S_ACTIVITYS on cvs_s.VSDS_ROW_ID equals cvs_s_a.VASS_FK_VESSEL_SCH_S
                                         where cvs_s.VSDS_FK_VEHICLE == MT_VEHICLE.VEH_ID && cvs_s.VSDS_STATUS == "SUBMIT"
                                         orderby cvs_s.VSDS_DATE_START
                                         select new
                                         {
                                             cvs_s.VSDS_ROW_ID,
                                             voyage_s = cvs_s.VSDS_DATE_START,
                                             voyage_e = cvs_s.VSDS_DATE_END,
                                             activity_s = cvs_s_a.VASS_DATE_START,
                                             activity_e = cvs_s_a.VASS_DATE_END,
                                             tc = cvs_s.VSDS_TC,
                                             cvs_s.VSDS_FK_VEHICLE,
                                             cvs_s_a.VASS_FK_VESSEL_ACTIVITY
                                         }).ToList();

                if (Search.sYearly != null)
                {
                    ListYear = 1;
                    typeSearch = "Yearly";
                    Month_Count = DateTime.Now.Month - 1;
                    if (Month_Count == 0)
                    {
                        Month_Count = 12;
                    }
                    var dayofmonth = DateTime.DaysInMonth(Convert.ToInt16(Search.sYearly), Month_Count);
                    DateTime dt1 = new DateTime(Convert.ToInt16(Search.sYearly), 1, 1);
                    DateTime dt2 = new DateTime(Convert.ToInt16(Search.sYearly), Month_Count, dayofmonth);
                    CPAI_VESSEL_SCH_S = CPAI_VESSEL_SCH_S.Where(w => w.voyage_s >= dt1 && w.voyage_e <= dt2).ToList();
                }
                else if (Search.sMonthlyFrom != null && Search.sMonthlyTo != null && Search.sYearlyFrom != null && Search.sYearlyTo != null)
                {
                    ListYear = 1;
                    typeSearch = "MonthToMonth";
                    if (Search.sYearlyFrom != Search.sYearlyTo)
                    {
                        Month_Count = 12 + Convert.ToInt16(Search.sMonthlyTo);
                    }
                    else
                    {
                        typeSearch = "Yearly";
                        Month_Count = Convert.ToInt16(Search.sMonthlyFrom) + Convert.ToInt16(Search.sMonthlyTo);
                    }
                    DateTime sYearlyFrom_dt1 = new DateTime(Convert.ToInt16(Search.sYearlyFrom), Convert.ToInt16(Search.sMonthlyFrom), 1);
                    var d = DateTime.DaysInMonth(Convert.ToInt16(Search.sYearlyTo), Convert.ToInt16(Search.sMonthlyTo));
                    DateTime sYearlyTo_dt2 = new DateTime(Convert.ToInt16(Search.sYearlyTo), Convert.ToInt16(Search.sMonthlyTo), d);
                    CPAI_VESSEL_SCH_S = CPAI_VESSEL_SCH_S.Where(w => w.voyage_s >= sYearlyFrom_dt1 && w.voyage_e <= sYearlyTo_dt2).ToList();
                }
                Vessel.Vessel_Name = MT_VEHICLE.VEH_VEHICLE;
                Vessel.Operating_Year = new List<Operating_Year>();
                Operating_Year Operating_Years = new Operating_Year();
                Operating_Years.Activity = new List<Operating_Activity>();
                foreach (var VASS_FK_VESSEL_ACTIVITY in CPAI_VESSEL_SCH_S.GroupBy(g => g.VASS_FK_VESSEL_ACTIVITY))
                {
                    var MT_VESSEL_ACTIVITY = (from M_VESSEL_ACTIVITY in context.MT_VESSEL_ACTIVITY
                                              join M_VESSEL_ACTIVITY_C in context.MT_VESSEL_ACTIVITY_CONTROL on M_VESSEL_ACTIVITY.MVA_ROW_ID equals M_VESSEL_ACTIVITY_C.MAC_FK_VESSEL_ACTIVITY
                                              where M_VESSEL_ACTIVITY.MVA_ROW_ID == VASS_FK_VESSEL_ACTIVITY.Key
                                              select new
                                              {
                                                  M_VESSEL_ACTIVITY.MVA_ROW_ID,
                                                  M_VESSEL_ACTIVITY.MVA_CODE,
                                                  M_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN,
                                                  M_VESSEL_ACTIVITY_C.MAC_COLOR_CODE
                                              }).FirstOrDefault();
                    Operating_Activity Operating_Activity = new Operating_Activity();
                    Operating_Activity.Qtr = new List<Operating_Qtr>();
                    foreach (var item in VASS_FK_VESSEL_ACTIVITY)
                    {
                        Operating_Years.Year = item.activity_s.Value.Year.ToString();
                        Operating_Activity.Activity_Name = MT_VESSEL_ACTIVITY.MVA_STANDARD_PHRASE_EN;
                        Operating_Qtr qtr = new Operating_Qtr();
                        if (Operating_Activity.Qtr.Count == 0)
                        {
                            if (typeSearch.Equals("Yearly"))
                            {
                                for (int i = 1; i <= Month_Count; i++)
                                {
                                    Operating_Qtr qtrs_g = new Operating_Qtr();
                                    var cc = MonthList.Where(w => w.Value == i.ToString("0#")).FirstOrDefault();
                                    qtrs_g.Qtr_Name_Year = cc.Text + " " + Search.sYearly;
                                    qtrs_g.Qtr_Vul = 0;
                                    Operating_Activity.Qtr.Add(qtrs_g);
                                }
                            }
                            else if (typeSearch.Equals("MonthToMonth"))
                            {
                                for (int i = 1; i <= Month_Count; i++)
                                {
                                    if (i <= 12)
                                    {
                                        Operating_Qtr qtrs_g = new Operating_Qtr();
                                        var cc = MonthList.Where(w => w.Value == i.ToString("0#")).FirstOrDefault();

                                        qtrs_g.Qtr_Name_Year = cc.Text + " " + Search.sYearlyFrom;
                                        qtrs_g.Qtr_Vul = 0;
                                        Operating_Activity.Qtr.Add(qtrs_g);
                                    }
                                    else
                                    {
                                        var m = i - 12;
                                        Operating_Qtr qtrs_g = new Operating_Qtr();
                                        var cc = MonthList.Where(w => w.Value == m.ToString("0#")).FirstOrDefault();
                                        qtrs_g.Qtr_Name_Year = cc.Text + " " + Search.sYearlyTo;
                                        qtrs_g.Qtr_Vul = 0;
                                        Operating_Activity.Qtr.Add(qtrs_g);
                                    }

                                }
                            }
                        }
                        var Month = MonthList.Where(w => w.Value == item.activity_s.Value.Month.ToString("0#")).FirstOrDefault();
                        if(item.activity_e != null)
                        {
                            qtr.Qtr_Name_Year = Month.Text + " " + item.activity_e.Value.Year;
                        }
                        else
                        {
                            qtr.Qtr_Name_Year = Month.Text + " " + item.activity_s.Value.Year;
                        }
                        
                        var setActivity = Operating_Activity.Qtr.Where(w => w.Qtr_Name_Year.Contains(qtr.Qtr_Name_Year)).FirstOrDefault();//set data in Operating_Activity
                        decimal timeOfday = 0;
                        TimeSpan times = new TimeSpan();
                        if(item.activity_e != null)
                        {
                            timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_e.Value.TimeOfDay.Hours);
                        }else
                        {
                            timeOfday = ((24 - item.activity_s.Value.TimeOfDay.Hours) + item.activity_s.Value.TimeOfDay.Hours);
                        }
                        
                        times = TimeSpan.FromHours(Convert.ToDouble(timeOfday));
                        if(item.activity_e != null)
                        {
                            var date_vul = ((item.activity_e.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                            setActivity.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                        }
                        else
                        {
                            var date_vul = ((item.activity_s.Value.DayOfYear) - item.activity_s.Value.DayOfYear) + Convert.ToDecimal(times.TotalDays);
                            setActivity.Qtr_Vul += Convert.ToDecimal(date_vul.ToString("#.##"));
                        }
                    }
                    Operating_Years.Activity.Add(Operating_Activity);
                }
                //hear
                Operating_Year Operating_Year = new Operating_Year();
                Operating_Year.Qtr = new System.Collections.Generic.List<Operating_Qtr>();
                for (int i = 1; i <= Month_Count; i++)
                {
                    if (typeSearch.Equals("Yearly"))
                    {
                        Operating_Qtr Operating_Qtr = new Operating_Qtr();
                        var cc = MonthList.Where(w => w.Value == i.ToString("0#")).FirstOrDefault();
                        var dayofmonth = DateTime.DaysInMonth(Convert.ToInt16(Search.sYearly), Convert.ToInt16(cc.Value));
                        Operating_Qtr.Qtr_Name_Year = cc.Text + " " + Search.sYearly;
                        Operating_Qtr.Qtr_Vul = dayofmonth;
                        Operating_Year.Qtr.Add(Operating_Qtr);
                    }
                    else if (typeSearch.Equals("MonthToMonth"))
                    {
                        if (i <= 12)
                        {
                            Operating_Qtr Operating_Qtr = new Operating_Qtr();
                            var cc = MonthList.Where(w => w.Value == i.ToString("0#")).FirstOrDefault();
                            var dayofmonth = DateTime.DaysInMonth(Convert.ToInt16(Search.sYearlyFrom), Convert.ToInt16(cc.Value));
                            Operating_Qtr.Qtr_Name_Year = cc.Text + " " + Search.sYearlyFrom;
                            Operating_Qtr.Qtr_Vul = dayofmonth;
                            Operating_Year.Qtr.Add(Operating_Qtr);
                        }
                        else
                        {
                            var m = i - 12;
                            Operating_Qtr Operating_Qtr = new Operating_Qtr();
                            var cc = MonthList.Where(w => w.Value == m.ToString("0#")).FirstOrDefault();
                            var dayofmonth = DateTime.DaysInMonth(Convert.ToInt16(Search.sYearlyTo), Convert.ToInt16(cc.Value));
                            Operating_Qtr.Qtr_Name_Year = cc.Text + " " + Search.sYearlyTo;
                            Operating_Qtr.Qtr_Vul = dayofmonth;
                            Operating_Year.Qtr.Add(Operating_Qtr);
                        }
                    }
                }
                Operating_Years.Qtr = Operating_Year.Qtr;
                Vessel.Operating_Year.Add(Operating_Years);
            }
            return Vessel;
        }
        public void GenExcel(Search Search)
        {
            var Month_Count = 0; var ECFNE = "";//ColumnFromNumber
            var MonthList = DropdownServiceModel.getMonth(false, "test", true, true);
            Vessel Vessel = new Vessel();
            #region get_vessels
            if (Search.sYearly != null || Search.sYearlyFrom != null)
            {
                Vessel = GenExcel_MonthtoMonth(Search);
                Month_Count = Vessel.Operating_Year.FirstOrDefault().Qtr.Count();
            }
            else if (Search.sYearlyComparsion1 != null)
            {
                Vessel = GenExcel_YearToYear(Search);
                Month_Count = Vessel.Operating_Year.FirstOrDefault().Qtr.Count();
            }
            else if (Search.VoyageFrom != null)
            {
                Vessel = GenExcel_Tc(Search);
                Month_Count = Vessel.Operating_Year.FirstOrDefault().Qtr.Count();
            }
            #endregion
            string file_name = string.Format("SchedulerCMCS_{0}_{1}.xlsx", Vessel.Vessel_Name, DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_namePdf = string.Format("SchedulerCMCS_{0}_{1}.xlsx", Vessel.Vessel_Name, DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "FREIGHT", file_name);
            FileInfo excelFile = new FileInfo(file_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {

                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Vessel_Utilization");
                ws.DefaultRowHeight = 18;
                ws.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //ws.Column(1).Width = 30;
                //ws.Column(2).Width = 24;
                ws.Row(1).Height = 30;
                ws.Row(2).Height = 18;

                //row 1
                ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Tahoma", 10));
                ws.Cells[1, 1, 1, Month_Count + 3].Merge = true;
                ws.Cells[1, 1, 1, Month_Count + 3].Value = "Vessel Utilization";
                ws.Cells[1, 1, 1, Month_Count + 3].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, Month_Count + 3].Style.Font.Italic = true;
                ws.Cells[1, 1, 1, Month_Count + 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells[1, 1, 1, Month_Count + 3].Style.Font.Size = 16;
                //row 2
                ws.Cells[2, 1, 2, Month_Count + 3].Merge = true;
                ws.Cells[2, 1, 2, Month_Count + 3].Value = Vessel.Vessel_Name;
                ws.Cells[2, 1, 2, Month_Count + 3].Style.Font.Bold = true;
                ws.Cells[2, 1, 2, Month_Count + 3].Style.Font.Italic = true;
                ws.Cells[2, 1, 2, Month_Count + 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells[2, 1, 2, Month_Count + 3].Style.Font.Size = 16;
                //3
                if (Vessel.Operating_Year.Count == 1)
                {
                    ws.Column(2).Width = 30;
                    for (int i = 1; i <= Month_Count; i++)
                    {
                        ws.Column(i + 3).Width = 24;
                    }
                    ECFNE = ExcelColumnFromNumber(Month_Count + 3);
                    ws.Cells["C4"].Formula = "=SUM(D4:" + ECFNE + "4)";
                    var count = 0;
                    foreach (var item in Vessel.Operating_Year)
                    {
                        ws.Cells[3, 3].Value = item.Year;
                        if (Search.VoyageFrom != null)
                        {
                            ws.Cells[3, 3].Value = "Tc " + Search.VoyageFrom + " - " + Search.VoyageTo;
                        }
                        else if (Search.sYearlyComparsion1 != null)
                        {
                            ws.Cells[3, 3].Value = "Year " + Search.sYearlyComparsion1 + " - " + Search.sYearlyComparsion2;
                        }
                        else if (Search.sMonthlyFrom != null)
                        {
                            ws.Cells[3, 3].Value = Search.sMonthlyFrom + " " + Search.sYearlyFrom + " - " + Search.sMonthlyTo + " " + Search.sYearlyTo;
                        }

                        var i = 0;
                        foreach (var Qtr in item.Qtr)
                        {
                            ws.Cells[3, 4 + i].Value = Qtr.Qtr_Name_Year;//3=c3
                            ws.Cells[4, 4 + i].Value = Qtr.Qtr_Vul;//4=c4
                            i++;
                        }
                        ws.Cells[4, 2].Value = "Operating Days";//
                        var j = 0;
                        foreach (var Activity in item.Activity)
                        {
                            ws.Cells[6 + j, 1].Value = j + 1;
                            ws.Cells[6 + j, 2].Value = Activity.Activity_Name;
                            ECFNE = ExcelColumnFromNumber(Month_Count + 3);

                            ws.Cells[6 + j, 3].Formula = "=SUM(D" + (6 + j) + ":" + ECFNE + (6 + j) + ")";
                            var k = 0;
                            foreach (var Qtr in Activity.Qtr)
                            {
                                ws.Cells[6 + j, 4 + k].Value = Qtr.Qtr_Name_Year;//3=c3
                                ws.Cells[6 + j, 4 + k].Value = Qtr.Qtr_Vul;//4=c4
                                k++;

                            }
                            j++;
                            count++;
                        }
                    }
                    //  + 3;
                    ws.Cells["A" + (5 + count + 1) + ":" + "B" + (5 + count + 1)].Merge = true;//5=rowที่5
                    ws.Cells["A" + (5 + count + 1) + ":" + "B" + (5 + count + 1)].Value = "Total Operating Days";
                    for (int i = 0; i <= Month_Count; i++)
                    {
                        ECFNE = ExcelColumnFromNumber(i + 3);
                        ws.Cells[(5 + count + 1), i + 3].Formula = "=SUM(" + ECFNE + "6:" + ECFNE + (count + 5) + ")";
                    }


                    ws.Cells["A" + (5 + count + 2) + ":" + "B" + (5 + count + 2)].Merge = true;
                    ws.Cells["A" + (5 + count + 2) + ":" + "B" + (5 + count + 2)].Value = "Idle Times";
                    for (int i = 0; i <= Month_Count; i++)
                    {
                        ECFNE = ExcelColumnFromNumber(i + 3);
                        var text = "=(" + ECFNE + "4-" + ECFNE + (count + 5 + 1) + ")";
                        ws.Cells[(5 + count + 2), i + 3].Formula = text;
                    }
                    ws.Cells["A" + (5 + count + 3) + ":" + "B" + (5 + count + 3)].Merge = true;
                    ws.Cells["A" + (5 + count + 3) + ":" + "B" + (5 + count + 3)].Value = "Vessel Utilization (%)";
                    for (int i = 0; i <= Month_Count; i++)
                    {
                        ECFNE = ExcelColumnFromNumber(i + 3);
                        var text = "=SUM(FIXED(((" + ECFNE + (count + 5 + 1) + "*100)/" + ECFNE + "4),2))";
                        ws.Cells[(5 + count + 3), i + 3].Formula = text; 
                    }
                    TempData["Total"] = count;
                }
                else
                {
                    // vs
                    var c = Vessel.Operating_Year.Count();
                    ws.Cells[3, 3].Value = Vessel.Operating_Year[0].Year + " - " + Vessel.Operating_Year[c - 1].Year;
                    for (int i = 0; i < Vessel.Operating_Year.Count(); i++)
                    {
                        var frequency = 0;//ความถี่ของข้อมูล
                        for (int j = 0; j < 4; j++)
                        {
                            for (int k = 0; k < c; k++)
                            {
                                ws.Cells[3, 4 + j + k + frequency].Value = Vessel.Operating_Year[k].Qtr[j].Qtr_Name_Year;//3=c3
                                ws.Cells[4, 4 + j + k + frequency].Value = Vessel.Operating_Year[k].Qtr[j].Qtr_Vul;//4=c4
                            }
                            frequency += c - 1;
                        }
                        i++;
                    }
                }

                int count2 = Convert.ToInt16(TempData["Total"].ToString());
                //chart part
                ExcelWorksheet ws2 = package.Workbook.Worksheets.Add("Data");
                ws.Column(1).Width = 24;
                ws.Column(2).Width = 18;
                ws.Column(3).Width = 18;
                ws.Column(4).Width = 18;
                ws.Column(5).Width = 18;
                ws2.Cells[1, 2].Value = "Days";
                ws2.Cells[1, 3].Value = "Operating Days";
                ws2.Cells[1, 4].Value = "Utilization (%)";
                ws2.Cells[1, 5].Value = "Idle Time (Days)";
                if (Search.sYearly != null)
                {
                    ws2.Cells[2, 1].Value = "Year " + Search.sYearly;
                }
                else if (Search.sYearlyFrom != null)
                {
                    ws2.Cells[2, 1].Value = "Year " + Search.sYearlyFrom + " - " + Search.sYearlyTo;
                }
                else if (Search.VoyageFrom != null)
                {
                    ws2.Cells[2, 1].Value = "Tc " + Search.VoyageFrom + " - " + Search.VoyageTo;
                }
                else
                {
                    ws2.Cells[2, 1].Value = "Year " + Search.sYearlyComparsion1 + " - " + Search.sYearlyComparsion2;
                }
                var column_ws2 = 2;//because i=1 if i=0 conlumn_ws2=3  //+1 total year column
                for (int i = 1; i <= Month_Count; i++)
                {
                    if (Search.sYearlyFrom != null)
                    {
                        Search.sYearly = Search.sYearlyFrom;
                    }
                    else if (Search.sYearlyComparsion1 != null)
                    {
                        ws2.Cells[i + column_ws2, 1].Value = Convert.ToInt32(Search.sYearlyComparsion1) - 1 + i;
                    }
                    else if (Search.VoyageFrom != null)
                    {
                        ws2.Cells[i + column_ws2, 1].Value = Vessel.Operating_Year[0].Qtr[i - 1].Qtr_Name_Year;
                    }
                    else
                    {
                        if (i <= 12)
                        {
                            var cc = MonthList.Where(w => w.Value == i.ToString("0#")).FirstOrDefault();
                            ws2.Cells[i + column_ws2, 1].Value = cc.Text + " " + Search.sYearly;
                        }
                        else
                        {
                            var m = i - 12;
                            var cc = MonthList.Where(w => w.Value == m.ToString("0#")).FirstOrDefault();
                            ws2.Cells[i + column_ws2, 1].Value = cc.Text + " " + Search.sYearlyTo;
                        }
                    }


                }
                ws2.Cells[2, 2].Formula = "=Vessel_Utilization!C4";//Year

                for (int i = 1; i <= Month_Count; i++)//Month
                {
                    ECFNE = ExcelColumnFromNumber(i + 3);
                    ws2.Cells[i + column_ws2, 2].Formula = "=Vessel_Utilization!" + ECFNE + 4;
                }
                ws2.Cells[2, 3].Formula = "=Vessel_Utilization!C" + (count2 + 5 + 1);//Operating Days
                for (int i = 1; i <= Month_Count; i++)//Operating Days
                {
                    ECFNE = ExcelColumnFromNumber(i + 3);
                    ws2.Cells[i + column_ws2, 3].Formula = "=Vessel_Utilization!" + ECFNE + (count2 + 5 + 1);
                }
                ws2.Cells[2, 4].Formula = "=Vessel_Utilization!C" + (count2 + 5 + 3);//Utilization
                ws2.Cells[2, 4].Style.Numberformat.Format = "#0\\.00%";
                for (int i = 1; i <= Month_Count; i++)//Utilization
                {
                    ECFNE = ExcelColumnFromNumber(i + 3);
                    ws2.Cells[i + column_ws2, 4].Formula = "=Vessel_Utilization!" + ECFNE + +(count2 + 5 + 3);
                    ws2.Cells[i + column_ws2, 4].Style.Numberformat.Format = "#0\\.00%";
                }
                ws2.Cells[2, 5].Formula = "=Vessel_Utilization!C" + (count2 + 5 + 2);//Idle Time 
                for (int i = 1; i <= Month_Count; i++)//Idle Time 
                {
                    ECFNE = ExcelColumnFromNumber(i + 3);
                    ws2.Cells[i + column_ws2, 5].Formula = "=Vessel_Utilization!" + ECFNE + +(count2 + 5 + 2);
                }

                var chart = ws.Drawings.AddChart("chart", eChartType.ColumnClustered);
                chart.SetSize(750, 350);
                chart.Legend.Position = OfficeOpenXml.Drawing.Chart.eLegendPosition.Bottom;
                chart.SetPosition(((count2+5) * 50), 15);
                chart.Title.Text = "Vessel Utilization";
                chart.Title.Font.Color = Color.White;
                chart.XAxis.Font.Color = Color.White;
                chart.YAxis.Font.Color = Color.White;
                //var chartType3 = chart.PlotArea.ChartTypes.Add(eChartType.Line);
                //var serie1 = chart.Series.Add(ws2.Cells["B2:B6"], ws2.Cells["A2:A6"]);
                //var serie2 = chart.Series.Add(ws2.Cells["C2:C6"], ws2.Cells["A2:A6"]);
                var serie3 = chart.Series.Add(ws2.Cells["D2:D" + (Month_Count + 2)], ws2.Cells["A2:A" + (Month_Count + 2)]);
                //serie1.Header = "Day";1
                //serie2.Header = "Operating Days";
                serie3.Header = "Utilization (%)";
                // ((OfficeOpenXml.Drawing.Chart.ExcelChartDataLabel)((OfficeOpenXml.Drawing.Chart.ExcelBarChartSerie)serie3).DataLabel).Font.Color = Color.White;

                ///set color grah
                chart.Fill.Color = Color.FromArgb(59, 56, 56);
                chart.Fill.Style = eFillStyle.SolidFill;
                //chart.Fill.Transparancy = 50;
                chart.Legend.Font.Color = Color.White;
                chart.PlotArea.Fill.Style = eFillStyle.SolidFill;
                chart.PlotArea.Fill.Color = Color.FromArgb(59, 56, 56);

                //    chart.PlotArea.Fill.Color = Color.White;
                ((OfficeOpenXml.Drawing.Chart.ExcelBarChart)chart).DataLabel.Font.Color = Color.White;

                chart.Style = eChartStyle.Style26;
                ((OfficeOpenXml.Drawing.Chart.ExcelBarChart)chart).DataLabel.ShowValue = true;
                //  ((OfficeOpenXml.Drawing.Chart.ExcelLineChartSerie)serie3).DataLabel.ShowValue = true;
                //end chart part

                ws.PrinterSettings.Orientation = eOrientation.Landscape;
                ws.PrinterSettings.PaperSize = ePaperSize.A4;
                ws.PrinterSettings.RepeatRows = new ExcelAddress("$1:$4");
                ws.PrinterSettings.FitToPage = true;
                package.Save();
            }
            //if (data == "Pdf")
            //{

            //    //  ReturnValue rtn = SaveAsPdf(file_path);
            //    Response.Redirect(string.Format("~/Web/FileUpload/FREIGHT/{0}", file_namePdf));

            //}
            //else // EXCEL
            //{
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
            Response.AddHeader("content-type", "application/Excel");
            Response.WriteFile(file_path);
            Response.End();
            //}

        }
        private void AllBorders(OfficeOpenXml.Style.Border _borders)
        {
            _borders.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        }
        #endregion
        public ActionResult VesselUtilizationReport()
        {
            Vessel_Utilization_Report vur = new Vessel_Utilization_Report();
            var yeardefault = DropdownServiceModel.getYearCrop(false, "Test", "2000", 0, 18).OrderByDescending(o => o.Value).ToList();

            vur.Vessel = DropdownServiceModel.getVehicle("SCH_C");
            vur.Yearly = yeardefault;
            vur.MonthlyFrom = DropdownServiceModel.getMonth(false, "test", false, true);
            vur.YearlyFrom = yeardefault;
            vur.MonthlyTo = DropdownServiceModel.getMonth(false, "test", false, true);
            vur.YearlyTo = yeardefault;
            vur.YearlyComparsion1 = yeardefault;
            vur.YearlyComparsion2 = yeardefault;
            return View(vur);
        }
        private static string ExcelColumnFromNumber(int column)
        {//https://stackoverflow.com/questions/1951517/convert-a-to-1-b-to-2-z-to-26-and-then-aa-to-27-ab-to-28-column-indexes-to
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }
    }
}
