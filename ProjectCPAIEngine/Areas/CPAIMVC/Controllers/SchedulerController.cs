﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALMaster;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.DAL.DALCharter;
using System.Globalization;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System.IO;
using OfficeOpenXml;
using System.Drawing;
using DevExpress.Spreadsheet;
using ProjectCPAIEngine.DAL.DALVessel;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;
using System.Text.RegularExpressions;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class SchedulerController : BaseController
    {
        #region ---- Initialize ------

        public List<ButtonAction> ButtonListScheduler
        {
            get
            {
                if (Session["ButtonListScheduler"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListScheduler"];
            }
            set { Session["ButtonListScheduler"] = value; }
        }

        ShareFn _FN = new ShareFn();                            // New Object Share Function
        const string returnPage = "../web/MainBoards.aspx";

        string vendorColorDefualt = COLOR_DEFUALT;

        // get Data For Dropdownlist
        private void initializeDropDownList(ref SchedulerViewModel model, string pType)
        {
            model.vessel = DropdownServiceModel.getVehicle(pType == ConstantPrm.SYSTEMTYPE.CRUDE ? "SCH_C" : "SCH_P");
            model.customer = DropdownServiceModel.getCustomerColor(pType == ConstantPrm.SYSTEMTYPE.CRUDE ? "VMCUSCS" : "CHR_FRE", "I");
            model.cargo_list = DropdownServiceModel.getMaterial(false, "", "MAT_CMMT");
            model.cargo_unit = getCargoUnit();
            model.freight = DropdownServiceModel.getFreight();
            model.portName = DropdownServiceModel.getPortName(false, "", pType == ConstantPrm.SYSTEMTYPE.CRUDE ? "PORT_CMCS" : "PORT_CMMT");
            model.ddl_Color = DropdownServiceModel.getColorPicker();
            model.scheduleType = new List<SelectListItem>();
            model.scheduleType.Add(new SelectListItem { Text = "Schedule", Value = "Schedule" });
            model.scheduleType.Add(new SelectListItem { Text = "Tank Cleaning", Value = "Tank Cleaning" });
            model.scheduleType.Add(new SelectListItem { Text = "Maintenance", Value = "Maintenance" });
            model.scheduleType.Add(new SelectListItem { Text = "Off-Hire", Value = "Off-Hire" });
            model.scheduleType.Add(new SelectListItem { Text = "Others", Value = "Others" });

            model.charterer_list = DropdownServiceModel.getCustomer("CHR_FRE");
            model.schd_vendorColorDefualt = COLOR_DEFUALT;
            //model.schd_dayLaycan = "Day";
            //model.schd_daySailing = "Day";

            if (model.sch_detail != null && !string.IsNullOrEmpty(model.sch_detail.vendor))
            {
                string vendor = model.sch_detail.vendor;
                model.sch_detail.vendor_color = model.customer.Where(x => x.Value.Contains(vendor)).FirstOrDefault() != null ? model.customer.Where(x => x.Value.Contains(vendor)).FirstOrDefault().Value.Split('|')[0] : null;
                model.schd_vendorColorValue = model.sch_detail.vendor_color + "|" + model.sch_detail.vendor;
            }

            model.vesselActivity = DropdownServiceModel.getVesselActivity(pType, true, "--- Please Select ---");
            model.activityType = DropdownServiceModel.getVesselActivityList("CMMT");
        }

        private void BindButtonPermission(List<ButtonAction> Button)
        {
            if (Request.QueryString["isDup"] == null)
            {
                ViewBag.ButtonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    ViewBag.PageMode = "READ";
                }
                foreach (ButtonAction _button in Button)
                {
                    if(_button.name == "EDIT ACTIVITY")
                    {
                        _button.name = "SAVE ACTIVITY";
                    }
                    if(_button.name == "EDIT SCHEDULE")
                    {
                        _button.name = "SAVE SCHEDULE";
                    }
                    string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                    ViewBag.ButtonCode += _btn;
                }
                ViewBag.ButtonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListScheduler = Button;
            }
            else
            {
                ViewBag.ButtonMode = "AUTO";
                ViewBag.PageMode = "EDIT";
            }
        }

        private void updateModel(FormCollection form, ref SchedulerViewModel model, bool isTextMode = false)
        {
            //Assign vendor_color
            if (!string.IsNullOrEmpty(model.schd_vendorColorValue))
            {
                var vendor = model.schd_vendorColorValue.Split('|');

                model.sch_detail.vendor_color = vendor[0];
                model.sch_detail.vendor = vendor[1];
            }
            else
            {
                model.sch_detail.vendor = "";
                model.sch_detail.vendor_color = "";
            }

            if (!string.IsNullOrEmpty(model.schd_tempDate))
            {
                string[] date = model.schd_tempDate.Split(' ');
                model.sch_detail.date_start = date[0];
                model.sch_detail.date_end = date[2];
            }

            if (model.sch_detail.schedule_type == "Schedule")
            {
                //Laycan
                if (!string.IsNullOrEmpty(model.schd_tempLaycan))
                {
                    string[] date = model.schd_tempLaycan.Split(' ');
                    model.sch_detail.laycan_from = date[0];
                    model.sch_detail.laycan_to = date[2];
                }

                // Set Index order cargo
                for (int i = 0; i < model.sch_detail.sch_cargo.Count; i++)
                {
                    model.sch_detail.sch_cargo[i].order = (i + 1) + "";
                }

                // Set Index order load port
                for (int i = 0; i < model.sch_detail.sch_load_ports.Count; i++)
                {
                    model.sch_detail.sch_load_ports[i].order = (i + 1) + "";
                }

                // Set Index order dis port
                for (int i = 0; i < model.sch_detail.sch_dis_ports.Count; i++)
                {
                    model.sch_detail.sch_dis_ports[i].order = (i + 1) + "";
                }

                //Assign vendor_color
                if (!string.IsNullOrEmpty(model.schd_vendorColorValue))
                {
                    var vendor = model.schd_vendorColorValue.Split('|');

                    model.sch_detail.vendor_color = vendor[0];
                    model.sch_detail.vendor = vendor[1];
                }
                else
                {
                    model.sch_detail.vendor = "";
                    model.sch_detail.vendor_color = "";
                }
            }
            else
            {
                if (model.sch_detail.schedule_type == "Tank Cleaning")
                {
                    model.sch_detail.color = "#ffff00";
                }
                if (model.sch_detail.schedule_type == "Maintenance")
                {
                    model.sch_detail.color = "#ff0000";
                }
                if (model.sch_detail.schedule_type == "Off-Hire")
                {
                    model.sch_detail.color = "#000000";
                }
                if (string.IsNullOrEmpty(model.sch_detail.color))
                {
                    model.sch_detail.color = "#8f8f8f";
                }
            }

            //Assign note
            if (model.sch_detail.note == null)
            {
                model.sch_detail.note = "";
            }
        }

        #endregion

        #region ----  Controller Action  ------

        // GET: CPAIMVC/Scheduler/Search
        public ActionResult Search(string Type = null, string StartDate = null, string EndDate = null, string MultiVesselID = null)
        {
            Session.Remove("PageAction");
            string sessTypeEncrypt = "";

            if (Type != null && sessTypeEncrypt == "")
                Session["Schedule_Type"] = Type;

            if (Session["Schedule_Type"] != null)
                sessTypeEncrypt = Convert.ToString(Session["Schedule_Type"]);

            // Get parameter from URL QueryString
            string pType = String.IsNullOrEmpty(Type) ? sessTypeEncrypt.Decrypt() : Type.Decrypt();
            string pStartDate = String.IsNullOrEmpty(StartDate) ? null : StartDate;
            string pEndDate = String.IsNullOrEmpty(EndDate) ? null : EndDate;
            string pMultiVesselID = String.IsNullOrEmpty(MultiVesselID) ? null : MultiVesselID;

            //Set Type to Session

            SchedulerViewModel model = new SchedulerViewModel();
            ViewBag.PageMode = "EDIT"; //PageMode is EDIT or READ
            ViewBag.ButtonMode = "AUTO"; //ButtonMode is AUTO or MANUAL

            bool rtn = false;

            if (pStartDate != null && pEndDate != null)
            { 
                var currentDate = DateTime.Now;
                var startDate = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(pStartDate));
                var multiVesselID = pMultiVesselID;
                rtn = SearchScheduleTransData(pStartDate, pEndDate, multiVesselID, pType, ref model);
                model.flagDefult = false;
            }
            else
            {
                // Get current month (Past 1 month, Next 1 month)
                var currentDate = DateTime.Now;
                var startDate = new DateTime(currentDate.AddMonths(-1).Year, currentDate.AddMonths(-1).Month, 1);               // Start date Past 1 month
                var endDate = new DateTime(currentDate.AddMonths(3).Year, currentDate.AddMonths(3).Month, 1).AddDays(-1);       // End date Next 2 month

                var startDateFormat = ConvertDateTimeToDateStringFormat(startDate, "dd/MM/yyyy");
                var endDateFormat = ConvertDateTimeToDateStringFormat(endDate, "dd/MM/yyyy");
                var multiVesselID = pMultiVesselID;
                rtn = SearchScheduleTransData(startDateFormat, endDateFormat, multiVesselID, pType, ref model);
                model.flagDefult = true;
            }

            // Set DDL
            initializeDropDownList(ref model, pType);
            using (var context = new EntityCPAIEngine())
            {
                var schedule_date = DateTime.Now;                 
                var schedule_user = ""; 
                var sql_schedule = (from a in context.CPAI_VESSEL_SCHEDULE
                           select new
                           {
                               Update_Date = a.VSD_UPDATED_DATE,
                               Update_By = a.VSD_UPDATED_BY, 
                           }).ToList();
                var result = sql_schedule.Distinct().ToList().OrderByDescending(o => o.Update_Date);
                var sql_spNote = (from a in context.CPAI_VESSEL_SCHEDULE_SP_NOTE
                                    select new
                                    {
                                        Update_Date = a.VSSP_UPDATED,
                                        Update_By = a.VSSP_UPDATED_BY,
                                    }).ToList();
                var result_spNote = sql_spNote.Distinct().ToList().OrderByDescending(o => o.Update_Date);
                var sp_schedule_date = DateTime.Now;
                var sp_schedule_user = "";
                var i = 0;
                if (result_spNote.Count() == 0)
                {
                    sp_schedule_date = DateTime.Now.AddDays(-1);
                }
                else {
                    foreach (var item in result_spNote)
                    {
                        if (i < 1)
                        {
                            sp_schedule_date = item.Update_Date;
                            sp_schedule_user = item.Update_By;
                        }
                        i++;
                    }
                }
                
                i = 0;
                foreach (var item in result)
                {
                    if(i < 1)
                    {
                        schedule_date = item.Update_Date;
                        schedule_user = item.Update_By;
                    }
                    i++;
                }
                if(sp_schedule_date > schedule_date)
                {
                    model.LatestUpdate = sp_schedule_date.ToString("dd/MM/yyyy HH:mm") + " by " + sp_schedule_user.ToUpper();
                }
                else
                {
                    model.LatestUpdate = schedule_date.ToString("dd/MM/yyyy HH:mm") + " by " + schedule_user.ToUpper();
                } 
            }
                if (rtn)
                    return View(model);
                else
                    return View(model);
        }

        // GET: CPAIMVC/Scheduler (Detail)
        public ActionResult Index()
        {
            Session["PageAction"] = "SCH"; // For _btn_Click

            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string search_date = String.IsNullOrEmpty(Request.QueryString["sch_date"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string search_charter = String.IsNullOrEmpty(Request.QueryString["sch_charter"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            SchedulerViewModel model = new SchedulerViewModel();            

            // Check TranId And Status is not null
            if (TranID != null && Status != null)
            {
                // Get Data by TranID
                ResponseData resData = LoadDataSchedulerDetail(TranID, Type, ref model);
                // Set DDL
                initializeDropDownList(ref model, Type);

                // Set Alert Message
                if (resData != null && resData.result_code != "1")
                {
                    TempData["res_message"] = resData.response_message;
                }

                // Check is not Duplicate
                if (Request.QueryString["isDup"] == null)
                {
                    ViewBag.TaskID = TranID;    // Set Transaction ID

                    if (Status.ToUpper() == "DRAFT")
                    {
                        ViewBag.Title = "Edit schedule (DRAFT)";
                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                    }
                    else if (Status.ToUpper() == "SUBMIT")
                    {
                        ViewBag.Title = "Edit schedule (SUBMIT)";
                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                    }
                    else // CANCEL or Other
                    {
                        ViewBag.Title = "View schedule (CANCEL)";
                        ViewBag.PageMode = "READ";       //PageMode is EDIT or READ
                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                    }

                    // Set parameter not Encrypt
                    model.schd_Transaction_id = String.IsNullOrEmpty(TranID) ? null : TranID;
                    model.schd_Status = String.IsNullOrEmpty(Status) ? null : Status; // Status (DRAFT,SUBMIT,CANCEL)
                    model.schd_Type = String.IsNullOrEmpty(Type) ? null : Type;
                    model.schd_Doc_no = String.IsNullOrEmpty(DocNO) ? null : DocNO;
                    model.schd_Req_transaction_id = String.IsNullOrEmpty(Tran_Req_ID) ? null : Tran_Req_ID;

                }
                else
                {
                    // Add
                    ViewBag.Title = "Add new schedule (Duplicate)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL

                }


                if (model.sch_detail.sch_cargo.Count() == 0) model.sch_detail.sch_cargo = null;
                if (model.sch_detail.sch_load_ports.Count() == 0) model.sch_detail.sch_load_ports = null;
                if (model.sch_detail.sch_dis_ports.Count() == 0) model.sch_detail.sch_dis_ports = null;

                return View("Index", model);
            }
            else
            {
                // New Schedule
                ViewBag.Title = "Add new schedule";
                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or 
                initializeDropDownList(ref model, Type);
                model.schd_dayLaycan = "Day";
                model.schd_daySailing = "Day";
                return View("Index", model);
            }
        }

        [HttpPost]
        public string getFreightDataList(string datestr, string vessel, string charterer, string laycan, string laytime, string cargo)
        {
            DateTime? date = string.IsNullOrEmpty(datestr) ? null : ShareFn.ConvertStringDateFormatToDatetime(datestr);
            DateTime? laycan_from = null; DateTime? laycan_to = null;
            if (!string.IsNullOrEmpty(laycan))
            {
                string[] arr_laycan = laycan.SplitWord(" to ");
                laycan_from = ShareFn.ConvertStringDateFormatToDatetime(arr_laycan[0]);
                laycan_to = ShareFn.ConvertStringDateFormatToDatetime(arr_laycan[1]);
            }
            return getFreightDataList(date, vessel, charterer, laycan_from, laycan_to, laytime, cargo);
        }

        public static string getFreightDataList(DateTime? date, string vessel, string charterer, DateTime? laycan_from, DateTime? laycan_to, string laytime, string cargo)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                   // var chot = (from c in context.CHOT_DATA where c.OTDA_STATUS != "CANCEL" select c);

                    var sql = (from a in context.CPAI_FREIGHT_DATA
                               join b in context.CPAI_FREIGHT_CARGO on a.FDA_ROW_ID equals b.FDC_FK_FREIGHT_DATA
                               join c in context.MT_VEHICLE on a.FDA_FK_VESSEL equals c.VEH_ID
                               join d in context.MT_CUST_DETAIL on a.FDA_FK_CHARTERER equals d.MCD_ROW_ID

                               //join e_value in chot on a.FDA_ROW_ID equals e_value.OTDA_FK_FREIGHT into e_join
                              // from e in e_join.DefaultIfEmpty()

                               where a.FDA_STATUS == "SUBMIT" //(e.OTDA_STATUS == "CANCEL" || e.OTDA_STATUS == null)

                               && (date.HasValue ? a.FDA_DATE_FILL_IN == date : true)
                               && (!string.IsNullOrEmpty(vessel) ? a.FDA_FK_VESSEL == vessel : true)
                               && (!string.IsNullOrEmpty(charterer) ? a.FDA_FK_CHARTERER == charterer : true)
                               && (laycan_from.HasValue ? a.FDA_LAYCAN_FROM == laycan_from : true)
                               && (laycan_to.HasValue ? a.FDA_LAYCAN_TO == laycan_to : true)
                               && (!string.IsNullOrEmpty(laytime) ? a.FDA_LAYTIME.Contains(laytime) : true)
                               && (!string.IsNullOrEmpty(cargo) ? b.FDC_FK_CARGO == cargo : true)
                               select new
                               {
                                   FREIGHT_ROW_ID = a.FDA_ROW_ID,
                                   FREIGHT_DATE = a.FDA_DATE_FILL_IN,
                                   FREIGHT_NO = a.FDA_DOC_NO,
                                   VESSEL = c.VEH_VEH_TEXT,
                                   LAYCAN_FROM = a.FDA_LAYCAN_FROM,
                                   LAYCAN_TO = a.FDA_LAYCAN_TO,
                                   CHARTERER = d.MCD_NAME_1,
                                   CREATED_BY = a.FDA_CREATED_BY,
                                   LAST_UPDATE = a.FDA_UPDATED,
                                   FCL_STATUS = a.FDA_STATUS,
                                  // OTDA_STATUS = e.OTDA_STATUS
                               });
                    var sql_list = sql.ToList();
                    var newSql = (from a in context.CPAI_FREIGHT_DATA
                                  join b in context.CPAI_FREIGHT_CARGO on a.FDA_ROW_ID equals b.FDC_FK_FREIGHT_DATA
                                  join c in context.MT_VEHICLE on a.FDA_FK_VESSEL equals c.VEH_ID
                                  //join e_value in chot on a.FDA_ROW_ID equals e_value.OTDA_FK_FREIGHT into e_join
                                  //from e in e_join.DefaultIfEmpty()
                                  where a.FDA_STATUS == "SUBMIT"
                                  && (date.HasValue ? a.FDA_DATE_FILL_IN == date : true)
                                  && (!string.IsNullOrEmpty(vessel) ? a.FDA_FK_VESSEL == vessel : true)
                                  && (!string.IsNullOrEmpty(charterer) ? a.FDA_FK_CHARTERER == charterer : true)
                                  && (laycan_from.HasValue ? a.FDA_LAYCAN_FROM == laycan_from : true)
                                  && (laycan_to.HasValue ? a.FDA_LAYCAN_TO == laycan_to : true)
                                  && (!string.IsNullOrEmpty(laytime) ? a.FDA_LAYTIME.Contains(laytime) : true)
                                  && (!string.IsNullOrEmpty(cargo) ? b.FDC_FK_CARGO == cargo : true)
                                  select new
                                  {
                                      FREIGHT_ROW_ID = a.FDA_ROW_ID,
                                      FREIGHT_DATE = a.FDA_DATE_FILL_IN,
                                      FREIGHT_NO = a.FDA_DOC_NO,
                                      VESSEL = c.VEH_VEH_TEXT,
                                      LAYCAN_FROM = a.FDA_LAYCAN_FROM,
                                      LAYCAN_TO = a.FDA_LAYCAN_TO,
                                      //CHARTERER = d.MCD_NAME_1,
                                      CREATED_BY = a.FDA_CREATED_BY,
                                      LAST_UPDATE = a.FDA_UPDATED,
                                      FCL_STATUS = a.FDA_STATUS,
                                     // OTDA_STATUS = e.OTDA_STATUS
                                  });
                    var newSqlList = newSql.ToList();
                    var newResult = newSqlList.Distinct().ToList().OrderByDescending(o => o.LAST_UPDATE);
                    var result = sql_list.Distinct().ToList().OrderByDescending(o => o.LAST_UPDATE);
                    List<ChotFreightModal> arr_data = new List<ChotFreightModal>();
                    foreach (var v in result)
                    {
                        arr_data.Add(new ChotFreightModal
                        {
                            chot_fcl_row_id = v.FREIGHT_ROW_ID,
                            chot_fcl_date = ShareFn.ConvertDateTimeToDateStringFormat(v.FREIGHT_DATE, "dd/MM/yyyy"),
                            chot_fcl_no = v.FREIGHT_NO,
                            chot_fcl_vessel = v.VESSEL,
                            chot_fcl_laycan = ShareFn.ConvertDateTimeToDateStringFormat(v.LAYCAN_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(v.LAYCAN_TO, "dd/MM/yyyy"),
                            chot_fcl_charterer = v.CHARTERER,
                            chot_fcl_create_by = v.CREATED_BY,
                            chot_fcl_update_date = ShareFn.ConvertDateTimeToDateStringFormat(v.LAST_UPDATE, "dd/MM/yyyy HH:mm")
                        });
                    }
                    foreach (var item in newResult)
                    {
                        var chkResult = arr_data.SingleOrDefault(a => a.chot_fcl_row_id == item.FREIGHT_ROW_ID);
                        if (chkResult == null)
                        {
                            arr_data.Add(new ChotFreightModal
                            {
                                chot_fcl_row_id = item.FREIGHT_ROW_ID,
                                chot_fcl_date = ShareFn.ConvertDateTimeToDateStringFormat(item.FREIGHT_DATE, "dd/MM/yyyy"),
                                chot_fcl_no = item.FREIGHT_NO,
                                chot_fcl_vessel = item.VESSEL,
                                chot_fcl_laycan = ShareFn.ConvertDateTimeToDateStringFormat(item.LAYCAN_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(item.LAYCAN_TO, "dd/MM/yyyy"),
                                //chot_fcl_charterer = v.CHARTERER,
                                chot_fcl_create_by = item.CREATED_BY,
                                chot_fcl_update_date = ShareFn.ConvertDateTimeToDateStringFormat(item.LAST_UPDATE, "dd/MM/yyyy HH:mm")
                            });
                        }
                    }
                    //var newArr_data = arr_data.OrderByDescending(a => ShareFn.ConvertStringDateFormatToDatetime(a.chot_fcl_update_date, "dd/MM/yyyy HH:mm"));

                    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                    var newArr_data = arr_data.OrderByDescending(a => DateTime.ParseExact(a.chot_fcl_update_date,
                       "dd/MM/yyyy HH:mm",CultureInfo.InvariantCulture));

                    return new JavaScriptSerializer().Serialize(newArr_data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: CPAIMVC/Scheduler/Activity
        public ActionResult Activity()
        {
            //TempData["res_message"] = "";
            //TempData["returnPage"] = "";

            string ttt = ShareFn.GetExcelColumnName(1);

            Session["PageAction"] = "ACT"; // For _btn_Click

            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();

            SchedulerViewModel model = new SchedulerViewModel();

            // Check TranId And Status is not null
            if (TranID != null && Status != null)
            {
                // Get Data by TranID
                ResponseData resData = LoadDataSchedulerActivity(TranID, Type, ref model);

                // Set Alert Message
                if (resData != null && resData.result_code != "1")
                {
                    TempData["res_message"] = resData.response_message;
                    TempData["returnPage"] = Url.Action("Search", "Scheduler", new { Type = Type.Encrypt() }); // returnPage;//returnPage;
                }


                ViewBag.TaskID = TranID;    // Set Transaction ID

                if (Status.ToUpper() == "SUBMIT")
                {
                    ViewBag.Title = "Edit activity (SUBMIT)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                }

                model.schd_Transaction_id = String.IsNullOrEmpty(TranID) ? null : TranID;
                model.schd_Status = String.IsNullOrEmpty(Status) ? null : Status; // Status (DRAFT,SUBMIT,CANCEL)
                model.schd_Type = String.IsNullOrEmpty(Type) ? null : Type;
                model.schd_Doc_no = String.IsNullOrEmpty(DocNO) ? null : DocNO;
                model.schd_Req_transaction_id = String.IsNullOrEmpty(Tran_Req_ID) ? null : Tran_Req_ID;

                //model.sch_activitys_model = new SchActivityViewModel();
                //model.sch_activitys.sch_activity_grid_model = new List<SchActivityViewModel_GridValues>();

                // Set DDL
                initializeDropDownList(ref model, Type);

                return View("Activity", model);
            }
            else
            {

                return RedirectToAction("Search", "Scheduler", new { Type = Type.Encrypt() });

            }


        }

        public ActionResult Test()
        {
            return View();
        }

        #endregion

        #region ---- Event Action  ------

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, SchedulerViewModel model)
        {
            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);

            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();


            DocumentActionStatus _status = DocumentActionStatus.Submit;
            this.updateModel(form, ref model);

            string typeDecrypt = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";


            ResponseData resData = SaveDataSchedule(_status, model, note, json_uploadFile);

            if (resData.result_code == "10000044")
            {
                TempData["res_message"] = "Duplicate Period. Please select new Period.";
            }
            else
            {
                TempData["res_message"] = resData.response_message;
            }
            

            if (resData != null && resData.result_code == "1")
            {
                //TempData["returnPage"] = Url.Action("Search", "Scheduler", new { Type = Session["Schedule_Type"].ToString() }); // returnPage;

                string tranID = resData.transaction_id.Encrypt();
                string reqID = resData.req_transaction_id.Encrypt();
                string docno = resData.resp_parameters[0].v.Encrypt();
                string status = "SUBMIT".Encrypt();
                string type = typeDecrypt.Encrypt();

                ViewBag.Title = "Edit schedule (SUBMIT)";
                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL

                string path = string.Format("~/CPAIMVC/Scheduler/Index?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Type={3}&Status={4}", tranID, reqID, docno, type, status);
                return Redirect(path);
            }
            else
            {
                // Check is not Duplicate
                if (isDup == null)
                {
                    ViewBag.Title = "Add new schedule";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or 
                }
                else
                {
                    // Add
                    ViewBag.Title = "Add new schedule (Duplicate)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL

                }
            }
            // Set DDL
            initializeDropDownList(ref model, Type);

            //return RedirectToAction("Search", "Scheduler", new { Type = Session["Schedule_Type"].ToString() });
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, SchedulerViewModel model)
        {
            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);

            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? null : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? null : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? null : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            DocumentActionStatus _status = DocumentActionStatus.Draft;
            this.updateModel(form, ref model);
            string typeDecrypt = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";

            ResponseData resData = SaveDataSchedule(_status, model, note, json_uploadFile);

            ViewBag.TaskID = resData.transaction_id;    // Set Transaction ID

            TempData["res_message"] = resData.response_message;

            if (resData != null && resData.result_code == "1")
            {
                string tranID = resData.transaction_id.Encrypt();
                string reqID = resData.req_transaction_id.Encrypt();
                string docno = resData.resp_parameters[0].v.Encrypt();
                string status = "DRAFT".Encrypt();
                string type = typeDecrypt.Encrypt();

                ViewBag.Title = "Edit schedule (DRAFT)";
                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL

                string path = string.Format("~/CPAIMVC/Scheduler/Index?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Type={3}&Status={4}", tranID, reqID, docno, type, status);
                return Redirect(path);
            }
            else
            {
                // Check is not Duplicate
                if (isDup == null)
                {
                    ViewBag.Title = "Add new schedule";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or 
                }
                else
                {
                    // Add
                    ViewBag.Title = "Add new schedule (Duplicate)";
                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL

                }
            }


            initializeDropDownList(ref model, typeDecrypt);

            return View("Index", model);
        }


        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, SchedulerViewModel model)
        {
            string TranID = String.IsNullOrEmpty(Request.QueryString["TranID"]) ? model.schd_Transaction_id : Request.QueryString["TranID"].ToString().Decrypt();
            string Status = String.IsNullOrEmpty(Request.QueryString["Status"]) ? model.schd_Status : Request.QueryString["Status"].ToString().Decrypt(); // Status (DRAFT,SUBMIT,CANCEL)
            string Type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : "";
            string DocNO = String.IsNullOrEmpty(Request.QueryString["DocNO"]) ? model.schd_Doc_no : Request.QueryString["DocNO"].ToString().Decrypt();
            string Tran_Req_ID = String.IsNullOrEmpty(Request.QueryString["Tran_Req_ID"]) ? model.schd_Req_transaction_id : Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
            string isDup = String.IsNullOrEmpty(Request.QueryString["isDup"]) ? null : Request.QueryString["TranID"].ToString().Decrypt();

            string PageAction = Convert.ToString(Session["PageAction"]);
            string buttonCode = model.ButtonCode;

            SchedulerViewModel modelTemp = model;   // Set model to temp for error message

            if (PageAction == "SCH")
            {
                // Save Schedule

                string type = "";
                this.updateModel(form, ref model);
                if (ButtonListScheduler != null)
                {
                    var _lstButton = ButtonListScheduler.Where(x => x.name == form["hdfEventClick"]).ToList();
                    if (_lstButton != null && _lstButton.Count > 0)
                    {
                        string _xml = _lstButton[0].call_xml;
                        string note = form["hdfNoteAction"];
                        List<ReplaceParam> _param = new List<ReplaceParam>();
                        string TranReqID = model.schd_Req_transaction_id;
                        SchRootObject obj = new SchRootObject();
                        obj.sch_detail = model.sch_detail;
                        var json = new JavaScriptSerializer().Serialize(obj);
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
                        _xml = _FN.RaplceParamXML(_param, _xml);
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                        RequestData _req = new RequestData();
                        ResponseData resData = new ResponseData();
                        _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                        resData = service.CallService(_req);

                        var btnName = _lstButton[0].name.ToUpper();
                        ViewBag.TaskID = resData.transaction_id;    // Set Transaction ID
                        
                        if (resData != null && resData.result_code == "1")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string docno = resData.resp_parameters[0].v.Encrypt();
                            type = !string.IsNullOrEmpty(model.schd_Type) ? model.schd_Type.Encrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString() : "";
                            string status = "";// btnName == "CANCEL" ? "CANCEL".Encrypt() : model.schd_Status.Encrypt();

                            if (btnName == "SAVE DRAFT")
                            {
                                status = "DRAFT".Encrypt();
                            }
                            else if (btnName == "CANCEL")
                            {
                                status = "CANCEL".Encrypt();
                            }
                            else if (btnName == "SUBMIT" || btnName == "SAVE SCHEDULE")
                            {
                                status = "SUBMIT".Encrypt();
                            }

                            string path = string.Format("~/CPAIMVC/Scheduler/Index?TranID={0}&Tran_Req_ID={1}&DocNO={2}&Type={3}&Status={4}", tranID, reqID, docno, type, status);

                            TempData["res_message"] = resData.response_message;
                            return Redirect(path);

                        }
                        else
                        {
                            
                            //TempData["res_message"] = resData.response_message;

                            // Get Data by TranID
                            ResponseData resDataLoad = LoadDataSchedulerDetail(TranID, Type, ref model);

                            // Set Alert Message
                            if (resDataLoad != null && resDataLoad.result_code == "1")
                            {
                                // Check is not Duplicate
                                if (isDup == null)
                                {
                                    ViewBag.TaskID = TranID;    // Set Transaction ID

                                    if (Status == "DRAFT")
                                    {
                                        ViewBag.Title = "Edit schedule (DRAFT)";
                                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                    }
                                    else if (Status == "SUBMIT")
                                    {
                                        ViewBag.Title = "Edit schedule (SUBMIT)";
                                        ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                    }
                                    else // CANCEL or Other
                                    {
                                        ViewBag.Title = "View schedule (CANCEL)";
                                        ViewBag.PageMode = "READ";       //PageMode is EDIT or READ
                                        ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                    }

                                    // set model temp
                                    model = modelTemp;
                                }
                                else
                                {
                                    // Add
                                    ViewBag.Title = "Add new schedule (Duplicate)";
                                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL

                                }

                            }
                            else
                            {
                                TempData["res_message"] = resDataLoad.response_message;
                            }
                            if (resData.result_code == "10000044")
                            {
                                TempData["res_message"] = "Duplicate Period. Please select new Period.";
                            }
                            else
                            {
                                TempData["res_message"] = resData.response_message;
                            }
                        }
                    }
                }
                else
                {
                    TempData["res_message"] = "ERROR";

                }

                initializeDropDownList(ref model, type.Decrypt());

                return View("Index", model);

            }
            else if (PageAction == "ACT")
            {
                // Save Activity
                Session["PageAction"] = "ACT"; // For _btn_Click
                string type = "";

                if (ButtonListScheduler != null)
                {
                    var _lstButton = ButtonListScheduler.Where(x => x.name == form["hdfEventClick"]).ToList();
                    if (_lstButton != null && _lstButton.Count > 0)
                    {
                        string _xml = _lstButton[0].call_xml;
                        string note = form["hdfNoteAction"];
                        List<ReplaceParam> _param = new List<ReplaceParam>();
                        string TranReqID = model.schd_Req_transaction_id;

                        SchRootObject obj = new SchRootObject();
                        obj.sch_activitys = new List<SchActivity>();

                        if(model.sch_activitys != null)
                        {
                            foreach (var item in model.sch_activitys)
                            {
                                var item_activity = item.activity.Split('|');
                                obj.sch_activitys.Add(new SchActivity { order = item.order, activity = item_activity[2], activity_name = item.activity_name, other_activity = item.other_activity, activity_color = item.activity_color, date = item.date, start_date = item.start_date, act_note = item.act_note, explanationAttach = item.explanationAttach, @ref = item.@ref });
                            }
                        }                       

                        var json = new JavaScriptSerializer().Serialize(obj);
                        _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID });
                        _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                        _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                        _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                        _xml = _FN.RaplceParamXML(_param, _xml);
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                        RequestData _req = new RequestData();
                        ResponseData resData = new ResponseData();
                        _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                        resData = service.CallService(_req);

                        var btnName = _lstButton[0].name.ToUpper();
                        ViewBag.TaskID = resData.transaction_id;    // Set Transaction ID
                        if (resData != null && resData.result_code == "1")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string docno = resData.resp_parameters[0].v.Encrypt();
                            type = !string.IsNullOrEmpty(model.schd_Type) ? model.schd_Type.Encrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString() : "";
                            string status = "";// btnName == "CANCEL" ? "CANCEL".Encrypt() : model.schd_Status.Encrypt();

                            if (btnName == "SAVE ACTIVITY")
                            {
                                status = "SUBMIT".Encrypt();
                            }

                            //TempData["res_message"] = resData.response_message;
                            //TempData["returnPage"] = Url.Action("Search", "Scheduler", new { Type = Session["Schedule_Type"].ToString() }); // returnPage;

                            string path = string.Format("~/CPAIMVC/Scheduler/Activity?TranID={0}&Type={1}&Tran_Req_ID={2}&DocNO={3}&Status={4}", tranID, type, reqID, docno, status);

                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            return Redirect(path);

                        }
                        else
                        {

                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;


                            // Get Data by TranID
                            ResponseData resDataLoad = LoadDataSchedulerActivity(TranID, Type, ref model);


                            // Set Alert Message
                            if (resDataLoad != null && resDataLoad.result_code == "1")
                            {

                                if (Status.ToUpper() == "SUBMIT")
                                {
                                    ViewBag.Title = "Save activity (SUBMIT)";
                                    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
                                    ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
                                }

                                // set model temp
                                model.sch_activitys_model.sch_activity_grid_model = modelTemp.sch_activitys_model.sch_activity_grid_model;

                            }
                            else
                            {
                                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            }



                            model.schd_Transaction_id = String.IsNullOrEmpty(TranID) ? null : TranID;
                            model.schd_Status = String.IsNullOrEmpty(Status) ? null : Status; // Status (DRAFT,SUBMIT,CANCEL)
                            model.schd_Type = String.IsNullOrEmpty(Type) ? null : Type;
                            model.schd_Doc_no = String.IsNullOrEmpty(DocNO) ? null : DocNO;
                            model.schd_Req_transaction_id = String.IsNullOrEmpty(Tran_Req_ID) ? null : Tran_Req_ID;

                            initializeDropDownList(ref model, Type);
                        }
                    }
                }
                else
                {
                    TempData["res_message"] = "เกิดข้อผิดพลาด";

                }
                return View("Activity", model);
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
                string sType = !string.IsNullOrEmpty(model.schd_Type) ? model.schd_Type.Encrypt() : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString() : "";
                return RedirectToAction("Search", "Scheduler", new { Type = sType.Encrypt() });
            }
        }

        [HttpPost]
        public string saveSPNote(string month_year, string note)
        {
            return SchedulerServiceModel.saveSPNote(month_year, note, lbUserName);
        }

        [HttpPost]
        public string getSPNote(string month_year)
        {
            return SchedulerServiceModel.getSPNote(month_year);
        }
        #endregion

        #region ---- Function Engine ----

        /// <summary>
        /// Get data Schedule Tranasaction from Service Engine
        /// ex. Get month Aug
        /// Start date set Back 1 Month 01/01/2016 00:00:00
        /// End date set Forward 1 Month  02/01/2017 23:59:59
        /// </summary>
        /// <param name="sDate">Start Date</param>
        /// <param name="eDate">End Date</param>
        /// <param name="multiVessel">Vessel Code["vessel1|vessel2|vessel3"]</param>
        /// <returns></returns>
        private bool SearchScheduleTransData(string sDate, string eDate, string multiVesselID, string pType, ref SchedulerViewModel _model)
        {

            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000010;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.SCHEDULE });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = _FN.ConvertDateFormat(sDate, true).Replace("-", "/") });
            req.Req_parameters.P.Add(new P { K = "to_date", V = _FN.ConvertDateFormat(eDate, true).Replace("-", "/") });

            if (String.IsNullOrEmpty(multiVesselID) == false)
                req.Req_parameters.P.Add(new P { K = "index_5", V = multiVesselID });

            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;
            string list_trans = "";

            if (_DataJson != "")
            {
                List_Scheduletrx scheduletrx = new List_Scheduletrx();
                scheduletrx = ShareFunction.DeserializeXMLFileToObject<List_Scheduletrx>(_DataJson);

                if (scheduletrx.ScheduleTransaction != null)
                {
                    // Config Data for Grid
                    _model.sch_trans_grid_model = getSchTransForGridView(scheduletrx.ScheduleTransaction);
                    // Config Data for Gantt                    
                    _model.sch_trans_gantt_model = getSchTransForGanttView(scheduletrx.ScheduleTransaction, multiVesselID, ref list_trans);

                    if (_model.sch_trans_gantt_model.Count > 0)
                        // Convert sch_trans_gantt_model to JSON for ganttView Data
                        _model.sch_trans_gantt_model_json = new JavaScriptSerializer().Serialize(_model.sch_trans_gantt_model);
                    else
                        _model.sch_trans_gantt_model_json = "[]";

                    _model.sch_trans_custcolor_model = getSchTransForCustomerColorView(scheduletrx.ScheduleTransaction);
                }

                var startDate = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(sDate));
                var endDate = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(eDate));
                _model.schd_currentMonthYear = ConvertDateTimeToDateStringFormat(DateTime.Now, "MMMM yyyy");
                _model.schd_startDate = ConvertDateTimeToDateStringFormat(startDate.AddDays(0), "yyyy-MM-dd");
                _model.schd_endDate = ConvertDateTimeToDateStringFormat(endDate.AddDays(0), "yyyy-MM-dd");
                _model.schd_searchStartEndDate = sDate + " to " + eDate;

                _model.sch_trans_list_id = list_trans;

                _model.schd_vendorColorDefualt = COLOR_DEFUALT;

                return true;

            }
            else
            {
                // Set Alert Message
                if (resData != null && resData.result_code != "1")
                {
                    TempData["res_message"] = resData.response_message;
                    TempData["returnPage"] = Url.Action("Search", "Scheduler", new { Type = pType.Encrypt() }); // returnPage;

                }
                else
                {
                    TempData["res_message"] = "Data Not Found";
                    TempData["returnPage"] = Url.Action("Search", "Scheduler", new { Type = pType.Encrypt() }); //returnPage;
                }

                // For Test
                //List_Scheduletrx scheduletrx = new List_Scheduletrx();
                //scheduletrx.ScheduleTransaction = new List<ScheduleTransaction>();
                // Data for Gantt View
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("SUBMIT", sDate, eDate, "1", "", "", "", "", "", "#6592c6"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("SUBMIT", sDate, eDate, "2", "", "", "", "", "", "#9679b0"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("SUBMIT", sDate, eDate, "3", "", "", "", "", "", "#cd5b5d"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("SUBMIT", sDate, eDate, "4", "", "", "", "", "", "#c69c6d"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("SUBMIT", sDate, eDate, "5", "", "", "", "", "", "#5fb8a3"));


                // Data for Grid View
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("DRAFT", "01/06/2016", "5/06/2016", "1", "Phubai Pattra 1", "PR000", "Domestics PX 5 KT", "SMPC", "SMPC", "#cd5b5d"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("CANCEL", "07/06/2016", "15/06/2016", "1", "Phubai Pattra 1", "PR000", "Import MTBE from 1 or 2 ports SG to TOP Sriracha ... ", "SMPC", "SMPC", "#c69c6d"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("DRAFT", "01/06/2016", "05/08/2016", "2", "Phubai Pattra 2", "PR000", "Domestics PX 5 KT", "SMPC", "SMPC", "#6592c6"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("DRAFT", "01/07/2016", "05/07/2016", "3", "Phubai Pattra 3", "PR000", "Domestics PX 5 KT", "SMPC", "SMPC", "#6592c6"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("DRAFT", "07/06/2016", "15/06/2016", "2", "Phubai Pattra 2", "PR000", "Import MTBE from 1 or 2 ports SG to TOP Sriracha ... ", "SMPC", "SMPC", "#5fb8a3"));
                //scheduletrx.ScheduleTransaction.Add(createSchTrans("CANCEL", "01/07/2016", "05/07/2016", "1", "Phubai Pattra 1", "PR000", "Domestics PX 5 KT", "SMPC", "SMPC", "#6592c6"));

                //Config Data for Grid

                //_model.sch_trans_grid_model = getSchTransForGridView(scheduletrx.ScheduleTransaction);
                //// Config Data for Gantt
                //_model.sch_trans_gantt_model = getSchTransForGanttView(scheduletrx.ScheduleTransaction);
                // // Convert sch_trans_gantt_model to JSON for ganttView Data
                // _model.sch_trans_gantt_model_json = new JavaScriptSerializer().Serialize(_model.sch_trans_gantt_model);

                return false;

            }



            //return resData;

        }

        private ResponseData LoadDataSchedulerDetail(string TransactionID, string userType, ref SchedulerViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000011;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.SCHEDULE });
            req.Req_parameters.P.Add(new P { K = "type", V = userType });
            req.Req_parameters.P.Add(new P { K = "button_type", V = "SCH" });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att != null)
                {
                    if (Att.attach_items.Count > 0)
                    {
                        foreach (string _item in Att.attach_items)
                        {
                            ViewBag.hdfFileUpload += _item + "|";
                        }
                    }
                }
            }
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                ViewBag.ButtonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    ViewBag.ButtonMode = "MANUAL";
                    ViewBag.PageMode = "READ";
                }
                else
                {
                    ViewBag.ButtonMode = "AUTO";
                    ViewBag.PageMode = "EDIT";
                }
            }
            //"<input type = 'button' class='btn btn-default' id='btnBack' onclick='location.href=\'" + Url.Action("Index","Scheduler") +"\' value='Back' />"
            string url = Url.Action("Search", "Scheduler", new { Type = userType.Encrypt() });
            string url_excel = Url.Action("GenActivityExcel", "Scheduler", new { trans_id = TransactionID, type = "Excel" });
            string url_pdf = Url.Action("GenActivityExcel", "Scheduler", new { trans_id = TransactionID, type = "Pdf" });
                       

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<SchedulerViewModel>(_model.data_detail);
                if (model.sch_detail != null)
                {
                    if (!string.IsNullOrEmpty(model.sch_detail.date_start) && !string.IsNullOrEmpty(model.sch_detail.date_end))
                    {
                        model.schd_tempDate = model.sch_detail.date_start + " to " + model.sch_detail.date_end;
                    }
                    if (!string.IsNullOrEmpty(model.sch_detail.laycan_from) && !string.IsNullOrEmpty(model.sch_detail.laycan_to))
                    {
                        model.schd_tempLaycan = model.sch_detail.laycan_from + " to " + model.sch_detail.laycan_to;
                        model.schd_dayLaycan = DateBetweenToDayText(model.sch_detail.laycan_from, model.sch_detail.laycan_to);
                    }
                    if (!string.IsNullOrEmpty(model.sch_detail.sailing_from) && !string.IsNullOrEmpty(model.sch_detail.sailing_to))
                    {
                        model.schd_tempSailing = model.sch_detail.sailing_from + " to " + model.sch_detail.sailing_to;
                        model.schd_daySailing = DateBetweenToDayText(model.sch_detail.sailing_from, model.sch_detail.sailing_to);
                    }

                    model.schd_vendorColorDefualt = COLOR_DEFUALT;                    
                    string[] dateStart = model.sch_detail.date_start.Split('/');
                    string[] dateEnd = model.sch_detail.date_start.Split('/');
                    var newDateStart = "01"+"/" + dateStart[1]+"/" + dateStart[2];
                    var newDateEnd = "";
                    if(dateEnd[1] == "04" || dateEnd[1] == "06" || dateEnd[1] == "09" || dateEnd[1] == "11")
                    {
                        newDateEnd =  "30" + "/" + dateEnd[1] + "/" + dateEnd[2];
                    }else if(dateEnd[1] == "02")
                    {
                        var convertDate = Convert.ToDateTime("01/03/"+ dateEnd[2]);
                        var newDate = convertDate.AddDays(-1);
                        newDateEnd = newDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        newDateEnd = "31" + "/" + dateEnd[1] + "/" + dateEnd[2];
                    }
                    string _btn = "<input type = 'button' class='btn btn-default' id='btnBackCurrent' onclick=\"location.href='" + url + "&StartDate=" + newDateStart + "&EndDate=" + newDateEnd + "'\" value='BACK' />";
                    _btn += "<input type = 'button' class='btn btn-default' id='btnBack' onclick=\"location.href='" + url + "'\" value='BACK TO HOME' />";
                    ViewBag.ButtonCode += _btn;
                }
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = returnPage;
            }

            // For Test
            //model.sch_detail = createSchDetail();
            //if (model.sch_detail != null)
            //    {
            //    if (!string.IsNullOrEmpty(model.sch_detail.date_start) && !string.IsNullOrEmpty(model.sch_detail.date_end))
            //    {
            //        model.schd_tempDate = model.sch_detail.date_start + " to " + model.sch_detail.date_end;
            //    }
            //    if (!string.IsNullOrEmpty(model.sch_detail.laycan_from) && !string.IsNullOrEmpty(model.sch_detail.laycan_to))
            //    {
            //        model.schd_tempLaycan = model.sch_detail.laycan_from + " to " + model.sch_detail.laycan_to;
            //    }
            //    if (!string.IsNullOrEmpty(model.sch_detail.sailing_from) && !string.IsNullOrEmpty(model.sch_detail.sailing_to))
            //    {
            //        model.schd_tempSailing = model.sch_detail.sailing_from + " to " + model.sch_detail.sailing_to;
            //    }
            //}
            model.ButtonCode = Convert.ToString(ViewBag.ButtonCode); // set button code to model
            return resData;
        }

        private ResponseData LoadDataSchedulerActivity(string TransactionID, string userType, ref SchedulerViewModel model)
        {
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();


            ExtraXML_ACT _model = new ExtraXML_ACT();

            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            string _DataJson = "";

            RequestCPAI req = new RequestCPAI();

            // Get Activity
            req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000011;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.SCHEDULE });
            req.Req_parameters.P.Add(new P { K = "type", V = userType });
            req.Req_parameters.P.Add(new P { K = "button_type", V = "ACT" });
            req.Extra_xml = "";


            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            _DataJson = resData.extra_xml;

            _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML_ACT>("<ExtraXML>" + _DataJson + "</ExtraXML>");


            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                ViewBag.ButtonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button);
            }
            else
            {
                ViewBag.ButtonMode = "AUTO";
                ViewBag.PageMode = "EDIT";
            }
            //<input type='button' class='btn btn-default' id='btnBack' onclick='location.href=\'" + Url.Action("Index","Scheduler") +"\' value='Back' />"
            //string _btn = "<input type = 'button' class='btn btn-default' id='btnBack' onclick=\"location.href='" + Url.Action("Search", "Scheduler") + "?Type=" + userType.Encrypt() + "'\" value='BACK' />";

            string url = Url.Action("Search", "Scheduler", new { Type = userType.Encrypt() });
            string url_excel = Url.Action("GenActivityExcel", "Scheduler", new { trans_id = TransactionID, type = "Excel" });
            string url_pdf = Url.Action("GenActivityExcel", "Scheduler", new { trans_id = TransactionID, type = "Pdf" });
            string _btn = "<input type = 'button' class='btn btn-default' id='btnExcel' onclick=\"window.open(' " + url_excel + "', '_blank');\" value='EXCEL' />";
            _btn += "<input type = 'button' class='btn btn-default' id='btnPDF' onclick=\"window.open(' " + url_pdf + "', '_blank');\" value='PDF' />";
            
            //_btn += "<input type = 'button' class='btn btn-default' id='btnBackCurrent' onclick=\"location.href='" + url + "'\" value='BACK' />";
            //_btn += "<input type = 'button' class='btn btn-default' id='btnBackCurrent' onclick=\"location.href='" + url + "&StartDate=" + model.sch_detail.date_start + "&EndDate=" + model.sch_detail.date_end + "'\" value='BACK' />";
            ViewBag.ButtonCode += _btn;


            if (_model.data_detail != null)
            {

                model = new JavaScriptSerializer().Deserialize<SchedulerViewModel>(_model.data_detail);
                //SchRootObject modelACT = new SchRootObject();
                //modelACT = new JavaScriptSerializer().Deserialize<SchRootObject>(_model.data_detail_act);

                if (model.sch_detail != null)
                {
                    string strActivityOrder = "";
                    string strActivityId = "";
                    string strDateStart = "";
                    string strDateEnd = "";
                    string strColorCode = "";
                    string strRef = "";

                    // Set Schedule
                    model.sch_activitys_model = new SchActivityViewModel();
                    model.sch_activitys_model.TranID = TransactionID;
                    model.sch_activitys_model.Type = userType;

                    if (!string.IsNullOrEmpty(model.sch_detail.date_start))
                    {
                        model.sch_activitys_model.schd_startDate = ShareFn.ConvertStringDateToDateFormat(model.sch_detail.date_start, "dd/MM/yyyy", "yyyy-MM-dd") + " 00:00";
                        model.sch_activitys_model.schd_calendar_startDate = model.sch_detail.date_start;
                    }

                    if (!string.IsNullOrEmpty(model.sch_detail.date_end))
                    {
                        model.sch_activitys_model.schd_endDate = ShareFn.ConvertStringDateToDateFormat(model.sch_detail.date_end, "dd/MM/yyyy", "yyyy-MM-dd") + " 23:59";
                        model.sch_activitys_model.schd_calendar_endDate = model.sch_detail.date_end;
                    }

                    // Set new object
                    model.sch_activitys_model.sch_activity_gantt_model = new List<SchActivityViewModel_GanttSource>();
                    model.sch_activitys_model.sch_activity_grid_model = new List<SchActivityViewModel_GridValues>();

                    // Set Gantt Source Schedule
                    model.sch_activitys_model.sch_activity_gantt_model.Add(ActivityGanttSourceSetValueSCH(model.sch_detail.vessel, model.sch_detail.vendor, model.sch_detail.note, model.sch_detail.date_start, model.sch_detail.date_end, model.sch_detail.vendor_color));

                    if (model.sch_activitys != null)
                    {

                        for (var i = 0; i < model.sch_activitys.Count; i++)
                        {
                            strActivityOrder = model.sch_activitys[i].order;
                            strActivityId = model.sch_activitys[i].activity;
                            strDateStart = model.sch_activitys[i].date;
                            strDateEnd = i == model.sch_activitys.Count - 1 ? ShareFn.ConvertStringDateToDateFormat(model.sch_detail.date_end, "dd/MM/yyyy", "dd/MM/yyyy") + " 23:59" : model.sch_activitys[i + 1].date;
                            strColorCode = model.sch_activitys[i].activity_color;
                            strRef = model.sch_activitys[i].@ref;
                            model.sch_activitys_model.sch_activity_gantt_model.Add(ActivityGanttSourceSetValueACT(userType, strActivityId, strDateStart, strDateEnd, strColorCode));

                            model.sch_activitys_model.sch_activity_grid_model.Add(ActivityGridValueAdd(userType, strActivityOrder, strActivityId, strDateStart, strRef));
                        }

                    }
                    else
                    {

                        model.sch_activitys_model.sch_activity_grid_model.Add(ActivityGridValueAdd(userType, "1", "", "", ""));
                        //    strActivityId = "mva000001";
                        //    strDateStart = "03/11/2016 05:00";
                        //    strDateEnd = "03/11/2016 07:30";
                        //    strColorCode = "#f8a81d";
                        //    // Test Simulate data
                        //    model.sch_activitys_model.sch_activity_gantt_model.Add(ActivityGanttSourceSetValueACT(strActivityId, strDateStart, strDateEnd, strColorCode));

                        //    strActivityId = "mva000003";
                        //    strDateStart = "03/11/2016 07:30";
                        //    strDateEnd = "03/11/2016 12:00";
                        //    strColorCode = "#fdcf08";
                        //    // Test Simulate data
                        //    model.sch_activitys_model.sch_activity_gantt_model.Add(ActivityGanttSourceSetValueACT(strActivityId, strDateStart, strDateEnd, strColorCode));
                    }

                    // Convert to JSON
                    model.sch_activitys_model.sch_activity_gantt_model_json = new JavaScriptSerializer().Serialize(model.sch_activitys_model.sch_activity_gantt_model);
                    string[] dateStart = model.sch_detail.date_start.Split('/');
                    string[] dateEnd = model.sch_detail.date_start.Split('/');
                    var newDateStart = "01" + "/" + dateStart[1] + "/" + dateStart[2];
                    var newDateEnd = "";
                    if (dateEnd[1] == "04" || dateEnd[1] == "06" || dateEnd[1] == "09" || dateEnd[1] == "11")
                    {
                        newDateEnd = "30" + "/" + dateEnd[1] + "/" + dateEnd[2];
                    }
                    else if (dateEnd[1] == "02")
                    {
                        var convertDate = Convert.ToDateTime("01/03/" + dateEnd[2]);
                        var newDate = convertDate.AddDays(-1);
                        newDateEnd = newDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        newDateEnd = "31" + "/" + dateEnd[1] + "/" + dateEnd[2];
                    }
                    _btn = "<input type = 'button' class='btn btn-default' id='btnBackCurrent' onclick=\"location.href='" + url + "&StartDate=" + newDateStart + "&EndDate=" + newDateEnd + "'\" value='BACK' />";
                    _btn += "<input type = 'button' class='btn btn-default' id='btnBack' onclick=\"location.href='" + url + "'\" value='BACK TO HOME' />";
                    ViewBag.ButtonCode += _btn;
                }                
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = returnPage;
            }


            return resData;
        }

        // Function for Save Data Schedule To Engine Service (F10000009)
        private ResponseData SaveDataSchedule(DocumentActionStatus _status, SchedulerViewModel model, string note, string json_fileUpload)
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }
            string type = !string.IsNullOrEmpty(model.schd_Type) ? model.schd_Type : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : ""; //Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : Request.QueryString["Type"].ToString().Decrypt();

            SchRootObject obj = new SchRootObject();
            obj.sch_detail = model.sch_detail;


            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000009;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input_sch", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.SCHEDULE });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        // Function for Save Data Activity To Engine Service (F10000009)
        private ResponseData SaveDataActivity(DocumentActionStatus _status, SchedulerViewModel model, string note)
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }
            string type = !string.IsNullOrEmpty(model.schd_Type) ? model.schd_Type : Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : ""; //Session["Schedule_Type"] != null ? Session["Schedule_Type"].ToString().Decrypt() : Request.QueryString["Type"].ToString().Decrypt();

            SchRootObject obj = new SchRootObject();
            obj.sch_activitys = new List<SchActivity>();

            foreach (var item in model.sch_activitys_model.sch_activity_grid_model)
            {
                obj.sch_activitys.Add(new SchActivity { order = item.act_order, activity = item.act_code, activity_color = item.act_color, date = item.act_date, @ref = item.act_ref });
            }


            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000009;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input_sch", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.SCHEDULE });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }


        #endregion

        #region ---- Function Common ----       
        // Get data Sch Trans (SUBMIT)
        private List<SchTransGanttViewModel_Data> getSchTransForGanttView(List<ScheduleTransaction> pSchTrans, string vessel, ref string list_trans)
        {
            var ganttData = new List<SchTransGanttViewModel_Data>();
            var ganttSeies = new List<SchTransGanttViewModel_Series>();
            var ganttActivities = new List<SchTransGanttViewModel_Activities>();
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            var imgEdit = baseUrl + "Content/images/icon-pencil.png";
            var imgActivity = baseUrl + "Content/images/icon_view_list.png";
            var imgCopy = baseUrl + "Content/images/icon-copy.png";

            // For config html format in Time Line            
            string strFormatTitle = "<div>{0}</div><p style=\"display: none;\">{1}</p>" +
                "<div class=\"btn-group\">" +
                    "<button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" style=\"margin-right: -6px;\" onclick=\"onShowEvent('{2}', this)\">" +
                        "<span class=\"sr-only\">Toggle Dropdown</span>" +
                    "</button>" +
                    "<ul class=\"dropdown-menu\" role=\"menu\" id=\"ul_{2}\" style=\"display: none;\">" +
                        "<li title=\"Edit\"><a href=\"{3}\"><img src=\"{4}\" /></a></li>" +
                        "<li title=\"Copy\"><a href=\"{5}\"><img src=\"{6}\" /></a></li>" +
                        "{7}" +
                    //"<li title=\"Activity\"><a href=\"{6}\"><img src=\"{7}\" /></a></li>" +
                    "</ul>" +
                "</div>";

            // Fillter status equals SUBMIT
            var filterList = from t in pSchTrans
                             where t.Status.ToUpper() == "SUBMIT"
                             orderby t.Vessel_id
                             select t;

            if (filterList.ToList().Count > 0)
            {

                var mt_vehicle = VehicleDAL.GetVehicle(PROJECT_NAME_SPACE, ACTIVE, "SCH_P").OrderBy(x => x.VEH_VEH_TEXT).ToList();
                var results0 = from r in mt_vehicle select new { VesselId = r.VEH_ID, VesselName = r.VEH_VEH_TEXT };

                // Group Vessel
                //var results1 = from p in filterList
                //              group p by new { p.Vessel_id, p.Vessel } into g
                //              select new { VesselId = g.Key.Vessel_id, VesselName = g.Key.Vessel };

                bool isVessel = false;
                if (vessel == null)
                {
                    vessel = "|";
                    isVessel = true;
                }

                string[] arr_vessel = vessel.SplitWord("|");

                var results1 = from r in mt_vehicle
                               where arr_vessel.Contains(r.VEH_ID)
                               select new { VesselId = r.VEH_ID, VesselName = r.VEH_VEH_TEXT };

                //int i = 1;
                // for loop Vessel
                foreach (var item in isVessel ? results0.ToList() : results1.ToList())
                {
                    ganttSeies = new List<SchTransGanttViewModel_Series>();

                    // group Vessel 
                    var filterVesselList = from v in filterList
                                           where v.Vessel_id.Equals(item.VesselId)
                                           orderby v.Date_start
                                           select v;

                    // New Activity
                    ganttActivities = new List<SchTransGanttViewModel_Activities>();
                    // Add Activity to Vessel
                    foreach (ScheduleTransaction itemAct in filterVesselList.ToList())
                    {
                        string strLinkSCH = Url.Action("Index", "Scheduler") + "?TranId=" + itemAct.Transaction_id.Encrypt() + "&Type=" + itemAct.Type.Encrypt() + "&Tran_Req_ID=" + itemAct.Req_transaction_id.Encrypt() + "&DocNO=" + itemAct.Purchase_no.Encrypt() + "&Status=" + itemAct.Status.Encrypt();
                        string strLinkSCH_Dup = Url.Action("Index", "Scheduler") + "?TranId=" + itemAct.Transaction_id.Encrypt() + "&Type=" + itemAct.Type.Encrypt() + "&Tran_Req_ID=" + itemAct.Req_transaction_id.Encrypt() + "&DocNO=" + itemAct.Purchase_no.Encrypt() + "&Status=" + itemAct.Status.Encrypt() + "&isDup=true";
                        string strLinkACT = "<li title=\"Activity\"><a href=\"" + Url.Action("Activity", "Scheduler") + "?TranId=" + itemAct.Transaction_id.Encrypt() + "&Type=" + itemAct.Type.Encrypt() + "&Tran_Req_ID=" + itemAct.Req_transaction_id.Encrypt() + "&DocNO=" + itemAct.Purchase_no.Encrypt() + "&Status=" + itemAct.Status.Encrypt() + "\"><img src=\"" + imgActivity + "\" /></a></li>";

                        SchDetail sch = SchedulerServiceModel.getScheduler(itemAct.Transaction_id);
                        string strTitle = "";
                        string tColor = "";
                        string strLaycan = "";

                        DateTime dt1 = DateTime.ParseExact(sch.date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        DateTime dt2 = DateTime.ParseExact(sch.date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        if (sch.schedule_type == "Schedule")
                        {
                            if (!string.IsNullOrEmpty(itemAct.Laycan_from) && !string.IsNullOrEmpty(itemAct.Laycan_to))
                            {

                                if ((dt2 - dt1).TotalDays > 2)
                                {
                                    strLaycan += _FN.ConvertDateFormatBackFormat(itemAct.Laycan_from, "dd MMM") + " - " + _FN.ConvertDateFormatBackFormat(itemAct.Laycan_to, "dd MMM");
                                    strLaycan += "<br>" + sch.vendor_name;
                                }
                                else
                                {
                                    strLaycan += _FN.ConvertDateFormatBackFormat(itemAct.Laycan_from, "dd/MM") + " - " + _FN.ConvertDateFormatBackFormat(itemAct.Laycan_to, "dd/MM");
                                }
                            }

                            strTitle += "Laycan : " + _FN.ConvertDateFormatBackFormat(itemAct.Laycan_from, "dd MMM") + " - " + _FN.ConvertDateFormatBackFormat(itemAct.Laycan_to, "dd MMM") + "\n";
                            strTitle += "Charterer : " + itemAct.Supplier + "\n";

                            List<SchCargo> cargo = SchedulerServiceModel.getCargoList(itemAct.Transaction_id);
                            List<SchLoadPorts> load_port = SchedulerServiceModel.getLoadPortList(itemAct.Transaction_id);
                            List<SchDisPorts> dis_port = SchedulerServiceModel.getDischargePortList(itemAct.Transaction_id);

                            list_trans += itemAct.Transaction_id + "|";

                            foreach (var v in cargo)
                            {
                                if(cargo.Count == 1)
                                {
                                    strTitle += "Cargo : " + MaterialsServiceModel.GetName(v.cargo) + " " + Convert.ToDecimal(v.qty).ToString("#,##0") + " " + (v.unit == "OTHER" ? v.others : v.unit) + "\n";
                                }
                                else
                                {
                                    strTitle += "Cargo " + v.order + " : " + MaterialsServiceModel.GetName(v.cargo) + " " + Convert.ToDecimal(v.qty).ToString("#,##0") + " " + (v.unit == "OTHER" ? v.others : v.unit) + "\n";
                                }                               
                            }

                            foreach (var v in load_port)
                            {
                                if (load_port.Count == 1)
                                {
                                    strTitle += "Load Port : " + v.port + "\n";
                                }
                                else
                                {
                                    strTitle += "Load Port " + v.order + " : " + v.port + "\n";
                                }
                            }

                            foreach (var v in dis_port)
                            {
                                if (dis_port.Count == 1)
                                {
                                    strTitle += "Discharge Port : " + v.port + "\n";
                                }
                                else
                                {
                                    strTitle += "Discharge Port " + v.order + " : " + v.port + "\n";
                                }                                
                            }

                            if (sch.tentative)
                            {
                                tColor = "#848484";
                            }
                            else
                            {
                                tColor = String.IsNullOrEmpty(itemAct.Supplier_color) ? COLOR_DEFUALT : itemAct.Supplier_color;
                            }
                        }
                        else
                        {
                            strTitle += "Period : " + dt1.ToString("dd MMM") + " - " + dt2.ToString("dd MMM") + "\n";
                            strTitle += "Type : " + (sch.schedule_type == "Others" ? sch.schedule_type_detail : sch.schedule_type) + "\n";
                            tColor = sch.color;
                            strLinkACT = "";
                        }

                        strTitle += "Attach File : " + SchedulerServiceModel.getCountAttachFile(itemAct.Transaction_id) + " File(s)\n";
                        strTitle += "Note : " + (string.IsNullOrEmpty(sch.note) ? "-" : sch.note) + "\n";
                        strTitle += "Latest update : " + (string.IsNullOrEmpty(sch.updateDate) ? "-" : sch.updateDate) + "\n";
                        strTitle += "Update by : " + (string.IsNullOrEmpty(sch.UpdateBy) ? "-" : sch.UpdateBy);
                        ganttActivities.Add(new SchTransGanttViewModel_Activities
                        {
                            //title = String.Format(strFormatTitle, itemAct.Remark, itemAct.Supplier, _FN.ConvertDateFormatBackFormat(itemAct.Date_start, "dd MMM"), _FN.ConvertDateFormatBackFormat(itemAct.Date_end, "dd MMM"), strLinkSCH, strLinkACT, imgEdit, imgActivity, strLinkSCH_Dup, imgCopy)
                            title = String.Format(strFormatTitle, strLaycan, strTitle, itemAct.Transaction_id, strLinkSCH, imgEdit, strLinkSCH_Dup, imgCopy, strLinkACT)
                        ,
                            start = _FN.ConvertDateFormatBack(itemAct.Date_start, true)
                        ,
                            end = _FN.ConvertDateFormatBack(itemAct.Date_end, true)
                        ,
                            color = tColor
                        });
                    }

                    ganttSeies.Add(new SchTransGanttViewModel_Series { name = item.VesselName, activities = ganttActivities });
                    ganttData.Add(new SchTransGanttViewModel_Data { id = item.VesselId, name = item.VesselName, series = ganttSeies });
                }

            }

            return ganttData;
        }

        // get Data Sch Trans (DRAFT,CANCEL)
        private List<SchTransGridViewModel> getSchTransForGridView(List<ScheduleTransaction> pSchTrans)
        {
            List<SchTransGridViewModel> schList = new List<SchTransGridViewModel>();


            foreach (ScheduleTransaction item in pSchTrans.ToList().OrderByDescending(o => o.Purchase_no).ToList())
            {
                var update_date = "";
                var update_by = "";
                using (var context = new EntityCPAIEngine())
                {
                    var schedule_detail = context.CPAI_VESSEL_SCHEDULE.Where(a => a.VSD_ROW_ID == item.Transaction_id).ToList();
                    foreach(var z in schedule_detail)
                    {
                        update_date = z.VSD_UPDATED_DATE.ToString("dd/MM/yyyy HH:mm");
                        update_by = z.VSD_UPDATED_BY;
                    }
                }
                schList.Add(new SchTransGridViewModel
                    {
                        Transaction_id_Encrypted = item.Transaction_id.Encrypt()
                    ,
                        Req_transaction_id_Encrypted = item.Req_transaction_id.Encrypt()
                    ,
                        Doc_no_Encrypted = item.Purchase_no.Encrypt()
                    ,
                        Type_Encrypted = item.Type.Encrypt()
                    ,
                        scht_Datetime = string.Format("{0} - {1}", item.Date_start, item.Date_end)
                    ,
                        scht_Doc_no = item.Purchase_no
                    ,
                        scht_vasselName = item.Vessel
                    ,
                        scht_userCustomer = item.Supplier
                    ,
                        scht_note = item.Remark
                    ,
                        scht_laycan = item.Laycan_from + " - " + item.Laycan_to
                    ,
                        scht_status = item.Status
                    ,
                        scht_type = item.Type
                    ,
                        scht_status_Encrypted = item.Status.Encrypt(),
                        scht_Update_by = update_by,
                        scht_Update_date = update_date
                    });
            } 
            List<SchTransGridViewModel> schList_New = new List<SchTransGridViewModel>();
            schList_New =  schList.OrderByDescending(a => DateTime.ParseExact(a.scht_Update_date,
                       "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture)).ToList();
            return schList_New;
        }

        // Get data Sch Trans (SUBMIT) for dropdownlist customer color
        private List<SchTransDropdownCustColorViewModel> getSchTransForCustomerColorView(List<ScheduleTransaction> pSchTrans)
        {
            var model = new List<SchTransDropdownCustColorViewModel>();


            // Fillter status equals SUBMIT
            var filterList = from t in pSchTrans
                             where t.Status.ToUpper() == "SUBMIT"
                             orderby t.Vessel_id
                             select new
                             {
                                 Supplier_id = t.Supplier_id,
                                 Supplier = (string.IsNullOrEmpty(t.Supplier) ? SchedulerServiceModel.getSchedulerNameColorById(t.Transaction_id, true) : t.Supplier),
                                 Supplier_color = (string.IsNullOrEmpty(t.Supplier_color) ? SchedulerServiceModel.getSchedulerNameColorById(t.Transaction_id, false) : t.Supplier_color)
                             };

            if (filterList.ToList().Count > 0)
            {

                // Group Customer
                var results = from p in filterList
                              orderby p.Supplier
                              group p by new { p.Supplier_id, p.Supplier, p.Supplier_color } into g
                              select new { SupplierId = g.Key.Supplier_id, Supplier = g.Key.Supplier, SupplierColor = g.Key.Supplier_color };

                var newOther = new List<SchTransDropdownCustColorViewModel>();
                //newOther = null;
                foreach (var item in results)
                {
                    if(item.SupplierId != "")
                    {
                        if (!String.IsNullOrEmpty(item.SupplierId))
                        {
                            model.Add(new SchTransDropdownCustColorViewModel { cust_Code = item.SupplierId, cust_Name = CustDetailDAL.GetCustSortNameById(item.SupplierId), cust_Color = item.SupplierColor });
                        }
                        else if (!String.IsNullOrEmpty(item.Supplier))
                        {
                            model.Add(new SchTransDropdownCustColorViewModel { cust_Code = item.SupplierId, cust_Name = item.Supplier, cust_Color = item.SupplierColor });
                        }
                    }else
                    {
                        if (item.Supplier != "")
                        {
                            newOther.Add(new SchTransDropdownCustColorViewModel {
                            cust_Code = item.SupplierId,
                            cust_Name = item.Supplier,
                            cust_Color = item.SupplierColor,
                        });
                            
                        }
                    }
                }
                model.Add(new SchTransDropdownCustColorViewModel { cust_Code = "", cust_Name = "Tank Cleaning", cust_Color = "#FFFF00" });
                model.Add(new SchTransDropdownCustColorViewModel { cust_Code = "", cust_Name = "Maintenance", cust_Color = "#FF0000" });
                model.Add(new SchTransDropdownCustColorViewModel { cust_Code = "", cust_Name = "Off-Hire", cust_Color = "#000000" });

                if(newOther.Count != 0)
                {
                    foreach(var item in newOther.OrderBy(a =>a.cust_Name))
                    {
                        model.Add(item);
                    }                    
                }
                

            }

            return model;
        }

        // 
        public int GetDifferenceInDaysX(DateTime startDate, DateTime endDate, bool countMe = false)
        {
            TimeSpan ts = endDate - startDate;
            int totalDays = (int)Math.Ceiling(ts.TotalDays);
            if (ts.TotalDays < 1 && ts.TotalDays > 0)
                totalDays = 1;
            else
                totalDays = (int)(ts.TotalDays);

            if (countMe)
                totalDays += 1;

            return totalDays;
        }

        /// <summary>
        /// Get Day Text from between date
        /// </summary>
        /// <param name="startDate">dd/MM/yyyy</param>
        /// <param name="endDate">dd/MM/yyyy</param>
        /// <returns></returns>
        public string DateBetweenToDayText(string startDate, string endDate)
        {
            var rtn = "Day";

            try
            {
                var fday = "{0} Day";
                var fdays = "{0} Days";
                var sDate = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(startDate));
                var eDate = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(endDate));
                var day = GetDifferenceInDaysX(sDate, eDate, true);

                if (day > 1)
                {
                    rtn = string.Format(fdays, day.ToString());
                }
                else
                {
                    rtn = string.Format(fday, day.ToString());
                }
            }
            catch
            {
                rtn = "Day";
            }

            return rtn;
        }

        public string ConvertDateTimeToDateStringFormat(DateTime pDate, string DateStringFormat = "yyyy-MM-dd")
        {
            return pDate.ToString(DateStringFormat, CultureInfo.InvariantCulture);
        }

        public SchActivityViewModel_GanttSource ActivityGanttSourceSetValueSCH(string pVesselID, string pVendorID, string pNote, string pFromDate, string pToDate, string pColorCode)
        {

            SchActivityViewModel_GanttSource obj = new SchActivityViewModel_GanttSource();
            SchActivityViewModel_GanttValues objValue = new SchActivityViewModel_GanttValues();
            obj.values = new List<SchActivityViewModel_GanttValues>();

            string strLabelScheduleFormat = "{0} ({1})<br><span>{2} - {3}</span>";  //Note (Vendor Name)<br><span>06 Jun - 12 Nov</span>
                                                                                    // Firth row -> Schedule
            string strVesselName = "";
            string strCustomerName = "";
            string strDateStart = "";
            string strDateEnd = "";
            string strColorClass = "";
            string strLabel = "";

            string strDateStartShort = "";
            string strDateEndShort = "";

            strVesselName = VehicleServiceModel.GetName(pVesselID);
            strCustomerName = CustomerServiceModel.GetName(pVendorID);
            strDateStart = ShareFn.ConvertStringDateToDateFormat(pFromDate, "dd/MM/yyyy", "yyyy-MM-dd") + " 00:00";
            strDateStartShort = ShareFn.ConvertStringDateToDateFormat(pFromDate, "dd/MM/yyyy", "dd MMM");
            strDateEnd = ShareFn.ConvertStringDateToDateFormat(pToDate, "dd/MM/yyyy", "yyyy-MM-dd") + " 23:59";
            strDateEndShort = ShareFn.ConvertStringDateToDateFormat(pToDate, "dd/MM/yyyy", "dd MMM");

            strColorClass = ShareFn.getColorClass(pColorCode);

            strLabel = String.Format(strLabelScheduleFormat, pNote, strCustomerName, strDateStartShort, strDateEndShort);

            obj.name = strVesselName;

            objValue.label = strLabel;
            objValue.from = strDateStart;          // "yyyy-MM-dd HH:mm"
            objValue.to = strDateEnd;              // "yyyy-MM-dd HH:mm"
            objValue.customClass = strColorClass;

            obj.values.Add(objValue);
            return obj;

        }

        public SchActivityViewModel_GanttSource ActivityGanttSourceSetValueACT(string pUserType, string pActivityID, string pFromDate, string pToDate, string pColorCode)
        {

            SchActivityViewModel_GanttSource obj = new SchActivityViewModel_GanttSource();
            SchActivityViewModel_GanttValues objValue = new SchActivityViewModel_GanttValues();
            obj.values = new List<SchActivityViewModel_GanttValues>();

            string strName = "";
            string strLabelActivityFormat = "{0}<br><span>{1} - {2}</span>";        //Activity Name<br><span>05:00 - 08:30</span>
                                                                                    // Firth row -> Schedule
            string strActivityName = "";
            string strDateStart = "";
            string strDateEnd = "";
            string strColorClass = "";
            string strLabel = "";

            string strDateStartShort = "";
            string strDateEndShort = "";

            strActivityName = VesselActivityServiceModel.GetName(pActivityID, pUserType);
            strDateStart = ShareFn.ConvertStringDateToDateFormat(pFromDate, "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm");
            strDateStartShort = ShareFn.ConvertStringDateToDateFormat(pFromDate, "dd/MM/yyyy HH:mm", "HH:mm");
            strDateEnd = ShareFn.ConvertStringDateToDateFormat(pToDate, "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm");
            strDateEndShort = ShareFn.ConvertStringDateToDateFormat(pToDate, "dd/MM/yyyy HH:mm", "HH:mm");

            strColorClass = ShareFn.getColorClass(pColorCode);

            strLabel = String.Format(strLabelActivityFormat, strActivityName, strDateStartShort, strDateEndShort);

            obj.name = strName;

            objValue.label = strLabel;
            objValue.from = strDateStart;          // "yyyy-MM-dd HH:mm"
            objValue.to = strDateEnd;              // "yyyy-MM-dd HH:mm"
            objValue.customClass = strColorClass;

            obj.values.Add(objValue);
            return obj;

        }

        public SchActivityViewModel_GanttSource ActivityGanttSourceAdd(string pName, string pLabel, string pFromDateTime, string pToDateTime, string pColorClass)
        {
            SchActivityViewModel_GanttSource obj = new SchActivityViewModel_GanttSource();
            SchActivityViewModel_GanttValues objValue = new SchActivityViewModel_GanttValues();
            obj.values = new List<SchActivityViewModel_GanttValues>();

            obj.name = pName;

            objValue.label = pLabel;
            objValue.from = pFromDateTime;          // "yyyy-MM-dd HH:mm"
            objValue.to = pToDateTime;              // "yyyy-MM-dd HH:mm"
            objValue.customClass = pColorClass;

            obj.values.Add(objValue);
            return obj;
        }

        public SchActivityViewModel_GridValues ActivityGridValueAdd(string pUserType, string act_order, string act_code, string act_date, string act_ref)
        {

            SchActivityViewModel_GridValues objValue = new SchActivityViewModel_GridValues();

            var strActCodeForDDL = VesselActivityServiceModel.GetStatus_Color_RowID(act_code, pUserType);
            var splitActivity = strActCodeForDDL.Split('|');
            if (splitActivity[0] != "")
            {
                objValue.act_order = act_order;
                objValue.act_code = strActCodeForDDL;   // RefStatus|ColorCode|RowID
                objValue.act_color = splitActivity[1];
                objValue.act_date = act_date;
                objValue.act_ref = act_ref;
                objValue.act_ref_status = splitActivity[0];
            }
            else
            {
                objValue.act_order = act_order;
                objValue.act_code = "";                 // RefStatus|ColorCode|RowID
                objValue.act_color = act_code;
                objValue.act_date = act_date;
                objValue.act_ref = act_ref;
                objValue.act_ref_status = "N";
            }


            return objValue;
        }


        public string ConvertStringDateToDateFormat(string pDDMMYYYY, string pDateFormat = "yyyy-MM-dd HH:mm", string pHHmm = "00:00")
        {
            var Date = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat(pDDMMYYYY));

            return "";
        }

        private List<SelectListItem> getCargoUnit()
        {
            Setting setting = JSONSetting.getSetting("JSON_FREIGHT");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_unit.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_unit[i], Value = setting.fcl_unit[i] });
            }
            return list;
        }

        private List<SelectListItem> getScheduleOther()
        {
            Setting setting = JSONSetting.getSetting("JSON_SCHEDULE_OTHER");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_unit.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_unit[i], Value = setting.fcl_unit[i] });
            }
            return list;
        }

        #endregion

        #region ---- For Test ----

        private ScheduleTransaction createSchTrans(string pStatus, string pDate_start, string pDate_end
      , string pVessel_id, string pVessel, string pPurchase_no, string pRemark
      , string pSupplier_id, string pSupplier, string pSupplier_color)
        {
            return new ScheduleTransaction
            {
                Function_id = "f10000010",
                Function_desc = "list transaction for schedule",
                Result_namespace = "CPAI",
                Result_status = "SUCCESS",
                Result_code = "1",
                Result_desc = "SUCCESS",
                Req_transaction_id = "201504301354261105262",
                Transaction_id = "201504301354270028804",
                Response_message = "",
                System = "SCHEDULE",
                Type = "CRUDE",
                Action = "APPROVE_3",
                Status = pStatus,
                Date_start = pDate_start,
                Date_end = pDate_end,
                Vessel_id = pVessel_id,
                Vessel = pVessel,
                Purchase_no = pPurchase_no,
                Remark = pRemark,
                Create_by = "ariya",
                Supplier_id = pSupplier_id,
                Supplier = pSupplier,
                Supplier_color = pSupplier_color,
                Reason = ""
            };
        }

        // For Test
        private SchDetail createSchDetail()
        {
            SchDetail model = new SchDetail();
            model.date_start = "01/06/2016";
            model.date_end = "01/06/2016";
            model.freight = "FRI-1611-0044";
            model.laycan_from = "01/06/2016";
            model.laycan_to = "01/06/2016";
            model.laytime = "1.25";
            model.note = "Test Note";
            model.sailing_from = "01/06/2016";
            model.sailing_to = "01/06/2016";
            model.vendor = "0000000011";        // Customer
            model.vessel = "1000001666";        // vessel


            List<SchLoadPorts> loadPort = new List<SchLoadPorts>();
            loadPort.Add(new SchLoadPorts { order = "1", port = "1" });
            loadPort.Add(new SchLoadPorts { order = "2", port = "2" });

            model.sch_load_ports = loadPort;

            List<SchDisPorts> disPort = new List<SchDisPorts>();
            disPort.Add(new SchDisPorts { order = "1", port = "2" });
            disPort.Add(new SchDisPorts { order = "2", port = "3" });

            model.sch_dis_ports = disPort;

            List<SchCargo> cargo = new List<SchCargo>();
            cargo.Add(new SchCargo { cargo = "Y900SN", qty = "1", tolerance = "Test", unit = "kg" });
            cargo.Add(new SchCargo { cargo = "YBIT40", qty = "100", tolerance = "TEst", unit = "gm" });
            model.sch_cargo = cargo;

            return model;
        }

        private List<SchActivity> createSchActivity()
        {
            List<SchActivity> model = new List<SchActivity>();
            model.Add(new SchActivity { order = "1", activity = "mva000074", activity_color = "#72b0c7", date = "30/06/2016 00:00", @ref = "" });
            model.Add(new SchActivity { order = "2", activity = "mva000081", activity_color = "#99bfe6", date = "30/06/2016 08:30", @ref = "Bunker Test1" });
            model.Add(new SchActivity { order = "3", activity = "mva000084", activity_color = "#8093cd", date = "30/06/2016 12:00", @ref = "" });
            model.Add(new SchActivity { order = "4", activity = "mva000003", activity_color = "#0172b8", date = "30/06/2016 15:00", @ref = "Bunker Test2" });
            return model;
        }

        //// GET: CPAIMVC/Scheduler/Test
        //public ActionResult Test()
        //{
        //    SchedulerViewModel model = new SchedulerViewModel();

        //    ViewBag.Title = "Test schedule";
        //    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
        //    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL

        //    return View("Test", model);

        //}

        //public ActionResult Add()
        //{


        //    SchedulerViewModel model = new SchedulerViewModel();

        //    ViewBag.Title = "Add new schedule";
        //    ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
        //    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL


        //    initializeDropDownList(ref model);

        //    return View("Index",model);

        //}

        //public ActionResult Edit()
        //{

        //    SchedulerViewModel model = new SchedulerViewModel();


        //    if (Request.QueryString["TranID"] != null && (Request.QueryString["Status"] != null))
        //    {

        //        string tranID = Request.QueryString["TranID"].ToString().Decrypt();
        //        string Status = Request.QueryString["Status"].ToString().Decrypt();


        //        ResponseData resData = LoadDataSchedulerDetail(tranID, ref model);
        //        initializeDropDownList(ref model);

        //        model.schd_Transaction_id_Encrypted = Request.QueryString["TranID"].ToString();
        //        model.schd_Status_Encrypted = Request.QueryString["Status"].ToString();
        //        if (Request.QueryString["Type"] != null)
        //        {
        //            model.schd_Type_Encrypted = Request.QueryString["Type"].ToString();
        //            ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
        //        }

        //        if (Request.QueryString["PURNO"] != null)
        //        {
        //            model.schd_Doc_no_Encrypted = Request.QueryString["PURNO"].ToString();
        //            ViewBag.PURNO = Request.QueryString["PURNO"].ToString().Decrypt();
        //        }

        //        if (Request.QueryString["Tran_Req_ID"] != null)
        //        {
        //            model.schd_Req_transaction_id_Encrypted = Request.QueryString["Tran_Req_ID"].ToString();
        //            ViewBag.Tran_Req_ID = Request.QueryString["Tran_Req_ID"].ToString().Decrypt();
        //        }

        //        if (Request.QueryString["isDup"] == null)
        //        {
        //            ViewBag.TaskID = tranID;    // Set Transaction ID

        //            if (Status.ToUpper() == "DRAFT")
        //            {
        //                ViewBag.Title = "Edit schedule (DRAFT)";
        //                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
        //                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
        //            }
        //            else if (Status.ToUpper() == "SUBMIT")
        //            {
        //                ViewBag.Title = "Edit schedule (SUBMIT)";
        //                ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
        //                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
        //            }
        //            else // CANCEL or Other
        //            {
        //                ViewBag.Title = "View schedule (CANCEL)";
        //                ViewBag.PageMode = "READ";       //PageMode is EDIT or READ
        //                ViewBag.ButtonMode = "MANUAL";    //ButtonMode is AUTO or MANUAL
        //            }
        //        }
        //        else
        //        {
        //            // Add
        //            ViewBag.Title = "Add new schedule (Duplicate)";
        //            ViewBag.PageMode = "EDIT";       //PageMode is EDIT or READ
        //            ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL
        //        }


        //        if (resData != null && resData.result_code != "1")
        //        {
        //            TempData["res_message"] = resData.response_message;
        //            TempData["returnPage"] = returnPage;
        //        }



        //        return View("Index", model);
        //    }
        //    else
        //    {

        //        // Go to index
        //        return RedirectToAction("Search","Scheduler");
        //    }


        //}

        //public ActionResult Read()
        //{
        //    ViewBag.Title = "View schedule";

        //    SchedulerViewModel model = new SchedulerViewModel();
        //    ViewBag.PageMode = "READ";       //PageMode is EDIT or READ
        //    ViewBag.ButtonMode = "AUTO";    //ButtonMode is AUTO or MANUAL


        //    if (Request.QueryString["TranID"] != null)
        //    {
        //        string tranID = Request.QueryString["TranID"].ToString().Decrypt();
        //        ResponseData resData = LoadDataSchedulerDetail(tranID, ref model);

        //        //if (resData != null && resData.result_code != "1")
        //        //{
        //        //    TempData["res_message"] = resData.response_message;
        //        //    TempData["returnPage"] = returnPage;
        //        //}
        //    }
        //    initializeDropDownList(ref model);

        //    return View("Detail", model);

        //}

        #endregion


        #region ---- Call Estimated Freight Calculation ----

        [HttpGet]
        public ActionResult FreightCalAjax(string DOCNO)
        {
            string result = SchedulerServiceModel.getFreightCalculationByDocNo(DOCNO);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult GenExcel(string date_start, string date_end, string type)
        {

            //string json_excel = LoadJsonFromTest();

            DateTime _date_start = DateTime.ParseExact(date_start, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime _date_end = DateTime.ParseExact(date_end, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            int months = ((_date_start.Year - _date_end.Year) * 12 + _date_end.Month - _date_start.Month) + 1;
            List<string> list_month = new List<string>();

            int month_start = _date_start.Month;
            int year_start = _date_start.Year;

            if (months == 1)
            {
                list_month.Add(new DateTime(year_start, month_start, 1).ToString("MM-yyyy"));
            }

            while (month_start.ToString() + year_start.ToString() != _date_end.Month.ToString() + _date_end.Year.ToString())
            {
                if (month_start > 12)
                {
                    month_start = 1;
                    year_start++;
                }

                string date = new DateTime(year_start, month_start, 1).ToString("MM-yyyy");
                list_month.Add(date);
                month_start++;

                if (month_start > 12)
                {
                    month_start = 1;
                    year_start++;
                }

                if (month_start.ToString() + year_start.ToString() == _date_end.Month.ToString() + _date_end.Year.ToString())
                {
                    date = new DateTime(year_start, month_start, 1).ToString("MM-yyyy");
                    list_month.Add(date);
                    break;
                }
            }

            string file_name = string.Format("Vessel-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_namePdf = string.Format("Vessel-{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "FREIGHT", file_name);
            FileInfo excelFile = new FileInfo(file_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                for (int idx_month = 0; idx_month < list_month.Count; idx_month++)
                {
                    int n_month = int.Parse(list_month[idx_month].SplitWord("-")[0]);
                    int n_year = int.Parse(list_month[idx_month].SplitWord("-")[1]);

                    string date_start_temp = new DateTime(n_year, n_month, 1).ToString("dd/MM/yyyy");
                    string date_end_temp = new DateTime(n_year, n_month, DateTime.DaysInMonth(n_year, n_month)).ToString("dd/MM/yyyy");
                    string json_excel = SchedulerServiceModel.GetJsonExcel(date_start_temp, date_end_temp);
                    RootObjectExcel model = new JavaScriptSerializer().Deserialize<RootObjectExcel>(json_excel);

                    int idx_main = 0;

                    string month_year = new DateTime(n_year, n_month, 1).ToString("MMM-yyyy");
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add(month_year);
                     
                    ws.DefaultRowHeight = 18;
                    ws.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    ws.Column(1).Width = 10;
                    ws.Column(2).Width = 24;
                    ws.Row(1).Height = 18;
                    ws.Row(2).Height = 18;

                    int max_day_months = DateTime.DaysInMonth(n_year, n_month);
                    for (int idx_day = 1; idx_day <= max_day_months; idx_day++)
                    {
                        ws.Column((idx_day + 2)).Width = 5;
                    }

                    string cell_max = getIndexColumn(max_day_months + 2);

                    //row 1
                    ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Tahoma", 10));
                    ws.Cells["A1:B1"].Merge = true;
                    ws.Cells["A1:B1"].Value = "Update as of : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    ws.Cells["A1:B1"].Style.Font.Bold = true;

                    ws.Cells["C1:" + cell_max + "1"].Merge = true;
                    ws.Cells["C1:" + cell_max + "1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["C1:" + cell_max + "1"].Value = month_year;
                    ws.Cells["C1:" + cell_max + "1"].Style.Font.Bold = true;
                    idx_main++;

                    //row 2
                    ws.Cells["A2:B2"].Merge = true;
                    ws.Cells["A2:B2"].Value = "Vessel Name";
                    ws.Cells["A2:B2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A2:B2"].Style.Font.Bold = true;

                    for (int idx_day = 1; idx_day <= max_day_months; idx_day++)
                    {
                        ws.Cells[2, (idx_day + 2)].Value = idx_day;
                    }
                    ws.Cells["B2:" + cell_max + "2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    idx_main++;

                    //row Detail
                    for (int i = 0; i < model.sch_vessel_excel.Count; i++)
                    {
                        ws.Row(i + 3).Height = 30;
                        ws.Row(i + 3).Style.WrapText = true;

                        ws.Cells["A" + (i + 3) + ":B" + (i + 3)].Merge = true;
                        ws.Cells[(i + 3), 1].Value = model.sch_vessel_excel[i].vessel;

                        for (int j = 0; j < model.sch_vessel_excel[i].sch_activity_excel.Count; j++)
                        {
                            string t_start = getIndexColumn(int.Parse(model.sch_vessel_excel[i].sch_activity_excel[j].date_start.SplitWord("/")[0]) + 2);
                            string t_end = getIndexColumn(int.Parse(model.sch_vessel_excel[i].sch_activity_excel[j].date_end.SplitWord("/")[0]) + 2);
                            string t_cell = t_start + (i + 3) + ":" + t_end + (i + 3);
                            ws.Cells[t_cell].Merge = true;
                            ws.Cells[t_cell].Value = model.sch_vessel_excel[i].sch_activity_excel[j].name;
                            ws.Cells[t_cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ws.Cells[t_cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(model.sch_vessel_excel[i].sch_activity_excel[j].color));
                            ws.Cells[t_cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        }
                        idx_main++;
                    }
                    AllBorders(ws.Cells["A1:" + cell_max + idx_main].Style.Border);
                    idx_main += 2;

                    ws.Cells["C" + idx_main].Value = "Note";
                    ws.Cells["C" + idx_main].Style.Font.Bold = true;
                    idx_main++;

                    int idx_note = idx_main;
                    for (int i = 0; i < model.sch_charterer_excel.Count; i++)
                    {
                        ws.Cells["A" + (idx_note + i)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        Color color = ColorTranslator.FromHtml(model.sch_charterer_excel[i].color);
                        ws.Cells["A" + (idx_note + i)].Style.Fill.BackgroundColor.SetColor(color);

                        ws.Cells["B" + (idx_note + i)].Value = model.sch_charterer_excel[i].name;
                        idx_main++;
                    }

                    //Detail Note
                    string cell_note = "C" + idx_note + ":" + cell_max + (idx_note + 14);
                    ws.Cells[cell_note].Merge = true;
                    ws.Cells[cell_note].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Cells[cell_note].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[cell_note].Style.WrapText = true;
                    ws.Cells[cell_note].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_note].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_note].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_note].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_note].Value = model.note;

                    //Special Note
                    ws.Cells["C" + (idx_note + 16)].Value = "Special Note";
                    ws.Cells["C" + (idx_note + 16)].Style.Font.Bold = true;

                    string cell_sp_note = "C" + (idx_note + 17) + ":" + cell_max + (idx_note + 35);
                    ws.Cells[cell_sp_note].Merge = true;
                    ws.Cells[cell_sp_note].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    ws.Cells[cell_sp_note].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[cell_sp_note].Style.WrapText = true;
                    ws.Cells[cell_sp_note].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_sp_note].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_sp_note].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells[cell_sp_note].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                    //HtmlRemoval htmlRemove = new HtmlRemoval();
                    //List<htmlTagList> Lst = htmlRemove.SplitHTMLTag(model.sp_note);
                    //ws.Cells[cell_sp_note].IsRichText = true;
                    //foreach (var item in Lst)
                    //{
                    //    if(item.TextString != "")
                    //    { 
                    //        var textString = HttpUtility.HtmlDecode(item.TextString.ToString()).ToString();
                    //        //OfficeOpenXml.Style.ExcelRichText ert = ws.Cells[cell_sp_note].RichText.Add(item.TextString.ToString().Replace("&nbsp;"," "));
                    //        OfficeOpenXml.Style.ExcelRichText ert = ws.Cells[cell_sp_note].RichText.Add(textString);
                    //        ert.Bold = item.Bold;
                    //        ert.Italic = item.Italic;
                    //        ert.UnderLine = item.Underline;
                    //        ert.Color = Color.FromArgb(Convert.ToInt16(item.FontColor.R), Convert.ToInt16(item.FontColor.G), Convert.ToInt16(item.FontColor.B));
                    //    }                        
                    //} 
                    SelectPdf.GlobalProperties.LicenseKey = "SmF7anh/e2p+eWp7f2R6anl7ZHt4ZHNzc3M=";
                    SelectPdf.HtmlToImage htmlToImageConverter = new SelectPdf.HtmlToImage();    
                    System.Drawing.Image image = htmlToImageConverter.ConvertHtmlString(model.sp_note == null ? "<p></p>": model.sp_note);

                    var picture = ws.Drawings.AddPicture(month_year, image);
                    picture.SetPosition(27, 0, 3, 0);
                    var imgHeight = image.Height > 375 ? 375 : image.Height;
                    var imgWidth = 0;
                    if (max_day_months == 28)
                    {
                        if (image.Width > 900)
                        {
                            imgWidth = 900;
                        }
                        else
                        {
                            imgWidth = image.Width;
                        }
                    }else if(max_day_months == 30)
                    {
                        if (image.Width > 950)
                        {
                            imgWidth = 950;
                        }
                        else
                        {
                            imgWidth = image.Width;
                        }
                    }
                    else
                    {
                        if (image.Width > 1024)
                        {
                            imgWidth = 1024;
                        }
                        else
                        {
                            imgWidth = image.Width;
                        }
                    } 
                    picture.SetSize(imgWidth, imgHeight);

                    // ws.Cells[cell_sp_note].Value = StripHTML(HttpUtility.HtmlDecode(string.IsNullOrEmpty(model.sp_note) ? "<p></p>" : model.sp_note)); 
                    ws.PrinterSettings.Orientation = eOrientation.Landscape;
                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.FitToPage = true;
                    ws.Column(45).PageBreak = true;
                    //ws.PrinterSettings.PrintArea = ws.Cells["A:AH"];

                }
                
                package.Save(); 
            }

            if (type == "Pdf")
            {
                ReturnValue rtn = SaveAsPdf(file_path);
                Response.Redirect(string.Format("~/Web/FileUpload/FREIGHT/{0}", file_namePdf));
            }
            else // EXCEL
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
                Response.AddHeader("content-type", "application/Excel");
                Response.WriteFile(file_path);
                Response.End();
            }

            return View();
        }

        
         

        private static string StripHTML(string input)
        {
            return Regex.Replace(input.Replace("&nbsp;", ""), "<.*?>", String.Empty);
        }

        public ActionResult GenActivityExcel(string trans_id, string type)
        {
            //string json_excel = LoadJsonFromTest();

            string file_name = string.Format("Vessel-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_namePdf = string.Format("Vessel-{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "FREIGHT", file_name);
            FileInfo excelFile = new FileInfo(file_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                string json_excel = SchedulerServiceModel.GetJsonActivityExcel(trans_id);
                //string json_excel = SchedulerServiceModel.GetJsonExcel(date_start_temp, date_end_temp);
                RootObjectActivityExcel model = new JavaScriptSerializer().Deserialize<RootObjectActivityExcel>(json_excel);
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("ACT");

                ws.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ws.Cells.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ws.Column(1).Width = 54;
                ws.Column(2).Width = 16;
                ws.Column(3).Width = 16;

                ws.Cells["A1"].Value = "Vessel Name : " + model.vessel_name;
                ws.Cells["A2"].Value = "Charterer : " + model.charterer_name;
                ws.Cells["B1:C1"].Merge = true;
                ws.Cells["B1:C1"].Value = "Period : " + model.period;
                ws.Cells["B2:C2"].Merge = true;
                ws.Cells["B2:C2"].Value = "Laycan : " + model.laycan;
                ws.Cells["A1:C2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                ws.Cells["A3:A4"].Merge = true;
                ws.Cells["A3:A4"].Value = "Activity";
                ws.Cells["B3:B4"].Merge = true;
                ws.Cells["B3:B4"].Value = "Start Time";
                ws.Cells["C3:C4"].Merge = true;
                ws.Cells["C3:C4"].Value = "End Time";

                DateTime d1 = DateTime.ParseExact(model.period.SplitWord(" to ")[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime d2 = DateTime.ParseExact(model.period.SplitWord(" to ")[1], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                int days = Convert.ToInt32((d2 - d1).TotalDays) + 1;

                List<DateTime> list_dates = new List<DateTime>();

                for (int i = 0; i < days; i++)
                {
                    list_dates.Add(d1.AddDays(i));
                }

                int hour = days * 24;
                int hour_half = hour * 2;

                for (int i = 0; i < hour_half; i++)
                {
                    ws.Column(i + 4).Width = 1.5;
                }

                int i_date = 0;
                for (int i = 4; i < hour_half + 4; i++)
                {
                    string t1 = ShareFn.GetExcelColumnName(i) + "3";
                    i += 47;
                    string t2 = ShareFn.GetExcelColumnName(i) + "3";
                    ws.Cells[t1 + ":" + t2].Merge = true;
                    ws.Cells[t1 + ":" + t2].Value = list_dates[i_date].ToString("dd MMM yyyy");
                    i_date++;
                }

                int ii = 0;
                string last_cell = "";
                for (int i = 0; i < hour_half; i += 2)
                {
                    string t_cell = ShareFn.GetExcelColumnName(i + 4) + "4:" + ShareFn.GetExcelColumnName(i + 5) + "4";
                    last_cell = ShareFn.GetExcelColumnName(i + 5) + "4";
                    ws.Cells[t_cell].Merge = true;
                    ws.Cells[t_cell].Value = ii;
                    ii++;
                    if (ii == 24)
                    {
                        ii = 0;
                    }
                }

                AllBorders(ws.Cells["A3:" + last_cell].Style.Border);
                for (int j = 0; j < model.sch_act_time_line_excel.Count; j++)
                {
                    for (int i = 0; i < hour_half; i += 2)
                    {
                        string t_cell = ShareFn.GetExcelColumnName(i + 5) + (j + 5);
                        if (j == model.sch_act_time_line_excel.Count - 1)
                        {
                            ws.Cells[t_cell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                            t_cell = ShareFn.GetExcelColumnName(i + 4) + (j + 5) + ":" + ShareFn.GetExcelColumnName(i + 5) + (j + 5);
                            ws.Cells[t_cell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        else
                        {
                            ws.Cells[t_cell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                    }
                }

                for (int i = 0; i < model.sch_act_time_line_excel.Count; i++)
                {
                    int idx_row = i + 5;
                    ws.Cells["A" + idx_row].Value = model.sch_act_time_line_excel[i].act_name;
                    ws.Cells["A" + idx_row].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells["A" + idx_row].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    ws.Cells["A" + idx_row].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                    ws.Cells["B" + idx_row].Value = model.sch_act_time_line_excel[i].time_start;
                    ws.Cells["B" + idx_row].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                    ws.Cells["C" + idx_row].Value = model.sch_act_time_line_excel[i].time_end;
                    ws.Cells["C" + idx_row].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                    if (i == model.sch_act_time_line_excel.Count - 1)
                    {
                        ws.Cells["A" + idx_row].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells["B" + idx_row].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        ws.Cells["C" + idx_row].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    }

                    DateTime time_from = DateTime.ParseExact(model.sch_act_time_line_excel[i].time_start, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    DateTime time_to = DateTime.ParseExact(model.sch_act_time_line_excel[i].time_end, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                    int time_line_start = (Convert.ToInt32((time_from - d1).TotalMinutes) / 30) + 4;

                    int time_line_end = 0;
                    int minutes_end = Convert.ToInt32((time_to - d1).TotalMinutes);
                    int ttt = minutes_end % 30;

                    if (time_to.Minute > 30)
                    {
                        time_line_end = (minutes_end / 30) + 4;
                    }
                    else
                    {
                        if ((minutes_end % 30) != 0)
                        {
                            time_line_end = (minutes_end / 30) + 4;
                        }
                        else
                        {
                            time_line_end = (minutes_end / 30) + 3;
                        }
                    }

                    if(time_line_start > time_line_end)
                    {
                        string t_cell = ShareFn.GetExcelColumnName(time_line_start) + idx_row;
                        ws.Cells[t_cell].Merge = true;
                        ws.Cells[t_cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells[t_cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(model.sch_act_time_line_excel[i].act_color));
                    }
                    else
                    {
                        string t_cell = ShareFn.GetExcelColumnName(time_line_start) + idx_row + ":" + ShareFn.GetExcelColumnName(time_line_end) + idx_row;
                        ws.Cells[t_cell].Merge = true;
                        ws.Cells[t_cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells[t_cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(model.sch_act_time_line_excel[i].act_color));
                    }
                }
                package.Save();
            }

            if (type == "Pdf")
            {
                ReturnValue rtn = SaveAsActPdf(file_path);
                Response.Redirect(string.Format("~/Web/FileUpload/FREIGHT/{0}", file_namePdf));
            }
            else // EXCEL
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
                Response.AddHeader("content-type", "application/Excel");
                Response.WriteFile(file_path);
                Response.End();
            }

            return View();
        }

        private void AllBorders(OfficeOpenXml.Style.Border _borders)
        {
            _borders.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        }

        private string getIndexColumn(int pnIndex)
        {
            string rs = "";
            int nChar = (64 + pnIndex);
            if (nChar > 90)
            {
                rs = "A" + ((char)(nChar - 26)).ToString();
            }
            else
            {
                rs = ((char)nChar).ToString();
            }

            return rs;
        }

        private string LoadJsonFromTest()
        {
            string PathTest = @"D:\JSON\ACT.json";
            StreamReader _sr = new StreamReader(PathTest);
            return _sr.ReadToEnd();


        }

        private ReturnValue SaveAsPdf(string saveAsLocation)
        {
            ReturnValue rtn = new ReturnValue();
            string saveas = (saveAsLocation.Split('.')[0]) + ".pdf";
            try
            {
                Workbook workbook = new Workbook();
                workbook.LoadDocument(saveAsLocation, DocumentFormat.OpenXml);

                using (FileStream pdfFileStream = new FileStream(saveas, FileMode.Create))
                {
                    foreach (var worksheet in workbook.Worksheets)
                    {
                        //worksheet.ActiveView.ShowHeadings = true;
                        worksheet.ActiveView.Orientation = PageOrientation.Landscape;
                        worksheet.ActiveView.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        worksheet.ActiveView.Margins.Left = 100;
                        worksheet.ActiveView.Margins.Right = 100;
                        worksheet.ActiveView.Margins.Bottom = 100;
                        worksheet.ActiveView.Margins.Top = 100;
                        worksheet.PrintOptions.FitToPage = true;
                    }
                    workbook.ExportToPdf(pdfFileStream);
                }

                rtn.Status = true;
                rtn.Message = saveas;
                return rtn;
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
                return rtn;
            }
        }

        private ReturnValue SaveAsActPdf(string saveAsLocation)
        {
            ReturnValue rtn = new ReturnValue();
            string saveas = (saveAsLocation.Split('.')[0]) + ".pdf";
            try
            {
                Workbook workbook = new Workbook();
                workbook.LoadDocument(saveAsLocation, DocumentFormat.OpenXml);

                using (FileStream pdfFileStream = new FileStream(saveas, FileMode.Create))
                {
                    foreach (var worksheet in workbook.Worksheets)
                    {
                        //worksheet.ActiveView.ShowHeadings = true;
                        worksheet.ActiveView.Orientation = PageOrientation.Landscape;
                        worksheet.ActiveView.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        worksheet.ActiveView.Margins.Left = 100;
                        worksheet.ActiveView.Margins.Right = 125;
                        worksheet.ActiveView.Margins.Bottom = 100;
                        worksheet.ActiveView.Margins.Top = 100;
                        worksheet.PrintOptions.PrintTitles.SetColumns(0, 2);
                        worksheet.PrintOptions.Scale = 65;
                        //worksheet.HorizontalPageBreaks.Add(3);
                        //worksheet.PrintOptions.PrintTitles.Clear();
                        //worksheet.PrintOptions.FitToPage = true;
                    }
                    workbook.ExportToPdf(pdfFileStream);
                }

                rtn.Status = true;
                rtn.Message = saveas;
                return rtn;
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
                return rtn;
            }
        }

        [HttpPost]
        public string getSchedulerDetail(string month_year)
        {
            return SchedulerServiceModel.getSchedulerDetail(month_year);
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }
        }
    }
}