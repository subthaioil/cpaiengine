﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class SpecController : BaseController
    {
        // GET: CPAIMVC/Spec
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Spec");
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                SpecViewModel model = initialModel();
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Spec" }));
            }
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Create(FormCollection frm, SpecViewModel viewModel)
        {
            SpecServiceModel serviceModel = new SpecServiceModel();
            ReturnValue rtn = new ReturnValue();
            ResponseData resfile = SaveFile(form: ref frm, model: ref viewModel);
            rtn = serviceModel.Add(viewModel.specViewModel_Detail, lbUserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            SpecViewModel model = initialModel();
            model.specViewModel_Detail.RowId = viewModel.specViewModel_Detail.RowId;
            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            SpecViewModel model = initialModel();
            return View(model);
        }


        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SearchResult")]
        public ActionResult SearchResult(SpecViewModel postModel)
        {
            SpecViewModel model = initialModel();
            SpecServiceModel serviceModel = new SpecServiceModel();
            SpecViewModel_Search searchModel = new SpecViewModel_Search();

            searchModel = postModel.spec_Search;
            serviceModel.Search(ref searchModel);
            model.spec_Search = searchModel;
            return View("Search", model);
        }

        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "DeleteResult")]
        public ActionResult DeleteResult(SpecViewModel viewModel)
        {
            if (ViewBag.action_edit)
            {
                SpecServiceModel serviceModel = new SpecServiceModel();

                ReturnValue rtn = new ReturnValue();
                rtn = serviceModel.Delete(viewModel.specViewModel_Detail, lbUserName);

                SpecViewModel model = initialModel();
                SpecViewModel_Search searchModel = new SpecViewModel_Search();

                searchModel = viewModel.spec_Search;
                serviceModel.Search(ref searchModel);
                model.spec_Search = searchModel;

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                return View("Search", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "CamTemplate" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string rowId)
        {
            if (ViewBag.action_edit)
            {
                SpecServiceModel serviceModel = new SpecServiceModel();

                SpecViewModel model = initialModel();
                model.specViewModel_Detail = serviceModel.GetSpecDetail(rowId);
                

                for(int i=0;i< model.specViewModel_Detail.SpecNote.Count(); i++)
                {
                    string a = model.specViewModel_Detail.SpecNote[i].Image;
                    if(a!=null && a.Any())
                    {
                        model.specViewModel_Detail.SpecNote[i].filename = a.Split('|');
                    }
                    else
                    {
                        model.specViewModel_Detail.SpecNote[i].filename = null;
                    }
                    
                }

                if (model.specViewModel_Detail.SpecNote.Count() == 0)
                {
                    model.specViewModel_Detail.SpecNote = new List<Spec_Note>();
                    model.specViewModel_Detail.SpecNote.Add(new Spec_Note());
                }
                if (model.specViewModel_Detail.SpecDetail.Count() == 0)
                {
                    model.specViewModel_Detail.SpecDetail = new List<SpecDetail>();
                    model.specViewModel_Detail.SpecDetail.Add(new SpecDetail());
                }

                TempData["rowId"] = rowId;
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Spec" }));
            }
        }


        public ActionResult getImage(string[] filename)
        {
            return null;
        }




        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Edit(FormCollection frm, SpecViewModel model)
        {
            if (ViewBag.action_edit)
            {
                SpecServiceModel serviceModel = new SpecServiceModel();

                ResponseData resfile = SaveFile(form: ref frm, model:ref model);

                ReturnValue rtn = new ReturnValue();
                var rowId = model.specViewModel_Detail.RowId;
                if (serviceModel.IsUsed(rowId, true))
                {
                    rtn = serviceModel.Add(model.specViewModel_Detail, lbUserName);
                    TempData["ConfirmMessage"] = "Do you sure to continue update all related Final CAM?";
                    ViewBag.action_edit = true;
                    ViewBag.actionName = "EDIT";
                }
                else if (serviceModel.IsUsed(rowId, false))
                {
                    rtn = serviceModel.Add(model.specViewModel_Detail, lbUserName);
                    ViewBag.action_edit = true;
                    ViewBag.actionName = "EDIT";
                }
                else
                {
                    model.specViewModel_Detail.RowId = rowId;
                    rtn = serviceModel.Edit(model.specViewModel_Detail, lbUserName);
                }

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["rowId"] = rowId;

                model.ddl_Material = DropdownServiceModel.getMaterial();
                model.ddl_Country = DropdownServiceModel.getCountryLANDX();
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Spec" }));
            }
        }

        [HttpPost, ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Delete")]
        public ActionResult Delete(FormCollection frm, SpecViewModel model)
        {
            if (ViewBag.action_edit)
            {
                SpecServiceModel serviceModel = new SpecServiceModel();
                ResponseData resfile = SaveFile(form: ref frm, model: ref model);
                ReturnValue rtn = new ReturnValue();
                var rowId = model.specViewModel_Detail.RowId;
                rtn = serviceModel.Delete(model.specViewModel_Detail, lbUserName);                
                if (serviceModel.IsUsed(rowId))
                {
                    TempData["rowId"] = rowId;
                }
                else
                {
                    TempData["rowId"] = rtn.newID;
                }
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                model.ddl_Material = DropdownServiceModel.getMaterial();
                model.ddl_Country = DropdownServiceModel.getCountryLANDX();
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Spec" }));
            }
        }

        [HttpGet]
        public ActionResult View(string rowId)
        {
            if (ViewBag.action_view)
            {
                SpecServiceModel serviceModel = new SpecServiceModel();

                SpecViewModel model = initialModel();
                model.specViewModel_Detail = serviceModel.GetSpecDetail(rowId);
                TempData["rowId"] = rowId;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Spec" }));
            }

        }


        public SpecViewModel initialModel()
        {
            SpecViewModel model = new SpecViewModel();

            model.spec_Search = new SpecViewModel_Search();
            model.spec_Search.sSearchData = new List<SpecViewModel_SearchData>();

            model.specViewModel_Detail = new SpecViewModel_Detail();
            //model.specViewModel_Detail.SpecNote = new List<Spec_Note>();
            model.ddl_Material = DropdownServiceModel.getMaterial();
            model.ddl_Country = DropdownServiceModel.getCountryLANDX(); //อย่าลืมแก้ Country
            return model;
        }



        private ResponseData SaveFile(ref FormCollection form, ref SpecViewModel model)
        {
            ResponseData resFile = new ResponseData();
            try
            {
                if (model.specViewModel_Detail.SpecNote != null && model.specViewModel_Detail.SpecNote.Any())
                {
                    for (int i = 0; i < model.specViewModel_Detail.SpecNote.Count; i++)
                    {
                        if (Request.Files != null && Request.Files.Count > 0)
                        {
                            List<HttpPostedFileBase> fileCollection = new List<HttpPostedFileBase>();
                            List<string> fileUploaded = new List<string>();
                            for (int x = 0; x < Request.Files.Count; x++)
                            {                              
                                if (Request.Files.Keys[x] == "specViewModel_Detail.SpecNote[" + i + "].Image")
                                {                                  
                                   fileUploaded.Add(UploadFile(Request.Files[x]));                                      
                                }
                            }
                            if(fileUploaded.Count()>0 && fileUploaded[0] != "")
                            {
                                model.specViewModel_Detail.SpecNote[i].Image = string.Join("|", fileUploaded);
                            }
                            else
                            {
                                model.specViewModel_Detail.SpecNote[i].Image = model.specViewModel_Detail.SpecNote[i].Image;
                            }
                            
                        }
                    }
                }              
            }
            catch (Exception ex)
            {
                resFile.response_message = ex.Message;
            }
            return resFile;
        }




        private string UploadFile(HttpPostedFileBase requestFile)
        {
            if (Request != null)
            {
                var file = requestFile;

                if (file != null && file.ContentLength > 0)
                {
                    var count = file.ContentLength;
                    
                    var fileName = Path.GetFileName(file.FileName);                          
                    string rootPath = Request.PhysicalApplicationPath;
                    string subPath = "Web/FileUpload/SPEC";
                    fileName = string.Format(Path.GetFileName(file.FileName));
                    if (!Directory.Exists(rootPath + subPath))
                        Directory.CreateDirectory(rootPath + subPath);
                    var path = Path.Combine(Server.MapPath("~/" + subPath), fileName);
                    file.SaveAs(path);
                    return fileName;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        [HttpGet]
        public JsonResult CheckFinalCAM(string rowID)
        {
            SpecServiceModel serviceModel = new SpecServiceModel();
            bool result = serviceModel.IsUsed(rowID, true);            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateFinalCAM(string rowID)
        {
            SpecServiceModel serviceModel = new SpecServiceModel();
            var result = true;
            Task.Factory.StartNew(() =>
            {
                serviceModel.GenerateSpecComparison();
            });
            TempData["ReponseMessage"] = "System is updating Final CAM documents, Please waiting for proccess.";
            TempData["ReponseStatus"] = true;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}