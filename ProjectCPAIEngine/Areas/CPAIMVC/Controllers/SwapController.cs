﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class SwapController : BaseController    
    {
        // GET: CPAIMVC/Swap
        public ActionResult index()
        {
            return RedirectToAction("Search", "Swap");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(SwapViewModel pModel)
        {
            SwapServiceModel serviceModel = new SwapServiceModel();
            ReturnValue rtn = serviceModel.SearchData(ref pModel);
            
            TempData["SwapViewModel"] = pModel;
            return RedirectToAction("Search");            
        }

        [HttpGet]
        public ActionResult Search()
        {
            SwapServiceModel serviceModel = new SwapServiceModel();
            SwapViewModel model = new SwapViewModel();
            if (TempData["SwapViewModel"] == null)
            {
                model = new SwapViewModel();
                TempData["SwapViewModel"] = model;
            }
            else
            {
                model = TempData["SwapViewModel"] as SwapViewModel;
                TempData["SwapViewModel"] = null;
            }
            initializeSearch(ref model);
            return View("Search", model);
        }

        [HttpGet]
        public ActionResult Create_Swap()
        {
            CrudeImportPlanController serviceModel = new CrudeImportPlanController();
            CrudeImportPlanViewModel model = new CrudeImportPlanViewModel();
            if (TempData["SwapViewModel"] == null)
            {
                model = new CrudeImportPlanViewModel();
                TempData["SwapViewModel"] = model;
            }
            else
            {
                model = TempData["SwapViewModel"] as CrudeImportPlanViewModel;
                TempData["SwapViewModel"] = null;
            }
            model = serviceModel.initialModel();
            return View("~/Areas/CPAIMVC/Views/CrudeImportPlan/Create_Swap.cshtml", model);
        }
        

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create()
        {
            SwapServiceModel serviceModel = new SwapServiceModel();
            SwapViewModel model = new SwapViewModel();

            initializeCreate(ref model, SwapViewModel.Mode.Create);
            return View("Create", model);
        }        

        //[HttpGet]
        //public ActionResult Edit(string pTripNo, string pItemNo, string pCrudeTypeID)
        //{
        //    SwapServiceModel serviceModel = new SwapServiceModel();
        //    SwapViewModel model = new SwapViewModel();
        //    model.Swap_Detail.trip_no = pTripNo;
        //    model.Swap_Detail.item_no = pItemNo;
        //    model.Swap_Detail.crude_type_id = pCrudeTypeID;
        //    serviceModel.LoadData(ref model);

        //    initializeCreate(ref model, SwapViewModel.Mode.Edit);
        //    return View("Create", model);
        //}

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(SwapViewModel pModel)
        {
            SwapServiceModel serviceModel = new SwapServiceModel();
            SwapViewModel model = new SwapViewModel();
            model = pModel;
            model.Swap_Detail = new List<SwapViewModel_Detail>();
            if(model.Swap_Search.SearchData != null)
            {
                foreach(var i in model.Swap_Search.SearchData)
                {
                    if(i.dIsSelect)
                    {
                        SwapViewModel_Detail item = new SwapViewModel_Detail();
                        item.trip_no = i.dTripNo;
                        item.item_no = i.dItemNo;
                        item.crude_type_id = i.dCrudeTypeID;
                        model.Swap_Detail.Add(item);
                    }
                    
                }
            }
            serviceModel.LoadData(ref model);

            initializeCreate(ref model, SwapViewModel.Mode.Edit);
            return View("Create", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Import")]
        public ActionResult Import(SwapViewModel pModel)
        {
            SwapServiceModel serviceModel = new SwapServiceModel();
            ReturnValue rtn = serviceModel.ImportData(ref pModel);

            initializeCreate(ref pModel);
            return View("Create", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Save(SwapViewModel pModel)
        {
            SwapServiceModel serviceModel = new SwapServiceModel();
            ReturnValue rtn = serviceModel.SaveData(ref pModel);            
            if (rtn != null)
            {                
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                pModel.PageMode = SwapViewModel.Mode.View;
            }
            initializeCreate(ref pModel);
            return View("Create", pModel);
        }
        
        private void initializeCreate(ref SwapViewModel model, SwapViewModel.Mode mode = SwapViewModel.Mode.None)
        {
            if (mode != SwapViewModel.Mode.None)
            {
                model.PageMode = mode;
            }            
            model.Swap_Detail = model.Swap_Detail ?? new List<SwapViewModel_Detail>();
            //model.Swap_Detail.prices = model.Swap_Detail.prices ?? new List<SwapViewModel_Price>();
            model.Swap_Import = model.Swap_Import ?? new SwapViewModel_Import();
            model.Swap_Import.ImportData = model.Swap_Import.ImportData ?? new List<SwapViewModel_ImportData>();
            model.ddl_Customer = SwapServiceModel.getCustomer();
            model.ddl_Product = SwapServiceModel.getMaterial();
            model.ddl_Vessel = SwapServiceModel.getVehicle("PRODUCT");
            model.ddl_Plant = SwapServiceModel.getPlant();
            model.ddl_PriceStatus = SwapServiceModel.getPriceStatus();
            model.ddl_Unit = SwapServiceModel.getUnit();
            model.ddl_UnitPrice = SwapServiceModel.getUnitPrice();
            model.ddl_UnitInvoice = SwapServiceModel.getUnitInvoice();
            model.ddl_UnitTotal = SwapServiceModel.getUnitTotal();
        }

        private void initializeSearch(ref SwapViewModel model)
        {
            model.ddl_Customer = SwapServiceModel.getCustomer();
            model.ddl_Product = SwapServiceModel.getMaterial();
            model.ddl_CrudeName = CrudeImportPlanServiceModel.getMaterial();
            model.json_Crude = CrudeImportPlanServiceModel.GetDataToJSON_TextValue(model.ddl_CrudeName);
            model.json_CrudeAuto = CrudeImportPlanServiceModel.GetDataToJSON_ValueText(model.ddl_CrudeName);
        }
    }
}