﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class UnitController : BaseController
    {
        // GET: CPAIMVC/Unit
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Unit");
        }

        [HttpGet]
        public ActionResult Search()
        {
            UnitViewModel model = initialModel();
            UnitServiceModel serviceModel = new UnitServiceModel();
            UnitViewModel_Search searchModel = new UnitViewModel_Search();

            serviceModel.Search(ref searchModel);
            model.unit_Search = searchModel;
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(UnitViewModel postModel)
        { 
            UnitViewModel model = initialModel();

            UnitServiceModel serviceModel = new UnitServiceModel();
            UnitViewModel_Search searchModel = new UnitViewModel_Search();

            searchModel = postModel.unit_Search;
            serviceModel.Search(ref searchModel);
            model.unit_Search = searchModel;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                UnitViewModel model = initialModel();
                if(model.unit_Detail.CreateType == ""||model.unit_Detail.CreateType == null)
                {
                    model.unit_Detail.CreateType = "CPAI";
                    model.unit_Detail.UnitCode = "";
                }

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Unit" }));
            }
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, UnitViewModel viewModel)
        {
            if (ViewBag.action_create)
            {
                UnitServiceModel serviceModel = new UnitServiceModel();
                ReturnValue rtn = new ReturnValue();
               // UnitViewModel model = initialModel();
                viewModel.unit_Detail.UnitStatus = viewModel.unit_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                viewModel.unit_Detail.UnitProcessConfirm = viewModel.unit_Detail.BoolProcessConfrim == true ? "YES" : "NO";
                rtn = serviceModel.Add(viewModel.unit_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                viewModel.ddl_UserGRoup = DropdownServiceModel.getUserGroup();
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Unit" }));
            }
        }


        [HttpGet]
        public ActionResult Edit(string unitCode)
        {
            if (ViewBag.action_edit)
            {
                UnitServiceModel serviceModel = new UnitServiceModel();

                UnitViewModel model = initialModel();
                model.unit_Detail = serviceModel.Get(unitCode);          

                TempData["unitCode"] = unitCode;
                if (serviceModel.IsUsed(unitCode))
                {
                    ViewBag.CanEdit = false;
                }
                else
                {
                    ViewBag.CanEdit = true;
                }
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Unit" }));
            }
        }
  

        [HttpPost]
        public ActionResult Edit(string unitCode, UnitViewModel model)
        {
            if (ViewBag.action_edit)
            {
                UnitServiceModel serviceModel = new UnitServiceModel();
               
                ReturnValue rtn = new ReturnValue();
                model.unit_Detail.UnitStatus = model.unit_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                model.unit_Detail.UnitProcessConfirm = model.unit_Detail.BoolProcessConfrim == true ? "YES" : "NO";
                model.unit_Detail.UnitCode = unitCode;

                var a = model.ddl_UserGRoup;
                rtn = serviceModel.Edit(model.unit_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["UnitCode"] = unitCode;
                ViewBag.CanEdit = true;
                UnitViewModel initModel = initialModel();
                model.ddl_UserGRoup = DropdownServiceModel.getUserGroup();
                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Unit" }));
            }
           
        }


        [HttpGet]
        public ActionResult View(string unitCode)
        {
            if (ViewBag.action_view)
            {
                UnitServiceModel serviceModel = new UnitServiceModel();

                UnitViewModel model = initialModel();
                model.unit_Detail = serviceModel.Get(unitCode);
                TempData["UnitCode"] = unitCode;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Unit" }));
            }

        }


        public UnitViewModel initialModel()
        {
            UnitServiceModel serviceModel = new UnitServiceModel();
            UnitViewModel model = new UnitViewModel();

            model.unit_Search = new UnitViewModel_Search();
            model.unit_Search.sSearchData = new List<UnitViewModel_SeachData>();

            model.unit_Detail = new UnitViewModel_Detail();
            model.unit_Detail.BoolStatus = true;
            model.unit_Detail.BoolProcessConfrim = false;

            model.ddl_UnitStatus = DropdownServiceModel.getMasterStatus();
            model.ddl_UserGRoup = DropdownServiceModel.getUserGroup();

            return model;
        }
    }
}