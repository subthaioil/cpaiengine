﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Model;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;
using System.Globalization;
using ProjectCPAIEngine.DAL;
using com.pttict.engine.dal.Entity;
 
namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class VCoolController : BaseController
    {
        public List<ButtonAction> ButtonListVCool
        {
            get
            {
                if (Session["ButtonListVCool"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListVCool"];
            }
            set { Session["ButtonListVCool"] = value; }
        }
        ShareFn _FN = new ShareFn();
        const string returnPage = "/web/MainBoards.aspx";
        const string JSON_COOL_CRUDE = "JSON_COOL_CRUDE";
        const string JSON_CRUDE_PURCHASE = "JSON_CRUDE_PURCHASE";

        // GET: CPAIMVC/VCool
        public ActionResult Index()
        {
            VCoolViewModel model;

            if (TempData["VCoolViewModel"] == null)
            {
                model = new VCoolViewModel();
                model.pageMode = "EDIT"; //PageMode is EDIT, EDIT_REASON or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL

                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataVCool(tranID, ref model);
                    initializeDropDownList(ref model);
                    model.ddlCountry = GetCountryRef(model.crude_info.crude_name);

                    if (resData != null)
                    {
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            else
                            {
                                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            }
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                }
                else
                {
                    initializeDropDownList(ref model);
                    model.requester_info = new Model.VcoRequesterInfo();
                    model.requester_info.name = Const.User.UserName;
                    model.requester_info.area_unit = Const.User.UserGroup;
                    model.requester_info.workflow_status = ConstantPrm.ACTION.DRAFT;
                    model.requester_info.date_purchase = String.Format("{0:" + VCoolViewModel.FORMAT_DATETIME + "}", DateTime.Now);
                }
            }
            else
            {
                model = TempData["VCoolViewModel"] as VCoolViewModel;
                initializeDropDownList(ref model);
                TempData["VCoolViewModel"] = null;
            }


            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("/", units);
                }
            }

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = MaterialsServiceModel.getMaterialsDDLVCool();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            return View(model);
        }

        public ActionResult Search()
        {
            VCoolSearchViewModel model;
            var units = VCoolServiceModel.getUserGroup(lbUserID);
            if (TempData["VCoolSearchViewModel"] == null)
            {
                model = new VCoolSearchViewModel();
                if (!string.IsNullOrEmpty(lbUserID))
                {
                    if (units != null && units.Any())
                    {
                        Const.User.UserGroup = string.Join("|", units);
                    }
                }

                List_VCooltrx res = SearchVCoolData();
                if (res != null)
                {
                    model.VCoolTransaction = res.VCoolTransaction.OrderByDescending(x => x.transaction_id).ThenBy(x => x.products).ToList();
                    foreach (var item in model.VCoolTransaction)
                    {
                        if (!string.IsNullOrEmpty(item.date_purchase))
                        {
                            if (item.date_purchase.Contains("/"))
                            {
                                string datetimeString = string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(item.date_purchase, "dd/MM/yyyy", null, DateTimeStyles.None));
                                item.date_purchase = datetimeString;
                            }
                        }
                        
                        item.Transaction_id_Encrypted = item.transaction_id.Encrypt();
                        item.Req_transaction_id_Encrypted = item.req_transaction_id.Encrypt();
                    }
                }
            }
            else
            {
                model = TempData["VCoolSearchViewModel"] as VCoolSearchViewModel;
                TempData["VCoolSearchViewModel"] = null;
            }
            if (units.Contains("CMCS"))
            {
                model.pageMode = "EDIT";
            }
            else
            {
                model.pageMode = "VIEW";
            }
            return View(model);
        }

        public ActionResult FinalCAM()
        {
            string tempFinalCAM = string.Empty;
            if (Request.QueryString["tempFinalCAM"] != null)
            {
                tempFinalCAM = Request.QueryString["tempFinalCAM"].ToString().Decrypt();                
            }
            return View("FinalCAM", "", tempFinalCAM);
        }


        [HttpPost]
        public ActionResult SearchResult(FormCollection fm, VCoolSearchViewModel model)
        {
            string userGroup = Const.User.UserGroup;
            string system = ConstantPrm.SYSTEM.VCOOL;
            string type = ConstantPrm.SYSTEMTYPE.CRUDE;

            if (!string.IsNullOrEmpty(model.loading_date))
            {
                string[] tempDate = model.loading_date.Split(' ');
                model.loading_from = tempDate[0];
                model.loading_to = tempDate[2];
            }

            List_VCooltrx res = SearchVCoolData(
                                system: system,
                                type: type,
                                products: model.crude_name,
                                origin: model.origin,
                                tpc_plan_month: model.tpc_plan_month.ToUpper() == "ALL" ? "" : model.tpc_plan_month,
                                tpc_plan_year: model.tpc_plan_year.ToUpper() == "ALL" ? "" : model.tpc_plan_year,
                                loading_from: model.loading_from,
                                loading_to: model.loading_to
                                );

            model.VCoolTransaction = res.VCoolTransaction.OrderByDescending(x => x.transaction_id).ToList();


            foreach (var item in model.VCoolTransaction)
            {
                if (!string.IsNullOrEmpty(item.date_purchase))
                {
                    if (item.date_purchase.Contains("/"))
                    {
                        string datetimeString = string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(item.date_purchase, "dd/MM/yyyy", null, DateTimeStyles.None));
                        item.date_purchase = datetimeString;
                    }
                }
                item.Transaction_id_Encrypted = item.transaction_id.Encrypt();
                item.Req_transaction_id_Encrypted = item.req_transaction_id.Encrypt();
            }

            TempData["VCoolSearchViewModel"] = model;
            return RedirectToAction("Search", "VCool");
        }

        private List_VCooltrx SearchVCoolData(string system = "", string type = "", string action = "", string date_purchase = "", string purchase_no = "", string create_by = "", string products = "", string origin = "", string tpc_plan_month = "", string tpc_plan_year = "", string loading_from = "", string loading_to = "")
        {
            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("/", units);
                }
                else
                {
                    Const.User.UserGroup = string.Empty;
                }
            }

            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000061;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();

            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = Const.User.UserGroup.Contains("TN") ? VCOOL_EXPERT_SH_FUNCTION_CODE : Const.User.UserGroup.Contains("SC") && !Const.User.UserGroup.Contains("EP") ? VCOOL_EXPERT_FUNCTION_CODE : VCOOL_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.VCOOL });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = products });
            req.Req_parameters.P.Add(new P { K = "index_11", V = origin });
            req.Req_parameters.P.Add(new P { K = "index_15", V = tpc_plan_month });
            req.Req_parameters.P.Add(new P { K = "index_16", V = tpc_plan_year });
            req.Req_parameters.P.Add(new P { K = "index_17", V = loading_from });
            req.Req_parameters.P.Add(new P { K = "index_18", V = loading_to });

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();


            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_VCooltrx _model = ShareFunction.DeserializeXMLFileToObject<List_VCooltrx>(_DataJson);
            if (_model == null)
            {
                _model = new List_VCooltrx();
            }

            if (_model.VCoolTransaction == null)
            {
                _model.VCoolTransaction = new List<VCoolEncrypt>();
            }
            else
            {
                foreach (var VCoolTransaction in _model.VCoolTransaction)
                {
                    VCoolTransaction.status_description = VCoolServiceModel.getWorkflowStatusDescription(VCoolTransaction.status);
                }
            }
            return _model;
        }

        private ResponseData LoadDataVCool(string TransactionID, ref VCoolViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000062;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<VCoolViewModel>(_model.data_detail);
                if (model.crude_info != null)
                {
                    if (!string.IsNullOrEmpty(model.crude_info.discharging_date_from) && !string.IsNullOrEmpty(model.crude_info.discharging_date_to))
                    {
                        model.tempDischargingDate = model.crude_info.discharging_date_from + " to " + model.crude_info.discharging_date_to;
                    }
                    if (!string.IsNullOrEmpty(model.crude_info.loading_date_from) && !string.IsNullOrEmpty(model.crude_info.loading_date_to))
                    {
                        model.tempLoadingDate = model.crude_info.loading_date_from + " to " + model.crude_info.loading_date_to;
                    }

                    if (!string.IsNullOrEmpty(model.crude_info.origin) && !string.IsNullOrEmpty(model.crude_info.crude_name))
                    {
                        model.tempFinalCAM = CoolServiceModel.getLatestFinalCAM(model.crude_info.crude_name, model.crude_info.origin);
                        model.tempFinalCAM_Encrypt = model.tempFinalCAM.Encrypt();
                    }
                }
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }

            string _btnBack = "<input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("btnBack") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" />";
            model.buttonCode += _btnBack;

            return resData;
        }

        public ActionResult Report()
        {
            VCoolReportViewModel model = null;
            if (TempData.ContainsKey("VCoolReportViewModel") && TempData["VCoolReportViewModel"] != null)
            {
                model = TempData["VCoolReportViewModel"] as VCoolReportViewModel;
            }
            else
            {
                model = new VCoolReportViewModel
                {
                    ActualPurchases = getActualPurchase(),
                    CrudeNames = DropdownServiceModel.getVCoolCrudeName(),
                    Countries = DropdownServiceModel.getCountryCrudePurchase(),
                    Suppliers = DropdownServiceModel.getVendorFreight(FEED_TYPE),
                };
            }
            TempData["VCoolReportViewModel"] = null;
            return View(model);
        }

        [HttpPost]
        public ActionResult ReportResult(VCoolReportViewModel model)
        {
            var suppliers = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            try
            {
                model.ActualPurchases = getActualPurchase();
                model.CrudeNames = DropdownServiceModel.getVCoolCrudeName();
                model.Countries = DropdownServiceModel.getCountryCrudePurchase();
                model.Suppliers = suppliers;

                var criteria = model.Criteria.Clone();
                var vcoolReports = new VCoolServiceModel().GetVCoolReport(criteria);

                //Convert supplierId to string
                var suppliersDictionary = suppliers.ToDictionary(c => c.Value, c => c.Text);
                vcoolReports.ForEach(c =>
                {
                    if (!string.IsNullOrEmpty(c.Supplier) && suppliersDictionary.ContainsKey(c.Supplier))
                    {
                        c.Supplier = suppliersDictionary[c.Supplier];
                    }
                });
                model.VCoolReportResults = vcoolReports.OrderBy(c => c.TransactionID).ToList();

                TempData["VCoolReportViewModel"] = model;
            }
            catch (Exception ex)
            {
                if (ModelState.IsValid)
                {
                    model.ErrorMessage = GetErrorHtmlMessage(ex);
                    TempData["VCoolReportViewModel"] = model;
                }
                else
                {
                    TempData["VCoolReportViewModel"] = new VCoolReportViewModel { ErrorMessage = ex.Message };
                }
            }
            return RedirectToAction("Report", "VCool");
        }

        [HttpGet]
        public JsonResult LoadTNComment(string transactionId)
        {
            try
            {
                if (string.IsNullOrEmpty(transactionId))
                    throw new ArgumentNullException(nameof(transactionId));

                var vcoComment = new DAL.DALVCOOL.VCO_COMMENT_DAL().FirstOrDefault(c => c.VCCO_FK_VCO_DATA.Equals(transactionId))?.VCCO_TN ?? string.Empty;
                return Json(new { success = true, comment = vcoComment, message = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, comment = string.Empty, message = GetErrorHtmlMessage(ex) }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetErrorHtmlMessage(Exception ex)
        {
            var inner = ex.FromHierarchy(c => c.InnerException).LastOrDefault();
            var sb = new System.Text.StringBuilder();
            sb.Append("<h3>Error has occurred.</h3>");
            sb.AppendFormat("<span>Source : {0}</span><br/>", inner?.Source);
            sb.AppendFormat("<span>HResult: {0}</span><br/>", inner?.HResult);
            sb.AppendFormat("<span>Message: {0}</span><br/>", inner?.Message);
            return sb.ToString();
        }

        public ActionResult LPRun(string TranID, string Tran_Req_ID, string PURNO)
        {
            VCoolViewModel model = new VCoolViewModel();
            model.pageMode = "EDIT";
            model.buttonMode = "AUTO";
            if (TranID != null)
            {
                ResponseData resData = LoadDataVCool(TranID.Decrypt(), ref model);
                if (resData != null)
                {
                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        if (resData.result_code == "10000033")
                        {
                            TempData["res_message"] = "This document has been changed by others. Please check it again.";
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        }
                        TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                    }
                }
            }
            else
            {
                model.requester_info = new Model.VcoRequesterInfo();
                model.requester_info.name = Const.User.UserName;
                model.requester_info.area_unit = Const.User.UserGroup;
                model.requester_info.workflow_status = ConstantPrm.ACTION.DRAFT;
                model.requester_info.date_purchase = DateTime.Now.ToString(VCoolViewModel.FORMAT_DATETIME);
            }

            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("/", units);
                }
            }

            List<string> userGroups = VCoolServiceModel.getUserGroup(lbUserID);
            if(userGroups.Count > 1)
            {
                if (userGroups.Contains("SCEP"))
                {
                    TempData["isSCEP_SH"] = false;
                }
            }
            else
            {
                TempData["isSCEP_SH"] = userGroups.Contains("SCEP_SH");
            }            

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            initializeDropDownList(ref model);
            model.crude_compare.yield = model.crude_compare.yield.OrderBy(x => x.order).ToList();
            List<string> comparison_list = new List<string>();
            if (model.crude_compare != null && model.crude_compare.comparison_json != null && model.crude_compare.comparison_json.Any())
            {
                foreach (var item in model.crude_compare.comparison_json)
                {
                    comparison_list.Add(item.selectedCrude.ToUpper());
                }

                model.SelectedCrudeNameList = model.ddlCrudeName.Where(x => comparison_list.Contains(x.Text.ToUpper())).ToList();
                model.ddlCrudeName = model.ddlCrudeName.Where(x => !comparison_list.Contains(x.Text.ToUpper())).ToList();
            }
            bool status = model.requester_info.workflow_status.ToUpper().Contains("APPROVE");
            ViewBag.Title = status == false ? "LP Run" : "Approve LP Run";
            ViewBag.Comment = status == false ? "SCEP Comment" : "SCEP (Section Head) Comment";
            model.taskID = TranID;
            model.reqID = Tran_Req_ID;
            model.purno = PURNO;
            model.CrudeYield = model.crude_compare.yield.Count() > 0 ? "Y" : "N";
            if (!String.IsNullOrEmpty(model.crude_info.propcessing_period_form) && !String.IsNullOrEmpty(model.crude_info.propcessing_period_to))
            {
                model.ProcessingPeriodDate = model.crude_info.propcessing_period_form + " to " + model.crude_info.propcessing_period_to;
            }
            return View(model);
        }

        public ActionResult PCAFinalPrice(string TranId, string Tran_Req_ID, string PURNO)
        {
            VCoolViewModel model = new VCoolViewModel();

            if (TranId != null)
            {
                ResponseData resData = LoadDataVCool(TranId.Decrypt(), ref model);
                if (resData != null)
                {
                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        if (resData.result_code == "10000033")
                        {
                            TempData["res_message"] = "This document has been changed by others. Please check it again.";
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        }
                        TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                    }
                }
            }
            else
            {
                model.requester_info = new Model.VcoRequesterInfo();
                model.requester_info.name = Const.User.UserName;
                model.requester_info.area_unit = Const.User.UserGroup;
                model.requester_info.workflow_status = ConstantPrm.ACTION.DRAFT;
                model.requester_info.date_purchase = DateTime.Now.ToString(VCoolViewModel.FORMAT_DATETIME);
            }

            if (model.requester_info.workflow_status == STATUS_WAITING_PROPOSE_ETA_DATE || model.requester_info.status_sc == STATUS_WAITING_EDIT_ETA_DATE)
            {
                ViewBag.Title = "Propose Discharging Date";
                model.stateModel = "1";
                //if (Const.User.UserGroup == "CMCS")
                //{
                //    model.pageMode = "EDIT";
                //}
                //else
                //{
                //    model.pageMode = "VIEW";
                //}
            }
            else if (model.requester_info.workflow_status == STATUS_WAITING_CHECK_TANK || model.requester_info.workflow_status == STATUS_WAITING_APPROVE_TANK || model.requester_info.workflow_status == STATUS_WAITING_CHECK_IMPACT || model.requester_info.workflow_status == STATUS_CONSENSUS_APPROVAL)
            {
                ViewBag.Title = "Propose Discharging Date";
                model.stateModel = "1";
                model.pageMode = "VIEW";
            }
            else if (model.requester_info.workflow_status == STATUS_WAITING_CONFIRM_PRICE)
            {
                ViewBag.Title = "Propose final price and volume";
                model.stateModel = "2";
                model.pageMode = "EDIT";
                if (Const.User.UserGroup == "CMCS_SH")
                {
                    if(model.requester_info.workflow_status_description == "WAITING PRICE CONFIRMATION")
                    {
                        model.stateModel = "2";
                        model.pageMode = "VIEW";
                    }
                    else
                    {
                        model.stateModel = "3";
                        model.pageMode = "VIEW";
                    }                    
                }                    
            }
            else if (model.requester_info.workflow_status == STATUS_WAITING_APPROVE_PRICE)
            {
                ViewBag.Title = "Approve final price and volume";
                model.stateModel = "3";
                if (Const.User.UserGroup == "EVPC")
                    model.stateModel = "2";
                //if (Const.User.UserGroup == "EVPC")
                //{
                //    model.pageMode = "EDIT";
                //}
                //else
                //{
                //    model.pageMode = "VIEW";
                //}
            }
            else if (model.requester_info.workflow_status == STATUS_WAITING_APPROVE_SUMMARY)
            {
                ViewBag.Title = "Summary Purchased Crude";
                model.stateModel = "4";
                //if (Const.User.UserGroup == "CMCS")
                //{
                //    model.pageMode = "EDIT";
                //}
                //else
                //{
                //    model.pageMode = "VIEW";
                //}
            }
            else
            {
                ViewBag.Title = "Summary Purchased Crude";
                model.stateModel = "4";
                model.pageMode = "VIEW";
            }
            initializeDropDownList(ref model);
            model.taskID = TranId;
            model.reqID = Tran_Req_ID;
            model.purno = PURNO;

            List<string> userGroup = VCoolServiceModel.getUserGroup(lbUserID);
            List<string> userCanEditFiles = new List<string>() { "SCEP", "CMCS", "SCVP", "CMVP", "TNVP", "EVPC" };
            TempData["isCanEditAttachFile"] = userGroup.Where(u => userCanEditFiles.Where(s => s == u).Any()).Any();
            TempData["isSCEP_SH"] = userGroup.Contains("SCEP_SH");
            //SCEP, CMCS, SCVP, CMVP, TNVP, EVPC

            TempData["isCMCS"] = userGroup.Contains("CMCS");    //CMCS
            TempData["isEVPC"] = userGroup.Contains("EVPC");
            TempData["isCMCS_SH"] = userGroup.Contains("CMCS_SH");    //CMCS_SH
            return View(model);
        }

        public ActionResult Consensus(string TranId, string Tran_Req_ID, string PURNO)
        {
            VCoolViewModel model = new VCoolViewModel();
            model.pageMode = "EDIT"; //PageMode is EDIT, EDIT_REASON or READ
            model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
            if (TranId != null)
            {
                ResponseData resData = LoadDataVCool(TranId.Decrypt(), ref model);
                if (resData != null)
                {
                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        if (resData.result_code == "10000033")
                        {
                            TempData["res_message"] = "This document has been changed by others. Please check it again.";
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        }
                        TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                    }
                }
            }
            else
            {
                model.requester_info = new Model.VcoRequesterInfo();
                model.requester_info.name = Const.User.UserName;
                model.requester_info.area_unit = Const.User.UserGroup;
                model.requester_info.workflow_status = ConstantPrm.ACTION.DRAFT;
                model.requester_info.date_purchase = DateTime.Now.ToString(VCoolViewModel.FORMAT_DATETIME);
            }

            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("/", units);
                }


                TempData["isCMVP"] = units.Contains("CMVP");
                TempData["isSCVP"] = units.Contains("SCVP");
                TempData["isTNVP"] = units.Contains("TNVP");
            }

            //List<string> userGroup = CoolServiceModel.getUserGroup(lbUserID);
            List<string> userGroup = VCoolServiceModel.getUserGroup(lbUserID);
            List<string> userCanEditFiles = new List<string>() { "SCEP", "CMCS", "SCVP", "CMVP", "TNVP", "EVPC" };
            TempData["isCanEditAttachFile"] = userGroup.Where(u => userCanEditFiles.Where(s => s == u).Any()).Any();
            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            initializeDropDownList(ref model);
            return View(model);
        }

        private void updateModel(FormCollection form, VCoolViewModel model)
        {
            if (!string.IsNullOrEmpty(model.tempDischargingDate))
            {
                string[] tempDate = model.tempDischargingDate.Split(' ');
                model.crude_info.discharging_date_from = tempDate[0];
                model.crude_info.discharging_date_to = tempDate[2];
            }

            if (!string.IsNullOrEmpty(model.tempLoadingDate))
            {
                string[] tempDate = model.tempLoadingDate.Split(' ');
                model.crude_info.loading_date_from = tempDate[0];
                model.crude_info.loading_date_to = tempDate[2];
            }

        }

        private void initializeDropDownList(ref VCoolViewModel model)
        {
            model.ddlPriority = getWorkflowPriority();
            model.ddlSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            model.ddlCrudeName = MaterialsServiceModel.getMaterialsDDLCool();
            model.ddlPurchaseType = getPurchaseType();
            model.ddlPurchaseMethod = getPurchaseMethod();
            model.ddlBenchmarkPrice = getBenchmarkPrice();
            //model.ddlIncoterm = getIncoterm();
            model.ddlIncoterm = DropdownServiceModel.getIncoterms();
            model.ddlPurchaseResult = getPurchaseResult();
            model.ddlCountry = DropdownServiceModel.getCountryCrudePurchase();
        }

        private List<SelectListItem> getFileType()
        {
            Setting setting = JSONSetting.getSetting("JSON_COOL_CRUDE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.file_type.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.crude_kerogen[i], Value = setting.file_type[i] });
            }
            return list;
        }

        private List<SelectListItem> getPurchaseType()
        {
            Setting setting = JSONSetting.getSetting("JSON_CRUDE_PURCHASE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.term.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.term[i], Value = setting.term[i] });
            }
            return list;
        }

        private List<SelectListItem> getPurchaseMethod()
        {
            Setting setting = JSONSetting.getSetting("JSON_CRUDE_PURCHASE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.purchase.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.purchase[i], Value = setting.purchase[i] });
            }
            return list;
        }

        private List<SelectListItem> getBenchmarkPrice()
        {
            Setting setting = JSONSetting.getSetting("JSON_CRUDE_PURCHASE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.bechmark_price.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.bechmark_price[i], Value = setting.bechmark_price[i] });
            }
            return list;
        }

        private List<SelectListItem> getIncoterm()
        {
            Setting setting = JSONSetting.getSetting("JSON_CRUDE_PURCHASE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.incoterms.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.incoterms[i], Value = setting.incoterms[i] });
            }
            return list;
        }

        private List<SelectListItem> getCrudeName()
        {
            Setting setting = JSONSetting.getSetting("JSON_CRUDE_PURCHASE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.file_type.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.crude_kerogen[i], Value = setting.file_type[i] });
            }
            return list;
        }

        private List<SelectListItem> getWorkflowPriority()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.workflow_priority.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.workflow_priority[i], Value = setting.workflow_priority[i] });
            }
            return list;
        }

        private List<SelectListItem> getActualPurchase()
        {
            Setting setting = JSONSetting.getSetting("JSON_VCOOL_CRUDE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.purchase_result.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.actual_purchase[i], Value = setting.actual_purchase[i] });
            }
            return list;
        }

        private List<SelectListItem> getPurchaseResult()
        {
            Setting setting = JSONSetting.getSetting("JSON_VCOOL_CRUDE");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.purchase_result.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.purchase_result[i], Value = setting.purchase_result[i] });
            }
            return list;
        }

        [HttpGet]
        public JsonResult CrudePurchasedResult(string sDatePurchase, string sFeedstock, string sProduct, string sSupplier)
        {
            CrudePurchaseSearchViewModel CrudePurchaseModel = new CrudePurchaseSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            msdata = msdata.OrderBy(x => x).ToList();

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);

            string datePurchase = sDatePurchase;
            string stDate = "";
            string enDate = "";
            string feedstock = sFeedstock;
            string product = sProduct;
            string supplier = sSupplier;

            if (!datePurchase.Equals(""))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true);
                enDate = _FN.ConvertDateFormat(s[1], true);
            }

            var a = MaterialsServiceModel.getMaterialsDDL();
            List<string> b = new List<string>();
            foreach (var item in a)
            {
                b.Add(item.Text.ToUpper());
            }
            List_CrudePurchasetrx res = SearchCrudePurchaseData(stDate, enDate, feedstock, product, supplier);
            CrudePurchaseModel.CrudePurchaseTransaction = res.CrudePurchaseTransaction.Where(y => y.status != "DRAFT" && y.status != "WAITING VERIFY" && b.Contains(y.product_name.ToUpper())).OrderByDescending(x => x.transaction_id).ToList();
            //CrudePurchaseModel.CrudePurchaseTransaction = res.CrudePurchaseTransaction.Where(y => y.status == "APPROVED").OrderByDescending(x => x.transaction_id).ToList();

            //for (int i = 0; i < res.CrudePurchaseTransaction.Where(y => y.status == "APPROVED" && b.Contains(y.product_name)).Count(); i++)
            //{
            for (int i = 0; i < CrudePurchaseModel.CrudePurchaseTransaction.Count(); i++)
            {
                CrudePurchaseModel.CrudePurchaseTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(CrudePurchaseModel.CrudePurchaseTransaction[i].date_purchase, "MMMM dd yyyy");
                CrudePurchaseModel.CrudePurchaseTransaction[i].Transaction_id_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].transaction_id.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].Req_transaction_id_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].req_transaction_id.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].Purchase_no_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].purchase_no.Encrypt();
                CrudePurchaseModel.CrudePurchaseTransaction[i].reason_Encrypted = CrudePurchaseModel.CrudePurchaseTransaction[i].reason.Encrypt();
            }

            return Json(CrudePurchaseModel, JsonRequestBehavior.AllowGet);
        }

        private List_CrudePurchasetrx SearchCrudePurchaseData(string sDate, string eDate, string feedstock, string product, string supplier)
        {

            if (!string.IsNullOrEmpty(sDate))
            {
                if (sDate.Contains("-"))
                {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy} 00:00:00", DateTime.ParseExact(sDate, "dd-MMM-yyyy HH:mm:ss", null, DateTimeStyles.None));
                    sDate = datetimeString;
                }
            }

            if (!string.IsNullOrEmpty(eDate))
            {
                if (eDate.Contains("-"))
                {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy} 00:00:00", DateTime.ParseExact(eDate, "dd-MMM-yyyy HH:mm:ss", null, DateTimeStyles.None));
                    eDate = datetimeString;
                }
            }

            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000004;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "function_code", V = CRUDE_PURCHASE_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_7", V = feedstock });
            req.Req_parameters.P.Add(new P { K = "index_10", V = product });
            req.Req_parameters.P.Add(new P { K = "index_12", V = supplier });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_CrudePurchasetrx _model = ShareFunction.DeserializeXMLFileToObject<List_CrudePurchasetrx>(_DataJson);
            if (_model == null) _model = new List_CrudePurchasetrx();
            if (_model.CrudePurchaseTransaction == null) _model.CrudePurchaseTransaction = new List<CrudePurchaseEncrypt>();
            return _model;
        }

        private List<SelectListItem> getFeedStock(bool isHidden = false)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            setting.feedstock = setting.feedstock.Where(x => x.key.ToUpper() == "CRUDE").ToList();
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.feedstock.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.feedstock[i].key, Value = isHidden ? setting.feedstock[i].unit : setting.feedstock[i].key });
            }
            return list;
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, VCoolViewModel model)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            string note = form["hdfNoteAction"];
            this.updateModel(form, model);
            ResponseData resData = SaveDataVCool(_status: _status, model: model, note: note);

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string purno = resData.resp_parameters[0].v.Encrypt();
                    string path = string.Format("~/CPAIMVC/VCool?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                    return Redirect(path);
                }
            }

            return View("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, VCoolViewModel model)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            string note = form["hdfNoteAction"];
            this.updateModel(form, model);
            ResponseData resData = SaveDataVCool(_status: _status, model: model, note: note);
            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string purno = resData.resp_parameters[0].v.Encrypt();
                    string path = string.Format("~/CPAIMVC/VCool?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                    return Redirect(path);
                }
            }
            ModelState.Clear();
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, VCoolViewModel model)
        {
            if (ButtonListVCool != null)
            {
                if (!string.IsNullOrEmpty(form["fileSupportingDocument"]))
                {
                    model.comment.lp_run_attach_file = form["fileSupportingDocument"];
                }

                if (!String.IsNullOrEmpty(model.Revise_dischangePeriod))
                {
                    model.date_history.history.Add(new DateHistory { date_history = model.Revise_dischangePeriod, user_group = Const.User.UserGroup });
                }
                if (!String.IsNullOrEmpty(model.Revise_dischangePeriod) && model.comment.scsc_agree_flag != "AGREE")
                {
                    string[] tempDate = model.Revise_dischangePeriod.Split(' ');
                    model.crude_info.revised_date_from = tempDate[0];
                    model.crude_info.revised_date_to = tempDate[2];
                }
                if(model.comment.scsc_agree_flag == "AGREE")
                {
                    model.comment.cmcs_request = "";
                }
                if (!String.IsNullOrEmpty(model.ProcessingPeriodDate))
                {
                    string[] tempDate = model.ProcessingPeriodDate.Split(' ');
                    model.crude_info.propcessing_period_form = tempDate[0];
                    model.crude_info.propcessing_period_to = tempDate[2];
                }
                if (model.CrudeYield == "N")
                {
                    model.crude_compare = null;
                }
                var _lstButton = ButtonListVCool.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    this.updateModel(form, model);
                    string json_fileUpload = string.Empty;
                     var chkCorrectPage = form["hdfSCVPAction"];
                    string noteRejectTNVP = form["hdfNoteTNVPAction"];
                    if (chkCorrectPage == "SCVP")
                    {
                        
                        model.requester_info.reason_reject_SCVP = string.IsNullOrEmpty(noteRejectTNVP) ? "-" : noteRejectTNVP;
                    }
                    else
                    { 
                        model.requester_info.reason_reject_TNVP = string.IsNullOrEmpty(noteRejectTNVP) ? "-" : noteRejectTNVP;
                    }
                    
                    string noteRejectSCSC_SH = form["hdfNoteSCSC_SHAction"];
                    model.requester_info.reason_reject_SCSC_SH = string.IsNullOrEmpty(noteRejectSCSC_SH) ? "-" : noteRejectSCSC_SH;
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    var json = new JavaScriptSerializer().Serialize(model);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = "-" });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = json_fileUpload });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2" || resData.result_code == "10000033")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string purno = "";
                            string url = this.Request.UrlReferrer.AbsoluteUri;
                            var urls = url.Split('?');
                            string path = string.Format(urls[0] + "?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "An error occurred on the system, Please contact the system administrator.";
            }

            TempData["VCoolViewModel"] = model;
            return RedirectToAction("Search", "VCool");
        }

        private ResponseData SaveDataVCool(DocumentActionStatus _status, VCoolViewModel model, string note = "", string json_fileUpload = "")
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_RUN_LP; }
            else if (DocumentActionStatus.Cancel == _status) { CurrentAction = ConstantPrm.ACTION.CANCEL; NextState = ConstantPrm.ACTION.CANCEL; }

            VcoRootObject obj = new VcoRootObject();

            //string[] crude_com = model.tempCrudeYield.Split('|');

            obj.comment = model.comment;
            obj.crude_info = model.crude_info;
            obj.requester_info = model.requester_info;
            obj.date_history = model.date_history;
            obj.crude_compare = model.crude_compare;

            //List<VcoCrudeYieldJson> vcoy_list = new List<VcoCrudeYieldJson>();
            //foreach (var item in crude_com)
            //{
            //    string[] temp = item.Split(':');
            //    VcoCrudeYieldJson vcoy = new VcoCrudeYieldJson();
            //    vcoy.selectedCrude = temp[0];
            //    vcoy.isChecked = temp[1];
            //    vcoy_list.Add(vcoy);
            //}

            //obj.crude_compare.comparison_json = vcoy_list;


            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000063;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.VCOOL });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            resData = service.CallService(req);
            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref VCoolViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SUBMIT).ToList().Count > 0)
                {
                    model.pageMode = "EDIT";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.APPROVE).ToList().Count > 0)
                {
                    model.pageMode = "EDIT";
                }
                foreach (ButtonAction _button in Button)
                {
                    string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                    model.buttonCode += _btn;
                }
                model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListVCool = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        public ActionResult SCSCTank(string TranID, string Tran_Req_ID, string PURNO)
        {
            VCoolViewModel model = new VCoolViewModel();
            model.pageMode = "EDIT";
            model.buttonMode = "AUTO";
            if (TranID != null)
            {
                ResponseData resData = LoadDataVCool(TranID.Decrypt(), ref model);
                if (resData != null)
                {
                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        if (resData.result_code == "10000033")
                        {
                            TempData["res_message"] = "This document has been changed by others. Please check it again.";
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        }
                        TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                    }
                }
            }
            else
            {
                model.requester_info = new Model.VcoRequesterInfo();
                model.requester_info.name = Const.User.UserName;
                model.requester_info.area_unit = Const.User.UserGroup;
                model.requester_info.workflow_status = ConstantPrm.ACTION.DRAFT;
                model.requester_info.date_purchase = DateTime.Now.ToString(CoolViewModel.FORMAT_DATETIME);
            }

            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("/", units);
                }
            }

            List<string> userGroups = VCoolServiceModel.getUserGroup(lbUserID);
            TempData["isSCSC_SH"] = userGroups.Contains("SCSC_SH");

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            initializeDropDownList(ref model);
            bool status = model.requester_info.status_sc.ToUpper().Contains("APPROVE") || model.requester_info.status_sc.ToUpper().Contains("APPROVAL");
            ViewBag.Title = status == false ? "Check tank available" : "Approve available tank";
            if (model.crude_info.revised_date_from != "" && model.crude_info.revised_date_to != "")
            {
                model.Revise_dischangePeriod = model.crude_info.revised_date_from + " to " + model.crude_info.revised_date_to;
            }
            model.taskID = TranID;
            model.reqID = Tran_Req_ID;
            model.purno = PURNO;
            return View(model);
        }

        [HttpPost]
        public ActionResult CompareCrude(string[] selectCrude)
        {
            VCoolViewModel model = new VCoolViewModel();
            model.CooSpiralList = new List<VCoolCompareCrude>();
            foreach (var item in selectCrude)
            {
                model.SelectedCrudeName.Add(item);
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var cooDataId = context.COO_DATA.Where(a => a.CODA_CRUDE_NAME.ToUpper() == item.ToUpper() && a.CODA_STATUS == "APPROVED").OrderByDescending(a => a.CODA_CREATED).FirstOrDefault();
                    if (cooDataId != null)
                    {
                        string cooRowId = cooDataId.CODA_ROW_ID;
                        string cooCrudeName = cooDataId.CODA_CRUDE_NAME;
                        var cooCamId = context.COO_CAM.Single(a => a.COCA_FK_COO_DATA == cooRowId);
                        var camRowId = cooCamId.COCA_ROW_ID;
                        var cooSpiralList = context.COO_SPIRAL.Where(a => a.COSP_FK_COO_CAM == camRowId && a.COSP_HEADER == "Yield (% wgt)").ToList();
                        Setting setting = JSONSetting.getSetting("JSON_VCOOL_CRUDE");
                        List<string> header = setting.crude_yield;
                        foreach (var z in header)
                        {
                            VCoolCompareCrude compareCrude = new VCoolCompareCrude();
                            compareCrude.header = z;
                            compareCrude.crude = cooCrudeName;
                            if (z == "C4-")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "C4 minus").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "C4 minus").FirstOrDefault().COSP_VALUE : "";
                            }
                            else if (z == "Tops(C5-100)")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "C5-65" || x.COSP_KEY == "65-100").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "C5-65" || x.COSP_KEY == "65-100").Sum(x => Decimal.Parse(x.COSP_VALUE)).ToString() : "";
                            }
                            else if (z == "Naphtha(100-150)")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "100-150").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "100-150").Sum(x => Decimal.Parse(x.COSP_VALUE)).ToString() : "";
                            }
                            else if (z == "Kerosene(150-250)")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "150-200" || x.COSP_KEY == "200-250").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "150-200" || x.COSP_KEY == "200-250").Sum(x => Decimal.Parse(x.COSP_VALUE)).ToString() : "";
                            }
                            else if (z == "Gasoil(250-370)")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "250-300" || x.COSP_KEY == "300-350" || x.COSP_KEY == "350-370").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "250-300" || x.COSP_KEY == "300-350" || x.COSP_KEY == "350-370").Sum(x => Decimal.Parse(x.COSP_VALUE)).ToString() : "";
                            }
                            else if (z == "Waxy(370-575)")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "370-525" || x.COSP_KEY == "525-540" || x.COSP_KEY == "540-575").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "370-525" || x.COSP_KEY == "525-540" || x.COSP_KEY == "540-575").Sum(x => Decimal.Parse(x.COSP_VALUE)).ToString() : "";
                            }
                            else if (z == "SR(575+)")
                            {
                                compareCrude.value = cooSpiralList.Where(x => x.COSP_KEY == "575+").FirstOrDefault() != null ? cooSpiralList.Where(x => x.COSP_KEY == "575+").Sum(x => Decimal.Parse(x.COSP_VALUE)).ToString() : "";
                            }
                            model.CooSpiralList.Add(compareCrude);
                        }
                    }
                    else
                    {
                        string cooCrudeName = item;
                        Setting setting = JSONSetting.getSetting("JSON_VCOOL_CRUDE");
                        List<string> header = setting.crude_yield;
                        foreach (var z in header)
                        {
                            VCoolCompareCrude compareCrude = new VCoolCompareCrude();
                            compareCrude.header = z;
                            compareCrude.crude = cooCrudeName;
                            if (z == "C4-")
                            {
                                compareCrude.value = "N/A";
                            }
                            else if (z == "Tops(C5-100)")
                            {
                                compareCrude.value = "N/A";
                            }
                            else if (z == "Naphtha(100-150)")
                            {
                                compareCrude.value = "N/A";
                            }
                            else if (z == "Kerosene(150-250)")
                            {
                                compareCrude.value = "N/A";
                            }
                            else if (z == "Gasoil(250-370)")
                            {
                                compareCrude.value = "N/A";
                            }
                            else if (z == "Waxy(370-575)")
                            {
                                compareCrude.value = "N/A";
                            }
                            else if (z == "SR(575+)")
                            {
                                compareCrude.value = "N/A";
                            }
                            model.CooSpiralList.Add(compareCrude);
                        }
                    }
                }
            }
            return PartialView("_CompareCrude", model);
        }

        public ActionResult TNCheckTech()
        {
            ////TODO
            VCoolViewModel model = new VCoolViewModel();
            model.pageMode = "EDIT"; //PageMode is EDIT, EDIT_REASON or READ
            model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL

            if (Request.QueryString["TranID"] != null)
            {
                string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                ResponseData resData = LoadDataVCool(tranID, ref model);

                if (resData != null)
                {
                    if (!(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        if (resData.result_code == "10000033")
                        {
                            TempData["res_message"] = "This document has been changed by others. Please check it again.";
                        }
                        else
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        }
                        TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                    }
                }
            }
            else
            {
                model.requester_info = new Model.VcoRequesterInfo();
                model.requester_info.name = Const.User.UserName;
                model.requester_info.area_unit = Const.User.UserGroup;
                model.requester_info.workflow_status = ConstantPrm.ACTION.DRAFT;
                model.requester_info.date_purchase = DateTime.Now.ToString(CoolViewModel.FORMAT_DATETIME);
            }


            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("/", units);
                }
            }

            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight(FEED_TYPE);
            initializeDropDownList(ref model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form, CoolViewModel model)
        {
            return RedirectToAction("Search");
        }

        [HttpGet]
        public JsonResult LoadDataCrudePurchase(string TransactionID)
        {
            string tranID = TransactionID.Decrypt();
            CrudePurchaseViewModel model_temp = new CrudePurchaseViewModel();
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = tranID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }
            if (_model.data_detail != null)
            {
                model_temp = new JavaScriptSerializer().Deserialize<CrudePurchaseViewModel>(_model.data_detail);
                model_temp.date_purchase = string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(model_temp.date_purchase, "dd/MM/yyyy", null, DateTimeStyles.None));
                model_temp.discharging_period.date_from = model_temp.discharging_period.date_from == "" ? "" : string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(model_temp.discharging_period.date_from, "dd/MM/yyyy", null, DateTimeStyles.None));
                model_temp.discharging_period.date_to = model_temp.discharging_period.date_to == "" ? "" : string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(model_temp.discharging_period.date_to, "dd/MM/yyyy", null, DateTimeStyles.None));
                model_temp.loading_period.date_from = model_temp.loading_period.date_from == "" ? "" : string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(model_temp.loading_period.date_from, "dd/MM/yyyy", null, DateTimeStyles.None));
                model_temp.loading_period.date_to = model_temp.loading_period.date_to == "" ? "" : string.Format("{0:dd}-{0:MMM}-{0:yyyy}", DateTime.ParseExact(model_temp.loading_period.date_to, "dd/MM/yyyy", null, DateTimeStyles.None));
            }

            model_temp.offers_items = model_temp.offers_items.Where(x => x.final_flag == "Y").ToList();

            return Json(model_temp, JsonRequestBehavior.AllowGet); ;
        }

        #region Approve VCool link from Email.
        public ActionResult ApproveVCool()
        {
            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }
            return Redirect(path);
        }


        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                                if (!string.IsNullOrEmpty(itemFunc.FTX_INDEX4))
                                {
                                    if (itemFunc.FTX_INDEX4 == "DRAFT" || itemFunc.FTX_INDEX4 == "WAITING APPROVE DRAFT CAM")
                                    {
                                        urlPage = "VCool";
                                    }
                                    else
                                    {
                                        urlPage = "VCool";
                                    }
                                }
                                else
                                {
                                    urlPage = "VCool";
                                }
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt();
                    }
                }
            }
            return path;
        }



        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }
        #endregion

        public List<SelectListItem> GetCountryRef(string crude)
        {
            List<SelectListItem> objcountry = DropdownServiceModel.getCountryByCrudePurchase(crude);
            return objcountry;
        }

        //------ Bind Country and CrudeName--------//
        private void initializeTracking(ref VCoolTrackingViewModel model)
        {
            model.ddlCountry = DropdownServiceModel.getCountryCrudePurchase();
            model.ddlCrudeName = DropdownServiceModel.getVCoolCrudeName();
        }

        public ActionResult WorkflowTracking()
        {
            VCoolTrackingViewModel model;

            if (TempData["VCoolTrackingViewModel"] == null)
            {
                model = new VCoolTrackingViewModel();
            }
            else
            {
                model = TempData["VCoolTrackingViewModel"] as VCoolTrackingViewModel;
                TempData["VCoolTrackingViewModel"] = null;
            }
            initializeTracking(ref model);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "TrackingResult")]
        public ActionResult TrackingResult(FormCollection fm, VCoolTrackingViewModel model)
        {
            if (!string.IsNullOrEmpty(lbUserID))
            {
                var units = VCoolServiceModel.getUserGroup(lbUserID);
                if (units != null && units.Any())
                {
                    Const.User.UserGroup = string.Join("|", units);
                }
            }

            string userGroup = Const.User.UserGroup;
            string system = ConstantPrm.SYSTEM.VCOOL;
            string type = ConstantPrm.SYSTEMTYPE.CRUDE;
            List_VCooltrx res = SearchTrackingData(model);
            model.VCoolTrackingTransaction = res.VCoolTransaction.OrderByDescending(x => x.transaction_id).ToList();
            if (model.VCoolTrackingTransaction != null)
            {
                for (int i = 0; i < model.VCoolTrackingTransaction.Count; i++)
                {
                    model.VCoolTrackingTransaction[i].Transaction_id_Encrypted = model.VCoolTrackingTransaction[i].transaction_id.Encrypt();
                    model.VCoolTrackingTransaction[i].Req_transaction_id_Encrypted = model.VCoolTrackingTransaction[i].req_transaction_id.Encrypt();

                    //model.VCoolTrackingTransaction[i].draft_tracking = GetDraftStatusTracking(model.VCoolTrackingTransaction[i].draft_cam_status);
                    //model.VCoolTrackingTransaction[i].final_tracking = GetFinalStatusTracking(model.VCoolTrackingTransaction[i].final_cam_status);
                    //model.VCoolTrackingTransaction[i].expert_tracking = GetExpertTracking(model.VCoolTrackingTransaction[i].expert_units);

                    // SWITCH DOWNLOAD DRAFT/FINAL CAM FILE WHEN FINAL CAM WAS GENERATED //
                    if (!string.IsNullOrEmpty(model.VCoolTrackingTransaction[i].final_cam_file))
                    {
                        model.VCoolTrackingTransaction[i].draft_cam_file = string.Empty;
                    }

                    // SET NOTE WHEN TRANSATION WAS CANCELLED //
                    if (model.VCoolTrackingTransaction[i].status != ConstantPrm.ACTION.CANCEL)
                    {
                        model.VCoolTrackingTransaction[i].note = string.Empty;
                        model.VCoolTrackingTransaction[i].updated_by = string.Empty;

                    }

                    model.VCoolTrackingTransaction[i].status_description = VCoolServiceModel.getWorkflowStatusDescription(model.VCoolTrackingTransaction[i].status);
                }
            }
            TempData["isEVPC"] = userGroup.Contains("EVPC");
            TempData["VCoolTrackingViewModel"] = model;
            return RedirectToAction("WorkflowTracking", "VCool");
        }

        private List_VCooltrx SearchTrackingData(VCoolTrackingViewModel model)
        {
            var cultureInfo = new CultureInfo("en-US");
            var monthDictionary = Enumerable.Range(0, 12).ToDictionary(i => cultureInfo.DateTimeFormat.AbbreviatedMonthNames[i], i => cultureInfo.DateTimeFormat.MonthNames[i]);
            var createDateString = new Func<string, string, string>((month, year) =>
            {
                var yearNo = 0;
                var isYear = int.TryParse(year, out yearNo);
                var monthName = !string.IsNullOrEmpty(month) && monthDictionary.ContainsKey(month) ? month : "MMM";
                var canGetMonthYear = !monthName.Equals("MMM") || isYear;
                return canGetMonthYear ? string.Format("01-{0}-{1}", monthName, isYear ? year : "0000") : string.Empty;
            });
            var tpc_plan_from = createDateString(model.tpc_plan_month_from, model.tpc_plan_year_from);
            var tpc_plan_to = createDateString(model.tpc_plan_month_to, model.tpc_plan_year_to);

            var dischargingDates = string.IsNullOrEmpty(model.tempDischargingDate) ? null : VCoolServiceModel.GetDatePair(model.tempDischargingDate, cultureInfo);
            var loadingDates = string.IsNullOrEmpty(model.tempLoadingDate) ? null : VCoolServiceModel.GetDatePair(model.tempLoadingDate, cultureInfo);

            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000061;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = "" });

            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.VCOOL });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "index_3", V = "" }); //action
            req.Req_parameters.P.Add(new P { K = "index_4", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_5", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_6", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_7", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_8", V = model.purchase_no });
            req.Req_parameters.P.Add(new P { K = "index_9", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_10", V = model.products }); //Crude Name
            req.Req_parameters.P.Add(new P { K = "index_11", V = model.origin }); //Country
            req.Req_parameters.P.Add(new P { K = "index_15", V = tpc_plan_from });
            req.Req_parameters.P.Add(new P { K = "index_16", V = tpc_plan_to });
            req.Req_parameters.P.Add(new P { K = "index_17", V = loadingDates == null ? "" : loadingDates.Item1.ToString(CoolViewModel.FORMAT_DATE, cultureInfo) });
            req.Req_parameters.P.Add(new P { K = "index_18", V = loadingDates == null ? "" : loadingDates.Item2.ToString(CoolViewModel.FORMAT_DATE, cultureInfo) });
            req.Req_parameters.P.Add(new P { K = "index_19", V = dischargingDates == null ? "" : dischargingDates.Item1.ToString(CoolViewModel.FORMAT_DATE, cultureInfo) });
            req.Req_parameters.P.Add(new P { K = "index_20", V = dischargingDates == null ? "" : dischargingDates.Item2.ToString(CoolViewModel.FORMAT_DATE, cultureInfo) });

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_VCooltrx _model = ShareFunction.DeserializeXMLFileToObject<List_VCooltrx>(_DataJson);
            if (_model == null) _model = new List_VCooltrx();
            if (_model.VCoolTransaction == null) _model.VCoolTransaction = new List<VCoolEncrypt>();
            return _model;
        }
    }
}