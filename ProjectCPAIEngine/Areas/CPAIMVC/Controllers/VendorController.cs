﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class VendorController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "Vendor");
        }

        [HttpPost]
        public ActionResult Search(VendorViewModel vendorModel)
        {
            
            VendorViewModel model = initialModel();

            VendorServiceModel serviceModel = new VendorServiceModel();
            VendorViewModel_Seach viewModelSearch = new VendorViewModel_Seach();

            viewModelSearch = vendorModel.vnd_Search;
            serviceModel.Search(ref viewModelSearch);
            model.vnd_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            VendorViewModel model = initialModel();
            return View(model);
        }

        public static VendorViewModel SearchQuery(VendorViewModel vendorModel)
        {
            VendorViewModel_Seach viewModelSearch = new VendorViewModel_Seach();
            VendorServiceModel serviceModel = new VendorServiceModel();

            vendorModel.ddl_Country = DropdownServiceModel.getCountry();
            vendorModel.ddl_CreateType = DropdownServiceModel.getCreateType();
            vendorModel.ddl_System = DropdownServiceModel.getVendorCtrlSystem();
            vendorModel.ddl_Type = DropdownServiceModel.getVendorCtrlType();
            vendorModel.ddl_Status = DropdownServiceModel.getMasterStatus();

            viewModelSearch = vendorModel.vnd_Search;
            serviceModel.Search(ref viewModelSearch);
            vendorModel.vnd_Search = viewModelSearch;

            return vendorModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, VendorViewModel model)
        {
            if(ViewBag.action_create)
            {
                VendorServiceModel serviceModel = new VendorServiceModel();
                ReturnValue rtn = new ReturnValue();
                VendorViewModel_Detail pModel = new VendorViewModel_Detail();
                pModel = model.vnd_Detail;
                rtn = serviceModel.Add(ref pModel, lbUserName);
                model.vnd_Detail = pModel;
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                VendorViewModel modelDropdownSet = initialModel();
                modelDropdownSet.vnd_Detail = model.vnd_Detail;

                return View(modelDropdownSet);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "vendor" }));
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                VendorViewModel model = initialModel();
                if (model.vnd_Detail.CreateType == "" || model.vnd_Detail.CreateType == null)
                {
                    model.vnd_Detail.CreateType = "CPAI";
                }
                model.vnd_Detail.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "", Type = "", Color = "", Status = "ACTIVE" });

                return View(model);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "vendor" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string vendorCode, VendorViewModel model)
        {
            if (ViewBag.action_edit)
            {
                VendorServiceModel serviceModel = new VendorServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(model.vnd_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["vendorCode"] = vendorCode;

                VendorViewModel modelDropdownSet = initialModel();
                modelDropdownSet.vnd_Detail = model.vnd_Detail;

                return View("Create", modelDropdownSet);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "vendor" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string vendorCode)
        {
            if (ViewBag.action_edit && !String.IsNullOrEmpty(vendorCode))
            {
                VendorServiceModel serviceModel = new VendorServiceModel();

                VendorViewModel model = initialModel();
                model.vnd_Detail = serviceModel.Get(vendorCode);
                

                TempData["vendorCode"] = vendorCode;

                return View("Create", model);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "vendor" }));
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(string vendorCode)
        {
            if (ViewBag.action_view)
            {
                VendorServiceModel serviceModel = new VendorServiceModel();

                VendorViewModel model = initialModel();
                model.vnd_Detail = serviceModel.Get(vendorCode);
                if (model.vnd_Detail.Control.Count < 1)
                {
                    model.vnd_Detail.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "", Color = "" });
                }

                TempData["vendorCode"] = vendorCode;

                return View("Create", model);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "vendor" }));
            }
        }

        public VendorViewModel initialModel()
        {
            VendorServiceModel serviceModel = new VendorServiceModel();
            VendorViewModel model = new VendorViewModel();

            model.vnd_Search = new VendorViewModel_Seach();
            model.vnd_Search.sSearchData = new List<VendorViewModel_SeachData>();

            model.vnd_Detail = new VendorViewModel_Detail();
            model.vnd_Detail.Control = new List<VendorViewModel_Control>();

            model.ddl_Country = DropdownServiceModel.getCountry();
            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_System = DropdownServiceModel.getVendorCtrlSystem();
            model.ddl_Type = DropdownServiceModel.getVendorCtrlType();
            model.ddl_Color = DropdownServiceModel.getColorPicker();

            model.vnd_SystemJSON = VendorServiceModel.GetSystemJSON();
            model.vnd_TypeJSON = VendorServiceModel.GetTypeJSON();

            return model;
        }
    }
}