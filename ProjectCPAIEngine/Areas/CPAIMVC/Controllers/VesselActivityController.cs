﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class VesselActivityController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Search", "VesselActivity");
        }

        [HttpPost]
        public ActionResult Search(VesselActivityViewModel VesselActivityModel)
        {
            
            VesselActivityViewModel model = initialModel();

            VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();
            VesselActivityViewModel_Seach viewModelSearch = new VesselActivityViewModel_Seach();

            viewModelSearch = VesselActivityModel.mva_Search;
            serviceModel.Search(ref viewModelSearch);
            model.mva_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            VesselActivityViewModel model = initialModel();
            return View(model);
        }

        public static VesselActivityViewModel SearchQuery(VesselActivityViewModel pModel)
        {
            VesselActivityViewModel_Seach viewModelSearch = new VesselActivityViewModel_Seach();
            VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();


            pModel.ddl_System = DropdownServiceModel.getVesselActivityCtrlSystem();
            pModel.ddl_Type = DropdownServiceModel.getVesselActivityCtrlType();
            pModel.ddl_Status = DropdownServiceModel.getMasterStatus();

            viewModelSearch = pModel.mva_Search;
            serviceModel.Search(ref viewModelSearch);
            pModel.mva_Search = viewModelSearch;

            return pModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, VesselActivityViewModel model)
        {
            if(ViewBag.action_create)
            {
                VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();
                ReturnValue rtn = new ReturnValue();

                VesselActivityViewModel_Detail modelDetail = new VesselActivityViewModel_Detail();
                modelDetail = model.mva_Detail;
                rtn = serviceModel.Add(ref modelDetail, lbUserName);
                model.mva_Detail = modelDetail;
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                VesselActivityViewModel modelDropdownSet = initialModel();
                modelDropdownSet.mva_Detail = model.mva_Detail;

                return View(modelDropdownSet);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "VesselActivity" }));
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                VesselActivityViewModel model = initialModel();
                model.mva_Detail.Control.Add(new VesselActivityViewModel_Control { VACRowID = "", VACOrder = "1", VACSystem = "", VACType = "", VACColor = "", VACStatus = "ACTIVE" });

                return View(model);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "VesselActivity" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string VARowID, VesselActivityViewModel model)
        {
            if (ViewBag.action_edit)
            {
                VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(model.mva_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["VARowID"] = VARowID;

                VesselActivityViewModel modelDropdownSet = initialModel();
                modelDropdownSet.mva_Detail = model.mva_Detail;

                return View("Create", modelDropdownSet);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "VesselActivity" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string VARowID)
        {
            if (ViewBag.action_edit)
            {
                VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();

                VesselActivityViewModel model = initialModel();
                model.mva_Detail = serviceModel.Get(VARowID);

                TempData["VARowID"] = VARowID;

                return View("Create", model);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "VesselActivity" }));
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(string VARowID)
        {
            if (ViewBag.action_view)
            {
                VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();

                VesselActivityViewModel model = initialModel();
                model.mva_Detail = serviceModel.Get(VARowID);
                if (model.mva_Detail.Control.Count < 1)
                {
                    model.mva_Detail.Control.Add(new VesselActivityViewModel_Control { VACRowID = "", VACOrder = "1", VACSystem = "", VACType = "", VACColor = "", VACStatus = "ACTIVE" });
                }

                TempData["VARowID"] = VARowID;

                return View("Create", model);
            } else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "VesselActivity" }));
            }
        }

        public VesselActivityViewModel initialModel()
        {
            VesselActivityServiceModel serviceModel = new VesselActivityServiceModel();
            VesselActivityViewModel model = new VesselActivityViewModel();

            model.mva_Search = new VesselActivityViewModel_Seach();
            model.mva_Search.sSearchData = new List<VesselActivityViewModel_SeachData>();

            model.mva_Detail = new VesselActivityViewModel_Detail();
            model.mva_Detail.Control = new List<VesselActivityViewModel_Control>();

           
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_System = DropdownServiceModel.getVesselActivityCtrlSystem();
            model.ddl_Type = DropdownServiceModel.getVesselActivityCtrlType();
            model.ddl_Color = DropdownServiceModel.getColorPicker();

            model.mva_SystemJSON = VesselActivityServiceModel.GetSystemJSON();
            model.mva_TypeJSON = VesselActivityServiceModel.GetTypeJSON();

            model.mva_CodeJSON = VesselActivityServiceModel.GetVACodeJSON();
            model.mva_KeyJSON = VesselActivityServiceModel.GetVAKeyJSON();

            return model;
        }
    }
}