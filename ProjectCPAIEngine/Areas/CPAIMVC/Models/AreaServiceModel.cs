﻿using Common.Logging;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class AreaServiceModel
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ReturnValue Add(AreaViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                MT_AREA_DAL dal = new MT_AREA_DAL();
                MT_AREA ent = new MT_AREA();

                MT_UNIT_DAL dalUnit = new MT_UNIT_DAL();
                MT_UNIT entUnit = new MT_UNIT();



                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Order))
                {
                    rtn.Message = "Order should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.AreaName))
                {
                    rtn.Message = "Area name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.AreaName != null)
                {
                    bool hasData = dal.ChkAreaByName(pModel.AreaName);
                    if(hasData == false)
                    {
                        rtn.Message = "Area name is duplicate.";
                        rtn.Status = false;
                        return rtn;
                    }
                }
                else if (string.IsNullOrEmpty(pModel.AreaStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

               

                DateTime now = DateTime.Now;              

                try
                {
                    var Code = ShareFn.GenerateCodeByDate("CPAI");                   
                    ent.MAR_ROW_ID = Code;
                    ent.MAR_NAME = pModel.AreaName;
                    ent.MAR_ORDER = pModel.Order;
                    ent.MAR_STATUS = pModel.AreaStatus;
                    //ent.MT_UNIT = pModel.Unit;
                    ent.MAR_CREATED_BY = pUser;
                    ent.MAR_CREATED_DATE = now;
                    ent.MAR_UPDATED_BY = pUser;
                    ent.MAR_UPDATED_DATE = now;

                    dal.Save(ent);


                    //Unit
                    if(pModel.Unit != null && pModel.Unit.Any())
                    {
                        int unitOrder = 1;
                        foreach(var item in pModel.Unit)
                        {
                          
                                entUnit = new MT_UNIT();
                                //entUnit.MUN_ROW_ID = item.Code;
                                entUnit.MUN_ROW_ID = item.Name;
                                entUnit.MUN_FK_MT_AREA = Code;
                                entUnit.MUN_UPDATED_BY = pUser;
                                entUnit.MUN_UPDATED_DATE = now;
                                dalUnit.Update(entUnit);
                                unitOrder++;                                                 
                        }
                    }





                    pModel.AreaCode = Code;
                    //dal.Save(ent);
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Search(ref AreaViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sAreaCode = pModel.sAreaCode == null ? "" : pModel.sAreaCode.ToUpper();
                    var sAreaName = pModel.sAreaName == null ? "" : pModel.sAreaName.ToUpper();
                    var sAreaStatus = pModel.sAreaStatus == null ? "" : pModel.sAreaStatus.ToUpper();



                    var query = (from u in context.MT_AREA

                                 select new AreaViewModel_SeachData
                                 {
                                     dAreaCode = u.MAR_ROW_ID,
                                     dAreaName = u.MAR_NAME,
                                     dAreaStatus = u.MAR_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sAreaCode))
                        query = query.Where(p => p.dAreaCode.ToUpper().Contains(sAreaCode));

                    if (!string.IsNullOrEmpty(sAreaName))
                        query = query.Where(p => p.dAreaName.ToUpper().Contains(sAreaName));

                    if (!string.IsNullOrEmpty(sAreaStatus))
                        query = query.Where(p => p.dAreaStatus.ToUpper().Equals(sAreaStatus));



                    if (query != null)
                    {

                        //pModel.sSearchData = query.ToList();
                        pModel.sSearchData = new List<AreaViewModel_SeachData>();
                        foreach (var item in query.OrderBy(c=>c.dAreaName).ToList())
                        {
                            pModel.sSearchData.Add(new AreaViewModel_SeachData
                            {
                                dAreaCode = string.IsNullOrEmpty(item.dAreaCode) ? "" : item.dAreaCode,
                                dAreaName = string.IsNullOrEmpty(item.dAreaName) ? "" : item.dAreaName,
                                dAreaStatus = string.IsNullOrEmpty(item.dAreaStatus) ? "" : item.dAreaStatus
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Edit(AreaViewModel_Detail pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            try
            {
                MT_AREA_DAL dal = new MT_AREA_DAL();
                MT_AREA ent = new MT_AREA();


                MT_UNIT_DAL dalUnit = new MT_UNIT_DAL();
                MT_UNIT entUnit = new MT_UNIT();

                var boolStatus = pModel.BoolStatus;
                if (boolStatus != null)
                {
                    pModel.AreaStatus = pModel.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                }
                else
                {
                    pModel.AreaStatus = "INACTIVE";
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null.";
                    rtn.Status = false;
                }
                else if (string.IsNullOrEmpty(pModel.AreaCode))
                {
                    rtn.Message = "Unit code should not be empty.";
                }
                else if (string.IsNullOrEmpty(pModel.AreaName))
                {
                    rtn.Message = "Unit name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (!string.IsNullOrEmpty(pModel.AreaName))
                {
                    bool hasData = dal.ChkAreaName(pModel.AreaCode, pModel.AreaName);
                    var a = Get(pModel.AreaCode);
                    if (a.AreaName != pModel.AreaName)
                    {
                        COO_DATA_DAL dalCooData = new COO_DATA_DAL();
                        bool dataInProgress = dalCooData.ChkDucomentInProgress();
                        if (dataInProgress == false)
                        {
                            rtn.Message = "Can not change the area name because some document is in progress.";
                            rtn.Status = false;
                            return rtn;
                        }
                    }

                    if (hasData == false)
                    {
                        rtn.Message = "Area name is duplicate.";
                        rtn.Status = false;
                        return rtn;
                    }

                   
                }
                else if (string.IsNullOrEmpty(pModel.AreaStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                

                DateTime now = DateTime.Now;

                try
                {
                    ent.MAR_ROW_ID = pModel.AreaCode;
                    ent.MAR_NAME = pModel.AreaName;
                    ent.MAR_ORDER = pModel.Order;
                    ent.MAR_STATUS = pModel.AreaStatus;
                    ent.MAR_UPDATED_BY = pUser;
                    ent.MAR_UPDATED_DATE = now;

                    dal.Update(ent);

                    var oldUnit = dalUnit.getUnitByArea(pModel.AreaCode).Count();
                    //Unit
                    if (pModel.Unit != null && pModel.Unit.Any())
                    {

                        
                        int unitOrder = 1;

                        if(oldUnit != pModel.Unit.Count())
                        {
                            try
                            {
                                dalUnit.UpdateArea(pModel.AreaCode);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            
                            foreach (var item in pModel.Unit)
                            {

                                entUnit = new MT_UNIT();
                                entUnit.MUN_ROW_ID = item.Name;
                                entUnit.MUN_FK_MT_AREA = pModel.AreaCode;
                                entUnit.MUN_STATUS = item.Status;
                                entUnit.MUN_UPDATED_BY = pUser;
                                entUnit.MUN_UPDATED_DATE = now;
                                dalUnit.Update(entUnit);
                                unitOrder++;
                            }

                        }
                        else
                        {
                            dalUnit.UpdateArea(pModel.AreaCode);
                            foreach (var item in pModel.Unit)
                            {
                                entUnit = new MT_UNIT();
                                entUnit.MUN_ROW_ID = item.Name;
                                entUnit.MUN_FK_MT_AREA = pModel.AreaCode;
                                entUnit.MUN_STATUS = item.Status;
                                entUnit.MUN_UPDATED_BY = pUser;
                                entUnit.MUN_UPDATED_DATE = now;
                                dalUnit.Update(entUnit);
                                unitOrder++;
                            }
                        }

                    }
                    else
                    {
                        if(oldUnit != 0)
                        {
                            try
                            {
                                dalUnit.UpdateArea(pModel.AreaCode);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }
                    pModel.AreaCode = pModel.AreaCode;
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public AreaViewModel_Detail Get(string pAreaCode)
        {
            AreaViewModel_Detail model = new AreaViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pAreaCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = context.MT_AREA.Where(c => c.MAR_ROW_ID.ToUpper() == pAreaCode.ToUpper());
                        var dataUnit = (from d in context.MT_UNIT
                                        where d.MUN_FK_MT_AREA == pAreaCode
                                        select new AreaViewModel_Unit
                                        {
                                            Code = d.MUN_ROW_ID,
                                            Name = d.MUN_NAME,
                                            Status = d.MUN_STATUS
                                        }).ToList();

                        if (query != null)
                        {
                            foreach (var g in query)
                            {
                                model.AreaCode = g.MAR_ROW_ID;
                                model.Order = g.MAR_ORDER;
                                model.AreaName = g.MAR_NAME;
                                model.Unit = dataUnit;
                                model.AreaStatus = g.MAR_STATUS;
                                model.BoolStatus = g.MAR_STATUS == "ACTIVE" ? true : false;
                            }
                        }

                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public static List<SelectListItem> getAreaDDL(string status = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_AREA                              
                             orderby new { d.MAR_NAME }
                             select new { UnitCode = d.MAR_ROW_ID, UnitName = d.MAR_NAME.ToUpper() }).Distinct().ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UnitCode, Text = item.UnitName });
                    }
                }
            }
            return rtn;
        }






    }
}