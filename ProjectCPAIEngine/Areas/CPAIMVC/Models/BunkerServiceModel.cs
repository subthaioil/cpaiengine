﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class BunkerServiceModel
    {
        public static string getTransactionByID(string id)
        {
            BunkerPurchase rootObj = new BunkerPurchase();

            rootObj.product = new List<Product>();
            rootObj.delivery_date_range = new DeliveryDateRange();
            rootObj.payment_terms = new PaymentTerms();
            rootObj.market_price = new MarketPrice();
            rootObj.proposal = new Proposal();
            rootObj.offers_items = new List<OffersItem>();
            rootObj.approve_items = new List<ApproveItem>();

            BNK_DATA_DAL dataDal = new BNK_DATA_DAL();
            BNK_DATA data = dataDal.GetByID(id);
            if (data != null)
            {
                rootObj.date_purchase = (data.BDA_PURCHASE_DATE == DateTime.MinValue || data.BDA_PURCHASE_DATE == null) ? "" : Convert.ToDateTime(data.BDA_PURCHASE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.vessel = data.BDA_FK_VESSEL;
                rootObj.vesselothers = data.BDA_VESSEL_OTHERS;
                rootObj.trip_no = data.BDA_TRIP_NO;
                rootObj.type_of_purchase = data.BDA_TYPE_OF_PURCHASE;
                rootObj.supplying_location = data.BDA_SUPPLYING_LOCATION;

                BNK_PRODUCT_ITEMS_DAL prdDal = new BNK_PRODUCT_ITEMS_DAL();
                List<BNK_PRODUCT_ITEMS> lstProduct = prdDal.GetByID(id);
                foreach (var items in lstProduct)
                {
                    Product item = new Product();
                    item.product_order = items.BPI_ORDER;
                    item.product_name = items.BPI_PRODUCT_NAME;
                    item.product_spec = items.BPI_PRODUCT_SPEC;
                    item.product_qty = items.BPI_PRODUCT_QTY;
                    item.product_unit = items.BPI_PRODUCT_UNIT;
                    rootObj.product.Add(item);
                }

                rootObj.contract_type = data.BDA_CONTRACT_TYPE;

                rootObj.delivery_date_range.date_from = (data.BDA_DELIVERY_DATE_FROM == DateTime.MinValue || data.BDA_DELIVERY_DATE_FROM == null) ? "" : Convert.ToDateTime(data.BDA_DELIVERY_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.delivery_date_range.date_to = (data.BDA_DELIVERY_DATE_TO == DateTime.MinValue || data.BDA_DELIVERY_DATE_TO == null) ? "" : Convert.ToDateTime(data.BDA_DELIVERY_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.voyage = data.BDA_VOYAGE;
                rootObj.explanation = data.BDA_EXPLANATION;
                

                string strExplanationAttach = "";
                BNK_ATTACH_FILE_DAL fileDal = new BNK_ATTACH_FILE_DAL();
                List<BNK_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "EXT");
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.BAF_PATH + ":" + item.BAF_INFO + "|";
                }
                rootObj.explanationAttach = strExplanationAttach;

                rootObj.remark = data.BDA_OTHER_SPECIAL;

                rootObj.payment_terms.payment_term = data.BDA_PAYMENT_TERMS;
                rootObj.payment_terms.payment_term_detail = data.BDA_PAYMENT_TERMS_DETAIL;
                rootObj.payment_terms.sub_payment_term = data.BDA_SUB_PAYMENT_TERMS;

                //rootObj.market_price.market_price_date = string.IsNullOrEmpty(data.BDA_MARKET_PRICE_DATE) ? "" : Convert.ToDateTime(data.BDA_MARKET_PRICE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.market_price.market_price_date = data.BDA_MARKET_PRICE_DATE;
                rootObj.market_price.market_price_nsfo380cst = data.BDA_MARKET_PRICE_NSFO380CST;
                rootObj.market_price.market_price_nsfo180cst = data.BDA_MARKET_PRICE_NSFO180CST;
                rootObj.market_price.market_price_mgo = data.BDA_MARKET_PRICE_MGO;

                rootObj.proposal.award_to = data.BDA_PROPOSAL_AWARD_TO;
                rootObj.proposal.award_price = data.BDA_PROPOSAL_AWARD_PRICE;
                rootObj.proposal.reason = data.BDA_PROPOSAL_REASON;
                rootObj.proposal.reasondetail = data.BDA_PROPOSAL_DETAIL;
                rootObj.reason = data.BDA_REASON;

                rootObj.notes = data.BDA_NOTE;
                rootObj.trader_by = "";
                rootObj.section_head_by = "";
                rootObj.request_date = (data.BDA_REQUESTED_DATE == DateTime.MinValue || data.BDA_REQUESTED_DATE == null) ? "" : Convert.ToDateTime(data.BDA_REQUESTED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.currency = data.BDA_CURRENCY_CODE;
                rootObj.currency_symbol = data.BDA_CURRENCY_SYMBOL;

                BNK_OFFER_ITEMS_DAL offDal = new BNK_OFFER_ITEMS_DAL();
                List<BNK_OFFER_ITEMS> offlst = offDal.GetByID(id);
                foreach (var off in offlst)
                {
                    OffersItem newoffer = new OffersItem();
                    newoffer.round_items = new List<RoundItem>();

                    newoffer.supplier = off.BOI_FK_VENDOR;
                    newoffer.contact_person = off.BOI_CONTACT_PERSON;
                    newoffer.purchasing_term = off.BOI_PURCHASING_TERM;
                    newoffer.others_cost_barge = off.BOI_OTHERS_COST_BARGE;
                    newoffer.others_cost_surveyor = off.BOI_OTHERS_COST_SURVEYOR;
                    newoffer.final_price = off.BOI_FINAL_PRICE;
                    newoffer.total_price = off.BOI_TOTLA_PRICE;
                    newoffer.final_flag = off.BOI_FINAL_FLAG;
                    newoffer.note = off.BOI_NOTE;

                    BNK_ROUND_ITEMS_DAL itemDal = new BNK_ROUND_ITEMS_DAL();
                    List<BNK_ROUND_ITEMS> itemlst = itemDal.GetByID(off.BOI_ROW_ID);
                    foreach (var item in itemlst)
                    {
                        RoundItem newitem = new RoundItem();
                        newitem.round_info = new List<RoundInfo>();
                        newitem.round_no = item.BRI_ROUND_NO;
                      
                        BNK_ROUND_DAL roundDal = new BNK_ROUND_DAL();
                        List<BNK_ROUND> roundlst = roundDal.GetByID(item.BRI_ROW_ID);

                        if (data.BDA_TYPE == ConstantPrm.SYSTEM.VESSEL)
                        {
                            BNK_PRODUCT_ITEMS_DAL productDal = new BNK_PRODUCT_ITEMS_DAL();
                            List<BNK_PRODUCT_ITEMS> productlst = productDal.GetByID(id);
                            RoundInfo newround = new RoundInfo();
                            foreach (var type in productlst)
                            {
                                var roundValue = roundlst.Where(x => x.BRD_ROUND_TYPE == type.BPI_PRODUCT_NAME).Select(x => new { x.BRD_ROUND_VALUE, x.BRD_ROUND_ORDER }).OrderBy(x => x.BRD_ROUND_ORDER).ToList();
                                newround = new RoundInfo();
                                newround.round_type = type.BPI_PRODUCT_NAME;
                                newround.round_value = new List<RoundValues>();
                                foreach (var rVal in roundValue)
                                {
                                    RoundValues rv = new RoundValues();
                                    rv.value = rVal.BRD_ROUND_VALUE;
                                    rv.order = rVal.BRD_ROUND_ORDER;
                                    newround.round_value.Add(rv);
                                }
                                newitem.round_info.Add(newround);
                            }
                        } else
                        {
                            var roundType = roundlst.Select(x => new { x.BRD_ROUND_TYPE }).ToList().Distinct();
                            RoundInfo newround = new RoundInfo();
                            foreach (var type in roundType)
                            {
                                var roundValue = roundlst.Where(x => x.BRD_ROUND_TYPE == type.BRD_ROUND_TYPE).Select(x => new { x.BRD_ROUND_VALUE, x.BRD_ROUND_ORDER }).OrderBy(x => x.BRD_ROUND_ORDER).ToList();
                                newround = new RoundInfo();
                                newround.round_type = type.BRD_ROUND_TYPE;
                                newround.round_value = new List<RoundValues>();
                                foreach (var rVal in roundValue)
                                {
                                    RoundValues rv = new RoundValues();
                                    rv.value = rVal.BRD_ROUND_VALUE;
                                    rv.order = rVal.BRD_ROUND_ORDER;
                                    newround.round_value.Add(rv);
                                }
                                newitem.round_info.Add(newround);
                            }
                        }

                        newoffer.round_items.Add(newitem);

                    }

                    rootObj.offers_items.Add(newoffer);
                }

                rootObj.approve_items = null;
                rootObj.reason = data.BDA_REASON;


            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getAttFileByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            BNK_ATTACH_FILE_DAL fileDal = new BNK_ATTACH_FILE_DAL();
            List<BNK_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].BAF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        public ReturnValue AddBunkerExplanation(string explanation, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            BNK_DATA_DAL dataDAL = new BNK_DATA_DAL();
                            BNK_DATA dataCHI = new BNK_DATA();
                            dataCHI.BDA_ROW_ID = transId;
                            dataCHI.BDA_UPDATED = DateTime.Now;
                            dataCHI.BDA_UPDATED_BY = pUser;
                            dataCHI.BDA_EXPLANATION = explanation;
                            dataDAL.UpdateExplanation(dataCHI, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue AddBunkerNote(string note, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            BNK_DATA_DAL dataDAL = new BNK_DATA_DAL();
                            BNK_DATA dataCHI = new BNK_DATA();
                            dataCHI.BDA_ROW_ID = transId;
                            dataCHI.BDA_UPDATED = DateTime.Now;
                            dataCHI.BDA_UPDATED_BY = pUser;
                            dataCHI.BDA_NOTE = note; 
                            dataDAL.UpdateNote(dataCHI, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }
    }
}