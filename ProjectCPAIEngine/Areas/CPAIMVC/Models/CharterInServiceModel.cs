﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CharterInServiceModel
    {
        public static string getTransactionByID(string id)
        {
            ChiRootObject rootObj = new ChiRootObject();
            rootObj.chi_evaluating = new ChiEvaluating();
            rootObj.chi_cargo_detail = new ChiCargoDetail();
            rootObj.chi_cargo_detail.chi_load_ports = new List<ChiLoadPort>();
            rootObj.chi_cargo_detail.discharge_ports = new List<ChiDisPort>();
            rootObj.chi_chartering_method = new ChiCharteringMethod();
            rootObj.chi_chartering_method.chi_type_of_fixings = new List<ChiTypeOfFixing>();
            rootObj.chi_negotiation_summary = new ChiNegotiationSummary();
            rootObj.chi_negotiation_summary.chi_offers_items = new List<ChiOffersItem>();
            rootObj.chi_proposed_for_approve = new ChiProposedForApprove();

            CHI_DATA_DAL dataDal = new CHI_DATA_DAL();
            CHI_DATA data = dataDal.GetCHIDATAByID(id);
            if (data != null)
            {
                rootObj.chi_evaluating.date_purchase = (data.IDA_DATE_PURCHASE == DateTime.MinValue || data.IDA_DATE_PURCHASE == null) ? "" : Convert.ToDateTime(data.IDA_DATE_PURCHASE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chi_evaluating.loading_month = data.IDA_LOADING_MONTH;
                rootObj.chi_evaluating.loading_year = data.IDA_LOADING_YEAR;
                //n/a
                rootObj.chi_evaluating.laycan_from = (data.IDA_P_LAYCAN_FROM == DateTime.MinValue || data.IDA_P_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.IDA_P_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //n/a
                rootObj.chi_evaluating.laycan_to = (data.IDA_P_LAYCAN_TO == DateTime.MinValue || data.IDA_P_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.IDA_P_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chi_evaluating.freight = data.IDA_FREIGHT;
                rootObj.chi_evaluating.vessel_size = data.IDA_VESSEL_SIZE;
                rootObj.chi_evaluating.year = data.IDA_YEAR;
                //n/a
                rootObj.chi_evaluating.flat_rate = data.IDA_P_FLAT_RATE;
                rootObj.chi_evaluating.market_ref = data.IDA_TD_MARKET;

                rootObj.chi_cargo_detail.route = data.IDA_ROUTE;
                CHI_LOAD_PORT_DAL lpDal = new CHI_LOAD_PORT_DAL();
                List<CHI_LOAD_PORT> lplst = lpDal.GetPortByDataID(id);
                foreach (var lp in lplst)
                {
                    ChiLoadPort newport = new ChiLoadPort();
                    newport.order_port = lp.IPT_ORDER_PORT;
                    newport.port = lp.IPT_FK_PORT.ToString();
                    newport.crude = lp.IPT_FK_MATERIALS;
                    newport.supplier = lp.IPT_FK_VENDOR;
                    newport.kbbls = lp.IPT_KBBLS;
                    newport.ktons = lp.IPT_KTONS;
                    newport.mtons = lp.IPT_MTONS;
                    newport.tolerance = lp.IPT_TOLERANCE;
                    newport.date = lp.IPT_DATE;
                    rootObj.chi_cargo_detail.chi_load_ports.Add(newport);
                }

                CHI_DISCHARGE_PORT_DAL dpDal = new CHI_DISCHARGE_PORT_DAL();
                List<CHI_DISCHARGE_PORT> dplst = dpDal.GetPortByDataID(id);
                foreach (var dp in dplst)
                {
                    ChiDisPort newport = new ChiDisPort();
                    newport.dis_order_port = dp.IDP_ORDER_PORT;
                    newport.dis_port = dp.IDP_PORT;
                    rootObj.chi_cargo_detail.discharge_ports.Add(newport);
                }

                rootObj.chi_chartering_method.reason = data.IDA_EXPLANATION;
                if (data.IDA_TYPE_OF_FIXINGS == "Marketing Order")
                {
                    rootObj.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Marketing Order", type_flag = "Y" });
                    rootObj.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Private Order", type_flag = "N" });
                } else
                {
                    rootObj.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Marketing Order", type_flag = "N" });
                    rootObj.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Private Order", type_flag = "Y" });
                }

                CHI_OFFER_ITEMS_DAL offDal = new CHI_OFFER_ITEMS_DAL();
                List<CHI_OFFER_ITEMS> offlst = offDal.GetOfferItemByDataID(id);
                foreach(var off in offlst)
                {
                    ChiOffersItem newoffer = new ChiOffersItem();
                    newoffer.chi_round_items = new List<ChiRoundItem>();
                    newoffer.broker = off.IOI_FK_VENDOR;
                    newoffer.vessel = off.IOI_FK_VEHICLE;
                    newoffer.year = off.IOI_YEAR;
                    newoffer.type = off.IOI_TYPE;
                    newoffer.KDWT = off.IOI_KDWT;
                    newoffer.owner = off.IOI_OWNER;
                    newoffer.final_flag = off.IOI_FINAL_FLAG;
                    newoffer.final_deal = off.IOI_FINAL_DEAL;

                    CHI_ROUND_ITEMS_DAL itemDal = new CHI_ROUND_ITEMS_DAL();
                    List<CHI_ROUND_ITEMS> itemlst = itemDal.GetRoundByOfferItemID(off.IOI_ROW_ID);
                    foreach (var item in itemlst)
                    {
                        ChiRoundItem newitem = new ChiRoundItem();
                        newitem.chi_round_info = new List<ChiRoundInfo>();
                        newitem.round_no = item.IRI_ROUND_NO;

                        CHI_ROUND_DAL roundDal = new CHI_ROUND_DAL();
                        List<CHI_ROUND> roundlst = roundDal.GetRoundByRoundItemID(item.IRI_ROW_ID);
                        int roundCount = 1;
                        foreach(var round in roundlst)
                        {
                            ChiRoundInfo newround = new ChiRoundInfo();
                            newround.order = string.IsNullOrEmpty(round.IRR_ORDER) ? roundCount + "" : round.IRR_ORDER;
                            newround.type = round.IIR_ROUND_TYPE;
                            newround.offer = round.IIR_ROUND_OFFER;
                            newround.counter = round.IIR_ROUND_COUNTER;

                            newitem.chi_round_info.Add(newround);
                            roundCount++;
                        }
                        newoffer.chi_round_items.Add(newitem);
                    }
                    
                    rootObj.chi_negotiation_summary.chi_offers_items.Add(newoffer);
                }

                rootObj.chi_proposed_for_approve.owner = data.IDA_P_OWNER;
                rootObj.chi_proposed_for_approve.charter_patry = data.IDA_P_CHARTER_PATRY;
                rootObj.chi_proposed_for_approve.owner_broker = data.IDA_P_OWNER_BROKER;
                rootObj.chi_proposed_for_approve.charterer_broker = data.IDA_P_FK_CHARTERER_BROKER;
                rootObj.chi_proposed_for_approve.vessel_name = data.IDA_P_FK_VESSEL_NAME;
                rootObj.chi_proposed_for_approve.laycan_from = (data.IDA_P_LAYCAN_FROM == DateTime.MinValue || data.IDA_P_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.IDA_P_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chi_proposed_for_approve.laycan_to = (data.IDA_P_LAYCAN_TO == DateTime.MinValue || data.IDA_P_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.IDA_P_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chi_proposed_for_approve.loading = data.IDA_P_LOADING;
                rootObj.chi_proposed_for_approve.discharging = data.IDA_P_DISCHARGING;
                rootObj.chi_proposed_for_approve.laytime = data.IDA_P_LAYTIME;
                rootObj.chi_proposed_for_approve.crude_oil_washing = data.IDA_P_CRUDE_OIL_WASHING;
                rootObj.chi_proposed_for_approve.special_requirment = data.IDA_P_SPECIAL_REQUIRMENT;
                rootObj.chi_proposed_for_approve.vessel_built = data.IDA_P_VESSEL_BUILT;
                rootObj.chi_proposed_for_approve.capacity = data.IDA_P_CAPACITY;
                rootObj.chi_proposed_for_approve.laden_speed = data.IDA_P_LADEN_SPEED;
                rootObj.chi_proposed_for_approve.exten_cost = data.IDA_P_EXTEN_COST;
                rootObj.chi_proposed_for_approve.flat_rate = data.IDA_P_FLAT_RATE;
                rootObj.chi_proposed_for_approve.freight_rate = data.IDA_P_FREIGHT_RATE;
                rootObj.chi_proposed_for_approve.est_total = data.IDA_P_EST_TOTAL;
                rootObj.chi_proposed_for_approve.est_freight = data.IDA_P_EST_FREIGHT;

                rootObj.chi_proposed_for_approve.address_com = data.IDA_ADDRESS_COM;
                rootObj.chi_proposed_for_approve.address_com_percent = data.IDA_ADDRESS_COM_PERCENT;
                rootObj.chi_proposed_for_approve.net_freight = data.IDA_NET_FREIGHT;

                rootObj.chi_reason = data.IDA_REASON;
                rootObj.chi_note = data.IDA_REMARK;
                rootObj.approve_items = null;
                //n/a
                string strExplanationAttach = "";
                CHI_ATTACH_FILE_DAL fileDal = new CHI_ATTACH_FILE_DAL();
                List<CHI_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "EXT");
                //var extFile = filelst.Where(x => x.IAF_TYPE == "EXT").ToList();
                foreach(var item in extFile)
                {
                    strExplanationAttach += item.IAF_PATH + ":" + item.IAF_INFO + "|";
                }
                rootObj.explanationAttach = strExplanationAttach;
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public ReturnValue AddCharterFor(CharterInViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {                                  
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_DATA_DAL dataDAL = new CHIT_DATA_DAL();
                            CHIT_DATA dataCHIT = new CHIT_DATA();
                            dataCHIT.IDAT_ROW_ID = transId; 
                            dataCHIT.IDAT_UPDATED = DateTime.Now;
                            dataCHIT.IDAT_UPDATED_BY = pUser;
                            dataCHIT.IDAT_SELECT_CHARTER_FOR = vm.chit_negotiation_summary.select_charter_for;
                            dataDAL.UpdateSelect(dataCHIT, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue AddTraderNote(CharterInViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_DATA_DAL dataDAL = new CHIT_DATA_DAL();
                            CHIT_DATA dataCHIT = new CHIT_DATA();
                            dataCHIT.IDAT_ROW_ID = transId;
                            dataCHIT.IDAT_UPDATED = DateTime.Now;
                            dataCHIT.IDAT_UPDATED_BY = pUser;
                            dataCHIT.IDAT_CHARTER_FOR_CMMENT = vm.chit_detail.ReasonTrader;
                            dataDAL.UpdateNote(dataCHIT, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public static string getTransactionCMMTByID(string id)
        {
            ChitRootObject rootObj = new ChitRootObject();
            rootObj.chit_negotiation_summary = new ChitNegotiationSummary();
            rootObj.chit_negotiation_summary.chit_offers_items = new List<ChitOffersItem>();
            rootObj.chit_proposed_for_approve = new ChitProposedForApprove();
            rootObj.chit_detail = new ChitDetail();
            rootObj.chit_detail.chit_load_port_control = new List<Model.ChitLoadPortControl>();
            rootObj.chit_detail.chit_discharge_port_control = new List<ChitDischargePortControl>();
            rootObj.chit_detail.chit_cargo_qty = new List<Model.ChitCargoQty>();

            rootObj.chit_evaluating = new ChitEvaluating();
            rootObj.chit_chartering_method = new ChitCharteringMethod();

            CHIT_DATA_DAL dataDal = new CHIT_DATA_DAL();
            CHIT_DATA data = dataDal.GetCHIDATAByID(id);
            if (data != null)
            {
                CHIT_PORT_DAL lpDal = new CHIT_PORT_DAL();
                List<CHIT_PORT> lplst = lpDal.GetPortByDataID(id);
                List<MT_JETTY> mt_port = PortDAL.GetPortData();

                foreach (var lp in lplst.Where(x => x.CHTP_PORT_TYPE.ToUpper() == "L").ToList())
                {
                    ChitLoadPortControl newport = new ChitLoadPortControl();
                    MT_JETTY _itemPort = mt_port.Where(x => x.MTJ_ROW_ID == lp.CHTP_FK_PORT).FirstOrDefault();
                    newport.PortOrder = lp.CHTP_ORDER_PORT;
                    //newport.PortType = (_itemPort != null) ? _itemPort.MTJ_PORT_TYPE : "";
                    //newport.PortCountry = (_itemPort != null) ? _itemPort.MTJ_MT_COUNTRY : "";
                    //newport.PortLocation = (_itemPort != null) ? _itemPort.MTJ_LOCATION : "";
                    //newport.PortName = (_itemPort != null) ? _itemPort.MTJ_JETTY_NAME + ", " + _itemPort.MTJ_LOCATION + ", " + _itemPort.MTJ_MT_COUNTRY + "|" + _itemPort.MTJ_ROW_ID : "";
                    newport.PortName = lp.CHTP_VALUE;
                    newport.PortOtherDesc = lp.CHTP_OTHER_DESC;
                    rootObj.chit_detail.chit_load_port_control.Add(newport);
                }

                foreach (var lp in lplst.Where(x => x.CHTP_PORT_TYPE.ToUpper() == "D").ToList())
                {
                    ChitDischargePortControl newport = new ChitDischargePortControl();
                    MT_JETTY _itemPort = mt_port.Where(x => x.MTJ_ROW_ID == lp.CHTP_FK_PORT).FirstOrDefault();
                    newport.PortOrder = lp.CHTP_ORDER_PORT;
                    //newport.PortType = (_itemPort != null) ? _itemPort.MTJ_PORT_TYPE : "";
                    //newport.PortCountry = (_itemPort != null) ? _itemPort.MTJ_MT_COUNTRY : "";
                    //newport.PortLocation = (_itemPort != null) ? _itemPort.MTJ_LOCATION : "";
                    //newport.PortName = (_itemPort != null) ? _itemPort.MTJ_JETTY_NAME + ", " + _itemPort.MTJ_LOCATION + ", " + _itemPort.MTJ_MT_COUNTRY + "|" + _itemPort.MTJ_ROW_ID : "";
                    newport.PortName = lp.CHTP_VALUE;
                    newport.PortOtherDesc = lp.CHTP_OTHER_DESC;
                    rootObj.chit_detail.chit_discharge_port_control.Add(newport);
                }
                         
                CHIT_CARGO_DAL lcDal = new CHIT_CARGO_DAL();
                List<CHIT_CARGO> cglst = lcDal.GetCargoByDataID(id);
                foreach (var _item in cglst)
                {
                    ChitCargoQty cgItem = new ChitCargoQty();
                    cgItem.cargo = _item.CHTC_FK_CARGO;
                    cgItem.order = _item.CHTC_ORDER;
                    cgItem.qty = _item.CHTC_QUANTITY;
                    cgItem.tolerance = _item.CHTC_TOLERANCE;
                    cgItem.unit = _item.CHTC_UNIT;
                    cgItem.Others = _item.CHTC_UNIT_OTHERS;
                    rootObj.chit_detail.chit_cargo_qty.Add(cgItem);
                }



                CHIT_OFFER_ITEMS_DAL offDal = new CHIT_OFFER_ITEMS_DAL();
                List<CHIT_OFFER_ITEMS> offlst = offDal.GetOfferItemByDataID(id);
                foreach (var off in offlst)
                {
                    ChitOffersItem newoffer = new ChitOffersItem();
                    newoffer.chit_round_items = new List<ChitRoundItem>();
                    newoffer.broker = off.ITOI_FK_VENDOR_BK;
                    newoffer.broker_init = MT_VENDOR_DAL.GetNameShort(off.ITOI_FK_VENDOR_BK);
                    newoffer.vessel = off.ITOI_VEHICLE;
                    newoffer.type = off.ITOI_TYPE;
                    newoffer.year = off.ITOI_YEAR;
                    newoffer.KDWT = off.ITOI_KDWT;
                    newoffer.owner = off.ITOI_VENDOR_OWNER;
                    newoffer.final_flag = off.ITOI_FINAL_FLAG;
                    newoffer.final_deal = off.ITOI_FINAL_DEAL == null ? "" : off.ITOI_FINAL_DEAL.Replace("\r\n", "#");
                    newoffer.finalremark = off.ITOI_REMARK;
                    CHIT_ROUND_ITEMS_DAL itemDal = new CHIT_ROUND_ITEMS_DAL();
                    List<CHIT_ROUND_ITEMS> itemlst = itemDal.GetRoundByOfferItemID(off.ITOI_ROW_ID);
                    foreach (var item in itemlst)
                    {
                        ChitRoundItem newitem = new ChitRoundItem();
                        newitem.chit_round_info = new List<ChitRoundInfo>();
                        newitem.round_no = item.ITRI_ROUND_NO;

                        CHIT_ROUND_DAL roundDal = new CHIT_ROUND_DAL();
                        List<CHIT_ROUND> roundlst = roundDal.GetRoundByRoundItemID(item.ITRI_ROW_ID);
                        int roundCount = 1;
                        foreach (var round in roundlst)
                        {
                            ChitRoundInfo newround = new ChitRoundInfo();
                            newround.order = string.IsNullOrEmpty(round.ITIR_ORDER) ? roundCount + "" : round.ITIR_ORDER;
                            newround.type = round.ITIR_ROUND_TYPE;
                            newround.typeOther = round.ITIR_TYPE_OTHERS;
                            newround.offer = round.ITIR_ROUND_OFFER;
                            newround.counter = round.ITIR_ROUND_COUNTER;

                            newitem.chit_round_info.Add(newround);
                            roundCount++;
                        }
                        newoffer.chit_round_items.Add(newitem);
                    }

                    rootObj.chit_negotiation_summary.chit_offers_items.Add(newoffer);
                }


                rootObj.chit_proposed_for_approve.laycan_from = (data.IDAT_LAYCAN_FROM == DateTime.MinValue || data.IDAT_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.IDAT_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chit_proposed_for_approve.laycan_to = (data.IDAT_LAYCAN_TO == DateTime.MinValue || data.IDAT_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.IDAT_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chit_detail.bss = data.IDAT_BSS;
                rootObj.chit_proposed_for_approve.charter_patry = data.IDAT_D_CHARTER_PATRY_FROM;
                rootObj.chit_proposed_for_approve.vessel_name = data.IDAT_D_VESSEL;
                rootObj.chit_proposed_for_approve.broker_name = data.IDAT_D_OWNER_BROKER;
                rootObj.chit_proposed_for_approve.freight = data.IDAT_D_FREIGHT;
                rootObj.chit_proposed_for_approve.laytime = data.IDAT_D_LAYTIME;
                rootObj.chit_proposed_for_approve.demurrage = data.IDAT_D_DEM;
                rootObj.chit_proposed_for_approve.payment_term = data.IDAT_D_PAYMENT_TERM;
                rootObj.chit_proposed_for_approve.charterer_brokerCom = data.IDAT_D_CHARTER_BROKER_COM;
                rootObj.chit_proposed_for_approve.broker_commission = data.IDAT_D_OWNER_BROKER_COM;
                rootObj.chit_proposed_for_approve.address_commission = data.IDAT_D_ADDRESS_COM;
                rootObj.chit_proposed_for_approve.withholding_tax = data.IDAT_D_WITHHOLDING_TAX;
                rootObj.chit_proposed_for_approve.vessel_built = data.IDAT_D_VESSEL_BUILT;
                rootObj.chit_proposed_for_approve.capacity = data.IDAT_D_KDWT;
                rootObj.chit_proposed_for_approve.Other = data.IDAT_D_OTHERS;
                rootObj.chit_proposed_for_approve.owner = data.IDAT_D_OWNER_BROKER;
                rootObj.chit_proposed_for_approve.total_freight = data.IDAT_TOTAL_FREIGHT;
                rootObj.chit_proposed_for_approve.charterer_broker = (string.IsNullOrEmpty(data.IDAT_D_CHARTERER_BROKER))?" ":data.IDAT_D_CHARTERER_BROKER;
                rootObj.chit_proposed_for_approve.net_freight = data.IDAT_NET_FREIGHT;
                rootObj.chit_detail.Reason = data.IDAT_EXPLANATION;
                rootObj.chit_detail.charterer = data.IDAT_FK_CUST;
                rootObj.chit_detail.date = (data.IDAT_DATE_PURCHASE!=null)?Convert.ToDateTime(data.IDAT_DATE_PURCHASE).ToString("dd/MM/yyyy"):"";
                rootObj.chit_reason = data.IDAT_REASON;
                rootObj.chit_note = data.IDAT_REMARK;
                rootObj.approve_items = null;
                rootObj.chit_detail.NegoUnit = data.IDAT_NEGOTIATION_UNIT;
                //n/a
                string strExplanationAttach = "";
                CHIT_ATTACH_FILE_DAL fileDal = new CHIT_ATTACH_FILE_DAL();
                List<CHIT_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "EXT");
                //var extFile = filelst.Where(x => x.IAF_TYPE == "EXT").ToList();
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.ITAF_PATH + ":" + item.ITAF_INFO + "|";
                }
                rootObj.explanationAttach = strExplanationAttach;
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getAttFileByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            CHIT_ATTACH_FILE_DAL fileDal = new CHIT_ATTACH_FILE_DAL();
            List<CHIT_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for(int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].ITAF_PATH + "\""; 

                    if (i < extFile.Count-1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}",strFile);

            return strJSON;
        }

        public static List<ChitListDropDownKey> getPortJetty(string fromType = "", string portType = "", string country = "", string location = "", string name = "")
        {
            List<MT_JETTY> mt_port = PortDAL.GetPortData();
            if (fromType != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_CREATE_TYPE.Trim().ToUpper() == fromType.Trim().ToUpper()).OrderBy(x => x.MTJ_PORT_TYPE).ToList();
            }
            if (portType != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_PORT_TYPE.Trim().ToUpper() == portType.Trim().ToUpper()).OrderBy(x => x.MTJ_MT_COUNTRY).ToList();
            }
            if (country != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_MT_COUNTRY.Trim().ToUpper() == country.Trim().ToUpper()).OrderBy(x => x.MTJ_LOCATION).ToList();
            }
            if (location != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_LOCATION.Trim().ToUpper() == location.Trim().ToUpper()).OrderBy(x => x.MTJ_JETTY_NAME).ToList();
            }
            if (name != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_JETTY_NAME.ToUpper().Trim() == name.ToUpper().Trim()).OrderBy(x => x.MTJ_JETTY_NAME).ToList();
            }

            if (fromType != "" && portType != "" && country != "" && location != "" && name != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_ROW_ID).ToList(), mt_port.Select(x => x.MTJ_JETTY_NAME).ToList(), mt_port.Select(x => (x.MTJ_JETTY_NAME + ", " + x.MTJ_LOCATION + ", " + x.MTJ_MT_COUNTRY + "|" + x.MTJ_ROW_ID)).ToList());
            }
            else if (fromType != "" && portType != "" && country != "" && location != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_ROW_ID).ToList(), mt_port.Select(x => x.MTJ_JETTY_NAME).ToList(), mt_port.Select(x => (x.MTJ_JETTY_NAME + ", " + x.MTJ_LOCATION + ", " + x.MTJ_MT_COUNTRY+"|"+x.MTJ_ROW_ID)).ToList());
            }
            else if (fromType != "" && portType != "" && country != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList(), mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList(), mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList());
            }
            else if (fromType != "" && portType != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList(), mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList(), mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList());
            }
            else if (fromType != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList(), mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList(), mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList());
            }
            else
                return new List<ChitListDropDownKey>();

        }

        private static List<ChitListDropDownKey> mapSelectListKey(List<string> key, List<string> text, List<string> value)
        {
            List<ChitListDropDownKey> selectList = new List<ChitListDropDownKey>();
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new ChitListDropDownKey { Key = key[i], Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public ReturnValue AddCharterNote(CharterInViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHI_DATA_DAL dataDAL = new CHI_DATA_DAL();
                            CHI_DATA dataCHI = new CHI_DATA();
                            dataCHI.IDA_ROW_ID = transId;
                            dataCHI.IDA_UPDATED = DateTime.Now;
                            dataCHI.IDA_UPDATED_BY = pUser;
                            dataCHI.IDA_REMARK = vm.chi_note;
                            dataCHI.IDA_EXPLANATION = vm.chi_chartering_method.reason;
                            dataDAL.UpdateNote(dataCHI, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue AddCharterNoteCMMT(CharterInViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_DATA_DAL dataDAL = new CHIT_DATA_DAL();
                            CHIT_DATA dataCHIT = new CHIT_DATA();
                            dataCHIT.IDAT_ROW_ID = transId;
                            dataCHIT.IDAT_UPDATED = DateTime.Now;
                            dataCHIT.IDAT_UPDATED_BY = pUser;
                            dataCHIT.IDAT_REMARK = vm.chit_note;
                            dataCHIT.IDAT_EXPLANATION = vm.chit_detail.Reason;
                            dataDAL.UpdateNoteCMMT(dataCHIT, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue AddCharterTraderUploadCMMT(CharterInViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue(); 
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_ATTACH_FILE_DAL dataAttachFileDAL = new CHIT_ATTACH_FILE_DAL();
                            CHIT_ATTACH_FILE dataCHIAF = new CHIT_ATTACH_FILE();
                            DateTime dtNow = DateTime.Now;
                            foreach (var item in vm.explanationAttachTrader.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList(); 
                                    dataCHIAF.ITAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHIAF.ITAF_FK_CHIT_DATA = transId;
                                    dataCHIAF.ITAF_PATH = text[0];
                                    dataCHIAF.ITAF_INFO = text[1];
                                    dataCHIAF.ITAF_TYPE = "TRA";
                                    dataCHIAF.ITAF_CREATED = dtNow;
                                    dataCHIAF.ITAF_CREATED_BY = pUser;
                                    dataCHIAF.ITAF_UPDATED = dtNow;
                                    dataCHIAF.ITAF_UPDATED_BY = pUser;
                                    dataAttachFileDAL.Save(dataCHIAF, context);
                                    dbContextTransaction.Commit();
                                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                    rtn.Status = true;
                                }
                            }
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            return rtn;
        }

        public ReturnValue AddCharterTraderAttachCMMT(string file, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_ATTACH_FILE_DAL dataAttachFileDAL = new CHIT_ATTACH_FILE_DAL();
                            CHIT_ATTACH_FILE dataCHIAF = new CHIT_ATTACH_FILE();
                            DateTime dtNow = DateTime.Now;
                            var chkUploaded = context.CHIT_ATTACH_FILE.Where(a => a.ITAF_FK_CHIT_DATA == transId && a.ITAF_TYPE == "ATT").ToList();
                            foreach (var item in file.Split('|'))
                            {
                                //var chkUploaded = context.CHIT_ATTACH_FILE.SingleOrDefault(a => a.ITAF_FK_CHIT_DATA == transId && a.ITAF_TYPE == "ATT" && a.ITAF_PATH == item);
                                if(!String.IsNullOrEmpty(item))
                                {
                                    var chkFileUploaded = chkUploaded.Any(a => a.ITAF_PATH == item);
                                    if (!chkFileUploaded)
                                    {
                                        dataCHIAF = new CHIT_ATTACH_FILE();
                                        dataCHIAF.ITAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCHIAF.ITAF_FK_CHIT_DATA = transId;
                                        dataCHIAF.ITAF_PATH = item;
                                        dataCHIAF.ITAF_INFO = "";
                                        dataCHIAF.ITAF_TYPE = "ATT";
                                        dataCHIAF.ITAF_CREATED = dtNow;
                                        dataCHIAF.ITAF_CREATED_BY = pUser;
                                        dataCHIAF.ITAF_UPDATED = dtNow;
                                        dataCHIAF.ITAF_UPDATED_BY = pUser;
                                        dataAttachFileDAL.Save(dataCHIAF, context);
                                        dbContextTransaction.Commit();
                                    }
                                }                                
                            }
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public static string getChartererById(string row_id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var sql = (from a in context.MT_CUST_DETAIL
                               join b in context.MT_CUST_PAYMENT_TERM on a.MCD_FK_CUS equals b.MCP_FK_MT_CUST
                               join c in context.MT_VENDOR on b.MCP_BROKER_NAME equals c.VND_ACC_NUM_VENDOR
                               where a.MCD_ROW_ID == row_id
                               select new
                               {
                                   PARTY_FORM = "",
                                   PAYMENT_TERM = b.MCP_PAYMENT_TERM,
                                   BROKER_NAME = c.VND_NAME1,
                                   BROKER_COMM = c.VND_BROKER_COMMISSION,
                                   ADDR_COMM = b.MCP_ADDRESS_COMMISSION,
                                   WITH_TAX = b.MCP_WITHOLDING_TAX
                               }
                               ).FirstOrDefault();
                    if (sql == null)
                    {
                        sql = (from a in context.MT_CUST_DETAIL
                               join b in context.MT_CUST_PAYMENT_TERM on a.MCD_FK_CUS equals b.MCP_FK_MT_CUST                               
                               where a.MCD_ROW_ID == row_id
                               select new
                               {
                                   PARTY_FORM = "",
                                   PAYMENT_TERM = b.MCP_PAYMENT_TERM,
                                   BROKER_NAME = "",
                                   BROKER_COMM = "",
                                   ADDR_COMM = b.MCP_ADDRESS_COMMISSION,
                                   WITH_TAX = b.MCP_WITHOLDING_TAX
                               }
                               ).FirstOrDefault();
                    }

                    return new JavaScriptSerializer().Serialize(sql);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
    
}