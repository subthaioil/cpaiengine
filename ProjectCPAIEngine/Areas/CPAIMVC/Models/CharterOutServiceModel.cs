﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALFreight;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using com.pttict.engine.dal.Entity;
using static ProjectCPAIEngine.Areas.CPAIMVC.ViewModels.CharterOutViewModel;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CharterOutServiceModel
    {
        public static string getTransactionCMMTByID(string id)
        {

            ChotObjectRoot rootObj = new ChotObjectRoot();
            rootObj.chot_no_charter_out_case_a = new ChotNoCharterOutCaseA();
            rootObj.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs = new List<ChotCharterOutCaseAOtherCost>();
            rootObj.chot_charter_out_case_b = new ChotCharterOutCaseB();
            rootObj.chot_charter_out_case_b.chot_est_income_b_one = new ChotEstIncomeBOne();
            rootObj.chot_charter_out_case_b.chot_est_expense_b_two = new ChotEstExpenseBTwo();
            rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one = new ChotEstExpenseBTwoPointOne();
            rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two = new ChotEstExpenseBTwoPointTwo();
            rootObj.chot_summary = new ChotSummary();
            rootObj.chot_summary.chot_cargo = new List<ChotCargo>();
            rootObj.chot_summary.chot_loading_port = new List<ChotLoadingPort>();
            rootObj.chot_summary.chot_discharging_port = new List<ChotDischargingPort>();
            rootObj.chot_result = new ChotResult();
            //rootObj.chot_result.chot_d_result = new ChotDResult();
            //rootObj.chot_result.chot_d_result.chot_saving = new ChotSaving();
            //rootObj.chot_result.chot_d_result.profit = new ChotProfit();

            rootObj.chot_result.chot_saving = new ChotSaving();
            rootObj.chot_result.chot_profit = new ChotProfit();

            CHOT_DATA_DAL dataDal = new CHOT_DATA_DAL();
            CHOT_DATA data = dataDal.GetCHOTDATAByID(id);
            if (data != null)
            {
                rootObj.chot_date = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_DATE, "dd/MM/yyyy");
                rootObj.vessel_id = data.OTDA_FK_VESSEL;
                rootObj.vessel_name = VehicleServiceModel.GetName(data.OTDA_FK_VESSEL);
                rootObj.freight_id = data.OTDA_FK_FREIGHT;
                if (!string.IsNullOrEmpty(data.OTDA_FK_FREIGHT))
                {
                    ChotObjectRoot fcl_obj = getFreightData(data.OTDA_FK_FREIGHT);
                    rootObj.freight_no = fcl_obj.freight_no;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.laycan = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.laycan;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.ex_rate = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.ex_rate;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.ex_date = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.ex_date;

                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.type = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.type;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.freight = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.freight;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.offer = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.offer;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight;
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.total_income = fcl_obj.chot_charter_out_case_b.chot_est_income_b_one.total_income;

                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.port_charge = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.port_charge;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.bunker = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.bunker;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_persent = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_persent;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_total = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_total;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_persent = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_persent;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_total = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_total;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.other = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.other;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.total_variable_costs = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.total_variable_costs;

                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.fix_costs = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.fix_costs;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.no = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.no;
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.total_fixed_costs = fcl_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.total_fixed_costs;

                    rootObj.chot_summary.charterer_name = fcl_obj.chot_summary.charterer_name;
                    rootObj.chot_summary.laycan = fcl_obj.chot_summary.laycan;
                    rootObj.chot_summary.bss = fcl_obj.chot_summary.bss;

                    rootObj.chot_summary.freight = fcl_obj.chot_summary.freight;
                    rootObj.chot_summary.laytime = fcl_obj.chot_summary.laytime;
                    rootObj.chot_summary.demurrage_unit = fcl_obj.chot_summary.demurrage_unit;
                    rootObj.chot_summary.demurrage = fcl_obj.chot_summary.demurrage;
                    rootObj.chot_summary.payment_term = fcl_obj.chot_summary.payment_term;
                    rootObj.chot_summary.charterer_broker = fcl_obj.chot_summary.charterer_broker;
                    rootObj.chot_summary.charterer_broker_comm = fcl_obj.chot_summary.charterer_broker_comm;
                    rootObj.chot_summary.addr_comm = fcl_obj.chot_summary.addr_comm;
                    rootObj.chot_summary.total_comm = fcl_obj.chot_summary.total_comm;
                    rootObj.chot_summary.withholding_tax = fcl_obj.chot_summary.withholding_tax;

                    foreach (var v in fcl_obj.chot_summary.chot_cargo)
                    {
                        rootObj.chot_summary.chot_cargo.Add(new ChotCargo
                        {
                            cargo = v.cargo,
                            cargo_name = v.cargo_name,
                            cargo_qty = v.cargo_qty,
                            cargo_unit = v.cargo_unit,
                            cargo_unit_other = v.cargo_unit_other,
                            cargo_tolerance = v.cargo_tolerance
                        });
                    }

                    foreach (var v in fcl_obj.chot_summary.chot_loading_port)
                    {
                        rootObj.chot_summary.chot_loading_port.Add(new ChotLoadingPort
                        {
                            port_full_name = v.port_full_name
                        });
                    }

                    foreach (var v in fcl_obj.chot_summary.chot_discharging_port)
                    {
                        rootObj.chot_summary.chot_discharging_port.Add(new ChotDischargingPort
                        {
                            port_full_name = v.port_full_name
                        });
                    }
                }


                rootObj.purchase_no = data.OTDA_PURCHASE_NO;
                rootObj.chot_summary.owner_broker = data.OTDA_OWNER_BROKER;
                rootObj.chot_summary.owner_broker_comm = data.OTDA_OWNER_BROKER_COMM;
                rootObj.chot_reason_explanation = data.OTDA_EXPLANATION;
                if (data.OTDA_CASE_A_PERIOD_DATE_FROM != null && data.OTDA_CASE_A_PERIOD_DATE_TO != null)
                {
                    rootObj.chot_no_charter_out_case_a.period = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_A_PERIOD_DATE_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_A_PERIOD_DATE_TO, "dd/MM/yyyy");
                    rootObj.chot_no_charter_out_case_a.period_from = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_A_PERIOD_DATE_FROM, "dd/MM/yyyy");
                    rootObj.chot_no_charter_out_case_a.period_to = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_A_PERIOD_DATE_TO, "dd/MM/yyyy");
                }

                rootObj.chot_no_charter_out_case_a.tc_hire = data.OTDA_CASE_A_TC_HIRE;
                rootObj.chot_no_charter_out_case_a.no_day = data.OTDA_CASE_A_NO_DAY;
                rootObj.chot_no_charter_out_case_a.tc_hire_val = data.OTDA_CASE_A_TOTAL_TC_HIRE;
                rootObj.chot_no_charter_out_case_a.idling = data.OTDA_CASE_A_IDLING;
                rootObj.chot_no_charter_out_case_a.bunker = data.OTDA_CASE_A_BUNKER;
                rootObj.chot_no_charter_out_case_a.total_expense = data.OTDA_TOTAL_EXPENSE;

                //B1 Est. Income
                if (data.OTDA_CASE_B1_PERIOD_DATE_FROM != null && data.OTDA_CASE_B1_PERIOD_DATE_TO != null)
                {
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.period = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B1_PERIOD_DATE_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B1_PERIOD_DATE_TO, "dd/MM/yyyy");
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.period_from = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B1_PERIOD_DATE_FROM, "dd/MM/yyyy");
                    rootObj.chot_charter_out_case_b.chot_est_income_b_one.period_to = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B1_PERIOD_DATE_TO, "dd/MM/yyyy");
                }


                //B2 Est. Expense
                if (data.OTDA_CASE_B2_PERIOD_DATE_FROM != null && data.OTDA_CASE_B2_PERIOD_DATE_TO != null)
                {
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.period = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B2_PERIOD_DATE_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B2_PERIOD_DATE_TO, "dd/MM/yyyy");
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.period_from = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B2_PERIOD_DATE_FROM, "dd/MM/yyyy");
                    rootObj.chot_charter_out_case_b.chot_est_expense_b_two.period_to = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CASE_B2_PERIOD_DATE_TO, "dd/MM/yyyy");
                }

                rootObj.chot_charter_out_case_b.chot_est_expense_b_two.total_expense = rootObj.chot_charter_out_case_b.chot_est_expense_b_two.total_expense;

                //Summary
                rootObj.chot_summary.charterer = data.OTDA_FK_CHARTERER;
                rootObj.chot_summary.cp_date = ShareFn.ConvertDateTimeToDateStringFormat(data.OTDA_CP_DATE, "dd/MM/yyyy");
                rootObj.chot_summary.charterer_party_form = data.OTDA_CHARTERER_PARTY_FORM;

                rootObj.chot_summary.owner_broker = data.OTDA_OWNER_BROKER;
                rootObj.chot_summary.owner_broker_comm = data.OTDA_OWNER_BROKER_COMM;

                //rootObj.chot_result.a_total_expense = data.OTDA_TOTAL_EXPENSE;
                //rootObj.chot_result.b_total_income = data.OTDA_TOTAL_INCOME;
                //rootObj.chot_result.c_total_net_income = data.OTDA_TOTAL_NET_INCOME;
                //rootObj.chot_result.chot_d_result.flag = data.OTDA_RESULT_FLAG;

                rootObj.chot_result.flag = data.OTDA_RESULT_FLAG;
                if (!string.IsNullOrEmpty(data.OTDA_RESULT_FLAG))
                {
                    if (data.OTDA_RESULT_FLAG.ToUpper() == "SAVING")
                    {
                        rootObj.chot_result.chot_saving.saving_total_expense = data.OTDA_TOTAL_EXPENSE;
                        rootObj.chot_result.chot_saving.saving_exp_inc = data.OTDA_TOTAL_INCOME;
                        rootObj.chot_result.chot_saving.saving_total = data.OTDA_TOTAL_NET_INCOME;
                    }
                    else
                    {
                        rootObj.chot_result.chot_profit.profit_total_expense = data.OTDA_TOTAL_EXPENSE;
                        rootObj.chot_result.chot_profit.profit_inc_var = data.OTDA_TOTAL_INCOME;
                        rootObj.chot_result.chot_profit.profit_total = data.OTDA_TOTAL_NET_INCOME;
                    }
                }

                rootObj.chot_reason = data.OTDA_REASON;
                rootObj.chot_note = data.OTDA_NOTE;
                rootObj.approve_items = null;

                string strExplanationAttach = "";
                CHOT_ATTACH_FILE_DAL fileDal = new CHOT_ATTACH_FILE_DAL();
                List<CHOT_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "EXT");
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.OTAF_PATH + ":" + item.OTAF_INFO + "|";
                }
                rootObj.explanationAttach = strExplanationAttach;

                CHOT_A_OTHER_COSTS_DAL otherCostsDal = new CHOT_A_OTHER_COSTS_DAL();
                List<CHOT_A_OTHER_COSTS> otherCostslst = otherCostsDal.GetByDataID(id).OrderBy(o => o.OMAO_ORDER).ToList();
                foreach (var item in otherCostslst)
                {
                    ChotCharterOutCaseAOtherCost otherCost = new ChotCharterOutCaseAOtherCost();
                    otherCost.order = item.OMAO_ORDER;
                    otherCost.detail = item.OMAO_DETAIL;
                    otherCost.value = item.OMAO_VALUE;
                    rootObj.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs.Add(otherCost);
                }
            }
            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getTransactionCMCSByID(string id)
        {
            #region New Object 

            ChosRootObject rootObj = new ChosRootObject();
            rootObj.chos_evaluating = new ChosEvaluating();
            rootObj.chos_chartering_method = new ChosCharteringMethod();
            rootObj.chos_chartering_method.chos_type_of_fixings = new List<ChosTypeOfFixing>();

            rootObj.chos_no_charter_out_case_a = new Model.ChosNoCharterOutCaseA();
            rootObj.chos_no_charter_out_case_a.chos_est_expense = new List<Model.ChosEstExpense>();

            rootObj.chos_charter_out_case_b = new ChosCharterOutCaseB();
            rootObj.chos_charter_out_case_b.chos_charter_out_case_b_one = new List<ChosCharterOutCaseBOne>();
            rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two = new ChosCharterOutCaseBTwo();
            rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt = new ChosCaseCostOfMt();
            rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_spot_in_vessel = new List<ChosSpotInVessel>();
            rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt.chos_other_expense = new List<ChosOtherExpense>();

            rootObj.chos_proposed_for_approve = new ChosProposedForApprove();
            rootObj.chos_benefit = new ChosBenefit();

            #endregion
            CHOS_DATA_DAL dataDal = new CHOS_DATA_DAL();
            CHOS_DATA data = dataDal.GetCHOSDATAByID(id);
            if (data != null)
            {
                #region --- chos_evaluating ---

                rootObj.chos_evaluating.vessel_id = data.OSDA_FK_VESSEL;
                string strVeh = VehicleServiceModel.GetName(data.OSDA_FK_VESSEL);
                if (strVeh != null)
                    rootObj.chos_evaluating.vessel_name = strVeh;

                rootObj.chos_evaluating.voyage_no = data.OSDA_VOYAGE_NO;
                rootObj.chos_evaluating.date_purchase = (data.OSDA_DATE_PURCHASE == DateTime.MinValue || data.OSDA_DATE_PURCHASE == null) ? "" : Convert.ToDateTime(data.OSDA_DATE_PURCHASE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.chos_evaluating.broker_id = data.OSDA_FK_BROKER;
                string strBroker = VendorServiceModel.GetName(data.OSDA_FK_BROKER);
                if (strBroker != null)
                    rootObj.chos_evaluating.broker_name = strBroker;

                rootObj.chos_evaluating.vessel_size = data.OSDA_VESSEL_SIZE;
                rootObj.chos_evaluating.freight = data.OSDA_FREIGHT;
                rootObj.chos_evaluating.flat_rate = data.OSDA_FLAT_RATE;
                rootObj.chos_evaluating.year = data.OSDA_YEAR;
                rootObj.chos_evaluating.unit = data.OSDA_UNIT;
                rootObj.chos_evaluating.period_date_from = (data.OSDA_PERIOD_DATE_FROM == DateTime.MinValue || data.OSDA_PERIOD_DATE_FROM == null) ? "" : Convert.ToDateTime(data.OSDA_PERIOD_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chos_evaluating.period_date_to = (data.OSDA_PERIOD_DATE_TO == DateTime.MinValue || data.OSDA_PERIOD_DATE_TO == null) ? "" : Convert.ToDateTime(data.OSDA_PERIOD_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chos_evaluating.freight_detail = data.OSDA_FREIGHT_DETAIL;

                #endregion

                #region --- chos_chartering_method ---

                rootObj.chos_chartering_method.explanation = data.OSDA_EXPLANATION;
                if (data.OSDA_TYPE_OF_FIXINGS == "Marketing Order")
                {
                    rootObj.chos_chartering_method.chos_type_of_fixings.Add(new Model.ChosTypeOfFixing { type_value = "Marketing Order", type_flag = "Y" });
                    rootObj.chos_chartering_method.chos_type_of_fixings.Add(new Model.ChosTypeOfFixing { type_value = "Private Order", type_flag = "N" });
                }
                else
                {
                    rootObj.chos_chartering_method.chos_type_of_fixings.Add(new Model.ChosTypeOfFixing { type_value = "Marketing Order", type_flag = "N" });
                    rootObj.chos_chartering_method.chos_type_of_fixings.Add(new Model.ChosTypeOfFixing { type_value = "Private Order", type_flag = "Y" });
                }

                #endregion

                #region --- chos_no_charter_out_case_a ---

                CHOS_CASE_A_DAL dataCaseADAL = new CHOS_CASE_A_DAL();
                CHOS_CASE_A dataCaseA = dataCaseADAL.GetByDataID(id);
                if (dataCaseA != null)
                {
                    rootObj.chos_no_charter_out_case_a.total_expense_no_charter = dataCaseA.OSAO_TOTAL_EXPENSE_NC;

                    CHOS_A_EST_EXPENSE_DAL dataExpenseADAL = new CHOS_A_EST_EXPENSE_DAL();
                    List<CHOS_A_EST_EXPENSE> dataExpenseA = dataExpenseADAL.GetByDataID(dataCaseA.OSAO_ROW_ID).OrderBy(o => o.OSEE_ORDER).ToList();
                    if (dataExpenseA != null)
                    {
                        rootObj.chos_no_charter_out_case_a.chos_est_expense = new List<Model.ChosEstExpense>();
                        foreach (var expense in dataExpenseA)
                        {
                            Model.ChosEstExpense item = new Model.ChosEstExpense();
                            item.order = expense.OSEE_ORDER;
                            item.chos_period_date_from = (expense.OSEE_PERIOD_DATE_FROM == DateTime.MinValue || expense.OSEE_PERIOD_DATE_FROM == null) ? "" : Convert.ToDateTime(expense.OSEE_PERIOD_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            item.chos_period_date_to = (expense.OSEE_PERIOD_DATE_TO == DateTime.MinValue || expense.OSEE_PERIOD_DATE_TO == null) ? "" : Convert.ToDateTime(expense.OSEE_PERIOD_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            item.tc_hire_no = expense.OSEE_TC_HIRE_NO;
                            item.tc_hire_cost = expense.OSEE_TC_HIRE_COST;
                            item.port_charge = expense.OSEE_PORT_CHARGE;
                            item.bunker_cost = expense.OSEE_BUNKER_COST;
                            item.other_costs = expense.OSEE_OTHER_COSTS;
                            item.other_remarks = expense.OSEE_OTHER_REMARKS;
                            item.total_tc_cost = expense.OSEE_TOTAL_TC_COST;
                            item.remarks = expense.OSEE_REMARK;

                            rootObj.chos_no_charter_out_case_a.chos_est_expense.Add(item);
                        }
                    }
                }


                #endregion

                #region --- chos_charter_out_case_b ---

                CHOS_CASE_B_DAL dataCaseBDAL = new CHOS_CASE_B_DAL();
                CHOS_CASE_B dataCaseB = dataCaseBDAL.GetByDataID(id);
                if (dataCaseB != null)
                {
                    rootObj.chos_charter_out_case_b.total_income_b_one = dataCaseB.OSBO_TOTAL_INCOME_B_ONE;
                    rootObj.chos_charter_out_case_b.total_expense_b_two = dataCaseB.OSBO_TOTAL_EXPENSE_B_TWO;

                    #region chos_charter_out_case_b_one

                    CHOS_B_ONE_EST_INCOME_DAL dataEstIncomeDAL = new CHOS_B_ONE_EST_INCOME_DAL();
                    List<CHOS_B_ONE_EST_INCOME> dataEstIncome = dataEstIncomeDAL.GetByDataID(dataCaseB.OSBO_ROW_ID).OrderBy(o => o.OSEI_ORDER).ToList();
                    rootObj.chos_charter_out_case_b.chos_charter_out_case_b_one = new List<Model.ChosCharterOutCaseBOne>();
                    if (dataEstIncome != null)
                    {
                        foreach (var CharterOutCaseBOne in dataEstIncome)
                        {
                            Model.ChosCharterOutCaseBOne itemCharterOutCaseBOne = new Model.ChosCharterOutCaseBOne();
                            itemCharterOutCaseBOne.order = CharterOutCaseBOne.OSEI_ORDER;

                            itemCharterOutCaseBOne.chos_period_date_from = (CharterOutCaseBOne.OSEI_PERIOD_DATE_FROM == DateTime.MinValue || CharterOutCaseBOne.OSEI_PERIOD_DATE_FROM == null) ? "" : Convert.ToDateTime(CharterOutCaseBOne.OSEI_PERIOD_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            itemCharterOutCaseBOne.chos_period_date_to = (CharterOutCaseBOne.OSEI_PERIOD_DATE_TO == DateTime.MinValue || CharterOutCaseBOne.OSEI_PERIOD_DATE_TO == null) ? "" : Convert.ToDateTime(CharterOutCaseBOne.OSEI_PERIOD_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                            itemCharterOutCaseBOne.vessel_id = rootObj.chos_evaluating.vessel_id;
                            itemCharterOutCaseBOne.vessel_name = rootObj.chos_evaluating.vessel_name;
                            itemCharterOutCaseBOne.voyage_no = rootObj.chos_evaluating.voyage_no;

                            itemCharterOutCaseBOne.fixing_ws = CharterOutCaseBOne.OSEI_FIXING_WS;
                            itemCharterOutCaseBOne.est_flat_rate = CharterOutCaseBOne.OSEI_EST_FLAT_RATE;
                            itemCharterOutCaseBOne.min_loaded = CharterOutCaseBOne.OSEI_MIN_LOADED;
                            //itemCharterOutCaseBOne.deduct = CharterOutCaseBOne.OSEI_DEDUCT;
                            itemCharterOutCaseBOne.freight = CharterOutCaseBOne.OSEI_FREIGHT;
                            itemCharterOutCaseBOne.total_income = CharterOutCaseBOne.OSEI_TOTAL_INCOME;

                            CHOS_B_ONE_EST_INCOME_DEDUCT_DAL dataBOneIncomeDeductDAL = new CHOS_B_ONE_EST_INCOME_DEDUCT_DAL();
                            List<CHOS_B_ONE_EST_INCOME_DEDUCT> dataBOneIncomeDeduct = dataBOneIncomeDeductDAL.GetByDataID(CharterOutCaseBOne.OSEI_ROW_ID);
                            if (dataBOneIncomeDeduct != null)
                            {
                                itemCharterOutCaseBOne.chos_b_one_est_income_deduct = new List<ChosBOneEstIncomeDeduct>();
                                foreach (var BOneIncomeDeduct in dataBOneIncomeDeduct)
                                {
                                    itemCharterOutCaseBOne.chos_b_one_est_income_deduct.Add(new ChosBOneEstIncomeDeduct { order = BOneIncomeDeduct.OSEID_ORDER, deduct = BOneIncomeDeduct.OSEID_DEDUCT, name = BOneIncomeDeduct.OSEID_DEDUCT_NAME });
                                }
                            }

                            CHOS_B_ONE_OTHER_EXPENSE_DAL dataBOneOtherExpenseDAL = new CHOS_B_ONE_OTHER_EXPENSE_DAL();
                            List<CHOS_B_ONE_OTHER_EXPENSE> dataBOneOtherExpense = dataBOneOtherExpenseDAL.GetByDataID(CharterOutCaseBOne.OSEI_ROW_ID);
                            if (dataBOneOtherExpense != null)
                            {
                                itemCharterOutCaseBOne.other_expense = new List<OtherExpense>();
                                foreach (var BOneOtherExpense in dataBOneOtherExpense)
                                {
                                    itemCharterOutCaseBOne.other_expense.Add(new OtherExpense { order = BOneOtherExpense.OSNE_ORDER, other_key = BOneOtherExpense.OSNE_OTHER_EXPENSE, other_value = BOneOtherExpense.OSNE_AMOUNT });
                                }
                            }
                            rootObj.chos_charter_out_case_b.chos_charter_out_case_b_one.Add(itemCharterOutCaseBOne);
                        }
                    }

                    #endregion

                    #region chos_charter_out_case_b_two

                    CHOS_B_TWO_COST_OF_MT_DAL dataBTwoCostMTDAL = new CHOS_B_TWO_COST_OF_MT_DAL();
                    CHOS_B_TWO_COST_OF_MT dataBTwoCostMT = dataBTwoCostMTDAL.GetByDataID(dataCaseB.OSBO_ROW_ID);
                    rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two = new Model.ChosCharterOutCaseBTwo();
                    rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt = new Model.ChosCaseCostOfMt();
                    if (dataBTwoCostMT != null)
                    {
                        #region chos_case_cost_of_mt

                        Model.ChosCaseCostOfMt itemBTwoCostMT = new Model.ChosCaseCostOfMt();
                        itemBTwoCostMT.order = dataBTwoCostMT.OSCM_ORDER;

                        itemBTwoCostMT.chos_period_date_from = (dataBTwoCostMT.OSCM_PERIOD_DATE_FROM == DateTime.MinValue || dataBTwoCostMT.OSCM_PERIOD_DATE_FROM == null) ? "" : Convert.ToDateTime(dataBTwoCostMT.OSCM_PERIOD_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        itemBTwoCostMT.chos_period_date_to = (dataBTwoCostMT.OSCM_PERIOD_DATE_TO == DateTime.MinValue || dataBTwoCostMT.OSCM_PERIOD_DATE_TO == null) ? "" : Convert.ToDateTime(dataBTwoCostMT.OSCM_PERIOD_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        itemBTwoCostMT.vessel_id = rootObj.chos_evaluating.vessel_id;
                        itemBTwoCostMT.vessel_name = rootObj.chos_evaluating.vessel_name;
                        itemBTwoCostMT.voyage_no = rootObj.chos_evaluating.voyage_no;

                        itemBTwoCostMT.tc_hire_no = dataBTwoCostMT.OSCM_TC_HIRE_NO;
                        itemBTwoCostMT.tc_hire_cost_day = dataBTwoCostMT.OSCM_TC_HIRE_COST_DAY;
                        itemBTwoCostMT.tc_hire_cost_voy = dataBTwoCostMT.OSCM_TC_HIRE_COST_VOY;
                        itemBTwoCostMT.port_charge = dataBTwoCostMT.OSCM_PORT_CHARGE;
                        itemBTwoCostMT.bunker_cost = dataBTwoCostMT.OSCM_BUNKER_COST;
                        itemBTwoCostMT.other_costs = dataBTwoCostMT.OSCM_OTHER_COSTS;
                        itemBTwoCostMT.other_remarks = dataBTwoCostMT.OSCM_OTHER_REMARKS;
                        itemBTwoCostMT.total_tc_cost = dataBTwoCostMT.OSCM_TOTAL_TC_COST;

                        #region chos_spot_in_vessel

                        CHOS_B_TWO_SPOT_IN_VESSEL_DAL dataSportInVesselDAL = new CHOS_B_TWO_SPOT_IN_VESSEL_DAL();
                        List<CHOS_B_TWO_SPOT_IN_VESSEL> dataSportInVessel = dataSportInVesselDAL.GetByDataID(dataBTwoCostMT.OSCM_ROW_ID);
                        if (dataSportInVessel != null)
                        {
                            itemBTwoCostMT.chos_spot_in_vessel = new List<Model.ChosSpotInVessel>();
                            foreach (var SportInVessel in dataSportInVessel)
                            {
                                Model.ChosSpotInVessel itemSportInVessel = new Model.ChosSpotInVessel();
                                itemSportInVessel.order = SportInVessel.OSSV_ORDER;

                                itemSportInVessel.vessel_id = SportInVessel.OSSV_FK_VESSEL;
                                string strSportInVesselName = VehicleServiceModel.GetName(SportInVessel.OSSV_FK_VESSEL);
                                if (strSportInVesselName != null)
                                    itemSportInVessel.vessel_name = strSportInVesselName;

                                itemSportInVessel.loading_date_from = (SportInVessel.OSSV_LOADING_DATE_FROM == DateTime.MinValue || SportInVessel.OSSV_LOADING_DATE_FROM == null) ? "" : Convert.ToDateTime(SportInVessel.OSSV_LOADING_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                itemSportInVessel.ws = SportInVessel.OSSV_WS;
                                itemSportInVessel.flat_rate = SportInVessel.OSSV_FLAT_RATE;
                                itemSportInVessel.min_loaded = SportInVessel.OSSV_MIN_LOADED;
                                itemSportInVessel.freight = SportInVessel.OSSV_FREIGHT;
                                //itemSportInVessel.deduct = SportInVessel.OSSV_DEDUCT;
                                CHOS_B_TWO_SPOT_IN_DEDUCT_DAL dataBTwoSpotInDeductDAL = new CHOS_B_TWO_SPOT_IN_DEDUCT_DAL();
                                List<CHOS_B_TWO_SPOT_IN_DEDUCT> dataBTwoSpotInDeduct = dataBTwoSpotInDeductDAL.GetByDataID(SportInVessel.OSSV_ROW_ID);
                                if (dataBTwoSpotInDeduct != null)
                                {
                                    itemSportInVessel.chos_b_two_spot_in_deduct = new List<ChosBTwoSpotInDeduct>();
                                    foreach (var BTwoSpotInDeduct in dataBTwoSpotInDeduct)
                                    {
                                        itemSportInVessel.chos_b_two_spot_in_deduct.Add(new ChosBTwoSpotInDeduct { order = BTwoSpotInDeduct.OSSVD_ORDER, deduct = BTwoSpotInDeduct.OSSVD_DEDUCT, name = BTwoSpotInDeduct.OSSVD_DEDUCT_NAME });
                                    }
                                }
                                itemSportInVessel.ws_percent = SportInVessel.OSSV_WS_PERCENT;
                                itemSportInVessel.additional = SportInVessel.OSSV_ADDITIONAL;
                                itemSportInVessel.loading_date_to = (SportInVessel.OSSV_LOADING_DATE_TO == DateTime.MinValue || SportInVessel.OSSV_LOADING_DATE_TO == null) ? "" : Convert.ToDateTime(SportInVessel.OSSV_LOADING_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                itemSportInVessel.deduct_action = SportInVessel.OSSV_DEDUCT_ACTION;
                                itemSportInVessel.additional_name = SportInVessel.OSSV_ADDITIONAL_NAME;
                                itemSportInVessel.freight_before_deduct = SportInVessel.OSSV_TOTAL;
                                itemSportInVessel.total_deduct_value = SportInVessel.OSSV_TOTAL_DEDUCT;
                                itemBTwoCostMT.chos_spot_in_vessel.Add(itemSportInVessel);
                            }
                        }

                        #endregion

                        #region chos_other_expense

                        CHOS_B_TWO_OTHER_EXPENSE_DAL dataOtherExpenseDAL = new CHOS_B_TWO_OTHER_EXPENSE_DAL();
                        List<CHOS_B_TWO_OTHER_EXPENSE> dataOtherExpense = dataOtherExpenseDAL.GetByDataID(dataBTwoCostMT.OSCM_ROW_ID);
                        if (dataOtherExpense != null)
                        {
                            itemBTwoCostMT.chos_other_expense = new List<Model.ChosOtherExpense>();
                            foreach (var OtherExpense in dataOtherExpense)
                            {
                                Model.ChosOtherExpense itemOtherExpense = new Model.ChosOtherExpense();
                                itemOtherExpense.order = OtherExpense.OSNE_ORDER;
                                itemOtherExpense.other_expense = OtherExpense.OSNE_OTHER_EXPENSE;
                                itemOtherExpense.amount = OtherExpense.OSNE_AMOUNT;

                                itemBTwoCostMT.chos_other_expense.Add(itemOtherExpense);
                            }
                        }

                        #endregion

                        rootObj.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt = itemBTwoCostMT;


                        #endregion

                    }


                    #endregion

                }

                #endregion

                #region --- chos_proposed_for_approve ---

                rootObj.chos_proposed_for_approve.charterer_id = data.OSDA_P_FK_CHARTERER;
                string strChartererName = CustomerServiceModel.GetName(data.OSDA_P_FK_CHARTERER);
                if (strChartererName != null)
                    rootObj.chos_proposed_for_approve.charterer_name = strChartererName;

                rootObj.chos_proposed_for_approve.charter_party = data.OSDA_P_CHARTER_PARTY;

                rootObj.chos_proposed_for_approve.charterer_broker = data.OSDA_P_CHARTERER_BROKER;
                string strchartererBrokerName = VendorServiceModel.GetName(data.OSDA_P_CHARTERER_BROKER);
                if (strchartererBrokerName != null)
                    rootObj.chos_proposed_for_approve.charterer_broker_name = strchartererBrokerName;

                rootObj.chos_proposed_for_approve.owner_broker = data.OSDA_P_OWNER_BROKER;
                string strOwnerBrokerName = VendorServiceModel.GetName(data.OSDA_P_OWNER_BROKER);
                if (strOwnerBrokerName != null)
                    rootObj.chos_proposed_for_approve.owner_broker_name = strOwnerBrokerName;

                rootObj.chos_proposed_for_approve.route = data.OSDA_P_ROUTE;
                rootObj.chos_proposed_for_approve.laycan_from = (data.OSDA_P_LAYCAN_FROM == DateTime.MinValue || data.OSDA_P_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.OSDA_P_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chos_proposed_for_approve.laycan_to = (data.OSDA_P_LAYCAN_TO == DateTime.MinValue || data.OSDA_P_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.OSDA_P_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chos_proposed_for_approve.loading_ports = data.OSDA_P_LOADING;
                rootObj.chos_proposed_for_approve.discharging_ports = data.OSDA_P_DISCHARGING;

                rootObj.chos_proposed_for_approve.a_date_from = (data.OSDA_A_DATE_FROM == DateTime.MinValue || data.OSDA_A_DATE_FROM == null) ? "" : Convert.ToDateTime(data.OSDA_A_DATE_FROM).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chos_proposed_for_approve.a_date_to = (data.OSDA_A_DATE_TO == DateTime.MinValue || data.OSDA_A_DATE_TO == null) ? "" : Convert.ToDateTime(data.OSDA_A_DATE_TO).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.chos_proposed_for_approve.b_date_from = (data.OSDA_B_DATE_FROM == DateTime.MinValue || data.OSDA_B_DATE_FROM == null) ? "" : Convert.ToDateTime(data.OSDA_B_DATE_FROM).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.chos_proposed_for_approve.b_date_to = (data.OSDA_B_DATE_TO == DateTime.MinValue || data.OSDA_B_DATE_TO == null) ? "" : Convert.ToDateTime(data.OSDA_B_DATE_TO).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);

                #endregion

                #region chos_benefit

                rootObj.chos_benefit.chos_total_expense_no_charter = data.OSDA_TOTAL_EXPENS_NC;
                rootObj.chos_benefit.chos_total_expense = data.OSDA_TOTAL_EXPENSE;
                rootObj.chos_benefit.chos_net_benefit = data.OSDA_NET_BENEFIT;

                #endregion

                rootObj.chos_reason = data.OSDA_REASON;
                rootObj.chos_note = data.OSDA_REMARK;
                rootObj.approve_items = null;

                string strExplanationAttach = "";
                CHOS_ATTACH_FILE_DAL fileDal = new CHOS_ATTACH_FILE_DAL();
                List<CHOS_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "EXT");
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.OSAF_PATH + ":" + item.OSAF_INFO + "|";
                }
                rootObj.explanationAttach = strExplanationAttach;
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getAttFileCMCSByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            CHOS_ATTACH_FILE_DAL fileDal = new CHOS_ATTACH_FILE_DAL();
            List<CHOS_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].OSAF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        public static string getAttFileCMMTByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            CHOT_ATTACH_FILE_DAL fileDal = new CHOT_ATTACH_FILE_DAL();
            List<CHOT_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].OTAF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        public static string getFreightDataList(DateTime? date, string vessel, string charterer, DateTime? laycan_from, DateTime? laycan_to, string laytime, string cargo)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var chot = (from c in context.CHOT_DATA where c.OTDA_STATUS != "CANCEL" select c);

                    var sql = (from a in context.CPAI_FREIGHT_DATA
                               join b in context.CPAI_FREIGHT_CARGO on a.FDA_ROW_ID equals b.FDC_FK_FREIGHT_DATA
                               join c in context.MT_VEHICLE on a.FDA_FK_VESSEL equals c.VEH_ID
                               join d in context.MT_CUST_DETAIL on a.FDA_FK_CHARTERER equals d.MCD_ROW_ID

                               join e_value in chot on a.FDA_ROW_ID equals e_value.OTDA_FK_FREIGHT into e_join
                               from e in e_join.DefaultIfEmpty()

                               where a.FDA_STATUS == "SUBMIT" && new[] { "CANCEL", null }.Contains(e.OTDA_STATUS)//(e.OTDA_STATUS == "CANCEL" || e.OTDA_STATUS == null)

                               && (date.HasValue ? a.FDA_DATE_FILL_IN == date : true)
                               && (!string.IsNullOrEmpty(vessel) ? a.FDA_FK_VESSEL == vessel : true)
                               && (!string.IsNullOrEmpty(charterer) ? a.FDA_FK_CHARTERER == charterer : true)
                               && (laycan_from.HasValue ? a.FDA_LAYCAN_FROM == laycan_from : true)
                               && (laycan_to.HasValue ? a.FDA_LAYCAN_TO == laycan_to : true)
                               && (!string.IsNullOrEmpty(laytime) ? a.FDA_LAYTIME.Contains(laytime) : true)
                               && (!string.IsNullOrEmpty(cargo) ? b.FDC_FK_CARGO == cargo : true)
                               select new
                               {
                                   FREIGHT_ROW_ID = a.FDA_ROW_ID,
                                   FREIGHT_DATE = a.FDA_DATE_FILL_IN,
                                   FREIGHT_NO = a.FDA_DOC_NO,
                                   VESSEL = c.VEH_VEH_TEXT,
                                   LAYCAN_FROM = a.FDA_LAYCAN_FROM,
                                   LAYCAN_TO = a.FDA_LAYCAN_TO,
                                   CHARTERER = d.MCD_NAME_1,
                                   CREATED_BY = a.FDA_CREATED_BY,
                                   LAST_UPDATE = a.FDA_UPDATED,
                                   FCL_STATUS = a.FDA_STATUS,
                                   OTDA_STATUS = e.OTDA_STATUS
                               });
                    var sql_list = sql.ToList();

                    var newSql = (from a in context.CPAI_FREIGHT_DATA
                                  join b in context.CPAI_FREIGHT_CARGO on a.FDA_ROW_ID equals b.FDC_FK_FREIGHT_DATA
                                  join c in context.MT_VEHICLE on a.FDA_FK_VESSEL equals c.VEH_ID
                                  join e_value in chot on a.FDA_ROW_ID equals e_value.OTDA_FK_FREIGHT into e_join
                                  from e in e_join.DefaultIfEmpty()
                                  where a.FDA_STATUS == "SUBMIT" && new[] { "CANCEL", null }.Contains(e.OTDA_STATUS)
                                  && (date.HasValue ? a.FDA_DATE_FILL_IN == date : true)
                                  && (!string.IsNullOrEmpty(vessel) ? a.FDA_FK_VESSEL == vessel : true)
                                  && (!string.IsNullOrEmpty(charterer) ? a.FDA_FK_CHARTERER == charterer : true)
                                  && (laycan_from.HasValue ? a.FDA_LAYCAN_FROM == laycan_from : true)
                                  && (laycan_to.HasValue ? a.FDA_LAYCAN_TO == laycan_to : true)
                                  && (!string.IsNullOrEmpty(laytime) ? a.FDA_LAYTIME.Contains(laytime) : true)
                                  && (!string.IsNullOrEmpty(cargo) ? b.FDC_FK_CARGO == cargo : true)
                                  select new
                                  {
                                      FREIGHT_ROW_ID = a.FDA_ROW_ID,
                                      FREIGHT_DATE = a.FDA_DATE_FILL_IN,
                                      FREIGHT_NO = a.FDA_DOC_NO,
                                      VESSEL = c.VEH_VEH_TEXT,
                                      LAYCAN_FROM = a.FDA_LAYCAN_FROM,
                                      LAYCAN_TO = a.FDA_LAYCAN_TO,
                                      //CHARTERER = d.MCD_NAME_1,
                                      CREATED_BY = a.FDA_CREATED_BY,
                                      LAST_UPDATE = a.FDA_UPDATED,
                                      FCL_STATUS = a.FDA_STATUS,
                                      OTDA_STATUS = e.OTDA_STATUS
                                  });
                    var newSqlList = newSql.ToList();
                    var newResult = newSqlList.Distinct().ToList().OrderByDescending(o => o.LAST_UPDATE);
                    var result = sql_list.Distinct().ToList().OrderByDescending(o => o.LAST_UPDATE);
                    List<ChotFreightModal> arr_data = new List<ChotFreightModal>();
                    foreach (var v in result)
                    {
                        arr_data.Add(new ChotFreightModal
                        {
                            chot_fcl_row_id = v.FREIGHT_ROW_ID,
                            chot_fcl_date = ShareFn.ConvertDateTimeToDateStringFormat(v.FREIGHT_DATE, "dd/MM/yyyy"),
                            chot_fcl_no = v.FREIGHT_NO,
                            chot_fcl_vessel = v.VESSEL,
                            chot_fcl_laycan = ShareFn.ConvertDateTimeToDateStringFormat(v.LAYCAN_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(v.LAYCAN_TO, "dd/MM/yyyy"),
                            chot_fcl_charterer = v.CHARTERER,
                            chot_fcl_create_by = v.CREATED_BY,
                            chot_fcl_update_date = ShareFn.ConvertDateTimeToDateStringFormat(v.LAST_UPDATE, "dd/MM/yyyy HH:mm")
                        });
                    }

                    foreach (var item in newResult)
                    {
                        var chkResult = arr_data.SingleOrDefault(a => a.chot_fcl_row_id == item.FREIGHT_ROW_ID);
                        if (chkResult == null)
                        {
                            arr_data.Add(new ChotFreightModal
                            {
                                chot_fcl_row_id = item.FREIGHT_ROW_ID,
                                chot_fcl_date = ShareFn.ConvertDateTimeToDateStringFormat(item.FREIGHT_DATE, "dd/MM/yyyy"),
                                chot_fcl_no = item.FREIGHT_NO,
                                chot_fcl_vessel = item.VESSEL,
                                chot_fcl_laycan = ShareFn.ConvertDateTimeToDateStringFormat(item.LAYCAN_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(item.LAYCAN_TO, "dd/MM/yyyy"),
                                //chot_fcl_charterer = v.CHARTERER,
                                chot_fcl_create_by = item.CREATED_BY,
                                chot_fcl_update_date = ShareFn.ConvertDateTimeToDateStringFormat(item.LAST_UPDATE, "dd/MM/yyyy HH:mm")
                            });
                        }
                    }
                    // var newArr_data = arr_data.OrderByDescending(a => ShareFn.ConvertStringDateFormatToDatetime(a.chot_fcl_update_date, "dd/MM/yyyy HH:mm"));
                    System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
                    var newArr_data = arr_data.OrderByDescending(a => DateTime.ParseExact(a.chot_fcl_update_date,
                       "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture));
                    return new JavaScriptSerializer().Serialize(newArr_data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ChotObjectRoot getFreightData(string row_id)
        {
            try
            {
                ChotObjectRoot chot_obj = new ChotObjectRoot();
                using (var context = new EntityCPAIEngine())
                {
                    var sql_freight = context.CPAI_FREIGHT_DATA.Where(f => f.FDA_ROW_ID == row_id).FirstOrDefault();
                    MT_VENDOR sql_vender = new MT_VENDOR();
                    if (sql_freight.FDA_BROKER_NAME != null)
                    {
                        sql_vender = context.MT_VENDOR.Where(v => v.VND_ACC_NUM_VENDOR == sql_freight.FDA_BROKER_NAME).FirstOrDefault();
                    }
                    var sql_cargo = (from c in context.CPAI_FREIGHT_CARGO
                                     join m in context.MT_MATERIALS on c.FDC_FK_CARGO equals m.MET_NUM
                                     where c.FDC_FK_FREIGHT_DATA == row_id
                                     select new
                                     {
                                         cargo_id = c.FDC_ROW_ID,
                                         cargo_name = m.MET_MAT_DES_ENGLISH,
                                         cargo_qty = c.FDC_QUANTITY,
                                         cargo_unit = c.FDC_UNIT,
                                         cargo_unit_other = c.FDC_OTHERS,
                                         cargo_tolerance = c.FDC_TOLERANCE
                                     }).ToList();
                    var sql_load_port = context.CPAI_FREIGHT_PORT.Where(p => p.FDP_FK_FREIGHT_DATA == row_id && p.FDP_PORT_TYPE == "L").ToList();
                    var sql_dis_port = context.CPAI_FREIGHT_PORT.Where(p => p.FDP_FK_FREIGHT_DATA == row_id && p.FDP_PORT_TYPE == "D").ToList();

                    //new Object
                    chot_obj.chot_charter_out_case_b = new ChotCharterOutCaseB();
                    chot_obj.chot_charter_out_case_b.chot_est_income_b_one = new ChotEstIncomeBOne();
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two = new ChotEstExpenseBTwo();
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one = new ChotEstExpenseBTwoPointOne();
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two = new ChotEstExpenseBTwoPointTwo();
                    chot_obj.chot_summary = new ChotSummary();
                    chot_obj.chot_summary.chot_cargo = new List<ChotCargo>();
                    chot_obj.chot_summary.chot_loading_port = new List<ChotLoadingPort>();
                    chot_obj.chot_summary.chot_discharging_port = new List<ChotDischargingPort>();

                    chot_obj.freight_id = sql_freight.FDA_ROW_ID;
                    chot_obj.freight_no = sql_freight.FDA_DOC_NO;
                    chot_obj.vessel_id = sql_freight.FDA_FK_VESSEL;

                    //B1
                    chot_obj.chot_charter_out_case_b.chot_est_income_b_one.ex_rate = sql_freight.FDA_EXCHANGE_RATE;
                    chot_obj.chot_charter_out_case_b.chot_est_income_b_one.ex_date = sql_freight.FDA_EXCHANGE_DATE.HasValue ? ShareFn.ConvertDateTimeToDateStringFormat(sql_freight.FDA_EXCHANGE_DATE, "dd/MM/yyyy") : "";
                    if (sql_freight.FDA_LAYCAN_FROM.HasValue && sql_freight.FDA_LAYCAN_TO.HasValue)
                    {
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.laycan = string.Format("{0} to {1}", ShareFn.ConvertDateTimeToDateStringFormat(sql_freight.FDA_LAYCAN_FROM, "dd/MM/yyyy"), ShareFn.ConvertDateTimeToDateStringFormat(sql_freight.FDA_LAYCAN_TO, "dd/MM/yyyy"));
                    }

                    if (sql_freight.FDA_OFFER_UNIT == "LUMPSUM")
                    {
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.type = sql_freight.FDA_OFFER_UNIT;
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.offer = sql_freight.FDA_OFFER_BY;
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = sql_freight.FDA_OFFER_FREIGHT;
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_income = sql_freight.FDA_OFFER_FREIGHT;
                        if (sql_freight.FDA_OFFER_BY == "USD") { chot_obj.chot_charter_out_case_b.chot_est_income_b_one.freight = sql_freight.FDA_OFFER_FREIGHT; }
                        else
                        {
                            chot_obj.chot_charter_out_case_b.chot_est_income_b_one.freight = sql_freight.FDA_OFFER_FREIGHT_B;
                            if (sql_freight.FDA_OFFER_BY == "THB")
                            {
                                chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = sql_freight.FDA_OFFER_FREIGHT_B;
                            }
                            else
                            {
                                chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = sql_freight.FDA_OFFER_FREIGHT_B;
                            }
                        }
                    }
                    else if (sql_freight.FDA_OFFER_UNIT == "UNIT PRICE")
                    {
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.type = sql_freight.FDA_OFFER_UNIT;
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.freight = sql_freight.FDA_OFFER_FREIGHT_MT;
                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.offer = string.Format("{0}/{1}", sql_freight.FDA_OFFER_BY, sql_cargo[0].cargo_unit);

                        if (sql_freight.FDA_OFFER_BY == "THB")
                        {
                            //string convertToTHB = (double.Parse(sql_freight.FDA_OFFER_FREIGHT) * double.Parse(sql_freight.FDA_EXCHANGE_RATE)).ToString();
                            //ShareFn _FN = new ShareFn();
                            //var stringTHB = _FN.intTostrMoney2(convertToTHB,false);
                            //chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = convertToTHB;
                            chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = sql_freight.FDA_OFFER_FREIGHT_B;
                        }
                        else
                            chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_freight = sql_freight.FDA_OFFER_FREIGHT;

                        chot_obj.chot_charter_out_case_b.chot_est_income_b_one.total_income = sql_freight.FDA_OFFER_FREIGHT;
                    }


                    //B2.1
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.port_charge = sql_freight.FDA_TOTAL_PORT_CHARGE;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.bunker = sql_freight.FDA_TOT_BUNKER_COST;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_persent = sql_freight.FDA_TOTAL_COMM;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.commission_total = sql_freight.FDA_TOTAL_COMMISSION;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_persent = sql_freight.FDA_WITHOLD_TAX;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.withholding_tax_total = sql_freight.FDA_WITHHOLDING_TAX;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.other = sql_freight.FDA_COST_OTHERS;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_one.total_variable_costs = "รวม";

                    //B2.2
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.fix_costs = sql_freight.FDA_FIX_COST_PER_DAY;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.no = sql_freight.FDA_TOTAL_DURATION;
                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.chot_est_expense_b_two_point_two.total_fixed_costs = "";

                    chot_obj.chot_charter_out_case_b.chot_est_expense_b_two.total_expense = "";

                    //Summary
                    chot_obj.chot_summary.charterer = sql_freight.FDA_FK_CHARTERER;
                    chot_obj.chot_summary.freight = sql_freight.FDA_OFFER_FREIGHT;
                    chot_obj.chot_summary.laytime = sql_freight.FDA_LAYTIME;
                    chot_obj.chot_summary.demurrage_unit = sql_freight.FDA_UNIT;
                    chot_obj.chot_summary.demurrage = sql_freight.FDA_DEM;
                    chot_obj.chot_summary.payment_term = sql_freight.FDA_PAYMENT_TERM;
                    chot_obj.chot_summary.charterer_broker = sql_vender.VND_NAME1;
                    chot_obj.chot_summary.charterer_broker_comm = sql_freight.FDA_BROKER_COMM;
                    chot_obj.chot_summary.addr_comm = sql_freight.FDA_ADD_COMM;
                    chot_obj.chot_summary.total_comm = sql_freight.FDA_TOTAL_COMM;
                    chot_obj.chot_summary.withholding_tax = sql_freight.FDA_WITHOLD_TAX;

                    if (sql_freight.FDA_LAYCAN_FROM.HasValue && sql_freight.FDA_LAYCAN_TO.HasValue)
                    {
                        chot_obj.chot_summary.laycan = string.Format("{0} to {1}", ShareFn.ConvertDateTimeToDateStringFormat(sql_freight.FDA_LAYCAN_FROM, "dd/MM/yyyy"), ShareFn.ConvertDateTimeToDateStringFormat(sql_freight.FDA_LAYCAN_TO, "dd/MM/yyyy"));
                    }

                    chot_obj.chot_summary.bss = sql_freight.FDA_BSS;

                    foreach (var v in sql_cargo)
                    {
                        chot_obj.chot_summary.chot_cargo.Add(new ChotCargo
                        {
                            cargo = v.cargo_id,
                            cargo_name = v.cargo_name,
                            cargo_qty = v.cargo_qty,
                            cargo_unit = v.cargo_unit,
                            cargo_unit_other = v.cargo_unit_other,
                            cargo_tolerance = v.cargo_tolerance
                        });
                    }

                    foreach (var v in sql_load_port)
                    {
                        string temp_port = "";
                        MT_JETTY temp = PortDAL.GetPortData().Where(p => p.MTJ_ROW_ID == v.FDP_FK_PORT).FirstOrDefault();
                        if (temp.MTJ_JETTY_NAME == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(v.FDP_OTHER_DESC))
                            {
                                temp_port = v.FDP_OTHER_DESC + ", " + temp.MTJ_LOCATION + ", " + temp.MTJ_MT_COUNTRY;
                            }
                            else
                            {
                                temp_port = temp.MTJ_LOCATION + ", " + temp.MTJ_MT_COUNTRY;
                            }
                        }
                        else
                        {
                            temp_port = temp.MTJ_JETTY_NAME + ", " + temp.MTJ_LOCATION + ", " + temp.MTJ_MT_COUNTRY;
                        }

                        chot_obj.chot_summary.chot_loading_port.Add(new ChotLoadingPort
                        {
                            port_full_name = temp_port
                        });
                    }

                    foreach (var v in sql_dis_port)
                    {
                        string temp_port = "";
                        MT_JETTY temp = PortDAL.GetPortData().Where(p => p.MTJ_ROW_ID == v.FDP_FK_PORT).FirstOrDefault();
                        if (temp.MTJ_JETTY_NAME == "OTHER")
                        {
                            if (!string.IsNullOrEmpty(v.FDP_OTHER_DESC))
                            {
                                temp_port = v.FDP_OTHER_DESC + ", " + temp.MTJ_LOCATION + ", " + temp.MTJ_MT_COUNTRY;
                            }
                            else
                            {
                                temp_port = temp.MTJ_LOCATION + ", " + temp.MTJ_MT_COUNTRY;
                            }
                        }
                        else
                        {
                            temp_port = temp.MTJ_JETTY_NAME + ", " + temp.MTJ_LOCATION + ", " + temp.MTJ_MT_COUNTRY;
                        }
                        chot_obj.chot_summary.chot_discharging_port.Add(new ChotDischargingPort
                        {
                            port_full_name = temp_port
                        });
                    }
                }
                return chot_obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getCharterer(string row_id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var sql = (from a in context.MT_CUST_DETAIL
                               join b in context.MT_CUST_PAYMENT_TERM on a.MCD_FK_CUS equals b.MCP_FK_MT_CUST
                               join c in context.MT_VENDOR on b.MCP_BROKER_NAME equals c.VND_ACC_NUM_VENDOR
                               where a.MCD_ROW_ID == row_id
                               select new
                               {
                                   PARTY_FORM = "",
                                   PAYMENT_TERM = b.MCP_PAYMENT_TERM,
                                   BROKER_NAME = c.VND_NAME1,
                                   BROKER_COMM = c.VND_BROKER_COMMISSION,
                                   ADDR_COMM = b.MCP_ADDRESS_COMMISSION,
                                   WITH_TAX = b.MCP_WITHOLDING_TAX
                               }
                               ).FirstOrDefault();

                    return new JavaScriptSerializer().Serialize(sql);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getFreightCalculationByDocNo(string DocNo)
        {
            CPAI_FREIGHT_DATA obj_freight = new FREIGHT_DATA_DAL().GetFreightDataByDocNo(DocNo);
            SchFreight fght = new SchFreight();
            if (obj_freight != null)
            {
                List<FUNCTION_TRANSACTION> obj_trans = new FunctionTransactionDAL().findByTransactionId(obj_freight.FDA_ROW_ID);
                if (obj_trans != null)
                {
                    foreach (var item in obj_trans)
                    {
                        fght.row_id = item.FTX_TRANS_ID.Encrypt();
                        fght.req_trans_id = item.FTX_REQ_TRANS.Encrypt();
                    }
                }
            }
            return new JavaScriptSerializer().Serialize(fght);
        }

        public static bool getDupFreight(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var query = (from c in context.CHOT_DATA
                                 where c.OTDA_ROW_ID == id
                                 select c).FirstOrDefault();

                    if (query.OTDA_STATUS == "CANCEL")
                    {
                        var query2 = (from c in context.CHOT_DATA
                                      where c.OTDA_FK_FREIGHT == query.OTDA_FK_FREIGHT && c.OTDA_STATUS != "CANCEL"
                                      select c).ToList();
                        if (query2.Count == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue AddCharterNoteCMMT(CharterOutViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOT_DATA_DAL dataDAL = new CHOT_DATA_DAL();
                            CHOT_DATA dataCHOT = new CHOT_DATA();
                            dataCHOT.OTDA_ROW_ID = transId;
                            dataCHOT.OTDA_UPDATED = DateTime.Now;
                            dataCHOT.OTDA_UPDATED_BY = pUser;
                            dataCHOT.OTDA_NOTE = vm.chot_note;
                            dataCHOT.OTDA_EXPLANATION = vm.chot_reason_explanation;
                            dataDAL.UpdateNote(dataCHOT, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue AddCharterNoteCMCS(CharterOutViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOS_DATA_DAL dataDAL = new CHOS_DATA_DAL();
                            CHOS_DATA dataCHOS = new CHOS_DATA();
                            dataCHOS.OSDA_ROW_ID = transId;
                            dataCHOS.OSDA_UPDATED = DateTime.Now;
                            dataCHOS.OSDA_UPDATED_BY = pUser;
                            dataCHOS.OSDA_REMARK = vm.chos_note;
                            dataCHOS.OSDA_EXPLANATION = vm.chos_chartering_method.explanation;
                            dataDAL.UpdateNote(dataCHOS, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }
    }
}