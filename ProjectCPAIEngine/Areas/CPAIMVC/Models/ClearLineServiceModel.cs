﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class ClearLineServiceModel
    {
        CultureInfo provider = new CultureInfo("en-US");
        string format = "dd/MM/yyyy";
        //NumberStyles _NumStyle = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
        public void Edit(ref ClearLineViewModel pModel)
        {

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //For query from DB
                    if (!String.IsNullOrEmpty(pModel.ClearLineDetail.sTripNo))
                    {
                        string sTripNo = pModel.ClearLineDetail.sTripNo;

                        var qryClearLineItem = context.CLEAR_LINE_CRUDE.Where(c => c.CLC_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper())).ToList();
                        if (qryClearLineItem.Count > 0)
                        {
                            if (pModel.ClearLineDetail == null) { pModel.ClearLineDetail = new ClearLineDetailViewModel(); }
                            pModel.ClearLineDetail.sTripNo = qryClearLineItem[0].CLC_TRIP_NO;
                            pModel.ClearLineDetail.sCompany = qryClearLineItem[0].CLC_COMPANY;
                            pModel.ClearLineDetail.sVesselName = qryClearLineItem[0].CLC_VESSEL_ID;
                            pModel.ClearLineDetail.sCustomer = qryClearLineItem[0].CLC_CUST_NUM;
                            if (qryClearLineItem[0].CLC_DELIVERY_DATE != null)
                            {
                                pModel.ClearLineDetail.sDeliveryDate = (qryClearLineItem[0].CLC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                            }
                            pModel.ClearLineDetail.sCrude = qryClearLineItem[0].CLC_CRUDE_TYPE_ID;
                            pModel.ClearLineDetail.sPlant = qryClearLineItem[0].CLC_PLANT;
                            if (qryClearLineItem[0].CLC_DUE_DATE != null)
                            {
                                pModel.ClearLineDetail.sDueDate = (qryClearLineItem[0].CLC_DUE_DATE ?? DateTime.Now).ToString(format, provider);
                            }

                            if (qryClearLineItem[0].CLC_VOLUME_BBL != null)
                                pModel.ClearLineDetail.sVolumeBBL = Convert.ToDecimal(qryClearLineItem[0].CLC_VOLUME_BBL).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_VOLUME_LITE != null)
                                pModel.ClearLineDetail.sVolumeLitres = Convert.ToDecimal(qryClearLineItem[0].CLC_VOLUME_LITE).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_VOLUME_MT != null)
                                pModel.ClearLineDetail.sVolumeMT = Convert.ToDecimal(qryClearLineItem[0].CLC_VOLUME_MT).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_OUTTURN_BBL != null)
                                pModel.ClearLineDetail.sOutturnBBL = Convert.ToDecimal(qryClearLineItem[0].CLC_OUTTURN_BBL).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_OUTTURN_LITE != null)
                                pModel.ClearLineDetail.sOutturnLitres = Convert.ToDecimal(qryClearLineItem[0].CLC_OUTTURN_LITE).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_OUTTURN_MT != null)
                                pModel.ClearLineDetail.sOutturnMT = Convert.ToDecimal(qryClearLineItem[0].CLC_OUTTURN_MT).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_PO_NO != null)
                                pModel.ClearLineDetail.sPONo = qryClearLineItem[0].CLC_PO_NO;
                            if (qryClearLineItem[0].CLC_EXCHANGE_RATE != null)
                                pModel.ClearLineDetail.sExchangeRate = Convert.ToDecimal(qryClearLineItem[0].CLC_EXCHANGE_RATE).ToString("#,##0.0000");
                            if (qryClearLineItem[0].CLC_TOTAL != null)
                                pModel.ClearLineDetail.sTotalAmount = Convert.ToDecimal(qryClearLineItem[0].CLC_TOTAL).ToString("#,##0.0000");

                            pModel.ClearLineDetail.sTotalUnit = qryClearLineItem[0].CLC_UNIT_TOTAL ?? "";
                            pModel.ClearLineDetail.sRemark = qryClearLineItem[0].CLC_REMARK ?? "";
                            pModel.ClearLineDetail.sInvoiceFigureType = qryClearLineItem[0].CLC_INVOICE_FIGURE ?? "";
                            pModel.ClearLineDetail.sSaleUnitType = qryClearLineItem[0].CLC_SALE_UNIT ?? "";
                            pModel.ClearLineDetail.sSaleOrder = qryClearLineItem[0].CLC_SALEORDER ?? "";
                            if (pModel.ClearLineDetail.PriceList == null) { pModel.ClearLineDetail.PriceList = new List<PriceViewModel>(); }

                            var qryClearLinePrice = context.CLEAR_LINE_PRICE.Where(z => z.CLP_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper())).OrderBy(p => p.CLP_NUM).ToList();
                            int m = 1;
                            foreach (var q in qryClearLinePrice)
                            {
                                PriceViewModel temp = new PriceViewModel();
                                temp.sPriceType = "Provisional Price";
                                if (q.CLP_NUM > 0)
                                {
                                    if (q.CLP_NUM.ToString().Substring(q.CLP_NUM.ToString().Length - 1, 1) == "2")
                                        temp.sPriceType = "Final Price";
                                }
                                if (q.CLP_DATE_PRICE != null)
                                {
                                    temp.sDueDate = (q.CLP_DATE_PRICE ?? DateTime.Now).ToString(format, provider);
                                }
                                if (q.CLP_PRICE != null)
                                {
                                    temp.sPrice = (q.CLP_PRICE ?? 0).ToString("#,##0.00");
                                }
                                temp.sPriceUnit = q.CLP_UNIT_ID ?? "";
                                if (q.CLP_INVOICE != null)
                                {
                                    temp.sInvoice = (q.CLP_INVOICE ?? 0).ToString("#,##0.00");
                                }
                                temp.sInvUnit = q.CLP_UNIT_INV ?? "";
                                temp.sNum = q.CLP_NUM.ToString();
                                pModel.ClearLineDetail.PriceList.Add(temp);
                                m++;
                            }

                        }
                        ////For Test
                        //if (pModel.ClearLineDetail == null) { pModel.ClearLineDetail = new ClearLineDetailViewModel(); }
                        ////pModel.ClearLineDetail.sTripNo = "SW-2017-06-001";
                        //pModel.ClearLineDetail.sVesselName = "TATEYAMA";
                        //pModel.ClearLineDetail.sCustomer = "PPT PLC.";
                        //pModel.ClearLineDetail.sDeliveryDate = "17/05/2017";
                        //pModel.ClearLineDetail.sCrude = "MURBAN";
                        //pModel.ClearLineDetail.sPlant = "";
                        //pModel.ClearLineDetail.sDueDate = "22/05/2017";
                        //pModel.ClearLineDetail.sVolumeBBL = "26,022.0210";
                        //pModel.ClearLineDetail.sVolumeLitres = "10,000,000";
                        //pModel.ClearLineDetail.sVolumeMT = "32,000,000";
                        //pModel.ClearLineDetail.sOutturnBBL = "26,022.0210";
                        //pModel.ClearLineDetail.sOutturnLitres = "10,000,000";
                        //pModel.ClearLineDetail.sOutturnMT = "32,000,000";
                        //pModel.ClearLineDetail.sPONo = "664587";
                        //pModel.ClearLineDetail.sExchangeRate = "35.08";
                        //pModel.ClearLineDetail.sTotalAmount = "37,000,000.00";
                        //pModel.ClearLineDetail.sTotalUnit = "230,000.0000";
                        //pModel.ClearLineDetail.sRemark = "";
                        //if (pModel.ClearLineDetail.PriceList == null) { pModel.ClearLineDetail.PriceList = new List<PriceViewModel>(); }
                        //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
                        //{
                        //    sPriceType = "P",
                        //    sDueDate = "23/05/2017",
                        //    sPrice = "32,000.00",
                        //    sPriceUnit = "BBL",
                        //    sInvoice = "23,000.00",
                        //    sInvUnit = "BBL"
                        //});
                        //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
                        //{
                        //    sPriceType = "F",
                        //    sDueDate = "25/05/2017",
                        //    sPrice = "31,000.00",
                        //    sPriceUnit = "BBL",
                        //    sInvoice = "21,000.00",
                        //    sInvUnit = "BBL"
                        //});
                        //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
                        //{
                        //    sPriceType = "A",
                        //    sDueDate = "25/05/2017",
                        //    sPrice = "30,000.00",
                        //    sPriceUnit = "BBL",
                        //    sInvoice = "30,000.00",
                        //    sInvUnit = "BBL"
                        //});
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Add(ref ClearLineViewModel pModel)
        {
            if (pModel.ClearLineDetail == null) { pModel.ClearLineDetail = new ClearLineDetailViewModel(); }
            pModel.ClearLineDetail.sTripNo = "";
            pModel.ClearLineDetail.sVesselName = "";
            pModel.ClearLineDetail.sCustomer = "";
            pModel.ClearLineDetail.sDeliveryDate = "";
            pModel.ClearLineDetail.sCrude = "";
            pModel.ClearLineDetail.sPlant = "";
            pModel.ClearLineDetail.sDueDate = "";
            pModel.ClearLineDetail.sVolumeBBL = "";
            pModel.ClearLineDetail.sVolumeLitres = "";
            pModel.ClearLineDetail.sVolumeMT = "";
            pModel.ClearLineDetail.sOutturnBBL = "";
            pModel.ClearLineDetail.sOutturnLitres = "";
            pModel.ClearLineDetail.sOutturnMT = "";
            pModel.ClearLineDetail.sPONo = "";
            pModel.ClearLineDetail.sExchangeRate = "";
            pModel.ClearLineDetail.sTotalAmount = "";
            pModel.ClearLineDetail.sTotalUnit = "";
            pModel.ClearLineDetail.sRemark = "";
            //if (pModel.ClearLineDetail.PriceList == null) { pModel.ClearLineDetail.PriceList = new List<PriceViewModel>(); }
            //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
            //{
            //    sPriceType = "P",
            //    sDueDate = "",
            //    sPrice = "",
            //    sPriceUnit = "",
            //    sInvoice = "",
            //    sInvUnit = ""
            //});
            //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
            //{
            //    sPriceType = "F",
            //    sDueDate = "",
            //    sPrice = "",
            //    sPriceUnit = "",
            //    sInvoice = "",
            //    sInvUnit = ""
            //});
            //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
            //{
            //    sPriceType = "A",
            //    sDueDate = "",
            //    sPrice = "",
            //    sPriceUnit = "",
            //    sInvoice = "",
            //    sInvUnit = ""
            //});

        }

        public ReturnValue Search(ref ClearLineViewModel pModel)
        {
            //CultureInfo provider = new CultureInfo("en-US");
            //string format = "dd/MM/yyyy";

            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var sTripNo = pModel.ClearLine_Search.sTripNo == null ? "" : pModel.ClearLine_Search.sTripNo.ToUpper();
                    var sVesselID = pModel.ClearLine_Search.sVesselName == null ? "" : pModel.ClearLine_Search.sVesselName.ToUpper();
                    var sCrude = pModel.ClearLine_Search.sCrude == null ? "" : pModel.ClearLine_Search.sCrude.ToUpper();
                    var sCustomer = pModel.ClearLine_Search.sCustomer == null ? "" : pModel.ClearLine_Search.sCustomer.ToUpper();
                    var sDeliveryDate = pModel.ClearLine_Search.sDeliveryDate;
                    var sDateFrom = String.IsNullOrEmpty(pModel.ClearLine_Search.sDeliveryDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy", provider) : pModel.ClearLine_Search.sDeliveryDate.Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.ClearLine_Search.sDeliveryDate) ? DateTime.Now.ToString("dd/MM/yyyy", provider) : pModel.ClearLine_Search.sDeliveryDate.Substring(14, 10);
                    var sMode = pModel.Mode != null ? pModel.Mode : "C";

                    DateTime dDateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dDateTo = DateTime.ParseExact(sDateTo, format, provider);

                    var query = (from cl in context.CLEAR_LINE_CRUDE
                                 join mat in context.MT_MATERIALS on cl.CLC_CRUDE_TYPE_ID equals mat.MET_NUM
                                 join veh in context.MT_VEHICLE on cl.CLC_VESSEL_ID equals veh.VEH_ID
                                 join cus in context.MT_CUST_DETAIL on new { ColA = cl.CLC_CUST_NUM, ColB = cl.CLC_COMPANY } equals new { ColA = cus.MCD_FK_CUS, ColB = cus.MCD_FK_COMPANY }
                                 where ((cl.CLC_DELIVERY_DATE >= dDateFrom && cl.CLC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                 && ((cl.CLC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                 && ((cl.CLC_VESSEL_ID.ToUpper().Equals(sVesselID.ToUpper())) || (String.IsNullOrEmpty(sVesselID)))
                                 && ((cl.CLC_CRUDE_TYPE_ID.ToUpper().Equals(sCrude.ToUpper())) || (String.IsNullOrEmpty(sCrude)))
                                 && ((cl.CLC_CUST_NUM.ToUpper().Equals(sCustomer.ToUpper())) || (String.IsNullOrEmpty(sCustomer)))
                                 && ((cl.CLC_PROCESS_TYPE.ToUpper().Equals(sMode.ToUpper())) || (String.IsNullOrEmpty(sMode)))
                                 && (cus.MCD_NATION == "I")
                                 select new
                                 {
                                     cl,
                                     MatName = mat.MET_MAT_DES_ENGLISH,
                                     VesselName = veh.VEH_VEHICLE,
                                     CustomerName = cus.MCD_NAME_1
                                 });

                    if (query != null)
                    {
                        pModel.ClearLine_Search.SearchData = new List<ClearLineViewModel_SearchData>();

                        foreach (var item in query.ToList())
                        {
                            ClearLineViewModel_SearchData temp = new ClearLineViewModel_SearchData();
                            temp.dTripNo = item.cl.CLC_TRIP_NO;
                            temp.dCustomer = item.CustomerName ?? "  "; /*item.CustomerName;*/
                            temp.dCustomerCode = item.cl.CLC_CUST_NUM;

                            if (item.cl.CLC_DELIVERY_DATE != null)
                            {
                                temp.dDeliveryDate = (item.cl.CLC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                            }
                            else
                            {
                                temp.dDeliveryDate = "";
                            }
                            temp.dPlant = item.cl.CLC_PLANT;
                            temp.dProduct = item.MatName;
                            temp.dProductCode = item.cl.CLC_CRUDE_TYPE_ID;
                            temp.dVolumeBBL = (item.cl.CLC_VOLUME_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dVolumeMT = (item.cl.CLC_VOLUME_MT ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dVolumeLitres = (item.cl.CLC_OUTTURN_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dOuternBBL = (item.cl.CLC_OUTTURN_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dOuternLitres = (item.cl.CLC_OUTTURN_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dOuternMT = (item.cl.CLC_OUTTURN_MT ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dPrice = (item.cl.CLC_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dSaleUnitType = item.cl.CLC_SALE_UNIT;
                            temp.dInvoiceFigureType = item.cl.CLC_INVOICE_FIGURE;

                            temp.dPONo = item.cl.CLC_PO_NO ?? "";

                            var cp_query = (from cp in context.CLEAR_LINE_PRICE
                                            where (cp.CLP_TRIP_NO.ToUpper().Contains(item.cl.CLC_TRIP_NO))
                                            select cp).OrderByDescending(x => x.CLP_NUM); ;
                            if (cp_query != null && cp_query.ToList().Count > 0)
                            {

                                temp.dAmount = (cp_query.ToList()[0].CLP_INVOICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                                temp.dUS4 = cp_query.ToList()[0].CLP_UNIT_ID ?? "";
                            }
                            else
                            {
                                temp.dAmount = "";
                                temp.dUS4 = "";
                            }

                            temp.dExchangeRate = (item.cl.CLC_EXCHANGE_RATE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dSaleOrder = item.cl.CLC_SALEORDER ?? "";
                            temp.dMEMONo = item.cl.CLC_MEMO ?? "";
                            temp.dFIDoc = item.cl.CLC_FI_DOC ?? "";
                            temp.dDO = item.cl.CLC_DO_NO ?? "";
                            temp.dCompany = item.cl.CLC_COMPANY;
                            pModel.ClearLine_Search.SearchData.Add(temp);
                        }

                        rtn = null;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                catch (Exception ex)
                {
                    rtn.Status = false;
                    rtn.Message = ex.Message;
                }

            }

            return rtn;
        }

        public ReturnValue SaveData(ref ClearLineViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();

            ResponseData resData = new ResponseData();
            try
            {

                if (string.IsNullOrEmpty(pModel.ClearLineDetail.sTripNo))
                {
                    if (!string.IsNullOrEmpty(pModel.ClearLineDetail.sCompany) && (!string.IsNullOrEmpty(pModel.Mode)))
                    {
                        pModel.ClearLineDetail.sTripNo = genTripNo(pModel.ClearLineDetail.sCompany, pModel.Mode);
                    }
                    else
                    {
                        pModel.ClearLineDetail.sTripNo = "";
                    }

                }
                ClearLine.ClearLineDetailModel obj = new ClearLine.ClearLineDetailModel();

                obj.sTripNo = pModel.ClearLineDetail.sTripNo;
                obj.sCompany = pModel.ClearLineDetail.sCompany;
                obj.sVesselName = pModel.ClearLineDetail.sVesselName;
                obj.sCustomer = pModel.ClearLineDetail.sCustomer;
                obj.sCrude = pModel.ClearLineDetail.sCrude;
                obj.sPlant = pModel.ClearLineDetail.sPlant;
                obj.sDeliveryDate = pModel.ClearLineDetail.sDeliveryDate;
                obj.sDueDate = pModel.ClearLineDetail.sDueDate;
                obj.sVolumeBBL = pModel.ClearLineDetail.sVolumeBBL;
                obj.sVolumeLitres = pModel.ClearLineDetail.sVolumeLitres;
                obj.sVolumeMT = pModel.ClearLineDetail.sVolumeMT;
                obj.sOutturnBBL = pModel.ClearLineDetail.sOutturnBBL;
                obj.sOutturnLitres = pModel.ClearLineDetail.sOutturnLitres;
                obj.sOutturnMT = pModel.ClearLineDetail.sOutturnMT;
                obj.sPONo = pModel.ClearLineDetail.sPONo;
                obj.sExchangeRate = pModel.ClearLineDetail.sExchangeRate;
                obj.sTotalAmount = pModel.ClearLineDetail.sTotalAmount;
                obj.sTotalUnit = pModel.ClearLineDetail.sTotalUnit;
                obj.sRemark = pModel.ClearLineDetail.sRemark;
                obj.sPricePer = pModel.ClearLineDetail.sPricePer;
                obj.sPrice = pModel.ClearLineDetail.sPrice;
                obj.sUnitID = String.IsNullOrEmpty(pModel.ClearLineDetail.sUnitID) ? "US4" : pModel.ClearLineDetail.sUnitID;
                obj.sType = pModel.ClearLineDetail.sType;
                obj.sStatus = pModel.ClearLineDetail.sStatus;
                obj.sRelease = pModel.ClearLineDetail.sRelease;
                obj.sTank = pModel.ClearLineDetail.sTank;
                obj.sSaleOrder = pModel.ClearLineDetail.sSaleOrder;
                obj.sPriceRelease = pModel.ClearLineDetail.sPriceRelease;
                obj.sDistriChann = pModel.ClearLineDetail.sDistriChann;
                obj.sTableName = pModel.ClearLineDetail.sTableName;
                obj.sMemo = pModel.ClearLineDetail.sMemo;
                obj.sDateSAP = pModel.ClearLineDetail.sDateSAP;
                obj.sFIDoc = pModel.ClearLineDetail.sFIDoc;
                obj.sDONo = pModel.ClearLineDetail.sDONo;
                obj.sStatusToSAP = pModel.ClearLineDetail.sStatusToSAP;
                obj.sFIDocReverse = pModel.ClearLineDetail.sFIDocReverse;
                obj.sIncoTermType = pModel.ClearLineDetail.sIncoTermType;
                obj.sTemp = pModel.ClearLineDetail.sTemp;
                obj.sDensity = pModel.ClearLineDetail.sDensity;
                obj.sStorageLocation = pModel.ClearLineDetail.sStorageLocation;
                obj.sUpdateDO = pModel.ClearLineDetail.sUpdateDO;
                obj.sUpdateGI = pModel.ClearLineDetail.sUpdateGI;
                obj.sInvoiceFigureType = pModel.ClearLineDetail.sInvoiceFigureType;
                obj.sSaleUnitType = pModel.ClearLineDetail.sSaleUnitType;
                obj.sProcessType = pModel.Mode != null ? pModel.Mode : "C";

                obj.PriceList = new List<ClearLine.PriceModel>();
                foreach (var item in pModel.ClearLineDetail.PriceList)
                {
                    item.sTripNo = pModel.ClearLineDetail.sTripNo;
                    obj.PriceList.Add(new ClearLine.PriceModel
                    {
                        //sPriceType = item.sPriceType,
                        sDueDate = item.sDueDate,
                        sPrice = item.sPrice,
                        sPriceUnit = item.sPriceUnit,
                        sInvoice = item.sInvoice,
                        sInvUnit = item.sInvUnit,
                        sTripNo = item.sTripNo,
                        sNum = item.sNum,
                        sMemo = item.sMemo,
                        sRelease = item.sRelease
                    });
                }

                var json = new JavaScriptSerializer().Serialize(obj);

                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000058;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";
                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();

                req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req.req_parameters.p.Add(new p { k = "system", v = "ClearLine" });
                req.req_parameters.p.Add(new p { k = "trip_no", v = obj.sTripNo });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        private string genTripNo(string com_code, string mode = "")
        {

            string ret = mode != "C" ? "TH" : "CL";
            com_code = com_code.Substring(0, 2);
            string yy = DateTime.Now.ToString("yyMM", new System.Globalization.CultureInfo("en-US"));

            string TripPrefix = ret + com_code + yy;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.CLEAR_LINE_CRUDE
                                 where v.CLC_TRIP_NO.Contains(TripPrefix)
                                 orderby v.CLC_TRIP_NO descending
                                 select v.CLC_TRIP_NO).ToList();


                    if (query != null && query.Count() > 0)
                    {
                        string lastTrip = query[0];
                        if (lastTrip != "")
                            lastTrip = lastTrip.Replace(TripPrefix, "");

                        string nextTrip = (int.Parse(lastTrip) + 1).ToString().PadLeft(2, '0');

                        ret = TripPrefix + nextTrip;
                    }
                    else
                    {
                        ret = TripPrefix + "01";
                    }
                }

            }
            catch (Exception ex) { }

            return ret;
        }

        public void setInitail_ddl(ref ClearLineViewModel pModel)
        {
            if (pModel.ddl_Crude == null)
            {
                pModel.ddl_Crude = new List<SelectListItem>();
                pModel.ddl_Crude = getMaterial();
            }
            if (pModel.ddl_Customer == null)
            {
                pModel.ddl_Customer = new List<SelectListItem>();
                pModel.ddl_Customer = getCustomer();
            }
            if (pModel.ddl_Vessel == null)
            {
                pModel.ddl_Vessel = new List<SelectListItem>();
                pModel.ddl_Vessel = getVehicle("PRODUCT", true, "");
            }
            if (pModel.ddl_Plant == null)
            {
                pModel.ddl_Plant = new List<SelectListItem>();
                pModel.ddl_Plant = getPlant("1100,1400");
            }
            if (pModel.ddl_Unit == null)
            {
                pModel.ddl_Unit = new List<SelectListItem>();
                pModel.ddl_Unit = getUnit();
            }
            if (pModel.ddl_UnitPrice == null)
            {
                pModel.ddl_UnitPrice = new List<SelectListItem>();
                pModel.ddl_UnitPrice = getUnitPrice();
            }
            if (pModel.ddl_UnitTotal == null)
            {
                pModel.ddl_UnitTotal = new List<SelectListItem>();
                pModel.ddl_UnitTotal = getUnitTotal();
            }
            if (pModel.ddl_UnitINV == null)
            {
                pModel.ddl_UnitINV = new List<SelectListItem>();
                pModel.ddl_UnitINV = getUnitINV();
            }
            if (pModel.ddl_PriceStatus == null)
            {
                pModel.ddl_PriceStatus = new List<SelectListItem>();
                pModel.ddl_PriceStatus = getPriceStatus();
            }
        }

        // get Data Customer
        public static List<SelectListItem> getCustomer(string type = "PCF", bool isOptional = false, string message = "")
        {
            List<MT_CUST_DETAIL> mt_cust_detail = CustDetailDAL.GetCustDetail(PROJECT_NAME_SPACE, ACTIVE, type);
            return insertSelectListValue(mt_cust_detail.Select(x => x.MCD_NAME_1).ToList(), mt_cust_detail.Select(x => x.MCD_FK_CUS).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterialClearLine(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterialsClearLine().OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterialThroughput(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterialsThroughput().OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVehicle(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(PROJECT_NAME_SPACE, ACTIVE, type).OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), mt_vehicle.Select(x => x.VEH_ID).ToList(), isOptional, message);
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
                //else
                //{
                //    selectList.Add(new SelectListItem { Text = "", Value = "" });
                //}
            }
            //selectList.Add(new SelectListItem { Text = "SPOT", Value = "SPOT" });
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static string GetDataToJSON_TextValue(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Value, text = v.Text });

                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> getPlant(string pCompanyCode)
        {
            List<SelectListItem> lstPlant = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sListComCode = pCompanyCode.Replace(",", "\",\"");
                    sListComCode = "\"" + sListComCode + "\"";
                    var tmpPlant = context.MT_PLANT.Where(z => z.MPL_STATUS == "ACTIVE" && (sListComCode.Contains(z.MPL_COMPANY_CODE) || String.IsNullOrEmpty(pCompanyCode))).OrderBy(o => o.MPL_PLANT);

                    foreach (var item in tmpPlant.ToList())
                    {
                        lstPlant.Add(new SelectListItem { Value = item.MPL_PLANT, Text = item.MPL_PLANT });
                    }

                }

            }
            catch (Exception ex)
            {
                lstPlant.Add(new SelectListItem { Value = "", Text = "" });
            }

            return lstPlant;
        }

        public static List<SelectListItem> getUnit()
        {
            string JsonD = MasterData.GetJsonMasterSetting("PCF_CLEAR_LINE");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);
            GlobalConfigCL dataList = (GlobalConfigCL)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigCL));

            List<SelectListItem> dList = new List<SelectListItem>();
            foreach (var item in dataList.PCF_UNIT_CAL)
            {
                dList.Add(new SelectListItem { Value = item.Code, Text = item.Name });
            }

            return dList;
        }

        public static List<SelectListItem> getUnitPrice()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "US4", Text = "US4" });
            dList.Add(new SelectListItem { Value = "TH4", Text = "TH4" });

            return dList;
        }

        public static List<SelectListItem> getUnitTotal()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "THB", Text = "THB" });
            dList.Add(new SelectListItem { Value = "USD", Text = "USD" });

            return dList;
        }
        public static List<SelectListItem> getPriceStatus()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "1", Text = "Provisional Price" });
            dList.Add(new SelectListItem { Value = "2", Text = "Final Price" });

            return dList;
        }
        public static List<SelectListItem> getUnitINV()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "THB", Text = "THB" });

            return dList;
        }

        public static List<SelectListItem> getSaleUnitType()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "BBL", Text = "BBL" });
            dList.Add(new SelectListItem { Value = "MT", Text = "MT" });
            dList.Add(new SelectListItem { Value = "L30", Text = "L30" });

            return dList;
        }

        public static List<SelectListItem> getInvoiceFigureType()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "B/L", Text = "B/L" });
            dList.Add(new SelectListItem { Value = "Outturn", Text = "Outturn" });

            return dList;
        }

        public ReturnValue CreateSO(ref ClearLineViewModel pModel)
        {
            var selectedTripNo = "";
            ReturnValue rtn = new ReturnValue();
            try
            {                
                ResponseData resData = new ResponseData();

                if (pModel.SOClearLine_Search != null)
                    selectedTripNo = pModel.SOClearLine_Search.sTripNo_Select;

                string conJsonD = MasterData.GetJsonMasterSetting("PCF_CLEAR_LINE");
                JObject config = JObject.Parse(string.IsNullOrEmpty(conJsonD) ? "" : conJsonD);
                GlobalConfigCL dataList = (GlobalConfigCL)Newtonsoft.Json.JsonConvert.DeserializeObject(config.ToString(), typeof(GlobalConfigCL));


                #region "For Use"
                if (!string.IsNullOrEmpty(selectedTripNo))
                {
                    var tempSelect = pModel.ClearLine_Search.SearchData.Where(p => p.dTripNo == selectedTripNo).ToList();
                    if (tempSelect != null && tempSelect.Count > 0)
                    {

                        SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();

                        //Create Sale Price
                        obj.SalePrice = new List<SalesPriceCreateModel>();
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var qryClearLinePrice = context.CLEAR_LINE_PRICE.Where(z => z.CLP_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper())).OrderBy(p => p.CLP_NUM).ToList();
                            int m = 1;
                            foreach (var q in qryClearLinePrice)
                            {
                                SalesPriceCreateModel temp = new SalesPriceCreateModel();
                                if (q.CLP_DATE_PRICE != null)
                                {
                                    DateTime datePrice = q.CLP_DATE_PRICE ?? DateTime.Now;
                                    string[] strDate = datePrice.ToString(format, provider).Replace('/', '-').Split('-');
                                    temp.DATAB = strDate[2] + "-" + strDate[1] + "-" + strDate[0];// "2017-06-01";
                                    temp.DATBI = strDate[2] + "-" + strDate[1] + "-" + strDate[0];// "2017-06-01";
                                }

                                temp.KBETR = (q.CLP_PRICE ?? 0).ToString();
                                temp.KMEIN = tempSelect[0].dSaleUnitType;
                                temp.KONWA = q.CLP_UNIT_ID;
                                temp.KSCHL = "ZCR" + (q.CLP_NUM.ToString().Length >= 2 ? q.CLP_NUM.ToString().Substring(0, 1) : ""); // ดึงจาก table clear line price rowindex = sNum
                                temp.KUNNR = tempSelect[0].dCustomerCode;//"0000000013";//Customer Code
                                temp.MATNR = tempSelect[0].dProductCode; ////"YMB";//Material
                                temp.VKORG = tempSelect[0].dCompany; //"1100";//ดึงจาก company
                                temp.VTWEG = dataList.DISTRIBUTION_CHANEL;//"31";//Config DISTRIBUTION CHANEL
                                temp.WERKS = tempSelect[0].dPlant;
                                temp.ZZVSTEL = dataList.SHIPPING_POINT;//"12PI";//Config sHIPPING POINT
                                obj.SalePrice.Add(temp);
                                m++;
                            }
                        }

                        string[] date = tempSelect[0].dDeliveryDate.Replace('/', '-').Split('-');

                        //Create Sale Order
                        obj.SaleOrder = new SaleOrderModel();
                        obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();

                        obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                        {
                            DOC_TYPE = "Z1P3", //fix
                            SALES_ORG = tempSelect[0].dCompany,//ดึงจาก company
                            DISTR_CHAN = dataList.DISTRIBUTION_CHANEL,//"31",//Config DISTRIBUTION CHANEL
                            DIVISION = dataList.DIVISION,//"00",//Config DIVISION
                            REQ_DATE_H = date[2] + "-" + date[1] + "-" + date[0],
                            DATE_TYPE = dataList.DATE_TYPE,//"1",//Config DATE_TYPE
                            PURCH_NO_C = tempSelect[0].dPONo,
                            PURCH_NO_S = tempSelect[0].dPONo,
                            CURRENCY = "THB"
                        });

                        decimal conVal = 0; decimal.TryParse(tempSelect[0].dAmount, out conVal);

                        obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                        obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                        {
                            ITM_NUMBER = dataList.ITM_NUMBER,//"000010",//Config ITM_NUMBER
                            COND_VALUE = conVal,
                            CURRENCY = "TH5"
                        });

                        obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                        obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                        {
                            ITM_NUMBER = dataList.ITM_NUMBER,//"000010",//Config ITM_NUMBER
                            MATERIAL = tempSelect[0].dProductCode,
                            PLANT = tempSelect[0].dPlant,
                            SALES_UNIT = tempSelect[0].dSaleUnitType
                        });

                        obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                        obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                        {
                            PARTN_NUMB = tempSelect[0].dCustomerCode
                        });

                        decimal conVolume = 0;
                        if (tempSelect[0].dSaleUnitType == "BBL")
                        {
                            if (tempSelect[0].dInvoiceFigureType == "Outturn")
                            {
                                if (!string.IsNullOrEmpty(tempSelect[0].dOuternBBL))
                                {
                                    conVolume = Convert.ToDecimal(tempSelect[0].dOuternBBL);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tempSelect[0].dVolumeBBL))
                                {
                                    conVolume = Convert.ToDecimal(tempSelect[0].dVolumeBBL);
                                }
                            }
                        }
                        else if (tempSelect[0].dSaleUnitType == "L30")
                        {
                            if (tempSelect[0].dInvoiceFigureType == "Outturn")
                            {
                                if (!string.IsNullOrEmpty(tempSelect[0].dOuternLitres))
                                {
                                    conVolume = Convert.ToDecimal(tempSelect[0].dOuternLitres);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tempSelect[0].dVolumeLitres))
                                {
                                    conVolume = Convert.ToDecimal(tempSelect[0].dVolumeLitres);
                                }
                            }

                        }
                        else if (tempSelect[0].dSaleUnitType == "MT")
                        {
                            if (tempSelect[0].dInvoiceFigureType == "Outturn")
                            {
                                if (!string.IsNullOrEmpty(tempSelect[0].dOuternMT))
                                {
                                    conVolume = Convert.ToDecimal(tempSelect[0].dOuternMT);
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(tempSelect[0].dVolumeMT))
                                {
                                    conVolume = Convert.ToDecimal(tempSelect[0].dVolumeMT);
                                }
                            }
                        }

                        obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                        obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                        {
                            ITM_NUMBER = dataList.ITM_NUMBER,
                            REQ_DATE = date[2] + "-" + date[1] + "-" + date[0],//tempSelect[0].dDeliveryDate.Replace('/', '-'),
                            DATE_TYPE = "1",
                            REQ_QTY = conVolume //Volume by type
                        });

                        obj.SaleOrder.SALESDOCUMENT_IN = "";
                        obj.SaleOrder.Flag = "";

                        var json = new JavaScriptSerializer().Serialize(obj);

                        RequestData req1 = new RequestData();
                        req1.function_id = ConstantPrm.FUNCTION.F10000059;
                        req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                        req1.state_name = "";
                        req1.req_parameters = new req_parameters();
                        req1.req_parameters.p = new List<p>();

                        req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                        req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                        req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                        req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                        req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                        req1.req_parameters.p.Add(new p { k = "status", v = "CREATE" });
                        req1.extra_xml = "";

                        ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                        resData = service1.CallService(req1);

                        if (resData.result_code == "1")
                        {
                            tempSelect[0].dSaleOrder = resData.resp_parameters[0].v;
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        else
                        {
                            if(resData.extra_xml.Length > 1)
                            {
                                rtn.Message = resData.extra_xml[1].ToString();
                            }
                            
                            rtn.Status = false;
                        }
                    }
                }


                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sTripNo = pModel.ClearLine_Search.sTripNo == null ? "" : pModel.ClearLine_Search.sTripNo.ToUpper();
                    var sVesselID = pModel.ClearLine_Search.sVesselName == null ? "" : pModel.ClearLine_Search.sVesselName.ToUpper();
                    var sCrude = pModel.ClearLine_Search.sCrude == null ? "" : pModel.ClearLine_Search.sCrude.ToUpper();
                    var sCustomer = pModel.ClearLine_Search.sCustomer == null ? "" : pModel.ClearLine_Search.sCustomer.ToUpper();
                    var sDeliveryDate = pModel.ClearLine_Search.sDeliveryDate;
                    var sDateFrom = String.IsNullOrEmpty(pModel.ClearLine_Search.sDeliveryDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy", provider) : pModel.ClearLine_Search.sDeliveryDate.Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.ClearLine_Search.sDeliveryDate) ? DateTime.Now.ToString("dd/MM/yyyy", provider) : pModel.ClearLine_Search.sDeliveryDate.Substring(14, 10);
                    var sMode = pModel.Mode != null ? pModel.Mode : "C";

                    DateTime dDateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dDateTo = DateTime.ParseExact(sDateTo, format, provider);

                    var query = (from cl in context.CLEAR_LINE_CRUDE
                                 join mat in context.MT_MATERIALS on cl.CLC_CRUDE_TYPE_ID equals mat.MET_NUM
                                 join veh in context.MT_VEHICLE on cl.CLC_VESSEL_ID equals veh.VEH_ID
                                 join cus in context.MT_CUST_DETAIL on new { ColA = cl.CLC_CUST_NUM, ColB = cl.CLC_COMPANY } equals new { ColA = cus.MCD_FK_CUS, ColB = cus.MCD_FK_COMPANY }
                                 where ((cl.CLC_DELIVERY_DATE >= dDateFrom && cl.CLC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                 && ((cl.CLC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                 && ((cl.CLC_VESSEL_ID.ToUpper().Equals(sVesselID.ToUpper())) || (String.IsNullOrEmpty(sVesselID)))
                                 && ((cl.CLC_CRUDE_TYPE_ID.ToUpper().Equals(sCrude.ToUpper())) || (String.IsNullOrEmpty(sCrude)))
                                 && ((cl.CLC_CUST_NUM.ToUpper().Equals(sCustomer.ToUpper())) || (String.IsNullOrEmpty(sCustomer)))
                                 && ((cl.CLC_PROCESS_TYPE.ToUpper().Equals(sMode.ToUpper())) || (String.IsNullOrEmpty(sMode)))
                                 && (cus.MCD_NATION == "I")
                                 select new
                                 {
                                     cl,
                                     MatName = mat.MET_MAT_DES_ENGLISH,
                                     VesselName = veh.VEH_VEHICLE,
                                     CustomerName = cus.MCD_NAME_1
                                 });

                    if (query != null)
                    {
                        pModel.ClearLine_Search.SearchData = new List<ClearLineViewModel_SearchData>();

                        foreach (var item in query.ToList())
                        {
                            ClearLineViewModel_SearchData temp = new ClearLineViewModel_SearchData();
                            temp.dTripNo = item.cl.CLC_TRIP_NO;
                            temp.dCustomer = item.CustomerName ?? "  "; /*item.CustomerName;*/
                            temp.dCustomerCode = item.cl.CLC_CUST_NUM;

                            if (item.cl.CLC_DELIVERY_DATE != null)
                            {
                                temp.dDeliveryDate = (item.cl.CLC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                            }
                            else
                            {
                                temp.dDeliveryDate = "";
                            }
                            temp.dPlant = item.cl.CLC_PLANT;
                            temp.dProduct = item.MatName;
                            temp.dProductCode = item.cl.CLC_CRUDE_TYPE_ID;
                            temp.dVolumeBBL = (item.cl.CLC_VOLUME_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dVolumeMT = (item.cl.CLC_VOLUME_MT ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dVolumeLitres = (item.cl.CLC_OUTTURN_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dOuternBBL = (item.cl.CLC_OUTTURN_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dOuternLitres = (item.cl.CLC_OUTTURN_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dOuternMT = (item.cl.CLC_OUTTURN_MT ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dPrice = (item.cl.CLC_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dSaleUnitType = item.cl.CLC_SALE_UNIT;
                            temp.dInvoiceFigureType = item.cl.CLC_INVOICE_FIGURE;

                            temp.dPONo = item.cl.CLC_PO_NO ?? "";

                            var cp_query = (from cp in context.CLEAR_LINE_PRICE
                                            where (cp.CLP_TRIP_NO.ToUpper().Contains(item.cl.CLC_TRIP_NO))
                                            select cp).OrderByDescending(x => x.CLP_NUM); ;
                            if (cp_query != null && cp_query.ToList().Count > 0)
                            {

                                temp.dAmount = (cp_query.ToList()[0].CLP_INVOICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                                temp.dUS4 = cp_query.ToList()[0].CLP_UNIT_ID ?? "";
                            }
                            else
                            {
                                temp.dAmount = "";
                                temp.dUS4 = "";
                            }

                            temp.dExchangeRate = (item.cl.CLC_EXCHANGE_RATE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dSaleOrder = item.cl.CLC_SALEORDER ?? "";
                            temp.dMEMONo = item.cl.CLC_MEMO ?? "";
                            temp.dFIDoc = item.cl.CLC_FI_DOC ?? "";
                            temp.dDO = item.cl.CLC_DO_NO ?? "";
                            temp.dCompany = item.cl.CLC_COMPANY;
                            pModel.ClearLine_Search.SearchData.Add(temp);
                        }
                    }
                }

                            #endregion

                            #region "For Test"
                            ////For Test
                            //SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();
                            //obj1.SalePrice = new List<SalesPriceCreateModel>();
                            //SalesPriceCreateModel sale = new SalesPriceCreateModel();
                            //sale.DATAB = "2017-06-27";
                            //sale.DATBI = "2017-06-27";
                            //sale.KBETR = "1609.140";
                            //sale.KMEIN = "BBL";
                            //sale.KONWA = "TH4";
                            //sale.KSCHL = "ZCR1";
                            //sale.KUNNR = "0000000023";
                            //sale.MATNR = "YMURBAN";
                            //sale.VKORG = "1100";
                            //sale.VTWEG = "31";
                            //sale.WERKS = "1200";
                            //sale.ZZVSTEL = "12PI";
                            //obj1.SalePrice.Add(sale);

                            //obj1.SaleOrder = new SaleOrderModel();
                            //obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                            //obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                            //{
                            //    DOC_TYPE = "Z1P3",
                            //    SALES_ORG = "1100",
                            //    DISTR_CHAN = "31",
                            //    DIVISION = "00",
                            //    REQ_DATE_H = "2017-06-27",
                            //    DATE_TYPE = "1",
                            //    PURCH_NO_C = "CIP_CLEAR_LINE_01",
                            //    PURCH_NO_S = "CIP_CLEAR_LINE_01",
                            //    CURRENCY = "THB"
                            //});

                            //obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                            //obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                            //{
                            //    ITM_NUMBER = "000010",
                            //    COND_VALUE = 33.3M,
                            //    CURRENCY = "TH5"
                            //});

                            //obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                            //obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                            //{
                            //    ITM_NUMBER = "000010",
                            //    MATERIAL = "YMURBAN",
                            //    PLANT = "1200",
                            //    //ITEM_CATEG = "ZTW1",
                            //    SALES_UNIT = "BBL"
                            //});

                            //obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                            //obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                            //{
                            //    PARTN_NUMB = "0000000023"
                            //});

                            //obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                            //obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                            //{
                            //    ITM_NUMBER = "000010",
                            //    REQ_DATE = "2017-06-27",
                            //    DATE_TYPE = "1",
                            //    REQ_QTY = 1609.140M
                            //});

                            //obj1.SaleOrder.SALESDOCUMENT_IN = "";
                            //obj1.SaleOrder.Flag = "";

                            //var json1 = new JavaScriptSerializer().Serialize(obj1);

                            //RequestData req2 = new RequestData();
                            //req2.function_id = ConstantPrm.FUNCTION.F10000059;
                            //req2.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                            //req2.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                            //req2.req_transaction_id = ConstantPrm.EnginGetEngineID();
                            //req2.state_name = "";
                            //req2.req_parameters = new req_parameters();
                            //req2.req_parameters.p = new List<p>();

                            //req2.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                            //req2.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                            //req2.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                            //req2.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                            //req2.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                            //req2.req_parameters.p.Add(new p { k = "status", v = "CREATE" });
                            //req2.extra_xml = "";

                            //ServiceProvider.ProjService service2 = new ServiceProvider.ProjService();

                            //resData = service2.CallService(req2);

                            //if (resData.result_code == "1")
                            //{
                            //    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            //    rtn.Status = true;
                            //}
                            //else
                            //{
                            //    rtn.Message = resData.result_desc;
                            //    rtn.Status = false;
                            //}
                            #endregion


                        }
            catch (Exception ex)
            {

            }
            return rtn;
        }

        public void UpdateSO(ref ClearLineViewModel pModel)
        {

            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();
                string conJsonD = MasterData.GetJsonMasterSetting("PCF_CLEAR_LINE");
                JObject config = JObject.Parse(string.IsNullOrEmpty(conJsonD) ? "" : conJsonD);
                GlobalConfigCL dataList = (GlobalConfigCL)Newtonsoft.Json.JsonConvert.DeserializeObject(config.ToString(), typeof(GlobalConfigCL));

                var selectedTripNo = pModel.SOClearLine_Search.sTripNo_Select;
                var tempSelect = pModel.ClearLine_Search.SearchData.Where(p => p.dTripNo == selectedTripNo).ToList();
                #region "For Use"
                SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                obj1.SaleOrder = new SaleOrderModel();
                obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                string[] date = tempSelect[0].dDeliveryDate.Replace('/', '-').Split('-');
                obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                {
                    DOC_TYPE = "Z1P3",
                    REQ_DATE_H = date[2] + "-" + date[1] + "-" + date[0]

                });

                decimal conVal = 0; decimal.TryParse(tempSelect[0].dAmount, out conVal);

                obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                {
                    ITM_NUMBER = dataList.ITM_NUMBER,//"000010",//Config ITM_NUMBER
                    COND_VALUE = conVal,
                    CURRENCY = "TH5"
                });

                obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                {
                    ITM_NUMBER = dataList.ITM_NUMBER,//"000010",
                    MATERIAL = tempSelect[0].dProductCode,//"YMURBAN",
                    PLANT = tempSelect[0].dPlant,//"1200",
                    //ITEM_CATEG = "ZTW1",
                    SALES_UNIT = tempSelect[0].dSaleUnitType//"BBL"
                });

                obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                {
                    PARTN_NUMB = tempSelect[0].dCustomerCode//"0000000023"
                });

                decimal conVolume = 0;
                if (tempSelect[0].dSaleUnitType == "BBL")
                {
                    if (!string.IsNullOrEmpty(tempSelect[0].dVolumeBBL))
                    {
                        conVolume = Convert.ToDecimal(tempSelect[0].dVolumeBBL);
                    }
                }
                else if (tempSelect[0].dSaleUnitType == "L30")
                {
                    if (!string.IsNullOrEmpty(tempSelect[0].dVolumeLitres))
                    {
                        conVolume = Convert.ToDecimal(tempSelect[0].dVolumeLitres);
                    }
                }
                else if (tempSelect[0].dSaleUnitType == "MT")
                {
                    if (!string.IsNullOrEmpty(tempSelect[0].dVolumeMT))
                    {
                        conVolume = Convert.ToDecimal(tempSelect[0].dVolumeMT);
                    }
                }
                obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                {
                    ITM_NUMBER = dataList.ITM_NUMBER,//"000010",
                    REQ_DATE = date[2] + "-" + date[1] + "-" + date[0],//"2017-06-27",
                    DATE_TYPE = "1",
                    REQ_QTY = conVolume //Volume by type
                });

                obj1.SaleOrder.SALESDOCUMENT_IN = tempSelect[0].dFIDoc;
                obj1.SaleOrder.Flag = "";
                #endregion

                #region "For Test"
                //For Test
                //SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                //obj1.SaleOrder = new SaleOrderModel();
                //obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //{
                //    DOC_TYPE = "Z1P3",
                //    REQ_DATE_H = "2017-06-27"

                //});

                //obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //{
                //    ITM_NUMBER = "000010",
                //    COND_VALUE = 33.3M,
                //    CURRENCY = "TH5"
                //});

                //obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //{
                //    ITM_NUMBER = "000010",
                //    MATERIAL = "YMURBAN",
                //    PLANT = "1200",
                //    //ITEM_CATEG = "ZTW1",
                //    SALES_UNIT = "BBL"
                //});

                //obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                //obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                //{
                //    PARTN_NUMB = "0000000023"
                //});

                //obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //{
                //    ITM_NUMBER = "000010",
                //    REQ_DATE = "2017-06-27",
                //    DATE_TYPE = "1",
                //    REQ_QTY = 1609.140M
                //});

                //obj1.SaleOrder.SALESDOCUMENT_IN = "1130002543";
                //obj1.SaleOrder.Flag = "";
                #endregion
                var json1 = new JavaScriptSerializer().Serialize(obj1);

                RequestData req1 = new RequestData();
                req1.function_id = ConstantPrm.FUNCTION.F10000059;
                req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req1.state_name = "";
                req1.req_parameters = new req_parameters();
                req1.req_parameters.p = new List<p>();

                req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                req1.req_parameters.p.Add(new p { k = "status", v = "CHANGE" });
                req1.extra_xml = "";

                ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                resData = service1.CallService(req1);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }


            }
            catch (Exception ex)
            {

            }
        }

        public void CancelSO(ref ClearLineViewModel pModel)
        {


            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();
                string conJsonD = MasterData.GetJsonMasterSetting("PCF_CLEAR_LINE");
                JObject config = JObject.Parse(string.IsNullOrEmpty(conJsonD) ? "" : conJsonD);
                GlobalConfigCL dataList = (GlobalConfigCL)Newtonsoft.Json.JsonConvert.DeserializeObject(config.ToString(), typeof(GlobalConfigCL));

                var selectedTripNo = pModel.SOClearLine_Search.sTripNo_Select;
                var tempSelect = pModel.ClearLine_Search.SearchData.Where(p => p.dTripNo == selectedTripNo).ToList();

                #region "For Use"
                SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();
                string[] date = tempSelect[0].dDeliveryDate.Replace('/', '-').Split('-');
                obj1.SaleOrder = new SaleOrderModel();
                obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                {
                    DOC_TYPE = "Z1P3",
                    REQ_DATE_H = date[2] + "-" + date[1] + "-" + date[0]
                });

                obj1.SaleOrder.SALESDOCUMENT_IN = tempSelect[0].dFIDoc;//"1130002544";
                obj1.SaleOrder.Flag = "X";

                decimal conVal = 0; decimal.TryParse(tempSelect[0].dAmount, out conVal);
                obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                {
                    ITM_NUMBER = dataList.ITM_NUMBER,//"000010",
                    COND_VALUE = conVal,
                    CURRENCY = "TH5"
                });

                obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                {
                    ITM_NUMBER = dataList.ITM_NUMBER,//"000010",
                    MATERIAL = tempSelect[0].dProductCode,//"YMURBAN",
                    PLANT = tempSelect[0].dPlant,//"1200",
                    SALES_UNIT = tempSelect[0].dSaleUnitType//"BBL"
                                                            //ITEM_CATEG = "ZTW1",
                                                            //PURCH_DATE = "2017-06-27",

                });

                decimal conVolume = 0;
                if (tempSelect[0].dSaleUnitType == "BBL")
                {
                    if (!string.IsNullOrEmpty(tempSelect[0].dVolumeBBL))
                    {
                        conVolume = Convert.ToDecimal(tempSelect[0].dVolumeBBL);
                    }
                }
                else if (tempSelect[0].dSaleUnitType == "L30")
                {
                    if (!string.IsNullOrEmpty(tempSelect[0].dVolumeLitres))
                    {
                        conVolume = Convert.ToDecimal(tempSelect[0].dVolumeLitres);
                    }
                }
                else if (tempSelect[0].dSaleUnitType == "MT")
                {
                    if (!string.IsNullOrEmpty(tempSelect[0].dVolumeMT))
                    {
                        conVolume = Convert.ToDecimal(tempSelect[0].dVolumeMT);
                    }
                }
                obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                {
                    ITM_NUMBER = dataList.ITM_NUMBER,//"000010",
                    SCHED_LINE = "0001",
                    REQ_DATE = date[2] + "-" + date[1] + "-" + date[0],//"2017-06-30",
                    REQ_QTY = conVolume//1609.140M
                });
                #endregion

                #region "For Test"
                //SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                //obj1.SaleOrder = new SaleOrderModel();
                //obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //{
                //    DOC_TYPE = "Z1P3",
                //    REQ_DATE_H = "2017-06-30"
                //});

                //obj1.SaleOrder.SALESDOCUMENT_IN = "1130002544";
                //obj1.SaleOrder.Flag = "X";

                //obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //{
                //    ITM_NUMBER = "000010",
                //    COND_VALUE = 33.3M,
                //    CURRENCY = "TH5"
                //});

                //obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //{
                //    ITM_NUMBER = "000010",
                //    MATERIAL = "YMURBAN",
                //    PLANT = "1200",
                //    //ITEM_CATEG = "ZTW1",
                //    //PURCH_DATE = "2017-06-27",
                //    SALES_UNIT = "BBL"
                //});

                //obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //{
                //    ITM_NUMBER = "000010",
                //    SCHED_LINE = "0001",
                //    REQ_DATE = "2017-06-30",
                //    REQ_QTY = 1609.140M
                //});
                #endregion
                var json1 = new JavaScriptSerializer().Serialize(obj1);

                RequestData req1 = new RequestData();
                req1.function_id = ConstantPrm.FUNCTION.F10000059;
                req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req1.state_name = "";
                req1.req_parameters = new req_parameters();
                req1.req_parameters.p = new List<p>();

                req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo.ToUpper() });
                req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                req1.req_parameters.p.Add(new p { k = "status", v = "CANCEL" });
                req1.extra_xml = "";

                ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                resData = service1.CallService(req1);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {

            }
        }

    }
}