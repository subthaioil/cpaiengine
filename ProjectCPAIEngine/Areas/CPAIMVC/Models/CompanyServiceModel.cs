﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CompanyServiceModel
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ReturnValue Add(CompanyViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                MT_COMPANY_DAL dal = new MT_COMPANY_DAL();
                MT_COMPANY ent = new MT_COMPANY();
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyCode))
                {                   
                    rtn.Message = "Company code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyName))
                {
                    rtn.Message = "Company name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                var hasId = dal.ChkRowId(pModel.CompanyCode);
                if (hasId)
                {
                    rtn.Message = "Company code is already used.";
                    rtn.Status = false;
                    return rtn;
                }


                HEDG_COMPANY_AUTH_TRADE_DAL autDal = new HEDG_COMPANY_AUTH_TRADE_DAL();
                MT_COMPANY_AUTH_TRADE aut = new MT_COMPANY_AUTH_TRADE();

                DateTime now = DateTime.Now;
                try
                {
                    ent.MCO_COMPANY_CODE = pModel.CompanyCode;
                    ent.MCO_SHORT_NAME = pModel.CompanyName.Trim();
                    ent.MCO_STATUS = pModel.Status;
                    ent.MCO_CREATE_TYPE = pModel.CreateType;
                    ent.MCO_FULL_NAME = pModel.fullName;
                    ent.MCO_DESCRIPTION = pModel.description;
                    ent.MCO_ACC_NUM_PAY = pModel.accountPay;
                    ent.MCO_ACC_NUM_RECEIVE = pModel.accountReceive;
                    ent.MCO_CREATED_BY = pUser;
                    ent.MCO_CREATED_DATE = now;
                    ent.MCO_UPDATED_BY = pUser;
                    ent.MCO_UPDATED_DATE = now;                
                    dal.Save(ent);

                    if (pModel.Company_auth != null && pModel.Company_auth.Any())
                    {
                        foreach (var item in pModel.Company_auth)
                        {                            
                            aut = new MT_COMPANY_AUTH_TRADE();
                            aut.MCAT_ROW_ID = ShareFn.GenerateCodeByDate("CPAI");
                            aut.MCAT_FK_MT_COMPANY = pModel.CompanyCode;
                            aut.MCAT_ORDER = item.Order;
                            aut.MCAT_TRADING_BOOK = item.Trading_book;
                            aut.MCAT_TRADE_FOR_FLAG = item.Trading_for_flag == true ? "Y" : "N";
                            aut.MCAT_FEE_BBL = item.Fee_bbl;
                            aut.MCAT_FEE_MT = item.Fee_mt;
                            aut.MCAT_CREATED_DATE = now;
                            aut.MCAT_CREATED_BY = pUser;
                            aut.MCAT_UPDATED_DATE = now;
                            aut.MCAT_UPDATED_BY = pUser;
                            autDal.Save(aut);
                        }                     
                    }

                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(CompanyViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel.BoolStatus != null)
                {
                    pModel.Status = pModel.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                }
                else
                {
                    pModel.Status = "INACTIVE";
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyCode))
                {
                    rtn.Message = "Company code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyName))
                {
                    rtn.Message = "Company name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_COMPANY_DAL dal = new MT_COMPANY_DAL();
                MT_COMPANY ent = new MT_COMPANY();

                DateTime now = DateTime.Now;

                //using (var context = new EntityCPAIEngine())
                //{
                //    using (var dbContextTransaction = context.Database.BeginTransaction())
                //    {
                try
                {
                    ent.MCO_COMPANY_CODE = pModel.CompanyCode;
                    ent.MCO_SHORT_NAME = pModel.CompanyName.Trim();
                    ent.MCO_STATUS = pModel.Status;

                    ent.MCO_UPDATED_BY = pUser;
                    ent.MCO_UPDATED_DATE = now;

                    dal.Update(ent);

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
                //    }

                //}

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
            }

            return rtn;
        }

        public ReturnValue EditHedge(CompanyViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel.BoolStatus != null)
                {
                    pModel.Status = pModel.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                }
                else
                {
                    pModel.Status = "INACTIVE";
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyCode))
                {
                    rtn.Message = "Company code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyName))
                {
                    rtn.Message = "Company name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_COMPANY_DAL dal = new MT_COMPANY_DAL();
                MT_COMPANY ent = new MT_COMPANY();
                HEDG_COMPANY_AUTH_TRADE_DAL autDal = new HEDG_COMPANY_AUTH_TRADE_DAL();
                MT_COMPANY_AUTH_TRADE aut = new MT_COMPANY_AUTH_TRADE();

                DateTime now = DateTime.Now;

                //using (var context = new EntityCPAIEngine())
                //{
                //    using (var dbContextTransaction = context.Database.BeginTransaction())
                //    {
                try
                {
                    ent.MCO_COMPANY_CODE = pModel.CompanyCode;
                    ent.MCO_SHORT_NAME = pModel.CompanyName.Trim();
                    ent.MCO_STATUS = pModel.Status;
                    ent.MCO_CREATE_TYPE = pModel.CreateType;
                    ent.MCO_FULL_NAME = pModel.fullName;
                    ent.MCO_DESCRIPTION = pModel.description;
                    ent.MCO_ACC_NUM_PAY = pModel.accountPay;
                    ent.MCO_ACC_NUM_RECEIVE = pModel.accountReceive;
                    ent.MCO_CREATED_BY = pUser;
                    ent.MCO_CREATED_DATE = now;
                    ent.MCO_UPDATED_BY = pUser;
                    ent.MCO_UPDATED_DATE = now;               
                    dal.Update(ent);


                    

                  
                if (pModel.Company_auth != null && pModel.Company_auth.Any())
                {
                    var com_auth = autDal.getComAuthByCompany(pModel.CompanyCode);

                    if (com_auth.Count != pModel.Company_auth.Count)
                    {
                        autDal.Delete(pModel.CompanyCode);
                        foreach (var item in pModel.Company_auth)
                        {
                            aut = new MT_COMPANY_AUTH_TRADE();
                            aut.MCAT_ROW_ID = ShareFn.GenerateCodeByDate("CPAI");
                            aut.MCAT_FK_MT_COMPANY = pModel.CompanyCode;
                            aut.MCAT_ORDER = item.Order;
                            aut.MCAT_TRADING_BOOK = item.Trading_book;
                            aut.MCAT_TRADE_FOR_FLAG = item.Trading_for_flag == true ? "Y" : "N";
                            aut.MCAT_FEE_BBL = item.Fee_bbl;
                            aut.MCAT_FEE_MT = item.Fee_mt;
                            aut.MCAT_CREATED_DATE = now;
                            aut.MCAT_CREATED_BY = pUser;
                            aut.MCAT_UPDATED_DATE = now;
                            aut.MCAT_UPDATED_BY = pUser;
                            autDal.Save(aut);
                        }
                    }
                    else
                    {
                            foreach (var item in pModel.Company_auth)
                            {
                                aut = new MT_COMPANY_AUTH_TRADE();
                                aut.MCAT_ROW_ID = item.RowID;
                                aut.MCAT_FK_MT_COMPANY = pModel.CompanyCode;
                                aut.MCAT_ORDER = item.Order;
                                aut.MCAT_TRADING_BOOK = item.Trading_book;
                                aut.MCAT_TRADE_FOR_FLAG = item.Trading_for_flag == true ? "Y" : "N";
                                aut.MCAT_FEE_BBL = item.Fee_bbl;
                                aut.MCAT_FEE_MT = item.Fee_mt;
                                aut.MCAT_CREATED_DATE = now;
                                aut.MCAT_CREATED_BY = pUser;
                                aut.MCAT_UPDATED_DATE = now;
                                aut.MCAT_UPDATED_BY = pUser;
                                autDal.Update(aut);
                            }
                        }
                    }
                    else
                    {
                        var com_auth = autDal.getComAuthByCompany(pModel.CompanyCode);
                        if (com_auth.Count > 0)
                        {
                            autDal.Delete(pModel.CompanyCode);
                        }
                    }

                   

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
                //    }

                //}

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
            }

            return rtn;
        }

        public ReturnValue Search(ref CompanyViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sCode = String.IsNullOrEmpty(pModel.sCompanyCode) ? "" : pModel.sCompanyCode.ToUpper();
                    var sName = String.IsNullOrEmpty(pModel.sCompanyName) ? "" : pModel.sCompanyName.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();
                    var sCreateType = String.IsNullOrEmpty(pModel.sCreatedType) ? "" : pModel.sCreatedType.ToUpper();

                    var sDateFrom = String.IsNullOrEmpty(pModel.sDateFrom) ? "" : pModel.sDateFrom.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.sDateFrom) ? "" : pModel.sDateFrom.ToUpper().Substring(14, 10);

                    var query = (from v in context.MT_COMPANY
                                 orderby new { v.MCO_SHORT_NAME }
                                 where v.MCO_COMPANY_CODE.ToUpper().Contains(sCode) && v.MCO_SHORT_NAME.ToUpper().Contains(sName)
                                 select v);



                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.MCO_UPDATED_DATE >= sDate && p.MCO_UPDATED_DATE <= eDate);
                    }

                    if (!string.IsNullOrEmpty(sStatus))
                    {
                        query = query.Where(p => p.MCO_STATUS == sStatus);
                    }

                    if (!string.IsNullOrEmpty(sCreateType))
                    {
                        if (sCreateType == "CPAI")
                        {
                            query = query.Where(p => p.MCO_CREATE_TYPE == sCreateType);
                        }
                        else
                        {
                            query = query.Where(p => p.MCO_CREATE_TYPE == null || p.MCO_CREATE_TYPE == "SAP");
                        }

                    }



                    if (query != null)
                    {
                        pModel.sSearchData = new List<CompanyViewModel_SeachData>();

                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new CompanyViewModel_SeachData
                                                    {
                                                        dCompanyCode = g.MCO_COMPANY_CODE,
                                                        dCompanyName = g.MCO_SHORT_NAME,
                                                        dStatus = g.MCO_STATUS,
                                                        dCreatedBy = g.MCO_CREATED_BY,
                                                        dCreatedDate = (g.MCO_CREATED_DATE ?? DateTime.Now).ToString("dd/MM/yyyy"),
                                                        dUpdatedBy = g.MCO_UPDATED_BY,
                                                        dUpdatedDate = (g.MCO_UPDATED_DATE ?? DateTime.Now).ToString("dd/MM/yyyy"),
                                                        dCreatedType = String.IsNullOrEmpty(g.MCO_CREATE_TYPE) ? "SAP" : g.MCO_CREATE_TYPE,
                                                    });
                        }

                        // pModel.sSearchData = queryFilter.ToList();
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }

                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                //string path = path = (LogManager.GetCurrentLoggers()[0].Logger.Repository.GetAppenders()[0] as FileAppender).File;
                rtn.Message = ex.Message;
                rtn.Status = false;
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
            }
            return rtn;
        }

        public CompanyViewModel_Detail Get(string pCompanyCode)
        {
            CompanyViewModel_Detail model = new CompanyViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pCompanyCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_COMPANY
                                     where v.MCO_COMPANY_CODE.ToUpper().Equals(pCompanyCode.ToUpper())
                                     select new
                                     {
                                         dCompanyCode = v.MCO_COMPANY_CODE
                                         ,
                                         dCompanyName = v.MCO_SHORT_NAME
                                         ,
                                         dStatus = v.MCO_STATUS
                                         ,
                                         dCreatedBy = v.MCO_CREATED_BY
                                         ,
                                         dCreatedDate = v.MCO_CREATED_DATE
                                         ,
                                         dUpdatedBy = v.MCO_UPDATED_BY
                                         ,
                                         dUpdatedDate = v.MCO_UPDATED_DATE
                                         ,
                                         dCreatedType = String.IsNullOrEmpty(v.MCO_CREATE_TYPE) ? "SAP" : v.MCO_CREATE_TYPE,


                                     });

                        if (query != null)
                        {
                            foreach (var g in query)
                            {
                                model.CreateType = g.dCreatedType;

                                model.CompanyCode = g.dCompanyCode;
                                model.CompanyName = g.dCompanyName;
                                model.Status = g.dStatus;
                                model.BoolStatus = g.dStatus == "ACTIVE" ? true : false;


                            }
                        }

                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public CompanyViewModel_Detail GetHedgeCompany(string pCompanyCode)
        {
            CompanyViewModel_Detail model = new CompanyViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pCompanyCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_COMPANY
                                     where v.MCO_COMPANY_CODE.ToUpper().Equals(pCompanyCode.ToUpper())
                                     select new
                                     {
                                         dCompanyCode = v.MCO_COMPANY_CODE
                                         ,
                                         dCompanyName = v.MCO_SHORT_NAME
                                         ,
                                         dFullName = v.MCO_FULL_NAME
                                         ,
                                         dDescription = v.MCO_DESCRIPTION
                                         ,
                                         dAccountPay = v.MCO_ACC_NUM_PAY
                                         ,
                                         dAccountReceive = v.MCO_ACC_NUM_RECEIVE
                                         ,
                                         dStatus = v.MCO_STATUS
                                         ,
                                         dCreatedBy = v.MCO_CREATED_BY
                                         ,
                                         dCreatedDate = v.MCO_CREATED_DATE
                                         ,
                                         dUpdatedBy = v.MCO_UPDATED_BY
                                         ,
                                         dUpdatedDate = v.MCO_UPDATED_DATE
                                         ,
                                         dCreatedType = String.IsNullOrEmpty(v.MCO_CREATE_TYPE) ? "SAP" : v.MCO_CREATE_TYPE,


                                     });

                        var dataAuth = (from d in context.MT_COMPANY_AUTH_TRADE
                                        where d.MCAT_FK_MT_COMPANY == pCompanyCode
                                        select new CompanyViewModel_Auth
                                        {
                                            RowID = d.MCAT_ROW_ID,
                                            Order = d.MCAT_ORDER,
                                            Trading_book = d.MCAT_TRADING_BOOK,
                                            Trading_for_flag = d.MCAT_TRADE_FOR_FLAG == "Y" ? true : false,
                                            Fee_bbl= d.MCAT_FEE_BBL,
                                            Fee_mt = d.MCAT_FEE_MT
                                        }).ToList();



                        if (query != null)
                        {
                            foreach (var g in query)
                            {
                                model.CreateType = g.dCreatedType;

                                model.CompanyCode = g.dCompanyCode;
                                model.CompanyName = g.dCompanyName;

                                model.Status = g.dStatus;
                                model.BoolStatus = g.dStatus == "ACTIVE" ? true : false;

                                model.fullName = g.dFullName;
                                model.description = g.dDescription;
                                model.accountPay = g.dAccountPay;
                                model.accountReceive = g.dAccountReceive;
                                model.Company_auth = dataAuth.OrderBy(c => c.Order).ToList() ;
                            }
                        }

                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }



        public static string GetCompanyCodeJSON(string pStatus = "ACTIVE")
        {
            string json = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCO_COMPANY_CODE }
                                    select new { System = d.MCO_COMPANY_CODE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetCompanyNameJSON(string pStatus = "ACTIVE")
        {
            string json = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCO_SHORT_NAME }
                                    select new { System = d.MCO_SHORT_NAME.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }


    }
}