﻿using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CoolServiceModel
    {
        public static string getTransactionByID(string id)
        {
            CooRootObject rootObj = new CooRootObject();
            rootObj.data = new CooData();
            rootObj.experts = new CooExperts();

            COO_DATA_DAL dataDal = new COO_DATA_DAL();
            COO_DATA data = dataDal.GetByID(id);
            if (data != null)
            {
                rootObj.data.date_purchase = (data.CODA_REQUESTED_DATE == DateTime.MinValue || data.CODA_REQUESTED_DATE == null) ? "" : Convert.ToDateTime(data.CODA_REQUESTED_DATE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.data.approval_date = (data.CODA_COMPLETE_DATE == DateTime.MinValue || data.CODA_COMPLETE_DATE == null) ? "" : Convert.ToDateTime(data.CODA_COMPLETE_DATE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.data.area_unit = data.CODA_AREA_UNIT;
                rootObj.data.assay_date = (data.CODA_ASSAY_DATE == DateTime.MinValue || data.CODA_ASSAY_DATE == null) ? "" : Convert.ToDateTime(data.CODA_ASSAY_DATE).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.data.assay_from = data.CODA_ASSAY_TYPE;
                rootObj.data.assay_ref = data.CODA_ASSAY_REF_NO;
                rootObj.data.country = data.CODA_COUNTRY;
                rootObj.data.origin = data.CODA_FK_COUNTRY;
                rootObj.data.crude_categories = data.CODA_CRUDE_CATEGORIES;
                rootObj.data.crude_characteristic = data.CODA_CRUDE_CHARACTERISTIC;
                rootObj.data.crude_kerogen = data.CODA_CRUDE_KEROGEN_TYPE;
                rootObj.data.crude_maturity = data.CODA_CRUDE_MATURITY;
                rootObj.data.crude_name = data.CODA_CRUDE_NAME;
                rootObj.data.crude_id = data.CODA_FK_MATERIALS;
                rootObj.data.type_of_cam = data.CODA_TYPE_OF_CAM;
                rootObj.data.update_from_crude = data.CODA_CRUDE_REFERENCE;
                rootObj.data.workflow_priority = data.CODA_PRIORITY;
                rootObj.data.workflow_status = data.CODA_STATUS;
                rootObj.data.workflow_status_description = getWorkflowStatusDescription(rootObj.data.workflow_status);
                rootObj.data.name = data.CODA_REQUESTED_BY;
                rootObj.data.fouling_possibility = data.CODA_FOULING_POSSIBILITY;
                rootObj.data.draft_cam_file = data.CODA_DRAFT_CAM_PATH;
                rootObj.data.final_cam_file = data.CODA_FINAL_CAM_PATH;
                rootObj.data.created_by = data.CODA_CREATED_BY;
                rootObj.data.created_date = (data.CODA_CREATED == DateTime.MinValue || data.CODA_CREATED == null) ? "" : Convert.ToDateTime(data.CODA_CREATED).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.note = data.CODA_NOTE;

                COO_EXPERT_DAL dataExpertDAL = new COO_EXPERT_DAL();
                List<COO_EXPERT> lstExpert = dataExpertDAL.GetAllByID(id);
                if (lstExpert != null)
                {
                    rootObj.experts.areas = new List<CooArea>();
                    var arealst = lstExpert.GroupBy(x => new { x.COEX_AREA, x.COEX_AREA_ORDER }).Select(g => new { order = g.First().COEX_AREA_ORDER, name = g.First().COEX_AREA }).ToList();
                    for (int i = 0; i < arealst.Count; i++)
                    {
                        CooArea area = new CooArea();
                        area.name = arealst[i].name;
                        area.order = arealst[i].order;
                        area.units = new List<CooUnit>();
                        var unitlst = lstExpert.Where(x => x.COEX_AREA == area.name).ToList();
                        foreach (var unit in unitlst)
                        {
                            CooUnit new_unit = new CooUnit();
                            new_unit.order = unit.COEX_UNIT_ORDER;
                            new_unit.name = unit.COEX_UNIT;
                            new_unit.key = unit.COEX_FK_MT_UNIT;
                            new_unit.question_flag = unit.COEX_QUESTION_FLAG;
                            new_unit.value = unit.COEX_ACTIVATION_STATUS == "UNSELECTED" ? "N" : "Y";
                            area.units.Add(new_unit);
                        }
                        rootObj.experts.areas.Add(area);
                    }
                }

                List<COO_EXPERT> lstExpertActive = dataExpertDAL.GetAllByID(id, true);
                if (lstExpertActive.Count > 0)
                {
                    lstExpertActive = lstExpertActive.OrderBy(x => x.COEX_AREA_ORDER).ThenBy(x => x.COEX_UNIT_ORDER).ToList();
                    rootObj.areas = new CoexAreas();
                    rootObj.areas.area = new List<CoexArea>();
                    List<string> areas = lstExpertActive.GroupBy(x => x.COEX_AREA).Select(x => x.Key).ToList();
                    for (int i = 0; i < areas.Count; i++)
                    {
                        CoexArea area = new CoexArea();
                        area.name = areas[i];
                        COO_EXPERT_ITEMS_DAL itemDal = new COO_EXPERT_ITEMS_DAL();
                        area.unit = new List<CoexUnit>();
                        List<COO_EXPERT> newLstExpert = lstExpertActive.Where(x => x.COEX_AREA == areas[i]).ToList();
                        for (int j = 0; j < newLstExpert.Count(); j++)
                        {
                            CoexUnit unit = new CoexUnit();
                            unit.key = newLstExpert[j].COEX_FK_MT_UNIT;
                            unit.comment = new CoexComment();
                            unit.comment.question_flag = newLstExpert[j].COEX_QUESTION_FLAG;
                            unit.comment.agreed_score = newLstExpert[j].COEX_AGREED_SCORE;
                            unit.comment.average_score = newLstExpert[j].COEX_AVERAGE_SCORE;
                            unit.comment.comment_expert = newLstExpert[j].COEX_COMMENT_EXPERT;
                            unit.comment.comment_expert_sh = newLstExpert[j].COEX_COMMENT_EXPERT_SH;
                            unit.comment.recommended_score = newLstExpert[j].COEX_RECOMMENDED_SCORE;
                            unit.comment.recommended_score_reason = newLstExpert[j].COEX_SCORE_REASON;
                            unit.comment.score_flag = newLstExpert[j].COEX_SCORE_FLAG;
                            unit.comment.processing_flag = newLstExpert[j].COEX_PROCESSING_FLAG;
                            unit.comment.processing_note = newLstExpert[j].COEX_PROCESSING_NOTE;
                            unit.name = newLstExpert[j].COEX_UNIT;
                            unit.status = newLstExpert[j].COEX_STATUS;
                            unit.attached_file = newLstExpert[j].COEX_ATTACHED_FILE;
                            unit.isSelected_expert = newLstExpert[j].COEX_EXPERT_SELECTED;
                            unit.isSelected_expert_sh = newLstExpert[j].COEX_EXPERT_SH_SELECTED;
                            unit.next_status = newLstExpert[j].COEX_STATUS == CPAIConstantUtil.STATUS_APPROVED ? "APPROVE" : newLstExpert[j].COEX_STATUS;
                            List<COO_EXPERT_ITEMS> item = itemDal.GetAllByID(newLstExpert[j].COEX_ROW_ID);
                            if (item.Count > 0)
                            {
                                unit.comment_items = new List<CoexCommentItem>();
                                for (int k = 0; k < item.Count; k++)
                                {
                                    CoexCommentItem comment = new JavaScriptSerializer().Deserialize<CoexCommentItem>(item[k].COEI_OP_JSON);
                                    unit.comment_items.Add(comment);
                                }
                                unit.comment_items = unit.comment_items.OrderBy(x => x.order).ToList();
                            }
                            area.unit.Add(unit);
                        }
                        rootObj.areas.area.Add(area);
                    }
                }

                COO_CAM_DAL dataCamDAL = new COO_CAM_DAL();
                COO_CAM dataCam = dataCamDAL.GetLatestByID(id);
                if(dataCam != null)
                {
                    rootObj.data.cam_file = dataCam.COCA_FILE_PATH;
                }
                
                COO_ASSAY_DAL dataAssayDAL = new COO_ASSAY_DAL();
                COO_ASSAY dataAssay = dataAssayDAL.GetLatestByID(id);
                rootObj.data.assay_file = dataAssay != null ? dataAssay.COAS_FILE_PATH : "";


                rootObj.comment_draft_cam = data.CODA_COMMENT_DRAFT_CAM;
                rootObj.comment_final_cam = data.CODA_COMMENT_FINAL_CAM;
                rootObj.approve_items = null;
                //n/a
                string strExplanationAttach = "";
                COO_SUP_DOC_DAL supDocDAL = new COO_SUP_DOC_DAL();
                List<COO_SUP_DOC> supDoc = supDocDAL.GetAllByID(id);
                foreach (var item in supDoc)
                {
                    strExplanationAttach += item.COSD_FILE_PATH + ":" + item.COSD_FILE_TYPE + "|";
                }
                rootObj.support_doc_file = strExplanationAttach;
            }
            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getWorkflowStatusDescription(string workflow_status)
        {
            var workflow_description = workflow_status;
            if (workflow_status == CPAIConstantUtil.STATUS_WAITING_APPROVE_DRAFT_CAM)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_APPROVE_DRAFT_CAM;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_EXPERT_APPROVE;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_APPROVE_FINAL_CAM)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_APPROVE_FINAL_CAM;
            }
            return workflow_description;
        }

        public static string getDocumentNoByID(string id)
        {
            COO_DATA_DAL dataDal = new COO_DATA_DAL();
            COO_DATA data = dataDal.GetByID(id);
            if (data != null)
            {
                return data.CODA_PURCHASE_NO;
            }
            return null;
        }

        public static string getAssayRefByID(string id)
        {
            COO_DATA_DAL dataDal = new COO_DATA_DAL();
            COO_DATA data = dataDal.GetByID(id);
            if (data != null)
            {
                return data.CODA_ASSAY_REF_NO;
            }
            return null;
        }

        public static string getLatestFinalCAM(string crude, string country)
        {
            COO_DATA_DAL dataDal = new COO_DATA_DAL();
            string path = dataDal.getLatestFinalCAM(crude, country);
            return path;
        }

        public static List<string> getUserGroup(string user)
        {
            UserGroupDAL ugDAL = new UserGroupDAL();
            List<CPAI_USER_GROUP> ug = ugDAL.getUserList(user, ConstantPrm.SYSTEM.COOL);
            if (ug != null)
            {
                List<string> groups = new List<string>();
                foreach (var group in ug)
                {
                    groups.Add(group.USG_USER_GROUP.ToUpper());
                }
                return groups;
            }
            else
            {
                return null;
            }
        }

        public static string getUserDepartment(string user)
        {
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from c in context.USERS
                             where c.USR_LOGIN.ToUpper() == user.ToUpper()
                             select c
                             );
                string department = "";
                if (query.Any())
                {
                    foreach (var item in query)
                    {
                        department = item.USR_SECTION;
                        break;
                    }
                }
                return department;
            }
            catch (Exception)
            {
                return "";
                throw;
            }
        }

        public static List<CooArea> getCooArea(string name = "")
        {
            List<CooArea> cooAreas = (from area in MT_AREA_DAL.GetArea()
                                      where area.MAR_NAME == (!string.IsNullOrEmpty(name) ? name : area.MAR_NAME) && area.MAR_STATUS == CPAIConstantUtil.ACTIVE
                                      select new CooArea()
                                      {
                                          name = area.MAR_NAME,
                                          order = area.MAR_ORDER,
                                          units = (from unit in area.MT_UNIT
                                                   where unit.MUN_STATUS == CPAIConstantUtil.ACTIVE
                                                   select new CooUnit() { key = unit.MUN_ROW_ID, name = unit.MUN_NAME, order = unit.MUN_ORDER, value = (unit.MUN_NAME != "TLB" ? "Y" : "N"), question_flag = unit.MUN_QUESTION_FLAG }).ToList()
                                      }).ToList();
            return cooAreas;
        }

        public static string getQuestionFlag(string unit_key)
        {
            MT_UNIT_DAL unitDal = new MT_UNIT_DAL();
            MT_UNIT unit = unitDal.getUnit(unit_key);
            return !string.IsNullOrEmpty(unit.MUN_QUESTION_FLAG) ? unit.MUN_QUESTION_FLAG : "N";
        }

        public static bool hasSelectedUnit(string id)
        {
            COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
            bool isSelected = expertDal.GetAllByID(id).Where(x => x.COEX_ACTIVATION_STATUS != "UNSELECTED").Count() > 0 ? true : false;
            return isSelected;
        }

        public static void updateModelFromGroup(string user, ref CoolViewModel model)
        {
            List<string> unitGroup = getUserGroup(user);

            if (model.areas != null)
            {
                CoexAreas areas = new CoexAreas();
                areas.area = new List<CoexArea>();
                List<string> unit_name = new List<string>();

                if (unitGroup.Contains("EXPERT_SH") && unitGroup.Contains("EXPERT"))
                {
                    for (int i = 0; i < model.areas.area.Count; i++)
                    {
                        CoexArea area = new CoexArea();
                        area.name = model.areas.area[i].name;

                        area.unit = new List<CoexUnit>();
                        for (int j = 0; j < model.areas.area[i].unit.Count; j++)
                        {
                            if (unitGroup.Contains(model.areas.area[i].unit[j].name.ToUpper()) && (model.areas.area[i].unit[j].status == CPAIConstantUtil.ACTION_APPROVE_3 || model.areas.area[i].unit[j].status == CPAIConstantUtil.STATUS_APPROVED || model.areas.area[i].unit[j].status == CPAIConstantUtil.ACTION_APPROVE_2))
                            {
                                area.unit.Add(model.areas.area[i].unit[j]);
                                unit_name.Add(model.areas.area[i].unit[j].name);
                            }
                        }
                        if (area.unit != null && area.unit.Count > 0)
                        {
                            areas.area.Add(area);
                        }
                    }
                }
                else
                {
                    if (unitGroup.Contains("EXPERT_SH"))
                    {
                        for (int i = 0; i < model.areas.area.Count; i++)
                        {
                            CoexArea area = new CoexArea();
                            area.name = model.areas.area[i].name;

                            if (unitGroup.Contains(model.areas.area[i].name.ToUpper()))
                            {
                                area.unit = new List<CoexUnit>();
                                for (int j = 0; j < model.areas.area[i].unit.Count; j++)
                                {
                                    if (model.areas.area[i].unit[j].status == CPAIConstantUtil.ACTION_APPROVE_3 || model.areas.area[i].unit[j].status == CPAIConstantUtil.STATUS_APPROVED)
                                    {
                                        area.unit.Add(model.areas.area[i].unit[j]);
                                        unit_name.Add(model.areas.area[i].unit[j].name);
                                    }
                                }
                            }
                            if (area.unit != null && area.unit.Count > 0)
                            {
                                areas.area.Add(area);
                            }
                        }
                    }

                    else if (unitGroup.Contains("EXPERT"))
                    {
                        for (int i = 0; i < model.areas.area.Count; i++)
                        {
                            CoexArea area = new CoexArea();
                            area.name = model.areas.area[i].name;
                            area.unit = new List<CoexUnit>();
                            for (int j = 0; j < model.areas.area[i].unit.Count; j++)
                            {
                                if (unitGroup.Contains(model.areas.area[i].unit[j].name.ToUpper()))
                                {
                                    area.unit.Add(model.areas.area[i].unit[j]);
                                    unit_name.Add(model.areas.area[i].unit[j].name);
                                }
                            }
                            if (area.unit != null && area.unit.Count > 0)
                            {
                                areas.area.Add(area);
                            }
                        }
                    }
                }


                model.areas = areas;

                foreach (var a in model.experts.areas)
                {
                    foreach (var b in a.units)
                    {
                        if (unitGroup.Contains("EXPERT") && !unitGroup.Contains("EXPERT_SH"))
                        {
                            if (unit_name.Contains(b.name))
                            {
                                b.value = "Y";
                            }
                            else
                            {
                                b.value = "N";
                            }
                        }
                    }
                }

                List<OperatParamViewModel_Detail> list = new List<OperatParamViewModel_Detail>();
                foreach (var models in model.experts.areas)
                {
                    foreach (var unit in models.units)
                    {
                        if (unit.value == "Y")
                        {
                            OperatParamServiceModel OperatParamModel = new OperatParamServiceModel();
                            OperatParamViewModel_Detail OperatParamDetail = OperatParamModel.GetOperParamDetail(unit.key, true);
                            if (OperatParamDetail != null)
                            {
                                list.Add(OperatParamDetail);
                            }
                        }
                    }
                }

                model.unit_score = new List<UnitScore>();
                for (int i = 0; i < model.areas.area.Count; i++)
                {
                    for (int j = 0; j < model.areas.area[i].unit.Count; j++)
                    {
                        UnitScore unitscore = new UnitScore();
                        unitscore.unit_key = model.areas.area[i].unit[j].key;
                        unitscore.score = new List<string>();
                        if (model.areas.area[i].unit[j].comment_items == null)
                        {
                            CoexArea area = new CoexArea();
                            List<CoexUnit> unitlist = new List<CoexUnit>();
                            CoexUnit units = new CoexUnit();
                            string unit_key = model.areas.area[i].unit[j].key;
                            OperatParamViewModel_Detail unit_oper = list.Where(x => x.Unit == unit_key).FirstOrDefault();
                            model.areas.area[i].unit[j].comment_items = new List<CoexCommentItem>();
                            if (unit_oper != null)
                            {
                                foreach (var b in unit_oper.Param)
                                {
                                    CoexCommentItem comment = new CoexCommentItem();
                                    comment.order = b.Order;
                                    comment.operational_parameter = b.Parameter;
                                    List<CoexScoreDetail> ScoreDetailTemp = new List<CoexScoreDetail>();
                                    foreach (var c in b.DataKeyValue)
                                    {
                                        CoexScoreDetail ScoreDetail = new CoexScoreDetail();
                                        ScoreDetail.score = c.Score;
                                        if (!unitscore.score.Contains(c.Score))
                                        {
                                            unitscore.score.Add(c.Score);
                                        }
                                        bool isMatchCondition = true;
                                        foreach (var d in c.KeyValue)
                                        {
                                            ScoreDetail.description += d.Key + " ";
                                            OperatParamViewModel_Condition dataCondition = unit_oper.DataCondition.Where(x => x.Key == d.Key).FirstOrDefault();
                                            if (dataCondition != null)
                                            {
                                                DataTable dt = new DataTable();
                                                string cells = dataCondition.Column + dataCondition.Row;
                                                string values = getDataFromExcel(model, cells);
                                                string value = "";

                                                string pattern = @"([A-Za-z])";
                                                Regex regex = new Regex(pattern);
                                                Match results = regex.Match(values);

                                                if (!results.Success)
                                                {
                                                    string patternSymbol = @"([-*+/=]+)";
                                                    Regex regexSymbol = new Regex(patternSymbol);
                                                    Match matchSymbol = regexSymbol.Match(values);
                                                    if (matchSymbol.Success)
                                                    {
                                                        value = !string.IsNullOrEmpty(values) ? dt.Compute(values, "").ToString() : null;
                                                    }
                                                    else
                                                    {
                                                        value = values;
                                                    }
                                                }
                                                else
                                                {
                                                    value = values;
                                                }

                                                bool isNumber = ShareFn.IsValidDecimalFormat(value);

                                                bool ck_main = true;
                                                bool ck = true;
                                                bool isFirst = true;
                                                if (isNumber)
                                                {
                                                    foreach (var e in dataCondition.ObjCondition.Condition)
                                                    {
                                                        bool isValueNumber = ShareFn.IsValidDecimalFormat(e.value);

                                                        if (isValueNumber)
                                                        {
                                                            switch (e.operater_value)
                                                            {
                                                                case "==": ck = (Convert.ToDouble(value) == Convert.ToDouble(e.value)); break;
                                                                case ">": ck = (Convert.ToDouble(value) > Convert.ToDouble(e.value)); break;
                                                                case "<": ck = (Convert.ToDouble(value) < Convert.ToDouble(e.value)); break;
                                                                case ">=": ck = (Convert.ToDouble(value) >= Convert.ToDouble(e.value)); break;
                                                                case "<=": ck = (Convert.ToDouble(value) <= Convert.ToDouble(e.value)); break;
                                                                default: ck = false; break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            switch (e.operater_value)
                                                            {
                                                                case "==": ck = value == e.value; break;
                                                                default: ck = false; break;
                                                            }
                                                        }

                                                        if (!isFirst)
                                                        {
                                                            switch (e.and_or)
                                                            {
                                                                case "&&": ck = (ck_main && ck); break;
                                                                case "||": ck = (ck_main || ck); break;
                                                                default: ck = ck_main; break;
                                                            }
                                                        }
                                                        ck_main = ck;
                                                        isFirst = false;
                                                    }
                                                }
                                                else
                                                {
                                                    foreach (var e in dataCondition.ObjCondition.Condition)
                                                    {
                                                        switch (e.operater_value)
                                                        {
                                                            case "==": ck = (value == e.value); break;
                                                            default: ck = false; break;
                                                        }

                                                        if (!isFirst)
                                                        {
                                                            switch (e.and_or)
                                                            {
                                                                case "&&": ck = (ck_main && ck); break;
                                                                case "||": ck = (ck_main || ck); break;
                                                                default: ck = ck_main; break;
                                                            }
                                                        }
                                                        ck_main = ck;
                                                        isFirst = false;
                                                    }
                                                }

                                                isMatchCondition &= ck_main;
                                            }
                                        }
                                        ScoreDetail.flag = isMatchCondition ? "Y" : "N";
                                        ScoreDetailTemp.Add(ScoreDetail);
                                    }
                                    comment.score_detail = ScoreDetailTemp;
                                    comment.weight_factor = b.Factor;
                                    model.areas.area[i].unit[j].comment_items.Add(comment);
                                }

                                unitscore.score.Sort();
                                model.unit_score.Add(unitscore);
                            }
                            else
                            {
                                model.areas.area[i].unit[j].comment_items.Add(new CoexCommentItem());
                            }
                        }
                        else
                        {
                            if (model.areas.area[i].unit[j].comment_items.Count > 0)
                            {
                                foreach (var item in model.areas.area[i].unit[j].comment_items)
                                {
                                    if (item.score_detail != null && item.score_detail.Count > 0)
                                    {
                                        foreach (var score in item.score_detail)
                                        {
                                            if (!unitscore.score.Contains(score.score))
                                            {
                                                unitscore.score.Add(score.score);
                                            }
                                        }
                                    }
                                    unitscore.score.Sort();
                                    model.unit_score.Add(unitscore);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static string getDataFromExcel(CoolViewModel model, string cell)
        {
            string file_path = !string.IsNullOrEmpty(model.data.draft_cam_file) ? model.data.draft_cam_file : !string.IsNullOrEmpty(model.data.final_cam_file) ? model.data.final_cam_file : "";
            if (!string.IsNullOrEmpty(file_path))
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file_path);
                try
                {
                    WebRequest request = WebRequest.Create(path);
                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            var excelPackage = new ExcelPackage(stream);
                            ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.First();
                            var Formula = excelWorksheet.Cells[cell].Formula;
                            if (!string.IsNullOrEmpty(Formula))
                            {
                                return Formula;
                            }
                            return excelWorksheet.Cells[cell].Value == null ? "" : excelWorksheet.Cells[cell].Value.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
            return "";
        }

        public List<ExpertTemplate> getExpertComment(string id, int start_row = 1)
        {
            List<ExpertTemplate> expertTemplates = new List<ExpertTemplate>();
            ExpertTemplate expertTemplate = new ExpertTemplate();
            string area_header_column = "A";
            string unit_header_column = "B";
            string data_header_1_column = "C";
            string data_detail_1_column = "D";
            string data_header_2_column = "E";
            string data_detail_2_column = "F";
            string area_header_width = "10";
            string unit_header_width = "15";
            string data_header_1_width = "30";
            string data_detail_1_width = "70";
            string data_header_2_width = "30";
            string data_detail_2_width = "30";
            string root_url = JSONSetting.getGlobalConfig("ROOT_URL");
            int current_row = start_row;

            if (!string.IsNullOrEmpty(id))
            {
                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                COO_DATA data = dataDal.GetByID(id);
                if (data != null)
                {
                    // EXPERT COMMENT FOR CURRENT CRUDE //
                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = area_header_column;
                    expertTemplate.row = (current_row++) + "";
                    expertTemplate.value = "Expert Comment (" + data.CODA_ASSAY_REF_NO + ") " + (data.CODA_STATUS == CPAIConstantUtil.STATUS_APPROVED ? "Issued Date: " + data.CODA_UPDATED.ToString("dd-MMM-yyyy") : "");
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "16";
                    expertTemplates.Add(expertTemplate);

                    COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                    List<COO_EXPERT> experts = expertDal.GetAllByID(id, true);
                    if (experts != null && experts.Count > 0)
                    {
                        List<string> areas = experts.Where(x => x.COEX_APPROVE_DATE.HasValue).GroupBy(x => x.COEX_AREA).Select(x => x.Key).ToList();
                        for (int i = 0; i < areas.Count; i++)
                        {
                            expertTemplate = new ExpertTemplate();
                            expertTemplate.column = area_header_column;
                            expertTemplate.row = (current_row++) + "";
                            expertTemplate.value = areas[i];
                            expertTemplate.font_bold = "Y";
                            expertTemplate.font_size = "16";
                            expertTemplates.Add(expertTemplate);

                            List<COO_EXPERT> newLstExpert = experts.Where(x => x.COEX_AREA == areas[i] && x.COEX_APPROVE_DATE.HasValue).ToList();
                            for (int j = 0; j < newLstExpert.Count; j++)
                            {
                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = unit_header_column;
                                expertTemplate.row = (current_row++) + "";
                                expertTemplate.value = newLstExpert[j].COEX_UNIT;
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Expert name:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                string department = getUserDepartment(newLstExpert[j].COEX_SUBMITTED_BY);
                                if (!string.IsNullOrEmpty(department))
                                {
                                    department = " (" + department + ")";
                                }
                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_detail_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = !string.IsNullOrEmpty(newLstExpert[j].COEX_SUBMITTED_BY) ? UsersDAL.getFullName(newLstExpert[j].COEX_SUBMITTED_BY) + department : "";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_2_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Comment date:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_detail_2_column;
                                expertTemplate.row = (current_row++) + "";
                                expertTemplate.value = !newLstExpert[j].COEX_SUBMIT_DATE.HasValue ? "" : newLstExpert[j].COEX_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy");
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Approver name:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_detail_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = !string.IsNullOrEmpty(newLstExpert[j].COEX_APPROVED_BY) ? UsersDAL.getFullName(newLstExpert[j].COEX_APPROVED_BY) : "";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_2_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Approve date:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_detail_2_column;
                                expertTemplate.row = (current_row++) + "";
                                expertTemplate.value = !newLstExpert[j].COEX_APPROVE_DATE.HasValue ? "" : newLstExpert[j].COEX_APPROVE_DATE.Value.ToString("dd-MMM-yyyy");
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Process ability score:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                int index_expert_item = 1;
                                COO_EXPERT_ITEMS_DAL itemDal = new COO_EXPERT_ITEMS_DAL();
                                List<COO_EXPERT_ITEMS> expertItems = itemDal.GetAllByID(newLstExpert[j].COEX_ROW_ID);
                                foreach (var expert_item in expertItems)
                                {
                                    CoexCommentItem comment = new JavaScriptSerializer().Deserialize<CoexCommentItem>(expert_item.COEI_OP_JSON);
                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = data_detail_1_column;
                                    expertTemplate.row = (current_row++) + "";
                                    expertTemplate.value = index_expert_item + ". " + comment.operational_parameter;
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);

                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = data_detail_1_column;
                                    expertTemplate.row = (current_row++) + "";
                                    expertTemplate.value = "Score = " + comment.weight_score + ", Weight factor = " + comment.weight_factor;
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);

                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = data_detail_1_column;
                                    expertTemplate.row = (current_row++) + "";
                                    expertTemplate.value = "Result: " + (!string.IsNullOrEmpty(comment.calculation_result) ? comment.calculation_result : "Warning calculate no result");
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);

                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = data_detail_1_column;
                                    expertTemplate.row = (current_row++) + "";
                                    expertTemplate.value = "Note: " + (!string.IsNullOrEmpty(comment.calculation_detail) ? comment.calculation_detail : "Warning calculate no detail");
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);
                                    index_expert_item++;

                                }

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_detail_1_column;
                                expertTemplate.row = (current_row++) + "";
                                expertTemplate.value = "COOL recommended score = " + newLstExpert[j].COEX_RECOMMENDED_SCORE;
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_detail_1_column;
                                expertTemplate.row = (current_row++) + "";
                                expertTemplate.value = "Expert recommended score = " + (newLstExpert[j].COEX_AGREED_SCORE ?? newLstExpert[j].COEX_RECOMMENDED_SCORE);
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                var QUESTION_FLAG = MT_UNIT_DAL.getUnitByID(newLstExpert[j].COEX_FK_MT_UNIT).MUN_QUESTION_FLAG;
                                if (QUESTION_FLAG == "Y")
                                {
                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = data_header_1_column;
                                    expertTemplate.row = (current_row) + "";
                                    expertTemplate.value = "Can process in " + newLstExpert[j].COEX_UNIT + "?:";
                                    expertTemplate.font_bold = "Y";
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);

                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = data_detail_1_column;
                                    expertTemplate.row = (current_row++) + "";
                                    expertTemplate.value = newLstExpert[j].COEX_PROCESSING_FLAG == "Y" ? "Yes" : "No";
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);

                                    if (newLstExpert[j].COEX_PROCESSING_FLAG != "Y")
                                    {
                                        expertTemplate = new ExpertTemplate();
                                        expertTemplate.column = data_detail_1_column;
                                        expertTemplate.row = (current_row++) + "";
                                        expertTemplate.value = "Note: " + newLstExpert[j].COEX_PROCESSING_NOTE;
                                        expertTemplate.font_size = "14";
                                        expertTemplates.Add(expertTemplate);
                                    }
                                }

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Comment:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplate.font_horizontal_alignment = "L";
                                expertTemplate.font_vertical_alignment = "T";
                                expertTemplates.Add(expertTemplate);

                                if (!string.IsNullOrEmpty(newLstExpert[j].COEX_COMMENT_EXPERT))
                                {
                                    string[] comments = newLstExpert[j].COEX_COMMENT_EXPERT.Split((char)13);
                                    foreach (var comment in comments)
                                    {
                                        expertTemplate = new ExpertTemplate();
                                        expertTemplate.column = data_detail_1_column;
                                        expertTemplate.row = (current_row++) + "";
                                        expertTemplate.value = comment.Trim();
                                        expertTemplate.font_size = "14";
                                        expertTemplate.font_horizontal_alignment = "L";
                                        expertTemplate.font_vertical_alignment = "T";
                                        expertTemplate.font_textwrap = "Y";
                                        expertTemplates.Add(expertTemplate);
                                    }
                                }

                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = data_header_1_column;
                                expertTemplate.row = (current_row) + "";
                                expertTemplate.value = "Attachment:";
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "14";
                                expertTemplates.Add(expertTemplate);

                                if (newLstExpert[j].COEX_ATTACHED_FILE != null && newLstExpert[j].COEX_ATTACHED_FILE != "")
                                {
                                    string[] ATTACHED_FILE_URL = newLstExpert[j].COEX_ATTACHED_FILE.Split('|');
                                    for (int ATTACHED_FILE_URL_INDEX = 0; ATTACHED_FILE_URL_INDEX < ATTACHED_FILE_URL.Length - 1; ATTACHED_FILE_URL_INDEX++)
                                    {
                                        var attach_file_path = ATTACHED_FILE_URL[ATTACHED_FILE_URL_INDEX];

                                        string[] attach_file_links = attach_file_path.Split(':');
                                        string attach_file_link = attach_file_links[0];
                                        string attach_file_url = string.Format("{0}{1}", root_url, attach_file_link);

                                        string[] attach_file_names = attach_file_url.Split('/');
                                        string attach_file_name = attach_file_names[attach_file_names.Length - 1];

                                        expertTemplate = new ExpertTemplate();
                                        expertTemplate.column = data_detail_1_column;
                                        expertTemplate.row = (current_row++) + "";
                                        expertTemplate.hyperlink = attach_file_url;
                                        expertTemplate.text = attach_file_name;
                                        expertTemplate.font_size = "14";
                                        expertTemplates.Add(expertTemplate);
                                    }
                                }
                                else
                                {
                                    current_row++;
                                }
                            }
                        }
                    }
                    else
                    {
                        expertTemplate = new ExpertTemplate();
                        expertTemplate.column = area_header_column;
                        expertTemplate.row = (current_row++) + "";
                        expertTemplate.value = "No Data Comment";
                        expertTemplate.font_bold = "N";
                        expertTemplate.font_size = "14";
                        expertTemplates.Add(expertTemplate);
                    }

                    // EXPERT COMMENT FOR CRUDE REFERANCE //
                    if (!string.IsNullOrEmpty(data.CODA_TYPE_OF_CAM))
                    {
                        var TYPE_OF_CAM = data.CODA_TYPE_OF_CAM;
                        var CRUDE_REFERENCE = data.CODA_CRUDE_REFERENCE;
                        while (TYPE_OF_CAM == "U")
                        {
                            COO_DATA data_old = COO_DATA_DAL.GetByAssayReferenceNo(CRUDE_REFERENCE);

                            if (data_old == null)
                            {
                                break;
                            }
                            TYPE_OF_CAM = data_old.CODA_TYPE_OF_CAM;
                            CRUDE_REFERENCE = data_old.CODA_CRUDE_REFERENCE;

                            expertTemplate = new ExpertTemplate();
                            expertTemplate.column = string.Format("{0}|{1}", area_header_column, data_detail_2_column);
                            expertTemplate.row = (current_row++) + "";
                            expertTemplate.border_bottom_style = "4";
                            expertTemplate.border_bottom_color = "#000000";
                            expertTemplates.Add(expertTemplate);

                            if (data_old != null)
                            {
                                expertTemplate = new ExpertTemplate();
                                expertTemplate.column = area_header_column;
                                expertTemplate.row = (current_row++) + "";
                                expertTemplate.value = "Expert Comment (" + data_old.CODA_ASSAY_REF_NO + ") " + (data_old.CODA_STATUS == CPAIConstantUtil.STATUS_APPROVED ? "Issued Date: " + data_old.CODA_UPDATED.ToString("dd-MMM-yyyy") : "");
                                expertTemplate.font_bold = "Y";
                                expertTemplate.font_size = "16";
                                expertTemplates.Add(expertTemplate);

                                COO_EXPERT_DAL expertDal_old = new COO_EXPERT_DAL();
                                List<COO_EXPERT> experts_old = expertDal_old.GetAllByID(data_old.CODA_ROW_ID, true);

                                if (experts_old != null && experts_old.Count > 0)
                                {
                                    List<string> areas_old = experts_old.Where(x => x.COEX_APPROVE_DATE.HasValue).GroupBy(x => x.COEX_AREA).Select(x => x.Key).ToList();
                                    for (int index = 0; index < areas_old.Count; index++)
                                    {
                                        expertTemplate = new ExpertTemplate();
                                        expertTemplate.column = area_header_column;
                                        expertTemplate.row = (current_row++) + "";
                                        expertTemplate.value = areas_old[index];
                                        expertTemplate.font_bold = "Y";
                                        expertTemplate.font_size = "16";
                                        expertTemplates.Add(expertTemplate);

                                        List<COO_EXPERT> newLstExpert_old = experts_old.Where(x => x.COEX_AREA == areas_old[index] && x.COEX_APPROVE_DATE.HasValue).ToList();
                                        for (int jindex = 0; jindex < newLstExpert_old.Count; jindex++)
                                        {
                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = unit_header_column;
                                            expertTemplate.row = (current_row++) + "";
                                            expertTemplate.value = newLstExpert_old[jindex].COEX_UNIT;
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Expert name:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_detail_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = !string.IsNullOrEmpty(newLstExpert_old[jindex].COEX_SUBMITTED_BY) ? UsersDAL.getFullName(newLstExpert_old[jindex].COEX_SUBMITTED_BY) : "";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_2_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Comment date:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_detail_2_column;
                                            expertTemplate.row = (current_row++) + "";
                                            expertTemplate.value = !newLstExpert_old[jindex].COEX_SUBMIT_DATE.HasValue ? "" : newLstExpert_old[jindex].COEX_SUBMIT_DATE.Value.ToString("dd-MMM-yyyy");
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Approver name:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_detail_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = !string.IsNullOrEmpty(newLstExpert_old[jindex].COEX_APPROVED_BY) ? UsersDAL.getFullName(newLstExpert_old[jindex].COEX_APPROVED_BY) : "";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_2_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Approve date:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_detail_2_column;
                                            expertTemplate.row = (current_row++) + "";
                                            expertTemplate.value = !newLstExpert_old[jindex].COEX_APPROVE_DATE.HasValue ? "" : newLstExpert_old[jindex].COEX_APPROVE_DATE.Value.ToString("dd-MMM-yyyy");
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Process ability score:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            int index_expert_item_old = 1;
                                            COO_EXPERT_ITEMS_DAL itemDal_old = new COO_EXPERT_ITEMS_DAL();
                                            List<COO_EXPERT_ITEMS> expertItems_old = itemDal_old.GetAllByID(newLstExpert_old[jindex].COEX_ROW_ID);

                                            foreach (var expert_item_old in expertItems_old)
                                            {
                                                CoexCommentItem comment_old = new JavaScriptSerializer().Deserialize<CoexCommentItem>(expert_item_old.COEI_OP_JSON);
                                                expertTemplate = new ExpertTemplate();
                                                expertTemplate.column = data_detail_1_column;
                                                expertTemplate.row = (current_row++) + "";
                                                expertTemplate.value = index_expert_item_old + ". " + comment_old.operational_parameter;
                                                expertTemplate.font_size = "14";
                                                expertTemplates.Add(expertTemplate);

                                                expertTemplate = new ExpertTemplate();
                                                expertTemplate.column = data_detail_1_column;
                                                expertTemplate.row = (current_row++) + "";
                                                expertTemplate.value = "Score = " + comment_old.weight_score + ", Weight factor = " + comment_old.weight_factor;
                                                expertTemplate.font_size = "14";
                                                expertTemplates.Add(expertTemplate);

                                                expertTemplate = new ExpertTemplate();
                                                expertTemplate.column = data_detail_1_column;
                                                expertTemplate.row = (current_row++) + "";
                                                expertTemplate.value = "Result: " + (!string.IsNullOrEmpty(comment_old.calculation_result) ? comment_old.calculation_result : "Warning calculate no result");
                                                expertTemplate.font_size = "14";
                                                expertTemplates.Add(expertTemplate);

                                                expertTemplate = new ExpertTemplate();
                                                expertTemplate.column = data_detail_1_column;
                                                expertTemplate.row = (current_row++) + "";
                                                expertTemplate.value = "Note: " + (!string.IsNullOrEmpty(comment_old.calculation_detail) ? comment_old.calculation_detail : "Warning calculate no detail");
                                                expertTemplate.font_size = "14";
                                                expertTemplates.Add(expertTemplate);
                                                index_expert_item_old++;
                                            }

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_detail_1_column;
                                            expertTemplate.row = (current_row++) + "";
                                            expertTemplate.value = "COOL recommended score = " + newLstExpert_old[jindex].COEX_RECOMMENDED_SCORE;
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_detail_1_column;
                                            expertTemplate.row = (current_row++) + "";
                                            expertTemplate.value = "Expert recommended score = " + newLstExpert_old[jindex].COEX_AGREED_SCORE ?? newLstExpert_old[jindex].COEX_RECOMMENDED_SCORE;
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            var QUESTION_FLAG = MT_UNIT_DAL.getUnitByID(newLstExpert_old[jindex].COEX_FK_MT_UNIT).MUN_QUESTION_FLAG;
                                            if (QUESTION_FLAG == "Y")
                                            {
                                                expertTemplate = new ExpertTemplate();
                                                expertTemplate.column = data_header_1_column;
                                                expertTemplate.row = (current_row) + "";
                                                expertTemplate.value = "Can process in " + newLstExpert_old[jindex].COEX_UNIT + "?:";
                                                expertTemplate.font_bold = "Y";
                                                expertTemplate.font_size = "14";
                                                expertTemplates.Add(expertTemplate);

                                                expertTemplate = new ExpertTemplate();
                                                expertTemplate.column = data_detail_1_column;
                                                expertTemplate.row = (current_row++) + "";
                                                expertTemplate.value = newLstExpert_old[jindex].COEX_PROCESSING_FLAG;
                                                expertTemplate.font_size = "14";
                                                expertTemplates.Add(expertTemplate);
                                            }

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Comment:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplate.font_horizontal_alignment = "L";
                                            expertTemplate.font_vertical_alignment = "T";
                                            expertTemplates.Add(expertTemplate);

                                            if (!string.IsNullOrEmpty(newLstExpert_old[jindex].COEX_COMMENT_EXPERT))
                                            {
                                                string[] comments = newLstExpert_old[jindex].COEX_COMMENT_EXPERT.Split((char)13);
                                                foreach (var comment in comments)
                                                {
                                                    expertTemplate = new ExpertTemplate();
                                                    expertTemplate.column = data_detail_1_column;
                                                    expertTemplate.row = (current_row++) + "";
                                                    expertTemplate.value = comment.Trim();
                                                    expertTemplate.font_size = "14";
                                                    expertTemplate.font_horizontal_alignment = "L";
                                                    expertTemplate.font_vertical_alignment = "T";
                                                    expertTemplate.font_textwrap = "Y";
                                                    expertTemplates.Add(expertTemplate);
                                                }
                                            }

                                            expertTemplate = new ExpertTemplate();
                                            expertTemplate.column = data_header_1_column;
                                            expertTemplate.row = (current_row) + "";
                                            expertTemplate.value = "Attachment:";
                                            expertTemplate.font_bold = "Y";
                                            expertTemplate.font_size = "14";
                                            expertTemplates.Add(expertTemplate);

                                            if (newLstExpert_old[jindex].COEX_ATTACHED_FILE != null && newLstExpert_old[jindex].COEX_ATTACHED_FILE != "")
                                            {
                                                string[] ATTACHED_FILE_URL_OLD = newLstExpert_old[jindex].COEX_ATTACHED_FILE.Split('|');
                                                for (int ATTACHED_FILE_URL_INDEX = 0; ATTACHED_FILE_URL_INDEX < ATTACHED_FILE_URL_OLD.Length - 1; ATTACHED_FILE_URL_INDEX++)
                                                {
                                                    var attach_file_path = ATTACHED_FILE_URL_OLD[ATTACHED_FILE_URL_INDEX];

                                                    string[] attach_file_links = attach_file_path.Split(':');
                                                    string attach_file_link = attach_file_links[0];
                                                    string attach_file_url = string.Format("{0}{1}", root_url, attach_file_link);

                                                    string[] attach_file_names = attach_file_url.Split('/');
                                                    string attach_file_name = attach_file_names[attach_file_names.Length - 1];

                                                    expertTemplate = new ExpertTemplate();
                                                    expertTemplate.column = data_detail_1_column;
                                                    expertTemplate.row = (current_row++) + "";
                                                    expertTemplate.hyperlink = attach_file_url;
                                                    expertTemplate.text = attach_file_name;
                                                    expertTemplate.font_size = "14";
                                                    expertTemplates.Add(expertTemplate);
                                                }
                                            }
                                            else
                                            {
                                                current_row++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    expertTemplate = new ExpertTemplate();
                                    expertTemplate.column = area_header_column;
                                    expertTemplate.row = (current_row++) + "";
                                    expertTemplate.value = "No Data Comment";
                                    expertTemplate.font_bold = "N";
                                    expertTemplate.font_size = "14";
                                    expertTemplates.Add(expertTemplate);
                                }
                            }
                        }
                    }

                    // REQUESTER INFORMATION //
                    current_row++;

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = string.Format("{0}|{1}", area_header_column, data_detail_2_column);
                    expertTemplate.row = (current_row++) + "";
                    expertTemplate.border_bottom_style = "4";
                    expertTemplate.border_bottom_color = "#000000";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = area_header_column;
                    expertTemplate.row = (current_row++) + "";
                    expertTemplate.value = "Requester information:";
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "16";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_header_1_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = "Requester name:";
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_detail_1_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = !string.IsNullOrEmpty(data.CODA_REQUESTED_BY) ? UsersDAL.getFullName(data.CODA_REQUESTED_BY) : "";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_header_2_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = "Request date:";
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_detail_2_column;
                    expertTemplate.row = (current_row++) + "";
                    expertTemplate.value = !data.CODA_REQUESTED_DATE.HasValue ? "" : data.CODA_REQUESTED_DATE.Value.ToString("dd-MMM-yyyy");
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_header_1_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = "Approver name:";
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_detail_1_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = !string.IsNullOrEmpty(data.CODA_DRAFT_APPROVED_BY) ? UsersDAL.getFullName(data.CODA_DRAFT_APPROVED_BY) : "";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_header_2_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = "Approver date:";
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_detail_2_column;
                    expertTemplate.row = (current_row++) + "";
                    expertTemplate.value = !data.CODA_DRAFT_APPROVE_DATE.HasValue ? "" : data.CODA_DRAFT_APPROVE_DATE.Value.ToString("dd-MMM-yyyy");
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    //expertTemplate = new ExpertTemplate();
                    //expertTemplate.column = data_header_1_column;
                    //expertTemplate.row = (current_row) + "";
                    //expertTemplate.value = "Comment:";
                    //expertTemplate.font_bold = "Y";
                    //expertTemplate.font_size = "14";
                    //expertTemplates.Add(expertTemplate);

                    //expertTemplate = new ExpertTemplate();
                    //expertTemplate.column = data_detail_1_column;
                    //expertTemplate.row = (current_row++) + "";
                    //expertTemplate.value = !string.IsNullOrEmpty(data.CODA_COMMENT_FINAL_CAM) ? data.CODA_COMMENT_FINAL_CAM : !string.IsNullOrEmpty(data.CODA_COMMENT_DRAFT_CAM) ? data.CODA_COMMENT_DRAFT_CAM : "";
                    //expertTemplate.font_size = "14";
                    //expertTemplates.Add(expertTemplate);

                    expertTemplate = new ExpertTemplate();
                    expertTemplate.column = data_header_1_column;
                    expertTemplate.row = (current_row) + "";
                    expertTemplate.value = "Attachment:";
                    expertTemplate.font_bold = "Y";
                    expertTemplate.font_size = "14";
                    expertTemplates.Add(expertTemplate);

                    COO_SUP_DOC_DAL supDocDAL = new COO_SUP_DOC_DAL();
                    List<COO_SUP_DOC> supDoc = supDocDAL.GetAllByID(id);
                    foreach (var item in supDoc)
                    {
                        var attach_file_path = item.COSD_FILE_PATH;

                        string[] attach_file_links = attach_file_path.Split(':');
                        string attach_file_link = attach_file_links[0];
                        string attach_file_url = string.Format("{0}{1}", root_url, attach_file_link);

                        string[] attach_file_names = attach_file_url.Split('/');
                        string attach_file_name = attach_file_names[attach_file_names.Length - 1];

                        expertTemplate = new ExpertTemplate();
                        expertTemplate.column = data_detail_1_column;
                        expertTemplate.row = (current_row++) + "";
                        expertTemplate.hyperlink = attach_file_url;
                        expertTemplate.text = attach_file_name;
                        expertTemplate.font_size = "14";
                        expertTemplates.Add(expertTemplate);

                    }
                }

                // BEGIN: SET WITH COLUMN
                expertTemplate = new ExpertTemplate();
                expertTemplate.column = area_header_column;
                expertTemplate.width = area_header_width;
                expertTemplates.Add(expertTemplate);

                expertTemplate = new ExpertTemplate();
                expertTemplate.column = unit_header_column;
                expertTemplate.width = unit_header_width;
                expertTemplates.Add(expertTemplate);

                expertTemplate = new ExpertTemplate();
                expertTemplate.column = data_header_1_column;
                expertTemplate.width = data_header_1_width;
                expertTemplates.Add(expertTemplate);

                expertTemplate = new ExpertTemplate();
                expertTemplate.column = data_detail_1_column;
                expertTemplate.width = data_detail_1_width;
                expertTemplates.Add(expertTemplate);

                expertTemplate = new ExpertTemplate();
                expertTemplate.column = data_header_2_column;
                expertTemplate.width = data_header_2_width;
                expertTemplates.Add(expertTemplate);

                expertTemplate = new ExpertTemplate();
                expertTemplate.column = data_detail_2_column;
                expertTemplate.width = data_detail_2_width;
                expertTemplates.Add(expertTemplate);
                // END: SET WITH COLUMN
            }
            return expertTemplates;
        }

        public List<SummaryTemplate> getSummary(string id, int start_row = 1)
        {
            List<SummaryTemplate> summaryTemplates = new List<SummaryTemplate>();
            SummaryTemplate summaryTemplate = new SummaryTemplate();
            string summary_header_column = "A";
            string summary_detail_column = "B";
            string unit_header_column = "D";
            string unit_score_column = "E";
            string summary_header_width = "10";
            string summary_detail_width = "100";
            string unit_header_width = "20";
            string unit_score_width = "10";
            int current_row = start_row;
            int unit_score_current_row = 1;




            if (!string.IsNullOrEmpty(id))
            {
                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                COO_DATA data = dataDal.GetByID(id);
                if (data != null)
                {
                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_header_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "CRUDE ACCEPTANCE MATRIX (CAM) REPORT: " + data.CODA_CRUDE_NAME + " (" + data.CODA_ASSAY_REF_NO + ")";
                    summaryTemplate.font_bold = "Y";
                    summaryTemplates.Add(summaryTemplate);

                    current_row++;

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_header_column;
                    summaryTemplate.row = (current_row) + "";
                    summaryTemplate.value = "Summary:";
                    summaryTemplate.font_bold = "Y";
                    summaryTemplates.Add(summaryTemplate);

                    current_row++;
                    unit_score_current_row = current_row + 1;

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Crude name : " + data.CODA_CRUDE_NAME;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Country : " + data.CODA_COUNTRY;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Crude category : " + data.CODA_CRUDE_CATEGORIES;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Crude kerogen type : " + data.CODA_CRUDE_KEROGEN_TYPE;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Crude characteristic : " + data.CODA_CRUDE_CHARACTERISTIC;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Crude maturity : " + data.CODA_CRUDE_MATURITY;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.row = (current_row++) + "";
                    summaryTemplate.value = "Crude fouling possibility : " + data.CODA_FOULING_POSSIBILITY;
                    summaryTemplates.Add(summaryTemplate);

                    current_row++;

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_header_column;
                    summaryTemplate.row = (current_row) + "";
                    summaryTemplate.value = "Comment:";
                    summaryTemplate.font_bold = "Y";
                    summaryTemplates.Add(summaryTemplate);

                    current_row++;
                    current_row++;

                    if (!string.IsNullOrEmpty(data.CODA_COMMENT_FINAL_CAM))
                    {
                        string[] comments = data.CODA_COMMENT_FINAL_CAM.Split((char)13);
                        foreach (var comment in comments)
                        {
                            summaryTemplate = new SummaryTemplate();
                            summaryTemplate.column = summary_detail_column;
                            summaryTemplate.row = (current_row++) + "";
                            summaryTemplate.value = comment.Trim();
                            summaryTemplate.font_horizontal_alignment = "L";
                            summaryTemplate.font_vertical_alignment = "T";
                            summaryTemplate.font_textwrap = "Y";
                            summaryTemplates.Add(summaryTemplate);
                        }
                    }
                    else if (!string.IsNullOrEmpty(data.CODA_COMMENT_DRAFT_CAM))
                    {
                        string[] comments = data.CODA_COMMENT_DRAFT_CAM.Split((char)13);
                        foreach (var comment in comments)
                        {
                            summaryTemplate = new SummaryTemplate();
                            summaryTemplate.column = summary_detail_column;
                            summaryTemplate.row = (current_row++) + "";
                            summaryTemplate.value = comment.Trim();
                            summaryTemplate.font_horizontal_alignment = "L";
                            summaryTemplate.font_vertical_alignment = "T";
                            summaryTemplate.font_textwrap = "Y";
                            summaryTemplates.Add(summaryTemplate);
                        }
                    }

                    COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                    List<COO_EXPERT> experts = expertDal.GetAllByID(id, true);
                    experts = experts.Where(x => x.COEX_APPROVE_DATE.HasValue).ToList();
                    experts = experts.OrderByDescending(z => z.COEX_SCORE_FLAG == "Y" ? z.COEX_RECOMMENDED_SCORE : z.COEX_AGREED_SCORE).ToList();
                    if (experts != null && experts.Count > 0)
                    {

                        summaryTemplate = new SummaryTemplate();
                        summaryTemplate.column = string.Format("{0}|{1}", unit_header_column, unit_score_column);
                        summaryTemplate.row = string.Format("{0}|{1}", unit_score_current_row - 1, unit_score_current_row - 1);
                        summaryTemplate.merge = "Y";
                        summaryTemplate.value = "Process Ability Score";
                        summaryTemplate.font_bold = "Y";
                        summaryTemplate.font_horizontal_alignment = "C";
                        summaryTemplate.border_top_style = "4";
                        summaryTemplate.border_top_color = "#000000";
                        summaryTemplate.border_bottom_style = "4";
                        summaryTemplate.border_bottom_color = "#000000";
                        summaryTemplate.border_left_style = "4";
                        summaryTemplate.border_left_color = "#000000";
                        summaryTemplate.border_right_style = "4";
                        summaryTemplate.border_right_color = "#000000";
                        summaryTemplates.Add(summaryTemplate);

                        for (int j = 0; j < experts.Count; j++)
                        {
                            summaryTemplate = new SummaryTemplate();
                            summaryTemplate.column = unit_header_column;
                            summaryTemplate.row = (unit_score_current_row) + "";
                            summaryTemplate.value = experts[j].COEX_UNIT;
                            summaryTemplate.font_horizontal_alignment = "C";
                            summaryTemplate.border_top_style = "4";
                            summaryTemplate.border_top_color = "#000000";
                            summaryTemplate.border_bottom_style = "4";
                            summaryTemplate.border_bottom_color = "#000000";
                            summaryTemplate.border_left_style = "4";
                            summaryTemplate.border_left_color = "#000000";
                            summaryTemplate.border_right_style = "4";
                            summaryTemplate.border_right_color = "#000000";
                            summaryTemplates.Add(summaryTemplate);

                            summaryTemplate = new SummaryTemplate();
                            summaryTemplate.column = unit_score_column;
                            summaryTemplate.row = (unit_score_current_row++) + "";
                            summaryTemplate.value = experts[j].COEX_SCORE_FLAG == "Y" ? experts[j].COEX_RECOMMENDED_SCORE : experts[j].COEX_AGREED_SCORE;
                            summaryTemplate.font_horizontal_alignment = "C";
                            summaryTemplate.border_top_style = "4";
                            summaryTemplate.border_top_color = "#000000";
                            summaryTemplate.border_bottom_style = "4";
                            summaryTemplate.border_bottom_color = "#000000";
                            summaryTemplate.border_left_style = "4";
                            summaryTemplate.border_left_color = "#000000";
                            summaryTemplate.border_right_style = "4";
                            summaryTemplate.border_right_color = "#000000";
                            if (!string.IsNullOrEmpty(summaryTemplate.value))
                            {
                                switch (summaryTemplate.value)
                                {
                                    case "0":
                                        summaryTemplate.bg_color = "#ff0000";
                                        break;
                                    case "1":
                                        summaryTemplate.bg_color = "#ff0000";
                                        break;
                                    case "2":
                                        summaryTemplate.bg_color = "#ffff00";
                                        break;
                                    default:
                                        summaryTemplate.bg_color = "#008000";
                                        break;
                                }
                            }
                            summaryTemplates.Add(summaryTemplate);
                        }
                        //}
                        int count = 0;
                        for (int jindex = 0; jindex < experts.Count; jindex++)
                        {
                            if (experts[jindex].COEX_QUESTION_FLAG == "Y")
                            {
                                if (count == 0)
                                {
                                    unit_score_current_row = unit_score_current_row + 4;
                                    summaryTemplate = new SummaryTemplate();
                                    summaryTemplate.column = string.Format("{0}|{1}", unit_header_column, unit_score_column);
                                    summaryTemplate.row = string.Format("{0}|{1}", unit_score_current_row, unit_score_current_row);
                                    summaryTemplate.merge = "Y";
                                    unit_score_current_row++;
                                    summaryTemplate.value = "Can process?";
                                    summaryTemplate.font_bold = "Y";
                                    summaryTemplate.font_horizontal_alignment = "C";
                                    summaryTemplate.border_top_style = "4";
                                    summaryTemplate.border_top_color = "#000000";
                                    summaryTemplate.border_bottom_style = "4";
                                    summaryTemplate.border_bottom_color = "#000000";
                                    summaryTemplate.border_left_style = "4";
                                    summaryTemplate.border_left_color = "#000000";
                                    summaryTemplate.border_right_style = "4";
                                    summaryTemplate.border_right_color = "#000000";
                                    summaryTemplates.Add(summaryTemplate);
                                    count++;
                                }
                                summaryTemplate = new SummaryTemplate();
                                summaryTemplate.column = unit_header_column;
                                summaryTemplate.row = (unit_score_current_row) + "";
                                summaryTemplate.value = experts[jindex].COEX_UNIT;
                                summaryTemplate.font_horizontal_alignment = "C";
                                summaryTemplate.border_top_style = "4";
                                summaryTemplate.border_top_color = "#000000";
                                summaryTemplate.border_bottom_style = "4";
                                summaryTemplate.border_bottom_color = "#000000";
                                summaryTemplate.border_left_style = "4";
                                summaryTemplate.border_left_color = "#000000";
                                summaryTemplate.border_right_style = "4";
                                summaryTemplate.border_right_color = "#000000";
                                summaryTemplates.Add(summaryTemplate);

                                summaryTemplate = new SummaryTemplate();
                                summaryTemplate.column = unit_score_column;
                                summaryTemplate.row = (unit_score_current_row++) + "";
                                summaryTemplate.value = experts[jindex].COEX_PROCESSING_FLAG == "Y" ? "Yes" : "No";
                                summaryTemplate.font_horizontal_alignment = "C";
                                summaryTemplate.border_top_style = "4";
                                summaryTemplate.border_top_color = "#000000";
                                summaryTemplate.border_bottom_style = "4";
                                summaryTemplate.border_bottom_color = "#000000";
                                summaryTemplate.border_left_style = "4";
                                summaryTemplate.border_left_color = "#000000";
                                summaryTemplate.border_right_style = "4";
                                summaryTemplate.border_right_color = "#000000";
                                summaryTemplates.Add(summaryTemplate);
                            }
                        }
                    }

                    // BEGIN: SET WITH COLUMN
                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_header_column;
                    summaryTemplate.width = summary_header_width;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = summary_detail_column;
                    summaryTemplate.width = summary_detail_width;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = unit_header_column;
                    summaryTemplate.width = unit_header_width;
                    summaryTemplates.Add(summaryTemplate);

                    summaryTemplate = new SummaryTemplate();
                    summaryTemplate.column = unit_score_column;
                    summaryTemplate.width = unit_score_width;
                    summaryTemplates.Add(summaryTemplate);
                    // END: SET WITH COLUMN
                }
            }
            return summaryTemplates;
        }

        public List<CAMTemplate> getCAMTemplate(string id, bool isFinal = false)
        {

            if (!string.IsNullOrEmpty(id))
            {
                List<CAMTemplate> camTemplst = new List<CAMTemplate>();
                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                COO_DATA data = dataDal.GetByID(id);
                if (data != null)
                {
                    COO_CAM_DAL camDal = new COO_CAM_DAL();
                    COO_CAM cam = camDal.GetLatestByID(data.CODA_ROW_ID);
                    if (cam != null)
                    {
                        COO_SPIRAL_DAL spiralDal = new COO_SPIRAL_DAL();
                        List<COO_SPIRAL> spiral = spiralDal.GetAllSpiralsByCAMID(cam.COCA_ROW_ID);
                        if (!string.IsNullOrEmpty(cam.COCA_FK_MT_CAM_TEMP) && !string.IsNullOrEmpty(cam.COCA_FK_MT_SPEC))
                        {
                            CamTemplateServiceModel csm = new CamTemplateServiceModel();
                            CamTemplateViewModel_Detail cam_detail = csm.Get(cam.COCA_FK_MT_CAM_TEMP);
                            SpecServiceModel ssm = new SpecServiceModel();
                            SpecViewModel_Detail spec_detail = ssm.GetSpecDetail(cam.COCA_FK_MT_SPEC);
                            SpecViewModel_Detail_Compared spec_detail_compared = ssm.GetLastSpecDetail();
                            if (cam_detail != null && spec_detail != null)
                            {
                                if (cam_detail.JSON != null && spec_detail.SpecDetail != null)
                                {
                                    string spiral_header_col = cam_detail.JSON.spiral_header_column;
                                    string spiral_data_col = cam_detail.JSON.spiral_data_column;
                                    string test_method_col = cam_detail.JSON.test_method_column;
                                    string source_col = cam_detail.JSON.source_column;
                                    string constraint_col = cam_detail.JSON.constraint_column;
                                    string remarks_col = cam_detail.JSON.remarks_column;
                                    string component_header_column = cam_detail.JSON.component_header_column;

                                    string g_border_style = cam_detail.JSON.g_border_style;
                                    string g_font_color = cam_detail.JSON.g_font_color;
                                    string g_font_size = cam_detail.JSON.g_font_size;
                                    string g_freeze_row = cam_detail.JSON.g_freeze_row;
                                    string g_freeze_col = cam_detail.JSON.g_freeze_col;
                                    string g_format_number = cam_detail.JSON.g_number_format;

                                    //Freeze row and col
                                    CAMTemplate camFreeze = new CAMTemplate();
                                    camFreeze.freeze_row = cam_detail.JSON.g_freeze_row;
                                    camFreeze.freeze_col = cam_detail.JSON.g_freeze_col;
                                    camTemplst.Add(camFreeze);

                                    #region COLUMN WIDTH 
                                    if (cam_detail.JSON.ColumnWidth != null)
                                    {
                                        foreach (var item in cam_detail.JSON.ColumnWidth)
                                        {
                                            CAMTemplate camTemp = new CAMTemplate();
                                            camTemp.column = item.column;
                                            camTemp.width = item.width;
                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    #region FIX HEADER 
                                    foreach (var item in cam_detail.JSON.Fix)
                                    {
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.number_format = g_format_number;
                                        if (string.IsNullOrEmpty(item.row_to))
                                        {
                                            camTemp.row = item.row_from;
                                        }
                                        else
                                        {
                                            camTemp.row = item.row_from + "|" + item.row_to;
                                        }

                                        if (string.IsNullOrEmpty(item.col_to))
                                        {
                                            camTemp.column = item.col_from;
                                        }
                                        else
                                        {
                                            camTemp.column = item.col_from + "|" + item.col_to;
                                        }

                                        if (!string.IsNullOrEmpty(item.value) && item.value == "spec_row_version")
                                        {
                                            item.value = "Updated Date : " + spec_detail.UpdatedDate.ToString("dd-MMM-yyyy");
                                            camTemp.number_format = "#,##0";
                                        }

                                        if (!string.IsNullOrEmpty(item.value) && item.value == "spec_new_row_version")
                                        {
                                            var lastSpecVersion = ssm.GetLastSpecVersion();
                                            item.value = "Updated Date : " + lastSpecVersion;
                                            camTemp.number_format = "#,##0";
                                        }
                                        camTemp.value = item.value;
                                        camTemplst.Add(camTemp);
                                    }
                                    #endregion

                                    #region CONFIG TABLE STYLE 
                                    if (cam_detail.JSON.Config != null) //อย่าลืมกลับมาสลับตำแหน่งด้วย เอา Config ไปไว้ข้างบน
                                    {
                                        foreach (var item in cam_detail.JSON.Config.Where(x => x.bg_color != "").ToList())
                                        {
                                            CAMTemplate camTemp = new CAMTemplate();
                                            if (string.IsNullOrEmpty(item.row_to))
                                            {
                                                camTemp.row = item.row_from;
                                            }
                                            else
                                            {
                                                camTemp.row = item.row_from + "|" + item.row_to;
                                            }

                                            if (string.IsNullOrEmpty(item.col_to))
                                            {
                                                camTemp.column = item.col_from;
                                            }
                                            else
                                            {
                                                camTemp.column = item.col_from + "|" + item.col_to;
                                            }
                                            camTemp.bg_color = item.bg_color;
                                            camTemp.alignment = item.alignment;
                                            camTemp.hidden = item.hidden;
                                            //camTemp.merge = item.merge;
                                            camTemp.font_bold = item.font_bold;
                                            camTemp.font_color = item.font_color;
                                            camTemp.font_size = !string.IsNullOrEmpty(item.font_size) ? item.font_size : camTemp.font_size;
                                            camTemp.number_format = item.number_format;
                                            camTemp.indent = item.indent;
                                            camTemp.center = item.center;

                                            camTemp.border_style = item.border_style;
                                            camTemp.border_color = item.border_color;

                                            camTemp.font_italic = item.font_italic;
                                            camTemp.font_underline = item.font_underline;
                                            camTemp.font_textwrap = item.font_textwrap;
                                            camTemp.font_horizontal_alignment = item.font_horizontal_alignment;
                                            camTemp.font_vertical_alignment = item.font_vertical_alignment;
                                            camTemp.border_top_style = item.border_top_style;
                                            camTemp.border_top_color = item.border_top_color;
                                            camTemp.border_bottom_style = item.border_bottom_style;
                                            camTemp.border_bottom_color = item.border_bottom_color;
                                            camTemp.border_left_style = item.border_left_style;
                                            camTemp.border_left_color = item.border_left_color;
                                            camTemp.border_right_style = item.border_right_style;
                                            camTemp.border_right_color = item.border_right_color;

                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    #region DATA FROM COMPONANT
                                    foreach (var item in cam_detail.JSON.Spiral)
                                    {
                                        var chkNull = 0;
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.number_format = g_format_number;
                                        if (string.IsNullOrEmpty(item.Row_to))
                                        {
                                            camTemp.row = item.Row_from;
                                        }
                                        else
                                        {
                                            camTemp.row = item.Row_from + "|" + item.Row_to;
                                            camTemp.merge = "Y";
                                        }

                                        camTemp.column = component_header_column;
                                        camTemp.value = item.Component;
                                        camTemplst.Add(camTemp);

                                        if (item.ObjProperty != null && item.ObjProperty.Property.Count > 0)
                                        {
                                            //string test_method_cache = "";
                                            //string start_row = "";
                                            //string end_row = ""; 
                                            foreach (var property in item.ObjProperty.Property)
                                            {
                                                SpecDetail specDetail;
                                                //ComparedSpecDetail comparedSpecDetail;

                                                camTemp = new CAMTemplate(spiral_data_col, camTemp.bg_color, camTemp.alignment);
                                                if (string.IsNullOrEmpty(property.txtTo))
                                                {
                                                    camTemp.row = property.txtFrom;
                                                }
                                                else
                                                {
                                                    camTemp.row = property.txtFrom + "|" + property.txtTo;
                                                }

                                                if (!string.IsNullOrEmpty(property.txtUnit))
                                                {
                                                    camTemp.column = spiral_header_col;
                                                    camTemp.value = property.txtUnit;
                                                    camTemplst.Add(camTemp);

                                                    specDetail = spec_detail.SpecDetail.Where(x => x.Component.ToUpper() == item.Component.ToUpper() && x.Property == property.txtProperty && x.Unit == property.txtUnit).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    camTemp.column = spiral_header_col;
                                                    camTemp.value = property.txtProperty;
                                                    camTemplst.Add(camTemp);

                                                    specDetail = spec_detail.SpecDetail.Where(x => x.Component.ToUpper() == item.Component.ToUpper() && x.Property == property.txtProperty && string.IsNullOrEmpty(x.Unit)).FirstOrDefault();
                                                }


                                                camTemp = new CAMTemplate(camTemp.row, spiral_data_col, camTemp.bg_color, camTemp.alignment);
                                                if (!string.IsNullOrEmpty(property.txtValueExcel))
                                                {
                                                    string pattern = @"(B\d+)";
                                                    Regex regex = new Regex(pattern);
                                                    var results = regex.Matches(property.txtValueExcel);
                                                    string temptxtValueExcel = property.txtValueExcel;
                                                    if (results.Count > 0)
                                                    {
                                                        foreach (Match match in results)
                                                        {
                                                            var chkMatchNull = spiral.Any(x => x.COSP_EXCEL_POSITION == match.Value);
                                                            if (chkMatchNull)
                                                            {
                                                                string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                                value = value == null ? "" : value;
                                                                property.txtValueExcel = regex.Replace(property.txtValueExcel, value, 1);
                                                                if (value == "")
                                                                {
                                                                    chkNull++;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                string value = "";
                                                                property.txtValueExcel = regex.Replace(property.txtValueExcel, value, 1);
                                                            }

                                                        }
                                                    }


                                                    if (property.txtValueExcel == "+" || property.txtValueExcel == "-" || property.txtValueExcel == "*" || property.txtValueExcel == "/")
                                                    {
                                                        property.txtValueExcel = "";
                                                    }

                                                    if (!string.IsNullOrEmpty(property.txtValueExcel))
                                                    {
                                                        char cLastCharacter = property.txtValueExcel[property.txtValueExcel.Length - 1];
                                                        if (cLastCharacter == '+' || cLastCharacter == '-' || cLastCharacter == '*' || cLastCharacter == '/')
                                                        {
                                                            property.txtValueExcel = "";
                                                        }
                                                    }

                                                    string patternSymbol = @"([-*+/=]+)";
                                                    Regex regexSymbol = new Regex(patternSymbol);
                                                    Match matchSymbol = regexSymbol.Match(property.txtValueExcel);
                                                    if (matchSymbol.Success)
                                                    {
                                                        camTemp.formula = chkNull > 0 ? "" : property.txtValueExcel;
                                                    }
                                                    else
                                                    {
                                                        camTemp.value = property.txtValueExcel;
                                                    }
                                                    chkNull = 0;
                                                }

                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, test_method_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.TestMethod : "";
                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, source_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.Source : "";
                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, constraint_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.Constraint : "";
                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, remarks_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.Remarks : "";
                                                camTemplst.Add(camTemp);

                                                #region VALUE FROM SPEC                                                
                                                if (specDetail != null)
                                                {
                                                    if (!string.IsNullOrEmpty(cam_detail.JSON.Ranking.min_range) && !string.IsNullOrEmpty(cam_detail.JSON.Ranking.min_col))
                                                    {
                                                        int min_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Ranking.min_col);
                                                        int min_range_from = Int32.Parse(cam_detail.JSON.Ranking.min_range.Split('|')[1]);
                                                        int min_range_to = Int32.Parse(cam_detail.JSON.Ranking.min_range.Split('|')[0]);
                                                        int scope_range_from = 10;
                                                        int scope_range_to = 1;
                                                        for (int i = scope_range_from; i >= scope_range_to; i--)
                                                        {
                                                            var min_column = ShareFn.GetExcelColumnName(min_col++);
                                                            if (min_range_from >= i && i >= min_range_to)
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate(camTemp.row, camTemp.bg_color, camTemp.alignment);
                                                                camTemp2.column = min_column;
                                                                switch (i)
                                                                {
                                                                    case 10: camTemp2.value = specDetail.Min10; break;
                                                                    case 9: camTemp2.value = specDetail.Min9; break;
                                                                    case 8: camTemp2.value = specDetail.Min8; break;
                                                                    case 7: camTemp2.value = specDetail.Min7; break;
                                                                    case 6: camTemp2.value = specDetail.Min6; break;
                                                                    case 5: camTemp2.value = specDetail.Min5; break;
                                                                    case 4: camTemp2.value = specDetail.Min4; break;
                                                                    case 3: camTemp2.value = specDetail.Min3; break;
                                                                    case 2: camTemp2.value = specDetail.Min2; break;
                                                                    case 1: camTemp2.value = specDetail.Min1; break;
                                                                    default: camTemp2.value = specDetail.Min1; break;
                                                                }
                                                                if (!string.IsNullOrEmpty(camTemp2.value))
                                                                {
                                                                    DataTable dt = new DataTable();
                                                                    try
                                                                    {
                                                                        string value = !string.IsNullOrEmpty(property.txtValueExcel) ? dt.Compute(property.txtValueExcel, "").ToString() : null;
                                                                        if (Convert.ToDecimal(camTemp2.value) > Convert.ToDecimal(value))
                                                                        {
                                                                            camTemp2.bg_color = "#FFFF00";
                                                                        }
                                                                    }
                                                                    catch (Exception)
                                                                    { }
                                                                }
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                            else
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate();
                                                                camTemp2.column = min_column;
                                                                camTemp2.width = "0";
                                                                camTemp2.hidden = "Y";
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                        }
                                                    }

                                                    if (!string.IsNullOrEmpty(cam_detail.JSON.Ranking.max_range) && !string.IsNullOrEmpty(cam_detail.JSON.Ranking.max_col))
                                                    {
                                                        int max_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Ranking.max_col);
                                                        int max_range_from = Int32.Parse(cam_detail.JSON.Ranking.max_range.Split('|')[0]);
                                                        int max_range_to = Int32.Parse(cam_detail.JSON.Ranking.max_range.Split('|')[1]);
                                                        int scope_range_from = 1;
                                                        int scope_range_to = 10;
                                                        max_col = max_col + (scope_range_from - scope_range_to);
                                                        for (int i = scope_range_from; i <= scope_range_to; i++)
                                                        {
                                                            var max_column = ShareFn.GetExcelColumnName(max_col++);
                                                            if (max_range_from <= i && i <= max_range_to)
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate(camTemp.row, camTemp.bg_color, camTemp.alignment);
                                                                camTemp2.column = max_column;
                                                                switch (i)
                                                                {
                                                                    case 1: camTemp2.value = specDetail.Max1; break;
                                                                    case 2: camTemp2.value = specDetail.Max2; break;
                                                                    case 3: camTemp2.value = specDetail.Max3; break;
                                                                    case 4: camTemp2.value = specDetail.Max4; break;
                                                                    case 5: camTemp2.value = specDetail.Max5; break;
                                                                    case 6: camTemp2.value = specDetail.Max6; break;
                                                                    case 7: camTemp2.value = specDetail.Max7; break;
                                                                    case 8: camTemp2.value = specDetail.Max8; break;
                                                                    case 9: camTemp2.value = specDetail.Max9; break;
                                                                    case 10: camTemp2.value = specDetail.Max10; break;
                                                                    default: camTemp2.value = specDetail.Max10; break;
                                                                }
                                                                if (!string.IsNullOrEmpty(camTemp2.value))
                                                                {
                                                                    DataTable dt = new DataTable();
                                                                    try
                                                                    {
                                                                        string value = !string.IsNullOrEmpty(property.txtValueExcel) ? String.Format("{0:#,##0.##}", dt.Compute(property.txtValueExcel, "")) : null;
                                                                        if (Convert.ToDecimal(camTemp2.value) < Convert.ToDecimal(value))
                                                                        {
                                                                            camTemp2.bg_color = "#FFFF00";
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    { }

                                                                }
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                            else
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate();
                                                                camTemp2.column = max_column;
                                                                camTemp2.width = "0";
                                                                camTemp2.hidden = "Y";
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                                #region VALUE FROM COMPARED SPEC
                                                // CHECK SPEC COMPARE ONLY FINAL CAM
                                                if (isFinal)
                                                {
                                                    // CHECK SPEC COMPARE WHEN FOUND NEW SPEC VERSION 
                                                    if (spec_detail.RowId != spec_detail_compared.RowId)
                                                    {
                                                        ComparedSpecDetail comparedSpecDetail = new ComparedSpecDetail();
                                                        comparedSpecDetail = spec_detail_compared.ComparedSpecDetail.Where(c => c.Component.ToUpper() == item.Component.ToUpper() && c.Property == property.txtProperty && c.Unit == property.txtUnit).FirstOrDefault();
                                                        if (comparedSpecDetail != null)
                                                        {
                                                            if (!string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.min_range) && !string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.min_col))
                                                            {
                                                                int min_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Compared_Ranking.min_col);
                                                                int min_range_from = Int32.Parse(cam_detail.JSON.Compared_Ranking.min_range.Split('|')[1]);
                                                                int min_range_to = Int32.Parse(cam_detail.JSON.Compared_Ranking.min_range.Split('|')[0]);
                                                                int scope_range_from = 10;
                                                                int scope_range_to = 1;
                                                                for (int i = scope_range_from; i >= scope_range_to; i--)
                                                                {
                                                                    var min_column = ShareFn.GetExcelColumnName(min_col++);
                                                                    if (min_range_from >= i && i >= min_range_to)
                                                                    {
                                                                        CAMTemplate camTemp2 = new CAMTemplate(camTemp.row, camTemp.bg_color, camTemp.alignment);
                                                                        camTemp2.column = min_column;
                                                                        switch (i)
                                                                        {
                                                                            case 10: camTemp2.value = comparedSpecDetail.Min10; break;
                                                                            case 9: camTemp2.value = comparedSpecDetail.Min9; break;
                                                                            case 8: camTemp2.value = comparedSpecDetail.Min8; break;
                                                                            case 7: camTemp2.value = comparedSpecDetail.Min7; break;
                                                                            case 6: camTemp2.value = comparedSpecDetail.Min6; break;
                                                                            case 5: camTemp2.value = comparedSpecDetail.Min5; break;
                                                                            case 4: camTemp2.value = comparedSpecDetail.Min4; break;
                                                                            case 3: camTemp2.value = comparedSpecDetail.Min3; break;
                                                                            case 2: camTemp2.value = comparedSpecDetail.Min2; break;
                                                                            case 1: camTemp2.value = comparedSpecDetail.Min1; break;
                                                                            default: camTemp2.value = comparedSpecDetail.Min1; break;
                                                                        }
                                                                        if (!string.IsNullOrEmpty(camTemp2.value))
                                                                        {
                                                                            DataTable dt = new DataTable();
                                                                            try
                                                                            {
                                                                                string value = !string.IsNullOrEmpty(property.txtValueExcel) ? String.Format("{0:#,##0.##}", dt.Compute(property.txtValueExcel, "")) : null;
                                                                                if (Convert.ToDecimal(camTemp2.value) > Convert.ToDecimal(value))
                                                                                {
                                                                                    camTemp2.bg_color = "#FFFF00";
                                                                                }
                                                                            }
                                                                            catch (Exception ex)
                                                                            { }

                                                                        }
                                                                        camTemplst.Add(camTemp2);
                                                                    }
                                                                    else
                                                                    {
                                                                        CAMTemplate camTemp2 = new CAMTemplate();
                                                                        camTemp2.column = min_column;
                                                                        camTemp2.width = "0";
                                                                        camTemp2.hidden = "Y";
                                                                        camTemplst.Add(camTemp2);
                                                                    }
                                                                }
                                                            }

                                                            if (!string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.max_range) && !string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.max_col))
                                                            {
                                                                int max_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Compared_Ranking.max_col);
                                                                int max_range_from = Int32.Parse(cam_detail.JSON.Compared_Ranking.max_range.Split('|')[0]);
                                                                int max_range_to = Int32.Parse(cam_detail.JSON.Compared_Ranking.max_range.Split('|')[1]);
                                                                int scope_range_from = 1;
                                                                int scope_range_to = 10;
                                                                max_col = max_col + (scope_range_from - scope_range_to);
                                                                for (int i = scope_range_from; i <= scope_range_to; i++)
                                                                {
                                                                    var max_column = ShareFn.GetExcelColumnName(max_col++);
                                                                    if (max_range_from <= i && i <= max_range_to)
                                                                    {
                                                                        CAMTemplate camTemp2 = new CAMTemplate(camTemp.row, camTemp.bg_color, camTemp.alignment);
                                                                        camTemp2.column = max_column;
                                                                        switch (i)
                                                                        {
                                                                            case 1: camTemp2.value = comparedSpecDetail.Max1; break;
                                                                            case 2: camTemp2.value = comparedSpecDetail.Max2; break;
                                                                            case 3: camTemp2.value = comparedSpecDetail.Max3; break;
                                                                            case 4: camTemp2.value = comparedSpecDetail.Max4; break;
                                                                            case 5: camTemp2.value = comparedSpecDetail.Max5; break;
                                                                            case 6: camTemp2.value = comparedSpecDetail.Max6; break;
                                                                            case 7: camTemp2.value = comparedSpecDetail.Max7; break;
                                                                            case 8: camTemp2.value = comparedSpecDetail.Max8; break;
                                                                            case 9: camTemp2.value = comparedSpecDetail.Max9; break;
                                                                            case 10: camTemp2.value = comparedSpecDetail.Max10; break;
                                                                            default: camTemp2.value = comparedSpecDetail.Max10; break;
                                                                        }
                                                                        if (!string.IsNullOrEmpty(camTemp2.value))
                                                                        {
                                                                            DataTable dt = new DataTable();
                                                                            try
                                                                            {
                                                                                string value = !string.IsNullOrEmpty(property.txtValueExcel) ? String.Format("{0:#,##0.##}", dt.Compute(property.txtValueExcel, "")) : null;
                                                                                if (Convert.ToDecimal(camTemp2.value) < Convert.ToDecimal(value))
                                                                                {
                                                                                    camTemp2.bg_color = "#FFFF00";
                                                                                }
                                                                            }
                                                                            catch (Exception ex)
                                                                            { }
                                                                        }
                                                                        camTemplst.Add(camTemp2);
                                                                    }
                                                                    else
                                                                    {
                                                                        CAMTemplate camTemp2 = new CAMTemplate();
                                                                        camTemp2.column = max_column;
                                                                        camTemp2.width = "0";
                                                                        camTemp2.hidden = "Y";
                                                                        camTemplst.Add(camTemp2);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                    // NOT COMPARE SPEC WHEN EXIST SPEC VERSION
                                                    else
                                                    {
                                                        if (!string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.min_range) && !string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.min_col))
                                                        {
                                                            int min_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Compared_Ranking.min_col);
                                                            int min_range_from = Int32.Parse(cam_detail.JSON.Compared_Ranking.min_range.Split('|')[1]);
                                                            int min_range_to = Int32.Parse(cam_detail.JSON.Compared_Ranking.min_range.Split('|')[0]);
                                                            int scope_range_from = 10;
                                                            int scope_range_to = 1;
                                                            for (int i = scope_range_from; i >= scope_range_to; i--)
                                                            {
                                                                var min_column = ShareFn.GetExcelColumnName(min_col++);
                                                                CAMTemplate camTemp2 = new CAMTemplate();
                                                                camTemp2.column = min_column;
                                                                camTemp2.width = "0";
                                                                camTemp2.hidden = "Y";
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                        }

                                                        if (!string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.max_range) && !string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.max_col))
                                                        {
                                                            int max_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Compared_Ranking.max_col);
                                                            int max_range_from = Int32.Parse(cam_detail.JSON.Compared_Ranking.max_range.Split('|')[0]);
                                                            int max_range_to = Int32.Parse(cam_detail.JSON.Compared_Ranking.max_range.Split('|')[1]);
                                                            int scope_range_from = 1;
                                                            int scope_range_to = 10;
                                                            max_col = max_col + (scope_range_from - scope_range_to);
                                                            for (int i = scope_range_from; i <= scope_range_to; i++)
                                                            {
                                                                var max_column = ShareFn.GetExcelColumnName(max_col++);
                                                                CAMTemplate camTemp2 = new CAMTemplate();
                                                                camTemp2.column = max_column;
                                                                camTemp2.width = "0";
                                                                camTemp2.hidden = "Y";
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                        }

                                                    }
                                                }
                                                // NOT COMPARE SPEC WHEN DRAFT CAM
                                                else
                                                {
                                                    if (!string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.min_range) && !string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.min_col))
                                                    {
                                                        int min_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Compared_Ranking.min_col);
                                                        int min_range_from = Int32.Parse(cam_detail.JSON.Compared_Ranking.min_range.Split('|')[1]);
                                                        int min_range_to = Int32.Parse(cam_detail.JSON.Compared_Ranking.min_range.Split('|')[0]);
                                                        int scope_range_from = 10;
                                                        int scope_range_to = 1;
                                                        for (int i = scope_range_from; i >= scope_range_to; i--)
                                                        {
                                                            var min_column = ShareFn.GetExcelColumnName(min_col++);
                                                            CAMTemplate camTemp2 = new CAMTemplate();
                                                            camTemp2.column = min_column;
                                                            camTemp2.width = "0";
                                                            camTemp2.hidden = "Y";
                                                            camTemplst.Add(camTemp2);
                                                        }
                                                    }

                                                    if (!string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.max_range) && !string.IsNullOrEmpty(cam_detail.JSON.Compared_Ranking.max_col))
                                                    {
                                                        int max_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Compared_Ranking.max_col);
                                                        int max_range_from = Int32.Parse(cam_detail.JSON.Compared_Ranking.max_range.Split('|')[0]);
                                                        int max_range_to = Int32.Parse(cam_detail.JSON.Compared_Ranking.max_range.Split('|')[1]);
                                                        int scope_range_from = 1;
                                                        int scope_range_to = 10;
                                                        max_col = max_col + (scope_range_from - scope_range_to);
                                                        for (int i = scope_range_from; i <= scope_range_to; i++)
                                                        {
                                                            var max_column = ShareFn.GetExcelColumnName(max_col++);
                                                            CAMTemplate camTemp2 = new CAMTemplate();
                                                            camTemp2.column = max_column;
                                                            camTemp2.width = "0";
                                                            camTemp2.hidden = "Y";
                                                            camTemplst.Add(camTemp2);
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            //if (start_row != end_row)
                                            //{
                                            //    //add merge
                                            //    CAMTemplate merge = new CAMTemplate();
                                            //    merge.row = start_row + "|" + end_row;
                                            //    merge.column = test_method_col;
                                            //    merge.merge = "Y";
                                            //    camTemplst.Add(merge);
                                            //}
                                        }
                                    }
                                    #endregion

                                    #region REFERENCE DATA FOR COMPONENT 
                                    getCAMTemplateReferenceSpiral(ref camTemplst, cam_detail, cam.COCA_FK_MT_CAM_TEMP);
                                    #endregion

                                    #region NOTE & EXPLANATION
                                    for (int i = 0; i < spec_detail.SpecNote.Count; i++)
                                    {
                                        //Note
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.column = cam_detail.JSON.note_explan_column;
                                        camTemp.row = cam_detail.JSON.note_explan_row != null ? Int32.Parse(cam_detail.JSON.note_explan_row) + i + "" : null;
                                        camTemp.font_size = g_font_size;
                                        camTemp.value = spec_detail.SpecNote[i].Note;
                                        camTemplst.Add(camTemp);

                                        //Explanation
                                        camTemp = new CAMTemplate();
                                        camTemp.column = cam_detail.JSON.explan_from + "|" + cam_detail.JSON.explan_to;
                                        camTemp.merge = "Y";
                                        camTemp.row = cam_detail.JSON.note_explan_row != null ? Int32.Parse(cam_detail.JSON.note_explan_row) + i + "" : null;
                                        camTemp.font_size = g_font_size;
                                        camTemp.value = spec_detail.SpecNote[i].Explanation;

                                        //Image
                                        if (!string.IsNullOrEmpty(spec_detail.SpecNote[i].Image))
                                        {
                                            camTemp.image = spec_detail.SpecNote[i].Image.Split('|');
                                        }
                                        camTemplst.Add(camTemp);
                                    }
                                    #endregion

                                    #region DATA FROM INPUT 
                                    foreach (var item in cam_detail.JSON.Data)
                                    {
                                        var chkNull = 0;
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.row = item.row;
                                        camTemp.column = spiral_data_col;
                                        camTemp.alignment = "H";
                                        camTemp.number_format = g_format_number;

                                        if (item.field == "Crude Name")
                                        {
                                            camTemp.value = data.CODA_CRUDE_NAME;
                                        }
                                        else if (item.field == "Country")
                                        {
                                            camTemp.value = data.CODA_COUNTRY;
                                        }
                                        else if (item.field == "Assay Reference")
                                        {
                                            camTemp.value = data.CODA_ASSAY_DATE.HasValue ? data.CODA_ASSAY_DATE.Value.ToString("dd-MMM-yyyy") : string.Empty;
                                        }
                                        else if (item.field == "Categories")
                                        {
                                            camTemp.value = data.CODA_CRUDE_CATEGORIES;
                                        }
                                        else if (item.field == "Kerogen")
                                        {
                                            camTemp.value = data.CODA_CRUDE_KEROGEN_TYPE;
                                        }
                                        else if (item.field == "Characteristic")
                                        {
                                            camTemp.value = data.CODA_CRUDE_CHARACTERISTIC;
                                        }
                                        else if (item.field == "Maturity")
                                        {
                                            camTemp.value = data.CODA_CRUDE_MATURITY;
                                        }
                                        else if (item.field == "Fouling Possibility")
                                        {
                                            camTemp.value = data.CODA_FOULING_POSSIBILITY;
                                        }
                                        else if (item.field == "Value from Excel")
                                        {
                                            camTemp.value = item.valueFromExcel;
                                        }
                                        else if (item.field == "Formula from Excel")
                                        {
                                            string pattern = @"(B\d+)";
                                            Regex regex = new Regex(pattern);
                                            var results = regex.Matches(item.valueFromExcel);
                                            if (results.Count > 0)
                                            {
                                                string tempValueFromExcel = item.valueFromExcel;
                                                foreach (Match match in results)
                                                {
                                                    string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "1" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                    value = value == null ? "" : value;
                                                    tempValueFromExcel = regex.Replace(tempValueFromExcel, value, 1);
                                                    if (value == "1")
                                                    {
                                                        chkNull++;
                                                    }
                                                }
                                                camTemp.formula = chkNull > 0 ? "" : tempValueFromExcel;
                                            }
                                            else
                                            {
                                                camTemp.formula = item.valueFromExcel;
                                            }
                                            chkNull = 0;
                                        }
                                        else if (item.field == "Value from Spiral")
                                        {
                                            string pattern = @"(B\d+)";
                                            Regex regex = new Regex(pattern);
                                            var results = regex.Matches(item.valueFromExcel);
                                            if (results.Count > 0)
                                            {
                                                try
                                                {
                                                    string tempValueFromExcel = item.valueFromExcel;
                                                    foreach (Match match in results)
                                                    {
                                                        string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                        value = value == null ? "" : value;
                                                        tempValueFromExcel = regex.Replace(tempValueFromExcel, value, 1);
                                                        if (value == "")
                                                        {
                                                            chkNull++;
                                                        }
                                                    }
                                                    camTemp.formula = chkNull > 0 ? "" : tempValueFromExcel;
                                                }
                                                catch (Exception)
                                                {
                                                    camTemp.formula = "";
                                                }

                                                if (!string.IsNullOrEmpty(camTemp.formula))
                                                {
                                                    char cLastCharacter = camTemp.formula[camTemp.formula.Length - 1];
                                                    if (cLastCharacter == '+' || cLastCharacter == '-' || cLastCharacter == '*' || cLastCharacter == '/')
                                                    {
                                                        camTemp.formula = "";
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                camTemp.formula = item.valueFromExcel;
                                            }

                                            chkNull = 0;
                                            //COO_SPIRAL field_spiral_value = spiral.Where(x => x.COSP_EXCEL_POSITION == item.valueFromExcel).FirstOrDefault();
                                            //camTemp.value = field_spiral_value != null ? field_spiral_value.COSP_VALUE : "";
                                        }

                                        camTemplst.Add(camTemp);
                                    }
                                    #endregion

                                    #region REFERENCE DATA FROM INPUT
                                    getCAMTemplateReferenceData(ref camTemplst, cam_detail);
                                    #endregion

                                    #region COMPARE CRUDE                                    
                                    if (data.CODA_TYPE_OF_CAM != null && data.CODA_TYPE_OF_CAM == "U") //1
                                    {
                                        if (!string.IsNullOrEmpty(data.CODA_CRUDE_REFERENCE))
                                        {
                                            COO_DATA_DAL compare_dataDal = new COO_DATA_DAL();
                                            COO_DATA compare_data = compare_dataDal.GetByCrudeReference(data.CODA_CRUDE_REFERENCE);
                                            if (compare_data != null)
                                            {
                                                COO_CAM_DAL compare_camDal = new COO_CAM_DAL();
                                                COO_CAM compare_cam = camDal.GetLatestByID(compare_data.CODA_ROW_ID);
                                                if (compare_cam != null)
                                                {
                                                    COO_SPIRAL_DAL compare_spiralDal = new COO_SPIRAL_DAL();
                                                    List<COO_SPIRAL> compare_spiral = compare_spiralDal.GetAllSpiralsByCAMID(compare_cam.COCA_ROW_ID);
                                                    if (!string.IsNullOrEmpty(cam.COCA_FK_MT_CAM_TEMP) && !string.IsNullOrEmpty(compare_cam.COCA_FK_MT_SPEC))
                                                    {
                                                        CamTemplateServiceModel compare_csm = new CamTemplateServiceModel();
                                                        CamTemplateViewModel_Detail compare_cam_detail = compare_csm.Get(cam.COCA_FK_MT_CAM_TEMP);
                                                        CamTemplateViewModel_Detail self_cam_detail = csm.Get(compare_cam.COCA_FK_MT_CAM_TEMP);
                                                        SpecServiceModel compare_ssm = new SpecServiceModel();
                                                        SpecViewModel_Detail compare_spec_detail = compare_ssm.GetSpecDetail(compare_cam.COCA_FK_MT_SPEC);
                                                        SpecViewModel_Detail_Compared compare_spec_detail_compared = compare_ssm.GetLastSpecDetail();
                                                        if (compare_cam_detail != null && compare_spec_detail != null)
                                                        {
                                                            if (compare_cam_detail.JSON != null && compare_spec_detail.SpecDetail != null)
                                                            {
                                                                string compare_spiral_header_col = cam_detail.JSON.spiral_header_column;
                                                                string compare_spiral_data_col = cam_detail.JSON.compare_clude;
                                                                string compare_test_method_col = cam_detail.JSON.test_method_column;
                                                                string compare_source_col = cam_detail.JSON.source_column;
                                                                string compare_constraint_col = cam_detail.JSON.constraint_column;
                                                                string compare_remarks_col = cam_detail.JSON.remarks_column;
                                                                string compare_component_header_column = cam_detail.JSON.component_header_column;

                                                                string compare_g_border_style = cam_detail.JSON.g_border_style;
                                                                string compare_g_font_color = cam_detail.JSON.g_font_color;
                                                                string compare_g_font_size = cam_detail.JSON.g_font_size;
                                                                string compare_g_freeze_row = cam_detail.JSON.g_freeze_row;
                                                                string compare_g_freeze_col = cam_detail.JSON.g_freeze_col;
                                                                string compare_g_format_number = cam_detail.JSON.g_number_format;

                                                                #region COMPARE DATA FROM COMPONANT
                                                                foreach (var compare_item in compare_cam_detail.JSON.Spiral)
                                                                {
                                                                    CAMTemplate compare_camTemp = new CAMTemplate();
                                                                    compare_camTemp.border_style = compare_g_border_style;
                                                                    compare_camTemp.font_color = compare_g_font_color;
                                                                    compare_camTemp.font_size = compare_g_font_size;
                                                                    compare_camTemp.number_format = compare_g_format_number;
                                                                    if (string.IsNullOrEmpty(compare_item.Row_to))
                                                                    {
                                                                        compare_camTemp.row = compare_item.Row_from;
                                                                    }
                                                                    else
                                                                    {
                                                                        compare_camTemp.row = compare_item.Row_from + "|" + compare_item.Row_to;
                                                                        compare_camTemp.merge = "Y";
                                                                    }

                                                                    compare_camTemp.column = component_header_column;
                                                                    compare_camTemp.value = compare_item.Component;
                                                                    camTemplst.Add(compare_camTemp);

                                                                    if (compare_item.ObjProperty != null && compare_item.ObjProperty.Property.Count > 0)
                                                                    {
                                                                        foreach (var compare_property in compare_item.ObjProperty.Property)
                                                                        {
                                                                            SpecDetail compare_specDetail;
                                                                            compare_camTemp = new CAMTemplate(compare_spiral_data_col, compare_camTemp.bg_color, compare_camTemp.alignment);
                                                                            if (string.IsNullOrEmpty(compare_property.txtTo))
                                                                            {
                                                                                compare_camTemp.row = compare_property.txtFrom;
                                                                            }
                                                                            else
                                                                            {
                                                                                compare_camTemp.row = compare_property.txtFrom + "|" + compare_property.txtTo;
                                                                            }

                                                                            if (!string.IsNullOrEmpty(compare_property.txtUnit))
                                                                            {
                                                                                compare_camTemp.column = compare_spiral_header_col;
                                                                                compare_camTemp.value = compare_property.txtUnit;
                                                                                camTemplst.Add(compare_camTemp);

                                                                                compare_specDetail = compare_spec_detail.SpecDetail.Where(x => x.Component == compare_item.Component && x.Property == compare_property.txtProperty && x.Unit == compare_property.txtUnit).FirstOrDefault();
                                                                            }
                                                                            else
                                                                            {
                                                                                compare_camTemp.column = compare_spiral_header_col;
                                                                                compare_camTemp.value = compare_property.txtProperty;
                                                                                camTemplst.Add(compare_camTemp);

                                                                                compare_specDetail = compare_spec_detail.SpecDetail.Where(x => x.Component == compare_item.Component && x.Property == compare_property.txtProperty).FirstOrDefault();
                                                                            }


                                                                            compare_camTemp = new CAMTemplate(compare_camTemp.row, compare_spiral_data_col, compare_camTemp.bg_color, compare_camTemp.alignment);

                                                                            //if (!string.IsNullOrEmpty(compare_property.txtValueExcel))
                                                                            //{
                                                                            //    if (self_cam_detail.JSON.Spiral.Where(x => x.ObjProperty.Property.Where(y => y.txtProperty.ToUpper() == compare_property.txtProperty.ToUpper() && ((y.txtUnit ?? "").ToUpper() == (compare_property.txtUnit ?? "").ToUpper())).Count() > 0).Count() == 0)
                                                                            //    {
                                                                            //        compare_camTemp.value = "N.A.";
                                                                            //        camTemplst.Add(compare_camTemp);
                                                                            //        continue;
                                                                            //    }

                                                                            //    //if (self_cam_detail.JSON.Spiral.Where(x => x.ObjProperty.Property.Where(y => y.txtProperty.ToUpper() == property.txtProperty.ToUpper() && ((y.txtUnit ?? "").ToUpper() == (property.txtUnit ?? "").ToUpper())).Count() > 0).Count() == 0)
                                                                            //    //{
                                                                            //    //    camTemp.value = "N.A.";
                                                                            //    //    camTemplst.Add(camTemp);
                                                                            //    //    continue;
                                                                            //    //}
                                                                            //    //else
                                                                            //    //{
                                                                            //    //    CamTemplateViewModel_Spiral cam_spiral = self_cam_detail.JSON.Spiral.Where(x => x.Component == item.Component && x.ObjProperty.Property.Where(y => y.txtProperty.ToUpper() == property.txtProperty.ToUpper() && ((y.txtUnit ?? "").ToUpper() == (property.txtUnit ?? "").ToUpper())).Count() > 0).FirstOrDefault();
                                                                            //    //    string ValueExcel = cam_spiral.ObjProperty.Property.Where(x => x.txtProperty.ToUpper() == property.txtProperty.ToUpper() && ((x.txtUnit ?? "").ToUpper() == (property.txtUnit ?? "").ToUpper())).FirstOrDefault().txtValueExcel;

                                                                            //    //    if (cam_spiral != null)
                                                                            //    //    {
                                                                            //    //        temptxtValueExcel = ValueExcel;
                                                                            //    //    }
                                                                            //    //}
                                                                            //}

                                                                            if (!string.IsNullOrEmpty(compare_property.txtValueExcel))
                                                                            {
                                                                                string compare_pattern = @"(B\d+)";
                                                                                Regex compare_regex = new Regex(compare_pattern);
                                                                                string temptxtValueExcel = compare_property.txtValueExcel;


                                                                                if (self_cam_detail.JSON.Spiral.Where(x => x.ObjProperty.Property.Where(y => y.txtProperty?.ToUpper() == compare_property.txtProperty?.ToUpper() && ((y.txtUnit ?? "").ToUpper() == (compare_property.txtUnit ?? "").ToUpper())).Count() > 0).Count() == 0)
                                                                                {
                                                                                    compare_camTemp.value = "N.A.";
                                                                                    camTemplst.Add(compare_camTemp);
                                                                                    continue;
                                                                                }
                                                                                else
                                                                                {
                                                                                    CamTemplateViewModel_Spiral cam_spiral = self_cam_detail.JSON.Spiral.Where(x => x.Component == compare_item.Component && x.ObjProperty.Property.Where(y => y.txtProperty?.ToUpper() == compare_property.txtProperty.ToUpper() && ((y.txtUnit ?? "").ToUpper() == (compare_property.txtUnit ?? "").ToUpper())).Count() > 0).FirstOrDefault();

                                                                                    if (cam_spiral != null)
                                                                                    {
                                                                                        string ValueExcel = cam_spiral.ObjProperty.Property.Where(x => x.txtProperty?.ToUpper() == compare_property.txtProperty.ToUpper() && ((x.txtUnit ?? "").ToUpper() == (compare_property.txtUnit ?? "").ToUpper())).FirstOrDefault().txtValueExcel;
                                                                                        temptxtValueExcel = ValueExcel;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        compare_camTemp.value = "N.A.";
                                                                                        camTemplst.Add(compare_camTemp);
                                                                                        continue;
                                                                                    }
                                                                                }

                                                                                var compare_results = compare_regex.Matches(temptxtValueExcel ?? "");

                                                                                foreach (Match compare_match in compare_results)
                                                                                {
                                                                                    string compare_value = compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_match.Value).FirstOrDefault() == null ? "" : compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_match.Value).FirstOrDefault().COSP_VALUE;
                                                                                    compare_property.txtValueExcel = compare_regex.Replace(compare_property.txtValueExcel, compare_value, 1);
                                                                                }

                                                                                if (!string.IsNullOrEmpty(compare_property.txtValueExcel))
                                                                                {
                                                                                    char cLastCharacter = compare_property.txtValueExcel[compare_property.txtValueExcel.Length - 1];
                                                                                    if (cLastCharacter == '+' || cLastCharacter == '-' || cLastCharacter == '*' || cLastCharacter == '/')
                                                                                    {
                                                                                        compare_property.txtValueExcel = "";
                                                                                    }
                                                                                }

                                                                                string compare_patternSymbol = @"([-*+/=]+)";
                                                                                Regex compare_regexSymbol = new Regex(compare_patternSymbol);
                                                                                Match compare_matchSymbol = compare_regexSymbol.Match(compare_property.txtValueExcel);
                                                                                if (compare_matchSymbol.Success)
                                                                                {
                                                                                    compare_camTemp.formula = compare_property.txtValueExcel;
                                                                                }
                                                                                else
                                                                                {
                                                                                    compare_camTemp.value = compare_property.txtValueExcel;
                                                                                }
                                                                            }
                                                                            camTemplst.Add(compare_camTemp);
                                                                        }
                                                                    }
                                                                }
                                                                #endregion

                                                                #region COMPARE DATA FROM INPUT 
                                                                foreach (var compare_item in compare_cam_detail.JSON.Data)
                                                                {
                                                                    CAMTemplate compare_camTemp = new CAMTemplate();
                                                                    compare_camTemp.border_style = compare_g_border_style;
                                                                    compare_camTemp.font_color = compare_g_font_color;
                                                                    compare_camTemp.font_size = compare_g_font_size;
                                                                    compare_camTemp.row = compare_item.row;
                                                                    compare_camTemp.column = compare_spiral_data_col;
                                                                    compare_camTemp.alignment = "H";
                                                                    compare_camTemp.number_format = compare_g_format_number;

                                                                    if (compare_item.field == "Crude Name")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_CRUDE_NAME;
                                                                    }
                                                                    else if (compare_item.field == "Country")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_COUNTRY;
                                                                    }
                                                                    else if (compare_item.field == "Assay Reference")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_ASSAY_DATE.HasValue ? compare_data.CODA_ASSAY_DATE.Value.ToString("dd-MMM-yyyy") : string.Empty;
                                                                    }
                                                                    else if (compare_item.field == "Categories")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_CRUDE_CATEGORIES;
                                                                    }
                                                                    else if (compare_item.field == "Kerogen")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_CRUDE_KEROGEN_TYPE;
                                                                    }
                                                                    else if (compare_item.field == "Characteristic")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_CRUDE_CHARACTERISTIC;
                                                                    }
                                                                    else if (compare_item.field == "Maturity")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_CRUDE_MATURITY;
                                                                    }
                                                                    else if (compare_item.field == "Fouling Possibility")
                                                                    {
                                                                        compare_camTemp.value = compare_data.CODA_FOULING_POSSIBILITY;
                                                                    }
                                                                    else if (compare_item.field == "Value from Excel")
                                                                    {
                                                                        compare_camTemp.value = compare_item.valueFromExcel;
                                                                    }
                                                                    else if (compare_item.field == "Formula from Excel")
                                                                    {
                                                                        string compare_pattern = @"(B\d+)";
                                                                        Regex compare_regex = new Regex(compare_pattern);
                                                                        var compare_results = compare_regex.Matches(compare_item.valueFromExcel);
                                                                        if (compare_results.Count > 0)
                                                                        {
                                                                            string tempValueFromExcel = compare_item.valueFromExcel;
                                                                            foreach (Match compare_match in compare_results)
                                                                            {
                                                                                string compare_value = compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_match.Value).FirstOrDefault() == null ? "1" : compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_match.Value).FirstOrDefault().COSP_VALUE;
                                                                                tempValueFromExcel = compare_regex.Replace(tempValueFromExcel, compare_value, 1);
                                                                            }
                                                                            compare_camTemp.formula = tempValueFromExcel;
                                                                        }
                                                                        else
                                                                        {
                                                                            compare_camTemp.formula = compare_item.valueFromExcel;
                                                                        }

                                                                    }
                                                                    else if (compare_item.field == "Value from Spiral")
                                                                    {
                                                                        string compare_pattern = @"(B\d+)";
                                                                        Regex compare_regex = new Regex(compare_pattern);
                                                                        var compare_results = compare_regex.Matches(compare_item.valueFromExcel);
                                                                        if (compare_results.Count > 0)
                                                                        {
                                                                            string tempValueFromExcel = compare_item.valueFromExcel;
                                                                            foreach (Match compare_match in compare_results)
                                                                            {
                                                                                string compare_value = compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_match.Value).FirstOrDefault() == null ? "" : compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_match.Value).FirstOrDefault().COSP_VALUE;
                                                                                tempValueFromExcel = compare_regex.Replace(tempValueFromExcel, compare_value, 1);
                                                                            }
                                                                            compare_camTemp.formula = tempValueFromExcel;
                                                                        }
                                                                        else
                                                                        {
                                                                            compare_camTemp.formula = compare_item.valueFromExcel;
                                                                        }



                                                                        //COO_SPIRAL compare_field_spiral_value = compare_spiral.Where(x => x.COSP_EXCEL_POSITION == compare_item.valueFromExcel).FirstOrDefault();
                                                                        //compare_camTemp.value = compare_field_spiral_value != null ? compare_field_spiral_value.COSP_VALUE : "";
                                                                    }
                                                                    camTemplst.Add(compare_camTemp);
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.column = cam_detail.JSON.compare_clude;
                                        camTemp.width = "0";
                                        camTemp.row = null;
                                        camTemplst.Add(camTemp);
                                    }
                                    #endregion

                                    #region CONFIG TABLE STYLE 
                                    if (cam_detail.JSON.Config != null) //อย่าลืมกลับมาสลับตำแหน่งด้วย เอา Config ไปไว้ข้างบน
                                    {
                                        foreach (var item in cam_detail.JSON.Config)
                                        {
                                            CAMTemplate camTemp = new CAMTemplate();
                                            if (string.IsNullOrEmpty(item.row_to))
                                            {
                                                camTemp.row = item.row_from;
                                            }
                                            else
                                            {
                                                camTemp.row = item.row_from + "|" + item.row_to;
                                            }

                                            if (string.IsNullOrEmpty(item.col_to))
                                            {
                                                camTemp.column = item.col_from;
                                            }
                                            else
                                            {
                                                camTemp.column = item.col_from + "|" + item.col_to;
                                            }
                                            //camTemp.bg_color = item.bg_color;
                                            camTemp.alignment = item.alignment;
                                            camTemp.hidden = item.hidden;
                                            camTemp.merge = item.merge;
                                            camTemp.font_bold = item.font_bold;
                                            camTemp.font_color = item.font_color;
                                            camTemp.font_size = !string.IsNullOrEmpty(item.font_size) ? item.font_size : camTemp.font_size;
                                            camTemp.number_format = item.number_format;
                                            camTemp.indent = item.indent;
                                            camTemp.center = item.center;

                                            camTemp.border_style = item.border_style;
                                            camTemp.border_color = item.border_color;

                                            camTemp.font_italic = item.font_italic;
                                            camTemp.font_underline = item.font_underline;
                                            camTemp.font_textwrap = item.font_textwrap;
                                            camTemp.font_horizontal_alignment = item.font_horizontal_alignment;
                                            camTemp.font_vertical_alignment = item.font_vertical_alignment;
                                            camTemp.border_top_style = item.border_top_style;
                                            camTemp.border_top_color = item.border_top_color;
                                            camTemp.border_bottom_style = item.border_bottom_style;
                                            camTemp.border_bottom_color = item.border_bottom_color;
                                            camTemp.border_left_style = item.border_left_style;
                                            camTemp.border_left_color = item.border_left_color;
                                            camTemp.border_right_style = item.border_right_style;
                                            camTemp.border_right_color = item.border_right_color;

                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    return camTemplst;
                                }
                            }
                        }
                    }
                }
            }

            return null;
        }

        public List<CAMTemplate> getCAMComparisonTemplate(ref CoolCAMComparisonViewModel model)
        {
            if (model != null && model.crude.Count != 0)
            {
                // FIRST CRUDE FOR GENERATE TEMPLATE AND SPEC //
                List<CAMTemplate> camTemplst = new List<CAMTemplate>();
                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                string id = dataDal.GetByCrudeReference(model.crude.FirstOrDefault().crude_assay_ref_no).CODA_ROW_ID;
                COO_DATA data = dataDal.GetByID(id);
                if (data != null)
                {
                    COO_CAM_DAL camDal = new COO_CAM_DAL();
                    COO_CAM cam = camDal.GetLatestByID(data.CODA_ROW_ID);
                    if (cam != null)
                    {
                        COO_SPIRAL_DAL spiralDal = new COO_SPIRAL_DAL();
                        List<COO_SPIRAL> spiral = spiralDal.GetAllSpiralsByCAMID(cam.COCA_ROW_ID);
                        if (!string.IsNullOrEmpty(cam.COCA_FK_MT_CAM_TEMP) && !string.IsNullOrEmpty(cam.COCA_FK_MT_SPEC))
                        {
                            CamTemplateServiceModel csm = new CamTemplateServiceModel();
                            CamTemplateViewModel_Detail cam_detail = csm.Get(cam.COCA_FK_MT_CAM_TEMP);
                            SpecServiceModel ssm = new SpecServiceModel();
                            SpecViewModel_Detail spec_detail = ssm.GetLastSpecDetailComparision(); //.GetSpecDetail(cam.COCA_FK_MT_SPEC);
                            if (cam_detail != null)
                            {
                                if (cam_detail.JSON != null)
                                {
                                    string spiral_header_col = cam_detail.JSON.spiral_header_column;
                                    string spiral_data_col = cam_detail.JSON.spiral_data_column;
                                    string test_method_col = cam_detail.JSON.test_method_column;
                                    string source_col = cam_detail.JSON.source_column;
                                    string constraint_col = cam_detail.JSON.constraint_column;
                                    string remarks_col = cam_detail.JSON.remarks_column;
                                    string component_header_column = cam_detail.JSON.component_header_column;

                                    string g_border_style = cam_detail.JSON.g_border_style;
                                    string g_font_color = cam_detail.JSON.g_font_color;
                                    string g_font_size = cam_detail.JSON.g_font_size;
                                    string g_freeze_row = cam_detail.JSON.g_freeze_row;
                                    string g_freeze_col = cam_detail.JSON.g_freeze_col;
                                    string g_format_number = cam_detail.JSON.g_number_format;

                                    model.hide_column_start = cam_detail.JSON.spiral_data_column;
                                    model.hide_column_end = cam_detail.JSON.compare_clude;
                                    model.hide_row_start = "1";
                                    model.hide_row_end = "3";
                                    model.score_row = cam_detail.JSON.note_explan_row;
                                    model.crude_column = cam_detail.JSON.compare_clude;
                                    model.footer_column_start = cam_detail.JSON.source_column;
                                    model.footer_column_end = cam_detail.JSON.remarks_column;

                                    var ref_order = 0;
                                    var ref_country = "";
                                    var ref_crude = "";
                                    var ref_assay_no = "";
                                    var ref_column = model.crude_column;

                                    // SET COMPARISION CRUDE TO REFERENCE 
                                    cam_detail.JSON.Reference = new List<CamTemplateViewModel_Ref>();
                                    foreach (var crude in model.crude)
                                    {
                                        ref_order++;
                                        ref_country = crude.crude_country;
                                        ref_crude = crude.crude_name;
                                        ref_assay_no = crude.crude_assay_ref_no;
                                        ref_column = ShareFn.GetExcelColumnName(ShareFn.ExcelColumnNameToNumber(ref_column) + 1);
                                        cam_detail.JSON.Reference.Add(new CamTemplateViewModel_Ref()
                                        {
                                            header = ref_assay_no,
                                            order = ref_order.ToString(),
                                            country = ref_country,
                                            crude = ref_crude,
                                            col = ref_column,
                                            AssayRef = ref_assay_no
                                        });
                                    }

                                    // SET FOOTER COLUMN TO HIDE
                                    if (ShareFn.ExcelColumnNameToNumber(ref_column) > ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.remarks_column))
                                    {
                                        model.footer_column_end = "";
                                        model.footer_column_start = "";
                                    }
                                    else
                                    {
                                        model.footer_column_start = ShareFn.GetExcelColumnName(ShareFn.ExcelColumnNameToNumber(ref_column) + 1);
                                        model.footer_column_end = cam_detail.JSON.remarks_column;
                                    }

                                    //Freeze row and col
                                    CAMTemplate camFreeze = new CAMTemplate();
                                    camFreeze.freeze_row = cam_detail.JSON.g_freeze_row;
                                    camFreeze.freeze_col = cam_detail.JSON.g_freeze_col;
                                    camTemplst.Add(camFreeze);

                                    #region COLUMN WIDTH 
                                    if (cam_detail.JSON.ColumnWidth != null)
                                    {
                                        foreach (var item in cam_detail.JSON.ColumnWidth)
                                        {
                                            CAMTemplate camTemp = new CAMTemplate();
                                            camTemp.column = item.column;
                                            camTemp.width = item.width;
                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    #region CONFIG TABLE STYLE 
                                    if (cam_detail.JSON.Config != null) //อย่าลืมกลับมาสลับตำแหน่งด้วย เอา Config ไปไว้ข้างบน
                                    {
                                        foreach (var item in cam_detail.JSON.Config.Where(x => x.bg_color != "").ToList())
                                        {
                                            CAMTemplate camTemp = new CAMTemplate();
                                            if (string.IsNullOrEmpty(item.row_to))
                                            {
                                                camTemp.row = item.row_from;
                                            }
                                            else
                                            {
                                                camTemp.row = item.row_from + "|" + item.row_to;
                                            }

                                            if (string.IsNullOrEmpty(item.col_to))
                                            {
                                                camTemp.column = item.col_from;
                                            }
                                            else
                                            {
                                                camTemp.column = item.col_from + "|" + item.col_to;
                                            }
                                            camTemp.bg_color = item.bg_color;
                                            camTemp.alignment = item.alignment;
                                            camTemp.hidden = item.hidden;
                                            //camTemp.merge = item.merge;
                                            camTemp.font_bold = item.font_bold;
                                            camTemp.font_color = item.font_color;
                                            camTemp.font_size = !string.IsNullOrEmpty(item.font_size) ? item.font_size : camTemp.font_size;
                                            camTemp.number_format = item.number_format;
                                            camTemp.indent = item.indent;
                                            camTemp.center = item.center;

                                            camTemp.border_style = item.border_style;
                                            camTemp.border_color = item.border_color;

                                            camTemp.font_italic = item.font_italic;
                                            camTemp.font_underline = item.font_underline;
                                            camTemp.font_textwrap = item.font_textwrap;
                                            camTemp.font_horizontal_alignment = item.font_horizontal_alignment;
                                            camTemp.font_vertical_alignment = item.font_vertical_alignment;
                                            camTemp.border_top_style = item.border_top_style;
                                            camTemp.border_top_color = item.border_top_color;
                                            camTemp.border_bottom_style = item.border_bottom_style;
                                            camTemp.border_bottom_color = item.border_bottom_color;
                                            camTemp.border_left_style = item.border_left_style;
                                            camTemp.border_left_color = item.border_left_color;
                                            camTemp.border_right_style = item.border_right_style;
                                            camTemp.border_right_color = item.border_right_color;

                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    #region DATA FROM COMPONANT
                                    foreach (var item in cam_detail.JSON.Spiral)
                                    {
                                        var chkNull = 0;
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.number_format = g_format_number;
                                        if (string.IsNullOrEmpty(item.Row_to))
                                        {
                                            camTemp.row = item.Row_from;
                                        }
                                        else
                                        {
                                            camTemp.row = item.Row_from + "|" + item.Row_to;
                                            camTemp.merge = "Y";
                                        }

                                        camTemp.column = component_header_column;
                                        camTemp.value = item.Component;
                                        camTemplst.Add(camTemp);

                                        if (item.ObjProperty != null && item.ObjProperty.Property.Count > 0)
                                        {
                                            foreach (var property in item.ObjProperty.Property)
                                            {
                                                SpecDetail specDetail;

                                                camTemp = new CAMTemplate(spiral_data_col, camTemp.bg_color, camTemp.alignment);
                                                if (string.IsNullOrEmpty(property.txtTo))
                                                {
                                                    camTemp.row = property.txtFrom;
                                                }
                                                else
                                                {
                                                    camTemp.row = property.txtFrom + "|" + property.txtTo;
                                                }

                                                if (!string.IsNullOrEmpty(property.txtUnit))
                                                {
                                                    camTemp.column = spiral_header_col;
                                                    camTemp.value = property.txtUnit;
                                                    camTemplst.Add(camTemp);

                                                    specDetail = spec_detail.SpecDetail.Where(x => x.Component.ToUpper() == item.Component.ToUpper() && x.Property == property.txtProperty && x.Unit == property.txtUnit).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    camTemp.column = spiral_header_col;
                                                    camTemp.value = property.txtProperty;
                                                    camTemplst.Add(camTemp);

                                                    specDetail = spec_detail.SpecDetail.Where(x => x.Component.ToUpper() == item.Component.ToUpper() && x.Property == property.txtProperty && string.IsNullOrEmpty(x.Unit)).FirstOrDefault();
                                                }


                                                camTemp = new CAMTemplate(camTemp.row, spiral_data_col, camTemp.bg_color, camTemp.alignment);
                                                if (!string.IsNullOrEmpty(property.txtValueExcel))
                                                {
                                                    string pattern = @"(B\d+)";
                                                    Regex regex = new Regex(pattern);
                                                    var results = regex.Matches(property.txtValueExcel);
                                                    string temptxtValueExcel = property.txtValueExcel;
                                                    foreach (Match match in results)
                                                    {
                                                        string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                        value = value == null ? "" : value;
                                                        property.txtValueExcel = regex.Replace(property.txtValueExcel, value, 1);
                                                        if (value == "")
                                                        {
                                                            chkNull++;
                                                        }
                                                    }

                                                    string patternSymbol = @"([-*+/=]+)";
                                                    Regex regexSymbol = new Regex(patternSymbol);
                                                    Match matchSymbol = regexSymbol.Match(property.txtValueExcel);
                                                    if (matchSymbol.Success)
                                                    {
                                                        camTemp.formula = chkNull > 0 ? "" : property.txtValueExcel;
                                                    }
                                                    else
                                                    {
                                                        camTemp.value = property.txtValueExcel;
                                                    }
                                                    chkNull = 0;
                                                }

                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, test_method_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.TestMethod : "";
                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, source_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.Source : "";
                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, constraint_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.Constraint : "";
                                                camTemplst.Add(camTemp);

                                                camTemp = new CAMTemplate(camTemp.row, remarks_col, camTemp.bg_color, camTemp.alignment);
                                                camTemp.value = specDetail != null ? specDetail.Remarks : "";
                                                camTemplst.Add(camTemp);

                                                #region VALUE FROM SPEC                                                
                                                if (specDetail != null)
                                                {
                                                    if (!string.IsNullOrEmpty(cam_detail.JSON.Ranking.min_range) && !string.IsNullOrEmpty(cam_detail.JSON.Ranking.min_col))
                                                    {
                                                        int min_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Ranking.min_col);
                                                        int min_range_from = Int32.Parse(cam_detail.JSON.Ranking.min_range.Split('|')[1]);
                                                        int min_range_to = Int32.Parse(cam_detail.JSON.Ranking.min_range.Split('|')[0]);
                                                        int scope_range_from = 10;
                                                        int scope_range_to = 1;
                                                        for (int i = scope_range_from; i >= scope_range_to; i--)
                                                        {
                                                            var min_column = ShareFn.GetExcelColumnName(min_col++);
                                                            if (min_range_from >= i && i >= min_range_to)
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate(camTemp.row, camTemp.bg_color, camTemp.alignment);
                                                                camTemp2.column = min_column;
                                                                switch (i)
                                                                {
                                                                    case 10: camTemp2.value = specDetail.Min10; break;
                                                                    case 9: camTemp2.value = specDetail.Min9; break;
                                                                    case 8: camTemp2.value = specDetail.Min8; break;
                                                                    case 7: camTemp2.value = specDetail.Min7; break;
                                                                    case 6: camTemp2.value = specDetail.Min6; break;
                                                                    case 5: camTemp2.value = specDetail.Min5; break;
                                                                    case 4: camTemp2.value = specDetail.Min4; break;
                                                                    case 3: camTemp2.value = specDetail.Min3; break;
                                                                    case 2: camTemp2.value = specDetail.Min2; break;
                                                                    case 1: camTemp2.value = specDetail.Min1; break;
                                                                    default: camTemp2.value = specDetail.Min1; break;
                                                                }
                                                                if (!string.IsNullOrEmpty(camTemp2.value))
                                                                {
                                                                    try
                                                                    {
                                                                        DataTable dt = new DataTable();
                                                                        string value = !string.IsNullOrEmpty(property.txtValueExcel) ? dt.Compute(property.txtValueExcel, "").ToString() : null;
                                                                        if (Convert.ToDecimal(camTemp2.value) > Convert.ToDecimal(value))
                                                                        {
                                                                            camTemp2.bg_color = "#FFFF00";
                                                                        }
                                                                    }
                                                                    catch (Exception)
                                                                    { }

                                                                }
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                            else
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate();
                                                                camTemp2.column = min_column;
                                                                camTemp2.width = "0";
                                                                camTemp2.hidden = "Y";
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                        }
                                                    }

                                                    if (!string.IsNullOrEmpty(cam_detail.JSON.Ranking.max_range) && !string.IsNullOrEmpty(cam_detail.JSON.Ranking.max_col))
                                                    {
                                                        int max_col = ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.Ranking.max_col);
                                                        int max_range_from = Int32.Parse(cam_detail.JSON.Ranking.max_range.Split('|')[0]);
                                                        int max_range_to = Int32.Parse(cam_detail.JSON.Ranking.max_range.Split('|')[1]);
                                                        int scope_range_from = 1;
                                                        int scope_range_to = 10;
                                                        max_col = max_col + (scope_range_from - scope_range_to);
                                                        for (int i = scope_range_from; i <= scope_range_to; i++)
                                                        {
                                                            var max_column = ShareFn.GetExcelColumnName(max_col++);
                                                            if (max_range_from <= i && i <= max_range_to)
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate(camTemp.row, camTemp.bg_color, camTemp.alignment);
                                                                camTemp2.column = max_column;
                                                                switch (i)
                                                                {
                                                                    case 1: camTemp2.value = specDetail.Max1; break;
                                                                    case 2: camTemp2.value = specDetail.Max2; break;
                                                                    case 3: camTemp2.value = specDetail.Max3; break;
                                                                    case 4: camTemp2.value = specDetail.Max4; break;
                                                                    case 5: camTemp2.value = specDetail.Max5; break;
                                                                    case 6: camTemp2.value = specDetail.Max6; break;
                                                                    case 7: camTemp2.value = specDetail.Max7; break;
                                                                    case 8: camTemp2.value = specDetail.Max8; break;
                                                                    case 9: camTemp2.value = specDetail.Max9; break;
                                                                    case 10: camTemp2.value = specDetail.Max10; break;
                                                                    default: camTemp2.value = specDetail.Max10; break;
                                                                }
                                                                if (!string.IsNullOrEmpty(camTemp2.value))
                                                                {
                                                                    DataTable dt = new DataTable();
                                                                    try
                                                                    {
                                                                        string value = !string.IsNullOrEmpty(property.txtValueExcel) ? String.Format("{0:F20}", dt.Compute(property.txtValueExcel, "")) : null;
                                                                        if (Convert.ToDecimal(camTemp2.value) < Convert.ToDecimal(value))
                                                                        {
                                                                            camTemp2.bg_color = "#FFFF00";
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    { }
                                                                }
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                            else
                                                            {
                                                                CAMTemplate camTemp2 = new CAMTemplate();
                                                                camTemp2.column = max_column;
                                                                camTemp2.width = "0";
                                                                camTemp2.hidden = "Y";
                                                                camTemplst.Add(camTemp2);
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion                                                
                                            }
                                        }
                                    }
                                    #endregion

                                    #region REFERENCE DATA FOR COMPONENT 
                                    getCAMTemplateReferenceSpiral(ref camTemplst, cam_detail, cam.COCA_FK_MT_CAM_TEMP);
                                    #endregion

                                    #region DATA FROM INPUT 
                                    foreach (var item in cam_detail.JSON.Data)
                                    {
                                        var chkNull = 0;
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.row = item.row;
                                        camTemp.column = spiral_data_col;
                                        camTemp.alignment = "H";
                                        camTemp.number_format = g_format_number;

                                        if (item.field == "Crude Name")
                                        {
                                            camTemp.value = data.CODA_CRUDE_NAME;
                                        }
                                        else if (item.field == "Country")
                                        {
                                            camTemp.value = data.CODA_COUNTRY;
                                        }
                                        else if (item.field == "Assay Reference")
                                        {
                                            camTemp.value = data.CODA_ASSAY_DATE.HasValue ? data.CODA_ASSAY_DATE.Value.ToString("dd-MMM-yyyy") : string.Empty;
                                        }
                                        else if (item.field == "Categories")
                                        {
                                            camTemp.value = data.CODA_CRUDE_CATEGORIES;
                                        }
                                        else if (item.field == "Kerogen")
                                        {
                                            camTemp.value = data.CODA_CRUDE_KEROGEN_TYPE;
                                        }
                                        else if (item.field == "Characteristic")
                                        {
                                            camTemp.value = data.CODA_CRUDE_CHARACTERISTIC;
                                        }
                                        else if (item.field == "Maturity")
                                        {
                                            camTemp.value = data.CODA_CRUDE_MATURITY;
                                        }
                                        else if (item.field == "Fouling Possibility")
                                        {
                                            camTemp.value = data.CODA_FOULING_POSSIBILITY;
                                        }
                                        else if (item.field == "Value from Excel")
                                        {
                                            camTemp.value = item.valueFromExcel;
                                        }
                                        else if (item.field == "Formula from Excel")
                                        {
                                            string pattern = @"(B\d+)";
                                            Regex regex = new Regex(pattern);
                                            var results = regex.Matches(item.valueFromExcel);
                                            if (results.Count > 0)
                                            {
                                                string tempValueFromExcel = item.valueFromExcel;
                                                foreach (Match match in results)
                                                {
                                                    string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "1" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                    value = value == null ? "" : value;
                                                    tempValueFromExcel = regex.Replace(tempValueFromExcel, value, 1);
                                                    if (value == "1")
                                                    {
                                                        chkNull++;
                                                    }
                                                }
                                                camTemp.formula = chkNull > 0 ? "" : tempValueFromExcel;
                                            }
                                            else
                                            {
                                                camTemp.formula = item.valueFromExcel;
                                            }
                                            chkNull = 0;
                                        }
                                        else if (item.field == "Value from Spiral")
                                        {
                                            string pattern = @"(B\d+)";
                                            Regex regex = new Regex(pattern);
                                            var results = regex.Matches(item.valueFromExcel);
                                            if (results.Count > 0)
                                            {
                                                string tempValueFromExcel = item.valueFromExcel;
                                                foreach (Match match in results)
                                                {
                                                    string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                    value = value == null ? "" : value;
                                                    tempValueFromExcel = regex.Replace(tempValueFromExcel, value, 1);
                                                    if (value == "")
                                                    {
                                                        chkNull++;
                                                    }
                                                }
                                                camTemp.formula = chkNull > 0 ? "" : tempValueFromExcel;
                                            }
                                            else
                                            {
                                                camTemp.formula = item.valueFromExcel;
                                            }
                                            chkNull = 0;
                                            //COO_SPIRAL field_spiral_value = spiral.Where(x => x.COSP_EXCEL_POSITION == item.valueFromExcel).FirstOrDefault();
                                            //camTemp.value = field_spiral_value != null ? field_spiral_value.COSP_VALUE : "";
                                        }

                                        camTemplst.Add(camTemp);
                                    }
                                    #endregion

                                    #region REFERENCE DATA FROM INPUT                                    
                                    getCAMTemplateReferenceData(ref camTemplst, cam_detail);
                                    #endregion

                                    #region REFERENCE EXPERT SCORE                                    
                                    getCAMTemplateReferenceExpert(ref camTemplst, cam_detail);
                                    #endregion

                                    #region FIX HEADER 
                                    foreach (var item in cam_detail.JSON.Fix)
                                    {
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.number_format = g_format_number;
                                        if (string.IsNullOrEmpty(item.row_to))
                                        {
                                            camTemp.row = item.row_from;
                                        }
                                        else
                                        {
                                            camTemp.row = item.row_from + "|" + item.row_to;
                                        }

                                        if (string.IsNullOrEmpty(item.col_to))
                                        {
                                            camTemp.column = item.col_from;
                                        }
                                        else
                                        {
                                            camTemp.column = item.col_from + "|" + item.col_to;
                                        }

                                        if (!string.IsNullOrEmpty(item.value) && item.value == "spec_row_version")
                                        {
                                            item.value = "Updated Date : " + spec_detail.UpdatedDate.ToString("dd-MMM-yyyy");
                                            camTemp.number_format = "#,##0";
                                        }

                                        if (!string.IsNullOrEmpty(item.value) && item.value == "spec_new_row_version")
                                        {
                                            var lastSpecVersion = ssm.GetLastSpecVersion();
                                            item.value = "Updated Date : " + lastSpecVersion;
                                            camTemp.number_format = "#,##0";
                                        }

                                        // HIDDEN UNUSED VALUE FOR COMPARISION
                                        if (item.col_from == cam_detail.JSON.constraint_column || item.col_from == cam_detail.JSON.remarks_column || item.col_from == cam_detail.JSON.source_column)
                                        {
                                            camTemp.value = string.Empty;
                                            camTemplst.Add(camTemp);
                                        }
                                        else if (item.value == "FOR REFERENCE" || item.value == "CRUDE ACCEPTANCE MATRIX (CAM)" || item.value == "(For new crude oil or new assay)" || item.value == "Note :")
                                        {
                                            camTemp.value = string.Empty;
                                            camTemplst.Add(camTemp);
                                        }
                                        else
                                        {
                                            camTemp.value = item.value;
                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    #region CONFIG TABLE STYLE 
                                    if (cam_detail.JSON.Config != null) //อย่าลืมกลับมาสลับตำแหน่งด้วย เอา Config ไปไว้ข้างบน
                                    {
                                        foreach (var item in cam_detail.JSON.Config)
                                        {
                                            CAMTemplate camTemp = new CAMTemplate();
                                            if (string.IsNullOrEmpty(item.row_to))
                                            {
                                                camTemp.row = item.row_from;
                                            }
                                            else
                                            {
                                                camTemp.row = item.row_from + "|" + item.row_to;
                                            }

                                            if (string.IsNullOrEmpty(item.col_to))
                                            {
                                                camTemp.column = item.col_from;
                                            }
                                            else
                                            {
                                                camTemp.column = item.col_from + "|" + item.col_to;
                                            }
                                            //camTemp.bg_color = item.bg_color;
                                            camTemp.alignment = item.alignment;
                                            camTemp.hidden = item.hidden;
                                            camTemp.merge = item.merge;
                                            camTemp.font_bold = item.font_bold;
                                            camTemp.font_color = item.font_color;
                                            camTemp.font_size = !string.IsNullOrEmpty(item.font_size) ? item.font_size : camTemp.font_size;
                                            camTemp.number_format = item.number_format;
                                            camTemp.indent = item.indent;
                                            camTemp.center = item.center;

                                            camTemp.border_style = item.border_style;
                                            camTemp.border_color = item.border_color;

                                            camTemp.font_italic = item.font_italic;
                                            camTemp.font_underline = item.font_underline;
                                            camTemp.font_textwrap = item.font_textwrap;
                                            camTemp.font_horizontal_alignment = item.font_horizontal_alignment;
                                            camTemp.font_vertical_alignment = item.font_vertical_alignment;
                                            camTemp.border_top_style = item.border_top_style;
                                            camTemp.border_top_color = item.border_top_color;
                                            camTemp.border_bottom_style = item.border_bottom_style;
                                            camTemp.border_bottom_color = item.border_bottom_color;
                                            camTemp.border_left_style = item.border_left_style;
                                            camTemp.border_left_color = item.border_left_color;
                                            camTemp.border_right_style = item.border_right_style;
                                            camTemp.border_right_color = item.border_right_color;

                                            camTemplst.Add(camTemp);
                                        }
                                    }
                                    #endregion

                                    #region UNMERGE HEADER FOR CRUDE COMPARISON
                                    var header_row = cam_detail.JSON.Data.OrderBy(i => Convert.ToDecimal(i.row)).Select(i => i.row).FirstOrDefault();
                                    var num_column_start = ShareFn.ExcelColumnNameToNumber(spiral_data_col) + 1;
                                    var num_column_end = ShareFn.ExcelColumnNameToNumber(source_col) - 1;
                                    var header_column_start = ShareFn.GetExcelColumnName(num_column_start); //cam_detail.JSON.Reference.OrderBy(o => ShareFn.ExcelColumnNameToNumber(o.col)).Select(i => i.col).FirstOrDefault();
                                    var header_column_end = ShareFn.GetExcelColumnName(num_column_end); //cam_detail.JSON.Reference.OrderByDescending(o => ShareFn.ExcelColumnNameToNumber(o.col)).Select(i => i.col).FirstOrDefault();
                                    if (header_row != null && header_column_start != null && header_column_end != null)
                                    {
                                        CAMTemplate camHeader = new CAMTemplate();
                                        camHeader.row = header_row;
                                        camHeader.column = string.Format("{0}|{1}", header_column_start, header_column_end);
                                        camHeader.merge = "N";
                                        camTemplst.Add(camHeader);
                                    }
                                    for (int i = num_column_start; i <= num_column_end; i++)
                                    {
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_top_style = "4";
                                        camTemp.border_top_color = "#000000";
                                        camTemp.border_bottom_style = "4";
                                        camTemp.border_bottom_color = "#000000";
                                        camTemp.border_left_style = "4";
                                        camTemp.border_left_color = "#000000";
                                        camTemp.border_right_style = "4";
                                        camTemp.border_right_color = "#000000";
                                        camTemp.row = header_row;
                                        camTemp.column = ShareFn.GetExcelColumnName(i);
                                        camTemplst.Add(camTemp);
                                    }
                                    #endregion

                                    #region INVISIBLE FOR CRUDE COMPARISON
                                    var row_from = cam_detail.JSON.Spiral.Where(i => !string.IsNullOrEmpty(i.Row_from)).Select(i => i.Row_from).OrderByDescending(o => Convert.ToDecimal(o)).FirstOrDefault(); //.Where(i => i.ObjProperty != null).Select(obj => obj.ObjProperty.Property.Where(i => i.txtFrom != null).Select(prop => prop.txtFrom)).ToList();
                                    var row_to = cam_detail.JSON.Spiral.Where(i => !string.IsNullOrEmpty(i.Row_to)).Select(i => i.Row_to).OrderByDescending(o => Convert.ToDecimal(o)).FirstOrDefault();

                                    if (row_from != null && row_to != null)
                                    {
                                        var num_row_from = Convert.ToInt32(row_from);
                                        var num_row_to = Convert.ToInt32(row_to);

                                        if (num_row_from > num_row_to)
                                        {
                                            model.footer_row_start = "" + (num_row_from + 1);
                                        }
                                        else
                                        {
                                            model.footer_row_start = "" + (num_row_to + 1);
                                        }

                                        var footer_row = Convert.ToInt32(cam_detail.JSON.note_explan_row);
                                        model.footer_row_end = "" + (footer_row - 1);
                                    }
                                    #endregion

                                    return camTemplst;
                                }
                            }
                        }
                    }

                }
            }
            return null;
        }

        private void getCAMTemplateReferenceData(ref List<CAMTemplate> camTemplst, CamTemplateViewModel_Detail cam_detail)
        {
            camTemplst = camTemplst ?? new List<CAMTemplate>();

            string g_border_style = cam_detail.JSON.g_border_style;
            string g_font_color = cam_detail.JSON.g_font_color;
            string g_font_size = cam_detail.JSON.g_font_size;
            string g_freeze_row = cam_detail.JSON.g_freeze_row;
            string g_freeze_col = cam_detail.JSON.g_freeze_col;
            string g_format_number = cam_detail.JSON.g_number_format;
            string sprial_data_column = cam_detail.JSON.spiral_data_column;

            foreach (var cam_detail_reference in cam_detail.JSON.Reference)
            {
                if (!string.IsNullOrEmpty(cam_detail_reference.AssayRef))
                {
                    COO_DATA dataRef = COO_DATA_DAL.GetByAssayReferenceNo(cam_detail_reference.AssayRef);

                    if (dataRef != null)
                    {
                        COO_CAM_DAL camDal = new COO_CAM_DAL();
                        COO_CAM camRef = camDal.GetLatestByID(dataRef.CODA_ROW_ID);

                        if (camRef != null)
                        {

                            COO_SPIRAL_DAL spiralDal = new COO_SPIRAL_DAL();
                            List<COO_SPIRAL> spiralRef = spiralDal.GetAllSpiralsByCAMID(camRef.COCA_ROW_ID);

                            // SET HEADER TEXT FOR REFERENCE
                            if (!string.IsNullOrEmpty(cam_detail_reference.header))
                            {
                                CAMTemplate camTemp = new CAMTemplate();
                                camTemp.border_style = g_border_style;
                                camTemp.font_color = g_font_color;
                                camTemp.font_size = g_font_size;
                                camTemp.column = cam_detail_reference.col;
                                camTemp.alignment = "H";
                                camTemp.number_format = g_format_number;
                                camTemp.value = cam_detail_reference.header;
                                camTemp.row = cam_detail.JSON.Data.Select(i => i.row).OrderBy(i => Convert.ToDecimal(i)).FirstOrDefault();
                                camTemplst.Add(camTemp);
                            }

                            foreach (var cam_detail_data in cam_detail.JSON.Data)
                            {
                                var chkNull = 0;
                                CAMTemplate camTemp = new CAMTemplate();
                                camTemp.border_style = g_border_style;
                                camTemp.font_color = g_font_color;
                                camTemp.font_size = g_font_size;
                                camTemp.row = cam_detail_data.row;
                                camTemp.column = cam_detail_reference.col;
                                camTemp.alignment = "H";
                                camTemp.number_format = g_format_number;
                                if (cam_detail_data.field == "Crude Name")
                                {
                                    camTemp.value = dataRef.CODA_CRUDE_NAME;
                                    camTemp.row = string.Format("{0:0}", (Convert.ToDecimal(cam_detail_data.row) + 1));
                                }
                                else if (cam_detail_data.field == "Country")
                                {
                                    camTemp.value = dataRef.CODA_COUNTRY;
                                }
                                else if (cam_detail_data.field == "Assay Reference")
                                {
                                    camTemp.value = dataRef.CODA_ASSAY_DATE.HasValue ? dataRef.CODA_ASSAY_DATE.Value.ToString("dd-MMM-yyyy") : string.Empty;
                                }
                                else if (cam_detail_data.field == "Categories")
                                {
                                    camTemp.value = dataRef.CODA_CRUDE_CATEGORIES;
                                }
                                else if (cam_detail_data.field == "Kerogen")
                                {
                                    camTemp.value = dataRef.CODA_CRUDE_KEROGEN_TYPE;
                                }
                                else if (cam_detail_data.field == "Characteristic")
                                {
                                    camTemp.value = dataRef.CODA_CRUDE_CHARACTERISTIC;
                                }
                                else if (cam_detail_data.field == "Maturity")
                                {
                                    camTemp.value = dataRef.CODA_CRUDE_MATURITY;
                                }
                                else if (cam_detail_data.field == "Fouling Possibility")
                                {
                                    camTemp.value = dataRef.CODA_FOULING_POSSIBILITY;
                                }
                                else if (cam_detail_data.field == "Value from Excel")
                                {
                                    camTemp.value = cam_detail_data.valueFromExcel;
                                }
                                else if (cam_detail_data.field == "Formula from Excel")
                                {
                                    string pattern = @"(B\d+)";
                                    Regex regex = new Regex(pattern);
                                    var results = regex.Matches(cam_detail_data.valueFromExcel);
                                    if (results.Count > 0)
                                    {
                                        string tempValueFromExcel = cam_detail_data.valueFromExcel;
                                        foreach (Match match in results)
                                        {
                                            string value = spiralRef.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "1" : spiralRef.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                            value = value == null ? "" : value;
                                            tempValueFromExcel = regex.Replace(tempValueFromExcel, value, 1);
                                            if (value == "1")
                                            {
                                                chkNull++;
                                            }
                                        }
                                        camTemp.formula = chkNull > 0 ? "" : tempValueFromExcel;
                                    }
                                    else
                                    {
                                        camTemp.formula = cam_detail_data.valueFromExcel;
                                    }


                                    if (camTemp.formula.ToUpper().Contains(sprial_data_column.ToUpper()))
                                    {
                                        camTemp.formula = camTemp.formula.ToUpper().Replace(sprial_data_column.ToUpper(), cam_detail_reference.col.ToUpper());
                                    }
                                    chkNull = 0;
                                }
                                else if (cam_detail_data.field == "Value from Spiral")
                                {
                                    string pattern = @"(B\d+)";
                                    Regex regex = new Regex(pattern);
                                    var results = regex.Matches(cam_detail_data.valueFromExcel);
                                    if (results.Count > 0)
                                    {
                                        try
                                        {
                                            string tempValueFromExcel = cam_detail_data.valueFromExcel;
                                            foreach (Match match in results)
                                            {
                                                string value = spiralRef.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "" : spiralRef.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                value = value == null ? "" : value;
                                                tempValueFromExcel = regex.Replace(tempValueFromExcel, value, 1);
                                                if (value == "")
                                                {
                                                    chkNull++;
                                                }
                                            }
                                            camTemp.formula = chkNull > 0 ? "" : tempValueFromExcel;
                                        }
                                        catch (Exception)
                                        {
                                            camTemp.formula = "";
                                        }

                                        if (!string.IsNullOrEmpty(camTemp.formula))
                                        {
                                            char cLastCharacter = camTemp.formula[camTemp.formula.Length - 1];
                                            if (cLastCharacter == '+' || cLastCharacter == '-' || cLastCharacter == '*' || cLastCharacter == '/')
                                            {
                                                camTemp.formula = "";
                                            }
                                        }
                                        chkNull = 0;
                                    }
                                    else
                                    {
                                        camTemp.formula = cam_detail_data.valueFromExcel;
                                    }

                                    //COO_SPIRAL field_spiral_value = spiralRef.Where(x => x.COSP_EXCEL_POSITION == cam_detail_data.valueFromExcel).FirstOrDefault();
                                    //camTemp.value = field_spiral_value != null ? field_spiral_value.COSP_VALUE : "";
                                }
                                camTemplst.Add(camTemp);
                            }
                        }
                    }
                }
            }
        }

        private void getCAMTemplateReferenceSpiral(ref List<CAMTemplate> camTemplst, CamTemplateViewModel_Detail master_cam_detail, string id = "")
        {
            camTemplst = camTemplst ?? new List<CAMTemplate>();

            foreach (var master_cam_detail_reference in master_cam_detail.JSON.Reference)
            {
                string spiral_data_col = master_cam_detail_reference.col;
                COO_DATA dataRef = COO_DATA_DAL.GetByAssayReferenceNo(master_cam_detail_reference.AssayRef);
                if (dataRef != null)
                {
                    COO_CAM_DAL camDal = new COO_CAM_DAL();
                    COO_CAM cam = camDal.GetLatestByID(dataRef.CODA_ROW_ID);
                    if (cam != null)
                    {
                        COO_SPIRAL_DAL spiralDal = new COO_SPIRAL_DAL();
                        List<COO_SPIRAL> spiral = spiralDal.GetAllSpiralsByCAMID(cam.COCA_ROW_ID);
                        if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(cam.COCA_FK_MT_SPEC))
                        {
                            CamTemplateServiceModel csm = new CamTemplateServiceModel();
                            CamTemplateViewModel_Detail cam_detail = csm.Get(id);
                            CamTemplateViewModel_Detail self_cam_detail = csm.Get(cam.COCA_FK_MT_CAM_TEMP);
                            SpecServiceModel ssm = new SpecServiceModel();
                            SpecViewModel_Detail spec_detail = ssm.GetSpecDetail(cam.COCA_FK_MT_SPEC);
                            if (cam_detail != null && spec_detail != null)
                            {
                                if (cam_detail.JSON != null && spec_detail.SpecDetail != null)
                                {
                                    string spiral_header_col = cam_detail.JSON.spiral_header_column;
                                    //string spiral_data_col = cam_detail.JSON.spiral_data_column;
                                    string test_method_col = cam_detail.JSON.test_method_column;
                                    string source_col = cam_detail.JSON.source_column;
                                    string constraint_col = cam_detail.JSON.constraint_column;
                                    string remarks_col = cam_detail.JSON.remarks_column;
                                    string component_header_column = cam_detail.JSON.component_header_column;

                                    string g_border_style = cam_detail.JSON.g_border_style;
                                    string g_font_color = cam_detail.JSON.g_font_color;
                                    string g_font_size = cam_detail.JSON.g_font_size;
                                    string g_freeze_row = cam_detail.JSON.g_freeze_row;
                                    string g_freeze_col = cam_detail.JSON.g_freeze_col;
                                    string g_format_number = cam_detail.JSON.g_number_format;

                                    foreach (var item in cam_detail.JSON.Spiral)
                                    {
                                        var chkNull = 0;
                                        CAMTemplate camTemp = new CAMTemplate();
                                        camTemp.border_style = g_border_style;
                                        camTemp.font_color = g_font_color;
                                        camTemp.font_size = g_font_size;
                                        camTemp.number_format = g_format_number;

                                        if (item.ObjProperty != null && item.ObjProperty.Property.Count > 0)
                                        {
                                            foreach (var property in item.ObjProperty.Property)
                                            {
                                                camTemp = new CAMTemplate(spiral_data_col, camTemp.bg_color, camTemp.alignment);
                                                if (string.IsNullOrEmpty(property.txtTo))
                                                {
                                                    camTemp.row = property.txtFrom;
                                                }
                                                else
                                                {
                                                    camTemp.row = property.txtFrom + "|" + property.txtTo;
                                                }

                                                camTemp = new CAMTemplate(camTemp.row, spiral_data_col, camTemp.bg_color, camTemp.alignment);


                                                if (!string.IsNullOrEmpty(property.txtValueExcel))
                                                {
                                                    string pattern = @"(B\d+)";
                                                    Regex regex = new Regex(pattern);
                                                    string temptxtValueExcel = property.txtValueExcel;

                                                    if (self_cam_detail.JSON.Spiral.Where(x => x.ObjProperty.Property.Where(y => (y.txtProperty ?? "").ToUpper() == (property.txtProperty ?? "").ToUpper() && ((y.txtUnit ?? "").ToUpper() == (property.txtUnit ?? "").ToUpper())).Count() > 0).Count() == 0)
                                                    {
                                                        camTemp.value = "N.A.";
                                                        camTemplst.Add(camTemp);
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        CamTemplateViewModel_Spiral cam_spiral = self_cam_detail.JSON.Spiral.Where(x => x.Component == item.Component && x.ObjProperty.Property.Where(y => (y.txtProperty ?? "").ToUpper() == (property.txtProperty ?? "").ToUpper() && ((y.txtUnit ?? "").ToUpper() == (property.txtUnit ?? "").ToUpper())).Count() > 0).FirstOrDefault();

                                                        if (cam_spiral != null)
                                                        {
                                                            string ValueExcel = cam_spiral.ObjProperty.Property.Where(x => (x.txtProperty ?? "").ToUpper() == (property.txtProperty ?? "").ToUpper() && ((x.txtUnit ?? "").ToUpper() == (property.txtUnit ?? "").ToUpper())).FirstOrDefault().txtValueExcel;
                                                            temptxtValueExcel = ValueExcel;
                                                        }
                                                        else
                                                        {
                                                            camTemp.value = "N.A.";
                                                            camTemplst.Add(camTemp);
                                                            continue;
                                                        }
                                                    }

                                                    var results = regex.Matches(temptxtValueExcel);

                                                    if (results.Count > 0)
                                                    {
                                                        foreach (Match match in results)
                                                        {
                                                            string value = spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault() == null ? "" : spiral.Where(x => x.COSP_EXCEL_POSITION == match.Value).FirstOrDefault().COSP_VALUE;
                                                            value = value == null ? "" : value;
                                                            temptxtValueExcel = regex.Replace(temptxtValueExcel, value, 1);
                                                            if (value == "")
                                                            {
                                                                chkNull++;
                                                            }
                                                        }
                                                    }


                                                    if (temptxtValueExcel == "+" || temptxtValueExcel == "-" || temptxtValueExcel == "*" || temptxtValueExcel == "/")
                                                    {
                                                        temptxtValueExcel = "";
                                                    }

                                                    if (!string.IsNullOrEmpty(temptxtValueExcel))
                                                    {
                                                        char cLastCharacter = temptxtValueExcel[temptxtValueExcel.Length - 1];
                                                        if (cLastCharacter == '+' || cLastCharacter == '-' || cLastCharacter == '*' || cLastCharacter == '/')
                                                        {
                                                            temptxtValueExcel = "";
                                                        }
                                                    }


                                                    string patternSymbol = @"([-*+/=]+)";
                                                    Regex regexSymbol = new Regex(patternSymbol);
                                                    Match matchSymbol = regexSymbol.Match(temptxtValueExcel);
                                                    if (matchSymbol.Success)
                                                    {
                                                        camTemp.formula = chkNull > 0 ? "" : temptxtValueExcel;
                                                    }
                                                    else
                                                    {
                                                        camTemp.value = temptxtValueExcel;
                                                    }
                                                    chkNull = 0;
                                                }
                                                camTemplst.Add(camTemp);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void getCAMTemplateReferenceExpert(ref List<CAMTemplate> camTemplst, CamTemplateViewModel_Detail cam_detail)
        {
            camTemplst = camTemplst ?? new List<CAMTemplate>();

            int current_row = Convert.ToInt32(cam_detail.JSON.note_explan_row);
            string g_border_style = cam_detail.JSON.g_border_style;
            string g_font_color = cam_detail.JSON.g_font_color;
            string g_font_size = cam_detail.JSON.g_font_size;
            string g_freeze_row = cam_detail.JSON.g_freeze_row;
            string g_freeze_col = cam_detail.JSON.g_freeze_col;
            string g_format_number = cam_detail.JSON.g_number_format;
            string unit_column_start = cam_detail.JSON.note_explan_column;
            string unit_column_end = cam_detail.JSON.compare_clude;
            string score_column_end = ShareFn.GetExcelColumnName(ShareFn.ExcelColumnNameToNumber(cam_detail.JSON.source_column) - 1);

            // READ EXPERT SCORE FOR ALL CRUDE
            List<COO_EXPERT> experts = new List<COO_EXPERT>();
            foreach (var cam_detail_reference in cam_detail.JSON.Reference)
            {
                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                COO_DATA data = dataDal.GetByCrudeReference(cam_detail_reference.AssayRef);

                List<COO_EXPERT> expert = new COO_EXPERT_DAL().GetAllByID(data.CODA_ROW_ID, true);
                experts.AddRange(expert);
            }

            // SET HEADER TEXT FOR EXPERT SCORE
            current_row++;
            CAMTemplate camTemp = new CAMTemplate();
            camTemp.bg_color = "#c0c0c0";
            camTemp.border_top_style = "4";
            camTemp.border_top_color = "#000000";
            camTemp.border_bottom_style = "4";
            camTemp.border_bottom_color = "#000000";
            camTemp.border_left_style = "4";
            camTemp.border_left_color = "#000000";
            camTemp.border_right_style = "4";
            camTemp.border_right_color = "#000000";
            camTemp.column = string.Format("{0}|{1}", unit_column_start, score_column_end);
            camTemp.row = current_row + "";
            if (experts.GroupBy(g => g.COEX_UNIT).Select(i => i.Key).OrderBy(o => o).Any())
            {
                camTemplst.Add(camTemp);
            }
            else
            {
                camTemp = new CAMTemplate();
            }

            camTemp = new CAMTemplate();
            camTemp.border_top_style = "4";
            camTemp.border_top_color = "#000000";
            camTemp.border_bottom_style = "4";
            camTemp.border_bottom_color = "#000000";
            camTemp.border_left_style = "4";
            camTemp.border_left_color = "#000000";
            camTemp.border_right_style = "4";
            camTemp.border_right_color = "#000000";
            camTemp.font_color = g_font_color;
            camTemp.font_size = g_font_size;
            camTemp.merge = "Y";
            camTemp.alignment = "H";
            camTemp.font_horizontal_alignment = "L";
            camTemp.font_vertical_alignment = "T";
            camTemp.number_format = g_format_number;
            camTemp.column = string.Format("{0}|{1}", unit_column_start, unit_column_end);
            camTemp.row = current_row + "";
            camTemp.value = "Process Ability Score";
            if (experts.GroupBy(g => g.COEX_UNIT).Select(i => i.Key).OrderBy(o => o).Any())
            {
                camTemplst.Add(camTemp);
            }
            else
            {
                camTemp = new CAMTemplate();
            }


            // GROUPING EXPERT BY UNIT
            var units = experts.GroupBy(g => g.COEX_UNIT).Select(i => i.Key).OrderBy(o => o);
            foreach (var unit in units)
            {
                // SET HEADER TEXT FOR EXPERT SCORE
                current_row++;
                camTemp = new CAMTemplate();
                camTemp.border_top_style = "4";
                camTemp.border_top_color = "#000000";
                camTemp.border_bottom_style = "4";
                camTemp.border_bottom_color = "#000000";
                camTemp.border_left_style = "4";
                camTemp.border_left_color = "#000000";
                camTemp.border_right_style = "4";
                camTemp.border_right_color = "#000000";
                camTemp.font_color = g_font_color;
                camTemp.font_size = g_font_size;
                camTemp.merge = "Y";
                camTemp.alignment = "H";
                camTemp.font_horizontal_alignment = "C";
                camTemp.font_vertical_alignment = "T";
                camTemp.number_format = g_format_number;
                camTemp.column = string.Format("{0}|{1}", cam_detail.JSON.note_explan_column, cam_detail.JSON.compare_clude);
                camTemp.row = current_row + "";
                camTemp.value = unit;
                camTemplst.Add(camTemp);

                foreach (var cam_detail_reference in cam_detail.JSON.Reference)
                {
                    if (!string.IsNullOrEmpty(cam_detail_reference.AssayRef))
                    {
                        COO_DATA_DAL dataDal = new COO_DATA_DAL();
                        COO_DATA data = dataDal.GetByCrudeReference(cam_detail_reference.AssayRef);

                        List<COO_EXPERT> crude_expert = new COO_EXPERT_DAL().GetAllByID(data.CODA_ROW_ID, true);
                        COO_EXPERT unit_expert = crude_expert.Where(i => i.COEX_UNIT == unit).FirstOrDefault();

                        string unit_score = "";
                        if (unit_expert != null)
                        {
                            unit_score = unit_expert.COEX_SCORE_FLAG == "Y" ? unit_expert.COEX_RECOMMENDED_SCORE : unit_expert.COEX_AGREED_SCORE;
                        }
                        unit_score = !string.IsNullOrEmpty(unit_score) ? unit_score : "-";


                        camTemp = new CAMTemplate();
                        camTemp.border_top_style = "4";
                        camTemp.border_top_color = "#000000";
                        camTemp.border_bottom_style = "4";
                        camTemp.border_bottom_color = "#000000";
                        camTemp.border_left_style = "4";
                        camTemp.border_left_color = "#000000";
                        camTemp.border_right_style = "4";
                        camTemp.border_right_color = "#000000";
                        camTemp.font_color = g_font_color;
                        camTemp.font_size = g_font_size;
                        camTemp.alignment = "H";
                        camTemp.font_horizontal_alignment = "C";
                        camTemp.font_vertical_alignment = "T";
                        camTemp.number_format = g_format_number;
                        camTemp.column = cam_detail_reference.col;
                        camTemp.row = current_row + "";
                        camTemp.value = unit_score;
                        camTemplst.Add(camTemp);
                    }
                }
            }
        }

        private void getCAMTemplateUpdateSpecification()
        {
            COO_DATA_DAL dataDal = new COO_DATA_DAL();
            List<COO_DATA> listData = dataDal.GetByStatus(ConstantPrm.ACTION.APPROVED);
            if (listData.Any())
            {

            }
        }

        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { }
            return false;
        }
    }
}