﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CreditRatingServiceModel
    {
        public HedgeCreditRatingViewModel GetList()
        {
            HedgeCreditRatingViewModel model = new HedgeCreditRatingViewModel();
            model.creditRating = new List<CreditRating>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_CREDIT_RATING
                                 where v.MCR_STATUS == "ACTIVE"
                                 select new
                                 {
                                     dID = v.MCR_ROW_ID,
                                     dOrder = v.MCR_ORDER,
                                     dRating = v.MCR_RATING,
                                     dAmount = v.MCR_AMOUNT,
                                     dDesc = v.MCR_DESC,
                                     dStatus = v.MCR_STATUS,
                                     dCreatedBy = v.MCR_CREATED_BY,
                                     dCreatedDate = v.MCR_CREATED_DATE,
                                     dUpdatedBy = v.MCR_UPDATED_BY,
                                     dUpdatedDate = v.MCR_UPDATED_DATE
                                 }
                                 ).ToList().OrderBy(o => o.dOrder);

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            CreditRating creditRating = new CreditRating();
                            creditRating.ID = g.dID;
                            creditRating.Order = g.dOrder.ToString();
                            creditRating.Rating = g.dRating;
                            creditRating.Amount = g.dAmount;
                            creditRating.Desc = g.dDesc;
                            creditRating.Status = g.dStatus;
                            model.creditRating.Add(creditRating);
                        }
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue Edit(HedgeCreditRatingViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                MT_CREDIT_RATING_DAL dal = new MT_CREDIT_RATING_DAL();
                MT_CREDIT_RATING ent = new MT_CREDIT_RATING();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            int i = 1;
                            List<string> dataList = new List<string>();
                            List<string> dataRemoveList = new List<string>();
                            foreach (var item in pModel.creditRating)
                            {
                                if (dal.ChkExist(item.ID))
                                {
                                    ent = new MT_CREDIT_RATING();
                                    ent.MCR_ROW_ID = item.ID;
                                    ent.MCR_ORDER = i;
                                    ent.MCR_RATING = item.Rating;
                                    ent.MCR_AMOUNT = item.Amount;
                                    ent.MCR_DESC = item.Desc;
                                    ent.MCR_STATUS = CPAIConstantUtil.ACTIVE;
                                    ent.MCR_UPDATED_DATE = now;
                                    ent.MCR_UPDATED_BY = pUser;
                                    dal.Update(ent, context);
                                }
                                else
                                {
                                    ent = new MT_CREDIT_RATING();
                                    ent.MCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    ent.MCR_ORDER = i;
                                    ent.MCR_RATING = item.Rating;
                                    ent.MCR_AMOUNT = item.Amount;
                                    ent.MCR_DESC = item.Desc;
                                    ent.MCR_STATUS = CPAIConstantUtil.ACTIVE;
                                    ent.MCR_CREATED_DATE = now;
                                    ent.MCR_CREATED_BY = pUser;
                                    ent.MCR_UPDATED_DATE = now;
                                    ent.MCR_UPDATED_BY = pUser;
                                    dal.Save(ent, context);
                                }
                                dataList.Add(ent.MCR_ROW_ID);
                                i++;
                            }

                            dataRemoveList = dal.ChkInActive(dataList);

                            foreach (var dataRemove in dataRemoveList)
                            {
                                ent = new MT_CREDIT_RATING();
                                ent.MCR_ROW_ID = dataRemove;
                                ent.MCR_STATUS = CPAIConstantUtil.INACTIVE;
                                ent.MCR_UPDATED_DATE = now;
                                ent.MCR_UPDATED_BY = pUser;
                                dal.InActive(ent, context);
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public string GetCreditRating(string creditRating)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string data = context.MT_CREDIT_RATING.Where(c => c.MCR_ROW_ID == creditRating).SingleOrDefault().MCR_AMOUNT;
                return data;
            }
        }
    }
}