﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.sap.Interface.Service;
using com.pttict.sap.Interface.Model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CrudeImportPlanMemoServiceModel
    {
        public CrudeApproveFormViewModel_Detail CreateCrudeMemoRecord(CrudeApproveFormViewModel_Detail model)
        {
            try
            {
                if (model.dDetail_Header == null)
                {
                    throw new Exception("dDetail_Header is NULL");
                }

                System.Globalization.CultureInfo cu = new System.Globalization.CultureInfo("en-US");

                var pcfHeader = model.dDetail_Header[0];

                #region Material

                if (model.dDetail_Material != null)
                {
                    foreach (var item in model.dDetail_Material)
                    {
                        try
                        {
                            //Not found the existing record
                            //if (IsMemoExistingByTripNoAndItemNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO)))
                            //{
                            //    continue;
                            //}

                            if (string.IsNullOrEmpty(item.PMA_SUPPLIER))
                            {
                                continue;
                            }

                            var resultFromMtSapMemoMapping = GetMtSapMemoMapping(pcfHeader.PHE_COMPANY_CODE, "Crude/Feedstock/Product", item.PMA_SUPPLIER, item.PMA_MET_NUM);
                            if (resultFromMtSapMemoMapping == null)
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Cannot find data Crude/Feedstock/Product in MT_SAP_MEMO_MAPPING";
                                continue;
                            }

                            DateTime LoadingDateFrom;
                            DateTime LoadingDateTo;

                            if (pcfHeader.PHE_CHANNEL.ToUpper() == "TRAIN")
                            {
                                if (!string.IsNullOrEmpty(item.PMA_LOADING_DATE_FROM))
                                {
                                    string[] arrDate = item.PMA_LOADING_DATE_FROM.Replace(" to ", "|").Split('|');
                                    LoadingDateFrom = DateTime.ParseExact(arrDate[0], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    LoadingDateTo = DateTime.ParseExact(arrDate[1], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    item.RET_STS = "E";
                                    item.RET_MSG = "Loading Date Is not null!";
                                    continue;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(item.PMA_DUE_DATE))
                                {
                                    LoadingDateFrom = DateTime.ParseExact(item.PMA_DUE_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    LoadingDateTo = LoadingDateFrom;
                                }
                                else
                                {
                                    item.RET_STS = "E";
                                    item.RET_MSG = "Due Date Is not null!";
                                    continue;
                                }
                            }

                            List<MemoRecordDeleteModel> modelToDeleteList = new List<MemoRecordDeleteModel>();
                            List<MemoRecordCreateModel> modelToAddList = new List<MemoRecordCreateModel>();
                            List<MemoRecordChangeModel> modelToChangeList = new List<MemoRecordChangeModel>();

                            List<PCF_DAILY_SAP_MEMO_NO> OldMemo = getMemoByTripNoAndItemNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO));
                            if (OldMemo != null)
                            {
                                foreach (var i in OldMemo)
                                {
                                    if (!(i.LOADING_DATE >= LoadingDateFrom && i.LOADING_DATE <= LoadingDateTo))
                                    {
                                        modelToDeleteList.Add(new MemoRecordDeleteModel
                                        {
                                            IDENR = i.MEMO_NO,
                                            BUKRS = pcfHeader.PHE_COMPANY_CODE,
                                            TESTRUN = ""
                                        });
                                    }
                                }
                            }



                            MemoRecordCreateModel modelToAdd = new MemoRecordCreateModel();
                            string merkm = item.PMA_MET_NAME;
                            

                            string volume = "";
                            double unitPrice = Double.Parse(GetUnitPrice(item.PMA_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO)).ToString());
                            string currency = GetCurrency(item.PMA_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO));
                            switch (item.PMA_PURCHASE_UNIT.ToLower())
                            {
                                case "bbl":
                                    volume = item.PMA_VOLUME_BBL;
                                    break;
                                case "mt":
                                    volume = item.PMA_VOLUME_MT;
                                    break;
                                case "ml":
                                    volume = item.PMA_VOLUME_ML;
                                    break;
                                case "l15":
                                    volume = item.PMA_VOLUME_L15;
                                    break;
                                case "lton":
                                    volume = item.PMA_VOLUME_LTON;
                                    break;
                                case "kg":
                                    volume = item.PMA_VOLUME_KG;
                                    break;
                                default:
                                    volume = "0";
                                    break;
                            }

                            Double datedif = (LoadingDateFrom - LoadingDateTo).TotalDays;
                            Double wrshb = (Convert.ToDouble(volume) * unitPrice); // / DateTime.DaysInMonth(Convert.ToDateTime(item.PMA_LOADING_DATE_FROM).Year, Convert.ToDateTime(item.PMA_LOADING_DATE_FROM).Month);
                            if (datedif != 0)
                                wrshb = (wrshb / datedif);

                            wrshb = wrshb * -1;

                            wrshb = Convert.ToDouble(wrshb.ToString("0.##")); //Keep only 2 digit because too many digit causes the crash when send to SAP


                            string sgtxt = GetVendorShortName(item.PMA_SUPPLIER) + "/" + merkm + "/" + pcfHeader.PHE_TRIP_NO;
                            if (sgtxt.Length > 40)
                                sgtxt = sgtxt.Substring(0, 40);



                            string planningDate = "";
                            int expiryDays = GetExpirayDays(item.PMA_SUPPLIER);

                            for (Double i = 0; i <= datedif; i++)
                            {
                                DateTime poDate = LoadingDateFrom.AddDays(i);
                                string datum = poDate.ToString("yyyy-MM-dd", cu);

                                if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                                {
                                    planningDate = poDate.AddDays(expiryDays).ToString("yyyy-MM-dd", cu);
                                }
                                else //If not found, add 3 months to expiry days
                                {
                                    planningDate = poDate.AddMonths(3).ToString("yyyy-MM-dd", cu);
                                }

                                var queryCheck = OldMemo.Where(p => p.LOADING_DATE.Equals(poDate));
                                if (queryCheck != null & queryCheck.ToList().Count > 0)
                                {
                                    modelToChangeList.Add(new MemoRecordChangeModel
                                    {
                                        AVDAT = String.IsNullOrEmpty(planningDate) ? "" : planningDate,
                                        BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE,
                                        DISPW = String.IsNullOrEmpty(currency) ? "" : currency,
                                        DATUM = String.IsNullOrEmpty(datum) ? "" : datum,
                                        DMSHB = (decimal)0,// dmshb,
                                        DMSHBSpecified = true,
                                        GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup,
                                        GVALT = datum, //(day == null) ? "" : day.ToString("yyyy-MM-dd"),
                                        IDENR = queryCheck.ToList()[0].MEMO_NO,
                                        KURST = "M",
                                        MERKM = String.IsNullOrEmpty(merkm) ? "" : merkm,
                                        REFER = String.IsNullOrEmpty(item.PMA_SUPPLIER) ? "" : item.PMA_SUPPLIER,
                                        SGTXT = String.IsNullOrEmpty(sgtxt) ? "" : sgtxt,
                                        TESTRUN = "",
                                        VOART = "",
                                        WRSHB = Decimal.Parse(wrshb.ToString()),
                                        WRSHBSpecified = true,
                                        XINVR = (currency.ToUpper() == "THB") ? "X" : "",
                                        ZUONR = String.IsNullOrEmpty(item.PMA_TRIP_NO) ? "" : item.PMA_TRIP_NO,
                                    });

                                }
                                else
                                {
                                    modelToAddList.Add(new MemoRecordCreateModel
                                    {
                                        AVDAT = String.IsNullOrEmpty(planningDate) ? "" : planningDate,
                                        BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE,
                                        DSART = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningType) ? "" : resultFromMtSapMemoMapping.PlanningType,
                                        DISPW = currency, // String.IsNullOrEmpty(currency) ? "" : currency;
                                        DATUM = datum, // String.IsNullOrEmpty(datum) ? "" : datum;
                                        GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup,
                                        GVALT = datum, // String.IsNullOrEmpty(item.PMA_LOADING_DATE_FROM) ? "" : Convert.ToDateTime(item.PMA_LOADING_DATE_FROM).ToString("yyyy-MM-dd");
                                        KURST = "M",
                                        MERKM = merkm, // String.IsNullOrEmpty(merkm) ? "" : merkm;
                                        REFER = String.IsNullOrEmpty(item.PMA_SUPPLIER) ? "" : item.PMA_SUPPLIER,
                                        SGTXT = sgtxt, // String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                                        TESTRUN = "",
                                        VOART = "",
                                        WRSHB = Decimal.Parse(wrshb.ToString()),
                                        XINVR = (currency.ToUpper() == "THB") ? "X" : "",
                                        ZUONR = String.IsNullOrEmpty(item.PMA_TRIP_NO) ? "" : item.PMA_TRIP_NO,
                                    });
                                }

                            }

                            string RET_MSG = "";
                            string RET_STS = "";
                            string PMA_MEMO_NO = "";
                            string tripno = pcfHeader.PHE_TRIP_NO;
                            string itemno = item.PMA_ITEM_NO;

                            DeleteMemoList(modelToDeleteList, tripno, itemno, ref RET_STS, ref RET_MSG);
                            if (RET_STS.Equals("E"))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = RET_MSG;
                            }


                            CreateMemoList(modelToAddList, tripno, itemno, ref PMA_MEMO_NO, ref RET_STS, ref RET_MSG);
                            if (RET_STS.Equals("E"))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = RET_MSG;
                            }

                            UpdateMemoList(modelToChangeList, tripno, itemno, ref PMA_MEMO_NO, ref RET_STS, ref RET_MSG);
                            if (RET_STS.Equals("E"))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = RET_MSG;
                            }

                            try
                            {
                                var qi = model.dDetail.Where(p => p.h_TripNo.Equals(tripno) && p.MatItemNo.Equals(itemno));
                                if (qi != null && qi.ToList().Count > 0)
                                {
                                    foreach (var ii in qi)
                                    {
                                        ii.MemoNo = PMA_MEMO_NO;
                                    }
                                }

                            }
                            catch (Exception ex)
                            {

                            }

                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    } //Next Item
                }

                #endregion

                #region Freight

                if (model.dDetail_Freight != null)
                {
                    foreach (var item in model.dDetail_Freight)
                    {
                        try
                        {
                            //กรณี Time Charter ไม่ต้องส่งค่า Freight ไปสร้าง Memo Record ในระบบ SAP
                            if ((item.FR_CharteringType ?? "") == "T")
                                continue;

                                string sOwner = string.IsNullOrEmpty(item.FR_Owner) ? "" : item.FR_Owner;
                            if (sOwner.Equals(""))
                                continue;

                            if (string.IsNullOrEmpty(item.FR_DueDate))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Due Date Is not null !";
                                continue;
                            }

                            var resultFromMtSapMemoMapping = GetMtSapMemoMapping(pcfHeader.PHE_COMPANY_CODE, "Freight", item.FR_Owner, "");
                            if (resultFromMtSapMemoMapping == null)
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Cannot find data Freight in MT_SAP_MEMO_MAPPING";
                                continue;
                            }

                            MemoRecordCreateModel modelToAdd = new MemoRecordCreateModel();
                            string merkm = "Freight";
                             

                            decimal unitPrice = Convert.ToDecimal(string.IsNullOrEmpty(item.FR_PriceUnit) ? "0" : item.FR_PriceUnit.Replace(",", ""));
                            string currency = string.IsNullOrEmpty(item.FR_AmtUnit) ? "" : item.FR_AmtUnit;

                            decimal wrshb = Convert.ToDecimal(string.IsNullOrEmpty(item.FR_Amt) ? "0" : item.FR_Amt.Replace(",", ""));
                            wrshb = Convert.ToDecimal((wrshb * -1).ToString("0.##"));

                            var dueDate = DateTime.ParseExact(item.FR_DueDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //Convert.ToDateTime(item.FR_DueDate);
                            int lastDayInMonth = DateTime.DaysInMonth(dueDate.Year, dueDate.Month);
                            string datum = dueDate.ToString("yyyy-MM-dd", cu);
                            string planningDate = "";
                            int expiryDays = GetExpirayDays(item.FR_Owner);
                            if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                            {
                                planningDate = dueDate.AddDays(expiryDays).ToString("yyyy-MM-dd", cu);
                            }
                            else //If not found, add 3 months to expiry days
                            {
                                planningDate = dueDate.AddMonths(3).ToString("yyyy-MM-dd", cu);
                            }
                            string sgtxt = GetVendorShortName(item.FR_Owner) + "/" + merkm;
                            if (sgtxt.Length > 40)
                                sgtxt = sgtxt.Substring(0, 40);


                            modelToAdd.AVDAT = String.IsNullOrEmpty(planningDate) ? "" : planningDate;
                            modelToAdd.BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                            modelToAdd.DSART = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningType) ? "" : resultFromMtSapMemoMapping.PlanningType;
                            modelToAdd.DISPW = currency;// String.IsNullOrEmpty(currency) ? "" : currency;                           
                            modelToAdd.DATUM = datum; // String.IsNullOrEmpty(datum) ? "" : datum;
                            modelToAdd.GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup;
                            modelToAdd.GVALT = datum; // String.IsNullOrEmpty(item.FR_DueDate.ToString()) ? "" : Convert.ToDateTime(item.FR_DueDate).ToString("yyyy-MM-dd");
                            modelToAdd.KURST = "M";
                            modelToAdd.MERKM = merkm; // String.IsNullOrEmpty(merkm) ? "" : merkm;
                            modelToAdd.REFER = sOwner; // String.IsNullOrEmpty(item.FR_Owner) ? "" : item.FR_Owner;
                            modelToAdd.SGTXT = sgtxt; // String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                            modelToAdd.TESTRUN = "";
                            modelToAdd.VOART = "";
                            modelToAdd.WRSHB = wrshb;
                            modelToAdd.XINVR = (currency.ToUpper() == "THB") ? "X" : "";
                            modelToAdd.ZUONR = String.IsNullOrEmpty(item.FR_TripNo) ? "" : item.FR_TripNo;

                            MemoRecordCreateServiceConnectorImpl MRService = new MemoRecordCreateServiceConnectorImpl();
                            MemoRecordService service = new MemoRecordService();
                            ConfigManagement configManagement = new ConfigManagement();
                            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CREATE");
                            var javaScriptSerializer = new JavaScriptSerializer();
                            string content = javaScriptSerializer.Serialize(modelToAdd);

                            var result = MRService.connect(jsonConfig, content);

                            if (!(result == null) || (result.TYPE.ToUpper() != "S")) //No error when send to SAP
                            {
                                UpdateMemoFreight(item.FR_TripNo, item.FR_ItemNo, result.MESSAGE_V1);
                                item.RET_STS = "S";
                                item.FR_MemoNo = result.MESSAGE_V1;
                                item.RET_MSG = "";

                            }
                            else //Error when send to SAP
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = result.MESSAGE + ", " + result.MESSAGE_V1;
                            }

                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    } //Next Item
                }

                #endregion

                #region Other

                if (model.dDetail_Other != null)
                {
                    foreach (var item in model.dDetail_Other)
                    {
                        try
                        {

                            string sOwner = string.IsNullOrEmpty(item.OC_Vendor) ? "" : item.OC_Vendor;
                            if (sOwner.Equals(""))
                                continue;

                            if (string.IsNullOrEmpty(item.OC_DueDate))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Due Date Is not null !";
                                continue;
                            }

                            var resultFromMtSapMemoMapping = GetMtSapMemoMapping(pcfHeader.PHE_COMPANY_CODE, "OtherExpense", item.OC_Vendor, "");
                            if (resultFromMtSapMemoMapping == null)
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Cannot find data OtherExpense in MT_SAP_MEMO_MAPPING";
                                continue;
                            }

                            MemoRecordCreateModel modelToAdd = new MemoRecordCreateModel();
                            string merkm = "OtherExpense";

                            decimal unitPrice = Convert.ToDecimal(string.IsNullOrEmpty(item.OC_Total) ? "0" : item.OC_Total.Replace(",", ""));
                            string currency = item.OC_AmountUnit;

                            decimal wrshb = Convert.ToDecimal(string.IsNullOrEmpty(item.OC_Amount) ? "0" : item.OC_Amount.Replace(",", ""));
                            wrshb = Convert.ToDecimal((wrshb * -1).ToString("0.##"));

                            var dueDate = DateTime.ParseExact(item.OC_DueDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); // Convert.ToDateTime(item.OC_DueDate);
                            int lastDayInMonth = DateTime.DaysInMonth(dueDate.Year, dueDate.Month);
                            string datum = dueDate.ToString("yyyy-MM-dd", cu);

                            string planningDate = "";
                            int expiryDays = GetExpirayDays(item.OC_Vendor);
                            if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                            {
                                planningDate = dueDate.AddDays(expiryDays).ToString("yyyy-MM-dd", cu);
                            }
                            else //If not found, add 3 months to expiry days
                            {
                                planningDate = dueDate.AddMonths(3).ToString("yyyy-MM-dd", cu);
                            }

                            string sgtxt = GetVendorShortName(item.OC_Vendor) + "/" + merkm;
                            if (sgtxt.Length > 40)
                                sgtxt = sgtxt.Substring(0, 40);

                            modelToAdd.AVDAT = String.IsNullOrEmpty(planningDate) ? "" : planningDate;
                            modelToAdd.BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                            modelToAdd.DSART = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningType) ? "" : resultFromMtSapMemoMapping.PlanningType;
                            modelToAdd.DISPW = String.IsNullOrEmpty(currency) ? "" : currency;
                            modelToAdd.DATUM = String.IsNullOrEmpty(datum) ? "" : datum;
                            modelToAdd.GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup;
                            modelToAdd.GVALT = String.IsNullOrEmpty(item.OC_DueDate.ToString()) ? "" : Convert.ToDateTime(item.OC_DueDate).ToString("yyyy-MM-dd");
                            modelToAdd.KURST = "M";
                            modelToAdd.MERKM = String.IsNullOrEmpty(merkm) ? "" : merkm;
                            modelToAdd.REFER = String.IsNullOrEmpty(item.OC_DueDate) ? "" : item.OC_DueDate;
                            modelToAdd.SGTXT = String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                            modelToAdd.TESTRUN = "";
                            modelToAdd.VOART = "";
                            modelToAdd.WRSHB = wrshb;
                            modelToAdd.XINVR = (currency.ToUpper() == "THB") ? "X" : "";
                            modelToAdd.ZUONR = String.IsNullOrEmpty(item.OC_TripNo) ? "" : item.OC_TripNo;

                            MemoRecordCreateServiceConnectorImpl MRService = new MemoRecordCreateServiceConnectorImpl();
                            MemoRecordService service = new MemoRecordService();
                            ConfigManagement configManagement = new ConfigManagement();
                            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CREATE");
                            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                            string content = javaScriptSerializer.Serialize(modelToAdd);

                            var result = MRService.connect(jsonConfig, content);

                            if (!(result == null) || (result.TYPE.ToUpper() != "S")) //No error when send to SAP
                            {

                                UpdateMemoOther(item.OC_TripNo, item.OC_ItemNo, result.MESSAGE_V1);

                                item.RET_MSG = "";
                                item.RET_STS = "S";
                                item.OC_MemoNo = result.MESSAGE_V1;

                            }
                            else //Error when send to SAP
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = result.MESSAGE + ", " + result.MESSAGE_V1;
                            }

                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    } //Next Item
                }

                #endregion

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteMemoList(List<MemoRecordDeleteModel> modelToDeleteList, string tripno, string itemno, ref string RET_STS, ref string RET_MSG)
        {
            if (modelToDeleteList == null)
                return;

            MemoRecordDeleteServiceConnectorImpl MRService = new MemoRecordDeleteServiceConnectorImpl();
            //MemoRecordService service = new MemoRecordService();
            //MemoRecordDeleteModel modelToDelete = new MemoRecordDeleteModel();

            ConfigManagement configManagement = new ConfigManagement();
            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_DELETE");
            var javaScriptSerializer = new JavaScriptSerializer();

            foreach (var item in modelToDeleteList)
            {
                string content = javaScriptSerializer.Serialize(item);

                var result = MRService.connect(jsonConfig, content);

                if ((result != null) && (result.TYPE.ToUpper() == "S")) //Failed add new SAP record
                {


                    string memo = item.IDENR;
                    //PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                    //PCF_DAILY_SAP_MEMO_NO sapMemo = new PCF_DAILY_SAP_MEMO_NO();
                    //sapMemo.MEMO_NO = memo;
                    //sapMemo.TRIP_NO = tripno;
                    //sapMemo.MAT_ITEM_NO = Convert.ToDecimal(itemno);
                    ////sapMemo.LOADING_DATE = DateTime.ParseExact(item.DATUM, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                    //sapMemo.IS_DELETE = "X";
                    //sapMemo.CREATED_DATE = DateTime.Now;
                    //sapMemo.CREATED_BY = Const.User.Name;
                    //sapMemo.UPDATED_DATE = DateTime.Now;
                    //sapMemo.UPDATED_BY = Const.User.Name;

                    //dal.Update(sapMemo);

                    UpdateMemoCrude(tripno, itemno, memo, Const.User.Name, "", "X");

                    RET_STS = "S";
                    RET_MSG = result.MESSAGE;
                }
                else
                {
                    RET_STS = "E";
                    RET_MSG = result.MESSAGE;
                }

            }

        }

        private void CreateMemoList(List<MemoRecordCreateModel> modelToAddList, string tripno, string itemno, ref string PMA_MEMO_NO, ref string RET_STS, ref string RET_MSG)
        {
            if (modelToAddList == null)
                return;

            MemoRecordCreateServiceConnectorImpl MRService = new MemoRecordCreateServiceConnectorImpl();
            MemoRecordService service = new MemoRecordService();
            ConfigManagement configManagement = new ConfigManagement();
            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CREATE");
            var javaScriptSerializer = new JavaScriptSerializer();

            foreach (var item in modelToAddList)
            {
                string content = javaScriptSerializer.Serialize(item);

                var result = MRService.connect(jsonConfig, content);

                if (!((result == null) || (result.TYPE.ToUpper() != "S"))) //No error when send to SAP
                {
                    //PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                    //PCF_DAILY_SAP_MEMO_NO sapMemo = new PCF_DAILY_SAP_MEMO_NO();
                    //sapMemo.MEMO_NO = result.MESSAGE_V1;
                    //sapMemo.TRIP_NO = tripno;
                    //sapMemo.MAT_ITEM_NO = Convert.ToDecimal(itemno);
                    ////DATUM
                    //sapMemo.LOADING_DATE = DateTime.ParseExact(item.DATUM, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);  //Convert.ToDateTime(item.PMA_LOADING_DATE_FROM);
                    //sapMemo.IS_DELETE = "";
                    //sapMemo.CREATED_DATE = DateTime.Now;
                    //sapMemo.CREATED_BY = Const.User.Name;
                    //sapMemo.UPDATED_DATE = DateTime.Now;
                    //sapMemo.UPDATED_BY = Const.User.Name;
                    //dal.Save(sapMemo);

                    InsertMemoCrude(tripno, itemno, result.MESSAGE_V1, Const.User.Name, item.DATUM);

                    RET_STS = "S";
                    PMA_MEMO_NO = result.MESSAGE_V1;

                }
                else //Error when send to SAP
                {
                    RET_STS = "E";
                    RET_MSG = result.MESSAGE + ", " + result.MESSAGE_V1;
                }

            }

        }

        private void UpdateMemoList(List<MemoRecordChangeModel> modelToChangeList, string tripno, string itemno, ref string PMA_MEMO_NO, ref string RET_STS, ref string RET_MSG)
        {
            if (modelToChangeList == null)
                return;

            MemoRecordChangeServiceConnectorImpl MRService = new MemoRecordChangeServiceConnectorImpl();
            MemoRecordService service = new MemoRecordService();
            ConfigManagement configManagement = new ConfigManagement();
            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CHANGE");
            var javaScriptSerializer = new JavaScriptSerializer();

            foreach (var item in modelToChangeList)
            {
                string content = javaScriptSerializer.Serialize(item);

                var result = MRService.connect(jsonConfig, content);

                if ((result != null) || (result.TYPE.ToUpper() == "S")) //No error when send to SAP
                {
                    string memo = item.IDENR;
                    //PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                    //PCF_DAILY_SAP_MEMO_NO sapMemo = new PCF_DAILY_SAP_MEMO_NO();
                    //sapMemo.MEMO_NO = memo;
                    //sapMemo.TRIP_NO = tripno;
                    //sapMemo.MAT_ITEM_NO = Convert.ToDecimal(itemno);
                    //sapMemo.LOADING_DATE = DateTime.ParseExact(item.DATUM, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                    //sapMemo.IS_DELETE = "";
                    //sapMemo.CREATED_DATE = DateTime.Now;
                    //sapMemo.CREATED_BY = Const.User.Name;
                    //sapMemo.UPDATED_DATE = DateTime.Now;
                    //sapMemo.UPDATED_BY = Const.User.Name;

                    //dal.Update(sapMemo);

                    UpdateMemoCrude(tripno, itemno, memo, Const.User.Name, item.DATUM, "");

                    PMA_MEMO_NO = memo;
                    RET_STS = "S";

                }
                else
                {
                    RET_STS = "E";
                    RET_MSG = result.MESSAGE + ", " + result.MESSAGE_V1;
                }


            }



        }

        public CrudeApproveFormViewModel_Detail UpdateCrudeMemoRecord(CrudeApproveFormViewModel_Detail model)
        {
            try
            {
                if (model.dDetail_Header == null)
                {
                    throw new Exception("dDetail_Header is NULL");
                }


                System.Globalization.CultureInfo cu = new System.Globalization.CultureInfo("en-US");

                var pcfHeader = model.dDetail_Header[0];

                #region Materia

                //if (model.dDetail_Material != null)
                //{
                //    foreach (var item in model.dDetail_Material)
                //    {
                //        try
                //        {
                //            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                //            DateTime fromDate;
                //            DateTime toDate;
                //            if (item.PMA_INCOTERMS.ToLower() == "fob")
                //            {
                //                var splitDate = item.PMA_RECEIVED_FROM.Replace(" to ", "|").Split('|');
                //                fromDate = DateTime.ParseExact(splitDate[0], "dd/MM/yyyy", cultureinfo);
                //                toDate = DateTime.ParseExact(splitDate[1], "dd/MM/yyyy", cultureinfo);
                //            }
                //            else
                //            {
                //                var splitDate = item.PMA_LOADING_DATE_FROM.Replace(" to ", "|").Split('|');
                //                fromDate = DateTime.ParseExact(splitDate[0], "dd/MM/yyyy", cultureinfo);
                //                toDate = DateTime.ParseExact(splitDate[1], "dd/MM/yyyy", cultureinfo);
                //            }

                //            string memoNo = GetMemoNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO));
                //            if (memoNo != null) //Found the existing record
                //            {
                //                foreach (DateTime day in EachDay(fromDate, toDate))
                //                {
                //                    var resultFromMtSapMemoMapping = GetMtSapMemoMapping(item.PMA_SUPPLIER);
                //                    if (resultFromMtSapMemoMapping == null)
                //                    {
                //                        //item.RET_STS = "E";
                //                        //item.RET_MSG = "Cannot find Vendor " + item.PMA_SUPPLIER + " in MT_SAP_MEMO_MAPPING";
                //                        continue;
                //                    }

                //                    MemoRecordChangeModel modelToChange = new MemoRecordChangeModel();
                //                    string merkm = (resultFromMtSapMemoMapping.CostName == "Crude/Feedstock/Product") ? resultFromMtSapMemoMapping.MetNum : resultFromMtSapMemoMapping.CostName;
                //                    if (merkm == null)
                //                    {
                //                        merkm = "";
                //                    }

                //                    string volume = "";
                //                    decimal unitPrice = GetUnitPrice(item.PMA_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO));
                //                    string currency = GetCurrency(item.PMA_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO));
                //                    switch (item.PMA_PURCHASE_UNIT.ToLower())
                //                    {
                //                        case "bbl":
                //                            volume = item.PMA_VOLUME_BBL;
                //                            break;
                //                        case "mt":
                //                            volume = item.PMA_VOLUME_MT;
                //                            break;
                //                        case "ml":
                //                            volume = item.PMA_VOLUME_ML;
                //                            break;
                //                        case "l15":
                //                            volume = item.PMA_VOLUME_L15;
                //                            break;
                //                        case "lton":
                //                            volume = item.PMA_VOLUME_LTON;
                //                            break;
                //                        case "kg":
                //                            volume = item.PMA_VOLUME_KG;
                //                            break;
                //                        default:
                //                            volume = "0";
                //                            break;
                //                    }
                //                    decimal wrshb = (Convert.ToDecimal(volume) * unitPrice) / DateTime.DaysInMonth(day.Year, day.Month);
                //                    wrshb = Convert.ToDecimal(wrshb.ToString("0.##")); //Keep only 2 digit because too many digit causes the crash when send to SAP
                //                    DateTime poDate = new DateTime();
                //                    if (pcfHeader.PHE_CHANNEL.ToUpper() == "TRAIN")
                //                    {
                //                        poDate = day;
                //                    }
                //                    else //VESSEL
                //                    {
                //                        poDate = Convert.ToDateTime(item.PMA_DUE_DATE);
                //                    }

                //                    int lastDayInMonth = DateTime.DaysInMonth(poDate.Year, poDate.Month);
                //                    string datum = poDate.AddDays(lastDayInMonth - poDate.Day).ToString("yyyy-MM-dd");
                //                    string planningDate = "";
                //                    int expiryDays = GetExpirayDays(item.PMA_SUPPLIER);
                //                    if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                //                    {
                //                        planningDate = poDate.AddDays(lastDayInMonth - poDate.Day).AddDays(expiryDays).ToString("yyyy-MM-dd");
                //                    }
                //                    else //If not found, add 3 months to expiry days
                //                    {
                //                        planningDate = poDate.AddDays(lastDayInMonth - poDate.Day).AddMonths(3).ToString("yyyy-MM-dd");
                //                    }
                //                    string sgtxt = GetVendorShortName(item.PMA_SUPPLIER) + "/" + merkm + "/" + pcfHeader.PHE_TRIP_NO;

                //                    decimal dmshb = 0;
                //                    if (currency.ToLower() == "usd")
                //                    {
                //                        dmshb = 0;
                //                    }
                //                    else
                //                    {
                //                        dmshb = wrshb;
                //                    }

                //                    modelToChange.AVDAT = String.IsNullOrEmpty(planningDate) ? "" : planningDate;
                //                    modelToChange.BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                //                    modelToChange.DISPW = String.IsNullOrEmpty(currency) ? "" : currency;
                //                    modelToChange.DATUM = String.IsNullOrEmpty(datum) ? "" : datum;
                //                    modelToChange.DMSHB = dmshb;
                //                    modelToChange.DMSHBSpecified = true;
                //                    modelToChange.GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup;
                //                    modelToChange.GVALT = (day == null) ? "" : day.ToString("yyyy-MM-dd");
                //                    modelToChange.IDENR = memoNo;
                //                    modelToChange.KURST = "M";
                //                    modelToChange.MERKM = String.IsNullOrEmpty(merkm) ? "" : merkm;
                //                    modelToChange.REFER = String.IsNullOrEmpty(item.PMA_SUPPLIER) ? "" : item.PMA_SUPPLIER;
                //                    modelToChange.SGTXT = String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                //                    modelToChange.TESTRUN = "";
                //                    modelToChange.VOART = "";
                //                    modelToChange.WRSHB = wrshb;
                //                    modelToChange.WRSHBSpecified = true;
                //                    modelToChange.XINVR = (currency.ToUpper() == "THB") ? "X" : "";
                //                    modelToChange.ZUONR = String.IsNullOrEmpty(item.PMA_TRIP_NO) ? "" : item.PMA_TRIP_NO;

                //                    MemoRecordChangeServiceConnectorImpl MRService = new MemoRecordChangeServiceConnectorImpl();
                //                    MemoRecordService service = new MemoRecordService();
                //                    ConfigManagement configManagement = new ConfigManagement();
                //                    string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CHANGE");
                //                    var javaScriptSerializer = new JavaScriptSerializer();
                //                    string content = javaScriptSerializer.Serialize(modelToChange);

                //                    var result = MRService.connect(jsonConfig, content);

                //                    if ((result != null) || (result.TYPE.ToUpper() == "S")) //No error when send to SAP
                //                    {
                //                        PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                //                        PCF_DAILY_SAP_MEMO_NO sapMemo = new PCF_DAILY_SAP_MEMO_NO();
                //                        sapMemo.MEMO_NO = memoNo;
                //                        sapMemo.TRIP_NO = item.PMA_TRIP_NO;
                //                        sapMemo.MAT_ITEM_NO = Convert.ToDecimal(item.PMA_ITEM_NO);
                //                        sapMemo.LOADING_DATE = day;
                //                        sapMemo.IS_DELETE = "";
                //                        sapMemo.CREATED_DATE = DateTime.Now;
                //                        sapMemo.CREATED_BY = Const.User.Name;
                //                        sapMemo.UPDATED_DATE = DateTime.Now;
                //                        sapMemo.UPDATED_BY = Const.User.Name;

                //                        dal.Update(sapMemo);

                //                        item.PMA_MEMO_NO = memoNo;
                //                    }

                //                }
                //            }
                //            else //Not Found Memo in DB
                //            {
                //                //item.RET_STS = "E";
                //                //item.RET_MSG = "Cannot find Memo No in DB";
                //            }

                //            var memoList = GetMemoNoExceptDate(item.PMA_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO), fromDate, toDate);
                //            if (memoList != null)
                //            {
                //                foreach (var memo in memoList)
                //                {
                //                    DeleteSpecificMemoRecord(memo, pcfHeader.PHE_COMPANY_CODE);
                //                }
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            item.RET_STS = "E";
                //            item.RET_MSG = ex.Message;
                //        }
                //    } //Next Item
                //}

                #endregion

                #region Frieght

                if (model.dDetail_Freight != null)
                {
                    foreach (var item in model.dDetail_Freight)
                    {
                        try
                        {
                            string memoNo = string.IsNullOrEmpty(item.FR_MemoNo) ? "" : item.FR_MemoNo; // GetMemoNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.FR_ItemNo));
                            if (memoNo.Equals(""))
                                continue;

                            string isDel = string.IsNullOrEmpty(item.IS_DELETE) ? "" : item.IS_DELETE;
                            if (isDel.Equals("X"))
                                continue;

                            var resultFromMtSapMemoMapping = GetMtSapMemoMapping(pcfHeader.PHE_COMPANY_CODE, "Freight", item.FR_Owner, "");
                            //var resultFromMtSapMemoMapping = GetMtSapMemoMapping(item.FR_Owner);
                            if (resultFromMtSapMemoMapping == null)
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Cannot find data Freight in MT_SAP_MEMO_MAPPING";
                                continue;
                            }

                            if (string.IsNullOrEmpty(item.FR_DueDate))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Due Date Is not null !";
                                continue;
                            }

                            MemoRecordChangeModel modelToChange = new MemoRecordChangeModel();
                            string merkm = "Freight";//  (resultFromMtSapMemoMapping.CostName == "Freight") ? resultFromMtSapMemoMapping.MetNum : resultFromMtSapMemoMapping.CostName;
                             

                            decimal unitPrice = Convert.ToDecimal((string.IsNullOrEmpty(item.FR_PriceUnit) ? "0" : item.FR_PriceUnit.Replace(",", "")));
                            string currency = string.IsNullOrEmpty(item.FR_AmtUnit) ? "" : item.FR_AmtUnit;

                            decimal wrshb = Convert.ToDecimal(string.IsNullOrEmpty(item.FR_Amt) ? "0" : item.FR_Amt.Replace(",", ""));
                            wrshb = Convert.ToDecimal((wrshb * -1).ToString("0.##"));

                            var dueDate = DateTime.ParseExact(item.FR_DueDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            int lastDayInMonth = DateTime.DaysInMonth(dueDate.Year, dueDate.Month);
                            string datum = dueDate.ToString("yyyy-MM-dd", cu);

                            string planningDate = "";
                            int expiryDays = GetExpirayDays(item.FR_Owner);
                            if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                            {
                                planningDate = dueDate.AddDays(lastDayInMonth - dueDate.Day).AddDays(expiryDays).ToString("yyyy-MM-dd", cu);
                            }
                            else //If not found, add 3 months to expiry days
                            {
                                planningDate = dueDate.AddDays(lastDayInMonth - dueDate.Day).AddMonths(3).ToString("yyyy-MM-dd", cu);
                            }
                            string sgtxt = GetVendorShortName(item.FR_Owner) + "/" + merkm;
                            if (sgtxt.Length > 40)
                                sgtxt = sgtxt.Substring(0, 40);

                            decimal dmshb = 0;
                            if (currency.ToLower() == "usd")
                                dmshb = 0;
                            else
                                dmshb = wrshb;

                            modelToChange.AVDAT = planningDate; // String.IsNullOrEmpty(planningDate) ? "" : planningDate;
                            modelToChange.BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                            modelToChange.DISPW = currency; // String.IsNullOrEmpty(currency) ? "" : currency;
                            modelToChange.DATUM = datum; // String.IsNullOrEmpty(datum) ? "" : datum;
                            modelToChange.DMSHB = dmshb;
                            modelToChange.DMSHBSpecified = true;
                            modelToChange.GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup;
                            modelToChange.GVALT = datum; // String.IsNullOrEmpty(item.FR_DueDate.ToString()) ? "" : Convert.ToDateTime(item.FR_DueDate).ToString("yyyy-MM-dd");
                            modelToChange.IDENR = memoNo;
                            modelToChange.KURST = "M";
                            modelToChange.MERKM = merkm; // String.IsNullOrEmpty(merkm) ? "" : merkm;
                            modelToChange.REFER = String.IsNullOrEmpty(item.FR_Owner) ? "" : item.FR_Owner;
                            modelToChange.SGTXT = sgtxt; // String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                            modelToChange.TESTRUN = "";
                            modelToChange.VOART = "";
                            modelToChange.WRSHB = wrshb;
                            modelToChange.WRSHBSpecified = true;
                            modelToChange.XINVR = (currency.ToUpper() == "THB") ? "X" : "";
                            modelToChange.ZUONR = String.IsNullOrEmpty(item.FR_TripNo) ? "" : item.FR_TripNo;

                            MemoRecordChangeServiceConnectorImpl MRService = new MemoRecordChangeServiceConnectorImpl();
                            MemoRecordService service = new MemoRecordService();
                            ConfigManagement configManagement = new ConfigManagement();
                            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CHANGE");
                            var javaScriptSerializer = new JavaScriptSerializer();
                            string content = javaScriptSerializer.Serialize(modelToChange);

                            var result = MRService.connect(jsonConfig, content);

                            if ((result != null) || (result.TYPE.ToUpper() == "S")) //No error when send to SAP
                            {

                                //UpdateMemoFreight(item.FR_TripNo, item.FR_ItemNo, memoNo);
                                item.RET_STS = "S";
                                item.RET_MSG = "";
                                item.FR_MemoNo = memoNo;
                            }

                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    } //Next Item
                }

                #endregion

                #region Other

                if (model.dDetail_Other != null)
                {
                    foreach (var item in model.dDetail_Other)
                    {
                        try
                        {
                            string memoNo = string.IsNullOrEmpty(item.OC_MemoNo) ? "" : item.OC_MemoNo; // GetMemoNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.OC_ItemNo));
                            if (memoNo.Equals(""))
                                continue;

                            string isDel = string.IsNullOrEmpty(item.IS_DELETE) ? "" : item.IS_DELETE;
                            if (isDel.Equals("X"))
                                continue;

                            if (string.IsNullOrEmpty(item.OC_DueDate))
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Due Date Is not null !";
                                continue;
                            }

                            var resultFromMtSapMemoMapping = GetMtSapMemoMapping(pcfHeader.PHE_COMPANY_CODE, "OtherExpense", item.OC_Vendor, "");
                            //var resultFromMtSapMemoMapping = GetMtSapMemoMapping(item.OC_Vendor);
                            if (resultFromMtSapMemoMapping == null)
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = "Cannot find data OtherExpense in MT_SAP_MEMO_MAPPING";
                                continue;
                            }

                            MemoRecordChangeModel modelToChange = new MemoRecordChangeModel();
                            string merkm = "OtherExpense";// (resultFromMtSapMemoMapping.CostName == "OtherExpense") ? resultFromMtSapMemoMapping.MetNum : resultFromMtSapMemoMapping.CostName;
                           

                            decimal unitPrice = Convert.ToDecimal(string.IsNullOrEmpty(item.OC_Total) ? "0" : item.OC_Total.Replace(",", ""));
                            string currency = string.IsNullOrEmpty(item.OC_AmountUnit) ? "" : item.OC_AmountUnit;

                            decimal wrshb = Convert.ToDecimal(string.IsNullOrEmpty(item.OC_Amount) ? "0" : item.OC_Amount.Replace(",", ""));
                            wrshb = Convert.ToDecimal((wrshb * -1).ToString("0.##"));


                            var dueDate = DateTime.ParseExact(item.OC_DueDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //Convert.ToDateTime(item.OC_DueDate);
                            int lastDayInMonth = DateTime.DaysInMonth(dueDate.Year, dueDate.Month);
                            string datum = dueDate.ToString("yyyy-MM-dd", cu);

                            string planningDate = "";
                            int expiryDays = GetExpirayDays(item.OC_Vendor);
                            if (expiryDays != -1) //If found expiry days in MT_SAPMEMO_EXPIRE, use SME_DAY_FORVENDOR as expiray days
                            {
                                planningDate = dueDate.AddDays(lastDayInMonth - dueDate.Day).AddDays(expiryDays).ToString("yyyy-MM-dd", cu);
                            }
                            else //If not found, add 3 months to expiry days
                            {
                                planningDate = dueDate.AddDays(lastDayInMonth - dueDate.Day).AddMonths(3).ToString("yyyy-MM-dd", cu);
                            }
                            string sgtxt = GetVendorShortName(item.OC_Vendor) + "/" + merkm;
                            if (sgtxt.Length > 40)
                                sgtxt = sgtxt.Substring(0, 40);

                            decimal dmshb = 0;
                            if (currency.ToLower() == "usd")
                                dmshb = 0;
                            else
                                dmshb = wrshb;

                            modelToChange.AVDAT = planningDate; // String.IsNullOrEmpty(planningDate) ? "" : planningDate;
                            modelToChange.BUKRS = String.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                            modelToChange.DISPW = currency; // String.IsNullOrEmpty(currency) ? "" : currency;
                            modelToChange.DATUM = datum; // String.IsNullOrEmpty(datum) ? "" : datum;
                            modelToChange.DMSHB = dmshb;
                            modelToChange.DMSHBSpecified = true;
                            modelToChange.GRUPP = String.IsNullOrEmpty(resultFromMtSapMemoMapping.PlanningGroup) ? "" : resultFromMtSapMemoMapping.PlanningGroup;
                            modelToChange.GVALT = datum; // String.IsNullOrEmpty(item.OC_DueDate.ToString()) ? "" : Convert.ToDateTime(item.OC_DueDate).ToString("yyyy-MM-dd");
                            modelToChange.IDENR = memoNo;
                            modelToChange.KURST = "M";
                            modelToChange.MERKM = merkm;// String.IsNullOrEmpty(merkm) ? "" : merkm;
                            modelToChange.REFER = datum; // String.IsNullOrEmpty(item.OC_DueDate) ? "" : item.OC_DueDate;
                            modelToChange.SGTXT = String.IsNullOrEmpty(sgtxt) ? "" : sgtxt;
                            modelToChange.TESTRUN = "";
                            modelToChange.VOART = "";
                            modelToChange.WRSHB = wrshb;
                            modelToChange.WRSHBSpecified = true;
                            modelToChange.XINVR = (currency.ToUpper() == "THB") ? "X" : "";
                            modelToChange.ZUONR = String.IsNullOrEmpty(item.OC_TripNo) ? "" : item.OC_TripNo;

                            MemoRecordChangeServiceConnectorImpl MRService = new MemoRecordChangeServiceConnectorImpl();
                            MemoRecordService service = new MemoRecordService();
                            ConfigManagement configManagement = new ConfigManagement();
                            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_CHANGE");
                            var javaScriptSerializer = new JavaScriptSerializer();
                            string content = javaScriptSerializer.Serialize(modelToChange);

                            var result = MRService.connect(jsonConfig, content);

                            if ((result != null) && (result.TYPE.ToUpper() == "S")) //No error when send to SAP
                            {                                
                                item.RET_MSG = "";
                                item.RET_STS = "S";
                                item.OC_MemoNo = memoNo;
                            }

                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    } //Next Item
                }

                #endregion

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Boolean InsertMemoCrude(string tripno, string itemno, string memono, string username, string loadingdate)
        {
            Boolean ret = false;
            try
            {
                PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                dal.InsertMemoCrude(tripno, itemno, memono, username, loadingdate);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        private Boolean UpdateMemoCrude(string tripno, string itemno, string memono, string username, string loadingdate, string isdel)
        {
            Boolean ret = false;
            try
            {
                PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                dal.UpdateMemoCrude(tripno, itemno, memono, username, loadingdate, isdel);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        private Boolean UpdateMemoFreight(string tripno, string itemno, string memono)
        {
            Boolean ret = false;
            try
            {
                PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                dal.UpdateMemoFreight(tripno, itemno, memono);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        private Boolean UpdateMemoOther(string tripno, string itemno, string memono)
        {
            Boolean ret = false;
            try
            {
                PCF_DAILY_SAP_MEMO_NO_DAL dal = new PCF_DAILY_SAP_MEMO_NO_DAL();
                dal.UpdateMemoOther(tripno, itemno, memono);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public CrudeApproveFormViewModel_Detail DeleteMemoRecord(CrudeApproveFormViewModel_Detail model)
        {
            try
            {
                if (model.dDetail_Header == null)
                {
                    throw new Exception("dDetail_Header is NULL");
                }

                var pcfHeader = model.dDetail_Header[0];

                #region Material

                //if (model.dDetail_Material != null)
                //{
                //    foreach (var item in model.dDetail_Material)
                //    {
                //        try
                //        {
                //            if (!String.IsNullOrEmpty(item.IS_DELETE))
                //            {
                //                MemoRecordDeleteServiceConnectorImpl MRService = new MemoRecordDeleteServiceConnectorImpl();
                //                MemoRecordService service = new MemoRecordService();
                //                MemoRecordDeleteModel modelToDelete = new MemoRecordDeleteModel();

                //                string memoNo = GetMemoNo(item.PMA_TRIP_NO, Convert.ToDecimal(item.PMA_ITEM_NO));
                //                if (memoNo == null)
                //                {
                //                    item.RET_STS = "E";
                //                    item.RET_MSG = "Cannot find Memo No in DB";
                //                    continue;
                //                }

                //                modelToDelete.IDENR = memoNo;
                //                modelToDelete.BUKRS = pcfHeader.PHE_COMPANY_CODE;
                //                modelToDelete.TESTRUN = "";

                //                ConfigManagement configManagement = new ConfigManagement();
                //                string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_DELETE");
                //                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                //                string content = javaScriptSerializer.Serialize(modelToDelete);

                //                var result = MRService.connect(jsonConfig, content);

                //                if ((result != null) && (result.TYPE.ToUpper() == "S")) //Failed add new SAP record
                //                {
                //                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                //                    {
                //                        var search = context.PCF_DAILY_SAP_MEMO_NO.Find(memoNo);
                //                        search.IS_DELETE = "X";
                //                        search.UPDATED_DATE = DateTime.Now;
                //                        search.UPDATED_BY = Const.User.Name;

                //                        context.SaveChanges();
                //                        item.RET_STS = "S";
                //                        item.RET_MSG = result.MESSAGE;
                //                    }
                //                }
                //                else
                //                {
                //                    item.RET_STS = "E";
                //                    item.RET_MSG = result.MESSAGE;
                //                }
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            item.RET_STS = "E";
                //            item.RET_MSG = ex.Message;
                //        }
                //    }
                //}

                #endregion

                #region Freight

                if (model.dDetail_Freight != null)
                {
                    foreach (var item in model.dDetail_Freight)
                    {
                        try
                        {
                            string memoNo = string.IsNullOrEmpty(item.FR_MemoNo) ? "" : item.FR_MemoNo; // GetMemoNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.FR_ItemNo));
                            if (memoNo.Equals(""))
                                continue;

                            string isDel = string.IsNullOrEmpty(item.IS_DELETE) ? "" : item.IS_DELETE;
                            if (!isDel.Equals("X"))
                                continue;


                            MemoRecordDeleteServiceConnectorImpl MRService = new MemoRecordDeleteServiceConnectorImpl();
                            MemoRecordService service = new MemoRecordService();
                            MemoRecordDeleteModel modelToDelete = new MemoRecordDeleteModel();

                            modelToDelete.IDENR = memoNo;
                            modelToDelete.BUKRS = string.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                            modelToDelete.TESTRUN = "";

                            ConfigManagement configManagement = new ConfigManagement();
                            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_DELETE");
                            var javaScriptSerializer = new JavaScriptSerializer();
                            string content = javaScriptSerializer.Serialize(modelToDelete);

                            var result = MRService.connect(jsonConfig, content);

                            if ((result != null) && (result.TYPE.ToUpper() == "S")) //Failed add new SAP record
                            {
                                UpdateMemoFreight(item.FR_TripNo, item.FR_ItemNo, "");

                                item.RET_STS = "S";
                                item.RET_MSG = result.MESSAGE;
                            }
                            else
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = result.MESSAGE;
                            }


                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    }
                }

                #endregion

                #region Other

                if (model.dDetail_Other != null)
                {
                    foreach (var item in model.dDetail_Other)
                    {
                        try
                        {
                            string memoNo = string.IsNullOrEmpty(item.OC_MemoNo) ? "" : item.OC_MemoNo; // GetMemoNo(pcfHeader.PHE_TRIP_NO, Convert.ToDecimal(item.FR_ItemNo));
                            if (memoNo.Equals(""))
                                continue;

                            string isDel = string.IsNullOrEmpty(item.IS_DELETE) ? "" : item.IS_DELETE;
                            if (!isDel.Equals("X"))
                                continue;

                            MemoRecordDeleteServiceConnectorImpl MRService = new MemoRecordDeleteServiceConnectorImpl();
                            MemoRecordService service = new MemoRecordService();
                            MemoRecordDeleteModel modelToDelete = new MemoRecordDeleteModel();

                            modelToDelete.IDENR = memoNo;
                            modelToDelete.BUKRS = string.IsNullOrEmpty(pcfHeader.PHE_COMPANY_CODE) ? "" : pcfHeader.PHE_COMPANY_CODE;
                            modelToDelete.TESTRUN = "";

                            ConfigManagement configManagement = new ConfigManagement();
                            string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_DELETE");
                            var javaScriptSerializer = new JavaScriptSerializer();
                            string content = javaScriptSerializer.Serialize(modelToDelete);

                            var result = MRService.connect(jsonConfig, content);

                            if ((result != null) && (result.TYPE.ToUpper() == "S")) //Failed add new SAP record
                            {
                                //UpdateMemoOther(item.OC_TripNo, item.OC_ItemNo, "");

                                item.RET_STS = "S";
                                item.RET_MSG = result.MESSAGE;
                            }
                            else
                            {
                                item.RET_STS = "E";
                                item.RET_MSG = result.MESSAGE;
                            }

                        }
                        catch (Exception ex)
                        {
                            item.RET_STS = "E";
                            item.RET_MSG = ex.Message;
                        }
                    }
                }

                #endregion

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteSpecificMemoRecord(string memoNo, string companyCode)
        {
            try
            {
                MemoRecordDeleteServiceConnectorImpl MRService = new MemoRecordDeleteServiceConnectorImpl();
                MemoRecordService service = new MemoRecordService();
                MemoRecordDeleteModel modelToDelete = new MemoRecordDeleteModel();

                modelToDelete.IDENR = memoNo;
                modelToDelete.BUKRS = companyCode;
                modelToDelete.TESTRUN = "";

                ConfigManagement configManagement = new ConfigManagement();
                string jsonConfig = configManagement.getDownstreamConfig("CIP_MEMO_DELETE");
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string content = javaScriptSerializer.Serialize(modelToDelete);

                var result = MRService.connect(jsonConfig, content);

                if ((result != null) && (result.TYPE.ToUpper() == "S")) //Failed add new SAP record
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var search = context.PCF_DAILY_SAP_MEMO_NO.Find(memoNo);
                        search.IS_DELETE = "X";
                        search.UPDATED_DATE = DateTime.Now;
                        search.UPDATED_BY = Const.User.Name;

                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PCF_HEADER GetPCFHeader(string tripNo)
        {
            try
            {
                PCF_HEADER pcfHeader = new PCF_HEADER();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from h in context.PCF_HEADER
                                 where h.PHE_TRIP_NO == tripNo
                                 select new
                                 {
                                     dTripNo = h.PHE_TRIP_NO,
                                     dCompanyCode = h.PHE_COMPANY_CODE,
                                     dArrivalYear = h.PHE_ARRIVAL_YEAR,
                                     dArrivalMonth = h.PHE_ARRIVAL_MONTH,
                                     dActualEtaFrom = h.PHE_ACTUAL_ETA_FROM,
                                     dActualEtaTo = h.PHE_ACTUAL_ETA_TO,
                                     dVessel = h.PHE_VESSEL,
                                     dCreatedDate = h.PHE_CREATED_DATE,
                                     dCreatedBy = h.PHE_CREATED_BY,
                                     dUpdatedDate = h.PHE_UPDATED_DATE,
                                     dUpdatedBy = h.PHE_UPDATED_BY,
                                     dCdaRowId = h.PHE_CDA_ROW_ID,
                                     dPlanningRelease = h.PHE_PLANNING_RELEASE,
                                     dBooingRelease = h.PHE_BOOKING_RELEASE,
                                     dStatus = h.PHE_STATUS,
                                     dChannel = h.PHE_CHANNEL,
                                     dSpotName = h.PHE_SPOT_NAME
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        foreach (var header in query)
                        {
                            pcfHeader = new PCF_HEADER
                            {
                                PHE_TRIP_NO = header.dTripNo,
                                PHE_COMPANY_CODE = header.dCompanyCode,
                                PHE_ARRIVAL_YEAR = header.dArrivalYear,
                                PHE_ARRIVAL_MONTH = header.dArrivalMonth,
                                PHE_ACTUAL_ETA_FROM = header.dActualEtaFrom,
                                PHE_ACTUAL_ETA_TO = header.dActualEtaTo,
                                PHE_VESSEL = header.dVessel,
                                PHE_CREATED_DATE = header.dCreatedDate,
                                PHE_CREATED_BY = header.dCreatedBy,
                                PHE_UPDATED_DATE = header.dUpdatedDate,
                                PHE_UPDATED_BY = header.dUpdatedBy,
                                PHE_CDA_ROW_ID = header.dCdaRowId,
                                PHE_PLANNING_RELEASE = header.dPlanningRelease,
                                PHE_BOOKING_RELEASE = header.dBooingRelease,
                                PHE_STATUS = header.dStatus,
                                PHE_CHANNEL = header.dChannel,
                                PHE_SPOT_NAME = header.dSpotName
                            };
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                return pcfHeader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PCF_HEADER> GetAllPCFHeader()
        {
            try
            {
                List<PCF_HEADER> pcfHeaderList = new List<PCF_HEADER>();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from h in context.PCF_HEADER
                                 select new
                                 {
                                     dTripNo = h.PHE_TRIP_NO,
                                     dCompanyCode = h.PHE_COMPANY_CODE,
                                     dArrivalYear = h.PHE_ARRIVAL_YEAR,
                                     dArrivalMonth = h.PHE_ARRIVAL_MONTH,
                                     dActualEtaFrom = h.PHE_ACTUAL_ETA_FROM,
                                     dActualEtaTo = h.PHE_ACTUAL_ETA_TO,
                                     dVessel = h.PHE_VESSEL,
                                     dCreatedDate = h.PHE_CREATED_DATE,
                                     dCreatedBy = h.PHE_CREATED_BY,
                                     dUpdatedDate = h.PHE_UPDATED_DATE,
                                     dUpdatedBy = h.PHE_UPDATED_BY,
                                     dCdaRowId = h.PHE_CDA_ROW_ID,
                                     dPlanningRelease = h.PHE_PLANNING_RELEASE,
                                     dBooingRelease = h.PHE_BOOKING_RELEASE,
                                     dStatus = h.PHE_STATUS,
                                     dChannel = h.PHE_CHANNEL,
                                     dSpotName = h.PHE_SPOT_NAME
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        foreach (var header in query)
                        {
                            pcfHeaderList.Add(new PCF_HEADER
                            {
                                PHE_TRIP_NO = header.dTripNo,
                                PHE_COMPANY_CODE = header.dCompanyCode,
                                PHE_ARRIVAL_YEAR = header.dArrivalYear,
                                PHE_ARRIVAL_MONTH = header.dArrivalMonth,
                                PHE_ACTUAL_ETA_FROM = header.dActualEtaFrom,
                                PHE_ACTUAL_ETA_TO = header.dActualEtaTo,
                                PHE_VESSEL = header.dVessel,
                                PHE_CREATED_DATE = header.dCreatedDate,
                                PHE_CREATED_BY = header.dCreatedBy,
                                PHE_UPDATED_DATE = header.dUpdatedDate,
                                PHE_UPDATED_BY = header.dUpdatedBy,
                                PHE_CDA_ROW_ID = header.dCdaRowId,
                                PHE_PLANNING_RELEASE = header.dPlanningRelease,
                                PHE_BOOKING_RELEASE = header.dBooingRelease,
                                PHE_STATUS = header.dStatus,
                                PHE_CHANNEL = header.dChannel,
                                PHE_SPOT_NAME = header.dSpotName
                            });
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                return pcfHeaderList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsMemoExistingByTripNoAndItemNo(string tripNo, decimal itemNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from s in context.PCF_DAILY_SAP_MEMO_NO
                                 where s.TRIP_NO == tripNo && s.MAT_ITEM_NO == itemNo
                                 select new
                                 {
                                     dTripNo = s.TRIP_NO
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PCF_DAILY_SAP_MEMO_NO> getMemoByTripNoAndItemNo(string tripNo, decimal itemNo)
        {
            List<PCF_DAILY_SAP_MEMO_NO> data;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    data = (from s in context.PCF_DAILY_SAP_MEMO_NO
                            where s.TRIP_NO == tripNo && s.MAT_ITEM_NO == itemNo && !s.IS_DELETE.Equals("X")
                            select s).ToList();

                }
            }
            catch (Exception ex)
            {
                data = null;
            }

            return data;
        }

        public string GetMemoNo(string tripNo, decimal itemNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from s in context.PCF_DAILY_SAP_MEMO_NO
                                 where s.TRIP_NO == tripNo && s.MAT_ITEM_NO == itemNo
                                 select new
                                 {
                                     dMemoNo = s.MEMO_NO
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        return query.ToList()[0].dMemoNo;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> GetMemoNoExceptDate(string tripNo, decimal itemNo, DateTime fromDate, DateTime toDate)
        {
            try
            {
                List<string> resultList = new List<string>();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from s in context.PCF_DAILY_SAP_MEMO_NO
                                 where s.TRIP_NO == tripNo && s.MAT_ITEM_NO == itemNo &&
                                 (s.LOADING_DATE >= fromDate && s.LOADING_DATE <= toDate)
                                 select new
                                 {
                                     dMemoNo = s.MEMO_NO
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        foreach (var item in query)
                        {
                            resultList.Add(item.dMemoNo);
                        }

                        return resultList;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*******/
        public List<PCF_DAILY_SAP_MEMO_NO> GetAllPCFMemoes()
        {
            try
            {
                List<PCF_DAILY_SAP_MEMO_NO> sapMemoList = new List<PCF_DAILY_SAP_MEMO_NO>();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from s in context.PCF_DAILY_SAP_MEMO_NO
                                 select new
                                 {
                                     dMemoNo = s.MEMO_NO,
                                     dTripNo = s.TRIP_NO,
                                     dItemNo = s.MAT_ITEM_NO,
                                     dLoadingDate = s.LOADING_DATE,
                                     dIsDelete = s.IS_DELETE,
                                     dCreatedDate = s.CREATED_DATE,
                                     dCreateBy = s.CREATED_BY,
                                     dUpdateDate = s.UPDATED_DATE,
                                     dUpdatedBy = s.UPDATED_BY
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        foreach (var memo in query)
                        {
                            sapMemoList.Add(new PCF_DAILY_SAP_MEMO_NO
                            {
                                MEMO_NO = memo.dMemoNo,
                                TRIP_NO = memo.dTripNo,
                                MAT_ITEM_NO = memo.dItemNo,
                                LOADING_DATE = memo.dLoadingDate,
                                IS_DELETE = memo.dIsDelete,
                                CREATED_DATE = memo.dCreatedDate,
                                CREATED_BY = memo.dCreateBy,
                                UPDATED_DATE = memo.dUpdateDate,
                                UPDATED_BY = memo.dUpdatedBy
                            });
                        }
                    }
                }

                return sapMemoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PCF_MATERIAL> GetAllItemsByTripNo(string tripNo)
        {
            try
            {
                List<PCF_MATERIAL> pcfItemList = new List<PCF_MATERIAL>();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from m in context.PCF_MATERIAL
                                 where m.PMA_TRIP_NO == tripNo
                                 select new
                                 {
                                     dTripNo = m.PMA_TRIP_NO,
                                     dItemNo = m.PMA_ITEM_NO,
                                     dMetNum = m.PMA_MET_NUM,
                                     dSupplier = m.PMA_SUPPLIER,
                                     dFormula = m.PMA_FORMULA,
                                     dContractType = m.PMA_CONTRACT_TYPE,
                                     dVolumeBBL = m.PMA_VOLUME_BBL,
                                     dVolumeMT = m.PMA_VOLUME_MT,
                                     dVolumeML = m.PMA_VOLUME_ML,
                                     dVolumeL15 = m.PMA_VOLUME_L15,
                                     dVolumeLTON = m.PMA_VOLUME_LTON,
                                     dVolumeKG = m.PMA_VOLUME_KG,
                                     dPurchaseUnit = m.PMA_PURCHASE_UNIT,
                                     dLoadingDateFrom = m.PMA_LOADING_DATE_FROM
                                 });
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            pcfItemList.Add(new PCF_MATERIAL
                            {
                                PMA_TRIP_NO = item.dTripNo,
                                PMA_ITEM_NO = item.dItemNo,
                                PMA_MET_NUM = item.dMetNum,
                                PMA_SUPPLIER = item.dSupplier,
                                PMA_FORMULA = item.dFormula,
                                PMA_CONTRACT_TYPE = item.dContractType,
                                PMA_VOLUME_BBL = item.dVolumeBBL,
                                PMA_VOLUME_MT = item.dVolumeMT,
                                PMA_VOLUME_ML = item.dVolumeML,
                                PMA_VOLUME_L15 = item.dVolumeL15,
                                PMA_VOLUME_LTON = item.dVolumeLTON,
                                PMA_VOLUME_KG = item.dVolumeKG,
                                PMA_PURCHASE_UNIT = item.dPurchaseUnit,
                                PMA_LOADING_DATE_FROM = item.dLoadingDateFrom
                            });
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                return pcfItemList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /*******/

        public ResultFromMtSapMemoMapping GetMtSapMemoMapping(string accNumVendor)
        {
            ResultFromMtSapMemoMapping result = new ResultFromMtSapMemoMapping();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from r in context.MT_SAP_MEMO_MAPPING
                             where r.SMM_ACC_NUM_VENDOR == accNumVendor
                             select new
                             {
                                 dCostName = r.SMM_COST_NAME,
                                 dMetNum = r.SMM_MET_NUM,
                                 dPlanningType = r.SMM_PLANNING_TYPE,
                                 dPlanningGroup = r.SMM_PLANNING_GROUP
                             });
                if (query != null && query.ToList().Count() > 0)
                {
                    var resultList = query.ToList();
                    if (resultList.Count() > 0)
                    {
                        result.CostName = resultList[0].dCostName;
                        result.MetNum = resultList[0].dMetNum;
                        result.PlanningType = resultList[0].dPlanningType;
                        result.PlanningGroup = resultList[0].dPlanningGroup;
                    }
                }
                else
                {
                    return null;
                }
            }

            return result;
        }

        public ResultFromMtSapMemoMapping GetMtSapMemoMapping(string comcode, string costname, string vendorcode, string matcode)
        {
            ResultFromMtSapMemoMapping result = new ResultFromMtSapMemoMapping();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from r in context.MT_SAP_MEMO_MAPPING
                             where r.SMM_COMPANY_CODE.Equals(comcode) &&  r.SMM_COST_NAME.Equals(costname)
                             select new
                             {
                                 dVendor = r.SMM_ACC_NUM_VENDOR == null ? "" : r.SMM_ACC_NUM_VENDOR, 
                                 dCostName = r.SMM_COST_NAME,
                                 dMetNum = r.SMM_MET_NUM == null ? "" : r.SMM_MET_NUM,
                                 dPlanningType = r.SMM_PLANNING_TYPE,
                                 dPlanningGroup = r.SMM_PLANNING_GROUP
                             });

                if (query == null)
                    return null;

                if (query.ToList().Count() <= 0)
                    return null;

                var query1 = query.Where(p => p.dVendor.Equals(vendorcode) && p.dMetNum.Equals(matcode));
                if (query1 != null && query1.ToList().Count > 0)
                {
                    var data = query1.ToList();
                    result.CostName = data[0].dCostName;
                    result.MetNum = data[0].dMetNum;
                    result.PlanningType = data[0].dPlanningType;
                    result.PlanningGroup = data[0].dPlanningGroup;
                    return result;
                }

                var query2 = query.Where(p => p.dVendor.Equals(vendorcode) &&  (p.dMetNum == null || p.dMetNum.Equals("")));
                if (query2 != null && query2.ToList().Count > 0)
                {
                    var data = query2.ToList();
                    result.CostName = data[0].dCostName;
                    result.MetNum = data[0].dMetNum;
                    result.PlanningType = data[0].dPlanningType;
                    result.PlanningGroup = data[0].dPlanningGroup;
                    return result;
                }

                var query3 = query.Where(p => (p.dVendor == null || p.dVendor.Equals("")) && p.dMetNum.Equals(matcode));
                if (query3 != null && query3.ToList().Count > 0)
                {
                    var data = query3.ToList();
                    result.CostName = data[0].dCostName;
                    result.MetNum = data[0].dMetNum;
                    result.PlanningType = data[0].dPlanningType;
                    result.PlanningGroup = data[0].dPlanningGroup;
                    return result;
                }

                var query4 = query.Where(p => (p.dVendor == null || p.dVendor.Equals("")) && (p.dMetNum == null || p.dMetNum.Equals("")));
                if (query4 != null && query4.ToList().Count > 0)
                {
                    var data = query4.ToList();
                    result.CostName = data[0].dCostName;
                    result.MetNum = data[0].dMetNum;
                    result.PlanningType = data[0].dPlanningType;
                    result.PlanningGroup = data[0].dPlanningGroup;
                    return result;
                }


            }

            return result;
        }

      

        public decimal GetUnitPrice(string tripNo, decimal matItemNo)
        {
            try
            {
                decimal unitPrice = 0;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PCF_MATERIAL_PRICE
                                 where p.PMP_TRIP_NO == tripNo && p.PMP_MAT_ITEM_NO == matItemNo
                                 select new
                                 {
                                     dUnitPrice = p.PMP_TOTAL_PRICE
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        unitPrice = query.ToList()[0].dUnitPrice.Value;
                    }
                    else
                    {
                        return 0;
                    }
                }

                return unitPrice;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCurrency(string tripNo, decimal matItemNo)
        {
            try
            {
                string currency = "";
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PCF_MATERIAL_PRICE
                                 where p.PMP_TRIP_NO == tripNo && p.PMP_MAT_ITEM_NO == matItemNo
                                 select new
                                 {
                                     dCurrency = p.PMP_CURRENCY
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        currency = query.ToList()[0].dCurrency;
                    }
                    else
                    {
                        return "";
                    }
                }

                return currency;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetExpirayDays(string accNumVendor)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from r in context.MT_SAP_MEMO_EXPIRE
                             where r.SME_ACC_NUM_VENDOR == accNumVendor
                             select new
                             {
                                 dExpiryDays = r.SME_DAY_FOR_EXPIRE
                             });
                if (query != null && query.ToList().Count() > 0)
                {
                    return Convert.ToInt32(query.ToList()[0].dExpiryDays);
                }
                else
                {
                    return -1;
                }
            }
        }

        public static string GetVendorShortName(string accNumVendor)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from r in context.MT_VENDOR
                                 where r.VND_ACC_NUM_VENDOR == accNumVendor
                                 select new
                                 {
                                     dShortName = r.VND_SORT_FIELD
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dShortName;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }


    }
}