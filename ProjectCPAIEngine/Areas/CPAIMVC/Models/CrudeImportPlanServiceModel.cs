﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.sap.Interface.sap.po.change;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.DALPCF;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using com.pttict.sap.Interface.Service;
using com.pttict.sap.Interface.Model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.downstream.common.utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CrudeImportPlanServiceModel
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        const string JSON_CRUDE_PURCHASE = "JSON_CRUDE_PURCHASE";
        const string FORMULA_PRICING_TYPE = "FORMULA_PRICING_TYPE";
        private static string strJSON = "{\"FORMULA_PRICING\": [{\"code\": \"DAILY\",\"type\": \"DATE_FROM_TO\"},{\"code\": \"OSP\",\"type\": \"MONTH\"}, {\"code\": \"CONSTANT\", \"type\": \"FACTOR\" }],\"FORMULA_SAP_PRICING_STRUCTURE\": [{\"code\": \"BASE_PRICE\",\"description\": \"Base Price\"},{\"code\": \"OSP_PREMIUM_DISCOUNT\",\"description\": \"OSP Premium/Discount\"},{\"code\": \"TRADING_PREMIUM_DISCOUNT\",\"description\": \"Trading Premium/Discount\"},{\"code\": \"OTHER_COST\",\"description\": \"Other Cost\"}],\"PCF_MT_PURCHASE_TYPE\": [{\"COMPANY_CODE\": \"1100\",\"CODE\": \"1\",\"NAME\": \"TOP purchase for TOP\"},{\"COMPANY_CODE\": \"1100\",\"CODE\": \"2\",\"NAME\": \"TOP purchase for SWAP\"},{\"COMPANY_CODE\": \"1400\",\"CODE\": \"3\", \"NAME\": \"TLB purchase for TLB\"},{\"COMPANY_CODE\": \"1400\", \"CODE\": \"4\",\"NAME\": \"TLB purchase for SWAP\"}],\"PCF_MT_STORAGE_LOCATION\": [{\"COMPANY_CODE\": \"1100\",\"CODE\": \"1100\",\"NAME\": \"Refinery\"},{\"COMPANY_CODE\": \"1100\",\"CODE\": \"12C1\",\"NAME\": \"IRPC\"},{\"COMPANY_CODE\": \"1400\",\"CODE\": \"1400\",\"NAME\": \"Refinery\"],\"PCF_MT_PRICE_STATUS\": [{\"CODE\": \"F\",\"NAME\": \"Forecast\"},{\"CODE\": \"P\",\"NAME\": \"Provisional\"},{\"CODE\": \"A\",\"NAME\": \"Actual\"}],\"PCF_MT_CHARTERING_TYPE\": [{\"CODE\": \"S\",\"NAME\": \"Spot\"},{\"CODE\": \"T\",\"NAME\": \"Time Charter\"}],\"PCF_MT_FREIGHT_TYPE\": [{\"CHARTERING_TYPE\": \"S\",\"CODE\": \"1\",\"NAME\": \"Freight Cost (Chartering)\"},{\"CHARTERING_TYPE\": \"T\",\"CODE\": \"2\",\"NAME\": \"Bunker\"},{\"CHARTERING_TYPE\": \"T\",\"CODE\": \"3\",\"NAME\": \"Ship Agent\"},{\"CHARTERING_TYPE\": \"T\",\"CODE\": \"4\",\"NAME\": \"Charter Hire\"}],\"PCF_MT_PO_SURVEYOR_TYPE\": [{\"NAME\": \"Loading Surveyor\",\"LOADING_DATE\": \"X\",\"LOADING_PORT\": \"X\",\"DISCHARGING_DATE\": \"\",\"DISCHARGING_PORT\": \"\"},{\"NAME\": \"Discharging Surveyor\",\"LOADING_DATE\": \"\",\"LOADING_PORT\": \"\",\"DISCHARGING_DATE\": \"X\",\"DISCHARGING_PORT\": \"X\"},{\"NAME\": \"Surveyor\",\"LOADING_DATE\": \"X\",\"LOADING_PORT\": \"X\",\"DISCHARGING_DATE\": \"X\",\"DISCHARGING_PORT\": \"X\"},{\"NAME\": \"STS Surveyor\",\"LOADING_DATE\": \"X\",\"LOADING_PORT\": \"X\",\"DISCHARGING_DATE\": \"X\",\"DISCHARGING_PORT\": \"X\"},{\"NAME\": \"Tast Lab Surveyor\",\"LOADING_DATE\": \"X\",\"LOADING_PORT\": \"X\",\"DISCHARGING_DATE\": \"X\",\"DISCHARGING_PORT\": \"X\"}],\"MT_COMPANY\": [{\"CODE\": \"1100\"},{\"CODE\": \"1400\"}],\"CURRENCY\": [{\"CODE\": \"THB\"},{\"CODE\": \"USD\"},{\"CODE\": \"SGD\"}]}";

        private static System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("en-US");
        private static string format = "dd/MM/yyyy";

        public ReturnValue Add(CompanyViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyCode))
                {
                    rtn.Message = "Company code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyName))
                {
                    rtn.Message = "Company name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_COMPANY_DAL dal = new MT_COMPANY_DAL();
                MT_COMPANY ent = new MT_COMPANY();

                DateTime now = DateTime.Now;

                //using (var context = new EntityCPAIEngine())
                //{
                //    using (var dbContextTransaction = context.Database.BeginTransaction())
                //    {
                try
                {
                    ent.MCO_COMPANY_CODE = pModel.CompanyCode;
                    ent.MCO_SHORT_NAME = pModel.CompanyName.Trim();
                    ent.MCO_STATUS = pModel.Status;
                    ent.MCO_CREATE_TYPE = pModel.CreateType;
                    ent.MCO_CREATED_BY = pUser;
                    ent.MCO_CREATED_DATE = now;
                    ent.MCO_UPDATED_BY = pUser;
                    ent.MCO_UPDATED_DATE = now;

                    //dal.Save(ent,context);
                    dal.Save(ent);
                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
                //    }

                //}

            }
            catch (Exception ex)
            {
                //string path = path = (LogManager.GetCurrentLoggers()[0].Logger.Repository.GetAppenders()[0] as FileAppender).File;
                rtn.Message = ex.Message;
                rtn.Status = false;
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
            }

            return rtn;
        }

        public ReturnValue Edit(CompanyViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel.BoolStatus != null)
                {
                    pModel.Status = pModel.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                }
                else
                {
                    pModel.Status = "INACTIVE";
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyCode))
                {
                    rtn.Message = "Company code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CompanyName))
                {
                    rtn.Message = "Company name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_COMPANY_DAL dal = new MT_COMPANY_DAL();
                MT_COMPANY ent = new MT_COMPANY();

                DateTime now = DateTime.Now;

                //using (var context = new EntityCPAIEngine())
                //{
                //    using (var dbContextTransaction = context.Database.BeginTransaction())
                //    {
                try
                {
                    ent.MCO_COMPANY_CODE = pModel.CompanyCode;
                    ent.MCO_SHORT_NAME = pModel.CompanyName.Trim();
                    ent.MCO_STATUS = pModel.Status;

                    ent.MCO_UPDATED_BY = pUser;
                    ent.MCO_UPDATED_DATE = now;

                    dal.Update(ent);

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    //dbContextTransaction.Rollback();
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
                //    }

                //}

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
            }

            return rtn;
        }

        private string getTextMonth(string sMonth)
        {
            string ret = "";
            switch (sMonth)
            {
                case "1": ret = "Jan"; break;
                case "2": ret = "Feb"; break;
                case "3": ret = "Mar"; break;
                case "4": ret = "Apr"; break;
                case "5": ret = "May"; break;
                case "6": ret = "Jun"; break;
                case "7": ret = "Jul"; break;
                case "8": ret = "Aug"; break;
                case "9": ret = "Sep"; break;
                case "10": ret = "Oct"; break;
                case "11": ret = "Nov"; break;
                case "12": ret = "Dec"; break;
                default:
                    break;
            }

            return ret;
        }

        private string getTextPort(CIP_Config CIPConfig, string code)
        {
            string ret = "";
            try
            {

            }
            catch (Exception ex) { }
            return ret;
        }

        private string getTextChartering(CIP_Config _CIPConfig, string code)
        {
            string ret = "";
            try
            {
                var query = _CIPConfig.PCF_MT_CHARTERING_TYPE.Where(p => p.CODE.Equals(code)).First();
                if (query != null)
                {
                    ret = query.NAME;
                }
            }
            catch (Exception ex) { }
            return ret;
        }
        private string getTextCostType(CIP_Config _CIPConfig, string code)
        {
            string ret = "";
            try
            {

            }
            catch (Exception ex) { }
            return ret;
        }

        private string getTextFreightType(CIP_Config _CIPConfig, string code)
        {
            string ret = "";
            try
            {
                var query = _CIPConfig.PCF_MT_FREIGHT_TYPE.Where(p => p.CODE.Equals(code)).First();
                if (query != null)
                {
                    ret = query.NAME;
                }
            }
            catch (Exception ex) { }
            return ret;
        }

        public string getPriceFormulaManual(List<string> MOPSPrdCode, string sPeriodFrom, string sPeriodTo)
        {
            string ret = "";

            CrudeImportPlanViewModel pModel = new CrudeImportPlanViewModel();
            pModel.PriceDetail = new PriceDataDetailViewModel();
            pModel.PriceDetail.PriceDaily = new List<PriceDailyDetail>();

            List<PCFMobDetail> result = Search(sPeriodFrom, MOPSPrdCode);

            //////////// Daily 
            if (result != null && result.Count > 0)
            {
                Decimal sum = 0;
                Double count = 0;
                foreach (var prodcoce in MOPSPrdCode)
                {
                    var qChk = result.Where(p => p.ProductCode.Equals(prodcoce));
                    count = qChk.Count();
                    sum = qChk.Sum(x => x.ProductValue);

                    var ProductName = "";
                    if (count > 0)
                        ProductName = qChk.ToList()[0].ProductName;

                    Double net = Convert.ToDouble(sum.ToString()) / count;

                    var qModel = pModel.PriceDetail.PriceDaily.Where(p => p.Code.Equals(prodcoce)).ToList();
                    if (qModel != null && qModel.Count > 0)
                    {
                        qModel[0].Price1 = net.ToString("#.####0");
                    }
                    else
                    {
                        pModel.PriceDetail.PriceDaily.Add(new PriceDailyDetail
                        {
                            Code = prodcoce,
                            Name = ProductName,
                            Price1 = net.ToString("#.####0")
                        });
                    }


                }


            }

            result = Search(sPeriodTo, MOPSPrdCode);
            if (result != null && result.Count > 0)
            {
                Decimal sum = 0;
                Double count = 0;
                foreach (var prodcoce in MOPSPrdCode)
                {
                    var qChk = result.Where(p => p.ProductCode.Equals(prodcoce));
                    count = qChk.Count();
                    sum = qChk.Sum(x => x.ProductValue);

                    var ProductName = "";
                    if (count > 0)
                        ProductName = qChk.ToList()[0].ProductName;

                    Double net = Convert.ToDouble(sum.ToString()) / count;

                    var qModel = pModel.PriceDetail.PriceDaily.Where(p => p.Code.Equals(prodcoce)).ToList();
                    if (qModel != null && qModel.Count > 0)
                    {
                        qModel[0].Price2 = net.ToString("#.####0");
                    }
                    else
                    {
                        pModel.PriceDetail.PriceDaily.Add(new PriceDailyDetail
                        {
                            Code = prodcoce,
                            Name = ProductName,
                            Price2 = net.ToString("#.####0")
                        });
                    }

                }



            }


            JavaScriptSerializer js = new JavaScriptSerializer();
            //json = js.Serialize(qDis.OrderBy(p => p.value));

            ret = js.Serialize(pModel.PriceDetail.PriceDaily);


            return ret;
        }

        public ReturnValue SearchCrudeImportPlan(string isBook, string sTrip, Decimal ArrivalMonth, Decimal ArrivalYear, CIP_Config _CIPConfig
                    , ref CrudeImportPlanViewModel pModel, ref List<string> retMOPSPrdCode)
        {
            string[] aTrip = sTrip.Split(',');
            List<string> arrTrip = new List<string>();
            foreach (var s in aTrip)
            {
                arrTrip.Add(s);
            }

            Boolean TripSearchSTS = false;
            Boolean PeriodSearchSTS = false;
            if (string.IsNullOrEmpty(sTrip))
                PeriodSearchSTS = true;
            else
                TripSearchSTS = true;

            string loadingMonthCheck = "";
            if (ArrivalYear != null) loadingMonthCheck = ArrivalYear.ToString();

            if (ArrivalMonth != null) {
                if (ArrivalMonth > 9) loadingMonthCheck += ArrivalMonth.ToString();
                else loadingMonthCheck += "0" + ArrivalMonth.ToString();
            }

            //where((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_ARRIVAL_MONTH == ArrivalMonth && h.PHE_ARRIVAL_YEAR == ArrivalYear))
            //                           && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))

            ReturnValue rtn = new ReturnValue();
            try
            {
                var cu = System.Globalization.CultureInfo.InvariantCulture;
                pModel.crude_approve_detail = new CrudeApproveFormViewModel_Detail();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    #region ############# Header ###################
                    var query = (from h in context.PCF_HEADER
                                 join v in context.MT_VEHICLE on h.PHE_VESSEL equals v.VEH_ID into tmpVessel
                                 from v in tmpVessel.DefaultIfEmpty()
                                 where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck ))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                 orderby h.PHE_UPDATED_DATE descending
                                 select new
                                 {
                                     PHE_TRIP_NO = h.PHE_TRIP_NO,
                                     PHE_COMPANY_CODE = h.PHE_COMPANY_CODE,
                                     PHE_ARRIVAL_YEAR = h.PHE_ARRIVAL_YEAR,
                                     PHE_ARRIVAL_MONTH = h.PHE_ARRIVAL_MONTH,
                                     PHE_ACTUAL_ETA_FROM = h.PHE_ACTUAL_ETA_FROM,
                                     PHE_VESSEL = h.PHE_VESSEL,
                                     PHE_VESSEL_TEXT = (h.PHE_VESSEL == "SPOT") ? "SPOT" : v.VEH_VEH_TEXT,
                                     PHE_CREATED_DATE = h.PHE_CREATED_DATE,
                                     PHE_CREATED_BY = h.PHE_CREATED_BY,
                                     PHE_UPDATED_DATE = h.PHE_UPDATED_DATE,
                                     PHE_UPDATED_BY = h.PHE_UPDATED_BY,
                                     PHE_CDA_ROW_ID = h.PHE_CDA_ROW_ID,
                                     PHE_PLANNING_RELEASE = h.PHE_PLANNING_RELEASE,
                                     PHE_BOOKING_RELEASE = h.PHE_BOOKING_RELEASE,
                                     PHE_STATUS = h.PHE_STATUS,
                                     PHE_CHANNEL = h.PHE_CHANNEL,
                                     PHE_SPOT_NAME = h.PHE_SPOT_NAME,
                                     PHE_ACTUAL_ETA_TO = h.PHE_ACTUAL_ETA_TO,
                                     PHE_MAT_TYPE = h.PHE_MAT_TYPE,
                                     PHE_LOADING_MONTH = h.PHE_LOADING_MONTH,
                                     PHE_CHARTERING_NO = h.PHE_CHARTERING_NO,
                                 }).Distinct().ToList();

                    pModel.crude_approve_detail.dDetail_Header = new List<HeaderViewModel>();

                    if (query != null)
                    {
                        foreach (var h in query)
                        {
                            string sendSTS = "";
                            if (h.PHE_MAT_TYPE.Equals("PRD"))
                            {
                                if (h.PHE_BOOKING_RELEASE.Equals("Y"))
                                    sendSTS = "Exported";
                            }
                            else
                            {
                                if (h.PHE_PLANNING_RELEASE.Equals("Y"))
                                    sendSTS = "Exported";
                            }


                            pModel.crude_approve_detail.dDetail_Header.Add(
                                             new HeaderViewModel
                                             {
                                                 PHE_TRIP_NO = h.PHE_TRIP_NO,
                                                 PHE_COMPANY_CODE = h.PHE_COMPANY_CODE,
                                                 PHE_ARRIVAL_YEAR = (h.PHE_ARRIVAL_YEAR != null) ? h.PHE_ARRIVAL_YEAR.ToString() : "",
                                                 PHE_ARRIVAL_MONTH = (h.PHE_ARRIVAL_MONTH != null) ? h.PHE_ARRIVAL_MONTH.ToString() : "",
                                                 PHE_ACTUAL_ETA_FROM = (h.PHE_ACTUAL_ETA_FROM != null) ? Convert.ToDateTime(h.PHE_ACTUAL_ETA_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PHE_ACTUAL_ETA_TO).ToString("dd/MM/yyyy", cu) : "",
                                                 PHE_ACTUAL_ETA_TO = "",
                                                 PHE_VESSEL = h.PHE_VESSEL,
                                                 PHE_CREATED_DATE = (h.PHE_CREATED_DATE != null) ? Convert.ToDateTime(h.PHE_CREATED_DATE).ToString() : "",
                                                 PHE_CREATED_BY = h.PHE_CREATED_BY,
                                                 PHE_UPDATED_DATE = (h.PHE_UPDATED_DATE != null) ? Convert.ToDateTime(h.PHE_UPDATED_DATE).ToString() : "",
                                                 PHE_UPDATED_BY = h.PHE_UPDATED_BY,
                                                 PHE_CDA_ROW_ID = h.PHE_CDA_ROW_ID,
                                                 PHE_PLANNING_RELEASE = h.PHE_PLANNING_RELEASE,
                                                 PHE_BOOKING_RELEASE = h.PHE_BOOKING_RELEASE,
                                                 PHE_STATUS = h.PHE_STATUS,
                                                 PHE_CHANNEL = h.PHE_CHANNEL,
                                                 PHE_SPOT_NAME = h.PHE_SPOT_NAME,
                                                 PHE_INDB_STATUS = "Y",
                                                 PHE_ACTION = h.PHE_STATUS,
                                                 PHE_VESSEL_TEXT = h.PHE_VESSEL_TEXT,
                                                 PHE_MAT_TYPE = h.PHE_MAT_TYPE,
                                                 PHE_EXPORT_STS = sendSTS,
                                                 PHE_LOADING_MONTH = h.PHE_LOADING_MONTH ?? "",
                                             });
                        }

                    }
                    #endregion

                    #region ############# Material & Customs ###################
                    var queryMatFile = (from h in context.PCF_HEADER
                                        join m in context.PCF_ATTACH_FILE on h.PHE_TRIP_NO equals m.PAF_TRIP_NO
                                        where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                             && (h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD"))))
                                             && (m.PAF_FILE_FOR.Equals("MAT"))
                                        orderby h.PHE_TRIP_NO, m.PAF_FILE_FOR, m.PAF_ITEM_NO, m.PAF_NO
                                        select new
                                        {
                                            PAF_ITEM_NO = m.PAF_ITEM_NO,
                                            PAF_NO = m.PAF_NO,
                                            PAF_TRIP_NO = m.PAF_TRIP_NO,
                                            PAF_FILE_NAME = m.PAF_FILE_NAME,
                                            PAF_PATH = m.PAF_PATH,
                                            PAF_DESCRIPTION = m.PAF_DESCRIPTION,
                                            PAF_FILE_FOR = m.PAF_FILE_FOR,
                                        }).ToList();

                    pModel.crude_approve_detail.dDetail_MaterialFile = new List<FileViewModel>();

                    foreach (var itemFile in queryMatFile)
                    {
                        string itemNo = (itemFile.PAF_ITEM_NO == null) ? "" : itemFile.PAF_ITEM_NO.ToString();
                        string no = itemFile.PAF_NO == null ? "" : itemFile.PAF_NO.ToString();
                        string refid = itemNo;// + no;

                        pModel.crude_approve_detail.dDetail_MaterialFile.Add(new FileViewModel
                        {
                            PAF_TRIP_NO = itemFile.PAF_TRIP_NO ?? "",
                            PAF_ITEM_NO = itemNo,
                            PAF_NO = no,
                            PAF_RED_ID = refid,
                            PAF_FILE_NAME = itemFile.PAF_FILE_NAME ?? "",
                            PAF_PATH = itemFile.PAF_PATH ?? "",
                            PAF_DESCRIPTION = itemFile.PAF_DESCRIPTION ?? "",
                            PAF_FILE_FOR = itemFile.PAF_FILE_FOR ?? "",
                            PAF_INDB_STS = "1",
                            PAF_STATUS = "",
                            PAF_DESC_TEMP = itemFile.PAF_DESCRIPTION ?? "",
                        });
                    }


                    pModel.crude_approve_detail.dDetail_Material = new List<MaterialViewModel>();
                    var queryM = (from h in context.PCF_HEADER
                                  join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO
                                  join c in context.PCF_CUSTOMS_VAT on new { tripno = m.PMA_TRIP_NO, itemno = m.PMA_ITEM_NO } equals new { tripno = c.TRIP_NO, itemno = c.MAT_ITEM_NO } into tmpCustom
                                  join v in context.MT_VENDOR on m.PMA_SUPPLIER equals v.VND_ACC_NUM_VENDOR into tmpVendor
                                  from c in tmpCustom.DefaultIfEmpty()
                                  from v in tmpVendor.DefaultIfEmpty()
                                  join t in context.MT_MATERIALS on m.PMA_MET_NUM equals t.MET_NUM into tmpMat
                                  from t in tmpMat.DefaultIfEmpty()                                 
                                  where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                  orderby m.PMA_TRIP_NO, m.PMA_ITEM_NO
                                  select new
                                  {
                                      PMA_TRIP_NO = m.PMA_TRIP_NO,
                                      PMA_ITEM_NO = m.PMA_ITEM_NO,
                                      PMA_MET_NUM = m.PMA_MET_NUM,
                                      PMA_MET_NAME = t.MET_MAT_DES_ENGLISH,
                                      PMA_SUPPLIER = m.PMA_SUPPLIER,
                                      PMA_FORMULA = m.PMA_FORMULA,
                                      PMA_CONTRACT_TYPE = m.PMA_CONTRACT_TYPE,
                                      PMA_INCOTERMS = m.PMA_INCOTERMS,
                                      PMA_GT_C = m.PMA_GT_C,
                                      PMA_PURCHASE_TYPE = m.PMA_PURCHASE_TYPE,
                                      PMA_STORAGE_LOCATION = m.PMA_STORAGE_LOCATION,
                                      PMA_ORIGIN_COUNTRY = m.PMA_ORIGIN_COUNTRY,
                                      PMA_VOLUME_MT = m.PMA_VOLUME_MT,
                                      PMA_VOLUME_BBL = m.PMA_VOLUME_BBL,
                                      PMA_VOLUME_ML = m.PMA_VOLUME_ML,
                                      PMA_VOLUME_L15 = m.PMA_VOLUME_L15,
                                      PMA_VOLUME_LTON = m.PMA_VOLUME_LTON,
                                      PMA_VOLUME_KG = m.PMA_VOLUME_KG,
                                      PMA_PURCHASE_UNIT = m.PMA_PURCHASE_UNIT,
                                      PMA_TOLERANCE = m.PMA_TOLERANCE,
                                      PMA_NOMINATED_FROM = m.PMA_NOMINATED_FROM,
                                      PMA_NOMINATED_TO = m.PMA_NOMINATED_TO,
                                      PMA_RECEIVED_FROM = m.PMA_RECEIVED_FROM,
                                      PMA_RECEIVED_TO = m.PMA_RECEIVED_TO,
                                      PMA_BL_DATE = m.PMA_BL_DATE,
                                      PMA_COMPLETE_TIME = m.PMA_COMPLETE_TIME,
                                      PMA_DUE_DATE = m.PMA_DUE_DATE,
                                      PMA_BSW = m.PMA_BSW,
                                      PMA_API = m.PMA_API,
                                      PMA_DENSITY = m.PMA_DENSITY,
                                      PMA_TEMP = m.PMA_TEMP,
                                      PMA_REMARK = m.PMA_REMARK,
                                      PMA_MEMO_NO = m.PMA_MEMO_NO,
                                      PMA_PO_NO = m.PMA_PO_NO,
                                      PMA_LOADING_PORT = m.PMA_LOADING_PORT,
                                      PMA_LOADING_PORT2 = m.PMA_LOADING_PORT2,
                                      PMA_LOADING_DATE_FROM = m.PMA_LOADING_DATE_FROM,
                                      PMA_LOADING_DATE_TO = m.PMA_LOADING_DATE_TO,
                                      PMA_DISCHARGE_LAYCAN_FROM = m.PMA_DISCHARGE_LAYCAN_FROM,
                                      PMA_DISCHARGE_LAYCAN_TO = m.PMA_DISCHARGE_LAYCAN_TO,
                                      PMA_CREATED_DATE = m.PMA_CREATED_DATE,
                                      PMA_CREATED_BY = m.PMA_CREATED_BY,
                                      PMA_UPDATED_DATE = m.PMA_UPDATED_DATE,
                                      PMA_UPDATED_BY = m.PMA_UPDATED_BY,
                                      PMA_GIT_NO = m.PMA_GIT_NO,
                                      PMA_STATUS = m.PMA_STATUS,
                                      PMA_PAYMENTTERM = m.PMA_PAYMENTTERM,
                                      PMA_PO_MAT_ITEM = m.PMA_PO_MAT_ITEM,
                                      PMA_PO_FREIGHT_ITEM = m.PMA_PO_FREIGHT_ITEM,
                                      PMA_PO_INSURANCE_ITEM = m.PMA_PO_INSURANCE_ITEM,
                                      PMA_PO_CUSTOMS_ITEM = m.PMA_PO_CUSTOMS_ITEM,
                                      PMA_BL_STATUS = m.PMA_BL_STATUS,
                                      CUSTOMS_PRICE = c.CUSTOMS_PRICE,
                                      FREIGHT_AMOUNT = c.FREIGHT_AMOUNT,
                                      INSURANCE_AMOUNT = c.INSURANCE_AMOUNT,
                                      //INSURANCE_RATE = (c.INSURANCE_RATE == null) ? "" : c.INSURANCE_RATE.ToString(),
                                      INSURANCE_RATE = c.INSURANCE_RATE,
                                      PMA_BECHMARK = m.PMA_BECHMARK,
                                      PMA_HOLIDAY = m.PMA_HOL_TYPE,
                                      CST_INVOICE_STATUS = c.INVOICE_STATUS,
                                      GROSS_QTY_BBL = m.PMA_GROSS_QTY_BBL,
                                      GROSS_QTY_MT = m.PMA_GROSS_QTY_MT,
                                      GROSS_QTY_ML = m.PMA_GROSS_QTY_ML,
                                      PMA_SUPPILER_SEARCHTERM = v.VND_NAME1,
                                      PO_CURRENCY = m.PMA_REMARK,
                                      PMA_FREIGHT_PRICE = m.PMA_FREIGHT_PRICE,
                                      PMA_INSURANCE_PRICE = m.PMA_INSURANCE_PRICE,
                                      PMA_IMPORTDUTY_PRICE = m.PMA_IMPORTDUTY_PRICE,
                                      CST_BL_DATE = c.BL_DATE,
                                      CST_QTY_UNIT = m.PMA_QTY_UNIT,
                                      PMA_CDA_ROW_ID = m.PMA_CDA_ROW_ID,
                                  }).ToList();

                    if (queryM != null)
                    {
                        foreach (var h in queryM)
                        {
                            pModel.crude_approve_detail.dDetail_Material.Add(
                                             new MaterialViewModel
                                             {
                                                 PMA_TRIP_NO = h.PMA_TRIP_NO,
                                                 PMA_ITEM_NO = h.PMA_ITEM_NO.ToString(),
                                                 PMA_MET_NUM = h.PMA_MET_NUM,
                                                 PMA_SUPPLIER = h.PMA_SUPPLIER,
                                                 PMA_FORMULA = h.PMA_FORMULA,
                                                 PMA_CONTRACT_TYPE = h.PMA_CONTRACT_TYPE,
                                                 PMA_INCOTERMS = h.PMA_INCOTERMS,
                                                 PMA_GT_C = h.PMA_GT_C,
                                                 PMA_PURCHASE_TYPE = (h.PMA_PURCHASE_TYPE == null) ? "" : h.PMA_PURCHASE_TYPE.ToString(),
                                                 PMA_STORAGE_LOCATION = h.PMA_STORAGE_LOCATION,
                                                 PMA_ORIGIN_COUNTRY = h.PMA_ORIGIN_COUNTRY,
                                                 PMA_VOLUME_BBL = (h.PMA_VOLUME_BBL == null) ? "" : h.PMA_VOLUME_BBL.ToString(),
                                                 PMA_VOLUME_MT = (h.PMA_VOLUME_MT == null) ? "" : h.PMA_VOLUME_MT.ToString(),
                                                 PMA_VOLUME_ML = (h.PMA_VOLUME_ML == null) ? "" : h.PMA_VOLUME_ML.ToString(),
                                                 PMA_VOLUME_L15 = (h.PMA_VOLUME_L15 == null) ? "" : h.PMA_VOLUME_L15.ToString(),
                                                 PMA_VOLUME_LTON = (h.PMA_VOLUME_LTON == null) ? "" : h.PMA_VOLUME_LTON.ToString(),
                                                 PMA_VOLUME_KG = (h.PMA_VOLUME_KG == null) ? "" : h.PMA_VOLUME_KG.ToString(),
                                                 PMA_PURCHASE_UNIT = h.PMA_PURCHASE_UNIT,
                                                 PMA_TOLERANCE = h.PMA_TOLERANCE,
                                                 PMA_NOMINATED_FROM = (h.PMA_NOMINATED_FROM != null) ? Convert.ToDateTime(h.PMA_NOMINATED_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PMA_NOMINATED_TO).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_NOMINATED_TO = "",
                                                 PMA_RECEIVED_FROM = (h.PMA_RECEIVED_FROM != null) ? Convert.ToDateTime(h.PMA_RECEIVED_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PMA_RECEIVED_TO).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_RECEIVED_TO = "",
                                                 PMA_BL_DATE = (h.PMA_BL_DATE != null) ? Convert.ToDateTime(h.PMA_BL_DATE).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_COMPLETE_TIME = (h.PMA_COMPLETE_TIME != null) ? Convert.ToDateTime(h.PMA_COMPLETE_TIME).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_DUE_DATE = (h.PMA_DUE_DATE != null) ? Convert.ToDateTime(h.PMA_DUE_DATE).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_BSW = (h.PMA_BSW == null) ? "" : h.PMA_BSW.ToString(),
                                                 PMA_API = (h.PMA_API == null) ? "" : h.PMA_API.ToString(),
                                                 PMA_DENSITY = (h.PMA_DENSITY == null) ? "" : h.PMA_DENSITY.ToString(),
                                                 PMA_TEMP = (h.PMA_TEMP == null) ? "" : h.PMA_TEMP.ToString(),
                                                 PMA_REMARK = h.PMA_REMARK,
                                                 PMA_MEMO_NO = h.PMA_MEMO_NO,
                                                 PMA_PO_NO = h.PMA_PO_NO,
                                                 PMA_LOADING_PORT = (h.PMA_LOADING_PORT == null) ? "" : h.PMA_LOADING_PORT.ToString(),
                                                 PMA_LOADING_PORT2 = (h.PMA_LOADING_PORT2 == null) ? "" : h.PMA_LOADING_PORT2.ToString(),
                                                 PMA_LOADING_DATE_FROM = (h.PMA_LOADING_DATE_FROM != null) ? Convert.ToDateTime(h.PMA_LOADING_DATE_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PMA_LOADING_DATE_TO).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_LOADING_DATE_TO = "",
                                                 PMA_DISCHARGE_LAYCAN_FROM = (h.PMA_DISCHARGE_LAYCAN_FROM != null) ? Convert.ToDateTime(h.PMA_DISCHARGE_LAYCAN_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PMA_DISCHARGE_LAYCAN_TO).ToString("dd/MM/yyyy", cu) : "",
                                                 PMA_DISCHARGE_LAYCAN_TO = "",
                                                 PMA_CREATED_DATE = Convert.ToDateTime(h.PMA_CREATED_DATE).ToString("dd/MM/yyyy", cu),
                                                 PMA_CREATED_BY = h.PMA_CREATED_BY,
                                                 PMA_UPDATED_DATE = Convert.ToDateTime(h.PMA_UPDATED_DATE).ToString("dd/MM/yyyy", cu),
                                                 PMA_UPDATED_BY = h.PMA_UPDATED_BY,
                                                 PMA_GIT_NO = h.PMA_GIT_NO,
                                                 PMA_STATUS = h.PMA_STATUS,
                                                 PMA_PO_MAT_ITEM = (h.PMA_PO_MAT_ITEM == null) ? "" : h.PMA_PO_MAT_ITEM.ToString(),
                                                 PMA_PO_FREIGHT_ITEM = (h.PMA_PO_FREIGHT_ITEM == null) ? "" : h.PMA_PO_FREIGHT_ITEM.ToString(),
                                                 PMA_PO_INSURANCE_ITEM = (h.PMA_PO_INSURANCE_ITEM == null) ? "" : h.PMA_PO_INSURANCE_ITEM.ToString(),
                                                 PMA_PO_CUSTOMS_ITEM = (h.PMA_PO_CUSTOMS_ITEM == null) ? "" : h.PMA_PO_CUSTOMS_ITEM.ToString(),
                                                 PMA_BL_STATUS = h.PMA_BL_STATUS,
                                                 PMA_PAYMENTTERM = (h.PMA_PAYMENTTERM == null) ? "" : h.PMA_PAYMENTTERM.ToString(),
                                                 CST_PRICE = (h.CUSTOMS_PRICE == null) ? "" : h.CUSTOMS_PRICE.ToString(),
                                                 CST_FREIGHT_AMOUNT = (h.FREIGHT_AMOUNT == null) ? "" : h.FREIGHT_AMOUNT.ToString(),
                                                 CST_INSURANCE_AMOUNT = (h.INSURANCE_AMOUNT == null) ? "" : h.INSURANCE_AMOUNT.ToString(),
                                                 CST_INSURANCE_RATE = (h.INSURANCE_RATE == null) ? "" : Convert.ToDecimal(h.INSURANCE_RATE.ToString()).ToString("0.###########"),
                                                 CST_INVOICE_STATUS = h.CST_INVOICE_STATUS,
                                                 PMA_BECHMARK = h.PMA_BECHMARK,
                                                 PMA_HOLIDAY = h.PMA_HOLIDAY,
                                                 GROSS_QTY_BBL = (h.GROSS_QTY_BBL == null) ? "" : h.GROSS_QTY_BBL.ToString(),
                                                 GROSS_QTY_MT = (h.GROSS_QTY_MT == null) ? "" : h.GROSS_QTY_MT.ToString(),
                                                 GROSS_QTY_ML = (h.GROSS_QTY_ML == null) ? "" : h.GROSS_QTY_ML.ToString(),
                                                 PMA_SUPPLIER_SEARCHTERM = h.PMA_SUPPILER_SEARCHTERM,
                                                 PO_CURRENCY = h.PO_CURRENCY,
                                                 FREIGHT_PRICE = (h.PMA_FREIGHT_PRICE == null) ? "" : h.PMA_FREIGHT_PRICE.ToString(),
                                                 INSURANCE_PRICE = (h.PMA_INSURANCE_PRICE == null) ? "" : h.PMA_INSURANCE_PRICE.ToString(),
                                                 IMPORTDUTY_PRICE = (h.PMA_IMPORTDUTY_PRICE == null) ? "" : h.PMA_IMPORTDUTY_PRICE.ToString(),
                                                 PMA_MET_NAME = h.PMA_MET_NAME,
                                                 CST_BL_DATE = (h.CST_BL_DATE != null) ? Convert.ToDateTime(h.CST_BL_DATE).ToString("dd/MM/yyyy", cu) : "",
                                                 CST_UNIT = string.IsNullOrEmpty(h.CST_QTY_UNIT) ? "" : h.CST_QTY_UNIT,
                                                 PMA_CDA_ROW_ID = string.IsNullOrEmpty(h.PMA_CDA_ROW_ID) ? "" : h.PMA_CDA_ROW_ID, 
                                             });
                        }

                    }
                    #endregion

                    #region ############# Material  Price ###################

                    pModel.crude_approve_detail.dDetail = new List<CrudeImportPlanViewModel_Detail>();
                    var queryP = (from h in context.PCF_HEADER
                                  join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO
                                  join p in context.PCF_MATERIAL_PRICE on new { tripno = m.PMA_TRIP_NO, matitem = m.PMA_ITEM_NO } equals new { tripno = p.PMP_TRIP_NO, matitem = p.PMP_MAT_ITEM_NO }
                                  join c in context.MT_MATERIALS on new { metnum = m.PMA_MET_NUM } equals new { metnum = c.MET_NUM }
                                  join v in context.MT_VEHICLE on new { Vessel = h.PHE_VESSEL } equals new { Vessel = v.VEH_ID } into tmpVess
                                  join n in context.MT_VENDOR on new { v = m.PMA_SUPPLIER } equals new { v = n.VND_ACC_NUM_VENDOR } into tmpVendor
                                  join l in context.MT_PORT on m.PMA_LOADING_PORT equals l.MLP_LOADING_PORT_ID into tmpPort
                                  join l2 in context.MT_PORT on m.PMA_LOADING_PORT2 equals l2.MLP_LOADING_PORT_ID into tmpPort2
                                  from v in tmpVess.DefaultIfEmpty()
                                  from n in tmpVendor.DefaultIfEmpty()
                                  from l in tmpPort.DefaultIfEmpty()
                                  from l2 in tmpPort2.DefaultIfEmpty()
                                  join d in context.CDP_DATA on m.PMA_CDA_ROW_ID equals d.CDA_ROW_ID into tmpCDP
                                  from td in tmpCDP.DefaultIfEmpty()
                                  where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                  orderby h.PHE_TRIP_NO, m.PMA_ITEM_NO, p.PMP_ITEM_NO
                                  select new
                                  {
                                      TripNo = h.PHE_TRIP_NO,
                                      MatItemNo = m.PMA_ITEM_NO,
                                      PriceItemNo = p.PMP_ITEM_NO,
                                      ComCode = h.PHE_COMPANY_CODE,
                                      VesselCode = h.PHE_VESSEL,
                                      VesselName = v.VEH_VEHICLE,
                                      CrudeCode = m.PMA_MET_NUM,
                                      CrudeName = c.MET_MAT_DES_ENGLISH,
                                      SupplierCode = m.PMA_SUPPLIER,
                                      SupplierName = n.VND_NAME1,
                                      CDA_ROW_ID = h.PHE_CDA_ROW_ID,
                                      PLANNING_RELEASE = h.PHE_PLANNING_RELEASE,
                                      BOOKING_RELEASE = h.PHE_BOOKING_RELEASE,
                                      CHANNEL = h.PHE_CHANNEL,
                                      GT_C = m.PMA_GT_C,
                                      FormulaPrice = m.PMA_FORMULA,
                                      Incoterms = m.PMA_INCOTERMS,
                                      PriceStatus = p.PMP_PRICE_STATUS,
                                      aYear = h.PHE_ARRIVAL_YEAR,
                                      aMonth = h.PHE_ARRIVAL_MONTH,
                                      PURCHASE_TYPE = m.PMA_PURCHASE_TYPE,
                                      CONTRACT_TYPE = m.PMA_CONTRACT_TYPE,
                                      STORAGE_LOCATION = m.PMA_STORAGE_LOCATION,
                                      ORIGIN_COUNTRY = m.PMA_ORIGIN_COUNTRY,
                                      PURCHASE_UNIT = m.PMA_PURCHASE_UNIT,
                                      TOLERANCE = m.PMA_TOLERANCE,
                                      BL_DATE = m.PMA_BL_DATE,
                                      DUE_DATE = p.PMP_DUE_DATE, // m.PMA_DUE_DATE,
                                      BL_STATUS = m.PMA_BL_STATUS,
                                      BSW = m.PMA_BSW,
                                      API = m.PMA_API,
                                      DENSITY = m.PMA_DENSITY,
                                      TEMP = m.PMA_TEMP,
                                      LOADING_PORT = m.PMA_LOADING_PORT,
                                      LOADING_PORT_TEXT = l.MLP_LOADING_PORT_NAME,
                                      LOADING_PORT2 = m.PMA_LOADING_PORT2,
                                      LOADING_PORT_TEXT2 = l2.MLP_LOADING_PORT_NAME,
                                      LOADING_DATE_FROM = m.PMA_LOADING_DATE_FROM,
                                      LOADING_DATE_TO = m.PMA_LOADING_DATE_TO,
                                      MEMO_NO = m.PMA_MEMO_NO,
                                      PO_NO = m.PMA_PO_NO,
                                      GIT_NO = m.PMA_GIT_NO,
                                      MatRemark = m.PMA_REMARK,
                                      BasePrice = p.PMP_BASE_PRICE,
                                      QtyBBL = m.PMA_VOLUME_BBL,
                                      QtyMT = m.PMA_VOLUME_MT,
                                      QtyML = m.PMA_VOLUME_ML,
                                      QtyL15 = m.PMA_VOLUME_L15,
                                      QtyKT = 0, //m.PMA_VOLUME_LTON,
                                      QtyLTON = m.PMA_VOLUME_LTON,
                                      QtyKG = m.PMA_VOLUME_KG,
                                      ExAgreement = p.PMP_FX_AGREEMENT,
                                      ExBOT = p.PMP_FX_BOT,
                                      ActualETAFrom = h.PHE_ACTUAL_ETA_FROM,
                                      ActualETATo = h.PHE_ACTUAL_ETA_TO,
                                      ApprovedDate = p.PMP_APPROVED_DATE,
                                      CertifiedDate = p.PMP_CREATED_DATE,
                                      TotalInBaht = p.PMP_TOTAL_AMOUNT_THB,
                                      NOMINATED_FROM = m.PMA_NOMINATED_FROM,
                                      NOMINATED_TO = m.PMA_NOMINATED_TO,
                                      RECEIVED_FROM = m.PMA_RECEIVED_FROM,
                                      RECEIVED_TO = m.PMA_RECEIVED_TO,
                                      DISCHARGE_LAYCAN_FROM = m.PMA_DISCHARGE_LAYCAN_FROM,
                                      DISCHARGE_LAYCAN_TO = m.PMA_DISCHARGE_LAYCAN_TO,
                                      TradingPremiumDiscount = p.PMP_TRADING_PREMIUM_DISCOUNT,
                                      OSP_PREMIUM_DISCOUNT = p.PMP_OSP_PREMIUM_DISCOUNT,
                                      OTHER_COST = p.PMP_OTHER_COST,
                                      TOTAL_PRICE = p.PMP_TOTAL_PRICE,
                                      TOTAL_AMOUNT = p.PMP_TOTAL_AMOUNT,
                                      PRICE_CURRENCY = p.PMP_CURRENCY,
                                      PREPAYMENT = p.PMP_PREPAYMENT,
                                      PREPAYMENT_CURRENCY = p.PMP_PREPAYMENT_CURRENCY,
                                      INVOICE_STATUS = p.PMP_INVOICE,
                                      PRICE_PERIOD_FROM = p.PMP_PRICE_PERIOD_FROM,
                                      PRICE_PERIOD_TO = p.PMP_PRICE_PERIOD_TO,
                                      Price_FormulaPrice = p.PMP_FORMULA,
                                      PONoCrude = m.PMA_PO_NO,
                                      MemoNo = m.PMA_MEMO_NO,
                                      InvoiceNo = p.PMP_INVOICE_NO,
                                      UnitPriceDigit = p.PMP_TOTALPRICE_DIGIT,
                                      FXDateFrom = p.PMP_FX_DATE_FROM,
                                      FxDateTo = p.PMP_FX_DATE_TO,
                                      PMP_PHET_VOL = p.PMP_PHET_VOL,
                                      PMA_PER_BOND = td.CDA_PERFORMANCE_BOND,
                                      LoadingMonth = h.PHE_LOADING_MONTH,
                                      CalTotalPrice = p.PMP_CAL_TOTAL_PRICE,
                                      OSPMonth = p.PMP_OSP_MONTH,
                                  }).ToList();

                    if (queryP != null)
                    {
                        foreach (var h in queryP)
                        {
                            DateTime? loadingDate = null;
                            if (h.Incoterms.Equals("FOB"))
                            {
                                loadingDate = h.NOMINATED_FROM;
                            }
                            else
                            {
                                loadingDate = h.LOADING_DATE_FROM;
                            }

                            string loadingMonth = "";
                            if (!string.IsNullOrEmpty(h.LoadingMonth)) {
                                 DateTime date = DateTime.ParseExact(h.LoadingMonth + "01", "yyyyMMdd", provider);
                                loadingMonth = date.ToString("MMM/yyyy");
                            }

                            pModel.crude_approve_detail.dDetail.Add(
                                           new CrudeImportPlanViewModel_Detail
                                           {
                                               ArrivalMonth = "",
                                               TripNo = h.TripNo,
                                               MatItemNo = h.MatItemNo.ToString(),
                                               MatPriceItemNo = h.PriceItemNo.ToString(),
                                               Vessel = h.VesselCode,
                                               Crude = h.CrudeCode,
                                               Supplier = h.SupplierCode,
                                               FormulaPrice = h.FormulaPrice,
                                               Incoterm = h.Incoterms,
                                               PriceStatus = h.PriceStatus,
                                               BLDate = (h.BL_DATE == null) ? "" : Convert.ToDateTime(h.BL_DATE).ToString("dd/MM/yyyy", cu),
                                               DuePaidDate = (h.DUE_DATE == null) ? "" : Convert.ToDateTime(h.DUE_DATE).ToString("dd/MM/yyyy", cu),
                                               BLQuantityBBL = (h.QtyBBL == null) ? "" : h.QtyBBL.ToString(),
                                               ProActual = (h.TOTAL_PRICE == null) ? "" : h.TOTAL_PRICE.ToString(),
                                               TotalAmtUSDBAHT = (h.TOTAL_AMOUNT == null) ? "" : h.TOTAL_AMOUNT.ToString(),
                                               ExAgreement = (h.ExAgreement == null) ? "" : h.ExAgreement.ToString(),
                                               ExBOT = (h.ExBOT == null) ? "" : h.ExBOT.ToString(),
                                               TotalInBaht = (h.TotalInBaht == null) ? "" : h.TotalInBaht.ToString(),
                                               CertifiedDate = (h.CertifiedDate == null) ? "" : Convert.ToDateTime(h.CertifiedDate).ToString("dd/MM/yyyy", cu),
                                               ApprovedDate = (h.ApprovedDate == null) ? "" : Convert.ToDateTime(h.ApprovedDate).ToString("dd/MM/yyyy", cu),
                                               QTYKBBL = (h.QtyBBL == null) ? "" : h.QtyBBL.ToString(),
                                               QTYKT = (h.QtyKT == null) ? "" : h.QtyKT.ToString(),
                                               Tolerance = h.TOLERANCE,
                                               LoadingPort = (h.LOADING_PORT == null) ? "" : h.LOADING_PORT.ToString(),
                                               h_LoadPortText = (h.LOADING_PORT_TEXT == null) ? "" : h.LOADING_PORT_TEXT.ToString(),
                                               h_LoadPortText2 = (h.LOADING_PORT_TEXT2 == null) ? "" : h.LOADING_PORT_TEXT2.ToString(),
                                               LDNominated = (h.NOMINATED_FROM == null) ? "" : Convert.ToDateTime(h.NOMINATED_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.NOMINATED_TO).ToString("dd/MM/yyyy", cu),
                                               LDReceived = (h.RECEIVED_FROM == null) ? "" : Convert.ToDateTime(h.RECEIVED_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.RECEIVED_TO).ToString("dd/MM/yyyy", cu),
                                               DischargeLayCan = (h.DISCHARGE_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(h.DISCHARGE_LAYCAN_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.DISCHARGE_LAYCAN_TO).ToString("dd/MM/yyyy", cu),
                                               ActualETA = (h.ActualETAFrom == null) ? "" : Convert.ToDateTime(h.ActualETAFrom).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.ActualETATo).ToString("dd/MM/yyyy", cu),
                                               GT_C = h.GT_C,
                                               BLReceived = h.BL_STATUS,
                                               PlanningRelease = h.PLANNING_RELEASE,
                                               BookingRelease = h.BOOKING_RELEASE,
                                               i_CrudeCode = h.CrudeCode,
                                               i_SupplierCode = h.SupplierCode,
                                               h_com_code = h.ComCode,
                                               h_CDA_ROW_ID = h.CDA_ROW_ID,
                                               h_refID = h.CDA_ROW_ID,
                                               ID = "",
                                               h_TripNo = h.TripNo,
                                               h_ItemNo = h.MatItemNo.ToString(),
                                               h_ArraivalDate = "",
                                               h_Year = (h.aYear == null) ? "" : h.aYear.ToString(),
                                               h_Month = (h.aMonth == null) ? "" : h.aMonth.ToString(),
                                               h_Supplier = h.SupplierName,
                                               h_FormulaPirce = h.FormulaPrice,
                                               h_Incoterm = h.Incoterms,
                                               h_GT_C = h.GT_C,
                                               h_Tolerance = h.TOLERANCE,
                                               h_Channel = h.CHANNEL,
                                               h_Vessel = h.VesselCode,
                                               h_VesselName = h.VesselName,
                                               h_MET_NUM = h.CrudeCode,
                                               h_CrudeName = h.CrudeName,
                                               h_ContractType = (h.CONTRACT_TYPE == null) ? "" : h.CONTRACT_TYPE.ToString(),
                                               h_PurchaseType = (h.PURCHASE_TYPE == null) ? "" : h.PURCHASE_TYPE.ToString(),
                                               h_StorageLocation = h.STORAGE_LOCATION,
                                               h_OriginCountry = h.ORIGIN_COUNTRY,
                                               h_PurchaseUnit = h.PURCHASE_UNIT,
                                               h_NorminateDate = (h.NOMINATED_FROM == null) ? "" : Convert.ToDateTime(h.NOMINATED_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.NOMINATED_TO).ToString("dd/MM/yyyy", cu),
                                               h_ReceiveDate = (h.RECEIVED_FROM == null) ? "" : Convert.ToDateTime(h.RECEIVED_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.RECEIVED_TO).ToString("dd/MM/yyyy", cu),
                                               h_BLDate = (h.BL_DATE == null) ? "" : Convert.ToDateTime(h.BL_DATE).ToString("dd/MM/yyyy", cu),
                                               h_CompleteTime = "",
                                               h_DueDate = (h.DUE_DATE == null) ? "" : Convert.ToDateTime(h.DUE_DATE).ToString("dd/MM/yyyy", cu),
                                               h_BSW = (h.BSW == null) ? "" : h.BSW.ToString(),
                                               h_API = (h.API == null) ? "" : h.API.ToString(),
                                               h_Density = (h.DENSITY == null) ? "" : h.DENSITY.ToString(),
                                               h_Temp = (h.TEMP == null) ? "" : h.TEMP.ToString(),
                                               h_Remark = h.MatRemark,
                                               h_MemoNo = h.MEMO_NO,
                                               h_PONo = h.PO_NO,
                                               h_LoadPort = (h.LOADING_PORT == null) ? "" : h.LOADING_PORT.ToString(),
                                               h_LoadPort2 = (h.LOADING_PORT2 == null) ? "" : h.LOADING_PORT2.ToString(),
                                               h_LodingDate = (h.LOADING_DATE_FROM == null) ? "" : Convert.ToDateTime(h.LOADING_DATE_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.LOADING_DATE_TO).ToString("dd/MM/yyyy", cu),
                                               h_DischargeLayCanDate = (h.DISCHARGE_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(h.DISCHARGE_LAYCAN_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.DISCHARGE_LAYCAN_TO).ToString("dd/MM/yyyy", cu),
                                               h_GitNo = string.IsNullOrEmpty(h.GIT_NO) || h.GIT_NO.Equals("0") ? "" : h.GIT_NO,
                                               h_QTY_BBL = (h.QtyBBL == null) ? "" : h.QtyBBL.ToString(),
                                               h_QTY_Kbbl = (h.QtyBBL == null) ? "" : (h.QtyBBL * 1000).ToString(),
                                               h_QTY_MT = (h.QtyMT == null) ? "" : h.QtyMT.ToString(),
                                               h_QTY_ML = (h.QtyML == null) ? "" : h.QtyML.ToString(),
                                               h_QTY_L15 = (h.QtyL15 == null) ? "" : h.QtyL15.ToString(),
                                               h_QTY_LTON = (h.QtyLTON == null) ? "" : h.QtyLTON.ToString(),
                                               h_QTY_KT = (h.QtyBBL == null) ? "" : (h.QtyBBL * 1000 * 65 / 500).ToString(),
                                               h_QTY_KG = (h.QtyKG == null) ? "" : h.QtyKG.ToString(),
                                               h_Actual_ETA = (h.ActualETAFrom == null) ? "" : Convert.ToDateTime(h.ActualETAFrom).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.ActualETATo).ToString("dd/MM/yyyy", cu),
                                               i_ItemNo = h.MatItemNo.ToString(),
                                               i_SubItemNo = h.PriceItemNo.ToString(),
                                               i_PriceStatus = h.PriceStatus,
                                               i_Formula = h.FormulaPrice,
                                               i_BasePrice = (h.BasePrice == null) ? "" : h.BasePrice.ToString(),
                                               i_BP_Rounding = "",
                                               i_OSP_PremiumDiscount = (h.OSP_PREMIUM_DISCOUNT == null) ? "" : h.OSP_PREMIUM_DISCOUNT.ToString(),
                                               i_OSP_Rounding = "",
                                               i_TradingPremiumDiscount = (h.TradingPremiumDiscount == null) ? "" : h.TradingPremiumDiscount.ToString(),
                                               i_TRD_ROUNDING = "",
                                               i_OTHER_COST = (h.OTHER_COST == null) ? "" : h.OTHER_COST.ToString(),
                                               i_OC_ROUNDING = "",
                                               i_TOTAL_PRICE = (h.TOTAL_PRICE == null) ? "" : h.TOTAL_PRICE.ToString(),
                                               i_TP_ROUNDING = "",
                                               i_TOTAL_AMOUNT = (h.TOTAL_AMOUNT == null) ? "" : h.TOTAL_AMOUNT.ToString(),
                                               i_TA_ROUNDING = "",
                                               i_CURRENCY = h.PRICE_CURRENCY,
                                               i_FX_AGREEMENT = (h.ExAgreement == null) ? "" : h.ExAgreement.ToString(),
                                               i_FXA_ROUNDING = "",
                                               i_FX_BOT = (h.ExBOT == null) ? "" : h.ExBOT.ToString(),
                                               i_FXB_ROUNDING = "",
                                               i_TOTAL_AMOUNT_THB = (h.TotalInBaht == null) ? "" : h.TotalInBaht.ToString(),
                                               i_CERTIFIED_DATE = (h.CertifiedDate == null) ? "" : Convert.ToDateTime(h.CertifiedDate).ToString("dd/MM/yyyy", cu),
                                               i_APPROVED_DATE = (h.ApprovedDate == null) ? "" : Convert.ToDateTime(h.ApprovedDate).ToString("dd/MM/yyyy", cu),
                                               i_PREPAYMENT = (h.PREPAYMENT == null) ? "" : h.PREPAYMENT.ToString(),
                                               i_PREPAYMENT_CURRENCY = h.PREPAYMENT_CURRENCY,
                                               i_INVOICE = h.INVOICE_STATUS,
                                               i_BLStatus = h.BL_STATUS,
                                               i_RecordStatus = "",
                                               i_MatPrice = h.Price_FormulaPrice,
                                               i_PricePeriod = (h.PRICE_PERIOD_FROM == null) ? "" : Convert.ToDateTime(h.PRICE_PERIOD_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PRICE_PERIOD_TO).ToString("dd/MM/yyyy", cu),
                                               i_PriceFor = h.PURCHASE_UNIT,
                                               LoadingMonth = loadingMonth, //(loadingDate == null) ? "" : Convert.ToDateTime(loadingDate).ToString("MMM", cu) + "/" + Convert.ToDateTime(loadingDate).ToString("yyyy", cu),
                                               PONoCrude = h.PONoCrude,
                                               MemoNo = h.MemoNo,
                                               INVOICE_NO = string.IsNullOrEmpty(h.InvoiceNo) ? "" : h.InvoiceNo,
                                               PriceUpdateDate = "",
                                               UnitPriceDigit = (h.UnitPriceDigit == null) ? "3" : h.UnitPriceDigit.ToString(),
                                               FXPeriodDate = (h.FXDateFrom == null) ? "" : Convert.ToDateTime(h.FXDateFrom).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.FxDateTo).ToString("dd/MM/yyyy", cu),
                                               PMP_PHET_VOL = (h.PMP_PHET_VOL == null) ? "" : h.PMP_PHET_VOL.ToString(),
                                               PerformanceBond = h.PMA_PER_BOND ?? "",
                                               CalTotalPrice = (h.CalTotalPrice == null) ? "" : h.CalTotalPrice.ToString(),
                                               OSP_MONTH_PRICE = h.OSPMonth ?? "",
                                           });
                        }
                    }
                    #endregion

                    #region ############# Daily  Price ###################

                    pModel.crude_approve_detail.dDetail_DailyPrice = new List<DailyPriceViewModel>();
                    var queryDaily = (from h in context.PCF_HEADER
                                      join m in context.PCF_MATERIAL_PRICE_DAILY on h.PHE_TRIP_NO equals m.TRIP_NO
                                      where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                            && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                      orderby h.PHE_TRIP_NO, m.BL_DATE
                                      select m).ToList();

                    if (queryP != null)
                    {
                        foreach (var h in queryDaily)
                        {
                            pModel.crude_approve_detail.dDetail_DailyPrice.Add(new DailyPriceViewModel
                            {
                                TRIP_NO = h.TRIP_NO,
                                MAT_NUM = h.MAT_NUM,
                                INVOICE_NO = h.INVOICE_NO,
                                BL_DATE = Convert.ToDateTime(h.BL_DATE).ToString("dd/MM/yyyy", cu),
                                PRO_PRICE = h.PRO_PRICE,
                                FINAL_PRICE = h.FINAL_PRICE,
                                DIFF_PRICE = h.DIFF_PRICE,
                                OUTTURN_QTY = h.OUTTURN_QTY,
                                P1_FX_RATE = h.P1_FX_RATE,
                                P1_UNIT_PRICE = h.P1_UNIT_PRICE,
                                P1_TOTAL_BF_VAT = h.P1_TOTAL_BF_VAT,
                                P1_VAT = h.P1_VAT,
                                P1_TOTAL_AF_VAT = h.P1_TOTAL_AF_VAT,
                                P2_FX_RATE = h.P2_FX_RATE,
                                P2_UNIT_PRICE = h.P2_UNIT_PRICE,
                                P2_DIFF_UNITPRICE = h.P2_DIFF_UNITPRICE,
                                P2_ADJUST_PRICE = h.P2_ADJUST_PRICE,
                                P2_TOTAL_DIFF = h.P2_TOTAL_DIFF,
                                P2_DIFF_DOCNO = h.P2_DIFF_DOCNO,
                                F1_FX_RATE = h.F1_FX_RATE,
                                F1_UNIT_PRICE = h.F1_UNIT_PRICE,
                                F1_TOTAL_BF_VAT = h.F1_TOTAL_BF_VAT,
                                F1_VAT = h.F1_VAT,
                                F1_TOTAL_AF_VAT = h.F1_TOTAL_AF_VAT,
                                F2_FX_RATE = h.F2_FX_RATE,
                                F2_TOTAL_DIFF1 = h.F2_TOTAL_DIFF1,
                                F2_TOTAL_DIFF2 = h.F2_TOTAL_DIFF2,
                                F2_TOTAL_DIFF3 = h.F2_TOTAL_DIFF3,

                            });

                        }
                    }
                    #endregion

                    #region ############# Freight ###################
                    var queryFile = (from h in context.PCF_HEADER
                                     join m in context.PCF_ATTACH_FILE on h.PHE_TRIP_NO equals m.PAF_TRIP_NO
                                     where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                          && (h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD"))))
                                          && (m.PAF_FILE_FOR.Equals("FREIGHT"))
                                     orderby h.PHE_TRIP_NO, m.PAF_FILE_FOR, m.PAF_ITEM_NO, m.PAF_NO
                                     select new
                                     {
                                         PAF_ITEM_NO = m.PAF_ITEM_NO,
                                         PAF_NO = m.PAF_NO,
                                         PAF_TRIP_NO = m.PAF_TRIP_NO,
                                         PAF_FILE_NAME = m.PAF_FILE_NAME,
                                         PAF_PATH = m.PAF_PATH,
                                         PAF_DESCRIPTION = m.PAF_DESCRIPTION,
                                         PAF_FILE_FOR = m.PAF_FILE_FOR,
                                     }).ToList();

                    pModel.crude_approve_detail.dDetail_FreightFile = new List<FileViewModel>();

                    foreach (var itemFile in queryFile)
                    {
                        string itemNo = (itemFile.PAF_ITEM_NO == null) ? "" : itemFile.PAF_ITEM_NO.ToString();
                        string no = itemFile.PAF_NO == null ? "" : itemFile.PAF_NO.ToString();
                        string refid = itemNo;// + no;

                        pModel.crude_approve_detail.dDetail_FreightFile.Add(new FileViewModel
                        {
                            PAF_TRIP_NO = itemFile.PAF_TRIP_NO ?? "",
                            PAF_ITEM_NO = itemNo,
                            PAF_NO = no,
                            PAF_RED_ID = refid,
                            PAF_FILE_NAME = itemFile.PAF_FILE_NAME ?? "",
                            PAF_PATH = itemFile.PAF_PATH ?? "",
                            PAF_DESCRIPTION = itemFile.PAF_DESCRIPTION ?? "",
                            PAF_FILE_FOR = itemFile.PAF_FILE_FOR ?? "",
                            PAF_INDB_STS = "1",
                            PAF_STATUS = "",
                            PAF_DESC_TEMP = itemFile.PAF_DESCRIPTION ?? "",
                        });
                    }


                    var queryFM = (from h in context.PCF_HEADER
                                   join m in context.PCF_FREIGHT_MATERIAL on h.PHE_TRIP_NO equals m.PFM_TRIP_NO
                                   join t in context.MT_MATERIALS on m.PFM_MET_NUM equals t.MET_NUM
                                   where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                   orderby m.PFM_TRIP_NO, m.PFM_ITEM_NO
                                   select new
                                   {
                                       PFM_TRIP_NO = m.PFM_TRIP_NO,
                                       PFM_ITEM_NO = m.PFM_ITEM_NO,
                                       PFM_MET_NUM = m.PFM_MET_NUM,
                                       PFM_MET_NAME = t.MET_MAT_DES_ENGLISH
                                   }).ToList();


                    pModel.crude_approve_detail.dDetail_Freight = new List<FreightViewModel>();
                    var queryF = (from h in context.PCF_HEADER
                                  join m in context.PCF_FREIGHT on h.PHE_TRIP_NO equals m.PFR_TRIP_NO
                                  join v in context.MT_VENDOR on m.PFR_SUPPLIER equals v.VND_ACC_NUM_VENDOR into tmpVendor
                                  from v in tmpVendor.DefaultIfEmpty()
                                  join p in context.MT_PORT on m.PFR_PORT equals p.MLP_LOADING_PORT_ID into tmpPort
                                  from p in tmpPort.DefaultIfEmpty()
                                  join f in context.PCF_MT_FREIGHT_TYPE on m.PFR_FREIGHT_SUB_TYPE equals f.PMF_CODE into tmpFType
                                  from f in tmpFType.DefaultIfEmpty()
                                  join b in context.MT_VENDOR on m.PFR_BROKER equals b.VND_ACC_NUM_VENDOR into tmpBroker
                                  from b in tmpBroker.DefaultIfEmpty()
                                  where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                  orderby m.PFR_TRIP_NO, m.PFR_ITEM_NO, m.PFR_CHARTERING_TYPE, m.PFR_FREIGHT_TYPE
                                  select new
                                  {
                                      PFR_TRIP_NO = m.PFR_TRIP_NO,
                                      PFR_ITEM_NO = m.PFR_ITEM_NO,
                                      PFR_CHARTERING_TYPE = m.PFR_CHARTERING_TYPE,
                                      PFR_FREIGHT_TYPE = m.PFR_FREIGHT_TYPE,
                                      PFR_SUPPLIER = m.PFR_SUPPLIER,
                                      PFR_BROKER = m.PFR_BROKER,
                                      PFR_BROKER_TEXT = b.VND_NAME1,
                                      PFR_LAYTIME = m.PFR_LAYTIME,
                                      PFR_WORLD_SCALE = m.PFR_WORLD_SCALE,
                                      PFR_MINIMUM_LOADABLE = m.PFR_MINIMUM_LOADABLE,
                                      PFR_DEMURRAGE = m.PFR_DEMURRAGE,
                                      PFR_WAR_RISK_PREMIUM = m.PFR_WAR_RISK_PREMIUM,
                                      PFR_PRICE_PER_TON = m.PFR_PRICE_PER_TON,
                                      PFR_AMOUNT = m.PFR_AMOUNT,
                                      PFR_CURRENCY = m.PFR_CURRENCY,
                                      PFR_FX_AGREEMENT = m.PFR_FX_AGREEMENT,
                                      PFR_FX_BOT = m.PFR_FX_BOT,
                                      PFR_CERTIFIED_DATE = m.PFR_CERTIFIED_DATE,
                                      PFR_APPROVED_DATE = m.PFR_APPROVED_DATE,
                                      PFR_REMARK = m.PFR_REMARK,
                                      PFR_MEMO_NO = m.PFR_MEMO_NO,
                                      PFR_PO_NO = m.PFR_PO_NO,
                                      PFR_FREIGHT_SUB_TYPE = m.PFR_FREIGHT_SUB_TYPE,
                                      PFR_PRODUCT = m.PFR_PRODUCT,
                                      PFR_PORT = m.PFR_PORT,
                                      OwnerText = v.VND_NAME1,
                                      PortText = p.MLP_LOADING_PORT_NAME,
                                      CostTypeText = f.PMF_NAME,
                                      InvoiceSTS = m.PFR_INVOICE_STATUS,
                                      Volume = m.PFR_VOLUME,
                                      VolumeUnit = m.PFR_VOLUME_UNIT,
                                      PFR_PERIOD_FROM = m.PFR_PERIOD_FROM,
                                      PFR_PERIOD_TO = m.PFR_PERIOD_TO,
                                      PFR_TOTAL_AMOUNT_THB = m.PFR_TOTAL_AMOUNT_THB,
                                      FR_DueDate = m.PFR_DUE_DATE,
                                      PFR_PRICE_UNIT = m.PFR_PRICE_UNIT,
                                      InvoiceNo = m.PFR_INVOICE_NO,
                                  }).ToList();

                    if (queryF != null)
                    {
                        foreach (var h in queryF)
                        {
                            string sFreightMat = "";
                            string sFreightMatName = "";
                            var SubItem = queryFM.Where(t => t.PFM_ITEM_NO == h.PFR_ITEM_NO && t.PFM_TRIP_NO == h.PFR_TRIP_NO);
                            foreach (var item in SubItem)
                            {
                                sFreightMat += "," + item.PFM_MET_NUM;
                                sFreightMatName += "," + item.PFM_MET_NAME;
                            }

                            if (sFreightMat.Length > 0)
                            {
                                sFreightMat = sFreightMat.Substring(1);
                                sFreightMatName = sFreightMatName.Substring(1);
                            }

                            pModel.crude_approve_detail.dDetail_Freight.Add(
                                             new FreightViewModel
                                             {
                                                 FR_TripNo = h.PFR_TRIP_NO,
                                                 FR_ItemNo = h.PFR_ITEM_NO.ToString(),
                                                 FR_CharteringType = h.PFR_CHARTERING_TYPE,
                                                 FR_CharteringTypeText = getTextChartering(_CIPConfig, h.PFR_CHARTERING_TYPE),
                                                 FR_FreightType = (h.PFR_FREIGHT_TYPE == null) ? "" : h.PFR_FREIGHT_TYPE.ToString(),
                                                 FR_FreightTypeText = getTextFreightType(_CIPConfig, (h.PFR_FREIGHT_TYPE == null) ? "" : h.PFR_FREIGHT_TYPE.ToString()),
                                                 FR_Owner = h.PFR_SUPPLIER,
                                                 FR_OwnerText = h.OwnerText,
                                                 FR_Broker = h.PFR_BROKER,
                                                 FR_BrokerText = h.PFR_BROKER_TEXT,
                                                 FR_LayTime = h.PFR_LAYTIME,
                                                 FR_WorldScale = (h.PFR_WORLD_SCALE == null) ? "" : h.PFR_WORLD_SCALE.ToString(),
                                                 FR_MinLoadable = (h.PFR_MINIMUM_LOADABLE == null) ? "" : h.PFR_MINIMUM_LOADABLE.ToString(),
                                                 FR_Demurrange = (h.PFR_DEMURRAGE == null) ? "" : h.PFR_DEMURRAGE.ToString(),
                                                 FR_WarRiskPremium = (h.PFR_WAR_RISK_PREMIUM == null) ? "" : h.PFR_WAR_RISK_PREMIUM.ToString(),
                                                 FR_Price = (h.PFR_PRICE_PER_TON == null) ? "" : h.PFR_PRICE_PER_TON.ToString(),
                                                 FR_Amt = (h.PFR_AMOUNT == null) ? "" : h.PFR_AMOUNT.ToString(),
                                                 FR_AmtUnit = h.PFR_CURRENCY,
                                                 //PFR_CF_CURRENCY = h.PFR_CF_CURRENCY,
                                                 //PFR_DUE_DATE = h.PFR_DUE_DATE,
                                                 FR_ExcAgree = (h.PFR_FX_AGREEMENT == null) ? "" : h.PFR_FX_AGREEMENT.ToString(),
                                                 FR_ExcBOT = (h.PFR_FX_BOT == null) ? "" : h.PFR_FX_BOT.ToString(),
                                                 //FR_Amt = (h.PFR_TOTAL_AMOUNT_THB == null) ? "" : h.PFR_TOTAL_AMOUNT_THB.ToString(),
                                                 FR_CerDate = (h.PFR_CERTIFIED_DATE == null) ? "" : Convert.ToDateTime(h.PFR_CERTIFIED_DATE).ToString("dd/MM/yyyy", cu),
                                                 FR_ApproveDate = (h.PFR_APPROVED_DATE == null) ? "" : Convert.ToDateTime(h.PFR_APPROVED_DATE).ToString("dd/MM/yyyy", cu),
                                                 FR_ExcCertiDate = (h.PFR_CERTIFIED_DATE == null) ? "" : Convert.ToDateTime(h.PFR_CERTIFIED_DATE).ToString("dd/MM/yyyy", cu),
                                                 FR_ExcApproveDate = (h.PFR_APPROVED_DATE == null) ? "" : Convert.ToDateTime(h.PFR_APPROVED_DATE).ToString("dd/MM/yyyy", cu),
                                                 FR_ExcTotal = (h.PFR_TOTAL_AMOUNT_THB == null) ? "" : h.PFR_TOTAL_AMOUNT_THB.ToString(),
                                                 FR_Remark = h.PFR_REMARK,
                                                 FR_MemoNo = h.PFR_MEMO_NO,
                                                 FR_PONo = h.PFR_PO_NO,
                                                 //PFR_CREATED_DATE = h.PFR_CREATED_DATE,
                                                 //PFR_CREATED_BY = h.PFR_CREATED_BY,
                                                 //PFR_UPDATED_DATE = h.PFR_UPDATED_DATE,
                                                 //PFR_UPDATED_BY = h.PFR_UPDATED_BY,
                                                 FR_CostType = (h.PFR_FREIGHT_SUB_TYPE == null) ? "" : h.PFR_FREIGHT_SUB_TYPE.ToString(),
                                                 FR_CostTypeText = h.CostTypeText,
                                                 FR_Product = (h.PFR_PRODUCT == null) ? "" : h.PFR_PRODUCT.ToString(),
                                                 FR_ProductText = (h.PFR_PRODUCT == null) ? "" : h.PFR_PRODUCT.ToString(),
                                                 FR_Port = (h.PFR_PORT == null) ? "" : h.PFR_PORT.ToString(),
                                                 FR_PortText = h.PortText,
                                                 //PFR_PERIOD_FROM = h.PFR_PERIOD_FROM,
                                                 //PFR_PERIOD_TO = h.PFR_PERIOD_TO,
                                                 //PFR_VOLUME = h.PFR_VOLUME,
                                                 //PFR_VOLUME_UNIT = h.PFR_VOLUME_UNIT,
                                                 //PFR_PRICE_PER_BBL = h.PFR_PRICE_PER_BBL,
                                                 FR_InvoiceSTS = h.InvoiceSTS,
                                                 FR_Volume = (h.Volume == null) ? "" : h.Volume.ToString(),
                                                 FR_VolumeUnit = h.VolumeUnit,
                                                 FR_PeriodDate = (h.PFR_PERIOD_FROM == null || h.PFR_PERIOD_TO == null) ? "" : Convert.ToDateTime(h.PFR_PERIOD_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PFR_PERIOD_TO).ToString("dd/MM/yyyy", cu),
                                                 FR_DueDate = (h.FR_DueDate == null) ? "" : Convert.ToDateTime(h.FR_DueDate).ToString("dd/MM/yyyy", cu),
                                                 FR_MatCodeList = sFreightMat,
                                                 FR_MatNameList = sFreightMatName,
                                                 FR_PriceUnit = string.IsNullOrEmpty(h.PFR_PRICE_UNIT) ? "" : h.PFR_PRICE_UNIT,
                                                 FR_PriceUnitText = string.IsNullOrEmpty(h.PFR_PRICE_UNIT) ? "" : h.PFR_PRICE_UNIT,
                                                 FR_InvoiceNo = string.IsNullOrEmpty(h.InvoiceNo) ? "" : h.InvoiceNo,
                                                 FR_RefId = h.PFR_ITEM_NO.ToString(),
                                             });
                        }
                    }
                    #endregion

                    #region  ############# Customs ###################
                    var queryCustomsFile = (from h in context.PCF_HEADER
                                            join m in context.PCF_ATTACH_FILE on h.PHE_TRIP_NO equals m.PAF_TRIP_NO
                                            where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                                 && (h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD"))))
                                                 && (m.PAF_FILE_FOR.Equals("CUSTOMS"))
                                            orderby h.PHE_TRIP_NO, m.PAF_FILE_FOR, m.PAF_ITEM_NO, m.PAF_NO
                                            select new
                                            {
                                                PAF_ITEM_NO = m.PAF_ITEM_NO,
                                                PAF_NO = m.PAF_NO,
                                                PAF_TRIP_NO = m.PAF_TRIP_NO,
                                                PAF_FILE_NAME = m.PAF_FILE_NAME,
                                                PAF_PATH = m.PAF_PATH,
                                                PAF_DESCRIPTION = m.PAF_DESCRIPTION,
                                                PAF_FILE_FOR = m.PAF_FILE_FOR,
                                            }).ToList();

                    pModel.crude_approve_detail.dDetail_CustomsFile = new List<FileViewModel>();

                    foreach (var itemFile in queryCustomsFile)
                    {
                        string itemNo = (itemFile.PAF_ITEM_NO == null) ? "" : itemFile.PAF_ITEM_NO.ToString();
                        string no = itemFile.PAF_NO == null ? "" : itemFile.PAF_NO.ToString();
                        string refid = itemNo;// + no;

                        pModel.crude_approve_detail.dDetail_CustomsFile.Add(new FileViewModel
                        {
                            PAF_TRIP_NO = itemFile.PAF_TRIP_NO ?? "",
                            PAF_ITEM_NO = itemNo,
                            PAF_NO = no,
                            PAF_RED_ID = refid,
                            PAF_FILE_NAME = itemFile.PAF_FILE_NAME ?? "",
                            PAF_PATH = itemFile.PAF_PATH ?? "",
                            PAF_DESCRIPTION = itemFile.PAF_DESCRIPTION ?? "",
                            PAF_FILE_FOR = itemFile.PAF_FILE_FOR ?? "",
                            PAF_INDB_STS = "1",
                            PAF_STATUS = "",
                            PAF_DESC_TEMP = itemFile.PAF_DESCRIPTION ?? "",
                        });
                    }

                    pModel.crude_approve_detail.dDetail_Custom = new List<CustomsViewModel>();
                    var queryC = (from h in context.PCF_HEADER
                                  join m in context.PCF_CUSTOMS_VAT on h.PHE_TRIP_NO equals m.TRIP_NO
                                  where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                  orderby m.TRIP_NO, m.MAT_ITEM_NO
                                  select m).ToList();
                    if (queryC != null)
                    {
                        foreach (var h in queryC)
                        {
                            pModel.crude_approve_detail.dDetail_Custom.Add(
                                             new CustomsViewModel
                                             {
                                                 CT_TripNo = h.TRIP_NO,
                                                 CT_MatItemNo = h.MAT_ITEM_NO.ToString(),
                                                 CT_Price = (h.CUSTOMS_PRICE == null) ? "" : h.CUSTOMS_PRICE.ToString(),
                                                 CT_FreightAmt = (h.FREIGHT_AMOUNT == null) ? "" : h.FREIGHT_AMOUNT.ToString(),
                                                 CT_InsAmt = (h.INSURANCE_AMOUNT == null) ? "" : h.INSURANCE_AMOUNT.ToString(),
                                                 CT_InsRate = (h.INSURANCE_RATE == null) ? "" : h.INSURANCE_RATE.ToString(),
                                             });
                        }
                    }
                    #endregion

                    #region ############# Other ###################

                    var queryOtherFile = (from h in context.PCF_HEADER
                                          join m in context.PCF_ATTACH_FILE on h.PHE_TRIP_NO equals m.PAF_TRIP_NO
                                          where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                               && (h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD"))))
                                               && (m.PAF_FILE_FOR.Equals("OTHER"))
                                          orderby h.PHE_TRIP_NO, m.PAF_FILE_FOR, m.PAF_ITEM_NO, m.PAF_NO
                                          select new
                                          {
                                              PAF_ITEM_NO = m.PAF_ITEM_NO,
                                              PAF_NO = m.PAF_NO,
                                              PAF_TRIP_NO = m.PAF_TRIP_NO,
                                              PAF_FILE_NAME = m.PAF_FILE_NAME,
                                              PAF_PATH = m.PAF_PATH,
                                              PAF_DESCRIPTION = m.PAF_DESCRIPTION,
                                              PAF_FILE_FOR = m.PAF_FILE_FOR,
                                          }).ToList();

                    pModel.crude_approve_detail.dDetail_OtherFile = new List<FileViewModel>();

                    foreach (var itemFile in queryOtherFile)
                    {
                        string itemNo = (itemFile.PAF_ITEM_NO == null) ? "" : itemFile.PAF_ITEM_NO.ToString();
                        string no = itemFile.PAF_NO == null ? "" : itemFile.PAF_NO.ToString();
                        string refid = itemNo;// + no;

                        pModel.crude_approve_detail.dDetail_OtherFile.Add(new FileViewModel
                        {
                            PAF_TRIP_NO = itemFile.PAF_TRIP_NO ?? "",
                            PAF_ITEM_NO = itemNo,
                            PAF_NO = no,
                            PAF_RED_ID = refid,
                            PAF_FILE_NAME = itemFile.PAF_FILE_NAME ?? "",
                            PAF_PATH = itemFile.PAF_PATH ?? "",
                            PAF_DESCRIPTION = itemFile.PAF_DESCRIPTION ?? "",
                            PAF_FILE_FOR = itemFile.PAF_FILE_FOR ?? "",
                            PAF_INDB_STS = "1",
                            PAF_STATUS = "",
                            PAF_DESC_TEMP = itemFile.PAF_DESCRIPTION ?? "",
                        });
                    }


                    pModel.crude_approve_detail.dDetail_Other = new List<OtherViewModel>();
                    var queryO = (from h in context.PCF_HEADER
                                  join m in context.PCF_OTHER_COST on h.PHE_TRIP_NO equals m.POC_TRIP_NO
                                  join v in context.MT_VENDOR on m.POC_SUPPLIER equals v.VND_ACC_NUM_VENDOR into tmpVendor
                                  join t in context.PCF_MT_OTHER_COST_TYPE on m.POC_OTHER_COST_TYPE equals t.PMO_CODE into tmpType
                                  join p in context.MT_PORT on m.POC_PORT equals p.MLP_LOADING_PORT_ID into tmpPort
                                  from p in tmpPort.DefaultIfEmpty()
                                  from t in tmpType.DefaultIfEmpty()
                                  from v in tmpVendor.DefaultIfEmpty()
                                  where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                  orderby m.POC_TRIP_NO, m.POC_ITEM_NO
                                  select new
                                  {
                                      POC_TRIP_NO = m.POC_TRIP_NO,
                                      POC_ITEM_NO = m.POC_ITEM_NO,
                                      POC_OTHER_COST_TYPE = m.POC_OTHER_COST_TYPE,
                                      POC_SUPPLIER = m.POC_SUPPLIER,
                                      POC_AMOUNT = m.POC_AMOUNT,
                                      POC_CURRENCY = m.POC_CURRENCY,
                                      POC_DUE_DATE = m.POC_DUE_DATE,
                                      POC_FX_AGREEMENT = m.POC_FX_AGREEMENT,
                                      POC_FX_BOT = m.POC_FX_BOT,
                                      POC_TOTAL_AMOUNT_THB = m.POC_TOTAL_AMOUNT_THB,
                                      POC_APPROVED_DATE = m.POC_APPROVED_DATE,
                                      POC_CERTIFIED_DATE = m.POC_CERTIFIED_DATE,
                                      POC_REMARK = m.POC_REMARK,
                                      POC_MEMO_NO = m.POC_MEMO_NO,
                                      OC_TypeText = t.PMO_NAME,
                                      OC_VendorText = v.VND_NAME1,
                                      POC_PORT = m.POC_PORT,
                                      PortText = p.MLP_LOADING_PORT_NAME,
                                      POC_PERIOD_FROM = m.POC_PERIOD_FROM,
                                      POC_PERIOD_TO = m.POC_PERIOD_TO,
                                      PCO_InvoiceSTS = m.POC_INVOICE_STATUS,
                                      PCO_Price = m.POC_PRICE,
                                      InvoiceNo = m.POC_INVOICE_NO,
                                  }).ToList();

                    if (queryO != null)
                    {
                        foreach (var h in queryO)
                        {
                            pModel.crude_approve_detail.dDetail_Other.Add(
                                             new OtherViewModel
                                             {
                                                 OC_TripNo = h.POC_TRIP_NO,
                                                 OC_ItemNo = h.POC_ITEM_NO.ToString(),
                                                 OC_Type = (h.POC_OTHER_COST_TYPE == null) ? "" : h.POC_OTHER_COST_TYPE.ToString(),
                                                 OC_TypeText = h.OC_TypeText,
                                                 OC_Vendor = h.POC_SUPPLIER,
                                                 OC_VendorText = h.OC_VendorText,
                                                 OC_Amount = (h.POC_AMOUNT == null) ? "" : h.POC_AMOUNT.ToString(),
                                                 OC_AmountUnit = h.POC_CURRENCY,
                                                 OC_AmountUnitText = h.POC_CURRENCY,
                                                 OC_DueDate = (h.POC_DUE_DATE == null) ? "" : Convert.ToDateTime(h.POC_DUE_DATE).ToString("dd/MM/yyyy", cu),

                                                 OC_FXAgree = (h.POC_FX_AGREEMENT == null) ? "" : h.POC_FX_AGREEMENT.ToString(),
                                                 OC_FXBOT = (h.POC_FX_BOT == null) ? "" : h.POC_FX_BOT.ToString(),
                                                 OC_Total = (h.POC_TOTAL_AMOUNT_THB == null) ? "" : h.POC_TOTAL_AMOUNT_THB.ToString(),
                                                 OC_CerDate = (h.POC_CERTIFIED_DATE == null) ? "" : Convert.ToDateTime(h.POC_CERTIFIED_DATE).ToString("dd/MM/yyyy", cu),
                                                 OC_ApprDate = (h.POC_APPROVED_DATE == null) ? "" : Convert.ToDateTime(h.POC_APPROVED_DATE).ToString("dd/MM/yyyy", cu),
                                                 OC_Remark = h.POC_REMARK,
                                                 OC_MemoNo = h.POC_MEMO_NO,
                                                 OC_Port = (h.POC_PORT == null) ? "" : h.POC_PORT.ToString(),
                                                 OC_PortText = h.PortText,
                                                 OC_OperationDate = (h.POC_PERIOD_FROM != null) ? Convert.ToDateTime(h.POC_PERIOD_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.POC_PERIOD_TO).ToString("dd/MM/yyyy", cu) : "",

                                                 //POC_CREATED_DATE = h.POC_CREATED_DATE,
                                                 //POC_CREATED_BY = h.POC_CREATED_BY,
                                                 //POC_UPDATED_DATE = h.POC_UPDATED_DATE,
                                                 //POC_UPDATED_BY = h.POC_UPDATED_BY,
                                                 //OC_Port = h.POC_PORT,
                                                 //POC_PERIOD_FROM = h.POC_PERIOD_FROM,
                                                 //POC_PERIOD_TO = h.POC_PERIOD_TO,
                                                 //POC_PRICE = h.POC_PRICE,
                                                 //POC_HOURS = h.POC_HOURS,

                                                 OC_InvoiceSTS = h.PCO_InvoiceSTS,
                                                 OC_Price = (h.PCO_Price == null) ? "" : h.PCO_Price.ToString(),
                                                 INVOICE_NO = string.IsNullOrEmpty(h.InvoiceNo) ? "" : h.InvoiceNo
                                             });

                        }
                    }
                    #endregion

                    #region ############# Surveyor ###################
                    var querySurveyorFile = (from h in context.PCF_HEADER
                                             join m in context.PCF_ATTACH_FILE on h.PHE_TRIP_NO equals m.PAF_TRIP_NO
                                             where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                                  && (h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD"))))
                                                  && (m.PAF_FILE_FOR.Equals("SURVEYOR"))
                                             orderby h.PHE_TRIP_NO, m.PAF_FILE_FOR, m.PAF_ITEM_NO, m.PAF_NO
                                             select new
                                             {
                                                 PAF_ITEM_NO = m.PAF_ITEM_NO,
                                                 PAF_NO = m.PAF_NO,
                                                 PAF_TRIP_NO = m.PAF_TRIP_NO,
                                                 PAF_FILE_NAME = m.PAF_FILE_NAME,
                                                 PAF_PATH = m.PAF_PATH,
                                                 PAF_DESCRIPTION = m.PAF_DESCRIPTION,
                                                 PAF_FILE_FOR = m.PAF_FILE_FOR,
                                             }).ToList();

                    pModel.crude_approve_detail.dDetail_SurveyorFile = new List<FileViewModel>();

                    foreach (var itemFile in querySurveyorFile)
                    {
                        string itemNo = (itemFile.PAF_ITEM_NO == null) ? "" : itemFile.PAF_ITEM_NO.ToString();
                        string no = itemFile.PAF_NO == null ? "" : itemFile.PAF_NO.ToString();
                        string refid = itemNo;// + no;

                        pModel.crude_approve_detail.dDetail_SurveyorFile.Add(new FileViewModel
                        {
                            PAF_TRIP_NO = itemFile.PAF_TRIP_NO ?? "",
                            PAF_ITEM_NO = itemNo,
                            PAF_NO = no,
                            PAF_RED_ID = refid,
                            PAF_FILE_NAME = itemFile.PAF_FILE_NAME ?? "",
                            PAF_PATH = itemFile.PAF_PATH ?? "",
                            PAF_DESCRIPTION = itemFile.PAF_DESCRIPTION ?? "",
                            PAF_FILE_FOR = itemFile.PAF_FILE_FOR ?? "",
                            PAF_INDB_STS = "1",
                            PAF_STATUS = "",
                            PAF_DESC_TEMP = itemFile.PAF_DESCRIPTION ?? "",
                        });
                    }

                    var querySM = (from h in context.PCF_HEADER
                                   join m in context.PCF_PO_SURVEYOR_MATERIAL on h.PHE_TRIP_NO equals m.PSM_TRIP_NO
                                   join t in context.MT_MATERIALS on m.PSM_MET_NUM equals t.MET_NUM
                                   where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))
                                   orderby m.PSM_TRIP_NO, m.PSM_ITEM_NO
                                   select new
                                   {
                                       PSM_TRIP_NO = m.PSM_TRIP_NO,
                                       PSM_ITEM_NO = m.PSM_ITEM_NO,
                                       PSM_MET_NUM = m.PSM_MET_NUM,
                                       PSM_MET_NAME = t.MET_MAT_DES_ENGLISH
                                   }).ToList();


                    pModel.crude_approve_detail.dDetail_Surveyor = new List<SurveyorViewModel>();
                    var queryS = (from h in context.PCF_HEADER
                                  join m in context.PCF_PO_SURVEYOR on h.PHE_TRIP_NO equals m.PPS_TRIP_NO
                                  join t in context.PCF_MT_PO_SURVEYOR_TYPE on m.PPS_PO_SERVEYOR_TYPE equals t.PMS_CODE into tmpType
                                  join v in context.MT_VEHICLE on m.PPS_VESSEL equals v.VEH_ID into tmpVessel
                                  join s in context.MT_VENDOR on m.PPS_SUPPLIER equals s.VND_ACC_NUM_VENDOR into tmpVendor
                                  join lp in context.MT_PORT on m.PPS_LOADING_PORT equals lp.MLP_LOADING_PORT_ID into tmpLPort
                                  join dp in context.MT_PORT on m.PPS_DISCHARGING_PORT equals dp.MLP_LOADING_PORT_ID into tmpDPort
                                  from t in tmpType.DefaultIfEmpty()
                                  from v in tmpVessel.DefaultIfEmpty()
                                  from s in tmpVendor.DefaultIfEmpty()
                                  from lp in tmpLPort.DefaultIfEmpty()
                                  from dp in tmpDPort.DefaultIfEmpty()
                                  where ((TripSearchSTS == true && arrTrip.Contains(h.PHE_TRIP_NO)) || (PeriodSearchSTS == true && h.PHE_LOADING_MONTH == loadingMonthCheck))
                                        && h.PHE_STATUS.Equals("ACTIVE") && ((string.IsNullOrEmpty(isBook)) ? true : (h.PHE_PLANNING_RELEASE.Equals("Y") && h.PHE_MAT_TYPE.Equals("CRD")))

                                  orderby m.PPS_TRIP_NO, m.PPS_ITEM_NO
                                  select new
                                  {
                                      PPS_ITEM_NO = m.PPS_ITEM_NO,
                                      PPS_TRIP_NO = m.PPS_TRIP_NO,
                                      PPS_PO_SERVEYOR_TYPE = m.PPS_PO_SERVEYOR_TYPE,
                                      PPS_LOADING_DATE = m.PPS_LOADING_DATE,
                                      PPS_LOADING_PORT = m.PPS_LOADING_PORT,
                                      PPS_DISCHARGING_DATE = m.PPS_DISCHARGING_DATE,
                                      PPS_DISCHARGING_PORT = m.PPS_DISCHARGING_PORT,
                                      PPS_VESSEL = m.PPS_VESSEL,
                                      PPS_SUPPLIER = m.PPS_SUPPLIER,
                                      PPS_AMOUNT = m.PPS_AMOUNT,
                                      PPS_CURRENCY = m.PPS_CURRENCY,
                                      PPS_COST_SHARING = m.PPS_COST_SHARING,
                                      PPS_REMARK = m.PPS_REMARK,
                                      PPS_PO_NO = m.PPS_PO_NO,
                                      Sur_TypeText = t.PMS_NAME,
                                      Sur_VesselText = v.VEH_VEH_TEXT,
                                      Sur_LoadPortText = lp.MLP_LOADING_PORT_NAME,
                                      Sur_DiscPortText = dp.MLP_LOADING_PORT_NAME,
                                      Sur_InvoiceSTS = m.PPS_INVOICE_STATUS,
                                      Sur_SurveyorText = s.VND_NAME1,
                                      PPS_LOADING_FROM_DATE = m.PPS_LOADING_FROM_DATE,
                                      PPS_LOADING_TO_DATE = m.PPS_LOADING_TO_DATE,
                                      PPS_DISCHARGING_FROM_DATE = m.PPS_DISCHARGING_FROM_DATE,
                                      PPS_DISCHARGING_TO_DATE = m.PPS_DISCHARGING_FROM_TO,
                                      InvoiceNo = m.PPS_INVOICE_NO,
                                  }).ToList();

                    if (queryS != null)
                    {
                        foreach (var h in queryS)
                        {
                            string sSurMat = "";
                            string sSurMatName = "";
                            var SubItem = querySM.Where(t => t.PSM_ITEM_NO == h.PPS_ITEM_NO && t.PSM_TRIP_NO == h.PPS_TRIP_NO);
                            foreach (var item in SubItem)
                            {
                                sSurMat += "," + item.PSM_MET_NUM;
                                sSurMatName += "," + item.PSM_MET_NAME;
                            }

                            pModel.crude_approve_detail.dDetail_Surveyor.Add(
                                         new SurveyorViewModel
                                         {
                                             Sur_TripNo = h.PPS_TRIP_NO,
                                             Sur_ItemNo = h.PPS_ITEM_NO.ToString(),
                                             Sur_Type = (h.PPS_PO_SERVEYOR_TYPE == null) ? "" : h.PPS_PO_SERVEYOR_TYPE.ToString(),
                                             Sur_TypeText = h.Sur_TypeText,
                                             //Sur_LoadDate = (h.PPS_LOADING_DATE == null) ? "" : Convert.ToDateTime(h.PPS_LOADING_DATE).ToString("dd/MM/yyyy", cu),
                                             Sur_LoadDate = (h.PPS_LOADING_FROM_DATE == null || h.PPS_LOADING_TO_DATE == null) ? "" : Convert.ToDateTime(h.PPS_LOADING_FROM_DATE).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PPS_LOADING_TO_DATE).ToString("dd/MM/yyyy", cu),
                                             Sur_LoadPort = (h.PPS_LOADING_PORT == null) ? "" : h.PPS_LOADING_PORT.ToString(),
                                             Sur_LoadPortText = h.Sur_LoadPortText,
                                             //Sur_DischargeDate = (h.PPS_DISCHARGING_DATE == null) ? "" : Convert.ToDateTime(h.PPS_DISCHARGING_DATE).ToString("dd/MM/yyyy", cu),
                                             Sur_DischargeDate = (h.PPS_DISCHARGING_FROM_DATE == null || h.PPS_DISCHARGING_TO_DATE == null) ? "" : Convert.ToDateTime(h.PPS_DISCHARGING_FROM_DATE).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(h.PPS_DISCHARGING_TO_DATE).ToString("dd/MM/yyyy", cu),
                                             Sur_DiscPort = (h.PPS_DISCHARGING_PORT == null) ? "" : h.PPS_DISCHARGING_PORT.ToString(),
                                             Sur_DiscPortText = h.Sur_DiscPortText,
                                             Sur_Vessel = h.PPS_VESSEL,
                                             Sur_VesselText = h.Sur_VesselText,
                                             Sur_Surveyor = h.PPS_SUPPLIER,
                                             Sur_SurveyorText = h.Sur_SurveyorText,
                                             Sur_Amount = (h.PPS_AMOUNT == null) ? "" : h.PPS_AMOUNT.ToString(),
                                             Sur_AmountUnit = h.PPS_CURRENCY,
                                             Sur_AmountUnitText = h.PPS_CURRENCY,
                                             Sur_CostSharing = (h.PPS_COST_SHARING == null) ? "" : h.PPS_COST_SHARING.ToString(),
                                             Sur_Remark = h.PPS_REMARK,
                                             Sur_PO = h.PPS_PO_NO,
                                             //PPS_CREATED_DATE = h.PPS_CREATED_DATE,
                                             //PPS_CREATED_BY = h.PPS_CREATED_BY,
                                             //PPS_UPDATED_DATE = h.PPS_UPDATED_DATE,
                                             //PPS_UPDATED_BY = h.PPS_UPDATED_BY
                                             Sur_CrudeText = (string.IsNullOrEmpty(sSurMatName)) ? "" : sSurMatName.Substring(1),
                                             Sur_Crude = (string.IsNullOrEmpty(sSurMat)) ? "" : sSurMat.Substring(1),
                                             Sur_SurMat = (string.IsNullOrEmpty(sSurMat)) ? "" : sSurMat.Substring(1),
                                             Sur_InvoiceSTS = h.Sur_InvoiceSTS,
                                             INVOICE_NO = string.IsNullOrEmpty(h.InvoiceNo) ? "" : h.InvoiceNo,
                                             Sur_RefId = h.PPS_ITEM_NO.ToString(),
                                         });

                        }
                    }
                    #endregion

                    #region ############### Price Detail ##################
                    if (PeriodSearchSTS)
                    {
                        //ArrivalMonth = 5;
                        //ArrivalYear = 2017;

                        pModel.PriceDetail = new PriceDataDetailViewModel();

                        var qPriceUpdate = (from p in context.PCF_MATERIAL_PRICE where p.PMP_PRICE_UPDATE != null orderby p.PMP_PRICE_UPDATE descending select p.PMP_PRICE_UPDATE);
                        if (qPriceUpdate != null)
                        {
                            if (qPriceUpdate.Count() > 0)
                            {
                                pModel.PriceDetail.PriceLastUpdate = qPriceUpdate.ToList()[0].Value.ToString("dd MMM yyyy", new System.Globalization.CultureInfo("en-US"));
                            }
                        }


                        string BKKDay = "";
                        string UKDay = "";

                        getHoliday(ArrivalMonth, ArrivalYear, ref BKKDay, ref UKDay, context);

                        pModel.PriceDetail.BKKBankHoliday = BKKDay;
                        pModel.PriceDetail.NKBankHoliday = UKDay;

                        string PCFLastUpdate = "";
                        string PriceLastUpdate = "";
                        getLastUpdate(ArrivalMonth, ArrivalYear, ref PCFLastUpdate, ref PriceLastUpdate, context);

                        pModel.PriceDetail.PCFLastUpdate = PCFLastUpdate;
                        pModel.PriceDetail.PriceLastUpdate = PriceLastUpdate;

                        List<string> OSPPrdCode = new List<string>();
                        List<string> MOPSPrdCode = new List<string>();

                        if (pModel.crude_approve_detail.dDetail != null)
                        {

                            foreach (var ip in pModel.crude_approve_detail.dDetail)
                            {
                                string priceSTS = ip.PriceStatus;
                                string tripno = ip.TripNo;
                                string matItem = ip.MatItemNo;
                                string BENCHMARK = "";
                                string MatCode = "";

                                var qMat = pModel.crude_approve_detail.dDetail_Material.Where(p => p.PMA_ITEM_NO.Equals(matItem) && p.PMA_TRIP_NO.Equals(tripno)).ToList();
                                if (qMat != null && qMat.Count > 0)
                                {
                                    BENCHMARK = qMat[0].PMA_BECHMARK;
                                    MatCode = qMat[0].PMA_MET_NUM;
                                }

                                //BENCHMARK = "OSP";
                                //priceSTS = "P";

                                var qTemp = (from f in context.FORMULA_PRICE where f.BENCHMARK.Equals(BENCHMARK) && f.PRICE_STATUS.Equals(priceSTS) select f);
                                if (qTemp == null)
                                    continue;

                                if (qTemp.Count() <= 0)
                                    continue;

                                var qFomular = qTemp.ToList();
                                if (MatCode.Trim().ToUpper() == "YPAT")
                                {
                                    qFomular = qTemp.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == "YPAT").ToList();
                                }
                                else
                                {
                                    qFomular = qTemp.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == MatCode).ToList();
                                    if (qFomular.Count == 0)
                                    {
                                        qFomular = qTemp.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() != "YPAT").ToList();
                                    }
                                }



                                //var qFomular = (from f in context.FORMULA_PRICE where f.BENCHMARK.Equals(BENCHMARK) && f.PRICE_STATUS.Equals(priceSTS) && f.MET_NUM.Equals(MatCode) select f).ToList();

                                foreach (var iF in qFomular)
                                {
                                    if (!string.IsNullOrEmpty(iF.DAILY_1))
                                        MOPSPrdCode.Add(iF.DAILY_1);

                                    if (!string.IsNullOrEmpty(iF.DAILY_2))
                                        MOPSPrdCode.Add(iF.DAILY_2);

                                    if (!string.IsNullOrEmpty(iF.DAILY_3))
                                        MOPSPrdCode.Add(iF.DAILY_3);

                                    if (!string.IsNullOrEmpty(iF.DAILY_4))
                                        MOPSPrdCode.Add(iF.DAILY_4);

                                    if (!string.IsNullOrEmpty(iF.DAILY_5))
                                        MOPSPrdCode.Add(iF.DAILY_5);

                                    if (!string.IsNullOrEmpty(iF.DAILY_6))
                                        MOPSPrdCode.Add(iF.DAILY_6);

                                    if (!string.IsNullOrEmpty(iF.DAILY_7))
                                        MOPSPrdCode.Add(iF.DAILY_7);


                                    if (!string.IsNullOrEmpty(iF.MONTHLY_1))
                                        OSPPrdCode.Add(iF.MONTHLY_1);

                                    if (!string.IsNullOrEmpty(iF.MONTHLY_2))
                                        OSPPrdCode.Add(iF.MONTHLY_2);

                                    if (!string.IsNullOrEmpty(iF.MONTHLY_3))
                                        OSPPrdCode.Add(iF.MONTHLY_3);

                                    if (!string.IsNullOrEmpty(iF.MONTHLY_4))
                                        OSPPrdCode.Add(iF.MONTHLY_4);

                                    if (!string.IsNullOrEmpty(iF.MONTHLY_5))
                                        OSPPrdCode.Add(iF.MONTHLY_5);

                                    if (!string.IsNullOrEmpty(iF.MONTHLY_6))
                                        OSPPrdCode.Add(iF.MONTHLY_6);

                                    if (!string.IsNullOrEmpty(iF.MONTHLY_7))
                                        OSPPrdCode.Add(iF.MONTHLY_7);

                                }


                            }

                        }

                        retMOPSPrdCode = MOPSPrdCode;


                        // OSP Monthly
                        int intYear = int.Parse(ArrivalYear.ToString());
                        int intMonth = int.Parse(ArrivalMonth.ToString());

                        int day = DateTime.DaysInMonth(intYear, intMonth);
                        DateTime CurrentDate = new DateTime(intYear, intMonth, day);
                        DateTime CurrentDate0 = new DateTime(intYear, intMonth, 1);

                        if (DateTime.Now > CurrentDate)
                            pModel.PriceDetail.Daily2 = "1-" + day.ToString() + " " + CurrentDate.ToString("MMM", provider) + " " + CurrentDate.ToString("yyyy", provider);
                        else
                            pModel.PriceDetail.Daily2 = "1-" + DateTime.Now.Day + " " + DateTime.Now.ToString("MMM", provider) + " " + DateTime.Now.ToString("yyyy", provider);


                        DateTime beforeDateTmp = CurrentDate.AddMonths(-1);
                        DateTime beforeDate0 = new DateTime(beforeDateTmp.Year, beforeDateTmp.Month, 1);

                        day = DateTime.DaysInMonth(beforeDate0.Year, beforeDate0.Month);
                        DateTime beforeDate = new DateTime(beforeDate0.Year, beforeDate0.Month, day);


                        pModel.PriceDetail.Daily1 = "1-" + day.ToString() + beforeDate0.ToString("MMM", provider) + " " + beforeDate0.ToString("yyyy", provider);

                        pModel.PriceDetail.Daily1_Hidden = "01" + beforeDate0.ToString("/MM/yyyy", provider) + " to " + day.ToString() + beforeDate0.ToString("/MM/yyyy", provider);

                        pModel.PriceDetail.Monthly2 = CurrentDate0.ToString("MMM", provider) + "'" + CurrentDate0.ToString("yyyy", provider);
                        string dateFromTo1 = CurrentDate0.ToString("dd/MM/yyyy", provider) + " to " + CurrentDate.ToString("dd/MM/yyyy", provider);

                        pModel.PriceDetail.Daily2 = dateFromTo1;

                        pModel.PriceDetail.PriceMonthly = new List<PriceMonthlyDetail>();

                        List<PCFMobDetail> result = Search(dateFromTo1, OSPPrdCode);
                        if (result != null && result.Count > 0)
                        {
                            foreach (var i in result)
                            {
                                var qChk = pModel.PriceDetail.PriceMonthly.Where(p => p.Code.Equals(i.ProductCode)).ToList();
                                if (qChk != null && qChk.Count > 0)
                                {
                                    qChk[0].Name = i.ProductName;
                                    qChk[0].Price2 = i.ProductValue.ToString("#.####0");
                                }
                                else
                                {
                                    pModel.PriceDetail.PriceMonthly.Add(new PriceMonthlyDetail
                                    {
                                        Code = i.ProductCode,
                                        Name = i.ProductName,
                                        Price2 = i.ProductValue.ToString("#.####0")
                                    });
                                }

                            }
                        }

                        pModel.PriceDetail.Monthly1 = beforeDate0.ToString("MMM", provider) + "'" + beforeDate0.ToString("yyyy", provider);

                        string dateFromTo2 = beforeDate0.ToString("dd/MM/yyyy", provider) + " to " + beforeDate.ToString("dd/MM/yyyy", provider);
                        result = Search(dateFromTo2, OSPPrdCode);
                        if (result != null && result.Count > 0)
                        {
                            foreach (var i in result)
                            {
                                var qChk = pModel.PriceDetail.PriceMonthly.Where(p => p.Code.Equals(i.ProductCode)).ToList();
                                if (qChk != null && qChk.Count > 0)
                                {
                                    qChk[0].Name = i.ProductName;
                                    qChk[0].Price1 = i.ProductValue.ToString("#.####0");
                                }
                                else
                                {
                                    pModel.PriceDetail.PriceMonthly.Add(new PriceMonthlyDetail
                                    {
                                        Code = i.ProductCode,
                                        Name = i.ProductName,
                                        Price1 = i.ProductValue.ToString("#.####0")
                                    });
                                }

                            }

                        }

                        pModel.PriceDetail.PriceDaily = new List<PriceDailyDetail>();

                        //////////// Daily
                        result = Search(dateFromTo1, MOPSPrdCode);
                        if (result != null && result.Count > 0)
                        {
                            Decimal sum = 0;
                            Double count = 0;
                            foreach (var prodcoce in MOPSPrdCode)
                            {
                                var qChk = result.Where(p => p.ProductCode.Equals(prodcoce));
                                count = qChk.Count();
                                sum = qChk.Sum(x => x.ProductValue);

                                var ProductName = "";
                                if (count > 0)
                                    ProductName = qChk.ToList()[0].ProductName;

                                Double net = Convert.ToDouble(sum.ToString()) / count;

                                var qModel = pModel.PriceDetail.PriceDaily.Where(p => p.Code.Equals(prodcoce)).ToList();
                                if (qModel != null && qModel.Count > 0)
                                {
                                    qModel[0].Name = ProductName;
                                    qModel[0].Price2 = net.ToString("#.####0");
                                }
                                else
                                {
                                    pModel.PriceDetail.PriceDaily.Add(new PriceDailyDetail
                                    {
                                        Code = prodcoce,
                                        Name = ProductName,
                                        Price2 = net.ToString("#.####0")
                                    });
                                }


                            }


                        }

                        result = Search(dateFromTo2, MOPSPrdCode);
                        if (result != null && result.Count > 0)
                        {
                            Decimal sum = 0;
                            Double count = 0;
                            foreach (var prodcoce in MOPSPrdCode)
                            {
                                var qChk = result.Where(p => p.ProductCode.Equals(prodcoce));
                                count = qChk.Count();
                                sum = qChk.Sum(x => x.ProductValue);

                                var ProductName = "";
                                if (count > 0)
                                    ProductName = qChk.ToList()[0].ProductName;

                                Double net = Convert.ToDouble(sum.ToString()) / count;

                                var qModel = pModel.PriceDetail.PriceDaily.Where(p => p.Code.Equals(prodcoce)).ToList();
                                if (qModel != null && qModel.Count > 0)
                                {
                                    qModel[0].Name = ProductName;
                                    qModel[0].Price1 = net.ToString("#.####0");
                                }
                                else
                                {
                                    pModel.PriceDetail.PriceDaily.Add(new PriceDailyDetail
                                    {
                                        Code = prodcoce,
                                        Name = ProductName,
                                        Price1 = net.ToString("#.####0")
                                    });
                                }

                            }



                        }


                    }

                    #endregion

                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;

        }

        private List<PCFMobDetail> Search(string dateFromTo, List<string> prdCode)
        {
            List<PCFMobDetail> result = new List<PCFMobDetail>();
            try
            {
                var sDateFrom = String.IsNullOrEmpty(dateFromTo) ? "" : dateFromTo.Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(dateFromTo) ? "" : dateFromTo.Substring(14, 10);
                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryTemp = (from v in context.MKT_TRN_MOPS_B
                                     join m in context.MKT_MST_MOPS_PRODUCT on v.T_MPB_PRDCODE equals m.M_MPR_PRDCODE
                                     where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T" && prdCode.Contains(v.T_MPB_PRDCODE)
                                     select new
                                     {
                                         v,
                                         M_MPR_PRDCODE = m.M_MPR_PRDCODE,
                                         M_MPR_PRDNAME = m.M_MPR_ACTPRODNAME,
                                     }).OrderBy(p => p.v.T_MPB_VALDATE);

                    var query = (from d in queryTemp.AsEnumerable()
                                 where Convert.ToInt32(string.IsNullOrEmpty(d.v.T_MPB_VALDATE) ? "0" : d.v.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                                 && Convert.ToInt32(string.IsNullOrEmpty(d.v.T_MPB_VALDATE) ? "0" : d.v.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                                 select d).OrderBy(p => p.v.T_MPB_PRDCODE).OrderBy(s => s.v.T_MPB_VALDATE);

                    if (query != null)
                    {
                        foreach (var i in query.ToList())
                        {
                            PCFMobDetail j = new PCFMobDetail();
                            j.date = DateTime.ParseExact(i.v.T_MPB_VALDATE.ToString().Substring(6, 2) + "/" + i.v.T_MPB_VALDATE.ToString().Substring(4, 2) + "/" + i.v.T_MPB_VALDATE.ToString().Substring(0, 4), format, provider);
                            j.ProductCode = i.M_MPR_PRDCODE;
                            j.ProductName = i.M_MPR_PRDNAME;
                            j.ProductValue = i.v.T_MPB_VALUE ?? 0;
                            result.Add(j);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return result;
        }

        private void getLastUpdate(Decimal month, decimal year, ref string PCFLastUpdate, ref string PriceLastUpdate, EntityCPAIEngine context)
        {
            try
            {
                var query = (from h in context.PCF_HEADER orderby h.PHE_UPDATED_DATE descending select h.PHE_UPDATED_DATE).FirstOrDefault();
                if (query != null)
                    PCFLastUpdate = query.ToString("dd MMM yyyy", new System.Globalization.CultureInfo("en-US"));

            }
            catch (Exception ex)
            {
            }


            try
            {
                var query = (from h in context.PCF_MATERIAL_PRICE where h.PMP_PRICE_UPDATE != null orderby h.PMP_PRICE_UPDATE descending select h.PMP_PRICE_UPDATE).FirstOrDefault();
                if (query != null)
                    PriceLastUpdate = Convert.ToDateTime(query).ToString("dd MMM yyyy", new System.Globalization.CultureInfo("en-US"));

            }
            catch (Exception ex)
            {
            }


        }

        private void getHoliday(Decimal month, decimal year, ref string sDayBBK, ref string sDayUK, EntityCPAIEngine context)
        {
            string THMonthText = "";
            string THDayText = "";

            string UKMonthText = "";
            string UKDayText = "";

            var query = (from h in context.MT_HOLIDAY
                         where h.MH_HOL_TYPE.Equals("Thai")
                         select new
                         {
                             hDay = (DateTime?)h.MH_HOL_DATE
                         }).ToList();



            if (query != null)
            {
                foreach (var i in query)
                {
                    DateTime d = Convert.ToDateTime(i.hDay);
                    int imonth = d.Month;
                    int iyear = d.Year;
                    if (iyear == year && imonth == month)
                    {

                        if (THDayText != "")
                            THDayText += ",";

                        if (THMonthText == "")
                            THMonthText = d.ToString("MMM", new System.Globalization.CultureInfo("en-US"));

                        THDayText += d.Day.ToString();
                    }

                }

                if (THMonthText != "")
                {
                    sDayBBK = THDayText + " " + THMonthText + " " + year.ToString();
                }

            }

            var queryUK = (from h in context.MT_HOLIDAY
                           where h.MH_HOL_TYPE.Equals("UK")
                           select new
                           {
                               hDay = (DateTime?)h.MH_HOL_DATE
                           }).ToList();

            if (queryUK != null)
            {
                foreach (var i in queryUK)
                {
                    DateTime d = Convert.ToDateTime(i.hDay);
                    int imonth = d.Month;
                    int iyear = d.Year;
                    if (iyear == year && imonth == month)
                    {
                        if (UKDayText != "")
                            UKDayText += ",";

                        if (UKMonthText == "")
                            UKMonthText = d.ToString("MMM", new System.Globalization.CultureInfo("en-US"));

                        UKDayText += d.Day.ToString();
                    }

                }

                if (UKMonthText != "")
                {
                    sDayUK = UKDayText + " " + UKMonthText + " " + year.ToString();
                }

            }

        }

        public ReturnValue Search(ref CrudeImportPlanViewModel_Seach pModel, string matType, string ExportSTS)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sCrude = String.IsNullOrEmpty(pModel.sCrudeName) ? "" : pModel.sCrudeName.ToUpper().Trim();
                    var sSupplier = String.IsNullOrEmpty(pModel.sSupplier) ? "" : pModel.sSupplier.ToUpper().Trim();
                    var sIncoterm = String.IsNullOrEmpty(pModel.sIncoterm) ? "" : pModel.sIncoterm.ToUpper().Trim();

                    //on new { tripno = m.PMA_TRIP_NO, matitem = m.PMA_ITEM_NO } equals new { tripno = p.PMP_TRIP_NO, matitem = p.PMP_MAT_ITEM_NO }
                    var query = (from h in context.PCF_HEADER
                                 join com in context.MT_COMPANY on h.PHE_COMPANY_CODE equals com.MCO_COMPANY_CODE into tmpCom
                                 join vss in context.MT_VEHICLE on h.PHE_VESSEL equals vss.VEH_ID into tmpvss
                                 join cda in context.CDP_DATA on h.PHE_CDA_ROW_ID equals cda.CDA_ROW_ID into tmpcda
                                 join mat in context.PCF_MATERIAL on h.PHE_TRIP_NO equals mat.PMA_TRIP_NO into tmpmat
                                 from com in tmpCom.DefaultIfEmpty()
                                 from vss in tmpvss.DefaultIfEmpty()
                                 from cda in tmpcda.DefaultIfEmpty()
                                 from mat in tmpmat.DefaultIfEmpty()
                                 join mtmat in context.MT_MATERIALS on mat.PMA_MET_NUM equals mtmat.MET_NUM into tmpmtmat
                                 join mtvnd in context.MT_VENDOR on mat.PMA_SUPPLIER equals mtvnd.VND_ACC_NUM_VENDOR into tmpmtvnd
                                 join port in context.MT_PORT on mat.PMA_LOADING_PORT equals port.MLP_LOADING_PORT_ID into tmpPort
                                 from port in tmpPort.DefaultIfEmpty()
                                 from mtmat in tmpmtmat.DefaultIfEmpty()
                                 from mtvnd in tmpmtvnd.DefaultIfEmpty()
                                 where (String.IsNullOrEmpty(sCrude) ? true : mtmat.MET_MAT_DES_ENGLISH.ToUpper().Equals(sCrude.ToUpper()))
                                        && (String.IsNullOrEmpty(sIncoterm) ? true : mat.PMA_INCOTERMS.Equals(sIncoterm))
                                        && (String.IsNullOrEmpty(sSupplier) ? true : mtvnd.VND_NAME1.ToUpper().Equals(sSupplier.ToUpper()))
                                        && h.PHE_STATUS.Equals("ACTIVE")
                                        && h.PHE_MAT_TYPE.Equals(matType)
                                 orderby h.PHE_TRIP_NO descending, mat.PMA_ITEM_NO
                                 select new
                                 {
                                     ArraivalDateMonth = h.PHE_ARRIVAL_MONTH,
                                     ArraivalDateYear = h.PHE_ARRIVAL_YEAR,
                                     TripNo = h.PHE_TRIP_NO,
                                     Vessel = vss.VEH_VEH_TEXT,
                                     CrudeCode = mtmat.MET_NUM,
                                     CrudeName = mtmat.MET_MAT_DES_ENGLISH,
                                     SupplierCode = mtvnd.VND_ACC_NUM_VENDOR,
                                     Supplier = mtvnd.VND_NAME1,
                                     FormulaPirce = mat.PMA_FORMULA,
                                     Incoterm = mat.PMA_INCOTERMS,
                                     QTY_Kbbl = mat.PMA_VOLUME_BBL / 1000,
                                     QTY_KT = (mat.PMA_VOLUME_BBL * 1000) * 65 / 500, //mat.PMA_VOLUME_MT * 1000,
                                     Tolerance = mat.PMA_TOLERANCE,
                                     LoadPort = port.MLP_LOADING_PORT_NAME,
                                     LodingDateFrom = mat.PMA_LOADING_DATE_FROM,
                                     LoadingDateTo = mat.PMA_LOADING_DATE_TO,
                                     Actual_ETA_F = h.PHE_ACTUAL_ETA_FROM,
                                     Actual_ETA_T = h.PHE_ACTUAL_ETA_TO,
                                     PMA_DISCHARGE_LAYCAN_FROM = mat.PMA_DISCHARGE_LAYCAN_FROM,
                                     PMA_DISCHARGE_LAYCAN_TO = mat.PMA_DISCHARGE_LAYCAN_TO,
                                     GT_C = mat.PMA_GT_C,
                                     TripStatus = h.PHE_STATUS,
                                     PlanningRelease = h.PHE_PLANNING_RELEASE,
                                     BookingRelease = h.PHE_BOOKING_RELEASE,
                                 });

                    pModel.sSearchData = new List<CrudeImportPlanViewModel_Detail>();

                    if (query == null)
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                        return rtn;
                    }

                    //int icount = query.Count();

                    //if (string.IsNullOrEmpty(ExportSTS))
                    //{

                    //}
                    //else if (ExportSTS.Equals("No"))
                    //{
                    //    query = query.Where(p => p.PlanningRelease.Equals("N"));
                    //}
                    //else if (ExportSTS.Equals("Yes"))
                    //{
                    //    query = query.Where(p => p.PlanningRelease.Equals("Y"));
                    //}

                    if (!string.IsNullOrEmpty(ExportSTS))
                    {
                        if (matType.Equals("PRD"))
                        {
                            if (ExportSTS.Equals("No"))
                            {
                                query = query.Where(p => p.BookingRelease.Equals("N"));
                            }
                            else if (ExportSTS.Equals("Yes"))
                            {
                                query = query.Where(p => p.BookingRelease.Equals("Y"));
                            }
                        }
                        else
                        {
                            if (ExportSTS.Equals("No"))
                            {
                                query = query.Where(p => p.PlanningRelease.Equals("N"));
                            }
                            else if (ExportSTS.Equals("Yes"))
                            {
                                query = query.Where(p => p.PlanningRelease.Equals("Y"));
                            }
                        }
                    }



                    System.Globalization.CultureInfo cu = new System.Globalization.CultureInfo("en-US");

                    foreach (var v in query)
                    {
                        string ExportStatus = "";
                        if (matType.Equals("PRD"))
                        {
                            ExportStatus = string.IsNullOrEmpty(v.BookingRelease) ? "" : v.BookingRelease;
                            ExportStatus = ExportStatus.Equals("Y") ? "Exported" : "";
                        }
                        else
                        {
                            ExportStatus = string.IsNullOrEmpty(v.PlanningRelease) ? "" : v.PlanningRelease;
                            ExportStatus = ExportStatus.Equals("Y") ? "Exported" : "";
                        }

                        string ArraivalDateNum = v.ArraivalDateYear.ToString() + ((v.ArraivalDateMonth.ToString().Length == 1) ? "0" + v.ArraivalDateMonth.ToString() : v.ArraivalDateMonth.ToString());

                        pModel.sSearchData.Add(
                                         new CrudeImportPlanViewModel_Detail
                                         {
                                             h_ArraivalDate = (string.IsNullOrEmpty(v.ArraivalDateMonth.ToString())) ? "" : getTextMonth(v.ArraivalDateMonth.ToString()) + "/" + v.ArraivalDateYear.ToString(),
                                             h_TripNo = v.TripNo,
                                             h_Vessel = v.Vessel,
                                             h_CrudeName = v.CrudeName,
                                             h_Supplier = v.Supplier,
                                             h_FormulaPirce = v.FormulaPirce,
                                             h_Incoterm = v.Incoterm,
                                             h_QTY_Kbbl = (v.QTY_Kbbl == null) ? "" : Convert.ToDecimal(v.QTY_Kbbl.ToString()).ToString("#,##0.#0"),
                                             h_QTY_KT = (v.QTY_KT == null) ? "" : Convert.ToDecimal(v.QTY_KT.ToString()).ToString("#,##0.#0"),
                                             h_Tolerance = string.IsNullOrEmpty(v.Tolerance) ? "" : v.Tolerance,
                                             h_LoadPort = string.IsNullOrEmpty(v.LoadPort) ? "" : v.LoadPort,
                                             h_LodingDate = (v.LodingDateFrom != null && v.LoadingDateTo != null) ? v.LodingDateFrom.Value.ToString("dd/MM/yyyy", cu) + " To " + v.LoadingDateTo.Value.ToString("dd/MM/yyyy", cu) : "",
                                             h_Actual_ETA = (v.Actual_ETA_F != null && v.Actual_ETA_T != null) ? v.Actual_ETA_F.Value.ToString("dd/MM/yyyy", cu) + " To " + v.Actual_ETA_T.Value.ToString("dd/MM/yyyy", cu) : "",
                                             h_GT_C = v.GT_C,
                                             h_DischargeLayCanDate = (v.PMA_DISCHARGE_LAYCAN_FROM != null && v.PMA_DISCHARGE_LAYCAN_TO != null) ? v.PMA_DISCHARGE_LAYCAN_FROM.Value.ToString("dd/MM/yyyy", cu) + " To " + v.PMA_DISCHARGE_LAYCAN_TO.Value.ToString("dd/MM/yyyy", cu) : "",
                                             TripStatus = v.TripStatus,
                                             PlanningRelease = v.PlanningRelease,
                                             BookingRelease = v.BookingRelease,
                                             ExportStatus = ExportStatus,
                                             h_ArraivalDateNum = ArraivalDateNum,
                                         });
                    }


                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;

            }
            return rtn;
        }

        public static List<SelectListItem> GetCrudeForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.CDP_DATA select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    select new { CrudeId = d.CDA_FK_MATERIALS, CrudeName = d.MT_MATERIALS.MET_MAT_DES_ENGLISH, CrudeOther = d.CDA_MATERIALS_OTHERS }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {
                            if (string.IsNullOrEmpty(item.CrudeId))
                            {
                                selectList.Add(new SelectListItem { Text = item.CrudeOther, Value = item.CrudeOther });
                            }
                            else
                            {
                                selectList.Add(new SelectListItem { Text = item.CrudeName, Value = item.CrudeName });
                            }

                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static List<SelectListItem> GetOtherCostType()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Text = "Select", Value = "" });
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_OTHER_COST_TYPE select v).OrderBy(p=>p.PMO_NAME).ToList();

                    if (query != null)
                    {
                        //query = query.OrderBy(p => p.PMO_NAME);

                        var qDis = (from d in query
                                    select new { code = d.PMO_CODE, name = d.PMO_NAME }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.name, Value = item.code.ToString() });
                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static List<SelectListItem> GetFreightType()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Text = "Select", Value = "" });
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_FREIGHT_TYPE orderby v.PMF_NAME select v).ToList();

                    if (query != null)
                    {
                        //var qDis = (from d in query orderby d.PMF_NAME
                        //            select new { code = d.PMF_CODE, name = d.PMF_NAME }).Distinct().ToArray();

                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.PMF_NAME, Value = item.PMF_CODE.ToString() });
                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static List<SelectListItem> GetHolidayType()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Text = "Select", Value = "" });
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_HOLIDAY
                                 select new
                                 {
                                     MH_HOL_TYPE = v.MH_HOL_TYPE
                                 }).Distinct();

                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.MH_HOL_TYPE, Value = item.MH_HOL_TYPE });

                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static List<SelectListItem> GetSurveyorType()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Text = "Select", Value = "" });
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_PO_SURVEYOR_TYPE orderby v.PMS_CODE select v);

                    if (query != null)
                    {
                        //var qDis = (from d in query
                        //            select new { code = d.PMS_CODE, name = d.PMS_NAME }).Distinct().ToArray();

                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.PMS_NAME, Value = item.PMS_CODE.ToString() });

                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static string GetFreightConfigToJSON()
        {
            string sJson = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_FREIGHT_TYPE select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    select d).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        sJson = js.Serialize(qDis);
                    }

                }

                return sJson;
            }
            catch (Exception)
            {
                return sJson;
            }

        }

        public static string GetOtherCostConfigToJSON()
        {
            string sJson = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_OTHER_COST_TYPE select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    select d).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        sJson = js.Serialize(qDis);
                    }

                }

                return sJson;
            }
            catch (Exception)
            {
                return sJson;
            }

        }

        public static string GetSurveyorConfigToJSON()
        {
            string sJson = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_PO_SURVEYOR_TYPE select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    select d).Distinct().ToArray();


                        JavaScriptSerializer js = new JavaScriptSerializer();
                        sJson = js.Serialize(qDis);
                    }

                }

                return sJson;
            }
            catch (Exception)
            {
                return sJson;
            }

        }


        public static string GetDataToJSON(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Text });


                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }



                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetDataToJSON_TextValue(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Value, text = v.Text });


                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }



                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }
        public static string GetDataToJSON_ValueText(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             where v.Text != "select" && v.Text != "Select Vessel"
                             select new { value = v.Text, text = v.Value });


                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }



                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }
        public static List<MT_MATERIALS> GetMaterials(string system, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            where v.MET_NUM.Substring(0, 1).Equals("Y")
                            select v;
                return query.ToList();
            }
        }

        public static List<SelectListItem> getPortName(string system = "PCF", bool isOptional = false, string message = "", string type = "VESSEL")
        {
            List<MT_PORT> mt_port = GetPort(system, type, ACTIVE).OrderBy(x => x.MLP_LOADING_PORT_NAME).ToList();
            return insertSelectListValue(mt_port.Select(x => x.MLP_LOADING_PORT_NAME).ToList(), mt_port.Select(x => x.MLP_LOADING_PORT_ID.ToString()).ToList(), isOptional, message);
        }
        private static List<MT_PORT> GetPort(string system, string type, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_PORT
                            join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT
                            where vc.MMP_SYSTEM.ToUpper() == system && vc.MMP_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }
        public static List<SelectListItem> getVehicle(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle("PCF", ACTIVE, "VESSEL").OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), mt_vehicle.Select(x => x.VEH_ID).ToList(), isOptional, message);
        }
        public static List<SelectListItem> getVendorAll(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(PROJECT_NAME_SPACE, type, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVendor(string system = "PCF", string type = "CRUDE", bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(system, type, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1 + " " + (x.VND_NAME2 ?? "") + " (" + x.VND_ACC_NUM_VENDOR + ")").ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
            }
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static List<SelectListItem> getMTIncoterms()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {

                var _MTIncoterm = context.MT_INCOTERMS.Where(x => x.MIN_STATUS.Equals("ACTIVE")).OrderBy(p => p.MIN_INCOTERMS).ToList();
                if (_MTIncoterm != null && _MTIncoterm.Count > 0)
                {
                    foreach (var item in _MTIncoterm)
                    {
                        list.Add(new SelectListItem { Text = item.MIN_INCOTERMS, Value = item.MIN_INCOTERMS });
                    }
                }
                else
                {
                    return list;
                }
            }

            return list;
        }

        public static List<MT_INCOTERMS> getMTIncotermsAll()
        {
            var _MTIncoterm = new List<MT_INCOTERMS>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {

                _MTIncoterm = context.MT_INCOTERMS.Where(x => x.MIN_STATUS.Equals("ACTIVE")).OrderBy(p => p.MIN_INCOTERMS).ToList();

            }

            return _MTIncoterm;
        }

        public static List<SelectListItem> getIncoterms()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.incoterms.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.incoterms[i], Value = setting.incoterms[i] });
            }
            return list;
        }

        public static List<SelectListItem> getYear()
        {
            // + - 2 Year
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "select", Value = "" });

            int y = Convert.ToInt16(DateTime.Now.AddYears(2).ToString("yyyy", new System.Globalization.CultureInfo("en-US")));
            for (int i = y; i > (y - 5); i--)
            {
                list.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            return list;
        }

        public static List<SelectListItem> getMonth(List<PCF_MONTH> MT_PCF_MONTH)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "select", Value = "" });

            int icount = MT_PCF_MONTH.Count;
            for (int i = 0; i < icount; i++)
            {
                list.Add(new SelectListItem { Text = MT_PCF_MONTH[i].NAME, Value = MT_PCF_MONTH[i].CODE });
            }

            return list;
        }

        public static List<SelectListItem> getChannel(List<PCF_CHANNEL> PCF_CHANNEL)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "Select Channel", Value = "" });

            int icount = PCF_CHANNEL.Count;
            for (int i = 0; i < icount; i++)
            {
                list.Add(new SelectListItem { Text = PCF_CHANNEL[i].NAME, Value = PCF_CHANNEL[i].CODE });
            }

            return list;
        }

        public static List<SelectListItem> getUnitCal(List<PCF_UNIT_CAL> PCF_UNITCAL)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "", Value = "" });

            int icount = PCF_UNITCAL.Count;
            for (int i = 0; i < icount; i++)
            {
                list.Add(new SelectListItem { Text = PCF_UNITCAL[i].NAME, Value = PCF_UNITCAL[i].CODE });
            }

            return list;
        }

        public static List<SelectListItem> getPurchaseType(List<PCF_MT_PURCHASE_TYPE> PCF_MT_PURCHASE_TYPE)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "select", Value = "" });

            int icount = PCF_MT_PURCHASE_TYPE.Count;
            for (int i = 0; i < icount; i++)
            {
                list.Add(new SelectListItem { Text = PCF_MT_PURCHASE_TYPE[i].NAME, Value = PCF_MT_PURCHASE_TYPE[i].CODE });
            }

            return list;
        }

        public static List<SelectListItem> getStorageLocation(List<PCF_MT_STORAGE_LOCATION> PCF_MT_STORAGE_LOCATION)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "select", Value = "" });

            int icount = PCF_MT_STORAGE_LOCATION.Count;
            for (int i = 0; i < icount; i++)
            {
                list.Add(new SelectListItem { Text = PCF_MT_STORAGE_LOCATION[i].NAME, Value = PCF_MT_STORAGE_LOCATION[i].CODE });
            }

            return list;
        }

        public static List<SelectListItem> getBLStatus()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "select", Value = "" });
            list.Add(new SelectListItem { Text = "Yes", Value = "Yes" });
            list.Add(new SelectListItem { Text = "No", Value = "No" });

            return list;
        }

        public static List<SelectListItem> getPriceStatus(List<PCF_MT_PRICE_STATUS> PCF_MT_PRICE_STATUS)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "select", Value = "" });

            int icount = PCF_MT_PRICE_STATUS.Count;
            for (int i = 0; i < icount; i++)
            {
                list.Add(new SelectListItem { Text = PCF_MT_PRICE_STATUS[i].NAME, Value = PCF_MT_PRICE_STATUS[i].CODE });
            }

            //list.Add(new SelectListItem { Text = "Provisional", Value = "P" });
            //list.Add(new SelectListItem { Text = "Actual", Value = "A" });
            //list.Add(new SelectListItem { Text = "Forecast", Value = "F" });

            return list;
        }

        public static List<SelectListItem> getCIPConfig()
        {
            string JsonD = MasterData.GetJsonMasterSetting(FORMULA_PRICING_TYPE);
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            CIP_Config dataList = (CIP_Config)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(CIP_Config));

            //LoadMaster From JSON

            List<SelectListItem> list = new List<SelectListItem>();
            //for (int i = 0; i < setting.incoterms.Count; i++)
            //{
            //    list.Add(new SelectListItem { Text = setting.incoterms[i], Value = setting.incoterms[i] });
            //}
            return list;
        }

        public static List<SelectListItem> getCurrencyConfig()
        {
            string JsonD = MasterData.GetJsonMasterSetting("CIP_CURRENCY");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            CIP_Currency dataList = (CIP_Currency)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(CIP_Currency));

            List<SelectListItem> list = new List<SelectListItem>();
            if (dataList != null)
            {
                list.Add(new SelectListItem { Text = "select", Value = "" });
                int icount = dataList.CURRENCY.Count;
                for (int i = 0; i < icount; i++)
                {
                    list.Add(new SelectListItem { Text = dataList.CURRENCY[i].NAME, Value = dataList.CURRENCY[i].CODE });
                }
            }

            return list;
        }

        public static CIP_Config getCIPConfig(String KeyName)
        {
            string JsonD = MasterData.GetJsonMasterSetting(KeyName);
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            CIP_Config dataList = (CIP_Config)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(CIP_Config));

            return dataList;
        }

        public static PCFHolidayFormulaModel getPCFHolidayFormula(String KeyName)
        {
            string JsonD = MasterData.GetJsonMasterSetting(KeyName);
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            PCFHolidayFormulaModel dataList = (PCFHolidayFormulaModel)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(PCFHolidayFormulaModel));

            return dataList;
        }

        public static String getGlobalConfig(String KeyName)
        {
            string JsonD = MasterData.GetJsonGlobalConfig(KeyName);
            //JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            //PCF_Authen dataList = (PCF_Authen)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(PCF_Authen));

            return JsonD;
        }

        public static string getCompanyCode(string comShortName)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {

                var _MTCom = context.MT_COMPANY.Where(x => x.MCO_SHORT_NAME == comShortName).ToList();
                if (_MTCom != null && _MTCom.Count > 0)
                {
                    return _MTCom[0].MCO_COMPANY_CODE;
                }
                else
                {
                    return "";
                }
            }
        }

        private static List<MT_MATERIALS> getMatMaster()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {

                var _MTMat = (from m in context.MT_MATERIALS
                              select m);

                return _MTMat.ToList();

            }
        }

        private static Decimal AvgFx(string yyyymm, EntityCPAIEngine context)
        {
            Decimal ret = 0;
            try
            {
                var queryFX = (from v in context.MKT_TRN_FX_B
                               where (v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD" && v.T_FXB_VALTYPE == "A")
                               && v.T_FXB_VALDATE.Contains(yyyymm)
                               select v).OrderBy(p => p.T_FXB_VALDATE);

                if (queryFX != null && queryFX.Count() > 0)
                {
                    decimal val = 0;
                    decimal num = 0;
                    foreach (var i in queryFX)
                    {
                        num = num + 1;
                        val += (i.T_FXB_VALUE1 ?? 0);
                    }
                    ret = Convert.ToDecimal((val / num).ToString("###0.###0"));
                }
            }
            catch (Exception ex)
            { }
            return ret;
        }


        public static List<DailyPriceViewModel> getDailyPrice(string tripno = "", string matnum = "", string benchmark = "", string pro_pricePeriod = ""
                                    , string pro_crudePhet_Vol = "", string final_pricePeriod = "", string final_crudePhet_Vol = "", string bl_date = "", string completeTime = ""
                                    , string recal = "0")
        {

            List<DailyPriceViewModel> DailyPrice = new List<DailyPriceViewModel>();
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();

                string sql = "";
                sql = "select TRIP_NO, MAT_NUM, to_char(posting_date, 'DD/MM/YYYY') BL_DATE, to_char(posting_date, 'YYYYMMDD') YYYYMMDD, sum(QTY_BBL) QTY_BBL ";
                sql += " from OUTTURN_FROM_SAP ";
                sql += " where TRIP_NO = '" + tripno + "' and MAT_NUM = '" + matnum + "' ";
                sql += " GROUP BY TRIP_NO, MAT_NUM, posting_date ";
                sql += " ORDER BY POSTING_DATE  ";
                List<DailyPriceOutturnViewModel> outturn = context.Database.SqlQuery<DailyPriceOutturnViewModel>(sql).ToList();

                sql = "select TRIP_NO, MAT_NUM, INVOICE_NO, to_char(BL_DATE, 'DD/MM/YYYY') BL_DATE, PRO_PRICE, FINAL_PRICE, DIFF_PRICE, OUTTURN_QTY, P1_FX_RATE";
                sql += ", P1_UNIT_PRICE, P1_TOTAL_BF_VAT, P1_VAT, P1_TOTAL_AF_VAT, P2_FX_RATE, P2_UNIT_PRICE, P2_DIFF_UNITPRICE ";
                sql += ", P2_ADJUST_PRICE, P2_TOTAL_DIFF, P2_DIFF_DOCNO, F1_FX_RATE, F1_UNIT_PRICE, F1_TOTAL_BF_VAT, F1_VAT ";
                sql += ", F1_TOTAL_AF_VAT, F2_FX_RATE, F2_TOTAL_DIFF1, F2_TOTAL_DIFF2, F2_TOTAL_DIFF3 ";
                sql += ", to_char(BL_DATE, 'YYYYMMDD') YYYYMMDD ";
                sql += " from PCF_MATERIAL_PRICE_DAILY ";
                sql += " where TRIP_NO='" + tripno + "' and MAT_NUM = '" + matnum + "' ";
                List<DailyPriceViewModel> dailyPrice = context.Database.SqlQuery<DailyPriceViewModel>(sql).ToList();

                if (outturn == null || dailyPrice == null)
                    return dailyPrice;


                Nullable<Decimal> avg_pro_price = null;
                Nullable<Decimal> avg_final_price = null;
                //Nullable<Decimal> avg_diff_price = null;

                if (recal == "1")
                {
                    if (pro_pricePeriod != "")
                        avg_pro_price = CrudeImportPlanServiceModel.CalPriceFormula(benchmark, matnum, "A", pro_pricePeriod, pro_crudePhet_Vol);

                    if (final_pricePeriod != "")
                        avg_final_price = CrudeImportPlanServiceModel.CalPriceFormula(benchmark, matnum, "A", final_pricePeriod, final_crudePhet_Vol);

                }

                //avg_pro_price = 46.2358M;
                //avg_final_price = 48.9051M;

                Nullable<Decimal> avg_fx_pro = 0M;
                Nullable<Decimal> avg_fx_final = 0M;
                string fx_period = final_pricePeriod;
                if (fx_period == "")
                    fx_period = pro_pricePeriod;


                var sDateFrom = String.IsNullOrEmpty(fx_period) ? "" : fx_period.Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(fx_period) ? "" : fx_period.Substring(14, 10);

                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                string yyyyMM = dateFrom.ToString("yyyyMM", provider);
                string b_yyyyMM = dateFrom.AddMonths(-1).ToString("yyyyMM", provider);
                string a_yyyyMM = dateFrom.AddMonths(1).ToString("yyyyMM", provider);
                string iDateFrom = dateFrom.ToString("yyyyMM", provider) + "01";
                string iDateTo = dateFrom.ToString("yyyyMM", provider) + "31";


                var queryFX = (from v in context.MKT_TRN_FX_B
                               where (v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD" && v.T_FXB_VALTYPE == "B")
                               && (v.T_FXB_VALDATE.Contains(yyyyMM) || v.T_FXB_VALDATE.Contains(b_yyyyMM))
                               select v).OrderBy(p => p.T_FXB_VALDATE);


                if (recal == "1")
                {
                    if (queryFX != null)
                        queryFX = queryFX.OrderByDescending(o => o.T_FXB_VALDATE);

                    avg_fx_pro = AvgFx(yyyyMM, context);
                    avg_fx_final = avg_fx_pro;
                }

                //&& (Convert.ToInt32(string.IsNullOrEmpty(v.T_FXB_VALDATE) ? "0" : v.T_FXB_VALDATE) <= Int32.Parse(iDateFrom)
                //                                          && Convert.ToInt32(string.IsNullOrEmpty(v.T_FXB_VALDATE) ? "0" : v.T_FXB_VALDATE) >= Int32.Parse(iDateTo)
                //                                      )

                Nullable<Decimal> FX_Before = 0;
                foreach (var ot in outturn)
                {
                    string s_tripno = ot.TRIP_NO ?? "";
                    string s_bldate = ot.BL_DATE ?? "";
                    string s_yyyymmdd = ot.YYYYMMDD ?? "";
                    string s_invoicno = "";

                    Nullable<Decimal> s_proprice = null;
                    Nullable<Decimal> s_finalprice = null;
                    Nullable<Decimal> s_diffprice = null;
                    Nullable<Decimal> s_outturn_qty = ot.QTY_BBL;
                    Nullable<Decimal> s_p1fxrate = null;
                    Nullable<Decimal> s_p1unitprice = null;
                    Nullable<Decimal> s_p1totBeVat = null;
                    Nullable<Decimal> s_p1Vat = null;
                    Nullable<Decimal> s_p1totAFVat = null;
                    Nullable<Decimal> s_p2FxRate = null;
                    Nullable<Decimal> s_p2UnitPrice = null;
                    Nullable<Decimal> s_p2DiffUnitPrice = null;
                    Nullable<Decimal> s_p2AdjPrice = null;
                    Nullable<Decimal> s_p2TotDiff = null;
                    string s_p2DiffDocNo = "";

                    Nullable<Decimal> s_f1FxRate = null;
                    Nullable<Decimal> s_f1UnitPrice = null;
                    Nullable<Decimal> s_f1TotBFVat = null;
                    Nullable<Decimal> s_f1TotAFVat = null;
                    Nullable<Decimal> s_f1Vat = null;

                    Nullable<Decimal> s_f2FxRate = null;
                    Nullable<Decimal> s_f2TotDiff1 = null;
                    Nullable<Decimal> s_f2TotDiff2 = null;
                    Nullable<Decimal> s_f2TotDiff3 = null;


                    var query = dailyPrice.ToList().Where(p => p.YYYYMMDD.Equals(s_yyyymmdd));
                    if (query != null)
                    {
                        if (query.Count() > 0)
                        {
                            var q = query.First();
                            s_invoicno = q.INVOICE_NO;
                            s_proprice = q.PRO_PRICE;
                            s_finalprice = q.FINAL_PRICE;
                            s_diffprice = q.DIFF_PRICE;
                            s_outturn_qty = q.OUTTURN_QTY;
                            s_p1fxrate = q.P1_FX_RATE;

                            s_p1unitprice = q.P1_UNIT_PRICE;
                            s_p1totBeVat = q.P1_TOTAL_BF_VAT;
                            s_p1Vat = q.P1_VAT;
                            s_p1totAFVat = q.P1_TOTAL_AF_VAT;

                            s_p2FxRate = q.P2_FX_RATE;
                            s_p2UnitPrice = q.P2_UNIT_PRICE;
                            s_p2DiffUnitPrice = q.P2_DIFF_UNITPRICE;
                            s_p2AdjPrice = q.P2_ADJUST_PRICE;
                            s_p2TotDiff = q.P2_TOTAL_DIFF;
                            s_p2DiffDocNo = q.P2_DIFF_DOCNO;

                            s_f1FxRate = q.F1_FX_RATE;
                            s_f1UnitPrice = q.F1_UNIT_PRICE;
                            s_f1TotBFVat = q.F1_TOTAL_BF_VAT;
                            s_f1TotAFVat = q.F1_TOTAL_AF_VAT;
                            s_f1Vat = q.F1_VAT;

                            s_f2FxRate = q.F2_FX_RATE;
                            s_f2TotDiff1 = q.F2_TOTAL_DIFF1;
                            s_f2TotDiff2 = q.F2_TOTAL_DIFF2;
                            s_f2TotDiff3 = q.F2_TOTAL_DIFF3;

                        }
                    }

                    // ให้ดึงค่าข้อมูลใหม่
                    if (recal == "1")
                    {
                        //var qFx = queryFX.Where(p => p.T_FXB_VALDATE.Equals(s_yyyymmdd));
                        var qFx = queryFX.AsEnumerable().Where(p => int.Parse(p.T_FXB_VALDATE) <= int.Parse(s_yyyymmdd));
                        if (qFx != null && qFx.Count() > 0)
                        {
                            s_p1fxrate = qFx.First().T_FXB_VALUE1;
                            s_f1FxRate = s_p1fxrate;
                            FX_Before = s_p1fxrate;
                        }
                        //else
                        //{
                        //    s_p1fxrate = FX_Before;
                        //    s_f1FxRate = FX_Before;
                        //}


                        s_proprice = avg_pro_price;
                        s_finalprice = avg_final_price;
                        s_p2FxRate = avg_fx_pro;
                        s_f2FxRate = avg_fx_final;
                        s_outturn_qty = ot.QTY_BBL;
                    }

                    DailyPrice.Add(new DailyPriceViewModel
                    {
                        TRIP_NO = s_tripno,
                        INVOICE_NO = s_invoicno,
                        BL_DATE = s_bldate,
                        PRO_PRICE = s_proprice,
                        FINAL_PRICE = s_finalprice,
                        DIFF_PRICE = s_diffprice,
                        OUTTURN_QTY = s_outturn_qty,

                        P1_FX_RATE = s_p1fxrate,
                        P1_UNIT_PRICE = s_p1unitprice,
                        P1_TOTAL_BF_VAT = s_p1totBeVat,
                        P1_VAT = s_p1Vat,
                        P1_TOTAL_AF_VAT = s_p1totAFVat,

                        P2_FX_RATE = s_p2FxRate,
                        P2_UNIT_PRICE = s_p2UnitPrice,
                        P2_DIFF_UNITPRICE = s_p2DiffUnitPrice,
                        P2_ADJUST_PRICE = s_p2AdjPrice,
                        P2_TOTAL_DIFF = s_p2TotDiff,
                        P2_DIFF_DOCNO = s_p2DiffDocNo,

                        F1_FX_RATE = s_f1FxRate,
                        F1_UNIT_PRICE = s_f1UnitPrice,
                        F1_TOTAL_BF_VAT = s_f1TotBFVat,
                        F1_TOTAL_AF_VAT = s_f1TotAFVat,
                        F1_VAT = s_f1Vat,

                        F2_FX_RATE = s_f2FxRate,
                        F2_TOTAL_DIFF1 = s_f2TotDiff1,
                        F2_TOTAL_DIFF2 = s_f2TotDiff2,
                        F2_TOTAL_DIFF3 = s_f2TotDiff3,

                    });

                }

            }
            catch (Exception ex) { }

            return DailyPrice;

        }

        private static void getProductApprove(ref List<CrudeApproveFormViewModel_SearchData> mApproveData, string ArriavalMonth = "", string LoadingMonth = "", string CrudeName = ""
                      , string Supplier = "", string Incoterm = "")
        {
            try
            {
                string sLoadingFrom = "";
                string sLoadingTo = "";
                string sDischargFrom = "";
                string sDischargTo = "";

                if (!string.IsNullOrEmpty(ArriavalMonth))
                {
                    ArriavalMonth = ArriavalMonth.Replace("to", "|");
                    sDischargFrom = ArriavalMonth.Split('|')[0].Trim() + " 00:00";
                    sDischargTo = ArriavalMonth.Split('|')[1].Trim() + " 23:59";
                }

                if (!string.IsNullOrEmpty(LoadingMonth))
                {
                    LoadingMonth = LoadingMonth.Replace("to", "|");
                    sLoadingFrom = LoadingMonth.Split('|')[0].Trim() + " 00:00";
                    sLoadingTo = LoadingMonth.Split('|')[1].Trim() + " 23:59";
                }


                string loadingPeriodFrom = sLoadingFrom;// "01/05/2017 00:00"; // dd/MM/yyyy HH:mm
                string loadingPeriodTo = sLoadingTo;// "30/06/2017 23:59";
                string dischargingPeriodFrom = sDischargFrom;
                string dischargingPeriodTo = sDischargTo;
                string crude = CrudeName; // "ORIENTE";
                string supplier = Supplier; // "Brightoil Petroleum (Singapore) Pte Ltd";
                string incoterm = Incoterm;

                List<VW_PAF_IMPORT> data2 = VW_PAF_IMPORT_DAL.GetData();
                if (data2 == null)
                    return;

                var sLoadingPeriodDateFrom = String.IsNullOrEmpty(loadingPeriodFrom) ? "" : loadingPeriodFrom.ToUpper().Substring(0, 10);
                var sLoadingPeriodDateTo = String.IsNullOrEmpty(loadingPeriodTo) ? "" : loadingPeriodTo.ToUpper().Substring(0, 10);

                var sDischargingPeriodDateFrom = String.IsNullOrEmpty(dischargingPeriodFrom) ? "" : dischargingPeriodFrom.ToUpper().Substring(0, 10);
                var sDischargingPeriodDateTo = String.IsNullOrEmpty(dischargingPeriodTo) ? "" : dischargingPeriodTo.ToUpper().Substring(0, 10);


                if (!incoterm.Equals(""))
                    data2 = data2.Where(p => p.INCOTERM.ToUpper().Equals(incoterm.ToUpper())).ToList();

                if (!CrudeName.Equals(""))
                    data2 = data2.Where(p => p.CRUDE_NAME.ToUpper().Equals(CrudeName.ToUpper())).ToList();

                if (!supplier.Equals(""))
                    data2 = data2.Where(p => p.SUPPLIER_NAME.ToUpper().Equals(supplier.ToUpper())).ToList();

                if (!string.IsNullOrEmpty(sLoadingPeriodDateFrom) && !string.IsNullOrEmpty(sLoadingPeriodDateTo))
                {
                    DateTime sDate = new DateTime();
                    DateTime eDate = new DateTime();
                    sDate = ShareFn.ConvertStrDateToDate(sLoadingPeriodDateFrom);
                    eDate = ShareFn.ConvertStrDateToDate(sLoadingPeriodDateTo).AddDays(1).AddSeconds(-1);
                    data2 = data2.Where(p => (p.LOADING_DATE_FROM >= sDate && p.LOADING_DATE_FROM <= eDate) && (p.LOADING_DATE_TO >= sDate && p.LOADING_DATE_TO <= eDate)).ToList();
                }

                if (!string.IsNullOrEmpty(sDischargingPeriodDateFrom) && !string.IsNullOrEmpty(sDischargingPeriodDateTo))
                {
                    DateTime sDate = new DateTime();
                    DateTime eDate = new DateTime();
                    sDate = ShareFn.ConvertStrDateToDate(sDischargingPeriodDateFrom);
                    eDate = ShareFn.ConvertStrDateToDate(sDischargingPeriodDateTo).AddDays(1).AddSeconds(-1);
                    data2 = data2.Where(p => (p.DISCHARGING_DATE_FROM >= sDate && p.DISCHARGING_DATE_FROM <= eDate) && (p.DISCHARGING_DATE_TO >= sDate && p.DISCHARGING_DATE_TO <= eDate)).ToList();
                }


                System.Globalization.CultureInfo cu = new System.Globalization.CultureInfo("en-US");

                List<MT_MATERIALS> queryMat = getMatMaster();

                var data = (from d in data2
                            join m in queryMat on d.CRUDE_CODE equals m.MET_NUM
                            select new
                            {
                                REF_ID = d.REF_ID,
                                CRUDE_CODE = d.CRUDE_CODE,
                                CRUDE_NAME = d.CRUDE_NAME,
                                SUPPLIER_CODE = d.SUPPLIER_CODE,
                                SUPPLIER_NAME = d.SUPPLIER_NAME,
                                COM_CODE = d.COM_CODE,
                                TOLERANCE = d.TOLERANCE,
                                DISCHARGING_DATE_FROM = d.DISCHARGING_DATE_FROM,
                                DISCHARGING_DATE_TO = d.DISCHARGING_DATE_TO,
                                ORIGIN_CODE = d.ORIGIN_CODE,
                                ORIGIN = d.ORIGIN,
                                GT_C = d.GT_C,
                                PAYMENT_TERM = d.PAYMENT_TERM,
                                BENCHMARK_CODE = d.BENCHMARK_CODE,
                                BENCHMARK = d.BENCHMARK,
                                LOADING_DATE_FROM = d.LOADING_DATE_FROM,
                                LOADING_DATE_TO = d.LOADING_DATE_TO,
                                CONTRACT_TYPE = d.CONTRACT_TYPE,
                                PRICING_PERIOD = d.PRICING_PERIOD,
                                OFFER_UNIT = d.OFFER_UNIT,
                                OFFER = d.OFFER,
                                INCOTERM = d.INCOTERM,
                                QUANTITY_MIN = d.QUANTITY_MIN,
                                QUANTITY_MAX = d.QUANTITY_MAX,
                                QUANTITY_UNIT = d.QUANTITY_UNIT,
                                MET_PRODUCT_DENSITY = m.MET_PRODUCT_DENSITY,
                                FORM_ID = d.FORM_ID,
                            });



                foreach (var view in data)
                {
                    string sLoadingMonth = "";
                    string sLoadingDate = "";
                    string vLoadingDateNum = "";

                    if (view.LOADING_DATE_FROM != null && view.LOADING_DATE_TO != null)
                    {
                        sLoadingDate = Convert.ToDateTime(view.LOADING_DATE_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(view.LOADING_DATE_TO).ToString("dd/MM/yyyy", cu);
                        sLoadingMonth = Convert.ToDateTime(view.LOADING_DATE_FROM).ToString("MMM/yyyy", cu);
                        vLoadingDateNum = Convert.ToDateTime(view.LOADING_DATE_FROM).ToString("yyyyMM", cu);
                    }

                    string sDischarge = "";
                    if (view.DISCHARGING_DATE_FROM != null & view.DISCHARGING_DATE_TO != null)
                        sDischarge = Convert.ToDateTime(view.DISCHARGING_DATE_FROM).ToString("dd/MM/yyyy", cu) + " to " + Convert.ToDateTime(view.DISCHARGING_DATE_TO).ToString("dd/MM/yyyy", cu);

                    string vArraivalDate = "";
                    string vArraivalMonth = "";
                    string vArraivalYear = "";
                    string vArraivalDateNum = "";

                    if (view.DISCHARGING_DATE_FROM != null)
                    {
                        DateTime dArraivalDate = Convert.ToDateTime(view.DISCHARGING_DATE_FROM);
                        vArraivalDate = dArraivalDate.ToString("MMM/yyyy", cu);
                        vArraivalMonth = Convert.ToInt16(dArraivalDate.ToString("MM")).ToString();
                        vArraivalYear = dArraivalDate.ToString("yyyy", cu);
                        vArraivalDateNum = dArraivalDate.ToString("yyyyMM", cu);
                    }


                    string sContType = string.IsNullOrEmpty(view.CONTRACT_TYPE) ? "" : view.CONTRACT_TYPE;
                    if (sContType.ToUpper().Contains("TERM"))
                        sContType = "Term";
                    else
                        sContType = "Spot";

                    mApproveData.Add(new CrudeApproveFormViewModel_SearchData
                    {
                        refID = view.REF_ID,
                        refTripNo = "",
                        ArraivalDate = vArraivalDate, //"Jan/2017",
                        ArraivalMonth = vArraivalMonth, // "01",
                        ArraivalYear = vArraivalYear, // "2017",
                        LoadingDate = sLoadingMonth, // string.IsNullOrEmpty(sLoadingDate) ? "" : DateTime.ParseExact(sLoadingDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MMM/yyyy"),  //, "Jan/2017",
                        CrudeName = view.CRUDE_NAME, // view.dCrudeId
                        SupplierName = view.SUPPLIER_NAME, // "SK global Chemical Singapore",
                        Price = (view.BENCHMARK ?? "") + ((view.OFFER == null) ? "" : ((view.OFFER > 0) ? "+" + view.OFFER.ToString() : view.OFFER.ToString())), // "DB(DEC)+0.1", // formula from approve
                        Incoterm = (view.INCOTERM ?? ""),
                        Formula = (view.BENCHMARK ?? ""), //view.dBechmarkPrice + ((string.IsNullOrEmpty(view.dOffer)) ? "" : (Convert.ToDecimal(view.dOffer) > 0) ? "+" + view.dOffer : view.dOffer), // "DB(DEC)+0.1", // formula from approve
                        GT_C = (view.GT_C ?? ""),
                        QTYKbbl = (view.QUANTITY_MAX != null) ? view.QUANTITY_MAX.ToString() : view.QUANTITY_MIN.ToString(),
                        QTYKKT = "0",
                        Tolerance = (view.TOLERANCE ?? ""), // "+10",
                        DischargeLayCan = sDischarge, //view.dDischargingPeriodDate, // "01/04/2017 to 31/05/2017",
                        LoadingDateFromTo = sLoadingDate,
                        com_code = (view.COM_CODE ?? ""), // "1100",
                        CrudeCode = (view.CRUDE_CODE ?? ""), // "Y900SN",
                        SupplierCode = (view.SUPPLIER_CODE ?? ""),
                        Origin = (view.ORIGIN ?? ""),
                        paymentTerm = (view.PAYMENT_TERM ?? ""),
                        bechmark = (view.BENCHMARK ?? ""),
                        ContractType = (sContType ?? ""),
                        PricingPeriod = "",
                        Offer = (view.OFFER == null) ? "" : view.OFFER.ToString(),
                        PurchaseNo = view.FORM_ID ?? "",
                        QTYUnit = view.QUANTITY_UNIT,
                        Density = string.IsNullOrEmpty(view.MET_PRODUCT_DENSITY) ? "" : view.MET_PRODUCT_DENSITY,
                        ArraivalDateNum = vArraivalDateNum,
                        LoadingDateNum = vLoadingDateNum,
                        PerformanceBond = "",
                    });
                }

            }
            catch (Exception ex) { }
        }

        private static void getCrudePurchaseApprove(ref List<CrudeApproveFormViewModel_SearchData> mApproveData, string ArriavalMonth = "", string LoadingMonth = "", string CrudeName = ""
                        , string Supplier = "", string Incoterm = "")
        {

            try
            {
                string sLoadingFrom = "";
                string sLoadingTo = "";
                string sDischargFrom = "";
                string sDischargTo = "";

                if (!string.IsNullOrEmpty(ArriavalMonth))
                {
                    ArriavalMonth = ArriavalMonth.Replace("to", "|");
                    sDischargFrom = ArriavalMonth.Split('|')[0].Trim() + " 00:00";
                    sDischargTo = ArriavalMonth.Split('|')[1].Trim() + " 23:59";
                }

                if (!string.IsNullOrEmpty(LoadingMonth))
                {
                    LoadingMonth = LoadingMonth.Replace("to", "|");
                    sLoadingFrom = LoadingMonth.Split('|')[0].Trim() + " 00:00";
                    sLoadingTo = LoadingMonth.Split('|')[1].Trim() + " 23:59";
                }


                CrudePurchaseServiceModel service = new CrudePurchaseServiceModel();
                string loadingPeriodFrom = sLoadingFrom;// "01/05/2017 00:00"; // dd/MM/yyyy HH:mm
                string loadingPeriodTo = sLoadingTo;// "30/06/2017 23:59";
                string dischargingPeriodFrom = sDischargFrom;
                string dischargingPeriodTo = sDischargTo;
                string crude = CrudeName; // "ORIENTE";
                string supplier = Supplier; // "Brightoil Petroleum (Singapore) Pte Ltd";
                string incoterm = Incoterm;
                CrudePurchaseReportViewModel_Search_PIT result = service.getDataCrudePurchase(loadingPeriodFrom, loadingPeriodTo, dischargingPeriodFrom, dischargingPeriodTo, crude, supplier, incoterm);
                List<CrudePurchaseReportViewModel_SearchData_PIT> t = result.sSearchData;
                //var t1 = t.dSupplier;
                //var t2 = t.dIncoterms;

                //String rr = "201705031421120104138";

                foreach (CrudePurchaseReportViewModel_SearchData_PIT view in t)
                {
                    string sLoadingDate = "";
                    string sArraivalDate = "";
                    string vArraivalDate = "";
                    string vArraivalMonth = "";
                    string vArraivalYear = "";

                    string vArraivalDateNum = "";
                    string vLoadingDateNum = "";

                    if (!string.IsNullOrEmpty(view.dLoadingPeriodDate))
                    {

                        sLoadingDate = view.dLoadingPeriodDate.Replace("to", "-").Split('-')[0].Trim();
                        DateTime dLoadDate = DateTime.ParseExact(sLoadingDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        vLoadingDateNum = dLoadDate.ToString("yyyyMM");
                    }

                    if (!string.IsNullOrEmpty(view.dDischargingPeriodDate))
                    {
                        sArraivalDate = view.dDischargingPeriodDate.Replace("to", "-").Split('-')[0].Trim();
                        DateTime dArraivalDate = DateTime.ParseExact(sArraivalDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        vArraivalDate = dArraivalDate.ToString("MMM/yyyy");
                        vArraivalMonth = Convert.ToInt16(dArraivalDate.ToString("MM")).ToString();
                        vArraivalYear = dArraivalDate.ToString("yyyy");

                        vArraivalDateNum = dArraivalDate.ToString("yyyyMM");
                    }

                    string adjPrice = "";
                    string sTerms = view.dTerms ?? "";
                    string[] arrCheck = sTerms.ToUpper().Replace("TERM", "|").Split('|');

                    if (arrCheck.Length > 1)
                    {
                        adjPrice = "(B/L Month)";
                    }
                    else {
                        string sPricePeriod = string.IsNullOrEmpty(view.dPricingPeriodDate) ? "" : view.dPricingPeriodDate.ToString();
                        if (sPricePeriod != "")
                        {
                            string[] arrDate = sPricePeriod.Replace(" to ", "-").Split('-');
                            if (arrDate.Length > 1)
                            {
                                DateTime d1 = DateTime.ParseExact(arrDate[0], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                string d1check = d1.ToString("yyyyMM");
                                string d2check = DateTime.ParseExact(arrDate[1], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyyMM");

                                if (d1check == d2check)
                                {
                                    adjPrice = "(" + d1.ToString("MMM") + ")";
                                }
                                else
                                {
                                    adjPrice = "(B/L Month)";
                                }
                            }
                        }
                    }

                    

                    string sdOffer = "";
                    if (!string.IsNullOrEmpty(view.dOffer)) {
                        Decimal dOffer = Convert.ToDecimal(view.dOffer);
                        if (dOffer == 0)
                        {

                        }
                        else if (dOffer > 0) 
                            sdOffer = "+" + view.dOffer; 
                        else  
                            sdOffer = view.dOffer; 
                    }


                    mApproveData.Add(new CrudeApproveFormViewModel_SearchData
                    {
                        refID = view.dRowId, // rr.ToString(),
                        refTripNo = "",
                        ArraivalDate = vArraivalDate, //"Jan/2017",
                        ArraivalMonth = vArraivalMonth, // "01",
                        ArraivalYear = vArraivalYear, // "2017",
                        LoadingDate = string.IsNullOrEmpty(sLoadingDate) ? "" : DateTime.ParseExact(sLoadingDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MMM/yyyy"),  //, "Jan/2017",
                        CrudeName = view.dCrudeName, // view.dCrudeId
                        SupplierName = view.dSupplier, // "SK global Chemical Singapore",
                        Price = view.dBechmarkPrice + adjPrice + sdOffer, //((string.IsNullOrEmpty(view.dOffer)) ? "" : (Convert.ToDecimal(view.dOffer) > 0) ? "+" + view.dOffer : view.dOffer), // "DB(DEC)+0.1", // formula from approve
                        Incoterm = view.dIncoterms, // "CFR",
                        Formula = view.dBechmarkPrice + ((string.IsNullOrEmpty(view.dOffer)) ? "" : (Convert.ToDecimal(view.dOffer) > 0) ? "+" + view.dOffer : view.dOffer), // "DB(DEC)+0.1", // formula from approve
                        GT_C = view.dGtc, // "ADNOC",
                        QTYKbbl = view.dQuantity,
                        QTYKKT = "0",
                        Tolerance = view.dTolerance, // "+10",
                        DischargeLayCan = view.dDischargingPeriodDate, // "01/04/2017 to 31/05/2017",
                        LoadingDateFromTo = view.dLoadingPeriodDate,
                        com_code = getCompanyCode(view.dFor), // "1100",
                        CrudeCode = view.dCrudeId, // "Y900SN",
                        SupplierCode = view.dSupplierId,
                        Origin = view.dOrigin,
                        paymentTerm = view.dPaymentTermsDetail,
                        bechmark = view.dBechmarkPrice,
                        ContractType = view.dTerms,
                        PricingPeriod = view.dPricingPeriodDate,
                        Offer = view.dOffer,
                        PurchaseNo = view.dPurchaseNo,
                        QTYUnit = view.dQuantityUnit,
                        Density = string.IsNullOrEmpty(view.dDensity) ? "" : view.dDensity,
                        ArraivalDateNum = vArraivalDateNum,
                        LoadingDateNum = vLoadingDateNum,
                        PerformanceBond = view.dPerBond,
                    });
                }

            }
            catch (Exception ex)
            {

            }

        }

        public static List<CrudeApproveFormViewModel_SearchData> getCrudeApprove(string ArriavalMonth = "", string LoadingMonth = "", string CrudeName = ""
                        , string Supplier = "", string Incoterm = "", string SearchType = "")
        {
            List<CrudeApproveFormViewModel_SearchData> mApproveData = new List<CrudeApproveFormViewModel_SearchData>();

            if (string.IsNullOrEmpty(SearchType))
            {

                getCrudePurchaseApprove(ref mApproveData, ArriavalMonth, LoadingMonth, CrudeName, Supplier, Incoterm);

            }
            else if (SearchType.Equals("CRD"))
            {
                getCrudePurchaseApprove(ref mApproveData, ArriavalMonth, LoadingMonth, CrudeName, Supplier, Incoterm);

            }
            else if (SearchType.Equals("PRD"))
            {
                getProductApprove(ref mApproveData, ArriavalMonth, LoadingMonth, CrudeName, Supplier, Incoterm);
            }



            return mApproveData;
        }

        public static string getFIDocSTS(string tripno = "")
        {
            string ret = "";
            try
            {
                

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_CUSTOMS_VAT
                                 where v.TRIP_NO.Equals(tripno)
                                 select new
                                 {
                                     TripNo = v.TRIP_NO ?? "",
                                     ItemNo = (v.MAT_ITEM_NO == null) ? "0" : v.MAT_ITEM_NO.ToString(),
                                     FIDoc = v.SAP_FI_DOC ?? "",
                                 }
                        );

                    if (query != null) {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        ret = js.Serialize(query);
                    }

                }
                 

            }
            catch (Exception ex)
            {

            }

            return ret;
        }

        public static string getPricePeriod(string cda_id = "")
        {
            string ret = "";
            try
            {
                string fromDate = "";
                string toDate = "";
                System.Globalization.CultureInfo cu = new System.Globalization.CultureInfo("en-US");

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.CDP_DATA
                                 where v.CDA_ROW_ID.Equals(cda_id)
                                 select new
                                 {
                                     CDA_PRICING_PERIOD_FROM = v.CDA_PRICING_PERIOD_FROM,
                                     CDA_PRICING_PERIOD_TO = v.CDA_PRICING_PERIOD_TO
                                 }
                        ).FirstOrDefault();

                    if (query != null)
                    {
                        fromDate = (query.CDA_PRICING_PERIOD_FROM == null) ? "" : Convert.ToDateTime(query.CDA_PRICING_PERIOD_FROM).ToString("dd/MM/yyyy", cu);
                        toDate = (query.CDA_PRICING_PERIOD_FROM == null) ? "" : Convert.ToDateTime(query.CDA_PRICING_PERIOD_TO).ToString("dd/MM/yyyy", cu);
                    }

                }

                if (fromDate != "" || toDate != "")
                {
                    if (fromDate == "") fromDate = toDate;
                    if (toDate == "") toDate = fromDate;

                    ret = fromDate + "to " + toDate;
                }
            }
            catch (Exception ex)
            {

            }
            return ret;
        }

        public static Decimal CalPriceFormula(string benchmark = "", string met_num = "", string price_status = "", string date_from_to = "", string CrudePhet_Vol = "")
        {
            //BASE_PRICE
            //OSP_PREMIUM_DISCOUNT
            //TRADING_PREMIUM_DISCOUNT
            //OTHER_COST

            Decimal ret = 0;
            FormulaPricingServiceModel service = new FormulaPricingServiceModel();
            List<SelectListItem> data = service.PriceCalculate(benchmark, met_num, price_status, date_from_to, CrudePhet_Vol, null, "PCF");


            if (data != null && data.Count > 0)
            {
                foreach (var row in data)
                {
                    ret += string.IsNullOrEmpty(row.Value) ? 0 : Convert.ToDecimal(row.Value);
                }
            }

            return ret;
        }

        public static String CalPriceFormula_RetJson(string benchmark = "", string met_num = "", string price_status = "", string date_from_to = "", string CrudePhet_Vol = "", System.Collections.ArrayList fixedData = null)
        {
            //BASE_PRICE
            //OSP_PREMIUM_DISCOUNT
            //TRADING_PREMIUM_DISCOUNT
            //OTHER_COST

            string ret = "";
            CrudeImportPlanServiceModel serviceModel = new CrudeImportPlanServiceModel();

            FormulaPricingServiceModel service = new FormulaPricingServiceModel();
            List<SelectListItem> data = service.PriceCalculate(benchmark, met_num, price_status, date_from_to, CrudePhet_Vol, fixedData, "PCF");

            string base_price = "0";
            string osp = "0";
            string tpd = "0";
            string other_cost = "0";
            string osp_month = "";
            if (data != null)
            {
                foreach (var item in data)
                {
                    if (item.Text == "BASE_PRICE")
                    {
                        base_price = item.Value;
                    }
                    else if (item.Text == "OSP_PREMIUM_DISCOUNT")
                    {
                        osp = item.Value;
                    }
                    else if (item.Text == "TRADING_PREMIUM_DISCOUNT")
                    {
                        tpd = item.Value;
                    }
                    else if (item.Text == "OTHER_COST")
                    {
                        other_cost = item.Value;
                    }
                    else if (item.Text == "OSP_MONTH_PRICE")
                    {
                        osp_month = item.Value;
                    }
                }
            }

            ret = base_price + "|" + osp + "|" + tpd + "|" + other_cost + "|" + osp_month;


            return ret;
        }

        private Double CalVolByForUnit(MaterialViewModel itemMatView)
        {
            Double retVal = 0;
            try
            {
                if (itemMatView.PMA_PURCHASE_UNIT == "BBL")
                {
                    retVal = string.IsNullOrEmpty(itemMatView.PMA_VOLUME_BBL) ? 0.0 : Convert.ToDouble(itemMatView.PMA_VOLUME_BBL);
                }
                else if (itemMatView.PMA_PURCHASE_UNIT == "MT")
                {
                    retVal = string.IsNullOrEmpty(itemMatView.PMA_VOLUME_MT) ? 0 : Convert.ToDouble(itemMatView.PMA_VOLUME_MT);
                }
                else if (itemMatView.PMA_PURCHASE_UNIT == "ML")
                {
                    retVal = string.IsNullOrEmpty(itemMatView.PMA_VOLUME_ML) ? 0 : Convert.ToDouble(itemMatView.PMA_VOLUME_ML);
                }
            }
            catch (Exception ex) { }

            return retVal;
        }
        private void CalFreightAmountInTrip(PCF_MATER_TYPE PCF_MATER_TYPE, List<FreightViewModel> Freight
                        , ref Double FreightAmountInTripUSD, ref Double FreightAmountInTripTHB)
        {


            try
            {

                if (Freight == null)
                    return;

                var query = (from m in PCF_MATER_TYPE.FreightTypeViewModel
                             join f in Freight on m.PMF_CODE.ToString() equals f.FR_CostType
                             where m.PMF_COST_TYPE.Equals("X")
                             select new
                             {
                                 FreightAmt = f.FR_Amt,
                                 FreightCurrency = f.FR_AmtUnit,
                                 ExRateAgree = f.FR_ExcAgree,
                                 ExRateBOT = f.FR_ExcBOT,

                             }).ToList();


                if (query != null)
                {
                    foreach (var item in query)
                    {
                        Double FX = 0;
                        FX = string.IsNullOrEmpty(item.ExRateAgree) ? 0 : Convert.ToDouble(item.ExRateAgree);
                        if (FX == 0)
                        {
                            FX = string.IsNullOrEmpty(item.ExRateBOT) ? 0 : Convert.ToDouble(item.ExRateBOT);
                        }

                        Double FAmount = string.IsNullOrEmpty(item.FreightAmt) ? 0 : Convert.ToDouble(item.FreightAmt.Replace(",", ""));

                        if (item.FreightCurrency.Equals("USD"))
                        {
                            FreightAmountInTripUSD += FAmount;
                            FreightAmountInTripTHB += FAmount * FX;
                        }
                        else if (item.FreightCurrency.Equals("THB"))
                        {
                            if (FX != 0)
                                FreightAmountInTripUSD += FAmount / FX;

                            FreightAmountInTripTHB += FAmount;

                        }
                        else
                        {

                        }



                    }

                }



            }
            catch (Exception ex)
            {

            }

        }

        private Double CalFreightPrice(Double FreightAmount, Double VolByForUnit, string CurrencyPrice, Double ExchangeRate)
        {
            Double retVol = 0.0;
            try
            {
                if (VolByForUnit == 0)
                    return retVol;

                if (CurrencyPrice == "USD")
                {
                    retVol = FreightAmount / VolByForUnit;
                }
                else if (CurrencyPrice == "THB")
                {
                    if (ExchangeRate == 0)
                    {
                        retVol = FreightAmount / VolByForUnit;
                    }
                    else
                        retVol = (FreightAmount / VolByForUnit) / ExchangeRate;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {

            }
            return retVol;
        }

        private void CalCrudeTotalAmountUSDTHB(Double CrudeTotalAmount, Double CrudeFX, string Currency
                                    , ref Double CrudeTotalAmountUSD, ref Double CrudeTotalAmountTHB)
        {
            try
            {
                if (Currency.Equals("USD"))
                {
                    CrudeTotalAmountUSD = CrudeTotalAmount;
                    CrudeTotalAmountTHB = CrudeTotalAmount * CrudeFX;
                }
                else if (Currency.Equals("THB"))
                {
                    CrudeTotalAmountUSD = CrudeTotalAmount / CrudeFX;
                    CrudeTotalAmountTHB = CrudeTotalAmount;
                }
                else
                {

                }

            }
            catch (Exception ex)
            {

            }
        }

        private Double CalFreightPrice(Double FreightAmountUSD, Double VolByForUnit)
        {
            Double retVol = 0.0;
            try
            {
                if (VolByForUnit != 0)
                    retVol = FreightAmountUSD / VolByForUnit;
            }
            catch (Exception ex)
            {

            }
            return retVol;
        }

        private Double GetMTInsuranceRate()
        {
            Double retVol = 0.0;
            try
            {
                //var queryMatPrice = modelData.crude_approve_detail.dDetail.Where(p => p.MatItemNo.Equals(item.PMA_ITEM_NO)).OrderByDescending(o => o.MatPriceItemNo).First();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_MT_INSURANCE orderby v.PIN_EFFECTIVE_TO descending select v).First();

                    if (query != null)
                    {
                        retVol = (query.PIN_RATE == null) ? 0.0 : Convert.ToDouble(query.PIN_RATE.ToString());
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return retVol;
        }

        private Double CalInsurPrice(Double CrudeTotalAmountUSD, Double FreightPriceUSD, Double VolByForUnit
                                , Double InsuranceRate, Double MTInsuranceRate)
        {
            Double retVol = 0;
            try
            {

                if (InsuranceRate == 0)
                {
                    InsuranceRate = MTInsuranceRate;
                }

                // ถ้ายังไม่มี InsuranceRate ก็ให้เอา 1 ไปคูณ (คือใช้ค่าเดิม)
                //if (InsuranceRate == 0)
                //    InsuranceRate = 1;

                Double A = 0;
                A = (CrudeTotalAmountUSD + (FreightPriceUSD * VolByForUnit)) * InsuranceRate;

                //ROUND(A+A,2)/2+ROUND(((ROUND(A+A,2)/2)*0.4)/100,2)
                retVol = Math.Round(A + A, 2) / 2 + Math.Round(((Math.Round(A + A, 2) / 2) * 0.4) / 100, 2);

            }
            catch (Exception ex) { }

            return retVol;
        }

        private void GetVatFactor(string CrudeCode, ref string FType, ref Double Factor)
        {
            try
            {
                //var queryMatPrice = modelData.crude_approve_detail.dDetail.Where(p => p.MatItemNo.Equals(item.PMA_ITEM_NO)).OrderByDescending(o => o.MatPriceItemNo).First();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.PCF_VAT_FACTOR where v.MET_NUM.Equals(CrudeCode) select v).First();

                    if (query != null)
                    {
                        if (query.IMPORT_DUTY_F1.Equals("X"))
                        {
                            FType = "1";
                            Factor = (query.IMPORT_DUTY_F1_CONST == null) ? 0 : Convert.ToDouble(query.IMPORT_DUTY_F1_CONST.ToString());
                        }
                        else if (query.IMPORT_DUTY_F2.Equals("X"))
                        {
                            FType = "2";
                            Factor = (query.IMPORT_DUTY_F2_CONST == null) ? 0 : Convert.ToDouble(query.IMPORT_DUTY_F2_CONST.ToString());
                        }

                    }

                }
            }
            catch (Exception ex)
            {

            }

        }

        private void CalTotalAmount(string CrudeCode, Double VolByForUnit, Double CrudeAmountTHB, Double FreightAmountTHB
            , Double InsuranceAmountTHB, Double CrudeExchangeRate, Double VolL30, Double VolBBL, ref Double TotalAmountTHB, ref Double TotalAmountUSD)
        {

            try
            {
                string FType = "";
                Double Factor = 0;
                GetVatFactor(CrudeCode, ref FType, ref Factor);

                Double Total_CFI = (CrudeAmountTHB + FreightAmountTHB + InsuranceAmountTHB);
                Double ImpDutyAmt = 0;
                // 0.1
                if (FType == "1")
                {
                    ImpDutyAmt = Total_CFI * Factor;
                }
                // 0.01
                else if (FType == "2")
                {
                    Double vol = VolL30;
                    if (vol == 0)
                        vol = (VolBBL * 158.9873);

                    ImpDutyAmt = vol * Factor;
                }

                TotalAmountTHB = Total_CFI + ImpDutyAmt;

                if (CrudeExchangeRate != 0)
                    TotalAmountUSD = TotalAmountTHB / CrudeExchangeRate;


            }
            catch (Exception ex)
            {

            }
        }

        private Double CalImportDuty(string FType, Double Factor, Double CrudeAmount, Double FreightAmount, Double InsuranceAmount
              , Double VolL30, Double VolBBL, String CalCurrency, Double ExchangeRate)
        {
            Double ImpDutyAmt = 0.0;
            try
            {
                //string FType = "";
                //Double Factor = 0;
                //GetVatFactor(CrudeCode, ref FType, ref Factor);

                Double Total_CFI = (CrudeAmount + FreightAmount + InsuranceAmount);
                // 0.1
                if (FType == "1")
                {
                    ImpDutyAmt = Total_CFI * Factor;
                }
                // 0.01
                else if (FType == "2")
                {
                    Double vol = VolL30;
                    if (vol == 0)
                        vol = (VolBBL * 158.9873);

                    // เป็นเงินบาทเสมอ
                    ImpDutyAmt = vol * Factor;

                    if (CalCurrency.Equals("USD"))
                    {
                        if (ExchangeRate == 0)
                            ImpDutyAmt = 0;
                        else
                            ImpDutyAmt = ImpDutyAmt / ExchangeRate;
                    }

                }


            }
            catch (Exception ex)
            {

            }

            return ImpDutyAmt;
        }


        private string getPricingDataFrom(string user_group)
        {
            string ret = "";
            if (user_group.Equals("plan"))
            {
                ret = "PLANNING";
            }
            else if (user_group.Equals("book"))
            {
                ret = "BOOKING";
            }
            else if (user_group.Equals("all"))
            {
                ret = "ACCOUNTING";
            }
            return ret;
        }

        private void DeleteFile(string rootPath, string subFolder, string fileName)
        {
            try
            {
                string fullFileName = rootPath + subFolder + fileName;
                if (System.IO.File.Exists(fullFileName))
                {
                    System.IO.File.Delete(fullFileName);
                }

            }
            catch (Exception ex) { }
        }

        public Boolean SaveCrudeImportPlan(ref CrudeImportPlanViewModel model, string matType, string user_group, string FilterTripNo
            , string rootPath, ref string msg)
        {
            Boolean ret = true;


            PCF_MATER_TYPE PCF_MATER_TYPE = null;
            string FreightTypeConfig = model.json_FreightTypeConfig;
            if (FreightTypeConfig != "")
            {
                //SurveyorTypeJSON = SurveyorTypeJSON.Replace("null", "\"\"");
                FreightTypeConfig = "{ \"FreightTypeViewModel\":" + FreightTypeConfig.Replace("null", "\"\"") + "}";
            }
            PCF_MATER_TYPE = (PCF_MATER_TYPE)Newtonsoft.Json.JsonConvert.DeserializeObject(FreightTypeConfig, typeof(PCF_MATER_TYPE));

            if (model != null && model.crude_approve_detail != null && model.crude_approve_detail.dDetail_Header != null)
            {
                int iSuccess = 0;
                int iError = 0;
                model.MsgAlert = "";

                Double MTInsuranceRate = 0; // GetMTInsuranceRate();

                CrudeImportPlanViewModel modelData = null;
                string p_tripno = "X";
                string tripno = "";
                string dataAction = "";
                var headerList = model.crude_approve_detail.dDetail_Header;
                if (FilterTripNo != "")
                {
                    headerList = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(FilterTripNo)).ToList();
                }

                foreach (HeaderViewModel hRow in headerList)
                {
                    dataAction = "insert";
                    p_tripno = "X";

                    if (hRow.PHE_ACTION != "INSERT")
                        p_tripno = hRow.PHE_TRIP_NO;

                    hRow.PHE_MAT_TYPE = matType;

                    if (matType.Equals("PRD"))
                    {
                        hRow.PHE_PLANNING_RELEASE = "Y";
                    }

                    modelData = new CrudeImportPlanViewModel();
                    modelData.crude_approve_detail = new CrudeApproveFormViewModel_Detail();

                    tripno = hRow.PHE_TRIP_NO;

                    modelData.crude_approve_detail.dDetail_Header = model.crude_approve_detail.dDetail_Header.Where(q => q.PHE_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_Material != null)
                        modelData.crude_approve_detail.dDetail_Material = model.crude_approve_detail.dDetail_Material.Where(q => q.PMA_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail != null)
                        modelData.crude_approve_detail.dDetail = model.crude_approve_detail.dDetail.Where(q => q.h_TripNo.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_Freight != null)
                        modelData.crude_approve_detail.dDetail_Freight = model.crude_approve_detail.dDetail_Freight.Where(q => q.FR_TripNo.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_Surveyor != null)
                        modelData.crude_approve_detail.dDetail_Surveyor = model.crude_approve_detail.dDetail_Surveyor.Where(q => q.Sur_TripNo.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_Other != null)
                        modelData.crude_approve_detail.dDetail_Other = model.crude_approve_detail.dDetail_Other.Where(q => q.OC_TripNo.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_FreightFile != null)
                        modelData.crude_approve_detail.dDetail_FreightFile = model.crude_approve_detail.dDetail_FreightFile.Where(q => q.PAF_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_MaterialFile != null)
                        modelData.crude_approve_detail.dDetail_MaterialFile = model.crude_approve_detail.dDetail_MaterialFile.Where(q => q.PAF_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_CustomsFile != null)
                        modelData.crude_approve_detail.dDetail_CustomsFile = model.crude_approve_detail.dDetail_CustomsFile.Where(q => q.PAF_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_SurveyorFile != null)
                        modelData.crude_approve_detail.dDetail_SurveyorFile = model.crude_approve_detail.dDetail_SurveyorFile.Where(q => q.PAF_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_OtherFile != null)
                        modelData.crude_approve_detail.dDetail_OtherFile = model.crude_approve_detail.dDetail_OtherFile.Where(q => q.PAF_TRIP_NO.Equals(tripno)).ToList();

                    if (model.crude_approve_detail.dDetail_DailyPrice != null)
                        modelData.crude_approve_detail.dDetail_DailyPrice = model.crude_approve_detail.dDetail_DailyPrice.Where(q => q.TRIP_NO.Equals(tripno)).ToList();


                    #region "################## PRICE to SAP "##################"

                    Double FreightAmountInTripUSD = 0;
                    Double FreightAmountInTripTHB = 0;
                    CalFreightAmountInTrip(PCF_MATER_TYPE, modelData.crude_approve_detail.dDetail_Freight, ref FreightAmountInTripUSD, ref FreightAmountInTripTHB);

                    var queryIncoterm = getMTIncotermsAll();

                    Double QTYTotalInTrip = 0;
                    foreach (var item in modelData.crude_approve_detail.dDetail_Material)
                    {
                        QTYTotalInTrip = QTYTotalInTrip + CalVolByForUnit(item);
                    }

                    modelData.crude_approve_detail.dDetail_PriceToSap = new List<PriceToSapViewModel>();
                    foreach (var item in modelData.crude_approve_detail.dDetail_Material)
                    {
                        if (modelData.crude_approve_detail.dDetail == null) continue;


                        if (item.PMA_PURCHASE_TYPE.Equals("2")) continue;

                        Boolean sendInsurance = false;

                        string incoterm = string.IsNullOrEmpty(item.PMA_INCOTERMS) ? "" : item.PMA_INCOTERMS;

                        var iIncoterm = queryIncoterm.Where(p => p.MIN_INCOTERMS.Equals(incoterm) && p.MIN_HAVE_INSURANCE.Equals("Y"));
                        if (iIncoterm != null)
                        {
                            if (iIncoterm.Count() > 0)
                                sendInsurance = true;
                        }


                        var queryMatPriceTmp = modelData.crude_approve_detail.dDetail.Where(p => p.MatItemNo.Equals(item.PMA_ITEM_NO)).OrderByDescending(o => o.MatPriceItemNo);
                        if (queryMatPriceTmp == null || queryMatPriceTmp.Count() == 0) continue;

                        var queryMatPrice = queryMatPriceTmp.First();

                        Double VolByForUnit = CalVolByForUnit(item);
                        string CrudeCurrency = string.IsNullOrEmpty(queryMatPrice.i_CURRENCY) ? "" : queryMatPrice.i_CURRENCY;
                        string qtyUnit = string.IsNullOrEmpty(item.PMA_PURCHASE_UNIT) ? "" : item.PMA_PURCHASE_UNIT;
                        if (CrudeCurrency.Equals("") || qtyUnit.Equals(""))
                            continue;

                        Double CrudeExchangeRate = string.IsNullOrEmpty(queryMatPrice.ExAgreement) ? 0 : Convert.ToDouble(queryMatPrice.ExAgreement);
                        if (CrudeExchangeRate == 0)
                        {
                            CrudeExchangeRate = string.IsNullOrEmpty(queryMatPrice.ExBOT) ? 0 : Convert.ToDouble(queryMatPrice.ExBOT);
                        }

                        Double VolL30 = string.IsNullOrEmpty(item.PMA_VOLUME_ML) ? 0 : Convert.ToDouble(item.PMA_VOLUME_ML.Replace(",", ""));
                        Double VolBBL = string.IsNullOrEmpty(item.PMA_VOLUME_BBL) ? 0 : Convert.ToDouble(item.PMA_VOLUME_BBL.Replace(",", ""));

                        Double CrudeTotalAmount = string.IsNullOrEmpty(queryMatPrice.i_TOTAL_AMOUNT) ? 0 : Convert.ToDouble(queryMatPrice.i_TOTAL_AMOUNT.Replace(",", ""));
                        Double InsuranceRate = string.IsNullOrEmpty(item.CST_INSURANCE_RATE) ? 0 : Convert.ToDouble(item.CST_INSURANCE_RATE.Replace(",", ""));

                        string CMCS_ETA = "";
                        string PHE_ACTUAL_ETA_FROM = modelData.crude_approve_detail.dDetail_Header[0].PHE_ACTUAL_ETA_FROM;
                        if (string.IsNullOrEmpty(PHE_ACTUAL_ETA_FROM) == false)
                        {
                            PHE_ACTUAL_ETA_FROM = PHE_ACTUAL_ETA_FROM.Replace("to", "|");
                            CMCS_ETA = PHE_ACTUAL_ETA_FROM.Split('|')[1].ToString().Trim();
                        }

                        Double CrudeTotalAmountUSD = 0;
                        Double CrudeTotalAmountTHB = 0;
                        CalCrudeTotalAmountUSDTHB(CrudeTotalAmount, CrudeExchangeRate, CrudeCurrency, ref CrudeTotalAmountUSD, ref CrudeTotalAmountTHB);

                        Double FreightPriceUSD = 0;
                        Double FreightPriceTHB = 0;
                        //FreightPriceUSD = CalFreightPrice(FreightAmountInTripUSD, VolByForUnit);
                        //FreightPriceTHB = CalFreightPrice(FreightAmountInTripTHB, VolByForUnit);

                        FreightPriceUSD = CalFreightPrice(FreightAmountInTripUSD, QTYTotalInTrip);
                        FreightPriceTHB = CalFreightPrice(FreightAmountInTripTHB, QTYTotalInTrip);


                        Double InsuranceAmountUSD = 0;
                        Double InsuranceAmountTHB = 0;
                        InsuranceAmountUSD = CalInsurPrice(CrudeTotalAmountUSD, FreightPriceUSD, VolByForUnit, InsuranceRate, MTInsuranceRate);
                        InsuranceAmountTHB = CalInsurPrice(CrudeTotalAmountTHB, FreightPriceTHB, VolByForUnit, InsuranceRate, MTInsuranceRate);

                        Double InsurancePriceUSD = 0;
                        Double InsurancePriceTHB = 0;
                        if (VolByForUnit != 0)
                        {
                            InsurancePriceUSD = InsuranceAmountUSD / VolByForUnit;
                            InsurancePriceTHB = InsuranceAmountTHB / VolByForUnit;
                        }


                        Double FreightAmountUSD = FreightPriceUSD * VolByForUnit;
                        Double FreightAmountTHB = FreightPriceTHB * VolByForUnit;


                        Double ImportDutyAmountTHB = 0;
                        Double ImportDutyAmountUSD = 0;
                        string FType = "";
                        Double Factor = 0;
                        GetVatFactor(item.PMA_MET_NUM, ref FType, ref Factor);

                        ImportDutyAmountTHB = CalImportDuty(FType, Factor, CrudeTotalAmountTHB, FreightAmountTHB, InsuranceAmountTHB, VolL30, VolBBL, "THB", CrudeExchangeRate);
                        ImportDutyAmountUSD = CalImportDuty(FType, Factor, CrudeTotalAmountUSD, FreightAmountUSD, InsuranceAmountUSD, VolL30, VolBBL, "USD", CrudeExchangeRate);


                        Double TotalAmountTHB = 0;
                        Double TotalAmountUSD = 0;
                        if (CrudeCurrency == "USD")
                        {
                            TotalAmountUSD = CrudeTotalAmountUSD + FreightAmountUSD + InsuranceAmountUSD + ImportDutyAmountUSD;
                            TotalAmountTHB = TotalAmountUSD * CrudeExchangeRate;
                        }
                        else
                        {
                            TotalAmountTHB = CrudeTotalAmountTHB + FreightAmountTHB + InsuranceAmountTHB + ImportDutyAmountTHB;
                            if (CrudeExchangeRate != 0)
                                TotalAmountUSD = TotalAmountTHB / CrudeExchangeRate;

                        }

                        //CalTotalAmount(item.PMA_MET_NUM, VolByForUnit, CrudeTotalAmountTHB, FreightAmountTHB, InsuranceAmountTHB, CrudeExchangeRate
                        //                , VolL30, VolBBL, ref TotalAmountTHB, ref TotalAmountUSD);

                        if (sendInsurance == false)
                        {
                            InsurancePriceUSD = 0;
                            InsurancePriceTHB = 0;
                        }

                        modelData.crude_approve_detail.dDetail_PriceToSap.Add(new PriceToSapViewModel
                        {
                            TRANS_TYPE = "C",
                            COM_CODE = modelData.crude_approve_detail.dDetail_Header[0].PHE_COMPANY_CODE,
                            TRIP_NO = item.PMA_TRIP_NO,
                            CREATE_DATE = null,
                            CMCS_ETA = CMCS_ETA,
                            MAT_NUM = item.PMA_MET_NUM,
                            PARTLY_ORDER = "1",
                            VENDOR_NO = item.PMA_SUPPLIER,
                            PLANT = "1200",
                            GROUP_LOCATION = "REFINERY",
                            EXCHANGE_RATE = string.IsNullOrEmpty(queryMatPrice.i_FX_AGREEMENT) ? "0" : queryMatPrice.i_FX_AGREEMENT.Replace(",", ""),
                            BASE_PRICE = string.IsNullOrEmpty(queryMatPrice.i_BasePrice) ? "0" : queryMatPrice.i_BasePrice.Replace(",", ""),
                            PREMIUM_OSP = string.IsNullOrEmpty(queryMatPrice.i_OSP_PremiumDiscount) ? "0" : queryMatPrice.i_OSP_PremiumDiscount.Replace(",", ""),
                            PREMIUM_MARKET = string.IsNullOrEmpty(queryMatPrice.i_TradingPremiumDiscount) ? "0" : queryMatPrice.i_TradingPremiumDiscount.Replace(",", ""),
                            OTHER_COST = string.IsNullOrEmpty(queryMatPrice.i_OTHER_COST) ? "0" : queryMatPrice.i_OTHER_COST.Replace(",", ""),
                            FREIGHT_PRICE = FreightPriceUSD.ToString("###0.#####0"),
                            INSURANCE_PRICE = InsurancePriceUSD.ToString("###0.#####0"),
                            TOTAL_AMOUNT_THB = TotalAmountTHB.ToString("###0.#####0"),

                            TOTAL_AMOUNT_USD = (TotalAmountUSD + FreightPriceUSD + InsurancePriceUSD).ToString("###0.#####0"),
                            QTY = string.IsNullOrEmpty(item.PMA_PURCHASE_UNIT) ? "" : item.PMA_PURCHASE_UNIT.Equals("BBL") ? item.PMA_VOLUME_BBL : item.PMA_PURCHASE_UNIT.Equals("MT") ? item.PMA_VOLUME_MT : item.PMA_PURCHASE_UNIT.Equals("ML") ? item.PMA_VOLUME_ML : "0",
                            QTY_UNIT = qtyUnit,
                            CURRENCY = CrudeCurrency,
                            SAP_RECEIVED_DATE = null,
                            STATUS = null,
                            GROUP_DATA = item.PMA_ITEM_NO,
                            FLAG_PLANNING = "",
                            DUE_DATE = item.PMA_DUE_DATE,
                            CODE = null,
                            REMARK = null,
                            DATA_FROM = getPricingDataFrom(user_group),
                        });

                        item.PO_CURRENCY = CrudeCurrency;
                        if (CrudeCurrency.Equals("USD"))
                        {
                            item.FREIGHT_PRICE = FreightPriceUSD.ToString("###0.#####0");
                            item.INSURANCE_PRICE = InsurancePriceUSD.ToString("###0.#####0");
                            item.IMPORTDUTY_PRICE = ImportDutyAmountUSD.ToString("###0.#####0");
                        }
                        else
                        {
                            item.FREIGHT_PRICE = FreightPriceTHB.ToString("###0.#####0");
                            item.INSURANCE_PRICE = InsurancePriceTHB.ToString("###0.#####0");
                            item.IMPORTDUTY_PRICE = ImportDutyAmountTHB.ToString("###0.#####0");
                        }


                    }

                    #endregion



                    var json = new JavaScriptSerializer().Serialize(modelData);

                    if (dataAction == "insert")
                    {
                        RequestData req = new RequestData();
                        req.function_id = ConstantPrm.FUNCTION.F10000027;
                        req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                        req.state_name = "";

                        req.req_parameters = new req_parameters();
                        req.req_parameters.p = new List<p>();
                        req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                        req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
                        req.req_parameters.p.Add(new p { k = "trip_no", v = p_tripno });
                        req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                        req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                        req.req_parameters.p.Add(new p { k = CPAIConstantUtil.UserGroup, v = user_group });
                        req.extra_xml = "";

                        ResponseData resData = new ResponseData();
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                        resData = service.CallService(req);

                        string ret_tip_no = resData.resp_parameters[0].v;

                        hRow.PHE_SAVE_STS = resData.result_code;
                        hRow.PHE_SAVE_MSG = resData.response_message;


                        if (resData.result_code == "1")
                        {
                            if (model.crude_approve_detail.dDetail_MaterialFile != null)
                            {
                                var matFile = model.crude_approve_detail.dDetail_MaterialFile; //.Where(p => (p.PAF_STATUS ?? "") != "D");
                                if (matFile != null)
                                {
                                    model.crude_approve_detail.dDetail_MaterialFile = new List<FileViewModel>();
                                    foreach (var it in matFile)
                                    {
                                        if ((it.PAF_STATUS ?? "") == "D")
                                        {
                                            DeleteFile(rootPath, it.PAF_PATH, it.PAF_FILE_NAME);
                                        }
                                        else
                                        {
                                            it.PAF_INDB_STS = "1";
                                            //it.PAF_FILE_FOR = "MAT";
                                            it.PAF_STATUS = "";
                                            it.PAF_DESC_TEMP = it.PAF_DESCRIPTION ?? "";
                                            model.crude_approve_detail.dDetail_MaterialFile.Add(it);
                                        }

                                    }
                                }
                            }

                            if (model.crude_approve_detail.dDetail_FreightFile != null)
                            {
                                var File = model.crude_approve_detail.dDetail_FreightFile;
                                if (File != null)
                                {
                                    model.crude_approve_detail.dDetail_FreightFile = new List<FileViewModel>();
                                    foreach (var it in File)
                                    {
                                        if ((it.PAF_STATUS ?? "") == "D")
                                        {
                                            DeleteFile(rootPath, it.PAF_PATH, it.PAF_FILE_NAME);
                                        }
                                        else
                                        {
                                            it.PAF_INDB_STS = "1";
                                            //it.PAF_FILE_FOR = "MAT";
                                            it.PAF_STATUS = "";
                                            it.PAF_DESC_TEMP = it.PAF_DESCRIPTION ?? "";
                                            model.crude_approve_detail.dDetail_MaterialFile.Add(it);
                                        }
                                    }
                                }
                            }

                            if (model.crude_approve_detail.dDetail_SurveyorFile != null)
                            {
                                var File = model.crude_approve_detail.dDetail_SurveyorFile;//.Where(p => (p.PAF_STATUS ?? "") != "D");
                                if (File != null)
                                {
                                    model.crude_approve_detail.dDetail_SurveyorFile = new List<FileViewModel>();
                                    foreach (var it in File)
                                    {
                                        if ((it.PAF_STATUS ?? "") == "D")
                                        {
                                            DeleteFile(rootPath, it.PAF_PATH, it.PAF_FILE_NAME);
                                        }
                                        else
                                        {
                                            it.PAF_INDB_STS = "1";
                                            //it.PAF_FILE_FOR = "MAT";
                                            it.PAF_STATUS = "";
                                            it.PAF_DESC_TEMP = it.PAF_DESCRIPTION ?? "";
                                            model.crude_approve_detail.dDetail_MaterialFile.Add(it);
                                        }
                                    }
                                }
                            }

                            if (model.crude_approve_detail.dDetail_OtherFile != null)
                            {
                                var File = model.crude_approve_detail.dDetail_OtherFile;//.Where(p => (p.PAF_STATUS ?? "") != "D");
                                if (File != null)
                                {

                                    model.crude_approve_detail.dDetail_OtherFile = new List<FileViewModel>();
                                    foreach (var it in File)
                                    {
                                        if ((it.PAF_STATUS ?? "") == "D")
                                        {
                                            DeleteFile(rootPath, it.PAF_PATH, it.PAF_FILE_NAME);
                                        }
                                        else
                                        {
                                            it.PAF_INDB_STS = "1";
                                            //it.PAF_FILE_FOR = "MAT";
                                            it.PAF_STATUS = "";
                                            it.PAF_DESC_TEMP = it.PAF_DESCRIPTION ?? "";
                                            model.crude_approve_detail.dDetail_MaterialFile.Add(it);
                                        }
                                    }
                                }
                            }

                            if (model.crude_approve_detail.dDetail_CustomsFile != null)
                            {
                                var File = model.crude_approve_detail.dDetail_CustomsFile;//.Where(p => (p.PAF_STATUS ?? "") != "D");
                                if (File != null)
                                {
                                    model.crude_approve_detail.dDetail_CustomsFile = new List<FileViewModel>();
                                    foreach (var it in File)
                                    {
                                        if ((it.PAF_STATUS ?? "") == "D")
                                        {
                                            DeleteFile(rootPath, it.PAF_PATH, it.PAF_FILE_NAME);
                                        }
                                        else
                                        {
                                            it.PAF_INDB_STS = "1";
                                            //it.PAF_FILE_FOR = "MAT";
                                            it.PAF_STATUS = "";
                                            it.PAF_DESC_TEMP = it.PAF_DESCRIPTION ?? "";
                                            model.crude_approve_detail.dDetail_MaterialFile.Add(it);
                                        }
                                    }
                                }
                            }


                            if ((string.IsNullOrEmpty(hRow.PHE_ACTION) ? "" : hRow.PHE_ACTION).Equals("DELETE"))
                            {
                                iSuccess++;
                                hRow.PHE_ACTION = "DELETED";
                                //model.crude_approve_detail.dDetail_Header.Remove(hRow);
                                continue;
                            }

                            hRow.PHE_TRIP_NO = ret_tip_no;
                            hRow.PHE_INDB_STATUS = "Y";
                            hRow.PHE_ACTION = "";

                            if (model.crude_approve_detail.dDetail_Material != null)
                            {
                                var items = model.crude_approve_detail.dDetail_Material.Where(q => q.PMA_TRIP_NO.Equals(tripno)).ToList();
                                foreach (var item in items)
                                {
                                    var ItmTemp = modelData.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && p.PMA_ITEM_NO.Equals(item.PMA_ITEM_NO ?? "")).FirstOrDefault();
                                    if (ItmTemp != null)
                                    {
                                        item.PO_CURRENCY = ItmTemp.PO_CURRENCY;
                                        item.FREIGHT_PRICE = ItmTemp.FREIGHT_PRICE;
                                        item.INSURANCE_PRICE = ItmTemp.INSURANCE_PRICE;
                                        item.IMPORTDUTY_PRICE = ItmTemp.IMPORTDUTY_PRICE;
                                    }
                                    item.PMA_TRIP_NO = ret_tip_no;
                                }
                            }

                            if (model.crude_approve_detail.dDetail != null)
                            {
                                var items = model.crude_approve_detail.dDetail.Where(q => q.h_TripNo.Equals(tripno)).ToList();
                                foreach (var item in items)
                                {
                                    item.TripNo = ret_tip_no;
                                    item.h_TripNo = ret_tip_no;
                                }
                            }

                            if (model.crude_approve_detail.dDetail_Freight != null)
                            {
                                var items = model.crude_approve_detail.dDetail_Freight.Where(q => q.FR_TripNo.Equals(tripno)).ToList();
                                foreach (var item in items)
                                {
                                    item.FR_TripNo = ret_tip_no;
                                }
                            }

                            if (model.crude_approve_detail.dDetail_Surveyor != null)
                            {
                                var items = model.crude_approve_detail.dDetail_Surveyor.Where(q => q.Sur_TripNo.Equals(tripno)).ToList();
                                foreach (var item in items)
                                {
                                    item.Sur_TripNo = ret_tip_no;
                                }
                            }

                            if (model.crude_approve_detail.dDetail_Other != null)
                            {
                                var items = model.crude_approve_detail.dDetail_Other.Where(q => q.OC_TripNo.Equals(tripno)).ToList();
                                foreach (var item in items)
                                {
                                    item.OC_TripNo = ret_tip_no;
                                }
                            }

                            if (model.crude_approve_Search != null && model.crude_approve_Search.sSearchData != null)
                            {
                                var items = model.crude_approve_Search.sSearchData.Where(q => q.refTripNo != null && q.refTripNo.Equals(tripno)).ToList();
                                foreach (var item in items)
                                {
                                    item.refTripNo = ret_tip_no;
                                }
                            }

                            iSuccess++;

                        }
                        else
                        {
                            iError++;
                            ret = false;
                        }
                    }


                }

                msg = "Save Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();


            }
            return ret;
        }

        public void setIsCheckHeader(ref CrudeImportPlanViewModel model)
        {
            if (model.crude_approve_detail == null)
                return;

            if (model.crude_approve_detail.dDetail == null)
                return;

            if (model.crude_approve_detail.dDetail_Header == null)
                return;

            if (model.crude_approve_detail.dDetail == null)
                return;

            foreach (var hItem in model.crude_approve_detail.dDetail_Header)
            {
                hItem.PHE_IS_CHECK = false;
            }

            var query = model.crude_approve_detail.dDetail.Where(p => p.i_IS_CHECK.Equals(true)).ToList();
            foreach (var iItem in query)
            {
                var queryHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(iItem.h_TripNo)).First();
                if (queryHead == null)
                    continue;

                queryHead.PHE_SAVE_STS = "";
                queryHead.PHE_SAVE_MSG = "";
                queryHead.PHE_IS_CHECK = true;
            }
        }

        public void clearTempPriceUpdate(ref CrudeImportPlanViewModel model)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail == null) return;

            foreach (var i in model.crude_approve_detail.dDetail)
            {
                i.PriceUpdateDate = "";
            }

        }

        public void initialModel(ref CrudeImportPlanViewModel model)
        {
            List<SelectListItem> dataList;

            if (!string.IsNullOrEmpty(model.json_LoadingPort))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_LoadingPort, typeof(List<SelectListItem>));
                model.ddl_LoadingPort = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_Vehicle))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_Vehicle, typeof(List<SelectListItem>));
                model.ddl_Vehicle = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_year))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_year, typeof(List<SelectListItem>));
                model.ddl_Year = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_month))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_month, typeof(List<SelectListItem>));
                model.ddl_Month = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_BLStatus))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_BLStatus, typeof(List<SelectListItem>));
                model.ddl_BLStatus = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_PriceStatus))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_PriceStatus, typeof(List<SelectListItem>));
                model.ddl_PriceStatus = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_PurchaseType))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_PurchaseType, typeof(List<SelectListItem>));
                model.ddl_PurchaseType = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_StorageLocation))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_StorageLocation, typeof(List<SelectListItem>));
                model.ddl_StorageLocation = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_Currency))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_Currency, typeof(List<SelectListItem>));
                model.ddl_Currency = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_SurveyorType))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_SurveyorType, typeof(List<SelectListItem>));
                model.ddl_SurveyorType = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_OtherCostType))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_OtherCostType, typeof(List<SelectListItem>));
                model.ddl_OtherCostType = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_Channel))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_Channel, typeof(List<SelectListItem>));
                model.ddl_Channel = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_UnitCal))
            {
                List<PCF_UNIT_CAL> mModel = JSonConvertUtil.jsonToModel<List<PCF_UNIT_CAL>>(model.json_UnitCal);
                model.ddl_UnitCal = CrudeImportPlanServiceModel.getUnitCal(mModel);
            }

            if (!string.IsNullOrEmpty(model.json_Supplier))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_Supplier, typeof(List<SelectListItem>));
                model.ddl_Supplier = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_Crude))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_Crude, typeof(List<SelectListItem>));
                model.ddl_CrudeName = dataList;
            }

            if (!string.IsNullOrEmpty(model.json_purgroup))
            {
                model.PurGroup = (List<PCF_PO_SURVEYOR_PUR_GROUP>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_purgroup, typeof(List<PCF_PO_SURVEYOR_PUR_GROUP>));
            }

            if (!string.IsNullOrEmpty(model.json_HolidayType))
            {
                dataList = (List<SelectListItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.json_HolidayType, typeof(List<SelectListItem>));
                model.ddl_HolidayType = dataList;
            }

        }

        public void SendToBookingService(ref CrudeImportPlanViewModel model)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null)
            {
                return;
            }

            var HeadList = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();

            model.MsgAlert = "";

            int iSuccess = 0;
            int iError = 0;

            CrudeImportPlanViewModel modelData = null;
            string tripno = "";
            foreach (var itemHead in HeadList)
            {
                tripno = itemHead.PHE_TRIP_NO;

                modelData = new CrudeImportPlanViewModel();
                modelData.crude_approve_detail = new CrudeApproveFormViewModel_Detail();

                modelData.crude_approve_detail.dDetail_Header = model.crude_approve_detail.dDetail_Header.Where(q => q.PHE_TRIP_NO.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Material != null)
                    modelData.crude_approve_detail.dDetail_Material = model.crude_approve_detail.dDetail_Material.Where(q => q.PMA_TRIP_NO.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail != null)
                    modelData.crude_approve_detail.dDetail = model.crude_approve_detail.dDetail.Where(q => q.h_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Freight != null)
                    modelData.crude_approve_detail.dDetail_Freight = model.crude_approve_detail.dDetail_Freight.Where(q => q.FR_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Surveyor != null)
                    modelData.crude_approve_detail.dDetail_Surveyor = model.crude_approve_detail.dDetail_Surveyor.Where(q => q.Sur_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Other != null)
                    modelData.crude_approve_detail.dDetail_Other = model.crude_approve_detail.dDetail_Other.Where(q => q.OC_TripNo.Equals(tripno)).ToList();

                var json = new JavaScriptSerializer().Serialize(modelData);

                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000028;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";

                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();
                req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
                req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
                req.req_parameters.p.Add(new p { k = "trip_no", v = tripno });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "status", v = "sendbook" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ResponseData resData = new ResponseData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                //string ret_tip_no = resData.resp_parameters[0].v;

                if (resData.result_code == "1")
                {
                    iSuccess++;
                    itemHead.PHE_PLANNING_RELEASE = "Y";
                    itemHead.PHE_EXPORT_STS = "Exported";
                }
                else
                {
                    iError++;
                }


            }

            model.MsgAlert = "Send to Booking Resutl<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();

        }

        public void SendToAccountService(ref CrudeImportPlanViewModel model)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null)
            {
                return;
            }

            var HeadList = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();

            model.MsgAlert = "";

            int iSuccess = 0;
            int iError = 0;

            CrudeImportPlanViewModel modelData = null;
            string tripno = "";
            foreach (var itemHead in HeadList)
            {
                tripno = itemHead.PHE_TRIP_NO;

                modelData = new CrudeImportPlanViewModel();
                modelData.crude_approve_detail = new CrudeApproveFormViewModel_Detail();

                modelData.crude_approve_detail.dDetail_Header = model.crude_approve_detail.dDetail_Header.Where(q => q.PHE_TRIP_NO.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Material != null)
                    modelData.crude_approve_detail.dDetail_Material = model.crude_approve_detail.dDetail_Material.Where(q => q.PMA_TRIP_NO.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail != null)
                    modelData.crude_approve_detail.dDetail = model.crude_approve_detail.dDetail.Where(q => q.h_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Freight != null)
                    modelData.crude_approve_detail.dDetail_Freight = model.crude_approve_detail.dDetail_Freight.Where(q => q.FR_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Surveyor != null)
                    modelData.crude_approve_detail.dDetail_Surveyor = model.crude_approve_detail.dDetail_Surveyor.Where(q => q.Sur_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Other != null)
                    modelData.crude_approve_detail.dDetail_Other = model.crude_approve_detail.dDetail_Other.Where(q => q.OC_TripNo.Equals(tripno)).ToList();

                var json = new JavaScriptSerializer().Serialize(modelData);

                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000032;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";

                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();
                req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
                req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
                req.req_parameters.p.Add(new p { k = "trip_no", v = tripno });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "status", v = "sendaccount" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ResponseData resData = new ResponseData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                //string ret_tip_no = resData.resp_parameters[0].v;

                if (resData.result_code == "1")
                {
                    iSuccess++;
                    itemHead.PHE_BOOKING_RELEASE = "Y";
                    itemHead.PHE_EXPORT_STS = "Exported";
                }
                else
                {
                    iError++;
                }


            }

            model.MsgAlert = "Send to Accounting Resutl<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();


        }

        private Boolean CallSapChangePO(PurchasingChangeModel modelChange, string pono, ref string msg)
        {
            Boolean ret = false;

            var json = new JavaScriptSerializer().Serialize(modelChange);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000030;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
            req.req_parameters.p.Add(new p { k = "system", v = "PurchaseOrder" });
            req.req_parameters.p.Add(new p { k = "po_no", v = pono });
            req.req_parameters.p.Add(new p { k = "status", v = "Change" });
            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            if (resData == null)
                return false;

            //resData.result_desc

            ret = ((resData.result_status.ToLower() == "fail") || (resData.result_status == "-")) ? false : true;

            msg = resData.result_desc;

            //ViewBag.ChangePOCompleted = true;
            //ViewBag.ChangePOResult = ((resData.result_status.ToLower() == "fail") || (resData.result_status == "-")) ? false : true;
            //ViewBag.ChangePOResutDesc = resData.result_desc;

            return ret;
        }

        private void getPOHeaderConfig(string comcode, string purchaseType, string incoterm, string UserRole, ref string StorageLocation, ref string PurGroup)
        {
            string ret = "";
            try
            {
                CIP_Config CIPConfig = CrudeImportPlanServiceModel.getCIPConfig("CIP_IMPORT_PLAN");
                if (CIPConfig == null)
                    return;

                if (CIPConfig.PCF_MT_STORAGE_LOCATION_DETAIL != null)
                {
                    var query = CIPConfig.PCF_MT_STORAGE_LOCATION_DETAIL.Where(p => p.COMPANY_CODE.Equals(comcode) && p.PURCHASE_TYPE.Equals(purchaseType) && p.INCOTERMS.Equals(incoterm)).ToList();
                    if (query != null && query.Count > 0)
                        StorageLocation = query[0].CODE;
                }

                if (CIPConfig.PCF_PO_SURVEYOR_PUR_GROUP != null)
                {
                    var query = CIPConfig.PCF_PO_SURVEYOR_PUR_GROUP.Where(p => p.ROLE.Equals(UserRole)).ToList();
                    if (query != null && query.Count > 0)
                        PurGroup = query[0].VALUE;

                }

            }
            catch (Exception ex)
            {

            }



        }

        private void getMaterialGroup(ref string mgFright, ref string mgInsur, ref string mgCustom)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var query = (from m in context.MT_MATERIALS_GROUP select m).ToList();

                    if (query == null)
                        return;

                    var qFreight = query.Where(p => p.MMT_GROUP_NAME.Equals("Freight")).First();
                    if (qFreight != null)
                        mgFright = qFreight.MMT_GROUP_CODE;

                    var qInsur = query.Where(p => p.MMT_GROUP_NAME.Equals("Insurance")).First();
                    if (qInsur != null)
                        mgInsur = qInsur.MMT_GROUP_CODE;

                    var qCustom = query.Where(p => p.MMT_GROUP_NAME.Equals("Customs Duty")).First();
                    if (qCustom != null)
                        mgCustom = qCustom.MMT_GROUP_CODE;


                };
            }
            catch (Exception ex) { }
        }

        private PurchasingChangeModel SetModelChangePO(HeaderViewModel itmHead, MaterialViewModel itmMat, CrudeImportPlanViewModel_Detail itmDetail
            , Boolean sendCancel, ref int PMA_PO_MAT_ITEM, ref int PMA_PO_FREIGHT_ITEM, ref int PMA_PO_INSURANCE_ITEM, ref int PMA_PO_CUSTOMS_ITEM, string CancelAll = "")
        {
            string sBL_Date = "";
            if (!string.IsNullOrEmpty(itmMat.PMA_BL_DATE))
                sBL_Date = DateTime.ParseExact(itmMat.PMA_BL_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

            string mgFreight = "";
            string mgInsur = "";
            string mgCustom = "";
            getMaterialGroup(ref mgFreight, ref mgInsur, ref mgCustom);


            PurchasingChangeModel model = new PurchasingChangeModel();

            string storageLocation = "";
            string PurGroup = "";
            getPOHeaderConfig(itmHead.PHE_COMPANY_CODE, itmMat.PMA_PURCHASE_TYPE, itmMat.PMA_INCOTERMS, Const.User.UserGroup, ref storageLocation, ref PurGroup);


            //Purchase Order No.
            model.PURCHASEORDER = itmMat.PMA_PO_NO;

            model.POHEADER = new BAPIMEPOHEADER();
            model.POHEADER.PO_NUMBER = itmMat.PMA_PO_NO;
            model.POHEADER.DOC_DATE = sBL_Date;
            model.POHEADER.INCOTERMS1 = itmMat.PMA_INCOTERMS; // FOB
            model.POHEADER.CURRENCY = itmDetail.i_CURRENCY;


            model.POHEADERX = new BAPIMEPOHEADERX();
            model.POHEADERX.PO_NUMBER = "X";
            model.POHEADERX.DOC_DATE = "X";
            model.POHEADERX.INCOTERMS1 = "X";
            model.POHEADERX.CURRENCY = "X";

            //ITEMS
            model.POITEM = new List<BAPIMEPOITEM>();
            model.POITEMX = new List<BAPIMEPOITEMX>();

            int maxItem = 0;
            PMA_PO_MAT_ITEM = (string.IsNullOrEmpty(itmMat.PMA_PO_MAT_ITEM) ? 0 : Convert.ToInt32(itmMat.PMA_PO_MAT_ITEM));
            if (PMA_PO_MAT_ITEM != 0 && PMA_PO_MAT_ITEM > maxItem)
            {
                maxItem = PMA_PO_MAT_ITEM;
            }

            PMA_PO_FREIGHT_ITEM = (string.IsNullOrEmpty(itmMat.PMA_PO_FREIGHT_ITEM) ? 0 : Convert.ToInt32(itmMat.PMA_PO_FREIGHT_ITEM));
            if (PMA_PO_FREIGHT_ITEM != 0 && PMA_PO_FREIGHT_ITEM > maxItem)
            {
                maxItem = PMA_PO_FREIGHT_ITEM;
            }


            PMA_PO_INSURANCE_ITEM = (string.IsNullOrEmpty(itmMat.PMA_PO_INSURANCE_ITEM) ? 0 : Convert.ToInt32(itmMat.PMA_PO_INSURANCE_ITEM));
            if (PMA_PO_INSURANCE_ITEM != 0 && PMA_PO_INSURANCE_ITEM > maxItem)
            {
                maxItem = PMA_PO_INSURANCE_ITEM;
            }


            PMA_PO_CUSTOMS_ITEM = (string.IsNullOrEmpty(itmMat.PMA_PO_CUSTOMS_ITEM) ? 0 : Convert.ToInt32(itmMat.PMA_PO_CUSTOMS_ITEM));
            if (PMA_PO_CUSTOMS_ITEM != 0 && PMA_PO_CUSTOMS_ITEM > maxItem)
            {
                maxItem = PMA_PO_CUSTOMS_ITEM;
            }

            string markCancel = "";
            Decimal qty = 0;
            if (itmDetail.i_PriceFor.Equals("BBL"))
            {
                qty = string.IsNullOrEmpty(itmMat.PMA_VOLUME_BBL) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_BBL);
            }
            else if (itmDetail.i_PriceFor.Equals("MT"))
            {
                qty = string.IsNullOrEmpty(itmMat.PMA_VOLUME_MT) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_MT);
            }
            else if (itmDetail.i_PriceFor.Equals("ML"))
            {
                qty = string.IsNullOrEmpty(itmMat.PMA_VOLUME_ML) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_ML);
            }

            string itemCheck = string.IsNullOrEmpty(itmMat.PMA_PO_MAT_ITEM) ? "0" : itmMat.PMA_PO_MAT_ITEM;

            Boolean sendSTS = false;

            #region Crude
            if (itemCheck.Equals("0"))
            {
                if (qty != 0)
                {
                    maxItem = maxItem + 10;
                    PMA_PO_MAT_ITEM = maxItem;
                    sendSTS = true;
                }

            }
            else
            {
                sendSTS = true;

                if (qty == 0)
                    markCancel = "X";

            }

            if (sendSTS)
            {
                if (CancelAll.Equals("X"))
                    markCancel = "X";

                BAPIMEPOITEM poItem = new BAPIMEPOITEM()
                {
                    PO_ITEM = PMA_PO_MAT_ITEM.ToString().PadLeft(6, '0'),
                    //SHORT_TEXT = String.IsNullOrEmpty(item.ShortText) ? "" : item.ShortText,
                    PLANT = "1200",
                    PO_UNIT = itmDetail.i_PriceFor,
                    NET_PRICE = string.IsNullOrEmpty(itmDetail.ProActual) ? 0 : decimal.Round(Convert.ToDecimal(itmDetail.ProActual), 2, MidpointRounding.AwayFromZero),
                    NET_PRICESpecified = true,
                    QUANTITY = qty,
                    QUANTITYSpecified = true,
                    STGE_LOC = storageLocation,
                    TRACKINGNO = itmMat.PMA_TRIP_NO,
                    NO_MORE_GR = "",
                };

                poItem.DELETE_IND = markCancel;
                model.POITEM.Add(poItem);

                //ITEMX
                BAPIMEPOITEMX poItemx = new BAPIMEPOITEMX()
                {
                    PO_ITEM = PMA_PO_MAT_ITEM.ToString().PadLeft(6, '0'),
                    PO_ITEMX = "X",
                    //SHORT_TEXT = "X",
                    PLANT = "X",
                    STGE_LOC = "X",
                    //MATL_GROUP = "X",
                    QUANTITY = "X",
                    PO_UNIT = "X",
                    NET_PRICE = "X",
                    NO_MORE_GR = "X",
                    TRACKINGNO = "X",
                };

                poItemx.DELETE_IND = "X";
                model.POITEMX.Add(poItemx);
                if (markCancel.Equals("X"))
                {
                    PMA_PO_MAT_ITEM = 0;
                }
            }

            #endregion

            #region Freight
            itemCheck = string.IsNullOrEmpty(itmMat.PMA_PO_FREIGHT_ITEM) ? "0" : itmMat.PMA_PO_FREIGHT_ITEM;
            sendSTS = false;
            Decimal freightPrice = string.IsNullOrEmpty(itmMat.FREIGHT_PRICE) ? 0 : Convert.ToDecimal(itmMat.FREIGHT_PRICE);
            if (itemCheck.Equals("0"))
            {
                if (freightPrice != 0)
                {
                    maxItem = maxItem + 10;
                    PMA_PO_FREIGHT_ITEM = maxItem;
                    sendSTS = true;
                }
            }
            else
            {
                sendSTS = true;
                if (freightPrice == 0)
                    markCancel = "X";
            }

            if (sendSTS)
            {
                if (CancelAll.Equals("X"))
                    markCancel = "X";

                string shortText = "FRI"
                                   + (string.IsNullOrEmpty(itmDetail.h_Supplier) ? "" : "/" + (itmDetail.h_Supplier.Length > 10 ? itmDetail.h_Supplier.Substring(0, 10) : itmDetail.h_Supplier))
                                   + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT);

                if (shortText.Length > 40)
                    shortText = shortText.Substring(0, 40);


                Decimal dFreight = qty * freightPrice;

                BAPIMEPOITEM poItem = new BAPIMEPOITEM()
                {
                    PO_ITEM = PMA_PO_FREIGHT_ITEM.ToString().PadLeft(6, '0'),
                    SHORT_TEXT = shortText,
                    PLANT = "1200",
                    NET_PRICE = string.IsNullOrEmpty(itmMat.FREIGHT_PRICE) ? 0 : decimal.Round(dFreight, 2, MidpointRounding.AwayFromZero),
                    NET_PRICESpecified = true,
                    QUANTITY = 1,
                    QUANTITYSpecified = true,
                    PO_UNIT = "LE",
                    TRACKINGNO = itmMat.PMA_TRIP_NO,
                    MATL_GROUP = mgFreight,
                    ACCTASSCAT = "S",
                    NO_MORE_GR = "",
                };

                poItem.DELETE_IND = markCancel;
                model.POITEM.Add(poItem);

                //ITEMX
                BAPIMEPOITEMX poItemx = new BAPIMEPOITEMX()
                {
                    PO_ITEM = PMA_PO_FREIGHT_ITEM.ToString().PadLeft(6, '0'),
                    PO_ITEMX = "X",
                    SHORT_TEXT = "X",
                    PLANT = "X",
                    //STGE_LOC = "X",
                    QUANTITY = "X",
                    //PO_UNIT = "X",
                    NET_PRICE = "X",
                    TRACKINGNO = "X",
                    MATL_GROUP = "X",
                    PO_UNIT = "X",
                    ACCTASSCAT = "X",
                    NO_MORE_GR = "X",
                };

                poItemx.DELETE_IND = "X";
                model.POITEMX.Add(poItemx);
                if (markCancel.Equals("X"))
                {
                    PMA_PO_FREIGHT_ITEM = 0;
                }

            }

            #endregion

            #region Insurance
            itemCheck = string.IsNullOrEmpty(itmMat.PMA_PO_INSURANCE_ITEM) ? "0" : itmMat.PMA_PO_INSURANCE_ITEM;
            sendSTS = false;
            Decimal InsurPrice = string.IsNullOrEmpty(itmMat.INSURANCE_PRICE) ? 0 : Convert.ToDecimal(itmMat.INSURANCE_PRICE);
            if (itemCheck.Equals("0"))
            {
                if (InsurPrice != 0)
                {
                    maxItem = maxItem + 10;
                    PMA_PO_INSURANCE_ITEM = maxItem;
                    sendSTS = true;
                }
            }
            else
            {
                sendSTS = true;
                if (InsurPrice == 0)
                    markCancel = "X";
            }

            if (sendSTS)
            {
                if (CancelAll.Equals("X"))
                    markCancel = "X";

                Decimal dInsur = qty * InsurPrice;

                string shortText = "INS"
                                 + (string.IsNullOrEmpty(itmDetail.h_Supplier) ? "" : "/" + (itmDetail.h_Supplier.Length > 10 ? itmDetail.h_Supplier.Substring(0, 10) : itmDetail.h_Supplier))
                                 + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT);

                if (shortText.Length > 40)
                    shortText = shortText.Substring(0, 40);

                BAPIMEPOITEM poItem = new BAPIMEPOITEM()
                {
                    PO_ITEM = PMA_PO_INSURANCE_ITEM.ToString().PadLeft(6, '0'),
                    SHORT_TEXT = shortText,
                    PLANT = "1200",
                    //PO_UNIT = itmDetail.i_PriceFor,
                    NET_PRICE = string.IsNullOrEmpty(itmMat.INSURANCE_PRICE) ? 0 : decimal.Round(dInsur, 2, MidpointRounding.AwayFromZero),
                    NET_PRICESpecified = true,
                    QUANTITY = 1,
                    QUANTITYSpecified = true,
                    TRACKINGNO = itmMat.PMA_TRIP_NO,
                    MATL_GROUP = mgInsur,
                    ACCTASSCAT = "S",
                    PO_UNIT = "LE",
                    NO_MORE_GR = "",
                };

                poItem.DELETE_IND = markCancel;
                model.POITEM.Add(poItem);

                //ITEMX
                BAPIMEPOITEMX poItemx = new BAPIMEPOITEMX()
                {
                    PO_ITEM = PMA_PO_INSURANCE_ITEM.ToString().PadLeft(6, '0'),
                    PO_ITEMX = "X",
                    SHORT_TEXT = "X",
                    PLANT = "X",
                    STGE_LOC = "X",
                    MATL_GROUP = "X",
                    QUANTITY = "X",
                    PO_UNIT = "X",
                    NET_PRICE = "X",
                    //NO_MORE_GR = "X"
                    TRACKINGNO = "X",
                    ACCTASSCAT = "X",
                    NO_MORE_GR = "X",
                };

                poItemx.DELETE_IND = "X";
                model.POITEMX.Add(poItemx);
                if (markCancel.Equals("X"))
                {
                    PMA_PO_INSURANCE_ITEM = 0;
                }
            }

            #endregion

            #region Insurance
            itemCheck = string.IsNullOrEmpty(itmMat.PMA_PO_CUSTOMS_ITEM) ? "0" : itmMat.PMA_PO_CUSTOMS_ITEM;
            sendSTS = false;
            Decimal ImportDutyPrice = string.IsNullOrEmpty(itmMat.IMPORTDUTY_PRICE) ? 0 : Convert.ToDecimal(itmMat.IMPORTDUTY_PRICE);
            if (itemCheck.Equals("0"))
            {
                if (ImportDutyPrice != 0)
                {
                    maxItem = maxItem + 10;
                    PMA_PO_CUSTOMS_ITEM = maxItem;
                    sendSTS = true;
                }
            }
            else
            {
                sendSTS = true;
                if (ImportDutyPrice == 0)
                    markCancel = "X";
            }

            if (sendSTS)
            {

                if (CancelAll.Equals("X"))
                    markCancel = "X";

                string shortText = "CUS"
                                 + (string.IsNullOrEmpty(itmDetail.h_Supplier) ? "" : "/" + (itmDetail.h_Supplier.Length > 10 ? itmDetail.h_Supplier.Substring(0, 10) : itmDetail.h_Supplier))
                                 + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT);

                if (shortText.Length > 40)
                    shortText = shortText.Substring(0, 40);

                Decimal dImportDuty = qty * ImportDutyPrice;

                BAPIMEPOITEM poItem = new BAPIMEPOITEM()
                {
                    PO_ITEM = PMA_PO_CUSTOMS_ITEM.ToString().PadLeft(6, '0'),
                    //SHORT_TEXT = String.IsNullOrEmpty(item.ShortText) ? "" : item.ShortText,
                    PLANT = "1200",
                    //PO_UNIT = itmDetail.i_PriceFor,
                    NET_PRICE = string.IsNullOrEmpty(itmMat.IMPORTDUTY_PRICE) ? 0 : decimal.Round(dImportDuty, 2, MidpointRounding.AwayFromZero),
                    NET_PRICESpecified = true,
                    QUANTITY = 1,
                    QUANTITYSpecified = true,
                    TRACKINGNO = itmMat.PMA_TRIP_NO,
                    MATL_GROUP = mgCustom,
                    PO_UNIT = "LE",
                    NO_MORE_GR = "",
                };

                poItem.DELETE_IND = markCancel;
                model.POITEM.Add(poItem);

                //ITEMX
                BAPIMEPOITEMX poItemx = new BAPIMEPOITEMX()
                {
                    PO_ITEM = PMA_PO_CUSTOMS_ITEM.ToString().PadLeft(6, '0'),
                    PO_ITEMX = "X",
                    SHORT_TEXT = "X",
                    PLANT = "X",
                    //STGE_LOC = "X",
                    MATL_GROUP = "X",
                    QUANTITY = "X",
                    PO_UNIT = "X",
                    NET_PRICE = "X",
                    //NO_MORE_GR = "X"
                    TRACKINGNO = "X",
                    NO_MORE_GR = "X",
                };

                poItemx.DELETE_IND = "X";
                model.POITEMX.Add(poItemx);
                if (markCancel.Equals("X"))
                {
                    PMA_PO_CUSTOMS_ITEM = 0;
                }

            }

            #endregion


            model.ALLVERSIONS = new List<BAPIMEDCM_ALLVERSIONS>();
            model.EXTENSIONIN = new List<BAPIPAREX>();
            model.EXTENSIONOUT = new List<BAPIPAREX>();
            model.INVPLANHEADER = new List<BAPI_INVOICE_PLAN_HEADER>();
            model.INVPLANHEADERX = new List<BAPI_INVOICE_PLAN_HEADERX>();
            model.INVPLANITEM = new List<BAPI_INVOICE_PLAN_ITEM>();
            model.INVPLANITEMX = new List<BAPI_INVOICE_PLAN_ITEMX>();
            model.POACCOUNT = new List<BAPIMEPOACCOUNT>();
            model.POACCOUNTPROFITSEGMENT = new List<BAPIMEPOACCOUNTPROFITSEGMENT>();
            model.POACCOUNTX = new List<BAPIMEPOACCOUNTX>();
            model.POADDRDELIVERY = new List<BAPIMEPOADDRDELIVERY>();
            model.POCOMPONENTS = new List<BAPIMEPOCOMPONENT>();
            model.POCOMPONENTSX = new List<BAPIMEPOCOMPONENTX>();
            model.POCOND = new List<BAPIMEPOCOND>();
            model.POCONDHEADER = new List<BAPIMEPOCONDHEADER>();
            model.POCONDHEADERX = new List<BAPIMEPOCONDHEADERX>();
            model.POCONDX = new List<BAPIMEPOCONDX>();
            model.POCONFIRMATION = new List<BAPIEKES>();
            model.POCONTRACTLIMITS = new List<BAPIESUCC>();
            model.POEXPIMPITEM = new List<BAPIEIPO>();
            model.POEXPIMPITEMX = new List<BAPIEIPOX>();
            model.POHISTORY = new List<BAPIEKBE>();
            model.POHISTORY_MA = new List<BAPIEKBE_MA>();
            model.POHISTORY_TOTALS = new List<BAPIEKBES>();
            model.POLIMITS = new List<BAPIESUHC>();
            model.POPARTNER = new List<BAPIEKKOP>();
            model.POSCHEDULE = new List<BAPIMEPOSCHEDULE>();
            model.POSCHEDULEX = new List<BAPIMEPOSCHEDULX>();
            model.POSERVICES = new List<BAPIESLLC>();
            model.POSERVICESTEXT = new List<BAPIESLLTX>();
            model.POSHIPPING = new List<BAPIITEMSHIP>();
            model.POSHIPPINGEXP = new List<BAPIMEPOSHIPPEXP>();
            model.POSHIPPINGX = new List<BAPIITEMSHIPX>();
            model.POSRVACCESSVALUES = new List<BAPIESKLC>();
            model.POTEXTHEADER = new List<BAPIMEPOTEXTHEADER>();
            model.POTEXTITEM = new List<BAPIMEPOTEXT>();
            model.SERIALNUMBER = new List<BAPIMEPOSERIALNO>();
            model.SERIALNUMBERX = new List<BAPIMEPOSERIALNOX>();
            model.EXPPOEXPIMPHEADER = new BAPIEIKP();

            // Set value at RETURN
            model.RETURN = new List<com.pttict.sap.Interface.sap.po.change.BAPIRET2>();
            model.RETURN.Add(new com.pttict.sap.Interface.sap.po.change.BAPIRET2()
            {
                FIELD = ""
            });

            return model;
        }

        private void SendChangePO(ref CrudeImportPlanViewModel model, string fixTripNo, string fixItemNo, string CancelAll = "")
        {
            var qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixItemNo != "")
                qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

            if (qHead == null)
                return;

            foreach (var itmHead in qHead)
            {

                var tripno = itmHead.PHE_TRIP_NO;

                var qMat = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && (string.IsNullOrEmpty(p.PMA_PO_NO) == false) && !p.PMA_PO_NO.Equals("0")).ToList();
                if (fixItemNo != "")
                    qMat = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && p.PMA_ITEM_NO.Equals(fixItemNo) && (string.IsNullOrEmpty(p.PMA_PO_NO) == false) && !p.PMA_PO_NO.Equals("0")).ToList();

                foreach (var itmMat in qMat)
                {
                    if (CancelAll.Equals("X"))
                    {
                        var gitno = string.IsNullOrEmpty(itmMat.PMA_GIT_NO) ? "0" : itmMat.PMA_GIT_NO;
                        if (!gitno.Equals("0"))
                            continue;
                    }

                    var qMatPrice = model.crude_approve_detail.dDetail.Where(p => p.h_TripNo.Equals(tripno) && p.MatItemNo.Equals(itmMat.PMA_ITEM_NO)).OrderByDescending(o => o.MatPriceItemNo).First();
                    if (qMatPrice == null)
                        continue;

                    int PMA_PO_MAT_ITEM = 0;
                    int PMA_PO_FREIGHT_ITEM = 0;
                    int PMA_PO_INSURANCE_ITEM = 0;
                    int PMA_PO_CUSTOMS_ITEM = 0;
                    PurchasingChangeModel modelChange = SetModelChangePO(itmHead, itmMat, qMatPrice, false, ref PMA_PO_MAT_ITEM, ref PMA_PO_FREIGHT_ITEM, ref PMA_PO_INSURANCE_ITEM, ref PMA_PO_CUSTOMS_ITEM, CancelAll);
                    string msg = "";
                    Boolean result = CallSapChangePO(modelChange, itmMat.PMA_PO_NO, ref msg);
                    if (result)
                    {
                        string pono = itmMat.PMA_PO_NO;
                        result = UpdateResultChangePO(tripno, itmMat.PMA_ITEM_NO, PMA_PO_MAT_ITEM.ToString(), PMA_PO_FREIGHT_ITEM.ToString(), PMA_PO_INSURANCE_ITEM.ToString(), PMA_PO_CUSTOMS_ITEM.ToString(), ref pono);
                        if (result)
                        {
                            itmMat.PMA_PO_MAT_ITEM = PMA_PO_MAT_ITEM.ToString();
                            itmMat.PMA_PO_FREIGHT_ITEM = PMA_PO_FREIGHT_ITEM.ToString();
                            itmMat.PMA_PO_INSURANCE_ITEM = PMA_PO_INSURANCE_ITEM.ToString();
                            itmMat.PMA_PO_CUSTOMS_ITEM = PMA_PO_CUSTOMS_ITEM.ToString();
                            itmMat.PMA_PO_NO = pono;
                            qMatPrice.PONoCrude = pono;
                        }
                    }
                    else
                    {
                        itmHead.PHE_SAVE_MSG = msg;
                        itmHead.PHE_SAVE_STS = "99";
                    }

                }
            }


        }

        private Boolean UpdateResultChangePO(string tripno, string matItemno, string ItemNoCrude, string ItemNoFreight, string ItemNoInsur, string ItemNoCustoms, ref string pono)
        {
            Boolean ret = false;
            try
            {
                string sItemNoCrude = (string.IsNullOrEmpty(ItemNoCrude) ? "0" : ItemNoCrude);
                string sItemNoFreight = (string.IsNullOrEmpty(ItemNoFreight) ? "0" : ItemNoFreight); ;
                string sItemNoInsur = (string.IsNullOrEmpty(ItemNoInsur) ? "0" : ItemNoInsur);
                string sItemNoCustoms = (string.IsNullOrEmpty(ItemNoCustoms) ? "0" : ItemNoCustoms);

                if (sItemNoCrude.Equals("0") && sItemNoFreight.Equals("0") && sItemNoInsur.Equals("0") && sItemNoCustoms.Equals("0"))
                    pono = "";

                CRUDE_IMPORT_PLAN_DAL service = new CRUDE_IMPORT_PLAN_DAL();
                using (var context = new EntityCPAIEngine())
                {
                    service.UpdateChangePOResult(tripno, matItemno, sItemNoCrude, sItemNoFreight, sItemNoInsur, sItemNoCustoms, context, pono);
                }

                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public void SendCancelPOAndGIT(ref CrudeImportPlanViewModel model, string fixTripNo, string fixItemNo)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null)
            {
                return;
            }


            sendCancelGIT(ref model, fixTripNo, fixItemNo);

            SendChangePO(ref model, fixTripNo, fixItemNo, "X");

            model.MsgAlert = "";
            int iSuccess = 0;
            int iError = 0;


            var qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixItemNo != "")
                qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

            if (qHead != null)
            {
                iSuccess = qHead.Count;

                foreach (var itmHead in qHead)
                {
                    string sts = string.IsNullOrEmpty(itmHead.PHE_SAVE_STS) ? "" : itmHead.PHE_SAVE_STS;

                    if (!sts.Equals("1") && !sts.Equals("S"))
                    {
                        iError++;
                    }
                }

                iSuccess = iSuccess - iError;
            }

            model.MsgAlert = "Send PO Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();

        }

        private void sendCancelGIT(ref CrudeImportPlanViewModel model, string fixTripNo, string fixItemNo)
        {

            var json = new JavaScriptSerializer().Serialize(model.crude_approve_detail);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000060;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";

            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
            req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
            req.req_parameters.p.Add(new p { k = "trip_no", v = fixTripNo });
            req.req_parameters.p.Add(new p { k = "item_no", v = fixItemNo });
            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = CPAIConstantUtil.Action, v = "" });

            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            if (!string.IsNullOrEmpty(resData.extra_xml))
            {
                model.crude_approve_detail = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(resData.extra_xml);

                var qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
                if (fixItemNo != "")
                    qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

                if (qHead != null)
                {

                    foreach (var itmHead in qHead)
                    {
                        string sts = "S";
                        string msg = "";

                        var queryMat = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(itmHead.PHE_TRIP_NO)).ToList();
                        if (fixItemNo != "")
                            queryMat = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(itmHead.PHE_TRIP_NO) && p.PMA_ITEM_NO.Equals(fixItemNo)).ToList();

                        if (queryMat != null)
                        {
                            foreach (var itmMat in queryMat)
                            {
                                if (!string.IsNullOrEmpty(itmMat.RET_STS) && itmMat.RET_STS.Equals("E"))
                                {
                                    sts = "E";
                                    msg += itmMat.RET_MSG;
                                }

                                var subItem = model.crude_approve_detail.dDetail.Where(p => p.h_TripNo.Equals(itmMat.PMA_TRIP_NO) && p.MatItemNo.Equals(itmMat.PMA_ITEM_NO)).ToList();
                                foreach (var s in subItem)
                                {
                                    s.h_GitNo = string.IsNullOrEmpty(itmMat.PMA_GIT_NO) || itmMat.PMA_GIT_NO.Equals("0") ? "" : itmMat.PMA_GIT_NO;
                                }

                            }
                        }

                        string oldsts = string.IsNullOrEmpty(itmHead.PHE_SAVE_STS) ? "" : itmHead.PHE_SAVE_STS;

                        if (!oldsts.Equals("E"))
                        {
                            itmHead.PHE_SAVE_STS = sts;
                        }

                        if (sts.Equals("E"))
                        {
                            itmHead.PHE_SAVE_MSG = msg;
                        }


                    }
                }

            }
        }

        private void sendCreatePO(ref CrudeImportPlanViewModel model, string action, string fixTripNo, string fixItemNo)
        {

            var json = new JavaScriptSerializer().Serialize(model.crude_approve_detail);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000031;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";

            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
            req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
            req.req_parameters.p.Add(new p { k = "trip_no", v = fixTripNo });
            req.req_parameters.p.Add(new p { k = "item_no", v = fixItemNo });
            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = CPAIConstantUtil.Action, v = action });

            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            if (!string.IsNullOrEmpty(resData.extra_xml))
            {
                model.crude_approve_detail = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(resData.extra_xml);


                var qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
                if (fixItemNo != "")
                    qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

                if (qHead != null)
                {

                    foreach (var itmHead in qHead)
                    {
                        string sts = "1";
                        string msg = "";

                        var queryMat = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(itmHead.PHE_TRIP_NO)).ToList();
                        if (fixItemNo != "")
                            queryMat = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(itmHead.PHE_TRIP_NO) && p.PMA_ITEM_NO.Equals(fixItemNo)).ToList();

                        if (queryMat != null)
                        {
                            foreach (var itmMat in queryMat)
                            {
                                if (!string.IsNullOrEmpty(itmMat.RET_STS) && itmMat.RET_STS.Equals("E"))
                                {
                                    sts = "99";
                                    msg += itmMat.RET_MSG;
                                }
                            }
                        }

                        string oldsts = string.IsNullOrEmpty(itmHead.PHE_SAVE_STS) ? "" : itmHead.PHE_SAVE_STS;

                        if (oldsts.Equals("1") || oldsts.Equals(""))
                        {
                            itmHead.PHE_SAVE_STS = sts;
                        }

                        if (!sts.Equals("1"))
                        {
                            itmHead.PHE_SAVE_MSG = msg;
                        }

                    }
                }

            }
        }

        public void SendCreatePOAndGIT(ref CrudeImportPlanViewModel model, string action, string fixTripNo, string fixItemno)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null)
            {
                return;
            }

            if (action != "GIT")
            {
                SendChangePO(ref model, fixTripNo, fixItemno);
            }


            sendCreatePO(ref model, action, fixTripNo, fixItemno);

            model.MsgAlert = "";
            int iSuccess = 0;
            int iError = 0;

            var qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixItemno != "")
                qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

            if (qHead != null)
            {
                iSuccess = qHead.Count;

                foreach (var itmHead in qHead)
                {
                    var itmItem = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(itmHead.PHE_TRIP_NO)).ToList();
                    foreach (var item in itmItem)
                    {
                        var subItem = model.crude_approve_detail.dDetail.Where(p => p.h_TripNo.Equals(item.PMA_TRIP_NO) && p.MatItemNo.Equals(item.PMA_ITEM_NO)).ToList();
                        foreach (var s in subItem)
                        {
                            s.h_GitNo = string.IsNullOrEmpty(item.PMA_GIT_NO) || item.PMA_GIT_NO.Equals("0") ? "" : item.PMA_GIT_NO;
                        }
                    }

                    string sts = string.IsNullOrEmpty(itmHead.PHE_SAVE_STS) ? "" : itmHead.PHE_SAVE_STS;

                    if (!sts.Equals("1"))
                    {
                        iError++;
                    }
                }

                iSuccess = iSuccess - iError;
            }


            model.MsgAlert = "Send PO Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();

        }

        public void SendCancelGIT(ref CrudeImportPlanViewModel model, string fixTripNo, string fixItemNo)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null)
            {
                return;
            }


            sendCancelGIT(ref model, fixTripNo, fixItemNo);

            model.MsgAlert = "";
            int iSuccess = 0;
            int iError = 0;

            var qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixItemNo != "")
                qHead = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

            if (qHead != null)
            {
                iSuccess = qHead.Count;

                foreach (var itmHead in qHead)
                {
                    string sts = string.IsNullOrEmpty(itmHead.PHE_SAVE_STS) ? "" : itmHead.PHE_SAVE_STS;

                    if (sts.Equals("E"))
                    {
                        iError++;
                    }

                }

                iSuccess = iSuccess - iError;
            }

            model.MsgAlert = "Send Cancel GIT Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();

        }


        public void SendMemoToSap(ref CrudeImportPlanViewModel model)
        {

            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null)
            {
                return;
            }

            model.MsgAlert = "";
            int iSuccess = 0;
            int iError = 0;

            var HeadList = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();

            foreach (var itemHead in HeadList)
            {
                itemHead.PHE_SAVE_MSG = "";
                itemHead.PHE_SAVE_STS = "";

                CrudeImportPlanViewModel modelTmp = new CrudeImportPlanViewModel();
                modelTmp.crude_approve_detail = new CrudeApproveFormViewModel_Detail();

                var tripno = itemHead.PHE_TRIP_NO;
                modelTmp.crude_approve_detail.dDetail_Header = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Material != null)
                    modelTmp.crude_approve_detail.dDetail_Material = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail != null)
                    modelTmp.crude_approve_detail.dDetail = model.crude_approve_detail.dDetail.Where(p => p.h_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Freight != null)
                    modelTmp.crude_approve_detail.dDetail_Freight = model.crude_approve_detail.dDetail_Freight.Where(p => p.FR_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Other != null)
                    modelTmp.crude_approve_detail.dDetail_Other = model.crude_approve_detail.dDetail_Other.Where(p => p.OC_TripNo.Equals(tripno)).ToList();

                if (model.crude_approve_detail.dDetail_Surveyor != null)
                    modelTmp.crude_approve_detail.dDetail_Surveyor = model.crude_approve_detail.dDetail_Surveyor.Where(p => p.Sur_TripNo.Equals(tripno)).ToList();


                var json = new JavaScriptSerializer().Serialize(modelTmp.crude_approve_detail);

                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000057;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";

                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();
                req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
                req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
                req.req_parameters.p.Add(new p { k = "trip_no", v = tripno });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ResponseData resData = new ResponseData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                int count_result = resData.resp_parameters.Count;
                string item = "";
                if (count_result > 0)
                {
                    item = resData.resp_parameters[0].v;

                    CrudeApproveFormViewModel_Detail modelDetailResp = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(item);

                    string ret_msg = "";
                    Boolean SuccessSTS = UpdateRespMemoToMainModel(ref ret_msg, tripno, ref model, modelDetailResp);
                    if (SuccessSTS)
                        iSuccess++;
                    else
                    {
                        itemHead.PHE_SAVE_MSG = ret_msg;
                        itemHead.PHE_SAVE_STS = "99";
                        iError++;
                    }


                }

            }

            model.MsgAlert = "Send Memo Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();


        }

        private Boolean UpdateRespMemoToMainModel(ref string msg, string tripno, ref CrudeImportPlanViewModel model, CrudeApproveFormViewModel_Detail ModelResp)
        {
            Boolean SuccessSTS = true;
            try
            {
                if (ModelResp == null)
                    return false;

                if (ModelResp.dDetail_Material != null)
                {
                    foreach (var itemMat in ModelResp.dDetail_Material)
                    {
                        var itemno = itemMat.PMA_ITEM_NO;
                        var query = model.crude_approve_detail.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && p.PMA_ITEM_NO.Equals(itemno)).First();
                        if (query != null)
                        {

                            query.PMA_MEMO_NO = itemMat.PMA_MEMO_NO;
                            query.RET_MSG = itemMat.RET_MSG;
                            query.RET_STS = itemMat.RET_STS;

                            string retSTS = string.IsNullOrEmpty(itemMat.RET_STS) ? "" : itemMat.RET_STS;
                            if (retSTS.Equals("E"))
                            {
                                msg += itemMat.RET_MSG;
                                SuccessSTS = false;
                            }
                        }



                    }
                }

                if (ModelResp.dDetail != null)
                {
                    foreach (var itemMatPrice in ModelResp.dDetail)
                    {
                        var itemno = itemMatPrice.MatItemNo;
                        var PriceItemNo = itemMatPrice.MatPriceItemNo;
                        var query = model.crude_approve_detail.dDetail.Where(p => p.h_TripNo.Equals(tripno) && p.MatItemNo.Equals(itemno) && p.MatPriceItemNo.Equals(PriceItemNo)).First();
                        if (query != null)
                        {

                            query.MemoNo = itemMatPrice.MemoNo;
                            query.h_MemoNo = itemMatPrice.MemoNo;

                            //string retSTS = string.IsNullOrEmpty(itemMat.RET_STS) ? "" : itemMat.RET_STS;
                            //if (retSTS.Equals("E"))
                            //{
                            //    msg += itemMat.RET_MSG;
                            //    SuccessSTS = false;
                            //}

                        }
                    }
                }

                if (ModelResp.dDetail_Freight != null)
                {
                    foreach (var itemFreight in ModelResp.dDetail_Freight)
                    {
                        var itemno = itemFreight.FR_ItemNo;
                        var query = model.crude_approve_detail.dDetail_Freight.Where(p => p.FR_TripNo.Equals(tripno) && p.FR_ItemNo.Equals(itemno)).First();
                        if (query != null)
                        {
                            query.FR_MemoNo = itemFreight.FR_MemoNo;
                            query.RET_MSG = itemFreight.RET_MSG;
                            query.RET_STS = itemFreight.RET_STS;

                            string retSTS = string.IsNullOrEmpty(itemFreight.RET_STS) ? "" : itemFreight.RET_STS;
                            if (retSTS.Equals("E"))
                            {
                                msg += itemFreight.RET_MSG;
                                SuccessSTS = false;
                            }

                        }
                    }
                }

                if (ModelResp.dDetail_Other != null)
                {
                    foreach (var itemOther in ModelResp.dDetail_Other)
                    {
                        var itemno = itemOther.OC_ItemNo;
                        var query = model.crude_approve_detail.dDetail_Other.Where(p => p.OC_TripNo.Equals(tripno) && p.OC_ItemNo.Equals(itemno)).First();
                        if (query != null)
                        {
                            query.OC_MemoNo = itemOther.OC_MemoNo;
                            query.RET_MSG = itemOther.RET_MSG;
                            query.RET_STS = itemOther.RET_STS;

                            string retSTS = string.IsNullOrEmpty(itemOther.RET_STS) ? "" : itemOther.RET_STS;
                            if (retSTS.Equals("E"))
                            {
                                msg += itemOther.RET_MSG;
                                SuccessSTS = false;
                            }

                        }
                    }
                }


            }
            catch (Exception ex)
            {

            }

            return SuccessSTS;
        }

        private string SurveyorDeliveryDate(string SurType, PCF_MATER_TYPE PCF_MATER_TYPE_DATA, SurveyorViewModel SurItem, ref string TypeText)
        {
            string ret = "";
            string Sur_LoadDate = "";
            string Sur_DischargeDate = "";
            if (!string.IsNullOrEmpty(SurItem.Sur_LoadDate))
            {
                Sur_LoadDate = SurItem.Sur_LoadDate.Replace(" to ", "|").Split('|')[0];
            }

            if (!string.IsNullOrEmpty(SurItem.Sur_DischargeDate))
            {
                Sur_DischargeDate = SurItem.Sur_DischargeDate.Replace(" to ", "|").Split('|')[0];
            }

            try
            {
                var query = PCF_MATER_TYPE_DATA.SurveyorTypeViewModel.Where(p => p.PMS_CODE.ToString().Equals(SurType)).First();
                if (query != null)
                {
                    TypeText = string.IsNullOrEmpty(query.PMS_NAME) ? "" : query.PMS_NAME;
                    if (query.PMS_LOADING_DATE.Equals("X") && query.PMS_DISCHARGING_DATE.Equals("X"))
                    {
                        if (!string.IsNullOrEmpty(Sur_LoadDate))
                        {
                            ret = DateTime.ParseExact(Sur_LoadDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));
                        }
                        else if (!string.IsNullOrEmpty(Sur_DischargeDate))
                        {
                            ret = DateTime.ParseExact(Sur_DischargeDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));
                        }

                    }
                    else if (query.PMS_LOADING_DATE.Equals("X"))
                    {
                        ret = DateTime.ParseExact(Sur_LoadDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));
                    }
                    else if (query.PMS_DISCHARGING_DATE.Equals("X"))
                    {
                        ret = DateTime.ParseExact(Sur_DischargeDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));
                    }
                }
            }
            catch (Exception ex) { }
            return ret;
        }

        private string SurveyorGetMatIO(string comcode, string crudecode)
        {
            string ret = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from h in context.MT_MATERIALS
                                 where h.MET_NUM.Equals(crudecode)
                                 select h).First();

                    if (query != null)
                    {
                        if (comcode == "1100")
                        {
                            ret = query.MET_IO;
                        }
                        else if (comcode == "1400")
                        {
                            ret = query.MET_IO_TLB;
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return ret;
        }
        private string SurveyorCrudeTextToSAP(string crudeList)
        {
            string ret = "";
            try
            {
                if (string.IsNullOrEmpty(crudeList))
                    return ret;

                ret = crudeList;

                //string[] arrText = crudeList.Split(',');
                //foreach (var item in arrText)
                //{
                //    if (!string.IsNullOrEmpty(item))
                //    {
                //        ret += "," + ((item.Length > 1) ? item.Substring(1) : item);
                //    }
                //}

                //if (ret != "")
                //    ret = ret.Substring(1);

            }
            catch (Exception ex) { }

            return ret;
        }

        private string SurveyorGetPurGroup(List<PCF_PO_SURVEYOR_PUR_GROUP> PurGroup)
        {
            string ret = "";
            try
            {
                string userGroup = Const.User.UserGroup;
                if (PurGroup == null)
                    return "";

                var query = PurGroup.Where(p => p.ROLE.Equals(userGroup)).First();
                if (query == null)
                    return "";

                ret = (query.VALUE == null) ? "" : query.VALUE;

            }
            catch (Exception ex) { }

            return ret;

        }

        public void SendSurveyorPopupToSap(ref CrudeImportPlanViewModel model, string FilterTripno, string action = "")
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null || model.crude_approve_detail.dDetail_Surveyor == null)
            {
                return;
            }

            model.MsgAlert = "";

            PCF_MATER_TYPE PCF_MATER_TYPE_DATA = null;
            string SurveyorTypeJSON = model.json_SurveyorConfig;
            if (SurveyorTypeJSON != "")
            {
                SurveyorTypeJSON = "{ \"SurveyorTypeViewModel\":" + SurveyorTypeJSON.Replace("null", "\"\"") + "}";
            }

            PCF_MATER_TYPE_DATA = (PCF_MATER_TYPE)Newtonsoft.Json.JsonConvert.DeserializeObject(SurveyorTypeJSON, typeof(PCF_MATER_TYPE));

            string PurGroup = SurveyorGetPurGroup(model.PurGroup);

            var HeadList = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(FilterTripno)).ToList();

            int iSuccess = 0;
            int iError = 0;

            foreach (var itemHead in HeadList)
            {
                itemHead.PHE_SAVE_STS = "";
                itemHead.PHE_SAVE_MSG = "";

                string phe_save_msg = "";
                Boolean IsError = false;
                var tripno = itemHead.PHE_TRIP_NO;

                var queryItem = model.crude_approve_detail.dDetail_Surveyor.Where(p => p.i_IS_CHECK.Equals(true)).ToList();

                foreach (var item in queryItem)
                {
                    string SurveyorPONo = string.IsNullOrEmpty(item.Sur_PO) ? "" : item.Sur_PO;
                    string flagDel = string.IsNullOrEmpty(item.IS_DELETE) ? "" : item.IS_DELETE;

                    if (action == "cancel")
                    {
                        flagDel = "X";

                        if (SurveyorPONo.Equals(""))
                        {
                            model.crude_approve_detail.dDetail_Surveyor.Remove(item);
                            continue;
                        }
                    }

                    string codecode = (string.IsNullOrEmpty(item.Sur_Crude)) ? "" : item.Sur_Crude.Split(',')[0];
                    string SurTypeText = "";
                    string sDeliveryDate = "";
                    sDeliveryDate = SurveyorDeliveryDate(item.Sur_Type, PCF_MATER_TYPE_DATA, item, ref SurTypeText);

                    var itemno = "00010";// item.Sur_ItemNo.PadLeft(5, '0');
                    var vendorcode = item.Sur_Surveyor;
                    if (!string.IsNullOrEmpty(vendorcode))
                    {
                        vendorcode = item.Sur_Surveyor.PadLeft(10, '0');
                    }

                    Model.POSuveyorModel SurModel = new Model.POSuveyorModel();
                    SurModel.mPOHEADER = new List<Model.POSuveyorModel.BAPIMEPOHEADER>();
                    SurModel.mPOHEADER.Add(new Model.POSuveyorModel.BAPIMEPOHEADER
                    {
                        PO_NUMBER = SurveyorPONo,
                        COMP_CODE = string.IsNullOrEmpty(itemHead.PHE_COMPANY_CODE) ? "" : itemHead.PHE_COMPANY_CODE,
                        Mflag = flagDel,
                        DOC_TYPE = "", // GLOBAL_CONFIG
                        DELETE_IND = "",
                        VENDOR = vendorcode, //0000000013
                        PMNTTRMS = "", // GLOBAL_CONFIG
                        PURCH_ORG = string.IsNullOrEmpty(itemHead.PHE_COMPANY_CODE) ? "" : itemHead.PHE_COMPANY_CODE,
                        PUR_GROUP = PurGroup, // GLOBAL_CONFIG โดยถ้า User เป็น Role CMCS จะเป็น 501, CMPS จะเป็น 502

                    });
                    SurModel.mPOACCOUNT = new List<Model.POSuveyorModel.BAPIMEPOACCOUNT>();
                    SurModel.mPOACCOUNT.Add(new Model.POSuveyorModel.BAPIMEPOACCOUNT
                    {
                        PO_ITEM = itemno,
                        SERIAL_NO = "01", // GLOBAL_CONFIG
                        DELETE_IND = "", // GLOBAL_CONFIG
                        QUANTITY = 1, // GLOBAL_CONFIG
                        GL_ACCOUNT = "0005000230", // GLOBAL_CONFIG
                        COSTCENTER = "0000113002", // GLOBAL_CONFIG
                        ORDERID = SurveyorGetMatIO(itemHead.PHE_COMPANY_CODE, codecode), //"D11C-ARLI",   // ถ้า 1100 ใช้ MT_MATERIALS.MET_IO, 1400 ใช้ MET_IO_TLB
                        CO_AREA = "1000", // GLOBAL_CONFIG

                    });

                    SurModel.mPOCOND = new List<Model.POSuveyorModel.BAPIMEPOCOND>();
                    SurModel.mPOCOND.Add(new Model.POSuveyorModel.BAPIMEPOCOND
                    {
                        ITM_NUMBER = itemno,
                        COND_ST_NO = "001", // GLOBAL_CONFIG
                        COND_VALUE = string.IsNullOrEmpty(item.Sur_Amount) ? 0M : Convert.ToDecimal(item.Sur_Amount), // 47112.57M,
                        CURRENCY = string.IsNullOrEmpty(item.Sur_AmountUnit) ? "" : item.Sur_AmountUnit,
                        CONDCOUNT = "01", // GLOBAL_CONFIG
                        CHANGE_ID = "U", // GLOBAL_CONFIG
                    });

                    string shortText = SurTypeText + " " + SurveyorCrudeTextToSAP(item.Sur_CrudeText) + " " + (string.IsNullOrEmpty(item.Sur_CostSharing) ? "" : item.Sur_CostSharing + "%");
                    if (shortText.Length > 40)
                        shortText = shortText.Substring(0, 40);

                    SurModel.mPOITEM = new List<Model.POSuveyorModel.BAPIMEPOITEM>();
                    SurModel.mPOITEM.Add(new Model.POSuveyorModel.BAPIMEPOITEM
                    {
                        PO_ITEM = itemno,
                        DELETE_IND = "", // type ' ' material ( ตัด y ทิ้ง, ) ' ' 100%
                        SHORT_TEXT = shortText,
                        PLANT = string.IsNullOrEmpty(itemHead.PHE_COMPANY_CODE) ? "" : itemHead.PHE_COMPANY_CODE,
                        TRACKINGNO = tripno,
                        MATL_GROUP = "IA5310", // GLOBAL_CONFIG IA5310
                        QUANTITY = 1, // GLOBAL_CONFIG
                        PO_UNIT = "JB", // GLOBAL_CONFIG
                        NET_PRICE = string.IsNullOrEmpty(item.Sur_Amount) ? 0M : Convert.ToDecimal(item.Sur_Amount), // 47112.57M,
                        ACCTASSCAT = "I", // GLOBAL_CONFIG
                        RET_ITEM = item.Sur_ItemNo,

                    });

                    SurModel.mPOSCHEDULE = new List<Model.POSuveyorModel.BAPIMEPOSCHEDULE>();
                    SurModel.mPOSCHEDULE.Add(new Model.POSuveyorModel.BAPIMEPOSCHEDULE
                    {
                        PO_ITEM = itemno,
                        SCHED_LINE = "0001", // GLOBAL_CONFIG
                        DEL_DATCAT_EXT = "D", // GLOBAL_CONFIG
                        DELIVERY_DATE = sDeliveryDate, // "2017-06-01", // PCF_PO_SURVEYOR.LOADIG_DATE หรือ DISCHARGING_DATE
                        QUANTITY = 1,
                    });


                    var json = new JavaScriptSerializer().Serialize(SurModel);

                    RequestData req = new RequestData();
                    req.function_id = ConstantPrm.FUNCTION.F10000056;
                    req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                    req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                    req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                    req.state_name = "";

                    req.req_parameters = new req_parameters();
                    req.req_parameters.p = new List<p>();
                    req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
                    req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
                    req.req_parameters.p.Add(new p { k = "trip_no", v = item.Sur_TripNo });
                    req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                    req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                    req.extra_xml = "";

                    ResponseData resData = new ResponseData();
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                    resData = service.CallService(req);

                    int count_result = resData.resp_parameters.Count;
                    if (count_result < 3)
                    {

                    }
                    else
                    {
                        string retsts = resData.resp_parameters[0].v.ToString();
                        string msg = resData.resp_parameters[1].v.ToString();
                        string pono = resData.resp_parameters[2].v.ToString();

                        item.Sur_PO = pono;
                        item.RET_MSG = msg;

                        if (retsts == "S")
                        {
                            if (flagDel == "X")
                            {
                                model.crude_approve_detail.dDetail_Surveyor.Remove(item);
                            }
                        }
                        else
                        {
                            phe_save_msg += msg;
                            IsError = true;
                        }

                    }


                }

                if (IsError)
                {
                    iError++;
                    itemHead.PHE_SAVE_STS = "9";
                    itemHead.PHE_SAVE_MSG = phe_save_msg;
                }
                else
                    iSuccess++;

            }


            model.MsgAlert = "Send Surveyor Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();



        }


        public void SendSurveyorToSap(ref CrudeImportPlanViewModel model)
        {
            if (model == null || model.crude_approve_detail == null || model.crude_approve_detail.dDetail_Header == null || model.crude_approve_detail.dDetail_Surveyor == null)
            {
                return;
            }

            model.MsgAlert = "";

            PCF_MATER_TYPE PCF_MATER_TYPE_DATA = null;
            string SurveyorTypeJSON = model.json_SurveyorConfig;
            if (SurveyorTypeJSON != "")
            {
                SurveyorTypeJSON = "{ \"SurveyorTypeViewModel\":" + SurveyorTypeJSON.Replace("null", "\"\"") + "}";
            }

            PCF_MATER_TYPE_DATA = (PCF_MATER_TYPE)Newtonsoft.Json.JsonConvert.DeserializeObject(SurveyorTypeJSON, typeof(PCF_MATER_TYPE));

            string PurGroup = SurveyorGetPurGroup(model.PurGroup);

            var HeadList = model.crude_approve_detail.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();

            int iSuccess = 0;
            int iError = 0;

            foreach (var itemHead in HeadList)
            {
                itemHead.PHE_SAVE_STS = "";
                itemHead.PHE_SAVE_MSG = "";

                string phe_save_msg = "";
                Boolean IsError = false;
                var tripno = itemHead.PHE_TRIP_NO;

                var queryItem = model.crude_approve_detail.dDetail_Surveyor.Where(p => p.Sur_TripNo.Equals(tripno)).ToList();


                foreach (var item in queryItem)
                {
                    string flagDel = string.IsNullOrEmpty(item.IS_DELETE) ? "" : item.IS_DELETE;
                    string codecode = (string.IsNullOrEmpty(item.Sur_Crude)) ? "" : item.Sur_Crude.Split(',')[0];
                    string SurTypeText = "";
                    string sDeliveryDate = "";
                    sDeliveryDate = SurveyorDeliveryDate(item.Sur_Type, PCF_MATER_TYPE_DATA, item, ref SurTypeText);

                    var itemno = "00010";// item.Sur_ItemNo.PadLeft(5, '0');
                    var vendorcode = item.Sur_Surveyor;
                    if (!string.IsNullOrEmpty(vendorcode))
                    {
                        vendorcode = item.Sur_Surveyor.PadLeft(10, '0');
                    }

                    Model.POSuveyorModel SurModel = new Model.POSuveyorModel();
                    SurModel.mPOHEADER = new List<Model.POSuveyorModel.BAPIMEPOHEADER>();
                    SurModel.mPOHEADER.Add(new Model.POSuveyorModel.BAPIMEPOHEADER
                    {
                        PO_NUMBER = string.IsNullOrEmpty(item.Sur_PO) ? "" : item.Sur_PO,
                        COMP_CODE = string.IsNullOrEmpty(itemHead.PHE_COMPANY_CODE) ? "" : itemHead.PHE_COMPANY_CODE,
                        Mflag = flagDel,
                        DOC_TYPE = "", // GLOBAL_CONFIG
                        DELETE_IND = "",
                        VENDOR = vendorcode, //0000000013
                        PMNTTRMS = "", // GLOBAL_CONFIG
                        PURCH_ORG = string.IsNullOrEmpty(itemHead.PHE_COMPANY_CODE) ? "" : itemHead.PHE_COMPANY_CODE,
                        PUR_GROUP = PurGroup, // GLOBAL_CONFIG โดยถ้า User เป็น Role CMCS จะเป็น 501, CMPS จะเป็น 502

                    });
                    SurModel.mPOACCOUNT = new List<Model.POSuveyorModel.BAPIMEPOACCOUNT>();
                    SurModel.mPOACCOUNT.Add(new Model.POSuveyorModel.BAPIMEPOACCOUNT
                    {
                        PO_ITEM = itemno,
                        SERIAL_NO = "01", // GLOBAL_CONFIG
                        DELETE_IND = "", // GLOBAL_CONFIG
                        QUANTITY = 1, // GLOBAL_CONFIG
                        GL_ACCOUNT = "0005000230", // GLOBAL_CONFIG
                        COSTCENTER = "0000113002", // GLOBAL_CONFIG
                        ORDERID = SurveyorGetMatIO(itemHead.PHE_COMPANY_CODE, codecode), //"D11C-ARLI",   // ถ้า 1100 ใช้ MT_MATERIALS.MET_IO, 1400 ใช้ MET_IO_TLB
                        CO_AREA = "1000", // GLOBAL_CONFIG

                    });

                    SurModel.mPOCOND = new List<Model.POSuveyorModel.BAPIMEPOCOND>();
                    SurModel.mPOCOND.Add(new Model.POSuveyorModel.BAPIMEPOCOND
                    {
                        ITM_NUMBER = itemno,
                        COND_ST_NO = "001", // GLOBAL_CONFIG
                        COND_VALUE = string.IsNullOrEmpty(item.Sur_Amount) ? 0M : Convert.ToDecimal(item.Sur_Amount), // 47112.57M,
                        CURRENCY = string.IsNullOrEmpty(item.Sur_AmountUnit) ? "" : item.Sur_AmountUnit,
                        CONDCOUNT = "01", // GLOBAL_CONFIG
                        CHANGE_ID = "U", // GLOBAL_CONFIG
                    });

                    string shortText = SurTypeText + " " + SurveyorCrudeTextToSAP(item.Sur_CrudeText) + " " + (string.IsNullOrEmpty(item.Sur_CostSharing) ? "" : item.Sur_CostSharing + "%");
                    if (shortText.Length > 40)
                        shortText = shortText.Substring(0, 40);

                    SurModel.mPOITEM = new List<Model.POSuveyorModel.BAPIMEPOITEM>();
                    SurModel.mPOITEM.Add(new Model.POSuveyorModel.BAPIMEPOITEM
                    {
                        PO_ITEM = itemno,
                        DELETE_IND = "", // type ' ' material ( ตัด y ทิ้ง, ) ' ' 100%
                        SHORT_TEXT = shortText,
                        PLANT = string.IsNullOrEmpty(itemHead.PHE_COMPANY_CODE) ? "" : itemHead.PHE_COMPANY_CODE,
                        TRACKINGNO = tripno,
                        MATL_GROUP = "IA5310", // GLOBAL_CONFIG IA5310
                        QUANTITY = 1, // GLOBAL_CONFIG
                        PO_UNIT = "JB", // GLOBAL_CONFIG
                        NET_PRICE = string.IsNullOrEmpty(item.Sur_Amount) ? 0M : Convert.ToDecimal(item.Sur_Amount), // 47112.57M,
                        ACCTASSCAT = "I", // GLOBAL_CONFIG
                        RET_ITEM = item.Sur_ItemNo,

                    });

                    SurModel.mPOSCHEDULE = new List<Model.POSuveyorModel.BAPIMEPOSCHEDULE>();
                    SurModel.mPOSCHEDULE.Add(new Model.POSuveyorModel.BAPIMEPOSCHEDULE
                    {
                        PO_ITEM = itemno,
                        SCHED_LINE = "0001", // GLOBAL_CONFIG
                        DEL_DATCAT_EXT = "D", // GLOBAL_CONFIG
                        DELIVERY_DATE = sDeliveryDate, // "2017-06-01", // PCF_PO_SURVEYOR.LOADIG_DATE หรือ DISCHARGING_DATE
                        QUANTITY = 1,
                    });


                    var json = new JavaScriptSerializer().Serialize(SurModel);

                    RequestData req = new RequestData();
                    req.function_id = ConstantPrm.FUNCTION.F10000056;
                    req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                    req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                    req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                    req.state_name = "";

                    req.req_parameters = new req_parameters();
                    req.req_parameters.p = new List<p>();
                    req.req_parameters.p.Add(new p { k = "user", v = ConstantPrm.ENGINECONF.EnginAppID });
                    req.req_parameters.p.Add(new p { k = "system", v = "PCF" });
                    req.req_parameters.p.Add(new p { k = "trip_no", v = item.Sur_TripNo });
                    req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                    req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                    req.extra_xml = "";

                    ResponseData resData = new ResponseData();
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                    resData = service.CallService(req);

                    int count_result = resData.resp_parameters.Count;
                    if (count_result < 3)
                    {

                    }
                    else
                    {
                        string retsts = resData.resp_parameters[0].v.ToString();
                        string msg = resData.resp_parameters[1].v.ToString();
                        string pono = resData.resp_parameters[2].v.ToString();

                        item.Sur_PO = pono;
                        item.RET_MSG = msg;

                        if (retsts == "S")
                        {
                            if (flagDel == "X")
                            {
                                model.crude_approve_detail.dDetail_Surveyor.Remove(item);
                            }
                        }
                        else
                        {
                            phe_save_msg += msg;
                            IsError = true;
                        }

                    }


                }

                if (IsError)
                {
                    iError++;
                    itemHead.PHE_SAVE_STS = "9";
                    itemHead.PHE_SAVE_MSG = phe_save_msg;
                }
                else
                    iSuccess++;

            }


            model.MsgAlert = "Send Surveyor Result<br>Success : " + iSuccess.ToString() + " <br> Error : " + iError.ToString();



        }




        public static string CalDueDateService(DateTime DueDate, string matcode = "")
        {
            string ret = "";
            try
            {
                ret = DueDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));

                PCFHolidayFormulaModel mPCFHolidayFormula = getPCFHolidayFormula("PCF_HOLIDAY_FORMULAR");
                if (mPCFHolidayFormula == null)
                    return ret;

                string mat_dc = "";
                var query = mPCFHolidayFormula.PCF_MATERIAL_DC.Where(p => p.MAT_CODE.Equals(matcode)).First();
                if (query != null)
                    mat_dc = query.DC;

                // LOCAL, IMPORT
                if (string.IsNullOrEmpty(mat_dc))
                    return ret;

                int nDayInWeek = (int)DueDate.DayOfWeek;

                Boolean ProcessCheckHoliday = true;
                while (ProcessCheckHoliday)
                {



                    // end check Holiday
                    ProcessCheckHoliday = false;
                }


                string BankType = "";
                int nCondition = 0;
                var query2 = mPCFHolidayFormula.PCF_HOLIDAY_FORMULA.Where(p => p.CRUDE_DC.Equals(mat_dc) && p.BANK_TYPE.ToUpper().Equals(BankType.ToUpper()) && (p.DAY_START >= nDayInWeek && p.DAY_END <= nDayInWeek)).First();
                if (query2 != null)
                {
                    BankType = query2.BANK_TYPE;
                    nCondition = string.IsNullOrEmpty(query2.HOLIDAY_ADJ) ? 0 : Convert.ToInt32(query2.HOLIDAY_ADJ);
                }

                if (string.IsNullOrEmpty(BankType))
                    return ret;

                if (nCondition == 0)
                    return ret;


                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{

                //    if (nCondition > 0)
                //    {
                //        var hquery = (from v in context.MT_HOLIDAY where v.MH_HOL_TYPE.Equals(BankType) orderby v.PIN_EFFECTIVE_TO descending select v).First();

                //        if (hquery != null)
                //        {
                //            retVol = (query.PIN_RATE == null) ? 0.0 : Convert.ToDouble(query.PIN_RATE.ToString());
                //        }


                //    }
                //    else {

                //    }


                //}


            }
            catch (Exception ex)
            {

            }



            return ret;
        }

        private static List<MT_HOLIDAY> getHoliday(string BankType)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var hquery = (from v in context.MT_HOLIDAY where v.MH_HOL_TYPE.ToUpper().Equals(BankType.ToUpper()) select v);
                if (hquery != null)
                    return hquery.ToList();
                else
                    return null;
            }
        }

        public static String checkSupplierInSAP(string code)
        {
            String ret = "0";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var hquery = (from v in context.MT_VENDOR where v.VND_ACC_NUM_VENDOR.Equals(code) && v.VND_CREATE_TYPE.ToUpper().Equals("SAP") select v);
                    if (hquery != null)
                        if (hquery.Count() > 0)
                            ret = "1"; 
                }
            }
            catch (Exception ex) { }

            return ret;
        }

        public static ResultModel CalDueDateService(DateTime DueDate, string bankType = "", string matCode = "", string currency = "")
        {
            ResultModel result = new ResultModel();

            try
            {
                result.TYPE = "S";
                result.MSG_ID = "1";
                result.MSG_TXT = "";

                result.VALUE = DueDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));

                PCFHolidayFormulaModel mPCFHolidayFormula = getPCFHolidayFormula("PCF_HOLIDAY_FORMULAR");
                if (mPCFHolidayFormula == null)
                {
                    result.TYPE = "E";
                    result.MSG_ID = "99";
                    result.MSG_TXT = "can not found 'PCF_HOLIDAY_FORMULAR' in GLOBAL CONFIG ";
                    return result;
                }


                string crude_dc = "";
                //if (string.IsNullOrEmpty(matcode))
                //{
                //    if (string.IsNullOrEmpty(bankType))
                //        bankType = "Thai";

                //    if (string.IsNullOrEmpty(mat_dc))
                //        mat_dc = "LOCAL";
                //}
                //else {
                //    var query = mPCFHolidayFormula.PCF_MATERIAL_DC.Where(p => p.MAT_CODE.ToUpper().Equals(matcode));
                //    if (query != null && query.Count() > 0)
                //    {
                //        mat_dc = query.First().DC ?? "";
                //        query = null;
                //    }
                //    else {

                //    }

                //}

                var query = mPCFHolidayFormula.PCF_MATERIAL_DC.Where(p => p.MAT_CODE.ToUpper().Equals(matCode));
                if (query != null && query.Count() > 0)
                {
                    crude_dc = query.First().DC ?? "";
                    query = null;
                }
                else
                {
                    crude_dc = "IMPORT";
                }

                // LOCAL, IMPORT
                if (string.IsNullOrEmpty(crude_dc))
                {
                    result.TYPE = "E";
                    result.MSG_ID = "99";
                    result.MSG_TXT = "can not found 'CRUDE DC' ";
                    return result;
                }

                if (string.IsNullOrEmpty(bankType) || string.IsNullOrEmpty(currency))
                {
                    var qBank = mPCFHolidayFormula.PCF_CRUDE_DC_BANK.Where(p => p.CRUDE_DC.ToUpper().Equals(crude_dc.ToUpper()));
                    if (qBank != null)
                    {

                        if (string.IsNullOrEmpty(bankType))
                            bankType = qBank.First().BANK_TYPE ?? "";

                        if (string.IsNullOrEmpty(currency))
                            currency = qBank.First().CURRENCY ?? "";

                        qBank = null;
                    }
                }

                // UK, Thai
                if (string.IsNullOrEmpty(bankType))
                {
                    result.TYPE = "E";
                    result.MSG_ID = "99";
                    result.MSG_TXT = "can not found 'BANK TYPE' ";
                    return result;
                }

                result.VALUE2 = bankType;

                var qHoliday = getHoliday(bankType);
                if (qHoliday == null)
                {
                    result.TYPE = "E";
                    result.MSG_ID = "99";
                    result.MSG_TXT = " Holiday Data is null !! ";
                    return result;
                }


                int nDayInWeek = (int)DueDate.DayOfWeek;
                int holidayAdjust = 0;
                var query2 = mPCFHolidayFormula.PCF_HOLIDAY_FORMULA.Where(p => p.CRUDE_DC.Equals(crude_dc) && p.BANK_TYPE.ToUpper().Equals(bankType.ToUpper()) && p.CURRENCY.ToUpper().Equals(currency.ToUpper()) && (nDayInWeek >= p.DAY_START && nDayInWeek <= p.DAY_END));
                if (query2 != null && query2.Count() > 0)
                    holidayAdjust = string.IsNullOrEmpty(query2.First().HOLIDAY_ADJ) ? 0 : Convert.ToInt32(query2.First().HOLIDAY_ADJ);
                else
                {
                    result.VALUE = DueDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));
                    return result;
                }


                DateTime startDate = DueDate;

                Boolean ProcessCheckHoliday = true;
                while (ProcessCheckHoliday)
                {
                    // end check Holiday
                    ProcessCheckHoliday = false;

                    int iDayInWeek = (int)startDate.DayOfWeek;

                    // sat & sun
                    if (iDayInWeek == 0 || iDayInWeek == 6)
                    {
                        ProcessCheckHoliday = true;

                        startDate = startDate.AddDays(holidayAdjust);

                    }
                    else
                    {
                        var qHolidayChk = qHoliday.Where(p => p.MH_HOL_DATE.Equals(startDate));
                        if (qHolidayChk != null && qHolidayChk.Count() > 0)
                        {
                            ProcessCheckHoliday = true;

                            startDate = startDate.AddDays(holidayAdjust);
                        }
                    }
                }

                result.VALUE = startDate.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("en-US"));

            }
            catch (Exception ex)
            {
                result.TYPE = "E";
                result.MSG_ID = "99";
                result.MSG_TXT = ex.ToString();
            }

            return result;
        }

    }
}
