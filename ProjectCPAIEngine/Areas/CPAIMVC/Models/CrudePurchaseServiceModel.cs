﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

using ProjectCPAIEngine.DAL.DALCDPH;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALBunker;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALCharter;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CrudePurchaseServiceModel
    {
        public static string getTransactionByID(string id)
        {
            CdpRootObject rootObj = new CdpRootObject();

            rootObj.approve_items = new List<ApproveItem>();
            rootObj.compet_offers_items = new List<CdpCompetOffersItem>();
            rootObj.payment_terms = new CdpPaymentTerms();
            rootObj.contract_period = new CdpContractPeriod();
            rootObj.proposal = new CdpProposal();
            rootObj.offers_items = new List<CdpOffersItem>();
            rootObj.loading_period = new CdpLoadingPeriod();
            rootObj.offers_items = new List<CdpOffersItem>();
            rootObj.discharging_period = new CdpDischargingPeriod();

            CDP_DATA_DAL dataDal = new CDP_DATA_DAL();
            CDP_DATA data = dataDal.GetByID(id);
            if (data != null)
            {
                MT_MATERIALS mat = MaterialsDAL.getMaterialById_New(PROJECT_NAME_MAT_CMCS, ACTIVE, data.CDA_FK_MATERIALS);
                //MT_COUNTRY ct = CountryDAL.getCountryByAbbrv(data.CDA_ORIGIN);

                rootObj.date_purchase = (data.CDA_PURCHASE_DATE == DateTime.MinValue || data.CDA_PURCHASE_DATE == null) ? "" : Convert.ToDateTime(data.CDA_PURCHASE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.plan_month = data.CDA_TPC_PLAN_MONTH;
                rootObj.plan_year = data.CDA_TPC_PLAN_YEAR;
                rootObj.feedstock = data.CDA_FEED_STOCK;
                rootObj.feedstock_others = data.CDA_FEED_STOCK_OTHERS;
                rootObj.for_feedStock = data.CDA_FOR_FEED_STOCK;
                rootObj.crude_id = data.CDA_FK_MATERIALS;
                rootObj.crude_name = mat != null ? mat.MET_MAT_DES_ENGLISH : data.CDA_MATERIALS_OTHERS;
                rootObj.crude_name_others = data.CDA_MATERIALS_OTHERS;
                rootObj.origin_id = data.CDA_ORIGIN;
                rootObj.origin = data.CDA_ORIGIN;
                //rootObj.origin = ct != null ? ct.MCT_LANDX : "";
                rootObj.term = data.CDA_TERMS;
                rootObj.term_others = data.CDA_TERMS_OTHERS;
                rootObj.purchase = data.CDA_PURCHASE;
                rootObj.supply_source = data.CDA_SUPPLIER_SOURCE;
                rootObj.explanation = data.CDA_EXPLANATION;
                rootObj.proposal.performance_bond = data.CDA_PERFORMANCE_BOND;
                rootObj.offered_by_ptt = data.CDA_OFFERED_BY_PTT;

                rootObj.reasonPTToffer = data.CDA_REASON_OFFER;
                rootObj.offerVia = data.CDA_OFFER_VIA;
                rootObj.reasonOfferVia = data.CDA_REASON_VIA;
                //if(!String.IsNullOrEmpty(data.CDA_OFFER_DATE))
                //{
                //    var newDate = Convert.ToDateTime(data.CDA_OFFER_DATE);
                //    rootObj.offerDate = newDate.ToString("dd/MM/yyyy");
                //}
                //else
                //{
                //    rootObj.offerDate = data.CDA_OFFER_DATE;
                //}                
                rootObj.offerDate = data.CDA_OFFER_DATE;
                string strExplanationAttach = "";
                CDP_ATTACH_FILE_DAL fileDal = new CDP_ATTACH_FILE_DAL();
                List<CDP_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "EXT");
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.CAF_PATH + ":" + item.CAF_INFO + "|";
                }
                rootObj.explanationAttach = strExplanationAttach;

                extFile = fileDal.GetFileByDataID(id, "BND");
                strExplanationAttach = "";
                foreach (var item in extFile)
                {
                    if (!strExplanationAttach.Equals("")) strExplanationAttach += "|";
                    strExplanationAttach += item.CAF_PATH;
                }
                rootObj.bond_documents = strExplanationAttach;

                CDP_OFFER_ITEMS_DAL offDal = new CDP_OFFER_ITEMS_DAL();
                List<CDP_OFFER_ITEMS> offlst = offDal.GetByID(id);
                foreach (var off in offlst)
                {
                    MT_VENDOR vd = VendorDAL.GetVendorById(off.COI_FK_VENDOR);
                    CdpOffersItem newoffer = new CdpOffersItem();
                    newoffer.round_items = new List<CdpRoundItem>();

                    newoffer.supplier = off.COI_FK_VENDOR;
                    newoffer.supplier_name = vd != null ? vd.VND_NAME1 : "";

                    newoffer.contact_person = off.COI_CONTACT_PERSON;
                    newoffer.quantity = off.COI_QUANTITY;
                    newoffer.quantity_max = off.COI_QUANTITY_MAX;
                    newoffer.quantity_unit = off.COI_QUANTITY_UNIT;
                    newoffer.incoterms = off.COI_INCOTERMS;
                    newoffer.market_source = off.COI_MARKET_SOURCE;
                    newoffer.latest_lp_plan_price = off.COI_LATEST_LP_PLAN_PRICE;
                    newoffer.bechmark_price = off.COI_BECHMARK_PRICE;
                    newoffer.rank_round = off.COI_RANK_ROUND;
                    newoffer.margin_lp = off.COI_MARGIN_LP;
                    newoffer.final_flag = off.COI_FINAL_FLAG;

                    CDP_ROUND_ITEMS_DAL itemDal = new CDP_ROUND_ITEMS_DAL();
                    List<CDP_ROUND_ITEMS> itemlst = itemDal.GetByID(off.COI_ROW_ID);
                    foreach (var item in itemlst)
                    {
                        CdpRoundItem newitem = new CdpRoundItem();
                        newitem.round_no = item.CRI_ROUND_NO;
                        newitem.round_value = new List<string>();

                        CDP_ROUND_DAL roundDal = new CDP_ROUND_DAL();
                        List<CDP_ROUND> roundlst = roundDal.GetByID(item.CRI_ROW_ID);
                        foreach (var round in roundlst)
                        {
                            newitem.round_value.Add(round.CRD_ROUND_VALUE);
                        }
                        newoffer.round_items.Add(newitem);
                    }

                    rootObj.offers_items.Add(newoffer);
                }

                CDP_OFFER_COMPET_ITEMS_DAL offCompetDal = new CDP_OFFER_COMPET_ITEMS_DAL();
                List<CDP_OFFER_COMPET_ITEMS> offCompetlst = offCompetDal.GetByID(id);
                foreach (var off in offCompetlst)
                {
                    MT_MATERIALS mat2 = MaterialsDAL.getMaterialById(PROJECT_NAME_MAT_CMCS, ACTIVE, off.CCI_FK_MATERIALS);
                    MT_VENDOR vd = VendorDAL.GetVendorById(off.CCI_FK_VENDOR);

                    CdpCompetOffersItem newoffer = new CdpCompetOffersItem();
                    newoffer.round_items = new List<CdpRoundItem>();

                    newoffer.crude_id = off.CCI_FK_MATERIALS;
                    newoffer.crude_name = mat2 != null ? mat2.MET_MAT_DES_ENGLISH : "";
                    newoffer.crude_name_others = off.CCI_CRUDE_NAME_OTHERS;

                    newoffer.supplier = off.CCI_FK_VENDOR;
                    newoffer.supplier_name = vd != null ? vd.VND_NAME1 : "";

                    newoffer.contact_person = off.CCI_CONTACT_PERSON;
                    newoffer.quantity = off.CCI_QUANTITY;
                    newoffer.quantity_max = off.CCI_QUANTITY_MAX;
                    newoffer.quantity_unit = off.CCI_QUANTITY_UNIT;
                    newoffer.incoterms = off.CCI_INCOTERMS;
                    newoffer.market_source = off.CCI_MARKET_SOURCE;
                    newoffer.latest_lp_plan_price = off.CCI_LATEST_LP_PLAN_PRICE;
                    newoffer.bechmark_price = off.CCI_BECHMARK_PRICE;
                    newoffer.rank_round = off.CCI_RANK_ROUND;
                    newoffer.margin_lp = off.CCI_MARGIN_LP;

                    CDP_ROUND_COMPET_ITEMS_DAL competItemDal = new CDP_ROUND_COMPET_ITEMS_DAL();
                    List<CDP_ROUND_COMPET_ITEMS> competItemlst = competItemDal.GetByID(off.CCI_ROW_ID);
                    foreach (var item in competItemlst)
                    {
                        CdpRoundItem newitem = new CdpRoundItem();
                        newitem.round_no = item.CIC_ROUND_NO;
                        newitem.round_value = new List<string>();

                        CDP_ROUND_COMPET_DAL roundDal = new CDP_ROUND_COMPET_DAL();
                        List<CDP_ROUND_COMPET> roundlst = roundDal.GetByID(item.CIC_ROW_ID);
                        foreach (var round in roundlst)
                        {
                            newitem.round_value.Add(round.CRC_ROUND_VALUE);
                        }
                        newoffer.round_items.Add(newitem);
                    }

                    rootObj.compet_offers_items.Add(newoffer);
                }

                rootObj.contract_period.date_from = (data.CDA_CONTRACT_DATE_FROM == DateTime.MinValue || data.CDA_CONTRACT_DATE_FROM == null) ? "" : Convert.ToDateTime(data.CDA_CONTRACT_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.contract_period.date_to = (data.CDA_CONTRACT_DATE_TO == DateTime.MinValue || data.CDA_CONTRACT_DATE_TO == null) ? "" : Convert.ToDateTime(data.CDA_CONTRACT_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.loading_period.date_from = (data.CDA_LOADING_DATE_FROM == DateTime.MinValue || data.CDA_LOADING_DATE_FROM == null) ? "" : Convert.ToDateTime(data.CDA_LOADING_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.loading_period.date_to = (data.CDA_LOADING_DATE_TO == DateTime.MinValue || data.CDA_LOADING_DATE_TO == null) ? "" : Convert.ToDateTime(data.CDA_LOADING_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.discharging_period.date_from = (data.CDA_DISCHARGING_DATE_FROM == DateTime.MinValue || data.CDA_DISCHARGING_DATE_FROM == null) ? "" : Convert.ToDateTime(data.CDA_DISCHARGING_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.discharging_period.date_to = (data.CDA_DISCHARGING_DATE_TO == DateTime.MinValue || data.CDA_DISCHARGING_DATE_TO == null) ? "" : Convert.ToDateTime(data.CDA_DISCHARGING_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.proposal.price_period_from = (data.CDA_PRICING_PERIOD_FROM == DateTime.MinValue || data.CDA_PRICING_PERIOD_FROM == null) ? "" : Convert.ToDateTime(data.CDA_PRICING_PERIOD_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.proposal.price_period_to = (data.CDA_PRICING_PERIOD_TO == DateTime.MinValue || data.CDA_PRICING_PERIOD_TO == null) ? "" : Convert.ToDateTime(data.CDA_PRICING_PERIOD_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.gtc = data.CDA_GTC;

                rootObj.other_condition = data.CDA_OTHER_CONDITION;

                rootObj.payment_terms.payment_term = data.CDA_PAYMENT_TERMS;
                rootObj.payment_terms.payment_term_detail = data.CDA_PAYMENT_TERMS_DETAIL;
                rootObj.payment_terms.sub_payment_term = data.CDA_SUB_PAYMENT_TERMS;
                rootObj.payment_terms.payment_term_others = data.CDA_PAYMENT_TERMS_OTHERS;

                rootObj.proposal.award_to = data.CDA_PROPOSAL_AWARD_TO;
                rootObj.proposal.reason = data.CDA_PROPOSAL_REASON;
                rootObj.proposal.reason_ref1_bbl = data.CDA_PROPOSAL_REF1_BBL;
                rootObj.proposal.reason_ref1_us = data.CDA_PROPOSAL_REF1_US;
                rootObj.proposal.reason_ref2_bbl = data.CDA_PROPOSAL_REF2_BBL;
                rootObj.proposal.reason_ref2_us = data.CDA_PROPOSAL_REF2_US;
                rootObj.proposal.reason_others = data.CDA_PROPOSAL_REASON_OTHERS;
                rootObj.proposal.volume = data.CDA_VOLUME;
                rootObj.proposal.tolerance = data.CDA_TOLERANCE;
                rootObj.proposal.tolerance_option = data.CDA_TOLERANCE_OPTION;
                rootObj.proposal.tolerance_type = data.CDA_TOLERANCE_TYPE;
                rootObj.proposal.supplier = data.CDA_PROPOSAL_SUPPLIER;

                rootObj.notes = data.CDA_NOTE;
                rootObj.approve_items = null;
                rootObj.reason = data.CDA_REASON;
                rootObj.requested_by = data.CDA_REQUESTED_BY;
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getAttFileByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            CDP_ATTACH_FILE_DAL fileDal = new CDP_ATTACH_FILE_DAL();
            List<CDP_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].CAF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        public ReturnValue Search(ref CrudePurchaseReportViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sLoadingPeriodDateFrom = String.IsNullOrEmpty(pModel.sLoadingPeriodDate) ? "" : pModel.sLoadingPeriodDate.ToUpper().Substring(0, 10);
                    var sLoadingPeriodDateTo = String.IsNullOrEmpty(pModel.sLoadingPeriodDate) ? "" : pModel.sLoadingPeriodDate.ToUpper().Substring(14, 10);

                    var sDischargingPeriodDateFrom = String.IsNullOrEmpty(pModel.sDischargingPeriodDate) ? "" : pModel.sDischargingPeriodDate.ToUpper().Substring(0, 10);
                    var sDischargingPeriodDateTo = String.IsNullOrEmpty(pModel.sDischargingPeriodDate) ? "" : pModel.sDischargingPeriodDate.ToUpper().Substring(14, 10);

                    var sFeedStock = String.IsNullOrEmpty(pModel.sFeedStock) ? "" : pModel.sFeedStock.ToUpper();
                    var sCrudeName = String.IsNullOrEmpty(pModel.sCrudeName) ? "" : pModel.sCrudeName.ToUpper();
                    var sSupplier = String.IsNullOrEmpty(pModel.sSupplier) ? "" : pModel.sSupplier.ToUpper();

                    var query = (from d in context.CDP_DATA
                                 join o in context.CDP_OFFER_ITEMS on d.CDA_ROW_ID equals o.COI_FK_CDP_DATA into viewOffer
                                 from lvo in viewOffer.DefaultIfEmpty()      
                                 where lvo.COI_FINAL_FLAG.Equals("Y")
                                 && d.CDA_STATUS.Equals("APPROVED")
                                 select new
                                 {
                                     dLoadingFrom = (DateTime?)d.CDA_LOADING_DATE_FROM
                                     ,
                                     dLoadingTo = (DateTime?)d.CDA_LOADING_DATE_TO
                                     ,
                                     dDischargingFrom = (DateTime?)d.CDA_DISCHARGING_DATE_FROM
                                     ,
                                     dDischargingTo = (DateTime?)d.CDA_DISCHARGING_DATE_TO
                                     ,
                                     dCrudeId = d.CDA_FK_MATERIALS
                                     ,
                                     dCrudeName = d.MT_MATERIALS.MET_MAT_DES_ENGLISH
                                     ,
                                     dCrudeOther = d.CDA_MATERIALS_OTHERS
                                     ,
                                     dOrigin = d.CDA_ORIGIN
                                     ,
                                     dFeedStock = d.CDA_FEED_STOCK
                                     ,
                                     dFeedStockOther = d.CDA_FEED_STOCK_OTHERS
                                     ,
                                     dFor = d.CDA_FOR_FEED_STOCK
                                     ,
                                     dSupplier = lvo.MT_VENDOR.VND_NAME1
                                     ,
                                     dContactPerson = lvo.COI_CONTACT_PERSON
                                     ,
                                     dQuantity = lvo.COI_QUANTITY
                                     ,
                                     dIncoterms = lvo.COI_INCOTERMS
                                     ,
                                     dOfferRowID = lvo.COI_ROW_ID
                                     ,
                                     dMarketSource = lvo.COI_MARKET_SOURCE
                                     ,
                                     dLatestLP = lvo.COI_LATEST_LP_PLAN_PRICE
                                     ,
                                     dBechmarkPrice = lvo.COI_BECHMARK_PRICE
                                     ,
                                     dRankRound = lvo.COI_RANK_ROUND
                                     ,
                                     dMarginLP = lvo.COI_MARGIN_LP
                                     
                                 });


                    if (!string.IsNullOrEmpty(sLoadingPeriodDateFrom) && !string.IsNullOrEmpty(sLoadingPeriodDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sLoadingPeriodDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sLoadingPeriodDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => (p.dLoadingFrom >= sDate && p.dLoadingFrom <= eDate) && (p.dLoadingTo >= sDate && p.dLoadingTo <= eDate));
                    }

                    if (!string.IsNullOrEmpty(sDischargingPeriodDateFrom) && !string.IsNullOrEmpty(sDischargingPeriodDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDischargingPeriodDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDischargingPeriodDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => (p.dDischargingFrom >= sDate && p.dDischargingFrom <= eDate) && (p.dDischargingTo >= sDate && p.dDischargingTo <= eDate));
                    }



                    if (query != null)
                    {
                        pModel.sSearchData = new List<CrudePurchaseReportViewModel_SearchData>();
                        decimal dMarginTotal = 0;
                        foreach (var g in query)
                        {
                            decimal quantity = string.IsNullOrEmpty(g.dQuantity) ? 0 :Convert.ToDecimal(g.dQuantity);
                            decimal MarginLP = string.IsNullOrEmpty(g.dMarginLP) ? 0 : Convert.ToDecimal(g.dMarginLP);
                            decimal dMargin = ((quantity * MarginLP) * 1000);
                            string Margin = dMargin.ToString("#,##0");
                            string offer = getLastRoundValue(g.dOfferRowID);

                            dMarginTotal +=  dMargin;


                            pModel.sSearchData.Add(
                                                    new CrudePurchaseReportViewModel_SearchData
                                                    {
                                                        dLoadingPeriodDate = g.dLoadingFrom == null && g.dLoadingTo == null ? "" : Convert.ToDateTime(g.dLoadingFrom).ToString("dd MMM yy", System.Globalization.CultureInfo.InvariantCulture) +" - "+ Convert.ToDateTime(g.dLoadingTo).ToString("dd MMM yy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dDischargingPeriodDate = g.dDischargingFrom == null && g.dDischargingTo == null ? "" : Convert.ToDateTime(g.dDischargingFrom).ToString("dd MMM yy", System.Globalization.CultureInfo.InvariantCulture) + " - " + Convert.ToDateTime(g.dDischargingTo).ToString("dd MMM yy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dCrudeName = string.IsNullOrEmpty(g.dCrudeOther)? g.dCrudeName: g.dCrudeOther,
                                                        dOrigin = g.dOrigin,
                                                        dFeedStock = string.IsNullOrEmpty(g.dFeedStockOther) ? g.dFeedStock : g.dFeedStockOther,
                                                        dFor = g.dFor,
                                                        dSupplier = g.dSupplier,
                                                        dContactPerson = g.dContactPerson,
                                                        dQuantity = g.dQuantity,
                                                        dIncoterms = g.dIncoterms,
                                                        dOffer = offer,
                                                        dMarketSource = g.dMarketSource,
                                                        dLatestLP = g.dLatestLP,
                                                        dBechmarkPrice = g.dBechmarkPrice,
                                                        dRankRound = g.dRankRound,
                                                        dMarginLP = g.dMarginLP,
                                                        dMargin = Margin
                                                    });
                        }

                        var queryFilter = from p in pModel.sSearchData
                                          select p;

                        if (!string.IsNullOrEmpty(sFeedStock))
                            queryFilter = queryFilter.Where(p => p.dFeedStock.ToUpper().Contains(sFeedStock.Trim()));

                        if (!string.IsNullOrEmpty(sCrudeName))
                            queryFilter = queryFilter.Where(p => p.dCrudeName.ToUpper().Contains(sCrudeName.Trim()));

                        if (!string.IsNullOrEmpty(sSupplier))
                            queryFilter = queryFilter.Where(p => p.dSupplier.ToUpper().Contains(sSupplier.Trim()));
                        
                        if (queryFilter != null)
                        {
                            pModel.sSearchData = queryFilter.ToList();
                            pModel.dMarginTotal = dMarginTotal.ToString("#,##0.00");
                        }

                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue AddCrudeNote(CrudePurchaseViewModel vm, string pUser, string transId)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CDP_DATA_DAL dataDAL = new CDP_DATA_DAL();
                            CDP_DATA dataCHI = new CDP_DATA();
                            dataCHI.CDA_ROW_ID = transId;
                            dataCHI.CDA_UPDATED = DateTime.Now;
                            dataCHI.CDA_UPDATED_BY = pUser;
                            dataCHI.CDA_NOTE = vm.notes;
                            dataCHI.CDA_EXPLANATION = vm.explanation;
                            dataDAL.UpdateNote(dataCHI, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public string getLastRoundValue(string pOfferRowID)
        {
            string rtn = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var queryOffer = context.CDP_ROUND_ITEMS.Where(x => x.CRI_FK_OFFER_ITEMS.Equals(pOfferRowID)).OrderByDescending(x => x.CRI_ROUND_NO).First();

                if (queryOffer != null)
                {
                      rtn = queryOffer.CDP_ROUND.First().CRD_ROUND_VALUE;
                }
            }

            return rtn;
        }

        public ReturnValue GetCrudePurchasesWithBond_New(ref CrudePurchaseSearchBondViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //var context = new EntityCPAIEngine();
                    //var items = context.Database.SqlQuery<VW_CRUDE_TRANS>("SELECT * FROM CMDB.VW_CRUDE_TRANS_2").ToList<VW_CRUDE_TRANS>();
                    //var testitems = context.VW_CRUDE_TRANS.ToList();
                    //var items = context.VW_CRUDE_TRANS.ToList<VW_CRUDE_TRANS>().Distinct();
                    string[] lstCreate;
                    string  _pStatus, _pCrude, _pSupplier;
                     
                    _pStatus = String.IsNullOrEmpty(pModel.Search_statuses) ? string.Empty : pModel.Search_statuses.Trim().ToLower();
                    _pCrude = String.IsNullOrEmpty(pModel.Search_crudes) ? string.Empty : pModel.Search_crudes.Trim().ToLower();
                    _pSupplier = String.IsNullOrEmpty(pModel.Search_suppliers) ? string.Empty : pModel.Search_suppliers.Trim().ToLower();
                    lstCreate = String.IsNullOrEmpty(pModel.Search_createdByUser) ? lstCreate = new string[0] : pModel.Search_createdByUser.Trim().ToLower().Split(char.Parse("|"));
                    
                    var items = context.VW_CRUDE_TRANS.ToList().Distinct();
                    DateTime[] tDates = null;

                    if (!string.IsNullOrEmpty(pModel.Search_datePurchase))
                    {
                        string[] tDatesStr = pModel.Search_datePurchase.Split(' ');
                        tDates = new DateTime[] { DateTime.Parse(tDatesStr[0]), DateTime.Parse(tDatesStr[2]) };
                        items = items.Where(a => Convert.ToDateTime(a.PURCHASE_DATE) >= tDates[0].Date && Convert.ToDateTime(a.PURCHASE_DATE) <= tDates[1].Date).ToList();
                    }

                    if (!string.IsNullOrEmpty(_pStatus))
                    {
                        items = items.Where(a => a.STATUS.Equals(_pStatus.Trim())).ToList();
                    }

                    if (!string.IsNullOrEmpty(_pCrude))
                    {
                        items = items.Where(a => a.MET_NO.Equals(_pCrude)).ToList();
                    }

                    if (!string.IsNullOrEmpty(_pSupplier))
                    {
                        items = items.Where(a => a.VENDOR_CODE.Equals(_pSupplier)).ToList();
                    }

                    if (lstCreate.Length > 0)
                        items = items.Where(x => x.CREATED_BY != null && lstCreate.Contains(x.CREATED_BY.Trim().ToLower())).ToList();

                    //if (!string.IsNullOrEmpty(pUsers))
                    //{
                    //    List<string> tUsers = new JavaScriptSerializer().Deserialize<string[]>(pUsers).ToList<string>();
                    //    items = items.Where(a => tUsers.Contains(a.CREATED_BY)).ToList();
                    //}

                    //rslt.CrudePurchases = new List<CrudePurchaseViewModel_SearchBond>();
                    pModel.CrudePurchases = new List<CrudePurchaseViewModel_SearchBond>();
                    foreach (VW_CRUDE_TRANS cp in items)
                    {
                        CrudePurchaseViewModel_SearchBond crd = new CrudePurchaseViewModel_SearchBond();
                        crd.CrudePurchaseRowId = cp.CDA_ROW_ID;
                        crd.TripNo = cp.TRIP_NO;
                        crd.PurchaseDate = cp.PURCHASE_DATE.ToString();
                        crd.Crude = cp.MAT_NAME;
                        crd.SupplierName = cp.VENDOR_NAME;
                        crd.Quantity = cp.QUANTITY;
                        crd.Margin = cp.MARGIN;
                        crd.CreatedBy = cp.CREATED_BY;
                        crd.BlDate = cp.BL_DATE.ToString();
                        crd.Status = cp.STATUS;
                        crd.Bond = cp.CDA_PERFORMANCE_BOND;
                        if (!string.IsNullOrEmpty(cp.FILE_UPLOAD))
                        {
                            crd.Bond_fileUpload = cp.FILE_UPLOAD;
                            string[] tFiles = cp.FILE_UPLOAD.Split('|');
                            crd.BondDocuments = new List<CrudePurchaseViewModel_SearchBond.PerformanceBondDocument>();
                            foreach (var tFile in tFiles)
                            {
                                crd.BondDocuments.Add(new CrudePurchaseViewModel_SearchBond.PerformanceBondDocument() { FileName = tFile, FilePath = tFile });
                            }
                        }
                        pModel.CrudePurchases.Add(crd);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return rtn;
        }


        public CrudePurchaseSearchBondViewModel GetCrudePurchasesWithBond(string pDates, string pStatus, string pCrude, string pSupplier, string pUsers)
        {
            CrudePurchaseSearchBondViewModel rslt = new CrudePurchaseSearchBondViewModel();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //var context = new EntityCPAIEngine();
                    //var items = context.Database.SqlQuery<VW_CRUDE_TRANS>("SELECT * FROM CMDB.VW_CRUDE_TRANS_2").ToList<VW_CRUDE_TRANS>();
                    //var testitems = context.VW_CRUDE_TRANS.ToList();
                    //var items = context.VW_CRUDE_TRANS.ToList<VW_CRUDE_TRANS>().Distinct();
                    var items = context.VW_CRUDE_TRANS.ToList().Distinct();
                    DateTime[] tDates = null;
                    if (!string.IsNullOrEmpty(pDates))
                    {
                        string[] tDatesStr = pDates.Split(' ');
                        tDates = new DateTime[] { DateTime.Parse(tDatesStr[0]), DateTime.Parse(tDatesStr[2]) };
                        items = items.Where(a => Convert.ToDateTime(a.PURCHASE_DATE) >= tDates[0].Date && Convert.ToDateTime(a.PURCHASE_DATE) <= tDates[1].Date).ToList();
                    }

                    if (!string.IsNullOrEmpty(pStatus))
                    {
                        items = items.Where(a => a.STATUS.ToUpper() == pStatus.ToUpper()).ToList();
                    }

                    if (!string.IsNullOrEmpty(pCrude))
                    {
                        items = items.Where(a => a.MET_NO.Equals(pCrude)).ToList();
                    }

                    if (!string.IsNullOrEmpty(pSupplier))
                    {
                        items = items.Where(a => a.VENDOR_CODE.Equals(pSupplier)).ToList();
                    }

                    if (!string.IsNullOrEmpty(pUsers))
                    {
                        List<string> tUsers = new JavaScriptSerializer().Deserialize<string[]>(pUsers).ToList<string>();
                        items = items.Where(a => tUsers.Contains(a.CREATED_BY)).ToList();
                    }

                    rslt.CrudePurchases = new List<CrudePurchaseViewModel_SearchBond>();
                    foreach (VW_CRUDE_TRANS cp in items)
                    {
                        CrudePurchaseViewModel_SearchBond crd = new CrudePurchaseViewModel_SearchBond();
                        crd.CrudePurchaseRowId = cp.CDA_ROW_ID;
                        crd.TripNo = cp.TRIP_NO;
                        crd.PurchaseDate = cp.PURCHASE_DATE.ToString();
                        crd.Crude = cp.MAT_NAME;
                        crd.SupplierName = cp.VENDOR_NAME;
                        crd.Quantity = cp.QUANTITY;
                        crd.Margin = cp.MARGIN;
                        crd.CreatedBy = cp.CREATED_BY;
                        crd.BlDate = cp.BL_DATE.ToString();
                        crd.Status = cp.STATUS;
                        crd.Bond = cp.CDA_PERFORMANCE_BOND;
                        if (!string.IsNullOrEmpty(cp.FILE_UPLOAD))
                        {
                            string[] tFiles = cp.FILE_UPLOAD.Split('|');
                            crd.BondDocuments = new List<CrudePurchaseViewModel_SearchBond.PerformanceBondDocument>();
                            foreach (var tFile in tFiles)
                            {
                                crd.BondDocuments.Add(new CrudePurchaseViewModel_SearchBond.PerformanceBondDocument() { FileName = tFile, FilePath = tFile });
                            }
                        }
                        rslt.CrudePurchases.Add(crd);
                    }
                }
            }
            catch(Exception ex)
            {
                 
            }
               
            return rslt;
        }

        public string [] GetCrudePurchasesBondDocFilesPath(string pCrudePurchaseRowId,ref string pTripNo)
        {
            string[] rslt = new string[] { };
            var context = new EntityCPAIEngine();
            var cp = context.VW_CRUDE_TRANS.Where(a=>a.CDA_ROW_ID.Equals(pCrudePurchaseRowId)).ToList<VW_CRUDE_TRANS>();
            if (cp[0].FILE_UPLOAD != null) {
                rslt = cp[0].FILE_UPLOAD.Split('|');
            }
            pTripNo = cp[0].TRIP_NO;
            return rslt;
        }

        //class VW_CRUDE_TRANS {
        //    public string CDA_ROW_ID { get; set; }
        //    public string TRIP_NO { get; set; }
        //    public Nullable<DateTime> PURCHASE_DATE { get; set; }
        //    public string MET_NO { get; set; }
        //    public string MAT_NAME { get; set; }
        //    public string VENDOR_NAME { get; set; }
        //    public string QUANTITY { get; set; }
        //    public string MARGIN { get; set; }
        //    public string CREATED_BY { get; set; }
        //    public Nullable<DateTime> BL_DATE { get; set; }
        //    public string STATUS { get; set; }
        //}

        //for PIT
        public CrudePurchaseReportViewModel_Search_PIT getDataCrudePurchase(string loadingPeriodFrom, string loadingPeriodTo, string dischargingPeriodFrom, string dischargingPeriodTo, string crude, string supplier, string incoterm)
        {
            CrudePurchaseReportViewModel_Search_PIT pModel = new CrudePurchaseReportViewModel_Search_PIT();
            pModel.sSearchData = new List<CrudePurchaseReportViewModel_SearchData_PIT>();
            pModel.Status = false;
            pModel.Message = "System Error";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sLoadingPeriodDateFrom = String.IsNullOrEmpty(loadingPeriodFrom) ? "" : loadingPeriodFrom.ToUpper().Substring(0, 10);
                    var sLoadingPeriodDateTo = String.IsNullOrEmpty(loadingPeriodTo) ? "" : loadingPeriodTo.ToUpper().Substring(0, 10);

                    var sDischargingPeriodDateFrom = String.IsNullOrEmpty(dischargingPeriodFrom) ? "" : dischargingPeriodFrom.ToUpper().Substring(0, 10);
                    var sDischargingPeriodDateTo = String.IsNullOrEmpty(dischargingPeriodTo) ? "" : dischargingPeriodTo.ToUpper().Substring(0, 10);

                    var sIncoterm = String.IsNullOrEmpty(incoterm) ? "" : incoterm.ToUpper();
                    var sCrudeName = String.IsNullOrEmpty(crude) ? "" : crude.ToUpper();
                    var sSupplier = String.IsNullOrEmpty(supplier) ? "" : supplier.ToUpper();

                 

                    var query = (from d in context.CDP_DATA
                                 join o in context.CDP_OFFER_ITEMS on d.CDA_ROW_ID equals o.COI_FK_CDP_DATA into viewOffer
                                 join m in context.MT_MATERIALS on d.CDA_FK_MATERIALS equals m.MET_NUM into viewMat
                                 from lvo in viewOffer.DefaultIfEmpty()
                                 from lmt in viewMat.DefaultIfEmpty()
                                 where lvo.COI_FINAL_FLAG.Equals("Y") && ( !d.CDA_TERMS.ToUpper().Equals("SPOT") || (d.CDA_TERMS.ToUpper().Equals("SPOT") && !context.PCF_HEADER.Any(w => w.PHE_CDA_ROW_ID == d.CDA_ROW_ID)))
                                 && ( d.CDA_STATUS.Equals("APPROVED") || d.CDA_STATUS.Equals("WAITING BOND DOC") || d.CDA_STATUS.Equals("WAIVED") || d.CDA_STATUS.Equals("COMPLETED"))
                                 select new
                                 {
                                     dRowId = d.CDA_ROW_ID
                                     ,
                                     dLoadingFrom = (DateTime?)d.CDA_LOADING_DATE_FROM
                                     ,
                                     dLoadingTo = (DateTime?)d.CDA_LOADING_DATE_TO
                                     ,
                                     dDischargingFrom = (DateTime?)d.CDA_DISCHARGING_DATE_FROM
                                     ,
                                     dDischargingTo = (DateTime?)d.CDA_DISCHARGING_DATE_TO
                                     ,
                                     dContractFrom = (DateTime?)d.CDA_CONTRACT_DATE_FROM
                                     ,
                                     dContractTo = (DateTime?)d.CDA_CONTRACT_DATE_TO
                                     ,
                                     dCrudeId = d.CDA_FK_MATERIALS
                                     ,
                                     dCrudeName = d.MT_MATERIALS.MET_MAT_DES_ENGLISH
                                     ,
                                     dCrudeOther = d.CDA_MATERIALS_OTHERS
                                     ,
                                     dTerms = d.CDA_TERMS
                                     ,
                                     dTermsOthers = d.CDA_TERMS_OTHERS
                                     ,
                                     dOrigin = d.CDA_ORIGIN
                                     ,
                                     dFeedStock = d.CDA_FEED_STOCK
                                     ,
                                     dFeedStockOther = d.CDA_FEED_STOCK_OTHERS
                                     ,
                                     dFor = d.CDA_FOR_FEED_STOCK
                                     ,
                                     dGtc = d.CDA_GTC
                                     ,
                                     dPaymentTerms = d.CDA_PAYMENT_TERMS
                                     ,
                                     dPaymentTermsDetail = d.CDA_PAYMENT_TERMS_DETAIL
                                     ,
                                     dPaymentTermsOthers = d.CDA_PAYMENT_TERMS_OTHERS
                                     ,
                                     dSubPaymentTerms = d.CDA_SUB_PAYMENT_TERMS
                                     ,
                                     dPricingPeriodFrom = (DateTime?)d.CDA_PRICING_PERIOD_FROM
                                     ,
                                     dPricingPeriodTo = (DateTime?)d.CDA_PRICING_PERIOD_TO
                                     ,
                                     dTolerance = d.CDA_TOLERANCE
                                     ,
                                     dToleranceType = d.CDA_TOLERANCE_TYPE
                                     ,
                                     dToleranceOption = d.CDA_TOLERANCE_OPTION
                                     ,
                                     dSupplierId = lvo.MT_VENDOR.VND_ACC_NUM_VENDOR
                                     ,
                                     dSupplier = lvo.MT_VENDOR.VND_NAME1
                                     ,
                                     dContactPerson = lvo.COI_CONTACT_PERSON
                                     ,
                                     dQuantity = lvo.COI_QUANTITY
                                     , 
                                     dQuantityUnit = lvo.COI_QUANTITY_UNIT
                                     ,
                                     dIncoterms = lvo.COI_INCOTERMS
                                     ,
                                     dOfferRowID = lvo.COI_ROW_ID
                                     ,
                                     dMarketSource = lvo.COI_MARKET_SOURCE
                                     ,
                                     dLatestLP = lvo.COI_LATEST_LP_PLAN_PRICE
                                     ,
                                     dBechmarkPrice = lvo.COI_BECHMARK_PRICE
                                     ,
                                     dRankRound = lvo.COI_RANK_ROUND
                                     ,
                                     dMarginLP = lvo.COI_MARGIN_LP
                                     ,
                                     dPurchaseNo = d.CDA_PURCHASE_NO
                                     ,
                                     dDensity = lmt.MET_PRODUCT_DENSITY,
                                     dPerBond = d.CDA_PERFORMANCE_BOND,
                                 });



                    //if (query != null) {
                    //    query = query.Where(c => !c.dRowId.Equals( query2.Any()));
                    //}

                    if (!string.IsNullOrEmpty(sLoadingPeriodDateFrom) && !string.IsNullOrEmpty(sLoadingPeriodDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sLoadingPeriodDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sLoadingPeriodDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => (p.dLoadingFrom >= sDate && p.dLoadingFrom <= eDate) && (p.dLoadingTo >= sDate && p.dLoadingTo <= eDate));
                    }

                    if (!string.IsNullOrEmpty(sDischargingPeriodDateFrom) && !string.IsNullOrEmpty(sDischargingPeriodDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDischargingPeriodDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDischargingPeriodDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => (p.dDischargingFrom >= sDate && p.dDischargingFrom <= eDate) && (p.dDischargingTo >= sDate && p.dDischargingTo <= eDate));
                    }

                    if (query != null)
                    {
                        decimal dMarginTotal = 0;
                        foreach (var g in query)
                        {
                            decimal quantity = string.IsNullOrEmpty(g.dQuantity) ? 0 : Convert.ToDecimal(g.dQuantity);
                            decimal MarginLP = string.IsNullOrEmpty(g.dMarginLP) ? 0 : Convert.ToDecimal(g.dMarginLP);
                            decimal dMargin = ((quantity * MarginLP) * 1000);
                            string Margin = dMargin.ToString("#,##0");
                            string offer = getLastRoundValue(g.dOfferRowID);

                            dMarginTotal += dMargin;


                            pModel.sSearchData.Add(
                                                    new CrudePurchaseReportViewModel_SearchData_PIT
                                                    {
                                                        dLoadingPeriodDate = g.dLoadingFrom == null && g.dLoadingTo == null ? "" : Convert.ToDateTime(g.dLoadingFrom).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + " to " + Convert.ToDateTime(g.dLoadingTo).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dDischargingPeriodDate = g.dDischargingFrom == null && g.dDischargingTo == null ? "" : Convert.ToDateTime(g.dDischargingFrom).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + " to " + Convert.ToDateTime(g.dDischargingTo).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dContractDate = g.dContractFrom == null && g.dContractTo == null ? "" : Convert.ToDateTime(g.dContractFrom).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + " to " + Convert.ToDateTime(g.dContractTo).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dPricingPeriodDate = g.dPricingPeriodFrom == null && g.dPricingPeriodTo == null ? "" : Convert.ToDateTime(g.dPricingPeriodFrom).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + " to " + Convert.ToDateTime(g.dPricingPeriodTo).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dCrudeId = g.dCrudeId,
                                                        dCrudeName = string.IsNullOrEmpty(g.dCrudeOther) ? g.dCrudeName : g.dCrudeOther,
                                                        dOrigin = g.dOrigin,
                                                        dFeedStock = string.IsNullOrEmpty(g.dFeedStockOther) ? g.dFeedStock : g.dFeedStockOther,
                                                        dFor = g.dFor,
                                                        dSupplier = g.dSupplier,
                                                        dContactPerson = g.dContactPerson,
                                                        dTerms = g.dTerms,
                                                        dTermsOthers = g.dTermsOthers,
                                                        dGtc = g.dGtc,
                                                        dPaymentTerms = g.dPaymentTerms,
                                                        dSubPaymentTerms = g.dSubPaymentTerms,
                                                        dPaymentTermsDetail = g.dPaymentTermsDetail,
                                                        dPaymentTermsOthers = g.dPaymentTermsOthers,
                                                        dTolerance = g.dTolerance,
                                                        dToleranceType = g.dToleranceType,
                                                        dToleranceOption = g.dToleranceOption,
                                                        dQuantity = g.dQuantity,
                                                        dIncoterms = g.dIncoterms,
                                                        dOffer = offer,
                                                        dMarketSource = g.dMarketSource,
                                                        dLatestLP = g.dLatestLP,
                                                        dBechmarkPrice = g.dBechmarkPrice,
                                                        dRankRound = g.dRankRound,
                                                        dMarginLP = g.dMarginLP,
                                                        dMargin = Margin,
                                                        dSupplierId = g.dSupplierId,
                                                        dRowId = g.dRowId,
                                                        dPurchaseNo = g.dPurchaseNo,
                                                        dQuantityUnit = g.dQuantityUnit,
                                                        dDensity = g.dDensity,
                                                        dPerBond = g.dPerBond ?? "",
                                                    });
                        }

                        var queryFilter = from p in pModel.sSearchData
                                          select p;

                        if (!string.IsNullOrEmpty(sIncoterm))
                            queryFilter = queryFilter.Where(p => p.dIncoterms.ToUpper().Contains(sIncoterm.Trim()));

                        if (!string.IsNullOrEmpty(sCrudeName))
                            queryFilter = queryFilter.Where(p => p.dCrudeName.ToUpper().Contains(sCrudeName.Trim()));

                        if (!string.IsNullOrEmpty(sSupplier))
                            queryFilter = queryFilter.Where(p => p.dSupplier.ToUpper().Contains(sSupplier.Trim()));

                        if (queryFilter != null)
                        {
                            pModel.sSearchData = queryFilter.ToList();
                        }

                        pModel.Status = true;
                        pModel.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        pModel.Status = false;
                        pModel.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                pModel.Message = ex.Message;
                pModel.Status = false;
            }
            return pModel;
        }


    }



}