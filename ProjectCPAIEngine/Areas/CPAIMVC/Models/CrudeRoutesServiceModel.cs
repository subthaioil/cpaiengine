﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Globalization;
using OfficeOpenXml;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CrudeRoutesServiceModel
    {
        public ReturnValue Add(CrudeRoutesViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Date))
                {
                    rtn.Message = "Date should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (ShareFn.IsValidDateFormat(pModel.Date) == false)
                {
                    rtn.Message = "Date is not valid format";
                    rtn.Status = false;
                    return rtn;
                }
                else if (CheckHaveData(pModel.Date) == true)
                {
                    rtn.Message = "Data already in the database.";
                    rtn.Status = false;
                    return rtn;
                }
                else if (HolidayServiceModel.CheckHoliday(pModel.Date, "UK") == true)
                {
                    rtn.Message = "Date is holiday";
                    rtn.Status = false;
                    return rtn;
                }
              



                //if (string.IsNullOrEmpty(pModel.TD2) && string.IsNullOrEmpty(pModel.TD3) && string.IsNullOrEmpty(pModel.TD15))
                //{
                //    rtn.Message = "Pleaes input a TD value";
                //    rtn.Status = false;
                //    return rtn;
                //}

                if (string.IsNullOrEmpty(pModel.TD2) == false && ShareFn.IsValidDecimalFormat(pModel.TD2) == false)
                {
                    rtn.Message = "TD2 is valid number format";
                    rtn.Status = false;
                    return rtn;
                }
               
               if (string.IsNullOrEmpty(pModel.TD3) == false && ShareFn.IsValidDecimalFormat(pModel.TD3) == false)
                {
                    rtn.Message = "TD3 is valid number format";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pModel.TD15) == false && ShareFn.IsValidDecimalFormat(pModel.TD15) == false)
                {
                    rtn.Message = "TD15 is valid number format";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CRUDE_ROUTES_DAL dal = new MT_CRUDE_ROUTES_DAL();
                MT_CRUDE_ROUTES ent = new MT_CRUDE_ROUTES();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    ShareFn _FN = new ShareFn();
                    
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                          
                            try
                            {

                                DateTime? d = _FN.ConvertDateFormatBackFormat(pModel.Date);
                                ent.MCR_DATE = d == null ? DateTime.Now : Convert.ToDateTime(d);
                                ent.MCR_TD2 = String.IsNullOrEmpty(pModel.TD2) ? "NA" : pModel.TD2;
                                ent.MCR_TD3 = String.IsNullOrEmpty(pModel.TD3) ? "NA" : pModel.TD3;
                                ent.MCR_TD15 = String.IsNullOrEmpty(pModel.TD15) ? "NA" : pModel.TD15;
                                ent.MCR_STATUS = pModel.Status;
                                ent.MCR_CREATED_BY = pUser;
                                ent.MCR_CREATED_DATE = now;
                                ent.MCR_UPDATED_BY = pUser;
                                ent.MCR_UPDATED_DATE = now;
                            

                                dal.Save(ent, context);

                                dbContextTransaction.Commit();
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                rtn.Message = ex.Message;
                                rtn.Status = false;
                            }
                        }


                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(CrudeRoutesViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Date))
                {
                    rtn.Message = "Date should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (ShareFn.IsValidDateFormat(pModel.Date) == false)
                {
                    rtn.Message = "Date is valid date format";
                    rtn.Status = false;
                    return rtn;
                }
                else if (HolidayServiceModel.CheckHoliday(pModel.Date, "UK") == true)
                {
                    rtn.Message = "Date is not holiday";
                    rtn.Status = false;
                    return rtn;
                }
                else if (CheckHaveData(pModel.Date) == false)
                {
                    rtn.Message = "Can not be edit the date "+ pModel.Date;
                    rtn.Status = false;
                    return rtn;
                }
                

                //if (string.IsNullOrEmpty(pModel.TD2) && string.IsNullOrEmpty(pModel.TD3) && string.IsNullOrEmpty(pModel.TD15))
                //{
                //    rtn.Message = "Pleaes input a TD value";
                //    rtn.Status = false;
                //    return rtn;
                //}

                if (string.IsNullOrEmpty(pModel.TD2) == false && ShareFn.IsValidDecimalFormat(pModel.TD2) == false)
                {
                    rtn.Message = "TD2 is valid number format";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pModel.TD3) == false && ShareFn.IsValidDecimalFormat(pModel.TD3) == false)
                {
                    rtn.Message = "TD3 is valid number format";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pModel.TD15) == false && ShareFn.IsValidDecimalFormat(pModel.TD15) == false)
                {
                    rtn.Message = "TD15 is valid number format";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CRUDE_ROUTES_DAL dal = new MT_CRUDE_ROUTES_DAL();
                MT_CRUDE_ROUTES ent = new MT_CRUDE_ROUTES();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.MCR_DATE = ShareFn.ConvertStrDateToDate(pModel.Date);           // Key
                            ent.MCR_TD2 = String.IsNullOrEmpty(pModel.TD2) ? "NA" : pModel.TD2;
                            ent.MCR_TD3 = String.IsNullOrEmpty(pModel.TD3) ? "NA" : pModel.TD3;
                            ent.MCR_TD15 = String.IsNullOrEmpty(pModel.TD15) ? "NA" : pModel.TD15;
                            ent.MCR_STATUS = pModel.Status;
                            ent.MCR_UPDATED_BY = pUser;
                            ent.MCR_UPDATED_DATE = now;

                            dal.Update(ent, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref CrudeRoutesViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sDateFromTo = String.IsNullOrEmpty(pModel.sDateFromTo) == true ? "" : pModel.sDateFromTo;
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) == true ? "" : pModel.sStatus;

                    var query = (from v in context.MT_CRUDE_ROUTES
                                 select v);
                   

                    if (!string.IsNullOrEmpty(sDateFromTo))
                    {
                        ShareFn _FN = new ShareFn();
                        var startDate = "";
                        var endDate = "";
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        if (sDateFromTo != "")
                        {
                            string[] date = sDateFromTo.Split(' ');
                            startDate = date[0];
                            sDate = ShareFn.ConvertStrDateToDate(startDate);
                            endDate = date[2];
                            eDate = ShareFn.ConvertStrDateToDate(endDate);
                        }
                        query = query.Where(q => q.MCR_DATE >= sDate && q.MCR_DATE <= eDate);

                    }

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.MCR_STATUS.ToUpper().Equals(sStatus));



                    if (query != null)
                    {
                        pModel.sSearchData = new List<CrudeRoutesViewModel_SeachData>();
                        foreach (var item in query)
                        {
                            pModel.sSearchData.Add(new CrudeRoutesViewModel_SeachData {
                                dDate = ShareFn.ConvertDateTimeToDateStringFormat(item.MCR_DATE, "dd/MM/yyyy")
                                         ,
                                dDateOrder = ShareFn.ConvertDateTimeToDateStringFormat(item.MCR_DATE, "yyyyMMdd")
                                ,
                                dTD2 = string.IsNullOrEmpty(item.MCR_TD2) ? "NA" : item.MCR_TD2
                                         ,
                                dTD3 = string.IsNullOrEmpty(item.MCR_TD3) ? "NA" : item.MCR_TD3
                                         ,
                                dTD15 = string.IsNullOrEmpty(item.MCR_TD15) ? "NA" : item.MCR_TD15
                                         ,
                                dStatus = item.MCR_STATUS
                            });
                        }

                        pModel.sSearchData = pModel.sSearchData.OrderBy(p => p.dDateOrder).ToList();


                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue ImportDataFromExcel(string FilePath, string worksheetName, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                MT_CRUDE_ROUTES_DAL dal = new MT_CRUDE_ROUTES_DAL();
                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    ShareFn _FN = new ShareFn();

                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {

                        try
                        {
                            if (!string.IsNullOrEmpty(FilePath) && !string.IsNullOrEmpty(worksheetName))
                            {
                                if (!CheckExcelFormat(FilePath, worksheetName))
                                {
                                    rtn.Message = "Invalid Template Data";
                                    rtn.Status = false;
                                    return rtn;
                                }

                                int start_row = 3;

                                ExcelWorksheet worksheetdata = getDataFromExcel(FilePath, worksheetName);

                                if (worksheetdata != null)
                                {
                                    string[] formats = { "d/M/yy", "dd/M/yy", "d/MM/yy", "dd/MM/yy", "d/MMM/yy", "dd/MMM/yy", "d/M/yyyy", "dd/M/yyyy", "d/MM/yyyy", "dd/MM/yyyy", "d/MMM/yyyy", "dd/MMM/yyyy",
                                                             "d-M-yy", "dd-M-yy", "d-MM-yy", "dd-MM-yy", "d-MMM-yy", "dd-MMM-yy", "d-M-yyyy", "dd-M-yyyy", "d-MM-yyyy", "dd-MM-yyyy", "d-MMM-yyyy", "dd-MMM-yyyy", };

                                    bool date_valid;
                                    DateTime date_value;

                                    do
                                    {
                                        string date_text = worksheetdata.Cells["A" + start_row].Text;
                                        date_valid = DateTime.TryParseExact(date_text, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date_value);
                                        string TD2 = worksheetdata.Cells["B" + start_row].Text;
                                        string TD3 = worksheetdata.Cells["D" + start_row].Text;
                                        string TD15 = worksheetdata.Cells["F" + start_row].Text;

                                        var query = (from a in context.MT_CRUDE_ROUTES
                                                     where a.MCR_DATE == date_value
                                                     select a).ToList();

                                        if (date_valid && !string.IsNullOrEmpty(TD2) && !string.IsNullOrEmpty(TD3) && !string.IsNullOrEmpty(TD15) && query.Count == 0)
                                        {
                                            MT_CRUDE_ROUTES ent = new MT_CRUDE_ROUTES();
                                            ent.MCR_DATE = date_value;
                                            ent.MCR_TD2 = string.IsNullOrEmpty(TD2) ? "NA" : TD2;
                                            ent.MCR_TD3 = string.IsNullOrEmpty(TD3) ? "NA" : TD3;
                                            ent.MCR_TD15 = string.IsNullOrEmpty(TD15) ? "NA" : TD15;
                                            ent.MCR_STATUS = "ACTIVE";
                                            ent.MCR_CREATED_BY = pUser;
                                            ent.MCR_CREATED_DATE = now;
                                            ent.MCR_UPDATED_BY = pUser;
                                            ent.MCR_UPDATED_DATE = now;
                                            dal.Save(ent, context);
                                        }
                                        else if(date_valid && !string.IsNullOrEmpty(TD2) && !string.IsNullOrEmpty(TD3) && !string.IsNullOrEmpty(TD15) && query.Count > 0)
                                        {
                                            MT_CRUDE_ROUTES ent = new MT_CRUDE_ROUTES();
                                            ent.MCR_DATE = date_value;
                                            ent.MCR_TD2 = string.IsNullOrEmpty(TD2) ? "NA" : TD2;
                                            ent.MCR_TD3 = string.IsNullOrEmpty(TD3) ? "NA" : TD3;
                                            ent.MCR_TD15 = string.IsNullOrEmpty(TD15) ? "NA" : TD15;
                                            ent.MCR_STATUS = "ACTIVE";
                                            ent.MCR_UPDATED_BY = pUser;
                                            ent.MCR_UPDATED_DATE = now;
                                            dal.Update(ent, context);
                                        }
                                        start_row++;
                                    } while (date_valid);
                                }
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public bool CheckExcelFormat(string FilePath, string worksheetName)
        {
            ExcelWorksheet worksheetdata = getDataFromExcel(FilePath, worksheetName);
            DateTime date_value;

            if (worksheetdata != null)
            {
                string[] formats = { "d/M/yy", "dd/M/yy", "d/MM/yy", "dd/MM/yy", "d/MMM/yy", "dd/MMM/yy", "d/M/yyyy", "dd/M/yyyy", "d/MM/yyyy", "dd/MM/yyyy", "d/MMM/yyyy", "dd/MMM/yyyy",
                                                                 "d-M-yy", "dd-M-yy", "d-MM-yy", "dd-MM-yy", "d-MMM-yy", "dd-MMM-yy", "d-M-yyyy", "dd-M-yyyy", "d-MM-yyyy", "dd-MM-yyyy", "d-MMM-yyyy", "dd-MMM-yyyy", };
                string date_text = worksheetdata.Cells["A3"].Text;
                string TD2 = worksheetdata.Cells["B3"].Text;
                string TD3 = worksheetdata.Cells["D3"].Text;
                string TD15 = worksheetdata.Cells["F3"].Text;

                double TD = 0.0;
                bool _TD2 = double.TryParse(TD2, out TD);
                bool _TD3 = double.TryParse(TD3, out TD);
                bool _TD15 = double.TryParse(TD15, out TD);

                return DateTime.TryParseExact(date_text, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date_value) && _TD2 && _TD3 && _TD15;

            }
            return false;
        }


        public CrudeRoutesViewModel_Detail Get(string pDateString)
        {
            CrudeRoutesViewModel_Detail model = new CrudeRoutesViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pDateString) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        DateTime date = ShareFn.ConvertStrDateToDate(pDateString);

                        var query = (from v in context.MT_CRUDE_ROUTES
                                     where v.MCR_DATE.Equals(date)
                                     select v);


                        if (query != null)
                        {
                            foreach(var item in query)
                            {
                                model.Date = Convert.ToDateTime(item.MCR_DATE).ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
                                model.TD2 = string.IsNullOrEmpty(item.MCR_TD2) || item.MCR_TD2 == "NA" ? "" : item.MCR_TD2;
                                model.TD3 = string.IsNullOrEmpty(item.MCR_TD3) || item.MCR_TD3 == "NA" ? "" : item.MCR_TD3;
                                model.TD15 = string.IsNullOrEmpty(item.MCR_TD15) || item.MCR_TD15 == "NA" ? "" : item.MCR_TD15;
                                model.Status = item.MCR_STATUS;
                                model.BoolStatus = item.MCR_STATUS == "ACTIVE" ? true : false;
                            }
                            return model;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static bool CheckHaveData(string pDateString)
        {
            if (string.IsNullOrEmpty(pDateString))
            {
                return false;
            }
            else if (ShareFn.IsValidDateFormat(pDateString) == false)
            {
                return false;
            }
            else
            {
                try
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        DateTime date = ShareFn.ConvertStrDateToDate(pDateString);
                        var _search = context.MT_CRUDE_ROUTES.Find(date);
                        return _search != null ? true : false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
               
            }
        }

        public static ExcelWorksheet getDataFromExcel(string excelPath, string worksheetName)
        {
            string file_path = !string.IsNullOrEmpty(excelPath) ? excelPath : "";
            if (!string.IsNullOrEmpty(file_path))
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file_path);
                try
                {
                    WebRequest request = WebRequest.Create(path);
                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            var excelPackage = new ExcelPackage(stream);
                            ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Where(i => i.Name.ToUpper().Contains(worksheetName)).FirstOrDefault();
                            return excelWorksheet;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        public static List<SelectListItem> getDataFromExcel(string excelPath)
        {
            var list = new List<SelectListItem>();
            string file_path = !string.IsNullOrEmpty(excelPath) ? excelPath : "";
            if (!string.IsNullOrEmpty(file_path))
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file_path);
                try
                {
                    WebRequest request = WebRequest.Create(path);
                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            var excelPackage = new ExcelPackage(stream);
                            //ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets;
                            foreach (var item in excelPackage.Workbook.Worksheets)
                            {
                                list.Add(new SelectListItem { Value = item.Name, Text = item.Name });
                            }
                            return list;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return list;
                }
            }
            return list;
        }
    }
}