﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using ProjectCPAIEngine.DAL.DALPCF;
using com.pttict.engine.utility;
using static ProjectCPAIEngine.Flow.F10000052.CPAICustomVatToSapMiroServiceState;
using System.IO;

using System.Text.RegularExpressions;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CustomVatServiceModel
    {
        public ReturnValue Search(ref CustomVatViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sTripNo = String.IsNullOrEmpty(pModel.sTripNo) ? "" : pModel.sTripNo;
                    var sVessel = String.IsNullOrEmpty(pModel.sVessel) ? "" : pModel.sVessel.ToUpper();
                    var sCrude = String.IsNullOrEmpty(pModel.sCrude) ? "" : pModel.sCrude.ToUpper();
                    var sSupplier = String.IsNullOrEmpty(pModel.sSupplier) ? "" : pModel.sSupplier.ToUpper();

                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public static string GetDataToJSON_TextValue(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Value, text = v.Text });

                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> getVendor(bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(PROJECT_NAME_SPACE, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), mt_vendor.Select(x => x.VND_NAME1).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "CPAI")
        {
            //List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterialAll().OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_NUM).ToList(), mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVehicle(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(PROJECT_NAME_SPACE, ACTIVE, type).OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_ID).ToList(), mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), isOptional, message);
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
                //else
                //{
                //    selectList.Add(new SelectListItem { Text = "", Value = "" });
                //}
            }
            //selectList.Add(new SelectListItem { Text = "SPOT", Value = "SPOT" });
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static List<CustomVatViewModel_SearchData> getCustomVatSearch(string BLDate, string DueDate, string TripNo, string Vessel, string Crude, string Supplier)
        {
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            List<CustomVatViewModel_SearchData> mCustomVatSearch = new List<CustomVatViewModel_SearchData>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<CustomVatViewModel_SearchData> mSearch = new List<CustomVatViewModel_SearchData>();

                var sBLDateFrom = String.IsNullOrEmpty(BLDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy") : BLDate.ToUpper().Substring(0, 10);
                var sBLDateTo = String.IsNullOrEmpty(BLDate) ? DateTime.Now.ToString("dd/MM/yyyy") : BLDate.ToUpper().Substring(14, 10);
                //var sBLDateFrom = String.IsNullOrEmpty(BLDate) ? "" : BLDate.ToUpper().Substring(0, 10);
                //var sBLDateTo = String.IsNullOrEmpty(BLDate) ? "" : BLDate.ToUpper().Substring(14, 10);
                DateTime bldateFrom = DateTime.ParseExact(sBLDateFrom, format, provider);
                DateTime bldateTo = DateTime.ParseExact(sBLDateTo, format, provider);

                var sDueDateFrom = String.IsNullOrEmpty(DueDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy") : DueDate.ToUpper().Substring(0, 10);
                var sDueDateTo = String.IsNullOrEmpty(DueDate) ? DateTime.Now.ToString("dd/MM/yyyy") : DueDate.ToUpper().Substring(14, 10);
                //var sDueDateFrom = String.IsNullOrEmpty(DueDate) ? "" : DueDate.ToUpper().Substring(0, 10);
                //var sDueDateTo = String.IsNullOrEmpty(DueDate) ? "" : DueDate.ToUpper().Substring(14, 10);
                DateTime duedateFrom = DateTime.ParseExact(sDueDateFrom, format, provider);
                DateTime duedateTo = DateTime.ParseExact(sDueDateTo, format, provider);

                //var query = (from cv in context.PCF_CUSTOMS_VAT.AsEnumerable()
                //             join h in context.PCF_HEADER on cv.TRIP_NO equals h.PHE_TRIP_NO
                //             join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO
                //             //join m in context.PCF_MATERIAL on new { cv.TRIP_NO ,cv.MAT_ITEM_NO} equals new { m.PMA_TRIP_NO, m.PMA_ITEM_NO}
                //             join ve in context.MT_VEHICLE on h.PHE_VESSEL equals ve.VEH_ID
                //             join s in context.MT_VENDOR on m.PMA_SUPPLIER equals s.VND_ACC_NUM_VENDOR
                //             join ma in context.MT_MATERIALS on m.PMA_MET_NUM equals ma.MET_NUM
                //             where ((m.PMA_BL_DATE >= bldateFrom && m.PMA_BL_DATE <= bldateTo) || (String.IsNullOrEmpty(BLDate)))
                //             && ((m.PMA_DUE_DATE >= duedateFrom && m.PMA_DUE_DATE <= duedateTo) || (String.IsNullOrEmpty(DueDate)))
                //             && (cv.TRIP_NO.Contains(TripNo) || String.IsNullOrEmpty(TripNo))
                //             && (ve.VEH_VEH_TEXT.Contains(Vessel) || String.IsNullOrEmpty(Vessel))
                //             && (ma.MET_MAT_DES_ENGLISH.Contains(Crude) || String.IsNullOrEmpty(Crude))
                //             && (s.VND_NAME1.Contains(Supplier) || String.IsNullOrEmpty(Supplier))
                //             && (cv.MAT_ITEM_NO == m.PMA_ITEM_NO)
                //             select new
                //             {
                //                 dTripNo = cv.TRIP_NO,
                //                 dVessel = ve.VEH_VEH_TEXT,
                //                 dCrude = ma.MET_MAT_DES_ENGLISH,
                //                 dSupplier = s.VND_NAME1,
                //                 dBLDate = m.PMA_BL_DATE,
                //                 dDueDate = m.PMA_DUE_DATE,
                //                 dQtyBBL = m.PMA_VOLUME_BBL,
                //                 dQtyMT = m.PMA_VOLUME_MT,
                //                 dQtyML = m.PMA_VOLUME_ML,
                //                 dIncoTerms = m.PMA_INCOTERMS,
                //                 dPONo = m.PMA_PO_NO,
                //                 dFIDocVAT = cv.SAP_FI_DOC,
                //                 dInvoiceDocMIRO = cv.SAP_INVOICE_DOC,
                //                 dFIDocMIRO = cv.SAP_FI_DOC_INVOICE,
                //                 dCustomPrice = cv.CUSTOMS_PRICE,
                //                 dFreightAmount = cv.FREIGHT_AMOUNT,
                //                 dMatItemNo = cv.MAT_ITEM_NO,
                //                 dMetNum = m.PMA_MET_NUM,
                //                 dVendorNo = s.VND_ACC_NUM_VENDOR,
                //                 dVesselNo = ve.VEH_VEHICLE,
                //                 dCompanyCode = h.PHE_COMPANY_CODE
                //             });

                var query = (from h in context.PCF_HEADER
                             join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO
                             join cv in context.PCF_CUSTOMS_VAT on new { f1 = m.PMA_TRIP_NO, f2 = m.PMA_ITEM_NO } equals new { f1 = cv.TRIP_NO, f2 = cv.MAT_ITEM_NO } into cv1
                             from cv in cv1.DefaultIfEmpty()
                             join ve in context.MT_VEHICLE on h.PHE_VESSEL equals ve.VEH_ID  //into ve1
                             //from ve in ve1.DefaultIfEmpty()
                             join s in context.MT_VENDOR on m.PMA_SUPPLIER equals s.VND_ACC_NUM_VENDOR //into s1
                             //from s in s1.DefaultIfEmpty()
                             join ma in context.MT_MATERIALS on m.PMA_MET_NUM equals ma.MET_NUM //into ma1
                             //from ma in ma1.DefaultIfEmpty()                                                          
                             where ((m.PMA_BL_DATE >= bldateFrom && m.PMA_BL_DATE <= bldateTo) || (String.IsNullOrEmpty(BLDate)))
                             && ((m.PMA_DUE_DATE >= duedateFrom && m.PMA_DUE_DATE <= duedateTo) || (String.IsNullOrEmpty(DueDate)))
                             && (cv.TRIP_NO.ToUpper().Contains(TripNo.ToUpper()) || String.IsNullOrEmpty(TripNo))
                             && (ve.VEH_VEH_TEXT.ToUpper().Contains(Vessel.ToUpper()) || String.IsNullOrEmpty(Vessel))
                             && (ma.MET_MAT_DES_ENGLISH.ToUpper().Contains(Crude.ToUpper()) || String.IsNullOrEmpty(Crude))
                             && (s.VND_NAME1.ToUpper().Contains(Supplier.ToUpper()) || String.IsNullOrEmpty(Supplier))
                             orderby h.PHE_TRIP_NO, cv.MAT_ITEM_NO
                             //&& (m.PMA_ITEM_NO == cv.MAT_ITEM_NO)                                                         
                             select new
                             {
                                 dTripNo = h.PHE_TRIP_NO,
                                 dVessel = ve.VEH_VEH_TEXT,
                                 dCrude = ma.MET_MAT_DES_ENGLISH,
                                 dSupplier = s.VND_NAME1,
                                 dBLDate = m.PMA_BL_DATE,
                                 dDueDate = m.PMA_DUE_DATE,
                                 dQtyBBL = m.PMA_VOLUME_BBL,
                                 dQtyMT = m.PMA_VOLUME_MT,
                                 dQtyML = m.PMA_VOLUME_ML,
                                 dIncoTerms = m.PMA_INCOTERMS,
                                 dPONo = m.PMA_PO_NO,
                                 dFIDocVAT = cv.SAP_FI_DOC,
                                 dInvoiceDocMIRO = cv.SAP_FI_DOC_INVOICE,
                                 dFIDocMIRO = cv.SAP_INVOICE_DOC,
                                 dCustomPrice = cv.CUSTOMS_PRICE,
                                 dFreightAmount = cv.FREIGHT_AMOUNT,
                                 dMatItemNo = m.PMA_ITEM_NO,
                                 dMetNum = m.PMA_MET_NUM,
                                 dVendorNo = s.VND_ACC_NUM_VENDOR,
                                 dVesselNo = ve.VEH_VEHICLE,
                                 dCompanyCode = h.PHE_COMPANY_CODE,
                                 dActual_from = h.PHE_ACTUAL_ETA_FROM,
                                 dActual_to = h.PHE_ACTUAL_ETA_TO
                             });

                if (query == null)
                    return mCustomVatSearch;

                if (query != null)
                {
                    query = query.Where(x => (x.dBLDate >= bldateFrom && x.dBLDate <= bldateTo || (String.IsNullOrEmpty(BLDate))) && (x.dDueDate >= duedateFrom && x.dDueDate <= duedateTo || (String.IsNullOrEmpty(DueDate))));
                    foreach (var item in query)
                    {
                        mCustomVatSearch.Add(new CustomVatViewModel_SearchData
                        {
                            dTripNo = item.dTripNo,
                            dVessel = String.IsNullOrEmpty(item.dVessel) ? "" : item.dVessel,
                            dCrude = String.IsNullOrEmpty(item.dCrude) ? "" : item.dCrude,
                            dSupplier = String.IsNullOrEmpty(item.dSupplier) ? "" : item.dSupplier,
                            dBLDate = String.IsNullOrEmpty(item.dBLDate.ToString()) ? "" : Convert.ToDateTime(item.dBLDate.ToString()).ToString(format, provider),
                            dDueDate = String.IsNullOrEmpty(item.dDueDate.ToString()) ? "" : Convert.ToDateTime(item.dDueDate.ToString()).ToString(format, provider),
                            dQtyBBL = String.IsNullOrEmpty(item.dQtyBBL.ToString()) ? "0" : Double.Parse(item.dQtyBBL.ToString()).ToString("#,##0.0000"),
                            dQtyMT = String.IsNullOrEmpty(item.dQtyMT.ToString()) ? "0" : Double.Parse(item.dQtyMT.ToString()).ToString("#,##0.0000"),
                            dQtyML = String.IsNullOrEmpty(item.dQtyML.ToString()) ? "0" : Double.Parse(item.dQtyML.ToString()).ToString("#,##0.0000"),
                            dIncoTerms = String.IsNullOrEmpty(item.dIncoTerms) ? "" : item.dIncoTerms,
                            dPONo = String.IsNullOrEmpty(item.dPONo) ? "" : item.dPONo,
                            dFIDocVAT = String.IsNullOrEmpty(item.dFIDocVAT) ? "" : item.dFIDocVAT,
                            dInvoiceDocMIRO = String.IsNullOrEmpty(item.dInvoiceDocMIRO) ? "" : item.dInvoiceDocMIRO,
                            dFIDocMIRO = String.IsNullOrEmpty(item.dFIDocMIRO) ? "" : item.dFIDocMIRO,
                            dCustomPrice = String.IsNullOrEmpty(item.dCustomPrice.ToString()) ? "0" : Double.Parse(item.dCustomPrice.ToString()).ToString("#,##0.0000"),
                            dFreightAmount = String.IsNullOrEmpty(item.dFreightAmount.ToString()) ? "0" : Double.Parse(item.dFreightAmount.ToString()).ToString("#,##0.0000"),
                            dMatItemNo = String.IsNullOrEmpty(item.dMatItemNo.ToString()) ? "" : item.dMatItemNo.ToString(),
                            dMetNum = String.IsNullOrEmpty(item.dMetNum) ? "" : item.dMetNum,
                            dVendorNo = String.IsNullOrEmpty(item.dVendorNo) ? "" : item.dVendorNo,
                            dVesselNo = String.IsNullOrEmpty(item.dVesselNo) ? "" : item.dVesselNo,
                            dCompanyCode = String.IsNullOrEmpty(item.dCompanyCode) ? "" : item.dCompanyCode,
                            dActual_from = String.IsNullOrEmpty(item.dActual_from.ToString()) ? "" : Convert.ToDateTime(item.dActual_from.ToString()).ToString(format, provider),
                            dActual_to = String.IsNullOrEmpty(item.dActual_to.ToString()) ? "" : Convert.ToDateTime(item.dActual_to.ToString()).ToString(format, provider)
                        });
                    }
                }

                ////Data for test
                //mSearch.Add(new CustomVatViewModel_SearchData
                //{
                //    dTripNo = "CF11170115",
                //    dVessel = "TATEYAMA",
                //    dCrude = "ARAB EXTRA LIGHT",
                //    dSupplier = "PTT Public Company Limited",
                //    dBLDate = "20/02/2017",
                //    dDueDate = "21/03/2017",
                //    dQtyBBL = Double.Parse("801004").ToString("#,##0.0000"),
                //    dQtyMT = Double.Parse("105130.05").ToString("#,##0.0000"),
                //    dQtyML = "10000",
                //    dIncoTerms = "FOB",
                //    dPONo = "4111000183",
                //    dFIDocVAT = "",
                //    dInvoiceDocMIRO = "",
                //    dFIDocMIRO = "",
                //    dCustomPrice = Double.Parse("32.925").ToString("#,##0.0000"),
                //    dFreightAmount = Double.Parse("684334.45").ToString("#,##0.0000"),
                //    dMatItemNo = "10",
                //    dMetNum = "YBGIMP",
                //    dVendorNo = "CPAI1000002",
                //    dVesselNo = "TATEYAMA",
                //    dCompanyCode = "1100"
                //});
                //mSearch.Add(new CustomVatViewModel_SearchData
                //{
                //    dTripNo = "CF11170115",
                //    dVessel = "TATEYAMA",
                //    dCrude = "ARAB LIGHT",
                //    dSupplier = "PTT Public Company Limited",
                //    dBLDate = "20/02/2017",
                //    dDueDate = "21/03/2017",
                //    dQtyBBL = Double.Parse("689710").ToString("#,##0.0000"),
                //    dQtyMT = Double.Parse("93930.45").ToString("#,##0.0000"),
                //    dQtyML = "10000",
                //    dIncoTerms = "CFR",
                //    dPONo = "4111000841",
                //    dFIDocVAT = "",
                //    dInvoiceDocMIRO = "",
                //    dFIDocMIRO = "",
                //    dCustomPrice = Double.Parse("30.425").ToString("#,##0.0000"),
                //    dFreightAmount = Double.Parse("611431.67").ToString("#,##0.0000"),
                //    dMatItemNo = "40",
                //    dMetNum = "YGBASE",
                //    dVendorNo = "CPAI1000002",
                //    dVesselNo = "TATEYAMA",
                //    dCompanyCode = "1100"
                //});
                //mSearch.Add(new CustomVatViewModel_SearchData
                //{
                //    dTripNo = "CF11170101",
                //    dVessel = "BRITISH INTREGITY",
                //    dCrude = "MURBAN",
                //    dSupplier = "Sapthip Co.,Ltd.",
                //    dBLDate = "09/04/2017",
                //    dDueDate = "09/05/2017",
                //    dQtyBBL = Double.Parse("999865").ToString("#,##0.0000"),
                //    dQtyMT = Double.Parse("130462").ToString("#,##0.0000"),
                //    dQtyML = "10000",
                //    dIncoTerms = "FOB",
                //    dPONo = "",  //4111001014
                //    dFIDocVAT = "",
                //    dInvoiceDocMIRO = "",
                //    dFIDocMIRO = "",
                //    dCustomPrice = Double.Parse("35.097").ToString("#,##0.0000"),
                //    dFreightAmount = Double.Parse("93227.75").ToString("#,##0.0000"),
                //    dMatItemNo = "10",
                //    dMetNum = "YGBASE",
                //    dVendorNo = "4000010",
                //    dVesselNo = "B. INTEG",
                //    dCompanyCode = "1100"
                //});

                //mCustomVatSearch = mSearch;
            }

            return mCustomVatSearch;
        }

        public static string getLastedInsuranceRate()
        {
            string sInsuranceRate = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var qry = (from cv in context.PCF_CUSTOMS_VAT select cv);

                if (qry != null)
                {
                    if (qry.Count() > 0)
                    {
                        var tmp = qry.ToList().LastOrDefault();
                        sInsuranceRate = tmp.INSURANCE_RATE.ToString();

                    }
                }
            }

            return sInsuranceRate;
        }

        public static string getROE()
        {
            string sROE = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var qry = (from cv in context.PCF_CUSTOMS_VAT select cv);

                if (qry != null)
                {
                    if (qry.Count() > 0)
                    {
                        var tmp = qry.ToList().LastOrDefault();
                        sROE = tmp.ROE.ToString();
                    }
                }
            }

            return sROE;
        }

        public static List<GLAccountVATViewModel> getGLAccountVAT()
        {
            List<GLAccountVATViewModel> GLAccount = new List<GLAccountVATViewModel>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var qry = (from gl in context.PCF_GL_VAT select gl);

                if (qry != null)
                {
                    foreach (var item in qry)
                    {
                        GLAccount.Add(new GLAccountVATViewModel
                        {
                            Type = item.TYPE,
                            GLAccount = item.GL_ACCOUNT,
                            TaxCodeVat = item.TAX_CODE_VAT,
                            ValuePart1 = item.VALUE_PART1,
                            TaxCodeMIRO = item.TAX_CODE_MIRO,
                            Currency = item.CURRENCY
                        });
                    }
                }

            }

            return GLAccount;
        }

        public static List<VATFactorViewModel> getVATFactor()
        {
            List<VATFactorViewModel> VATFactor = new List<VATFactorViewModel>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var qry = (from f in context.PCF_VAT_FACTOR select f);

                if (qry != null)
                {
                    foreach (var item in qry)
                    {
                        VATFactor.Add(new VATFactorViewModel
                        {
                            MetNum = item.MET_NUM,
                            ImportDutyF1 = String.IsNullOrEmpty(item.IMPORT_DUTY_F1) ? "" : item.IMPORT_DUTY_F1,
                            ImportDutyF1Const = String.IsNullOrEmpty(item.IMPORT_DUTY_F1_CONST.ToString()) ? "" : item.IMPORT_DUTY_F1_CONST.ToString(),
                            ImportDutyF2 = String.IsNullOrEmpty(item.IMPORT_DUTY_F2) ? "" : item.IMPORT_DUTY_F2,
                            ImportDutyF2Const = String.IsNullOrEmpty(item.IMPORT_DUTY_F2_CONST.ToString()) ? "" : item.IMPORT_DUTY_F2_CONST.ToString(),
                            ExciseTax = String.IsNullOrEmpty(item.EXCISE_TAX) ? "" : item.EXCISE_TAX,
                            ExciseTaxConst = String.IsNullOrEmpty(item.EXCISE_TAX_CONST.ToString()) ? "" : item.EXCISE_TAX_CONST.ToString(),
                            MunicipalTax = String.IsNullOrEmpty(item.MUNICIPAL_TAX) ? "" : item.MUNICIPAL_TAX,
                            MunicipalTaxConst = String.IsNullOrEmpty(item.MUNICIPAL_TAX_CONST.ToString()) ? "" : item.MUNICIPAL_TAX_CONST.ToString(),
                            DepositImportDuty = String.IsNullOrEmpty(item.DEPOSIT_IMPORT_DUTY) ? "" : item.DEPOSIT_IMPORT_DUTY,
                            DepositImportDutyConst = String.IsNullOrEmpty(item.DEPOSIT_IMPORT_DUTY_CONST.ToString()) ? "" : item.DEPOSIT_IMPORT_DUTY_CONST.ToString(),
                            DepositExciseTax = String.IsNullOrEmpty(item.DEPOSIT_EXCISE_TAX) ? "" : item.DEPOSIT_EXCISE_TAX,
                            DepositExciseTaxConst = String.IsNullOrEmpty(item.DEPOSIT_EXCISE_TAX_CONST.ToString()) ? "" : item.DEPOSIT_EXCISE_TAX_CONST.ToString(),
                            DepositMunicipalTax = String.IsNullOrEmpty(item.DEPOSIT_MUNICIPAL_TAX) ? "" : item.DEPOSIT_MUNICIPAL_TAX,
                            DepositMunicipalTaxConst = String.IsNullOrEmpty(item.DEPOSIT_MUNICIPAL_TAX_CONST.ToString()) ? "" : item.DEPOSIT_MUNICIPAL_TAX_CONST.ToString()
                        });
                    }
                }
            }

            //VATFactor.Add(new VATFactorViewModel
            //{
            //    MetNum = "YBGIMP",
            //    ImportDutyF1 = "X",
            //    ImportDutyF1Const = "0.1",
            //    ImportDutyF2 = "X",
            //    ImportDutyF2Const = "",
            //    ExciseTax = "",
            //    ExciseTaxConst = "",
            //    MunicipalTax = "",
            //    MunicipalTaxConst = "",
            //    DepositImportDuty = "",
            //    DepositImportDutyConst = "",
            //    DepositExciseTax = "",
            //    DepositExciseTaxConst = "",
            //    DepositMunicipalTax = "",
            //    DepositMunicipalTaxConst = ""
            //});

            //VATFactor.Add(new VATFactorViewModel
            //{
            //    MetNum = "YGBASE",
            //    ImportDutyF1 = "",
            //    ImportDutyF1Const = "",
            //    ImportDutyF2 = "X",
            //    ImportDutyF2Const = "0.01",
            //    ExciseTax = "X",
            //    ExciseTaxConst = "6.5",
            //    MunicipalTax = "X",
            //    MunicipalTaxConst = "0.1",
            //    DepositImportDuty = "X",
            //    DepositImportDutyConst = "0.05",
            //    DepositExciseTax = "X",
            //    DepositExciseTaxConst = "0.05",
            //    DepositMunicipalTax = "X",
            //    DepositMunicipalTaxConst = "0.02"
            //});

            //VATFactor.Add(new VATFactorViewModel
            //{
            //    MetNum = "YJP1IMP",
            //    ImportDutyF1 = "X",
            //    ImportDutyF1Const = "0.01",
            //    ImportDutyF2 = "X",
            //    ImportDutyF2Const = "",
            //    ExciseTax = "",
            //    ExciseTaxConst = "",
            //    MunicipalTax = "",
            //    MunicipalTaxConst = "",
            //    DepositImportDuty = "",
            //    DepositImportDutyConst = "",
            //    DepositExciseTax = "",
            //    DepositExciseTaxConst = "",
            //    DepositMunicipalTax = "",
            //    DepositMunicipalTaxConst = ""
            //});

            //VATFactor.Add(new VATFactorViewModel
            //{
            //    MetNum = "YHSDIMP",
            //    ImportDutyF1 = "",
            //    ImportDutyF1Const = "",
            //    ImportDutyF2 = "X",
            //    ImportDutyF2Const = "0.01",
            //    ExciseTax = "X",
            //    ExciseTaxConst = "5.85",
            //    MunicipalTax = "X",
            //    MunicipalTaxConst = "0.1",
            //    DepositImportDuty = "X",
            //    DepositImportDutyConst = "0.05",
            //    DepositExciseTax = "X",
            //    DepositExciseTaxConst = "0.05",
            //    DepositMunicipalTax = "X",
            //    DepositMunicipalTaxConst = "0.02"
            //});

            return VATFactor;
        }

        //public ReturnValue SendToSAP(ref CustomVatViewModel pModel, ref string sts, ref string msg, ref string FiDoc_Vat, ref string FiDoc_InvMiro, ref string InvDoc_Miro)
        public ReturnValue SendToSAP(ref CustomVatViewModel pModel)
        {
            Sendmail(pModel);
            //, ref string sts, ref string msg, ref string refno1, ref string refno2

            //CultureInfo provider = new CultureInfo("en-US");
            //string format = "dd/MM/yyyy";
            ReturnValue rtn = new ReturnValue();

            if (pModel == null)
            {
                rtn.Message = "Model is null.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sROE))
            {
                rtn.Message = "Please specify ROE.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sInsuranceRate))
            {
                rtn.Message = "Please specify Insurance Rate.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sDocumentDate))
            {
                rtn.Message = "Please specify Document Date.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sPostingDate))
            {
                rtn.Message = "Please specify Posting Date.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sCalculationUnit))
            {
                rtn.Message = "Please specify Calculation Unit.";
                rtn.Status = false;
                return rtn;
            }

            if (pModel.CustomVat_Calc == null)
            {
                rtn.Message = "Please select data for calculation.";
                rtn.Status = false;
                return rtn;
            }

            ResponseData resData = new ResponseData();
            try
            {
                CustomVat obj = new CustomVat();

                obj.sCalculationUnit = pModel.sCalculationUnit;
                obj.sROE = pModel.sROE;
                obj.sInsuranceRate = pModel.sInsuranceRate;
                obj.sDocumentDate = pModel.sDocumentDate;
                obj.sPostingDate = pModel.sPostingDate;
                obj.sTotalAll = pModel.sTotalAll;
                obj.sTotalImportDuty = pModel.sTotalImportDuty;
                obj.CustomVat_Calc = new List<CustomVat_Calc>();
                obj.GLAccountVATList = new List<GLAccountVAT>();

                if (pModel.GLAccountVAT == null)
                {
                    pModel.GLAccountVAT = getGLAccountVAT();
                }
                foreach (var gl in pModel.GLAccountVAT)
                {
                    GLAccountVAT glAcc = new GLAccountVAT();
                    glAcc.Type = gl.Type;
                    glAcc.GLAccount = gl.GLAccount;
                    glAcc.TaxCodeVat = gl.TaxCodeVat;
                    glAcc.ValuePart1 = gl.ValuePart1;
                    glAcc.TaxCodeMIRO = gl.TaxCodeMIRO;
                    glAcc.Currency = gl.Currency;

                    obj.GLAccountVATList.Add(glAcc);
                }
                if (pModel.ddl_InsuranceRate == null) { pModel.ddl_InsuranceRate = getCIPConfigInsuranceRate(); }

                foreach (var item in pModel.CustomVat_Calc)
                {
                    CustomVat_Calc temp = new CustomVat_Calc();
                    temp.No = item.No;
                    temp.PoNo = item.PoNo;
                    temp.CrudeSupplier = item.CrudeSupplier;
                    temp.ROE = item.ROE;
                    temp.QtyBBL = item.QtyBBL;
                    temp.QtyMT = item.QtyMT;
                    temp.QtyML = item.QtyML;
                    temp.CustomsPrice = item.CustomsPrice;
                    temp.FOB = item.FOB;
                    temp.FRT = item.FRT;
                    temp.CFR = item.CFR;
                    temp.INS = item.INS;
                    temp.CIF = item.CIF;
                    temp.Premium = item.Premium;
                    temp.TotalUSD100 = item.TotalUSD100;
                    temp.Baht100 = item.Baht100;
                    temp.Baht105 = item.Baht105;
                    temp.VAT = item.VAT;
                    temp.TaxBase = item.TaxBase;
                    temp.CorrectVAT = item.CorrectVAT;
                    temp.ImportDuty = item.ImportDuty;
                    temp.ExciseTax = item.ExciseTax;
                    temp.MunicipalTax = item.MunicipalTax;
                    temp.OilFuelFund = item.OilFuelFund;
                    temp.DepositOfImportDuty = item.DepositOfImportDuty;
                    temp.DepositOfExciseTax = item.DepositOfExciseTax;
                    temp.DepositOfMunicipalTax = item.DepositOfMunicipalTax;
                    temp.EnergyOilFuelFund = item.EnergyOilFuelFund;
                    temp.MatItemNo = "00" + item.MatItemNo;
                    temp.TripNo = item.TripNo;
                    temp.MetNum = item.MetNum;
                    temp.MatItemName = item.MatItemName;
                    temp.VendorNo = item.VendorNo;
                    temp.VendorName = item.VendorName;
                    temp.VesselNo = item.VesselNo;
                    temp.VesselName = item.VesselName;
                    temp.CompanyCode = item.CompanyCode;

                    obj.CustomVat_Calc.Add(temp);
                }

                var json = new JavaScriptSerializer().Serialize(obj);

                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000052;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";
                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();

                req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req.req_parameters.p.Add(new p { k = "system", v = "CustomVat" });
                req.req_parameters.p.Add(new p { k = "po_no", v = obj.CustomVat_Calc[0].PoNo });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                if (resData.result_code == "1")
                {
                    CultureInfo provider = new CultureInfo("en-US");
                    string format = "dd/MM/yyyy";
                    string Sapmsg_Vat = "";
                    string Saplogon_vat = "";
                    string Sapmsg_Miro = "";
                    string Saplogon_miro = "";
                    string InvDoc_Miro = "";
                    string FiDoc_Vat = "";
                    string FiDoc_InvMiro = "";

                    FiDoc_Vat = resData.resp_parameters[0].v;
                    if (FiDoc_Vat.Length > 10) FiDoc_Vat = FiDoc_Vat.Substring(0, 10);
                    Sapmsg_Vat = resData.resp_parameters[1].v;
                    if (Sapmsg_Vat.Length > 20) Sapmsg_Vat = Sapmsg_Vat.Substring(0, 20);
                    Saplogon_vat = resData.resp_parameters[2].v;
                    if (Saplogon_vat.Length > 4000) Saplogon_vat = Saplogon_vat.Substring(0, 4000);

                    /* --> Temporarily disabled
                    FiDoc_InvMiro = resData.resp_parameters[3].v;
                    InvDoc_Miro = resData.resp_parameters[4].v; //InvoiceDoc
                    Sapmsg_Miro = resData.resp_parameters[5].v;
                    Saplogon_miro = resData.resp_parameters[6].v;
                    */

                    //sts = resData.resp_parameters[1].v;
                    //var FIDocInvMiro = FiDoc_InvMiro.Split('|');
                    //For set SAP FI Doc of CustomVat_Calc


                    CUSTOMS_VAT_DAL dalCustomVat = new CUSTOMS_VAT_DAL();
                    PCF_CUSTOMS_VAT entCustomVat = new PCF_CUSTOMS_VAT();
                    using (var context = new EntityCPAIEngine())
                    {
                        for (var i = 0; i < pModel.CustomVat_Calc.ToList().Count(); i++)
                        {
                            entCustomVat.TRIP_NO = pModel.CustomVat_Calc.ToList()[i].TripNo;
                            entCustomVat.MAT_ITEM_NO = Int32.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].MatItemNo) ? "0" : pModel.CustomVat_Calc.ToList()[i].MatItemNo);
                            entCustomVat.CUSTOMS_PRICE = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].CustomsPrice) ? "0" : pModel.CustomVat_Calc.ToList()[i].CustomsPrice);
                            entCustomVat.FREIGHT_AMOUNT = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].FRT) ? "0" : pModel.CustomVat_Calc.ToList()[i].FRT);
                            entCustomVat.INSURANCE_AMOUNT = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].INS) ? "0" : pModel.CustomVat_Calc.ToList()[i].INS);
                            entCustomVat.INSURANCE_RATE = Decimal.Parse(String.IsNullOrEmpty(pModel.sInsuranceRate) ? "0" : pModel.sInsuranceRate);
                            entCustomVat.ROE = Decimal.Parse(String.IsNullOrEmpty(pModel.sROE) ? "1" : pModel.sROE);
                            entCustomVat.FOB = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].FOB) ? "0" : pModel.CustomVat_Calc.ToList()[i].FOB);
                            entCustomVat.FRT = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].FRT) ? "0" : pModel.CustomVat_Calc.ToList()[i].FRT);
                            entCustomVat.CFR = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].CFR) ? "0" : pModel.CustomVat_Calc.ToList()[i].CFR);
                            entCustomVat.INS = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].INS) ? "0" : pModel.CustomVat_Calc.ToList()[i].INS);
                            entCustomVat.CIF_SUM_INS = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].CIF) ? "0" : pModel.CustomVat_Calc.ToList()[i].CIF);
                            entCustomVat.PREMIUM = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].Premium) ? "0" : pModel.CustomVat_Calc.ToList()[i].Premium);
                            entCustomVat.TOTAL_USD_100 = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].TotalUSD100) ? "0" : pModel.CustomVat_Calc.ToList()[i].TotalUSD100);
                            entCustomVat.BAHT_100 = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].Baht100) ? "0" : pModel.CustomVat_Calc.ToList()[i].Baht100);
                            entCustomVat.BAHT_105 = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].Baht105) ? "0" : pModel.CustomVat_Calc.ToList()[i].Baht105);
                            entCustomVat.VAT = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].VAT) ? "0" : pModel.CustomVat_Calc.ToList()[i].VAT);
                            entCustomVat.TAX_BASE = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].TaxBase) ? "0" : pModel.CustomVat_Calc.ToList()[i].TaxBase);
                            entCustomVat.CORRECT_VAT = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].CorrectVAT) ? "0" : pModel.CustomVat_Calc.ToList()[i].CorrectVAT);
                            entCustomVat.IMPORT_DUTY = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].ImportDuty) ? "0" : pModel.CustomVat_Calc.ToList()[i].ImportDuty);
                            entCustomVat.EXCISE_TAX = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].ExciseTax) ? "0" : pModel.CustomVat_Calc.ToList()[i].ExciseTax);
                            entCustomVat.MUNICIPAL_TAX = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].MunicipalTax) ? "0" : pModel.CustomVat_Calc.ToList()[i].MunicipalTax);
                            entCustomVat.OIL_FUEL_FUND = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].OilFuelFund) ? "0" : pModel.CustomVat_Calc.ToList()[i].OilFuelFund);
                            entCustomVat.ENERY_OIL_FUEL_FUND = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].EnergyOilFuelFund) ? "0" : pModel.CustomVat_Calc.ToList()[i].EnergyOilFuelFund);
                            entCustomVat.DEPOSIT_OF_EXCISE_TAX = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].DepositOfExciseTax) ? "0" : pModel.CustomVat_Calc.ToList()[i].DepositOfExciseTax);
                            entCustomVat.DEPOSIT_OF_MUNICIPAL_TAX = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].DepositOfMunicipalTax) ? "0" : pModel.CustomVat_Calc.ToList()[i].DepositOfMunicipalTax);
                            entCustomVat.DEPOSIT_OF_IMPORT_DUTY = Decimal.Parse(String.IsNullOrEmpty(pModel.CustomVat_Calc.ToList()[i].DepositOfImportDuty) ? "0" : pModel.CustomVat_Calc.ToList()[i].DepositOfImportDuty);
                            entCustomVat.CAL_VOLUME_UNIT = String.IsNullOrEmpty(pModel.sCalculationUnit) ? "" : pModel.sCalculationUnit;

                            if (String.IsNullOrEmpty(pModel.sDocumentDate))
                            {
                                entCustomVat.DOCUMENT_DATE = null;
                            }
                            else
                            {
                                entCustomVat.DOCUMENT_DATE = DateTime.ParseExact(pModel.sDocumentDate, format, provider);
                            }
                            if (String.IsNullOrEmpty(pModel.sPostingDate))
                            {
                                entCustomVat.POSTING_DATE = null;
                            }
                            else
                            {
                                entCustomVat.POSTING_DATE = DateTime.ParseExact(pModel.sPostingDate, format, provider);
                            }

                            entCustomVat.SAP_FI_DOC = FiDoc_Vat;
                            entCustomVat.SAP_FI_DOC_STATUS = Saplogon_vat;
                            entCustomVat.SAP_FI_DOC_ERROR = Sapmsg_Vat;

                            /*
                             * --- Miro ---
                             * Miro is temporarily disabled due to the requirement change.
                             */
                            //entCustomVat.SAP_INVOICE_DOC = FIDocInvMiro.Length > 0 ? FIDocInvMiro[1] : "";
                            /*
                            entCustomVat.SAP_INVOICE_DOC = InvDoc_Miro;
                            entCustomVat.SAP_INVOICE_DOC_STATUS = Saplogon_miro;
                            entCustomVat.SAP_INVOICE_DOC_ERROR = Sapmsg_Miro;
                            */
                            //entCustomVat.SAP_FI_DOC_INVOICE = FIDocInvMiro.Length > 0 ? FIDocInvMiro[0] : "";
                            /*
                            entCustomVat.SAP_FI_DOC_INVOICE = FiDoc_InvMiro;
                            entCustomVat.SAP_FI_DOC_INVOICE_STATUS = Saplogon_miro;
                            entCustomVat.SAP_FI_DOC_INVOICE_ERROR = Sapmsg_Miro;
                            */

                            var _qry = context.PCF_CUSTOMS_VAT.Find(pModel.CustomVat_Calc.ToList()[i].TripNo, Int32.Parse(pModel.CustomVat_Calc.ToList()[i].MatItemNo));
                            if (_qry != null)
                            {
                                dalCustomVat.UpdateSapStatus(entCustomVat);
                            }


                            pModel.CustomVat_Calc.ToList()[i].FIDocVAT = FiDoc_Vat;
                            /*
                            pModel.CustomVat_Calc.ToList()[i].FIDocVATStatus = Saplogon_vat;
                            pModel.CustomVat_Calc.ToList()[i].FIDocVATMessage = Sapmsg_Vat;
                            //pModel.CustomVat_Calc.ToList()[i].FIDocMIRO = FIDocInvMiro.Length > 0 ? FIDocInvMiro[0] : "";
                            pModel.CustomVat_Calc.ToList()[i].FIDocMIRO = FiDoc_InvMiro;
                            pModel.CustomVat_Calc.ToList()[i].FIDocMIROStatus = Saplogon_miro;
                            pModel.CustomVat_Calc.ToList()[i].FIDocMIROMessage = Sapmsg_Miro;
                            //pModel.CustomVat_Calc.ToList()[i].InvoiceDocMIRO = FIDocInvMiro.Length > 0 ? FIDocInvMiro[1] : "";
                            pModel.CustomVat_Calc.ToList()[i].InvoiceDocMIRO = InvDoc_Miro;
                            pModel.CustomVat_Calc.ToList()[i].InvoiceDocMIROStatus = Saplogon_miro;
                            pModel.CustomVat_Calc.ToList()[i].InvoiceDocMIROMessage = Sapmsg_Miro;
                            */
                        }
                    }
                    //   Sendmail( pModel);
                    //For set SAP FI Doc of CustomVat_Search
                    /*
                    for (var j = 0; j < pModel.CustomVat_Search.sSearchData.ToList().Count; j++)
                    {
                        for (var k = 0; k < pModel.CustomVat_Calc.ToList().Count; k++)
                        {
                            if (pModel.CustomVat_Search.sSearchData.ToList()[j].dTripNo == pModel.CustomVat_Calc.ToList()[k].TripNo
                               && pModel.CustomVat_Search.sSearchData.ToList()[j].dMatItemNo == pModel.CustomVat_Calc.ToList()[k].MatItemNo)
                            {
                                pModel.CustomVat_Search.sSearchData.ToList()[j].dFIDocVAT = FiDoc_Vat;
                                //pModel.CustomVat_Search.sSearchData.ToList()[j].dFIDocMIRO = FIDocInvMiro.Length > 0 ? FIDocInvMiro[0] : "";
                                //pModel.CustomVat_Search.sSearchData.ToList()[j].dInvoiceDocMIRO = FIDocInvMiro.Length > 0 ? FIDocInvMiro[1] : "";
                                pModel.CustomVat_Search.sSearchData.ToList()[j].dFIDocMIRO = FiDoc_InvMiro;
                                pModel.CustomVat_Search.sSearchData.ToList()[j].dInvoiceDocMIRO = InvDoc_Miro;
                            }
                        }
                    }*/
                    //sirichai.
                    //todo

                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue SaveData(ref CustomVatViewModel pModel)
        {
            //CultureInfo provider = new CultureInfo("en-US");
            //string format = "dd/MM/yyyy";
            ReturnValue rtn = new ReturnValue();

            if (pModel == null)
            {
                rtn.Message = "Model is null.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sROE))
            {
                rtn.Message = "Please specify ROE.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sInsuranceRate))
            {
                rtn.Message = "Please specify Insurance Rate.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sDocumentDate))
            {
                rtn.Message = "Please specify Document Date.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sPostingDate))
            {
                rtn.Message = "Please specify Posting Date.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(pModel.sCalculationUnit))
            {
                rtn.Message = "Please specify Calculation Unit.";
                rtn.Status = false;
                return rtn;
            }

            if (pModel.CustomVat_Calc == null)
            {
                rtn.Message = "Please select data for calculation.";
                rtn.Status = false;
                return rtn;
            }

            ResponseData resData = new ResponseData();
            try
            {
                CustomVat obj = new CustomVat();

                obj.sCalculationUnit = pModel.sCalculationUnit;
                obj.sROE = pModel.sROE;
                obj.sInsuranceRate = pModel.sInsuranceRate;
                obj.sDocumentDate = pModel.sDocumentDate;
                obj.sPostingDate = pModel.sPostingDate;
                obj.sTotalAll = pModel.sTotalAll;
                obj.sTotalImportDuty = pModel.sTotalImportDuty;
                obj.CustomVat_Calc = new List<CustomVat_Calc>();
                obj.GLAccountVATList = new List<GLAccountVAT>();

                if (pModel.GLAccountVAT == null)
                {
                    pModel.GLAccountVAT = getGLAccountVAT();
                }
                foreach (var gl in pModel.GLAccountVAT)
                {
                    GLAccountVAT glAcc = new GLAccountVAT();
                    glAcc.Type = gl.Type;
                    glAcc.GLAccount = gl.GLAccount;
                    glAcc.TaxCodeVat = gl.TaxCodeVat;
                    glAcc.ValuePart1 = gl.ValuePart1;
                    glAcc.TaxCodeMIRO = gl.TaxCodeMIRO;
                    glAcc.Currency = gl.Currency;

                    obj.GLAccountVATList.Add(glAcc);
                }
                if (pModel.ddl_InsuranceRate == null) { pModel.ddl_InsuranceRate = getCIPConfigInsuranceRate(); }

                foreach (var item in pModel.CustomVat_Calc)
                {
                    CustomVat_Calc temp = new CustomVat_Calc();
                    temp.No = item.No;
                    temp.PoNo = item.PoNo;
                    temp.CrudeSupplier = item.CrudeSupplier;
                    temp.ROE = item.ROE;
                    temp.QtyBBL = item.QtyBBL;
                    temp.QtyMT = item.QtyMT;
                    temp.QtyML = item.QtyML;
                    temp.CustomsPrice = item.CustomsPrice;
                    temp.FOB = item.FOB;
                    temp.FRT = item.FRT;
                    temp.CFR = item.CFR;
                    temp.INS = item.INS;
                    temp.CIF = item.CIF;
                    temp.Premium = item.Premium;
                    temp.TotalUSD100 = item.TotalUSD100;
                    temp.Baht100 = item.Baht100;
                    temp.Baht105 = item.Baht105;
                    temp.VAT = item.VAT;
                    temp.TaxBase = item.TaxBase;
                    temp.CorrectVAT = item.CorrectVAT;
                    temp.ImportDuty = item.ImportDuty;
                    temp.ExciseTax = item.ExciseTax;
                    temp.MunicipalTax = item.MunicipalTax;
                    temp.OilFuelFund = item.OilFuelFund;
                    temp.DepositOfImportDuty = item.DepositOfImportDuty;
                    temp.DepositOfExciseTax = item.DepositOfExciseTax;
                    temp.DepositOfMunicipalTax = item.DepositOfMunicipalTax;
                    temp.EnergyOilFuelFund = item.EnergyOilFuelFund;
                    temp.MatItemNo = item.MatItemNo;
                    temp.TripNo = item.TripNo;
                    temp.MetNum = item.MetNum;
                    temp.MatItemName = item.MatItemName;
                    temp.VendorNo = item.VendorNo;
                    temp.VendorName = item.VendorName;
                    temp.VesselNo = item.VesselNo;
                    temp.VesselName = item.VesselName;
                    temp.CompanyCode = item.CompanyCode;

                    obj.CustomVat_Calc.Add(temp);
                }

                var json = new JavaScriptSerializer().Serialize(obj);

                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000047;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";
                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();

                req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req.req_parameters.p.Add(new p { k = "system", v = "CustomVat" });
                req.req_parameters.p.Add(new p { k = "po_no", v = obj.CustomVat_Calc[0].PoNo });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }


        public static List<SelectListItem> getCIPConfigInsuranceRate()
        {
            var strJSON = "{ { \"Name\":\"0.00001805\",\"Code\":\"0.00001805\"} ,{ \"Code\":\"0.00001083\",\"Name\":\"0.00001083\"}}";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_IMPORT_PLAN");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfig dataList = (GlobalConfig)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfig));

            List<SelectListItem> dList = new List<SelectListItem>();
            foreach (var item in dataList.PCF_MT_INSURANCE_RATE)
            {
                dList.Add(new SelectListItem { Value = item.Code, Text = item.Name });
            }

            return dList;
        }

        public static List<CustomVatSavedData> getSavedData(string[] pTripAndMatNos)
        {
            List<CustomVatSavedData> tList = new List<CustomVatSavedData>();
            try
            {
                CUSTOMS_VAT_DAL custDal = new CUSTOMS_VAT_DAL();
                foreach (string tTM in pTripAndMatNos)
                {
                    string[] tTripAndMatNos = tTM.Split('|');
                    PCF_CUSTOMS_VAT tCv = custDal.GetCustomVatsByTripAndMatNo(tTripAndMatNos[0], decimal.Parse(tTripAndMatNos[1]));
                    if (tCv != null)
                    {
                        CustomVatSavedData tCust = new CustomVatSavedData();
                        tCust.TripNo = tCv.TRIP_NO;
                        tCust.MatItemNo = tCv.MAT_ITEM_NO.ToString().Equals("") ? "0.0000" : tCv.MAT_ITEM_NO.ToString();
                        tCust.TaxBase = tCv.TAX_BASE.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.TAX_BASE.ToString()).ToString("#,##0.0000");
                        tCust.CorrectVat = tCv.CORRECT_VAT.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.CORRECT_VAT.ToString()).ToString("#,##0.0000");
                        tCust.ImportDuty = tCv.IMPORT_DUTY.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.IMPORT_DUTY.ToString()).ToString("#,##0.0000");
                        tCust.ExciseTax = tCv.EXCISE_TAX.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.EXCISE_TAX.ToString()).ToString("#,##0.0000");
                        tCust.MunicipalTax = tCv.MUNICIPAL_TAX.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.MUNICIPAL_TAX.ToString()).ToString("#,##0.0000");
                        tCust.OilFuelFund = tCv.OIL_FUEL_FUND.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.OIL_FUEL_FUND.ToString()).ToString("#,##0.0000");
                        tCust.DepositOfImportDuty = tCv.DEPOSIT_OF_IMPORT_DUTY.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.DEPOSIT_OF_IMPORT_DUTY.ToString()).ToString("#,##0.0000");
                        tCust.DepositOfExciseTax = tCv.DEPOSIT_OF_EXCISE_TAX.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.DEPOSIT_OF_EXCISE_TAX.ToString()).ToString("#,##0.0000");
                        tCust.DepositOfMunicipalTax = tCv.DEPOSIT_OF_MUNICIPAL_TAX.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.DEPOSIT_OF_MUNICIPAL_TAX.ToString()).ToString("#,##0.0000");
                        tCust.EnergyOilFuelFund = tCv.ENERY_OIL_FUEL_FUND.ToString().Equals("") ? "0.0000" : decimal.Parse(tCv.ENERY_OIL_FUEL_FUND.ToString()).ToString("#,##0.0000");
                        if ((double.Parse(tCust.TaxBase) + double.Parse(tCust.CorrectVat) + double.Parse(tCust.ImportDuty) +
                            double.Parse(tCust.ExciseTax) + double.Parse(tCust.MunicipalTax) +
                            double.Parse(tCust.OilFuelFund) + double.Parse(tCust.DepositOfImportDuty) +
                            double.Parse(tCust.DepositOfExciseTax) + double.Parse(tCust.DepositOfMunicipalTax) +
                            double.Parse(tCust.EnergyOilFuelFund)) > 0.0)
                        {
                            tList.Add(tCust);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tList;
        }


        public void Sendmail(CustomVatViewModel pModel)
        {
            try
            {

                string Vessel = "", mail_to = "", mail_cc = "", mail_bcc = "", mail_subject = "", mail_subject_val = "", path1 = "", mail_bodyheader = "", mail_bodyheader_text = "", mail_Body = "", mail_bodyfooter_text = "", mail_bodyfooter = "", content = "", Actual_from = "", Actual_to = "", Actual_date = "";

                content = pModel.SenMailResult;
                List<string> mail_subject_text = new List<string>();
                
              
                bool first = true;
                string log = "", log1 = "";
                int count = 0;
                //

                var str1 = "class=" + "\"table table-hover display\"";
                var str2 = "border=" + "\"1\"";

                var string1 = "td colspan=" + "\"26\"";
                var string2 = "td colspan = " + "\"11\"";
                
                content = @"" + content.Replace(string1, string2).Replace(str1, str2);
                int i = 0; ;
                string JsonD = MasterData.GetJsonMasterSetting("CIP_VAT");

                JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);
                GlobalConfigVat dataList = (GlobalConfigVat)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigVat));
                String PATH_FILE = dataList.PATH_FILE;
                foreach (var item in dataList.TO)
                {
                    mail_to += item.VAL.ToString() + ";";
                }
                foreach (var item in dataList.CC)
                {
                    mail_cc += item.VAL.ToString() + ";";
                }
                foreach (var item in dataList.BCC)
                {
                    mail_bcc += item.VAL.ToString() + ";";
                }
                String LastTOstr = mail_to.Substring(mail_to.Length - 1, 1);
                String LastCCstr = mail_cc.Substring(mail_cc.Length - 1, 1);
                String LastBCCstr = mail_bcc.Substring(mail_bcc.Length - 1, 1);

                if (LastTOstr.Equals(";"))
                {
                    mail_to = mail_to.Remove(mail_to.Length - 1);
                }
                if (LastCCstr.Equals(";"))
                {
                    mail_cc = mail_cc.Remove(mail_cc.Length - 1);
                }
                if (LastBCCstr.Equals(";"))
                {
                    mail_bcc = mail_bcc.Remove(mail_bcc.Length - 1);
                }
                List<string> strvalsubject = new List<string>();

                foreach (var item in dataList.SUBJECT)
                {
                    mail_subject_text.Add(item.TEXT);

                    List<VALUE> VALUE = item.VALUES.ToList();
                    int Count = VALUE.Count();
                    if (Count != 0)
                    {
                        // 
                        foreach (var s in VALUE)
                        {

                            strvalsubject.Add(s.VAL.ToString());
                            i++;
                        }

                    }

                }
                if (strvalsubject.Count == 0)
                {
                    foreach (var item in pModel.CustomVat_Calc)
                    {
                        Actual_from = item.sActual_from;
                        Actual_to = item.sActual_to;
                        Actual_date += (Actual_from.Equals(Actual_to) ? Actual_to : Actual_from) + ",";
                        Vessel += item.VesselName + ",";
                    }
                    String[] VesselName = !string.IsNullOrEmpty(Vessel) ? Vessel.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries) : VesselName = null;
                    String[] ActualDate = Actual_date.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    VesselName = VesselName.Distinct().ToArray();
                    if (VesselName.Length >= 1)
                    {
                        for (int k = 0; k < VesselName.Length - 1; k++)
                        {
                            mail_subject_text.Add(mail_subject_text[0]);

                        }
                    }
                    int j = 0;
                    foreach (var item in ActualDate)
                    {
                        DateTime dt = DateTime.ParseExact(item, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        ActualDate[j] = dt.ToString("dd MMMM  yyyy");
                        j++;
                    }
                    j = 0;

                    foreach (var item in mail_subject_text)
                    {
                        if (j == 0 && mail_subject_text.Count == 1)
                        {
                            mail_subject += string.Format(mail_subject_text[j], VesselName[j], ActualDate[j].ToString());
                        }
                        else
                        {
                            mail_subject += string.Format(mail_subject_text[j], VesselName[j], ActualDate[j].ToString()) + ",";
                        }

                        j++;
                    }



                }

                List<string> strvalheader = new List<string>();
                foreach (var item in dataList.BODY_HEADER)
                {
                    mail_bodyheader_text = item.TEXT;

                    List<VALUE> VALUE = item.VALUES.ToList();
                    int Count = VALUE.Count();
                    if (Count != 0)
                    {

                        foreach (var s in VALUE)
                        {
                            if (s.VAL != null)
                            {
                                strvalheader.Add(s.VAL.ToString());
                                i++;
                            }

                        }
                       
                        if (strvalheader.Count == 0)
                        {
                            string postingdate = pModel.sPostingDate;
                            string TotalAll = pModel.sTotalAll;
                            DateTime dt = DateTime.ParseExact(postingdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            strvalheader.Add(dt.ToString("dd MMMM  yyyy"));
                            strvalheader.Add(TotalAll);
                            mail_bodyheader = string.Format(mail_bodyheader_text, strvalheader.ToArray());
                        }
                        else
                        {
                            mail_bodyheader = string.Format(mail_bodyheader_text, strvalheader);
                        }
                        
                    }

                }
                foreach (var item in dataList.BODY_FOOTER)
                {
                    mail_bodyfooter_text = item.TEXT;

                    List<VALUE> VALUE = item.VALUES.ToList();
                    int Count = VALUE.Count();
                    if (Count != 0)
                    {
                        string[] strval = new string[Count];
                        foreach (var s in VALUE)
                        {
                            if (s.VAL != null)
                            {
                                strval[i] = s.VAL.ToString();
                                i++;
                            }

                        }
                        if (strval.Length > 0 && strval[0] != null)
                        {
                            mail_bodyfooter_text = string.Format(mail_bodyfooter_text, strval);
                        }
                        mail_bodyfooter = mail_bodyfooter_text;
                    }

                }

                //   var str1 = "class=" + "\"table table-hover display\"";
                //   FolderName | FileName | MailFromName | MailTo | MailCC | MailBCC | Subject | Body | ArchiveFile
                string strColumn = "FileName|MailFromName|MailTo|MailCC|MailBCC|Subject|Body|ArchiveFile";
                mail_Body = "<div style="+ "\"font-family: cordia new;font-size: 14pt;\">" + mail_bodyheader + content + mail_bodyfooter+ "</div>";
                CreateFile(strColumn, PATH_FILE, mail_to, mail_cc, mail_bcc, mail_subject, mail_Body);


            }
            catch (Exception ex)
            {

            }

        }

        public void CreateFile(string strColumn, string PATH_FILE, string mail_to, string mail_cc, string mail_bcc, string mail_subject, string mail_Body)
        {
            //FolderName | FileName | MailFromName | MailTo | MailCC | MailBCC | Subject | Body | ArchiveFile
            string MailFromName = "";
            DateTime datenow = DateTime.Now;
            string s = datenow.ToString("yyyyMMddHHmmss").Replace("/", "").Replace(":", "").Replace("PM", "").Replace("AM", "");
            string sub1 = s.Substring(0, 8);
            string path1 = "";
            string sub2 = s.Substring(8);
            string filename = "VAT" + sub1 + "_" + sub2 + ".txt";
            string strSO = "" + "|" + MailFromName + "|" + mail_to + "|" + mail_cc + "|" + mail_bcc + "|" + mail_subject + "|" + mail_Body + "|" + "NO";
            path1 = @"" + PATH_FILE + "\\" + filename;
            int i = strSO.Length;
            Byte[] info = new UTF8Encoding(true).GetBytes(strColumn);
            Byte[] content = new UTF8Encoding(true).GetBytes(strSO);

            byte[] newline = Encoding.ASCII.GetBytes(Environment.NewLine);


            if (!File.Exists(path1))
            {
                using (FileStream newFile = new FileStream(path1, FileMode.Create, FileAccess.ReadWrite))
                {
                    newFile.Write(info, 0, info.Length);
                    newFile.Write(newline, 0, newline.Length);
                    newFile.Write(content, 0, content.Length);
                }

            }
        }

        //public static string  SwapColumn(string root)
        //{

        //    retur
        //}

    }
}