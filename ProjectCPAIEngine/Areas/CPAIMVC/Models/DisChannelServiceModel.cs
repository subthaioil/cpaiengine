﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class DisChannelServiceModel
    {
        public ReturnValue Add(DisChannelViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCDC))
                {
                    rtn.Message = "DC should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCZone))
                {
                    rtn.Message = "Zone should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCDescription))
                {
                    rtn.Message = "Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_DISTRIBUTION_CHANNEL_DAL dal = new MT_DISTRIBUTION_CHANNEL_DAL();
                MT_DISTRIBUTION_CHANNEL ent = new MT_DISTRIBUTION_CHANNEL();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.MDC_DC = pModel.MDCDC;
                            ent.MDC_ZONE = pModel.MDCZone.Trim();
                            ent.MDC_DESCRIPTION = pModel.MDCDescription.Trim();
                            ent.MDC_STATUS = pModel.MDCStatus.Trim();
                            ent.MDC_CREATED_BY = pUser;
                            ent.MDC_CREATED_DATE = now;
                            ent.MDC_UPDATED_BY = pUser;
                            ent.MDC_UPDATED_DATE = now;

                            dal.Save(ent, context);
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(DisChannelViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCDC))
                {
                    rtn.Message = "DC should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCZone))
                {
                    rtn.Message = "Zone should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCDescription))
                {
                    rtn.Message = "Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MDCStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_DISTRIBUTION_CHANNEL_DAL dal = new MT_DISTRIBUTION_CHANNEL_DAL();
                MT_DISTRIBUTION_CHANNEL ent = new MT_DISTRIBUTION_CHANNEL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.MDC_DC = pModel.MDCDC;
                            ent.MDC_ZONE = pModel.MDCZone.Trim();
                            ent.MDC_DESCRIPTION = pModel.MDCDescription.Trim();
                            ent.MDC_STATUS = pModel.MDCStatus.Trim();
                            ent.MDC_UPDATED_BY = pUser;
                            ent.MDC_UPDATED_DATE = now;

                            dal.Update(ent, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref DisChannelViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sMDCDC = String.IsNullOrEmpty(pModel.sMDCDC) == true ? "" : pModel.sMDCDC;
                    var sMDCZone = String.IsNullOrEmpty(pModel.sMDCZone) == true ? "" : pModel.sMDCZone;
                    var sMDCStatus = String.IsNullOrEmpty(pModel.sMDCStatus) == true ? "" : pModel.sMDCStatus;
                    var sMDCDateFromTo = String.IsNullOrEmpty(pModel.sMDCDateFromTo) == true ? "" : pModel.sMDCDateFromTo;

                    var query = (from v in context.MT_DISTRIBUTION_CHANNEL select v);

                    if (!string.IsNullOrEmpty(sMDCDC))
                    {
                        query = query.Where(p => p.MDC_DC == sMDCDC);
                    }
                    if (!string.IsNullOrEmpty(sMDCZone))
                    {
                        query = query.Where(p => p.MDC_ZONE == sMDCZone);
                    }
                    if (!string.IsNullOrEmpty(sMDCStatus))
                    {
                        query = query.Where(p => p.MDC_STATUS == sMDCStatus);
                    }
                    if (!string.IsNullOrEmpty(sMDCDateFromTo))
                    {
                        ShareFn _FN = new ShareFn();
                        var startDate = "";
                        var endDate = "";
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        if (sMDCDateFromTo != "")
                        {
                            string[] date = sMDCDateFromTo.Split(' ');
                            startDate = date[0];
                            sDate = ShareFn.ConvertStrDateToDate(startDate);
                            endDate = date[2];
                            eDate = ShareFn.ConvertStrDateToDate(endDate);
                        }
                        query = query.Where(p => p.MDC_CREATED_DATE >= sDate && p.MDC_CREATED_DATE <= eDate);
                    }

                    if (query != null)
                    {
                        pModel.sSearchData = new List<DisChannelViewModel_SearchData>();
                        foreach (var item in query)
                        {
                            pModel.sSearchData.Add(new DisChannelViewModel_SearchData
                            {
                                dMDCDC = string.IsNullOrEmpty(item.MDC_DC) ? "NA" : item.MDC_DC,
                                dMDCZone = string.IsNullOrEmpty(item.MDC_ZONE) ? "NA" : item.MDC_ZONE,
                                dDescription = string.IsNullOrEmpty(item.MDC_DESCRIPTION) ? "NA" : item.MDC_DESCRIPTION,
                                dMDCStatus = string.IsNullOrEmpty(item.MDC_STATUS) ? "NA" : item.MDC_STATUS,
                                dMDCCreatedDate = ShareFn.ConvertDateTimeToDateStringFormat(item.MDC_CREATED_DATE, "dd/MM/yyyy"),
                                dMDCCreatedBy = string.IsNullOrEmpty(item.MDC_CREATED_BY) ? "NA" : item.MDC_CREATED_BY
                            });
                        }

                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public DisChannelViewModel_Detail Get(string pMCD_DC)
        {
            DisChannelViewModel_Detail model = new DisChannelViewModel_Detail();
            try
            {
                if (!String.IsNullOrEmpty(pMCD_DC))
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_DISTRIBUTION_CHANNEL
                                     where v.MDC_DC.Equals(pMCD_DC)
                                     select v);


                        if (query != null)
                        {
                            foreach (var item in query)
                            {
                                model.MDCDC = string.IsNullOrEmpty(item.MDC_DC) || item.MDC_DC == "NA" ? "" : item.MDC_DC;
                                model.MDCZone = string.IsNullOrEmpty(item.MDC_ZONE) || item.MDC_ZONE == "NA" ? "" : item.MDC_ZONE;
                                model.MDCDescription = string.IsNullOrEmpty(item.MDC_DESCRIPTION) || item.MDC_DESCRIPTION == "NA" ? "" : item.MDC_DESCRIPTION;
                                model.MDCStatus = string.IsNullOrEmpty(item.MDC_STATUS) || item.MDC_STATUS == "NA" ? "" : item.MDC_STATUS;
                                model.MDCCreatedDate = Convert.ToDateTime(item.MDC_CREATED_DATE).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            return model;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static string GetDisChannelIDJSON(string pStatus = "ACTIVE")
        {
            string json = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_DISTRIBUTION_CHANNEL
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MDC_DC }
                                    select new { System = d.MDC_DC.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetDisChannelZoneJSON(string pStatus = "ACTIVE")
        {
            string json = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_DISTRIBUTION_CHANNEL
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MDC_ZONE }
                                    select new { System = d.MDC_ZONE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

    }
}