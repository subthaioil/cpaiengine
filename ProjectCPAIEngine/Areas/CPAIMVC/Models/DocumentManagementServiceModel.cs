﻿using System.Web.Mvc;
using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class DocumentManagementServiceModel
    {
        public ReturnValue SearchSupportingDocument(ref SearchDocumentManagemantViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            var sDCrudeName = pModel.s_crude_name == null ? "" : pModel.s_crude_name.ToUpper();
            var sDCountry = pModel.s_country == null ? "" : pModel.s_country.ToUpper();
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from d in context.COO_DATA
                             where !string.IsNullOrEmpty(d.CODA_COUNTRY)
                             select new SupportingDocument_SearchData
                             {
                                 s_crude_name = d.CODA_CRUDE_NAME,
                                 s_country = d.CODA_COUNTRY
                             });
                if (!string.IsNullOrEmpty(sDCrudeName))
                    query = query.Where(p => p.s_crude_name.ToUpper() == sDCrudeName.ToUpper()).OrderBy(c => c.s_crude_name);
                if (!string.IsNullOrEmpty(sDCountry))
                    query = query.Where(p => p.s_country.ToUpper() == sDCountry.ToUpper()).OrderBy(c => c.s_crude_name);

                if (query != null)
                {
                    pModel.search_data = query.ToList();
                    //pModel.search_data = pModel.search_data.
                    pModel.search_data = new List<SupportingDocument_SearchData>();
                    foreach (var item in query)
                    {
                        pModel.search_data.Add(new SupportingDocument_SearchData
                        {
                            s_crude_name = string.IsNullOrEmpty(item.s_crude_name) ? "" : item.s_crude_name,
                            s_country = string.IsNullOrEmpty(item.s_country) ? "" : item.s_country
                        });
                    }
                    pModel.search_data = pModel.search_data.GroupBy(c => new { c.s_crude_name, c.s_country }).Select(c => c.FirstOrDefault()).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return rtn;
        }

        public ReturnValue SearchDocument(ref SearchDocumentManagemantViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            var sCrudeName = pModel.d_crude_name == null || pModel.d_crude_name == "" ? "" : pModel.d_crude_name.ToUpper();
            var sCountry = pModel.d_country == null || pModel.d_country == "" ? "" : pModel.d_country.ToUpper();
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from d in context.COO_DOC_MANAGE
                             select new Document_SearchData
                             {
                                 d_crude_name = d.CODM_CRUDE_NAME,
                                 d_country = d.CODM_COUNTRY,
                             });

                if (!string.IsNullOrEmpty(sCrudeName))
                    query = query.Where(p => p.d_crude_name.ToUpper() == sCrudeName.ToUpper()).OrderBy(c => c.d_crude_name);
                if (!string.IsNullOrEmpty(sCountry))
                    query = query.Where(p => p.d_country.ToUpper() == sCountry.ToUpper()).OrderBy(c => c.d_country);
                if (query != null)
                {
                    pModel.search_doc = query.ToList();
                    pModel.search_doc = new List<Document_SearchData>();
                    foreach (var item in query)
                    {
                        pModel.search_doc.Add(new Document_SearchData
                        {
                            d_crude_name = string.IsNullOrEmpty(item.d_crude_name) ? "" : item.d_crude_name,
                            d_country = string.IsNullOrEmpty(item.d_country) ? "" : item.d_country
                        });
                    }
                    pModel.search_doc = pModel.search_doc.GroupBy(c => new { c.d_country, c.d_crude_name }).Select(c => c.First()).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return rtn;
        }

        public ReturnValue saveFileDocManagement(DocumentManagemantViewModel pModel, string pUser)
        {
            DateTime dtNow = DateTime.Now;
            ReturnValue rtn = new ReturnValue();

            if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.filePath == null)
            {
                rtn.Message = "File path is not null";
                rtn.Status = false;
                return rtn;
            }

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region Create File Document
                            COO_DOC_MANAGE_DAL docManageDAL = new COO_DOC_MANAGE_DAL();
                            if (pModel != null)
                            {
                                int index = 0;
                                string[] filepath = pModel.filePath.Split('|');
                                for (int i = 0; i < filepath.Length - 1; i++)
                                {
                                    string[] file_path = filepath[i].Split(':');
                                    string[] file_type = file_path[1].Split('#');
                                    string[] crude_name = file_type[1].Split('!');

                                    for (int j = 0; j < crude_name.Length - 1; j++)
                                    {
                                        var crude_country = crude_name[j].Split('&');
                                        COO_DOC_MANAGE docManage = new COO_DOC_MANAGE();
                                        docManage.CODM_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + index.ToString().PadLeft(3, '0');
                                        docManage.CODM_CRUDE_NAME = crude_country[0];
                                        docManage.CODM_COUNTRY = crude_country[1];
                                        docManage.CODM_FILE_PATH = file_path[0];
                                        docManage.CODM_FILE_TYPE = file_type[0];
                                        docManage.CODM_CREATED = dtNow;
                                        docManage.CODM_CREATED_BY = pUser;
                                        docManage.CODM_UPDATED_BY = pUser;
                                        docManage.CODM_UPDATED = dtNow;
                                        docManageDAL.Save(docManage, context);
                                        index++;
                                    }
                                }
                            }
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public string CheckFileFormat(string path = "")
        {
            if (!string.IsNullOrEmpty(path))
            {
                List<string> worksheetName = new List<string>();
                worksheetName.Add("CDU-1");
                worksheetName.Add("CDU-2");
                worksheetName.Add("CDU-3");
                foreach (var item in worksheetName)
                {
                    ExcelWorksheet cpu = getDataFromExcel(path, item);
                    if (cpu == null)
                    {
                        return "0";
                    }
                }
            }

            return "1";
        }

        public ReturnValue saveFileCPU(SearchDocumentManagemantViewModel pModel, string pUser)
        {
            DateTime dtNow = DateTime.Now;
            ReturnValue rtn = new ReturnValue();

            if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
                return rtn;
            }

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region Create CPU
                            if (!string.IsNullOrEmpty(pModel.CPU_Path))
                            {
                                int start_row = 4;
                                int start_col = 3;
                                int index = 0;
                                COO_PROCESSING_UNIT_DAL ProUnitDAL = new COO_PROCESSING_UNIT_DAL();
                                List<string> worksheetName = new List<string>();
                                worksheetName.Add("CDU-1");
                                worksheetName.Add("CDU-2");
                                worksheetName.Add("CDU-3");

                                foreach (var item in worksheetName)
                                {
                                    ExcelWorksheet cpu = getDataFromExcel(pModel.CPU_Path, item);
                                    if (cpu == null)
                                    {
                                        //dbContextTransaction.Rollback();
                                        rtn.Message = "Invalid format, please check file again";
                                        rtn.Status = false;
                                        return rtn;
                                    }
                                    string crude = "";
                                    start_col = 3;
                                    do
                                    {
                                        string[] formats = { "d/M/yy", "dd/M/yy", "d/MM/yy", "dd/MM/yy", "d/MMM/yy", "dd/MMM/yy", "d/M/yyyy", "dd/M/yyyy", "d/MM/yyyy", "dd/MM/yyyy", "d/MMM/yyyy", "dd/MMM/yyyy",
                                                             "d-M-yy", "dd-M-yy", "d-MM-yy", "dd-MM-yy", "d-MMM-yy", "dd-MMM-yy", "d-M-yyyy", "dd-M-yyyy", "d-MM-yyyy", "dd-MM-yyyy", "d-MMM-yyyy", "dd-MMM-yyyy", };
                                        bool date_valid;
                                        string date_text = "";
                                        DateTime date_value;
                                        string current_col = ColumnNumberToName(start_col) + "3";
                                        start_row = 4;
                                        if (cpu != null)
                                        {
                                            crude = cpu.Cells[ColumnNumberToName(start_col) + "3"].Text;
                                            if (!string.IsNullOrEmpty(crude))
                                            {
                                                do
                                                {
                                                    string data = cpu.Cells[ColumnNumberToName(start_col) + start_row].Text;
                                                    date_text = cpu.Cells["B" + start_row].Text;
                                                    date_valid = DateTime.TryParseExact(date_text, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date_value);
                                                    start_row++;
                                                    if (date_valid)
                                                    {
                                                        COO_PROCESSING_UNIT ProUnit = new COO_PROCESSING_UNIT();
                                                        ProUnit.CPU_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + index.ToString().PadLeft(3, '0');
                                                        ProUnit.CPU_DATE = date_value;
                                                        ProUnit.CPU_CRUDE_ABBRV = crude;
                                                        ProUnit.CPU_VALUE = string.IsNullOrEmpty(data) ? "0" : data;
                                                        ProUnit.CPU_UNIT = item;
                                                        ProUnit.CPU_CREATED = DateTime.Now;
                                                        ProUnit.CPU_CREATED_BY = pUser;
                                                        ProUnit.CPU_UPDATED = DateTime.Now;
                                                        ProUnit.CPU_UPDATED_BY = pUser;
                                                        ProUnitDAL.Save(ProUnit, context);
                                                        index++;
                                                    }
                                                } while (date_valid);
                                            }
                                            start_col++;
                                        }
                                    } while (!string.IsNullOrEmpty(crude));
                                }
                            }
                            #endregion

                            #region Create CUA
                            COO_UNIT_ABBRV_DAL Unit_Abbrv_DAL = new COO_UNIT_ABBRV_DAL();
                            Unit_Abbrv_DAL.DeleteAll(context);
                            string[] crudes = pModel.cpu_name.Split('|');
                            int abbr_index = 0;
                            foreach (var crude_item in crudes)
                            {
                                if (!string.IsNullOrEmpty(crude_item))
                                {
                                    string[] unit_name = crude_item.Split('#');
                                    string[] crude_items = unit_name[1].Split('!');
                                    for (int i = 0; i < crude_items.Length - 1; i++)
                                    {
                                        var crude_abbrev = crude_items[i].Split('&');
                                        if (!string.IsNullOrEmpty(crude_abbrev[1]))
                                        {
                                            COO_UNIT_ABBV Unit_Abbrv = new COO_UNIT_ABBV();
                                            Unit_Abbrv.CUA_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + abbr_index++.ToString().PadLeft(2, '0');
                                            Unit_Abbrv.CUA_UNIT = unit_name[0];
                                            Unit_Abbrv.CUA_CRUDE_NAME = crude_abbrev[0];
                                            Unit_Abbrv.CUA_CRUDE_ABBRV = crude_abbrev[1];
                                            Unit_Abbrv.CUA_CREATED = DateTime.Now;
                                            Unit_Abbrv.CUA_CREATED_BY = pUser;
                                            Unit_Abbrv.CUA_UPDATED = DateTime.Now;
                                            Unit_Abbrv.CUA_UPDATED_BY = pUser;
                                            Unit_Abbrv_DAL.Save(Unit_Abbrv, context);
                                        }
                                    }
                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue getTranID(ref SupportingDocumentViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            string crude_name = model.crude_name;
            string country = model.country;
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from c in context.COO_DATA
                             where c.CODA_CRUDE_NAME == crude_name && c.CODA_COUNTRY == country
                             select c
                             );
                query = query.OrderBy(x => x.CODA_ROW_ID);
                foreach (var item in query)
                {
                    model.tranID = item.CODA_ROW_ID;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue getAbbreviation(ref SearchDocumentManagemantViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from c in context.COO_UNIT_ABBV
                             select c
                             );
                query = query.OrderBy(x => x.CUA_UNIT);

                model.abbr_name = new List<Abbreviation>();
                foreach (var item in query)
                {
                    Abbreviation abbr = new Abbreviation();
                    abbr.unit = item.CUA_UNIT;
                    abbr.crude = item.CUA_CRUDE_NAME;
                    abbr.abbreviation = item.CUA_CRUDE_ABBRV;
                    model.abbr_name.Add(abbr);
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue getFileSupport(ref SupportingDocumentViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            string id = model.tranID;
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from c in context.COO_SUP_DOC
                             where c.COSD_FK_COO_DATA == id
                             select c
                             );
                var query_expert = (from e in context.COO_EXPERT
                                    where e.COEX_FK_COO_DATA == id
                                    select e
                                    );
                if (query != null)
                {
                    foreach (var item in query)
                    {
                        model.file_path += item.COSD_FILE_PATH + ":" + item.COSD_FILE_TYPE + "|";
                    }
                }
                if (query_expert != null)
                {
                    foreach (var item in query_expert)
                    {
                        model.file_path += item.COEX_ATTACHED_FILE;
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue saveFileSupport(SupportingDocumentViewModel pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            DateTime dtNow = DateTime.Now;

            if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.file_path == null)
            {
                rtn.Message = "file path is not null";
                rtn.Status = false;
                return rtn;
            }

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region COO_SUP_DOC
                            COO_SUP_DOC_DAL dataSupDocDAL = new COO_SUP_DOC_DAL();
                            dataSupDocDAL.Delete(pModel.tranID, context);
                            COO_SUP_DOC dataSupDoc;
                            int supDoc_count = 1;
                            foreach (var item in pModel.file_path.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataSupDoc = new COO_SUP_DOC();
                                    dataSupDoc.COSD_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataSupDoc.COSD_FK_COO_DATA = pModel.tranID;
                                    dataSupDoc.COSD_ORDER = supDoc_count + "";
                                    dataSupDoc.COSD_FILE_PATH = text[0];
                                    dataSupDoc.COSD_FILE_TYPE = text[1];
                                    dataSupDoc.COSD_CREATED = dtNow;
                                    dataSupDoc.COSD_CREATED_BY = pUser;
                                    dataSupDoc.COSD_UPDATED = dtNow;
                                    dataSupDoc.COSD_UPDATED_BY = pUser;
                                    dataSupDocDAL.Save(dataSupDoc, context);
                                    supDoc_count++;
                                }
                            }
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue updateFileDocument(DocumentManagemantViewModel pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            DateTime dtNow = DateTime.Now;

            if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.filePath == null)
            {
                rtn.Message = "file path is not null";
                rtn.Status = false;
                return rtn;
            }

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region COO_S
                            COO_DOC_MANAGE_DAL dataDocDAL = new COO_DOC_MANAGE_DAL();
                            dataDocDAL.Delete(pModel.crudeName, pModel.country, context);
                            COO_DOC_MANAGE dataDoc;
                            foreach (var item in pModel.filePath.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataDoc = new COO_DOC_MANAGE();
                                    dataDoc.CODM_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataDoc.CODM_FILE_PATH = text[0];
                                    dataDoc.CODM_FILE_TYPE = text[1];
                                    dataDoc.CODM_CRUDE_NAME = pModel.crudeName;
                                    dataDoc.CODM_COUNTRY = pModel.country;
                                    dataDoc.CODM_CREATED = dtNow;
                                    dataDoc.CODM_CREATED_BY = pUser;
                                    dataDoc.CODM_UPDATED = dtNow;
                                    dataDoc.CODM_UPDATED_BY = pUser;
                                    dataDocDAL.Save(dataDoc, context);
                                }
                            }
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue getDetailFile(ref DocumentManagemantViewDetailModel model)
        {
            ReturnValue rtn = new ReturnValue();
            var sCrudeName = model.crudeName == null ? "" : model.crudeName.ToUpper();
            var sCountry = model.country == null ? "" : model.country.ToUpper();

            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                var query = (from c in context.COO_DOC_MANAGE
                             where c.CODM_CRUDE_NAME.ToUpper() == sCrudeName && c.CODM_COUNTRY.ToUpper() == sCountry
                             select c
                             );

                query = query.Where(x => x.CODM_CRUDE_NAME.ToUpper().Contains(sCrudeName) && x.CODM_COUNTRY.ToUpper().Contains(sCountry));

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        model.filePath += item.CODM_FILE_PATH + ":" + item.CODM_FILE_TYPE + "|";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue getCPUState(ref CPUViewModel model, string crude, string date)
        {
            ReturnValue rtn = new ReturnValue();
            var sCrudeName = crude == null ? "" : crude.ToUpper();

            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                string abbv_cuu1 = (from c in context.COO_UNIT_ABBV
                                    where c.CUA_CRUDE_NAME.ToUpper() == sCrudeName && c.CUA_UNIT.ToUpper() == "CDU-1" 
                                    select c.CUA_CRUDE_ABBRV).FirstOrDefault() ?? "";
                string abbv_cuu2 = (from c in context.COO_UNIT_ABBV
                                    where c.CUA_CRUDE_NAME.ToUpper() == sCrudeName && c.CUA_UNIT.ToUpper() == "CDU-2"
                                    select c.CUA_CRUDE_ABBRV).FirstOrDefault() ?? "";
                string abbv_cuu3 = (from c in context.COO_UNIT_ABBV
                                    where c.CUA_CRUDE_NAME.ToUpper() == sCrudeName && c.CUA_UNIT.ToUpper() == "CDU-3"
                                    select c.CUA_CRUDE_ABBRV).FirstOrDefault() ?? "";

                var query = (from u in context.COO_PROCESSING_UNIT
                             join a in context.COO_UNIT_ABBV on u.CPU_CRUDE_ABBRV.ToUpper() equals a.CUA_CRUDE_ABBRV.ToUpper()
                             where /*(condition_date != DateTime.MinValue ? (u.CPU_DATE >= min_condition_date && u.CPU_DATE < max_condition_date) : true) && */ a.CUA_CRUDE_NAME.ToUpper() == sCrudeName && u.CPU_VALUE != "0"
                             select u);

                if (query != null)
                {
                    var list = query.ToList();
                    model.cdu1 = list.Where(i => i.CPU_UNIT == "CDU-1" && i.CPU_VALUE != "0" && i.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu1.ToUpper()).Any() ? "Y" : "N";
                    model.processingDate_cdu1 = list.Where(i => i.CPU_UNIT == "CDU-1" && i.CPU_VALUE != "0" && i.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu1.ToUpper()).Any() ? list.Where(l => l.CPU_UNIT == "CDU-1" && l.CPU_VALUE != "0" && l.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu1.ToUpper()).OrderByDescending(c => c.CPU_ROW_ID).FirstOrDefault().CPU_DATE.Value.ToString("MMM-yy") : "";
                    model.cdu2 = list.Where(i => i.CPU_UNIT == "CDU-2" && i.CPU_VALUE != "0" && i.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu2.ToUpper()).Any() ? "Y" : "N";
                    model.processingDate_cdu2 = list.Where(i => i.CPU_UNIT == "CDU-2" && i.CPU_VALUE != "0" && i.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu2.ToUpper()).Any() ? list.Where(l => l.CPU_UNIT == "CDU-2" && l.CPU_VALUE != "0" && l.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu2.ToUpper()).OrderByDescending(c => c.CPU_ROW_ID).FirstOrDefault().CPU_DATE.Value.ToString("MMM-yy") : "";
                    model.cdu3 = list.Where(i => i.CPU_UNIT == "CDU-3" && i.CPU_VALUE != "0" && i.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu3.ToUpper()).Any() ? "Y" : "N";
                    model.processingDate_cdu3 = list.Where(i => i.CPU_UNIT == "CDU-3" && i.CPU_VALUE != "0" && i.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu3.ToUpper()).Any() ? list.Where(l => l.CPU_UNIT == "CDU-3" && l.CPU_VALUE != "0" && l.CPU_CRUDE_ABBRV.ToUpper() == abbv_cuu3.ToUpper()).OrderByDescending(c => c.CPU_ROW_ID).FirstOrDefault().CPU_DATE.Value.ToString("MMM-yy") : "";
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public static ExcelWorksheet getDataFromExcel(string excelPath, string worksheetName)
        {
            string file_path = !string.IsNullOrEmpty(excelPath) ? excelPath : "";
            if (!string.IsNullOrEmpty(file_path))
            {
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file_path);
                try
                {
                    WebRequest request = WebRequest.Create(path);
                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            var excelPackage = new ExcelPackage(stream);
                            ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.Where(i => i.Name.ToUpper().Contains(worksheetName)).FirstOrDefault();
                            return excelWorksheet;
                        }
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        private string ColumnNumberToName(int col_num)
        {
            if (col_num < 1) return "A"; //See if it's out of bounds.

            //Calculate the letters.
            string result = "";
            while (col_num > 0)
            {
                col_num -= 1; //Get the least significant digit.
                int digit = col_num % 26;
                result = (char)((int)'A' + digit) + result; //Convert the digit into a letter.
                col_num = (int)(col_num / 26);
            }

            return result;
        }

        public static List<SelectListItem> getMaterialsDDL()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS
                             where d.MET_CREATE_TYPE == "COOL" && d.MET_MAT_DES_ENGLISH != null
                             orderby new { d.MET_MAT_DES_ENGLISH }
                             select new { mKey = d.MET_NUM, mValue = d.MET_MAT_DES_ENGLISH }).ToList();
                if (query != null)
                {
                    foreach (var item in query.GroupBy(x => x.mValue))
                    {
                        rtn.Add(new SelectListItem { Value = item.Key, Text = item.Key });
                    }
                }
            }
            return rtn;
        }
    }
}