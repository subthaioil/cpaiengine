﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALFreight;
using ProjectCPAIEngine.DAL.Entity;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.DALVCOOL;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    [Serializable]
    public class SelectListData
    {
        public string value { get; set; }
        public string text { get; set; }
    }

    public class DropdownServiceModel_Data
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class DropdownServiceModel
    {
        public enum HedgeStatus
        {
            PreDeal,
            Deal,
            Ticket,
        };

        private const string JSON_HEDG = "JSON_HEDG";

        private const string JSON_TOOL = "JSON_TOOL";

        /// <summary>
        /// Get Vehicle
        /// </summary>
        /// <param name="Type">Type</param>
        /// <param name="isOptional"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static List<SelectListItem> getVehicle(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(PROJECT_NAME_SPACE, ACTIVE, type).OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), mt_vehicle.Select(x => x.VEH_ID).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVehicleTceMK(bool isOptional = false, string message = "")
        {
            List<SelectListItem> model = new List<SelectListItem>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var vehDis = (from v in context.CPAI_TCE_MK
                              select new { VEH_ID = v.CTM_FK_MT_VEHICLE }).Distinct().ToList();

                var query = from v in vehDis
                            join vc in context.MT_VEHICLE on v.VEH_ID equals vc.VEH_ID into view
                            from vc in view.DefaultIfEmpty()
                            select new SelectListItem { Value = v.VEH_ID, Text = vc.VEH_VEH_TEXT };
                model = query.ToList();
            }

            return insertSelectListValue(model, isOptional, message);
        }
        // get Data Customer
        public static List<SelectListItem> getCustomer(string type = "CUS", bool isOptional = false, string message = "")
        {
            List<MT_CUST_DETAIL> mt_cust_detail = CustDetailDAL.GetCustDetail(PROJECT_NAME_SPACE, ACTIVE, type);
            return insertSelectListValue(mt_cust_detail.Select(x => x.MCD_NAME_1).ToList(), mt_cust_detail.Select(x => x.MCD_ROW_ID).ToList(), isOptional, message);

        }

        public static List<SelectListItem> getVendorPIT(bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendorPIT(PROJECT_NAME_PIT, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }

        public static List<SelectListItem> GetCompanyCode(string pStatus = "ACTIVE")
        {

            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCO_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCO_COMPANY_CODE }
                                    select new { System = d.MCO_COMPANY_CODE.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new SelectListItem { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
                return rtn;

            }
        }

        public static List<SelectListItem> GetCompanyShortName(string pStatus = "ACTIVE")
        {

            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCO_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCO_SHORT_NAME }
                                    select new { System = d.MCO_SHORT_NAME.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new SelectListItem { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
                return rtn;
            }
        }

        public static List<SelectListItem> getCompany(string pStatus = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCO_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCO_SHORT_NAME }
                                    select new { value = d.MCO_COMPANY_CODE, text = d.MCO_SHORT_NAME.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.text))
                        {
                            rtn.Add(new SelectListItem { Value = item.value, Text = item.text });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
                return rtn;

            }
        }

        public static List<SelectListItem> getVendorColor(string type = "CPAI")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<DAL.Model.VendorColor> mt_vendor = VendorDAL.GetVendorColor(type, ACTIVE).OrderBy(x => x.VENDOR.VND_NAME1).ToList();
            for (int i = 0; i < mt_vendor.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = mt_vendor[i].VENDOR.VND_NAME1, Value = String.IsNullOrEmpty(mt_vendor[i].VENDOR_COLOR_CODE) ? COLOR_DEFUALT + "|" + mt_vendor[i].VENDOR.VND_ACC_NUM_VENDOR : mt_vendor[i].VENDOR_COLOR_CODE + "|" + mt_vendor[i].VENDOR.VND_ACC_NUM_VENDOR });
            }
            return selectList;
        }

        public static List<SelectListItem> getCustomerColor(string type = "CPAI", string nation = "I")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<DAL.Model.CustColor> mt_cust = CustDetailDAL.GetCustomerColor(type, ACTIVE, nation).OrderBy(x => x.CUSTOMER.MCD_NAME_1).ToList();
            for (int i = 0; i < mt_cust.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = mt_cust[i].CUSTOMER.MCD_NAME_1, Value = String.IsNullOrEmpty(mt_cust[i].CUSTOMER_COLOR_CODE) ? COLOR_DEFUALT + "|" + mt_cust[i].CUSTOMER.MCD_ROW_ID : mt_cust[i].CUSTOMER_COLOR_CODE + "|" + mt_cust[i].CUSTOMER.MCD_ROW_ID });
            }
            return selectList;
        }

        public static List<SelectListItem> getVendor(bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(PROJECT_NAME_SPACE, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }


        public static List<SelectListItem> getVendorMapped(bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendorCP(PROJECT_NAME_SPACE, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVendorFreight(string type = "", bool isOptional = false, string message = "", bool Simbol = false)
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(PROJECT_NAME_SPACE, type, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            MT_VENDOR mt_vendor_na = mt_vendor.Where(v => v.VND_NAME1 == "N/A").FirstOrDefault();
            for (int i = 0; i < mt_vendor.Count; i++)
            {
                if (mt_vendor[i].VND_NAME1 == "N/A")
                {
                    mt_vendor.RemoveAt(i);
                }
            }
            if (mt_vendor_na != null)
            {
                mt_vendor.Add(mt_vendor_na);
            }

            if (Simbol)
            {
                List<string> list_text = new List<string>();
                List<string> list_value = new List<string>();
                foreach (var v in mt_vendor)
                {
                    list_value.Add(v.VND_ACC_NUM_VENDOR);
                    if (v.VND_NAME1 != "N/A")
                    {
                        list_text.Add(v.VND_SORT_FIELD + " - " + v.VND_NAME1);
                    }
                    else
                    {
                        list_text.Add(v.VND_NAME1);
                    }
                }
                return insertSelectListValue(list_text, list_value, isOptional, message);
                //return insertSelectListValue(mt_vendor.Select(x => "(" + x.VND_SORT_FIELD + ") " + x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
            }
            else
            {
                return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
            }
        }

        // get Data Freight
        public static List<SelectListItem> getFreight()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //List<CPAI_FREIGHT_DATA> freight = FREIGHT_DATA_DAL.GetFreightData("");
            List<CPAI_FREIGHT_DATA> freight = FREIGHT_DATA_DAL.GetFreightData("SUBMIT");
            return insertSelectListValue(freight.Select(x => x.FDA_DOC_NO).ToList(), freight.Select(x => x.FDA_DOC_NO).ToList());
        }

        // get Data Product
        // same as getMaterial
        // Only use for schedule
        public static List<SelectListItem> getProduct(string type = "CPAI")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE);
            for (int i = 0; i < mt_materials.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = mt_materials[i].MET_MAT_DES_ENGLISH, Value = mt_materials[i].MET_NUM });
            }
            return selectList;
        }

        public static List<SelectListItem> getPortName(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_PORT> mt_port = PortDAL.GetPort(type, ACTIVE).OrderBy(x => x.MLP_LOADING_PORT_NAME).ToList();
            return insertSelectListValue(mt_port.Select(x => x.MLP_LOADING_PORT_NAME).ToList(), mt_port.Select(x => x.MLP_LOADING_PORT_ID.ToString()).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getPortJettyList(string fromType = "", string portType = "", string country = "", string location = "")
        {
            List<MT_JETTY> mt_port = PortDAL.GetPortData();
            if (fromType != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_CREATE_TYPE.Trim().ToUpper() == fromType.Trim().ToUpper()).OrderBy(x => x.MTJ_PORT_TYPE).ToList();
            }
            if (portType != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_PORT_TYPE.Trim().ToUpper() == portType.Trim().ToUpper()).OrderBy(x => x.MTJ_MT_COUNTRY).ToList();
            }
            if (country != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_MT_COUNTRY.Trim().ToUpper() == country.Trim().ToUpper()).OrderBy(x => x.MTJ_LOCATION).ToList();
            }
            if (location != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_LOCATION.Trim().ToUpper() == location.Trim().ToUpper()).OrderBy(x => x.MTJ_JETTY_NAME).ToList();
            }

            if (fromType != "" && portType != "" && country != "" && location != "")
            {
                return insertSelectListValue(mt_port.Select(x => x.MTJ_JETTY_NAME).Distinct().ToList(), mt_port.Select(x => (x.MTJ_JETTY_NAME + ", " + x.MTJ_LOCATION + ", " + x.MTJ_MT_COUNTRY)).Distinct().ToList());
            }
            else if (fromType != "" && portType != "" && country != "")
            {
                return insertSelectListValue(mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList(), mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList());
            }
            else if (fromType != "" && portType != "")
            {
                return insertSelectListValue(mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList(), mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList());
            }
            else if (fromType != "")
            {
                return insertSelectListValue(mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList(), mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList());
            }
            else
                return new List<SelectListItem>();

        }


        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "MAT_CMCS")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterialCountry(bool isOptional = false, string message = "", string type = "MAT_CMCS")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_ORIG_CRTY).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getBroker(bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(BROKER_TYPE, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
                //else
                //{
                //    selectList.Add(new SelectListItem { Text = "", Value = "" });
                //}
            }
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        private static List<SelectListItem> insertSelectListValue(List<SelectListItem> selectList, bool isOptional = false, string message = "")
        {

            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Insert(0, new SelectListItem { Text = message, Value = "" });
                }
                else
                {
                    selectList.Insert(0, new SelectListItem { Text = "Please select", Value = "" });
                }
            }

            return selectList;
        }

        private static List<SelectListItem> insertSelectListValue(List<SelectListItem> selectList, string name, string nameEng, string underlying, string unitPrice, string unitVolume, string unitVolumeMnt, bool isUnit)
        {
            if (name != null && !name.Trim().Equals(string.Empty))
            {
                if (isUnit)
                    selectList.Add(new SelectListItem { Text = name, Value = underlying + "|" + unitPrice + "|" + unitVolume + "|" + unitVolumeMnt });
                else
                    selectList.Add(new SelectListItem { Text = name, Value = underlying });
            }
            else
            {
                if (isUnit)
                    selectList.Add(new SelectListItem { Text = nameEng, Value = underlying + "|" + unitPrice + "|" + unitVolume + "|" + unitVolumeMnt });
                else
                    selectList.Add(new SelectListItem { Text = nameEng, Value = underlying });
            }
            return selectList;
        }

        public static List<SelectListItem> getColorPicker()
        {
            List<SelectListItem> colorItem = new List<SelectListItem>();
            var data = new List<DropdownServiceModel_Data>();
            data.Add(new DropdownServiceModel_Data { Value = "#8f8f8f", Text = "Silver" });
            data.Add(new DropdownServiceModel_Data { Value = "#ab1e41", Text = "DeepRed" });
            data.Add(new DropdownServiceModel_Data { Value = "#77242e", Text = "BrickRed" });
            data.Add(new DropdownServiceModel_Data { Value = "#d11f2d", Text = "BrightRed" });
            data.Add(new DropdownServiceModel_Data { Value = "#ea417a", Text = "BrightPink" });
            data.Add(new DropdownServiceModel_Data { Value = "#dca4af", Text = "DustyPink" });
            data.Add(new DropdownServiceModel_Data { Value = "#fcb4cc", Text = "LightPink" });
            data.Add(new DropdownServiceModel_Data { Value = "#fcbca0", Text = "Peach" });
            data.Add(new DropdownServiceModel_Data { Value = "#f48572", Text = "Coral" });
            data.Add(new DropdownServiceModel_Data { Value = "#fc8c2a", Text = "Tangerine" });
            data.Add(new DropdownServiceModel_Data { Value = "#f16d25", Text = "VibrantOrange" });
            data.Add(new DropdownServiceModel_Data { Value = "#c54134", Text = "Ginger" });
            data.Add(new DropdownServiceModel_Data { Value = "#c36655", Text = "TerraCotta" });
            data.Add(new DropdownServiceModel_Data { Value = "#b88253", Text = "Tan" });
            data.Add(new DropdownServiceModel_Data { Value = "#653c2a", Text = "Chocolate" });
            data.Add(new DropdownServiceModel_Data { Value = "#594745", Text = "EarthBrown" });
            data.Add(new DropdownServiceModel_Data { Value = "#88754b", Text = "Gold" });
            data.Add(new DropdownServiceModel_Data { Value = "#dd7b24", Text = "Amber" });
            data.Add(new DropdownServiceModel_Data { Value = "#f8a81d", Text = "GoldenYellow" });
            data.Add(new DropdownServiceModel_Data { Value = "#fdcf08", Text = "BrightYellow" });
            data.Add(new DropdownServiceModel_Data { Value = "#f6e068", Text = "LightYellow" });
            data.Add(new DropdownServiceModel_Data { Value = "#d6da4f", Text = "Chartreuse" });
            data.Add(new DropdownServiceModel_Data { Value = "#add690", Text = "LightGreen" });
            data.Add(new DropdownServiceModel_Data { Value = "#8a8b47", Text = "OliveGreen" });
            data.Add(new DropdownServiceModel_Data { Value = "#78993e", Text = "Lime" });
            data.Add(new DropdownServiceModel_Data { Value = "#124534", Text = "DarkGreen" });
            data.Add(new DropdownServiceModel_Data { Value = "#136736", Text = "FoliageGreens" });
            data.Add(new DropdownServiceModel_Data { Value = "#05994d", Text = "BrightGreen" });
            data.Add(new DropdownServiceModel_Data { Value = "#009b7a", Text = "Emerald" });
            data.Add(new DropdownServiceModel_Data { Value = "#a1d4cb", Text = "Aqua" });
            data.Add(new DropdownServiceModel_Data { Value = "#4cc1af", Text = "Turquoise" });
            data.Add(new DropdownServiceModel_Data { Value = "#036781", Text = "Teal" });
            data.Add(new DropdownServiceModel_Data { Value = "#72b0c7", Text = "SkyBlue" });
            data.Add(new DropdownServiceModel_Data { Value = "#99bfe6", Text = "LightBlue" });
            data.Add(new DropdownServiceModel_Data { Value = "#8093cd", Text = "Periwinkle" });
            data.Add(new DropdownServiceModel_Data { Value = "#0172b8", Text = "BrightBlue" });
            data.Add(new DropdownServiceModel_Data { Value = "#212d69", Text = "DeepBlue" });
            data.Add(new DropdownServiceModel_Data { Value = "#bb3e99", Text = "Lavender" });
            data.Add(new DropdownServiceModel_Data { Value = "#a17691", Text = "Mauve" });
            data.Add(new DropdownServiceModel_Data { Value = "#b984ba", Text = "Amethyst" });
            data.Add(new DropdownServiceModel_Data { Value = "#643293", Text = "BluePurples" });
            data.Add(new DropdownServiceModel_Data { Value = "#862c8e", Text = "RedPurples" });
            data.Add(new DropdownServiceModel_Data { Value = "#3b1a53", Text = "DeepPurples" });
            data.Add(new DropdownServiceModel_Data { Value = "#898d8c", Text = "NeutralGray" });
            data.Add(new DropdownServiceModel_Data { Value = "#57585a", Text = "CharcoalGray" });
            data.Add(new DropdownServiceModel_Data { Value = "#a8a089", Text = "Taupe" });
            data.Add(new DropdownServiceModel_Data { Value = "#fae1a8", Text = "Ivory" });
            data.Add(new DropdownServiceModel_Data { Value = "#000000", Text = "Black" });
            data.Add(new DropdownServiceModel_Data { Value = "#ffffff", Text = "White" });

            foreach (var item in data)
            {
                colorItem.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return colorItem;
        }

        #region --- Vendor ---

        // get Distinct Data Country
        public static List<SelectListItem> getCountry(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<MT_COUNTRY> mt_country = MT_COUNTRY_DAL.GetCountry(ACTIVE).OrderBy(x => x.MCT_LANDX).ToList();
            var disData = (from m in mt_country
                           select new { Text = m.MCT_LANDX, Value = m.MCT_LAND1 }).Distinct().ToList();
            foreach (var item in disData)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }
            return insertSelectListValue(selectList, isOptional, message);
        }


        public static List<SelectListItem> getCountryLANDX(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<MT_COUNTRY> mt_country = MT_COUNTRY_DAL.GetCountry(ACTIVE).OrderBy(x => x.MCT_LANDX).ToList();
            var disData = (from m in mt_country
                           select new { Text = m.MCT_LANDX, Value = m.MCT_LANDX }).Distinct().ToList();
            foreach (var item in disData)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }
            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getCountryCrudePurchase(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<MT_COUNTRY> mt_country = MT_COUNTRY_DAL.GetCountry(ACTIVE).ToList();
            var disData = (from m in mt_country
                           select new { Text = m.MCT_LANDX, Value = m.MCT_LANDX }).Distinct().ToList();
            disData = disData.OrderBy(x=>x.Text).ToList();
            foreach (var item in disData)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS
                             where d.MET_CREATE_TYPE == ConstantPrm.SYSTEM.COOL
                             orderby new { d.MET_MAT_DES_ENGLISH }
                             select new { mKey = d.MET_ORIG_CRTY, mValue = d.MET_ORIG_CRTY }).Distinct().ToList();
                if (query != null)
                {
                    foreach (var item in query)
                    {
                        selectList.Add(new SelectListItem { Value = item.mKey, Text = item.mValue });
                    }
                }
            }
            selectList = selectList.GroupBy(x => x.Value).Select(y => y.First()).OrderBy(x => x.Text).ToList();
            selectList = selectList.Where(x => !string.IsNullOrEmpty(x.Value)).ToList();
            return insertSelectListValue(selectList, isOptional, message);
        }


        public static List<SelectListItem> getCountryByCrudePurchase(string crude = "")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                if (!string.IsNullOrEmpty(crude))
                {
                    var query = (from d in context.MT_MATERIALS
                                 where d.MET_CREATE_TYPE == ConstantPrm.SYSTEM.COOL && d.MET_MAT_DES_ENGLISH.ToUpper() == crude.ToUpper()
                                 orderby new { d.MET_MAT_DES_ENGLISH }
                                 select new { mKey = d.MET_ORIG_CRTY, mValue = d.MET_ORIG_CRTY }).Distinct().ToList().OrderBy(x => x.mKey);
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            rtn.Add(new SelectListItem { Value = item.mKey, Text = item.mValue });
                        }
                    }
                    else
                    {
                        rtn.Add(new SelectListItem { Value = "0", Text = "--select--" });
                    }
                }
                else
                {
                    var query = (from d in context.MT_MATERIALS
                                 where d.MET_CREATE_TYPE == ConstantPrm.SYSTEM.COOL
                                 orderby new { d.MET_MAT_DES_ENGLISH }
                                 select new { mKey = d.MET_ORIG_CRTY, mValue = d.MET_ORIG_CRTY }).Distinct().ToList().OrderBy(x => x.mKey);
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            if (item.mValue != null)
                            {
                                rtn.Add(new SelectListItem { Value = item.mKey, Text = item.mValue });
                            }
                        }
                    }
                    else
                    {
                        rtn.Add(new SelectListItem { Value = "0", Text = "--select--" });
                    }
                }




            }
            return rtn;
        }

        public static List<SelectListItem> getMaterialOrigin(bool isOptional = false, string message = "")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterialOrigin().OrderBy(x => x.MET_ORIG_CRTY).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_ORIG_CRTY).Distinct().ToList(), mt_materials.Select(x => x.MET_ORIG_CRTY).Distinct().ToList(), isOptional, message);
        }

        // get Data Nation
        public static List<SelectListItem> getNation()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "I", Text = "INTER" });
            data.Add(new DropdownServiceModel_Data { Value = "T", Text = "THAI" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Create Type
        public static List<SelectListItem> getCreateType(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "SAP", Text = "SAP" });
            data.Add(new DropdownServiceModel_Data { Value = "CPAI", Text = "CPAI" });

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return insertSelectListValue(selectList, isOptional, message);
        }

        // get Data VendorSystem
        public static List<SelectListItem> getVendorCtrlSystem()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = VendorServiceModel.GetSystemForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Vendor Control Type
        public static List<SelectListItem> getVendorCtrlType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = VendorServiceModel.GetTypeForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Vendor 
        public static List<SelectListItem> getVendor(string System, string Type, string Status = "ACTIVE")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();


            selectList = VendorServiceModel.getVendorDDL(System, Type, Status);

            return selectList;
        }


        public static List<SelectListItem> getUserGroup(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            RoleServiceModel rsm = new RoleServiceModel();
            List<ROLE> roles = rsm.getAllRoles();
            foreach (var item in roles)
            {
                selectList.Add(new SelectListItem { Text = item.ROL_NAME, Value = item.ROL_NAME });
            }
            return insertSelectListValue(selectList, isOptional, message);
        }

        // get Data Status
        public static List<SelectListItem> getMasterStatus()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "ACTIVE", Text = "ACTIVE" });
            data.Add(new DropdownServiceModel_Data { Value = "INACTIVE", Text = "INACTIVE" });

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }


        public static List<SelectListItem> getSelectCountDate()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "CD", Text = "CD" });
            data.Add(new DropdownServiceModel_Data { Value = "BD", Text = "BD" });

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }


        // get Data Status
        public static List<SelectListItem> getMasterMenuControlType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "MENU", Text = "MENU" });
            data.Add(new DropdownServiceModel_Data { Value = "FIELD", Text = "FIELD" });

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Status
        public static List<SelectListItem> getMasterADLogin()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "YES", Text = "YES" });
            data.Add(new DropdownServiceModel_Data { Value = "NO", Text = "NO" });

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }


        #endregion

        #region Unit
        public static List<SelectListItem> getUnit(string Status = "ACTIVE")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();


            selectList = UnitServiceModel.getUnitDDL(Status);

            return selectList;
        }




        public static List<SelectListItem> getUnitByArea(string area)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();


            selectList = UnitServiceModel.getUnitByArea(area);

            return selectList;
        }



        public static List<SelectListItem> getFreeUnit()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();


            selectList = UnitServiceModel.getFreeUnit();

            return selectList;
        }


        public static List<SelectListItem> getFreeUnitFromOper()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList = UnitServiceModel.getFreeUnitFromOper();
            return selectList;
        }




        public static List<SelectListItem> getFreeUnitFromOper2(string operid)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList = UnitServiceModel.getFreeUnitFromOper2(operid);
            return selectList;
        }
        #endregion


        #region Material
        public static List<SelectListItem> getMaterial()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();


            selectList = MaterialsServiceModel.getMaterialDDL();

            return selectList;
        }

        public static List<SelectListItem> getAllMaterials()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList = MaterialsServiceModel.getMaterialDDL();
            selectList.AddRange(MaterialsServiceModel.getMaterialsDDL());

            return selectList;
        }
        #endregion

        #region CrudeRef
        public static List<SelectListItem> getCrudeReference(string country = "", string crude_name = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    if (!string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(crude_name))
                    {
                        var qDis = (from l in context.COO_DATA
                                    where l.CODA_COUNTRY == country && l.CODA_CRUDE_NAME == crude_name && l.CODA_STATUS.Equals("APPROVED")
                                    select new { Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();
                        foreach (var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Assay_Ref });
                        }
                    }
                }
                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }
        }


        //GET ASSAY REFERNCE
        public static List<SelectListItem> getAllApprovedCrudeReference(string country = "", string crude_name = "", string assayref = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    if (!string.IsNullOrEmpty(assayref) && assayref != "0")
                    {
                        if (!string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(crude_name))
                        {
                            var qDis = (from l in context.COO_DATA
                                        where l.CODA_COUNTRY.ToUpper() == country.ToUpper() && l.CODA_CRUDE_NAME.ToUpper() == crude_name.ToUpper() && l.CODA_STATUS.Equals("APPROVED") || l.CODA_ASSAY_REF_NO == assayref
                                        select new { Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();

                            foreach (var item in qDis)
                            {
                                selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Assay_Ref });
                            }
                        }
                        else
                        {
                            var qDis = (from l in context.COO_DATA
                                        where l.CODA_STATUS.Equals("APPROVED")
                                        select new { Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();
                            foreach (var item in qDis)
                            {
                                selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Assay_Ref });
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(crude_name))
                        {
                            var qDis = (from l in context.COO_DATA
                                        where l.CODA_COUNTRY.ToUpper() == country.ToUpper() && l.CODA_CRUDE_NAME.ToUpper() == crude_name.ToUpper() && l.CODA_STATUS.Equals("APPROVED")
                                        select new { Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();

                            foreach (var item in qDis)
                            {
                                selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Assay_Ref });
                            }
                        }
                        else
                        {
                            var qDis = (from l in context.COO_DATA
                                        where l.CODA_STATUS.Equals("APPROVED")
                                        select new { Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();
                            foreach (var item in qDis)
                            {
                                selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Assay_Ref });
                            }
                        }
                    }


                }
                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }
        }







        #endregion
        // get Data CustomerSystem
        public static List<SelectListItem> getCustomerCtrlSystem()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = CustomerServiceModel.GetSystemForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Customer Control Type
        public static List<SelectListItem> getCustomerCtrlType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = CustomerServiceModel.GetTypeForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }


        // get Data Country
        public static List<SelectListItem> getVesselActivity(string Type, bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList = VesselActivityServiceModel.GetActivityForDDL(Type);
            selectList.Add(new SelectListItem() { Value = "N|" + CPAIConstantUtil.COLOR_DEFUALT + "|", Text = "Other" });
            //var data = new List<DropdownServiceModel_Data>();

            //data.Add(new DropdownServiceModel_Data { Value = "N|mva000003", Text = "SB : Ship Breakdown" });
            //data.Add(new DropdownServiceModel_Data { Value = "Y|mva000001", Text = "XX : Harbour Steaming" });


            //foreach (var item in data)
            //{
            //    selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            //}

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getVesselActivityList(string Type, bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList = VesselActivityServiceModel.GetVesselActivityList(Type);
            selectList.Add(new SelectListItem { Text = "Other", Value = "Other" });
            return insertSelectListValue(selectList, isOptional, message);
        }


        public static List<SelectListItem> GetDDLFromJson(string sType)
        {
            //List<SelectListItem> ddlTitle = DropdownServiceModel.GetDDLFromJson("title_thai");

            Setting setting = JSONSetting.getSetting("JSON_MT_USER");

            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            if (sType.Equals("title_eng"))
            {
                foreach (var item in setting.title_eng)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("title_thai"))
            {
                foreach (var item in setting.title_thai)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("company"))
            {
                foreach (var item in setting.company)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("system"))
            {
                foreach (var item in setting.system)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("ad_login_flag"))
            {
                foreach (var item in setting.ad_login_flag)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("status"))
            {
                foreach (var item in setting.status)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("user_group"))
            {
                foreach (var item in setting.user_group)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }
            else if (sType.Equals("user_group_system"))
            {
                foreach (var item in setting.user_group_system)
                {
                    data.Add(new DropdownServiceModel_Data { Value = item, Text = item });
                }
            }

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getRoleName(string pStatus = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.ROLE
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.ROL_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.ROL_NAME }
                                    select new { value = d.ROL_ROW_ID.ToUpper(), text = d.ROL_NAME.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.text))
                        {
                            rtn.Add(new SelectListItem { Value = item.value, Text = item.text });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
                return rtn;

            }
        }

        public static List<SelectListItem> getRoleDesc(string pStatus = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.ROLE
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.ROL_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.ROL_DESCRIPTION }
                                    select new { value = d.ROL_DESCRIPTION.ToUpper(), text = d.ROL_DESCRIPTION.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.text))
                        {
                            rtn.Add(new SelectListItem { Value = item.value, Text = item.text });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
                return rtn;

            }
        }


        // get Data Language
        public static List<SelectListItem> getLanguage(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "TH", Text = "TH" });
            data.Add(new DropdownServiceModel_Data { Value = "EN", Text = "EN" });

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return insertSelectListValue(selectList, isOptional, message);
        }

        // get Data Vehicle Control System
        public static List<SelectListItem> getVehicleCtrlSystem(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList = VehicleServiceModel.GetCtrlSystemForDDL();


            return insertSelectListValue(selectList, isOptional, message);
        }

        // get Data Vehicle Control Type
        public static List<SelectListItem> getVehicleCtrlType(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList = VehicleServiceModel.GetCtrlTypeForDDL();


            return insertSelectListValue(selectList, isOptional, message);
        }

        // get Data Materials Control System
        public static List<SelectListItem> getMaterialsCtrlSystem(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList = MaterialsServiceModel.GetCtrlSystemForDDL();


            return insertSelectListValue(selectList, isOptional, message);
        }

        // get Data Plant System
        public static List<SelectListItem> getPlantCtrlSystem()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = PlantServiceModel.GetSystemForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Plant Company
        public static List<SelectListItem> getPlantCompany()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = PlantServiceModel.GetPlantCompanyForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Holiday Type
        public static List<SelectListItem> getHolidayType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "Thai", Text = "Thai" });
            data.Add(new DropdownServiceModel_Data { Value = "UK", Text = "UK" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Vessel Activity Ctrl System
        public static List<SelectListItem> getVesselActivityCtrlSystem()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = VesselActivityServiceModel.GetSystemForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Vessel Activity Ctrl Type
        public static List<SelectListItem> getVesselActivityCtrlType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = VesselActivityServiceModel.GetTypeForDDL("");
            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }
        //get jetty create type
        public static List<SelectListItem> getJettyCreateType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "D", Text = "Discharge port" });
            data.Add(new DropdownServiceModel_Data { Value = "L", Text = "Load port" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        // get Data Port System
        public static List<SelectListItem> getPortCtrlSystem()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();
            data = PortServiceModel.GetSystemForDDL("");

            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getPricingMaterial()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "YMURBAN", Text = "YMURBAN" });
            data.Add(new DropdownServiceModel_Data { Value = "YUPPERZAKUUM", Text = "YUPPERZAKUUM" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getPricingStatus()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "Provisional", Text = "Provisional" });
            data.Add(new DropdownServiceModel_Data { Value = "Final", Text = "Final" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getPricingStructure()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "BASE_PRICE", Text = "BASE_PRICE" });
            data.Add(new DropdownServiceModel_Data { Value = "OSP_PREMIUM_DISCOUNT", Text = "OSP_PREMIUM_DISCOUNT" });
            data.Add(new DropdownServiceModel_Data { Value = "TRADING_PREMIUM_DISCOUNT", Text = "TRADING_PREMIUM_DISCOUNT" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getPricingVariable()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "V1001", Text = "OSP UPPER ZAKUUM(Month)" });
            data.Add(new DropdownServiceModel_Data { Value = "V1002", Text = "OSP_PREMIUM_DISCOUNT" });
            data.Add(new DropdownServiceModel_Data { Value = "V1003", Text = "TRADING_PREMIUM_DISCOUNT" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getPricingType()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "OSP", Text = "OSP" });
            data.Add(new DropdownServiceModel_Data { Value = "DAILY", Text = "DAILY" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getPricingMOPS()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            var data = new List<DropdownServiceModel_Data>();

            data.Add(new DropdownServiceModel_Data { Value = "AAKUB00", Text = "AAKUB00" });
            data.Add(new DropdownServiceModel_Data { Value = "AAILC00", Text = "AAILC00" });


            foreach (var item in data)
            {
                selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            return selectList;
        }

        public static List<SelectListItem> getYearList()
        {
            List<SelectListItem> loading_year = new List<SelectListItem>();
            int year = DateTime.Now.Year;
            int max_year = year + 5;
            int min_year = year - 5;
            for (int i = max_year; i >= min_year; i--)
            {
                loading_year.Add(new SelectListItem { Text = i + "", Value = i + "", Selected = (i == year) ? true : false });
            }
            return loading_year;
        }

        public static List<SelectListItem> getMonthList()
        {
            List<SelectListItem> loading_month = new List<SelectListItem>();
            loading_month.Add(new SelectListItem { Text = "Jan", Value = "Jan" });
            loading_month.Add(new SelectListItem { Text = "Feb", Value = "Feb" });
            loading_month.Add(new SelectListItem { Text = "Mar", Value = "Mar" });
            loading_month.Add(new SelectListItem { Text = "Apr", Value = "Apr" });
            loading_month.Add(new SelectListItem { Text = "May", Value = "May" });
            loading_month.Add(new SelectListItem { Text = "Jun", Value = "Jun" });
            loading_month.Add(new SelectListItem { Text = "Jul", Value = "Jul" });
            loading_month.Add(new SelectListItem { Text = "Aug", Value = "Aug" });
            loading_month.Add(new SelectListItem { Text = "Sep", Value = "Sep" });
            loading_month.Add(new SelectListItem { Text = "Oct", Value = "Oct" });
            loading_month.Add(new SelectListItem { Text = "Nov", Value = "Nov" });
            loading_month.Add(new SelectListItem { Text = "Dec", Value = "Dec" });
            return loading_month;
        }
        //Get deduct action spot in vessel 
        public static List<SelectListItem> getDeductActionList()
        {
            List<SelectListItem> deduct_action = new List<SelectListItem>();
            deduct_action.Add(new SelectListItem { Text = "Calc. % commission before incl. additional expense", Value = "Deduct Before" });
            deduct_action.Add(new SelectListItem { Text = "Calc. % commission after incl. additional expense", Value = "Deduct After" });
            return deduct_action;
        }

        public static List<SelectListItem> getFormula()
        {
            List<FORMULA> formula = FORMULA_DAL.GetFormula();
            return insertSelectListValue(formula.Select(x => x.FOR_NAME).ToList(), formula.Select(x => x.FOR_CODE).ToList());
        }

        public static List<SelectListItem> getIncoterms()
        {
            List<MT_INCOTERMS> incoterms = MT_INCOTERMS_DAL.GetIncoterms().OrderBy(x => x.MIN_INCOTERMS).ToList();
            return insertSelectListValue(incoterms.Select(x => x.MIN_INCOTERMS).ToList(), incoterms.Select(x => x.MIN_INCOTERMS).ToList());

        }

        public static List<SelectListItem> getDelegateSystem(string id)
        {
            List<string> data = CPAI_DELEGATE_AUTH_DAL.GetDelegateSystem(id).ToList();
            return insertSelectListValue(data.ToList(), data.ToList());
        }

        public static List<SelectListItem> getDelegateGroup(string id, string system)
        {
            List<string> data = CPAI_DELEGATE_AUTH_DAL.GetDelegateGroup(id, system).ToList();
            return insertSelectListValue(data.ToList(), data.ToList());
        }

        public static List<SelectListItem> getCustData()
        {
            List<MT_CUST_DATA> mt_cust = MT_CUST_DAL.GetCUSTData().ToList();
            List<MT_CUST_DETAIL> mt_custD = MT_CUST_DAL.GetCUSTDetail().ToList();
            var X = from cust in mt_cust
                    join custD in mt_custD on cust.MCS_FK_CUS equals custD.MCD_FK_CUS
                    where custD.MCD_NATION.ToUpper() == "I" && custD.MCD_FK_COMPANY == "1100"
                    select new SelectListItem { Value = cust.MCS_ROW_ID, Text = custD.MCD_NAME_1 };
            X = X.Distinct().ToList();
            return insertSelectListValue(X.Select(x => x.Text).ToList(), X.Select(x => x.Value).ToList());
        }

        public static List<SelectListItem> getGICLEARLINEType()
        {
            List<SelectListItem> type = new List<SelectListItem>();
            type.Add(new SelectListItem { Text = "GI Clear Line", Value = "GI Clear Line" });
            type.Add(new SelectListItem { Text = "Borrow", Value = "Borrow" });
            type.Add(new SelectListItem { Text = "Swap", Value = "Swap" });


            return type;
        }

        public static List<SelectListItem> getVCoolCrudeName()
        {
            List<VCO_DATA> data = VCO_DATA_DAL.GetAllCrude();
            List<string> crude_name = data.Select(x => x.VCDA_CRUDE_NAME.ToUpper()).Distinct().ToList();
            return insertSelectListValue(crude_name, crude_name);
        }

        public static List<SelectListItem> getToolKnock()
        {
            Setting setting = JSONSetting.getSetting(JSON_TOOL);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.knock_option.Count; i++)
                list.Add(new SelectListItem { Text = setting.knock_option[i].value, Value = setting.knock_option[i].key });
            return insertSelectListValue(list);
        }

        public static List<SelectListItem> getToolOption()
        {
            Setting setting = JSONSetting.getSetting(JSON_TOOL);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.condition_option.Count; i++)
                list.Add(new SelectListItem { Text = setting.condition_option[i].value, Value = setting.condition_option[i].key });
            return insertSelectListValue(list);
        }

        public static List<SelectListItem> getToolConditionOperand()
        {
            Setting setting = JSONSetting.getSetting(JSON_TOOL);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.condition_operand_option.Count; i++)
                list.Add(new SelectListItem { Text = setting.condition_operand_option[i].value, Value = setting.condition_operand_option[i].key });
            return insertSelectListValue(list);
        }

        public static List<SelectListItem> getToolConditionVariable()
        {
            Setting setting = JSONSetting.getSetting(JSON_TOOL);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.formular_valiable.Count; i++)
                list.Add(new SelectListItem { Text = setting.formular_valiable[i].value, Value = setting.formular_valiable[i].key });
            return insertSelectListValue(list);
        }

        public static List<SelectListItem> getToolFormularOprator()
        {
            Setting setting = JSONSetting.getSetting(JSON_TOOL);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.condition_operator_formular.Count; i++)
                list.Add(new SelectListItem { Text = setting.condition_operator_formular[i].value, Value = setting.condition_operator_formular[i].key });
            return insertSelectListValue(list);
        }

        public static List<SelectListItem> getToolConditionOperator()
        {
            Setting setting = JSONSetting.getSetting(JSON_TOOL);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.condition_operator_option.Count; i++)
                list.Add(new SelectListItem { Text = setting.condition_operator_option[i].value, Value = setting.condition_operator_option[i].key });
            return insertSelectListValue(list);
        }

        #region --- Hedging ---
        public static List<SelectListItem> getCreditRating()
        {
            List<MT_CREDIT_RATING> crList = MT_CREDIT_RATING_DAL.GetAllCreditRating().ToList();
            return insertSelectListValue(crList.Select(x => x.MCR_RATING).ToList(), crList.Select(x => x.MCR_ROW_ID).ToList());
        }

        public static List<SelectListItem> getUnderlying(bool isOptional = false, string message = "", string system = "", bool isUnit = false)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            Setting setting = JSONSetting.getSetting(JSON_HEDG);


            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            join vc in context.MT_MATERIALS_CONTROL on v.MET_NUM equals vc.MMC_FK_MATRIALS
                            where vc.MMC_SYSTEM.ToUpper() == system && vc.MMC_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                            select new
                            {
                                name = vc.MMC_NAME,
                                nameEng = v.MET_MAT_DES_ENGLISH,
                                underlying = v.MET_NUM,
                                unitprice = vc.MMC_UNIT_PRICE

                            };
                foreach (var g in query)
                {
                    var querySetting = setting.unitBig.Where(p => p.price.Equals(g.unitprice)).FirstOrDefault();
                    if (querySetting != null)
                        insertSelectListValue(list, g.name, g.nameEng, g.underlying, g.unitprice, querySetting.volume, querySetting.volume_mnt, isUnit);
                }

            }
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getUnitVolume(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unitBig.Count; i++)
                list.Add(new SelectListItem { Text = setting.unitBig[i].volume, Value = setting.unitBig[i].volume });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getUnitVolumeMnt(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unitBig.Count; i++)
                list.Add(new SelectListItem { Text = setting.unitBig[i].volume_mnt, Value = setting.unitBig[i].volume_mnt });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getUnitPrice(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unitBig.Count; i++)
                list.Add(new SelectListItem { Text = setting.unitBig[i].price, Value = setting.unitBig[i].price });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getUnitFee(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fee.Count; i++)
                list.Add(new SelectListItem { Text = setting.fee[i].key, Value = setting.fee[i].key });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getUnitPrice()
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unitBig.Count; i++)
                list.Add(new SelectListItem { Text = setting.unitBig[i].price, Value = setting.unitBig[i].price });
            return list;
        }

        public static List<SelectListItem> getHedgeType(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.hedge_type.Count; i++)
                list.Add(new SelectListItem { Text = setting.hedge_type[i].value, Value = setting.hedge_type[i].key });
            return insertSelectListValue(list, isOptional, message);
        }

        //public static List<SelectListItem> getHedgeTypeForDeal(bool isOptional = false, string message = "", string date = "", string underlying = "")
        //{
        //    Setting setting = JSONSetting.getSetting(JSON_HEDG);
        //    List<SelectListItem> selectList = new List<SelectListItem>();
        //    using (EntityCPAIEngine context = new EntityCPAIEngine())
        //    {
        //        DateTime? pDate = ShareFn.ConvertStringDateFormatToDatetime(date.Substring(0, 10), "dd/MM/yyyy");

        //        var query = from v in context.HEDG_FW_ANNUAL
        //                    join vc in context.HEDG_FW_ANNUAL_DETAIL on v.HFT_ROW_ID equals vc.HFTD_FK_FW_ANNUAL
        //                    where
        //                    v.HFT_FK_UNDERLYING == underlying
        //                    && v.HFT_ACTIVEDATE_START <= pDate
        //                    && v.HFT_ACTIVEDATE_END >= pDate
        //                    && vc.HFTD_HEDGE_TYPE != "CROPPLAN"
        //                    group v by vc.HFTD_HEDGE_TYPE into r
        //                    select new
        //                    {
        //                        HFT_ROW_ID = r.Max(d => d.HFT_ROW_ID),
        //                        HFTD_HEDGE_TYPE = r.Key
        //                    };

        //        var query2 = from v in query.AsEnumerable()
        //                     select new
        //                     {
        //                         HFT_ROW_ID = v.HFT_ROW_ID,
        //                         HFTD_HEDGE_TYPE = v.HFTD_HEDGE_TYPE,
        //                         TEXT = (from p in setting.hedge_type where p.key == v.HFTD_HEDGE_TYPE select p.value).FirstOrDefault()
        //                     };

        //        var query3 = query2.AsEnumerable()
        //                    .Select(x => new SelectListItem
        //                    {
        //                        Value = string.Format("{0}|{1}", x.HFT_ROW_ID, x.HFTD_HEDGE_TYPE),
        //                        Text = x.TEXT
        //                    });

        //        selectList = query3.ToList();
        //    }

        //    return insertSelectListValue(selectList, isOptional, message);
        //}

        public static List<SelectListItem> getType(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.annual_fmw_type.Count; i++)
                list.Add(new SelectListItem { Text = setting.annual_fmw_type[i].value, Value = setting.annual_fmw_type[i].key });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getValueType(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.annual_fmw_value_type.Count; i++)
                list.Add(new SelectListItem { Text = setting.annual_fmw_value_type[i].value, Value = setting.annual_fmw_value_type[i].key });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getStatus(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList.Add(new SelectListItem { Text = "DRAFT", Value = "DRAFT" });
            selectList.Add(new SelectListItem { Text = "SUBMIT", Value = "SUBMIT" });
            selectList.Add(new SelectListItem { Text = "CANCEL", Value = "CANCEL" });

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getStatus(bool isOptional = false, string message = "", HedgeStatus hedgeStatus = HedgeStatus.PreDeal)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            if (hedgeStatus.Equals(HedgeStatus.PreDeal))
            {
                selectList.Add(new SelectListItem { Text = "DRAFT", Value = "DRAFT" });
                selectList.Add(new SelectListItem { Text = "SUBMIT", Value = "SUBMIT" });
                selectList.Add(new SelectListItem { Text = "CANCEL", Value = "CANCEL" });
            }
            else if (hedgeStatus.Equals(HedgeStatus.Deal))
            {
                selectList.Add(new SelectListItem { Text = "DRAFT", Value = "DRAFT" });
                selectList.Add(new SelectListItem { Text = "SUBMIT", Value = "SUBMIT" });
                selectList.Add(new SelectListItem { Text = "HOLD", Value = "HOLD" });
                selectList.Add(new SelectListItem { Text = "TICKETED", Value = "TICKETED" });
                selectList.Add(new SelectListItem { Text = "APPROVED", Value = "APPROVED" });
                selectList.Add(new SelectListItem { Text = "COMPLETED", Value = "COMPLETED" });
            }
            else if (hedgeStatus.Equals(HedgeStatus.Ticket))
            {
                selectList.Add(new SelectListItem { Text = "DRAFT", Value = "DRAFT" });
                selectList.Add(new SelectListItem { Text = "WAITING VERIFY", Value = "WAITING VERIFY" });
                selectList.Add(new SelectListItem { Text = "WAITING ENDORSE", Value = "WAITING ENDORSE" });
                selectList.Add(new SelectListItem { Text = "WAITING APPROVE", Value = "WAITING APPROVE" });
                selectList.Add(new SelectListItem { Text = "APPROVED", Value = "APPROVED" });
            }

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getTradeFor(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList.Add(new SelectListItem { Text = "LABIX", Value = "T" });
            selectList.Add(new SelectListItem { Text = "NO LABIX", Value = "F" });


            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getStatusManagementCounterparty(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList.Add(new SelectListItem { Text = "SUSPEND", Value = "SUSPEND" });
            selectList.Add(new SelectListItem { Text = "NORMAL", Value = "NORMAL" });

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getMonth(bool isOptional = false, string message = "", bool isShortMnt = false, bool isNumberValue = false)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            int month;

            month = 0;
            if (isShortMnt)
                month = 1;
            switch (month)
            {
                case 0:
                    {
                        if (!isNumberValue)
                        {
                            selectList.Add(new SelectListItem { Text = "January", Value = "Jan" });
                            selectList.Add(new SelectListItem { Text = "February", Value = "Feb" });
                            selectList.Add(new SelectListItem { Text = "March", Value = "Mar" });
                            selectList.Add(new SelectListItem { Text = "April", Value = "Apr" });
                            selectList.Add(new SelectListItem { Text = "May", Value = "May" });
                            selectList.Add(new SelectListItem { Text = "June", Value = "Jun" });
                            selectList.Add(new SelectListItem { Text = "July", Value = "Jul" });
                            selectList.Add(new SelectListItem { Text = "August", Value = "Aug" });
                            selectList.Add(new SelectListItem { Text = "September", Value = "Sep" });
                            selectList.Add(new SelectListItem { Text = "October", Value = "Oct" });
                            selectList.Add(new SelectListItem { Text = "November", Value = "Nov" });
                            selectList.Add(new SelectListItem { Text = "December", Value = "Dec" });
                        }
                        else
                        {
                            selectList.Add(new SelectListItem { Text = "January", Value = "01" });
                            selectList.Add(new SelectListItem { Text = "February", Value = "02" });
                            selectList.Add(new SelectListItem { Text = "March", Value = "03" });
                            selectList.Add(new SelectListItem { Text = "April", Value = "04" });
                            selectList.Add(new SelectListItem { Text = "May", Value = "05" });
                            selectList.Add(new SelectListItem { Text = "June", Value = "06" });
                            selectList.Add(new SelectListItem { Text = "July", Value = "07" });
                            selectList.Add(new SelectListItem { Text = "August", Value = "08" });
                            selectList.Add(new SelectListItem { Text = "September", Value = "09" });
                            selectList.Add(new SelectListItem { Text = "October", Value = "10" });
                            selectList.Add(new SelectListItem { Text = "November", Value = "11" });
                            selectList.Add(new SelectListItem { Text = "December", Value = "12" });
                        }
                    }
                    break;
                case 1:
                    {
                        if (!isNumberValue)
                        {
                            selectList.Add(new SelectListItem { Text = "Jan", Value = "Jan" });
                            selectList.Add(new SelectListItem { Text = "Feb", Value = "Feb" });
                            selectList.Add(new SelectListItem { Text = "Mar", Value = "Mar" });
                            selectList.Add(new SelectListItem { Text = "Apr", Value = "Apr" });
                            selectList.Add(new SelectListItem { Text = "May", Value = "May" });
                            selectList.Add(new SelectListItem { Text = "Jun", Value = "Jun" });
                            selectList.Add(new SelectListItem { Text = "Jul", Value = "Jul" });
                            selectList.Add(new SelectListItem { Text = "Aug", Value = "Aug" });
                            selectList.Add(new SelectListItem { Text = "Sep", Value = "Sep" });
                            selectList.Add(new SelectListItem { Text = "Oct", Value = "Oct" });
                            selectList.Add(new SelectListItem { Text = "Nov", Value = "Nov" });
                            selectList.Add(new SelectListItem { Text = "Dec", Value = "Dec" });
                        }
                        else
                        {
                            selectList.Add(new SelectListItem { Text = "Jan", Value = "01" });
                            selectList.Add(new SelectListItem { Text = "Feb", Value = "02" });
                            selectList.Add(new SelectListItem { Text = "Mar", Value = "03" });
                            selectList.Add(new SelectListItem { Text = "Apr", Value = "04" });
                            selectList.Add(new SelectListItem { Text = "May", Value = "05" });
                            selectList.Add(new SelectListItem { Text = "Jun", Value = "06" });
                            selectList.Add(new SelectListItem { Text = "Jul", Value = "07" });
                            selectList.Add(new SelectListItem { Text = "Aug", Value = "08" });
                            selectList.Add(new SelectListItem { Text = "Sep", Value = "09" });
                            selectList.Add(new SelectListItem { Text = "Oct", Value = "10" });
                            selectList.Add(new SelectListItem { Text = "Nov", Value = "11" });
                            selectList.Add(new SelectListItem { Text = "Dec", Value = "12" });
                        }
                    }
                    break;
            }

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getYearCrop(bool isOptional = false, string message = "", string defaultYear = "", int numYearBefore = 0, int numYearAfter = 0)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            int year;

            try
            {
                if (defaultYear.Trim().Equals(string.Empty))
                {
                    year = DateTime.Now.Year;
                    selectList.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });
                }
                else
                {
                    year = Convert.ToInt16(defaultYear.Trim());
                    selectList.Add(new SelectListItem { Text = year.ToString(), Value = year.ToString() });
                }
                for (int i = year - 1; i >= year - numYearBefore; i--)
                    selectList.Insert(0, new SelectListItem { Text = i.ToString(), Value = i.ToString() });
                for (int i = year + 1; i < (year + numYearAfter + 1); i++)
                    selectList.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            catch
            {
            }

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getDealType(bool isOptional = false, string message = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList.Add(new SelectListItem { Text = "SELL", Value = "SEL" });
            selectList.Add(new SelectListItem { Text = "BUY", Value = "BUY" });

            return insertSelectListValue(selectList, isOptional, message);
        }

        public static List<SelectListItem> getTool(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.tool.Count; i++)
                list.Add(new SelectListItem { Text = setting.tool[i].value, Value = setting.tool[i].key });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> getUnitPayment(bool isOptional = false, string message = "")
        {
            Setting setting = JSONSetting.getSetting(JSON_HEDG);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.unit_payment.Count; i++)
                list.Add(new SelectListItem { Text = setting.unit_payment[i].value, Value = setting.unit_payment[i].key });
            return insertSelectListValue(list, isOptional, message);
        }

        public static List<SelectListItem> GetHEDGCompany()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 where v.MCO_CREATE_TYPE != "CPAI" /*&& v.MCO_STATUS == "ACTIVE"*/
                                 orderby v.MCO_COMPANY_CODE
                                 select new
                                 {
                                     dCompanyCode = v.MCO_COMPANY_CODE,
                                     dFullName = v.MCO_SHORT_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.dFullName, Value = g.dCompanyCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public static List<SelectListItem> getFormulaByTool()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<HEDG_MT_FORMULA> itemList = HEDG_MT_FORMULAR_DAL.GetAllFormula().ToList();
            for (int i = 0; i < itemList.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = itemList[i].HMFM_NAME, Value = itemList[i].HMFM_ROW_ID + "|" + itemList[i].HMFM_TEXT });
            }
            return selectList;
        }
        #endregion
    }
}