﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Collections;
using com.pttict.downstream.common.utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{

    public class FeedstockBookingPriceServiceModel : BasicBean
    {
        private CultureInfo provider = new CultureInfo("en-US");
        private string format = "dd/MM/yyyy";
        ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private static readonly ILog Log =
        //      LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public void getProductList(ref FeedstockBookingPriceViewModel pModel)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                CultureInfo provider = new CultureInfo("en-US");
                string format = "dd/MM/yyyy";
                var sDateFrom = String.IsNullOrEmpty(pModel.data_Search.vUpdatePeriod) ? "" : pModel.data_Search.vUpdatePeriod.ToUpper().Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(pModel.data_Search.vUpdatePeriod) ? "" : pModel.data_Search.vUpdatePeriod.ToUpper().Substring(14, 10);
                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);
                //where b.PO_DATE >= dateFrom && b.PO_DATE <= dateTo

                var queryVal = from h in context.PIT_PO_HEADER
                               join i in context.PIT_PO_ITEM on h.PO_NO equals i.PO_NO
                               where h.PO_DATE >= dateFrom && h.PO_DATE <= dateTo
                               select new
                               {
                                   PO_NO = h.PO_NO,
                                   Trip_No = i.TRIP_NO,
                                   Company = h.COMPANY_CODE,
                                   Supplier = h.ACC_NUM_VENDOR,
                                   Feedstock = i.MET_NUM,
                                   Incoterms = h.INCOTERMS,
                                   Volume = i.VOLUME,
                                   VolumeUnit = i.VOLUME_UNIT,
                                   PO_DATE = h.PO_DATE,
                                   Cargo = h.CARGO_NO
                               };
                List<ProductDetail> tempData = new List<ProductDetail>();
                var queryTemp = queryVal.ToList();
                for (int i = 0; i < queryTemp.Count(); i++)
                {
                    string pCompany = "";
                    if (pModel.ddl_Company == null)
                    {
                        List<SelectListItem> companyList = new List<SelectListItem>();
                        var ddlCompany = DropdownServiceModel.getCompany();
                        var companyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").MT_COMPANY;
                        foreach (var c in ddlCompany)
                        {
                            var result = companyGlobalConfig.Where(x => x.CODE == c.Value).ToList();
                            if (result.Count != 0)
                            {
                                companyList.Add(c);
                            }
                        }
                        pModel.ddl_Company = companyList;
                        pModel.ddl_Supplier = DropdownServiceModel.getVendorPIT();
                        ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PurchaseOrderController item = new ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PurchaseOrderController();
                        pModel.ddl_Feedstock = item.GetFeedStockPIT();

                    }
                    if (pModel.ddl_Company.Where(p => p.Value == queryTemp[i].Company).Count() > 0)
                    {//เอาแค่ 1100 TOP
                        if (pModel.ddl_Company.Where(p => p.Value == queryTemp[i].Company).FirstOrDefault().Text.ToUpper() == "TOP")
                        {
                            pCompany = pModel.ddl_Company.Where(p => p.Value == queryTemp[i].Company).FirstOrDefault().Text;

                            string pSupplier = "";
                            if (pModel.ddl_Supplier.Where(p => p.Value.PadLeft(10, '0') == queryTemp[i].Supplier).Count() > 0)
                                pSupplier = pModel.ddl_Supplier.Where(p => p.Value.PadLeft(10, '0') == queryTemp[i].Supplier).FirstOrDefault().Text;
                            string pFeedstock = "";
                            if (pModel.ddl_Feedstock.Where(p => p.Value == queryTemp[i].Feedstock).Count() > 0)
                                pFeedstock = pModel.ddl_Feedstock.Where(p => p.Value == queryTemp[i].Feedstock).FirstOrDefault().Text;

                            ProductDetail temp = new ProductDetail();
                            temp.PO_NO = queryTemp[i].PO_NO;
                            temp.Trip_No = queryTemp[i].Trip_No;
                            temp.Company = pCompany;
                            temp.Supplier = pSupplier;
                            temp.Feedstock = pFeedstock;
                            temp.Feedstock_CodeName = queryTemp[i].Feedstock + "|" + pFeedstock;
                            temp.Incoterms = queryTemp[i].Incoterms;
                            temp.Volume = queryTemp[i].Volume.ToString();
                            temp.VolumeUnit = queryTemp[i].VolumeUnit.ToString();
                            temp.PO_Date = (queryTemp[i].PO_DATE ?? DateTime.Now.AddYears(100)).ToString(format, provider);
                            temp.CargoNo = queryTemp[i].Cargo;

                            DateTime date = queryTemp[i].PO_DATE ?? DateTime.Now.AddYears(100);
                            dateFrom = DateTime.ParseExact(
                                "01/" + (date.Month).ToString().PadLeft(2, '0') + "/" + (date.Year).ToString()
                                , format, provider);
                            dateTo = DateTime.ParseExact(
                                "15/" + (date.Month).ToString().PadLeft(2, '0') + "/" + (date.Year).ToString()
                                , format, provider);
                            temp.Date_1ST_Half = dateFrom.ToString(format, provider) + " " + dateTo.ToString(format, provider);
                            temp.IsFinal_1ST_Half = (from p in context.PIT_PO_PRICE
                                                     where p.PO_NO == temp.PO_NO && p.INVOICE_STATUS == "FINAL"
                                                     && p.PO_DATE >= dateFrom && p.PO_DATE <= dateTo
                                                     select new
                                                     {
                                                         PO_DATE = p.PO_DATE
                                                     }).ToList().Count;

                            dateFrom = DateTime.ParseExact(
                                    "16/" + (date.Month).ToString().PadLeft(2, '0') + "/" + (date.Year).ToString()
                                    , format, provider);
                            dateTo = DateTime.ParseExact(
                                "01/" + (date.Month).ToString().PadLeft(2, '0') + "/" + (date.Year).ToString()
                                , format, provider).AddMonths(1).AddDays(-1);
                            temp.Date_2ND_Half = dateFrom.ToString(format, provider) + " " + dateTo.ToString(format, provider);
                            temp.IsFinal_2ND_Half = (from p in context.PIT_PO_PRICE
                                                     where p.PO_NO == temp.PO_NO && p.INVOICE_STATUS == "FINAL"
                                                     && p.PO_DATE >= dateFrom && p.PO_DATE <= dateTo
                                                     select new
                                                     {
                                                         PO_DATE = p.PO_DATE
                                                     }).ToList().Count;
                            tempData.Add(temp);
                        }
                    }
                }
                pModel.data_Search.sPriceDetail.dProductList = tempData;
            }

        }
        public CONFIG_INTERCOMPANY getCIPConfig(String KeyName)
        {
            string JsonD = MasterData.GetJsonMasterSetting(KeyName);
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);
            CONFIG_INTERCOMPANY dataList = (CONFIG_INTERCOMPANY)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(CONFIG_INTERCOMPANY));

            return dataList;
        }

        public static List<SelectListItem> getPONOlist_DDL(string company, string supplier, string fromTo)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                CultureInfo provider = new CultureInfo("en-US");
                string format = "dd/MM/yyyy";
                var sDateFrom = String.IsNullOrEmpty(fromTo) ? "" : fromTo.ToUpper().Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(fromTo) ? "" : fromTo.ToUpper().Substring(14, 10);
                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);
                string Lsupplier = supplier.PadLeft(10, '0');

                var queryTemp = from v in context.PIT_PO_HEADER
                                where v.COMPANY_CODE == company && v.ACC_NUM_VENDOR == Lsupplier
                                && v.PO_DATE >= dateFrom && v.PO_DATE <= dateTo
                                select v;

                List<PIT_PO_HEADER> tempData = queryTemp.ToList();
                return insertSelectListValue(tempData.Select(x => x.PO_NO).ToList(), tempData.Select(x => x.PO_NO).ToList(), false, "");
            }

        }

        public static List<QualityProductDetail> getProductQuality(string po_no, string feedstock, string fromTo, string invoiceNOStatus, string tripNo, string supplier)
        {
            List<QualityProductDetail> tempData = new List<QualityProductDetail>();
            try
            {
                //JObject json = JObject.Parse(string.IsNullOrEmpty(exchangeRate) ? "[{}]" : exchangeRate);
                //List<TrnFXSearchDetail> exchangeList = (List<TrnFXSearchDetail>)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(List<TrnFXSearchDetail>));

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    CultureInfo provider = new CultureInfo("en-US");
                    string format = "dd/MM/yyyy";
                    var sDateFrom = String.IsNullOrEmpty(fromTo) ? "" : fromTo.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(fromTo) ? "" : fromTo.ToUpper().Substring(11, 10);
                    DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);
                    int sumDate = dateTo.Subtract(dateFrom).Days + 1;

                    string[] feedStockValue = feedstock.Split('|');
                    string feedCode = "";
                    string feedDesc = "";
                    if (feedStockValue.Length == 2)
                    {
                        feedCode = feedStockValue[0];
                        feedDesc = feedStockValue[1];
                    }

                    var queryTemp = (from h in context.PIT_PO_HEADER
                                     join i in context.PIT_PO_ITEM on h.PO_NO equals i.PO_NO
                                     join p in context.PIT_PO_PRICE on new { i.PO_NO, i.PO_ITEM } equals new { p.PO_NO, p.PO_ITEM }

                                     where i.PO_NO == po_no && p.PO_DATE >= dateFrom && p.PO_DATE <= dateTo
                                     && i.MET_NUM == feedCode && p.INVOICE_STATUS == invoiceNOStatus
                                     select new
                                     {
                                         NO = "0",
                                         PO_NO = p.PO_NO,
                                         PO_ITEM = p.PO_ITEM,
                                         PO_DATE = p.PO_DATE,
                                         INVOICE_NO = p.INVOICE_NO,
                                         INVOICE_STATUS = p.INVOICE_STATUS,
                                         TRIP_NO = i.TRIP_NO,
                                         PRODUCT = feedDesc,
                                         VOLUMN_MT = p.BILLING_VOLUME_MT,
                                         DENSITY15C = p.DENSITY15C,
                                         VISCOSITY50C = p.VISCOSITY50C,
                                         SULPHUR = p.SULPHUR,
                                         NEW_UNIT_PRICE_USD = p.NEW_UNIT_PRICE_USD,
                                         OLD_UNIT_PRICE_USD = p.OLD_UNIT_PRICE_USD,
                                         ADJUST_PRICE_USD = p.ADJUST_PRICE_USD,
                                         EXCHANGE_RATE_NEW = p.NEW_ROE,
                                         EXCHANGE_RATE_OLD = p.OLD_ROE,
                                         NEW_UNIT_PRICE_THB = p.NEW_UNIT_PRICE_THB,
                                         OLD_UNIT_PRICE_THB = p.OLD_UNIT_PRICE_THB,
                                         PRICE_THB = p.PRICE_THB,
                                         VAT_THB = p.VAT_THB,
                                         TOTAL_THB = p.TOTAL_THB,
                                         UNIT_PRICE = p.UNIT_PRICE
                                     }).ToList();

                    //รอคอนเฟิร์มว่า 1 PO มีแค่ 1 Bill หรือเท่าจำนวนวัน
                    var queryBill = (from a in context.PIT_BILLING
                                     where a.PO_NO == po_no && a.BILLING_DATE >= dateFrom && a.BILLING_DATE <= dateTo
                                     select a).ToList();

                    var queryMainNull = from h in context.PIT_PO_HEADER
                                        join i in context.PIT_PO_ITEM on h.PO_NO equals i.PO_NO
                                        where h.PO_DATE >= dateFrom && h.PO_DATE <= dateTo
                                        select new
                                        {
                                            PO_NO = h.PO_NO,
                                            POItem = h.PIT_PO_ITEM,
                                            PODate = h.PO_DATE,
                                            Trip_No = i.TRIP_NO,
                                            Volume = i.VOLUME
                                        };
                    
                    if(string.IsNullOrEmpty(po_no))
                    {
                        if (queryTemp != null)
                        {
                            if (queryTemp.ToList().Count > 0)
                            {
                                if (!string.IsNullOrEmpty(queryTemp[0].PO_NO))
                                {
                                    po_no = queryTemp[0].PO_NO;
                                }
                            }
                        }
                    }
                    
                    for (int i = 0; i < sumDate; i++)
                    {
                        DateTime tempPODate = dateFrom.AddDays(i);
                        var tempMain = queryTemp.Where(p => p.PO_NO == po_no && p.PO_DATE == tempPODate).ToList();
                        QualityProductDetail temp = new QualityProductDetail();
                        temp.NO = (i + 1).ToString();
                        temp.PODate = tempPODate.ToString(format, provider);

                        if (tempMain.Count() > 0)
                        {
                            temp.PO_NO = po_no;
                            temp.PO_ITEM = tempMain[0].PO_ITEM.ToString();
                            //temp.PODate = tempMain[0].PO_DATE.ToString(format, provider);
                            temp.InvoiceNo = tempMain[0].INVOICE_NO ?? "";
                            temp.InvoiceNoStatus = tempMain[0].INVOICE_STATUS ?? "";
                            temp.TripNo = tripNo;
                            temp.IsThaiLube = supplier.ToUpper().Contains("LUBE") ? "1" : "0";
                            temp.Product = feedDesc;
                            temp.Product_Code = feedCode;
                            temp.VolumnMT = tempMain[0].VOLUMN_MT == null ? "0" : tempMain[0].VOLUMN_MT.ToString();
                            temp.Den_15 = tempMain[0].DENSITY15C == null ? "0" : tempMain[0].DENSITY15C.ToString();
                            temp.Visc_cst_50 = tempMain[0].VISCOSITY50C == null ? "0" : tempMain[0].VISCOSITY50C.ToString();
                            temp.Sulfur = tempMain[0].SULPHUR == null ? "0" : tempMain[0].SULPHUR.ToString();
                            temp.UnitPriceUSDNew = tempMain[0].NEW_UNIT_PRICE_USD == null ? "0" : tempMain[0].NEW_UNIT_PRICE_USD.ToString();
                            temp.UnitPriceUSDOld = tempMain[0].OLD_UNIT_PRICE_USD == null ? "0" : tempMain[0].OLD_UNIT_PRICE_USD.ToString();
                            temp.AdjustPrice = tempMain[0].ADJUST_PRICE_USD == null ? "0" : tempMain[0].ADJUST_PRICE_USD.ToString();
                            temp.ExchangeRateNew = tempMain[0].EXCHANGE_RATE_NEW == null ? "0" : tempMain[0].EXCHANGE_RATE_NEW.ToString();
                            temp.ExchangeRateOld = tempMain[0].EXCHANGE_RATE_OLD == null ? "0" : tempMain[0].EXCHANGE_RATE_OLD.ToString();
                            temp.UnitPriceTHBNew = tempMain[0].NEW_UNIT_PRICE_THB == null ? "0" : tempMain[0].NEW_UNIT_PRICE_THB.ToString();
                            temp.UnitPriceTHBOld = tempMain[0].OLD_UNIT_PRICE_THB == null ? "0" : tempMain[0].OLD_UNIT_PRICE_THB.ToString();
                            temp.Price = tempMain[0].PRICE_THB == null ? "0" : tempMain[0].PRICE_THB.ToString();
                            temp.Vat = tempMain[0].VAT_THB == null ? "0" : tempMain[0].VAT_THB.ToString();
                            temp.Total = tempMain[0].TOTAL_THB == null ? "0" : tempMain[0].TOTAL_THB.ToString();
                            temp.UnitPrice = tempMain[0].UNIT_PRICE == null ? "0" : tempMain[0].UNIT_PRICE.ToString();

                            var tempBill = queryBill.Where(p => p.PO_NO == po_no && p.BILLING_DATE == tempPODate).ToList();

                            if (tempBill.Count > 0)
                            {
                                if(!string.IsNullOrEmpty(tempBill[0].INVOICE_NO))
                                {
                                    temp.InvoiceNo = tempBill[0].INVOICE_NO;
                                }
                                if (tempBill[0].TOTAL != null && tempBill[0].CURRENCY_ZWHO != null)
                                {
                                    decimal tempVal = 0;
                                    if (tempBill[0].SALES_ORG == "1700")
                                    {
                                        if (tempBill[0].SALES_UNIT == "BBL")
                                        {
                                            tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_BBL ?? (decimal)0);
                                            tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);

                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_BBL.ToString();
                                        }
                                        else if (tempBill[0].SALES_UNIT == "MT")
                                        {
                                            tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_MT ?? (decimal)0);
                                            tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);

                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_MT.ToString();
                                        }
                                        else if (tempBill[0].SALES_UNIT == "L30")
                                        {
                                            tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_L30 ?? (decimal)0);
                                            tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);

                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_L30.ToString();
                                        }

                                        temp.invoice_amount = Math.Round(tempVal, 2, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else
                                    {
                                        if (tempBill[0].SALES_UNIT == "BBL")
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_BBL.ToString();
                                        }
                                        else if (tempBill[0].SALES_UNIT == "MT")
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_MT.ToString();
                                        }
                                        else if (tempBill[0].SALES_UNIT == "L30")
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_L30.ToString();
                                        }

                                        temp.invoice_amount = Math.Round(tempBill[0].TOTAL ?? 0, 2, MidpointRounding.AwayFromZero).ToString();

                                    }
                                }
                                else
                                {
                                    temp.invoice_amount = "0";
                                }
                            }
                            else
                            {
                                temp.invoice_amount = "0";
                            }

                        }
                        else if (tempMain.Count() == 0)
                        {
                            var tempBill = queryBill.Where(p => p.PO_NO == po_no && p.BILLING_DATE == tempPODate).ToList();
                            var tempMainNull = queryMainNull.Where(p => p.PO_NO == po_no && p.PODate == tempPODate).ToList();
                            //var tempBill = queryBill.Where(p => p.PO_NO == po_no).ToList();
                            //var tempMainNull = queryMainNull.Where(p => p.PO_NO == po_no && p.PODate == tempPODate).ToList();

                            temp.PO_NO = po_no;
                            temp.Product = feedDesc;
                            temp.Product_Code = feedCode;
                            temp.IsThaiLube = supplier.ToUpper().Contains("LUBE") ? "1" : "0";
                            temp.Den_15 = "0";
                            temp.Visc_cst_50 = "0";
                            temp.Sulfur = "0";
                            temp.UnitPriceUSDNew = "0";
                            temp.AdjustPrice = "0";
                            temp.ExchangeRateNew = "0";
                            temp.UnitPriceTHBNew = "0";
                            temp.Price = "0";
                            temp.Vat = "0";
                            temp.Total = "0";

                            temp.InvoiceNo = "";
                            temp.InvoiceNoStatus = "PRO";
                            temp.VolumnMT = "0";
                            temp.UnitPrice = "0";
                            if (tempBill.Count() > 0)
                            {
                                if (!string.IsNullOrEmpty(tempBill[0].INVOICE_NO))
                                    temp.InvoiceNo = tempBill[0].INVOICE_NO;

                                temp.VolumnMT = "0";
                                if (!string.IsNullOrEmpty(tempBill[0].SALES_UNIT))
                                {
                                    if (tempBill[0].SALES_UNIT == "L30")
                                    {
                                        if (tempBill[0].ACTUAL_VOLUME_L30 != null)
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_L30.ToString();
                                        }
                                    }
                                    else if (tempBill[0].SALES_UNIT == "BBL")
                                    {
                                        if (tempBill[0].ACTUAL_VOLUME_BBL != null)
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_BBL.ToString();
                                        }
                                    }
                                    else if (tempBill[0].SALES_UNIT == "MT")
                                    {
                                        if (tempBill[0].ACTUAL_VOLUME_MT != null)
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_MT.ToString();
                                        }
                                    }

                                }

                                if (tempBill.Count > 0)
                                {
                                    decimal tempVal = 0;
                                    if (tempBill[0].SALES_ORG == "1700")
                                    {
                                        if (tempBill[0].SALES_UNIT == "BBL")
                                        {
                                            tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_BBL ?? (decimal)0);
                                            tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else if (tempBill[0].SALES_UNIT == "MT")
                                        {
                                            tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_MT ?? (decimal)0);
                                            tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);
                                        }
                                        else if (tempBill[0].SALES_UNIT == "L30")
                                        {
                                            tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_L30 ?? (decimal)0);
                                            tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);
                                        }

                                        temp.invoice_amount = Math.Round(tempVal, 2, MidpointRounding.AwayFromZero).ToString();
                                    }
                                    else
                                    {
                                        if (tempBill[0].SALES_UNIT == "BBL")
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_BBL.ToString();
                                        }
                                        else if (tempBill[0].SALES_UNIT == "MT")
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_MT.ToString();
                                        }
                                        else if (tempBill[0].SALES_UNIT == "L30")
                                        {
                                            temp.VolumnMT = tempBill[0].ACTUAL_VOLUME_L30.ToString();
                                        }

                                        temp.invoice_amount = Math.Round(tempBill[0].TOTAL ?? 0, 2, MidpointRounding.AwayFromZero).ToString();

                                    }

                                }
                                else
                                {
                                    temp.invoice_amount = "0";
                                }
                            }
                            else
                            {
                                if (tempMainNull.Count() > 0)
                                {
                                    temp.PO_ITEM = tempMainNull[0].POItem.ToString();
                                    temp.TripNo = tempMainNull[0].Trip_No;
                                    if (temp.VolumnMT == "" && tempMainNull[0].Volume != null)
                                    {
                                        temp.VolumnMT = tempMainNull[0].Volume.ToString();
                                    }

                                    if (tempBill.Count > 0)
                                    {
                                        decimal tempVal = 0;
                                        if (tempBill[0].SALES_ORG == "1700")
                                        {
                                            if (tempBill[0].SALES_UNIT == "BBL")
                                            {
                                                tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_BBL ?? (decimal)0);
                                                tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);
                                            }
                                            else if (tempBill[0].SALES_UNIT == "MT")
                                            {
                                                tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_MT ?? (decimal)0);
                                                tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);
                                            }
                                            else if (tempBill[0].SALES_UNIT == "L30")
                                            {
                                                tempVal = (decimal)(tempBill[0].PRICE_ZWHO ?? (decimal)0) * (decimal)(tempBill[0].ACTUAL_VOLUME_L30 ?? (decimal)0);
                                                tempVal = tempVal + Math.Round(tempVal * (decimal)0.07, 2, MidpointRounding.AwayFromZero);
                                            }

                                            temp.invoice_amount = Math.Round(tempVal, 2, MidpointRounding.AwayFromZero).ToString();
                                        }
                                        else
                                        {
                                            temp.invoice_amount = Math.Round(tempBill[0].TOTAL ?? 0, 2, MidpointRounding.AwayFromZero).ToString();

                                        }

                                    }
                                    else
                                    {
                                        temp.invoice_amount = "0";
                                    }
                                }
                            }

                            temp.PO_ITEM = "";
                            temp.TripNo = tripNo;

                            temp.UnitPriceUSDOld = "0";
                            temp.ExchangeRateOld = "0";
                            temp.UnitPriceTHBOld = "0";
                        }

                        tempData.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return tempData;
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
            }
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public List<MobDetail> Search(string dateFromTo, List<string> prdCode)
        {
            List<MobDetail> result = new List<MobDetail>();
            try
            {
                var sDateFrom = String.IsNullOrEmpty(dateFromTo) ? "" : dateFromTo.Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(dateFromTo) ? "" : dateFromTo.Substring(14, 10);
                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryTemp = (from v in context.MKT_TRN_MOPS_B
                                     where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                                     && prdCode.Contains(v.T_MPB_PRDCODE)
                                     select v).OrderBy(p => p.T_MPB_VALDATE);

                    var query = (from d in queryTemp.AsEnumerable()
                                 where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                                 && Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                                 select d).OrderBy(p => p.T_MPB_PRDCODE).OrderBy(s => s.T_MPB_VALDATE);

                    if (query != null)
                    {
                        foreach (var i in query.ToList())
                        {
                            MobDetail j = new MobDetail();
                            j.date = DateTime.ParseExact(i.T_MPB_VALDATE.ToString().Substring(6, 2) + "/" + i.T_MPB_VALDATE.ToString().Substring(4, 2) + "/" + i.T_MPB_VALDATE.ToString().Substring(0, 4), format, provider);
                            j.ProductName = i.T_MPB_PRDCODE;
                            j.ProductValue = i.T_MPB_VALUE ?? 0;
                            result.Add(j);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                Log.Error(ex.Message);
            }

            return result;
        }

        public void setMOBVal(int idx, string mob, decimal value, List<TrnMOBSearchDetail> Temp)
        {
            switch (mob)
            {
                case "ULG92": Temp[idx].d_ULG92 = value.ToString(); break;
                case "ULG95": Temp[idx].d_ULG95 = value.ToString(); break;
                case "KERO": Temp[idx].d_KERO = value.ToString(); break;
                case "GASOIL": Temp[idx].d_GASOIL = value.ToString(); break;
                case "LSFO": Temp[idx].d_LSFO = value.ToString(); break;
                case "HSFO": Temp[idx].d_HSFO = value.ToString(); break;
                case "PROPANE": Temp[idx].d_PROPANE = value.ToString(); break;
                case "BUTANE": Temp[idx].d_BUTANE = value.ToString(); break;
                case "NAPHTA": Temp[idx].d_NAPHTA = value.ToString(); break;
                case "LABIX_GASOIL": Temp[idx].d_LABIX_GASOIL = value.ToString(); break;
                case "LABIX_FO380": Temp[idx].d_LABIX_FO380 = value.ToString(); break;
                case "LABIX_PROPANE": Temp[idx].d_LABIX_PROPANE = value.ToString(); break;
                case "LABIX_BUTANE": Temp[idx].d_LABIX_BUTANE = value.ToString(); break;
                case "TPX_NAPHTA": Temp[idx].d_TPX_NAPTHA = value.ToString(); break;
            }
        }
        public void setMOBPrice(string date, string mob, decimal value, ref List<TrnMOBSearchDetail> Temp)
        {
            int index = Temp.Count() - 1;
            if ((index >= 0 && date.Substring(6, 2) == "01") || (index >= 0 && date.Substring(6, 2) == "16"))
            {
                setMOBVal(0, mob, value, Temp);
            }
            else if ((index >= 1 && date.Substring(6, 2) == "02") || (index >= 1 && date.Substring(6, 2) == "17"))
            {
                setMOBVal(1, mob, value, Temp);
            }
            else if ((index >= 2 && date.Substring(6, 2) == "03") || (index >= 2 && date.Substring(6, 2) == "18"))
            {
                setMOBVal(2, mob, value, Temp);
            }
            else if ((index >= 3 && date.Substring(6, 2) == "04") || (index >= 3 && date.Substring(6, 2) == "19"))
            {
                setMOBVal(3, mob, value, Temp);
            }
            else if ((index >= 4 && date.Substring(6, 2) == "05") || (index >= 4 && date.Substring(6, 2) == "20"))
            {
                setMOBVal(4, mob, value, Temp);
            }
            else if ((index >= 5 && date.Substring(6, 2) == "06") || (index >= 5 && date.Substring(6, 2) == "21"))
            {
                setMOBVal(5, mob, value, Temp);
            }
            else if ((index >= 6 && date.Substring(6, 2) == "07") || (index >= 6 && date.Substring(6, 2) == "22"))
            {
                setMOBVal(6, mob, value, Temp);
            }
            else if ((index >= 7 && date.Substring(6, 2) == "08") || (index >= 7 && date.Substring(6, 2) == "23"))
            {
                setMOBVal(7, mob, value, Temp);
            }
            else if ((index >= 8 && date.Substring(6, 2) == "09") || (index >= 8 && date.Substring(6, 2) == "24"))
            {
                setMOBVal(8, mob, value, Temp);
            }
            else if ((index >= 9 && date.Substring(6, 2) == "10") || (index >= 9 && date.Substring(6, 2) == "25"))
            {
                setMOBVal(9, mob, value, Temp);
            }
            else if ((index >= 10 && date.Substring(6, 2) == "11") || (index >= 10 && date.Substring(6, 2) == "26"))
            {
                setMOBVal(10, mob, value, Temp);
            }
            else if ((index >= 11 && date.Substring(6, 2) == "12") || (index >= 11 && date.Substring(6, 2) == "27"))
            {
                setMOBVal(11, mob, value, Temp);
            }
            else if ((index >= 12 && date.Substring(6, 2) == "13") || (index >= 12 && date.Substring(6, 2) == "28"))
            {
                setMOBVal(12, mob, value, Temp);
            }
            else if ((index >= 13 && date.Substring(6, 2) == "14") || (index >= 13 && date.Substring(6, 2) == "29"))
            {
                setMOBVal(13, mob, value, Temp);
            }
            else if ((index >= 14 && date.Substring(6, 2) == "15") || (index >= 14 && date.Substring(6, 2) == "30"))
            {
                setMOBVal(14, mob, value, Temp);
            }
            else if ((index >= 15 && date.Substring(6, 2) == "16") || (index >= 15 && date.Substring(6, 2) == "31"))
            {
                setMOBVal(15, mob, value, Temp);
            }
            else if (index >= 16 && date.Substring(6, 2) == "17")
            {
                setMOBVal(16, mob, value, Temp);
            }
            else if (index >= 17 && date.Substring(6, 2) == "18")
            {
                setMOBVal(17, mob, value, Temp);
            }
            else if (index >= 18 && date.Substring(6, 2) == "19")
            {
                setMOBVal(18, mob, value, Temp);
            }
            else if (index >= 19 && date.Substring(6, 2) == "20")
            {
                setMOBVal(19, mob, value, Temp);
            }
            else if (index >= 20 && date.Substring(6, 2) == "21")
            {
                setMOBVal(20, mob, value, Temp);
            }
            else if (index >= 21 && date.Substring(6, 2) == "22")
            {
                setMOBVal(21, mob, value, Temp);
            }
            else if (index >= 22 && date.Substring(6, 2) == "23")
            {
                setMOBVal(22, mob, value, Temp);
            }
            else if (index >= 23 && date.Substring(6, 2) == "24")
            {
                setMOBVal(23, mob, value, Temp);
            }
            else if (index >= 24 && date.Substring(6, 2) == "25")
            {
                setMOBVal(24, mob, value, Temp);
            }
            else if (index >= 25 && date.Substring(6, 2) == "26")
            {
                setMOBVal(25, mob, value, Temp);
            }
            else if (index >= 26 && date.Substring(6, 2) == "27")
            {
                setMOBVal(26, mob, value, Temp);
            }
            else if (index >= 27 && date.Substring(6, 2) == "28")
            {
                setMOBVal(27, mob, value, Temp);
            }
            else if (index >= 28 && date.Substring(6, 2) == "29")
            {
                setMOBVal(28, mob, value, Temp);
            }
            else if (index >= 29 && date.Substring(6, 2) == "30")
            {
                setMOBVal(29, mob, value, Temp);
            }
            else if (index >= 30 && date.Substring(6, 2) == "31")
            {
                setMOBVal(30, mob, value, Temp);
            }

        }

        public ReturnValue Search(ref FeedstockBookingPriceViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (String.IsNullOrEmpty(pModel.data_Search.vSearchPeriod))
                {
                    rtn.Message = "กรุณาเลือกช่วงเวลา";
                    rtn.Status = false;
                    return rtn;
                }
                if (String.IsNullOrEmpty(pModel.data_Search.vUpdatePeriod))
                {
                    pModel.data_Search.vUpdatePeriod = pModel.data_Search.vSearchPeriod;
                }
                FeedstockBookingPriceServiceModel serviceModel = new FeedstockBookingPriceServiceModel();
                CONFIG_INTERCOMPANY dataList = serviceModel.getCIPConfig("INTER_PRICE_UPDATE");
                //pModel.exchangeRate = dataList.EXCHANGE_RATE.OrderByDescending(x => x.CODE).ToList();
                pModel.productCode = dataList.PRODUCT_CODE;
                pModel.consProductPrice = dataList.CONSTANT_PRODUCT_PRICE;

                List<string> prdCode = new List<string>();
                foreach (PRODUCT_CODE p in pModel.productCode)
                {
                    prdCode.Add(p.CODE);
                }

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string ULG92 = pModel.productCode.Where(p => p.NAME == "ULG92").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "ULG92").FirstOrDefault().CODE;
                    string ULG95 = pModel.productCode.Where(p => p.NAME == "ULG95").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "ULG95").FirstOrDefault().CODE;
                    string KERO = pModel.productCode.Where(p => p.NAME == "KERO").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "KERO").FirstOrDefault().CODE;
                    string GASOIL = pModel.productCode.Where(p => p.NAME == "GASOIL").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "GASOIL").FirstOrDefault().CODE;
                    string LSFO = pModel.productCode.Where(p => p.NAME == "LSFO").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "LSFO").FirstOrDefault().CODE;
                    string HSFO = pModel.productCode.Where(p => p.NAME == "HSFO").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "HSFO").FirstOrDefault().CODE;
                    string PROPANE = pModel.productCode.Where(p => p.NAME == "PROPANE").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "PROPANE").FirstOrDefault().CODE;
                    string BUTANE = pModel.productCode.Where(p => p.NAME == "BUTANE").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "BUTANE").FirstOrDefault().CODE;
                    string NAPHTA = pModel.productCode.Where(p => p.NAME == "NAPHTA").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "NAPHTA").FirstOrDefault().CODE;

                    string LABIX_GASOIL = pModel.productCode.Where(p => p.NAME == "LABIX_GASOIL").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "LABIX_GASOIL").FirstOrDefault().CODE;
                    string LABIX_FO380 = pModel.productCode.Where(p => p.NAME == "LABIX_FO380").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "LABIX_FO380").FirstOrDefault().CODE;
                    string LABIX_PROPANE = pModel.productCode.Where(p => p.NAME == "LABIX_PROPANE").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "LABIX_PROPANE").FirstOrDefault().CODE;
                    string LABIX_BUTANE = pModel.productCode.Where(p => p.NAME == "LABIX_BUTANE").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "LABIX_BUTANE").FirstOrDefault().CODE;

                    string TPX_NAPTHA = pModel.productCode.Where(p => p.NAME == "TPX_NAPHTA").ToList().Count == 0 ? "_" : pModel.productCode.Where(p => p.NAME == "TPX_NAPHTA").FirstOrDefault().CODE;

                    var sDateFrom = String.IsNullOrEmpty(pModel.data_Search.vSearchPeriod) ? "" : pModel.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.data_Search.vSearchPeriod) ? "" : pModel.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                    DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                    sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                    sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                    var queryTemp = (from v in context.MKT_TRN_MOPS_B
                                     where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                                     && prdCode.Contains(v.T_MPB_PRDCODE)
                                     select v).OrderBy(p => p.T_MPB_VALDATE);

                    var query = from d in queryTemp.AsEnumerable()
                                where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                                && Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                                select d;


                    if (query != null)
                    {
                        pModel.data_Search.sMobDetail = new List<TrnMOBSearchDetail>();
                        pModel.data_Search.sPriceDetail = new UpdatePriceDetail();
                        pModel.data_Search.sPriceDetail.dQualityList = new List<QualityProductDetail>();

                        int sumDate = dateTo.Subtract(dateFrom).Days + 1;
                        int dateFocus = Convert.ToInt32(sDateFrom);
                        double a_ULG92 = 0;
                        double a_ULG95 = 0;
                        double a_KERO = 0;
                        double a_GASOIL = 0;
                        double a_LSFO = 0;
                        double a_HSFO = 0;
                        double a_PROPANE = 0;
                        double a_BUTANE = 0;
                        double a_NAPHTA = 0;

                        double a_LABIX_GASOIL = 0;
                        double a_LABIX_FO380 = 0;
                        double a_LABIX_PROPANE = 0;
                        double a_LABIX_BUTANE = 0;

                        double a_TPX_NAPTHA = 0;

                        int count_ULG92 = 0;
                        int count_ULG95 = 0;
                        int count_KERO = 0;
                        int count_GASOIL = 0;
                        int count_LSFO = 0;
                        int count_HSFO = 0;
                        int count_PROPANE = 0;
                        int count_BUTANE = 0;
                        int count_NAPHTA = 0;

                        int count_LABIX_GASOIL = 0;
                        int count_LABIX_FO380 = 0;
                        int count_LABIX_PROPANE = 0;
                        int count_LABIX_BUTANE = 0;

                        double count_TPX_NAPTHA = 0;

                        List<TrnMOBSearchDetail> mobPrice = new List<TrnMOBSearchDetail>();
                        #region get daily MOB TLB,TPX,LABIX 
                        for (int j = 1; j <= sumDate; j++)
                        {
                            mobPrice.Add(
                                                new TrnMOBSearchDetail
                                                {
                                                    d_Date = dateFocus.ToString(),
                                                    d_Date_num = (j - 1).ToString(),
                                                    d_ULG92 = "",
                                                    d_ULG95 = "",
                                                    d_KERO = "",
                                                    d_GASOIL = "",
                                                    d_LSFO = "",
                                                    d_HSFO = "",
                                                    d_PROPANE = "",
                                                    d_BUTANE = "",
                                                    d_NAPHTA = "",
                                                    d_LABIX_BUTANE = "",
                                                    d_LABIX_PROPANE = "",
                                                    d_LABIX_FO380 = "",
                                                    d_LABIX_GASOIL = "",
                                                    d_TPX_NAPTHA = ""
                                                });

                            DateTime tempDate = DateTime.ParseExact(dateFocus.ToString().Substring(6, 2) + "/" + dateFocus.ToString().Substring(4, 2) + "/" + dateFocus.ToString().Substring(0, 4)
                                 , format, provider);

                            tempDate = tempDate.AddDays(1);
                            dateFocus = Convert.ToInt32(
                                tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(6, 4) +
                                tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(3, 2) +
                                tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(0, 2)
                                        );
                        }

                        foreach (var k in query)
                        {
                            string v_ULG92 = "", v_ULG95 = "", v_KERO = "", v_GASOIL = "", v_LSFO = "", v_HSFO = "", v_PROPANE = "", v_BUTANE = "", v_NAPHTA = "", v_LABIX_GASOIL = "", v_LABIX_FO380 = "", v_LABIX_PROPANE = "", v_LABIX_BUTANE = "", v_TPX_NAPTHA = "";

                            if (k.T_MPB_PRDCODE == ULG92)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "ULG92", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_ULG92 = (k.T_MPB_VALUE ?? 0).ToString();
                                a_ULG92 += Convert.ToDouble(v_ULG92);
                                count_ULG92++;
                            }

                            //var list_ULG95 = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == ULG95);
                            if (k.T_MPB_PRDCODE == ULG95)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "ULG95", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_ULG95 = (k.T_MPB_VALUE ?? 0).ToString(); //list_ULG95.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_ULG95 += Convert.ToDouble(v_ULG95);
                                count_ULG95++;
                            }

                            //var list_KERO = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == KERO);
                            if (k.T_MPB_PRDCODE == KERO)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "KERO", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_KERO = (k.T_MPB_VALUE ?? 0).ToString();//list_KERO.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_KERO += Convert.ToDouble(v_KERO);
                                count_KERO++;
                            }

                            //var list_GASOIL = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == GASOIL);
                            if (k.T_MPB_PRDCODE == GASOIL)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "GASOIL", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_GASOIL = (k.T_MPB_VALUE ?? 0).ToString();//list_GASOIL.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_GASOIL += Convert.ToDouble(v_GASOIL);
                                count_GASOIL++;
                            }

                            //var list_LSFO = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LSFO);
                            if (k.T_MPB_PRDCODE == LSFO)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "LSFO", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_LSFO = (k.T_MPB_VALUE ?? 0).ToString();//list_LSFO.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_LSFO += Convert.ToDouble(v_LSFO);
                                count_LSFO++;
                            }

                            //var list_HSFO = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == HSFO);
                            if (k.T_MPB_PRDCODE == HSFO)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "HSFO", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_HSFO = (k.T_MPB_VALUE ?? 0).ToString();//list_HSFO.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_HSFO += Convert.ToDouble(v_HSFO);
                                count_HSFO++;
                            }
                            //var list_PROPANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == PROPANE);
                            if (k.T_MPB_PRDCODE == PROPANE)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "PROPANE", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_PROPANE = (k.T_MPB_VALUE ?? 0).ToString();//list_PROPANE.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_PROPANE += Convert.ToDouble(v_PROPANE);
                                count_PROPANE++;
                            }

                            //var list_BUTANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == BUTANE);
                            if (k.T_MPB_PRDCODE == BUTANE)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "BUTANE", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_BUTANE = (k.T_MPB_VALUE ?? 0).ToString();//list_BUTANE.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_BUTANE += Convert.ToDouble(v_BUTANE);
                                count_BUTANE++;
                            }

                            //var list_NAPHTA = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == NAPHTA);
                            if (k.T_MPB_PRDCODE == NAPHTA)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "NAPHTA", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_NAPHTA = (k.T_MPB_VALUE ?? 0).ToString();//list_NAPHTA.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_NAPHTA += Convert.ToDouble(v_NAPHTA);
                                count_NAPHTA++;
                            }

                            //var list_LABIX_GASOIL = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_GASOIL);
                            if (k.T_MPB_PRDCODE == LABIX_GASOIL)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "LABIX_GASOIL", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_LABIX_GASOIL = (k.T_MPB_VALUE ?? 0).ToString();//list_LABIX_GASOIL.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_LABIX_GASOIL += Convert.ToDouble(v_LABIX_GASOIL);
                                count_LABIX_GASOIL++;
                            }

                            //var list_LABIX_FO380 = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_FO380);
                            if (k.T_MPB_PRDCODE == LABIX_FO380)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "LABIX_FO380", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_LABIX_FO380 = (k.T_MPB_VALUE ?? 0).ToString();//list_LABIX_FO380.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_LABIX_FO380 += Convert.ToDouble(v_LABIX_FO380);
                                count_LABIX_FO380++;
                            }

                            //var list_LABIX_PROPANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_PROPANE);
                            if (k.T_MPB_PRDCODE == LABIX_PROPANE)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "LABIX_PROPANE", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_LABIX_PROPANE = (k.T_MPB_VALUE ?? 0).ToString();//list_LABIX_PROPANE.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_LABIX_PROPANE += Convert.ToDouble(v_LABIX_PROPANE);
                                count_LABIX_PROPANE++;
                            }
                            //var list_LABIX_BUTANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_BUTANE);
                            if (k.T_MPB_PRDCODE == LABIX_BUTANE)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "LABIX_BUTANE", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_LABIX_BUTANE = (k.T_MPB_VALUE ?? 0).ToString();//list_LABIX_BUTANE.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_LABIX_BUTANE += Convert.ToDouble(v_LABIX_BUTANE);
                                count_LABIX_BUTANE++;
                            }

                            //var list_TPX_NAPTHA = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == TPX_NAPTHA);
                            if (k.T_MPB_PRDCODE == TPX_NAPTHA)
                            {
                                setMOBPrice(k.T_MPB_VALDATE, "TPX_NAPHTA", k.T_MPB_VALUE ?? 0, ref mobPrice);
                                v_TPX_NAPTHA = (k.T_MPB_VALUE ?? 0).ToString();//list_TPX_NAPTHA.FirstOrDefault().T_MPB_VALUE.ToString();
                                a_TPX_NAPTHA += Convert.ToDouble(v_TPX_NAPTHA);
                                count_TPX_NAPTHA++;
                            }
                        }
                        pModel.data_Search.sMobDetail = mobPrice;
                        //for (int j = 1; j <= sumDate; j++)
                        //{
                        //    string v_ULG92 = "", v_ULG95 = "", v_KERO = "", v_GASOIL = "", v_LSFO = "", v_HSFO = "", v_PROPANE = "", v_BUTANE = "", v_NAPHTA = "", v_LABIX_GASOIL = "", v_LABIX_FO380 = "", v_LABIX_PROPANE = "", v_LABIX_BUTANE = "", v_TPX_NAPTHA = "";

                        //    var list_ULG92 = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == ULG92);
                        //    if (list_ULG92.Count() > 0)
                        //    {
                        //        v_ULG92 = list_ULG92.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_ULG92 += Convert.ToDouble(v_ULG92);
                        //        count_ULG92++;
                        //    }

                        //    var list_ULG95 = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == ULG95);
                        //    if (list_ULG95.Count() > 0)
                        //    {
                        //        v_ULG95 = list_ULG95.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_ULG95 += Convert.ToDouble(v_ULG95);
                        //        count_ULG95++;
                        //    }

                        //    var list_KERO = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == KERO);
                        //    if (list_KERO.Count() > 0)
                        //    {
                        //        v_KERO = list_KERO.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_KERO += Convert.ToDouble(v_KERO);
                        //        count_KERO++;
                        //    }

                        //    var list_GASOIL = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == GASOIL);
                        //    if (list_GASOIL.Count() > 0)
                        //    {
                        //        v_GASOIL = list_GASOIL.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_GASOIL += Convert.ToDouble(v_GASOIL);
                        //        count_GASOIL++;
                        //    }

                        //    var list_LSFO = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LSFO);
                        //    if (list_LSFO.Count() > 0)
                        //    {
                        //        v_LSFO = list_LSFO.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_LSFO += Convert.ToDouble(v_LSFO);
                        //        count_LSFO++;
                        //    }

                        //    var list_HSFO = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == HSFO);
                        //    if (list_HSFO.Count() > 0)
                        //    {
                        //        v_HSFO = list_HSFO.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_HSFO += Convert.ToDouble(v_HSFO);
                        //        count_HSFO++;
                        //    }

                        //    var list_PROPANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == PROPANE);
                        //    if (list_PROPANE.Count() > 0)
                        //    {
                        //        v_PROPANE = list_PROPANE.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_PROPANE += Convert.ToDouble(v_PROPANE);
                        //        count_PROPANE++;
                        //    }

                        //    var list_BUTANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == BUTANE);
                        //    if (list_BUTANE.Count() > 0)
                        //    {
                        //        v_BUTANE = list_BUTANE.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_BUTANE += Convert.ToDouble(v_BUTANE);
                        //        count_BUTANE++;
                        //    }

                        //    var list_NAPHTA = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == NAPHTA);
                        //    if (list_NAPHTA.Count() > 0)
                        //    {
                        //        v_NAPHTA = list_NAPHTA.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_NAPHTA += Convert.ToDouble(v_NAPHTA);
                        //        count_NAPHTA++;
                        //    }

                        //    var list_LABIX_GASOIL = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_GASOIL);
                        //    if (list_LABIX_GASOIL.Count() > 0)
                        //    {
                        //        v_LABIX_GASOIL = list_LABIX_GASOIL.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_LABIX_GASOIL += Convert.ToDouble(v_LABIX_GASOIL);
                        //        count_LABIX_GASOIL++;
                        //    }

                        //    var list_LABIX_FO380 = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_FO380);
                        //    if (list_LABIX_FO380.Count() > 0)
                        //    {
                        //        v_LABIX_FO380 = list_LABIX_FO380.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_LABIX_FO380 += Convert.ToDouble(v_LABIX_FO380);
                        //        count_LABIX_FO380++;
                        //    }

                        //    var list_LABIX_PROPANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_PROPANE);
                        //    if (list_LABIX_PROPANE.Count() > 0)
                        //    {
                        //        v_LABIX_PROPANE = list_LABIX_PROPANE.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_LABIX_PROPANE += Convert.ToDouble(v_LABIX_PROPANE);
                        //        count_LABIX_PROPANE++;
                        //    }

                        //    var list_LABIX_BUTANE = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == LABIX_BUTANE);
                        //    if (list_LABIX_BUTANE.Count() > 0)
                        //    {
                        //        v_LABIX_BUTANE = list_LABIX_BUTANE.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_LABIX_BUTANE += Convert.ToDouble(v_LABIX_BUTANE);
                        //        count_LABIX_BUTANE++;
                        //    }

                        //    var list_TPX_NAPTHA = query.Where(s => Int32.Parse(s.T_MPB_VALDATE) == dateFocus && s.T_MPB_PRDCODE == TPX_NAPTHA);
                        //    if (list_TPX_NAPTHA.Count() > 0)
                        //    {
                        //        v_TPX_NAPTHA = list_TPX_NAPTHA.FirstOrDefault().T_MPB_VALUE.ToString();
                        //        a_TPX_NAPTHA += Convert.ToDouble(v_TPX_NAPTHA);
                        //        count_TPX_NAPTHA++;
                        //    }

                        //    if (list_ULG92.Count() > 0 ||
                        //        list_ULG95.Count() > 0 ||
                        //        list_KERO.Count() > 0 ||
                        //        list_GASOIL.Count() > 0 ||
                        //        list_LSFO.Count() > 0 ||
                        //        list_HSFO.Count() > 0 ||
                        //        list_PROPANE.Count() > 0 ||
                        //        list_BUTANE.Count() > 0 ||
                        //        list_NAPHTA.Count() > 0)
                        //    {
                        //        pModel.data_Search.sMobDetail.Add(
                        //                            new TrnMOBSearchDetail
                        //                            {
                        //                                d_Date = dateFocus.ToString(),
                        //                                d_ULG92 = v_ULG92,
                        //                                d_ULG95 = v_ULG95,
                        //                                d_KERO = v_KERO,
                        //                                d_GASOIL = v_GASOIL,
                        //                                d_LSFO = v_LSFO,
                        //                                d_HSFO = v_HSFO,
                        //                                d_PROPANE = v_PROPANE,
                        //                                d_BUTANE = v_BUTANE,
                        //                                d_NAPHTA = v_NAPHTA,
                        //                                d_LABIX_BUTANE = v_LABIX_BUTANE,
                        //                                d_LABIX_PROPANE = v_LABIX_PROPANE,
                        //                                d_LABIX_FO380 = v_LABIX_FO380,
                        //                                d_LABIX_GASOIL = v_LABIX_GASOIL,
                        //                                d_TPX_NAPTHA = v_TPX_NAPTHA
                        //                            });
                        //    }

                        //    DateTime tempDate = DateTime.ParseExact(dateFocus.ToString().Substring(6, 2) + "/" + dateFocus.ToString().Substring(4, 2) + "/" + dateFocus.ToString().Substring(0, 4)
                        //         , format, provider);

                        //    tempDate = tempDate.AddDays(1);
                        //    dateFocus = Convert.ToInt32(
                        //        tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(6, 4) +
                        //        tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(3, 2) +
                        //        tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(0, 2)
                        //                );
                        //}
                        #endregion

                        string ulg92 = "";
                        string[] ulg92_t = (a_ULG92 / count_ULG92).ToString("0.####").Split('.');
                        if (ulg92_t.Length == 2)
                        {
                            ulg92 = ulg92_t[0] + "." + ulg92_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            ulg92 = ulg92_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_ULG92 = ulg92;

                        string ulg95 = "";
                        string[] ulg95_t = (a_ULG95 / count_ULG95 * Convert.ToDouble(pModel.consProductPrice.ULG95)).ToString("0.####").Split('.');
                        if (ulg95_t.Length == 2)
                        {
                            ulg95 = ulg95_t[0] + "." + ulg95_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            ulg95 = ulg95_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_ULG95 = ulg95;

                        string kero = "";
                        string[] kero_t = (a_KERO / count_KERO * Convert.ToDouble(pModel.consProductPrice.KERO)).ToString("0.####").Split('.');
                        if (kero_t.Length == 2)
                        {
                            kero = kero_t[0] + "." + kero_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            kero = kero_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_KERO = kero;

                        string gasoil = "";
                        string[] gasoil_t = (a_GASOIL / count_GASOIL * Convert.ToDouble(pModel.consProductPrice.GASOIL)).ToString("0.####").Split('.');
                        if (gasoil_t.Length == 2)
                        {
                            gasoil = gasoil_t[0] + "." + gasoil_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            gasoil = gasoil_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_GASOIL = gasoil;

                        string lsfo = "";
                        string[] lsfo_t = (a_LSFO / count_LSFO).ToString("0.####").Split('.');
                        if (lsfo_t.Length == 2)
                        {
                            lsfo = lsfo_t[0] + "." + lsfo_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            lsfo = lsfo_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LSFO = lsfo;

                        string hsfo = "";
                        string[] hsfo_t = (a_HSFO / count_HSFO).ToString("0.####").Split('.');
                        if (hsfo_t.Length == 2)
                        {
                            hsfo = hsfo_t[0] + "." + hsfo_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            hsfo = hsfo_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_HSFO = hsfo;

                        string lpg = "";
                        string[] lpg_t = ((a_PROPANE / count_PROPANE * 0.6) + (a_BUTANE / count_BUTANE * 0.4)).ToString("0.####").Split('.');
                        if (lpg_t.Length == 2)
                        {
                            lpg = lpg_t[0] + "." + lpg_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            lpg = lpg_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LPG = lpg;

                        string naphta = "";
                        string[] naphta_t = (a_NAPHTA / count_NAPHTA).ToString("0.####").Split('.');
                        if (naphta_t.Length == 2)
                        {
                            naphta = naphta_t[0] + "." + naphta_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            naphta = naphta_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.USD_NAPHTA = naphta;

                        ////////////////////////////////////Labix Start
                        string labix_kero = "";
                        string[] labix_kero_t = Math.Round((Decimal)(a_KERO / count_KERO), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_kero_t.Length == 2)
                        {
                            labix_kero = labix_kero_t[0] + "." + labix_kero_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_kero = labix_kero_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_KERO = labix_kero;

                        string labix_Propane = "";
                        string[] labix_propane_t = Math.Round((Decimal)(a_LABIX_PROPANE / count_LABIX_PROPANE), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_propane_t.Length == 2)
                        {
                            labix_Propane = labix_propane_t[0] + "." + labix_propane_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_Propane = labix_propane_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_PROPANE = labix_Propane;


                        string labix_butane = "";
                        string[] labix_butane_t = Math.Round((Decimal)a_LABIX_BUTANE / count_LABIX_BUTANE, 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_butane_t.Length == 2)
                        {
                            labix_butane = labix_butane_t[0] + "." + labix_butane_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_butane = labix_butane_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_BUTANE = labix_butane;

                        string labix_lpg = "";
                        string[] labix_lpg_t = Math.Round((Decimal)((a_LABIX_PROPANE / count_LABIX_PROPANE * 0.5) + (a_LABIX_BUTANE / count_LABIX_BUTANE * 0.5)), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_lpg_t.Length == 2)
                        {
                            labix_lpg = labix_lpg_t[0] + "." + labix_lpg_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_lpg = labix_lpg_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_LPG = labix_lpg;

                        string labix_gasoil = "";
                        string[] labix_gasoil_t = Math.Round((Decimal)(a_LABIX_GASOIL / count_LABIX_GASOIL), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_gasoil_t.Length == 2)
                        {
                            labix_gasoil = labix_gasoil_t[0] + "." + labix_gasoil_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_gasoil = labix_gasoil_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_GASOIL = labix_gasoil;

                        string labix_fo380 = "";
                        string[] labix_fo380_t = Math.Round((Decimal)(a_LABIX_FO380 / count_LABIX_FO380), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_fo380_t.Length == 2)
                        {
                            labix_fo380 = labix_fo380_t[0] + "." + labix_fo380_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_fo380 = labix_fo380_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_FO380 = labix_fo380;

                        string labix_ulg92 = "";
                        string[] labix_ulg92_t = Math.Round(Convert.ToDecimal(ulg92), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_ulg92_t.Length == 2)
                        {
                            labix_ulg92 = labix_ulg92_t[0] + "." + labix_ulg92_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_ulg92 = labix_ulg92_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_ULG92 = labix_ulg92;

                        string labix_naphta = "";
                        string[] labix_naphta_t = Math.Round(Convert.ToDecimal(naphta), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (labix_naphta_t.Length == 2)
                        {
                            labix_naphta = labix_naphta_t[0] + "." + labix_naphta_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            labix_naphta = labix_naphta_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_LABIX_NAPHTA = labix_naphta;
                        ////////////////////////////////////Labix End

                        ////////////////////////////////////TPX Start

                        pModel.data_Search.sPriceDetail.USD_TPX_KERO = labix_kero;
                        string tpx_gasoil = "";
                        string[] tpx_gasoil_t = Math.Round((Decimal)(a_GASOIL / count_GASOIL), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (tpx_gasoil_t.Length == 2)
                        {
                            tpx_gasoil = tpx_gasoil_t[0] + "." + tpx_gasoil_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            tpx_gasoil = tpx_gasoil_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_TPX_GASOIL = tpx_gasoil;

                        string tpx_ulg95 = "";
                        string[] tpx_ulg95_t = Math.Round((Decimal)(a_ULG95 / count_ULG95), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (tpx_ulg95_t.Length == 2)
                        {
                            tpx_ulg95 = tpx_ulg95_t[0] + "." + tpx_ulg95_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            tpx_ulg95 = tpx_ulg95_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_TPX_ULG95 = tpx_ulg95;

                        string tpx_naptha = "";
                        string[] tpx_naptha_t = Math.Round((Decimal)(a_TPX_NAPTHA / count_TPX_NAPTHA), 3, MidpointRounding.AwayFromZero).ToString("0.####").Split('.');
                        if (tpx_naptha_t.Length == 2)
                        {
                            tpx_naptha = tpx_naptha_t[0] + "." + tpx_naptha_t[1].PadRight(3, '0');
                        }
                        else
                        {
                            tpx_naptha = tpx_naptha_t[0] + ".000";
                        }
                        pModel.data_Search.sPriceDetail.USD_TPX_NAPHTA = tpx_naptha;

                        ////////////////////////////////////TPX End

                        ///////////////////////////////////TLB , TPX row second
                        string bbl_ulg95 = "";
                        string[] bbl_ulg95_t = (a_NAPHTA / count_NAPHTA).ToString("0.####").Split('.');
                        if (bbl_ulg95_t.Length == 2)
                        {
                            bbl_ulg95 = bbl_ulg95_t[0] + "." + bbl_ulg95_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            bbl_ulg95 = bbl_ulg95_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.BBL_ULG95 = bbl_ulg95;

                        string bbl_kero = "";
                        string[] bbl_kero_t = pModel.consProductPrice.KERO.Split('.');
                        if (bbl_kero_t.Length == 2)
                        {
                            bbl_kero = bbl_kero_t[0] + "." + bbl_kero_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            bbl_kero = bbl_kero_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.BBL_KERO = bbl_kero;

                        string bbl_gasoil = "";
                        string[] bbl_gasoil_t = pModel.consProductPrice.GASOIL.Split('.');
                        if (bbl_gasoil_t.Length == 2)
                        {
                            bbl_gasoil = bbl_gasoil_t[0] + "." + bbl_gasoil_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            bbl_gasoil = bbl_gasoil_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.BBL_GASOIL = bbl_gasoil;



                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }

                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                    ///---------------------------------------------EXCHANGE RATE AVERAGE
                    var tempFx = (from v in context.MKT_TRN_FX_B
                                  where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                  select v).OrderBy(p => p.T_FXB_VALDATE);

                    var queryFx = from d in tempFx.AsEnumerable()
                                  where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo)
                                  && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom)
                                  select d;

                    if (queryFx != null)
                    {
                        pModel.data_Search.sFXDetail = new List<TrnFXSearchDetail>();
                        int sumDate = dateTo.Subtract(dateFrom).Days + 1;
                        int dateFocus = Convert.ToInt32(sDateFrom);
                        decimal a_BUYING = 0;
                        decimal a_SELLING = 0;
                        string v_BUYING = "", v_SELLING = "";
                        int count_BUYING = 0, count_SELLING = 0;

                        for (int j = 1; j <= sumDate; j++)
                        {

                            var list_BUYING = queryFx.Where(s => (Int32.Parse(s.T_FXB_VALDATE) == dateFocus
                            && s.T_FXB_VALTYPE == "B" && s.T_FXB_ENABLED == "T")
                            && s.T_FXB_CUR == "USD");
                            if (list_BUYING.Count() > 0)
                            {
                                v_BUYING = list_BUYING.FirstOrDefault().T_FXB_VALUE1.ToString();
                                a_BUYING += Convert.ToDecimal(v_BUYING);
                                count_BUYING++;
                            }


                            var list_SELLING = queryFx.Where(s => (Int32.Parse(s.T_FXB_VALDATE) == dateFocus
                            && s.T_FXB_VALTYPE == "M" && s.T_FXB_ENABLED == "T")
                            && s.T_FXB_CUR == "USD");
                            if (list_SELLING.Count() > 0)
                            {
                                v_SELLING = list_SELLING.FirstOrDefault().T_FXB_VALUE1.ToString();
                                a_SELLING += Convert.ToDecimal(v_SELLING);
                                count_SELLING++;
                            }

                            if (list_BUYING.Count() > 0 || list_SELLING.Count() > 0)
                            {
                                pModel.data_Search.sFXDetail.Add(
                                                    new TrnFXSearchDetail
                                                    {
                                                        d_Date = dateFocus.ToString(),
                                                        d_Buying_value = v_BUYING,
                                                        d_Selling_value = v_SELLING
                                                    });
                            }


                            DateTime tempDate = DateTime.ParseExact(dateFocus.ToString().Substring(6, 2) + "/" + dateFocus.ToString().Substring(4, 2) + "/" + dateFocus.ToString().Substring(0, 4)
                                , format, provider);

                            tempDate = tempDate.AddDays(1);
                            dateFocus = Convert.ToInt32(
                                tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(6, 4) +
                                tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(3, 2) +
                                tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(0, 2)
                                        );
                        }
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        string json = js.Serialize(pModel.data_Search.sFXDetail);
                        pModel.data_Search.jsonExchageRate = json;

                        string buying = "";
                        string[] buying_t = Math.Round((a_BUYING / count_BUYING), 4, MidpointRounding.AwayFromZero).ToString().Split('.');//ToString("0.####")
                        if (buying_t.Length == 2)
                        {
                            buying = buying_t[0] + "." + buying_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            buying = buying_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.EXC_BUYING = buying;

                        string selling = "";
                        string[] selling_t = Math.Round((a_SELLING / count_SELLING), 4, MidpointRounding.AwayFromZero).ToString().Split('.');
                        if (selling_t.Length == 2)
                        {
                            selling = selling_t[0] + "." + selling_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            selling = selling_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.EXC_SELLING = selling;

                        string average = "";
                        decimal t_average = (Convert.ToDecimal(selling) + Convert.ToDecimal(buying)) / (decimal)2;
                        t_average = Math.Round(t_average, 4, MidpointRounding.AwayFromZero);
                        string[] average_t = t_average.ToString().Split('.');
                        if (average_t.Length == 2)
                        {
                            average = average_t[0] + "." + average_t[1].PadRight(4, '0');
                        }
                        else
                        {
                            average = average_t[0] + ".0000";
                        }
                        pModel.data_Search.sPriceDetail.EXC_AVERAGE = average;

                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }

                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }

                    ////////////////////////////////////////// Crude Phet
                    var tempCrudePhet = (from v in context.FORMULA_PRICE
                                         where v.BENCHMARK == "OSP" && v.MET_NUM == "YBKC" && v.PRICE_STATUS == "P"
                                         select v).ToList();
                    pModel.data_Search.sCrudePhet = new CrudePhetProductDetail();
                    pModel.data_Search.sCrudePhet.Daily = new List<SelectListItem>();
                    pModel.data_Search.sCrudePhet.Monthly = new List<SelectListItem>();
                    if (tempCrudePhet.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_1))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_1;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }

                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_2))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_2;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_3))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_3;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_4))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_4;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_5))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_5;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_6))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_6;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().DAILY_7))
                        {
                            string code = tempCrudePhet.FirstOrDefault().DAILY_7;
                            SelectListItem iDaily = new SelectListItem();
                            iDaily.Value = code;
                            iDaily.Text = "";

                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iDaily.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Daily.Add(iDaily);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_1))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_1;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_2))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_2;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_3))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_3;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_4))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_4;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_5))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_5;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_6))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_6;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }
                        if (!string.IsNullOrEmpty(tempCrudePhet.FirstOrDefault().MONTHLY_7))
                        {
                            string code = tempCrudePhet.FirstOrDefault().MONTHLY_7;
                            SelectListItem iMonthly = new SelectListItem();
                            iMonthly.Value = code;
                            iMonthly.Text = "";
                            var tempValue = (from v in context.MKT_MST_MOPS_PRODUCT
                                             where v.M_MPR_PRDCODE == code
                                             select v);
                            if (tempValue != null)
                            {
                                if (tempValue.ToList().Count > 0)
                                    iMonthly.Text = tempValue.FirstOrDefault().M_MPR_ACTPRODNAME;
                            }
                            pModel.data_Search.sCrudePhet.Monthly.Add(iMonthly);
                        }

                        if (pModel.data_Search.sCrudePhet.Daily.Count > 0
                         || pModel.data_Search.sCrudePhet.Monthly.Count > 0)
                        {
                            string temp_dateFrom = String.IsNullOrEmpty(pModel.data_Search.vSearchPeriod) ? "" : pModel.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                            string temp_dateTo = String.IsNullOrEmpty(pModel.data_Search.vSearchPeriod) ? "" : pModel.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                            #region Get bongKotPrice from Formula Bongkot
                            string naph_status = (pModel.data_Search.vInvoice == "FINAL" ? "A" : "P");
                            FormulaPricingDetail item = new FormulaPricingDetail();

                            var queryBongKot = (from v in context.FORMULA_PRICE
                                                where v.BENCHMARK.Equals("OSP") &&
                                                v.PRICE_STATUS.Equals(naph_status)
                                                select new
                                                {
                                                    MET_NUM = v.MET_NUM,
                                                    PRICE_STATUS = v.PRICE_STATUS,
                                                    DAILY_1 = v.DAILY_1,
                                                    DAILY_2 = v.DAILY_2,
                                                    DAILY_3 = v.DAILY_3,
                                                    DAILY_4 = v.DAILY_4,
                                                    DAILY_5 = v.DAILY_5,
                                                    DAILY_6 = v.DAILY_6,
                                                    DAILY_7 = v.DAILY_7,
                                                    MONTHLY_1 = v.MONTHLY_1,
                                                    MONTHLY_2 = v.MONTHLY_2,
                                                    MONTHLY_3 = v.MONTHLY_3,
                                                    MONTHLY_4 = v.MONTHLY_4,
                                                    MONTHLY_5 = v.MONTHLY_5,
                                                    MONTHLY_6 = v.MONTHLY_6,
                                                    MONTHLY_7 = v.MONTHLY_7,
                                                    BP_PRICING_PERIOD_FROM = v.BP_PRICING_PERIOD_FROM,
                                                    BP_PRICING_PERIOD_TO = v.BP_PRICING_PERIOD_TO,
                                                    OSP_PRICING_PERIOD = v.OSP_PRICING_PERIOD,

                                                    BASE_PRICE = v.BASE_PRICE,
                                                    BASE_PRICE_TEXT = v.BASE_PRICE_TEXT,
                                                    OSP_PREMIUM_DISCOUNT = v.OSP_PREMIUM_DISCOUNT,
                                                    OSP_PREMIUM_DISCOUNT_TEXT = v.OSP_PREMIUM_DISCOUNT_TEXT,
                                                    TRADING_PREMIUM_DISCOUNT = v.TRADING_PREMIUM_DISCOUNT,
                                                    TRADING_PREMIUM_DISCOUNT_TEXT = v.TRADING_PREMIUM_DISCOUNT_TEXT,
                                                    OTHER_COST = v.OTHER_COST,
                                                    OTHER_COST_TEXT = v.OTHER_COST_TEXT,
                                                    BASE_PRICE_PERIOD = v.BASE_PRICE_PERIOD,
                                                    TRADING_PRICE_PERIOD = v.TRADING_PRICE_PERIOD,
                                                    OTHER_PRICE_PERIOD = v.OTHER_PRICE_PERIOD
                                                });

                            var naphthaList = queryBongKot.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == "YBKC").ToList();
                            if (naphthaList.Count > 0)
                            {
                                item.BASE_PRICE = naphthaList[0].BASE_PRICE;
                                item.BASE_PRICE_TEXT = naphthaList[0].BASE_PRICE_TEXT;
                                item.BP_PRICING_PERIOD_FROM = naphthaList[0].BP_PRICING_PERIOD_FROM;
                                item.BP_PRICING_PERIOD_TO = naphthaList[0].BP_PRICING_PERIOD_TO;
                                item.DAILY_1 = naphthaList[0].DAILY_1;
                                item.DAILY_2 = naphthaList[0].DAILY_2;
                                item.DAILY_3 = naphthaList[0].DAILY_3;
                                item.DAILY_4 = naphthaList[0].DAILY_4;
                                item.DAILY_5 = naphthaList[0].DAILY_5;
                                item.DAILY_6 = naphthaList[0].DAILY_6;
                                item.DAILY_7 = naphthaList[0].DAILY_7;
                                item.MET_NUM = naphthaList[0].MET_NUM;
                                item.MONTHLY_1 = naphthaList[0].MONTHLY_1;
                                item.MONTHLY_2 = naphthaList[0].MONTHLY_2;
                                item.MONTHLY_3 = naphthaList[0].MONTHLY_3;
                                item.MONTHLY_4 = naphthaList[0].MONTHLY_4;
                                item.MONTHLY_5 = naphthaList[0].MONTHLY_5;
                                item.MONTHLY_6 = naphthaList[0].MONTHLY_6;
                                item.MONTHLY_7 = naphthaList[0].MONTHLY_7;
                                item.OSP_PREMIUM_DISCOUNT = naphthaList[0].OSP_PREMIUM_DISCOUNT;
                                item.OSP_PREMIUM_DISCOUNT_TEXT = naphthaList[0].OSP_PREMIUM_DISCOUNT_TEXT;
                                item.OSP_PRICING_PERIOD = naphthaList[0].OSP_PRICING_PERIOD;
                                item.OTHER_COST = naphthaList[0].OTHER_COST;
                                item.OTHER_COST_TEXT = naphthaList[0].OTHER_COST_TEXT;
                                item.PRICE_STATUS = naphthaList[0].PRICE_STATUS;
                                item.TRADING_PREMIUM_DISCOUNT = naphthaList[0].TRADING_PREMIUM_DISCOUNT;
                                item.TRADING_PREMIUM_DISCOUNT_TEXT = naphthaList[0].TRADING_PREMIUM_DISCOUNT_TEXT;
                                item.BASE_PRICE_PERIOD = naphthaList[0].BASE_PRICE_PERIOD;
                                item.TRADING_PRICE_PERIOD = naphthaList[0].TRADING_PRICE_PERIOD;
                                item.OTHER_PRICE_PERIOD = naphthaList[0].OTHER_PRICE_PERIOD;
                            }

                            #region Check Rollback Date
                            FormulaConditionItemList conItem = new FormulaConditionItemList();
                            conItem = (FormulaConditionItemList)Newtonsoft.Json.JsonConvert.DeserializeObject(item.BASE_PRICE.ToString(), typeof(FormulaConditionItemList));
                            conItem.result = new List<ResultTemp>();
                            List<FormulaPricingCondition> conItemList = new List<FormulaPricingCondition>();
                            conItemList = conItem.ConditionItem;
                            FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                            if (conItemList != null)
                            {
                                DateTime naph_dateFrom = DateTime.ParseExact(temp_dateFrom, format, provider);
                                DateTime naph_dateTo = DateTime.ParseExact(temp_dateTo, format, provider);
                                var IsBackWardPeriod = false;
                                do
                                {
                                    IsBackWardPeriod = false;
                                    DateTime dateTempStart = DateTime.ParseExact(temp_dateFrom, format, provider);
                                    if ((dateTempStart - naph_dateFrom).TotalDays > 365)
                                    {
                                        IsBackWardPeriod = true;
                                    }
                                    foreach (FormulaPricingCondition i in conItemList)
                                    {
                                        string date = naph_dateFrom.ToString(format, provider) + " to " + naph_dateTo.ToString(format, provider);
                                        if (i.Variable1 == "DAILY_1" || i.Variable1 == "DAILY_2" || i.Variable1 == "DAILY_3" || i.Variable1 == "DAILY_4" || i.Variable1 == "DAILY_5" || i.Variable1 == "DAILY_6" || i.Variable1 == "DAILY_7" || i.Variable1 == "MONTHLY_1" || i.Variable1 == "MONTHLY_2" || i.Variable1 == "MONTHLY_3" || i.Variable1 == "MONTHLY_4" || i.Variable1 == "MONTHLY_5" || i.Variable1 == "MONTHLY_6" || i.Variable1 == "MONTHLY_7")
                                        {
                                            int rowCount = 0;
                                            var val1 = service.getVariable(i.Variable1, item, conItem, date, context, 0, new ArrayList(), ref rowCount);
                                            if (val1 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                        }

                                        if (i.Variable2 == "DAILY_1" || i.Variable2 == "DAILY_2" || i.Variable2 == "DAILY_3" || i.Variable2 == "DAILY_4" || i.Variable2 == "DAILY_5" || i.Variable2 == "DAILY_6" || i.Variable2 == "DAILY_7" || i.Variable2 == "MONTHLY_1" || i.Variable2 == "MONTHLY_2" || i.Variable2 == "MONTHLY_3" || i.Variable2 == "MONTHLY_4" || i.Variable2 == "MONTHLY_5" || i.Variable2 == "MONTHLY_6" || i.Variable2 == "MONTHLY_7")
                                        {
                                            int rowCount = 0;
                                            var val2 = service.getVariable(i.Variable2, item, conItem, date, context, 0, new ArrayList(), ref rowCount);
                                            if (val2 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                        }
                                    }
                                    if (IsBackWardPeriod)
                                    {
                                        DateTime sDate = DateTime.ParseExact("01" + naph_dateFrom.ToString("/MM/yyyy", provider), format, provider);
                                        naph_dateFrom = sDate.AddMonths(-1);
                                        naph_dateTo = naph_dateFrom.AddMonths(1).AddDays(-1);
                                    }
                                    else
                                    {
                                        temp_dateFrom = naph_dateFrom.ToString(format, provider);
                                        temp_dateTo = naph_dateTo.ToString(format, provider);
                                        //date_from_to = dateFrom.ToString(format, provider) + " to " + naph_dateTo.ToString(format, provider);
                                    }
                                } while (IsBackWardPeriod);

                            }
                            #endregion

                            #endregion

                            pModel.data_Search.sCrudePhet.Item = new List<CrudePhetItemList>();
                            //int days = DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month);
                            DateTime startDate = DateTime.ParseExact(temp_dateFrom, format, provider);
                            int days = DateTime.DaysInMonth(startDate.Year, startDate.Month);
                            for (int k = 0; k < days; k++)
                            {
                                CrudePhetItemList vItem = new CrudePhetItemList();
                                vItem.Date = (k + 1).ToString().PadLeft(2, '0') + "/" + startDate.Month.ToString().PadLeft(2, '0') + "/" + dateFrom.Year.ToString();

                                pModel.data_Search.sCrudePhet.Item.Add(vItem);
                            }

                            int j = 0;
                            foreach (var m in pModel.data_Search.sCrudePhet.Daily)
                            {
                                j++;
                                //sDateFrom = "01/" + dateFrom.Month.ToString().PadLeft(2, '0') + "/" + dateFrom.Year.ToString();
                                //sDateTo = DateTime.ParseExact(sDateFrom, format, provider).AddMonths(1).AddDays(-1).ToString(format, provider);
                                sDateFrom = temp_dateFrom;
                                sDateTo = temp_dateTo;

                                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                                var t_mpb_valdate = (from v in context.MKT_TRN_MOPS_B
                                                     where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                                                     && m.Value.Contains(v.T_MPB_PRDCODE)
                                                     select v).OrderBy(p => p.T_MPB_VALDATE);

                                var bongkot = from d in t_mpb_valdate.AsEnumerable()
                                              where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                                              && Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                                              select d;
                                if (bongkot != null)
                                {
                                    if (bongkot.ToList().Count > 0)
                                    {
                                        foreach (var n in bongkot)
                                        {
                                            string bongkotDate = n.T_MPB_VALDATE.ToString(provider).Substring(6, 2) + "/" + n.T_MPB_VALDATE.ToString(provider).Substring(4, 2) + "/" + n.T_MPB_VALDATE.ToString(provider).Substring(0, 4);
                                            var o = pModel.data_Search.sCrudePhet.Item.Where(p => p.Date == bongkotDate).ToList();
                                            if (o.Count > 0)
                                            {
                                                switch (j)
                                                {
                                                    case 1:
                                                        o.FirstOrDefault().dValue1 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 2:
                                                        o.FirstOrDefault().dValue2 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 3:
                                                        o.FirstOrDefault().dValue3 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 4:
                                                        o.FirstOrDefault().dValue4 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 5:
                                                        o.FirstOrDefault().dValue5 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 6:
                                                        o.FirstOrDefault().dValue6 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 7:
                                                        o.FirstOrDefault().dValue7 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    default:
                                                        break;
                                                }

                                            }
                                        }
                                    }
                                }

                            }

                            j = 0;
                            foreach (var m in pModel.data_Search.sCrudePhet.Monthly)
                            {
                                j++;
                                //sDateFrom = "01/" + dateFrom.Month.ToString().PadLeft(2, '0') + "/" + dateFrom.Year.ToString();
                                //sDateTo = DateTime.ParseExact(sDateFrom, format, provider).AddMonths(1).AddDays(-1).ToString(format, provider);
                                sDateFrom = temp_dateFrom;
                                sDateTo = temp_dateTo;

                                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                                var queryBongkot = (from v in context.MKT_TRN_MOPS_B
                                                    where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                                                    && m.Value.Contains(v.T_MPB_PRDCODE)
                                                    select v).OrderBy(p => p.T_MPB_VALDATE);

                                var bongkot = from d in queryBongkot.AsEnumerable()
                                              where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                                              orderby d.T_MPB_VALDATE descending
                                              select d;
                                //Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom) && 
                                if (bongkot != null)
                                {
                                    if (bongkot.ToList().Count > 0)
                                    {
                                        //foreach (var n in bongkot)
                                        //{
                                        var n = bongkot.FirstOrDefault();
                                        //string bongkotDate = n.T_MPB_VALDATE.ToString().Substring(6, 2) + "/" + n.T_MPB_VALDATE.ToString().Substring(4, 2) + "/" + n.T_MPB_VALDATE.ToString().Substring(0, 4);
                                        if (pModel.data_Search.sCrudePhet.Item != null)
                                        {
                                            if (pModel.data_Search.sCrudePhet.Item.Count > 0)
                                            {
                                                switch (j)
                                                {
                                                    case 1:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue1 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 2:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue2 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 3:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue3 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 4:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue4 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 5:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue5 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 6:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue6 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    case 7:
                                                        pModel.data_Search.sCrudePhet.Item[0].mValue7 = (n.T_MPB_VALUE ?? 0).ToString();
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }

                                    }
                                }

                            }
                            //Add bongkot average value
                            if (pModel.data_Search.sCrudePhet.Item != null)
                            {
                                decimal d1 = 0; int count_d1 = 0;
                                decimal d2 = 0; int count_d2 = 0;
                                decimal d3 = 0; int count_d3 = 0;
                                decimal d4 = 0; int count_d4 = 0;
                                decimal d5 = 0; int count_d5 = 0;
                                decimal d6 = 0; int count_d6 = 0;
                                decimal d7 = 0; int count_d7 = 0;
                                decimal m1 = 0; int count_m1 = 0;
                                decimal m2 = 0; int count_m2 = 0;
                                decimal m3 = 0; int count_m3 = 0;
                                decimal m4 = 0; int count_m4 = 0;
                                decimal m5 = 0; int count_m5 = 0;
                                decimal m6 = 0; int count_m6 = 0;
                                decimal m7 = 0; int count_m7 = 0;
                                foreach (var s in pModel.data_Search.sCrudePhet.Item)
                                {
                                    decimal d1_number;
                                    if (decimal.TryParse(s.dValue1, out d1_number))
                                    {
                                        d1 += d1_number;
                                        count_d1++;
                                    }
                                    decimal d2_number;
                                    if (decimal.TryParse(s.dValue2, out d2_number))
                                    {
                                        d2 += d2_number;
                                        count_d2++;
                                    }
                                    decimal d3_number;
                                    if (decimal.TryParse(s.dValue3, out d3_number))
                                    {
                                        d3 += d3_number;
                                        count_d3++;
                                    }
                                    decimal d4_number;
                                    if (decimal.TryParse(s.dValue4, out d4_number))
                                    {
                                        d4 += d4_number;
                                        count_d4++;
                                    }
                                    decimal d5_number;
                                    if (decimal.TryParse(s.dValue5, out d5_number))
                                    {
                                        d5 += d5_number;
                                        count_d5++;
                                    }
                                    decimal d6_number;
                                    if (decimal.TryParse(s.dValue6, out d6_number))
                                    {
                                        d6 += d6_number;
                                        count_d6++;
                                    }
                                    decimal d7_number;
                                    if (decimal.TryParse(s.dValue7, out d7_number))
                                    {
                                        d7 += d7_number;
                                        count_d7++;
                                    }

                                    decimal m1_number;
                                    if (decimal.TryParse(s.mValue1, out m1_number))
                                    {
                                        m1 += m1_number;
                                        count_m1++;
                                    }
                                    decimal m2_number;
                                    if (decimal.TryParse(s.mValue2, out m2_number))
                                    {
                                        m2 += m2_number;
                                        count_m2++;
                                    }
                                    decimal m3_number;
                                    if (decimal.TryParse(s.mValue3, out m3_number))
                                    {
                                        m3 += m3_number;
                                        count_m3++;
                                    }
                                    decimal m4_number;
                                    if (decimal.TryParse(s.mValue4, out m4_number))
                                    {
                                        m4 += m4_number;
                                        count_m4++;
                                    }
                                    decimal m5_number;
                                    if (decimal.TryParse(s.mValue5, out m5_number))
                                    {
                                        m5 += m5_number;
                                        count_m5++;
                                    }
                                    decimal m6_number;
                                    if (decimal.TryParse(s.mValue6, out m6_number))
                                    {
                                        m6 += m6_number;
                                        count_m6++;
                                    }
                                    decimal m7_number;
                                    if (decimal.TryParse(s.mValue7, out m7_number))
                                    {
                                        m7 += m7_number;
                                        count_m7++;
                                    }
                                }
                                CrudePhetItemList vItem = new CrudePhetItemList();
                                vItem.Date = "Average";
                                vItem.dValue1 = count_d1 == 0 ? "" : Math.Round(d1 / count_d1, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.dValue2 = count_d2 == 0 ? "" : Math.Round(d2 / count_d2, 8, MidpointRounding.AwayFromZero).ToString(); ;
                                vItem.dValue3 = count_d3 == 0 ? "" : Math.Round(d3 / count_d3, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.dValue4 = count_d4 == 0 ? "" : Math.Round(d4 / count_d4, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.dValue5 = count_d5 == 0 ? "" : Math.Round(d5 / count_d5, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.dValue6 = count_d6 == 0 ? "" : Math.Round(d6 / count_d6, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.dValue7 = count_d7 == 0 ? "" : Math.Round(d7 / count_d7, 8, MidpointRounding.AwayFromZero).ToString();

                                vItem.mValue1 = count_m1 == 0 ? "" : Math.Round(m1 / count_m1, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.mValue2 = count_m2 == 0 ? "" : Math.Round(m2 / count_m2, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.mValue3 = count_m3 == 0 ? "" : Math.Round(m3 / count_m3, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.mValue4 = count_m4 == 0 ? "" : Math.Round(m4 / count_m4, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.mValue5 = count_m5 == 0 ? "" : Math.Round(m5 / count_m5, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.mValue6 = count_m6 == 0 ? "" : Math.Round(m6 / count_m6, 8, MidpointRounding.AwayFromZero).ToString();
                                vItem.mValue7 = count_m7 == 0 ? "" : Math.Round(m7 / count_m7, 8, MidpointRounding.AwayFromZero).ToString();

                                pModel.data_Search.sCrudePhet.Item.Add(vItem);

                            }


                        }
                    }

                }
                //--------------------------------------------Get  Product List
                getProductList(ref pModel);
            }
            catch (Exception ex)
            {
                //string path = path = (LogManager.GetCurrentLoggers()[0].Logger.Repository.GetAppenders()[0] as FileAppender).File;
                rtn.Message = ex.Message;
                rtn.Status = false;
                //log4net.Repository.Hierarchy.Hierarchy h = (log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository();
                //foreach (IAppender a in h.Root.Appenders)
                //{
                //    if (a is FileAppender)
                //    {
                //        FileAppender fa = (FileAppender)a;
                //        string sNowDate = DateTime.Now.ToLongDateString();
                //        // Programmatically set this to the desired location here
                //        string FileLocationinWebConfig = fa.File;
                //        // Determine where last backslash is.
                //        int position = FileLocationinWebConfig.LastIndexOf('\\');
                //        // If there is no backslash, assume that this is a filename.
                //        if (position == -1)
                //        {
                //            //หาไม่เจอให้ใช้ path file ในเวป config
                //        }
                //        else
                //        {
                //            // Determine whether file exists using filepath.
                //            FileLocationinWebConfig = FileLocationinWebConfig.Substring(0,position + 1);
                //            string logFileLocation = FileLocationinWebConfig + "Error.log";
                //            fa.File = logFileLocation;
                //        }
                //        fa.ActivateOptions();
                //        break;
                //    }
                //}

                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(":line"));
                //log.Error("1=====================================================================");
                //log.Error("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //log.Error(ex.Message);
                //Log.Error("2=====================================================================");
                //Log.Error("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
            }
            return rtn;
        }

        public void getPeriodFinalBongkot(ref FeedstockBookingPriceViewModel pModel, string sDateFrom, string sDateTo, EntityCPAIEngine context)
        {
            sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
            sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

            var queryTemp = (from v in context.MKT_TRN_MOPS_B
                             where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                             //&& prdCode.Contains(v.T_MPB_PRDCODE)
                             select v).OrderBy(p => p.T_MPB_VALDATE);

            var query = from d in queryTemp.AsEnumerable()
                        where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                        && Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                        select d;
        }

        public void getProductQualityLastFinal(ref FeedstockBookingPriceViewModel model)
        {
            try
            {
                CultureInfo provider = new CultureInfo("en-US");
                string format = "dd/MM/yyyy";
                var sDateFrom = String.IsNullOrEmpty(model.data_Search.vChangeDate) ? "" : model.data_Search.vChangeDate.ToUpper().Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(model.data_Search.vChangeDate) ? "" : model.data_Search.vChangeDate.ToUpper().Substring(14, 10);
                model.data_Search.vChangeDate = "";
                if (sDateFrom != "" && sDateTo != "")
                {
                    if (model.data_Search.vCopyFinalPrice)
                    {
                        model.data_Search.vCopyFinalPrice = false;

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            string tempFrom = model.data_Search.sPriceDetail.dQualityList[0].PODate;
                            DateTime tempDateFrom = DateTime.ParseExact(tempFrom, format, provider);

                            string feedCode = model.data_Search.sPriceDetail.dQualityList[0].Product_Code;
                            string feedDesc = model.data_Search.sPriceDetail.dQualityList[0].Product;

                            DateTime startDate = DateTime.Now;
                            DateTime endtDate = DateTime.Now;
                            //หาค่า final ย้อนหลังไปครึ่งเดือน 
                            if (tempDateFrom.Date.Day < 16)
                            {
                                startDate = DateTime.ParseExact("16/" +
                                (tempDateFrom.AddMonths(-1).Month).ToString().PadLeft(2, '0') + "/"
                                + (tempDateFrom.AddMonths(-1).Year).ToString(), format, provider);

                                endtDate = DateTime.ParseExact(
                                    "01/" + (tempDateFrom.Month).ToString().PadLeft(2, '0') + "/" + (tempDateFrom.Year).ToString()
                                    , format, provider).AddDays(-1);
                            }
                            else
                            {
                                startDate = DateTime.ParseExact("01/" +
                                (tempDateFrom.Month).ToString().PadLeft(2, '0') + "/"
                                + (tempDateFrom.Year).ToString(), format, provider);

                                endtDate = DateTime.ParseExact(
                                   "15/" + (tempDateFrom.Month).ToString().PadLeft(2, '0') + "/" + (tempDateFrom.Year).ToString()
                                   , format, provider);
                            }

                            var queryHalf = (from h in context.PIT_PO_HEADER
                                             join i in context.PIT_PO_ITEM on h.PO_NO equals i.PO_NO
                                             join p in context.PIT_PO_PRICE on new { i.PO_NO, i.PO_ITEM } equals new { p.PO_NO, p.PO_ITEM }
                                             where i.MET_NUM == feedCode && p.PO_DATE >= startDate && p.PO_DATE <= endtDate
                                                    && p.INVOICE_STATUS.Trim().ToUpper() == "FINAL" 
                                                    && p.NEW_UNIT_PRICE_THB > 0
                                             select new
                                             {
                                                 PO_DATE = p.PO_DATE,
                                                 OLD_UNIT_PRICE_USD = p.NEW_UNIT_PRICE_USD,
                                                 OLD_UNIT_PRICE_THB = p.NEW_UNIT_PRICE_THB,
                                                 OLD_EXCHANGE_RATE = p.NEW_ROE,
                                                 OLD_UNIT_PRICE = p.UNIT_PRICE
                                             }).OrderByDescending(p => p.PO_DATE).ToList();

                            DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                            DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);
                            sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                            sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);
                            if (queryHalf.Count > 0)
                            {
                                foreach (QualityProductDetail k in model.data_Search.sPriceDetail.dQualityList.
                        Where(p => Convert.ToInt32(p.PODate.Substring(6, 4) + p.PODate.Substring(3, 2) + p.PODate.Substring(0, 2)) >= Convert.ToInt32(sDateFrom)
                        && Convert.ToInt32(p.PODate.Substring(6, 4) + p.PODate.Substring(3, 2) + p.PODate.Substring(0, 2)) <= Convert.ToInt32(sDateTo)
                        ))
                                {
                                    
                                    //k.UnitPriceUSDNew = "0";
                                    //k.ExchangeRateNew = "0";
                                    //k.UnitPriceTHBOld = "0";

                                    k.UnitPriceUSDOld = queryHalf.FirstOrDefault().OLD_UNIT_PRICE_USD.ToString();
                                    k.ExchangeRateOld = queryHalf.FirstOrDefault().OLD_EXCHANGE_RATE.ToString();
                                    k.UnitPriceTHBOld = queryHalf.FirstOrDefault().OLD_UNIT_PRICE_THB.ToString();
                                    k.UnitPrice = queryHalf.FirstOrDefault().OLD_UNIT_PRICE.ToString();
                                }
                            }
                            else
                            {
                                foreach (QualityProductDetail k in model.data_Search.sPriceDetail.dQualityList.
                        Where(p => Convert.ToInt32(p.PODate.Substring(6, 4) + p.PODate.Substring(3, 2) + p.PODate.Substring(0, 2)) >= Convert.ToInt32(sDateFrom)
                        && Convert.ToInt32(p.PODate.Substring(6, 4) + p.PODate.Substring(3, 2) + p.PODate.Substring(0, 2)) <= Convert.ToInt32(sDateTo)
                        ))
                                {
                                    //k.UnitPriceUSDNew = "0";
                                    //k.ExchangeRateNew = "0";
                                    //k.UnitPriceTHBOld = "0";

                                    k.UnitPriceUSDOld = "0";
                                    k.ExchangeRateOld = "0";
                                    k.UnitPriceTHBOld = "0";

                                }
                            }
                        }
                    }
                    else
                    {
                        sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                        sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                        foreach (QualityProductDetail k in model.data_Search.sPriceDetail.dQualityList.
                        Where(p => Convert.ToInt32(string.IsNullOrEmpty(p.PODate) ? "0" : p.PODate.Substring(6, 4) + p.PODate.Substring(3, 2) + p.PODate.Substring(0, 2)) >= Convert.ToInt32(sDateFrom)
                        && Convert.ToInt32(string.IsNullOrEmpty(p.PODate) ? "0" : p.PODate.Substring(6, 4) + p.PODate.Substring(3, 2) + p.PODate.Substring(0, 2)) <= Convert.ToInt32(sDateTo)
                        ))
                        {
                            if (model.data_Search.vDen15_select == true && model.data_Search.vDen15 != "")
                            {
                                k.Den_15 = model.data_Search.vDen15;
                            }
                            if (model.data_Search.vVisc_select == true && model.data_Search.vVisc != "")
                            {
                                k.Visc_cst_50 = model.data_Search.vVisc;
                            }
                            if (model.data_Search.vSulfer_select == true && model.data_Search.vSulfer != "")
                            {
                                k.Sulfur = model.data_Search.vSulfer;

                            }
                            if (model.data_Search.vExchangeOld_select == true && model.data_Search.vExchangeOld != "")
                            {
                                k.ExchangeRateOld = model.data_Search.vExchangeOld;

                            }
                            if (model.data_Search.vUSDOld_select == true && model.data_Search.vUSDOld != "")
                            {
                                k.UnitPriceUSDOld = model.data_Search.vUSDOld;

                            }
                            if (model.data_Search.vTHBOld_select == true && model.data_Search.vTHBOld != "")
                            {
                                k.UnitPriceTHBOld = model.data_Search.vTHBOld;

                            }
                            if (model.data_Search.vInvoice_select == true && model.data_Search.vInvoice != "")
                            {
                                k.InvoiceNoStatus = model.data_Search.vInvoice;

                            }
                        }

                        model.data_Search.vDen15_select = false;
                        model.data_Search.vDen15 = "";
                        model.data_Search.vVisc_select = false;
                        model.data_Search.vVisc = "";
                        model.data_Search.vSulfer_select = false;
                        model.data_Search.vSulfer = "";
                        model.data_Search.vExchangeOld_select = false;
                        model.data_Search.vExchangeOld = "";
                        model.data_Search.vUSDOld_select = false;
                        model.data_Search.vUSDOld = "";
                        model.data_Search.vTHBOld_select = false;
                        model.data_Search.vTHBOld = "";
                        //model.data_Search.vInvoice_select = false;
                        //model.data_Search.vInvoice = "";
                    }
                }
                getProductList(ref model);
            }
            catch (Exception ex)
            {

            }

        }

        public void setProductQuality(ref FeedstockBookingPriceViewModel pModel)
        {
            pModel.data_Search.sPriceDetail.dQualityList.Add(
                new QualityProductDetail
                {
                    NO = "0",
                    PODate = "0",
                    Visc_cst_50 = "0",
                    Sulfur = "0",
                    Den_15 = "0",
                    Price = "0"
                });

            if (String.IsNullOrEmpty(pModel.data_Search.vUpdatePeriod))
            {
                return;
            }

            var sDateFrom = String.IsNullOrEmpty(pModel.data_Search.vUpdatePeriod) ? "" : pModel.data_Search.vUpdatePeriod.ToUpper().Substring(0, 10);
            var sDateTo = String.IsNullOrEmpty(pModel.data_Search.vUpdatePeriod) ? "" : pModel.data_Search.vUpdatePeriod.ToUpper().Substring(14, 10);
            DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
            DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

            sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
            sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

            int sumDate = dateTo.Subtract(dateFrom).Days + 1;
            int dateFocus = Convert.ToInt32(sDateFrom);

            for (int j = 1; j <= sumDate; j++)
            {
                pModel.data_Search.sPriceDetail.dQualityList.Add(
                new QualityProductDetail
                {
                    NO = j.ToString(),
                    PODate = dateFocus.ToString().Substring(6, 2) + "/" + dateFocus.ToString().Substring(4, 2) + "/" + dateFocus.ToString().Substring(0, 4),
                    Visc_cst_50 = "0",
                    Sulfur = "0",
                    Den_15 = "0",
                    Price = "0"
                });

                DateTime tempDate = DateTime.ParseExact(dateFocus.ToString().Substring(6, 2) + "/" + dateFocus.ToString().Substring(4, 2) + "/" + dateFocus.ToString().Substring(0, 4)
                                , format, provider);

                tempDate = tempDate.AddDays(1);
                dateFocus = Convert.ToInt32(
                    tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(6, 4) +
                    tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(3, 2) +
                    tempDate.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(0, 2)
                            );
            }
        }


        public BlendedKeroseneControl setValueBlendedKerosene(FeedStockBookingPriceSearch temp)
        {
            CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
            BlendedKeroseneControl item = new BlendedKeroseneControl();

            item.MOPSKero = Math.Round(Convert.ToDecimal(temp.sPriceDetail.USD_LABIX_KERO ?? "0"), 3, MidpointRounding.AwayFromZero).ToString();
            item.MOPSKeroMinus1 = "0";//item.MOPSKeroMinus1 = "0.550";
            if (dataList.BlendedKerosene.Where(p => p.CODE == "C5").ToList().Count > 0)
            {
                string val = dataList.BlendedKerosene.Where(p => p.CODE == "C5").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    item.MOPSKeroMinus1 = val;
                }

            }
            item.MOPSKeroMinus2 = "0"; //item.MOPSKeroMinus2 = "0.2";
            if (dataList.BlendedKerosene.Where(p => p.CODE == "C7").ToList().Count > 0)
            {
                string val = dataList.BlendedKerosene.Where(p => p.CODE == "C7").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    item.MOPSKeroMinus2 = val;
                }

            }
            item.FX = "1";
            if (!string.IsNullOrEmpty(temp.sPriceDetail.EXC_AVERAGE) && temp.sPriceDetail.EXC_AVERAGE != "0")
            {
                decimal valTemp = 0m;
                if (dataList.BlendedKerosene.Where(p => p.CODE == "B9").ToList().Count > 0)
                {
                    string val = dataList.BlendedKerosene.Where(p => p.CODE == "B9").FirstOrDefault().VALUE;
                    decimal number;
                    if (decimal.TryParse(val, out number))
                    {
                        valTemp = number;
                    }

                }
                item.FX = (Convert.ToDecimal(temp.sPriceDetail.EXC_AVERAGE ?? "0") + valTemp).ToString();
            }

            decimal mopsKero = ((Decimal)Math.Round(Convert.ToDecimal(item.MOPSKero ?? "0"), 3, MidpointRounding.AwayFromZero));
            item.MOPSKeroMinus1_Sum = (mopsKero - Convert.ToDecimal(item.MOPSKeroMinus1 ?? "0")).ToString();
            item.BlendedKerosene_US = (Convert.ToDecimal(item.MOPSKeroMinus1_Sum ?? "0") + Convert.ToDecimal(item.MOPSKeroMinus2 ?? "0")).ToString();
            item.BlendedKerosene_Baht = Math.Round((Convert.ToDecimal(item.BlendedKerosene_US ?? "0") * Convert.ToDecimal(item.FX ?? "0")), 3, MidpointRounding.AwayFromZero).ToString();

            return item;
        }

        public StripperOVHDLiquidControl setValueStripperOVHDLiquid(FeedStockBookingPriceDetail temp, FeedstockBookingPriceViewModel model)
        {
            CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
            StripperOVHDLiquidControl i = new StripperOVHDLiquidControl();
            i.Union_MOPSNaph = (Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_NAPHTA), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Union_MOPSNaphMinus = "3";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C18").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C18").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Union_MOPSNaphMinus = val;
                }
            }
            i.Union_MOPSNaphMultiply = "0.54";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C20").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C20").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Union_MOPSNaphMultiply = val;
                }
            }
            i.Union_MOPSNaphMinus_Sum = (Math.Round(((Decimal)Convert.ToDecimal(i.Union_MOPSNaph ?? "0") - (Decimal)Convert.ToDecimal(i.Union_MOPSNaphMinus ?? "0")), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Union_firstPortion = (Math.Round((Decimal)Convert.ToDecimal(i.Union_MOPSNaphMinus_Sum ?? "0") * (Decimal)Convert.ToDecimal(i.Union_MOPSNaphMultiply ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            i.Union_MOPSKero = (Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_KERO), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Union_MOPSKeroMinus = "0.550";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C24").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C24").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Union_MOPSKeroMinus = val;
                }
            }
            i.Union_MOPSKero_Multiply = "0.46";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C26").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C26").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Union_MOPSKero_Multiply = val;
                }
            }
            i.Union_MOPSKeroMinus_Sum = ((Decimal)Convert.ToDecimal(i.Union_MOPSKero ?? "0") - (Decimal)Convert.ToDecimal(i.Union_MOPSKeroMinus ?? "0")).ToString();
            i.Union_SecondPortion = (Math.Round((Decimal)Convert.ToDecimal(i.Union_MOPSKeroMinus_Sum ?? "0") * (Decimal)Convert.ToDecimal(i.Union_MOPSKero_Multiply ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            i.Union_First_Second = ((Decimal)Convert.ToDecimal(i.Union_firstPortion ?? "0") + (Decimal)Convert.ToDecimal(i.Union_SecondPortion ?? "0")).ToString();

            decimal c31 = 16.7m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C31").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C31").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    c31 = number;
                }
            }
            decimal e17 = 9m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "E17").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "E17").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    e17 = number;
                }
            }
            decimal e23 = 7.9m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "E23").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "E23").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    e23 = number;
                }
            }
            i.Union_MinusProcessingCostBBL = Math.Round((Decimal)c31 / ((Convert.ToDecimal(i.Union_MOPSNaphMultiply) * e17) + (Convert.ToDecimal(i.Union_MOPSKero_Multiply) * e23)), 3, MidpointRounding.AwayFromZero).ToString();
            i.Union_UnionfiningPrice = (Convert.ToDecimal(i.Union_First_Second ?? "0") - Convert.ToDecimal(i.Union_MinusProcessingCostBBL ?? "0")).ToString();

            i.Pacol_LPG = (Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_LPG), 3, MidpointRounding.AwayFromZero)).ToString(); ;
            i.Pacol_LPG_Multiply = "0.76";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C39").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C39").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_LPG_Multiply = val;
                }
            }
            i.Pacol_LPG_Multiply_Sum1 = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_LPG ?? "0") * (Decimal)Convert.ToDecimal(i.Pacol_LPG_Multiply ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Pacol_LPG_Multiply_Plus = "79.680";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C41").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C41").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_LPG_Multiply_Plus = val;
                }
            }
            i.Pacol_LPG_Multiply_Sum1 = ((Decimal)Convert.ToDecimal(i.Pacol_LPG_Multiply_Sum1 ?? "0") + (Decimal)Convert.ToDecimal(i.Pacol_LPG_Multiply_Plus ?? "0")).ToString();
            i.Pacol_LPG_Multiply_Multiple = "0.07";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C43").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C43").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_LPG_Multiply_Multiple = val;
                }
            }
            i.Pacol_LPG_firstPortionTon = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_LPG_Multiply_Sum1 ?? "0") * (Decimal)Convert.ToDecimal(i.Pacol_LPG_Multiply_Multiple ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            decimal e38 = 11.2m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "E38").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "E38").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    e38 = number;
                }
            }
            i.Pacol_LPG_Portion = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_LPG_firstPortionTon ?? "0") / e38, 3, MidpointRounding.AwayFromZero)).ToString();

            i.Pacol_Naph = (Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_NAPHTA), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Pacol_Naph_Minus = "3";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C48").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C48").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_Naph_Minus = val;
                }
            }
            i.Pacol_Naph_Minus_Sum1 = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_Naph ?? "0") - (Decimal)Convert.ToDecimal(i.Pacol_Naph_Minus ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Pacol_Naph_Multiply = "0.28";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C50").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C50").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_Naph_Multiply = val;
                }
            }
            i.Pacol_Naph_SecondPortion = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_Naph_Minus_Sum1 ?? "0") * (Decimal)Convert.ToDecimal(i.Pacol_Naph_Multiply ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            i.Pacol_ULG92 = (Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_ULG92), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Pacol_ULG92_Minus = "1";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C54").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C54").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_ULG92_Minus = val;
                }
            }
            i.Pacol_ULG92_Minus_Sum = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_ULG92 ?? "0") - (Decimal)Convert.ToDecimal(i.Pacol_ULG92_Minus ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();
            i.Pacol_ULG92_Multiply = "0.65";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C56").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C56").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Pacol_ULG92_Multiply = val;
                }
            }
            i.Pacol_ULG92_ThirdPortion = (Math.Round((Decimal)Convert.ToDecimal(i.Pacol_ULG92_Minus_Sum ?? "0") * (Decimal)Convert.ToDecimal(i.Pacol_ULG92_Multiply ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            i.Plus_First_Second_Third = ((Decimal)Convert.ToDecimal(i.Pacol_LPG_Portion ?? "0") + (Decimal)Convert.ToDecimal(i.Pacol_Naph_SecondPortion ?? "0") + (Decimal)Convert.ToDecimal(i.Pacol_ULG92_ThirdPortion ?? "0")).ToString();

            i.MinusProcessingCostTon = "22.8";
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "C61").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "C61").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.MinusProcessingCostTon = val;
                }
            }
            decimal e47 = 9m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "E47").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "E47").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    e47 = number;
                }
            }

            decimal e53 = 9m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "E53").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "E53").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    e53 = number;
                }
            }
            i.MinusProcessingCostBBL = (Math.Round(Convert.ToDecimal(i.MinusProcessingCostTon ?? "0") / ((Convert.ToDecimal(i.Pacol_LPG_Multiply_Multiple) * e38) +
                (Convert.ToDecimal(i.Pacol_Naph_Multiply) * e47) +
                (Convert.ToDecimal(i.Pacol_ULG92_Multiply) * e53)), 3, MidpointRounding.AwayFromZero)).ToString();
            i.PacolPrice = ((Decimal)Convert.ToDecimal(i.Plus_First_Second_Third ?? "0") - (Decimal)Convert.ToDecimal(i.MinusProcessingCostBBL ?? "0")).ToString();

            string status = "PRO";
            Decimal unfinVolumn = 0;
            Decimal pacalVolumn = 0;
            if (model.data_Search != null)
            {
                if (!string.IsNullOrEmpty(model.data_Search.vInvoice))
                {
                    if (model.data_Search.vInvoice == "FINAL")
                    {
                        status = "FINAL";
                        if (!string.IsNullOrEmpty(model.data_Search.vVolumnUnionFining))
                        {
                            unfinVolumn = Convert.ToDecimal(model.data_Search.vVolumnUnionFining);
                        }
                        if (!string.IsNullOrEmpty(model.data_Search.vVolumnPacol))
                        {
                            pacalVolumn = Convert.ToDecimal(model.data_Search.vVolumnPacol);
                        }
                    }
                }
            }

            decimal b70 = 3.84m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "B70").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "B70").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    b70 = number;
                }
            }

            decimal b71 = 3m;
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "B71").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "B71").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    b71 = number;
                }
            }

            decimal b73 = 0m;//default 0.05
            if (dataList.StripperOVHDLiquid.Where(p => p.CODE == "B73").ToList().Count > 0)
            {
                string val = dataList.StripperOVHDLiquid.Where(p => p.CODE == "B73").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    b73 = number;
                }
            }
            if (status == "PRO")
            {
                i.Pro_StripperOVHD_US = (Math.Round(((Convert.ToDecimal(i.Union_UnionfiningPrice) * b70) +
                (Convert.ToDecimal(i.PacolPrice) * b71)) / (b70 + b71), 3, MidpointRounding.AwayFromZero)).ToString();
                i.Pro_FX = "1";
                if (temp.exchangeRate.Count > 0)
                {
                    i.Pro_FX = (Convert.ToDecimal(temp.exchangeRate[0].Average ?? "0") + b73).ToString();
                }
                i.Pro_StripperOVHD_Baht = (Math.Round((Decimal)Convert.ToDecimal(i.Pro_StripperOVHD_US ?? "0") * (Decimal)Convert.ToDecimal(i.Pro_FX ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                i.Pro_StripperOVHD_US = (Math.Round(((Convert.ToDecimal(i.Union_UnionfiningPrice) * unfinVolumn) +
                (Convert.ToDecimal(i.PacolPrice) * pacalVolumn)) / (unfinVolumn + pacalVolumn), 3, MidpointRounding.AwayFromZero)).ToString();
                i.Pro_FX = "1";
                if (temp.exchangeRate.Count > 0)
                {
                    i.Pro_FX = (Convert.ToDecimal(temp.exchangeRate[0].Average ?? "0") + b73).ToString();
                }
                i.Pro_StripperOVHD_Baht = (Math.Round((Decimal)Convert.ToDecimal(i.Pro_StripperOVHD_US ?? "0") * (Decimal)Convert.ToDecimal(i.Pro_FX ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            }
            return i;
        }

        public PEPAromaticsAndHABControl setValuePEPAromatics(FeedStockBookingPriceDetail temp, FeedstockBookingPriceViewModel model)
        {
            CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
            PEPAromaticsAndHABControl i = new PEPAromaticsAndHABControl();
            i.MOPSGO = Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_GASOIL), 3, MidpointRounding.AwayFromZero).ToString();
            i.MOPSGOMinus = "0.7";
            if (dataList.PEPAromatics.Where(p => p.CODE == "I5").ToList().Count > 0)
            {
                string val = dataList.PEPAromatics.Where(p => p.CODE == "I5").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.MOPSGOMinus = val;
                }

            }
            i.PEPAromatics = ((Decimal)Convert.ToDecimal(i.MOPSGO ?? "0") - (Decimal)Convert.ToDecimal(i.MOPSGOMinus ?? "0")).ToString();

            i.MOPSFO380 = model.data_Search.sPriceDetail.USD_LABIX_FO380;
            i.MOPSFO380_PLUS = "80.00";
            if (dataList.PEPAromatics.Where(p => p.CODE == "I11").ToList().Count > 0)
            {
                string val = dataList.PEPAromatics.Where(p => p.CODE == "I11").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.MOPSFO380_PLUS = val;
                }

            }
            i.HAB_Ton = (Math.Round(Convert.ToDecimal(i.MOPSFO380 ?? "0") + Convert.ToDecimal(i.MOPSFO380_PLUS ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();
            i.HAB_BBL_DIV = "7.3";
            if (dataList.PEPAromatics.Where(p => p.CODE == "K10").ToList().Count > 0)
            {
                string val = dataList.PEPAromatics.Where(p => p.CODE == "K10").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.HAB_BBL_DIV = val;
                }

            }
            i.HAB_BBL = (Math.Round(Convert.ToDecimal(i.HAB_Ton ?? "0") / (Decimal)Convert.ToDecimal(i.HAB_BBL_DIV ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            string status = "PRO";
            Decimal pepVolumn = 0;
            Decimal hapVolumn = 0;
            if (model.data_Search != null)
            {
                if (!string.IsNullOrEmpty(model.data_Search.vInvoice))
                {
                    if (model.data_Search.vInvoice == "FINAL")
                    {
                        status = "FINAL";
                        if (!string.IsNullOrEmpty(model.data_Search.vVolumnPEPAromatics))
                        {
                            pepVolumn = Convert.ToDecimal(model.data_Search.vVolumnPEPAromatics);
                        }
                        if (!string.IsNullOrEmpty(model.data_Search.vVolumnHAB))
                        {
                            hapVolumn = Convert.ToDecimal(model.data_Search.vVolumnHAB);
                        }
                    }
                }
            }

            decimal h21 = 9.07m;
            if (dataList.PEPAromatics.Where(p => p.CODE == "H21").ToList().Count > 0)
            {
                string val = dataList.PEPAromatics.Where(p => p.CODE == "H21").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    h21 = number;
                }

            }
            decimal h22 = 4.34m;
            if (dataList.PEPAromatics.Where(p => p.CODE == "H22").ToList().Count > 0)
            {
                string val = dataList.PEPAromatics.Where(p => p.CODE == "H22").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    h22 = number;
                }

            }
            decimal h24 = 0.05m;
            if (dataList.PEPAromatics.Where(p => p.CODE == "H24").ToList().Count > 0)
            {
                string val = dataList.PEPAromatics.Where(p => p.CODE == "H24").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    h24 = number;
                }

            }
            if (status == "PRO")
            {
                //Pro
                i.Pro_FX = "1";
                if (temp.exchangeRate.Count > 0)
                {
                    i.Pro_FX = (Convert.ToDecimal(temp.exchangeRate[0].Average ?? "0") + h24).ToString();
                }

                i.Pro_PEPAromaticsHAB_US = (Math.Round(((Convert.ToDecimal(i.PEPAromatics) * h21) +
                    (Convert.ToDecimal(i.HAB_BBL) * h22)) / (h21 + h22), 3, MidpointRounding.AwayFromZero)).ToString();
                i.Pro_PEPAromaticsHAB_Baht = (Math.Round((Decimal)Convert.ToDecimal(i.Pro_PEPAromaticsHAB_US ?? "0") * (Decimal)Convert.ToDecimal(i.Pro_FX ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            }
            else
            {
                //Final
                //i.Final_PEPAromaticsHAB_US = (Math.Round(((Convert.ToDecimal(i.PEPAromatics) * pepVolumn) +
                //    (Convert.ToDecimal(i.HAB_BBL) * hapVolumn)) / (pepVolumn + hapVolumn), 3)).ToString();

                //i.Final_PEPAromaticsHAB_Baht = ((Decimal)Convert.ToDecimal(i.Final_PEPAromaticsHAB_US ?? "0") * (Decimal)Convert.ToDecimal(i.Final_FX ?? "0")).ToString();
                //i.Final_FX = "1";
                //if (temp.exchangeRate.Count > 0)
                //{
                //    i.Final_FX = (i.Final_FX + h24).ToString();
                //}
                i.Final_FX = "1";
                if (temp.exchangeRate.Count > 0)
                {
                    i.Final_FX = (Convert.ToDecimal(temp.exchangeRate[0].Average ?? "0") + h24).ToString();
                }
                i.Pro_PEPAromaticsHAB_US = (Math.Round(((Convert.ToDecimal(i.PEPAromatics) * pepVolumn) +
                    (Convert.ToDecimal(i.HAB_BBL) * hapVolumn)) / ((pepVolumn + hapVolumn) == 0 ? 1 : pepVolumn + hapVolumn), 3, MidpointRounding.AwayFromZero)).ToString();

                i.Pro_PEPAromaticsHAB_Baht = (Math.Round((Decimal)Convert.ToDecimal(i.Pro_PEPAromaticsHAB_US ?? "0") * (Decimal)Convert.ToDecimal(i.Final_FX ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            }
            return i;
        }

        public DrainHydrocarbon setValueDrainHydrocarbon(FeedStockBookingPriceDetail temp, FeedstockBookingPriceViewModel model)
        {
            CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
            DrainHydrocarbon i = new DrainHydrocarbon();
            i.MOPSKero = Math.Round(Convert.ToDecimal(model.data_Search.sPriceDetail.USD_LABIX_KERO), 3, MidpointRounding.AwayFromZero).ToString();
            i.Minus1 = "0.550";
            if (dataList.DrainHydrocarbons.Where(p => p.CODE == "I39").ToList().Count > 0)
            {
                string val = dataList.DrainHydrocarbons.Where(p => p.CODE == "I39").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Minus1 = val;
                }

            }
            i.Minus2 = "0.2";
            if (dataList.DrainHydrocarbons.Where(p => p.CODE == "I41").ToList().Count > 0)
            {
                string val = dataList.DrainHydrocarbons.Where(p => p.CODE == "I41").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.Minus2 = val;
                }

            }
            i.MinusProcessionCost = "19.81";
            if (dataList.DrainHydrocarbons.Where(p => p.CODE == "I42").ToList().Count > 0)
            {
                string val = dataList.DrainHydrocarbons.Where(p => p.CODE == "I42").FirstOrDefault().VALUE;
                decimal number;
                if (decimal.TryParse(val, out number))
                {
                    i.MinusProcessionCost = val;
                }

            }

            i.DrainHCPrice_US = ((Decimal)Convert.ToDecimal(i.MOPSKero ?? "0") - ((Decimal)Convert.ToDecimal(i.Minus1 ?? "0") + (Decimal)Convert.ToDecimal(i.Minus2 ?? "0") + (Decimal)Convert.ToDecimal(i.MinusProcessionCost ?? "0"))).ToString();
            i.FX = "1";
            if (temp.exchangeRate.Count > 0)
            {
                decimal valTemp = 0m;
                if (dataList.DrainHydrocarbons.Where(p => p.CODE == "H44").ToList().Count > 0)
                {
                    string val = dataList.DrainHydrocarbons.Where(p => p.CODE == "H44").FirstOrDefault().VALUE;
                    decimal number;
                    if (decimal.TryParse(val, out number))
                    {
                        valTemp = number;
                    }

                }
                i.FX = (Convert.ToDecimal(temp.exchangeRate[0].Average ?? "0") + (decimal)valTemp).ToString();
            }
            i.DrainHCPrice_Baht = (Math.Round((Decimal)Convert.ToDecimal(i.DrainHCPrice_US ?? "0") * (Decimal)Convert.ToDecimal(i.FX ?? "0"), 3, MidpointRounding.AwayFromZero)).ToString();

            return i;
        }

        public decimal getBongKotPrice(string status, string sDateFrom, string sDateTo)
        {
            FormulaPricingServiceModel ser = new FormulaPricingServiceModel();
            var bongkotItemList = ser.PriceCalculate("OSP", "YBKC", (status == "FINAL" ? "A" : "P"), sDateFrom + " To " + sDateTo, "", new ArrayList());
            //pModel.data_Search.sPriceDetail.dQualityList
            decimal bongkotPrice = 0;
            try
            {
                if (bongkotItemList.Count > 0)
                {
                    foreach (var i in bongkotItemList)
                    {
                        if (i.Text == "BASE_PRICE")
                        {
                            bongkotPrice = Decimal.Parse(i.Value);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                bongkotPrice = 0;
            }
            return bongkotPrice;
        }

        //public FeedStockBookingPriceDetail setValueNaphTha(FeedStockBookingPriceDetail temp, string bongkotPrice, string den15Value)
        //{
        //    temp.naphThaPro = new List<NaphThaProViewModel>();
        //    foreach (FeedStockDetailViewModel i in temp.feedstockDetail)
        //    {
        //        string den15 = den15Value;//ดึงมาจากไหน
        //        string avgBongkot = bongkotPrice; //"37.7472";//ดึงมาจากไหน
        //        if (i.Product.Trim().ToUpper().Contains("NAPHTHA"))
        //        {
        //            NaphThaProViewModel naph = new NaphThaProViewModel();
        //            naph.Date = i.Date;
        //            naph.TripNo = i.TripNo;
        //            naph.Product = i.Product;
        //            naph.Volume = i.Volume;
        //            naph.Den15 = den15;
        //            naph.AvgBongkot = avgBongkot;
        //            naph.Price = Math.Round(((Convert.ToDecimal(naph.AvgBongkot)) * (Convert.ToDecimal("6.293") / (Convert.ToDecimal(naph.Den15) - Convert.ToDecimal("0.0011")))), 4).ToString();
        //            temp.naphThaPro.Add(naph);

        //            i.UnitPriceNewMT = naph.Price;
        //            i.Volume = i.Volume == "" ? "0" : i.Volume;
        //            i.UnitPriceNewBathMT = ((Decimal)Math.Round(Convert.ToDecimal(i.UnitPriceNewMT ?? "0") * (Convert.ToDecimal(i.ExchangeRateNew ?? "0")), 4)).ToString();
        //            i.UnitPriceOldBathMT = ((Decimal)Math.Round(Convert.ToDecimal(i.UnitPriceOldMT ?? "0") * (Convert.ToDecimal(i.ExchangeRateOld ?? "0")), 4)).ToString();
        //            i.ProvisionalPrice = ((Decimal)Math.Round(Convert.ToDecimal(i.Volume ?? "0") * (Convert.ToDecimal(i.UnitPriceOldBathMT ?? "0")), 2)).ToString();
        //            i.ProvisionalVat = Math.Round((Convert.ToDecimal(i.ProvisionalPrice) / 100) * 7, 2).ToString();
        //            i.ProvisionalTotal = (Convert.ToDecimal(i.ProvisionalPrice) + Convert.ToDecimal(i.ProvisionalVat)).ToString();
        //            i.FinalPrice = ((Decimal)Math.Round(Convert.ToDecimal(i.Volume ?? "0") * (Convert.ToDecimal(i.UnitPriceNewBathMT ?? "0")), 2)).ToString();
        //            i.FinalVat = Math.Round((Convert.ToDecimal(i.FinalPrice) / 100) * 7, 2).ToString();
        //            i.FinalTotal = (Convert.ToDecimal(i.FinalPrice) + Convert.ToDecimal(i.FinalVat)).ToString();

        //            i.PriceIncludeVat = (Convert.ToDecimal(i.FinalTotal) / Convert.ToDecimal(i.Volume)).ToString();
        //            i.Final_Pro = (Convert.ToDecimal(i.UnitPriceNewBathMT) - Convert.ToDecimal(i.UnitPriceOldBathMT)).ToString();
        //            i.AmountExcVat = Math.Round(Convert.ToDecimal(i.Final_Pro) * Convert.ToDecimal(i.Volume), 2).ToString();
        //            i.Vat = Math.Round((Convert.ToDecimal(i.AmountExcVat) / 100) * 7, 2).ToString();
        //            i.TotalIncVat = (Convert.ToDecimal(i.AmountExcVat) + Convert.ToDecimal(i.Vat)).ToString();
        //        }
        //    }

        //    return temp;
        //}

        public void mainCalculatePrice(ref FeedstockBookingPriceViewModel model)
        {
            try
            {
                FeedStockBookingPriceDetail temp = new FeedStockBookingPriceDetail();
                temp.feedstockDetail = new List<FeedStockDetailViewModel>();
                List<QualityProductDetail> dataVal = new List<QualityProductDetail>();
                //model.data_Search.sPriceDetail.dQualityList.Where(p => (p.Den_15 != "" && p.Den_15 != "0") &&
                //(p.Visc_cst_50 != "" && p.Visc_cst_50 != "0") && (p.Sulfur != "" && p.Sulfur != "0"))
                dataVal = model.data_Search.sPriceDetail.dQualityList.ToList();

                foreach (QualityProductDetail q in dataVal)
                {
                    FeedStockDetailViewModel qList = new FeedStockDetailViewModel();

                    qList.Date = q.PODate;
                    qList.InvoiceNo = q.InvoiceNo;
                    qList.TripNo = q.TripNo;
                    qList.Product = q.Product;
                    qList.Volume = q.VolumnMT;

                    qList.Quality_Den15 = !string.IsNullOrEmpty(q.Den_15) ? q.Den_15 : "0";
                    qList.Quality_cst50 = !string.IsNullOrEmpty(q.Visc_cst_50) ? q.Visc_cst_50 : "0";
                    qList.Quality_Sulfur = !string.IsNullOrEmpty(q.Sulfur) ? q.Sulfur : "0";
                    qList.UnitPriceNewMT = q.UnitPriceUSDNew;
                    qList.UnitPriceOldMT = q.UnitPriceUSDOld;

                    qList.AdjustPrice = q.AdjustPrice;
                    qList.ExchangeRateNew = q.ExchangeRateNew;
                    qList.ExchangeRateOld = q.ExchangeRateOld;
                    qList.UnitPriceNewBathMT = q.UnitPriceTHBNew;
                    qList.UnitPriceOldBathMT = q.UnitPriceTHBOld;
                    temp.feedstockDetail.Add(qList);
                }
                var sDateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);

                if (model.data_Search.sPriceDetail.dQualityList.Count > 17)
                {
                    sDateFrom = String.IsNullOrEmpty(model.data_Search.sPriceDetail.dQualityList[0].PODate) ? "" : model.data_Search.sPriceDetail.dQualityList[0].PODate;
                    DateTime dTemp = DateTime.ParseExact(sDateFrom, format, provider);
                    sDateTo = dTemp.AddMonths(1).AddDays(-1).ToString(format, provider);
                }

                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                temp.pricePeriod = new List<PricePeriodControl>();
                PricePeriodControl tmpPeriod = new PricePeriodControl();
                tmpPeriod.RowID = "1";
                tmpPeriod.PeriodFrom = sDateFrom;
                tmpPeriod.PeriodTo = sDateTo;
                tmpPeriod.Type = "mt";
                tmpPeriod.ULG95 = model.data_Search.sPriceDetail.USD_ULG95;
                tmpPeriod.Kero = model.data_Search.sPriceDetail.USD_KERO;
                tmpPeriod.Gasoil = model.data_Search.sPriceDetail.USD_GASOIL;
                tmpPeriod.LSFO = model.data_Search.sPriceDetail.USD_LSFO;
                tmpPeriod.HSFO = model.data_Search.sPriceDetail.USD_HSFO;
                tmpPeriod.H2 = model.data_Search.sPriceDetail.USD_H2;
                tmpPeriod.H2_Date = dateFrom.ToString("MMMyy", provider);
                tmpPeriod.LPG = model.data_Search.sPriceDetail.USD_LPG;
                tmpPeriod.ULG92 = model.data_Search.sPriceDetail.USD_ULG92;
                tmpPeriod.NAPHTA = model.data_Search.sPriceDetail.USD_NAPHTA;

                temp.pricePeriod.Add(tmpPeriod);

                tmpPeriod = new PricePeriodControl();
                tmpPeriod.RowID = "2";
                tmpPeriod.PeriodFrom = sDateFrom;
                tmpPeriod.PeriodTo = sDateTo;
                tmpPeriod.Type = "bbl/mt";
                tmpPeriod.ULG95 = model.data_Search.sPriceDetail.BBL_ULG95;
                tmpPeriod.Kero = model.data_Search.sPriceDetail.BBL_KERO;
                tmpPeriod.Gasoil = model.data_Search.sPriceDetail.BBL_GASOIL;
                tmpPeriod.LSFO = "";
                tmpPeriod.HSFO = "";
                tmpPeriod.H2 = "";
                tmpPeriod.H2_Date = "";
                tmpPeriod.LPG = "";
                tmpPeriod.ULG92 = "";
                tmpPeriod.NAPHTA = "";
                temp.pricePeriod.Add(tmpPeriod);

                temp.exchangeRate = new List<ExchangeRateControl>();
                ExchangeRateControl tmpValue = new ExchangeRateControl();
                tmpValue.RowID = "1";
                tmpValue.PeriodFrom = sDateFrom;
                tmpValue.PeriodTo = sDateTo;
                tmpValue.Buying = model.data_Search.sPriceDetail.EXC_BUYING;
                tmpValue.Selling = model.data_Search.sPriceDetail.EXC_SELLING;
                tmpValue.Average = model.data_Search.sPriceDetail.EXC_AVERAGE;
                temp.exchangeRate.Add(tmpValue);


                //Calculate Only TBL Product ดึงราคา product เป็นรายวัน
                model.data_Detail = setCalculateProduct(temp, model.data_Search);

                if (model.data_Detail.feedstockDetail.Count > 0)
                {
                    string productName = model.data_Detail.feedstockDetail[0].Product;
                    decimal unit = 0;
                    decimal Price = 0;
                    decimal vat = 0;
                    decimal total = 0;
                    decimal invoice_amount = 0;
                    int sum_count = 0;

                    if (productName.Trim().ToUpper().Contains("BLENDED KEROSENE"))//Labix ราคา product รายเดือน
                    {
                        BlendedKeroseneControl val = setValueBlendedKerosene(model.data_Search);
                        if (model.data_Detail.feedstockDetail.Count > 0)
                        {
                            if (model.data_Detail.feedstockDetail.Count > 27)
                            {
                                if (model.data_Search.vAC_1ST_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                    {
                                        string price = val.BlendedKerosene_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }

                                    }
                                }
                                if (model.data_Search.vAC_2SD_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                    {
                                        string price = val.BlendedKerosene_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                {
                                    string price = val.BlendedKerosene_Baht;

                                    if (stringToDouble(v.Volume) > 0)
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                            vFinalPirce.Price = (Math.Round(Decimal.Parse(price) *
                                                Decimal.Parse(vFinalPirce.VolumnMT), 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Vat = (Math.Round(Decimal.Parse(vFinalPirce.Price) * (decimal)0.07, 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Total = (Math.Round(
                                                Decimal.Parse(vFinalPirce.Price) + Decimal.Parse(vFinalPirce.Vat)
                                                , 2, MidpointRounding.AwayFromZero)).ToString();
                                            vFinalPirce.price_include_vat = (Decimal.Parse(vFinalPirce.Total) / Decimal.Parse(vFinalPirce.VolumnMT)).ToString();

                                            unit += Decimal.Parse(vFinalPirce.VolumnMT);
                                            Price += Decimal.Parse(vFinalPirce.Price);
                                            vat += Decimal.Parse(vFinalPirce.Vat);
                                            total += Decimal.Parse(vFinalPirce.Total);
                                            invoice_amount += Decimal.Parse(vFinalPirce.invoice_amount);
                                            sum_count++;
                                        }

                                    }

                                }
                            }

                        }
                    }
                    else if (productName.Trim().ToUpper().Contains("PEP AROMATICS AND HAB"))//Labix ราคา product รายเดือน
                    {
                        PEPAromaticsAndHABControl val = setValuePEPAromatics(temp, model);
                        if (model.data_Detail.feedstockDetail.Count > 0)
                        {
                            if (model.data_Detail.feedstockDetail.Count > 27)
                            {
                                if (model.data_Search.vAC_1ST_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                    {
                                        string price = val.Pro_PEPAromaticsHAB_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }

                                    }
                                }
                                if (model.data_Search.vAC_2SD_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                    {
                                        string price = val.Pro_PEPAromaticsHAB_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                {
                                    string price = val.Pro_PEPAromaticsHAB_Baht;

                                    if (stringToDouble(v.Volume) > 0)
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                            vFinalPirce.Price = (Math.Round(Decimal.Parse(price) *
                                                Decimal.Parse(vFinalPirce.VolumnMT), 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Vat = (Math.Round(Decimal.Parse(vFinalPirce.Price) * (decimal)0.07, 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Total = (Math.Round(
                                                Decimal.Parse(vFinalPirce.Price) + Decimal.Parse(vFinalPirce.Vat)
                                                , 2, MidpointRounding.AwayFromZero)).ToString();
                                            vFinalPirce.price_include_vat = (Double.Parse(vFinalPirce.Total) / Double.Parse(vFinalPirce.VolumnMT)).ToString();

                                            unit += Decimal.Parse(vFinalPirce.VolumnMT);
                                            Price += Decimal.Parse(vFinalPirce.Price);
                                            vat += Decimal.Parse(vFinalPirce.Vat);
                                            total += Decimal.Parse(vFinalPirce.Total);
                                            invoice_amount += Decimal.Parse(vFinalPirce.invoice_amount);
                                            sum_count++;
                                        }

                                    }
                                    //else if (model.data_Search.vInvoice == "AC")
                                    //{
                                    //    var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                    //    if (finalPirce.ToList().Count > 0)
                                    //    {
                                    //        var vFinalPirce = finalPirce.FirstOrDefault();
                                    //        vFinalPirce.UnitPriceTHBNew = "0";
                                    //        vFinalPirce.UnitPriceUSDNew = "0";
                                    //    }
                                    //}
                                }
                            }
                        }

                    }
                    else if (productName.Trim().ToUpper().Contains("STRIPPER OVHD LIQUID"))//Labix ราคา product รายเดือน
                    {

                        StripperOVHDLiquidControl val = setValueStripperOVHDLiquid(temp, model);
                        if (model.data_Detail.feedstockDetail.Count > 0)
                        {
                            if (model.data_Detail.feedstockDetail.Count > 27)
                            {
                                if (model.data_Search.vAC_1ST_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                    {
                                        string price = val.Pro_StripperOVHD_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }

                                    }
                                }
                                if (model.data_Search.vAC_2SD_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                    {
                                        string price = val.Pro_StripperOVHD_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                {
                                    string price = val.Pro_StripperOVHD_Baht;

                                    if (stringToDouble(v.Volume) > 0)
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                            vFinalPirce.Price = (Math.Round(Decimal.Parse(price) *
                                                Decimal.Parse(vFinalPirce.VolumnMT), 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Vat = (Math.Round(Decimal.Parse(vFinalPirce.Price) * (Decimal)0.07, 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Total = (Math.Round(
                                                Decimal.Parse(vFinalPirce.Price) + Decimal.Parse(vFinalPirce.Vat)
                                                , 2, MidpointRounding.AwayFromZero)).ToString();
                                            vFinalPirce.price_include_vat = (Decimal.Parse(vFinalPirce.Total) / Decimal.Parse(vFinalPirce.VolumnMT)).ToString();

                                            unit += Decimal.Parse(vFinalPirce.VolumnMT);
                                            Price += Decimal.Parse(vFinalPirce.Price);
                                            vat += Decimal.Parse(vFinalPirce.Vat);
                                            total += Decimal.Parse(vFinalPirce.Total);
                                            invoice_amount += Decimal.Parse(vFinalPirce.invoice_amount);
                                            sum_count++;
                                        }

                                    }
                                    //else if (model.data_Search.vInvoice == "AC")
                                    //{
                                    //    var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                    //    if (finalPirce.ToList().Count > 0)
                                    //    {
                                    //        var vFinalPirce = finalPirce.FirstOrDefault();
                                    //        vFinalPirce.UnitPriceTHBNew = "0";
                                    //        vFinalPirce.UnitPriceUSDNew = "0";
                                    //    }
                                    //}
                                }
                            }
                        }
                    }
                    else if (productName.ToUpper().Contains("HYDROCARBON"))//Labix ราคา product รายเดือน
                    {
                        DrainHydrocarbon val = setValueDrainHydrocarbon(temp, model);
                        if (model.data_Detail.feedstockDetail.Count > 0)
                        {
                            if (model.data_Detail.feedstockDetail.Count > 27)
                            {
                                if (model.data_Search.vAC_1ST_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                    {
                                        string price = val.DrainHCPrice_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }

                                    }
                                }
                                if (model.data_Search.vAC_2SD_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                    {
                                        string price = val.DrainHCPrice_Baht;

                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                {
                                    string price = val.DrainHCPrice_Baht;

                                    if (stringToDouble(v.Volume) > 0)
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                            vFinalPirce.Price = (Math.Round(Decimal.Parse(price) *
                                                Decimal.Parse(vFinalPirce.VolumnMT), 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Vat = (Math.Round(Decimal.Parse(vFinalPirce.Price) * (Decimal)0.07, 2)).ToString();

                                            vFinalPirce.Total = (Math.Round(
                                                Decimal.Parse(vFinalPirce.Price) + Decimal.Parse(vFinalPirce.Vat)
                                                , 2, MidpointRounding.AwayFromZero)).ToString();
                                            vFinalPirce.price_include_vat = (Double.Parse(vFinalPirce.Total) / Double.Parse(vFinalPirce.VolumnMT)).ToString();

                                            unit += Decimal.Parse(vFinalPirce.VolumnMT);
                                            Price += Decimal.Parse(vFinalPirce.Price);
                                            vat += Decimal.Parse(vFinalPirce.Vat);
                                            total += Decimal.Parse(vFinalPirce.Total);
                                            invoice_amount += Decimal.Parse(vFinalPirce.invoice_amount);
                                            sum_count++;
                                        }

                                    }
                                    //else if (model.data_Search.vInvoice == "AC")
                                    //{
                                    //    var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                    //    if (finalPirce.ToList().Count > 0)
                                    //    {
                                    //        var vFinalPirce = finalPirce.FirstOrDefault();
                                    //        vFinalPirce.UnitPriceTHBNew = "0";
                                    //        vFinalPirce.UnitPriceUSDNew = "0";
                                    //    }
                                    //}
                                }
                            }
                        }

                    }
                    //TPX  ราคา product รายเดือน
                    else if (productName.ToUpper().Contains("A9") ||
                    productName.ToUpper().Contains("C4") ||
                    productName.ToUpper().Contains("TOLUENE") ||
                    productName.ToUpper().Contains("HEAVIES") || productName.ToUpper().Contains("HEAVY") || productName.ToUpper().Contains("C10") ||
                    productName.ToUpper().Contains("RAFFINATE") || productName.ToUpper().Contains("C5"))
                    {
                        FeedStockDetailViewModel tempDate = new FeedStockDetailViewModel();
                        tempDate.Date = model.data_Detail.feedstockDetail[0].Date;
                        string price = ComparisonTPX(productName, tempDate, model.data_Search.sPriceDetail, temp.exchangeRate);
                        if (model.data_Detail.feedstockDetail.Count > 0)
                        {
                            if (model.data_Detail.feedstockDetail.Count > 27)
                            {
                                if (model.data_Search.vAC_1ST_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }

                                    }
                                }
                                if (model.data_Search.vAC_2SD_select)
                                {
                                    foreach (var v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                {

                                    if (stringToDouble(v.Volume) > 0)
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate.Where(p =>
                                        int.Parse(p.PeriodFrom) <= sDate &&
                                        int.Parse(p.PeriodTo) >= sDate);

                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceTHBNew = price;
                                            vFinalPirce.UnitPrice = price;
                                            vFinalPirce.Price = Math.Round(Math.Round(Decimal.Parse(price) *
                                                Decimal.Parse(vFinalPirce.VolumnMT), 3, MidpointRounding.AwayFromZero), 2, MidpointRounding.AwayFromZero).ToString();

                                            vFinalPirce.Vat = (Math.Round(Decimal.Parse(vFinalPirce.Price) * (Decimal)0.07, 2, MidpointRounding.AwayFromZero)).ToString();

                                            vFinalPirce.Total = (Math.Round(
                                                Decimal.Parse(vFinalPirce.Price) + Decimal.Parse(vFinalPirce.Vat)
                                                , 2, MidpointRounding.AwayFromZero)).ToString();
                                            vFinalPirce.price_include_vat = (Decimal.Parse(vFinalPirce.Total) / Decimal.Parse(vFinalPirce.VolumnMT)).ToString();

                                            unit += Decimal.Parse(vFinalPirce.VolumnMT);
                                            Price += Decimal.Parse(vFinalPirce.Price);
                                            vat += Decimal.Parse(vFinalPirce.Vat);
                                            total += Decimal.Parse(vFinalPirce.Total);
                                            invoice_amount += Decimal.Parse(vFinalPirce.invoice_amount);
                                            sum_count++;

                                        }

                                    }
                                    //else if (model.data_Search.vInvoice == "AC")
                                    //{
                                    //    var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                    //    if (finalPirce.ToList().Count > 0)
                                    //    {
                                    //        var vFinalPirce = finalPirce.FirstOrDefault();
                                    //        vFinalPirce.UnitPriceTHBNew = "0";
                                    //        vFinalPirce.UnitPriceUSDNew = "0";
                                    //    }
                                    //}
                                }
                            }

                        }
                    }
                    else //TLB ใช้ราคารายวัน
                    {
                        if (model.data_Detail.feedstockDetail.Count > 0)
                        {
                            if (model.data_Detail.feedstockDetail.Count > 27)
                            {
                                decimal bongKotPrice = 0;
                                if (productName.ToUpper().Contains("NAPHTHA"))
                                {
                                    string temp_dateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                                    string temp_dateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                                    //model.data_Search.vInvoice "OSP", "YBKC", (status == "FINAL" ? "A" : "P")

                                    #region Get bongKotPrice from Formula Bongkot
                                    string naph_status = (model.data_Search.vInvoice == "FINAL" ? "A" : "P");
                                    FormulaPricingDetail item = new FormulaPricingDetail();
                                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                                    {
                                        var query = (from v in context.FORMULA_PRICE
                                                     where v.BENCHMARK.Equals("OSP") &&
                                                     v.PRICE_STATUS.Equals(naph_status)
                                                     select new
                                                     {
                                                         MET_NUM = v.MET_NUM,
                                                         PRICE_STATUS = v.PRICE_STATUS,
                                                         DAILY_1 = v.DAILY_1,
                                                         DAILY_2 = v.DAILY_2,
                                                         DAILY_3 = v.DAILY_3,
                                                         DAILY_4 = v.DAILY_4,
                                                         DAILY_5 = v.DAILY_5,
                                                         DAILY_6 = v.DAILY_6,
                                                         DAILY_7 = v.DAILY_7,
                                                         MONTHLY_1 = v.MONTHLY_1,
                                                         MONTHLY_2 = v.MONTHLY_2,
                                                         MONTHLY_3 = v.MONTHLY_3,
                                                         MONTHLY_4 = v.MONTHLY_4,
                                                         MONTHLY_5 = v.MONTHLY_5,
                                                         MONTHLY_6 = v.MONTHLY_6,
                                                         MONTHLY_7 = v.MONTHLY_7,
                                                         BP_PRICING_PERIOD_FROM = v.BP_PRICING_PERIOD_FROM,
                                                         BP_PRICING_PERIOD_TO = v.BP_PRICING_PERIOD_TO,
                                                         OSP_PRICING_PERIOD = v.OSP_PRICING_PERIOD,

                                                         BASE_PRICE = v.BASE_PRICE,
                                                         BASE_PRICE_TEXT = v.BASE_PRICE_TEXT,
                                                         OSP_PREMIUM_DISCOUNT = v.OSP_PREMIUM_DISCOUNT,
                                                         OSP_PREMIUM_DISCOUNT_TEXT = v.OSP_PREMIUM_DISCOUNT_TEXT,
                                                         TRADING_PREMIUM_DISCOUNT = v.TRADING_PREMIUM_DISCOUNT,
                                                         TRADING_PREMIUM_DISCOUNT_TEXT = v.TRADING_PREMIUM_DISCOUNT_TEXT,
                                                         OTHER_COST = v.OTHER_COST,
                                                         OTHER_COST_TEXT = v.OTHER_COST_TEXT,
                                                         BASE_PRICE_PERIOD = v.BASE_PRICE_PERIOD,
                                                         TRADING_PRICE_PERIOD = v.TRADING_PRICE_PERIOD,
                                                         OTHER_PRICE_PERIOD = v.OTHER_PRICE_PERIOD
                                                     });

                                        var naphthaList = query.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == "YBKC").ToList();
                                        if (naphthaList.Count > 0)
                                        {
                                            item.BASE_PRICE = naphthaList[0].BASE_PRICE;
                                            item.BASE_PRICE_TEXT = naphthaList[0].BASE_PRICE_TEXT;
                                            item.BP_PRICING_PERIOD_FROM = naphthaList[0].BP_PRICING_PERIOD_FROM;
                                            item.BP_PRICING_PERIOD_TO = naphthaList[0].BP_PRICING_PERIOD_TO;
                                            item.DAILY_1 = naphthaList[0].DAILY_1;
                                            item.DAILY_2 = naphthaList[0].DAILY_2;
                                            item.DAILY_3 = naphthaList[0].DAILY_3;
                                            item.DAILY_4 = naphthaList[0].DAILY_4;
                                            item.DAILY_5 = naphthaList[0].DAILY_5;
                                            item.DAILY_6 = naphthaList[0].DAILY_6;
                                            item.DAILY_7 = naphthaList[0].DAILY_7;
                                            item.MET_NUM = naphthaList[0].MET_NUM;
                                            item.MONTHLY_1 = naphthaList[0].MONTHLY_1;
                                            item.MONTHLY_2 = naphthaList[0].MONTHLY_2;
                                            item.MONTHLY_3 = naphthaList[0].MONTHLY_3;
                                            item.MONTHLY_4 = naphthaList[0].MONTHLY_4;
                                            item.MONTHLY_5 = naphthaList[0].MONTHLY_5;
                                            item.MONTHLY_6 = naphthaList[0].MONTHLY_6;
                                            item.MONTHLY_7 = naphthaList[0].MONTHLY_7;
                                            item.OSP_PREMIUM_DISCOUNT = naphthaList[0].OSP_PREMIUM_DISCOUNT;
                                            item.OSP_PREMIUM_DISCOUNT_TEXT = naphthaList[0].OSP_PREMIUM_DISCOUNT_TEXT;
                                            item.OSP_PRICING_PERIOD = naphthaList[0].OSP_PRICING_PERIOD;
                                            item.OTHER_COST = naphthaList[0].OTHER_COST;
                                            item.OTHER_COST_TEXT = naphthaList[0].OTHER_COST_TEXT;
                                            item.PRICE_STATUS = naphthaList[0].PRICE_STATUS;
                                            item.TRADING_PREMIUM_DISCOUNT = naphthaList[0].TRADING_PREMIUM_DISCOUNT;
                                            item.TRADING_PREMIUM_DISCOUNT_TEXT = naphthaList[0].TRADING_PREMIUM_DISCOUNT_TEXT;
                                            item.BASE_PRICE_PERIOD = naphthaList[0].BASE_PRICE_PERIOD;
                                            item.TRADING_PRICE_PERIOD = naphthaList[0].TRADING_PRICE_PERIOD;
                                            item.OTHER_PRICE_PERIOD = naphthaList[0].OTHER_PRICE_PERIOD;
                                        }

                                        #region Check Rollback Date
                                        FormulaConditionItemList conItem = new FormulaConditionItemList();
                                        conItem = (FormulaConditionItemList)Newtonsoft.Json.JsonConvert.DeserializeObject(item.BASE_PRICE.ToString(), typeof(FormulaConditionItemList));
                                        conItem.result = new List<ResultTemp>();
                                        List<FormulaPricingCondition> conItemList = new List<FormulaPricingCondition>();
                                        conItemList = conItem.ConditionItem;
                                        FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                                        if (conItemList != null)
                                        {
                                            DateTime naph_dateFrom = DateTime.ParseExact(temp_dateFrom, format, provider);
                                            DateTime naph_dateTo = DateTime.ParseExact(temp_dateTo, format, provider);
                                            var IsBackWardPeriod = false;
                                            do
                                            {
                                                IsBackWardPeriod = false;
                                                DateTime dateTempStart = DateTime.ParseExact(temp_dateFrom, format, provider);
                                                if ((dateTempStart - naph_dateFrom).TotalDays > 365)
                                                {
                                                    IsBackWardPeriod = true;
                                                }
                                                foreach (FormulaPricingCondition i in conItemList)
                                                {
                                                    string date = naph_dateFrom.ToString(format, provider) + " to " + naph_dateTo.ToString(format, provider);
                                                    if (i.Variable1 == "DAILY_1" || i.Variable1 == "DAILY_2" || i.Variable1 == "DAILY_3" || i.Variable1 == "DAILY_4" || i.Variable1 == "DAILY_5" || i.Variable1 == "DAILY_6" || i.Variable1 == "DAILY_7" || i.Variable1 == "MONTHLY_1" || i.Variable1 == "MONTHLY_2" || i.Variable1 == "MONTHLY_3" || i.Variable1 == "MONTHLY_4" || i.Variable1 == "MONTHLY_5" || i.Variable1 == "MONTHLY_6" || i.Variable1 == "MONTHLY_7")
                                                    {
                                                        int rowCount = 0;
                                                        var val1 = service.getVariable(i.Variable1, item, conItem, date, context, 0, new ArrayList(), ref rowCount);
                                                        if (val1 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                                    }

                                                    if (i.Variable2 == "DAILY_1" || i.Variable2 == "DAILY_2" || i.Variable2 == "DAILY_3" || i.Variable2 == "DAILY_4" || i.Variable2 == "DAILY_5" || i.Variable2 == "DAILY_6" || i.Variable2 == "DAILY_7" || i.Variable2 == "MONTHLY_1" || i.Variable2 == "MONTHLY_2" || i.Variable2 == "MONTHLY_3" || i.Variable2 == "MONTHLY_4" || i.Variable2 == "MONTHLY_5" || i.Variable2 == "MONTHLY_6" || i.Variable2 == "MONTHLY_7")
                                                    {
                                                        int rowCount = 0;
                                                        var val2 = service.getVariable(i.Variable2, item, conItem, date, context, 0, new ArrayList(), ref rowCount);
                                                        if (val2 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                                    }
                                                }
                                                if (IsBackWardPeriod)
                                                {
                                                    DateTime sDate = DateTime.ParseExact("01" + naph_dateFrom.ToString("/MM/yyyy", provider), format, provider);
                                                    naph_dateFrom = sDate.AddMonths(-1);
                                                    naph_dateTo = naph_dateFrom.AddMonths(1).AddDays(-1);
                                                }
                                                else
                                                {
                                                    temp_dateFrom = naph_dateFrom.ToString(format, provider);
                                                    temp_dateTo = naph_dateTo.ToString(format, provider);
                                                    //date_from_to = dateFrom.ToString(format, provider) + " to " + naph_dateTo.ToString(format, provider);
                                                }
                                            } while (IsBackWardPeriod);

                                            bongKotPrice = getBongKotPrice((model.data_Search.vInvoice_select ? "FINAL" : "PRO"), temp_dateFrom, temp_dateTo);
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }

                                if (model.data_Search.vAC_1ST_select)
                                {
                                    if (productName.ToUpper().Contains("NAPHTHA"))
                                    {
                                        foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                        {
                                            if (v.Product.Trim().ToUpper().Contains("NAPHTHA"))
                                            {
                                                int count = model.data_Detail.feedstockDetail.Count;
                                                //decimal bongKotPrice = getBongKotPrice(model.data_Search.vInvoice, v.Date, temp_dateFrom, temp_dateTo);
                                                string den15 = v.Quality_Den15;//ดึงมาจากไหน
                                                string avgBongkot = Math.Round(bongKotPrice, 4, MidpointRounding.AwayFromZero).ToString(); //bongKotPrice.ToString(); //"37.7472";//ดึงมาจากไหน
                                                NaphThaProViewModel naph = new NaphThaProViewModel();
                                                naph.Den15 = den15;
                                                naph.AvgBongkot = avgBongkot;

                                                CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
                                                StripperOVHDLiquidControl i = new StripperOVHDLiquidControl();

                                                decimal h11_1 = 0m;
                                                if (dataList.NaphTha.Where(p => p.CODE == "H11_1").ToList().Count > 0)
                                                {
                                                    string val = dataList.NaphTha.Where(p => p.CODE == "H11_1").FirstOrDefault().VALUE;
                                                    decimal number;
                                                    if (decimal.TryParse(val, out number))
                                                    {
                                                        h11_1 = number;
                                                    }
                                                }
                                                decimal h11_2 = 0m;
                                                if (dataList.NaphTha.Where(p => p.CODE == "H11_2").ToList().Count > 0)
                                                {
                                                    string val = dataList.NaphTha.Where(p => p.CODE == "H11_2").FirstOrDefault().VALUE;
                                                    decimal number;
                                                    if (decimal.TryParse(val, out number))
                                                    {
                                                        h11_2 = number;
                                                    }
                                                }
                                                naph.Price = Math.Round(((Convert.ToDecimal(naph.AvgBongkot)) * (h11_1 / (Convert.ToDecimal(naph.Den15) - h11_2))), 4, MidpointRounding.AwayFromZero).ToString();
                                                v.FinalPrice = naph.Price;
                                            }
                                        }
                                    }

                                    foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day <= 15))
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        //int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate;


                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceUSDNew = v.FinalPrice;
                                            
                                            vFinalPirce.IsThaiLube = "1";
                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDNew) &&
                                               !string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDOld))
                                                vFinalPirce.AdjustPrice = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDNew) -
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDOld), 2, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.AdjustPrice = "0";

                                            if (vExchageRate.ToList().Count > 0)
                                                vFinalPirce.ExchangeRateNew = (Math.Round(Decimal.Parse(vExchageRate.FirstOrDefault().Average), 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.ExchangeRateNew = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDNew) &&
                                               !string.IsNullOrEmpty(vFinalPirce.ExchangeRateNew))
                                                vFinalPirce.UnitPriceTHBNew = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDNew) * Decimal.Parse(vFinalPirce.ExchangeRateNew)
                                                    , 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.UnitPriceTHBNew = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDOld) &&
                                               !string.IsNullOrEmpty(vFinalPirce.ExchangeRateOld))
                                                vFinalPirce.UnitPriceTHBOld = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDOld) * Decimal.Parse(vFinalPirce.ExchangeRateOld)
                                                    , 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.UnitPriceTHBOld = "0";

                                            if (model.data_Search.vInvoice_select)
                                            {
                                                vFinalPirce.UnitPrice = vFinalPirce.UnitPriceTHBNew;
                                            }
                                            else
                                            {
                                                vFinalPirce.UnitPrice = vFinalPirce.UnitPriceTHBOld;
                                            }

                                        }
                                    }

                                }
                                if (model.data_Search.vAC_2SD_select)
                                {
                                    if (productName.ToUpper().Contains("NAPHTHA"))
                                    {
                                        foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                        {
                                            if (v.Product.Trim().ToUpper().Contains("NAPHTHA"))
                                            {
                                                int count = model.data_Detail.feedstockDetail.Count;
                                                //decimal bongKotPrice = getBongKotPrice(model.data_Search.vInvoice, v.Date, temp_dateFrom, temp_dateTo);
                                                string den15 = v.Quality_Den15;//ดึงมาจากไหน
                                                string avgBongkot = Math.Round(bongKotPrice, 4, MidpointRounding.AwayFromZero).ToString(); //bongKotPrice.ToString(); //"37.7472";//ดึงมาจากไหน
                                                NaphThaProViewModel naph = new NaphThaProViewModel();
                                                naph.Den15 = den15;
                                                naph.AvgBongkot = avgBongkot;

                                                CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
                                                StripperOVHDLiquidControl i = new StripperOVHDLiquidControl();

                                                decimal h11_1 = 0m;
                                                if (dataList.NaphTha.Where(p => p.CODE == "H11_1").ToList().Count > 0)
                                                {
                                                    string val = dataList.NaphTha.Where(p => p.CODE == "H11_1").FirstOrDefault().VALUE;
                                                    decimal number;
                                                    if (decimal.TryParse(val, out number))
                                                    {
                                                        h11_1 = number;
                                                    }
                                                }
                                                decimal h11_2 = 0m;
                                                if (dataList.NaphTha.Where(p => p.CODE == "H11_2").ToList().Count > 0)
                                                {
                                                    string val = dataList.NaphTha.Where(p => p.CODE == "H11_2").FirstOrDefault().VALUE;
                                                    decimal number;
                                                    if (decimal.TryParse(val, out number))
                                                    {
                                                        h11_2 = number;
                                                    }
                                                }
                                                naph.Price = Math.Round(((Convert.ToDecimal(naph.AvgBongkot)) * (h11_1 / (Convert.ToDecimal(naph.Den15) - h11_2))), 4, MidpointRounding.AwayFromZero).ToString();
                                                v.FinalPrice = naph.Price;
                                            }
                                        }
                                    }

                                    foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail.Where(p => DateTime.ParseExact(p.Date, format, provider).Day >= 16 && DateTime.ParseExact(p.Date, format, provider).Day <= 31))
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        //int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate;


                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceUSDNew = v.FinalPrice;
                                           
                                            vFinalPirce.IsThaiLube = "1";
                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDNew) &&
                                               !string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDOld))
                                                vFinalPirce.AdjustPrice = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDNew) -
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDOld), 2, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.AdjustPrice = "0";

                                            if (vExchageRate.ToList().Count > 0)
                                                vFinalPirce.ExchangeRateNew = (Math.Round(Decimal.Parse(vExchageRate.FirstOrDefault().Average), 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.ExchangeRateNew = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDNew) &&
                                               !string.IsNullOrEmpty(vFinalPirce.ExchangeRateNew))
                                                vFinalPirce.UnitPriceTHBNew = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDNew) * Decimal.Parse(vFinalPirce.ExchangeRateNew)
                                                    , 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.UnitPriceTHBNew = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDOld) &&
                                               !string.IsNullOrEmpty(vFinalPirce.ExchangeRateOld))
                                                vFinalPirce.UnitPriceTHBOld = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDOld) * Decimal.Parse(vFinalPirce.ExchangeRateOld)
                                                    , 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.UnitPriceTHBOld = "0";

                                            if (model.data_Search.vInvoice_select)
                                            {
                                                vFinalPirce.UnitPrice = vFinalPirce.UnitPriceTHBNew;
                                            }
                                            else
                                            {
                                                vFinalPirce.UnitPrice = vFinalPirce.UnitPriceTHBOld;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (productName.ToUpper().Contains("NAPHTHA"))
                                {
                                    string temp_dateFrom = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(0, 10);
                                    string temp_dateTo = String.IsNullOrEmpty(model.data_Search.vSearchPeriod) ? "" : model.data_Search.vSearchPeriod.ToUpper().Substring(14, 10);
                                    //model.data_Search.vInvoice "OSP", "YBKC", (status == "FINAL" ? "A" : "P")
                                    decimal bongKotPrice = 0;
                                    #region Get bongKotPrice from Formula Bongkot
                                    string naph_status = (model.data_Search.vInvoice == "FINAL" ? "A" : "P");
                                    FormulaPricingDetail item = new FormulaPricingDetail();
                                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                                    {
                                        var query = (from v in context.FORMULA_PRICE
                                                     where v.BENCHMARK.Equals("OSP") &&
                                                     v.PRICE_STATUS.Equals(naph_status)
                                                     select new
                                                     {
                                                         MET_NUM = v.MET_NUM,
                                                         PRICE_STATUS = v.PRICE_STATUS,
                                                         DAILY_1 = v.DAILY_1,
                                                         DAILY_2 = v.DAILY_2,
                                                         DAILY_3 = v.DAILY_3,
                                                         DAILY_4 = v.DAILY_4,
                                                         DAILY_5 = v.DAILY_5,
                                                         DAILY_6 = v.DAILY_6,
                                                         DAILY_7 = v.DAILY_7,
                                                         MONTHLY_1 = v.MONTHLY_1,
                                                         MONTHLY_2 = v.MONTHLY_2,
                                                         MONTHLY_3 = v.MONTHLY_3,
                                                         MONTHLY_4 = v.MONTHLY_4,
                                                         MONTHLY_5 = v.MONTHLY_5,
                                                         MONTHLY_6 = v.MONTHLY_6,
                                                         MONTHLY_7 = v.MONTHLY_7,
                                                         BP_PRICING_PERIOD_FROM = v.BP_PRICING_PERIOD_FROM,
                                                         BP_PRICING_PERIOD_TO = v.BP_PRICING_PERIOD_TO,
                                                         OSP_PRICING_PERIOD = v.OSP_PRICING_PERIOD,

                                                         BASE_PRICE = v.BASE_PRICE,
                                                         BASE_PRICE_TEXT = v.BASE_PRICE_TEXT,
                                                         OSP_PREMIUM_DISCOUNT = v.OSP_PREMIUM_DISCOUNT,
                                                         OSP_PREMIUM_DISCOUNT_TEXT = v.OSP_PREMIUM_DISCOUNT_TEXT,
                                                         TRADING_PREMIUM_DISCOUNT = v.TRADING_PREMIUM_DISCOUNT,
                                                         TRADING_PREMIUM_DISCOUNT_TEXT = v.TRADING_PREMIUM_DISCOUNT_TEXT,
                                                         OTHER_COST = v.OTHER_COST,
                                                         OTHER_COST_TEXT = v.OTHER_COST_TEXT,
                                                         BASE_PRICE_PERIOD = v.BASE_PRICE_PERIOD,
                                                         TRADING_PRICE_PERIOD = v.TRADING_PRICE_PERIOD,
                                                         OTHER_PRICE_PERIOD = v.OTHER_PRICE_PERIOD
                                                     });

                                        var naphthaList = query.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == "YBKC").ToList();
                                        if (naphthaList.Count > 0)
                                        {
                                            item.BASE_PRICE = naphthaList[0].BASE_PRICE;
                                            item.BASE_PRICE_TEXT = naphthaList[0].BASE_PRICE_TEXT;
                                            item.BP_PRICING_PERIOD_FROM = naphthaList[0].BP_PRICING_PERIOD_FROM;
                                            item.BP_PRICING_PERIOD_TO = naphthaList[0].BP_PRICING_PERIOD_TO;
                                            item.DAILY_1 = naphthaList[0].DAILY_1;
                                            item.DAILY_2 = naphthaList[0].DAILY_2;
                                            item.DAILY_3 = naphthaList[0].DAILY_3;
                                            item.DAILY_4 = naphthaList[0].DAILY_4;
                                            item.DAILY_5 = naphthaList[0].DAILY_5;
                                            item.DAILY_6 = naphthaList[0].DAILY_6;
                                            item.DAILY_7 = naphthaList[0].DAILY_7;
                                            item.MET_NUM = naphthaList[0].MET_NUM;
                                            item.MONTHLY_1 = naphthaList[0].MONTHLY_1;
                                            item.MONTHLY_2 = naphthaList[0].MONTHLY_2;
                                            item.MONTHLY_3 = naphthaList[0].MONTHLY_3;
                                            item.MONTHLY_4 = naphthaList[0].MONTHLY_4;
                                            item.MONTHLY_5 = naphthaList[0].MONTHLY_5;
                                            item.MONTHLY_6 = naphthaList[0].MONTHLY_6;
                                            item.MONTHLY_7 = naphthaList[0].MONTHLY_7;
                                            item.OSP_PREMIUM_DISCOUNT = naphthaList[0].OSP_PREMIUM_DISCOUNT;
                                            item.OSP_PREMIUM_DISCOUNT_TEXT = naphthaList[0].OSP_PREMIUM_DISCOUNT_TEXT;
                                            item.OSP_PRICING_PERIOD = naphthaList[0].OSP_PRICING_PERIOD;
                                            item.OTHER_COST = naphthaList[0].OTHER_COST;
                                            item.OTHER_COST_TEXT = naphthaList[0].OTHER_COST_TEXT;
                                            item.PRICE_STATUS = naphthaList[0].PRICE_STATUS;
                                            item.TRADING_PREMIUM_DISCOUNT = naphthaList[0].TRADING_PREMIUM_DISCOUNT;
                                            item.TRADING_PREMIUM_DISCOUNT_TEXT = naphthaList[0].TRADING_PREMIUM_DISCOUNT_TEXT;
                                            item.BASE_PRICE_PERIOD = naphthaList[0].BASE_PRICE_PERIOD;
                                            item.TRADING_PRICE_PERIOD = naphthaList[0].TRADING_PRICE_PERIOD;
                                            item.OTHER_PRICE_PERIOD = naphthaList[0].OTHER_PRICE_PERIOD;
                                        }

                                        #region Check Rollback Date
                                        FormulaConditionItemList conItem = new FormulaConditionItemList();
                                        conItem = (FormulaConditionItemList)Newtonsoft.Json.JsonConvert.DeserializeObject(item.BASE_PRICE.ToString(), typeof(FormulaConditionItemList));
                                        conItem.result = new List<ResultTemp>();
                                        List<FormulaPricingCondition> conItemList = new List<FormulaPricingCondition>();
                                        conItemList = conItem.ConditionItem;
                                        FormulaPricingServiceModel service = new FormulaPricingServiceModel();
                                        if (conItemList != null)
                                        {
                                            DateTime naph_dateFrom = DateTime.ParseExact(temp_dateFrom, format, provider);
                                            DateTime naph_dateTo = DateTime.ParseExact(temp_dateTo, format, provider);
                                            var IsBackWardPeriod = false;
                                            do
                                            {
                                                IsBackWardPeriod = false;
                                                DateTime dateTempStart = DateTime.ParseExact(temp_dateFrom, format, provider);
                                                if ((dateTempStart - naph_dateFrom).TotalDays > 365)
                                                {
                                                    IsBackWardPeriod = true;
                                                }
                                                foreach (FormulaPricingCondition i in conItemList)
                                                {
                                                    string date = naph_dateFrom.ToString(format, provider) + " to " + naph_dateTo.ToString(format, provider);
                                                    if (i.Variable1 == "DAILY_1" || i.Variable1 == "DAILY_2" || i.Variable1 == "DAILY_3" || i.Variable1 == "DAILY_4" || i.Variable1 == "DAILY_5" || i.Variable1 == "DAILY_6" || i.Variable1 == "DAILY_7" || i.Variable1 == "MONTHLY_1" || i.Variable1 == "MONTHLY_2" || i.Variable1 == "MONTHLY_3" || i.Variable1 == "MONTHLY_4" || i.Variable1 == "MONTHLY_5" || i.Variable1 == "MONTHLY_6" || i.Variable1 == "MONTHLY_7")
                                                    {
                                                        int rowCount = 0;
                                                        var val1 = service.getVariable(i.Variable1, item, conItem, date, context, 0, new ArrayList(), ref rowCount);
                                                        if (val1 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                                    }

                                                    if (i.Variable2 == "DAILY_1" || i.Variable2 == "DAILY_2" || i.Variable2 == "DAILY_3" || i.Variable2 == "DAILY_4" || i.Variable2 == "DAILY_5" || i.Variable2 == "DAILY_6" || i.Variable2 == "DAILY_7" || i.Variable2 == "MONTHLY_1" || i.Variable2 == "MONTHLY_2" || i.Variable2 == "MONTHLY_3" || i.Variable2 == "MONTHLY_4" || i.Variable2 == "MONTHLY_5" || i.Variable2 == "MONTHLY_6" || i.Variable2 == "MONTHLY_7")
                                                    {
                                                        int rowCount = 0;
                                                        var val2 = service.getVariable(i.Variable2, item, conItem, date, context, 0, new ArrayList(), ref rowCount);
                                                        if (val2 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                                    }
                                                }
                                                if (IsBackWardPeriod)
                                                {
                                                    DateTime sDate = DateTime.ParseExact("01" + naph_dateFrom.ToString("/MM/yyyy", provider), format, provider);
                                                    naph_dateFrom = sDate.AddMonths(-1);
                                                    naph_dateTo = naph_dateFrom.AddMonths(1).AddDays(-1);
                                                }
                                                else
                                                {
                                                    temp_dateFrom = naph_dateFrom.ToString(format, provider);
                                                    temp_dateTo = naph_dateTo.ToString(format, provider);
                                                    //date_from_to = dateFrom.ToString(format, provider) + " to " + naph_dateTo.ToString(format, provider);
                                                }
                                            } while (IsBackWardPeriod);

                                            bongKotPrice = getBongKotPrice((model.data_Search.vInvoice_select ? "FINAL" : "PRO"), temp_dateFrom, temp_dateTo);
                                        }
                                        #endregion
                                    }
                                    #endregion

                                    foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                    {
                                        if (v.Product.Trim().ToUpper().Contains("NAPHTHA"))
                                        {
                                            int count = model.data_Detail.feedstockDetail.Count;
                                            //decimal bongKotPrice = getBongKotPrice(model.data_Search.vInvoice, v.Date, temp_dateFrom, temp_dateTo);
                                            string den15 = v.Quality_Den15;//ดึงมาจากไหน
                                            string avgBongkot = Math.Round(bongKotPrice, 4, MidpointRounding.AwayFromZero).ToString(); //bongKotPrice.ToString(); //"37.7472";//ดึงมาจากไหน
                                            NaphThaProViewModel naph = new NaphThaProViewModel();
                                            naph.Den15 = den15;
                                            naph.AvgBongkot = avgBongkot;

                                            CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
                                            StripperOVHDLiquidControl i = new StripperOVHDLiquidControl();

                                            decimal h11_1 = 0m;
                                            if (dataList.NaphTha.Where(p => p.CODE == "H11_1").ToList().Count > 0)
                                            {
                                                string val = dataList.NaphTha.Where(p => p.CODE == "H11_1").FirstOrDefault().VALUE;
                                                decimal number;
                                                if (decimal.TryParse(val, out number))
                                                {
                                                    h11_1 = number;
                                                }
                                            }
                                            decimal h11_2 = 0m;
                                            if (dataList.NaphTha.Where(p => p.CODE == "H11_2").ToList().Count > 0)
                                            {
                                                string val = dataList.NaphTha.Where(p => p.CODE == "H11_2").FirstOrDefault().VALUE;
                                                decimal number;
                                                if (decimal.TryParse(val, out number))
                                                {
                                                    h11_2 = number;
                                                }
                                            }
                                            naph.Price = Math.Round(((Convert.ToDecimal(naph.AvgBongkot)) * (h11_1 / (Convert.ToDecimal(naph.Den15) - h11_2))), 4, MidpointRounding.AwayFromZero).ToString();
                                            v.FinalPrice = naph.Price;

                                        }
                                    }
                                }

                                foreach (FeedStockDetailViewModel v in model.data_Detail.feedstockDetail)
                                {
                                    if (stringToDouble(v.Volume) > 0)
                                    {
                                        string str_date = v.Date.Substring(6, 4) + v.Date.Substring(3, 2) + v.Date.Substring(0, 2);
                                        //int sDate = stringToInt(str_date);
                                        var vExchageRate = model.data_Detail.exchangeRate;


                                        var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                        if (finalPirce.ToList().Count > 0)
                                        {
                                            var vFinalPirce = finalPirce.FirstOrDefault();
                                            vFinalPirce.UnitPriceUSDNew = v.FinalPrice;

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDNew) &&
                                               !string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDOld))
                                                vFinalPirce.AdjustPrice = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDNew) -
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDOld), 2, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.AdjustPrice = "0";

                                            if (vExchageRate.ToList().Count > 0)
                                                vFinalPirce.ExchangeRateNew = (Math.Round(Decimal.Parse(vExchageRate.FirstOrDefault().Average), 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.ExchangeRateNew = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDNew) &&
                                               !string.IsNullOrEmpty(vFinalPirce.ExchangeRateNew))
                                                vFinalPirce.UnitPriceTHBNew = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDNew) * Decimal.Parse(vFinalPirce.ExchangeRateNew)
                                                    , 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.UnitPriceTHBNew = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.UnitPriceUSDOld) &&
                                               !string.IsNullOrEmpty(vFinalPirce.ExchangeRateOld))
                                                vFinalPirce.UnitPriceTHBOld = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.UnitPriceUSDOld) * Decimal.Parse(vFinalPirce.ExchangeRateOld)
                                                    , 4, MidpointRounding.AwayFromZero)).ToString();
                                            else
                                                vFinalPirce.UnitPriceTHBOld = "0";

                                            if (!string.IsNullOrEmpty(vFinalPirce.VolumnMT))
                                            {
                                                string unitPriceTh = "0";
                                                if (model.data_Search != null)
                                                {
                                                    if (!string.IsNullOrEmpty(model.data_Search.vInvoice))
                                                    {
                                                        if (model.data_Search.vInvoice == "FINAL")
                                                        {
                                                            unitPriceTh = vFinalPirce.UnitPriceTHBNew;
                                                        }
                                                        else
                                                        {
                                                            unitPriceTh = vFinalPirce.UnitPriceTHBOld;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        unitPriceTh = vFinalPirce.UnitPriceTHBOld;
                                                    }
                                                }
                                                else
                                                {
                                                    unitPriceTh = vFinalPirce.UnitPriceTHBOld;
                                                }
                                                vFinalPirce.UnitPrice = unitPriceTh;

                                                vFinalPirce.Price = (Math.Round(Decimal.Parse(unitPriceTh) *
                                                    Decimal.Parse(vFinalPirce.VolumnMT), 2, MidpointRounding.AwayFromZero)).ToString();

                                                vFinalPirce.Vat = (Math.Round(Decimal.Parse(vFinalPirce.Price) * (Decimal)0.07, 2, MidpointRounding.AwayFromZero)).ToString();

                                                vFinalPirce.Total = (Math.Round(
                                                    Decimal.Parse(vFinalPirce.Price) + Decimal.Parse(vFinalPirce.Vat)
                                                    , 2, MidpointRounding.AwayFromZero)).ToString();
                                                vFinalPirce.price_include_vat = (Double.Parse(vFinalPirce.Total) / Double.Parse(vFinalPirce.VolumnMT)).ToString();
                                                vFinalPirce.final_minus_pro = (Double.Parse(vFinalPirce.UnitPriceTHBNew) - Double.Parse(vFinalPirce.UnitPriceTHBOld)).ToString();
                                                vFinalPirce.total_exc_vat = (Math.Round(Decimal.Parse(vFinalPirce.final_minus_pro) * Decimal.Parse(vFinalPirce.VolumnMT), 2, MidpointRounding.AwayFromZero)).ToString();
                                                vFinalPirce.total_vat = (Math.Round(Decimal.Parse(vFinalPirce.total_exc_vat) * (Decimal)0.07, 2, MidpointRounding.AwayFromZero)).ToString();
                                                vFinalPirce.total_inc_vat = (Double.Parse(vFinalPirce.total_exc_vat) + Double.Parse(vFinalPirce.total_vat)).ToString();

                                                unit += Decimal.Parse(vFinalPirce.VolumnMT);
                                                Price += Decimal.Parse(vFinalPirce.Price);
                                                vat += Decimal.Parse(vFinalPirce.Vat);
                                                total += Decimal.Parse(vFinalPirce.Total);
                                                invoice_amount += Decimal.Parse(vFinalPirce.invoice_amount);
                                                sum_count++;

                                                //if (model.data_Search.vInvoice_select)
                                                //{
                                                //    vFinalPirce.UnitPrice = vFinalPirce.UnitPriceTHBNew;
                                                //}
                                                //else
                                                //{
                                                //    vFinalPirce.UnitPrice = vFinalPirce.UnitPriceTHBOld;
                                                //}
                                            }

                                        }
                                    }
                                    //else if (model.data_Search.vInvoice == "AC")
                                    //{
                                    //    var finalPirce = model.data_Search.sPriceDetail.dQualityList.Where(p => p.PODate == v.Date);
                                    //    if (finalPirce.ToList().Count > 0)
                                    //    {
                                    //        var vFinalPirce = finalPirce.FirstOrDefault();
                                    //        vFinalPirce.UnitPriceTHBNew = "0";
                                    //        vFinalPirce.UnitPriceUSDNew = "0";
                                    //    }
                                    //}
                                }
                            }

                        }

                    }
                    if (sum_count > 0)
                    {
                        //Price = 0;
                        //unit = 0;
                        //vat = 0;
                        //total = 0;
                        //invoice_amount = 0;
                        //sum_count = 0;

                        QualityProductDetail tempQuantity = new QualityProductDetail();
                        foreach (var z in model.data_Search.sPriceDetail.dQualityList)
                        {
                            if (z.NO != "99")
                            {
                                //Price += Convert.ToDecimal(z.Price);
                                //unit += Convert.ToDecimal(z.VolumnMT);
                                //vat += Convert.ToDecimal(z.Vat);
                                //total += Convert.ToDecimal(z.Total);
                                //invoice_amount += Convert.ToDecimal(z.invoice_amount);
                                //sum_count++;
                            }
                            else
                            {
                                tempQuantity = z;
                            }
                        }
                        if (!string.IsNullOrEmpty(tempQuantity.NO))
                        {
                            model.data_Search.sPriceDetail.dQualityList.Remove(tempQuantity);
                        }


                        QualityProductDetail item = new QualityProductDetail();
                        item.NO = "99";
                        item.Product = productName;
                        item.Price = Math.Round((Price), 2, MidpointRounding.AwayFromZero).ToString();
                        item.VolumnMT = Math.Round((unit), 4, MidpointRounding.AwayFromZero).ToString();
                        item.Vat = Math.Round((vat), 2, MidpointRounding.AwayFromZero).ToString();
                        item.Total = Math.Round((total), 2, MidpointRounding.AwayFromZero).ToString();
                        item.invoice_amount = Math.Round((invoice_amount), 2, MidpointRounding.AwayFromZero).ToString();

                        //item.Price = Math.Round((Price / sum_count), 2, MidpointRounding.AwayFromZero).ToString();
                        //item.VolumnMT = Math.Round((unit / sum_count), 4, MidpointRounding.AwayFromZero).ToString();
                        //item.Vat = Math.Round((vat / sum_count), 2, MidpointRounding.AwayFromZero).ToString();
                        //item.Total = Math.Round((total / sum_count), 2, MidpointRounding.AwayFromZero).ToString();
                        //item.invoice_amount = Math.Round((invoice_amount / sum_count), 2, MidpointRounding.AwayFromZero).ToString();
                        model.data_Search.sPriceDetail.dQualityList.Add(item);
                    }
                    //getProductList(ref model);
                }
                //data_Search.vUpdatePeriod Model.data_Search.sPriceDetail.dProductList
                getProductList(ref model);
            }
            catch (Exception ex)
            {

            }

        }

        public FeedStockBookingPriceDetail setCalculateProduct(FeedStockBookingPriceDetail temp, FeedStockBookingPriceSearch searchData)
        {
            FeedStockDetailViewModel tempRemove = new FeedStockDetailViewModel();
            string status = searchData.vInvoice;

            FeedStockDetailViewModel tempQuantity = new FeedStockDetailViewModel();
            foreach (var z in temp.feedstockDetail)
            {
                if (string.IsNullOrEmpty(z.Date))
                {
                    tempQuantity = z;
                }

            }
            temp.feedstockDetail.Remove(tempQuantity);

            foreach (FeedStockDetailViewModel valList in temp.feedstockDetail)
            {
                FeedStockDetailViewModel row = setConstantValue(valList);
                string code = "";
                if (!string.IsNullOrEmpty(row.Product))
                {
                    if (row.Product.Trim().ToUpper().Contains("EXTRACT") ||
                    row.Product.Trim().ToUpper().Contains("VR") ||
                    row.Product.Trim().ToUpper().Contains("RSO"))
                    {
                        code = "1";
                        row = calValuePayment(row, code, temp.pricePeriod, temp.exchangeRate);
                        row = setCompairTable(row);

                        row = PhysicalCalculate(row);
                        InverseData(row);
                        ComparisonTable(row, temp.pricePeriod, code);
                    }
                    else if (row.Product.Trim().ToUpper().Contains("LVGO"))
                    {
                        code = "2";
                        row = setCompairTable(row);
                        ComparisonTable(row, temp.pricePeriod, code);
                    }
                    else if (row.Product.Trim().ToUpper().Contains("VGO"))
                    {
                        code = "3";
                        row = calValuePayment(row, code, temp.pricePeriod, temp.exchangeRate);
                        row = setCompairTable(row);

                        row = PhysicalCalculate(row);//Inverse ก้อนบน
                        InverseData(row);
                        ComparisonTable(row, temp.pricePeriod, code);
                    }
                    else if (row.Product.Trim().ToUpper().Contains("SW") || row.Product.Trim().ToUpper().Contains("DAO"))
                    {
                        code = "4";
                        row = calValuePayment(row, code, temp.pricePeriod, temp.exchangeRate);
                        row = setCompairTable(row);

                        row = PhysicalCalculate(row);
                        InverseData(row);
                        ComparisonTable(row, temp.pricePeriod, code);

                    }
                }
                else
                {
                    tempRemove = valList;
                }

                ////TPX ใช้ราคาเฉลี่ยทั้งเดือน และอัตราแลกเปลี่ยนทั้งเดือน
                //else if (row.Product.Trim().ToUpper().Contains("A9") ||
                //    row.Product.Trim().ToUpper().Contains("C4") ||
                //    row.Product.Trim().ToUpper().Contains("TOLUENE") ||
                //    row.Product.Trim().ToUpper().Contains("HEAVIES") || row.Product.Trim().ToUpper().Contains("HEAVY") || row.Product.Trim().ToUpper().Contains("C10") ||
                //    row.Product.Trim().ToUpper().Contains("RAFFINATE") || row.Product.Trim().ToUpper().Contains("C5"))
                //{
                //    string product = row.Product.Trim().ToUpper();
                //    ComparisonTPX(product, row, searchData.sPriceDetail, temp.exchangeRate);
                //}

            }
            temp.feedstockDetail.Remove(tempRemove);
            return temp;
        }

        public string ComparisonTPX(string product, FeedStockDetailViewModel item, UpdatePriceDetail pricePeriod, List<ExchangeRateControl> exchangeRate)
        {
            string dateTimeString = Regex.Replace(item.Date, @"[^\u0000-\u007F]", string.Empty);
            DateTime itemDate = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            //PricePeriodControl pricePeriod = pricePeriodList.Where(p => itemDate >= DateTime.ParseExact(p.PeriodFrom, "yyyyMMdd", CultureInfo.CurrentCulture)
            //            && itemDate <= DateTime.ParseExact(p.PeriodTo, "yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"))).FirstOrDefault();

            ExchangeRateControl exchangeVal = exchangeRate.ToList().FirstOrDefault();

            string strDateFrom = "01" + item.Date.Substring(2, 8);
            DateTime from = DateTime.Now;
            from = DateTime.ParseExact(strDateFrom, "dd/MM/yyyy", new CultureInfo("en-US"));
            DateTime to = new DateTime();
            to = DateTime.ParseExact(strDateFrom, format, provider).AddMonths(1).AddDays(-1);

            var FromAllMonth = 0;
            FromAllMonth = Convert.ToInt32(
                        from.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(6, 4) +
                        from.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(3, 2) +
                        from.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(0, 2)
                                );
            int ToAllMonth = 0;
            ToAllMonth = Convert.ToInt32(
                        to.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(6, 4) +
                        to.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(3, 2) +
                        to.ToString("dd/MM/yyyy", new CultureInfo("en-US")).Substring(0, 2)
                                );

            CONFIG_INTERCOMPANY dataList = getCIPConfig("INTER_PRICE_UPDATE");
            if (product.Contains("A9") || product.Contains("MX")
                || product.Contains("C9"))
            {
                decimal ulg95 = Convert.ToDecimal(pricePeriod.USD_TPX_ULG95 ?? "0");
                decimal formulaNum = 0m;
                if (dataList.A9_MX_C9.ToList().Count > 0)
                {
                    decimal number;
                    if (decimal.TryParse(dataList.A9_MX_C9[0].VALUE, out number))
                    {
                        formulaNum = number;
                    }

                }
                return string.Format("{0:0.###########}", Math.Round(((Decimal)(ulg95 + formulaNum) * Decimal.Parse(exchangeVal.Selling)), 3, MidpointRounding.AwayFromZero));
            }//201.53 ค่าจริง 201.56 ค่าที่ได้
            else if (product.Contains("C4") || product.Contains("C7"))
            {
                decimal ulg95 = Convert.ToDecimal(pricePeriod.USD_TPX_ULG95 ?? "0");
                decimal formulaNum = 0m;
                if (dataList.C4_C7.ToList().Count > 0)
                {
                    decimal number;
                    if (decimal.TryParse(dataList.C4_C7[0].VALUE, out number))
                    {
                        formulaNum = number;
                    }

                }
                return string.Format("{0:0.###########}", Math.Round(((Decimal)(ulg95 / formulaNum) * Decimal.Parse(exchangeVal.Selling) * (Decimal)0.9818), 3, MidpointRounding.AwayFromZero));
            }
            else if (product.Contains("TOLUENE"))
            {
                decimal ulg95 = Convert.ToDecimal(pricePeriod.USD_TPX_ULG95 ?? "0");
                decimal formulaNum = 0m;
                if (dataList.TOLUENE.ToList().Count > 0)
                {
                    decimal number;
                    if (decimal.TryParse(dataList.TOLUENE[0].VALUE, out number))
                    {
                        formulaNum = number;
                    }

                }
                return string.Format("{0:0.###########}", Math.Round(((Decimal)(ulg95 + formulaNum) * Decimal.Parse(exchangeVal.Selling)), 3, MidpointRounding.AwayFromZero));
            }
            else if (product.Contains("C10") || product.Contains("HEAVIES") || product.Contains("HEAVY"))
            {
                decimal kero = Convert.ToDecimal(pricePeriod.USD_TPX_KERO ?? "0");//Math.Round(Convert.ToDecimal(pricePeriod.USD_TPX_KERO ?? "0"), 3, MidpointRounding.AwayFromZero);
                decimal gasoil = Convert.ToDecimal(pricePeriod.USD_TPX_GASOIL ?? "0");//Math.Round(Convert.ToDecimal(pricePeriod.USD_TPX_GASOIL ?? "0"), 3, MidpointRounding.AwayFromZero);
                decimal avgVal = (Decimal)((kero + gasoil) / 2);// Math.Round((Decimal)((kero + gasoil) / 2),3, MidpointRounding.AwayFromZero);
                decimal formulaNum = 0m;
                if (dataList.C10_HEAVIES.ToList().Count > 0)
                {
                    decimal number;
                    if (decimal.TryParse(dataList.C10_HEAVIES[0].VALUE, out number))
                    {
                        formulaNum = number;
                    }

                }
                return string.Format("{0:0.###########}", Math.Round(((Decimal)(avgVal + formulaNum) * Decimal.Parse(exchangeVal.Selling)), 3, MidpointRounding.AwayFromZero));
            }
            else if (product.Contains("RAFFINATE") || product.Contains("C5"))
            {
                decimal formulaNum = 0m;
                if (dataList.C5_RAFFINATE.ToList().Count > 0)
                {
                    decimal number;
                    if (decimal.TryParse(dataList.C5_RAFFINATE[0].VALUE, out number))
                    {
                        formulaNum = number;
                    }

                }
                decimal jNaphtha = Convert.ToDecimal(pricePeriod.USD_TPX_NAPHTA ?? "0") + formulaNum;//formulaNum = -24
                //Math.Round(Convert.ToDecimal(pricePeriod.USD_TPX_NAPHTA ?? "0")-24, 3, MidpointRounding.AwayFromZero);
                return string.Format("{0:0.###########}", Math.Round(((decimal)jNaphtha * Decimal.Parse(exchangeVal.Selling)), 3, MidpointRounding.AwayFromZero));
            }

            return "0";

        }

        public ReturnValue Save(FeedstockBookingPriceViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            int t_count = 0;
            string p_no = "";
            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.data_Search.sPriceDetail.dQualityList.Count == 0)
                {
                    rtn.Message = "have no data to save.";
                    rtn.Status = false;
                    return rtn;
                }

                UPDATE_PRICE_DAL dal = new UPDATE_PRICE_DAL();
                PIT_PO_PRICE ent = new PIT_PO_PRICE();

                DateTime now = DateTime.Now;

                QualityProductDetail tempQuantity = new QualityProductDetail();
                foreach (var z in pModel.data_Search.sPriceDetail.dQualityList)
                {
                    if (z.NO == "99")
                    {
                        tempQuantity = z;
                    }

                }
                pModel.data_Search.sPriceDetail.dQualityList.Remove(tempQuantity);

                using (var context = new EntityCPAIEngine())
                {
                    string invoiceStatus = "PRO";
                    if (pModel.data_Search.sPriceDetail.dQualityList.ToList().Count > 25)//กรณีเป็น AC จะมีครบเดือน
                    {
                        invoiceStatus = "AC";
                    }
                    else
                    {
                        if (pModel.data_Search.vInvoice_select)
                        {
                            invoiceStatus = "FINAL";
                        }
                    }


                    if (pModel.data_Search.sPriceDetail.dQualityList[0].PO_NO != null)
                    {
                        //Clear old data
                        int num = pModel.data_Search.sPriceDetail.dQualityList.ToList().Count;
                        string startDate = pModel.data_Search.sPriceDetail.dQualityList[0].PODate;
                        string endDate = pModel.data_Search.sPriceDetail.dQualityList[num - 1].PODate;
                        dal.Delete(pModel.data_Search.sPriceDetail.dQualityList[0].PO_NO, invoiceStatus, startDate, endDate, context);

                        var tmp_product = pModel.data_Search.sPriceDetail.dProductList.Where(p => p.PO_NO == pModel.data_Search.sPriceDetail.dQualityList[0].PO_NO).ToList();
                        if (tmp_product != null)
                        {
                            if (tmp_product.Count > 0)
                            {
                                if (invoiceStatus == "FINAL" && startDate.Substring(0, 2) == "01")
                                {
                                    tmp_product[0].IsFinal_1ST_Half = 15;
                                }
                                else if (invoiceStatus == "FINAL" && startDate.Substring(0, 2) == "16")
                                {
                                    tmp_product[0].IsFinal_2ND_Half = 15;
                                }
                            }
                        }

                    }

                    //Get Last UnitPrice
                    Decimal unitPriect = 0;
                    //if (invoiceStatus == "AC")
                    //{
                    //    if (pModel.data_Search.vAC_1ST_select)
                    //    {
                    //        unitPriect = 0;
                    //        //get value from max Date 
                    //        foreach (var item in pModel.data_Search.sPriceDetail.dQualityList.Where(p => int.Parse(p.NO) <= 15))
                    //        {
                    //            if (Decimal.Parse(item.UnitPriceTHBNew) > 0)
                    //            {
                    //                unitPriect = Decimal.Parse(item.UnitPriceTHBNew);
                    //            }
                    //        }
                    //        foreach (var item in pModel.data_Search.sPriceDetail.dQualityList.Where(p => int.Parse(p.NO) <= 15))
                    //        {
                    //            p_no = item.NO;
                    //            if (item.PO_NO != null)
                    //            {
                    //                ent = new PIT_PO_PRICE();
                    //                //ConstantPrm.GetDynamicCtrlID();
                    //                decimal poItem = 10;
                    //                if (item.PO_ITEM != null)
                    //                {
                    //                    if (item.PO_ITEM.Length < 10)
                    //                    {
                    //                        poItem = Decimal.Parse(item.PO_ITEM ?? "10");
                    //                    }
                    //                }
                    //                ent.PO_ITEM = poItem;
                    //                ent.PO_NO = item.PO_NO;
                    //                ent.PO_DATE = DateTime.ParseExact(item.PODate, format, provider).Date;
                    //                ent.INVOICE_NO = item.InvoiceNo ?? "";
                    //                ent.INVOICE_STATUS = invoiceStatus;
                    //                ent.CUST_PO_NO = item.TripNo;
                    //                ent.MET_MAT = item.Product_Code;
                    //                ent.BILLING_VOLUME_MT = Decimal.Parse(item.VolumnMT ?? "0");
                    //                ent.DENSITY15C = Decimal.Parse(item.Den_15 ?? "0");
                    //                ent.VISCOSITY50C = Decimal.Parse(item.Visc_cst_50 ?? "0");
                    //                ent.SULPHUR = Decimal.Parse(item.Sulfur ?? "0");
                    //                ent.NEW_UNIT_PRICE_USD = Decimal.Parse(item.UnitPriceUSDNew ?? "0");
                    //                ent.OLD_UNIT_PRICE_USD = Decimal.Parse(item.UnitPriceUSDOld ?? "0");
                    //                ent.ADJUST_PRICE_USD = Decimal.Parse(item.AdjustPrice ?? "0");
                    //                ent.NEW_ROE = Decimal.Parse(item.ExchangeRateNew ?? "0");
                    //                ent.OLD_ROE = Decimal.Parse(item.ExchangeRateOld ?? "0");
                    //                ent.NEW_UNIT_PRICE_THB = Decimal.Parse(item.UnitPriceTHBNew ?? "0");
                    //                ent.OLD_UNIT_PRICE_THB = Decimal.Parse(item.UnitPriceTHBOld ?? "0");
                    //                ent.PRICE_THB = Decimal.Parse(item.Price ?? "0");
                    //                ent.VAT_THB = Decimal.Parse(item.Vat ?? "0");
                    //                ent.TOTAL_THB = Decimal.Parse(item.Total ?? "0");

                    //                ent.UNIT_PRICE = unitPriect;
                    //                ent.NEW_UNIT_PRICE_THB = unitPriect;
                    //                item.UnitPrice = unitPriect.ToString();
                    //                item.UnitPriceTHBNew = unitPriect.ToString();

                    //                ent.UPDATED_BY = pUser;
                    //                ent.UPDATED_DATE = DateTime.Now.Date;
                    //                //dal.Save(ent, context);
                    //                dal.Save(ent, context);
                    //            }
                    //        }
                    //    }

                    //    if (pModel.data_Search.vAC_2SD_select)
                    //    {
                    //        unitPriect = 0;
                    //        foreach (var item in pModel.data_Search.sPriceDetail.dQualityList.Where(p => int.Parse(p.NO) <= 31 && int.Parse(p.NO) >= 16))
                    //        {
                    //            if (Decimal.Parse(item.UnitPriceTHBNew) > 0)
                    //            {
                    //                unitPriect = Decimal.Parse(item.UnitPriceTHBNew);
                    //            }
                    //        }

                    //        foreach (var item in pModel.data_Search.sPriceDetail.dQualityList.Where(p => int.Parse(p.NO) <= 31 && int.Parse(p.NO) >= 16))
                    //        {
                    //            p_no = item.NO;
                    //            if (item.PO_NO != null)
                    //            {
                    //                ent = new PIT_PO_PRICE();
                    //                //ConstantPrm.GetDynamicCtrlID();
                    //                decimal poItem = 10;
                    //                if (item.PO_ITEM != null)
                    //                {
                    //                    if (item.PO_ITEM.Length < 10)
                    //                    {
                    //                        poItem = Decimal.Parse(item.PO_ITEM ?? "10");
                    //                    }
                    //                }
                    //                ent.PO_ITEM = poItem;
                    //                ent.PO_NO = item.PO_NO;
                    //                ent.PO_DATE = DateTime.ParseExact(item.PODate, format, provider).Date;
                    //                ent.INVOICE_NO = item.InvoiceNo ?? "";
                    //                ent.INVOICE_STATUS = invoiceStatus;
                    //                ent.CUST_PO_NO = item.TripNo;
                    //                ent.MET_MAT = item.Product_Code;
                    //                ent.BILLING_VOLUME_MT = Decimal.Parse(item.VolumnMT ?? "0");
                    //                ent.DENSITY15C = Decimal.Parse(item.Den_15 ?? "0");
                    //                ent.VISCOSITY50C = Decimal.Parse(item.Visc_cst_50 ?? "0");
                    //                ent.SULPHUR = Decimal.Parse(item.Sulfur ?? "0");
                    //                ent.NEW_UNIT_PRICE_USD = Decimal.Parse(item.UnitPriceUSDNew ?? "0");
                    //                ent.OLD_UNIT_PRICE_USD = Decimal.Parse(item.UnitPriceUSDOld ?? "0");
                    //                ent.ADJUST_PRICE_USD = Decimal.Parse(item.AdjustPrice ?? "0");
                    //                ent.NEW_ROE = Decimal.Parse(item.ExchangeRateNew ?? "0");
                    //                ent.OLD_ROE = Decimal.Parse(item.ExchangeRateOld ?? "0");
                    //                ent.NEW_UNIT_PRICE_THB = Decimal.Parse(item.UnitPriceTHBNew ?? "0");
                    //                ent.OLD_UNIT_PRICE_THB = Decimal.Parse(item.UnitPriceTHBOld ?? "0");
                    //                ent.PRICE_THB = Decimal.Parse(item.Price ?? "0");
                    //                ent.VAT_THB = Decimal.Parse(item.Vat ?? "0");
                    //                ent.TOTAL_THB = Decimal.Parse(item.Total ?? "0");

                    //                ent.UNIT_PRICE = unitPriect;
                    //                ent.NEW_UNIT_PRICE_THB = unitPriect;
                    //                item.UnitPrice = unitPriect.ToString();
                    //                item.UnitPriceTHBNew = unitPriect.ToString();

                    //                ent.UPDATED_BY = pUser;
                    //                ent.UPDATED_DATE = DateTime.Now.Date;
                    //                //dal.Save(ent, context);
                    //                dal.Save(ent, context);
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    foreach (var item in pModel.data_Search.sPriceDetail.dQualityList)
                    {
                        p_no = item.NO;
                        if (item.PO_NO != null)
                        {
                            ent = new PIT_PO_PRICE();
                            //ConstantPrm.GetDynamicCtrlID();
                            decimal poItem = 10;
                            if (item.PO_ITEM != null)
                            {
                                if (item.PO_ITEM.Length < 10)
                                {
                                    poItem = Decimal.Parse(item.PO_ITEM ?? "10");
                                }
                            }
                            ent.PO_ITEM = poItem;
                            ent.PO_NO = item.PO_NO;
                            ent.PO_DATE = DateTime.ParseExact(item.PODate, format, provider).Date;
                            ent.INVOICE_NO = item.InvoiceNo ?? "";
                            ent.INVOICE_STATUS = invoiceStatus;
                            ent.CUST_PO_NO = item.TripNo;
                            ent.MET_MAT = item.Product_Code;
                            ent.BILLING_VOLUME_MT = Decimal.Parse(item.VolumnMT ?? "0");
                            ent.DENSITY15C = Decimal.Parse(item.Den_15 ?? "0");
                            ent.VISCOSITY50C = Decimal.Parse(item.Visc_cst_50 ?? "0");
                            ent.SULPHUR = Decimal.Parse(item.Sulfur ?? "0");
                            ent.NEW_UNIT_PRICE_USD = Decimal.Parse(item.UnitPriceUSDNew ?? "0");
                            ent.OLD_UNIT_PRICE_USD = Decimal.Parse(item.UnitPriceUSDOld ?? "0");
                            ent.ADJUST_PRICE_USD = Decimal.Parse(item.AdjustPrice ?? "0");
                            ent.NEW_ROE = Decimal.Parse(item.ExchangeRateNew ?? "0");
                            ent.OLD_ROE = Decimal.Parse(item.ExchangeRateOld ?? "0");
                            ent.NEW_UNIT_PRICE_THB = Decimal.Parse(item.UnitPriceTHBNew ?? "0");
                            ent.OLD_UNIT_PRICE_THB = Decimal.Parse(item.UnitPriceTHBOld ?? "0");
                            ent.PRICE_THB = Decimal.Parse(item.Price ?? "0");
                            ent.VAT_THB = Decimal.Parse(item.Vat ?? "0");
                            ent.TOTAL_THB = Decimal.Parse(item.Total ?? "0");

                            ent.UNIT_PRICE = Decimal.Parse(item.UnitPrice ?? "0");
                            //if ((item.IsThaiLube ?? "") == "1")
                            //{
                            //    if (pModel.data_Search.vInvoice_select)
                            //    {
                            //        ent.UNIT_PRICE = Decimal.Parse(item.UnitPriceTHBNew);
                            //    }
                            //    else
                            //    {
                            //        ent.UNIT_PRICE = Decimal.Parse(item.UnitPriceTHBOld);
                            //    }
                            //}
                            //else
                            //{
                            //    ent.UNIT_PRICE = Decimal.Parse(item.UnitPriceTHBNew);
                            //}

                            ent.UPDATED_BY = pUser;
                            ent.UPDATED_DATE = DateTime.Now.Date;
                            //dal.Save(ent, context);
                            dal.Save(ent, context);
                        }
                    }
                }
                //}
                //dbContextTransaction.Commit();
                getProductList(ref pModel);
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                rtn.Status = true;
            }
            catch (Exception ex)
            {
                //string path = path = (LogManager.GetCurrentLoggers()[0].Logger.Repository.GetAppenders()[0] as FileAppender).File;
                rtn.Message = ex.Message;
                rtn.Status = false;
                var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                Log.Error(ex.Message);
            }

            return rtn;
        }

        public static FeedStockDetailViewModel calValuePayment(FeedStockDetailViewModel item, string code, List<PricePeriodControl> pricePeriodList, List<ExchangeRateControl> exchangeRate)
        {
            if (code == "1" || code == "3" || code == "4")
            {//1 EXTRACT ,VR , RSO
             //3 VGO , 
             //4 SW , DAO
                foreach (ThaiOilPaymentControl product in item.PaymentList)
                {
                    string dateTimeString = Regex.Replace(item.Date, @"[^\u0000-\u007F]", string.Empty);
                    DateTime itemDate = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
                    PricePeriodControl pricePeriod = pricePeriodList.Where(p => itemDate >= DateTime.ParseExact(p.PeriodFrom, "yyyyMMdd", CultureInfo.CurrentCulture)
                    && itemDate <= DateTime.ParseExact(p.PeriodTo, "yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"))).FirstOrDefault();
                    //baseProduct = (ThaiOilPaymentControl)feedItem.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper() == "HSFO").FirstOrDefault();
                    if (product.Baseproduct.Trim().ToUpper().Contains("HSFO") && pricePeriod != null)
                    {
                        product.FOBSPorePrice = pricePeriod.HSFO;
                        product.Loss = "0";
                        product.ExportParity = ((Decimal)Math.Round((Convert.ToDecimal(product.FOBSPorePrice ?? "0") - Convert.ToDecimal(product.Freight ?? "0") - Convert.ToDecimal(product.Loss ?? "0")), 3, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else if (product.Baseproduct.Trim().ToUpper().Contains("LSFO") && pricePeriod != null)
                    {
                        product.FOBSPorePrice = pricePeriod.LSFO;
                        product.Loss = ((Decimal)Math.Round((((Convert.ToDecimal(product.FOBSPorePrice ?? "0") + Convert.ToDecimal(product.Freight ?? "0")) / Convert.ToDecimal("100.0000")) / Convert.ToDecimal("2.0000")), 4)).ToString();
                        product.ImportParity = ((Decimal)Math.Round((Convert.ToDecimal(product.FOBSPorePrice ?? "0") + Convert.ToDecimal(product.Freight ?? "0") + Convert.ToDecimal(product.Loss ?? "0")), 3, MidpointRounding.AwayFromZero)).ToString();
                    }
                    else if (product.Baseproduct.Trim().ToUpper().Contains("GASOIL") && pricePeriod != null)
                    {
                        product.FOBSPorePrice = pricePeriod.Gasoil;
                        product.Loss = ((Decimal)Math.Round((((Convert.ToDecimal(product.FOBSPorePrice ?? "0") + Convert.ToDecimal(product.Freight ?? "0")) / Convert.ToDecimal("100.0000")) / Convert.ToDecimal("2.0000")), 4)).ToString();
                        product.ImportParity = ((Decimal)Math.Round((Convert.ToDecimal(product.FOBSPorePrice ?? "0") + Convert.ToDecimal(product.Freight ?? "0") + Convert.ToDecimal(product.Loss ?? "0")), 3, MidpointRounding.AwayFromZero)).ToString();
                    }
                }


            }


            return item;
        }

        public FeedStockDetailViewModel setCompairTable(FeedStockDetailViewModel item)
        {
            item.CT_SulphurContent = item.Quality_Sulfur;
            item.CT_ViscosityCST = item.Quality_cst50;
            if (item.Product.Trim().ToUpper().Contains("RSO"))
            {
                //=10^(10^((D7-19.2-115*LOG((50+273)/323))/33.5))-0.85
                double val0 = stringToDouble(item.CT_ViscosityCST);
                double val1 = Math.Round(Math.Log10((50 + 273) / 323), 14, MidpointRounding.AwayFromZero);
                double val2 = Math.Round(val0 - 19.2 - 115 * val1, 14, MidpointRounding.AwayFromZero);
                double val3 = Math.Round(val2 / 33.5, 14);
                double val4 = Math.Round(Math.Pow(10, val3), 14, MidpointRounding.AwayFromZero);
                double val5 = Math.Round(Math.Pow(10, val4), 14, MidpointRounding.AwayFromZero);
                decimal val6 = Math.Round(((Decimal)val5 - (Decimal)0.85), 14, MidpointRounding.AwayFromZero);
                item.CT_ViscosityCST = val6.ToString();
            }
            if (item.CT_ViscosityCST != "0")
            {
                item.CT_ViscosityBlend = ((Decimal)(23.098 + 33.47 * Math.Log10(Math.Log10(Convert.ToDouble(item.CT_ViscosityCST ?? "0") + Convert.ToDouble("0.8"))))).ToString();
            }
            else
            {
                item.CT_ViscosityBlend = "0";
            }
            return item;
        }

        public static FeedStockDetailViewModel PhysicalCalculate(FeedStockDetailViewModel item)
        {

            PhysicalPropertiesBasedControl HSFO = new PhysicalPropertiesBasedControl();
            HSFO = item.PhysicalBasedList.Where(p => p.Product.Trim().ToUpper().Contains("HSFO")).FirstOrDefault();
            HSFO.fv1 = ((Decimal)(23.098 + 33.47 * Math.Log10(Math.Log10(Convert.ToDouble(HSFO.VIS_CST ?? "0") + Convert.ToDouble("0.8"))))).ToString();

            PhysicalPropertiesBasedControl LSFO = new PhysicalPropertiesBasedControl();
            LSFO = item.PhysicalBasedList.Where(p => p.Product.Trim().ToUpper().Contains("LSFO")).FirstOrDefault();
            LSFO.fv1 = ((Decimal)(23.098 + 33.47 * Math.Log10(Math.Log10(Convert.ToDouble(LSFO.VIS_CST ?? "0") + Convert.ToDouble("0.8"))))).ToString();

            PhysicalPropertiesBasedControl GasOil = new PhysicalPropertiesBasedControl();
            GasOil = item.PhysicalBasedList.Where(p => p.Product.Trim().ToUpper().Contains("GASOIL")).FirstOrDefault();
            GasOil.fv1 = ((Decimal)(23.098 + 33.47 * Math.Log10(Math.Log10(Convert.ToDouble(GasOil.VIS_CST ?? "0") + Convert.ToDouble("0.8"))))).ToString();


            if (item.PhysicalBasedList.Count > 0)
            {
                item.InverseData[0].X = "1";
                item.InverseData[0].Y = "1";
                item.InverseData[0].Z = "1";
                item.InverseData[0].RHS = "1";

                item.PhysicalBasedList[0].X = "1";
                item.PhysicalBasedList[0].Y = "1";
                item.PhysicalBasedList[0].Z = "1";
                item.PhysicalBasedList[0].RHS = "1";
            }

            if (item.PhysicalBasedList.Count > 1)
            {
                item.InverseData[1].X = HSFO.fv1;
                item.InverseData[1].Y = LSFO.fv1;
                item.InverseData[1].Z = GasOil.fv1;
                item.InverseData[1].RHS = item.CT_ViscosityBlend;

                item.PhysicalBasedList[1].X = HSFO.fv1;
                item.PhysicalBasedList[1].Y = LSFO.fv1;
                item.PhysicalBasedList[1].Z = GasOil.fv1;
                item.PhysicalBasedList[1].RHS = item.CT_ViscosityBlend;
            }

            if (item.PhysicalBasedList.Count > 2)
            {
                item.InverseData[2].X = HSFO.SUL_W;
                item.InverseData[2].Y = LSFO.SUL_W;
                item.InverseData[2].Z = GasOil.SUL_W;
                item.InverseData[2].RHS = item.CT_SulphurContent;

                item.PhysicalBasedList[2].X = HSFO.SUL_W;
                item.PhysicalBasedList[2].Y = LSFO.SUL_W;
                item.PhysicalBasedList[2].Z = GasOil.SUL_W;
                item.PhysicalBasedList[2].RHS = item.CT_SulphurContent;
            }
            return item;
        }


        public static List<FeedStockDetailViewModel> calculateValueMainData()
        {
            List<FeedStockDetailViewModel> data = new List<FeedStockDetailViewModel>();

            foreach (FeedStockDetailViewModel i in data)
            {
                i.Volume = i.Volume == "" ? "0" : i.Volume;
                i.UnitPriceNewBathMT = ((Decimal)Math.Round(Convert.ToDecimal(i.UnitPriceNewMT ?? "0") * (Convert.ToDecimal(i.ExchangeRateNew ?? "0")), 4, MidpointRounding.AwayFromZero)).ToString();
                i.UnitPriceOldBathMT = ((Decimal)Math.Round(Convert.ToDecimal(i.UnitPriceOldMT ?? "0") * (Convert.ToDecimal(i.ExchangeRateOld ?? "0")), 4, MidpointRounding.AwayFromZero)).ToString();
                i.ProvisionalPrice = ((Decimal)Math.Round(Convert.ToDecimal(i.Volume ?? "0") * (Convert.ToDecimal(i.UnitPriceOldBathMT ?? "0")), 2, MidpointRounding.AwayFromZero)).ToString();
                i.ProvisionalVat = Math.Round((Convert.ToDecimal(i.ProvisionalPrice) / 100) * 7, 2, MidpointRounding.AwayFromZero).ToString();
                i.ProvisionalTotal = (Convert.ToDecimal(i.ProvisionalPrice) + Convert.ToDecimal(i.ProvisionalVat)).ToString();
                i.FinalPrice = ((Decimal)Math.Round(Convert.ToDecimal(i.Volume ?? "0") * (Convert.ToDecimal(i.UnitPriceNewBathMT ?? "0")), 2, MidpointRounding.AwayFromZero)).ToString();
                i.FinalVat = Math.Round((Convert.ToDecimal(i.FinalPrice) / 100) * 7, 2, MidpointRounding.AwayFromZero).ToString();
                i.FinalTotal = (Convert.ToDecimal(i.FinalPrice) + Convert.ToDecimal(i.FinalVat)).ToString();

                i.PriceIncludeVat = (Convert.ToDecimal(i.FinalTotal) / Convert.ToDecimal(i.Volume)).ToString();
                i.Final_Pro = (Convert.ToDecimal(i.UnitPriceNewBathMT) - Convert.ToDecimal(i.UnitPriceOldBathMT)).ToString();
                i.AmountExcVat = Math.Round(Convert.ToDecimal(i.Final_Pro) * Convert.ToDecimal(i.Volume), 2, MidpointRounding.AwayFromZero).ToString();
                i.Vat = Math.Round((Convert.ToDecimal(i.AmountExcVat) / 100) * 7, 2, MidpointRounding.AwayFromZero).ToString();
                i.TotalIncVat = Math.Round(Convert.ToDecimal(i.AmountExcVat) + Convert.ToDecimal(i.Vat), 2, MidpointRounding.AwayFromZero).ToString();
            }

            return data;
        }

        public FeedStockDetailViewModel setConstantValue(FeedStockDetailViewModel item)
        {
            item.PaymentList = new List<ThaiOilPaymentControl>();
            item.PhysicalBasedList = new List<PhysicalPropertiesBasedControl>();
            item.InverseData = new List<INVERSE_DATA>();

            FeedstockBookingPriceServiceModel serviceModel = new FeedstockBookingPriceServiceModel();
            CONFIG_INTERCOMPANY dataList = serviceModel.getCIPConfig("INTER_PRICE_UPDATE");

            if (dataList != null && dataList.CONSTANT_PRODUCT_BASE_DETAIL != null)
            {
                item.CT_TemperatureDeg = dataList.CONSTANT_PRODUCT_BASE_DETAIL.TEMPERATURE_DEG_C;
                item.PhysicalValueMin = dataList.CONSTANT_PRODUCT_BASE_DETAIL.MIN;
                item.PhysicalValueMax = dataList.CONSTANT_PRODUCT_BASE_DETAIL.MAX;
                item.InverseData = dataList.CONSTANT_PRODUCT_BASE_DETAIL.INVERSE_DATA;

                foreach (PRODUCT_DETAIL p in dataList.CONSTANT_PRODUCT_BASE_DETAIL.PRODUCT_DETAIL)
                {
                    string product = p.TYPE;
                    string den15 = p.DEN15;
                    string sul_w = p.SUL_W;
                    string vis_cst = p.VIS_CST;
                    string fv = p.FV;
                    string freight = p.FREIGHT;
                    string temp_c = p.TEMP_C;

                    ThaiOilPaymentControl paymentValues = new ThaiOilPaymentControl();
                    paymentValues.Baseproduct = product;
                    paymentValues.Percent_S = sul_w;
                    paymentValues.Viscosity40 = "0";
                    paymentValues.Viscosity50 = vis_cst;
                    paymentValues.ViscosityBlend = fv;
                    paymentValues.FOBSPorePrice = "0";
                    paymentValues.Freight = freight;
                    paymentValues.Loss = "0";
                    paymentValues.ExportParity = "0";
                    paymentValues.ImportParity = "0";
                    item.PaymentList.Add(paymentValues);

                    PhysicalPropertiesBasedControl physical = new PhysicalPropertiesBasedControl();
                    physical.Product = product;
                    physical.DEN15 = den15;
                    physical.VIS_CST = vis_cst;
                    physical.TEMP_C = temp_c;
                    physical.SUL_W = sul_w;
                    physical.fv1 = fv;
                    item.PhysicalBasedList.Add(physical);
                }
            }

            return item;
        }

        public void ComparisonTable(FeedStockDetailViewModel item, List<PricePeriodControl> pricePeriodList, string code)
        {
            string dateTimeString = Regex.Replace(item.Date, @"[^\u0000-\u007F]", string.Empty);
            DateTime itemDate = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            //PricePeriodControl pricePeriod = pricePeriodList.Where(p => itemDate >= DateTime.ParseExact(p.PeriodFrom, "yyyyMMdd", CultureInfo.CurrentCulture)
            //            && itemDate <= DateTime.ParseExact(p.PeriodTo, "yyyyMMdd", CultureInfo.CreateSpecificCulture("en-US"))).FirstOrDefault();
            PricePeriodControl pricePeriod = pricePeriodList.Where(p => p.RowID == "1").FirstOrDefault();
            if (code == "1")
            {
                ThaiOilPaymentControl HSFO = new ThaiOilPaymentControl();
                HSFO = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("HSFO")).FirstOrDefault();

                ThaiOilPaymentControl LSFO = new ThaiOilPaymentControl();
                LSFO = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("LSFO")).FirstOrDefault();

                ThaiOilPaymentControl GasOil = new ThaiOilPaymentControl();
                GasOil = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("GASOIL")).FirstOrDefault();

                item.CT_Formula = ((Convert.ToDouble(item.CT_X) * Convert.ToDouble(HSFO.ExportParity)) +
                    (Convert.ToDouble(item.CT_Y) * Convert.ToDouble(LSFO.ImportParity)) +
                    (Convert.ToDouble(item.CT_Z) * Convert.ToDouble(GasOil.ImportParity))
                    ).ToString();
                item.FinalPrice = string.Format("{0:0.##}", Math.Round((Convert.ToDecimal
                    (item.CT_Formula ?? "0") - (decimal)0.5), 2, MidpointRounding.AwayFromZero));
            }//201.53 ค่าจริง 201.56 ค่าที่ได้
            else if (code == "2") //LVGO
            {

                item.CT_MOPS_GO = pricePeriod.Gasoil;
                item.FinalPrice = string.Format("{0:0.##}", Math.Round((Convert.ToDecimal(item.CT_MOPS_GO ?? "0") - (Decimal)0.5), 2, MidpointRounding.AwayFromZero));
            }
            else if (code == "3") //VGO
            {
                ThaiOilPaymentControl HSFO = new ThaiOilPaymentControl();
                HSFO = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("HSFO")).FirstOrDefault();

                ThaiOilPaymentControl LSFO = new ThaiOilPaymentControl();
                LSFO = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("LSFO")).FirstOrDefault();

                ThaiOilPaymentControl GasOil = new ThaiOilPaymentControl();
                GasOil = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("GASOIL")).FirstOrDefault();

                item.CT_Formula = ((Convert.ToDouble(item.CT_X) * Convert.ToDouble(HSFO.ExportParity)) +
                    (Convert.ToDouble(item.CT_Y) * Convert.ToDouble(LSFO.ImportParity)) +
                    (Convert.ToDouble(item.CT_Z) * Convert.ToDouble(GasOil.ImportParity))
                    ).ToString();
                item.CT_FBV = string.Format("{0:0.##}", Math.Round((Convert.ToDecimal(item.CT_Formula) - (Decimal)0.5), 2, MidpointRounding.AwayFromZero));
                item.HCU = new HCUFormulaControl();
                item.HCU.Kero = Convert.ToDouble(pricePeriod.Kero ?? "0");
                item.HCU.Gasoil = Convert.ToDouble(pricePeriod.Gasoil ?? "0");
                item.HCU.ULG95 = Convert.ToDouble(pricePeriod.ULG95 ?? "0");
                item.HCU.HSFO = Convert.ToDouble(pricePeriod.HSFO ?? "0");
                item.HCU.H2 = Convert.ToDouble(pricePeriod.H2 ?? "0");
                item.HCU.HCU_Benefit = (Double)Math.Round((Decimal)(0.67 * (item.HCU.Kero + item.HCU.Gasoil) / 2 + (0.24 * item.HCU.ULG95) + (0.09 * item.HCU.HSFO) - (0.03 * item.HCU.H2) - 41), 2, MidpointRounding.AwayFromZero);

                item.CT_HCU = Math.Round((Decimal)(0.3 * (item.HCU.HCU_Benefit - Convert.ToDouble(item.CT_FBV))), 2, MidpointRounding.AwayFromZero).ToString();
                item.FinalPrice = string.Format("{0:0.##}", (Convert.ToDouble(item.CT_FBV ?? "0") + Convert.ToDouble(item.CT_HCU ?? "0")));
            }
            else if (code == "4") //SW ,DAO
            {
                ThaiOilPaymentControl HSFO = new ThaiOilPaymentControl();
                HSFO = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("HSFO")).FirstOrDefault();

                ThaiOilPaymentControl LSFO = new ThaiOilPaymentControl();
                LSFO = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("LSFO")).FirstOrDefault();

                ThaiOilPaymentControl GasOil = new ThaiOilPaymentControl();
                GasOil = item.PaymentList.Where(p => p.Baseproduct.Trim().ToUpper().Contains("GASOIL")).FirstOrDefault();

                item.CT_Formula = ((Convert.ToDouble(item.CT_X) * Convert.ToDouble(HSFO.ExportParity)) +
                    (Convert.ToDouble(item.CT_Y) * Convert.ToDouble(LSFO.ImportParity)) +
                    (Convert.ToDouble(item.CT_Z) * Convert.ToDouble(GasOil.ImportParity))
                    ).ToString();
                item.CT_FBV = string.Format("{0:0.##}", Math.Round((Convert.ToDecimal(item.CT_Formula) - (Decimal)0.5), 2, MidpointRounding.AwayFromZero));
                item.HCU = new HCUFormulaControl();
                item.HCU.Gasoil = Convert.ToDouble(pricePeriod.Gasoil ?? "0");
                item.HCU.ULG95 = Convert.ToDouble(pricePeriod.ULG95 ?? "0");
                item.HCU.HSFO = Convert.ToDouble(pricePeriod.HSFO ?? "0");
                item.HCU.LPG = Convert.ToDouble(pricePeriod.LPG ?? "0");
                item.HCU.FCC_Benefit = (Double)Math.Round((Decimal)((0.47 * item.HCU.ULG95) + (0.23 * item.HCU.Gasoil) + (0.21 * item.HCU.HSFO) + (0.09 * item.HCU.LPG)), 2, MidpointRounding.AwayFromZero);
                item.CT_FCC = Math.Round((Decimal)(0.3 * (item.HCU.FCC_Benefit - Convert.ToDouble(item.CT_FBV))), 2, MidpointRounding.AwayFromZero).ToString();
                item.FinalPrice = string.Format("{0:0.##}", (Convert.ToDouble(item.CT_FBV ?? "0") + Convert.ToDouble(item.CT_FCC ?? "0")));
            }


        }

        public void InverseData(FeedStockDetailViewModel item)
        {
            Double[][] mVal = MatrixCreate(3, 3);
            //mVal[0][0] = 1.0; mVal[0][1] = 1.0; mVal[0][2] = 1.0;
            //mVal[1][0] = 34.9319791569898; mVal[1][1] = 34.9319791569898; mVal[1][2] = 16.4617355316244;
            //mVal[2][0] = 3.0; mVal[2][1] = 2.0; mVal[2][2] = 0.5;
            Double[][] bVal = MatrixCreate(3, 1);
            PhysicalPropertiesBasedControl HSFO = new PhysicalPropertiesBasedControl();
            HSFO = item.PhysicalBasedList.Where(p => p.Product.Trim().ToUpper().Contains("HSFO")).FirstOrDefault();
            bVal[0][0] = Convert.ToDouble(HSFO.RHS);
            mVal[0][0] = Convert.ToDouble(HSFO.X); mVal[0][1] = Convert.ToDouble(HSFO.Y); mVal[0][2] = Convert.ToDouble(HSFO.Z);

            PhysicalPropertiesBasedControl LSFO = new PhysicalPropertiesBasedControl();
            LSFO = item.PhysicalBasedList.Where(p => p.Product.Trim().ToUpper().Contains("LSFO")).FirstOrDefault();
            bVal[1][0] = Convert.ToDouble(LSFO.RHS);
            mVal[1][0] = Convert.ToDouble(LSFO.X); mVal[1][1] = Convert.ToDouble(LSFO.Y); mVal[1][2] = Convert.ToDouble(LSFO.Z);

            PhysicalPropertiesBasedControl GasOil = new PhysicalPropertiesBasedControl();
            GasOil = item.PhysicalBasedList.Where(p => p.Product.Trim().ToUpper().Contains("GASOIL")).FirstOrDefault();
            bVal[2][0] = Convert.ToDouble(GasOil.RHS);
            mVal[2][0] = Convert.ToDouble(GasOil.X); mVal[2][1] = Convert.ToDouble(GasOil.Y); mVal[2][2] = Convert.ToDouble(GasOil.Z);

            //เอาไว้ convert เป็น matrix แถวล่าง
            Double[][] inv = MatrixCreate(3, 3); //MatrixInverse(mVal);
            inv[0][0] = Double.Parse(item.InverseData[0].INV_X); inv[0][1] = Double.Parse(item.InverseData[0].INV_Y); inv[0][2] = Double.Parse(item.InverseData[0].INV_Z);
            inv[1][0] = Double.Parse(item.InverseData[1].INV_X); inv[1][1] = Double.Parse(item.InverseData[1].INV_Y); inv[1][2] = Double.Parse(item.InverseData[1].INV_Z);
            inv[2][0] = Double.Parse(item.InverseData[2].INV_X); inv[2][1] = Double.Parse(item.InverseData[2].INV_Y); inv[2][2] = Double.Parse(item.InverseData[2].INV_Z);
            //Double[][] invTest = MatrixInverse(mVal);
            //Matrix result = new Matrix(_a.N, b.M);
            Double[][] result = MatrixCreate(3, 3);
            //คำนวนหา MMULT แถวล่าง
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 1; j++)
                {
                    Double sum = 0.0;
                    for (int k = 0; k < 3; k++)
                        sum += inv[i][k] * bVal[k][j];
                    //Math.Round(Math.Round(inv[i][k], 8) * Math.Round(bVal[k][j], 8), 8);
                    result[i][j] = sum;
                    if (i == 0 && j == 0)
                    {
                        HSFO.RHS_Sum = sum.ToString();
                        item.CT_X = sum.ToString();//sum.ToString("0.#########");
                    }
                    else if (i == 1 && j == 0)
                    {
                        LSFO.RHS_Sum = sum.ToString();
                        item.CT_Y = sum.ToString();
                    }
                    else if (i == 2 && j == 0)
                    {
                        GasOil.RHS_Sum = sum.ToString();
                        item.CT_Z = sum.ToString();
                    }
                }

            //3.308267494590620         -2.453779157651020          0.145511663060409
            //Check =IF(SUM(J40:J42)=1,"OK", "ERROR")
            Double temp = Convert.ToDouble(Convert.ToDouble(item.CT_X).ToString())
            + Convert.ToDouble(Convert.ToDouble(item.CT_Y).ToString())
            + Convert.ToDouble(Convert.ToDouble(item.CT_Z).ToString());
        } // Main

        static Double[][] MatrixInverse(Double[][] matrix)
        {
            // assumes determinant is not 0
            // that is, the matrix does have an inverse
            int n = matrix.Length;
            Double[][] result = MatrixCreate(n, n); // make a copy of matrix
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    result[i][j] = matrix[i][j];

            Double[][] lum; // combined lower & upper
            int[] perm;
            int toggle;
            toggle = MatrixDecompose(matrix, out lum, out perm);

            Double[] b = new Double[n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                    if (i == perm[j])
                        b[j] = 1.0;
                    else
                        b[j] = 0.0;

                Double[] x = Helper(lum, b); // 
                for (int j = 0; j < n; ++j)
                    result[j][i] = x[j];
            }
            return result;
        } // MatrixInverse

        static int MatrixDecompose(Double[][] m, out Double[][] lum, out int[] perm)
        {

            int toggle = +1; // even (+1) or odd (-1) row permutatuions
            int n = m.Length;

            // make a copy of m[][] into result lu[][]
            lum = MatrixCreate(n, n);
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j)
                    lum[i][j] = m[i][j];


            // make perm[]
            perm = new int[n];
            for (int i = 0; i < n; ++i)
                perm[i] = i;

            for (int j = 0; j < n - 1; ++j) // process by column. note n-1 
            {
                Double max = Math.Abs(lum[j][j]);
                int piv = j;

                for (int i = j + 1; i < n; ++i) // find pivot index
                {
                    Double xij = Math.Abs(lum[i][j]);
                    if (xij > max)
                    {
                        max = xij;
                        piv = i;
                    }
                } // i

                if (piv != j)
                {
                    Double[] tmp = lum[piv]; // swap rows j, piv
                    lum[piv] = lum[j];
                    lum[j] = tmp;

                    int t = perm[piv]; // swap perm elements
                    perm[piv] = perm[j];
                    perm[j] = t;

                    toggle = -toggle;
                }

                Double xjj = lum[j][j];
                if (xjj != 0.0)
                {
                    for (int i = j + 1; i < n; ++i)
                    {
                        //mVal[0][0] = 1.0; mVal[0][1] = 1.0; mVal[0][2] = 1.0;
                        //mVal[1][0] = 34.93; mVal[1][1] = 34.93; mVal[1][2] = 16.46;
                        //mVal[2][0] = 3.0; mVal[2][1] = 2.0; mVal[2][2] = 0.5;
                        // 0.836885631   -0.081211706   1.000000000  
                        //-1.728142718    0.135352844  -1.000000000
                        // 1.891257087   -0.054141138   0.000000000
                        // 3.308267495   -2.453779158   0.145511663

                        //0.02862868594
                        string tmpVal1 = Math.Round(lum[i][j] / xjj, 9).ToString();
                        Double xij = Convert.ToDouble(tmpVal1.Substring(0, tmpVal1.Length));
                        lum[i][j] = xij;
                        for (int k = j + 1; k < n; ++k)
                        {
                            Double forCheck = lum[i][k];
                            Double tmpVal2 = xij * lum[j][k];
                            Double tmpVal3 = Math.Round(forCheck - tmpVal2, 9);
                            lum[i][k] = tmpVal3.ToString().Length > 0 ? Convert.ToDouble(tmpVal3.ToString().Substring(0, tmpVal3.ToString().Length - 1)) : tmpVal3;
                        }
                    }
                }

            } // j

            return toggle;
        } // MatrixDecompose

        static Double[] Helper(Double[][] luMatrix, Double[] b) // helper
        {
            int n = luMatrix.Length;
            Double[] x = new Double[n];
            b.CopyTo(x, 0);

            for (int i = 1; i < n; ++i)
            {
                Double sum = x[i];
                for (int j = 0; j < i; ++j)
                    sum = sum - luMatrix[i][j] * x[j];
                x[i] = sum;
            }

            // 1 / 0.52877182937303169 = 1.89117487818
            Double aa = Convert.ToDouble(string.Format("{0:0.#########}", x[n - 1]));
            Double bb = Convert.ToDouble(string.Format("{0:0.#########}", luMatrix[n - 1][n - 1]));
            Double cc = aa / bb;
            Double dd = aa / luMatrix[n - 1][n - 1];
            x[n - 1] = Convert.ToDouble(string.Format("{0:0.#########}",
                Convert.ToDouble(string.Format("{0:0.#########}", x[n - 1])) /
                Convert.ToDouble(string.Format("{0:0.#########}", luMatrix[n - 1][n - 1]))));
            //1.891257087
            for (int i = n - 2; i >= 0; --i)
            {
                Double sum = x[i];
                for (int j = i + 1; j < n; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = Convert.ToDouble(string.Format("{0:0.#########}", (sum / luMatrix[i][i])));
            }

            return x;
        } // Helper

        static Double[][] MatrixCreate(int rows, int cols)
        {
            Double[][] result = new Double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new Double[cols];
            return result;
        }
        // ----------------------------------------------------------------
        public Double stringToDouble(string value)
        {
            double temp_value = 0;
            double number;
            if (Double.TryParse(value, out number))
            {
                temp_value = number;
            }
            return temp_value;
        }

        public Int32 stringToInt(string value)
        {
            Int32 temp_value = 0;
            Int32 number;
            if (Int32.TryParse(value, out number))
            {
                temp_value = number;
            }
            return temp_value;
        }
    }

}