﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.Entity;
using System.Web.Mvc;
using System.Globalization;
using System.Collections;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class FormulaPricingServiceModel
    {
        private CultureInfo provider = new CultureInfo("en-US");
        private string format = "dd/MM/yyyy";
        public ReturnValue Add(FormulaPricingViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            return rtn;
        }

        public ReturnValue Edit(FormulaPricingViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            return rtn;
        }

        public ReturnValue Search(ref FormulaPricingViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var sFormulaName = "";
                var sMaterial = "";
                var sPriceStatus = "";

                var query = (from v in context.FORMULA select v);
                if (query != null)
                {
                    pModel.sSearchData = new List<FormulaPricingViewModel_SearchData>();
                    foreach (var g in query)
                    {
                        pModel.sSearchData.Add(new FormulaPricingViewModel_SearchData
                        {
                            dFormulaCode = g.FOR_CODE,
                            dFormulaName = g.FOR_NAME,
                            dMaterial = g.FOR_MET_NUM,
                            dPriceStatus = g.FOR_PRICE_STATUS
                        });
                    }

                    rtn.Status = true;
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = "Data is null";
                }


            }


            return rtn;
        }

        public FormulaPricingViewModel_Detail Get(string pFOPCode)
        {
            FormulaPricingViewModel_Detail model = new FormulaPricingViewModel_Detail();
            try
            {
                if (string.IsNullOrEmpty(pFOPCode))
                    return null;

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.FORMULA
                                 where v.FOR_CODE.Equals(pFOPCode)
                                 select new
                                 {
                                     FormulaCode = v.FOR_CODE
                                     ,
                                     FormulaName = v.FOR_NAME
                                     ,
                                     Material = v.FOR_MET_NUM
                                     ,
                                     PriceStatus = v.FOR_PRICE_STATUS

                                 });

                    if (query == null)
                        return null;

                    foreach (var g in query)
                    {
                        model.FormulaCode = g.FormulaCode;
                        model.FormulaName = g.FormulaName;
                        model.Material = g.Material;
                        model.PriceStatus = g.PriceStatus;
                    }

                    var query2 = (from v in context.FORMULA_SAP_PRICING
                                  where v.FK_FOR_CODE.ToUpper().Contains(pFOPCode.ToUpper())
                                  select v);

                    if (query2 != null)
                    {
                        model.dDetailVariable = new List<FormulaPricingViewModel_DetailVariable>();
                        foreach (var q in query2)
                        {
                            model.dDetailVariable.Add(new FormulaPricingViewModel_DetailVariable
                            {
                                fos_code = q.FOS_CODE,
                                PriceStructureCode = q.FOS_SAP_PRICING_STRUCTURE,
                                PriceStructure = q.FOS_SAP_PRICING_STRUCTURE,
                                VariableCode = q.FOS_VARIABLE_CODE,
                                VariableDesc = q.FOS_VARIABLE_CODE
                            });
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }
        public string ExchangeReteCalculate(string bl_date, string completeTime, string exchange_type, string transport_type)
        {
            string value = "";
            try
            {
                var sDate = bl_date;
                if (exchange_type.Trim().ToUpper() == "A")//Exchange rate as Agreement
                {
                    if (transport_type.Trim().ToUpper() == "TRAIN")
                    {
                        DateTime tempDate = DateTime.ParseExact(sDate, format, provider);
                        DateTime sDateFrom = DateTime.ParseExact("01/" + tempDate.Month.ToString().PadLeft(2, '0') + "/" + sDate.Substring(6, 4), format, provider);
                        DateTime sDateTo = sDateFrom.AddMonths(1).AddDays(-1);

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo.ToString("yyyyMMdd", provider))
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom.ToString("yyyyMMdd", provider))
                                          select d;
                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal val = 0;
                                var valList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Selling Rates" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "M").ToList();
                                decimal num = 0;
                                foreach (var i in valList)
                                {
                                    num = num + 1;
                                    val += (i.T_FXB_VALUE1 ?? 0);
                                }
                                value = (val / num).ToString();
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }
                    else//by VESSEL
                    {
                        sDate = sDate.Substring(6, 4) + sDate.Substring(3, 2) + sDate.Substring(0, 2);
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) == Int32.Parse(sDate)
                                          select d;

                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal sightBill = 0;
                                var sightBillList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Sight Bill").ToList();
                                if (sightBillList.Count > 0)
                                {
                                    sightBill = sightBillList.FirstOrDefault().T_FXB_VALUE1 ?? 0;
                                }

                                decimal sellingRate = 0;
                                var sellingRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Selling Rates" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "M").ToList();
                                if (sellingRateList.Count > 0)
                                {
                                    sellingRate = sellingRateList.FirstOrDefault().T_FXB_VALUE1 ?? 0;
                                }
                                value = ((sightBill + sellingRate) / 2).ToString();
                            }
                            else
                            {
                                return "";
                            }

                        }
                    }
                }
                else if (exchange_type.Trim().ToUpper() == "B")//Exchange rate as BOT
                {
                    string[] time = completeTime.Split(':');
                    if (time.Length == 2)
                    {
                        int hr = stringToInt(time[0]);
                        int minute = stringToInt(time[1]);
                        if (hr < 18)
                        {
                            sDate = sDate.Substring(6, 4) + sDate.Substring(3, 2) + (stringToInt(sDate.Substring(0, 2)) - 1).ToString().PadLeft(2, '0');
                        }
                        else
                        {
                            sDate = sDate.Substring(6, 4) + sDate.Substring(3, 2) + sDate.Substring(0, 2);
                        }
                    }

                    if (transport_type.Trim().ToUpper() == "TRAIN")
                    {
                        sDate = sDate.Substring(6, 2) + "/" + sDate.Substring(4, 2) + "/" + sDate.Substring(0, 4);
                        DateTime tempDate = DateTime.ParseExact(sDate, format, provider);
                        DateTime sDateFrom = DateTime.ParseExact("01/" + tempDate.Month.ToString().PadLeft(2, '0') + "/" + sDate.Substring(6, 4), format, provider);
                        DateTime sDateTo = sDateFrom.AddMonths(1).AddDays(-1);

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo.ToString("yyyyMMdd", provider))
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom.ToString("yyyyMMdd", provider))
                                          select d;

                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal val = 0;
                                var valList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Transfer" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "B").ToList();
                                decimal num = 0;
                                foreach (var i in valList)
                                {
                                    num = num + 1;
                                    val += (i.T_FXB_VALUE1 ?? 0);
                                }
                                value = (val / num).ToString();
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }
                    else//by VESSEL
                    {
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) == Int32.Parse(sDate)
                                          select d;

                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal buyingRate = 0;
                                var sellingRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Transfer" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "B").ToList();
                                if (sellingRateList.Count > 0)
                                {
                                    buyingRate = sellingRateList.FirstOrDefault().T_FXB_VALUE1 ?? 0;
                                }
                                value = buyingRate.ToString();
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                value = "";
                throw ex;
            }
            return value;
        }

        public string ExchangeReteCalculatePCF_V2(string fx_date_period, string completeTime, string exchange_type, string transport_type)
        {
            string value = "";
            try
            {
                if (string.IsNullOrEmpty(fx_date_period))
                    return "";

                string[] arrdate = fx_date_period.Replace(" to ", "|").Split('|');
                if (arrdate.Length != 2)
                    return "";

                DateTime dFromDate = DateTime.ParseExact(arrdate[0], format, provider);
                DateTime dToDate = DateTime.ParseExact(arrdate[1], format, provider);

                int iFromDate = Int32.Parse(dFromDate.ToString("yyyyMMdd", provider));
                int iToDate = Int32.Parse(dToDate.ToString("yyyyMMdd", provider));

                if (exchange_type.Trim().ToUpper() == "XA")//Exchange rate as Agreement
                {
                    if (transport_type.Trim().ToUpper() == "VESSEL")
                    {

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= iToDate
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= iFromDate
                                          select d;

                            if (queryFx == null) return "";

                            var temp = queryFx.ToList();
                            if (temp.Count == 0) return "";

                            decimal val = 0;
                            decimal num = 0;

                            decimal sightBill = 0;
                            var sightBillList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Sight Bill").ToList();
                            foreach (var i in sightBillList)
                            {
                                num = num + 1;
                                val += (i.T_FXB_VALUE1 ?? 0);
                            }
                            sightBill = (val / num);

                            num = 0;
                            val = 0;
                            decimal sellingRate = 0;
                            var sellingRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Selling Rates" ||
                                                                  p.T_FXB_VALTYPE.Trim() == "M").ToList();
                            foreach (var i in sellingRateList)
                            {
                                num = num + 1;
                                val += (i.T_FXB_VALUE1 ?? 0);
                            }
                            sellingRate = (val / num);

                            value = ((sightBill + sellingRate) / 2).ToString();

                        }
                    }


                }
                else if (exchange_type.Trim().ToUpper() == "XB")//Exchange rate as BOT
                {

                    if (transport_type.Trim().ToUpper() == "VESSEL")
                    {

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= iToDate
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= iFromDate
                                          select d;

                            if (queryFx == null) return "";

                            var temp = queryFx.ToList();
                            if (temp.Count == 0) return "";


                            decimal val = 0;
                            decimal num = 0;
                            var buyingRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Transfer" ||
                                                                     p.T_FXB_VALTYPE.Trim() == "B").ToList();

                            foreach (var i in buyingRateList)
                            {
                                num = num + 1;
                                val += (i.T_FXB_VALUE1 ?? 0);
                            }
                            value = (val / num).ToString();

                        }
                    }

                }
                else if (exchange_type.Trim().ToUpper() == "A" || exchange_type.Trim().ToUpper() == "B")
                {

                    if (transport_type.Trim().ToUpper() == "VESSEL")
                    {

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= iToDate
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= iFromDate
                                          select d;

                            if (queryFx == null) return "";

                            var temp = queryFx.ToList();
                            if (temp.Count == 0) return "";


                            decimal val = 0;
                            decimal num = 0;
                            var SellRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Selling Rates" ||
                                                                  p.T_FXB_VALTYPE.Trim() == "M").ToList();

                            foreach (var i in SellRateList)
                            {
                                num = num + 1;
                                val += (i.T_FXB_VALUE1 ?? 0);
                            }
                            value = (val / num).ToString();

                        }
                    }

                }

            }
            catch (Exception ex)
            {
                value = "";
                throw ex;
            }

            return value;
        }

        public string ExchangeReteCalculatePCF(string bl_date, string completeTime, string exchange_type, string transport_type)
        {
            string value = "";
            try
            {
                var sDate = bl_date;
                if (exchange_type.Trim().ToUpper() == "A")//Exchange rate as Agreement
                {
                    if (transport_type.Trim().ToUpper() == "TRAIN")
                    {
                        DateTime tempDate = DateTime.ParseExact(sDate, format, provider);
                        DateTime sDateFrom = DateTime.ParseExact("01/" + tempDate.Month.ToString().PadLeft(2, '0') + "/" + sDate.Substring(6, 4), format, provider);
                        DateTime sDateTo = sDateFrom.AddMonths(1).AddDays(-1);

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo.ToString("yyyyMMdd", provider))
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom.ToString("yyyyMMdd", provider))
                                          select d;
                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal val = 0;
                                var valList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Selling Rates" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "M").ToList();
                                decimal num = 0;
                                foreach (var i in valList)
                                {
                                    num = num + 1;
                                    val += (i.T_FXB_VALUE1 ?? 0);
                                }
                                value = (val / num).ToString();
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }
                    else//by VESSEL
                    {
                        sDate = sDate.Substring(6, 4) + sDate.Substring(3, 2) + sDate.Substring(0, 2);
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDate)
                                          select d;

                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal sightBill = 0;
                                var sightBillList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Sight Bill").ToList();
                                if (sightBillList.Count > 0)
                                {
                                    sightBill = sightBillList.OrderByDescending(p => p.T_FXB_VALDATE).FirstOrDefault().T_FXB_VALUE1 ?? 0;
                                }

                                decimal sellingRate = 0;
                                var sellingRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Selling Rates" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "M").ToList();
                                if (sellingRateList.Count > 0)
                                {
                                    sellingRate = sellingRateList.OrderByDescending(p => p.T_FXB_VALDATE).FirstOrDefault().T_FXB_VALUE1 ?? 0;
                                }
                                value = ((sightBill + sellingRate) / 2).ToString();
                            }
                            else
                            {
                                return "";
                            }

                        }
                    }
                }
                else if (exchange_type.Trim().ToUpper() == "B")//Exchange rate as BOT
                {
                    string[] time = completeTime.Split(':');
                    if (time.Length == 2)
                    {
                        int hr = stringToInt(time[0]);
                        int minute = stringToInt(time[1]);
                        if (hr < 18)
                        {
                            sDate = sDate.Substring(6, 4) + sDate.Substring(3, 2) + (stringToInt(sDate.Substring(0, 2)) - 1).ToString().PadLeft(2, '0');
                        }
                        else
                        {
                            sDate = sDate.Substring(6, 4) + sDate.Substring(3, 2) + sDate.Substring(0, 2);
                        }
                    }

                    if (transport_type.Trim().ToUpper() == "TRAIN")
                    {
                        sDate = sDate.Substring(6, 2) + "/" + sDate.Substring(4, 2) + "/" + sDate.Substring(0, 4);
                        DateTime tempDate = DateTime.ParseExact(sDate, format, provider);
                        DateTime sDateFrom = DateTime.ParseExact("01/" + tempDate.Month.ToString().PadLeft(2, '0') + "/" + sDate.Substring(6, 4), format, provider);
                        DateTime sDateTo = sDateFrom.AddMonths(1).AddDays(-1);

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo.ToString("yyyyMMdd", provider))
                                          && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom.ToString("yyyyMMdd", provider))
                                          select d;

                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {
                                decimal val = 0;
                                var valList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Transfer" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "B").ToList();
                                decimal num = 0;
                                foreach (var i in valList)
                                {
                                    num = num + 1;
                                    val += (i.T_FXB_VALUE1 ?? 0);
                                }
                                value = (val / num).ToString();
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }
                    else//by VESSEL
                    {
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var tempFx = (from v in context.MKT_TRN_FX_B
                                          where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                                          select v).OrderBy(p => p.T_FXB_VALDATE);

                            var queryFx = from d in tempFx.AsEnumerable()
                                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDate)
                                          select d;

                            var temp = queryFx.ToList();
                            if (temp.Count > 0)
                            {

                                decimal buyingRate = 0;
                                var sellingRateList = temp.Where(p => p.T_FXB_VALTYPE.Trim() == "BOT - Average Buying Rates - Transfer" ||
                                                                      p.T_FXB_VALTYPE.Trim() == "B").ToList();
                                if (sellingRateList.Count > 0)
                                {
                                    buyingRate = sellingRateList.OrderByDescending(p => p.T_FXB_VALDATE).FirstOrDefault().T_FXB_VALUE1 ?? 0;
                                }
                                value = buyingRate.ToString();
                            }
                            else
                            {
                                return "";
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                value = "";
                throw ex;
            }
            return value;
        }
        public List<SelectListItem> PriceCalculate(string benchmark, string met_num, string price_status, string date_from_to, string phet_actual_crude_api_val, ArrayList fixed_Data, string projectName = "PIT")
        {
            List<SelectListItem> model = new List<SelectListItem>();

            try
            {
                if (string.IsNullOrEmpty(benchmark) && string.IsNullOrEmpty(met_num) &&
                    string.IsNullOrEmpty(price_status) && string.IsNullOrEmpty(date_from_to))
                    return model;

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.FORMULA_PRICE
                                 where v.BENCHMARK.Equals(benchmark) &&
                                 v.PRICE_STATUS.Equals(price_status)
                                 select new
                                 {
                                     MET_NUM = v.MET_NUM,
                                     PRICE_STATUS = v.PRICE_STATUS,
                                     DAILY_1 = v.DAILY_1,
                                     DAILY_2 = v.DAILY_2,
                                     DAILY_3 = v.DAILY_3,
                                     DAILY_4 = v.DAILY_4,
                                     DAILY_5 = v.DAILY_5,
                                     DAILY_6 = v.DAILY_6,
                                     DAILY_7 = v.DAILY_7,
                                     MONTHLY_1 = v.MONTHLY_1,
                                     MONTHLY_2 = v.MONTHLY_2,
                                     MONTHLY_3 = v.MONTHLY_3,
                                     MONTHLY_4 = v.MONTHLY_4,
                                     MONTHLY_5 = v.MONTHLY_5,
                                     MONTHLY_6 = v.MONTHLY_6,
                                     MONTHLY_7 = v.MONTHLY_7,
                                     BP_PRICING_PERIOD_FROM = v.BP_PRICING_PERIOD_FROM,
                                     BP_PRICING_PERIOD_TO = v.BP_PRICING_PERIOD_TO,
                                     OSP_PRICING_PERIOD = v.OSP_PRICING_PERIOD,

                                     BASE_PRICE = v.BASE_PRICE,
                                     BASE_PRICE_TEXT = v.BASE_PRICE_TEXT,
                                     OSP_PREMIUM_DISCOUNT = v.OSP_PREMIUM_DISCOUNT,
                                     OSP_PREMIUM_DISCOUNT_TEXT = v.OSP_PREMIUM_DISCOUNT_TEXT,
                                     TRADING_PREMIUM_DISCOUNT = v.TRADING_PREMIUM_DISCOUNT,
                                     TRADING_PREMIUM_DISCOUNT_TEXT = v.TRADING_PREMIUM_DISCOUNT_TEXT,
                                     OTHER_COST = v.OTHER_COST,
                                     OTHER_COST_TEXT = v.OTHER_COST_TEXT,
                                     BASE_PRICE_PERIOD = v.BASE_PRICE_PERIOD,
                                     TRADING_PRICE_PERIOD = v.TRADING_PRICE_PERIOD,
                                     OTHER_PRICE_PERIOD = v.OTHER_PRICE_PERIOD
                                 });
                    Double crude_value = 0;
                    if (!string.IsNullOrEmpty(phet_actual_crude_api_val))
                    {
                        crude_value = Convert.ToDouble(phet_actual_crude_api_val);
                    }
                    if (query.ToList().Count > 0)
                    {
                        FormulaPricingDetail item = new FormulaPricingDetail();
                        var temp = query.ToList();
                        if (met_num.Trim().ToUpper() == "YPAT")
                        {
                            temp = query.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == "YPAT").ToList();
                        }
                        else
                        {
                            temp = query.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() == met_num).ToList();
                            if (temp.Count == 0)
                            {
                                temp = query.Where(p => (p.MET_NUM ?? "").Trim().ToUpper() != "YPAT").ToList();
                            }
                        }

                        if (temp.Count > 0)
                        {
                            item.BASE_PRICE = temp[0].BASE_PRICE;
                            item.BASE_PRICE_TEXT = temp[0].BASE_PRICE_TEXT;
                            item.BP_PRICING_PERIOD_FROM = temp[0].BP_PRICING_PERIOD_FROM;
                            item.BP_PRICING_PERIOD_TO = temp[0].BP_PRICING_PERIOD_TO;
                            item.DAILY_1 = temp[0].DAILY_1;
                            item.DAILY_2 = temp[0].DAILY_2;
                            item.DAILY_3 = temp[0].DAILY_3;
                            item.DAILY_4 = temp[0].DAILY_4;
                            item.DAILY_5 = temp[0].DAILY_5;
                            item.DAILY_6 = temp[0].DAILY_6;
                            item.DAILY_7 = temp[0].DAILY_7;
                            item.MET_NUM = temp[0].MET_NUM;
                            item.MONTHLY_1 = temp[0].MONTHLY_1;
                            item.MONTHLY_2 = temp[0].MONTHLY_2;
                            item.MONTHLY_3 = temp[0].MONTHLY_3;
                            item.MONTHLY_4 = temp[0].MONTHLY_4;
                            item.MONTHLY_5 = temp[0].MONTHLY_5;
                            item.MONTHLY_6 = temp[0].MONTHLY_6;
                            item.MONTHLY_7 = temp[0].MONTHLY_7;
                            item.OSP_PREMIUM_DISCOUNT = temp[0].OSP_PREMIUM_DISCOUNT;
                            item.OSP_PREMIUM_DISCOUNT_TEXT = temp[0].OSP_PREMIUM_DISCOUNT_TEXT;
                            item.OSP_PRICING_PERIOD = temp[0].OSP_PRICING_PERIOD;
                            item.OTHER_COST = temp[0].OTHER_COST;
                            item.OTHER_COST_TEXT = temp[0].OTHER_COST_TEXT;
                            item.PRICE_STATUS = temp[0].PRICE_STATUS;
                            item.TRADING_PREMIUM_DISCOUNT = temp[0].TRADING_PREMIUM_DISCOUNT;
                            item.TRADING_PREMIUM_DISCOUNT_TEXT = temp[0].TRADING_PREMIUM_DISCOUNT_TEXT;
                            item.BASE_PRICE_PERIOD = temp[0].BASE_PRICE_PERIOD;
                            item.TRADING_PRICE_PERIOD = temp[0].TRADING_PRICE_PERIOD;
                            item.OTHER_PRICE_PERIOD = temp[0].OTHER_PRICE_PERIOD;

                            ArrayList fixed_val = new ArrayList();
                            if (string.IsNullOrEmpty(temp[0].DAILY_1) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_1)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }

                            }
                            if (string.IsNullOrEmpty(temp[0].DAILY_2) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_2)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].DAILY_3) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_3)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].DAILY_4) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_4)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].DAILY_5) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_5)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].DAILY_6) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_6)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].DAILY_7) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].DAILY_7)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_1) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_1)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_2) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_2)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_3) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_3)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_4) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_4)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_5) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_5)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_6) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_6)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            if (string.IsNullOrEmpty(temp[0].MONTHLY_7) || fixed_Data == null)
                            {
                                fixed_val.Add("");
                            }
                            else
                            {
                                int cInt = 0;
                                foreach (string[] v in fixed_Data)
                                {
                                    if (v.Length == 2)
                                    {
                                        if (v[0].ToString() == temp[0].MONTHLY_7)
                                        {
                                            fixed_val.Add(v[1].ToString());
                                            cInt++;
                                        }
                                    }
                                }
                                if (cInt == 0)
                                {
                                    fixed_val.Add("");
                                }
                            }
                            //----------------------------- Calculate BASE_PRICE
                            if (!string.IsNullOrEmpty(item.BASE_PRICE))// && !string.IsNullOrEmpty(item.BASE_PRICE_TEXT))
                            {
                                string adj_date_from_to = "";
                                SelectListItem i = new SelectListItem();
                                i.Text = "BASE_PRICE";
                                //i.Value = getFormulaValue(item, item.BASE_PRICE.ToString(), date_from_to, context, crude_value, fixed_val, "").ToString();
                                var resultVal = getFormulaValue(item, item.BASE_PRICE.ToString(), date_from_to, context, crude_value, fixed_val, "", ref adj_date_from_to);
                                //กรณีเป็นบงกช และไม่มาจาก PCF
                                if (projectName == "PIT" && item.MET_NUM == "YBKC")
                                {
                                    double bongkot = 0;
                                    double number;
                                    if (double.TryParse(item.TRADING_PREMIUM_DISCOUNT_TEXT, out number))
                                    {
                                        bongkot += number;
                                    }
                                    resultVal += bongkot;
                                }
                                i.Value = resultVal.ToString();
                                model.Add(i);
                            }
                            else
                            {
                                SelectListItem i = new SelectListItem();
                                i.Text = "BASE_PRICE";
                                i.Value = "0.00";
                                model.Add(i);
                            }

                            //----------------------------- Calculate OSP_PREMIUM_DISCOUNT
                            if (!string.IsNullOrEmpty(item.OSP_PREMIUM_DISCOUNT))// && !string.IsNullOrEmpty(item.OSP_PREMIUM_DISCOUNT_TEXT))
                            {
                                string adj_date_from_to = "";
                                SelectListItem i = new SelectListItem();
                                i.Text = "OSP_PREMIUM_DISCOUNT";
                                i.Value = getFormulaValue(item, item.OSP_PREMIUM_DISCOUNT.ToString(), date_from_to, context, crude_value, fixed_val, item.OSP_PRICING_PERIOD, ref adj_date_from_to).ToString();
                                model.Add(i);

                                if (adj_date_from_to != "") {
                                    string[] arrdate = adj_date_from_to.Replace(" to ", "|").Split('|');
                                    if (arrdate.Length == 2)
                                    {
                                        DateTime dFromDate = DateTime.ParseExact(arrdate[0], format, provider);
                                        SelectListItem it = new SelectListItem();
                                        it.Text = "OSP_MONTH_PRICE";
                                        it.Value = dFromDate.ToString("MMM/yyyy", provider);
                                        model.Add(it);                                        
                                    }                                    
                                }

                            }
                            else
                            {
                                SelectListItem i = new SelectListItem();
                                i.Text = "OSP_PREMIUM_DISCOUNT";
                                i.Value = "0.00";
                                model.Add(i);

                                SelectListItem it = new SelectListItem();
                                it.Text = "OSP_MONTH_PRICE";
                                it.Value = "";
                                model.Add(it);
                            }

                            //----------------------------- Calculate TRADING_PREMIUM_DISCOUNT

                            if (!string.IsNullOrEmpty(item.TRADING_PREMIUM_DISCOUNT))// && !string.IsNullOrEmpty(item.TRADING_PREMIUM_DISCOUNT_TEXT))
                            {
                                string adj_date_from_to = "";
                                SelectListItem i = new SelectListItem();
                                i.Text = "TRADING_PREMIUM_DISCOUNT";
                                i.Value = getFormulaValue(item, item.TRADING_PREMIUM_DISCOUNT.ToString(), date_from_to, context, crude_value, fixed_val, projectName, ref adj_date_from_to).ToString();
                                
                                model.Add(i);
                            }
                            else
                            {
                                SelectListItem i = new SelectListItem();
                                i.Text = "TRADING_PREMIUM_DISCOUNT";
                                i.Value = "0.00";
                                model.Add(i);
                            }
                            //----------------------------- Calculate OTHER_COST_TEXT

                            if (!string.IsNullOrEmpty(item.OTHER_COST))//&& !string.IsNullOrEmpty(item.OTHER_COST_TEXT))
                            {
                                string adj_date_from_to = "";
                                SelectListItem i = new SelectListItem();
                                i.Text = "OTHER_COST";
                                i.Value = getFormulaValue(item, item.OTHER_COST.ToString(), date_from_to, context, crude_value, fixed_val, "", ref adj_date_from_to).ToString();
                                model.Add(i);
                            }
                            else
                            {
                                SelectListItem i = new SelectListItem();
                                i.Text = "OTHER_COST";
                                i.Value = "0.00";
                                model.Add(i);
                            }
                        }

                    }
                    else
                    {
                        SelectListItem item = new SelectListItem();
                        item.Text = "BASE_PRICE";
                        item.Value = "0.00";
                        model.Add(item);

                        item = new SelectListItem();
                        item.Text = "OSP_PREMIUM_DISCOUNT";
                        item.Value = "0.00";
                        model.Add(item);

                        item = new SelectListItem();
                        item.Text = "TRADING_PREMIUM_DISCOUNT";
                        item.Value = "0.00";
                        model.Add(item);

                        item = new SelectListItem();
                        item.Text = "OTHER_COST";
                        item.Value = "0.00";
                        model.Add(item);
                    }

                }

            }
            catch (Exception ex)
            {
                SelectListItem item = new SelectListItem();
                item.Text = "BASE_PRICE";
                item.Value = "0.00";
                model.Add(item);

                item = new SelectListItem();
                item.Text = "OSP_PREMIUM_DISCOUNT";
                item.Value = "0.00";
                model.Add(item);

                item = new SelectListItem();
                item.Text = "TRADING_PREMIUM_DISCOUNT";
                item.Value = "0.00";
                model.Add(item);

                item = new SelectListItem();
                item.Text = "OTHER_COST";
                item.Value = "0.00";
                model.Add(item);

            }

            return model;
        }


        public Double getFormulaValue(FormulaPricingDetail item, string txtFormular, string date_from_to, EntityCPAIEngine context, double other_value, ArrayList fixed_val, string typePeriod, ref string adj_date_from_to)
        {
            Double returnVal = 0;
            try
            {
                List<FormulaPricingCondition> conItemList = new List<FormulaPricingCondition>();

                FormulaConditionItemList conItem = new FormulaConditionItemList();
                conItem = (FormulaConditionItemList)Newtonsoft.Json.JsonConvert.DeserializeObject(txtFormular, typeof(FormulaConditionItemList));
                conItemList = conItem.ConditionItem;
                conItem.result = new List<ResultTemp>();
                if (conItemList != null)
                {
                    ///Case backward period date from OSP PRICING_PERIOD 
                    ///ทำเพื่อตรวจสอบ ว่า product ทุกตัวในสูตร มีราคาครบตรงกันทั้งเดือน ถ้าไม่ครบก็จะลดไปทีละเดือน
                    //OSP ราคาจะมีแค่เดือนละวัน
                    #region Case When OSP Have Date Backward
                    if (!string.IsNullOrEmpty(typePeriod))
                    {
                        var pDateOSP = date_from_to;
                        var sDateFrom = String.IsNullOrEmpty(pDateOSP) ? "" : "01" + pDateOSP.Substring(2, 8);
                        DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                        DateTime dateTo = dateFrom.AddMonths(1).AddDays(-1);
                        var IsBackWardPeriod = false;
                        do
                        {
                            IsBackWardPeriod = false;
                            DateTime dateTempStart = DateTime.ParseExact(sDateFrom, format, provider);
                            if ((dateTempStart - dateFrom).TotalDays > 365)
                            {
                                IsBackWardPeriod = true;
                            }
                            foreach (FormulaPricingCondition i in conItemList)
                            {
                                string date = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);
                                if (i.Variable1 == "DAILY_1" || i.Variable1 == "DAILY_2" || i.Variable1 == "DAILY_3" || i.Variable1 == "DAILY_4" || i.Variable1 == "DAILY_5" || i.Variable1 == "DAILY_6" || i.Variable1 == "DAILY_7" || i.Variable1 == "MONTHLY_1" || i.Variable1 == "MONTHLY_2" || i.Variable1 == "MONTHLY_3" || i.Variable1 == "MONTHLY_4" || i.Variable1 == "MONTHLY_5" || i.Variable1 == "MONTHLY_6" || i.Variable1 == "MONTHLY_7")
                                {
                                    int rowCount = 0;
                                    var val1 = getVariable(i.Variable1, item, conItem, date, context, other_value, fixed_val, ref rowCount);
                                    if (val1 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                }

                                if (i.Variable2 == "DAILY_1" || i.Variable2 == "DAILY_2" || i.Variable2 == "DAILY_3" || i.Variable2 == "DAILY_4" || i.Variable2 == "DAILY_5" || i.Variable2 == "DAILY_6" || i.Variable2 == "DAILY_7" || i.Variable2 == "MONTHLY_1" || i.Variable2 == "MONTHLY_2" || i.Variable2 == "MONTHLY_3" || i.Variable2 == "MONTHLY_4" || i.Variable2 == "MONTHLY_5" || i.Variable2 == "MONTHLY_6" || i.Variable2 == "MONTHLY_7")
                                {
                                    int rowCount = 0;
                                    var val2 = getVariable(i.Variable2, item, conItem, date, context, other_value, fixed_val, ref rowCount);
                                    if (val2 <= 0 && rowCount == 0) { IsBackWardPeriod = true; }
                                }
                            }
                            if (IsBackWardPeriod)
                            {
                                DateTime sDate = DateTime.ParseExact("01" + dateFrom.ToString("/MM/yyyy", provider), format, provider);
                                dateFrom = sDate.AddMonths(-1);
                                dateTo = dateFrom.AddMonths(1).AddDays(-1);
                            }
                            else
                            {
                                date_from_to = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);
                            }
                        } while (IsBackWardPeriod);

                        //pDateOSP = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);
                        //date_from_to = getPeriodBackward(date_from_to, IsBackWardPeriod, typePeriod);

                    }
                    #endregion

                    adj_date_from_to = date_from_to;

                    foreach (FormulaPricingCondition i in conItemList)
                    {
                        double valTemp = 0;
                        int rowCount = 0;
                        bool IsBackWardPeriod = false;
                        if (i.Symbol != "")
                        {
                            var val1 = getVariable(i.Variable1, item, conItem, date_from_to, context, other_value, fixed_val, ref rowCount);
                            if (!string.IsNullOrEmpty(i.Round1))
                            {
                                if (i.Round1.Trim() != "")
                                {
                                    val1 = (Double)Math.Round((decimal)val1, stringToInt(i.Round1), MidpointRounding.AwayFromZero);
                                }
                            }
                            var val2 = getVariable(i.Variable2, item, conItem, date_from_to, context, other_value, fixed_val, ref rowCount);
                            #region Bongkot Backward date
                            //if (item.MET_NUM == "YBKC" && val2 == 0)// Date Backward
                            //{
                            //    var pDateOSP = date_from_to;
                            //    var sDateFrom = String.IsNullOrEmpty(pDateOSP) ? "" : "01" + pDateOSP.Substring(2, 8);
                            //    DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider).AddMonths(-1);
                            //    DateTime dateTo = dateFrom.AddMonths(1).AddDays(-1);
                            //    do
                            //    {
                            //        IsBackWardPeriod = false;
                            //        DateTime dateTempStart = DateTime.ParseExact(sDateFrom, format, provider);
                            //        if ((dateTempStart - dateFrom).TotalDays > 365)
                            //        {
                            //            IsBackWardPeriod = true;
                            //        }

                            //        string date = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);
                            //        if (i.Variable2 == "DAILY_1" || i.Variable2 == "DAILY_2" || i.Variable2 == "DAILY_3" || i.Variable2 == "DAILY_4" || i.Variable2 == "DAILY_5" || i.Variable2 == "DAILY_6" || i.Variable2 == "DAILY_7" || i.Variable2 == "MONTHLY_1" || i.Variable2 == "MONTHLY_2" || i.Variable2 == "MONTHLY_3" || i.Variable2 == "MONTHLY_4" || i.Variable2 == "MONTHLY_5" || i.Variable2 == "MONTHLY_6" || i.Variable2 == "MONTHLY_7")
                            //        {
                            //            val2 = getVariable(i.Variable2, item, conItem, date, context, other_value, fixed_val);
                            //            if (val2 <= 0) { IsBackWardPeriod = true; }
                            //        }

                            //        if (IsBackWardPeriod)
                            //        {
                            //            dateFrom = dateFrom.AddMonths(-1);
                            //            dateTo = dateFrom.AddMonths(1).AddDays(-1);
                            //        }

                            //    } while (IsBackWardPeriod);
                            //}
                            #endregion
                            if (!string.IsNullOrEmpty(i.Round2))
                            {
                                if (i.Round2.Trim() != "")
                                {
                                    val2 = (Double)Math.Round((decimal)val2, stringToInt(i.Round2), MidpointRounding.AwayFromZero);
                                }
                            }
                            if (i.Symbol.Contains("TIER"))
                            {
                                valTemp = getTierValue(val1, item.MET_NUM, item.PRICE_STATUS, val2, conItem, context);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(i.Symbol) && val2 != 0)
                                {
                                    valTemp = symbolCalculate(val1, i.Symbol, val2);
                                }
                                else
                                {
                                    valTemp = val1;
                                }
                            }

                            //Rounding ผลลัพธ์
                            if (!string.IsNullOrEmpty(i.ResultOut_Round))
                            {
                                if (i.ResultOut_Round.Trim() != "")
                                {
                                    valTemp = (Double)Math.Round((decimal)valTemp, stringToInt(i.ResultOut_Round), MidpointRounding.AwayFromZero);
                                }
                            }
                            //กรณีระบุ set ค่า ให้ ตัวแปรหลัก
                            if (string.IsNullOrEmpty(i.ResultOut) || i.ResultOut.Trim().ToUpper() == "MAIN")
                            {
                                conItem.MainValue = valTemp;
                            }
                            else//กรณีระบุ set ค่า ให้ ตัวแปรอื่นๆ
                            {
                                var result = conItem.result.Where(p => p.Name.Trim() == i.ResultOut.Trim());
                                if (result.ToList().Count > 0)
                                {
                                    result.ToList().FirstOrDefault().Value = valTemp;
                                }
                                else
                                {
                                    ResultTemp r = new ResultTemp();
                                    r.Name = i.ResultOut.Trim();
                                    r.Value = valTemp;
                                    conItem.result.Add(r);
                                }
                            }
                        }
                        else
                        {

                            valTemp = getVariable(i.Variable1, item, conItem, date_from_to, context, other_value, fixed_val, ref rowCount);
                            //Rounding ผลลัพธ์
                            if (!string.IsNullOrEmpty(i.Round1))
                            {
                                if (i.Round1.Trim() != "")
                                {
                                    valTemp = (Double)Math.Round((decimal)valTemp, stringToInt(i.Round1), MidpointRounding.AwayFromZero);
                                }
                            }
                            //กรณีระบุ set ค่า ให้ ตัวแปรหลัก
                            if (string.IsNullOrEmpty(i.ResultOut) || i.ResultOut.Trim().ToUpper() == "MAIN")
                            {
                                conItem.MainValue = valTemp;
                            }
                            else//กรณีระบุ set ค่า ให้ ตัวแปรอื่นๆ
                            {
                                var result = conItem.result.Where(p => p.Name.Trim() == i.ResultOut.Trim());
                                if (result.ToList().Count > 0)
                                {
                                    result.ToList().FirstOrDefault().Value = valTemp;
                                }
                                else
                                {
                                    ResultTemp r = new ResultTemp();
                                    r.Name = i.ResultOut.Trim();
                                    r.Value = valTemp;
                                    conItem.result.Add(r);
                                }
                            }
                        }
                    }
                    returnVal = conItem.MainValue;
                }
            }
            catch (Exception ex)
            {
                returnVal = 0;
                throw ex;
            }

            return returnVal;
        }

        public string getPeriodBackward(string date_from_to, bool IsBackWardPeriod, string typePeriod)
        {
            string pDateOSP = date_from_to;
            if (IsBackWardPeriod)
            {
                if (typePeriod.Split('-').Length == 2)
                {
                    var sDateFrom = String.IsNullOrEmpty(pDateOSP) ? "" : "01" + pDateOSP.Substring(2, 8);
                    //var sDateTo = String.IsNullOrEmpty(pDateOSP) ? "" : pDateOSP.Substring(14, 10);
                    DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dateTo = dateFrom.AddMonths(1).AddDays(-1);

                    string months = typePeriod.Split('-')[1];
                    int number;
                    if (int.TryParse(months, out number))
                    {
                        dateFrom = dateFrom.AddMonths(-(number));
                        dateTo = dateTo.AddMonths(-(number));

                        pDateOSP = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);
                    }


                }
                else if (typePeriod.Split('+').Length == 2)
                {
                    var sDateFrom = String.IsNullOrEmpty(pDateOSP) ? "" : "01" + pDateOSP.Substring(2, 8);
                    //var sDateTo = String.IsNullOrEmpty(pDateOSP) ? "" : pDateOSP.Substring(14, 10);
                    DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dateTo = dateFrom.AddMonths(1).AddDays(-1);

                    string months = typePeriod.Split('+')[1];
                    int number;
                    if (int.TryParse(months, out number))
                    {
                        dateFrom = dateFrom.AddMonths(number);
                        dateTo = dateTo.AddMonths(number);

                        pDateOSP = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);
                    }

                }
                //else
                //{
                //    var sDateFrom = String.IsNullOrEmpty(pDateOSP) ? "" : "01" + pDateOSP.Substring(2, 8);
                //    //var sDateTo = String.IsNullOrEmpty(pDateOSP) ? "" : pDateOSP.Substring(14, 10);
                //    DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                //    DateTime dateTo = dateFrom.AddMonths(1).AddDays(-1);

                //    pDateOSP = dateFrom.ToString(format, provider) + " to " + dateTo.ToString(format, provider);

                //}
            }
            return pDateOSP;
        }
        public Double getVariable(string valName, FormulaPricingDetail item, FormulaConditionItemList tempResult, string date_from_to, EntityCPAIEngine context, double other_value, ArrayList fixed_val, ref int rowCount)
        {
            Double result = 0;
            try
            {
                switch (valName.Trim().ToUpper())
                {
                    case "DAILY_1":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[0]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[0], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_1, date_from_to, context, ref rowCount);//getPeriodBackward(date_from_to, item.DAILY_1, calculateType,context), context);
                            }
                            else
                                result = avgDaily(item.DAILY_1, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_1, date_from_to, context, ref rowCount);
                        break;
                    case "DAILY_2":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[1]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[1], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_2, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgDaily(item.DAILY_2, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_2, date_from_to, context, ref rowCount);
                        break;
                    case "DAILY_3":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[2]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[2], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_3, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgDaily(item.DAILY_3, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_3, date_from_to, context, ref rowCount);
                        break;
                    case "DAILY_4":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[3]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[3], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_4, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgDaily(item.DAILY_4, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_4, date_from_to, context, ref rowCount);
                        break;
                    case "DAILY_5":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[4]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[4], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_5, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgDaily(item.DAILY_5, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_5, date_from_to, context, ref rowCount);
                        break;
                    case "DAILY_6":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[5]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[5], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_6, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgDaily(item.DAILY_6, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_6, date_from_to, context, ref rowCount);
                        break;
                    case "DAILY_7":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[6]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[6], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgDaily(item.DAILY_7, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgDaily(item.DAILY_7, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgDaily(item.DAILY_7, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_1":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[7]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[7], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_1, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_1, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_1, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_2":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[8]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[8], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_2, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_2, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_2, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_3":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[9]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[9], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_3, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_3, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_3, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_4":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[10]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[10], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_4, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_4, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_4, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_5":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[11]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[11], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_5, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_5, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_5, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_6":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[12]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[12], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_6, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_6, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_6, date_from_to, context, ref rowCount);
                        break;
                    case "MONTHLY_7":
                        if (fixed_val != null)
                        {
                            if (fixed_val.Count == 14 && !string.IsNullOrEmpty((string)fixed_val[13]))
                            {
                                double number;
                                if (Double.TryParse((string)fixed_val[13], out number))
                                {
                                    result = number;
                                }
                                else
                                    result = avgMonthly(item.MONTHLY_7, date_from_to, context, ref rowCount);
                            }
                            else
                                result = avgMonthly(item.MONTHLY_7, date_from_to, context, ref rowCount);
                        }
                        else
                            result = avgMonthly(item.MONTHLY_7, date_from_to, context, ref rowCount);
                        break;
                    case "OTHER_VALUE":
                        result = other_value;
                        break;
                    case "MAIN":
                        result = tempResult.MainValue;
                        break;
                    default:
                        var temp = tempResult.result.Where(p => p.Name.Trim() == valName.Trim());
                        if (temp.ToList().Count > 0)//ตัวแปรอื่นที่ทำเป็น Temp
                        {
                            result = temp.ToList().FirstOrDefault().Value;
                        }
                        else
                        {
                            //กรณีเป็นการส่งตัวเลข
                            double number;
                            if (Double.TryParse(valName, out number))
                            {
                                result = number;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                result = 0;
                throw ex;
            }

            ////กรณีเป็นบงกช และไม่มาจาก PCF
            //if(calculateType == "PIT" && item.MET_NUM == "YBKC")
            //{
            //    double bongkot = 0;
            //    double number;
            //    if (double.TryParse(item.TRADING_PREMIUM_DISCOUNT_TEXT, out number))
            //    {
            //        bongkot += number;
            //    }
            //    result += bongkot;
            //}

            return result;
        }

        public Double symbolCalculate(double input1, string symbol, double input2)
        {
            Double returnVal = 0;
            try
            {
                switch (symbol.Trim().ToUpper())
                {
                    case "+":
                        returnVal = input1 + input2;
                        break;
                    case "-":
                        returnVal = input1 - input2;
                        break;
                    case "*":
                        returnVal = input1 * input2;
                        break;
                    case "/":
                        returnVal = input1 / input2;
                        break;
                    case "MIN":
                        if (input1 > input2)
                        {
                            returnVal = input2;
                        }
                        else
                        {
                            returnVal = input1;
                        }
                        break;
                    case "MAX":
                        if (input1 > input2)
                        {
                            returnVal = input1;
                        }
                        else
                        {
                            returnVal = input2;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                returnVal = 0;
                throw ex;
            }
            return returnVal;
        }

        public Double getTierValue(Double input, string met_num, string price_status, Double tier_value, FormulaConditionItemList conItem, EntityCPAIEngine context)
        {
            Double returnVal = 0;
            try
            {
                FormulaConditionItemList conditionList = new FormulaConditionItemList();
                conditionList.TierItem = new List<FormulaPricingTier>();
                Decimal? nValue = Convert.ToDecimal(tier_value);
                var tier = context.FORMULA_TIER.Where(p => p.MET_NUM == met_num && p.PRICE_STATUS == price_status && p.FOT_VALUE == nValue);
                if (tier != null)
                {
                    if (tier.ToList().Count > 0)//Convert.ToDouble(p.FOT_VALUE ?? 0) == Double.Parse(tier_value.ToString())
                    {
                        conditionList = (FormulaConditionItemList)Newtonsoft.Json.JsonConvert.DeserializeObject(tier.ToList().FirstOrDefault().FOT_FORMULA.ToString(), typeof(FormulaConditionItemList));
                        foreach (FormulaPricingTier i in conditionList.TierItem)
                        {
                            int rowCount = 0;
                            bool trueCondition = true;
                            if (i.Compare_with.Count > 0)
                            {
                                Double vCompare = input;
                                if (!string.IsNullOrEmpty(i.Compare_name) && i.Compare_name != "INPUT")
                                {
                                    vCompare = getVariable(i.Compare_name, new FormulaPricingDetail(), conItem, "", context, 0, new ArrayList(),ref rowCount);
                                }
                                //ตรวจสอบตามเงื่อนไข
                                foreach (TierCondition j in i.Compare_with)
                                {
                                    switch (j.Compare_Symbol.Trim())
                                    {
                                        case "<":
                                            if (vCompare < stringToDouble(j.Compare_Value.Trim())) { }
                                            else { trueCondition = false; }
                                            break;
                                        case "<=":
                                            if (vCompare <= stringToDouble(j.Compare_Value.Trim())) { }
                                            else { trueCondition = false; }
                                            break;
                                        case ">":
                                            if (vCompare > stringToDouble(j.Compare_Value.Trim())) { }
                                            else { trueCondition = false; }
                                            break;
                                        case ">=":
                                            if (vCompare >= stringToDouble(j.Compare_Value.Trim())) { }
                                            else { trueCondition = false; }
                                            break;
                                        default:
                                            trueCondition = false;
                                            break;
                                    }
                                }

                                //กรณีเข้าเงื่อนไข
                                if (trueCondition)
                                {
                                    if (!string.IsNullOrEmpty(i.Return_Value))
                                    {
                                        returnVal = stringToDouble(i.Return_Value.Trim());
                                        return returnVal;
                                    }
                                    else if (i.ConditionItem != null)
                                    {
                                        if (i.ConditionItem.Count > 0)
                                        {
                                            double valTemp = input;
                                            foreach (FormulaPricingCondition k in i.ConditionItem)
                                            {
                                                if (k.Symbol != "")
                                                {
                                                    double val1 = 0;
                                                    if (string.IsNullOrEmpty(k.Variable1) || k.Variable1.Trim().ToUpper() == "INPUT")
                                                    {
                                                        val1 = valTemp;
                                                    }
                                                    else
                                                    {
                                                        val1 = getVariable(k.Variable1, new FormulaPricingDetail(), conItem, "", context, 0, new ArrayList(), ref rowCount);
                                                    }
                                                    if (!string.IsNullOrEmpty(k.Round1))
                                                    {
                                                        if (k.Round1.Trim() != "")
                                                        {
                                                            val1 = (Double)Math.Round((decimal)val1, stringToInt(k.Round1), MidpointRounding.AwayFromZero);
                                                        }
                                                    }

                                                    double val2 = 0;
                                                    if (string.IsNullOrEmpty(k.Variable2) || k.Variable2.Trim().ToUpper() == "INPUT")
                                                    {
                                                        val2 = valTemp;
                                                    }
                                                    else
                                                    {
                                                        val2 = getVariable(k.Variable2, new FormulaPricingDetail(), conItem, "", context, 0, new ArrayList(), ref rowCount);
                                                    }
                                                    if (!string.IsNullOrEmpty(k.Round2))
                                                    {
                                                        if (k.Round2.Trim() != "")
                                                        {
                                                            val2 = (Double)Math.Round((decimal)val2, stringToInt(k.Round2), MidpointRounding.AwayFromZero);
                                                        }
                                                    }

                                                    valTemp = symbolCalculate(val1, k.Symbol, val2);
                                                    valTemp = (Double)Math.Round((decimal)valTemp, 14, MidpointRounding.AwayFromZero);
                                                    if (!string.IsNullOrEmpty(k.ResultOut_Round))
                                                    {
                                                        if (k.ResultOut_Round.Trim() != "")
                                                        {
                                                            valTemp = (Double)Math.Round((decimal)valTemp, stringToInt(k.ResultOut_Round), MidpointRounding.AwayFromZero);
                                                        }
                                                    }
                                                }
                                            }
                                            return valTemp;
                                        }

                                    }

                                }
                                else
                                {
                                    trueCondition = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnVal = 0;
                throw ex;
            }
            return returnVal;
        }

        public Double stringToDouble(string value)
        {
            double temp_value = 0;
            double number;
            if (Double.TryParse(value, out number))
            {
                temp_value = number;
            }
            return temp_value;
        }

        public Int32 stringToInt(string value)
        {
            Int32 temp_value = 0;
            Int32 number;
            if (Int32.TryParse(value, out number))
            {
                temp_value = number;
            }
            return temp_value;
        }


        public Double avgDaily(string prdCode, string pdate, EntityCPAIEngine context,ref int rowCount)
        {
            Double calTemp = 0;
            try
            {
                var sDateFrom = String.IsNullOrEmpty(pdate) ? "" : pdate.Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(pdate) ? "" : pdate.Substring(14, 10);
                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                var queryTemp = (from v in context.MKT_TRN_MOPS_B
                                 where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                                 && prdCode.Contains(v.T_MPB_PRDCODE)
                                 select v).OrderBy(p => p.T_MPB_VALDATE);

                var query = from d in queryTemp.AsEnumerable()
                            where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                            && Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                            select d;

                rowCount = query.ToList().Count;
                if (query.ToList().Count > 0)
                {
                    int count = 0;
                    double val = 0;
                    foreach (var i in query.ToList())
                    {
                        count++;
                        val += (Double)(i.T_MPB_VALUE ?? Convert.ToDecimal("0"));
                    }
                    calTemp = (val / count);
                }
            }
            catch (Exception ex)
            {
                calTemp = 0;
                throw ex;
            }

            return calTemp;
        }

        public Double avgMonthly(string prdCode, string pdate, EntityCPAIEngine context, ref int rowCount)
        {
            Double calTemp = 0;
            try
            {
                var sDateFrom = String.IsNullOrEmpty(pdate) ? "" : pdate.Substring(0, 10);
                var sDateTo = String.IsNullOrEmpty(pdate) ? "" : pdate.Substring(14, 10);
                DateTime dateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                DateTime dateTo = DateTime.ParseExact(sDateTo, format, provider);

                sDateFrom = sDateFrom.Substring(6, 4) + sDateFrom.Substring(3, 2) + sDateFrom.Substring(0, 2);
                sDateTo = sDateTo.Substring(6, 4) + sDateTo.Substring(3, 2) + sDateTo.Substring(0, 2);

                var queryTemp = (from v in context.MKT_TRN_MOPS_B
                                 where v.T_MPB_VALTYPE == "M" && v.T_MPB_ENABLED == "T"
                                 && prdCode.Contains(v.T_MPB_PRDCODE)
                                 select v).OrderBy(p => p.T_MPB_VALDATE);

                var query = from d in queryTemp.AsEnumerable()
                            where Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) >= Int32.Parse(sDateFrom)
                            && Convert.ToInt32(string.IsNullOrEmpty(d.T_MPB_VALDATE) ? "0" : d.T_MPB_VALDATE) <= Int32.Parse(sDateTo)
                            select d;

                rowCount = query.ToList().Count;
                if (query.ToList().Count > 0)
                {
                    int count = 0;
                    double val = 0;
                    foreach (var i in query.ToList())
                    {
                        count++;
                        val += (Double)(i.T_MPB_VALUE ?? Convert.ToDecimal("0"));
                    }
                    calTemp = (val / count);
                }
            }
            catch (Exception ex)
            {
                calTemp = 0;
                throw ex;
            }

            return calTemp;
        }

        public List<CharteringOutbound> getChartering(string sqlCondi)
        {
            List<CharteringOutbound> objRet = new List<CharteringOutbound>();
            try
            {
                string sql = "";
                  
                sql = " select IDA_P_FK_VESSEL_NAME VesselCode ";
                sql += " , VEH_VEH_TEXT VesselName ";
                sql += " , IIR_ROUND_TYPE RoundType ";
                sql += " , VAL RoundVol ";
                sql += " , IDA_P_LAYTIME LayTime ";
                sql += " , IDA_P_FLAT_RATE FlatRate ";
                sql += " , ida_freight FreightType ";
                sql += " , IDA_P_EST_TOTAL Total ";
                sql += " from VW_CHARTERING t ";
                sql += " where " + sqlCondi;

                EntityCPAIEngine context = new EntityCPAIEngine();
                objRet = context.Database.SqlQuery<CharteringOutbound>(sql).ToList();

            }
            catch (Exception ex)
            {

            }

            return objRet;
        }

        public List<CharteringOutbound> searchChartering(string sqlCondi)
        {
            List<CharteringOutbound> objRet = new List<CharteringOutbound>();
            try
            {
                string sql = "";

                sql = " select distinct IDA_P_FK_VESSEL_NAME VesselCode ";
                sql += " , VEH_VEH_TEXT VesselName ";
                sql += " , vnd_name1 VendorName ";
                sql += " , ida_purchase_no PurchaseNo ";
                sql += " , ida_freight FreightType ";
                sql += " from VW_CHARTERING t ";
                sql += " where " + sqlCondi;

                EntityCPAIEngine context = new EntityCPAIEngine();
                objRet = context.Database.SqlQuery<CharteringOutbound>(sql).ToList();

            }
            catch (Exception ex)
            {

            }

            return objRet;
        }


    }
}