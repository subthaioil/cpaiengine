﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using ProjectCPAIEngine.DAL.DALPCF;
using com.pttict.engine.utility;
using OfficeOpenXml;
using System.Data;
using System.IO;
using com.pttict.sap.Interface.sap.accrual.post;
using com.pttict.sap.Interface.Service;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class GIClearLineServiceModel
    {
        CultureInfo provider = new CultureInfo("en-US");
        string format = "dd/MM/yyyy";
        public void Search(ref GIClearLineViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                #region "For Use"
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sType = pModel.GIClearLine_Search.sType == null ? "" : pModel.GIClearLine_Search.sType;
                    var sTripNo = pModel.GIClearLine_Search.sTripNo == null ? "" : pModel.GIClearLine_Search.sTripNo.ToUpper();
                    var sDeliveryDate = pModel.GIClearLine_Search.sDeliveryDate;
                    var sDateFrom = String.IsNullOrEmpty(pModel.GIClearLine_Search.sDeliveryDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy") : pModel.GIClearLine_Search.sDeliveryDate.Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.GIClearLine_Search.sDeliveryDate) ? DateTime.Now.ToString("dd/MM/yyyy") : pModel.GIClearLine_Search.sDeliveryDate.Substring(14, 10);

                    DateTime dDateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dDateTo = DateTime.ParseExact(sDateTo, format, provider);
                    //pModel.GIClearLine_Search = new List<GIClearLineViewModel_SearchData>();
                    #region "sType = GI CLEAR LINE"

                    if (sType == "GI Clear Line")

                    {
                        var query = (from cl in context.CLEAR_LINE_CRUDE
                                     join clp in (
                                         from order in context.CLEAR_LINE_PRICE
                                         group order by order.CLP_TRIP_NO into g
                                         select new
                                         {
                                             CLP_TRIP_NO = g.Key,
                                             maxCLP_NUM = g.Max(order => order.CLP_NUM)

                                         }) on cl.CLC_TRIP_NO equals clp.CLP_TRIP_NO
                                     join clp_max in context.CLEAR_LINE_PRICE on new { x1 = clp.CLP_TRIP_NO, x2 = clp.maxCLP_NUM } equals new { x1 = clp_max.CLP_TRIP_NO, x2 = clp_max.CLP_NUM }
                                     join mat in context.MT_MATERIALS on cl.CLC_CRUDE_TYPE_ID equals mat.MET_NUM
                                     join veh in context.MT_VEHICLE on cl.CLC_VESSEL_ID equals veh.VEH_ID
                                     join cus in context.MT_CUST_DETAIL on new { c1 = cl.CLC_CUST_NUM, c2 = cl.CLC_COMPANY } equals new { c1 = cus.MCD_FK_CUS, c2 = cus.MCD_FK_COMPANY }
                                     where ((cl.CLC_DELIVERY_DATE >= dDateFrom && cl.CLC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                      && ((cl.CLC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                      && (cus.MCD_NATION == "I") && (cl.CLC_PROCESS_TYPE == "C")
                                     select new
                                     {
                                         cl,
                                         clp_max,
                                         MatName = mat.MET_MAT_DES_ENGLISH,
                                         CustomerName = cus.MCD_NAME_1
                                     });



                        if (query != null)
                        {
                            if (pModel.GIClearLine_Search.SearchData != null)
                            {
                                pModel.GIClearLine_Search.SearchData.Clear();
                            }
                            else if (pModel.GIClearLine_Search.SearchData == null) { pModel.GIClearLine_Search.SearchData = new List<GIClearLineViewModel_SearchData>(); }

                            foreach (var item in query.ToList())
                            {
                                pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                                {

                                    dType = "GI Clear Line",
                                    dTripNo = item.cl.CLC_TRIP_NO,
                                    dDONo = (item.cl.CLC_DO_NO ?? "  "),
                                    dDeliveryDate = Convert.ToDateTime(item.cl.CLC_DELIVERY_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                    dPlant = item.cl.CLC_PLANT ?? "  ",
                                    dProduct = item.CustomerName ?? "  ",
                                    dVolumeBBL = (item.cl.CLC_VOLUME_BBL ?? Convert.ToDecimal(item.cl.CLC_VOLUME_BBL)).ToString("#,##0.00"),
                                    dVolumeMT = (item.cl.CLC_VOLUME_MT ?? Convert.ToDecimal(item.cl.CLC_VOLUME_MT)).ToString("#,##0.00"),
                                    dVolumeLitres = (item.cl.CLC_VOLUME_LITE ?? Convert.ToDecimal(item.cl.CLC_VOLUME_LITE)).ToString("#,##0.00"),
                                    dPrice = (item.clp_max.CLP_PRICE ?? Convert.ToDecimal(item.clp_max.CLP_PRICE)).ToString("#,##0.00"),
                                    dAmount = (item.cl.CLC_TOTAL ?? Convert.ToDecimal(item.cl.CLC_TOTAL)).ToString("#,##0.00"),
                                    dUS4 = (item.cl.CLC_UNIT_TOTAL ?? " "),
                                    dStorageLoc = item.cl.CLC_STORAGE_LOCATION,
                                    dTemp = item.cl.CLC_TEMP.ToString(),
                                    dDensity = item.cl.CLC_DENSITY.ToString(),
                                    dSaleOrder = item.cl.CLC_SALEORDER,
                                    dPONo = (item.cl.CLC_PO_NO ?? " "),
                                    isSelected = (item.cl.CLC_UPDATE_GI == "Y") ? true : false

                                });
                            }
                        }

                    }
                    #endregion
                    #region "sType = Borrow"
                    else if (sType == "Borrow")
                    {
                        var query = (from bo in context.BORROW_CRUDE
                                     join bop in (
                                         from order in context.BORROW_PRICE
                                         group order by order.BRP_TRIP_NO into g
                                         select new
                                         {
                                             bop_TRIP_NO = g.Key,
                                             maxBLP_NUM = g.Max(order => order.BRP_NUM)

                                         }) on bo.BRC_TRIP_NO equals bop.bop_TRIP_NO /*on cl.CLC_TRIP_NO equals clp.CLP_TRIP_NO*/
                                     join clp_max in context.BORROW_PRICE on new { x1 = bop.bop_TRIP_NO, x2 = bop.maxBLP_NUM } equals new { x1 = clp_max.BRP_TRIP_NO, x2 = clp_max.BRP_NUM }
                                     where ((bo.BRC_DELIVERY_DATE >= dDateFrom && bo.BRC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                     && ((bo.BRC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                     select new
                                     {
                                         bo,
                                         clp_max,

                                     });


                        if (query != null)
                        {
                            if (pModel.GIClearLine_Search.SearchData != null)
                            {
                                pModel.GIClearLine_Search.SearchData.Clear();
                            }
                            else if (pModel.GIClearLine_Search.SearchData == null) { pModel.GIClearLine_Search.SearchData = new List<GIClearLineViewModel_SearchData>(); }

                            foreach (var item in query.ToList())
                            {
                                pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                                {
                                    // dDONo = (item.cl.CLC_DO_NO ?? "  "),
                                    dType = "Borrow",
                                    dTripNo = item.bo.BRC_TRIP_NO,
                                    dDONo = (item.bo.BRC_DO_NO ?? " "),
                                    dDeliveryDate = Convert.ToDateTime(item.bo.BRC_DELIVERY_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                    dPlant = (item.bo.BRC_PLANT ?? " "),
                                    dVolumeBBL = Convert.ToString(item.bo.BRC_VOLUME_BBL).ToString(),
                                    dVolumeMT = Convert.ToString(item.bo.BRC_VOLUME_MT).ToString(),
                                    dVolumeLitres = Convert.ToString(item.bo.BRC_VOLUME_LITE).ToString(),
                                    dTemp = item.bo.BRC_TEMP.ToString(),
                                    dDensity = item.bo.BRC_DENSITY.ToString(),
                                    isSelected = (item.bo.BRC_UPDATE_GI == "Y") ? true : false

                                });
                            }
                        }

                    }


                    #endregion
                    #region "sType = Swap"
                    else if (sType == "Swap")
                    {
                        //SWAP_CRUDE
                        var query = (from bo in context.SWAP_CRUDE
                                     join bop in (
                                         from order in context.SWAP_PRICE
                                         group order by order.SWP_TRIP_NO into g
                                         select new
                                         {
                                             bop_TRIP_NO = g.Key,
                                             maxBLP_NUM = g.Max(order => order.SWP_NUM)

                                         }) on bo.SWC_TRIP_NO equals bop.bop_TRIP_NO /*on cl.CLC_TRIP_NO equals clp.CLP_TRIP_NO*/
                                     join clp_max in context.BORROW_PRICE on new { x1 = bop.bop_TRIP_NO, x2 = bop.maxBLP_NUM } equals new { x1 = clp_max.BRP_TRIP_NO, x2 = clp_max.BRP_NUM }
                                     where ((bo.SWC_DELIVERY_DATE >= dDateFrom && bo.SWC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                     && ((bo.SWC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                     select new
                                     {
                                         bo,
                                         clp_max,

                                     });

                        if (query != null)
                        {
                            if (pModel.GIClearLine_Search.SearchData != null)
                            {
                                pModel.GIClearLine_Search.SearchData.Clear();
                            }
                            else if (pModel.GIClearLine_Search.SearchData == null) { pModel.GIClearLine_Search.SearchData = new List<GIClearLineViewModel_SearchData>(); }

                            foreach (var item in query.ToList())
                            {
                                pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                                {
                                    dType = "Swap",
                                    dTripNo = (item.bo.SWC_TRIP_NO ?? " "),
                                    dDONo = (item.bo.SWC_DO_NO ?? " "),
                                    dDeliveryDate = Convert.ToDateTime(item.bo.SWC_DELIVERY_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                    dPlant = item.bo.SWC_PLANT,
                                    dVolumeBBL = Convert.ToString(item.bo.SWC_VOLUME_BBL).ToString(),
                                    dVolumeMT = Convert.ToString(item.bo.SWC_VOLUME_MT).ToString(),
                                    dVolumeLitres = Convert.ToString(item.bo.SWC_VOLUME_LITE).ToString(),
                                    dStorageLoc = item.bo.SWC_STORAGE_LOCATION,
                                    dTemp = item.bo.SWC_TEMP.ToString(),
                                    dDensity = item.bo.SWC_DENSITY.ToString(),
                                    dSaleOrder = item.bo.SWC_SALEORDER,
                                    dPONo = item.bo.SWC_PO_NO,
                                    isSelected = (item.bo.SWC_UPDATE_GI == "Y") ? true : false
                                    //     classify = (input > 0) ? "positive" : "negative";(item.bo.SWC_UPDATE_GI ?? true )
                                });
                            }
                        }
                    }
                    #endregion

                    #endregion

                    #region "For Test"
                    // For Test
                    //if (pModel.GIClearLine_Search == null) { pModel.GIClearLine_Search = new GIClearLineViewModel_Search(); }
                    //if (pModel.GIClearLine_Search.SearchData == null) { pModel.GIClearLine_Search.SearchData = new List<GIClearLineViewModel_SearchData>(); }

                    //pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                    //{
                    //    isSelected = "",
                    //    dTripNo = "CL-2017-06-001",
                    //    dDONo = "xxxxxxx",
                    //    dDeliveryDate = "13/03/2016",
                    //    dPlant = "1200",
                    //    dProduct = "Murban",
                    //    dVolumeBBL = "138.19",
                    //    dVolumeMT = "179.535",
                    //    dVolumeLitres = "22,255.59",
                    //    dPrice = "1,190.1015",
                    //    dAmount = "30,000",
                    //    dUS4 = "US4",
                    //    dStorageLoc = "",
                    //    dTemp = "",
                    //    dDensity = ""
                    //});
                    //pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                    //{
                    //    isSelected = "",
                    //    dTripNo = "CL-2017-06-002",
                    //    dDONo = "xxxxxxx",
                    //    dDeliveryDate = "13/03/2016",
                    //    dPlant = "1200",
                    //    dProduct = "Murban",
                    //    dVolumeBBL = "138.19",
                    //    dVolumeMT = "179.535",
                    //    dVolumeLitres = "22,255.59",
                    //    dPrice = "1,190.1015",
                    //    dAmount = "30,000",
                    //    dUS4 = "US4",
                    //    dStorageLoc = "",
                    //    dTemp = "",
                    //    dDensity = ""
                    //});
                    //pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                    //{
                    //    isSelected = "",
                    //    dTripNo = "CL-2017-06-003",
                    //    dDONo = "xxxxxxx",
                    //    dDeliveryDate = "13/03/2016",
                    //    dPlant = "1200",
                    //    dProduct = "Murban",
                    //    dVolumeBBL = "138.19",
                    //    dVolumeMT = "179.535",
                    //    dVolumeLitres = "22,255.59",
                    //    dPrice = "1,190.1015",
                    //    dAmount = "30,000",
                    //    dUS4 = "US4",
                    //    dStorageLoc = "",
                    //    dTemp = "",
                    //    dDensity = ""
                    //});
                    //pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                    //{
                    //    isSelected = "",
                    //    dTripNo = "CL-2017-06-004",
                    //    dDONo = "xxxxxxx",
                    //    dDeliveryDate = "13/03/2016",
                    //    dPlant = "1200",
                    //    dProduct = "Murban",
                    //    dVolumeBBL = "138.19",
                    //    dVolumeMT = "179.535",
                    //    dVolumeLitres = "22,255.59",
                    //    dPrice = "1,190.1015",
                    //    dAmount = "30,000",
                    //    dUS4 = "US4",
                    //    dStorageLoc = "",
                    //    dTemp = "",
                    //    dDensity = ""
                    //});
                    //pModel.GIClearLine_Search.SearchData.Add(new GIClearLineViewModel_SearchData
                    //{
                    //    isSelected = "",
                    //    dTripNo = "CL-2017-06-005",
                    //    dDONo = "xxxxxxx",
                    //    dDeliveryDate = "13/03/2016",
                    //    dPlant = "1200",
                    //    dProduct = "Murban",
                    //    dVolumeBBL = "138.19",
                    //    dVolumeMT = "179.535",
                    //    dVolumeLitres = "22,255.59",
                    //    dPrice = "1,190.1015",
                    //    dAmount = "30,000",
                    //    dUS4 = "US4",
                    //    dStorageLoc = "",
                    //    dTemp = "",
                    //    dDensity = ""
                    //});
                    #endregion

                }
            }
            catch (Exception ex)
            {

            }
        }

        public ReturnValue UpdateData(ref GIClearLineViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            int numberOfSuccess = 0;
            int numberOfFail = 0;
            try
            {
                foreach (var item in pModel.GIClearLine_Search.SearchData)
                {
                    if (item.isSelected == true)
                    {
                        GoodsIssueUpdateModel obj = new GoodsIssueUpdateModel();
                        obj.ACT_GI_DATE = DateTime.Now.ToString("yyyy-MM-dd");
                        //obj.ACT_GI_DATE = "2017-08-25";
                        obj.mDELIVERY_ITEM = new List<GoodsIssueUpdateModel.ZDO_ITEM>();
                        obj.mDELIVERY_HEADER = new List<GoodsIssueUpdateModel.LIKP>();
                        obj.mDELIVERY_ITEM_ADD_QTY = new List<GoodsIssueUpdateModel.LIPSO2>();
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            using (var dbContextTransaction = context.Database.BeginTransaction())
                            {
                                if (item.dType == "GI Clear Line")
                                {
                                    var qryClearLineCrude = context.CLEAR_LINE_CRUDE.SingleOrDefault(z => z.CLC_TRIP_NO.ToUpper().Equals(item.dTripNo.ToUpper()));
                                    if (qryClearLineCrude != null)
                                    {
                                        qryClearLineCrude.CLC_STORAGE_LOCATION = item.dStorageLoc;
                                        qryClearLineCrude.CLC_TEMP = Convert.ToDecimal(item.dTemp);
                                        qryClearLineCrude.CLC_DENSITY = Convert.ToDecimal(item.dDensity);
                                        CLEAR_LINE_DAL dalCLC = new CLEAR_LINE_DAL();
                                        dalCLC.UpdateClearLineCrude(qryClearLineCrude);
                                        dbContextTransaction.Commit();
                                        var itemNo = "000010";
                                        obj.mDELIVERY_HEADER.Add(new GoodsIssueUpdateModel.LIKP
                                        {
                                            VBELN = qryClearLineCrude.CLC_DO_NO,
                                            TRATY = "Z008",
                                        });
                                        obj.mDELIVERY_ITEM.Add(new GoodsIssueUpdateModel.ZDO_ITEM
                                        {
                                            POSNR_VL = itemNo,
                                            MATNR = qryClearLineCrude.CLC_CRUDE_TYPE_ID,
                                            WERKS = qryClearLineCrude.CLC_PLANT,
                                            ZTEMP = qryClearLineCrude.CLC_TEMP ?? 0,
                                            ZDENS_15C = qryClearLineCrude.CLC_DENSITY ?? 0,
                                            LFIMG = qryClearLineCrude.CLC_TOTAL ?? 0,
                                            LGORT = qryClearLineCrude.CLC_STORAGE_LOCATION,
                                            CustomerCode = qryClearLineCrude.CLC_CUST_NUM,
                                            SALES_ORG = qryClearLineCrude.CLC_COMPANY
                                        });
                                        if (qryClearLineCrude.CLC_INVOICE_FIGURE == "B/L")
                                        {
                                            if (qryClearLineCrude.CLC_VOLUME_BBL != null)
                                            {
                                                obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                                {
                                                    VBELN = qryClearLineCrude.CLC_DO_NO,
                                                    POSNR = itemNo,
                                                    MSEHI = "BBL",
                                                    ADQNT = Convert.ToDouble(qryClearLineCrude.CLC_VOLUME_BBL),
                                                });
                                            }
                                            if (qryClearLineCrude.CLC_VOLUME_MT != null)
                                            {
                                                obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                                {
                                                    VBELN = qryClearLineCrude.CLC_DO_NO,
                                                    POSNR = itemNo,
                                                    MSEHI = "MT",
                                                    ADQNT = Convert.ToDouble(qryClearLineCrude.CLC_VOLUME_MT),
                                                });
                                            }
                                            if (qryClearLineCrude.CLC_VOLUME_LITE != null)
                                            {
                                                obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                                {
                                                    VBELN = qryClearLineCrude.CLC_DO_NO,
                                                    POSNR = itemNo,
                                                    MSEHI = "L30",
                                                    ADQNT = Convert.ToDouble(qryClearLineCrude.CLC_VOLUME_LITE),
                                                });
                                            }
                                        }
                                        else
                                        {
                                            if (qryClearLineCrude.CLC_OUTTURN_BBL != null)
                                            {
                                                obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                                {
                                                    VBELN = qryClearLineCrude.CLC_DO_NO,
                                                    POSNR = itemNo,
                                                    MSEHI = "BBL",
                                                    ADQNT = Convert.ToDouble(qryClearLineCrude.CLC_OUTTURN_BBL),
                                                });
                                            }
                                            if (qryClearLineCrude.CLC_OUTTURN_MT != null)
                                            {
                                                obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                                {
                                                    VBELN = qryClearLineCrude.CLC_DO_NO,
                                                    POSNR = itemNo,
                                                    MSEHI = "MT",
                                                    ADQNT = Convert.ToDouble(qryClearLineCrude.CLC_OUTTURN_MT),
                                                });
                                            }
                                            if (qryClearLineCrude.CLC_OUTTURN_LITE != null)
                                            {
                                                obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                                {
                                                    VBELN = qryClearLineCrude.CLC_DO_NO,
                                                    POSNR = itemNo,
                                                    MSEHI = "L30",
                                                    ADQNT = Convert.ToDouble(qryClearLineCrude.CLC_OUTTURN_LITE),
                                                });
                                            }
                                        }
                                    }
                                }
                                else if (item.dType == "Borrow")
                                {
                                    var qryBorrowCrude = context.BORROW_CRUDE.SingleOrDefault(z => z.BRC_TRIP_NO.ToUpper().Equals(item.dTripNo.ToUpper()));
                                    if (qryBorrowCrude != null)
                                    {
                                        qryBorrowCrude.BRC_STORAGE_LOCATION = item.dStorageLoc;
                                        qryBorrowCrude.BRC_TEMP = Convert.ToDecimal(item.dTemp);
                                        qryBorrowCrude.BRC_DENSITY = Convert.ToDecimal(item.dDensity);
                                        BORROW_DAL dalBRRC = new BORROW_DAL();
                                        dalBRRC.UpdateBorrowCrude(qryBorrowCrude);
                                        dbContextTransaction.Commit();
                                        var itemNo = "000010";
                                        obj.mDELIVERY_HEADER.Add(new GoodsIssueUpdateModel.LIKP
                                        {
                                            VBELN = qryBorrowCrude.BRC_DO_NO,
                                            TRATY = "Z009",
                                        });
                                        obj.mDELIVERY_ITEM.Add(new GoodsIssueUpdateModel.ZDO_ITEM
                                        {
                                            POSNR_VL = itemNo,
                                            MATNR = qryBorrowCrude.BRC_CRUDE_TYPE_ID,
                                            WERKS = qryBorrowCrude.BRC_PLANT,
                                            ZTEMP = qryBorrowCrude.BRC_TEMP ?? 0,
                                            ZDENS_15C = qryBorrowCrude.BRC_DENSITY ?? 0,
                                            LFIMG = qryBorrowCrude.BRC_TOTAL ?? 0,
                                            LGORT = qryBorrowCrude.BRC_STORAGE_LOCATION,
                                            CustomerCode = qryBorrowCrude.BRC_CUST_NUM,
                                            SALES_ORG = qryBorrowCrude.BRC_COMPANY
                                        });

                                        //if (qryBorrowCrude == "B/L")
                                        //{
                                        if (qryBorrowCrude.BRC_VOLUME_BBL != null)
                                        {
                                            obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                            {
                                                VBELN = qryBorrowCrude.BRC_DO_NO,
                                                POSNR = itemNo,
                                                MSEHI = "BBL",
                                                ADQNT = Convert.ToDouble(qryBorrowCrude.BRC_VOLUME_BBL),
                                            });
                                        }
                                        if (qryBorrowCrude.BRC_VOLUME_MT != null)
                                        {
                                            obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                            {
                                                VBELN = qryBorrowCrude.BRC_DO_NO,
                                                POSNR = itemNo,
                                                MSEHI = "MT",
                                                ADQNT = Convert.ToDouble(qryBorrowCrude.BRC_VOLUME_MT),
                                            });
                                        }
                                        if (qryBorrowCrude.BRC_VOLUME_LITE != null)
                                        {
                                            obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                            {
                                                VBELN = qryBorrowCrude.BRC_DO_NO,
                                                POSNR = itemNo,
                                                MSEHI = "L30",
                                                ADQNT = Convert.ToDouble(qryBorrowCrude.BRC_VOLUME_LITE),
                                            });
                                        }
                                        //}
                                        //else
                                        //{
                                        //if (qryBorrowCrude.BRC_OUTTURN_BBL != null)
                                        //{
                                        //    obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                        //    {
                                        //        VBELN = qryBorrowCrude.BRC_DO_NO,
                                        //        POSNR = itemNo,
                                        //        MSEHI = "BBL",
                                        //        ADQNT = Convert.ToDouble(qryBorrowCrude.BRC_OUTTURN_BBL),
                                        //    });
                                        //}
                                        //if (qryBorrowCrude.BRC_OUTTURN_MT != null)
                                        //{
                                        //    obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                        //    {
                                        //        VBELN = qryBorrowCrude.BRC_DO_NO,
                                        //        POSNR = itemNo,
                                        //        MSEHI = "MT",
                                        //        ADQNT = Convert.ToDouble(qryBorrowCrude.BRC_OUTTURN_MT),
                                        //    });
                                        //}
                                        //if (qryBorrowCrude.BRC_OUTTURN_LITE != null)
                                        //{
                                        //    obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                        //    {
                                        //        VBELN = qryBorrowCrude.BRC_DO_NO,
                                        //        POSNR = itemNo,
                                        //        MSEHI = "L30",
                                        //        ADQNT = Convert.ToDouble(qryBorrowCrude.BRC_OUTTURN_LITE),
                                        //    });
                                        //}
                                        //}
                                    }
                                }
                                else if (item.dType == "Swap")
                                {
                                    var qrySwapCrude = context.SWAP_CRUDE.SingleOrDefault(z => z.SWC_TRIP_NO.ToUpper().Equals(item.dTripNo.ToUpper()));
                                    if (qrySwapCrude != null)
                                    {
                                        qrySwapCrude.SWC_STORAGE_LOCATION = item.dStorageLoc;
                                        qrySwapCrude.SWC_TEMP = Convert.ToDecimal(item.dTemp);
                                        qrySwapCrude.SWC_DENSITY = Convert.ToDecimal(item.dDensity);
                                        SWAP_CRUDE_DAL dalSWAP = new SWAP_CRUDE_DAL();
                                        dalSWAP.Update(qrySwapCrude);
                                        dbContextTransaction.Commit();
                                        var itemNo = "000010";
                                        obj.mDELIVERY_HEADER.Add(new GoodsIssueUpdateModel.LIKP
                                        {
                                            VBELN = qrySwapCrude.SWC_DO_NO,
                                            TRATY = "Z005",
                                        });
                                        obj.mDELIVERY_ITEM.Add(new GoodsIssueUpdateModel.ZDO_ITEM
                                        {
                                            POSNR_VL = itemNo,
                                            MATNR = qrySwapCrude.SWC_CRUDE_TYPE_ID,
                                            WERKS = qrySwapCrude.SWC_PLANT,
                                            ZTEMP = qrySwapCrude.SWC_TEMP ?? 0,
                                            ZDENS_15C = qrySwapCrude.SWC_DENSITY ?? 0,
                                            LFIMG = qrySwapCrude.SWC_TOTAL ?? 0,
                                            LGORT = qrySwapCrude.SWC_STORAGE_LOCATION,
                                            //CustomerCode = qrySwapCrude.,
                                            //SALES_ORG = qrySwapCrude. 
                                        });

                                        //if (qrySwapCrude.fi == "B/L")
                                        //{
                                        if (qrySwapCrude.SWC_VOLUME_BBL != null)
                                        {
                                            obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                            {
                                                VBELN = qrySwapCrude.SWC_DO_NO,
                                                POSNR = itemNo,
                                                MSEHI = "BBL",
                                                ADQNT = Convert.ToDouble(qrySwapCrude.SWC_VOLUME_BBL),
                                            });
                                        }
                                        if (qrySwapCrude.SWC_VOLUME_MT != null)
                                        {
                                            obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                            {
                                                VBELN = qrySwapCrude.SWC_DO_NO,
                                                POSNR = itemNo,
                                                MSEHI = "MT",
                                                ADQNT = Convert.ToDouble(qrySwapCrude.SWC_VOLUME_MT),
                                            });
                                        }
                                        if (qrySwapCrude.SWC_VOLUME_LITE != null)
                                        {
                                            obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                            {
                                                VBELN = qrySwapCrude.SWC_DO_NO,
                                                POSNR = itemNo,
                                                MSEHI = "L30",
                                                ADQNT = Convert.ToDouble(qrySwapCrude.SWC_VOLUME_LITE),
                                            });
                                        }
                                        //}
                                        //else
                                        //{
                                        //if (qrySwapCrude.SWC_OUTTURN_BBL != null)
                                        //{
                                        //    obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                        //    {
                                        //        VBELN = qrySwapCrude.SWC_DO_NO,
                                        //        POSNR = itemNo,
                                        //        MSEHI = "BBL",
                                        //        ADQNT = Convert.ToDouble(qrySwapCrude.SWC_OUTTURN_BBL),
                                        //    });
                                        //}
                                        //if (qrySwapCrude.SWC_OUTTURN_MT != null)
                                        //{
                                        //    obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                        //    {
                                        //        VBELN = qrySwapCrude.SWC_DO_NO,
                                        //        POSNR = itemNo,
                                        //        MSEHI = "MT",
                                        //        ADQNT = Convert.ToDouble(qrySwapCrude.SWC_OUTTURN_MT),
                                        //    });
                                        //}
                                        //if (qrySwapCrude.SWC_OUTTURN_LITE != null)
                                        //{
                                        //    obj.mDELIVERY_ITEM_ADD_QTY.Add(new GoodsIssueUpdateModel.LIPSO2
                                        //    {
                                        //        VBELN = qrySwapCrude.SWC_DO_NO,
                                        //        POSNR = itemNo,
                                        //        MSEHI = "L30",
                                        //        ADQNT = Convert.ToDouble(qrySwapCrude.SWC_OUTTURN_LITE),
                                        //    });
                                        //}
                                        //}
                                    }
                                }
                            }
                        }

                        var json = new JavaScriptSerializer().Serialize(obj);

                        RequestCPAI req = new RequestCPAI();
                        ResponseData resData = new ResponseData();
                        RequestData reqData = new RequestData();
                        ServiceProvider.ProjService projService = new ServiceProvider.ProjService();
                        req.Function_id = ConstantPrm.FUNCTION.F10000066;
                        req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                        req.State_name = "";
                        req.Req_parameters = new Req_parameters();
                        req.Req_parameters.P = new List<P>();
                        req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                        req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                        req.Req_parameters.P.Add(new P { K = "system", V = "GIClearLine" });
                        req.Req_parameters.P.Add(new P { K = "trip_no", V = item.dTripNo.ToUpper() });
                        req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                        req.Extra_xml = "";
                        var xml = ShareFunction.XMLSerialize(req);
                        reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                        resData = projService.CallService(reqData);

                        if (resData.result_code == "1")
                        {
                            numberOfSuccess++;
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                            using (EntityCPAIEngine context = new EntityCPAIEngine())
                            {
                                using (var dbContextTransaction = context.Database.BeginTransaction())
                                {
                                    var qryClearLineCrude = context.CLEAR_LINE_CRUDE.SingleOrDefault(z => z.CLC_TRIP_NO.ToUpper().Equals(item.dTripNo.ToUpper()));
                                    qryClearLineCrude.CLC_UPDATE_GI = "Y";
                                    CLEAR_LINE_DAL dalCLC = new CLEAR_LINE_DAL();
                                    dalCLC.UpdateClearLineCrude(qryClearLineCrude);
                                    dbContextTransaction.Commit();
                                }
                            }
                        }
                        else
                        {
                            numberOfFail++;
                            rtn.Message = resData.response_message;
                            rtn.Status = false;
                        }
                    }
                    rtn.Message = "Send to SAP completed. Success = " + numberOfSuccess + ". Fail = " + numberOfFail + ".";
                }
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }
            return rtn;
        }

        public List<SelectListItem> getStorageLocation()
        {
            string strJSON = "{{ \"COMPANY_CODE\":\"\",\"CODE\":\"\",\"NAME\":\"\"}}";
            string JsonD = MasterData.GetJsonGlobalConfig("CIP_IMPORT_PLAN");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigStorageLocGIClearLine dataList = (GlobalConfigStorageLocGIClearLine)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigStorageLocGIClearLine));

            var lstItem = new List<SelectListItem>();
            foreach (var z in dataList.PCF_MT_STORAGE_LOCATION)
            {
                lstItem.Add(new SelectListItem { Value = z.CODE, Text = z.NAME });
            }

            return lstItem;
        }

        public void setInitail_ddl(ref GIClearLineViewModel pModel)
        {
            if (pModel.ddl_StorageLoc == null) { pModel.ddl_StorageLoc = new List<SelectListItem>(); }
            pModel.ddl_StorageLoc = getStorageLocation();

        }
    }
}
