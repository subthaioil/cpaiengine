﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Globalization;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingConfigServiceModel
    {
        public ReturnValue VaildateInput(ref HedgingConfigViewModel pModel,string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            rtn.Message = string.Empty;
            if (string.IsNullOrEmpty(pUser))
            {
                rtn.Message = "User should not be empty";
                rtn.Status = false;
            }
            else if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
            }
            else if (string.IsNullOrEmpty(pModel.reason))
            {
                rtn.Message = "Reason should is not null";
                rtn.Status = false;
            }
            else
            {
                foreach (var list in pModel.HedgingConfig_NewData)
                {
                    if (list.nValue == "")
                    {
                        rtn.Message = list.nName + "should not be empty";
                        rtn.Status = false;
                        break;
                    }
                }
            }
            return rtn;
        }

        private bool VaildateValue(HedgingConfig_NewData nModel, HedgingConfig_OldData oModel)
        {
            if (nModel.IsChange == "1") {
                if (nModel.nValue != oModel.oValue)
                {
                    return true;
                }
                else if(nModel.nDesc != oModel.oDesc)
                {
                    return true;
                }
            }
            return false;
        }

        public ReturnValue Update(ref HedgingConfigViewModel pModel,string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                rtn = VaildateInput(ref pModel, pUser);
                if (!rtn.Status)
                    return rtn;
                HEDG_MT_CRCONFIG cr_config = new HEDG_MT_CRCONFIG();
                HEDG_HIS_CRCONFIG his_config = new HEDG_HIS_CRCONFIG();
                HEDG_MT_CRCONFIG_DAL cr_config_dal = new HEDG_MT_CRCONFIG_DAL();
                HEDG_HIS_CRCONFIG_DAL his_config_dal = new HEDG_HIS_CRCONFIG_DAL();
                
                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            for (int index = 0;index < pModel.HedgingConfig_NewData.Count();index++)
                            {
                                if (VaildateValue(pModel.HedgingConfig_NewData[index],pModel.HedgingConfig_OldData[index]))
                                {
                                    cr_config = new HEDG_MT_CRCONFIG();

                                    cr_config.HMCC_ROW_ID = pModel.HedgingConfig_NewData[index].nRowID;
                                    cr_config.HMCC_NAME = pModel.HedgingConfig_NewData[index].nName;
                                    cr_config.HMCC_VALUE = pModel.HedgingConfig_NewData[index].nValue;
                                    cr_config.HMCC_DESC = pModel.HedgingConfig_NewData[index].nDesc;
                                    cr_config.HMCC_UPDATED_BY = pUser;
                                    cr_config.HMCC_UPDATED_DATE = now;

                                    cr_config_dal.Update(cr_config, context);

                                    his_config = new HEDG_HIS_CRCONFIG();

                                    var CtrlID = ConstantPrm.GetDynamicCtrlID();
                                    his_config.HICC_ROW_ID = CtrlID;
                                    his_config.HICC_NAME = pModel.HedgingConfig_OldData[index].oName;
                                    his_config.HICC_DESC = pModel.HedgingConfig_OldData[index].oDesc;
                                    his_config.HICC_VALUE = pModel.HedgingConfig_OldData[index].oValue;
                                    his_config.HICC_FK_HEDG_CRCONFIG = pModel.HedgingConfig_OldData[index].oRowID;
                                    his_config.HICC_HISTORY_REASON = pModel.reason;
                                    his_config.HICC_CREATED_BY = pUser;
                                    his_config.HICC_CREATED_DATE = now;
                                    his_config.HICC_UPDATED_BY = pUser;
                                    his_config.HICC_UPDATED_DATE = now;

                                    his_config_dal.Save(his_config, context);
                                }
                            }
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch(Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HedgingConfigViewModel GetDetail()
        {
            HedgingConfigViewModel detailData = new HedgingConfigViewModel();
            detailData.HedgingConfig_OldData = new List<HedgingConfig_OldData>();
            detailData.HedgingConfig_NewData = new List<HedgingConfig_NewData>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CRCONFIG
                                 orderby v.HMCC_ROW_ID
                                 select new
                                 {
                                     dOrderID = v.HMCC_ROW_ID
                                     ,
                                     dName = v.HMCC_NAME
                                     ,
                                     dValue = v.HMCC_VALUE
                                     ,
                                     dDesc = v.HMCC_DESC
                                     ,
                                     dCreateDate = (DateTime?)v.HMCC_CREATED_DATE
                                     ,
                                     dCreateBy = v.HMCC_CREATED_BY
                                 }
                    );
                    if (query != null)
                    {
                        foreach (var g in query.ToList())
                        {
                            detailData.HedgingConfig_OldData.Add(new HedgingConfig_OldData() {
                                oRowID = g.dOrderID,
                                oName = g.dName,
                                oValue = g.dValue,
                                oDesc = g.dDesc
                            });
                        }
                        foreach (var g in query.ToList())
                        {
                            detailData.HedgingConfig_NewData.Add(new HedgingConfig_NewData()
                            {
                                nRowID = g.dOrderID,
                                nName = g.dName,
                                nValue = g.dValue,
                                nDesc = g.dDesc,
                                IsChange = "0"
                            });
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return detailData;
        }
    }
}