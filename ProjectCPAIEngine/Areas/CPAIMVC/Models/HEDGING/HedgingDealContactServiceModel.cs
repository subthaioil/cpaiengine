﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.Script.Serialization;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;

using com.pttict.engine.utility;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingDealContactServiceModel
    {
        public ReturnValue Search(ref HedgingDealContactViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            string[] lstStatus, lstDealType;            
            string sDateFrom, sDateTo, sStatus, sDealNo, sTicketNo, sContactNo, sCompany, sDealType, sCounterparty, sUnderlying, sVS, sTool, sVolumeMonth_From, sVolumeMonth_To, sPrice_From, sPrice_To, sSystemType;

            try
            {
                try
                {                    
                    sDateFrom = String.IsNullOrEmpty(pModel.sDealDate) ? string.Empty : pModel.sDealDate.Trim().Substring(0, 10);
                    sDateTo = String.IsNullOrEmpty(pModel.sDealDate) ? string.Empty : pModel.sDealDate.Trim().Substring(14, 10);
                    sStatus = String.IsNullOrEmpty(pModel.sStatus) ? string.Empty : pModel.sStatus.Trim().ToLower();
                    lstStatus = String.IsNullOrEmpty(pModel.sStatusSelected) ? lstStatus = new string[0] : pModel.sStatusSelected.Trim().ToLower().Split(char.Parse("|"));
                    sDealNo = String.IsNullOrEmpty(pModel.sDealNo) ? string.Empty : pModel.sDealNo.Trim().ToLower();
                    sTicketNo = String.IsNullOrEmpty(pModel.sTicketNo) ? string.Empty : pModel.sTicketNo.Trim().ToLower();
                    sContactNo = String.IsNullOrEmpty(pModel.sContactNo) ? string.Empty : pModel.sContactNo.Trim().ToLower();
                    sCompany = String.IsNullOrEmpty(pModel.sCompany) ? string.Empty : pModel.sCompany.Trim().ToLower();
                    sDealType = String.IsNullOrEmpty(pModel.sDealType) ? string.Empty : pModel.sDealType.Trim().ToLower();                    
                    sCounterparty = String.IsNullOrEmpty(pModel.sCounterparty) ? string.Empty : pModel.sCounterparty.Trim().ToLower();
                    sUnderlying = String.IsNullOrEmpty(pModel.sUnderlying) ? string.Empty : pModel.sUnderlying.Trim().ToLower();
                    sVS = String.IsNullOrEmpty(pModel.sVS) ? string.Empty : pModel.sVS.Trim().ToLower();
                    sTool = String.IsNullOrEmpty(pModel.sTool) ? string.Empty : pModel.sTool.Trim().ToLower();
                    sVolumeMonth_From = String.IsNullOrEmpty(pModel.sVolumeMonth_From) ? string.Empty : pModel.sVolumeMonth_From.Trim().ToLower();
                    sVolumeMonth_To = String.IsNullOrEmpty(pModel.sVolumeMonth_To) ? string.Empty : pModel.sVolumeMonth_To.Trim().ToLower();
                    sPrice_From = String.IsNullOrEmpty(pModel.sPrice_From) ? string.Empty : pModel.sPrice_From.Trim().ToLower();
                    sPrice_To = String.IsNullOrEmpty(pModel.sPrice_To) ? string.Empty : pModel.sPrice_To.Trim().ToLower();
                    sSystemType = String.IsNullOrEmpty(pModel.systemType) ? string.Empty : pModel.systemType.Trim().ToLower();
                    lstDealType = new string[0];
                    if (pModel.sDealType_Sell)
                    {
                        Array.Resize(ref lstDealType, lstDealType.Length + 1);
                        lstDealType[lstDealType.Length - 1] = "sell";
                    }
                    if (pModel.sDealType_Buy)
                    {
                        Array.Resize(ref lstDealType, lstDealType.Length + 1);
                        lstDealType[lstDealType.Length - 1] = "buy";
                    }
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_DEAL_DATA                                 
                                 select new
                                 {
                                     dSystem = v.HDDA_ENG_SYSTEM,
                                     dType = v.HDDA_ENG_TYPE,
                                     dStatus = v.HDDA_STATUS,
                                     dStatusTicket = v.HDDA_FLAG_TICKET,
                                     dStatusTracking = v.HDDA_STATUS_TRACKING,
                                     dDealNo = v.HDDA_DEAL_NO,
                                     dTicketNo = v.HDDA_TICKET_NO,
                                     dContactNo = v.HDDA_CONTRACT_NO,
                                     dDealDate = (DateTime?)v.HDDA_DEAL_DATE,
                                     dCompany = v.HDDA_FK_COMPANY,
                                     dDealType = v.HDDA_DEAL_TYPE,
                                     dCounterparty = v.HDDA_FK_COUNTER_PARTIES,
                                     dUnderlying = v.HDDA_FK_UNDERLY,
                                     dVS = v.HDDA_FK_UNDERLY_VS,
                                     dTool = v.HDDA_FK_TOOL,
                                     dVolumeMonth = v.HDDA_VOL_MNT,
                                     dPrice = v.HDDA_PRICE
                                 }).OrderByDescending(x => x.dDealNo).ToList();
                    if (!string.IsNullOrEmpty(sSystemType))
                        query = query.Where(p => (p.dSystem != null && p.dType != null) && (p.dSystem.Trim().ToLower().Equals(ConstantPrm.SYSTEM.HEDG_DEAL.Trim().ToLower()) && (p.dType.Trim().ToLower().Equals(sSystemType)))).ToList();
                    if (!string.IsNullOrEmpty(sDealNo))                        
                        query = query.Where(p => p.dDealNo != null && p.dDealNo.Trim().ToLower().Contains(sDealNo)).ToList();
                    if (!string.IsNullOrEmpty(sTicketNo))
                        query = query.Where(p => p.dTicketNo != null && p.dTicketNo.Trim().ToLower().Contains(sTicketNo)).ToList();
                    if (!string.IsNullOrEmpty(sContactNo))
                        query = query.Where(p => p.dContactNo != null && p.dContactNo.Trim().ToLower().Contains(sContactNo)).ToList();
                    if (!string.IsNullOrEmpty(sCompany))
                        query = query.Where(p => p.dCompany != null && p.dCompany.Trim().ToLower().Equals(sCompany)).ToList();
                    if (!string.IsNullOrEmpty(sCounterparty))
                        query = query.Where(p => p.dCounterparty != null && p.dCounterparty.Trim().ToLower().Equals(sCounterparty)).ToList();
                    if (!string.IsNullOrEmpty(sUnderlying))
                        query = query.Where(p => p.dUnderlying != null && p.dUnderlying.Trim().ToLower().Equals(sUnderlying)).ToList();
                    if (!string.IsNullOrEmpty(sVS))
                        query = query.Where(p => p.dVS != null && p.dVS.Trim().ToLower().Equals(sVS)).ToList();
                    if (!string.IsNullOrEmpty(sTool))
                        query = query.Where(p => p.dTool != null && p.dTool.Trim().ToLower().Equals(sTool)).ToList();                    
                    if (!string.IsNullOrEmpty(sDealType))
                        query = query.Where(p => p.dDealType != null && p.dDealType.Trim().ToLower().Equals(sDealType)).ToList();
                    if ( !string.IsNullOrEmpty(sVolumeMonth_From) && !string.IsNullOrEmpty(sVolumeMonth_To) )
                        query = query.Where( p => p.dVolumeMonth != null && (p.dVolumeMonth >= Convert.ToDecimal(sVolumeMonth_From) && p.dVolumeMonth <= Convert.ToDecimal(sVolumeMonth_To)) ).ToList();
                    if ( !string.IsNullOrEmpty(sPrice_From) && !string.IsNullOrEmpty(sPrice_To))
                        query = query.Where(p => p.dPrice != null && (p.dPrice >= Convert.ToDecimal(sPrice_From) && p.dPrice <= Convert.ToDecimal(sPrice_To))).ToList();
                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dDealDate >= sDate && p.dDealDate <= eDate).ToList();
                    }                    
                    if (lstStatus.Length > 0)
                        query = query.Where(p => p.dStatusTracking != null && lstStatus.Contains(p.dStatusTracking.Trim().ToLower())).ToList();
                    if (lstDealType.Length > 0)
                        query = query.Where(p => p.dDealType != null && lstDealType.Contains(p.dDealType.Trim().ToLower())).ToList();
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (query != null)
                    {
                        pModel.SearchData = new List<HedgingDealContactViewModel_SearchData>();                        
                        foreach (var i in query)
                        {
                            pModel.SearchData.Add(new HedgingDealContactViewModel_SearchData
                            {
                                dStatus = i.dStatus,
                                dStatusTicket = i.dStatusTicket,
                                dDealNo = i.dDealNo,
                                dTicketNo = i.dTicketNo,
                                dContactNo = i.dContactNo,
                                dDealDate = (i.dDealDate == null ? string.Empty : Convert.ToDateTime(i.dDealDate).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                dCompany = i.dCompany,
                                dDealType = i.dDealType,
                                dCounterparty = i.dCounterparty,
                                dUnderlying = i.dUnderlying,
                                dTool = i.dTool,
                                dVolumeMonth = string.Format("{0:0.##}", i.dVolumeMonth),
                                dPrice = string.Format("{0:0.##}", i.dPrice)
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }
    }
}
