﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.Script.Serialization;
using Microsoft.Exchange.WebServices.Data;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALHedg;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;

using com.pttict.engine.utility;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingDealServiceModel
    {
        private ReturnValue Mail(String from, String to, String cc, String subject, String message, Stream attachment, string fileName)
        {
            ReturnValue rtn = new ReturnValue();
            SmtpClient smtp = new SmtpClient();

            try
            {
                using (MailMessage mail = new MailMessage(from, to))
                {
                    mail.CC.Add(cc);
                    mail.Subject = subject;
                    mail.Body = message;
                    mail.IsBodyHtml = false;
                    if (attachment != null)
                        mail.Attachments.Add(new System.Net.Mail.Attachment(attachment, fileName));

                    smtp.Host = "maildist_p.pttgrp.corp";
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = new NetworkCredential("topgroupapplication", "28Ggk2Dh");
                    smtp.Port = 25;
                    smtp.Send(mail);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                    rtn.Status = true;
                    rtn.Message = "Send mail success";
                }
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }
            return rtn;
        }

        private ReturnValue MailEWS(string host, string port, string user, string password, string from, string from_name, string to, string subject, string message, Stream attachment, string fileName, string cc, string bcc)
        {
            ReturnValue rtn = new ReturnValue();
            int i = 0;

            try
            {
                ExchangeService service = new ExchangeService();
                EmailMessage email = new EmailMessage(service);
                service.Credentials = new WebCredentials(user, password);
                try
                {
                    service.AutodiscoverUrl(from, RedirectionUrlValidationCallback);
                    email.From = new EmailAddress();
                    email.From.Name = from_name;
                    email.From.Address = from;
                }
                catch
                {
                    service.AutodiscoverUrl(from, RedirectionUrlValidationCallback);
                    email.Sender = new EmailAddress();
                    email.Sender.Name = from_name;
                    email.Sender.Address = from;
                }
                if (!string.IsNullOrEmpty(to))
                {
                    string[] mailTo = to.Split(';');
                    for (i = 0; i <= mailTo.Length - 1; i++)
                        email.ToRecipients.Add(mailTo[i]);
                }
                if (!string.IsNullOrEmpty(cc))
                {
                    string[] mailCC = cc.Split(';');
                    for (i = 0; i <= mailCC.Length - 1; i++)
                        email.CcRecipients.Add(mailCC[i]);
                }
                if (!string.IsNullOrEmpty(bcc))
                {
                    string[] mailBCC = bcc.Split(';');
                    for (i = 0; i <= mailBCC.Length - 1; i++)
                        email.BccRecipients.Add(mailBCC[i]);
                }
                if (attachment != null)
                    email.Attachments.AddFileAttachment(fileName, attachment);
                email.Subject = subject;
                email.Body = new MessageBody(message);
                email.SendAndSaveCopy();
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                rtn.Status = true;
                rtn.Message = "Send mail success";

            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }

            return rtn;
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        public static string getTransactionByID(string id)
        {
            try
            {
                HedgDealRootObject rootObj = new HedgDealRootObject();
                rootObj.hedging_deal = new HedgingDealViewModel_Detail();
                rootObj.hedging_deal.tenor = new Tenor();
                rootObj.hedging_deal.verify = new Verify();
                rootObj.hedging_deal.other_bid_price = new List<OtherBidPrice>();

                HEDG_DEAL_DATA_DAL DEAL_DAL = new HEDG_DEAL_DATA_DAL();
                HEDG_DEAL_DATA data = DEAL_DAL.GetDEALDATAByID(id);

                if (data != null)
                {
                    rootObj.hedging_deal.deal_no = data.HDDA_DEAL_NO;
                    rootObj.hedging_deal.ref_pre_deal_no = data.HDDA_PRE_DEAL_NO;
                    rootObj.hedging_deal.ref_ticket_no = data.HDDA_TICKET_NO;
                    rootObj.hedging_deal.ref_deal_no = data.HDDA_REF_DEAL_NO;

                    rootObj.hedging_deal.unwide_status = data.HDDA_UNWIDE_STATUS;
                    rootObj.hedging_deal.status = data.HDDA_STATUS;
                    rootObj.hedging_deal.status_tracking = data.HDDA_STATUS_TRACKING;
                    rootObj.hedging_deal.revision = data.HDDA_REVISION;
                    rootObj.hedging_deal.create_by = data.HDDA_CREATED_BY;
                    rootObj.hedging_deal.create_date = (data.HDDA_CREATED == DateTime.MinValue || data.HDDA_CREATED == null) ? "" : Convert.ToDateTime(data.HDDA_CREATED).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                    rootObj.hedging_deal.update_by = data.HDDA_UPDATED_BY;
                    rootObj.hedging_deal.update_date = (data.HDDA_UPDATED == DateTime.MinValue || data.HDDA_UPDATED == null) ? "" : Convert.ToDateTime(data.HDDA_UPDATED).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);

                    rootObj.hedging_deal.suggestion = data.HDDA_SUGGESTION;
                    rootObj.hedging_deal.deal_date = (data.HDDA_DEAL_DATE == DateTime.MinValue || data.HDDA_DEAL_DATE == null) ? "" : Convert.ToDateTime(data.HDDA_DEAL_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                    rootObj.hedging_deal.deal_type = data.HDDA_DEAL_TYPE;
                    rootObj.hedging_deal.m1m_status = data.HDDA_M1M_STATUS;
                    rootObj.hedging_deal.company = data.HDDA_FK_COMPANY;
                    rootObj.hedging_deal.counter_parties = data.HDDA_FK_COUNTER_PARTIES;
                    rootObj.hedging_deal.counter_additional = data.HDDA_CP_ADDITIONAL;

                    rootObj.hedging_deal.labix_trade_thru_top = data.HDDA_LB_STATUS;
                    rootObj.hedging_deal.labix_fee = data.HDDA_LB_FEE.ToString();
                    rootObj.hedging_deal.labix_fee_unit = data.HDDA_LB_FEE_UNIT;
                    rootObj.hedging_deal.underlying = data.HDDA_FK_UNDERLY;
                    rootObj.hedging_deal.underlying_cf_opr = data.HDDA_UNDERLY_CF_OPR;
                    rootObj.hedging_deal.underlying_cf_val = data.HDDA_UNDERLY_CF_VAL.ToString();
                    rootObj.hedging_deal.underlying_weight_percent = data.HDDA_UNDERLY_WEIGHT_PERCENT.ToString();
                    rootObj.hedging_deal.ref_underlying = data.HDDA_FK_REF_UNDERLY;

                    rootObj.hedging_deal.underlying_vs = data.HDDA_FK_UNDERLY_VS;
                    rootObj.hedging_deal.underlying_vs_cf_opr = data.HDDA_UNDERLY_VS_CF_OPR;
                    rootObj.hedging_deal.underlying_vs_cf_val = data.HDDA_UNDERLY_VS_CF_VAL.ToString();
                    rootObj.hedging_deal.underlying_vs_weight_percent = data.HDDA_UNDERLY_VS_WEIGHT_PERCENT.ToString();
                    rootObj.hedging_deal.ref_underlying_vs = data.HDDA_FK_REF_UNDERLY_VS;
                    rootObj.hedging_deal.fw_annual_id = data.HDDA_FK_FW_ANNUAL;
                    rootObj.hedging_deal.fw_hedged_type = data.HDDA_FK_HEDG_TYPE;

                    rootObj.hedging_deal.tenor.start_date = (data.HDDA_TENOR_DATE_START == DateTime.MinValue || data.HDDA_TENOR_DATE_START == null) ? "" : Convert.ToDateTime(data.HDDA_TENOR_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    rootObj.hedging_deal.tenor.end_date = (data.HDDA_TENOR_DATE_END == DateTime.MinValue || data.HDDA_TENOR_DATE_END == null) ? "" : Convert.ToDateTime(data.HDDA_TENOR_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    if (rootObj.hedging_deal.tenor.start_date != null || rootObj.hedging_deal.tenor.end_date != null)
                        rootObj.hedging_deal.tenor.start_end_date = rootObj.hedging_deal.tenor.start_date + " to " + rootObj.hedging_deal.tenor.start_date;

                    rootObj.hedging_deal.tenor.result = data.HDDA_TENOR_RESULT;
                    rootObj.hedging_deal.tenor.extend_date = (data.HDDA_TENOR_DATE_EXT == null) ? "" : Convert.ToDateTime(data.HDDA_TENOR_DATE_EXT).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    rootObj.hedging_deal.tenor.excerise_date = (data.HDDA_EXCERISE_DATE == null) ? "" : Convert.ToDateTime(data.HDDA_EXCERISE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    rootObj.hedging_deal.tenor.result_extend = data.HDDA_TENOR_RESULT_EXT;

                    rootObj.hedging_deal.volume_month = data.HDDA_VOL_MNT.ToString();
                    rootObj.hedging_deal.volume_month_unit = data.HDDA_VOL_MNT_UNIT;
                    rootObj.hedging_deal.volume_month_total = data.HDDA_VOL_MNT_TOOL.ToString();
                    rootObj.hedging_deal.volume_month_total_unit = data.HDDA_VOL_MNT_TOOL_UNIT;
                    rootObj.hedging_deal.price = data.HDDA_PRICE.ToString();
                    rootObj.hedging_deal.unit_price = data.HDDA_PRICE_UNIT;
                    rootObj.hedging_deal.tool = data.HDDA_FK_TOOL;
                    rootObj.hedging_deal.payment_day = data.HDDA_PAYMENT_DAY.ToString();

                    rootObj.hedging_deal.payment_day_unit = data.HDDA_PAYMENT_DAY_UNIT;
                    rootObj.hedging_deal.note = data.HDDA_REMARK;

                    rootObj.hedging_deal.verify.credit_rating = data.HDDA_VRF_CREDIT_RT_STATUS;
                    rootObj.hedging_deal.verify.credit_rating_reason = data.HDDA_VRF_CREDIT_RT_REASON;
                    rootObj.hedging_deal.verify.credit_rating_file = data.HDDA_VRF_CREDIT_RT_FILE;
                    rootObj.hedging_deal.verify.cds = data.HDDA_VRF_CDS_STATUS;
                    rootObj.hedging_deal.verify.cds_reason = data.HDDA_VRF_CDS_REASON;
                    rootObj.hedging_deal.verify.cds_file = data.HDDA_VRF_CDS_FILE;
                    rootObj.hedging_deal.verify.credit_limit = data.HDDA_VRF_CREDIT_LM_STATUS;
                    rootObj.hedging_deal.verify.credit_limit_reason = data.HDDA_VRF_CREDIT_LM_REASON;
                    rootObj.hedging_deal.verify.credit_limit_file = data.HDDA_VRF_CREDIT_LM_FILE;
                    rootObj.hedging_deal.verify.annual_framework = data.HDDA_VRF_ANNUAL_FW_STATUS;
                    rootObj.hedging_deal.verify.annual_framework_reason = data.HDDA_VRF_ANNUAL_FW_REASON;
                    rootObj.hedging_deal.verify.annual_framework_file = data.HDDA_VRF_ANNUAL_FW_FILE;
                    rootObj.hedging_deal.verify.committee_framework = data.HDDA_VRF_COMMITTEE_FW_STATUS;
                    rootObj.hedging_deal.verify.committee_framework_reason = data.HDDA_VRF_COMMITTEE_FW_REASON;
                    rootObj.hedging_deal.verify.committee_framework_file = data.HDDA_VRF_COMMITTEE_FW_FILE;
                    rootObj.hedging_deal.verify.other_bypass = data.HDDA_VRF_OTHER_STATUS;
                    rootObj.hedging_deal.verify.other_reason = data.HDDA_VRF_OTHER_REASON;
                    rootObj.hedging_deal.verify.other_file = data.HDDA_VRF_OTHER_FILE;
                    rootObj.hedging_deal.reason = data.HDDA_REASON;

                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        HEDG_DEAL_CONTRACT_DAL CONTRACT_DAL = new HEDG_DEAL_CONTRACT_DAL();
                        List<HEDG_DEAL_CONTRACT> c_data = CONTRACT_DAL.GetDealContractByDealID(id, context);
                        foreach (var item in c_data)
                        {
                            Contract Contract_data = new Contract();
                            Contract_data.order = item.HDCT_ORDER;
                            Contract_data.contract_no = item.HDCT_CONTRACT_NO;
                            Contract_data.contract_file = item.HDCT_CONTRACT_FILE;
                            rootObj.hedging_deal.contract.Add(Contract_data);
                        }
                    }

                    HEDG_DEAL_BID_PRICE_DAL BID_DEAL = new HEDG_DEAL_BID_PRICE_DAL();
                    List<HEDG_DEAL_BID_PRICE> biddata = BID_DEAL.GetBIDPRICEByDataID(id);
                    foreach (var item in biddata)
                    {
                        OtherBidPrice BidPrice = new OtherBidPrice();
                        BidPrice.order = item.HDBP_ORDER;
                        BidPrice.price = item.HDBP_PRICE.ToString();
                        BidPrice.counter_parties = item.HDBP_FK_CONTER_PARTIES;
                        rootObj.hedging_deal.other_bid_price.Add(BidPrice);
                    }
                }
                return new JavaScriptSerializer().Serialize(rootObj);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string getFNTransactionIDByDealNo(string id)
        {
            string rtn = "";

            FunctionTransactionDAL dataDal = new FunctionTransactionDAL();
            List<FUNCTION_TRANSACTION> data = dataDal.findByTransactionIdDealNo(id);
            if (data != null)
            {
                rtn = data.FirstOrDefault().FTX_TRANS_ID + "|" + data.FirstOrDefault().FTX_INDEX2;
            }

            return rtn;
        }

        public ReturnValue AssignTicketNo(string ticketNo, string ticketId, string dealNo, string pUser, EntityCPAIEngine context)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_DEAL_DATA ent = new HEDG_DEAL_DATA();
            HEDG_DEAL_DATA_DAL dal = new HEDG_DEAL_DATA_DAL();
            FunctionTransactionDAL funcTran = new FunctionTransactionDAL();
            List<FUNCTION_TRANSACTION> listTran;
            FUNCTION_TRANSACTION modelTran;
            DateTime dt;

            try
            {
                try
                {
                    dt = new DateTime();
                    ent = dal.GetDealDataByDealNo(dealNo, context);
                    if (ent != null)
                    {
                        ent.HDDA_TICKET_NO = ticketNo.Trim();
                        ent.HDDA_FK_TICKET = ticketId.Trim();
                        ent.HDDA_UPDATED_BY = pUser;
                        ent.HDDA_UPDATED = dt;
                        dal.Update(ent, context);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////
                    listTran = funcTran.GetDealByDealNo(dealNo);
                    foreach (var item in listTran)
                    {
                        modelTran = item;
                        modelTran.FTX_INDEX7 = ticketNo.Trim();
                        modelTran.FTX_UPDATED_BY = pUser;
                        modelTran.FTX_UPDATED = dt;
                        funcTran.UpdateTicket(modelTran);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////                        
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue RemoveTicketNo(string dealNo, string pUser, EntityCPAIEngine context)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_DEAL_DATA ent = new HEDG_DEAL_DATA();
            HEDG_DEAL_DATA_DAL dal = new HEDG_DEAL_DATA_DAL();
            FunctionTransactionDAL funcTran = new FunctionTransactionDAL();
            List<FUNCTION_TRANSACTION> listTran;
            FUNCTION_TRANSACTION modelTran;
            DateTime dt;

            try
            {
                try
                {
                    dt = new DateTime();
                    ent = dal.GetDealDataByDealNo(dealNo, context);
                    if (ent != null)
                    {
                        ent.HDDA_TICKET_NO = string.Empty;
                        ent.HDDA_FK_TICKET = string.Empty;
                        ent.HDDA_UPDATED_BY = pUser;
                        ent.HDDA_UPDATED = dt;
                        dal.Update(ent, context);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////
                    listTran = funcTran.GetDealByDealNo(dealNo);
                    foreach (var item in listTran)
                    {
                        modelTran = item;
                        modelTran.FTX_INDEX7 = string.Empty;
                        modelTran.FTX_UPDATED_BY = pUser;
                        modelTran.FTX_UPDATED = dt;
                        funcTran.UpdateTicket(modelTran);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////                       
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue SendEmail(HedgingDealViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_DEAL_DATA entDeal = new HEDG_DEAL_DATA();
            HEDG_DEAL_CREATOR_EMAIL entEmail = new HEDG_DEAL_CREATOR_EMAIL();
            HEDG_DEAL_CREATOR_FILE entFile = new HEDG_DEAL_CREATOR_FILE();
            HEDG_DEAL_DATA_DAL dalDeal = new HEDG_DEAL_DATA_DAL();
            HEDG_DEAL_CREATOR_EMAIL_DAL dalCreatorEmail = new HEDG_DEAL_CREATOR_EMAIL_DAL();
            HEDG_DEAL_CREATOR_FILE_DAL dalCreatorFile = new HEDG_DEAL_CREATOR_FILE_DAL();
            DateTime dt;
            FileStream stream;
            string pathFile;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dt = DateTime.Now;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            
                            if (!File.Exists(pModel.sendMailFileUrl))
                            {
                                rtn.Status = false;
                                rtn.Message = "File not found [" + pModel.sendMailFile + "]";
                            }
                            else
                            {
                                stream = File.Open(pModel.sendMailFileUrl, FileMode.Open);                                
                                rtn = Mail(pModel.sendMailFrom.Trim(), pModel.sendMailTo.Trim(), pModel.sendMailCC.Trim(), pModel.sendMailSubject.Trim(), pModel.sendMailBody.Trim(), stream, pModel.sendMailFile.Trim());
                            }
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////                            
                            entEmail = new HEDG_DEAL_CREATOR_EMAIL();
                            entDeal = dalDeal.GetDealDataByDealNo(pModel.dealNo, context);
                            if (entDeal != null)
                                entEmail.HDCRM_FK_DEAL_CREATOR = entDeal.HDDA_ROW_ID;
                            else
                                entEmail.HDCRM_FK_DEAL_CREATOR = string.Empty;
                            entFile = dalCreatorFile.GetDealCreatorFileByFileName(pModel.sendMailFile, null);
                            if (entFile != null)
                                entEmail.HDCRM_FK_FILE = entFile.HDCRF_ROW_ID;
                            else
                                entEmail.HDCRM_FK_FILE = string.Empty;
                            entEmail.HDCRM_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                            entEmail.HDCRM_FROM = pModel.sendMailFrom.Trim();
                            entEmail.HDCRM_SEND_TO = pModel.sendMailTo.Trim();
                            entEmail.HDCRM_CC = pModel.sendMailCC.Trim();
                            entEmail.HDCRM_SUBJECT = pModel.sendMailSubject.Trim();
                            entEmail.HDCRM_BODY = pModel.sendMailBody.Trim();
                            entEmail.HDCRM_STATUS = rtn.Status.ToString();
                            entEmail.HDCRM_REMARK = rtn.Message;
                            entEmail.HDCRM_CREATED_BY = pUser;
                            entEmail.HDCRM_CREATED = dt;
                            entEmail.HDCRM_UPDATED_BY = pUser;
                            entEmail.HDCRM_UPDATED = dt;
                            dalCreatorEmail.Save(entEmail, context);                            
                            dbContextTransaction.Commit();
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////                            
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue SaveContact(HedgingDealViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_DEAL_DATA entDeal = new HEDG_DEAL_DATA();
            HEDG_DEAL_CREATOR entCreator = new HEDG_DEAL_CREATOR();            
            HEDG_DEAL_CREATOR_FILE entFile = new HEDG_DEAL_CREATOR_FILE();
            List<HEDG_DEAL_CREATOR_FILE> entFileList = new List<HEDG_DEAL_CREATOR_FILE>();
            HEDG_DEAL_DATA_DAL dalDeal = new HEDG_DEAL_DATA_DAL();
            HEDG_DEAL_CREATOR_DAL dalCreator = new HEDG_DEAL_CREATOR_DAL();           
            HEDG_DEAL_CREATOR_FILE_DAL dalCreatorFile = new HEDG_DEAL_CREATOR_FILE_DAL();
            DateTime dt;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dt = DateTime.Now;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            entCreator = dalCreator.GetDealCreatorByDealNo(pModel.dealNo, context);
                            if (entCreator != null && !string.IsNullOrEmpty(entCreator.HDCR_ROW_ID))
                            {
                                entCreator.HDCR_CONTENT = pModel.htmlPDF.Trim();
                                entCreator.HDCR_UPDATED_BY = pUser;
                                entCreator.HDCR_UPDATED = dt;
                                dalCreator.Update(entCreator, context);
                            }
                            else
                            {
                                entCreator = new HEDG_DEAL_CREATOR();
                                entDeal = dalDeal.GetDealDataByDealNo(pModel.dealNo, context);
                                if (entDeal != null)
                                    entCreator.HDCR_FK_DEAL = entDeal.HDDA_ROW_ID;
                                else
                                    entCreator.HDCR_FK_DEAL = string.Empty;
                                entCreator.HDCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCreator.HDCR_DEAL_NO = pModel.dealNo.Trim();
                                entCreator.HDCR_CONTRACT_NO = pModel.contractNo.Trim();
                                entCreator.HDCR_CONTENT = pModel.htmlPDF.Trim();
                                entCreator.HDCR_CREATED_BY = pUser;
                                entCreator.HDCR_CREATED = dt;
                                entCreator.HDCR_UPDATED_BY = pUser;
                                entCreator.HDCR_UPDATED = dt;
                                dalCreator.Save(entCreator, context);
                            }                            
                            entFile = new HEDG_DEAL_CREATOR_FILE();
                            entFile.HDCRF_FK_DEAL_CREATOR = entCreator.HDCR_ROW_ID;
                            entFile.HDCRF_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                            entFile.HDCRF_FILE_PATH = pModel.fileUrl.Trim();
                            entFile.HDCRF_FILE_NAME = pModel.fileContractPdf.Trim();
                            entFile.HDCRF_CREATED_BY = pUser;
                            entFile.HDCRF_CREATED = dt;
                            entFile.HDCRF_UPDATED_BY = pUser;
                            entFile.HDCRF_UPDATED = dt;
                            dalCreatorFile.Save(entFile, context);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            entFileList = dalCreatorFile.GetDealCreatorFileByDealId(entCreator.HDCR_ROW_ID, null);
                            if (entFileList != null)
                            {
                                pModel.contractPDFList = new List<HedgingDealContractPDF>();
                                foreach (var i in entFileList)
                                {
                                    pModel.contractPDFList.Add(new HedgingDealContractPDF
                                    {
                                        fileUrl = i.HDCRF_FILE_PATH,
                                        fileContractPdf = i.HDCRF_FILE_NAME,
                                        createDate = Convert.ToDateTime(i.HDCRF_CREATED),
                                        createBy = i.HDCRF_CREATED_BY
                                    });
                                }
                                if (pModel.contractPDFList.Count > 0)
                                    pModel.contractPDFList = pModel.contractPDFList.OrderByDescending(x => x.createDate).ToList();
                            }
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////                            
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }        

        public ReturnValue GetContact(ref HedgingDealViewModel model, string dealNo)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_DEAL_CREATOR entCreator = new HEDG_DEAL_CREATOR();
            HEDG_DEAL_CREATOR_EMAIL entEmail = new HEDG_DEAL_CREATOR_EMAIL();
            List<HEDG_DEAL_CREATOR_FILE> entFile = new List<HEDG_DEAL_CREATOR_FILE>();            
            HEDG_DEAL_CREATOR_DAL dalCreator = new HEDG_DEAL_CREATOR_DAL();
            HEDG_DEAL_CREATOR_EMAIL_DAL dalCreatorEmail = new HEDG_DEAL_CREATOR_EMAIL_DAL();
            HEDG_DEAL_CREATOR_FILE_DAL dalCreatorFile = new HEDG_DEAL_CREATOR_FILE_DAL();

            try
            {
                entCreator = dalCreator.GetDealCreatorByDealNo(dealNo, null);
                if (entCreator != null && !string.IsNullOrEmpty(entCreator.HDCR_ROW_ID))
                {
                    model = new HedgingDealViewModel();
                    model.dealNo = entCreator.HDCR_DEAL_NO;
                    model.contractNo = entCreator.HDCR_CONTRACT_NO;
                    model.htmlPDF = entCreator.HDCR_CONTENT;
                    entEmail = dalCreatorEmail.GetDealCreatorEmailByDealId(entCreator.HDCR_ROW_ID, null);
                    entFile = dalCreatorFile.GetDealCreatorFileByDealId(entCreator.HDCR_ROW_ID, null);
                    if (entEmail != null)
                    {
                        //model.htmlPDF = entEmail.HDCRM_CONTENT;
                    }
                    if (entFile != null)
                    {
                        model.contractPDFList = new List<HedgingDealContractPDF>();
                        foreach (var i in entFile)
                        {
                            model.contractPDFList.Add(new HedgingDealContractPDF
                            {
                                fileUrl = i.HDCRF_FILE_PATH,
                                fileContractPdf = i.HDCRF_FILE_NAME,
                                createDate = Convert.ToDateTime(i.HDCRF_CREATED),
                                createBy = i.HDCRF_CREATED_BY
                            });
                        }
                        if (model.contractPDFList.Count > 0)
                            model.contractPDFList = model.contractPDFList.OrderByDescending(x => x.createDate).ToList();
                    }
                    rtn.Status = true;
                    rtn.Message = "Success";
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                }
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;                
            }

            return rtn;
        }        

        public ReturnValue Search(ref HedgingDealViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            RequestCPAI req = new RequestCPAI();
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService projService = new ServiceProvider.ProjService();
            List<Transaction> _transaction = new List<Flow.Model.Transaction>();
            List_TCEtrx _model;
            string[] lstStatus;
            string _strfrom, _strTo, sDealNo, sTicketNo, sContactNo, sStatus, sHedgedType, sTrade_For, sTool, sSeller, sBuyer, sUnderlying, sUnderlyingVS, sCreateBy, sVolumeMonth_From, sVolumeMonth_To, sPrice_From, sPrice_To;

            try
            {
                try
                {
                    _strfrom = String.IsNullOrEmpty(pModel.sDate) ? string.Empty : pModel.sDate.Trim().Substring(0, 10);
                    _strTo = String.IsNullOrEmpty(pModel.sDate) ? string.Empty : pModel.sDate.Trim().Substring(14, 10);
                    _strfrom = _strfrom.Replace("-", "/");
                    _strTo = _strTo.Replace("-", "/");
                    sDealNo = String.IsNullOrEmpty(pModel.sDeal_no) ? string.Empty : pModel.sDeal_no.Trim().ToLower();
                    sTicketNo = String.IsNullOrEmpty(pModel.sTicket_no) ? string.Empty : pModel.sTicket_no.Trim().ToLower();
                    sContactNo = String.IsNullOrEmpty(pModel.sContactNo) ? string.Empty : pModel.sContactNo.Trim().ToLower();
                    sStatus = String.IsNullOrEmpty(pModel.strStatus) ? string.Empty : pModel.strStatus.Trim().ToLower();
                    lstStatus = String.IsNullOrEmpty(pModel.sStatusSelected) ? lstStatus = new string[0] : pModel.sStatusSelected.Trim().ToLower().Split(char.Parse("|"));
                    sHedgedType = String.IsNullOrEmpty(pModel.strHedgedType) ? string.Empty : pModel.strHedgedType.Trim().ToLower();
                    sTrade_For = String.IsNullOrEmpty(pModel.sTrade_For) ? string.Empty : pModel.sTrade_For.Trim().ToLower();
                    sTool = String.IsNullOrEmpty(pModel.strTool) ? string.Empty : pModel.strTool.Trim().ToLower();
                    sSeller = String.IsNullOrEmpty(pModel.strSeller) ? string.Empty : pModel.strSeller.Trim().ToLower();
                    sBuyer = String.IsNullOrEmpty(pModel.strBuyer) ? string.Empty : pModel.strBuyer.Trim().ToLower();
                    sUnderlying = String.IsNullOrEmpty(pModel.strUnderlying) ? string.Empty : pModel.strUnderlying.Trim().ToLower();
                    sUnderlyingVS = String.IsNullOrEmpty(pModel.strVS) ? string.Empty : pModel.strVS.Trim().ToLower();
                    sCreateBy = String.IsNullOrEmpty(pModel.sCreateBy) ? string.Empty : pModel.sCreateBy.Trim().ToLower();
                    sVolumeMonth_From = String.IsNullOrEmpty(pModel.sVolumeMonth_From) ? string.Empty : pModel.sVolumeMonth_From.Trim().ToLower();
                    sVolumeMonth_To = String.IsNullOrEmpty(pModel.sVolumeMonth_To) ? string.Empty : pModel.sVolumeMonth_To.Trim().ToLower();
                    sPrice_From = String.IsNullOrEmpty(pModel.sPrice_From) ? string.Empty : pModel.sPrice_From.Trim().ToLower();
                    sPrice_To = String.IsNullOrEmpty(pModel.sPrice_To) ? string.Empty : pModel.sPrice_To.Trim().ToLower();
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                req.Function_id = ConstantPrm.FUNCTION.F10000048;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_DEAL });
                req.Req_parameters.P.Add(new P { K = "type", V = pModel.systemType });
                req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
                req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
                req.Req_parameters.P.Add(new P { K = "status", V = sStatus });
                req.Req_parameters.P.Add(new P { K = "from_date", V = _strfrom });
                req.Req_parameters.P.Add(new P { K = "to_date", V = _strTo });
                //req.Req_parameters.P.Add(new P { K = "index_5", V = (_strfrom != "" && _strTo != "") ? _strfrom + "|" + _strTo : "" });
                req.Extra_xml = "";

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = projService.CallService(reqData);
                if (!resData.result_code.Trim().Equals("1"))
                {
                    rtn.Status = false;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
                _model = ShareFunction.DeserializeXMLFileToObject<List_TCEtrx>(resData.extra_xml);
                if (_model == null || _model.TCETransaction == null || _model.TCETransaction.Count < 1)
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                    return rtn;
                }
                _transaction = _model.TCETransaction;
                if (_transaction == null || _transaction.Count < 1)
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                    return rtn;
                }
                _transaction = _transaction.OrderByDescending(a => a.deal_no).ToList();
                if (!String.IsNullOrEmpty(sDealNo))
                    _transaction = _transaction.Where(x => x.deal_no != null && x.deal_no.Trim().ToLower().Contains(sDealNo)).ToList();
                if (!String.IsNullOrEmpty(sTicketNo))
                    _transaction = _transaction.Where(x => x.ticket_no != null && x.ticket_no.Trim().ToLower().Contains(sTicketNo)).ToList();
                if (!String.IsNullOrEmpty(sContactNo))
                    _transaction = _transaction.Where(x => x.contact_no != null && x.ticket_no.Trim().ToLower().Contains(sContactNo)).ToList();
                if (!String.IsNullOrEmpty(sHedgedType))
                    _transaction = _transaction.Where(x => x.hedg_type != null && x.hedg_type.Trim().ToLower().Equals(sHedgedType)).ToList();
                if (!String.IsNullOrEmpty(sTrade_For))
                    _transaction = _transaction.Where(x => x.trade_for != null && x.trade_for.Trim().ToLower().Equals(sTrade_For)).ToList();
                if (!String.IsNullOrEmpty(sTool))
                    _transaction = _transaction.Where(x => x.tool != null && x.tool.Trim().ToLower().Equals(sTool)).ToList();
                if (!String.IsNullOrEmpty(sSeller))
                    _transaction = _transaction.Where(x => x.seller != null && x.seller.Trim().ToLower().Equals(sSeller)).ToList();
                if (!String.IsNullOrEmpty(sBuyer))
                    _transaction = _transaction.Where(x => x.buyer != null && x.buyer.Trim().ToLower().Equals(sBuyer)).ToList();
                if (!String.IsNullOrEmpty(sUnderlying))
                    _transaction = _transaction.Where(x => x.underlying != null && x.underlying.Trim().ToLower().Equals(sUnderlying)).ToList();
                if (!String.IsNullOrEmpty(sUnderlyingVS))
                    _transaction = _transaction.Where(x => x.underlying_vs != null && x.underlying_vs.Trim().ToLower().Equals(sUnderlyingVS)).ToList();
                if (!String.IsNullOrEmpty(sCreateBy))
                    _transaction = _transaction.Where(x => x.create_by != null && x.create_by.Trim().ToLower().Equals(sCreateBy)).ToList();
                if (!string.IsNullOrEmpty(sVolumeMonth_From) && !string.IsNullOrEmpty(sVolumeMonth_To))
                    _transaction = _transaction.Where(x => x.volume_month != null && (Convert.ToDecimal(x.volume_month) >= Convert.ToDecimal(sVolumeMonth_From) && Convert.ToDecimal(x.volume_month) <= Convert.ToDecimal(sVolumeMonth_To))).ToList();
                if (!string.IsNullOrEmpty(sPrice_From) && !string.IsNullOrEmpty(sPrice_To))
                    _transaction = _transaction.Where(x => x.price != null && (Convert.ToDecimal(x.price) >= Convert.ToDecimal(sPrice_From) && Convert.ToDecimal(x.price) <= Convert.ToDecimal(sPrice_To))).ToList();
                if (!string.IsNullOrEmpty(_strfrom) && !string.IsNullOrEmpty(_strTo))
                {
                    DateTime sDate = new DateTime();
                    DateTime eDate = new DateTime();
                    sDate = ShareFn.ConvertStrDateToDate(_strfrom);
                    eDate = ShareFn.ConvertStrDateToDate(_strTo).AddDays(1).AddSeconds(-1);
                    _transaction = _transaction.Where(x => x.deal_date != null && Convert.ToDateTime(x.deal_date) >= sDate && Convert.ToDateTime(x.deal_date) <= eDate).ToList();
                }  
                if(lstStatus.Length > 0)                         
                    _transaction = _transaction.Where(x => x.status_tracking != null && lstStatus.Contains(x.status_tracking.Trim().ToLower())).ToList();                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////                             
                pModel.sSearchData = new List<HedgingDealViewModel_SearchData>();
                foreach (var i in _transaction)
                {
                    pModel.sSearchData.Add(new HedgingDealViewModel_SearchData
                    {
                        hTransactionIdEncrypted = i.transaction_id.Encrypt(),
                        hTransactionRequestEncrypted = i.req_transaction_id.Encrypt(),
                        hSystemEncrypted = i.system.Encrypt(),
                        hTypeEncrypted = i.type.Encrypt(),
                        hScreenTypeEncrypted = ("DEAL").Encrypt(),
                        hDealEncrypted = i.deal_no.Encrypt(),

                        dStatus = i.status_tracking,
                        dDate = i.deal_date,
                        dDealNo = i.deal_no,
                        dTicketNo = i.ticket_no,
                        dContactNo = i.contact_no,
                        dHedged_Type = i.hedg_type,
                        dTrade_For = i.trade_for,
                        dTool = i.tool,
                        dUnderlying = i.underlying,
                        dSeller = i.seller,
                        dBuyer = i.buyer,
                        dCreatedBy = i.create_by
                    });
                }
                rtn.Status = true;
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }

            return rtn;
        }

        public ReturnValue VerifyCreditLimit(string CounterParty, Decimal Amount)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if(Amount > 2000000)
                {
                    rtn.Message = "Over 2,000,000";
                    rtn.Status = false;
                }
                else
                {
                    rtn.Message = "Verify pass";
                    rtn.Status = true;
                }
              
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtn;
        }

        public ReturnValue VerifyCreditRate(string CounterParty, Decimal Amount)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (Amount > 1000000)
                {
                    rtn.Message = "Over 1,000,000";
                    rtn.Status = false;
                }
                else
                {
                    rtn.Message = "Verify pass";
                    rtn.Status = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtn;
        }

        public ReturnValue VerifyCDS(string CounterParty, Decimal Amount)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (Amount > 3000000)
                {
                    rtn.Message = "Over 3,000,000";
                    rtn.Status = false;
                }
                else
                {
                    rtn.Message = "Verify pass";
                    rtn.Status = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtn;
        }

        public ReturnValue VerifyAnnualFramework(string CounterParty, Decimal Amount)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (Amount > 2000)
                {
                    rtn.Message = "Over 2,000";
                    rtn.Status = false;
                }
                else
                {
                    rtn.Message = "Verify pass";
                    rtn.Status = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtn;
        }

        public ReturnValue VerifyCommitteeFramework(string CounterParty, Decimal Amount)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (Amount > 1000)
                {
                    rtn.Message = "Over 1,000";
                    rtn.Status = false;
                }
                else
                {
                    rtn.Message = "Verify pass";
                    rtn.Status = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtn;
        }

        public ReturnValue Verify(string verifyType, string dealNo, bool SetHold, string pUser, EntityCPAIEngine context)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_DEAL_DATA ent = new HEDG_DEAL_DATA();
            HEDG_DEAL_DATA_DAL dal = new HEDG_DEAL_DATA_DAL();
            FunctionTransactionDAL funcTran = new FunctionTransactionDAL();
            List<FUNCTION_TRANSACTION> listTran;
            FUNCTION_TRANSACTION modelTran;
            DateTime dt;

            try
            {
                try
                {
                    dt = new DateTime();
                    ent = dal.GetDealDataByDealNo(dealNo, context);
                    if (ent != null)
                    {
                        //ent.HDDA_TICKET_NO = ticketNo.Trim();
                        //ent.HDDA_FK_TICKET = ticketId.Trim();
                        //ent.HDDA_UPDATED_BY = pUser;
                        //ent.HDDA_UPDATED = dt;
                        dal.Update(ent, context);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////
                    listTran = funcTran.GetDealByDealNo(dealNo);
                    foreach (var item in listTran)
                    {
                        //modelTran = item;
                        //modelTran.FTX_INDEX7 = ticketNo.Trim();
                        //modelTran.FTX_UPDATED_BY = pUser;
                        //modelTran.FTX_UPDATED = dt;
                        //funcTran.UpdateTicket(modelTran);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////                        
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public string GetStatusByDealNo(string dealNo)
        {
            HEDG_DEAL_DATA DEAL = new HEDG_DEAL_DATA();
            HEDG_DEAL_DATA_DAL DAL = new HEDG_DEAL_DATA_DAL();
            using (var context = new EntityCPAIEngine())
            {
                DEAL = DAL.GetDealDataByDealNo(dealNo,context);
            }
            return DEAL.HDDA_STATUS;
        }
    }
}
