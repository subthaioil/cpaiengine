﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Globalization;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingFrameworkServiceModel
    {        
        private DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ReturnValue ValidateSubmit(EntityCPAIEngine context, HedgingFrameworkViewModel_Detail pModel)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            rtn.Message = string.Empty;
            ShareFn shareFn = new ShareFn();
            string fromDate = shareFn.ConvertDateFormat(pModel.ActiveDate.ToUpper().Substring(0, 10), true).Replace("-", "/");
            string toDate = shareFn.ConvertDateFormat(pModel.ActiveDate.ToUpper().Substring(14, 10), true).Replace("-", "/");
            DateTime fDate = parseInputDate(fromDate);
            DateTime tDate = parseInputDate(toDate);
            List<HEDG_FW_ANNUAL> query;

            if (!CPAIConstantUtil.ACTION_SUBMIT.ToLower().Contains(pModel.Status.ToLower()))
                return rtn;

            var querySubmit = context.HEDG_FW_ANNUAL.Where(p => p.HFT_STATUS.Equals(ConstantPrm.ACTION.SUBMIT));
            if (pModel.IsChange == "1") {
                if (querySubmit != null && querySubmit.Count() > 0)
                {
                    query = querySubmit.Where(p => p.HFT_FK_UNDERLYING.ToLower().Contains(pModel.Underlying.ToLower())
                                   && ((DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_START) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_START) <= DbFunctions.TruncateTime(tDate))
                                       || (DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_END) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_END) <= DbFunctions.TruncateTime(tDate)))
                                   ).ToList();
                    if (query != null && query.Count() > 0)
                    {
                        rtn.Status = false;
                        rtn.Message = "Underlying or Date is duplicate";
                    }
                    else
                    {
                        query = querySubmit.Where(p => p.HFT_FK_UNDERLYING.ToLower().Contains(pModel.Underlying.ToLower())
                                        && ((DbFunctions.TruncateTime(fDate) >= DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_START) && DbFunctions.TruncateTime(tDate) <= DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_END))
                                            || (DbFunctions.TruncateTime(fDate) <= DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_START) && DbFunctions.TruncateTime(tDate) >= DbFunctions.TruncateTime(p.HFT_ACTIVEDATE_END)))
                                        ).ToList();
                        if (query != null && query.Count() > 0)
                        {
                            rtn.Status = false;
                            rtn.Message = "Underlying or Date is duplicate";
                        }
                    }
                }
            }

           

            return rtn;
        }

        private ReturnValue ValidateInput(ref HedgingFrameworkViewModel_Detail pModel, string pUser, bool add)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            rtn.Message = string.Empty;

            if (string.IsNullOrEmpty(pUser))
            {
                rtn.Message = "User should not be empty";
                rtn.Status = false;
            }
            else if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
            }
            else if (string.IsNullOrEmpty(pModel.Status))
            {
                rtn.Message = "Status should not be empty";
                rtn.Status = false;
            }
            else if (pModel.Status == ConstantPrm.ACTION.CANCEL)
            {
                rtn.Status = true;
                return rtn;
            }
            else if (pModel.Status == ConstantPrm.ACTION.SUBMIT)
            {
                if (string.IsNullOrEmpty(pModel.ActiveDate))
                {
                    rtn.Message = "ActiveDate should not be empty";
                    rtn.Status = false;
                }
                else if (string.IsNullOrEmpty(pModel.Underlying))
                {
                    rtn.Message = "Underlying should not be empty";
                    rtn.Status = false;
                }
                else if (string.IsNullOrEmpty(pModel.Description))
                {
                    rtn.Message = "Description should not be empty";
                    rtn.Status = false;
                }
                else if (string.IsNullOrEmpty(pModel.Remark))
                {
                    rtn.Message = "Remark should not be empty";
                    rtn.Status = false;
                }
                else if (pModel.DetailData != null) {
                    foreach (var line in pModel.DetailData.GroupBy(info => info.HedgeType).Select(group => new { Metric = group.Key, Count = group.Count() }))
                    {
                        if (line.Count > 2)
                        {
                            rtn.Message = "HedgeType is Duplicate";
                            rtn.Status = false;
                            break;
                        }
                    }
                }
                else if (pModel.DetailData != null) {
                    foreach (var item in pModel.DetailData)
                    {
                        if (string.IsNullOrEmpty(item.Order))
                        {
                            rtn.Message = "Order should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.HedgeType))
                        {
                            rtn.Message = "HedgeType should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.Type))
                        {
                            rtn.Message = "Type should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.Title))
                        {
                            rtn.Message = "Title should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.ValueType))
                        {
                            rtn.Message = "ValueType should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.PriceUnit))
                        {
                            rtn.Message = "PriceUnit should not be empty";
                            rtn.Status = false;
                            break;
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////mode edit
            if (!add)
            {
                if (string.IsNullOrEmpty(pModel.OrderID))
                {
                    rtn.Message = "Order should not be empty";
                    rtn.Status = false;
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            if (rtn.Status)
            {
                try
                {
                    pModel.ActiveDate.ToUpper().Substring(0, 10);
                    pModel.ActiveDate.ToUpper().Substring(14, 10);
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Active date invalid format";
                }
            }


            return rtn;
        }                
               
        public ReturnValue Add(ref HedgingFrameworkViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                rtn = ValidateInput(ref pModel, pUser, true);
                if (!rtn.Status)
                    return rtn;

                HEDG_FW_ANNUAL_DAL dal = new HEDG_FW_ANNUAL_DAL();
                HEDG_FW_ANNUAL ent = new HEDG_FW_ANNUAL();
                HEDG_FW_ANNUAL_DETAIL_DAL dalDetail = new HEDG_FW_ANNUAL_DETAIL_DAL();
                HEDG_FW_ANNUAL_DETAIL entDetail = new HEDG_FW_ANNUAL_DETAIL();
                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            rtn = ValidateSubmit(context, pModel);
                            if (!rtn.Status)
                                return rtn;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            var CtrlID = ConstantPrm.GetDynamicCtrlID();
                            ent.HFT_ROW_ID = CtrlID;
                            ent.HFT_ACTIVEDATE_START = ShareFn.ConvertStrDateToDate(pModel.ActiveDate.ToUpper().Substring(0, 10));
                            ent.HFT_ACTIVEDATE_END = ShareFn.ConvertStrDateToDate(pModel.ActiveDate.ToUpper().Substring(14, 10));
                            ent.HFT_FK_UNDERLYING = pModel.Underlying;
                            ent.HFT_DESC = pModel.Description;
                            ent.HFT_REMARK = pModel.Remark;
                            ent.HFT_STATUS = pModel.Status;
                            ent.HFT_CREATED_BY = pUser;
                            ent.HFT_CREATED_DATE = now;
                            ent.HFT_UPDATED_BY = pUser;
                            ent.HFT_UPDATED_DATE = now;
                            dal.Save(ent, context);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            foreach (var item in pModel.DetailData)
                            {
                                entDetail = new HEDG_FW_ANNUAL_DETAIL();
                                entDetail.HFTD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entDetail.HFTD_FK_FW_ANNUAL = CtrlID;
                                entDetail.HFTD_ORDER = item.Order;
                                entDetail.HFTD_HEDGE_TYPE = item.HedgeType;
                                entDetail.HFTD_TYPE = item.Type;
                                entDetail.HFTD_TITLE = item.Title;
                                entDetail.HFTD_VALUE_TYPE = item.ValueType;
                                entDetail.HFTD_VOLUME = item.Volume;
                                entDetail.HFTD_VOLUME_UNIT = item.VolumeUnit;
                                entDetail.HFTD_PRICE1 = item.Price1;
                                entDetail.HFTD_PRICE1_REMARK = item.Price1Remark;
                                entDetail.HFTD_PRICE2 = item.Price2;
                                entDetail.HFTD_PRICE2_REMARK = item.Price2Remark;
                                entDetail.HFTD_PRICE3 = item.Price3;
                                entDetail.HFTD_PRICE3_REMARK = item.Price3Remark;
                                entDetail.HFTD_PRICE_UNIT = item.PriceUnit;
                                entDetail.HFTD_CREATED_BY = pUser;
                                entDetail.HFTD_CREATED_DATE = now;
                                entDetail.HFTD_UPDATED_BY = pUser;
                                entDetail.HFTD_UPDATED_DATE = now;
                                dalDetail.Save(entDetail, context);
                            }
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            pModel.OrderID = CtrlID;
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;                            
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(HedgingFrameworkViewModel_Detail pModel, string pUser,string preStatus = "")
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                rtn = ValidateInput(ref pModel, pUser, false);
                if (!rtn.Status)
                    return rtn;

                HEDG_FW_ANNUAL_DAL dal = new HEDG_FW_ANNUAL_DAL();
                HEDG_FW_ANNUAL ent = new HEDG_FW_ANNUAL();
                HEDG_FW_ANNUAL_DETAIL_DAL dalDetail = new HEDG_FW_ANNUAL_DETAIL_DAL();
                HEDG_FW_ANNUAL_DETAIL entDetail = new HEDG_FW_ANNUAL_DETAIL();
                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            rtn = ValidateSubmit(context, pModel);
                            if (!rtn.Status)
                                return rtn;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            ent.HFT_ROW_ID = pModel.OrderID;
                            ent.HFT_ACTIVEDATE_START = ShareFn.ConvertStrDateToDate(pModel.ActiveDate.ToUpper().Substring(0, 10));
                            ent.HFT_ACTIVEDATE_END = ShareFn.ConvertStrDateToDate(pModel.ActiveDate.ToUpper().Substring(14, 10));
                            ent.HFT_FK_UNDERLYING = pModel.Underlying;
                            ent.HFT_DESC = pModel.Description;
                            ent.HFT_REMARK = pModel.Remark;
                            ent.HFT_STATUS = pModel.Status;                            
                            ent.HFT_UPDATED_BY = pUser;
                            ent.HFT_UPDATED_DATE = now;
                            dal.Update(ent, context);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            if(ent.HFT_STATUS == ConstantPrm.ACTION.DRAFT)
                            {
                                dalDetail.DeleteByAnnualID(ent.HFT_ROW_ID, context);

                                foreach (var item in pModel.DetailData)
                                {
                                    entDetail = new HEDG_FW_ANNUAL_DETAIL();
   
                                    entDetail.HFTD_FK_FW_ANNUAL = pModel.OrderID;
                                    entDetail.HFTD_ORDER = item.Order;
                                    entDetail.HFTD_HEDGE_TYPE = item.HedgeType;
                                    entDetail.HFTD_TYPE = item.Type;
                                    entDetail.HFTD_TITLE = item.Title;
                                    entDetail.HFTD_VALUE_TYPE = item.ValueType;
                                    entDetail.HFTD_VOLUME = item.Volume;
                                    entDetail.HFTD_VOLUME_UNIT = item.VolumeUnit;
                                    entDetail.HFTD_PRICE1 = item.Price1;
                                    entDetail.HFTD_PRICE1_REMARK = item.Price1Remark;
                                    entDetail.HFTD_PRICE2 = item.Price2;
                                    entDetail.HFTD_PRICE2_REMARK = item.Price2Remark;
                                    entDetail.HFTD_PRICE3 = item.Price3;
                                    entDetail.HFTD_PRICE3_REMARK = item.Price3Remark;
                                    entDetail.HFTD_PRICE_UNIT = item.PriceUnit;
                                    
                                    entDetail.HFTD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entDetail.HFTD_CREATED_BY = pUser;
                                    entDetail.HFTD_CREATED_DATE = now;
                                    entDetail.HFTD_UPDATED_BY = pUser;
                                    entDetail.HFTD_UPDATED_DATE = now;
                                    dalDetail.Save(entDetail, context);

                                }
                            }
                            else if (preStatus == "DRAFT" && ent.HFT_STATUS == ConstantPrm.ACTION.SUBMIT)
                            {
                                dalDetail.DeleteByAnnualID(ent.HFT_ROW_ID, context);

                                foreach (var item in pModel.DetailData)
                                {
                                    entDetail = new HEDG_FW_ANNUAL_DETAIL();

                                    entDetail.HFTD_FK_FW_ANNUAL = pModel.OrderID;
                                    entDetail.HFTD_ORDER = item.Order;
                                    entDetail.HFTD_HEDGE_TYPE = item.HedgeType;
                                    entDetail.HFTD_TYPE = item.Type;
                                    entDetail.HFTD_TITLE = item.Title;
                                    entDetail.HFTD_VALUE_TYPE = item.ValueType;
                                    entDetail.HFTD_VOLUME = item.Volume;
                                    entDetail.HFTD_VOLUME_UNIT = item.VolumeUnit;
                                    entDetail.HFTD_PRICE1 = item.Price1;
                                    entDetail.HFTD_PRICE1_REMARK = item.Price1Remark;
                                    entDetail.HFTD_PRICE2 = item.Price2;
                                    entDetail.HFTD_PRICE2_REMARK = item.Price2Remark;
                                    entDetail.HFTD_PRICE3 = item.Price3;
                                    entDetail.HFTD_PRICE3_REMARK = item.Price3Remark;
                                    entDetail.HFTD_PRICE_UNIT = item.PriceUnit;

                                    entDetail.HFTD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entDetail.HFTD_CREATED_BY = pUser;
                                    entDetail.HFTD_CREATED_DATE = now;
                                    entDetail.HFTD_UPDATED_BY = pUser;
                                    entDetail.HFTD_UPDATED_DATE = now;
                                    dalDetail.Save(entDetail, context);

                                }
                            }
                            else
                            {
                                foreach (var item in pModel.DetailData)
                                {
                                    entDetail = new HEDG_FW_ANNUAL_DETAIL();
                                    entDetail.HFTD_ROW_ID = item.RowID;
                                    entDetail.HFTD_FK_FW_ANNUAL = pModel.OrderID;
                                    entDetail.HFTD_ORDER = item.Order;
                                    entDetail.HFTD_HEDGE_TYPE = item.HedgeType;
                                    entDetail.HFTD_TYPE = item.Type;
                                    entDetail.HFTD_TITLE = item.Title;
                                    entDetail.HFTD_VALUE_TYPE = item.ValueType;
                                    entDetail.HFTD_VOLUME = item.Volume;
                                    entDetail.HFTD_VOLUME_UNIT = item.VolumeUnit;
                                    entDetail.HFTD_PRICE1 = item.Price1;
                                    entDetail.HFTD_PRICE1_REMARK = item.Price1Remark;
                                    entDetail.HFTD_PRICE2 = item.Price2;
                                    entDetail.HFTD_PRICE2_REMARK = item.Price2Remark;
                                    entDetail.HFTD_PRICE3 = item.Price3;
                                    entDetail.HFTD_PRICE3_REMARK = item.Price3Remark;
                                    entDetail.HFTD_PRICE_UNIT = item.PriceUnit;
                                    if (string.IsNullOrEmpty(entDetail.HFTD_ROW_ID))
                                    {
                                        entDetail.HFTD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        entDetail.HFTD_CREATED_BY = pUser;
                                        entDetail.HFTD_CREATED_DATE = now;
                                        entDetail.HFTD_UPDATED_BY = pUser;
                                        entDetail.HFTD_UPDATED_DATE = now;
                                        dalDetail.Save(entDetail, context);
                                    }
                                    else
                                    {
                                        entDetail.HFTD_UPDATED_BY = pUser;
                                        entDetail.HFTD_UPDATED_DATE = now;
                                        dalDetail.Update(entDetail, context);
                                    }
                                }
                            }
                              
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Cancel(HedgingFrameworkViewModel_Detail pModel, string pUser, string pReason)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (string.IsNullOrEmpty(pReason))
                {
                    rtn.Message = "Reason not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                rtn = ValidateInput(ref pModel, pUser, false);
                if (!rtn.Status)
                    return rtn;

                HEDG_FW_ANNUAL_DAL dal = new HEDG_FW_ANNUAL_DAL();
                HEDG_FW_ANNUAL ent = new HEDG_FW_ANNUAL();              
                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {                          
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            ent.HFT_ROW_ID = pModel.OrderID;
                            ent.HFT_STATUS = pModel.Status;
                            ent.HFT_REASON = pReason;
                            ent.HFT_UPDATED_BY = pUser;
                            ent.HFT_UPDATED_DATE = now;
                            dal.Cancel(ent, context);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref HedgingFrameworkViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            string sDateFrom, sDateTo, sUnderlying, sStatus;
            try
            {
                try
                {
                    sDateFrom = String.IsNullOrEmpty(pModel.sActiveDate) ? "" : pModel.sActiveDate.ToUpper().Substring(0, 10);
                    sDateTo = String.IsNullOrEmpty(pModel.sActiveDate) ? "" : pModel.sActiveDate.ToUpper().Substring(14, 10);
                    sUnderlying = String.IsNullOrEmpty(pModel.sUnderlying) ? "" : pModel.sUnderlying.ToUpper();
                    sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {                    
                    var query = (from v in context.HEDG_FW_ANNUAL
                                 join vd in context.MT_MATERIALS on v.HFT_FK_UNDERLYING equals vd.MET_NUM into viewD
                                 from vdl in viewD.DefaultIfEmpty()
                                 join vc in context.MT_MATERIALS_CONTROL on vdl.MET_NUM equals vc.MMC_FK_MATRIALS into viewC
                                 from vcl in viewC.DefaultIfEmpty()
                                 where vcl.MMC_SYSTEM == "HEDG"
                                 orderby new { v.HFT_ACTIVEDATE_START, v.HFT_ACTIVEDATE_END, v.HFT_STATUS }
                                 select new
                                 {
                                     dOrderID = v.HFT_ROW_ID
                                     ,
                                     dActiveDateStart = (DateTime?)v.HFT_ACTIVEDATE_START                                    
                                     ,
                                     dActiveDateEnd = (DateTime?)v.HFT_ACTIVEDATE_END
                                     ,
                                     dUnderlying = string.IsNullOrEmpty(vcl.MMC_NAME) ? vdl.MET_MAT_DES_ENGLISH : vcl.MMC_NAME
                                     ,
                                     dDescription = v.HFT_DESC
                                     ,
                                     dStatus = v.HFT_STATUS
                                     ,
                                     dCreateDate = (DateTime?)v.HFT_CREATED_DATE
                                     ,
                                     dCreateBy = v.HFT_CREATED_BY
                                 });  
                    if (!string.IsNullOrEmpty(sUnderlying))
                        query = query.Where(p => p.dUnderlying.ToUpper().Contains(sUnderlying));
                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));
                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dActiveDateStart >= sDate && p.dActiveDateEnd <= eDate);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (query != null)
                    {
                        pModel.sSearchData = new List<HedgingFrameworkViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            var startdate = g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            var enddate = g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            pModel.sSearchData.Add(
                                                    new HedgingFrameworkViewModel_SearchData
                                                    {
                                                        dOrderID = g.dOrderID,
                                                        dActiveDate = startdate + " To " + enddate ,                                                        
                                                        dUnderlying = g.dUnderlying,
                                                        dDescription = g.dDescription,
                                                        dStatus = g.dStatus,
                                                        dCreateDate = g.dCreateDate == null ? "" : Convert.ToDateTime(g.dCreateDate).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dCreateBy = g.dCreateBy
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }                
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HedgingFrameworkViewModel_Detail GetDetail(string pOrderID)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingFrameworkViewModel_Detail detailData = new HedgingFrameworkViewModel_Detail();            
            string sOrder;
            try
            {
                sOrder = String.IsNullOrEmpty(pOrderID) ? "" : pOrderID;
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Edit by ice : 01/06/2017
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_FW_ANNUAL
                                 where v.HFT_ROW_ID.Equals(sOrder)
                                 select new {
                                     dOrderID = v.HFT_ROW_ID
                                     ,
                                     dActiveDateStart = (DateTime?)v.HFT_ACTIVEDATE_START
                                     ,
                                     dActiveDateEnd = (DateTime?)v.HFT_ACTIVEDATE_END
                                     ,
                                     dUnderlying = v.HFT_FK_UNDERLYING
                                     ,
                                     dDescription = v.HFT_DESC
                                     ,
                                     dRemark = v.HFT_REMARK
                                     ,
                                     dStatus = v.HFT_STATUS
                                     ,
                                     dCreateDate = (DateTime?)v.HFT_CREATED_DATE
                                     ,
                                     dCreateBy = v.HFT_CREATED_BY
                                     ,
                                     dReason = v.HFT_REASON
                                 }
                    );
                    if (query != null)
                    {
                        int i = 1;
                        foreach (var g in query.ToList())
                        {
                            if (i == 1)
                            {
                                string startDate = g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                string endDate = g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                                detailData.OrderID = g.dOrderID;
                                detailData.ActiveDate = startDate + " To " + endDate;
                                detailData.Underlying = g.dUnderlying;
                                detailData.Description = g.dDescription;
                                detailData.Remark = g.dRemark;
                                detailData.Status = g.dStatus;
                                detailData.CancelReamark = g.dReason;
                                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                var queryDetail = (from v in context.HEDG_FW_ANNUAL_DETAIL
                                                   orderby v.HFTD_ORDER
                                                   where v.HFTD_FK_FW_ANNUAL.Equals(pOrderID)
                                                   select v);
                                if(queryDetail != null)
                                {
                                    detailData.DetailData = new List<HedgingFrameworkViewModel_DetailData>();
                                    foreach (var list in queryDetail)
                                    {
                                        detailData.DetailData.Add(new HedgingFrameworkViewModel_DetailData
                                        {
                                            RowID = list.HFTD_ROW_ID,
                                            Order = list.HFTD_ORDER,
                                            HedgeType = list.HFTD_HEDGE_TYPE,
                                            Title = list.HFTD_TITLE,
                                            Type = list.HFTD_TYPE,
                                            ValueType = list.HFTD_VALUE_TYPE,
                                            Volume = list.HFTD_VOLUME,
                                            VolumeUnit = list.HFTD_VOLUME_UNIT,
                                            Price1 = list.HFTD_PRICE1,
                                            Price1Remark = list.HFTD_PRICE1_REMARK,
                                            Price2 = list.HFTD_PRICE2,
                                            Price2Remark = list.HFTD_PRICE2_REMARK,
                                            Price3 = list.HFTD_PRICE3,
                                            Price3Remark = list.HFTD_PRICE3_REMARK,
                                            PriceUnit = list.HFTD_PRICE_UNIT,
                                            CreateDate = list.HFTD_CREATED_DATE == null ? "" : Convert.ToDateTime(list.HFTD_CREATED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                            CreateBy = list.HFTD_CREATED_BY
                                        });
                                    }
                                }
                            }
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return detailData;
        }        
    }
}
