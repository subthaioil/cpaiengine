﻿using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Services;
using static ProjectCPAIEngine.Services.CDSService;
using System.Data.Entity.SqlServer;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingManagementCounterpartyServiceModel
    {

        public ReturnValue Add(ref HedgingManagementCounterpartyViewModel_Detail pModel, string pUser)
        {
            CDSService cdsService = new CDSService();
            ReturnValue rtn = new ReturnValue();

            try
            {
                rtn = ValidateInput(ref pModel, pUser);
                if (!rtn.Status)
                    return rtn;

                HEDG_MT_CP_DAL dal = new HEDG_MT_CP_DAL();
                HEDG_MT_CP ent = new HEDG_MT_CP();
                HEDG_MT_CP_CDS_DAL dalCdsDetail = new HEDG_MT_CP_CDS_DAL();
                HEDG_MT_CP_CDS entCdsDetail = new HEDG_MT_CP_CDS();
                HEDG_MT_CP_LIMIT_DAL dalLimitDetail = new HEDG_MT_CP_LIMIT_DAL();
                HEDG_MT_CP_LIMIT entLimitDetail = new HEDG_MT_CP_LIMIT();
                HEDG_MT_CP_STATUS_DAL dalStatusDetail = new HEDG_MT_CP_STATUS_DAL();
                HEDG_MT_CP_STATUS entStatusDetail = new HEDG_MT_CP_STATUS();
                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //rtn = ValidateSubmit(context, pModel);
                            //if (!rtn.Status)
                            //    return rtn;

                            var CtrlID = ConstantPrm.GetDynamicCtrlID();
                            ent.HMC_ROW_ID = CtrlID;
                            ent.HMC_FK_VENDOR = pModel.CouterPartyID;
                            ent.HMC_FK_HEDG_CDS_FILE = "";
                            ent.HMC_CREDIT_RATING = pModel.CreditRating;
                            ent.HMC_CREDIT_LIMIT = pModel.CreditLimit;
                            ent.HMC_STATUS = pModel.Status;
                            ent.HMC_REASON = "";
                            ent.HMC_CREATED_DATE = now;
                            ent.HMC_CREATED_BY = pUser;
                            ent.HMC_UPDATED_DATE = now;
                            ent.HMC_UPDATED_BY = pUser;
                            dal.Save(ent, context);

                            if (pModel.DetailCds != null)
                            {
                                foreach (var item in pModel.DetailCds)
                                {
                                    entCdsDetail = new HEDG_MT_CP_CDS();
                                    entCdsDetail.HMCD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCdsDetail.HMCD_FK_HEDG_CP = CtrlID;
                                    entCdsDetail.HMCD_FK_HEDG_CDS_FILE = "";
                                    entCdsDetail.HMCD_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.Date, "dd/MM/yyyy");
                                    entCdsDetail.HMCD_CDS = item.Cds;
                                    entCdsDetail.HMCD_STATUS = "ACTIVE";
                                    entCdsDetail.HMCD_CREATED_DATE = now;
                                    entCdsDetail.HMCD_CREATED_BY = pUser;
                                    entCdsDetail.HMCD_UPDATED_DATE = now;
                                    entCdsDetail.HMCD_UPDATED_BY = pUser;
                                    dalCdsDetail.Save(entCdsDetail, context);
                                }
                            }

                            if (pModel.DetailCreditLimit != null)
                            {
                                foreach (var item in pModel.DetailCreditLimit)
                                {
                                    entLimitDetail = new HEDG_MT_CP_LIMIT();
                                    entLimitDetail.HMCL_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entLimitDetail.HMCL_FK_HEDG_CP = CtrlID;
                                    entLimitDetail.HMCL_DATE_START = ShareFn.ConvertStringDateFormatToDatetime(item.dDate.Substring(0, 10), "dd/MM/yyyy");
                                    entLimitDetail.HMCL_DATE_END = ShareFn.ConvertStringDateFormatToDatetime(item.dDate.Substring(14, 10), "dd/MM/yyyy");
                                    entLimitDetail.HMCL_CREDIT_LIMIT = item.CreditLimit;
                                    entLimitDetail.HMCL_STATUS = item.Status;
                                    entLimitDetail.HMCL_CREATED_DATE = now;
                                    entLimitDetail.HMCL_CREATED_BY = pUser;
                                    entLimitDetail.HMCL_UPDATED_DATE = now;
                                    entLimitDetail.HMCL_UPDATED_BY = pUser;
                                    dalLimitDetail.Save(entLimitDetail, context);
                                }
                            }

                            entStatusDetail = new HEDG_MT_CP_STATUS();
                            entStatusDetail.HMCS_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                            entStatusDetail.HMCS_FK_HEDG_CP = CtrlID;
                            entStatusDetail.HMCS_STATUS = pModel.Status;
                            entStatusDetail.HMCS_REASON = "";
                            entStatusDetail.HMCS_CREATED_DATE = now;
                            entStatusDetail.HMCS_CREATED_BY = pUser;
                            entStatusDetail.HMCS_UPDATED_DATE = now;
                            entStatusDetail.HMCS_UPDATED_BY = pUser;
                            dalStatusDetail.Save(entStatusDetail, context);

                            dbContextTransaction.Commit();
                            rtn.newID = CtrlID;
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            ResultCheckCDS5 rCDS5 = cdsService.CheckCDS5(null, pUser);
            return rtn;
        }

        public ReturnValue Edit(HedgingManagementCounterpartyViewModel_Detail pModel, string pUser)
        {
            CDSService cdsService = new CDSService();
            ReturnValue rtn = new ReturnValue();

            try
            {
                rtn = ValidateInput(ref pModel, pUser);
                if (!rtn.Status)
                    return rtn;

                HEDG_MT_CP_DAL dal = new HEDG_MT_CP_DAL();
                HEDG_MT_CP ent = new HEDG_MT_CP();

                HEDG_MT_CP_CDS_DAL dalCdsDetail = new HEDG_MT_CP_CDS_DAL();
                HEDG_MT_CP_CDS entCdsDetail = new HEDG_MT_CP_CDS();
                HEDG_MT_CP_LIMIT_DAL dalLimitDetail = new HEDG_MT_CP_LIMIT_DAL();
                HEDG_MT_CP_LIMIT entLimitDetail = new HEDG_MT_CP_LIMIT();
                HEDG_MT_CP_STATUS_DAL dalStatusDetail = new HEDG_MT_CP_STATUS_DAL();
                HEDG_MT_CP_STATUS entStatusDetail = new HEDG_MT_CP_STATUS();

                HEDG_HIS_CP_DAL dalHis = new HEDG_HIS_CP_DAL();
                HEDG_HIS_CP entHis = new HEDG_HIS_CP();

                HEDG_HIS_CP_CDS_DAL dalHisDetail = new HEDG_HIS_CP_CDS_DAL();
                HEDG_HIS_CP_CDS entHisDetail = new HEDG_HIS_CP_CDS();
                HEDG_HIS_CP_LIMIT_DAL dalHisLimitDetail = new HEDG_HIS_CP_LIMIT_DAL();
                HEDG_HIS_CP_LIMIT entHisLimitDetail = new HEDG_HIS_CP_LIMIT();
                HEDG_HIS_CP_STATUS_DAL dalHisStatusDetail = new HEDG_HIS_CP_STATUS_DAL();
                HEDG_HIS_CP_STATUS entHisStatusDetail = new HEDG_HIS_CP_STATUS();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //rtn = ValidateSubmit(context, pModel);
                            //if (!rtn.Status)
                            //    return rtn;
                            /////////////////////////////////////////////////////////////////////////////////////////////////////

                            #region Insert His
                            var CtrlID = ConstantPrm.GetDynamicCtrlID();
                            var queryDetail = (from v in context.HEDG_MT_CP
                                               orderby v.HMC_ROW_ID
                                               where v.HMC_ROW_ID.Equals(pModel.HMC_ROW_ID)
                                               select v).FirstOrDefault();
                            if (queryDetail != null)
                            {
                                entHis.HIC_ROW_ID = CtrlID;
                                entHis.HIC_FK_VENDOR = queryDetail.HMC_FK_VENDOR;
                                entHis.HIC_FK_HEDG_CDS_FILE = queryDetail.HMC_FK_HEDG_CDS_FILE;
                                entHis.HIC_CREDIT_RATING = queryDetail.HMC_CREDIT_RATING;
                                entHis.HIC_CREDIT_LIMIT = queryDetail.HMC_CREDIT_LIMIT;
                                entHis.HIC_STATUS = queryDetail.HMC_STATUS;
                                entHis.HIC_REASON = queryDetail.HMC_REASON;
                                entHis.HIC_CREATED_DATE = queryDetail.HMC_CREATED_DATE;
                                entHis.HIC_CREATED_BY = queryDetail.HMC_CREATED_BY;
                                entHis.HIC_UPDATED_DATE = queryDetail.HMC_UPDATED_DATE;
                                entHis.HIC_UPDATED_BY = queryDetail.HMC_UPDATED_BY;
                                entHis.HIC_HISTORY_REASON = "";
                                entHis.HIC_FK_HEDG_CP = pModel.HMC_ROW_ID;
                                dalHis.Save(entHis, context);

                                if (pModel.DetailCds != null)
                                {
                                    foreach (var list in pModel.DetailCds)
                                    {
                                        DateTime? datetime = ShareFn.ConvertStringDateFormatToDatetime(list.Date, "dd/MM/yyyy");
                                        var queryCdsDetail = (from v in context.HEDG_MT_CP_CDS
                                                              orderby v.HMCD_ROW_ID
                                                              where v.HMCD_ROW_ID.Equals(list.HMCD_ROW_ID)
                                                              select v).FirstOrDefault();

                                        if (queryCdsDetail != null)
                                        {
                                            if (queryCdsDetail.HMCD_CDS != list.Cds)
                                            {
                                                entHisDetail = new HEDG_HIS_CP_CDS();
                                                entHisDetail.HICD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                entHisDetail.HICD_FK_HEDG_CP = CtrlID;
                                                entHisDetail.HICD_FK_HEDG_CDS_FILE = queryCdsDetail.HMCD_FK_HEDG_CDS_FILE;
                                                entHisDetail.HICD_DATE = queryCdsDetail.HMCD_DATE;
                                                entHisDetail.HICD_CDS = queryCdsDetail.HMCD_CDS;
                                                entHisDetail.HICD_STATUS = queryCdsDetail.HMCD_STATUS;
                                                entHisDetail.HICD_CREATED_DATE = queryCdsDetail.HMCD_CREATED_DATE;
                                                entHisDetail.HICD_CREATED_BY = queryCdsDetail.HMCD_CREATED_BY;
                                                entHisDetail.HICD_UPDATED_DATE = queryCdsDetail.HMCD_UPDATED_DATE;
                                                entHisDetail.HICD_UPDATED_BY = queryCdsDetail.HMCD_UPDATED_BY;
                                                entHisDetail.HICD_HISTORY_REASON = "";
                                                entHisDetail.HICD_FK_HEDG_CDS = queryCdsDetail.HMCD_ROW_ID;
                                                dalHisDetail.Save(entHisDetail, context);
                                            }
                                        }
                                    }
                                }

                                if (pModel.DetailCreditLimit != null)
                                {
                                    foreach (var list in pModel.DetailCreditLimit)
                                    {
                                        DateTime? fromDate = ShareFn.ConvertStringDateFormatToDatetime(list.dDate.Substring(0, 10));
                                        DateTime? toDate = ShareFn.ConvertStringDateFormatToDatetime(list.dDate.Substring(14, 10));

                                        var queryLimitDetail = (from v in context.HEDG_MT_CP_LIMIT
                                                                orderby v.HMCL_ROW_ID
                                                                where v.HMCL_ROW_ID.Equals(list.HMCL_ROW_ID)
                                                                select v).FirstOrDefault();

                                        if (queryLimitDetail != null)
                                        {
                                            if (queryLimitDetail.HMCL_CREDIT_LIMIT != list.CreditLimit || queryLimitDetail.HMCL_STATUS != list.Status)
                                            {
                                                entHisLimitDetail = new HEDG_HIS_CP_LIMIT();
                                                entHisLimitDetail.HICL_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                entHisLimitDetail.HICL_FK_HEDG_CP = CtrlID;
                                                entHisLimitDetail.HICL_DATE_START = queryLimitDetail.HMCL_DATE_START;
                                                entHisLimitDetail.HICL_DATE_END = queryLimitDetail.HMCL_DATE_END;
                                                entHisLimitDetail.HICL_CREDIT_LIMIT = queryLimitDetail.HMCL_CREDIT_LIMIT;
                                                entHisLimitDetail.HICL_STATUS = queryLimitDetail.HMCL_STATUS;
                                                entHisLimitDetail.HICL_CREATED_DATE = queryLimitDetail.HMCL_CREATED_DATE;
                                                entHisLimitDetail.HICL_CREATED_BY = queryLimitDetail.HMCL_CREATED_BY;
                                                entHisLimitDetail.HICL_UPDATED_DATE = queryLimitDetail.HMCL_UPDATED_DATE;
                                                entHisLimitDetail.HICL_UPDATED_BY = queryLimitDetail.HMCL_UPDATED_BY;
                                                entHisLimitDetail.HICL_HISTORY_REASON = "";
                                                entHisLimitDetail.HICL_FK_HEDG_CP_LIMIT = queryLimitDetail.HMCL_ROW_ID;
                                                dalHisLimitDetail.Save(entHisLimitDetail, context);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Update Data

                            var queryStatusDetail = (from v in context.HEDG_MT_CP
                                                     where v.HMC_ROW_ID.Equals(pModel.HMC_ROW_ID)
                                                     select v).FirstOrDefault();
                            string oldStatus = queryStatusDetail.HMC_STATUS;
                            ent.HMC_ROW_ID = pModel.HMC_ROW_ID;
                            ent.HMC_FK_VENDOR = pModel.CouterPartyID;
                            ent.HMC_CREDIT_RATING = pModel.CreditRating;
                            ent.HMC_CREDIT_LIMIT = pModel.CreditLimit;

                            if (queryStatusDetail.HMC_STATUS == CPAIConstantUtil.RESUME && pModel.Status == CPAIConstantUtil.SUSPEND)
                                ent.HMC_STATUS = CPAIConstantUtil.SUSPEND;
                            else if (queryStatusDetail.HMC_STATUS == CPAIConstantUtil.RESUME_50 && pModel.Status == CPAIConstantUtil.SUSPEND)
                                ent.HMC_STATUS = CPAIConstantUtil.SUSPEND;
                            else if (queryStatusDetail.HMC_STATUS == CPAIConstantUtil.RESUME_50 && pModel.Status == CPAIConstantUtil.RESUME)
                                ent.HMC_STATUS = CPAIConstantUtil.RESUME_50;
                            else if (queryStatusDetail.HMC_STATUS == CPAIConstantUtil.SUSPEND && pModel.Status == CPAIConstantUtil.RESUME)
                                ent.HMC_STATUS = CPAIConstantUtil.RESUME_50;
                            else if (queryStatusDetail.HMC_STATUS == pModel.Status)
                                ent.HMC_STATUS = pModel.Status;

                            ent.HMC_REASON = (pModel.Status == CPAIConstantUtil.RESUME || pModel.Status == CPAIConstantUtil.RESUME_50) ? null : pModel.Reason;
                            ent.HMC_UPDATED_DATE = now;
                            ent.HMC_UPDATED_BY = pUser;
                            dal.Update(ent, context);

                            if (pModel.DetailCds != null)
                            {
                                foreach (var item in pModel.DetailCds)
                                {
                                    entCdsDetail = new HEDG_MT_CP_CDS();
                                    DateTime? datetime = ShareFn.ConvertStringDateFormatToDatetime(item.Date, "dd/MM/yyyy");
                                    var queryCdsDetail = (from v in context.HEDG_MT_CP_CDS
                                                          orderby v.HMCD_ROW_ID
                                                          where v.HMCD_ROW_ID.Equals(item.HMCD_ROW_ID) &&
                                                          v.HMCD_FK_HEDG_CP.Equals(pModel.HMC_ROW_ID)
                                                          select v).FirstOrDefault();
                                    if (queryCdsDetail != null)
                                    {
                                        if (queryCdsDetail.HMCD_ROW_ID != null)
                                            entCdsDetail.HMCD_ROW_ID = queryCdsDetail.HMCD_ROW_ID;
                                        else
                                            entCdsDetail.HMCD_ROW_ID = item.HMCD_ROW_ID;
                                    }
                                    else
                                        entCdsDetail.HMCD_ROW_ID = item.HMCD_ROW_ID;
                                    //TEST

                                    //entCdsDetail.HMCD_ROW_ID = item.HMCD_ROW_ID;
                                    entCdsDetail.HMCD_FK_HEDG_CP = pModel.HMC_ROW_ID;
                                    entCdsDetail.HMCD_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.Date, "dd/MM/yyyy");
                                    entCdsDetail.HMCD_CDS = item.Cds;
                                    entCdsDetail.HMCD_STATUS = "ACTIVE";

                                    if (string.IsNullOrEmpty(entCdsDetail.HMCD_ROW_ID))
                                    {
                                        entCdsDetail.HMCD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        entCdsDetail.HMCD_CREATED_DATE = now;
                                        entCdsDetail.HMCD_CREATED_BY = pUser;
                                        entCdsDetail.HMCD_UPDATED_DATE = now;
                                        entCdsDetail.HMCD_UPDATED_BY = pUser;
                                        dalCdsDetail.Save(entCdsDetail, context);
                                    }
                                    else if (queryCdsDetail.HMCD_CDS != item.Cds)
                                    {
                                        entCdsDetail.HMCD_UPDATED_DATE = now;
                                        entCdsDetail.HMCD_UPDATED_BY = pUser;
                                        dalCdsDetail.Update(entCdsDetail, context);
                                    }
                                }
                            }

                            if (pModel.DetailCreditLimit != null)
                            {
                                foreach (var item in pModel.DetailCreditLimit)
                                {
                                    entLimitDetail = new HEDG_MT_CP_LIMIT();
                                    var queryLimitDetail = (from v in context.HEDG_MT_CP_LIMIT
                                                            orderby v.HMCL_ROW_ID
                                                            where v.HMCL_ROW_ID.Equals(item.HMCL_ROW_ID) &&
                                                            v.HMCL_FK_HEDG_CP.Equals(pModel.HMC_ROW_ID)
                                                            select v).FirstOrDefault();

                                    if (queryLimitDetail != null)
                                    {
                                        if (queryLimitDetail.HMCL_ROW_ID != null)
                                            entLimitDetail.HMCL_ROW_ID = queryLimitDetail.HMCL_ROW_ID;
                                        else
                                            entLimitDetail.HMCL_ROW_ID = item.HMCL_ROW_ID;
                                    }
                                    else
                                        entLimitDetail.HMCL_ROW_ID = item.HMCL_ROW_ID;

                                    entLimitDetail.HMCL_FK_HEDG_CP = pModel.HMC_ROW_ID;
                                    entLimitDetail.HMCL_DATE_START = ShareFn.ConvertStringDateFormatToDatetime(item.dDate.Substring(0, 10), "dd/MM/yyyy");
                                    entLimitDetail.HMCL_DATE_END = ShareFn.ConvertStringDateFormatToDatetime(item.dDate.Substring(14, 10), "dd/MM/yyyy");
                                    entLimitDetail.HMCL_CREDIT_LIMIT = item.CreditLimit;
                                    entLimitDetail.HMCL_STATUS = item.Status;

                                    if (string.IsNullOrEmpty(entLimitDetail.HMCL_ROW_ID))
                                    {
                                        entLimitDetail.HMCL_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        entLimitDetail.HMCL_CREATED_DATE = now;
                                        entLimitDetail.HMCL_CREATED_BY = pUser;
                                        entLimitDetail.HMCL_UPDATED_DATE = now;
                                        entLimitDetail.HMCL_UPDATED_BY = pUser;
                                        dalLimitDetail.Save(entLimitDetail, context);
                                    }
                                    else if (queryLimitDetail.HMCL_CREDIT_LIMIT != item.CreditLimit || queryLimitDetail.HMCL_STATUS != item.Status)
                                    {
                                        entLimitDetail.HMCL_UPDATED_DATE = now;
                                        entLimitDetail.HMCL_UPDATED_BY = pUser;
                                        dalLimitDetail.Update(entLimitDetail, context);
                                    }
                                }
                            }

                            if (queryStatusDetail.HMC_STATUS != oldStatus)
                            {
                                entStatusDetail = new HEDG_MT_CP_STATUS();
                                entStatusDetail.HMCS_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entStatusDetail.HMCS_FK_HEDG_CP = pModel.HMC_ROW_ID;
                                entStatusDetail.HMCS_STATUS = ent.HMC_STATUS;
                                entStatusDetail.HMCS_REASON = (pModel.Status == CPAIConstantUtil.RESUME || pModel.Status == CPAIConstantUtil.RESUME_50) ? null : pModel.Reason;
                                entStatusDetail.HMCS_CREATED_DATE = now;
                                entStatusDetail.HMCS_CREATED_BY = pUser;
                                entStatusDetail.HMCS_UPDATED_DATE = now;
                                entStatusDetail.HMCS_UPDATED_BY = pUser;
                                dalStatusDetail.Save(entStatusDetail, context);
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            ResultCheckCDS5 rCDS5 = cdsService.CheckCDS5(null, pUser);
            return rtn;
        }

        public HedgingManagementCounterpartyViewModel_Detail GetDetail(string pID)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingManagementCounterpartyViewModel_Detail detail = new HedgingManagementCounterpartyViewModel_Detail();

            try
            {
                string sID = String.IsNullOrEmpty(pID) ? "" : pID;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP
                                 where v.HMC_ROW_ID.Equals(sID)
                                 select new
                                 {
                                     HMC_ROW_ID = v.HMC_ROW_ID,
                                     CouterPartyID = v.HMC_FK_VENDOR,
                                     FileID = v.HMC_FK_HEDG_CDS_FILE,
                                     CreditRating = v.HMC_CREDIT_RATING,
                                     CreditLimit = v.HMC_CREDIT_LIMIT,
                                     Status = v.HMC_STATUS,
                                     Reason = v.HMC_REASON
                                 });

                    if (query != null)
                    {
                        foreach (var g in query.ToList())
                        {
                            detail.HMC_ROW_ID = g.HMC_ROW_ID;
                            detail.CouterPartyID = g.CouterPartyID;
                            detail.FileID = g.FileID;
                            detail.CreditRating = g.CreditRating;
                            detail.CreditLimit = g.CreditLimit;
                            detail.Status = g.Status;
                            detail.Reason = g.Reason;
                            //detail.Log_Reason = g.dCouterPartyID;

                            var queryCdsDetail = (from v in context.HEDG_MT_CP_CDS
                                                  orderby v.HMCD_DATE
                                                  where v.HMCD_FK_HEDG_CP.Equals(pID)
                                                  select v);
                            if (queryCdsDetail != null)
                            {
                                detail.DetailCds = new List<HedgingManagementCounterpartyViewModel_DetailCds>();
                                foreach (var list in queryCdsDetail)
                                {
                                    detail.DetailCds.Add(new HedgingManagementCounterpartyViewModel_DetailCds
                                    {
                                        HMCD_ROW_ID = list.HMCD_ROW_ID,
                                        CouterPartyID = list.HMCD_FK_HEDG_CP,
                                        FileID = list.HMCD_FK_HEDG_CDS_FILE,
                                        Date = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCD_DATE, "dd/MM/yyyy"),
                                        Cds = list.HMCD_CDS,
                                        Status = list.HMCD_STATUS,
                                    });
                                }
                            }

                            var queryCreditLimitDetail = (from v in context.HEDG_MT_CP_LIMIT
                                                          orderby v.HMCL_DATE_START
                                                          where v.HMCL_FK_HEDG_CP.Equals(pID)
                                                          select v);
                            if (queryCreditLimitDetail != null)
                            {
                                detail.DetailCreditLimit = new List<HedgingManagementCounterpartyViewModel_DetailCreditLimit>();
                                foreach (var list in queryCreditLimitDetail)
                                {
                                    detail.DetailCreditLimit.Add(new HedgingManagementCounterpartyViewModel_DetailCreditLimit
                                    {
                                        HMCL_ROW_ID = list.HMCL_ROW_ID,
                                        CouterPartyID = list.HMCL_FK_HEDG_CP,
                                        DateStart = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_START),
                                        DateEnd = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_END),
                                        CreditLimit = list.HMCL_CREDIT_LIMIT,
                                        Status = list.HMCL_STATUS,
                                        CreateDate = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_CREATED_DATE, "yyyyMMdd HHmmss"),
                                        CreateBy = list.HMCL_CREATED_BY,
                                        dDate = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_START, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_END, "dd/MM/yyyy")
                                    });
                                }
                            }

                            var queryStatusDetail = (from v in context.HEDG_MT_CP_STATUS
                                                     orderby v.HMCS_CREATED_DATE descending
                                                     where v.HMCS_FK_HEDG_CP.Equals(pID)
                                                     select v);
                            if (queryStatusDetail != null)
                            {
                                detail.DetailStatus = new List<HedgingManagementCounterpartyViewModel_DetailStatus>();
                                foreach (var list in queryStatusDetail)
                                {
                                    detail.Log_Reason =
                                        detail.Log_Reason +
                                        ShareFn.ConvertDateTimeToDateStringFormat(list.HMCS_CREATED_DATE, "dd/MM/yyyy HH:mm:ss") +
                                        "\t\tSTATUS : " + list.HMCS_STATUS;
                                    if (list.HMCS_STATUS == CPAIConstantUtil.SUSPEND)
                                        detail.Log_Reason = detail.Log_Reason + "\t\tREASON : " + list.HMCS_REASON;
                                    detail.Log_Reason = detail.Log_Reason + Environment.NewLine;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return detail;
        }

        public List<HedgingManagementCounterpartyViewModel_DetailCds> GetDetailCds(string pDate, string pID)
        {
            List<HedgingManagementCounterpartyViewModel_DetailCds> detailCds = new List<HedgingManagementCounterpartyViewModel_DetailCds>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryCdsDetail = (from v in context.HEDG_MT_CP_CDS
                                          orderby v.HMCD_DATE
                                          where v.HMCD_FK_HEDG_CP.Equals(pID)
                                          select v);

                    if (!string.IsNullOrEmpty(pDate) && !string.IsNullOrEmpty(pID))
                    {
                        DateTime? fromDate = ShareFn.ConvertStringDateFormatToDatetime(pDate.Substring(0, 10));
                        DateTime? toDate = ShareFn.ConvertStringDateFormatToDatetime(pDate.Substring(14, 10));
                        queryCdsDetail = (from vc in queryCdsDetail
                                          where (vc.HMCD_DATE >= fromDate && vc.HMCD_DATE <= toDate)
                                          orderby vc.HMCD_DATE descending
                                          select vc);
                    }

                    if (queryCdsDetail != null)
                    {
                        foreach (var list in queryCdsDetail)
                        {
                            detailCds.Add(new HedgingManagementCounterpartyViewModel_DetailCds
                            {
                                HMCD_ROW_ID = list.HMCD_ROW_ID,
                                CouterPartyID = list.HMCD_FK_HEDG_CP,
                                FileID = list.HMCD_FK_HEDG_CDS_FILE,
                                Date = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCD_DATE, "dd/MM/yyyy"),
                                Cds = list.HMCD_CDS,
                                Status = list.HMCD_STATUS
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return detailCds;
        }

        public List<HedgingManagementCounterpartyViewModel_DetailCreditLimit> GetDetailCreditLimit(string pStatus, string pID)
        {
            List<HedgingManagementCounterpartyViewModel_DetailCreditLimit> detailCds = new List<HedgingManagementCounterpartyViewModel_DetailCreditLimit>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryCreditLimitDetail = (from v in context.HEDG_MT_CP_LIMIT
                                                  orderby v.HMCL_DATE_START
                                                  where v.HMCL_FK_HEDG_CP.Equals(pID) && v.HMCL_STATUS.Equals(pStatus)
                                                  select v);
                    if (queryCreditLimitDetail != null)
                    {
                        foreach (var list in queryCreditLimitDetail)
                        {
                            detailCds.Add(new HedgingManagementCounterpartyViewModel_DetailCreditLimit
                            {
                                HMCL_ROW_ID = list.HMCL_ROW_ID,
                                CouterPartyID = list.HMCL_FK_HEDG_CP,
                                DateStart = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_START),
                                DateEnd = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_END),
                                CreditLimit = list.HMCL_CREDIT_LIMIT,
                                Status = list.HMCL_STATUS,
                                CreateDate = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_CREATED_DATE),
                                CreateBy = list.HMCL_CREATED_BY,
                                UpdateDate = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_UPDATED_DATE),
                                UpdateBy = list.HMCL_UPDATED_BY,
                                dDate = ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_START, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(list.HMCL_DATE_END, "dd/MM/yyyy")
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return detailCds;
        }

        public ReturnValue Search(ref HedgingManagementCounterpartyViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            string sCouterParty, sCreditRating, sStatus;
            try
            {
                try
                {
                    sCouterParty = String.IsNullOrEmpty(pModel.sCouterParty) ? "" : pModel.sCouterParty.Trim().ToUpper();
                    sCreditRating = String.IsNullOrEmpty(pModel.sCreditRating) ? "" : pModel.sCreditRating.Trim().ToUpper();
                    sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.Trim().ToUpper();
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP
                                 join v2 in context.MT_VENDOR on v.HMC_FK_VENDOR equals v2.VND_ACC_NUM_VENDOR
                                 select new
                                 {
                                     dCouterPartyID = v.HMC_ROW_ID,
                                     dVendorID = v.HMC_FK_VENDOR,
                                     dVendorName = v2.VND_NAME1,
                                     dFileID = v.HMC_FK_HEDG_CDS_FILE,
                                     dCreditRating = v.HMC_CREDIT_RATING,
                                     dCreditLimit = v.HMC_CREDIT_LIMIT,
                                     dStatus = v.HMC_STATUS,
                                     dReason = v.HMC_REASON,
                                     dUpdateBy = v.HMC_UPDATED_BY,
                                     dUpdateDate = (DateTime?)v.HMC_UPDATED_DATE
                                 });
                    if (!string.IsNullOrEmpty(sCouterParty))
                        query = query.Where(p => p.dCouterPartyID.Trim().ToUpper().Equals(sCouterParty));
                    if (!string.IsNullOrEmpty(sCreditRating))
                        query = query.Where(p => p.dCreditRating.Trim().ToUpper().Equals(sCreditRating));
                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.Trim().ToUpper().Equals(sStatus));
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (query != null)
                    {
                        pModel.Detail = new List<HedgingManagementCounterpartyViewModel_Detail>();
                        foreach (var g in query)
                        {
                            pModel.Detail.Add(
                                                    new HedgingManagementCounterpartyViewModel_Detail
                                                    {
                                                        HMC_ROW_ID = g.dCouterPartyID,
                                                        CouterPartyID = g.dVendorID,
                                                        CouterPartyName = g.dVendorName,
                                                        FileID = g.dFileID,
                                                        CreditRating = g.dCreditRating,
                                                        CreditLimit = g.dCreditLimit,
                                                        Status = g.dStatus,
                                                        Reason = g.dReason,
                                                        UpdateBy = g.dUpdateBy,
                                                        UpdateDate = (g.dUpdateDate == null ? "" : Convert.ToDateTime(g.dUpdateDate).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        #region Excel

        public ReturnValue UploadExcel(string pFileName, string pFilePath, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            List<HedgingManagementCounterpartyViewModel_Detail> hmc_detail = new List<HedgingManagementCounterpartyViewModel_Detail>();

            List<HedgingManagementCounterpartyViewModel_Detail> hmc_detailTmp = new List<HedgingManagementCounterpartyViewModel_Detail>();
            List<HedgingManagementCounterpartyViewModel_DetailCds> hmc_DetailCdsTmp = new List<HedgingManagementCounterpartyViewModel_DetailCds>();

            List<CounterPartyTemp> vendorMatchList = new List<CounterPartyTemp>();
            List<CounterPartyTemp> vendorUnMatchList = new List<CounterPartyTemp>();

            List<CounterPartyTemp> vendorMatchGreatList = new List<CounterPartyTemp>();
            List<CounterPartyTemp> vendorMatchLessList = new List<CounterPartyTemp>();

            List<CounterPartyTemp> vendorMatchSuccessList = new List<CounterPartyTemp>();
            List<CounterPartyTemp> vendorMatchFailedList = new List<CounterPartyTemp>();

            List<CounterPartyTemp> counterPartyList = new List<CounterPartyTemp>();

            List<MT_VENDOR> vendorResultList = new List<MT_VENDOR>();

            try
            {
                List<VendorViewModel_SeachData> vendorControlList = new List<VendorViewModel_SeachData>();
                vendorControlList = VendorServiceModel.getVendorControl("CPAI", "HEDG_MT_CP", "ACTIVE");
                #region ReadExcelFile
                byte[] file = System.IO.File.ReadAllBytes(pFilePath);
                using (MemoryStream ms = new MemoryStream(file))
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.First();

                    for (int c = 1; c < ws.Dimension.End.Column; c++)
                    {

                        int vendorExist = vendorControlList.Count(x => (ws.Cells[1, c + 1].Value.ToString().Trim().Equals(x.dVendorName)));
                        if (vendorExist != 0)
                        {
                            string vendorIDx = vendorControlList.Where(x => (ws.Cells[1, c + 1].Value.ToString().Trim().Equals(x.dVendorName))).Select(x => x.dVendorCode).FirstOrDefault();
                            for (int r = 2; r < ws.Dimension.End.Row; r++)
                            {
                                counterPartyList.Add(new CounterPartyTemp
                                {
                                    VendorID = vendorIDx,
                                    Name = ws.Cells[2, c].Value.ToString().Trim(),
                                    Rating = ws.Cells[2, c + 1].Value.ToString().Trim(),
                                    Date = ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[r + 1, 1].Value).Date, "yyyyMMdd"),
                                    Value = ws.Cells[r + 1, c + 1].Value == null ? "" : ws.Cells[r + 1, c + 1].Value.ToString()
                                });
                            }
                            #region VendorMatch
                            vendorMatchList.Add(new CounterPartyTemp
                            {
                                VendorID = vendorIDx,
                                Name = ws.Cells[1, c + 1].Value.ToString().Trim(),
                                Rating = ws.Cells[2, c + 1].Value.ToString().Trim(),
                                Date = ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[2 + 1, 1].Value).Date, "yyyyMMdd") + " to " +
                                             ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[ws.Dimension.End.Row, 1].Value).Date, "yyyyMMdd"),
                                Count = (ws.Dimension.End.Row - 2).ToString()
                            });
                            #endregion
                        }
                        else
                        {
                            #region VendorUnMatch
                            vendorUnMatchList.Add(new CounterPartyTemp
                            {
                                VendorID = ws.Cells[1, c + 1].Value.ToString().Trim(),
                                Name = ws.Cells[1, c + 1].Value.ToString(),
                                Rating = ws.Cells[2, c + 1].Value.ToString(),
                                Date = ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[2 + 1, 1].Value).Date, "yyyyMMdd") + " to " +
                                             ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[ws.Dimension.End.Row, 1].Value).Date, "yyyyMMdd"),
                                Count = (ws.Dimension.End.Row - 2).ToString()
                            });
                            #endregion
                        }
                    }
                }
                #endregion

                #region MatchDataFillDate
                foreach (CounterPartyTemp counterParty in counterPartyList)
                {
                    string rCounterPartyID = counterParty.VendorID;
                    string rName = counterParty.Name;
                    string rRating = counterParty.Rating;
                    string rDate = counterParty.Date;
                    string rValue = counterParty.Value;
                    double rResult;


                    string maxCounterpartyDate = getCPDateControl().Where(x => x.VendorID == rCounterPartyID).Select(x => x.Date).FirstOrDefault();
                    if (Convert.ToInt32(rDate) > Convert.ToInt32(maxCounterpartyDate))
                    {
                        string newValue = null;

                        if (!double.TryParse(rValue, out rResult))
                        {
                            newValue = counterPartyList.AsEnumerable().Where(x => x.VendorID == rCounterPartyID
                            && Convert.ToInt32(x.Date) < Convert.ToInt32(rDate)
                            && !String.IsNullOrEmpty(x.Value)
                            ).OrderByDescending(x => x.Date).Select(x => x.Value).FirstOrDefault().ToString();
                        }
                        else
                        {
                            newValue = rValue;
                        }

                        hmc_DetailCdsTmp.Add(new HedgingManagementCounterpartyViewModel_DetailCds
                        {
                            CouterPartyID = rCounterPartyID,
                            Date = rDate,
                            Cds = newValue,
                            Status = "Add"
                        });
                    }
                    else
                    {
                        hmc_DetailCdsTmp.Add(new HedgingManagementCounterpartyViewModel_DetailCds
                        {
                            CouterPartyID = rCounterPartyID,
                            Date = rDate,
                            //Cds = rValue,
                            Status = "Exist"
                        });
                    }
                }
                #endregion

                #region FillDetailToHead
                foreach (var item in vendorMatchList)
                {
                    hmc_detail.Add(new HedgingManagementCounterpartyViewModel_Detail
                    {
                        HMC_ROW_ID = null,
                        CouterPartyID = item.VendorID,
                        FileID = null,
                        CreditRating = item.Rating,
                        CreditLimit = null,
                        Status = CPAIConstantUtil.RESUME,
                        Reason = null,
                        DetailCds = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID).ToList()
                    });

                    string countAdd = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Add").Count().ToString();
                    string beginDateAdd = "0";
                    string endDateAdd = "0";
                    if (hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Add").OrderBy(x => x.Date).Select(x => x.Date).FirstOrDefault() != null)
                    {
                        beginDateAdd = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Add").OrderBy(x => x.Date).Select(x => x.Date).FirstOrDefault().ToString();
                        endDateAdd = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Add").OrderByDescending(x => x.Date).Select(x => x.Date).FirstOrDefault().ToString();

                        vendorMatchGreatList.Add(new CounterPartyTemp
                        {
                            VendorID = item.VendorID,
                            Name = item.Name,
                            Date = beginDateAdd + " to " + endDateAdd,
                            Count = countAdd
                        });
                    }

                    string countExist = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Exist").Count().ToString();
                    string beginDateExist = "0";
                    string endDateExist = "0";
                    if (hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Exist").OrderBy(x => x.Date).Select(x => x.Date).FirstOrDefault() != null)
                    {
                        beginDateExist = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Exist").OrderBy(x => x.Date).Select(x => x.Date).FirstOrDefault().ToString();
                        endDateExist = hmc_DetailCdsTmp.Where(x => x.CouterPartyID == item.VendorID && x.Status == "Exist").OrderByDescending(x => x.Date).Select(x => x.Date).FirstOrDefault().ToString();

                        vendorMatchLessList.Add(new CounterPartyTemp
                        {
                            VendorID = item.VendorID,
                            Name = item.Name,
                            Date = beginDateExist + " to " + endDateExist,
                            Count = countExist
                        });
                    }
                }
                #endregion

                #region Result
                rtn = SaveFileToDb(pFileName, pFilePath, pUser);
                string fileNewID = rtn.newID;

                foreach (var item in hmc_detail)
                {
                    rtn = SaveToDb(fileNewID, pUser, true, item);

                    var vendorMatchGreat = vendorMatchGreatList.Where(x => x.VendorID == item.CouterPartyID).FirstOrDefault();
                    var vendorMatchLess = vendorMatchLessList.Where(x => x.VendorID == item.CouterPartyID).FirstOrDefault();

                    if (rtn.Status == true)
                    {
                        if (vendorMatchGreat != null)
                        {
                            vendorMatchSuccessList.Add(new CounterPartyTemp
                            {
                                VendorID = vendorMatchGreat.VendorID,
                                Name = vendorMatchGreat.Name,
                                Date = null,
                                Count = null
                            });
                        }
                    }
                    else
                    {
                        vendorMatchFailedList.Add(new CounterPartyTemp
                        {
                            VendorID = vendorMatchGreat.VendorID,
                            Name = vendorMatchGreat.Name,
                            Date = null,
                            Count = null
                        });
                    }
                }

                string htmlText = "";
                htmlText += "Match " + vendorMatchList.Count() + "<br>";
                foreach (var item in vendorMatchList)
                {
                    htmlText += "counterparty name : " + item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                }
                htmlText += "Success<br>";
                //foreach (var item in vendorMatchSuccessList)
                //{
                //    htmlText += item.Name + " " + item.Date + " " + item.Count + "<br>";
                //}

                htmlText += "Add " + vendorMatchGreatList.Count() + "<br>";
                foreach (var item in vendorMatchGreatList)
                {
                    htmlText += "counterparty name : " + item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                }
                htmlText += "Edit " + vendorMatchLessList.Count() + "<br>";
                foreach (var item in vendorMatchLessList)
                {
                    htmlText += "counterparty name : " + item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                }
                htmlText += "Failed " + vendorMatchFailedList.Count() + "<br>";
                foreach (var item in vendorMatchFailedList)
                {
                    htmlText += "counterparty name : " + item.Name + " <br>";
                }
                htmlText += "Unmatch " + vendorUnMatchList.Count() + " <br>";
                foreach (var item in vendorUnMatchList)
                {
                    htmlText += "counterparty name : " + item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                }

                foreach (var item in vendorMatchList)
                {
                    vendorResultList.Add(new MT_VENDOR
                    {
                        VND_ACC_NUM_VENDOR = item.VendorID,
                        VND_NAME1 = item.Name,
                    }
                    );
                }

                ResultMail rMail = new CDSService().PrepareSendMail(CPAIConstantUtil.CDS_ACTION, CPAIConstantUtil.CDS_SYS_HEDG, CPAIConstantUtil.CDS_TYP_HEDG, CPAIConstantUtil.FUN_CDS);
                ResultCheckCDS5 rCDS5 = new CDSService().CheckCDS5(vendorResultList, pUser);
                string jsonText = rMail.lstMsg[0].AMS_MAIL_DETAIL.Replace("#result_detail", htmlText).Replace("#result_cds", rCDS5.message);

                var rSendMail = new SendMailService().sendMail(CPAIConstantUtil.CDS_MAIL, rMail.lstMailTo, jsonText);

                #endregion

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                return rtn;
            }

            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
            rtn.Status = true;
            return rtn;
        }

        public ReturnValue SaveFileToDb(string pFileName, string pFilePath, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            if (pFileName != "" && pFilePath != "")
            {
                try
                {
                    HEDG_MT_CDS_FILE_DAL fileDal = new HEDG_MT_CDS_FILE_DAL();
                    HEDG_MT_CDS_FILE fileEnt = new HEDG_MT_CDS_FILE();

                    DateTime now = DateTime.Now;

                    using (var context = new EntityCPAIEngine())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                string fileCtrlID = ConstantPrm.GetDynamicCtrlID();
                                fileEnt.HMF_ROW_ID = fileCtrlID;
                                fileEnt.HMF_FILE_NAME = pFileName;
                                fileEnt.HMF_FILE_PATH = pFilePath;
                                fileEnt.HMF_CREATED_DATE = now;
                                fileEnt.HMF_CREATED_BY = pUser;
                                fileEnt.HMF_UPDATED_DATE = now;
                                fileEnt.HMF_UPDATED_BY = pUser;
                                fileDal.Save(fileEnt, context);

                                dbContextTransaction.Commit();
                                rtn.newID = fileCtrlID;
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                rtn.Message = ex.Message;
                                rtn.Status = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }

            return rtn;
        }

        public ReturnValue SaveToDb(string fileCtrlID, string pUser, bool Excel, HedgingManagementCounterpartyViewModel_Detail hmc_detail)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                HEDG_MT_CP_DAL cpDal = new HEDG_MT_CP_DAL();
                HEDG_MT_CP cpEnt = new HEDG_MT_CP();

                HEDG_MT_CP_CDS_DAL cpCdsDal = new HEDG_MT_CP_CDS_DAL();
                HEDG_MT_CP_CDS cpCdsEnt = new HEDG_MT_CP_CDS();

                HEDG_HIS_CP_DAL cpHisDal = new HEDG_HIS_CP_DAL();
                HEDG_HIS_CP cpHisEnt = new HEDG_HIS_CP();

                HEDG_HIS_CP_CDS_DAL cpHisCdsDal = new HEDG_HIS_CP_CDS_DAL();
                HEDG_HIS_CP_CDS cpHisCdsEnt = new HEDG_HIS_CP_CDS();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var cpExist = getAllCounterParty().Where(x => x.CouterPartyID == hmc_detail.CouterPartyID).FirstOrDefault();
                            if (cpExist == null)
                            {
                                #region Insert Data And Insert Detail
                                cpEnt = new HEDG_MT_CP();
                                string cpCtrlID = ConstantPrm.GetDynamicCtrlID();
                                cpEnt.HMC_ROW_ID = cpCtrlID;
                                cpEnt.HMC_FK_VENDOR = hmc_detail.CouterPartyID;
                                cpEnt.HMC_FK_HEDG_CDS_FILE = fileCtrlID;
                                cpEnt.HMC_CREDIT_RATING = hmc_detail.CreditRating;
                                cpEnt.HMC_CREDIT_LIMIT = "";
                                cpEnt.HMC_STATUS = hmc_detail.Status;
                                cpEnt.HMC_REASON = "";
                                cpEnt.HMC_CREATED_DATE = now;
                                cpEnt.HMC_CREATED_BY = pUser;
                                cpEnt.HMC_UPDATED_DATE = now;
                                cpEnt.HMC_UPDATED_BY = pUser;
                                cpDal.Save(cpEnt, context);

                                foreach (var itemData in hmc_detail.DetailCds.Where(x => x.CouterPartyID == hmc_detail.CouterPartyID && x.Status == "Add"))
                                {
                                    cpCdsEnt = new HEDG_MT_CP_CDS();
                                    string cpCdsCtrlID = ConstantPrm.GetDynamicCtrlID();
                                    cpCdsEnt.HMCD_ROW_ID = cpCdsCtrlID;
                                    cpCdsEnt.HMCD_FK_HEDG_CP = cpCtrlID;
                                    cpCdsEnt.HMCD_FK_HEDG_CDS_FILE = fileCtrlID;
                                    cpCdsEnt.HMCD_DATE = DateTime.ParseExact(itemData.Date, "yyyyMMdd", null);
                                    cpCdsEnt.HMCD_CDS = itemData.Cds;
                                    cpCdsEnt.HMCD_STATUS = "ACTIVE";
                                    cpCdsEnt.HMCD_CREATED_DATE = now;
                                    cpCdsEnt.HMCD_CREATED_BY = pUser;
                                    cpCdsEnt.HMCD_UPDATED_DATE = now;
                                    cpCdsEnt.HMCD_UPDATED_BY = pUser;
                                    cpCdsDal.Save(cpCdsEnt, context);
                                }
                                #endregion
                            }
                            else
                            {

                                #region Insert Head Data into His
                                cpHisEnt = new HEDG_HIS_CP();
                                string cpHisCtrlID = ConstantPrm.GetDynamicCtrlID();
                                cpHisEnt.HIC_ROW_ID = cpHisCtrlID;
                                cpHisEnt.HIC_FK_VENDOR = cpExist.CouterPartyID;
                                cpHisEnt.HIC_FK_HEDG_CDS_FILE = cpExist.FileID;
                                cpHisEnt.HIC_CREDIT_RATING = cpExist.CreditRating;
                                cpHisEnt.HIC_CREDIT_LIMIT = cpExist.CreditLimit;
                                cpHisEnt.HIC_STATUS = cpExist.Status;
                                cpHisEnt.HIC_REASON = cpExist.Reason;
                                cpHisEnt.HIC_CREATED_DATE = ShareFn.ConvertStringDateFormatToDatetime(cpExist.CreateDate, "yyyyMMdd HHmmss");
                                cpHisEnt.HIC_CREATED_BY = cpExist.CreateBy;
                                cpHisEnt.HIC_UPDATED_DATE = ShareFn.ConvertStringDateFormatToDatetime(cpExist.UpdateDate, "yyyyMMdd HHmmss");
                                cpHisEnt.HIC_UPDATED_BY = cpExist.UpdateBy;
                                cpHisEnt.HIC_HISTORY_REASON = "";
                                cpHisEnt.HIC_FK_HEDG_CP = cpExist.HMC_ROW_ID;
                                cpHisDal.Save(cpHisEnt, context);
                                #endregion

                                #region Edit Data And Insert Detail
                                if (Excel == true)
                                {
                                    cpEnt.HMC_ROW_ID = cpExist.HMC_ROW_ID;
                                    cpEnt.HMC_FK_VENDOR = hmc_detail.CouterPartyID;
                                    cpEnt.HMC_FK_HEDG_CDS_FILE = fileCtrlID;
                                    cpEnt.HMC_CREDIT_RATING = hmc_detail.CreditRating;
                                    cpEnt.HMC_CREDIT_LIMIT = hmc_detail.CreditLimit;
                                    cpEnt.HMC_STATUS = hmc_detail.Status;
                                    cpEnt.HMC_REASON = hmc_detail.Reason;
                                    cpEnt.HMC_CREATED_DATE = ShareFn.ConvertStringDateFormatToDatetime(cpExist.CreateDate, "yyyyMMdd HHmmss");
                                    cpEnt.HMC_CREATED_BY = cpExist.CreateBy;
                                    cpEnt.HMC_UPDATED_DATE = now;
                                    cpEnt.HMC_UPDATED_BY = pUser;
                                    cpDal.Update(cpEnt, context);

                                    foreach (var itemData in hmc_detail.DetailCds.Where(x => x.CouterPartyID == hmc_detail.CouterPartyID && x.Status == "Add"))
                                    {
                                        cpCdsEnt = new HEDG_MT_CP_CDS();
                                        string cpCdsCtrlID = ConstantPrm.GetDynamicCtrlID();
                                        cpCdsEnt.HMCD_ROW_ID = cpCdsCtrlID;
                                        cpCdsEnt.HMCD_FK_HEDG_CP = cpExist.HMC_ROW_ID;
                                        cpCdsEnt.HMCD_FK_HEDG_CDS_FILE = fileCtrlID;
                                        cpCdsEnt.HMCD_DATE = DateTime.ParseExact(itemData.Date, "yyyyMMdd", null);
                                        cpCdsEnt.HMCD_CDS = itemData.Cds;
                                        cpCdsEnt.HMCD_STATUS = "ACTIVE";
                                        cpCdsEnt.HMCD_CREATED_DATE = now;
                                        cpCdsEnt.HMCD_CREATED_BY = pUser;
                                        cpCdsEnt.HMCD_UPDATED_DATE = now;
                                        cpCdsEnt.HMCD_UPDATED_BY = pUser;
                                        cpCdsDal.Save(cpCdsEnt, context);
                                    }
                                }
                                #endregion
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        #endregion

        #region Class

        public static List<HedgingManagementCounterpartyViewModel_Detail> getAllCounterParty()
        {
            List<HedgingManagementCounterpartyViewModel_Detail> rtn = new List<HedgingManagementCounterpartyViewModel_Detail>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from c in context.HEDG_MT_CP
                            select new
                            {
                                HMC_ROW_ID = c.HMC_ROW_ID,
                                HMC_FK_VENDOR = c.HMC_FK_VENDOR,
                                HMC_FK_HEDG_CDS_FILE = c.HMC_FK_HEDG_CDS_FILE,
                                HMC_CREDIT_RATING = c.HMC_CREDIT_RATING,
                                HMC_CREDIT_LIMIT = c.HMC_CREDIT_LIMIT,
                                HMC_STATUS = c.HMC_STATUS,
                                HMC_REASON = c.HMC_REASON,
                                HMC_CREATED_DATE = c.HMC_CREATED_DATE,
                                HMC_CREATED_BY = c.HMC_CREATED_BY,
                                HMC_UPDATED_DATE = c.HMC_UPDATED_DATE,
                                HMC_UPDATED_BY = c.HMC_UPDATED_BY
                            };

                if (query != null)
                {
                    foreach (var g in query)
                    {
                        var queryDetail = from c in context.HEDG_MT_CP_CDS
                                          where c.HMCD_FK_HEDG_CP == g.HMC_ROW_ID
                                          select new
                                          {
                                              HMCD_ROW_ID = c.HMCD_ROW_ID,
                                              HMCD_FK_HEDG_CP = c.HMCD_FK_HEDG_CP,
                                              HMCD_FK_HEDG_CDS_FILE = c.HMCD_FK_HEDG_CDS_FILE,
                                              HMCD_DATE = c.HMCD_DATE,
                                              HMCD_CDS = c.HMCD_CDS,
                                              HMCD_STATUS = c.HMCD_STATUS,
                                              HMCD_CREATED_DATE = c.HMCD_CREATED_DATE,
                                              HMCD_CREATED_BY = c.HMCD_CREATED_BY,
                                              HMCD_UPDATED_DATE = c.HMCD_UPDATED_DATE,
                                              HMCD_UPDATED_BY = c.HMCD_UPDATED_BY
                                          };

                        List<HedgingManagementCounterpartyViewModel_DetailCds> rtnData = new List<HedgingManagementCounterpartyViewModel_DetailCds>();
                        foreach (var item in queryDetail)
                        {
                            rtnData.Add(new HedgingManagementCounterpartyViewModel_DetailCds
                            {
                                HMCD_ROW_ID = item.HMCD_ROW_ID,
                                CouterPartyID = item.HMCD_FK_HEDG_CP,
                                FileID = item.HMCD_FK_HEDG_CDS_FILE,
                                Date = ShareFn.ConvertDateTimeToDateStringFormat(item.HMCD_DATE, "yyyyMMdd HHmmss"),
                                Cds = item.HMCD_CDS,
                                Status = item.HMCD_STATUS,
                                CreateDate = ShareFn.ConvertDateTimeToDateStringFormat(item.HMCD_CREATED_DATE, "yyyyMMdd HHmmss"),
                                CreateBy = item.HMCD_CREATED_BY,
                                UpdateDate = ShareFn.ConvertDateTimeToDateStringFormat(item.HMCD_UPDATED_DATE, "yyyyMMdd HHmmss"),
                                UpdateBy = item.HMCD_UPDATED_BY
                            });
                        }

                        rtn.Add(new HedgingManagementCounterpartyViewModel_Detail
                        {
                            HMC_ROW_ID = g.HMC_ROW_ID,
                            CouterPartyID = g.HMC_FK_VENDOR,
                            FileID = g.HMC_FK_HEDG_CDS_FILE,
                            CreditRating = g.HMC_CREDIT_RATING,
                            CreditLimit = g.HMC_CREDIT_LIMIT,
                            Status = g.HMC_STATUS,
                            Reason = g.HMC_REASON,
                            CreateDate = ShareFn.ConvertDateTimeToDateStringFormat(g.HMC_CREATED_DATE, "yyyyMMdd HHmmss"),
                            CreateBy = g.HMC_CREATED_BY,
                            UpdateDate = ShareFn.ConvertDateTimeToDateStringFormat(g.HMC_UPDATED_DATE, "yyyyMMdd HHmmss"),
                            UpdateBy = g.HMC_UPDATED_BY,
                            DetailCds = rtnData
                        });
                    }
                }
            }
            return rtn;
        }

        public static List<DateTime?> getAllHoliday()
        {
            List<DateTime?> rtn = new List<DateTime?>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from c in context.MT_HOLIDAY
                            select new
                            {
                                MH_HOL_DATE = c.MH_HOL_DATE,
                            };

                foreach (var item in query.ToList())
                {
                    rtn.Add(item.MH_HOL_DATE);
                }
            }
            return rtn;

            #endregion
        }

        public string genCPDateForAddControl(string pDate, string cpID)
        {
            string rtn = "";
            DateTime now = DateTime.Now.Date;
            if (!string.IsNullOrEmpty(pDate))
                now = DateTime.ParseExact(pDate, "dd/MM/yyyy", null).AddDays(1);

            List<DateTime?> holiday = getAllHoliday();
            DateTime? cdsMax = ShareFn.ConvertStringDateFormatToDatetime(getCPDateForAddControl(cpID).Select(x => x.Date).FirstOrDefault(), "yyyyMMdd");

            int countHoliday = holiday.Where(x => x.Value == now).Count();
            while (countHoliday != 0 || now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday || cdsMax >= now)
            {
                now = now.AddDays(1);
                countHoliday = holiday.Where(x => x.Value == now).Count();
            }
            rtn = ShareFn.ConvertDateTimeToDateStringFormat(now, "dd/MM/yyyy");

            return rtn;
        }

        public static List<HedgingManagementCounterpartyViewModel_CdsMax> getCPDateForAddControl(string cpID)
        {

            List<HedgingManagementCounterpartyViewModel_CdsMax> rtn = new List<HedgingManagementCounterpartyViewModel_CdsMax>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.HEDG_MT_CP
                            join vc in context.HEDG_MT_CP_CDS on v.HMC_ROW_ID equals vc.HMCD_FK_HEDG_CP
                            select new
                            { HMC_ROW_ID = v.HMC_ROW_ID, Date = vc.HMCD_DATE };

                if (!string.IsNullOrEmpty(cpID))
                    query = query.Where(p => p.HMC_ROW_ID.Equals(cpID));

                query = from p in query
                        group p by p.HMC_ROW_ID into g
                        select new { HMC_ROW_ID = g.Key, Date = g.Max(d => d.Date) };

                if (query.Count() != 0)
                {
                    foreach (var g in query)
                    {
                        rtn.Add(new HedgingManagementCounterpartyViewModel_CdsMax
                        {
                            VendorID = g.HMC_ROW_ID,
                            Date = g.Date != null ? g.Date.Value.ToString("yyyyMMdd") : "n/a"
                        });
                    }
                }
            }
            return rtn;
        }

        public static List<HedgingManagementCounterpartyViewModel_CdsMax> getCPDateControl()
        {

            List<HedgingManagementCounterpartyViewModel_CdsMax> rtn = new List<HedgingManagementCounterpartyViewModel_CdsMax>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.HEDG_MT_CP
                            join vc in context.HEDG_MT_CP_CDS on v.HMC_ROW_ID equals vc.HMCD_FK_HEDG_CP
                            select new
                            { VendorID = v.HMC_FK_VENDOR, Date = vc.HMCD_DATE };

                query = from p in query
                        group p by p.VendorID into g
                        select new { VendorID = g.Key, Date = g.Max(d => d.Date) };

                if (query.Count() != 0)
                {
                    foreach (var g in query)
                    {
                        rtn.Add(new HedgingManagementCounterpartyViewModel_CdsMax
                        {
                            VendorID = g.VendorID,
                            Date = g.Date != null ? g.Date.Value.ToString("yyyyMMdd") : "n/a"
                        });
                    }
                }
            }
            return rtn;
        }

        private DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue ValidateInput(ref HedgingManagementCounterpartyViewModel_Detail pModel, string pUser)
        {
            ShareFn shareFn = new ShareFn();
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            rtn.Message = string.Empty;

            if (string.IsNullOrEmpty(pUser))
            {
                rtn.Message = "User should not be empty";
                rtn.Status = false;
            }
            else if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
            }
            else if (string.IsNullOrEmpty(pModel.Status))
            {
                rtn.Message = "Status should not be empty";
                rtn.Status = false;
            }
            else if (pModel.DetailCreditLimit != null)
            {
                bool check = false;
                for (int index = 0; index < pModel.DetailCreditLimit.Count(); index++)
                {
                    string fromDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[index].dDate.ToUpper().Substring(0, 10), true).Replace("-", "/");
                    string toDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[index].dDate.ToUpper().Substring(14, 10), true).Replace("-", "/");
                    DateTime fDate = parseInputDate(fromDate);
                    DateTime tDate = parseInputDate(toDate);
                    for (int i = 0; i < pModel.DetailCreditLimit.Count(); i++)
                    {
                        fromDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[i].dDate.ToUpper().Substring(0, 10), true).Replace("-", "/");
                        toDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[i].dDate.ToUpper().Substring(14, 10), true).Replace("-", "/");
                        DateTime cfDate = parseInputDate(fromDate);
                        DateTime ctDate = parseInputDate(toDate);
                        if (index != i)
                        {
                            if (fDate >= cfDate && tDate <= ctDate || fDate <= cfDate && tDate >= ctDate)
                            {
                                rtn.Message = "Period Date is Duplicate";
                                rtn.Status = false;
                                check = true;
                                break;
                            }
                        }
                    }
                    if (check) break;
                }
                if (!check)
                {
                    foreach (var item in pModel.DetailCreditLimit)
                    {
                        if (string.IsNullOrEmpty(item.dDate))
                        {
                            rtn.Message = "Period Date should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.CreditLimit))
                        {
                            rtn.Message = "Period CreditLimit should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(item.Status))
                        {
                            rtn.Message = "Period Status should not be empty";
                            rtn.Status = false;
                            break;
                        }
                    }
                }
            }
            else if (pModel.DetailCds != null)
            {
                foreach (var item in pModel.DetailCds)
                {
                    if (string.IsNullOrEmpty(item.Date))
                    {
                        rtn.Message = "CDS Date should not be empty";
                        rtn.Status = false;
                        break;
                    }
                    else if (string.IsNullOrEmpty(item.Cds))
                    {
                        rtn.Message = "CDS Value should not be empty";
                        rtn.Status = false;
                        break;
                    }
                }
            }
            return rtn;
        }

        public static string getCounterAdditional(string cp)
        {
            string result = "";
            if (string.IsNullOrEmpty(cp) == false)
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = from v in context.HEDG_MT_CP
                                where
                                v.HMC_FK_VENDOR.Equals(cp)
                                select new
                                {
                                    HMC_ROW_ID = v.HMC_ROW_ID,
                                    HMC_CREDIT_LIMIT = v.HMC_CREDIT_LIMIT,
                                    HMC_CREDIT_RATING = v.HMC_CREDIT_RATING
                                };
                    if (query.Count() != 0)
                    {
                        var query2 = query.ToList().FirstOrDefault();
                        result = "Credit Rating: " + query2.HMC_CREDIT_RATING + " Credit Remaining: " + query2.HMC_CREDIT_LIMIT + " THB";
                    }
                }
            }
            return result;
        }

    }
}
