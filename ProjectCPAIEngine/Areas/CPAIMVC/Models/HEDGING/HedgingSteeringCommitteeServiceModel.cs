﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.Globalization;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingSteeringCommitteeServiceModel
    {
        private DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ReturnValue ValidateSubmit(EntityCPAIEngine context, HedgingSteeringCommitteeViewModel_DetailList pModel, int rowID)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            rtn.Message = string.Empty;
            List<HEDG_FW_COMT> query;
            ShareFn shareFn = new ShareFn();
            DateTime fDate, tDate;
            string fromDate, toDate;
            
            fromDate = shareFn.ConvertDateFormat(pModel.detail[rowID].ActiveDate.ToUpper().Substring(0, 10), true).Replace("-", "/");
            toDate = shareFn.ConvertDateFormat(pModel.detail[rowID].ActiveDate.ToUpper().Substring(14, 10), true).Replace("-", "/");
            fDate = parseInputDate(fromDate);
            tDate = parseInputDate(toDate);
            if (fDate > tDate)
            {
                rtn.Status = false;
                rtn.Message = "Date incorrect format";
                return rtn;
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (string.IsNullOrEmpty(pModel.detail[rowID].OrderID))
            {
                query = context.HEDG_FW_COMT.Where(p => p.HFC_FK_UNDERLYING.ToLower().Contains(pModel.Underlying.ToLower())
                                && ((DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_START) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_START) <= DbFunctions.TruncateTime(tDate))
                                    || (DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_END) >= DbFunctions.TruncateTime(fDate) && DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_END) <= DbFunctions.TruncateTime(tDate)))
                                ).ToList();
                if (query != null && query.Count() > 0)
                {
                    rtn.Status = false;
                    rtn.Message = "Underlying or Date is duplicate";
                }
                else
                {
                    query = context.HEDG_FW_COMT.Where(p => p.HFC_FK_UNDERLYING.ToLower().Contains(pModel.Underlying.ToLower())
                                    && ((DbFunctions.TruncateTime(fDate) >= DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_START) && DbFunctions.TruncateTime(tDate) <= DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_END))
                                        || (DbFunctions.TruncateTime(fDate) <= DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_START) && DbFunctions.TruncateTime(tDate) >= DbFunctions.TruncateTime(p.HFC_ACTIVEDATE_END)))
                                    ).ToList();
                    if (query != null && query.Count() > 0)
                    {
                        rtn.Status = false;
                        rtn.Message = "Underlying or Date is duplicate";
                    }
                }
            }        

            return rtn;
        }

        private ReturnValue ValidateInput(ref HedgingSteeringCommitteeViewModel_DetailList pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            rtn.Message = string.Empty;

            if (string.IsNullOrEmpty(pUser))
            {
                rtn.Message = "User should not be empty";
                rtn.Status = false;
            }
            else if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;               
            }
            else if (string.IsNullOrEmpty(pModel.Underlying))
            {
                rtn.Message = "Underlying should not be empty";
                rtn.Status = false;
            }
            else if (string.IsNullOrEmpty(pModel.CropPlan))
            {
                rtn.Message = "CropPlan should not be empty";
                rtn.Status = false;
            }
            else
            {
                foreach (var detail in pModel.detail)
                {
                    if (string.IsNullOrEmpty(detail.Production))
                    {
                        rtn.Message = "Production should not be empty";
                        rtn.Status = false;
                    }
                    /*else if (string.IsNullOrEmpty(detail.UnitPrice))
                    {
                        rtn.Message = "UnitPrice should not be empty";
                        rtn.Status = false;
                    }
                    else if (string.IsNullOrEmpty(detail.UnitVolume))
                    {
                        rtn.Message = "UnitVolume should not be empty";
                        rtn.Status = false;
                    }*/
                    else if (string.IsNullOrEmpty(detail.ActiveDate_FromMonth))
                    {
                        rtn.Message = "ActiveDate_FromMonth should not be empty";
                        rtn.Status = false;
                    }
                    else if (string.IsNullOrEmpty(detail.ActiveDate_FromYear))
                    {
                        rtn.Message = "ActiveDate_FromYear should not be empty";
                        rtn.Status = false;
                    }
                    else if (string.IsNullOrEmpty(detail.ActiveDate_ToMonth))
                    {
                        rtn.Message = "ActiveDate_ToMonth should not be empty";
                        rtn.Status = false;
                    }
                    else if (string.IsNullOrEmpty(detail.ActiveDate_ToYear))
                    {
                        rtn.Message = "ActiveDate_ToYear should not be empty";
                        rtn.Status = false;
                    }
                    foreach (var detailData in detail.DetailData)
                    {
                        if (string.IsNullOrEmpty(detailData.Order))
                        {
                            rtn.Message = "Order should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(detailData.Price))
                        {
                            rtn.Message = "Price should not be empty";
                            rtn.Status = false;
                            break;
                        }
                        else if (string.IsNullOrEmpty(detailData.Volume))
                        {
                            rtn.Message = "Volume should not be empty";
                            rtn.Status = false;
                            break;
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////mode edit
            /*if (!add)
            {
                foreach (var detail in pModel.detail)
                {
                    if (string.IsNullOrEmpty(detail.OrderID))
                    {
                        rtn.Message = "Order should not be empty";
                        rtn.Status = false;
                    }
                }
            }*/
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            if (rtn.Status)
            {
                try
                {
                    for (int i = 0; i < pModel.detail.Count; i++)
                    {
                        pModel.detail[i].ActiveDate = "01"
                                        + "/" + pModel.detail[i].ActiveDate_FromMonth.PadLeft(2, '0') + "/" + pModel.detail[i].ActiveDate_FromYear
                                        + " to "
                                        + DateTime.DaysInMonth(Convert.ToInt16(pModel.detail[i].ActiveDate_ToYear), Convert.ToInt16(pModel.detail[i].ActiveDate_ToMonth))
                                        + "/" + pModel.detail[i].ActiveDate_ToMonth.PadLeft(2, '0') + "/" + pModel.detail[i].ActiveDate_ToYear;
                        pModel.detail[i].ActiveDate.ToUpper().Substring(0, 10);
                        pModel.detail[i].ActiveDate.ToUpper().Substring(14, 10);
                    }
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Active date invalid format";
                }
            }
            return rtn;
        }

        private List<HedgingSteeringCommitteeViewModel_DetailData> getDetailData(EntityCPAIEngine context, string OrderID)
        {
            List<HedgingSteeringCommitteeViewModel_DetailData> detailData = new List<HedgingSteeringCommitteeViewModel_DetailData>();
            List<HEDG_FW_COMT_DETAIL> query;

            query = context.HEDG_FW_COMT_DETAIL.Where(p => p.HFCD_FK_COMMITTEE.ToLower().Contains(OrderID)).OrderBy(p => p.HFCD_ORDER).ToList();
            if (query != null && query.Count() > 0)
            {
                foreach (var item in query)
                {
                    detailData.Add(new HedgingSteeringCommitteeViewModel_DetailData
                    {
                        OrderID = OrderID,
                        RowID = item.HFCD_ROW_ID,
                        Order = item.HFCD_ORDER,
                        Price = item.HFCD_PRICE,
                        Volume = item.HFCD_VOLUME
                    });
                }
            }

            return detailData;
        }

        private void deleteOrder(HedgingSteeringCommitteeViewModel_DetailList pModel, EntityCPAIEngine context)
        {
            List<string> orderID = new List<string>();
            foreach (var item in pModel.detail)
            {
                if (!string.IsNullOrEmpty(item.OrderID))
                    orderID.Add(item.OrderID);
            }
            if (orderID.Count > 0)
            {
                var query = from v in context.HEDG_FW_COMT
                            where v.HFC_FK_UNDERLYING.Trim().ToLower() == pModel.Underlying.Trim().ToLower() && v.HFC_CROP_PLAN.Trim().ToLower() == pModel.CropPlan.Trim().ToLower() && !orderID.Contains(v.HFC_ROW_ID)
                            select v;
                foreach (var item in query)
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_FW_COMT WHERE HFC_ROW_ID = '" + item.HFC_ROW_ID + "'");
                    context.SaveChanges();
                    new HEDG_FW_COMT_DETAIL_DAL().DeleteByFK(item.HFC_ROW_ID, context);
                }
            }
        }

        private void getUnderlyingUnit(HedgingSteeringCommitteeViewModel_DetailList pModel)
        {
            string underlying, unitPrice, unitVolume, unitVolumeMnt;
            string[] unit;
            List<SelectListItem> underlyingList = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
            foreach(var item in underlyingList)
            {
                unit = item.Value.Split(char.Parse("|"));
                underlying = unit[0];
                unitPrice = unit[1];
                unitVolume = unit[2];
                unitVolumeMnt = unit[3];
                if (underlying.Trim().ToLower().Equals(pModel.Underlying.Trim().ToLower()))
                {                    
                    pModel.UnitPrice = unitPrice;
                    pModel.UnitVolume = unitVolume;
                    break;
                }
            }
        }

        private string getUnderlyingName(List<SelectListItem> underlyingList, string underlyingSearch)
        {
            string underlying, unitPrice, unitVolume, unitVolumeMnt;
            string[] unit;
            
            foreach (var item in underlyingList)
            {
                unit = item.Value.Split(char.Parse("|"));
                underlying = unit[0];
                unitPrice = unit[1];
                unitVolume = unit[2];
                unitVolumeMnt = unit[3];
                if (underlying.Trim().ToLower().Equals(underlyingSearch.Trim().ToLower()))
                {
                    return item.Text.Trim();                    
                }
            }

            return string.Empty;
        }

        public List<string> getUnderlyingCommittee()
        {
            List<string> result = new List<string>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_FW_COMT                            
                            select new
                            {
                                Underlying = v.HFC_FK_UNDERLYING
                            }).Distinct();
                foreach (var item in query)
                {
                    if (!string.IsNullOrEmpty(item.Underlying.Trim()))
                        result.Add(item.Underlying);
                }
            }
            return result;
        }       

        public ReturnValue Update(HedgingSteeringCommitteeViewModel_DetailList pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                rtn = ValidateInput(ref pModel, pUser);
                if (!rtn.Status)
                    return rtn;

                HEDG_FW_COMT_DAL dal = new HEDG_FW_COMT_DAL();
                HEDG_FW_COMT ent = new HEDG_FW_COMT();
                HEDG_FW_COMT_DETAIL_DAL dalDetail = new HEDG_FW_COMT_DETAIL_DAL();
                HEDG_FW_COMT_DETAIL entDetail = new HEDG_FW_COMT_DETAIL();
                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            deleteOrder(pModel, context);
                            getUnderlyingUnit(pModel);
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            for (int i = 0; i < pModel.detail.Count; i++)
                            {
                                rtn = ValidateSubmit(context, pModel, i);
                                if (!rtn.Status)
                                    continue;
                                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                                ent = new HEDG_FW_COMT();
                                var CtrlID = ConstantPrm.GetDynamicCtrlID();
                                if (string.IsNullOrEmpty(pModel.detail[i].OrderID))
                                    ent.HFC_ROW_ID = CtrlID;
                                else
                                    ent.HFC_ROW_ID = pModel.detail[i].OrderID;
                                ent.HFC_PERCENT_PRD = pModel.detail[i].Production;
                                if (string.IsNullOrEmpty(pModel.detail[i].OrderID))
                                {
                                    ent.HFC_FK_UNDERLYING = pModel.Underlying;
                                    ent.HFC_CROP_PLAN = pModel.CropPlan;
                                    ent.HFC_ACTIVEDATE_START = ShareFn.ConvertStrDateToDate(pModel.detail[i].ActiveDate.ToUpper().Substring(0, 10));
                                    ent.HFC_ACTIVEDATE_END = ShareFn.ConvertStrDateToDate(pModel.detail[i].ActiveDate.ToUpper().Substring(14, 10));
                                    ent.HFC_UNIT_PRICE = pModel.UnitPrice;
                                    ent.HFC_UNIT_VOLUME = pModel.UnitVolume;
                                    ent.HFC_CREATED_BY = pUser;
                                    ent.HFC_CREATED_DATE = now;
                                    ent.HFC_UPDATED_BY = pUser;
                                    ent.HFC_UPDATED_DATE = now;
                                    dal.Save(ent, context);
                                }
                                else
                                {
                                    ent.HFC_UPDATED_BY = pUser;
                                    ent.HFC_UPDATED_DATE = now;
                                    dal.Update(ent, context);

                                    dalDetail.DeleteByFK(pModel.detail[i].OrderID, context);
                                    for (int j = 0; j < pModel.detail[i].DetailData.Count; j++)
                                        pModel.detail[i].DetailData[j].RowID = string.Empty;
                                }
                                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                                foreach (var detailData in pModel.detail[i].DetailData)
                                {
                                    entDetail = new HEDG_FW_COMT_DETAIL();
                                    if (string.IsNullOrEmpty(pModel.detail[i].OrderID))
                                        entDetail.HFCD_FK_COMMITTEE = CtrlID;
                                    else
                                        entDetail.HFCD_FK_COMMITTEE = pModel.detail[i].OrderID;
                                    entDetail.HFCD_ORDER = detailData.Order;
                                    entDetail.HFCD_PRICE = detailData.Price;
                                    entDetail.HFCD_VOLUME = detailData.Volume;
                                    if (string.IsNullOrEmpty(detailData.RowID))
                                    {
                                        entDetail.HFCD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        entDetail.HFCD_CREATED_BY = pUser;
                                        entDetail.HFCD_CREATED_DATE = now;
                                        entDetail.HFCD_UPDATED_BY = pUser;
                                        entDetail.HFCD_UPDATED_DATE = now;
                                        dalDetail.Save(entDetail, context);
                                    }
                                    else
                                    {
                                        entDetail.HFCD_ROW_ID = detailData.RowID;
                                        entDetail.HFCD_UPDATED_BY = pUser;
                                        entDetail.HFCD_UPDATED_DATE = now;
                                        dalDetail.Update(entDetail, context);
                                    }
                                }
                            }
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            if (rtn.Status)
                            {
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }       

        public ReturnValue Search(ref HedgingSteeringCommitteeViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            string sDateFrom, sDateTo, sUnderlying, sCropPlan;
            try
            {
                try
                {
                    if (pModel.sActiveDate_FromMonth != null && pModel.sActiveDate_FromYear != null && pModel.sActiveDate_ToMonth != null && pModel.sActiveDate_ToYear != null)
                    {
                        pModel.sActiveDate = "01"
                                            + "/" + pModel.sActiveDate_FromMonth.PadLeft(2, '0') + "/" + pModel.sActiveDate_FromYear
                                            + " to "
                                            + DateTime.DaysInMonth(Convert.ToInt16(pModel.sActiveDate_ToYear), Convert.ToInt16(pModel.sActiveDate_ToMonth))
                                            + "/" + pModel.sActiveDate_ToMonth.PadLeft(2, '0') + "/" + pModel.sActiveDate_ToYear;
                    }
                    sDateFrom = String.IsNullOrEmpty(pModel.sActiveDate) ? "" : pModel.sActiveDate.ToUpper().Substring(0, 10);
                    sDateTo = String.IsNullOrEmpty(pModel.sActiveDate) ? "" : pModel.sActiveDate.ToUpper().Substring(14, 10);
                    sUnderlying = String.IsNullOrEmpty(pModel.sUnderlying) ? "" : pModel.sUnderlying.ToUpper();
                    sCropPlan = String.IsNullOrEmpty(pModel.sCropPlan) ? "" : pModel.sCropPlan.ToUpper();
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_FW_COMT
                                 orderby new { v.HFC_CROP_PLAN, v.HFC_FK_UNDERLYING }
                                 select new
                                 {
                                     dOrderID = v.HFC_ROW_ID,
                                     dActiveDateStart = (DateTime?)v.HFC_ACTIVEDATE_START,
                                     dActiveDateEnd = (DateTime?)v.HFC_ACTIVEDATE_END,
                                     dUnderlying = v.HFC_FK_UNDERLYING,
                                     dCropPlan = v.HFC_CROP_PLAN,
                                     dProduction = v.HFC_PERCENT_PRD,
                                     dUnitPrice = v.HFC_UNIT_PRICE,
                                     dUnitVolume = v.HFC_UNIT_VOLUME
                                 });
                    if (!string.IsNullOrEmpty(sUnderlying))
                        query = query.Where(p => p.dUnderlying.ToUpper().Contains(sUnderlying));
                    if (!string.IsNullOrEmpty(sCropPlan))
                        query = query.Where(p => p.dCropPlan.Trim().ToUpper().Equals(sCropPlan.Trim()));
                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dActiveDateStart >= sDate && p.dActiveDateEnd <= eDate);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (query != null)
                    {
                        pModel.SearchData = new List<HedgingSteeringCommitteeViewModel_SearchData>();
                        List<SelectListItem> underlyingList = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
                        foreach (var g in query)
                        {
                            pModel.SearchData.Add(
                                                    new HedgingSteeringCommitteeViewModel_SearchData
                                                    {
                                                        dOrderID = g.dOrderID.Trim(),
                                                        dUnderlying = g.dUnderlying,
                                                        dUnderlyingName = getUnderlyingName(underlyingList, g.dUnderlying),
                                                        dCropPlan = g.dCropPlan,
                                                        dProduction = g.dProduction,
                                                        dUnitPrice = g.dUnitPrice,
                                                        dUnitVolume = g.dUnitVolume,
                                                        dActiveDate = (g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("MMM yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                                        + " - " + (g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("MMM yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                                        DetailData = getDetailData(context, g.dOrderID)
                                                    });
                        }                        
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HedgingSteeringCommitteeViewModel_DetailList GetDetail(string underlying, string cropPlan)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingSteeringCommitteeViewModel_DetailList detailList = new HedgingSteeringCommitteeViewModel_DetailList();
            string sUnderlying, sCropPlan;

            try
            {
                sUnderlying = String.IsNullOrEmpty(underlying) ? "" : underlying;
                sCropPlan = String.IsNullOrEmpty(cropPlan) ? "" : cropPlan;
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_FW_COMT
                                 orderby new { v.HFC_CROP_PLAN, v.HFC_FK_UNDERLYING, v.HFC_ACTIVEDATE_START, v.HFC_ACTIVEDATE_END }
                                 select new
                                 {
                                     dOrderID = v.HFC_ROW_ID,                                     
                                     dUnderlying = v.HFC_FK_UNDERLYING,
                                     dCropPlan = v.HFC_CROP_PLAN,
                                     dProduction = v.HFC_PERCENT_PRD,
                                     dUnitPrice = v.HFC_UNIT_PRICE,
                                     dUnitVolume = v.HFC_UNIT_VOLUME,
                                     dActiveDateStart = (DateTime?)v.HFC_ACTIVEDATE_START,
                                     dActiveDateEnd = (DateTime?)v.HFC_ACTIVEDATE_END
                                 });
                    if (!string.IsNullOrEmpty(sUnderlying))
                        query = query.Where(p => p.dUnderlying.ToUpper().Contains(sUnderlying.ToUpper()));
                    if (!string.IsNullOrEmpty(sCropPlan))
                        query = query.Where(p => p.dCropPlan.ToUpper().Contains(sCropPlan.ToUpper()));
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if (query != null)
                    {                        
                        detailList.Underlying = underlying;
                        detailList.CropPlan = cropPlan;
                        getUnderlyingUnit(detailList);
                        detailList.detail = new List<HedgingSteeringCommitteeViewModel_Detail>();
                        foreach (var g in query)
                        {                            
                            detailList.detail.Add(new HedgingSteeringCommitteeViewModel_Detail
                            {
                                OrderID = g.dOrderID.Trim(),
                                Production = g.dProduction,
                                UnitPrice = g.dUnitPrice,
                                UnitVolume = g.dUnitVolume,
                                ActiveDate = (g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                + " To " + (g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                ActiveDate_FromMonth = g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("MM", System.Globalization.CultureInfo.InvariantCulture),
                                ActiveDate_FromYear = g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                ActiveDate_ToMonth = g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("MM", System.Globalization.CultureInfo.InvariantCulture),
                                ActiveDate_ToYear = g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                DetailData = getDetailData(context, g.dOrderID)
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return detailList;
        }
    }
}
