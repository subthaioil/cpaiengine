﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.Script.Serialization;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;

using com.pttict.engine.utility;
using com.pttict.engine.dal.Entity;
using com.pttict.downstream.common.utilities;

using ProjectCPAIEngine.Model;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingTicketServiceModel
    {
        ShareFn _FN = new ShareFn();
        public static string getTransactionByID(string id)
        {
            try
            {
                TradingTicketRootObject rootObj = new TradingTicketRootObject();
                rootObj.trading_ticket = new HedgingTicketViewModel.TradingTicket();
                rootObj.deal_detail = new HedgingTicketViewModel.DealDetail();
                rootObj.deal_detail.hedging_target = new List<HedgingTicketViewModel.HedgingTarget>();
                rootObj.deal_detail.hedging_chart = new HedgingTicketViewModel.HedgingChart();
                rootObj.deal_detail.hedging_execution = new HedgingTicketViewModel.HedgingExecution();
                rootObj.deal_detail.hedging_execution.hedging_execution_data = new List<HedgingTicketViewModel.HedgingExecutionData>();
                rootObj.deal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
                rootObj.deal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();
                //rootObj.deal_detail.action = "E";
                rootObj.deal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
                rootObj.deal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();

                HEDG_TICKET_DAL TICKET_DAL = new HEDG_TICKET_DAL();
                HEDG_TICKET data = TICKET_DAL.GetTicketByTicketId(id);
                if (data != null)
                {
                    rootObj.trading_ticket.ticket_no = data.HTDA_TICKET_NO;
                    rootObj.trading_ticket.ticket_date = (data.HTDA_TICKET_DATE == DateTime.MinValue || data.HTDA_TICKET_DATE == null) ? "" : Convert.ToDateTime(data.HTDA_TICKET_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    rootObj.trading_ticket.trader = data.HTDA_TRADER;
                    rootObj.trading_ticket.status = data.HTDA_STATUS;
                    rootObj.trading_ticket.revision = data.HTDA_REVISION;
                    rootObj.trading_ticket.note = data.HTDA_REMARK;
                    rootObj.trading_ticket.reason = data.HTDA_REASON;

                    rootObj.deal_detail.deal_type = data.HTDA_DEAL_TYPE;
                    rootObj.deal_detail.underlying_id = data.HTDA_UNDERLYING_ID;
                    rootObj.deal_detail.underlying_name = data.HTDA_UNDERLYING_NAME;
                    rootObj.deal_detail.underlying_vs_id = data.HTDA_UNDERLYING_VS_ID;
                    rootObj.deal_detail.underlying_vs_name = data.HTDA_UNDERLYING_VS_NAME;
                    rootObj.deal_detail.tool_id = data.HTDA_TOOL_ID;
                    rootObj.deal_detail.tool_name = data.HTDA_TOOL_NAME;
                    rootObj.deal_detail.annual_detail_id = data.HTDA_FK_FW_ANNUAL_D;
                    rootObj.deal_detail.hedg_type_name = data.HTDA_HEDG_TYPE_NAME;

                    HEDG_TICKET_TARGET_DAL TARGET_DAL = new HEDG_TICKET_TARGET_DAL();
                    List<HEDG_TICKET_TARGET> targetdata = TARGET_DAL.GetTicketTarget(id);
                    foreach (var list in targetdata)
                    {
                        HedgingTicketViewModel.HedgingTarget item = new HedgingTicketViewModel.HedgingTarget();
                        item.order = list.HTDA_ORDER;
                        item.tenor = list.HTDA_TENOR;
                        item.price = list.HTDA_PRICE.ToString();
                        item.volume = list.HTDA_VOLUME.ToString();
                        item.remaining = list.HTDA_REMAINING.ToString();
                        rootObj.deal_detail.hedging_target.Add(item);
                    }

                    rootObj.deal_detail.hedging_chart.hedged_vol = data.HTDA_CHART_VOL;
                    rootObj.deal_detail.hedging_chart.limit = data.HTDA_CHART_LIMIT;
                    rootObj.deal_detail.hedging_chart.target = data.HTDA_CHART_TARGET;
                    rootObj.deal_detail.hedging_execution.price_unit = data.HTDA_EXC_PRC_UNIT;
                    rootObj.deal_detail.hedging_execution.volume_mnt_unit = data.HTDA_EXC_VOL_UNIT;

                    HEDG_TICKET_EXECUTE_DAL EXECUTE_DAL = new HEDG_TICKET_EXECUTE_DAL();
                    List<HEDG_TICKET_EXECUTE> executedata = EXECUTE_DAL.GetTicketExecute(id);
                    foreach (var list in executedata)
                    {
                        HedgingTicketViewModel.HedgingExecutionData item = new HedgingTicketViewModel.HedgingExecutionData();
                        HedgingDealServiceModel service_deal = new HedgingDealServiceModel();
                        item.deal_status = service_deal.GetStatusByDealNo(list.HTDA_DEAL_NO);
                        item.deal_id = list.HTDA_FK_DEAL;
                        item.deal_date = (list.HTDA_DEAL_DATE == DateTime.MinValue || list.HTDA_DEAL_DATE == null) ? "" : Convert.ToDateTime(list.HTDA_DEAL_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        item.deal_no = list.HTDA_DEAL_NO;
                        item.tenor = list.HTDA_TENOR;
                        item.counter_parties_id = list.HTDA_COUNTER_PARTIES_ID;
                        item.counter_parties_name = list.HTDA_COUNTER_PARTIES_NAME;
                        item.bid_price = JSonConvertUtil.jsonToModel<List<HedgingTicketViewModel.BidPrice>>(list.HTDA_BID_PRICE);
                        item.price = list.HTDA_PRICE.ToString();
                        item.volume = list.HTDA_VOLUME.ToString();
                        rootObj.deal_detail.hedging_execution.hedging_execution_data.Add(item);
                    }
                    rootObj.deal_detail.hedging_execution.hedging_position.price_unit = data.HTDA_EXC_PST_PRC_UNIT;
                    rootObj.deal_detail.hedging_execution.hedging_position.volume_unit = data.HTDA_EXC_PST_VOL_UNIT;

                    HEDG_TICKET_POST_DAL POST_DAL = new HEDG_TICKET_POST_DAL();
                    List<HEDG_TICKET_POST> postdata = POST_DAL.GetTicketPost(id);
                    foreach (var list in postdata)
                    {
                        HedgingTicketViewModel.HedgingPositionData item = new HedgingTicketViewModel.HedgingPositionData();
                        item.price = list.HTDA_PRICE.ToString();
                        item.volume = list.HTDA_VOLUME.ToString();
                        item.tenor = list.HTDA_TENOR;
                        rootObj.deal_detail.hedging_execution.hedging_position.hedging_position_data.Add(item);
                    }
                }
                return new JavaScriptSerializer().Serialize(rootObj);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string getAttFileByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            HEDG_TICKET_ATTACH_FILE_DAL fileDal = new HEDG_TICKET_ATTACH_FILE_DAL();
            List<HEDG_TICKET_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].HTAF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        private List<HedgingTicketViewModel_Detail> getDealDetail(string ticketNo, string deal, string counterParty)
        {
            List<HedgingTicketViewModel_Detail> detail = new List<HedgingTicketViewModel_Detail>();
            string[] listDeal = new string[0];
            string[] listCounterParty = new string[0];
            string cp;

            if (deal != null)
                listDeal = deal.Split(char.Parse("|"));
            if (counterParty != null)
                listCounterParty = counterParty.Split(char.Parse("|"));            
            for (int i = 0; i < listDeal.Length; i++)
            {
                if (listDeal.Length == listCounterParty.Length)
                    cp = listCounterParty[i];
                else
                    cp = string.Empty;
                detail.Add(new HedgingTicketViewModel_Detail
                {
                    ref_ticket_no = ticketNo,
                    deal_no = listDeal[i],                    
                    counterParty = cp
                });                
            }

            return detail;
        }

        private List<HedgingTicketViewModel.BidPrice> getBridPrice(EntityCPAIEngine context, string dealId)
        {
            List<HedgingTicketViewModel.BidPrice> detail = new List<HedgingTicketViewModel.BidPrice>();

            var query = (from d in context.HEDG_DEAL_BID_PRICE
                         join v in context.MT_VENDOR on d.HDBP_FK_CONTER_PARTIES equals v.VND_ACC_NUM_VENDOR
                         select new
                         {
                             dHDBP_FK_DEAL = d.HDBP_FK_DEAL,
                             dBid_Price_Counter_id = d.HDBP_FK_CONTER_PARTIES,
                             dBid_Price_Counter_name = v.VND_NAME1,
                             dBid_Price = d.HDBP_PRICE,
                             dBid_Order = d.HDBP_ORDER                             
                         }).ToList();
            if (!String.IsNullOrEmpty(dealId.Trim()))
                query = query.Where(x => x.dHDBP_FK_DEAL != null && x.dHDBP_FK_DEAL.Trim().ToLower().Equals(dealId.Trim().ToLower())).ToList();
            if (query != null)
            {
                foreach (var item in query)
                {
                    detail.Add(new HedgingTicketViewModel.BidPrice
                    {
                        order = item.dBid_Order,
                        price = item.dBid_Price.ToString(),
                        counter_parties_id = item.dBid_Price_Counter_id,
                        counter_parties_name = item.dBid_Price_Counter_name
                    });
                }
            }

            return detail;
        }

        private string getUnderlyingName(List<SelectListItem> underlyingList, string underlyingSearch)
        {
            string underlying, unitPrice, unitVolume, unitVolumeMnt;
            string[] unit;

            foreach (var item in underlyingList)
            {
                unit = item.Value.Split(char.Parse("|"));
                underlying = unit[0];
                unitPrice = unit[1];
                unitVolume = unit[2];
                unitVolumeMnt = unit[3];
                if (underlyingSearch != null && underlying.Trim().ToLower().Equals(underlyingSearch.Trim().ToLower()))
                {
                    return item.Text.Trim();
                }
            }

            return string.Empty;
        }


        private string getNameFromList(List<SelectListItem> list, string value)
        {
            var name = list.SingleOrDefault(a => a.Value == value);
            if (name != null)
                return name.Text;
            else
                return value;
        }

        public ReturnValue LoadButton(string transactionID, string Type, string System, ref HedgingTicketViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            RequestCPAI req = new RequestCPAI();
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService projService = new ServiceProvider.ProjService();

            try
            {
                req.Function_id = ConstantPrm.FUNCTION.F10000051;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = System });
                req.Req_parameters.P.Add(new P { K = "type", V = Type });
                req.Req_parameters.P.Add(new P { K = "transaction_id", V = transactionID });
                req.Extra_xml = "";

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = projService.CallService(reqData);
                if (!resData.result_code.Trim().Equals("1"))
                {
                    rtn.Status = false;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
                string _DataJson = resData.extra_xml;
                ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
                if (_model.data_detail != null)
                {
                    var modelRoot = new JavaScriptSerializer().Deserialize<TradingTicketRootObject>(_model.data_detail);

                    model.trading_ticket = modelRoot.trading_ticket;
                    model.deal_detail = modelRoot.deal_detail;

                }
                if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
                {
                    model.buttonMode = "MANUAL";
                    BindButtonPermission(_model.buttonDetail.Button, ref model);
                }
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }

            return rtn;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref HedgingTicketViewModel model)
        {
            model.buttonMode = "MANUAL";
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
            {
                model.pageMode = "EDIT";
            }
            string _btn = "";
            foreach (ButtonAction _button in Button)
            {
                _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                model.buttonCode += _btn;
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            //model.buttonCode += "<input type='submit' value='MANUAL' id='btnSaveDraft' style='display: none;' name='action:SaveDraft' />";
        }

        public ReturnValue Search(ref HedgingTicketViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            RequestCPAI req = new RequestCPAI();
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService projService = new ServiceProvider.ProjService();
            List<Transaction> _transaction = new List<Flow.Model.Transaction>();
            List_TCEtrx _model;
            string[] lstStatus;
            string _strfrom, _strTo, sDealNo, sTicketNo, sStatus, sDealType, sTool, sUnderlying, sUnderlyingVS, sTrader, sCounterParty;
            pModel.systemType = pModel.systemType.Decrypt();
            try
            {
                try
                {
                    _strfrom = String.IsNullOrEmpty(pModel.sTicketDate) ? string.Empty : pModel.sTicketDate.Trim().Substring(0, 10);
                    _strTo = String.IsNullOrEmpty(pModel.sTicketDate) ? string.Empty : pModel.sTicketDate.Trim().Substring(14, 10);
                    _strfrom = _strfrom.Replace("-", "/");
                    _strTo = _strTo.Replace("-", "/");
                    sTicketNo = String.IsNullOrEmpty(pModel.sTicketNo) ? string.Empty : pModel.sTicketNo.Trim().ToLower();
                    sDealNo = String.IsNullOrEmpty(pModel.sDealNo) ? string.Empty : pModel.sDealNo.Trim().ToLower();
                    sStatus = String.IsNullOrEmpty(pModel.sStatus) ? string.Empty : pModel.sStatus.Trim().ToLower();
                    lstStatus = String.IsNullOrEmpty(pModel.sStatusSelected) ? lstStatus = new string[0] : pModel.sStatusSelected.Trim().ToLower().Split(char.Parse("|"));
                    sDealType = String.IsNullOrEmpty(pModel.sDealType) ? string.Empty : pModel.sDealType.Trim().ToLower();
                    sTool = String.IsNullOrEmpty(pModel.sTool) ? string.Empty : pModel.sTool.Trim().ToLower();
                    sUnderlying = String.IsNullOrEmpty(pModel.sUnderlying) ? string.Empty : pModel.sUnderlying.Trim().ToLower();
                    sUnderlyingVS = String.IsNullOrEmpty(pModel.sVS) ? string.Empty : pModel.sVS.Trim().ToLower();
                    sTrader = String.IsNullOrEmpty(pModel.sTrader) ? string.Empty : pModel.sTrader.Trim().ToLower();
                    sCounterParty = String.IsNullOrEmpty(pModel.sCounterParty) ? string.Empty : pModel.sCounterParty.Trim().ToLower();
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                req.Function_id = ConstantPrm.FUNCTION.F10000050;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_TCKT });
                req.Req_parameters.P.Add(new P { K = "type", V = pModel.systemType });
                req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
                req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
                req.Req_parameters.P.Add(new P { K = "status", V = sStatus });
                req.Req_parameters.P.Add(new P { K = "from_date", V = _strfrom });
                req.Req_parameters.P.Add(new P { K = "to_date", V = _strTo });
                //req.Req_parameters.P.Add(new P { K = "index_5", V = (_strfrom != "" && _strTo != "") ? _strfrom + "|" + _strTo : "" });
                req.Extra_xml = "";

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = projService.CallService(reqData);
                if (!resData.result_code.Trim().Equals("1"))
                {
                    rtn.Status = false;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
                _model = ShareFunction.DeserializeXMLFileToObject<List_TCEtrx>(resData.extra_xml);
                if (_model == null || _model.TCETransaction == null || _model.TCETransaction.Count < 1)
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                    return rtn;
                }
                _transaction = _model.TCETransaction;
                if (_transaction == null || _transaction.Count < 1)
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                    return rtn;
                }
                _transaction = _transaction.OrderByDescending(a => a.ticket_no).ToList();
                if (!String.IsNullOrEmpty(sTicketNo))
                    _transaction = _transaction.Where(x => x.ticket_no != null && x.ticket_no.Trim().ToLower().Contains(sTicketNo)).ToList();                
                if (!String.IsNullOrEmpty(sDealType))
                    _transaction = _transaction.Where(x => x.deal_type != null && x.deal_type.Trim().ToLower().Equals(sDealType)).ToList();
                if (!String.IsNullOrEmpty(sTool))
                    _transaction = _transaction.Where(x => x.tool != null && x.tool.Trim().ToLower().Equals(sTool)).ToList();
                if (!String.IsNullOrEmpty(sUnderlying))
                    _transaction = _transaction.Where(x => x.underlying != null && x.underlying.Trim().ToLower().Equals(sUnderlying)).ToList();
                if (!String.IsNullOrEmpty(sUnderlyingVS))
                    _transaction = _transaction.Where(x => x.underlying_vs != null && x.underlying_vs.Trim().ToLower().Equals(sUnderlyingVS)).ToList();
                if (!String.IsNullOrEmpty(sTrader))
                    _transaction = _transaction.Where(x => x.trade_for != null && x.trade_for.Trim().ToLower().Contains(sTrader)).ToList();
                if (!string.IsNullOrEmpty(_strfrom) && !string.IsNullOrEmpty(_strTo))
                {
                    DateTime sDate = new DateTime();
                    DateTime eDate = new DateTime();
                    sDate = ShareFn.ConvertStrDateToDate(_strfrom);
                    eDate = ShareFn.ConvertStrDateToDate(_strTo).AddDays(1).AddSeconds(-1);                                  
                    _transaction = _transaction.Where(x => x.ticket_date != null && Convert.ToDateTime(x.ticket_date) >= sDate && Convert.ToDateTime(x.ticket_date) <= eDate).ToList();
                }
                if (lstStatus.Length > 0)
                    _transaction = _transaction.Where(x => x.status != null && lstStatus.Contains(x.status.Trim().ToLower())).ToList();
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////                             
                pModel.sSearchData = new List<HedgingTicketViewModel_SearchData>();
                foreach (var i in _transaction)
                {
                    pModel.sSearchData.Add(new HedgingTicketViewModel_SearchData
                    {
                        hTransactionIdEncrypted = i.transaction_id.Encrypt(),
                        hTransactionRequestEncrypted = i.req_transaction_id.Encrypt(),
                        hSystemEncrypted = i.system.Encrypt(),
                        hTypeEncrypted = i.type.Encrypt(), 
                        hTicketNoEncrypted = i.ticket_no.Encrypt(),

                        dStatus = i.status,
                        dDate = i.ticket_date,
                        dTicketNo = i.ticket_no,
                        dDealNo = i.deal_no,
                        dDealType = i.deal_type,
                        dTool = i.tool,
                        dUnderlying = i.underlying,
                        dTrader = i.trade_for,                        
                        detail = getDealDetail(i.ticket_no, i.deal_no, i.counter_party)
                    });
                }
                if (!String.IsNullOrEmpty(sDealNo))
                    pModel.sSearchData = pModel.sSearchData.Where(x => x.detail.Any(y => y.deal_no.Trim().ToLower().Contains(sDealNo))).ToList();
                if (!String.IsNullOrEmpty(sCounterParty))
                    pModel.sSearchData = pModel.sSearchData.Where(x => x.detail.Any(y => y.counterParty.Trim().ToLower().Equals(sCounterParty))).ToList();
                rtn.Status = true;
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }

            return rtn;
        }

        public ReturnValue Save(ref HedgingTicketViewModel model, string json_fileUpload)
        {
            ReturnValue rtn = new ReturnValue();
            RequestCPAI req = new RequestCPAI();
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService projService = new ServiceProvider.ProjService();
            string jsonData, tradingTicket, dealDetail;

            try
            {
                tradingTicket = new JavaScriptSerializer().Serialize(model.trading_ticket);
                dealDetail = new JavaScriptSerializer().Serialize(model.deal_detail);

                tradingTicket = "\"trading_ticket\": " + tradingTicket;
                dealDetail = "\"deal_detail\": " + dealDetail;
                jsonData = "{" + tradingTicket + ", " + dealDetail + "}";
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                req.Function_id = ConstantPrm.FUNCTION.F10000038;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_TCKT });
                req.Req_parameters.P.Add(new P { K = "type", V = model.systemType });
                req.Req_parameters.P.Add(new P { K = "note", V = model.trading_ticket.reason });
                req.Req_parameters.P.Add(new P { K = "current_action", V = model.action });
                req.Req_parameters.P.Add(new P { K = "next_status", V = model.next_status });
                req.Req_parameters.P.Add(new P { K = "data_detail_input", V = jsonData });
                req.Req_parameters.P.Add(new P { K = "attach_items", V = json_fileUpload });
                req.Extra_xml = "";
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = projService.CallService(reqData);
                if (resData.result_code.Trim().Equals("1") || resData.result_code.Trim().Equals("2"))
                {
                    model.reqID = resData.req_transaction_id;
                    model.tranID = resData.transaction_id;
                    rtn.Status = true;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }

            return rtn;
        }

        public ReturnValue GetDetail(ref HedgingTicketViewModel model, bool chkAddDeal)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                string date_From = String.IsNullOrEmpty(model.search_deal.sDate_start) ? string.Empty : model.search_deal.sDate_start;
                string date_To = String.IsNullOrEmpty(model.search_deal.sDate_to) ? string.Empty : model.search_deal.sDate_to;
                string deal_no = String.IsNullOrEmpty(model.search_deal.deal_no) ? string.Empty : model.search_deal.deal_no;
                string deal_Type = String.IsNullOrEmpty(model.search_deal.sDeal_type) ? string.Empty : model.search_deal.sDeal_type;
                string tool = String.IsNullOrEmpty(model.search_deal.sTool) ? string.Empty : model.search_deal.sTool;
                string underlying = String.IsNullOrEmpty(model.search_deal.sUnderlying) ? string.Empty : model.search_deal.sUnderlying;
                string underlyingVS = String.IsNullOrEmpty(model.search_deal.sVS) ? string.Empty : model.search_deal.sVS;
                string hedge_Type = String.IsNullOrEmpty(model.search_deal.sHedgeType) ? string.Empty : model.search_deal.sHedgeType;
                string ticket_no = String.IsNullOrEmpty(model.trading_ticket.ticket_no) ? string.Empty : model.trading_ticket.ticket_no;
                string systemType = String.IsNullOrEmpty(model.systemType) ? string.Empty : model.systemType;
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_DEAL_DATA
                                 join a in context.HEDG_FW_ANNUAL on v.HDDA_FK_FW_ANNUAL equals a.HFT_ROW_ID into viewD
                                 from a in viewD.DefaultIfEmpty()
                                 join ad in context.HEDG_FW_ANNUAL_DETAIL on a.HFT_ROW_ID equals ad.HFTD_FK_FW_ANNUAL into viewE
                                 from ad in viewE.DefaultIfEmpty()
                                 join c in context.MT_VENDOR on v.HDDA_FK_COUNTER_PARTIES equals c.VND_ACC_NUM_VENDOR into viewC
                                 from c in viewC.DefaultIfEmpty()                                 
                                 orderby new { v.HDDA_TICKET_NO, v.HDDA_STATUS }
                                 select new
                                 {
                                     dSystemType = v.HDDA_ENG_TYPE,
                                     dDealID = v.HDDA_ROW_ID,
                                     dDealNo = v.HDDA_DEAL_NO,
                                     dDealDate = v.HDDA_DEAL_DATE,
                                     dDealType = v.HDDA_DEAL_TYPE,
                                     dTool = v.HDDA_FK_TOOL,
                                     dUnderlying = v.HDDA_FK_UNDERLY,
                                     dUnderlyingVS = v.HDDA_FK_UNDERLY_VS,
                                     dTicketNo = v.HDDA_TICKET_NO,
                                     dStatus = v.HDDA_STATUS,
                                     dDateStart = (DateTime?)v.HDDA_TENOR_DATE_START,
                                     dDateEnd = (DateTime?)v.HDDA_TENOR_DATE_END,
                                     dPrice = v.HDDA_PRICE,
                                     dPriceUnit = v.HDDA_PRICE_UNIT,
                                     dVolume = v.HDDA_VOL_MNT,
                                     dVolumeUnit = v.HDDA_VOL_MNT_UNIT,
                                     dCounter_parties_id = v.HDDA_FK_COUNTER_PARTIES,
                                     dCounter_parties_name = c.VND_NAME1,
                                     dAnnual_id = a.HFT_ROW_ID,
                                     dActiveDateStart = a.HFT_ACTIVEDATE_START,
                                     dActiveDateEnd = a.HFT_ACTIVEDATE_END,
                                     dHedge_Type = ad.HFTD_HEDGE_TYPE
                                 }).ToList().Distinct();
                    query = query.Where(p => p.dTicketNo == null || p.dTicketNo == string.Empty).ToList();
                    query = query.Where(p => p.dStatus.ToUpper().Contains("SUBMIT")).ToList();
                    if (!string.IsNullOrEmpty(deal_no))
                        query = query.Where(p => p.dDealNo != null && p.dDealNo.ToUpper().Contains(deal_no)).ToList();
                    if (!string.IsNullOrEmpty(deal_Type))
                        query = query.Where(p => p.dDealType != null && p.dDealType.ToUpper().Contains(deal_Type)).ToList();
                    if (!string.IsNullOrEmpty(tool))
                        query = query.Where(p => p.dTool != null && p.dTool.ToUpper().Contains(tool)).ToList();
                    if (!string.IsNullOrEmpty(underlying))
                        query = query.Where(p => p.dUnderlying != null && p.dUnderlying.ToUpper().Contains(underlying)).ToList();
                    if (!string.IsNullOrEmpty(underlyingVS))
                        query = query.Where(p => p.dUnderlyingVS != null && p.dUnderlyingVS.ToString().Contains(underlyingVS)).ToList();
                    if (!string.IsNullOrEmpty(hedge_Type))
                        query = query.Where(p => p.dHedge_Type != null && p.dHedge_Type.ToString().Contains(hedge_Type)).ToList();
                    if (!string.IsNullOrEmpty(systemType))
                        query = query.Where(p => p.dSystemType != null && p.dSystemType.ToString().Contains(systemType)).ToList();
                    if (!string.IsNullOrEmpty(date_From) && !string.IsNullOrEmpty(date_To))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(date_From);
                        eDate = ShareFn.ConvertStrDateToDate(date_To).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dDealDate != null && (p.dDealDate >= sDate && p.dDealDate <= eDate)).ToList();
                    }
                    model.oldDeal_detail = new HedgingTicketViewModel.DealDetail();
                    model.oldDeal_detail.hedging_execution = new HedgingTicketViewModel.HedgingExecution();
                    model.oldDeal_detail.hedging_execution.hedging_execution_data = new List<HedgingTicketViewModel.HedgingExecutionData>();
                    if (model.trading_ticket.status != "New" && chkAddDeal)
                    {                        
                        var queryOldDeal = (from v in context.HEDG_DEAL_DATA
                                            join a in context.HEDG_FW_ANNUAL on v.HDDA_FK_FW_ANNUAL equals a.HFT_ROW_ID into viewD
                                            from a in viewD.DefaultIfEmpty()
                                            join ad in context.HEDG_FW_ANNUAL_DETAIL on a.HFT_ROW_ID equals ad.HFTD_FK_FW_ANNUAL into viewE
                                            from ad in viewE.DefaultIfEmpty()
                                            join c in context.MT_VENDOR on v.HDDA_FK_COUNTER_PARTIES equals c.VND_ACC_NUM_VENDOR into viewC
                                            from c in viewC.DefaultIfEmpty()                                            
                                            orderby new { v.HDDA_TICKET_NO, v.HDDA_STATUS }
                                            select new
                                            {
                                                dSystemType = v.HDDA_ENG_TYPE,
                                                dDealID = v.HDDA_ROW_ID,
                                                dDealNo = v.HDDA_DEAL_NO,
                                                dDealDate = v.HDDA_DEAL_DATE,
                                                dDealType = v.HDDA_DEAL_TYPE,
                                                dTool = v.HDDA_FK_TOOL,
                                                dUnderlying = v.HDDA_FK_UNDERLY,
                                                dUnderlyingVS = v.HDDA_FK_UNDERLY_VS,
                                                dTicketNo = v.HDDA_TICKET_NO,
                                                dStatus = v.HDDA_STATUS,
                                                dDateStart = (DateTime?)v.HDDA_TENOR_DATE_START,
                                                dDateEnd = (DateTime?)v.HDDA_TENOR_DATE_END, 
                                                dPrice = v.HDDA_PRICE,
                                                dPriceUnit = v.HDDA_PRICE_UNIT,
                                                dVolume = v.HDDA_VOL_MNT,
                                                dVolumeUnit = v.HDDA_VOL_MNT_UNIT,
                                                dCounter_parties_id = v.HDDA_FK_COUNTER_PARTIES,
                                                dCounter_parties_name = c.VND_NAME1,
                                                dAnnual_id = a.HFT_ROW_ID,
                                                dActiveDateStart = a.HFT_ACTIVEDATE_START,
                                                dActiveDateEnd = a.HFT_ACTIVEDATE_END,
                                                dHedge_Type = ad.HFTD_HEDGE_TYPE
                                            }).ToList().Distinct();
                        queryOldDeal = queryOldDeal.Where(p => p.dTicketNo == ticket_no).ToList();
                        if (queryOldDeal != null)
                        {
                            foreach (var g in queryOldDeal)
                            {
                                model.oldDeal_detail.hedging_execution.hedging_execution_data.Add(new HedgingTicketViewModel.HedgingExecutionData
                                {
                                    deal_id = g.dDealID,
                                    deal_no = g.dDealNo,
                                    tenor = (g.dDateStart == null ? "" : Convert.ToDateTime(g.dDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                + " To " + (g.dDateEnd == null ? "" : Convert.ToDateTime(g.dDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                    price = g.dPrice.ToString(),
                                    volume = g.dVolume.ToString(),
                                    counter_parties_id = g.dCounter_parties_id,
                                    counter_parties_name = g.dCounter_parties_name,
                                    record_status = "N",
                                    deal_status = g.dStatus,
                                    bid_price = getBridPrice(context, g.dDealID)
                                });
                                model.oldDeal_detail.annual_detail_id = g.dAnnual_id;
                                model.oldDeal_detail.hedging_execution.price_unit = g.dPriceUnit;
                                model.oldDeal_detail.hedging_execution.volume_mnt_unit = g.dVolumeUnit;
                                model.oldDeal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
                                model.oldDeal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();
                                model.oldDeal_detail.hedging_execution.hedging_position.hedging_position_data.Add(new HedgingTicketViewModel.HedgingPositionData
                                {
                                    tenor = (g.dDateStart == null ? "" : Convert.ToDateTime(g.dDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                + " To " + (g.dDateEnd == null ? "" : Convert.ToDateTime(g.dDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                    price = g.dPrice.ToString(),
                                    volume = g.dVolume.ToString(),
                                });
                                model.oldDeal_detail.hedging_execution.hedging_position.price_unit = g.dPriceUnit;
                                model.oldDeal_detail.hedging_execution.hedging_position.volume_unit = g.dVolumeUnit;
                            }
                        }
                    }                    

                    if (query != null)
                    {
                        model.search_deal.search_deal_list = new List<HedgingTicketViewModel.SearchDealList>();
                        model.deal_detail.hedging_execution = new HedgingTicketViewModel.HedgingExecution();
                        model.deal_detail.hedging_execution.hedging_execution_data = new List<HedgingTicketViewModel.HedgingExecutionData>();
                        model.deal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
                        model.deal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();
                        model.deal_detail.hedging_chart = new HedgingTicketViewModel.HedgingChart();
                        model.deal_detail.hedging_target = new List<HedgingTicketViewModel.HedgingTarget>();
                        List<SelectListItem> underlyingList = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
                        foreach (var g in query)
                        {
                            if (chkAddDeal)
                            {
                                model.deal_detail.hedging_execution.hedging_execution_data.Add(new HedgingTicketViewModel.HedgingExecutionData
                                {
                                    deal_id = g.dDealID,
                                    deal_no = g.dDealNo,
                                    tenor = (g.dDateStart == null ? "" : Convert.ToDateTime(g.dDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                + " To " + (g.dDateEnd == null ? "" : Convert.ToDateTime(g.dDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                    price = g.dPrice.ToString(),
                                    volume = g.dVolume.ToString(),
                                    counter_parties_id = g.dCounter_parties_id,
                                    counter_parties_name = g.dCounter_parties_name,
                                    record_status = "N",
                                    deal_status = g.dStatus,
                                    bid_price = getBridPrice(context, g.dDealID)
                                });
                                model.deal_detail.hedging_execution.price_unit = g.dPriceUnit;
                                model.deal_detail.hedging_execution.volume_mnt_unit = g.dVolumeUnit;
                                model.deal_detail.hedging_target.Add(new HedgingTicketViewModel.HedgingTarget
                                {
                                    order = g.dDealID,
                                    tenor = (g.dDateStart == null ? "" : Convert.ToDateTime(g.dDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                + " To " + (g.dDateEnd == null ? "" : Convert.ToDateTime(g.dDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                    price = g.dPrice.ToString(),
                                    volume = g.dVolume.ToString(),
                                });
                                model.deal_detail.deal_type = model.search_deal.sDeal_type;
                                model.deal_detail.underlying_id = model.search_deal.sUnderlying;
                                model.deal_detail.underlying_name = model.search_deal.sUnderlying;
                                model.deal_detail.tool_id = model.search_deal.sTool;
                                model.deal_detail.tool_name = model.search_deal.sTool;
                                model.deal_detail.annual_detail_id = g.dAnnual_id;
                                model.deal_detail.hedging_execution.hedging_position.hedging_position_data.Add(new HedgingTicketViewModel.HedgingPositionData
                                {
                                    tenor = (g.dDateStart == null ? "" : Convert.ToDateTime(g.dDateStart).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture))
                                                + " To " + (g.dDateEnd == null ? "" : Convert.ToDateTime(g.dDateEnd).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)),
                                    price = g.dPrice.ToString(),
                                    volume = g.dVolume.ToString(),
                                });
                                model.deal_detail.hedging_execution.hedging_position.price_unit = g.dPriceUnit;
                                model.deal_detail.hedging_execution.hedging_position.volume_unit = g.dVolumeUnit;
                            }
                            else
                            {
                                model.search_deal.search_deal_list.Add(new HedgingTicketViewModel.SearchDealList
                                {
                                    deal_id = g.dDealID,
                                    deal_no = g.dDealNo,
                                    deal_date = g.dDealDate == null ? "" : Convert.ToDateTime(g.dDealDate).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                    deal_type = g.dDealType,
                                    tool = g.dTool,
                                    underlying = getUnderlyingName(underlyingList, g.dUnderlying),
                                    hedging_type = getNameFromList(model.search_deal.hegingType, g.dHedge_Type),
                                    annual_id = g.dAnnual_id,
                                    date_active = (g.dActiveDateStart == null ? "" : Convert.ToDateTime(g.dActiveDateStart).ToString("MMM yyyy", CultureInfo.InvariantCulture))
                                                                        + " - " + (g.dActiveDateEnd == null ? "" : Convert.ToDateTime(g.dActiveDateEnd).ToString("MMM yyyy", CultureInfo.InvariantCulture)),
                                });
                            }
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue GetDetailByTranId(ref HedgingTicketViewModel model, string tranId)
        {
            ReturnValue rtn = new ReturnValue();
            FunctionTransactionDAL dalTran = new FunctionTransactionDAL();
            FUNCTION_TRANSACTION dataTran, dataDeal;
            HEDG_TICKET_DAL dalTicket = new HEDG_TICKET_DAL();
            HEDG_TICKET dataTicket;

            try
            {
                dataTran = dalTran.GetFnTranDetail(tranId.Trim());
                dataDeal = dalTran.GetFnTranDetail(tranId.Trim());
                dataTicket = dalTicket.GetTicketByTicketId(tranId.Trim());
                if (dataTran != null)
                {
                    model.trading_ticket = new HedgingTicketViewModel.TradingTicket();
                    model.deal_detail = new HedgingTicketViewModel.DealDetail();
                    model.deal_detail.hedging_chart = new HedgingTicketViewModel.HedgingChart();
                    model.deal_detail.hedging_target = new List<HedgingTicketViewModel.HedgingTarget>();
                    model.deal_detail.hedging_execution = new HedgingTicketViewModel.HedgingExecution();
                    model.deal_detail.hedging_execution.hedging_execution_data = new List<HedgingTicketViewModel.HedgingExecutionData>();
                    model.deal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
                    model.deal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    model.trading_ticket.ticket_no = dataTran.FTX_INDEX7;
                    model.trading_ticket.trader = dataTran.FTX_INDEX13;
                    model.trading_ticket.ticket_date = dataTran.FTX_INDEX12;
                    model.trading_ticket.status = dataTran.FTX_INDEX4;
                    model.trading_ticket.revision = dataTicket.HTDA_REVISION;
                    model.trading_ticket.reason = dataTicket.HTDA_REASON;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }
    }
}
