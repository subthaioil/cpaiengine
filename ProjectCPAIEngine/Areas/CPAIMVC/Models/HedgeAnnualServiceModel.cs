﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALHedg;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeAnnualServiceModel
    {
        public static string getTransactionByID(string id)
        {
            HedgingRootObject rootObj = new HedgingRootObject();
            rootObj.hedg_annual_data = new HedgAnnualData();
            rootObj.hedg_annual_submit_note = new HedgAnnualSubmitNote();
            rootObj.hedg_annual_type = new HedgAnnualType();
            rootObj.hedg_annual_type.hedg_type_data = new List<HedgTypeData>();

            HEDG_ANNUAL_FW_DAL fwDal = new HEDG_ANNUAL_FW_DAL();
            HEDG_ANNUAL_FW fw = fwDal.GetByID(id);

            if (fw != null)
            {
                rootObj.hedg_annual_data.approved_date = (fw.HAF_APPROVED_DATE == DateTime.MinValue || fw.HAF_APPROVED_DATE == null) ? "" : Convert.ToDateTime(fw.HAF_APPROVED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.hedg_annual_data.trading_book = fw.HAF_FK_MT_COMPANY;
                rootObj.hedg_annual_data.version = fw.HAF_VERSION;
                rootObj.hedg_annual_data.status = fw.HAF_STATUS;
                rootObj.hedg_annual_note = fw.HAF_NOTE;
                
                HEDG_ANNUAL_FW_HEDGE_TYPE_DAL htDal = new HEDG_ANNUAL_FW_HEDGE_TYPE_DAL();
                List<HEDG_ANNUAL_FW_HEDGE_TYPE> ht = htDal.GetByAnnualID(fw.HAF_ROW_ID);


                for (int i = 0; i < ht.Count; i++)
                {
                    HedgTypeData htdata = new HedgTypeData();
                    htdata.hedg_type_key = ht[i].HAFH_FK_HEDG_MT_HEDGE_TYPE;
                    htdata.hedg_type_detail = new List<HedgTypeDetail>();

                    HEDG_ANNUAL_FW_DETAIL_DAL fdDal = new HEDG_ANNUAL_FW_DETAIL_DAL();
                    List<HEDG_ANNUAL_FW_DETAIL> fd = fdDal.GetByHedgeTypeID(ht[i].HAFH_ROW_ID);

                    for (int j = 0; j < fd.Count; j++)
                    {
                        HedgTypeDetail htdetail = new HedgTypeDetail();
                        htdetail.key = fd[j].HAFD_ROW_ID;
                        htdetail.order = fd[j].HAFD_ORDER;
                        htdetail.product_back = fd[j].HAFD_FK_HEDG_PRODUCT_BACK;
                        htdetail.product_front = fd[j].HAFD_FK_HEDG_PRODUCT_FRONT;
                        htdetail.title = fd[j].HAFD_TITLE;
                        htdetail.unit_price = fd[j].HAFD_UNIT_PRICE;
                        htdetail.unit_volume = fd[j].HAFD_UNIT_VOLUME;
                        htdetail.year = fd[j].HAFD_YEAR;
                        htdetail.price_year = fd[j].HAFD_PRICE_YEAR;
                        htdetail.price_Q1 = fd[j].HAFD_PRICE_Q1;
                        htdetail.price_Q2 = fd[j].HAFD_PRICE_Q2;
                        htdetail.price_Q3 = fd[j].HAFD_PRICE_Q3;
                        htdetail.price_Q4 = fd[j].HAFD_PRICE_Q4;
                        htdetail.prod_volume = fd[j].HAFD_PRODUCTION_VOLUME;
                        htdetail.approved_volume = fd[j].HAFD_APPROVED_VOLUME;
                        htdetail.limit_volume = fd[j].HAFD_LIMIT_VOLUME;
                        htdetail.min_max = fd[j].HAFD_MIN_MAX;
                        htdetail.note = fd[j].HAFD_NOTE;
                        htdetail.approved_date = (fd[j].HAFD_APPROVED_DATE == DateTime.MinValue || fd[j].HAFD_APPROVED_DATE == null) ? "" : Convert.ToDateTime(fd[j].HAFD_APPROVED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        htdata.hedg_type_detail.Add(htdetail);
                    }

                    rootObj.hedg_annual_type.hedg_type_data.Add(htdata);
                }

                string strExplanationAttach = "";
                HEDG_ANNUAL_ATTACH_FILE_DAL fileDal = new HEDG_ANNUAL_ATTACH_FILE_DAL();
                List<HEDG_ANNUAL_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id);
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.HAAF_PATH + "|";
                }
                rootObj.attach_file = strExplanationAttach.Replace("|","");

                HEDG_ANNUAL_SUBMIT_NOTE_DAL snDal = new HEDG_ANNUAL_SUBMIT_NOTE_DAL();
                HEDG_ANNUAL_SUBMIT_NOTE sn = snDal.GetByAnnualID(id);

                if (sn != null)
                {
                    rootObj.hedg_annual_submit_note.subject = sn.HASN_SUBJECT;
                    rootObj.hedg_annual_submit_note.ref_no = sn.HASN_REF_NO;
                    rootObj.hedg_annual_submit_note.note = sn.HASN_NOTE;
                    rootObj.hedg_annual_submit_note.approved_date = (sn.HASN_APPROVED_DATE == DateTime.MinValue || sn.HASN_APPROVED_DATE == null) ? "" : Convert.ToDateTime(sn.HASN_APPROVED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    strExplanationAttach = "";
                    HEDG_ANNUAL_SN_ATTACH_FILE_DAL filesnDal = new HEDG_ANNUAL_SN_ATTACH_FILE_DAL();
                    List<HEDG_ANNUAL_SN_ATTACH_FILE> extsnFile = filesnDal.GetFileByDataID(sn.HASN_ROW_ID);
                    foreach (var item in extsnFile)
                    {
                        strExplanationAttach += item.HASF_PATH + "|";
                    }
                    rootObj.hedg_annual_submit_note.attach_file = strExplanationAttach.Replace("|","");
                }

            }
            return new JavaScriptSerializer().Serialize(rootObj);
        }
        
        public static string GetHedgeAnnualLatestVersion(string id)
        {
            var latest_version = "";
            HEDG_ANNUAL_FW_DAL dataDal = new HEDG_ANNUAL_FW_DAL();
            if (dataDal.IsLatestVersionByID(id))
            {
                latest_version = "Y";
            }
            else
            {
                latest_version = "N";
            }
            return latest_version;
        }

        public List<HedgeAnnualHistoryViewModel> getHedgeAnnualHistory(string id)
        {
            List<HedgeAnnualHistoryViewModel> history = new List<HedgeAnnualHistoryViewModel>();

            HEDG_ANNUAL_FW_DAL dataDal = new HEDG_ANNUAL_FW_DAL();
            HEDG_ANNUAL_SUBMIT_NOTE_DAL noteDal = new HEDG_ANNUAL_SUBMIT_NOTE_DAL();

            List<SelectListItem> companyLst = GetHedgeCompany();

            HEDG_ANNUAL_FW data = dataDal.GetByID(id);

            while (data != null)
            {
                HEDG_ANNUAL_SUBMIT_NOTE note = noteDal.GetByAnnualID(data.HAF_ROW_ID);

                HedgeAnnualHistoryViewModel new_history = new HedgeAnnualHistoryViewModel();

                new_history.tran_id = data.HAF_ROW_ID;
                new_history.trading_book = companyLst.Where(x => x.Value == data.HAF_FK_MT_COMPANY).FirstOrDefault() != null ? companyLst.Where(x => x.Value == data.HAF_FK_MT_COMPANY).FirstOrDefault().Text : "";
                new_history.version = data.HAF_VERSION;
                new_history.approved_date = data.HAF_APPROVED_DATE.HasValue ? data.HAF_APPROVED_DATE.Value.ToString("dd/MM/yyyy") : "";
                new_history.update_by = data.HAF_UPDATED_BY;
                new_history.update_date = data.HAF_UPDATED_DATE.ToString("dd/MM/yyyy");
                if (note != null)
                {
                    new_history.ref_no = note.HASN_REF_NO;
                    new_history.subject = note.HASN_SUBJECT;
                    new_history.note = note.HASN_NOTE;
                }

                history.Add(new_history);

                if (!string.IsNullOrEmpty(data.HAF_HISTORY_REF))
                {
                    data = dataDal.GetByID(data.HAF_HISTORY_REF);
                }
                else
                {
                    data = null;
                }
            }

            return history;
        }

        public List<HedgeAnnualProductHistoryViewModel> getHedgeAnnualProductHistory(string key)
        {
            List<HedgeAnnualProductHistoryViewModel> history = new List<HedgeAnnualProductHistoryViewModel>();

            HEDG_ANNUAL_FW_DETAIL_DAL dataDal = new HEDG_ANNUAL_FW_DETAIL_DAL();

            List<SelectListItem> productLst = GetHedgeProduct();

            HEDG_ANNUAL_FW_DETAIL data = dataDal.GetByID(key);
            HEDG_ANNUAL_FW fw = dataDal.GetAnnualParentByID(key);

            while (data != null)
            {
                HedgeAnnualProductHistoryViewModel new_history = new HedgeAnnualProductHistoryViewModel();
                new_history.product_back = productLst.Where(x => x.Value == data.HAFD_FK_HEDG_PRODUCT_BACK).FirstOrDefault() != null ? productLst.Where(x => x.Value == data.HAFD_FK_HEDG_PRODUCT_BACK).FirstOrDefault().Text : "";
                new_history.product_front = productLst.Where(x => x.Value == data.HAFD_FK_HEDG_PRODUCT_FRONT).FirstOrDefault() != null ? productLst.Where(x => x.Value == data.HAFD_FK_HEDG_PRODUCT_FRONT).FirstOrDefault().Text : "";
                new_history.title = data.HAFD_TITLE;
                new_history.version = data.HAFD_VERSION;
                new_history.approved_date = data.HAFD_APPROVED_DATE.HasValue ? data.HAFD_APPROVED_DATE.Value.ToString("dd/MM/yyyy") : "";
                new_history.unit_price = data.HAFD_UNIT_PRICE;
                new_history.unit_vol = data.HAFD_UNIT_VOLUME;
                new_history.year = data.HAFD_YEAR;
                new_history.min_max = data.HAFD_MIN_MAX;
                new_history.price_year = data.HAFD_PRICE_YEAR;
                new_history.price_q1 = data.HAFD_PRICE_Q1;
                new_history.price_q2 = data.HAFD_PRICE_Q2;
                new_history.price_q3 = data.HAFD_PRICE_Q3;
                new_history.price_q4 = data.HAFD_PRICE_Q4;
                new_history.prod_vol = data.HAFD_PRODUCTION_VOLUME;
                new_history.limit_vol = data.HAFD_LIMIT_VOLUME;
                new_history.approved_vol = data.HAFD_APPROVED_VOLUME;
                new_history.note = data.HAFD_NOTE;

                history.Add(new_history);

                if (!string.IsNullOrEmpty(data.HAFD_HISTORY_REF))
                {
                    HEDG_ANNUAL_FW_DETAIL old_data = data;
                    data = dataDal.GetByID(data.HAFD_HISTORY_REF);
                    if ((data != null && old_data.HAFD_VERSION == data.HAFD_VERSION) && !(fw.HAF_STATUS == "ACTIVE" && fw.HAF_VERSION == "1"))
                    {
                        history.Remove(new_history);
                    }
                }
                else if (fw.HAF_STATUS == "DRAFT" && fw.HAF_VERSION == "1")
                {
                    history.Remove(new_history);
                }
                else
                {
                    data = null;
                }
            }

            return history;
        }
        
        public List<SelectListItem> GetHedgeCompany()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {                    
                    var query = (from v in context.MT_COMPANY
                                 where v.MCO_CREATE_TYPE != "CPAI"
                                 orderby v.MCO_COMPANY_CODE
                                 select new
                                 {
                                     dCompanyCode = v.MCO_COMPANY_CODE,
                                     dFullName = v.MCO_SHORT_NAME
                                 });
                    
                    if (query != null)
                    {
                        foreach (var g in query)
                        {                            
                            list.Add(new SelectListItem { Text = g.dFullName, Value = g.dCompanyCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public List<SelectListItem> GetHedgeCompany(string trading_book = "")
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    trading_book = string.IsNullOrEmpty(trading_book) ? "" : trading_book;
                    var query = (from v in context.MT_COMPANY
                                 join h in context.HEDG_ANNUAL_FW on v.MCO_COMPANY_CODE equals h.HAF_FK_MT_COMPANY into h_join
                                 from h in h_join.DefaultIfEmpty()
                                 where v.MCO_CREATE_TYPE != "CPAI" && (h == null || (trading_book == h.HAF_FK_MT_COMPANY))
                                 orderby v.MCO_COMPANY_CODE
                                 select new
                                 {
                                     dCompanyCode = v.MCO_COMPANY_CODE,
                                     dFullName = v.MCO_SHORT_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.dFullName, Value = g.dCompanyCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }
        public string GetHedgeCompanyName(string trading_book )
        {
            string CompanyName = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    trading_book = string.IsNullOrEmpty(trading_book) ? "" : trading_book;
                    var query = (from v in context.MT_COMPANY
                                 join h in context.HEDG_ANNUAL_FW on v.MCO_COMPANY_CODE equals h.HAF_FK_MT_COMPANY into h_join
                                 from h in h_join.DefaultIfEmpty()
                                 where v.MCO_CREATE_TYPE != "CPAI" && (h == null || (trading_book == h.HAF_FK_MT_COMPANY))
                                 orderby v.MCO_COMPANY_CODE
                                 select new
                                 {
                                     dCompanyCode = v.MCO_COMPANY_CODE,
                                     dFullName = v.MCO_SHORT_NAME
                                 });

                    if (query != null)
                    {
                        query.Distinct().ToList();
                        foreach (var g in query)
                        {
                            CompanyName = g.dFullName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return CompanyName;
        }
        public List<SelectListItem> GetHedgeType()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_HEDGE_TYPE
                                 where v.HMHT_STATUS == "ACTIVE"
                                 orderby v.HMHT_ROW_ID
                                 select new
                                 {
                                     HedgeTypeCode = v.HMHT_ROW_ID,
                                     HedgeTypeName = v.HMHT_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.HedgeTypeName, Value = g.HedgeTypeCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public List<SelectListItem> GetHedgeProduct() {

            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_PROD
                                 where v.HMP_STATUS == "ACTIVE"
                                 orderby v.HMP_ROW_ID
                                 select new
                                 {
                                     HedgeProductID = v.HMP_ROW_ID,
                                     HedgeProductName = v.HMP_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.HedgeProductName, Value = g.HedgeProductID });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }
        public string GetHedgeProductname(string HedgeProductID )
        {

            string HedgeProductName = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_PROD
                                 where( v.HMP_STATUS == "ACTIVE" && v.HMP_ROW_ID== HedgeProductID)
                                 orderby v.HMP_ROW_ID
                                 select new
                                 {
                                     HedgeProductID = v.HMP_ROW_ID,
                                     HedgeProductName = v.HMP_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            HedgeProductName =g.HedgeProductName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return HedgeProductName;
        }

    }
}