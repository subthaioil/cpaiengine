﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeCounterpartyLimitServiceModel
    {
        public HedgeCounterpartyLimitViewModel GetData()
        {
            HedgeCounterpartyLimitViewModel model = new HedgeCounterpartyLimitViewModel();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var result = (from v in context.HEDG_CP_LIMIT
                                  select new
                                  {
                                      dID = v.HCPL_ROW_ID,
                                      //dCreditRating = v.HCPL_FK_CREDIT_RATING,
                                      //dRating = v.HCPL_CREDIT_RATING_DESC,
                                      ////dSptradePercent = v.HCPL_SPTRADE_PERCENT,
                                      ////dSptradePercentDesc = v.HCPL_SPTRADE_PERCENT_DESC,
                                      //dLimitOverPercent = v.HCPL_LIMIT_OVER_PERCENT,
                                      //dLimitOverPercentDesc = v.HCPL_LIMIT_OVER_PERCENT_DESC,
                                      //dLimitOverPercentMail = v.HCPL_LIMIT_OVER_PERCENT_EMAIL,
                                      dCreateBy = v.HCPL_CREATED_BY,
                                      dCreateDate = v.HCPL_CREATED_DATE,
                                      dUpdateBy = v.HCPL_UPDATED_BY,
                                      dUpdateDate = v.HCPL_UPDATED_DATE
                                  }
                                  ).FirstOrDefault();

                    if (result != null)
                    {
                        model.ID = result.dID;
                        //model.CreditRatingID = result.dCreditRating;
                        //model.CreditRatingDesc = result.dRating;
                        ////model.SptradePercent = result.dSptradePercent;
                        ////model.SptradePercentDesc = result.dSptradePercentDesc;
                        //model.LimitOverPercent = result.dLimitOverPercent;
                        //model.LimitOverPercentDesc = result.dLimitOverPercentDesc;
                        //model.LimitOverPercentEmail = result.dLimitOverPercentMail;
                        model.CreateBy = result.dCreateBy;
                        model.CreateDate = result.dCreateDate;
                        model.UpdateBy = result.dUpdateBy;
                        model.UpdateDate = result.dUpdateDate;

                        var resultItem = (from v in context.HEDG_CP_LIMIT_CDS
                                          where v.HCLC_FK_CP_LIMIT == result.dID
                                          select new
                                          {
                                              dID = v.HCLC_ROW_ID,
                                              dOrder = v.HCLC_ORDER,
                                              dType = v.HCLC_TYPE,
                                              dValue = v.HCLC_VALUE,
                                              dDay = v.HCLC_DAY,
                                              dAction = v.HCLC_ACTION,
                                              dDesc = v.HCLC_DESC,
                                              dEmail = v.HCLC_EMAIL,
                                              dCreateBy = v.HCLC_CREATED_BY,
                                              dCreateDate = v.HCLC_CREATED_DATE,
                                              dUpdateBy = v.HCLC_UPDATED_BY,
                                              dUpdateDate = v.HCLC_UPDATED_DATE
                                          }
                                        ).OrderBy(x => x.dOrder).ToList();

                        if (resultItem.Count() != 0)
                        {
                            model.CounterpartyCDS = new List<CounterpartyCDS>();
                            foreach (var g in resultItem)
                            {
                                CounterpartyCDS counterpartyCDS = new CounterpartyCDS();
                                counterpartyCDS.ID = g.dID;
                                counterpartyCDS.Order = g.dOrder;
                                counterpartyCDS.Type = g.dType;
                                counterpartyCDS.Value = g.dValue;
                                counterpartyCDS.Day = g.dDay;
                                counterpartyCDS.Action = g.dAction;
                                counterpartyCDS.Desc = g.dDesc;
                                counterpartyCDS.Email = g.dEmail;
                                model.CounterpartyCDS.Add(counterpartyCDS);
                            }
                        }
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue Edit(HedgeCounterpartyLimitViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                HEDG_CP_LIMIT_DAL dal = new HEDG_CP_LIMIT_DAL();
                HEDG_CP_LIMIT ent = new HEDG_CP_LIMIT();
                HEDG_CP_LIMIT_CDS_DAL dalItem = new HEDG_CP_LIMIT_CDS_DAL();
                HEDG_CP_LIMIT_CDS entItem = new HEDG_CP_LIMIT_CDS();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(pModel.ID))
                            {
                                ent.HCPL_ROW_ID = pModel.ID;
                                //ent.HCPL_FK_CREDIT_RATING = pModel.CreditRatingID;
                                //ent.HCPL_CREDIT_RATING_DESC = pModel.CreditRatingDesc;
                                ////ent.HCPL_SPTRADE_PERCENT = pModel.SptradePercent;
                                ////ent.HCPL_SPTRADE_PERCENT_DESC = pModel.SptradePercentDesc;
                                //ent.HCPL_LIMIT_OVER_PERCENT = pModel.LimitOverPercent;
                                //ent.HCPL_LIMIT_OVER_PERCENT_DESC = pModel.LimitOverPercentDesc;
                                //ent.HCPL_LIMIT_OVER_PERCENT_EMAIL = pModel.LimitOverPercentEmail;
                                ent.HCPL_UPDATED_BY = pUser;
                                ent.HCPL_UPDATED_DATE = now;
                                dal.Update(ent, context);
                            }

                            dalItem.Delete(pModel.ID, context);
                            if (pModel.CounterpartyCDS != null)
                            {
                                foreach (var item in pModel.CounterpartyCDS)
                                {
                                    entItem = new HEDG_CP_LIMIT_CDS();
                                    entItem.HCLC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entItem.HCLC_FK_CP_LIMIT = pModel.ID;
                                    entItem.HCLC_ORDER = item.Order;
                                    entItem.HCLC_TYPE = item.Type;
                                    entItem.HCLC_VALUE = item.Value;
                                    entItem.HCLC_DAY = item.Day;
                                    entItem.HCLC_ACTION = item.Action;
                                    entItem.HCLC_DESC = item.Desc;
                                    entItem.HCLC_EMAIL = item.Email;
                                    entItem.HCLC_CREATED_BY = pUser;
                                    entItem.HCLC_CREATED_DATE = now;
                                    entItem.HCLC_UPDATED_BY = pUser;
                                    entItem.HCLC_UPDATED_DATE = now;
                                    dalItem.Save(entItem, context);
                                }
                            }

                            //Update CDS

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }


        public ReturnValue SearchEmail(ref HedgeCounterpartyLimitViewModel model, string userGroup, string userSystem, string fullNameEN, string fullNameTH, string[] oldProduct)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var data = (from a in context.CPAI_USER_GROUP
                                join b in context.USERS on a.USG_FK_USERS equals b.USR_ROW_ID
                                select new
                                {
                                    UserGorup = a.USG_USER_GROUP,
                                    UserSystem = a.USG_USER_SYSTEM,
                                    FullNameEN = b.USR_FIRST_NAME_EN,
                                    LastNameEN = b.USR_LAST_NAME_EN,
                                    FullNameTH = b.USR_FIRST_NAME_COUNTRY,
                                    LastNameTH = b.USR_LAST_NAME_COUNTRY,
                                    UserName = b.USR_LOGIN,
                                    Email = b.USR_EMAIL,
                                });


                    if (data != null)
                    {
                        var alldata = data.ToList();

                        if (!String.IsNullOrEmpty(userGroup))
                        {
                            alldata = alldata.Where(c => c.UserGorup == userGroup).ToList();
                        }

                        if (!String.IsNullOrEmpty(userSystem))
                        {
                            alldata = alldata.Where(c => c.UserSystem == userSystem).ToList();
                        }

                        if (!String.IsNullOrEmpty(fullNameEN))
                        {
                            alldata = alldata.Where(c => c.FullNameEN == fullNameEN).ToList();
                            alldata = alldata.Where(c => c.LastNameEN == fullNameEN).ToList();
                        }

                        if (!String.IsNullOrEmpty(fullNameTH))
                        {
                            alldata = alldata.Where(c => c.FullNameTH == fullNameTH).ToList();
                            alldata = alldata.Where(c => c.LastNameTH == fullNameTH).ToList();
                        }




                        if (alldata != null)
                        {
                            model.EmailList = new List<SearchEmailData>();
                            foreach (var item in alldata.ToList())
                            {
                                model.EmailList.Add(new SearchEmailData
                                {
                                    UserName = item.UserName,
                                    UserGroup = item.UserGorup,
                                    UserSystem = item.UserSystem,
                                    UserFullNameEN = item.FullNameEN,
                                    UserFullNameTH = item.FullNameTH,
                                    UserEmail = item.Email
                                });
                            }
                            if (oldProduct != null)
                            {
                                foreach (var z in oldProduct)
                                {
                                    model.oldSelectList.Add(z);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }
    }
}