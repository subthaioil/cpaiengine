﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeFormularServiceModel
    {

        public ReturnValue Save(HedgeFormularViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            if (pModel.Formular_Detail!=null && pModel.Formular_Detail.Any())
            {
                foreach (var item in pModel.Formular_Detail)
                {
                    if (item.Row_Id == "0")
                    {
                        rtn = Add(item,pUser);
                    }
                    else
                    {
                        rtn = Edit(item, pUser);
                    }
                }
            }
            return rtn;
        }



        public ReturnValue Add(Formular pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_MT_FORMULA formular = new HEDG_MT_FORMULA();
            HEDG_MT_FORMULA_EQUATION formular_equat = new HEDG_MT_FORMULA_EQUATION();
            HEDG_MT_FORMULAR_DAL formular_dal = new HEDG_MT_FORMULAR_DAL();
            HEDG_MT_FORMULAR_EQUATION_DAL formular_equat_dal = new HEDG_MT_FORMULAR_EQUATION_DAL();
            EntityCPAIEngine context = new EntityCPAIEngine();
            try
            {
                #region Save Formular
               
                    var id = ConstantPrm.GetDynamicCtrlID();
                    formular.HMFM_ORDER = pModel.OrderFormular;
                    formular.HMFM_ROW_ID = id;
                    formular.HMFM_NAME = pModel.Name;
                    formular.HMFM_DESC = pModel.Description;
                    formular.HMFM_TEXT = pModel.Expression;
                    formular.HMFM_STATUS = pModel.Status;
                    formular.HMFM_CREATED_DATE = DateTime.Now;
                    formular.HMFM_CREATED_BY = pUser;
                    formular.HMFM_UPDATED_DATE = DateTime.Now;
                    formular.HMFM_UPDATED_BY = pUser;
                    formular_dal.Save(formular);


                    var c = 0;
                    foreach (var i in pModel.Formular_Exp)
                    {
                        var eid = ConstantPrm.GetDynamicCtrlID();
                        formular_equat.HMFE_ROW_ID = eid;
                        formular_equat.HMFE_FK_MT_FORMULA = id;
                        formular_equat.HMFM_ORDER = c.ToString();                       
                        formular_equat.HMFM_VALUE = i.ValueEx;
                        formular_equat.HMFM_TYPE = i.TypeEX;
                        formular_equat.HMFM_CREATED_BY = pUser;
                        formular_equat.HMFM_CREATED_DATE = DateTime.Now;
                        formular_equat.HMFM_UPDATED_BY = pUser;
                        formular_equat.HMFM_UPDATED_DATE = DateTime.Now;
                        formular_equat_dal.Save(formular_equat);
                        c++;
                    }
                
                //foreach (var item in pModel.Formular_Detail)
                //{
                //    var id = ConstantPrm.GetDynamicCtrlID();
                //    formular.HMFM_ORDER = item.OrderFormular;
                //    formular.HMFM_ROW_ID = id;
                //    formular.HMFM_NAME = item.Name;
                //    formular.HMFM_DESC = item.Description;
                //    formular.HMFM_TEXT = item.Expression;
                //    formular.HMFM_STATUS = item.Status;
                //    formular.HMFM_CREATED_DATE = DateTime.Now;
                //    formular.HMFM_CREATED_BY = pUser;
                //    formular.HMFM_UPDATED_DATE = DateTime.Now;
                //    formular.HMFM_UPDATED_BY = pUser;
                //    formular_dal.Save(formular);


                //    var c = 0;
                //    foreach (var i in item.Formular_Exp)
                //    {
                //        var eid = ConstantPrm.GetDynamicCtrlID();
                //        formular_equat.HMFE_ROW_ID = eid;
                //        formular_equat.HMFE_FK_MT_FORMULA = id;
                //        formular_equat.HMFM_ORDER = c.ToString();
                //        //formular_equat.HMFM_ORDER = i.OrderEX;
                //        formular_equat.HMFM_VALUE = i.ValueEx;
                //        formular_equat.HMFM_TYPE = i.TypeEX;
                //        formular_equat.HMFM_CREATED_BY = pUser;
                //        formular_equat.HMFM_CREATED_DATE = DateTime.Now;
                //        formular_equat.HMFM_UPDATED_BY = pUser;
                //        formular_equat.HMFM_UPDATED_DATE = DateTime.Now;
                //        formular_equat_dal.Save(formular_equat);
                //        c++;
                //    }
                //}

                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                rtn.Status = true;

                #endregion
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(Formular pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            HEDG_MT_FORMULA formular = new HEDG_MT_FORMULA();
            HEDG_MT_FORMULA_EQUATION formular_equat = new HEDG_MT_FORMULA_EQUATION();
            HEDG_MT_FORMULAR_DAL formular_dal = new HEDG_MT_FORMULAR_DAL();
            HEDG_MT_FORMULAR_EQUATION_DAL formular_equat_dal = new HEDG_MT_FORMULAR_EQUATION_DAL();
            EntityCPAIEngine context = new EntityCPAIEngine();
            try
            {
                #region Update Formular

                    formular.HMFM_ORDER = pModel.OrderFormular;
                    formular.HMFM_ROW_ID = pModel.Row_Id;
                    formular.HMFM_NAME = pModel.Name;
                    formular.HMFM_DESC = pModel.Description;
                    formular.HMFM_TEXT = pModel.Expression;
                    formular.HMFM_STATUS = pModel.Status;                 
                    formular.HMFM_UPDATED_DATE = DateTime.Now;
                    formular.HMFM_UPDATED_BY = pUser;
                    formular_dal.Update(formular);


                    var data = context.HEDG_MT_FORMULA_EQUATION.Where(d => d.HMFE_FK_MT_FORMULA == pModel.Row_Id);
                    if (data != null)
                    {
                        formular_equat_dal.Delete(pModel.Row_Id);
                    }
                    var c = 0;
                    foreach (var i in pModel.Formular_Exp)
                    {
                        var eid = ConstantPrm.GetDynamicCtrlID();
                        formular_equat.HMFE_ROW_ID = eid;
                        formular_equat.HMFE_FK_MT_FORMULA = pModel.Row_Id;
                        formular_equat.HMFM_ORDER = c.ToString();                       
                        formular_equat.HMFM_VALUE = i.ValueEx;
                        formular_equat.HMFM_TYPE = i.TypeEX;
                        formular_equat.HMFM_CREATED_BY = pUser;
                        formular_equat.HMFM_CREATED_DATE = DateTime.Now;
                        formular_equat.HMFM_UPDATED_BY = pUser;
                        formular_equat.HMFM_UPDATED_DATE = DateTime.Now;
                        formular_equat_dal.Save(formular_equat);
                        c++;
                    }
              

                //foreach (var item in pModel.Formular_Detail)
                //{
                //    var id = ConstantPrm.GetDynamicCtrlID();
                //    formular.HMFM_ORDER = item.OrderFormular;
                //    formular.HMFM_ROW_ID = id;
                //    formular.HMFM_NAME = item.Name;
                //    formular.HMFM_DESC = item.Description;
                //    formular.HMFM_TEXT = item.Expression;
                //    formular.HMFM_STATUS = item.Status;
                //    formular.HMFM_CREATED_DATE = DateTime.Now;
                //    formular.HMFM_CREATED_BY = pUser;
                //    formular.HMFM_UPDATED_DATE = DateTime.Now;
                //    formular.HMFM_UPDATED_BY = pUser;
                //    formular_dal.Save(formular);


                //    var c = 0;
                //    foreach (var i in item.Formular_Exp)
                //    {
                //        var eid = ConstantPrm.GetDynamicCtrlID();
                //        formular_equat.HMFE_ROW_ID = eid;
                //        formular_equat.HMFE_FK_MT_FORMULA = id;
                //        formular_equat.HMFM_ORDER = c.ToString();
                //        //formular_equat.HMFM_ORDER = i.OrderEX;
                //        formular_equat.HMFM_VALUE = i.ValueEx;
                //        formular_equat.HMFM_TYPE = i.TypeEX;
                //        formular_equat.HMFM_CREATED_BY = pUser;
                //        formular_equat.HMFM_CREATED_DATE = DateTime.Now;
                //        formular_equat.HMFM_UPDATED_BY = pUser;
                //        formular_equat.HMFM_UPDATED_DATE = DateTime.Now;
                //        formular_equat_dal.Save(formular_equat);
                //        c++;
                //    }
                //}

                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                rtn.Status = true;

                #endregion
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public List<Formular> GetAllFormular()
        {
            List<Formular> model = new List<Formular>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = context.HEDG_MT_FORMULA.ToList().OrderBy(c=>c.HMFM_ORDER);
               
                foreach (var item in query)
                {
                    Formular f = new Formular();
                    f.Row_Id = item.HMFM_ROW_ID;
                    f.Name = item.HMFM_NAME;
                    f.Description = item.HMFM_DESC;
                    f.OrderFormular = item.HMFM_ORDER;
                    f.Formular_Exp = GetExpression(item.HMFM_ROW_ID);
                    f.Expression = item.HMFM_TEXT;
                    f.Status = item.HMFM_STATUS;
                    model.Add(f);
                }


            }
            return model;
        }

        private List<Formular_Expression> GetExpression(string formularId)
        {
            List<Formular_Expression> exp = new List<Formular_Expression>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var expression = context.HEDG_MT_FORMULA_EQUATION.Where(c => c.HMFE_FK_MT_FORMULA == formularId).ToList();
                if (expression != null )
                {
                    var data = expression.OrderBy(c => c.HMFM_ORDER);
                    foreach (var item in data)
                    {
                        Formular_Expression dataExp = new Formular_Expression();
                        dataExp.Row_idEX = item.HMFE_ROW_ID;
                        dataExp.Formular_IdEX = item.HMFE_FK_MT_FORMULA;
                        dataExp.OrderEX = item.HMFM_ORDER;
                        dataExp.TypeEX = item.HMFM_TYPE;
                        dataExp.ValueEx = item.HMFM_VALUE;
                        exp.Add(dataExp);
                    }

                }
            }
            return exp;
        }

        public string DeleteFormular(string id)
        {
            var result = string.Empty;
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {                
                HEDG_MT_FORMULAR_DAL formular_dal = new HEDG_MT_FORMULAR_DAL();
                // HEDG_MT_FORMULAR_EQUATION_DAL formular_equat_dal = new HEDG_MT_FORMULAR_EQUATION_DAL();
                bool IsUse = context.HEDG_MT_TOOL_CONDITION.Where(c => c.HMTC_FK_MT_FORMULA == id).Any();
                if (IsUse == false)
                {
                    try
                    {
                        formular_dal.Delete(id);
                        result = "success";
                    }
                    catch (Exception ex)
                    {
                        result = ex.Message;
                        throw;
                    }
                }
                else
                {
                    result = "Can not delete this Formular. Because it is already use.";
                }
               
               
            }
            return result;
        }
    }

}