﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeProductServiceModel
    {
        ShareFn _FN = new ShareFn();
        public ReturnValue SearchMarket(ref HedgeProductViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string userlist = "";
                    if (model.sSelectUpdateBy != null)
                    {
                        userlist = model.sSelectUpdateBy.ToString().Replace(",", "|");
                    }

                    var sCode = String.IsNullOrEmpty(model.sCode) ? "" : model.sCode.ToUpper();
                    var sName = String.IsNullOrEmpty(model.sName) ? "" : model.sName.ToUpper();
                    var sFullName = String.IsNullOrEmpty(model.sFullName) ? "" : model.sFullName.ToUpper();
                    var sDesc = String.IsNullOrEmpty(model.sDescription) ? "" : model.sDescription;
                    var sStatus = String.IsNullOrEmpty(model.sStatus) ? "" : model.sStatus.ToUpper();
                    var sDateFrom = String.IsNullOrEmpty(model.sUpdateDate) ? "" : model.sUpdateDate.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(model.sUpdateDate) ? "" : model.sUpdateDate.ToUpper().Substring(14, 10);

                    var query = (from a in context.HEDG_MT_PROD
                                 select a).ToList();
                    if (!String.IsNullOrEmpty(sCode))
                    {
                        query = query.Where(c => c.HMP_ROW_ID == sCode).ToList();
                    }
                    if (!String.IsNullOrEmpty(sName))
                    {
                        query = query.Where(c => c.HMP_NAME == sName).ToList();
                    }
                    if (!String.IsNullOrEmpty(sFullName))
                    {
                        query = query.Where(c => c.HMP_FULL_NAME == sFullName).ToList();
                    }
                    if (!String.IsNullOrEmpty(sStatus))
                    {
                        query = query.Where(c => c.HMP_STATUS == sStatus).ToList();
                    }
                    if (!String.IsNullOrEmpty(sDesc))
                    {
                        query = query.Where(c => c.HMP_DESC == sDesc).ToList();
                    }
                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(c => c.HMP_UPDATED_DATE >= sDate && c.HMP_UPDATED_DATE <= eDate).ToList();
                    }
                    if (!string.IsNullOrEmpty(userlist))
                    {
                        //List<string> tUsers = new JavaScriptSerializer().Deserialize<string[]>(userlist).ToList<string>();
                        string[] tUsers = userlist.Split('|');
                        query = query.Where(a => tUsers.Contains(a.HMP_UPDATED_BY.ToUpper())).ToList();
                    }
                    if (query != null)
                    {
                        model.SearchProductList = new List<SearchProduct>();
                        foreach (var item in query)
                        {
                            model.SearchProductList.Add(new SearchProduct
                            {
                                CodeEncrypt = item.HMP_ROW_ID.Encrypt(),
                                Code = item.HMP_ROW_ID,
                                Name = item.HMP_NAME,
                                FullName = item.HMP_FULL_NAME,
                                Description = item.HMP_DESC,
                                Status = item.HMP_STATUS,
                                UpdateDate = item.HMP_UPDATED_DATE.ToString("dd/MM/yyyy"),
                                UpdateBy = item.HMP_UPDATED_BY,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue SearchMarket(ref HedgeProductViewModel model, string marketId, string marketName, string productId, string productName, string[] oldProduct)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from a in context.MKT_MST_MOPS_PRODUCT
                                 join b in context.MKT_MST_MOPS_MARKET on a.M_MPR_MKTID_FK equals b.M_MKT_ID
                                 where ((a.M_MPR_ID.ToString().ToUpper().Contains(productId.ToUpper())) || (String.IsNullOrEmpty(productId)))
                                 && ((a.M_MPR_ACTPRODNAME.ToUpper().Contains(productName.ToUpper())) || String.IsNullOrEmpty(productName))
                                 && ((b.M_MKT_ID.ToString().ToUpper().Contains(marketId.ToUpper())) || String.IsNullOrEmpty(marketId))
                                 //&& ((b.M_MKT_MKTNAME.ToUpper().Contains(marketName.ToUpper())) || String.IsNullOrEmpty(marketName))
                                 && ((b.M_MKT_ID.ToString().ToUpper().Contains(marketName.ToUpper())) || String.IsNullOrEmpty(marketName))
                                 select new
                                 {
                                     MarketID = b.M_MKT_ID,
                                     MarketName = b.M_MKT_MKTNAME,
                                     ProductID = a.M_MPR_ID,
                                     ProductName = a.M_MPR_ACTPRODNAME,
                                     Unit = a.M_MPR_UNIT,
                                 });

                    if (query != null)
                    {
                        model.SearchMarketList = new List<SearchMarketData>();
                        foreach (var item in query.ToList())
                        {
                            model.SearchMarketList.Add(new SearchMarketData
                            {
                                MarketId = item.MarketID.ToString(),
                                MarketName = item.MarketName,
                                ProductId = item.ProductID.ToString(),
                                ProductName = item.ProductName,
                                Unit = item.Unit
                            });
                        }
                        if (oldProduct != null)
                        {
                            foreach (var z in oldProduct)
                            {
                                model.oldSelectList.Add(z);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue AddMarketPrice(ref HedgeProductViewModel model, string[] market_no)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from a in context.MKT_MST_MOPS_PRODUCT
                                 join b in context.MKT_MST_MOPS_MARKET on a.M_MPR_MKTID_FK equals b.M_MKT_ID
                                 select new
                                 {
                                     MarketID = b.M_MKT_ID,
                                     MarketName = b.M_MKT_MKTNAME,
                                     ProductID = a.M_MPR_ID,
                                     ProductName = a.M_MPR_ACTPRODNAME,
                                     Unit = a.M_MPR_UNIT,
                                 });

                    if (query != null)
                    {
                        if (model.pageMode == "EDIT")
                        {
                            model.SearchMarketList = new List<SearchMarketData>();
                            //model.MappingMarketPriceList.RemoveAll(x => !market_no.Contains(x.ProductId_F));///delete
                            model.oldSelectList.AddRange(market_no);
                            var z = 0;
                            foreach(var chk in model.MappingMarketPriceList)
                            {
                                if(chk.Delete == "TRUE")
                                {
                                    z++;
                                }
                            }
                            for (int i = 0; i < market_no.Length; i++)
                            {
                                var chkOldSelect = model.MappingMarketPriceList.Where(x => x.ProductId_F == market_no[i]).FirstOrDefault();
                                if (chkOldSelect == null)
                                {
                                    decimal num = Convert.ToDecimal(market_no[i]);
                                    var item = query.Where(x => x.ProductID == num).FirstOrDefault();
                                    if (item != null)
                                    {
                                        List<MappingForward> fwdList = new List<MappingForward>();
                                        for(var j =0; j< model.MappingMarketPriceList[0].MappingForwardList.Count();j++)
                                        {
                                            fwdList.Add(new MappingForward {
                                                FKId = null,
                                                Name = null,
                                                RowId = null, 
                                                Number = null,
                                                ProductId = null,
                                                ProductName = null,
                                                Unit = null
                                            }); 
                                        }
                                             
                                        model.MappingMarketPriceList.Add(new MappingMarketPrice
                                        {
                                            ProductId_F = item.ProductID.ToString(),
                                            FirstPrice = item.ProductName,
                                            FirstPriceUnit = item.Unit,
                                            ProductId_S = item.ProductID.ToString(),
                                            SecondPrice = item.ProductName,
                                            SecondPriceUnit = item.Unit,
                                            ProductUnit = item.Unit,
                                            Show = false,
                                            MappingForwardList = fwdList,
                                            Number = ((model.MappingMarketPriceList.Count() - z) + 1).ToString()
                                        }); 
                                    }
                                } 
                            }
                        }
                        else
                        {
                            model.SearchMarketList = new List<SearchMarketData>();
                            if (model.MappingMarketPriceList == null)
                            {
                                model.MappingMarketPriceList = new List<MappingMarketPrice>();
                            }
                            if (model.MappingMarketPriceList.Count == 0)
                            {
                                var newMappingMarketPrice = new List<MappingMarketPrice>();
                                model.MMMPRODUCT_List = new List<MKT_MST_MOPS_PRODUCT_List>();
                                foreach (var item in query.Distinct().ToList())
                                {
                                    newMappingMarketPrice.Add(new MappingMarketPrice
                                    {
                                        ProductId_F = item.ProductID.ToString(),
                                        FirstPrice = item.ProductName,
                                        FirstPriceUnit = item.Unit,
                                        ProductId_S = item.ProductID.ToString(),
                                        SecondPrice = item.ProductName,
                                        SecondPriceUnit = item.Unit,
                                        ProductUnit = item.Unit,
                                        Show = false,
                                        MappingForwardList = new List<MappingForward>(),
                                    });
                                    model.MMMPRODUCT_List.Add(new MKT_MST_MOPS_PRODUCT_List
                                    {
                                        Id = item.ProductID.ToString(),
                                        MKT_MST_MOPS_MARKET_ID = item.MarketID.ToString(),
                                        Name = item.ProductName,
                                        Unit = item.Unit,
                                    });
                                }
                                if (market_no != null)
                                {
                                    var i = 1;
                                    foreach (var z in market_no)
                                    {
                                        var data = newMappingMarketPrice.SingleOrDefault(a => a.ProductId_F == z);
                                        if (data != null)
                                        {
                                            data.Number = i.ToString();
                                            data.MappingForwardList.Add(new MappingForward
                                            {
                                                Name = "",
                                                Unit = "",
                                                Number = "",
                                                FKId = "",
                                                RowId = "",
                                                ProductId = "",
                                                ProductName = "",
                                            });
                                            model.MappingMarketPriceList.Add(data);
                                            model.oldSelectList.Add(z);
                                        }
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                model.MappingMarketPriceList.RemoveAll(x => !market_no.Contains(x.ProductId_F));
                                model.oldSelectList.AddRange(market_no);
                                for (int i = 0; i < market_no.Length; i++)
                                {
                                    var chkOldSelect = model.MappingMarketPriceList.Where(x => x.ProductId_F == market_no[i]).FirstOrDefault();
                                    if (chkOldSelect == null)
                                    {
                                        decimal num = Convert.ToDecimal(market_no[i]);
                                        var item = query.Where(x => x.ProductID == num).FirstOrDefault();
                                        if (item != null)
                                        {
                                            List<MappingForward> fwdList = model.MappingMarketPriceList[0].MappingForwardList;
                                            for (int j = 0; j < fwdList.Count; j++)
                                            {
                                                fwdList[j].FKId = null;
                                                fwdList[j].Number = null;
                                                fwdList[j].ProductId = null;
                                                fwdList[j].ProductName = null;
                                                fwdList[j].RowId = null;
                                                fwdList[j].Unit = null;
                                            }
                                            model.MappingMarketPriceList.Add(new MappingMarketPrice
                                            {
                                                ProductId_F = item.ProductID.ToString(),
                                                FirstPrice = item.ProductName,
                                                FirstPriceUnit = item.Unit,
                                                ProductId_S = item.ProductID.ToString(),
                                                SecondPrice = item.ProductName,
                                                SecondPriceUnit = item.Unit,
                                                ProductUnit = item.Unit,
                                                Show = false,
                                                MappingForwardList = fwdList,
                                                Number = (model.MappingMarketPriceList.Count() + 1).ToString()
                                            });
                                        }
                                    }
                                }
                            } 
                        }
                        foreach (var z in query.Distinct().ToList())
                        {
                            model.MMMPRODUCT_List.Add(new MKT_MST_MOPS_PRODUCT_List
                            {
                                Id = z.ProductID.ToString(),
                                MKT_MST_MOPS_MARKET_ID = z.MarketID.ToString(),
                                Name = z.ProductName,
                                Unit = z.Unit,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue SaveDATA(ref HedgeProductViewModel model, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                HEDG_MT_PROD entHMP = new HEDG_MT_PROD();
                HEDG_MT_PROD_DAL dal = new HEDG_MT_PROD_DAL();
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var Code = ShareFn.GenerateCodeByDate("CPAI");
                            entHMP.HMP_ROW_ID = Code;
                            entHMP.HMP_STATUS = model.status;
                            entHMP.HMP_NAME = model.name;
                            entHMP.HMP_FULL_NAME = model.fullName;
                            entHMP.HMP_DESC = model.description;
                            entHMP.HMP_CREATED_DATE = DateTime.Now;
                            entHMP.HMP_CREATED_BY = pUser;
                            entHMP.HMP_UPDATED_DATE = DateTime.Now;
                            entHMP.HMP_UPDATED_BY = pUser;
                            dal.Save(entHMP, context);

                            if(model.MappingMarketPriceList != null)
                            {
                                foreach (var item in model.MappingMarketPriceList)
                                {
                                    if (item.Delete == null)
                                    {
                                        HEDG_MT_PROD_MKT entHMPK = new HEDG_MT_PROD_MKT();
                                        var CtrlID = ConstantPrm.GetDynamicCtrlID();
                                        entHMPK.HMPM_ROW_ID = CtrlID;
                                        entHMPK.HMPM_FK_PROD = Code;
                                        entHMPK.HMPM_ORDER = item.Number;
                                        entHMPK.HMPM_FK_MKT_PROD_F = item.ProductId_F;
                                        entHMPK.HMPM_MKT_PROD_F_NAME = item.FirstPrice;
                                        entHMPK.HMPM_MKT_PROD_F_UNIT = item.FirstPriceUnit;
                                        entHMPK.HMPM_FK_MKT_PROD_S = item.ProductId_S;
                                        entHMPK.HMPM_MKT_PROD_S_NAME = item.SecondPrice;
                                        entHMPK.HMPM_MKT_PROD_S_UNIT = item.SecondPriceUnit;
                                        entHMPK.HMPM_DISPLAY_NAME = item.ProductDisplayName;
                                        entHMPK.HMPM_UNIT = item.ProductUnit;
                                        entHMPK.HMPM_CF = item.CFBBL;
                                        entHMPK.HMPM_SHOW_FLAG = item.Show == false ? "F" : "T";
                                        entHMPK.HMPM_CREATED_DATE = DateTime.Now;
                                        entHMPK.HMPM_CREATED_BY = pUser;
                                        entHMPK.HMPM_UPDATED_DATE = DateTime.Now;
                                        entHMPK.HMPM_UPDATED_BY = pUser;
                                        dal.SaveMappingMarketPrice(entHMPK, context);
                                        var i = 0;
                                        foreach (var z in item.MappingForwardList)
                                        {
                                            HEDG_MT_PROD_MKT_FW entHMPMK = new HEDG_MT_PROD_MKT_FW();
                                            var CtrlIDdetail = ConstantPrm.GetDynamicCtrlID();
                                            entHMPMK.HMPF_ROW_ID = CtrlIDdetail;
                                            entHMPMK.HMPF_FK_PROD_MKT = CtrlID;
                                            entHMPMK.HMPF_ORDER = (i + 1).ToString();
                                            entHMPMK.HMPF_MKT_NAME = model.MappingMarketPriceList[0].MappingForwardList[i].Name;
                                            entHMPMK.HMPF_FK_MKT_PROD = z.ProductId;
                                            entHMPMK.HMPF_MKT_PROD_NAME = z.ProductName;
                                            entHMPMK.HMPF_MKT_PROD_UNIT = z.Unit;
                                            entHMPMK.HMPF_CREATED_DATE = DateTime.Now;
                                            entHMPMK.HMPF_CREATED_BY = pUser;
                                            entHMPMK.HMPF_UPDATED_DATE = DateTime.Now;
                                            entHMPMK.HMPF_UPDATED_BY = pUser;
                                            dal.SaveMappingForward(entHMPMK, context);
                                            i++;
                                        }
                                    }
                                }
                            }                           

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue UpdateDATA(ref HedgeProductViewModel model, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                HEDG_MT_PROD entHMP = new HEDG_MT_PROD();
                HEDG_MT_PROD_DAL dal = new HEDG_MT_PROD_DAL();
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            entHMP.HMP_ROW_ID = model.rowId.Decrypt();
                            entHMP.HMP_STATUS = model.status;
                            entHMP.HMP_NAME = model.name;
                            entHMP.HMP_FULL_NAME = model.fullName;
                            entHMP.HMP_DESC = model.description;
                            entHMP.HMP_UPDATED_DATE = DateTime.Now;
                            entHMP.HMP_UPDATED_BY = pUser;
                            dal.Update(entHMP, context);
                            var mappId = "";
                            foreach (var item in model.MappingMarketPriceList)
                            {
                                var fkId = model.rowId.Decrypt();
                                HEDG_MT_PROD_MKT entHMPK = context.HEDG_MT_PROD_MKT.SingleOrDefault(p => p.HMPM_ROW_ID == item.RowId && p.HMPM_FK_PROD == fkId);
                                if(item.Delete == "TRUE")
                                {
                                    dal.DeleteMappingMarketPrice(entHMPK, context);
                                }
                                else
                                {
                                    if (entHMPK == null)
                                    {
                                        entHMPK = new HEDG_MT_PROD_MKT();
                                        var CtrlID = ConstantPrm.GetDynamicCtrlID();
                                        mappId = CtrlID;
                                        entHMPK.HMPM_ROW_ID = CtrlID;
                                        entHMPK.HMPM_FK_PROD = fkId;
                                        entHMPK.HMPM_ORDER = item.Number;
                                        entHMPK.HMPM_FK_MKT_PROD_F = item.ProductId_F;
                                        entHMPK.HMPM_MKT_PROD_F_NAME = item.FirstPrice;
                                        entHMPK.HMPM_MKT_PROD_F_UNIT = item.FirstPriceUnit;
                                        entHMPK.HMPM_FK_MKT_PROD_S = item.ProductId_S;
                                        entHMPK.HMPM_MKT_PROD_S_NAME = item.SecondPrice;
                                        entHMPK.HMPM_MKT_PROD_S_UNIT = item.SecondPriceUnit;
                                        entHMPK.HMPM_DISPLAY_NAME = item.ProductDisplayName;
                                        entHMPK.HMPM_UNIT = item.ProductUnit;
                                        entHMPK.HMPM_CF = item.CFBBL;
                                        entHMPK.HMPM_SHOW_FLAG = item.Show == false ? "F" : "T";
                                        entHMPK.HMPM_CREATED_DATE = DateTime.Now;
                                        entHMPK.HMPM_CREATED_BY = pUser;
                                        entHMPK.HMPM_UPDATED_DATE = DateTime.Now;
                                        entHMPK.HMPM_UPDATED_BY = pUser;
                                        dal.SaveMappingMarketPrice(entHMPK, context);
                                    }
                                    else
                                    {
                                        entHMPK.HMPM_ROW_ID = item.RowId;
                                        entHMPK.HMPM_FK_PROD = fkId;
                                        entHMPK.HMPM_ORDER = item.Number;
                                        entHMPK.HMPM_FK_MKT_PROD_F = item.ProductId_F;
                                        entHMPK.HMPM_MKT_PROD_F_NAME = item.FirstPrice;
                                        entHMPK.HMPM_MKT_PROD_F_UNIT = item.FirstPriceUnit;
                                        entHMPK.HMPM_FK_MKT_PROD_S = item.ProductId_S;
                                        entHMPK.HMPM_MKT_PROD_S_NAME = item.SecondPrice;
                                        entHMPK.HMPM_MKT_PROD_S_UNIT = item.SecondPriceUnit;
                                        entHMPK.HMPM_DISPLAY_NAME = item.ProductDisplayName;
                                        entHMPK.HMPM_UNIT = item.ProductUnit;
                                        entHMPK.HMPM_CF = item.CFBBL;
                                        entHMPK.HMPM_SHOW_FLAG = item.Show == false ? "F" : "T";
                                        entHMPK.HMPM_UPDATED_DATE = DateTime.Now;
                                        entHMPK.HMPM_UPDATED_BY = pUser;
                                        dal.UpdateMappingMarketPrice(entHMPK, context);
                                    }
                                }                                
                                var i = 0;
                                foreach (var z in item.MappingForwardList.OrderBy(a =>a.Number))
                                {
                                    HEDG_MT_PROD_MKT_FW entHMPMK = context.HEDG_MT_PROD_MKT_FW.SingleOrDefault(p => p.HMPF_ROW_ID == z.RowId && p.HMPF_FK_PROD_MKT == item.RowId);
                                    if (entHMPMK == null)
                                    {
                                        entHMPMK = new HEDG_MT_PROD_MKT_FW(); 
                                        var CtrlIDdetail = ConstantPrm.GetDynamicCtrlID();
                                        entHMPMK.HMPF_ROW_ID = CtrlIDdetail;
                                        entHMPMK.HMPF_FK_PROD_MKT = mappId;
                                        entHMPMK.HMPF_ORDER = (i + 1).ToString();
                                        entHMPMK.HMPF_MKT_NAME = model.MappingMarketPriceList[0].MappingForwardList[i].Name;
                                        entHMPMK.HMPF_FK_MKT_PROD = z.ProductId;
                                        entHMPMK.HMPF_MKT_PROD_NAME = z.ProductName;
                                        entHMPMK.HMPF_MKT_PROD_UNIT = z.Unit;
                                        entHMPMK.HMPF_CREATED_DATE = DateTime.Now;
                                        entHMPMK.HMPF_CREATED_BY = pUser;
                                        entHMPMK.HMPF_UPDATED_DATE = DateTime.Now;
                                        entHMPMK.HMPF_UPDATED_BY = pUser;
                                        dal.SaveMappingForward(entHMPMK, context);
                                    }
                                    else
                                    {
                                        entHMPMK.HMPF_ROW_ID = z.RowId;
                                        entHMPMK.HMPF_FK_PROD_MKT = item.RowId;
                                        entHMPMK.HMPF_ORDER = z.Number;
                                        entHMPMK.HMPF_MKT_NAME = model.MappingMarketPriceList[0].MappingForwardList[i].Name;
                                        entHMPMK.HMPF_FK_MKT_PROD = z.ProductId;
                                        entHMPMK.HMPF_MKT_PROD_NAME = z.ProductName;
                                        entHMPMK.HMPF_MKT_PROD_UNIT = z.Unit;
                                        entHMPMK.HMPF_UPDATED_DATE = DateTime.Now;
                                        entHMPMK.HMPF_UPDATED_BY = pUser;
                                        dal.UpdateMappingForward(entHMPMK, context);
                                    }
                                    i++;                                  
                                }
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue GetDATA(ref HedgeProductViewModel model, string rowID)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (string.IsNullOrEmpty(rowID))
                {
                    rtn.Message = "Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                model.MappingMarketPriceList = new List<MappingMarketPrice>();
                using (var context = new EntityCPAIEngine())
                {
                    var query = context.HEDG_MT_PROD.Where(c => c.HMP_ROW_ID == rowID);
                    if (query != null)
                    {
                        foreach (var item in query.ToList())
                        {
                            model.code = item.HMP_ROW_ID;
                            model.status = item.HMP_STATUS;
                            model.name = item.HMP_NAME;
                            model.fullName = item.HMP_FULL_NAME;
                            model.description = item.HMP_DESC;

                            var HMPMKT = context.HEDG_MT_PROD_MKT.Where(a => a.HMPM_FK_PROD == item.HMP_ROW_ID).OrderBy(a => a.HMPM_ORDER).ToList();
                            foreach (var z in HMPMKT)
                            {
                                model.MappingMarketPriceList.Add(new MappingMarketPrice
                                {
                                    RowId = z.HMPM_ROW_ID,
                                    Number = z.HMPM_ORDER,
                                    ProductId_F = z.HMPM_FK_MKT_PROD_F,
                                    FirstPrice = z.HMPM_MKT_PROD_F_NAME,
                                    FirstPriceUnit = z.HMPM_MKT_PROD_F_UNIT,
                                    ProductId_S = z.HMPM_FK_MKT_PROD_S,
                                    SecondPrice = z.HMPM_MKT_PROD_S_NAME,
                                    SecondPriceUnit = z.HMPM_MKT_PROD_S_UNIT,
                                    ProductDisplayName = z.HMPM_DISPLAY_NAME,
                                    ProductUnit = z.HMPM_UNIT,
                                    CFBBL = z.HMPM_CF,
                                    Show = z.HMPM_SHOW_FLAG == "F" ? false : true,
                                    MappingForwardList = new List<MappingForward>(),
                                });
                                var HMPMF = context.HEDG_MT_PROD_MKT_FW.Where(a => a.HMPF_FK_PROD_MKT == z.HMPM_ROW_ID).OrderBy(a => a.HMPF_ORDER).ToList();
                                foreach (var a in HMPMF)
                                {
                                    foreach (var ab in model.MappingMarketPriceList)
                                    {
                                        if (a.HMPF_FK_PROD_MKT == ab.RowId)
                                        {
                                            ab.MappingForwardList.Add(new MappingForward
                                            {
                                                FKId = ab.RowId,
                                                Name = a.HMPF_MKT_NAME,
                                                Number = a.HMPF_ORDER,
                                                ProductId = a.HMPF_FK_MKT_PROD,
                                                ProductName = a.HMPF_MKT_PROD_NAME,
                                                Unit = a.HMPF_MKT_PROD_UNIT,
                                                RowId = a.HMPF_ROW_ID,
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        var queryMarketProduct = (from a in context.MKT_MST_MOPS_PRODUCT
                                                  join b in context.MKT_MST_MOPS_MARKET on a.M_MPR_MKTID_FK equals b.M_MKT_ID
                                                  select new
                                                  {
                                                      MarketID = b.M_MKT_ID,
                                                      MarketName = b.M_MKT_MKTNAME,
                                                      ProductID = a.M_MPR_ID,
                                                      ProductName = a.M_MPR_ACTPRODNAME,
                                                      Unit = a.M_MPR_UNIT,
                                                  });
                        if (queryMarketProduct != null)
                        {
                            model.MMMPRODUCT_List = new List<MKT_MST_MOPS_PRODUCT_List>();
                            foreach (var item in queryMarketProduct.Distinct().ToList())
                            {
                                model.MMMPRODUCT_List.Add(new MKT_MST_MOPS_PRODUCT_List
                                {
                                    Id = item.ProductID.ToString(),
                                    MKT_MST_MOPS_MARKET_ID = item.MarketID.ToString(),
                                    Name = item.ProductName,
                                    Unit = item.Unit,
                                });
                            }
                        }

                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                        rtn.Status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue DeleteDATA(ref HedgeProductViewModel model, string rowID)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (string.IsNullOrEmpty(rowID))
                {
                    rtn.Message = "Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                model.MappingMarketPriceList = new List<MappingMarketPrice>();
                HEDG_MT_PROD_DAL dal = new HEDG_MT_PROD_DAL();
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var query = context.HEDG_MT_PROD.Where(c => c.HMP_ROW_ID == rowID);
                            if (query != null)
                            {
                                foreach (var item in query.ToList())
                                {
                                    var HMPMKT = context.HEDG_MT_PROD_MKT.Where(a => a.HMPM_FK_PROD == item.HMP_ROW_ID).ToList();
                                    foreach (var z in HMPMKT)
                                    {
                                        var HMPMF = context.HEDG_MT_PROD_MKT_FW.Where(a => a.HMPF_FK_PROD_MKT == z.HMPM_ROW_ID).ToList();
                                        foreach (var a in HMPMF)
                                        {
                                            dal.DeleteMappingForward(a, context);
                                        }
                                        dal.DeleteMappingMarketPrice(z, context);
                                    }
                                    dal.Delete(item, context);
                                }
                                dbContextTransaction.Commit();
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

    }
}