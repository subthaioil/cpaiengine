﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALHedg;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeSteeringCommitteeServiceModel
    {
        public static string getTransactionByID(string id)
        {
            HedgingRootObject rootObj = new HedgingRootObject();
            rootObj.hedg_STRCMT_data = new HedgSTRCMTData();
            rootObj.hedg_STRCMT_extend_choice = new HedgSTRCMTExtendChoice();
            rootObj.hedg_STRCMT_main_choice = new HedgSTRCMTMainChoice();
            rootObj.hedg_STRCMT_submit_note = new HedgSTRCMTSubmitNote();
            rootObj.hedg_STRCMT_tool = new HedgSTRCMTTool();

            HEDG_STR_CMT_FW_DAL fwDal = new HEDG_STR_CMT_FW_DAL();
            HEDG_STR_CMT_FW fw = fwDal.GetByID(id);

            if (fw != null)
            {
                rootObj.hedg_STRCMT_data.approved_date = (fw.HSCF_APPROVED_DATE == DateTime.MinValue || fw.HSCF_APPROVED_DATE == null) ? "" : Convert.ToDateTime(fw.HSCF_APPROVED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.hedg_STRCMT_data.trading_book = fw.HSCF_FK_MT_COMPANY;
                rootObj.hedg_STRCMT_data.version = fw.HSCF_VERSION;
                rootObj.hedg_STRCMT_data.status = fw.HSCF_STATUS;
                rootObj.hedg_STRCMT_data.consecutive_flag = fw.HSCF_CONSECUTIVE_FLAG;
                rootObj.hedg_STRCMT_data.hedge_type = fw.HSCF_FK_HEDG_MT_HEDGE_TYPE;
                rootObj.hedg_STRCMT_data.m1_m_flag = fw.HSCF_M1_M_FLAG;
                rootObj.hedg_STRCMT_data.ref_no = fw.HSCF_REF_NO;
                rootObj.hedg_STRCMT_data.template = fw.HSCF_TEMPLATE;
                //rootObj.hedg_STRCMT_data.tenor_period_from = (fw.HSCF_TENOR_PERIOD_FROM == DateTime.MinValue || fw.HSCF_TENOR_PERIOD_FROM == null) ? "" : Convert.ToDateTime(fw.HSCF_TENOR_PERIOD_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //rootObj.hedg_STRCMT_data.tenor_period_to = (fw.HSCF_TENOR_PERIOD_TO == DateTime.MinValue || fw.HSCF_TENOR_PERIOD_TO == null) ? "" : Convert.ToDateTime(fw.HSCF_TENOR_PERIOD_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.hedg_STRCMT_data.time_spread = fw.HSCF_TIME_SPREAD_FLAG;
                rootObj.hedg_str_cmt_note = fw.HSCF_NOTE;

                HEDG_STR_CHOICE_TYPE_DAL ctDal = new HEDG_STR_CHOICE_TYPE_DAL();
                HEDG_STR_CHOICE_DETAIL_DAL cdDal = new HEDG_STR_CHOICE_DETAIL_DAL();
                HEDG_STR_CHOICE_ROLE_DAL crDal = new HEDG_STR_CHOICE_ROLE_DAL();
                HEDG_STR_CHOICE_PRODUCT_DAL cpDal = new HEDG_STR_CHOICE_PRODUCT_DAL();
                List<HEDG_STR_CHOICE_TYPE> ct = ctDal.GetBySTRCMTID(fw.HSCF_ROW_ID);

                if (ct != null)
                {
                    for (int i = 0; i < ct.Count; i++)
                    {
                        if (ct[i].HSCT_TYPE == CPAIConstantUtil.HEDG_CONST_MAIN)
                        {
                            HedgSTRCMTMainChoice mcdata = new HedgSTRCMTMainChoice();
                            mcdata.main_volume = ct[i].HSCT_VOLUME;
                            mcdata.unit = ct[i].HSCT_UNIT_PRICE;
                            mcdata.HedgSTRCMTMainChoiceDetail = new List<HedgSTRCMTMainChoiceDetail>();

                            List<HEDG_STR_CHOICE_DETAIL> cd = cdDal.GetBySTRCTID(ct[i].HSCT_ROW_ID);
                            if (cd != null)
                            {
                                for (int j = 0; j < cd.Count; j++)
                                {
                                    HedgSTRCMTMainChoiceDetail mddata = new HedgSTRCMTMainChoiceDetail();
                                    //mddata.order = cd[j].HSCD_ORDER;
                                    mddata.name = cd[j].HSCD_CHOICE_NAME;
                                    mddata.net_af_price = cd[j].HSCD_NET_AF_PRICE;
                                    mddata.package_name = cd[j].HSCD_PACKAGE_NAME;
                                    mddata.operation = cd[j].HSCD_OPERATION;

                                    List<HEDG_STR_CHOICE_ROLE> cr = crDal.GetBySTRCDID(cd[j].HSCD_ROW_ID);
                                    if (cr != null)
                                    {
                                        for (int k = 0; k < cr.Count; k++)
                                        {
                                            if (cr[k].HSCR_ROLE == CPAIConstantUtil.HEDG_CONST_SUBJECT)
                                            {
                                                mddata.HedgSTRCMTSubjectProduct = new HedgSTRCMTSubjectProduct();
                                                mddata.HedgSTRCMTSubjectProduct.avg_af_price = cr[k].HSCR_AVG_AF_PRICE;
                                                List<HEDG_STR_CHOICE_PRODUCT> cp  = cpDal.GetBySTRCRID(cr[k].HSCR_ROW_ID);
                                                if (cp != null)
                                                {
                                                    mddata.HedgSTRCMTSubjectProduct = new HedgSTRCMTSubjectProduct();
                                                    for (int l = 0; l < cp.Count; l++)
                                                    {
                                                        HedgSTRCMTProductDetail cpdata = new HedgSTRCMTProductDetail();
                                                        cpdata.order = cp[l].HSCP_ORDER;
                                                        cpdata.af_frame = cp[l].HSCP_AF_FRAME;
                                                        cpdata.af_price = cp[l].HSCP_AF_PRICE;
                                                        cpdata.price_unit = cp[l].HSCP_UNIT_PRICE;
                                                        cpdata.product_back = cp[l].HSCP_FK_HEDG_PRODUCT_BACK;
                                                        cpdata.product_front = cp[l].HSCP_FK_HEDG_PRODUCT_FRONT;
                                                        //cpdata.type = cp[l].HSCP_TYPE;
                                                        cpdata.weight_percent = cp[l].HSCP_WEIGHT_PERCENT;
                                                        cpdata.weight_volume = cp[l].HSCP_WEIGHT_VOLUME;
                                                        mddata.HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail.Add(cpdata);
                                                    }
                                                }
                                            }
                                            else if (cr[k].HSCR_ROLE == CPAIConstantUtil.HEDG_CONST_OBJECT)
                                            {
                                                mddata.HedgSTRCMTObjectProduct = new HedgSTRCMTObjectProduct();
                                                mddata.HedgSTRCMTObjectProduct.avg_af_price = cr[k].HSCR_AVG_AF_PRICE;
                                                List<HEDG_STR_CHOICE_PRODUCT> cp = cpDal.GetBySTRCRID(cr[k].HSCR_ROW_ID);
                                                if (cp != null)
                                                {
                                                    mddata.HedgSTRCMTObjectProduct = new HedgSTRCMTObjectProduct();
                                                    for (int l = 0; l < cp.Count; l++)
                                                    {
                                                        HedgSTRCMTProductDetail cpdata = new HedgSTRCMTProductDetail();
                                                        cpdata.order = cp[l].HSCP_ORDER;
                                                        cpdata.af_frame = cp[l].HSCP_AF_FRAME;
                                                        cpdata.af_price = cp[l].HSCP_AF_PRICE;
                                                        cpdata.price_unit = cp[l].HSCP_UNIT_PRICE;
                                                        cpdata.product_back = cp[l].HSCP_FK_HEDG_PRODUCT_BACK;
                                                        cpdata.product_front = cp[l].HSCP_FK_HEDG_PRODUCT_FRONT;
                                                        //cpdata.type = cp[l].HSCP_TYPE;
                                                        cpdata.weight_percent = cp[l].HSCP_WEIGHT_PERCENT;
                                                        cpdata.weight_volume = cp[l].HSCP_WEIGHT_VOLUME;
                                                        mddata.HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail.Add(cpdata);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            rootObj.hedg_STRCMT_main_choice = mcdata;

                        } else if (ct[i].HSCT_TYPE == CPAIConstantUtil.HEDG_CONST_EXTEND)
                        {
                            HedgSTRCMTExtendChoice mcdata = new HedgSTRCMTExtendChoice();
                            mcdata.extend_volume = ct[i].HSCT_VOLUME;
                            mcdata.unit = ct[i].HSCT_UNIT_PRICE;
                            mcdata.HedgSTRCMTExtendChoiceDetail = new List<HedgSTRCMTExtendChoiceDetail>();

                            List<HEDG_STR_CHOICE_DETAIL> cd = cdDal.GetBySTRCTID(ct[i].HSCT_ROW_ID);
                            if (cd != null)
                            {
                                for (int j = 0; j < cd.Count; j++)
                                {
                                    HedgSTRCMTExtendChoiceDetail mddata = new HedgSTRCMTExtendChoiceDetail();
                                    mddata.order = cd[j].HSCD_ORDER;
                                    mddata.name = cd[j].HSCD_CHOICE_NAME;
                                    mddata.net_af_price = cd[j].HSCD_NET_AF_PRICE;
                                    mddata.package_name = cd[j].HSCD_PACKAGE_NAME;
                                    mddata.operation = cd[j].HSCD_OPERATION;

                                    List<HEDG_STR_CHOICE_ROLE> cr = crDal.GetBySTRCDID(cd[j].HSCD_ROW_ID);
                                    if (cr != null)
                                    {
                                        for (int k = 0; k < cr.Count; k++)
                                        {
                                            if (cr[k].HSCR_ROLE == CPAIConstantUtil.HEDG_CONST_SUBJECT)
                                            {
                                                mddata.HedgSTRCMTSubjectProduct = new HedgSTRCMTSubjectProduct();
                                                mddata.HedgSTRCMTSubjectProduct.avg_af_price = cr[k].HSCR_AVG_AF_PRICE;
                                                List<HEDG_STR_CHOICE_PRODUCT> cp = cpDal.GetBySTRCRID(cr[k].HSCR_ROW_ID);
                                                if (cp != null)
                                                {
                                                    mddata.HedgSTRCMTSubjectProduct = new HedgSTRCMTSubjectProduct();
                                                    for (int l = 0; l < cp.Count; l++)
                                                    {
                                                        HedgSTRCMTProductDetail cpdata = new HedgSTRCMTProductDetail();
                                                        cpdata.order = cp[l].HSCP_ORDER;
                                                        cpdata.af_frame = cp[l].HSCP_AF_FRAME;
                                                        cpdata.af_price = cp[l].HSCP_AF_PRICE;
                                                        cpdata.price_unit = cp[l].HSCP_UNIT_PRICE;
                                                        cpdata.product_back = cp[l].HSCP_FK_HEDG_PRODUCT_BACK;
                                                        cpdata.product_front = cp[l].HSCP_FK_HEDG_PRODUCT_FRONT;
                                                        //cpdata.type = cp[l].HSCP_TYPE;
                                                        cpdata.weight_percent = cp[l].HSCP_WEIGHT_PERCENT;
                                                        cpdata.weight_volume = cp[l].HSCP_WEIGHT_VOLUME;
                                                        mddata.HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail.Add(cpdata);
                                                    }
                                                }
                                            }
                                            else if (cr[k].HSCR_ROLE == CPAIConstantUtil.HEDG_CONST_OBJECT)
                                            {
                                                mddata.HedgSTRCMTObjectProduct = new HedgSTRCMTObjectProduct();
                                                mddata.HedgSTRCMTObjectProduct.avg_af_price = cr[k].HSCR_AVG_AF_PRICE;
                                                List<HEDG_STR_CHOICE_PRODUCT> cp = cpDal.GetBySTRCRID(cr[k].HSCR_ROW_ID);
                                                if (cp != null)
                                                {
                                                    mddata.HedgSTRCMTObjectProduct = new HedgSTRCMTObjectProduct();
                                                    for (int l = 0; l < cp.Count; l++)
                                                    {
                                                        HedgSTRCMTProductDetail cpdata = new HedgSTRCMTProductDetail();
                                                        cpdata.order = cp[l].HSCP_ORDER;
                                                        cpdata.af_frame = cp[l].HSCP_AF_FRAME;
                                                        cpdata.af_price = cp[l].HSCP_AF_PRICE;
                                                        cpdata.price_unit = cp[l].HSCP_UNIT_PRICE;
                                                        cpdata.product_back = cp[l].HSCP_FK_HEDG_PRODUCT_BACK;
                                                        cpdata.product_front = cp[l].HSCP_FK_HEDG_PRODUCT_FRONT;
                                                        //cpdata.type = cp[l].HSCP_TYPE;
                                                        cpdata.weight_percent = cp[l].HSCP_WEIGHT_PERCENT;
                                                        cpdata.weight_volume = cp[l].HSCP_WEIGHT_VOLUME;
                                                        mddata.HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail.Add(cpdata);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    mcdata.HedgSTRCMTExtendChoiceDetail.Add(mddata);
                                }
                            }

                            rootObj.hedg_STRCMT_extend_choice = mcdata;
                        }
                    }
                }

                HEDG_STR_TOOL_DAL stDal = new HEDG_STR_TOOL_DAL();
                //HEDG_STR_TOOL_EXTENSION_DAL teDal = new HEDG_STR_TOOL_EXTENSION_DAL();
                HEDG_STR_TOOL_OPTION_DAL toDal = new HEDG_STR_TOOL_OPTION_DAL();
                HEDG_STR_TOOL_KIO_OPTION_DAL tkDal = new HEDG_STR_TOOL_KIO_OPTION_DAL();
                HEDG_STR_TOOL_FORMULA_DAL tfDal = new HEDG_STR_TOOL_FORMULA_DAL();
                HEDG_STR_TOOL_NET_PREMIUM_DAL npDal = new HEDG_STR_TOOL_NET_PREMIUM_DAL();
                HEDG_STR_TOOL_NP_ITEM_DAL niDal = new HEDG_STR_TOOL_NP_ITEM_DAL();

                List<HEDG_STR_TOOL> tool = stDal.GetBySTRCMTID(id);
                if (tool != null)
                {
                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail = new List<HedgSTRCMTToolDetail>();

                    for (int j = 0; j < tool.Count; j++)
                    {
                        //List<HEDG_STR_TOOL_EXTENSION> te = teDal.GetBySTRToolID(tool[j].HST_ROW_ID);
                        List<HEDG_STR_TOOL_OPTION> to = toDal.GetBySTRToolID(tool[j].HST_ROW_ID);
                        List<HEDG_STR_TOOL_FORMULA> tf = tfDal.GetBySTRToolID(tool[j].HST_ROW_ID);
                        List<HEDG_STR_TOOL_NET_PREMIUM> np = npDal.GetBySTRToolID(tool[j].HST_ROW_ID);

                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].order = tool[j].HST_ORDER;
                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].name = tool[j].HST_NAME;
                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].use_net_premium_flag = tool[j].HST_USE_NET_PREMIUM;
                        //rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].net_premium_type = tool[j].HST_NET_PREMIUM_TYPE;
                        //rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].net_premium_volume = tool[j].HST_NET_PREMIUM_VOLUME;
                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].use_formula_flag = tool[j].HST_USE_FORMULA;
                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTFormula = new HedgSTRCMTFormula();
                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTFormula.expression = tool[j].HST_EXPRESSION;

                        //if (te != null)
                        //{
                        //    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTExtension = new List<HedgSTRCMTExtension>();
                        //    for (int k = 0; k < te.Count; k++)
                        //    {
                        //        HedgSTRCMTExtension extension = new HedgSTRCMTExtension();
                        //        extension.order = te[k].HSTE_ORDER;
                        //        extension.exercise_date = (te[k].HSTE_EXCERCISE_DATE == DateTime.MinValue || te[k].HSTE_EXCERCISE_DATE == null) ? "" : Convert.ToDateTime(te[k].HSTE_EXCERCISE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        //        extension.extend_period_from = (te[k].HSTE_EXTEND_PERIOD_FROM == DateTime.MinValue || te[k].HSTE_EXTEND_PERIOD_FROM == null) ? "" : Convert.ToDateTime(te[k].HSTE_EXTEND_PERIOD_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        //        extension.extend_period_to = (te[k].HSTE_EXTEND_PERIOD_TO == DateTime.MinValue || te[k].HSTE_EXTEND_PERIOD_TO == null) ? "" : Convert.ToDateTime(te[k].HSTE_EXTEND_PERIOD_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        //        extension.extend_volume = te[k].HSTE_EXTEND_VOLUME;
                        //        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTExtension.Add(extension);
                        //    }
                        //}

                        if (to != null)
                        {
                            for (int k = 0; k < to.Count; k++)
                            {
                                //rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].capped_value = to[k].HSTO_CAPPED_VALUE;
                                //rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].target_redemption_gain_loss = to[k].HSTO_GAIN_LOSS;
                                rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].target_redemption_per_unit = to[k].HSTO_VALUE_PER_UNIT;
                                rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].swaption_exercise_date = (to[k].HSTO_EXCERCISE_DATE == DateTime.MinValue || to[k].HSTO_EXCERCISE_DATE == null) ? "" : Convert.ToDateTime(to[k].HSTO_EXCERCISE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                if (to[k].HSTO_KNOCK_IN_OUT_BY == "MARKET")
                                {
                                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_by_market = "Y";
                                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_by_package = "N";
                                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_market_option = new HedgSTRCMTKIOOption();
                                    List<HEDG_STR_TOOL_KIO_OPTION> tk = tkDal.GetBySTRTOID(to[k].HSTO_ROW_ID);
                                    if (tk != null && tk.Count > 0)
                                    {
                                        HedgSTRCMTKIOOption option = new HedgSTRCMTKIOOption();
                                        option.activated_operation = tk[0].HSTK_ACTIVATED_OPERATOR;
                                        option.activated_option = tk[0].HSTK_ACTIVATED_OPTION;
                                        option.hedge_price = tk[0].HSTK_HEDGE_PRICE;
                                        option.option = tk[0].HSTK_OPTION;
                                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[k].knock_in_out_market_option = option;
                                    }
                                }
                                else if (to[k].HSTO_KNOCK_IN_OUT_BY == "PACKAGE")
                                {
                                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_by_market = "N";
                                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_by_package = "Y";
                                    rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_package_option = new HedgSTRCMTKIOOption();
                                    List<HEDG_STR_TOOL_KIO_OPTION> tk = tkDal.GetBySTRTOID(to[k].HSTO_ROW_ID);
                                    if (tk != null && tk.Count > 0)
                                    {
                                        HedgSTRCMTKIOOption option = new HedgSTRCMTKIOOption();
                                        option.activated_operation = tk[0].HSTK_ACTIVATED_OPERATOR;
                                        option.activated_option = tk[0].HSTK_ACTIVATED_OPTION;
                                        option.hedge_price = tk[0].HSTK_HEDGE_PRICE;
                                        option.option = tk[0].HSTK_OPTION;
                                        rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].knock_in_out_package_option = option;
                                    }
                                }
                            }
                        }

                        if (tf != null)
                        {
                            rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail = new List<HedgSTRCMTFormulaDetail>();

                            for (int k = 0; k < tf.Count; k++)
                            {
                                HedgSTRCMTFormulaDetail detail = new HedgSTRCMTFormulaDetail();
                                detail.order = tf[k].HSTF_ORDER;
                                detail.type = tf[k].HSTF_TYPE;
                                detail.value = tf[k].HSTF_VALUE;
                                rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail.Add(detail);
                            }
                        }

                        if (np != null)
                        {
                            for (int k = 0; k < np.Count; k++)
                            {
                                rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTNetPremium = new HedgSTRCMTNetPremium();
                                rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTNetPremium.HedgSTRCMTOption = new List<HedgSTRCMTOption>();

                                HedgSTRCMTOption option = new HedgSTRCMTOption();
                                option.order = np[k].HSTN_ORDER;
                                option.name = np[k].HSTN_OPTION;
                                option.HedgSTRCMTOptionDetail = new List<HedgSTRCMTOptionDetail>();

                                List<HEDG_STR_TOOL_NP_ITEM> np_item = niDal.GetBySTRNPID(np[k].HSTN_ROW_ID);
                                if (np_item != null)
                                {
                                    for (int l = 0; l < np_item.Count; l++)
                                    {
                                        HedgSTRCMTOptionDetail detail = new HedgSTRCMTOptionDetail();
                                        detail.order = np_item[l].HSNI_ORDER;
                                        detail.net_target_value = np_item[l].HSNI_NET_TARGET;
                                        detail.premium_value = np_item[l].HSNI_PREMIUM;
                                        detail.return_value = np_item[l].HSNI_RETURN;
                                        option.HedgSTRCMTOptionDetail.Add(detail);
                                    }
                                }
                                rootObj.hedg_STRCMT_tool.HedgSTRCMTToolDetail[j].HedgSTRCMTNetPremium.HedgSTRCMTOption.Add(option);
                            }
                        }
                    }

                }

                string strExplanationAttach = "";
                HEDG_STR_CMT_ATTACH_FILE_DAL fileDal = new HEDG_STR_CMT_ATTACH_FILE_DAL();
                List<HEDG_STR_CMT_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id);
                foreach (var item in extFile)
                {
                    strExplanationAttach += item.HSAF_PATH + "|";
                }
                rootObj.attach_file = strExplanationAttach.Replace("|", "");

                HEDG_STR_CMT_SUBMIT_NOTE_DAL snDal = new HEDG_STR_CMT_SUBMIT_NOTE_DAL();
                HEDG_STR_CMT_SUBMIT_NOTE sn = snDal.GetBySTRCMTID(id);

                if (sn != null)
                {
                    rootObj.hedg_STRCMT_submit_note.subject = sn.HSSN_SUBJECT;
                    rootObj.hedg_STRCMT_submit_note.ref_no = sn.HSSN_REF_NO;
                    rootObj.hedg_STRCMT_submit_note.note = sn.HSSN_NOTE;
                    rootObj.hedg_STRCMT_submit_note.approved_date = (sn.HSSN_APPROVED_DATE == DateTime.MinValue || sn.HSSN_APPROVED_DATE == null) ? "" : Convert.ToDateTime(sn.HSSN_APPROVED_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    strExplanationAttach = "";
                    HEDG_STR_CMT_SN_ATTACH_FILE_DAL filesnDal = new HEDG_STR_CMT_SN_ATTACH_FILE_DAL();
                    List<HEDG_STR_CMT_SN_ATTACH_FILE> extsnFile = filesnDal.GetFileByDataID(sn.HSSN_ROW_ID);
                    foreach (var item in extsnFile)
                    {
                        strExplanationAttach += item.HSSF_PATH + "|";
                    }
                    rootObj.hedg_STRCMT_submit_note.attach_file = strExplanationAttach.Replace("|", "");
                }

            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public List<SelectListItem> GetHedgeCompany()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 where v.MCO_CREATE_TYPE != "CPAI"
                                 orderby v.MCO_COMPANY_CODE
                                 select new
                                 {
                                     dCompanyCode = v.MCO_COMPANY_CODE,
                                     dFullName = v.MCO_SHORT_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.dFullName, Value = g.dCompanyCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public List<SelectListItem> GetHedgeCompany(string trading_book = "")
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    trading_book = string.IsNullOrEmpty(trading_book) ? "" : trading_book;
                    var query = (from v in context.MT_COMPANY
                                 join h in context.HEDG_ANNUAL_FW on v.MCO_COMPANY_CODE equals h.HAF_FK_MT_COMPANY into h_join
                                 from h in h_join.DefaultIfEmpty()
                                 where v.MCO_CREATE_TYPE != "CPAI" && (h == null || (trading_book == h.HAF_FK_MT_COMPANY))
                                 orderby v.MCO_COMPANY_CODE
                                 select new
                                 {
                                     dCompanyCode = v.MCO_COMPANY_CODE,
                                     dFullName = v.MCO_SHORT_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.dFullName, Value = g.dCompanyCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public List<SelectListItem> GetHedgeType()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_HEDGE_TYPE
                                 where v.HMHT_STATUS == "ACTIVE"
                                 orderby v.HMHT_ROW_ID
                                 select new
                                 {
                                     HedgeTypeCode = v.HMHT_ROW_ID,
                                     HedgeTypeName = v.HMHT_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.HedgeTypeName, Value = g.HedgeTypeCode });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public List<SelectListItem> GetHedgeTitle()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from h in context.HEDG_ANNUAL_FW
                                 join ht in context.HEDG_ANNUAL_FW_HEDGE_TYPE on h.HAF_ROW_ID equals ht.HAFH_FK_HEDG_ANNUAL_FW
                                 join hd in context.HEDG_ANNUAL_FW_DETAIL on ht.HAFH_ROW_ID equals hd.HAFD_FK_ANNUAL_HEDGE_TYPE
                                 where h.HAF_STATUS == "ACTIVE"
                                 orderby hd.HAFD_TITLE
                                 select new
                                 {
                                     HedgeTitleID = hd.HAFD_ROW_ID,
                                     HedgeTitleName = hd.HAFD_TITLE
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.HedgeTitleName, Value = g.HedgeTitleID });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public List<SelectListItem> GetHedgeProduct()
        {

            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_PROD
                                 where v.HMP_STATUS == "ACTIVE"
                                 orderby v.HMP_ROW_ID
                                 select new
                                 {
                                     HedgeProductID = v.HMP_ROW_ID,
                                     HedgeProductName = v.HMP_NAME
                                 });

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            list.Add(new SelectListItem { Text = g.HedgeProductName, Value = g.HedgeProductID });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        public HedgeAnnualTypeDetailViewModel GetHedgeAnnualProductDetail(string rowid)
        {
            HedgeAnnualTypeDetailViewModel model = new HedgeAnnualTypeDetailViewModel();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from h in context.HEDG_ANNUAL_FW
                                 join ht in context.HEDG_ANNUAL_FW_HEDGE_TYPE on h.HAF_ROW_ID equals ht.HAFH_FK_HEDG_ANNUAL_FW
                                 join hd in context.HEDG_ANNUAL_FW_DETAIL on ht.HAFH_ROW_ID equals hd.HAFD_FK_ANNUAL_HEDGE_TYPE
                                 where h.HAF_STATUS == "ACTIVE" && hd.HAFD_ROW_ID.ToUpper() == rowid.ToUpper()
                                 orderby hd.HAFD_TITLE
                                 select hd
                                 );

                    if (query != null)
                    {
                        foreach (var g in query)
                        {
                            model.product_front = g.HAFD_FK_HEDG_PRODUCT_FRONT;
                            model.product_back = g.HAFD_FK_HEDG_PRODUCT_BACK;
                            model.unit_volume = g.HAFD_UNIT_VOLUME;
                            model.price_year = g.HAFD_PRICE_YEAR;
                            model.unit_price = g.HAFD_UNIT_PRICE;
                            model.min_max = g.HAFD_MIN_MAX;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
    }
}