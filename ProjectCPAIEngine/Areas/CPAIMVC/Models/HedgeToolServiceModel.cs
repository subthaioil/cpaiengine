﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeToolServiceModel
    {
        public ReturnValue Add(HedgeToolViewModel_Detail HedgeToolViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (HedgeToolViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(HedgeToolViewDetailModel.Name))
                {
                    rtn.Message = "Tool Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(HedgeToolViewDetailModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (HedgeToolViewDetailModel.FeatureKnockInOutFlag == "Y" && HedgeToolViewDetailModel.ToolSwitch == null)
                {
                    rtn.Message = "Tool Switch type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (HedgeToolViewDetailModel.ToolOption == null)
                {
                    rtn.Message = "Tool Option type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                HEDG_MT_TOOL_DAL dal = new HEDG_MT_TOOL_DAL();
                HEDG_MT_TOOL ent = new HEDG_MT_TOOL();

                HEDG_MT_TOOL_SWITCH_DAL sDal = new HEDG_MT_TOOL_SWITCH_DAL();
                HEDG_MT_TOOL_SWITCH sEnt = new HEDG_MT_TOOL_SWITCH();

                HEDG_MT_TOOL_OPTION_DAL oDal = new HEDG_MT_TOOL_OPTION_DAL();
                HEDG_MT_TOOL_OPTION oEnt = new HEDG_MT_TOOL_OPTION();

                HEDG_MT_TOOL_CONDITION_DAL cDal = new HEDG_MT_TOOL_CONDITION_DAL();
                HEDG_MT_TOOL_CONDITION cEnt = new HEDG_MT_TOOL_CONDITION();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ent = new HEDG_MT_TOOL();
                            string ctrlID = ConstantPrm.GetDynamicCtrlID();
                            string ctrlToolID = "TOOL-" + MasterData.GetSeqHGTOOL();
                            ent.HMT_ROW_ID = ctrlID;
                            ent.HMT_CODE = ctrlToolID;
                            ent.HMT_NAME = HedgeToolViewDetailModel.Name;
                            ent.HMT_DESC = HedgeToolViewDetailModel.Description;
                            ent.HMT_FEATURE_TARGET_FLAG = HedgeToolViewDetailModel.FeatureTargetFlag;
                            ent.HMT_FEATURE_SWAP_FLAG = HedgeToolViewDetailModel.FeatureSwapFlag;
                            //ent.HMT_FEATURE_EXTEND_FLAG = HedgeToolViewDetailModel.FeatureExtendFlag;
                            ent.HMT_FEATURE_CAPPED_FLAG = HedgeToolViewDetailModel.FeatureCappedFlag;
                            ent.HMT_FEATURE_KNOCK_IN_OUT_FLAG = HedgeToolViewDetailModel.FeatureKnockInOutFlag;
                            ent.HMT_VERSION = "1";
                            ent.HMT_REASON = HedgeToolViewDetailModel.Reason;
                            ent.HMT_STATUS = HedgeToolViewDetailModel.Status;
                            ent.HMT_CREATED_BY = pUser;
                            ent.HMT_CREATED_DATE = now;
                            ent.HMT_UPDATED_BY = pUser;
                            ent.HMT_UPDATED_DATE = now;
                            dal.Save(ent, context);

                            if (HedgeToolViewDetailModel.FeatureKnockInOutFlag == "Y")
                            {
                                var switchDataList = HedgeToolViewDetailModel.ToolSwitch;
                                foreach (var switchData in switchDataList)
                                {
                                    sEnt = new HEDG_MT_TOOL_SWITCH();
                                    string switchCtrlID = ConstantPrm.GetDynamicCtrlID();
                                    sEnt.HMTS_ROW_ID = switchCtrlID;
                                    sEnt.HMTS_FK_HEDG_MT_TOOL = ctrlID;
                                    sEnt.HMTS_ORDER = switchData.Order;
                                    sEnt.HMTS_OPTION = switchData.Option;
                                    sEnt.HMTS_CREATED_DATE = now;
                                    sEnt.HMTS_CREATED_BY = pUser;
                                    sEnt.HMTS_UPDATED_DATE = now;
                                    sEnt.HMTS_UPDATED_BY = pUser;
                                    sDal.Save(sEnt, context);
                                }
                            }

                            var optionDataList = HedgeToolViewDetailModel.ToolOption;
                            foreach (var optionData in optionDataList)
                            {
                                oEnt = new HEDG_MT_TOOL_OPTION();
                                string optionCtrlID = ConstantPrm.GetDynamicCtrlID();
                                oEnt.HMTO_ROW_ID = optionCtrlID;
                                oEnt.HMTO_FK_HEDG_MT_TOOL = ctrlID;
                                oEnt.HMTO_ORDER = optionData.Order;
                                oEnt.HMTO_OPTION = optionData.Option;
                                oEnt.HMTO_CREATED_DATE = now;
                                oEnt.HMTO_CREATED_BY = pUser;
                                oEnt.HMTO_UPDATED_DATE = now;
                                oEnt.HMTO_UPDATED_BY = pUser;
                                oDal.Save(oEnt, context);

                                var conditionDataList = optionData.ToolCondition;
                                foreach (var conditionData in conditionDataList)
                                {
                                    cEnt = new HEDG_MT_TOOL_CONDITION();
                                    string conditionCtrlID = ConstantPrm.GetDynamicCtrlID();
                                    cEnt.HMTC_ROW_ID = conditionCtrlID;
                                    cEnt.HMTC_FK_HEDG_TOOL_OPTION = optionCtrlID;
                                    cEnt.HMTC_FK_MT_FORMULA = conditionData.Formula;
                                    cEnt.HMTC_ORDER = conditionData.Order;
                                    cEnt.HMTC_VALUE_A = conditionData.Value_A;
                                    cEnt.HMTC_VALUE_B = conditionData.Value_B;
                                    cEnt.HMTC_OPERATOR = conditionData.Operator;
                                    cEnt.HMTC_EXPRESSION = conditionData.Expression;
                                    cEnt.HMTC_RETURN_FLAG = conditionData.ReturnFlag;
                                    cEnt.HMTC_PREMIUM_FLAG = conditionData.PremiumFlag;
                                    cEnt.HMTC_CREATED_DATE = now;
                                    cEnt.HMTC_CREATED_BY = pUser;
                                    cEnt.HMTC_UPDATED_DATE = now;
                                    cEnt.HMTC_UPDATED_BY = pUser;
                                    cDal.Save(cEnt, context);
                                }
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(HedgeToolViewModel_Detail HedgeToolViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (HedgeToolViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(HedgeToolViewDetailModel.Name))
                {
                    rtn.Message = "Tool Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(HedgeToolViewDetailModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (HedgeToolViewDetailModel.FeatureKnockInOutFlag == "Y" && HedgeToolViewDetailModel.ToolSwitch == null)
                {
                    rtn.Message = "Tool Switch type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (HedgeToolViewDetailModel.ToolOption == null)
                {
                    rtn.Message = "Tool Option type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                HEDG_MT_TOOL_DAL dal = new HEDG_MT_TOOL_DAL();
                HEDG_MT_TOOL ent = new HEDG_MT_TOOL();

                HEDG_MT_TOOL_SWITCH_DAL sDal = new HEDG_MT_TOOL_SWITCH_DAL();
                HEDG_MT_TOOL_SWITCH sEnt = new HEDG_MT_TOOL_SWITCH();

                HEDG_MT_TOOL_OPTION_DAL oDal = new HEDG_MT_TOOL_OPTION_DAL();
                HEDG_MT_TOOL_OPTION oEnt = new HEDG_MT_TOOL_OPTION();

                HEDG_MT_TOOL_CONDITION_DAL cDal = new HEDG_MT_TOOL_CONDITION_DAL();
                HEDG_MT_TOOL_CONDITION cEnt = new HEDG_MT_TOOL_CONDITION();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.HMT_ROW_ID = HedgeToolViewDetailModel.HMT_ROW_ID;
                            ent.HMT_REASON = HedgeToolViewDetailModel.Reason;
                            ent.HMT_STATUS = ConstantPrm.ACTION.HISTORY;
                            ent.HMT_UPDATED_BY = pUser;
                            ent.HMT_UPDATED_DATE = now;
                            dal.Update(ent, context);

                            ent = new HEDG_MT_TOOL();
                            string ctrlID = ConstantPrm.GetDynamicCtrlID();
                            ent.HMT_ROW_ID = ctrlID;
                            ent.HMT_CODE = HedgeToolViewDetailModel.Code;
                            ent.HMT_NAME = HedgeToolViewDetailModel.Name;
                            ent.HMT_DESC = HedgeToolViewDetailModel.Description;
                            ent.HMT_FEATURE_TARGET_FLAG = HedgeToolViewDetailModel.FeatureTargetFlag;
                            ent.HMT_FEATURE_SWAP_FLAG = HedgeToolViewDetailModel.FeatureSwapFlag;
                            //ent.HMT_FEATURE_EXTEND_FLAG = HedgeToolViewDetailModel.FeatureExtendFlag;
                            ent.HMT_FEATURE_CAPPED_FLAG = HedgeToolViewDetailModel.FeatureCappedFlag;
                            ent.HMT_FEATURE_KNOCK_IN_OUT_FLAG = HedgeToolViewDetailModel.FeatureKnockInOutFlag;
                            ent.HMT_VERSION = (Convert.ToInt32(HedgeToolViewDetailModel.Version) + 1).ToString();
                            ent.HMT_REASON = null;
                            ent.HMT_STATUS = HedgeToolViewDetailModel.Status;
                            ent.HMT_CREATED_BY = pUser;
                            ent.HMT_CREATED_DATE = now;
                            ent.HMT_UPDATED_BY = pUser;
                            ent.HMT_UPDATED_DATE = now;
                            dal.Save(ent, context);

                            if (HedgeToolViewDetailModel.FeatureKnockInOutFlag == "Y")
                            {
                                var switchDataList = HedgeToolViewDetailModel.ToolSwitch;
                                foreach (var switchData in switchDataList)
                                {
                                    sEnt = new HEDG_MT_TOOL_SWITCH();
                                    string switchCtrlID = ConstantPrm.GetDynamicCtrlID();
                                    sEnt.HMTS_ROW_ID = switchCtrlID;
                                    sEnt.HMTS_FK_HEDG_MT_TOOL = ctrlID;
                                    sEnt.HMTS_ORDER = switchData.Order;
                                    sEnt.HMTS_OPTION = switchData.Option;
                                    sEnt.HMTS_CREATED_DATE = now;
                                    sEnt.HMTS_CREATED_BY = pUser;
                                    sEnt.HMTS_UPDATED_DATE = now;
                                    sEnt.HMTS_UPDATED_BY = pUser;
                                    sDal.Save(sEnt, context);
                                }
                            }

                            var optionDataList = HedgeToolViewDetailModel.ToolOption;
                            foreach (var optionData in optionDataList)
                            {
                                oEnt = new HEDG_MT_TOOL_OPTION();
                                string optionCtrlID = ConstantPrm.GetDynamicCtrlID();
                                oEnt.HMTO_ROW_ID = optionCtrlID;
                                oEnt.HMTO_FK_HEDG_MT_TOOL = ctrlID;
                                oEnt.HMTO_ORDER = optionData.Order;
                                oEnt.HMTO_OPTION = optionData.Option;
                                oEnt.HMTO_CREATED_DATE = now;
                                oEnt.HMTO_CREATED_BY = pUser;
                                oEnt.HMTO_UPDATED_DATE = now;
                                oEnt.HMTO_UPDATED_BY = pUser;
                                oDal.Save(oEnt, context);

                                var conditionDataList = optionData.ToolCondition;
                                foreach (var conditionData in conditionDataList)
                                {
                                    cEnt = new HEDG_MT_TOOL_CONDITION();
                                    string conditionCtrlID = ConstantPrm.GetDynamicCtrlID();
                                    cEnt.HMTC_ROW_ID = conditionCtrlID;
                                    cEnt.HMTC_FK_HEDG_TOOL_OPTION = optionCtrlID;
                                    cEnt.HMTC_FK_MT_FORMULA = conditionData.Formula;
                                    cEnt.HMTC_ORDER = conditionData.Order;
                                    cEnt.HMTC_VALUE_A = conditionData.Value_A;
                                    cEnt.HMTC_VALUE_B = conditionData.Value_B;
                                    cEnt.HMTC_OPERATOR = conditionData.Operator;
                                    cEnt.HMTC_EXPRESSION = conditionData.Expression;
                                    cEnt.HMTC_RETURN_FLAG = conditionData.ReturnFlag;
                                    cEnt.HMTC_PREMIUM_FLAG = conditionData.PremiumFlag;
                                    cEnt.HMTC_CREATED_DATE = now;
                                    cEnt.HMTC_CREATED_BY = pUser;
                                    cEnt.HMTC_UPDATED_DATE = now;
                                    cEnt.HMTC_UPDATED_BY = pUser;
                                    cDal.Save(cEnt, context);
                                }
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref HedgeToolViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            ShareFn ShareFn = new ShareFn();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sCode = String.IsNullOrEmpty(pModel.sCode) ? "" : pModel.sCode.ToUpper();
                    var sName = String.IsNullOrEmpty(pModel.sName) ? "" : pModel.sName.ToUpper();
                    var sDesc = String.IsNullOrEmpty(pModel.sDesc) ? "" : pModel.sDesc.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();
                    var sOption = String.IsNullOrEmpty(pModel.sOption) ? "" : pModel.sOption.ToUpper();
                    var sUpdateDate = String.IsNullOrEmpty(pModel.sUpdateDate) ? "" : pModel.sUpdateDate;
                    var sUpdateBy = String.IsNullOrEmpty(pModel.sUpdateBy) ? "" : pModel.sUpdateBy.ToUpper();

                    string userlist = "";
                    if (pModel.sSelectUpdateBy != null)
                    {
                        userlist = pModel.sSelectUpdateBy.ToString().Replace(",", "|");
                    }

                    var queryTemp = (from v in context.HEDG_MT_TOOL
                                     join vo in context.HEDG_MT_TOOL_OPTION on v.HMT_ROW_ID equals vo.HMTO_FK_HEDG_MT_TOOL
                                     orderby new { v.HMT_ROW_ID, v.HMT_STATUS }
                                     where v.HMT_STATUS != ConstantPrm.ACTION.HISTORY
                                     select new
                                     {
                                         dHedgeToolID = v.HMT_ROW_ID,
                                         dCode = v.HMT_CODE,
                                         dName = v.HMT_NAME,
                                         dDesc = v.HMT_DESC,
                                         dOption = vo.HMTO_OPTION,
                                         dStatus = v.HMT_STATUS,
                                         dVersion = v.HMT_VERSION,
                                         dUpdateDate = v.HMT_UPDATED_DATE,
                                         dUpdateBy = v.HMT_UPDATED_BY,

                                         dFeatureTargetFlag = v.HMT_FEATURE_TARGET_FLAG,
                                         dFeatureSwapFlag = v.HMT_FEATURE_SWAP_FLAG,
                                         //dFeatureExtendFlag = v.HMT_FEATURE_EXTEND_FLAG,
                                         dFeatureCappedFlag = v.HMT_FEATURE_CAPPED_FLAG,
                                         dFeatureKnockInOutFlag = v.HMT_FEATURE_KNOCK_IN_OUT_FLAG

                                     }).AsEnumerable();

                    var query = queryTemp
                    .GroupBy(l => new
                    {
                        l.dHedgeToolID,
                        l.dCode,
                        l.dName,
                        l.dDesc,
                        l.dStatus,
                        l.dVersion,
                        l.dUpdateDate,
                        l.dUpdateBy,
                        l.dFeatureTargetFlag,
                        l.dFeatureSwapFlag,
                        //l.dFeatureExtendFlag,
                        l.dFeatureCappedFlag,
                        l.dFeatureKnockInOutFlag
                    })
                    .Select(g => new HedgeToolViewModel_SearchData
                    {
                        dHedgeToolID = g.Key.dHedgeToolID,
                        dCode = g.Key.dCode,
                        dName = g.Key.dName,
                        dDesc = g.Key.dDesc,
                        dStatus = g.Key.dStatus,
                        dOption = string.Join(",", g.Select(i => i.dOption)),
                        dVersion = g.Key.dVersion,
                        dUpdateDate = g.Key.dUpdateDate.ToString("dd/MM/yyyy"),
                        dUpdateBy = g.Key.dUpdateBy,

                        dFeatureTargetFlag = g.Key.dFeatureTargetFlag,
                        dFeatureSwapFlag = g.Key.dFeatureSwapFlag,
                        //dFeatureExtendFlag = g.Key.dFeatureExtendFlag,
                        dFeatureCappedFlag = g.Key.dFeatureCappedFlag,
                        dFeatureKnockInOutFlag = g.Key.dFeatureKnockInOutFlag
                    });


                    if (!string.IsNullOrEmpty(sCode))
                        query = query.Where(p => p.dCode.ToUpper().Contains(sCode.ToUpper()));

                    if (!string.IsNullOrEmpty(sName))
                        query = query.Where(p => p.dName.ToUpper().Contains(sName.ToUpper()));

                    if (!string.IsNullOrEmpty(sDesc))
                        query = query.Where(p => p.dDesc.ToUpper().Contains(sDesc.ToUpper()));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus.ToUpper()));

                    if (!string.IsNullOrEmpty(sOption))
                        query = query.Where(p => p.dOption.ToUpper().Contains(sOption.ToUpper()));

                    if (!string.IsNullOrEmpty(sUpdateDate))
                    {
                        string[] sUpdateDateData = sUpdateDate.Split(new[] { " to " }, StringSplitOptions.None);
                        DateTime? sDateFrom = ShareFn.ConvertStringDateFormatToDatetime(sUpdateDateData[0], "dd/MM/yyyy");
                        DateTime? sDateTo = ShareFn.ConvertStringDateFormatToDatetime(sUpdateDateData[1], "dd/MM/yyyy");
                        query = query.Where(p => ShareFn.ConvertStringDateFormatToDatetime(p.dUpdateDate, "dd/MM/yyyy") >= sDateFrom &&
                        ShareFn.ConvertStringDateFormatToDatetime(p.dUpdateDate, "dd/MM/yyyy") <= sDateTo);
                    }
                    if (!string.IsNullOrEmpty(userlist))
                    {
                        string[] tUsers = userlist.Split('|');
                        query = query.Where(a => tUsers.Contains(a.dUpdateBy.ToUpper())).ToList();
                    }

                    if (query != null)
                    {
                        pModel.sSearchData = new List<HedgeToolViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new HedgeToolViewModel_SearchData
                                                    {
                                                        dHedgeToolID = g.dHedgeToolID,
                                                        dCode = g.dCode,
                                                        dName = g.dName,
                                                        dDesc = g.dDesc,
                                                        dStatus = g.dStatus,
                                                        dOption = g.dOption,
                                                        dVersion = g.dVersion,
                                                        dUpdateDate = g.dUpdateDate,
                                                        dUpdateBy = g.dUpdateBy,

                                                        dFeatureTargetFlag = g.dFeatureTargetFlag,
                                                        dFeatureSwapFlag = g.dFeatureSwapFlag,
                                                        dFeatureExtendFlag = g.dFeatureExtendFlag,
                                                        dFeatureCappedFlag = g.dFeatureCappedFlag,
                                                        dFeatureKnockInOutFlag = g.dFeatureKnockInOutFlag
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HedgeToolViewModel_Detail Get(string pID)
        {
            HedgeToolViewModel_Detail model = new HedgeToolViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        HEDG_MT_TOOL_SWITCH_DAL sDal = new HEDG_MT_TOOL_SWITCH_DAL();
                        HEDG_MT_TOOL_OPTION_DAL oDal = new HEDG_MT_TOOL_OPTION_DAL();
                        HEDG_MT_TOOL_CONDITION_DAL cDal = new HEDG_MT_TOOL_CONDITION_DAL();

                        var query = (from v in context.HEDG_MT_TOOL
                                     where v.HMT_ROW_ID == pID
                                     orderby new { v.HMT_ROW_ID }
                                     select new
                                     {
                                         dHedgeToolID = v.HMT_ROW_ID,
                                         dCode = v.HMT_CODE,
                                         dName = v.HMT_NAME,
                                         dDesc = v.HMT_DESC,
                                         dTargetFlag = v.HMT_FEATURE_TARGET_FLAG,
                                         dSwapFlag = v.HMT_FEATURE_SWAP_FLAG,
                                         //dExtendFlag = v.HMT_FEATURE_EXTEND_FLAG,
                                         dCappedFlag = v.HMT_FEATURE_CAPPED_FLAG,
                                         dKnockFlag = v.HMT_FEATURE_KNOCK_IN_OUT_FLAG,
                                         dVersion = v.HMT_VERSION,
                                         dReason = v.HMT_REASON,
                                         dStatus = v.HMT_STATUS,
                                         //dCreateDate = v.HMT_CREATED_DATE,
                                         //dCreateBy = v.HMT_CREATED_BY,
                                         //dUpdateDate = v.HMT_UPDATED_DATE,
                                         //dUpdateBy = v.HMT_UPDATED_BY
                                     });
                        if (query != null)
                        {
                            foreach (var item in query)
                            {
                                model.HMT_ROW_ID = string.IsNullOrEmpty(item.dHedgeToolID) ? "" : item.dHedgeToolID;
                                model.Code = string.IsNullOrEmpty(item.dCode) ? "" : item.dCode;
                                model.Name = string.IsNullOrEmpty(item.dName) ? "" : item.dName;
                                model.Description = string.IsNullOrEmpty(item.dDesc) ? "" : item.dDesc;
                                model.FeatureTargetFlag = string.IsNullOrEmpty(item.dTargetFlag) ? "" : item.dTargetFlag;
                                model.FeatureSwapFlag = string.IsNullOrEmpty(item.dSwapFlag) ? "" : item.dSwapFlag;
                                //model.FeatureExtendFlag = string.IsNullOrEmpty(item.dExtendFlag) ? "" : item.dExtendFlag;
                                model.FeatureCappedFlag = string.IsNullOrEmpty(item.dCappedFlag) ? "" : item.dCappedFlag;
                                model.FeatureKnockInOutFlag = string.IsNullOrEmpty(item.dKnockFlag) ? "" : item.dKnockFlag;
                                model.Version = string.IsNullOrEmpty(item.dVersion) ? "" : item.dVersion;
                                model.Reason = string.IsNullOrEmpty(item.dReason) ? "" : item.dReason;
                                model.Status = string.IsNullOrEmpty(item.dStatus) ? "" : item.dStatus.ToUpper();
                            }

                            List<HEDG_MT_TOOL_SWITCH> sLst = sDal.GetDataByFkID(pID);
                            model.ToolSwitch = new List<ToolSwitch>();
                            foreach (HEDG_MT_TOOL_SWITCH item in sLst)
                            {
                                ToolSwitch toolSwitch = new ToolSwitch();
                                toolSwitch.HMTS_ROW_ID = item.HMTS_ROW_ID;
                                toolSwitch.Order = item.HMTS_ORDER;
                                toolSwitch.Option = item.HMTS_OPTION;
                                toolSwitch.ToolID = item.HMTS_FK_HEDG_MT_TOOL;
                                model.ToolSwitch.Add(toolSwitch);
                            }

                            List<HEDG_MT_TOOL_OPTION> oLst = oDal.GetDataByFkID(pID);
                            model.ToolOption = new List<ToolOption>();
                            foreach (HEDG_MT_TOOL_OPTION item in oLst)
                            {
                                ToolOption toolOption = new ToolOption();
                                toolOption.HMTO_ROW_ID = item.HMTO_ROW_ID;
                                toolOption.ToolID = item.HMTO_FK_HEDG_MT_TOOL;
                                toolOption.Order = item.HMTO_ORDER;
                                toolOption.Option = item.HMTO_OPTION;

                                List<HEDG_MT_TOOL_CONDITION> cLst = cDal.GetDataByFkID(item.HMTO_ROW_ID);
                                toolOption.ToolCondition = new List<ToolCondition>();
                                foreach (HEDG_MT_TOOL_CONDITION cItem in cLst)
                                {
                                    ToolCondition toolCondition = new ToolCondition();
                                    toolCondition.HMTC_ROW_ID = cItem.HMTC_ROW_ID;
                                    toolCondition.ToolOptionID = cItem.HMTC_FK_HEDG_TOOL_OPTION;
                                    toolCondition.Order = cItem.HMTC_ORDER;
                                    toolCondition.Value_A = cItem.HMTC_VALUE_A;
                                    toolCondition.Value_B = cItem.HMTC_VALUE_B;
                                    toolCondition.Operator = cItem.HMTC_OPERATOR;
                                    toolCondition.Formula = cItem.HMTC_FK_MT_FORMULA + "|" + cItem.HMTC_EXPRESSION;
                                    toolCondition.Expression = cItem.HMTC_EXPRESSION;
                                    toolCondition.ReturnFlag = cItem.HMTC_RETURN_FLAG;
                                    toolCondition.PremiumFlag = cItem.HMTC_PREMIUM_FLAG;
                                    toolOption.ToolCondition.Add(toolCondition);
                                }

                                model.ToolOption.Add(toolOption);
                            }
                        }
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public ReturnValue Edit(HedgeToolViewModel_Detail HedgeToolViewDetailModel, string pUser)
        //{
        //    ReturnValue rtn = new ReturnValue();

        //    try
        //    {
        //        if (HedgeToolViewDetailModel == null)
        //        {
        //            rtn.Message = "Model is not null";
        //            rtn.Status = false;
        //            return rtn;
        //        }
        //        else if (string.IsNullOrEmpty(HedgeToolViewDetailModel.Name))
        //        {
        //            rtn.Message = "Tool Name should not be empty";
        //            rtn.Status = false;
        //            return rtn;
        //        }
        //        else if (string.IsNullOrEmpty(HedgeToolViewDetailModel.Status))
        //        {
        //            rtn.Message = "Status should not be empty";
        //            rtn.Status = false;
        //            return rtn;
        //        }
        //        else if (HedgeToolViewDetailModel.ToolSwitch == null)
        //        {
        //            rtn.Message = "Tool Switch type should not be empty";
        //            rtn.Status = false;
        //            return rtn;
        //        }
        //        else if (HedgeToolViewDetailModel.ToolOption == null)
        //        {
        //            rtn.Message = "Tool Option type should not be empty";
        //            rtn.Status = false;
        //            return rtn;
        //        }

        //        if (string.IsNullOrEmpty(pUser))
        //        {
        //            rtn.Message = "User should not be empty";
        //            rtn.Status = false;
        //            return rtn;
        //        }

        //        HEDG_MT_TOOL_DAL dal = new HEDG_MT_TOOL_DAL();
        //        HEDG_MT_TOOL ent = new HEDG_MT_TOOL();

        //        HEDG_MT_TOOL_SWITCH_DAL sDal = new HEDG_MT_TOOL_SWITCH_DAL();
        //        HEDG_MT_TOOL_SWITCH sEnt = new HEDG_MT_TOOL_SWITCH();

        //        HEDG_MT_TOOL_OPTION_DAL oDal = new HEDG_MT_TOOL_OPTION_DAL();
        //        HEDG_MT_TOOL_OPTION oEnt = new HEDG_MT_TOOL_OPTION();

        //        HEDG_MT_TOOL_CONDITION_DAL cDal = new HEDG_MT_TOOL_CONDITION_DAL();
        //        HEDG_MT_TOOL_CONDITION cEnt = new HEDG_MT_TOOL_CONDITION();

        //        DateTime now = DateTime.Now;
        //        using (var context = new EntityCPAIEngine())
        //        {
        //            using (var dbContextTransaction = context.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    ent.HMT_ROW_ID = HedgeToolViewDetailModel.HMT_ROW_ID;
        //                    ent.HMT_NAME = HedgeToolViewDetailModel.Name;
        //                    ent.HMT_DESC = HedgeToolViewDetailModel.Description;
        //                    ent.HMT_FEATURE_TARGET_FLAG = HedgeToolViewDetailModel.FeatureTargetFlag;
        //                    ent.HMT_FEATURE_SWAP_FLAG = HedgeToolViewDetailModel.FeatureSwapFlag;
        //                    ent.HMT_FEATURE_EXTEND_FLAG = HedgeToolViewDetailModel.FeatureExtendFlag;
        //                    ent.HMT_FEATURE_CAPPED_FLAG = HedgeToolViewDetailModel.FeatureCappedFlag;
        //                    ent.HMT_FEATURE_KNOCK_IN_OUT_FLAG = HedgeToolViewDetailModel.FeatureKnockInOutFlag;
        //                    ent.HMT_VERSION = (Convert.ToInt32(HedgeToolViewDetailModel.Version) + 1).ToString();
        //                    ent.HMT_REASON = HedgeToolViewDetailModel.Reason;
        //                    ent.HMT_STATUS = HedgeToolViewDetailModel.Status;
        //                    ent.HMT_UPDATED_BY = pUser;
        //                    ent.HMT_UPDATED_DATE = now;
        //                    dal.Update(ent, context);

        //                    sDal.DeleteAll(HedgeToolViewDetailModel.HMT_ROW_ID, context);
        //                    if (HedgeToolViewDetailModel.FeatureKnockInOutFlag == "Y")
        //                    {
        //                        var switchDataList = HedgeToolViewDetailModel.ToolSwitch;
        //                        foreach (var switchData in switchDataList)
        //                        {
        //                            sEnt = new HEDG_MT_TOOL_SWITCH();
        //                            string switchCtrlID = ConstantPrm.GetDynamicCtrlID();
        //                            sEnt.HMTS_ROW_ID = switchCtrlID;
        //                            sEnt.HMTS_FK_HEDG_MT_TOOL = HedgeToolViewDetailModel.HMT_ROW_ID;
        //                            sEnt.HMTS_ORDER = switchData.Order;
        //                            sEnt.HMTS_OPTION = switchData.Option;
        //                            sEnt.HMTS_CREATED_DATE = now;
        //                            sEnt.HMTS_CREATED_BY = pUser;
        //                            sEnt.HMTS_UPDATED_DATE = now;
        //                            sEnt.HMTS_UPDATED_BY = pUser;
        //                            sDal.Save(sEnt, context);
        //                        }
        //                    }

        //                    oDal.DeleteAll(HedgeToolViewDetailModel.HMT_ROW_ID, context);
        //                    var optionDataList = HedgeToolViewDetailModel.ToolOption;
        //                    foreach (var optionData in optionDataList)
        //                    {
        //                        oEnt = new HEDG_MT_TOOL_OPTION();
        //                        string optionCtrlID = ConstantPrm.GetDynamicCtrlID();
        //                        oEnt.HMTO_ROW_ID = optionCtrlID;
        //                        oEnt.HMTO_FK_HEDG_MT_TOOL = HedgeToolViewDetailModel.HMT_ROW_ID;
        //                        oEnt.HMTO_ORDER = optionData.Order;
        //                        oEnt.HMTO_OPTION = optionData.Option;
        //                        oEnt.HMTO_CREATED_DATE = now;
        //                        oEnt.HMTO_CREATED_BY = pUser;
        //                        oEnt.HMTO_UPDATED_DATE = now;
        //                        oEnt.HMTO_UPDATED_BY = pUser;
        //                        oDal.Save(oEnt, context);

        //                        var conditionDataList = optionData.ToolCondition;
        //                        foreach (var conditionData in conditionDataList)
        //                        {
        //                            cEnt = new HEDG_MT_TOOL_CONDITION();
        //                            string conditionCtrlID = ConstantPrm.GetDynamicCtrlID();
        //                            cEnt.HMTC_ROW_ID = conditionCtrlID;
        //                            cEnt.HMTC_FK_HEDG_TOOL_OPTION = optionCtrlID;
        //                            cEnt.HMTC_FK_MT_FORMULA = conditionData.Formula;
        //                            cEnt.HMTC_ORDER = conditionData.Order;
        //                            cEnt.HMTC_VALUE_A = conditionData.Value_A;
        //                            cEnt.HMTC_VALUE_B = conditionData.Value_B;
        //                            cEnt.HMTC_OPERATOR = conditionData.Operator;
        //                            cEnt.HMTC_EXPRESSION = conditionData.Expression;
        //                            cEnt.HMTC_RETURN_FLAG = conditionData.ReturnFlag;
        //                            cEnt.HMTC_PREMIUM_FLAG = conditionData.PremiumFlag;
        //                            cEnt.HMTC_CREATED_DATE = now;
        //                            cEnt.HMTC_CREATED_BY = pUser;
        //                            cEnt.HMTC_UPDATED_DATE = now;
        //                            cEnt.HMTC_UPDATED_BY = pUser;
        //                            cDal.Save(cEnt, context);
        //                        }
        //                    }

        //                    dbContextTransaction.Commit();
        //                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
        //                    rtn.Status = true;
        //                }
        //                catch (Exception ex)
        //                {
        //                    dbContextTransaction.Rollback();
        //                    rtn.Message = ex.Message;
        //                    rtn.Status = false;
        //                }
        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        rtn.Message = ex.Message;
        //        rtn.Status = false;
        //    }

        //    return rtn;
        //}

    }
}