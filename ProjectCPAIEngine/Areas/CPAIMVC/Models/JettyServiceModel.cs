﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class JettyServiceModel
    {
        public ReturnValue Add(JettyViewModel_Detail jettyViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                if (jettyViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.JettyID))
                {
                    rtn.Message = "Jetty ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.JettyName))
                {
                    rtn.Message = "Jetty Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Type))
                {
                    rtn.Message = "Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Country))
                {
                    rtn.Message = "Country should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Charge))
                {
                    rtn.Message = "Charge should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.CreateType))
                {
                    rtn.Message = "Create type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_JETTY_DAL jdal = new MT_JETTY_DAL();
                MT_JETTY jent = new MT_JETTY();

                MT_JETTY_CHARGE_DAL jcdal = new MT_JETTY_CHARGE_DAL();
                MT_JETTY_CHARGE jcent = new MT_JETTY_CHARGE();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            jent.MTJ_ROW_ID = jettyViewDetailModel.JettyID.Trim();
                            jent.MTJ_JETTY_NAME = jettyViewDetailModel.JettyName.Trim();
                            jent.MTJ_MT_COUNTRY = jettyViewDetailModel.Country.Trim();
                            jent.MTJ_CREATE_TYPE = jettyViewDetailModel.CreateType.Trim();
                            jent.MTJ_STATUS = jettyViewDetailModel.Status.Trim();
                            jent.MTJ_CREATED_BY = pUser;
                            jent.MTJ_CREATED_DATE = now;
                            jent.MTJ_UPDATED_BY = pUser;
                            jent.MTJ_UPDATED_DATE = now;

                            jdal.Save(jent, context);

                            jcent.MJC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                            jcent.MJC_FK_MT_JETTY = jettyViewDetailModel.JettyID.Trim();
                            jcent.MJC_FK_MT_VEHICLE = jettyViewDetailModel.Vehicle.Trim();
                            jcent.MJC_PORT_CHARGE = jettyViewDetailModel.Charge.Trim();
                            jcent.MJC_STATUS = jettyViewDetailModel.Status.Trim();
                            jcent.MJC_CREATE_TYPE = jettyViewDetailModel.CreateType.Trim();
                            jcent.MJC_CREATED_BY = pUser;
                            jcent.MJC_CREATED_DATE = now;
                            jcent.MJC_UPDATED_BY = pUser;
                            jcent.MJC_UPDATED_DATE = now;

                            jcdal.Save(jcent, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(JettyViewModel_Detail jettyViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (jettyViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.JettyID))
                {
                    rtn.Message = "Jetty ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.JettyName))
                {
                    rtn.Message = "Jetty Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Type))
                {
                    rtn.Message = "Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Country))
                {
                    rtn.Message = "Country should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Charge))
                {
                    rtn.Message = "Charge should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.Status))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(jettyViewDetailModel.CreateType))
                {
                    rtn.Message = "Create type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_JETTY_DAL jdal = new MT_JETTY_DAL();
                MT_JETTY jent = new MT_JETTY();

                MT_JETTY_CHARGE_DAL jcdal = new MT_JETTY_CHARGE_DAL();
                MT_JETTY_CHARGE jcent = new MT_JETTY_CHARGE();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            jent.MTJ_ROW_ID = jettyViewDetailModel.JettyID.Trim();
                            jent.MTJ_JETTY_NAME = jettyViewDetailModel.JettyName.Trim();
                            jent.MTJ_MT_COUNTRY = jettyViewDetailModel.Country.Trim();
                            jent.MTJ_CREATE_TYPE = jettyViewDetailModel.CreateType.Trim();
                            jent.MTJ_STATUS = jettyViewDetailModel.Status.Trim();
                            jent.MTJ_UPDATED_BY = pUser;
                            jent.MTJ_UPDATED_DATE = now;

                            jdal.Update(jent, context);

                            jcent.MJC_ROW_ID = jettyViewDetailModel.JettyChargeID.Trim();
                            jcent.MJC_FK_MT_JETTY = jettyViewDetailModel.JettyID.Trim();
                            jcent.MJC_FK_MT_VEHICLE = jettyViewDetailModel.Vehicle.Trim();
                            jcent.MJC_PORT_CHARGE = jettyViewDetailModel.Charge.Trim();
                            jcent.MJC_STATUS = jettyViewDetailModel.Status.Trim();
                            jcent.MJC_CREATE_TYPE = jettyViewDetailModel.CreateType.Trim();
                            jcent.MJC_UPDATED_BY = pUser;
                            jcent.MJC_UPDATED_DATE = now;

                            jcdal.Update(jcent, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref JettyViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sJettyID = String.IsNullOrEmpty(pModel.sJettyID) ? "" : pModel.sJettyID.ToUpper();
                    var sJettyName = String.IsNullOrEmpty(pModel.sJettyName) ? "" : pModel.sJettyName.ToLower();
                    var sType = String.IsNullOrEmpty(pModel.sType) ? "" : pModel.sType.ToUpper();
                    var sCountry = String.IsNullOrEmpty(pModel.sCountry) ? "" : pModel.sCountry.ToUpper();
                    var sLocation = String.IsNullOrEmpty(pModel.sLocation) ? "" : pModel.sLocation.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();
                    var sCreateType = String.IsNullOrEmpty(pModel.sCreateType) ? "" : pModel.sCreateType.ToUpper();

                    var query = (from v in context.MT_JETTY
                                 join vc in context.MT_JETTY_CHARGE on v.MTJ_ROW_ID equals vc.MJC_FK_MT_JETTY into vc2
                                 from vc in vc2.DefaultIfEmpty()
                                 join co in context.MT_COUNTRY on v.MTJ_MT_COUNTRY equals co.MCT_LAND1 into co2
                                 from co in co2.DefaultIfEmpty()
                                     //join ve in context.MT_VEHICLE on vc.MJC_FK_MT_VEHICLE equals ve.VEH_ID into ve2
                                     //from ve in ve2.DefaultIfEmpty()

                                 orderby new { v.MTJ_ROW_ID, vc.MJC_STATUS }
                                 select new JettyViewModel_SearchData
                                 {
                                     dJettyID = v.MTJ_ROW_ID,
                                     dJettyName = v.MTJ_JETTY_NAME,
                                     dType = v.MTJ_PORT_TYPE,
                                     dCountry = co.MCT_LANDX,
                                     dLocation = v.MTJ_LOCATION,
                                     dCharge = vc.MJC_PORT_CHARGE,
                                     dStatus = v.MTJ_STATUS,
                                     dCreateType = v.MTJ_CREATE_TYPE
                                 });

                    if (!string.IsNullOrEmpty(sJettyID))
                        query = query.Where(p => p.dJettyID.ToUpper().Contains(sJettyID.Trim()));

                    if (!string.IsNullOrEmpty(sJettyName))
                        query = query.Where(p => p.dJettyName.ToUpper().Contains(sJettyName.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sType))
                        query = query.Where(p => p.dType.ToUpper().Contains(sType.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sCountry))
                        query = query.Where(p => p.dCountry.ToUpper().Equals(sCountry.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sLocation))
                        query = query.Where(p => p.dLocation.ToUpper().Contains(sLocation.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Equals(sCreateType.ToUpper().Trim()));


                    if (query != null)
                    {
                        pModel.sSearchData = new List<JettyViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new JettyViewModel_SearchData
                                                    {
                                                        dJettyID = g.dJettyID,
                                                        dJettyName = g.dJettyName,
                                                        dCountry = g.dCountry,
                                                        dType = g.dType,
                                                        dCreateType = g.dCreateType == "L" ? "Load port" : "Discharge port",
                                                        dLocation = g.dLocation,
                                                        dCharge = g.dCharge,
                                                        dStatus = g.dStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public JettyViewModel_Detail Get(string pJettyID)
        {
            JettyViewModel_Detail model = new JettyViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pJettyID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_JETTY
                                     where v.MTJ_ROW_ID == pJettyID
                                     join vc in context.MT_JETTY_CHARGE on v.MTJ_ROW_ID equals vc.MJC_FK_MT_JETTY
                                     join co in context.MT_COUNTRY on v.MTJ_MT_COUNTRY equals co.MCT_LAND1
                                     orderby new { v.MTJ_ROW_ID }
                                     select new
                                     {
                                         dJettyID = v.MTJ_ROW_ID,
                                         dJettyName = v.MTJ_JETTY_NAME,
                                         dJettyChargeID = vc.MJC_ROW_ID,
                                         dCountry = v.MTJ_MT_COUNTRY,
                                         dVehicle = vc.MJC_FK_MT_VEHICLE,
                                         dType = v.MTJ_PORT_TYPE,
                                         dLocation = v.MTJ_LOCATION,
                                         dCharge = vc.MJC_PORT_CHARGE,
                                         dCreateType = v.MTJ_CREATE_TYPE,
                                         dStatus = v.MTJ_STATUS,

                                     });
                        if (query != null)
                        {
                            foreach (var item in query)
                            {
                                model.JettyID = string.IsNullOrEmpty(item.dJettyID) ? "" : item.dJettyID;
                                model.JettyChargeID = string.IsNullOrEmpty(item.dJettyChargeID) ? "" : item.dJettyChargeID;
                                model.JettyName = string.IsNullOrEmpty(item.dJettyName) ? "" : item.dJettyName;
                                model.Status = string.IsNullOrEmpty(item.dStatus) ? "" : item.dStatus.ToUpper();
                                model.Charge = string.IsNullOrEmpty(item.dCharge) ? "" : item.dCharge;
                                model.Country = string.IsNullOrEmpty(item.dCountry) ? "" : item.dCountry;
                                model.Vehicle = string.IsNullOrEmpty(item.dVehicle) ? "" : item.dVehicle;
                                model.CreateType = string.IsNullOrEmpty(item.dCreateType) ? "" : item.dCreateType;
                                model.Location = string.IsNullOrEmpty(item.dLocation) ? "" : item.dLocation;
                                model.Type = string.IsNullOrEmpty(item.dType) ? "" : item.dType;
                            }
                        }
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetJettyIDJSON(string pStatus = "ACTIVE")
        {
            string json = "";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_JETTY
                                 select v);

                    if (query != null)
                    {
                        var qJet = (from d in query
                                    orderby new { d.MTJ_ROW_ID }
                                    select new { System = d.MTJ_ROW_ID.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qJet.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

    }
}