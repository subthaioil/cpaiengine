﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class MaterialsServiceModel
    {
        public ReturnValue Edit(MaterialsViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MET_CREATE_TYPE))
                {
                    rtn.Message = "CREATE TYPE should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MET_NUM))
                {
                    rtn.Message = "NUM should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                }

                MT_MATERIALS_DAL dal = new MT_MATERIALS_DAL();
                MT_MATERIALS ent = new MT_MATERIALS();

                MT_MATERIALS_CONTROL_DAL dalCtr = new MT_MATERIALS_CONTROL_DAL();
                MT_MATERIALS_CONTROL entCtr = new MT_MATERIALS_CONTROL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            // CPAI
                            if (pModel.MET_CREATE_TYPE.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                            {
                                // Update data in MT_MATERIALS
                               
                            }


                            // Insert data to MT_MATERIALS_CONTROL
                            foreach (var item in pModel.Control)
                            {
                                if (string.IsNullOrEmpty(item.RowID))
                                {
                                    // Add
                                    entCtr = new MT_MATERIALS_CONTROL();
                                    entCtr.MMC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MMC_FK_MATRIALS = pModel.MET_NUM;
                                    entCtr.MMC_SYSTEM = String.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MMC_NAME = String.IsNullOrEmpty(item.Name) ? "" : item.Name.Trim().ToUpper();
                                    entCtr.MMC_UNIT_PRICE = String.IsNullOrEmpty(item.UnitPrice) ? "" : item.UnitPrice.Trim().ToUpper();
                                    //entCtr.MMC_UNIT_VOLUME = String.IsNullOrEmpty(item.UnitVolume) ? "" : item.UnitVolume.Trim().ToUpper();
                                    entCtr.MMC_STATUS = item.Status;
                                    entCtr.MMC_CREATED_BY = pUser;
                                    entCtr.MMC_CREATED_DATE = now;
                                    entCtr.MMC_UPDATED_BY = pUser;
                                    entCtr.MMC_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_MATERIALS_CONTROL();
                                    entCtr.MMC_ROW_ID = item.RowID;
                                    entCtr.MMC_FK_MATRIALS = pModel.MET_NUM;
                                    entCtr.MMC_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MMC_NAME = String.IsNullOrEmpty(item.Name) ? "" : item.Name.Trim().ToUpper();
                                    entCtr.MMC_UNIT_PRICE = String.IsNullOrEmpty(item.UnitPrice) ? "" : item.UnitPrice.Trim().ToUpper();
                                    //entCtr.MMC_UNIT_VOLUME = String.IsNullOrEmpty(item.UnitVolume) ? "" : item.UnitVolume.Trim().ToUpper();
                                    entCtr.MMC_STATUS = item.Status;
                                    entCtr.MMC_UPDATED_BY = pUser;
                                    entCtr.MMC_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref MaterialsViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sNUM = String.IsNullOrEmpty(pModel.sNUM) ? "" : pModel.sNUM.ToUpper();
                    var sDesEN = String.IsNullOrEmpty(pModel.sDesEN) ? "" : pModel.sDesEN.ToUpper();
                    var sDivision = String.IsNullOrEmpty(pModel.sDivision) ? "" : pModel.sDivision.ToUpper();
                    var sNamePerCre = String.IsNullOrEmpty(pModel.sNamePerCre) ? "" : pModel.sNamePerCre.ToUpper();
                    var sMatType = String.IsNullOrEmpty(pModel.sMatType) ? "" : pModel.sMatType.ToUpper();
                    var sMatGroup = String.IsNullOrEmpty(pModel.sMatGroup) ? "" : pModel.sMatGroup.ToUpper();
                    var sFlaMatDeletion = String.IsNullOrEmpty(pModel.sFlaMatDeletion) ? "" : pModel.sFlaMatDeletion.ToUpper();

                    var sCreateType = String.IsNullOrEmpty(pModel.sCreateType) ? "" : pModel.sCreateType.ToUpper();
                    var sSystem = String.IsNullOrEmpty(pModel.sSystem) ? "" : pModel.sSystem.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();

                    var sDateFrom = String.IsNullOrEmpty(pModel.sCreateDate) ? "" : pModel.sCreateDate.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.sCreateDate) ? "" : pModel.sCreateDate.ToUpper().Substring(14, 10);

                    var query = (from v in context.MT_MATERIALS
                                 join vc in context.MT_MATERIALS_CONTROL on v.MET_NUM equals vc.MMC_FK_MATRIALS into view
                                 from lvc in view.DefaultIfEmpty()
                                 orderby new { v.MET_NUM, lvc.MMC_SYSTEM }
                                 select new
                                 {
                                     dCreateDate = (DateTime?)lvc.MMC_CREATED_DATE
                                     ,
                                     dNUM = v.MET_NUM
                                     ,
                                     dDesEN = v.MET_MAT_DES_ENGLISH
                                     ,
                                     dDivision = v.MET_DIVISION
                                     ,
                                     dNamePerCre = v.MET_NAME_PER_CRE
                                     ,
                                     dMatType = v.MET_MAT_TYPE
                                     ,
                                     dMatGroup = v.MET_MAT_GROUP
                                     ,
                                     dFlaMatDeletion = v.MET_FLA_MAT_DELETION
                                     ,
                                     dCreateType = v.MET_CREATE_TYPE
                                     ,
                                     dSystem = lvc.MMC_SYSTEM
                                     ,
                                     dStatus = lvc.MMC_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sNUM))
                        query = query.Where(p => p.dNUM.ToUpper().Contains(sNUM.Trim()));

                    if (!string.IsNullOrEmpty(sDesEN))
                        query = query.Where(p => p.dDesEN.ToUpper().Contains(sDesEN.Trim()));

                    if (!string.IsNullOrEmpty(sDivision))
                        query = query.Where(p => p.dDivision.ToUpper().Contains(sDivision.Trim()));

                    if (!string.IsNullOrEmpty(sNamePerCre))
                        query = query.Where(p => p.dNamePerCre.ToUpper().Contains(sNamePerCre.Trim()));

                    if (!string.IsNullOrEmpty(sMatType))
                        query = query.Where(p => p.dMatType.ToUpper().Contains(sMatType.Trim()));

                    if (!string.IsNullOrEmpty(sMatGroup))
                        query = query.Where(p => p.dMatGroup.ToUpper().Contains(sMatGroup.Trim()));

                    if (!string.IsNullOrEmpty(sFlaMatDeletion))
                        query = query.Where(p => p.dFlaMatDeletion.ToUpper().Equals(sFlaMatDeletion.Trim()));


                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Contains(sCreateType));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dSystem.ToUpper().Contains(sSystem));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));

                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dCreateDate >= sDate && p.dCreateDate <= eDate);
                    }

                    if (query != null)
                    {
                        pModel.sSearchData = new List<MaterialsViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new MaterialsViewModel_SearchData
                                                    {
                                                        dCreateDate = g.dCreateDate == null ? "" : Convert.ToDateTime( g.dCreateDate).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dNUM = g.dNUM,
                                                        dDesEN = g.dDesEN,
                                                        dDivision = g.dDivision,
                                                        dNamePerCre = g.dNamePerCre,
                                                        dMatType = g.dMatType,
                                                        dMatGroup = g.dMatGroup,
                                                        dFlaMatDeletion = g.dFlaMatDeletion,

                                                        dCreateType = String.IsNullOrEmpty(g.dCreateType) == true ? "SAP" : g.dCreateType,
                                                        dSystem = g.dSystem,
                                                        dStatus = g.dStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public MaterialsViewModel_Detail Get(string pMET_NUM)
        {
            ShareFn _FN = new Utilities.ShareFn();
            MaterialsViewModel_Detail model = new MaterialsViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pMET_NUM) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_MATERIALS
                                     where v.MET_NUM.ToUpper().Equals(pMET_NUM.ToUpper())
                                     select v).FirstOrDefault();

                        if (query != null)
                        {
                            model.MET_NUM = query.MET_NUM;
                            model.MET_CREATE_TYPE = String.IsNullOrEmpty(query.MET_CREATE_TYPE) == true ? "SAP" : query.MET_CREATE_TYPE;

                            // Require
                            
                            model.MET_DIVISION = query.MET_DIVISION;
                            model.MET_NAME_PER_CRE = query.MET_NAME_PER_CRE;
                            model.MET_MAT_TYPE = query.MET_MAT_TYPE;
                            model.MET_MAT_GROUP = query.MET_MAT_GROUP;
                            // Show
                            model.MET_OLD_MAT_NUM = query.MET_OLD_MAT_NUM;
                            model.MET_BASE_MEA = query.MET_BASE_MEA;
                            model.MET_MAT_DES_THAI = query.MET_MAT_DES_THAI;
                            model.MET_MAT_DES_ENGLISH = query.MET_MAT_DES_ENGLISH;
                            model.MET_CRUDE_TYPE = query.MET_CRUDE_TYPE;
                            model.MET_CRUDE_SRC = query.MET_CRUDE_SRC;
                            model.MET_LOAD_PORT = query.MET_LOAD_PORT;
                            model.MET_ORIG_CRTY = query.MET_ORIG_CRTY;
                            // Optional
                            model.MET_CRE_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.MET_CRE_DATE, "dd/MM/yyyy");
                            model.MET_DATE_LAST = ShareFn.ConvertDateTimeToDateStringFormat(query.MET_DATE_LAST, "dd/MM/yyyy"); 
                            model.MET_LAB_DESIGN_OFFICE = query.MET_LAB_DESIGN_OFFICE;
                            model.MET_FLA_MAT_DELETION = query.MET_FLA_MAT_DELETION;
                            model.MET_GRP_CODE = query.MET_GRP_CODE;
                            model.MET_PRD_TYPE = query.MET_PRD_TYPE;
                            model.MET_BL_SRC = query.MET_BL_SRC;
                            model.MET_FK_PRD = query.MET_FK_PRD;
                            model.MET_DEN_CONV = query.MET_DEN_CONV;
                            model.MET_VFC_CONV = query.MET_VFC_CONV;
                            model.MET_IO = query.MET_IO;
                            model.MET_MAT_ADD = query.MET_MAT_ADD;
                            model.MET_LORRY = query.MET_LORRY;
                            model.MET_SHIP = query.MET_SHIP;
                            model.MET_THAPP = query.MET_THAPP;
                            model.MET_PIPE = query.MET_PIPE;
                            model.MET_MAT_GROP_TYPE = query.MET_MAT_GROP_TYPE;
                            model.MET_ZONE = query.MET_ZONE;
                            model.MET_PORT = query.MET_PORT;
                            model.MET_IO_TLB = query.MET_IO_TLB;
                            model.MET_IO_TPX = query.MET_IO_TPX;
                            model.MET_DOEB_CODE = query.MET_DOEB_CODE;
                            model.MET_IO_LABIX = query.MET_IO_LABIX;
                            model.MET_EXCISE_CODE = query.MET_EXCISE_CODE;
                            model.MET_EXCISE_UOM = query.MET_EXCISE_UOM;
                            model.MET_PRODUCT_DENSITY = query.MET_PRODUCT_DENSITY;
                            model.MET_GL_ACCOUNT = query.MET_GL_ACCOUNT;

                            // Not show
                            model.MET_CREATED_BY = query.MET_CREATED_BY;
                            model.MET_CREATED_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.MET_CREATED_DATE, "dd/MM/yyyy");
                            model.MET_UPDATED_BY = query.MET_UPDATED_BY;
                            model.MET_UPDATED_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.MET_UPDATED_DATE, "dd/MM/yyyy");
                            
                            var queryControl = (from v in context.MT_MATERIALS_CONTROL
                                                where v.MMC_FK_MATRIALS.ToUpper().Equals(pMET_NUM.ToUpper())
                                                select v);
                            model.Control = new List<MaterialsViewModel_Control>();
                            foreach (var g in queryControl)
                            {
                                model.Control.Add(new MaterialsViewModel_Control { RowID = g.MMC_ROW_ID, System = g.MMC_SYSTEM, Name = g.MMC_NAME, UnitPrice = g.MMC_UNIT_PRICE, UnitVolume = "", Status = g.MMC_STATUS });
                            }
                            if(model.Control.Count == 0)
                                model.Control.Add(new MaterialsViewModel_Control { RowID = "", System = "", Name = "", UnitPrice = "", UnitVolume = "", Status = CPAIConstantUtil.ACTIVE });
                        }
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetName(string pMET_NUM)
        {

            try
            {
                if (String.IsNullOrEmpty(pMET_NUM) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_MATERIALS
                                     where v.MET_NUM.ToUpper().Equals(pMET_NUM.ToUpper())
                                     select v.MET_MAT_DES_ENGLISH).FirstOrDefault();

                        return query == null ? "" : query;

                    }

                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetDivisionJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_MATERIALS
                                 select v);


                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MET_DIVISION }
                                    select new { value = d.MET_DIVISION.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetMatTypeJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_MATERIALS
                                 select v);


                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MET_MAT_TYPE }
                                    select new { value = d.MET_MAT_TYPE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetFlaMatDeletionJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_MATERIALS
                                 select v);


                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MET_FLA_MAT_DELETION }
                                    select new { value = d.MET_FLA_MAT_DELETION.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.value));
                        json = json.Replace("null", "\"\"");
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetCtrlSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_MATERIALS_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MMC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MMC_SYSTEM }
                                    select new { value = d.MMC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> GetCtrlSystemForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_MATERIALS_CONTROL select v);


                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MMC_SYSTEM }
                                    select new { Value = d.MMC_SYSTEM.ToUpper(), Text = d.MMC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static string getMaterialsDDLJson(string System)
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_MATERIALS
                             join vc in context.MT_MATERIALS_CONTROL on v.MET_NUM equals vc.MMC_FK_MATRIALS into view
                             from lvc in view.DefaultIfEmpty()
                             where lvc.MMC_SYSTEM.ToUpper() == System.ToUpper()
                                && lvc.MMC_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                             orderby new { v.MET_NUM, lvc.MMC_SYSTEM }
                             select new
                             {
                                 dCreateDate = (DateTime?)lvc.MMC_CREATED_DATE
                                 ,
                                 dNUM = v.MET_NUM
                                 ,
                                 dDesEN = v.MET_MAT_DES_ENGLISH
                                 ,
                                 dDivision = v.MET_DIVISION
                                 ,
                                 dNamePerCre = v.MET_NAME_PER_CRE
                                 ,
                                 dMatType = v.MET_MAT_TYPE
                                 ,
                                 dMatGroup = v.MET_MAT_GROUP
                                 ,
                                 dFlaMatDeletion = v.MET_FLA_MAT_DELETION
                                 ,
                                 dCreateType = v.MET_CREATE_TYPE
                                 ,
                                 dSystem = lvc.MMC_SYSTEM
                                 ,
                                 dStatus = lvc.MMC_STATUS
                             });

                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.dNUM, text = p.dDesEN }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }

        public static string getMaterialsDDLCoolJson()
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS
                             where d.MET_CREATE_TYPE == "COOL" && d.MET_MAT_DES_ENGLISH != null
                             orderby new { d.MET_MAT_DES_ENGLISH }
                             select new { mKey = d.MET_MAT_DES_ENGLISH, mValue = d.MET_MAT_DES_ENGLISH }).ToList();

                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.mKey, text = p.mValue }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }

        public static string getMaterialsCountryDDLCoolJson()
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_MATERIALS
                            where v.MET_ORIG_CRTY != null
                            select v;

                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.MET_ORIG_CRTY, text = p.MET_ORIG_CRTY }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }

        public static List<SelectListItem> getMaterialDDL()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS_CONTROL
                             where d.MMC_STATUS == "ACTIVE" && !string.IsNullOrEmpty(d.MMC_NAME)
                             orderby new { d.MMC_NAME }
                             select new { mKey = d.MMC_FK_MATRIALS, mValue = d.MMC_NAME }).Distinct().ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.mKey, Text = item.mValue });
                    }
                }
            }
            return rtn;
        }

        public static List<SelectListItem> getMaterialsDDL()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS
                             where d.MET_CREATE_TYPE == "COOL" && d.MET_MAT_DES_ENGLISH != null
                             orderby new {d.MET_MAT_DES_ENGLISH}
                             select new { mKey = d.MET_NUM, mValue = d.MET_MAT_DES_ENGLISH }).ToList();

                if (query != null)
                {
                    foreach (var item in query.Distinct())
                    {
                        rtn.Add(new SelectListItem { Value = item.mKey, Text = item.mValue });
                    }
                }
            }
            return rtn;
        }

        public static List<SelectListItem> getMaterialsDDLCool()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS
                             where d.MET_CREATE_TYPE == "COOL" && d.MET_MAT_DES_ENGLISH != null
                             orderby new { d.MET_MAT_DES_ENGLISH }
                             select new { mKey = d.MET_MAT_DES_ENGLISH, mValue = d.MET_MAT_DES_ENGLISH }).ToList();

                if (query != null)
                {
                    foreach (var item in query.Distinct())
                    {
                        rtn.Add(new SelectListItem { Value = item.mKey, Text = item.mValue.ToUpper() });
                    }
                }
            }
            return rtn;
        }

        public static List<SelectListItem> getMaterialsDDLVCool()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_MATERIALS
                             join c in context.CDP_DATA on d.MET_NUM equals c.CDA_FK_MATERIALS
                             where d.MET_CREATE_TYPE == "COOL" && d.MET_MAT_DES_ENGLISH != null
                             orderby new { d.MET_MAT_DES_ENGLISH }
                             select new { mKey = d.MET_NUM, mValue = d.MET_MAT_DES_ENGLISH }).ToList();

                if (query != null)
                {
                    foreach (var item in query.Distinct())
                    {
                        rtn.Add(new SelectListItem { Value = item.mKey, Text = item.mValue.ToUpper() });
                    }
                }
            }
            return rtn;
        }

    }
}