﻿using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class MenuServiceModel
    {
        public ReturnValue Add(MenuViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }

                //else if (!string.IsNullOrEmpty(pModel.MEU_LEVEL))
                //{
                //    int iLevel = Convert.ToInt32(pModel.MEU_LEVEL);
                //    if (iLevel>3)
                //    {
                //        rtn.Message = "Cannot add Sub Menu more than 3 Levels";
                //        rtn.Status = false;
                //        return rtn;
                //    }
                //}
                //else if (string.IsNullOrEmpty(pModel.MEU_GROUP_MENU))
                //{
                //    rtn.Message = "Group Menu should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                else if (string.IsNullOrEmpty(pModel.MEU_PARENT_ID))
                {
                    rtn.Message = "Parent ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.LNG_DESCRIPTION))
                {
                    rtn.Message = "Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (string.IsNullOrEmpty(pModel.MEU_LEVEL))
                //{
                //    rtn.Message = "Level should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                else if (string.IsNullOrEmpty(pModel.MEU_CONTROL_TYPE))
                {
                    rtn.Message = "Control Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.MEU_CONTROL_TYPE == "FIELD" && GetControlTypeMenu(pModel.MEU_PARENT_ID) != "MENU")
                {

                    rtn.Message = "Control Type FIELD should be under MENU";
                    rtn.Status = false;
                    return rtn;

                }
                else if (pModel.MEU_CONTROL_TYPE == "MENU" && GetControlTypeMenu(pModel.MEU_PARENT_ID) == "FIELD")
                {

                    rtn.Message = "Control Type MENU should not be under FIELD";
                    rtn.Status = false;
                    return rtn;

                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MENU_DAL menuDAL = new MENU_DAL();
                MENU menuDetail = new MENU();


                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var CtrlID = ConstantPrm.GetDynamicCtrlID();
                            menuDetail.MEU_ROW_ID = CtrlID;
                            if (pModel.MEU_PARENT_ID.Equals("#"))
                            {
                                menuDetail.MEU_GROUP_MENU = pModel.LNG_DESCRIPTION;
                                menuDetail.MEU_LEVEL = "1";
                            }
                            else
                            {
                                menuDetail.MEU_GROUP_MENU = GetGroupMenu(pModel.MEU_PARENT_ID);
                                menuDetail.MEU_LEVEL = GetLevelMenu(pModel.MEU_PARENT_ID, pModel.MEU_CONTROL_TYPE);
                            }
                            //menuDetail.MEU_GROUP_MENU = pModel.MEU_GROUP_MENU;
                            menuDetail.MEU_PARENT_ID = pModel.MEU_PARENT_ID;
                            menuDetail.LNG_DESCRIPTION = pModel.LNG_DESCRIPTION;
                            menuDetail.MEU_URL = pModel.MEU_URL;
                            menuDetail.MEU_IMG = pModel.MEU_IMG;

                            menuDetail.MEU_LIST_NO = GetMaxListNoMenu(pModel.MEU_PARENT_ID);
                            menuDetail.MEU_CONTROL_TYPE = pModel.MEU_CONTROL_TYPE;
                            menuDetail.MEU_ACTIVE = pModel.MEU_ACTIVE;
                            menuDetail.MEU_URL_DIRECT = pModel.MEU_URL_DIRECT;
                            menuDetail.MEU_CREATED_BY = pUser;
                            menuDetail.MEU_CREATED_DATE = now;
                            menuDetail.MEU_UPDATED_BY = pUser;
                            menuDetail.MEU_UPDATED_DATE = now;
                            menuDAL.Save(menuDetail, context);


                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(MenuViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (string.IsNullOrEmpty(pModel.MEU_GROUP_MENU))
                //{
                //    rtn.Message = "Group Menu should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                else if (string.IsNullOrEmpty(pModel.MEU_PARENT_ID))
                {
                    rtn.Message = "Parent ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.LNG_DESCRIPTION))
                {
                    rtn.Message = "Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.MEU_PARENT_ID_CURRENT))
                {
                    rtn.Message = "Parent ID CURRENT should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (string.IsNullOrEmpty(pModel.MEU_LEVEL))
                //{
                //    rtn.Message = "Level should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                else if (string.IsNullOrEmpty(pModel.MEU_CONTROL_TYPE))
                {
                    rtn.Message = "Control Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.MEU_CONTROL_TYPE == "FIELD" && GetControlTypeMenu(pModel.MEU_PARENT_ID) != "MENU")
                {

                    rtn.Message = "Control Type FIELD should be under MENU";
                    rtn.Status = false;
                    return rtn;

                }
                else if (pModel.MEU_CONTROL_TYPE == "MENU" && GetControlTypeMenu(pModel.MEU_PARENT_ID) == "FIELD")
                {

                    rtn.Message = "Control Type MENU should not be under FIELD";
                    rtn.Status = false;
                    return rtn;

                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MENU_DAL menuDAL = new MENU_DAL();
                MENU menuListDetail = new MENU();

                MENU menuDetail = new MENU();

                DateTime now = DateTime.Now;


                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            menuDetail = new MENU();

                            menuDetail.MEU_ROW_ID = pModel.MEU_ROW_ID;
                            if (pModel.MEU_PARENT_ID.Equals("#"))
                            {
                                menuDetail.MEU_GROUP_MENU = pModel.LNG_DESCRIPTION;
                                menuDetail.MEU_LEVEL = "1";
                            }
                            else
                            {
                                menuDetail.MEU_GROUP_MENU = GetGroupMenu(pModel.MEU_PARENT_ID);
                                menuDetail.MEU_LEVEL = GetLevelMenu(pModel.MEU_PARENT_ID, pModel.MEU_CONTROL_TYPE);
                            }

                            menuDetail.MEU_PARENT_ID = pModel.MEU_PARENT_ID;
                            menuDetail.LNG_DESCRIPTION = pModel.LNG_DESCRIPTION;
                            menuDetail.MEU_URL = pModel.MEU_URL;
                            menuDetail.MEU_IMG = pModel.MEU_IMG;
                            if (pModel.MEU_LIST_NO != null)
                            {
                                menuDetail.MEU_LIST_NO = pModel.MEU_LIST_NO + "+";
                            }
                            else
                            {
                                menuDetail.MEU_LIST_NO = GetMaxListNoMenu(pModel.MEU_PARENT_ID);    // Move to end
                            }

                            menuDetail.MEU_CONTROL_TYPE = pModel.MEU_CONTROL_TYPE;
                            menuDetail.MEU_ACTIVE = pModel.MEU_ACTIVE;
                            menuDetail.MEU_URL_DIRECT = pModel.MEU_URL_DIRECT;
                            menuDetail.MEU_UPDATED_BY = pUser;
                            menuDetail.MEU_UPDATED_DATE = now;
                            menuDAL.Update(menuDetail, context);

                            // Update List no
                            if (pModel.MEU_LIST_NO != null)
                            {
                                var allListMenuParrentSelect = (from m in context.MENU
                                                                where m.MEU_PARENT_ID.Equals(pModel.MEU_PARENT_ID)
                                                                select m).ToList();

                                int i = allListMenuParrentSelect.Count;
                                int flag = 0;
                                int ListNo = 0;
                                foreach (var item in allListMenuParrentSelect.OrderByDescending(m => m.MEU_LIST_NO))
                                {
                                   

                                    if (item.MEU_LIST_NO.Contains("+"))
                                    {
                                        flag = 1;
                                        menuDAL.UpdateListNo(item.MEU_ROW_ID, pModel.MEU_LIST_NO, pUser, now, context);
                                    }
                                    else
                                    {
                                       
                                        ListNo = int.Parse(item.MEU_LIST_NO) + 1;
                                        menuDAL.UpdateListNo(item.MEU_ROW_ID, ListNo.ToString(), pUser, now, context);

                                        if (flag == 1)
                                            break;
                                    }
                                    i--;
                                }
                            }


                            #region Update ListNo of Current

                            var allListMenuParrentCurrent = (from m in context.MENU
                                                             where m.MEU_PARENT_ID.Equals(pModel.MEU_PARENT_ID_CURRENT)
                                                             select m).ToList();

                            
                            if (allListMenuParrentCurrent != null)
                            {
                                int i = 1;
                                foreach (var item in allListMenuParrentCurrent.OrderBy(m => int.Parse(m.MEU_LIST_NO)))
                                {
                                    menuDAL.UpdateListNo(item.MEU_ROW_ID, i.ToString(), pUser, now, context);
                                    i++;
                                }
                            }

                            #endregion

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref MenuViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sMenuID = pModel.dMenuRowID == null ? "" : pModel.dMenuRowID.ToUpper();
                    var sMenuGroupID = pModel.dMenuGroupMenu == null ? "" : pModel.dMenuGroupMenu.ToUpper();
                    var sMenuParentID = pModel.dMenuParentID == null ? "" : pModel.dMenuParentID.ToUpper();
                    var sDescription = pModel.dDescription == null ? "" : pModel.dDescription.ToUpper();
                    var sMenuURL = pModel.dMenuURL == null ? "" : pModel.dMenuURL.ToUpper();
                    var sMenuIMG = pModel.dMenuIMG == null ? "" : pModel.dMenuIMG.ToUpper();
                    var sMenuLevel = pModel.dMenuLevel == null ? "" : pModel.dMenuLevel.ToUpper();
                    var sMenuListNo = pModel.dMenuListNo == null ? "" : pModel.dMenuListNo.ToUpper();
                    var sMenuControlType = pModel.dMenuControlType == null ? "" : pModel.dMenuControlType.ToUpper();

                    var query = (from v in context.MENU
                                 select new MenuViewModel_SearchData
                                 {
                                     dMenuRowID = v.MEU_ROW_ID,
                                     dMenuGroupMenu = v.MEU_GROUP_MENU,
                                     dMenuParentID = v.MEU_PARENT_ID,
                                     dDescription = v.LNG_DESCRIPTION,
                                     dMenuURL = v.MEU_URL,
                                     dMenuIMG = v.MEU_IMG,
                                     dMenuLevel = v.MEU_LEVEL,
                                     dMenuListNo = v.MEU_LIST_NO,
                                     dMenuControlType = v.MEU_CONTROL_TYPE,
                                 });

                    if (!string.IsNullOrEmpty(sMenuID))
                        query = query.Where(p => p.dMenuRowID.ToUpper().Contains(sMenuID));

                    if (!string.IsNullOrEmpty(sMenuGroupID))
                        query = query.Where(p => p.dMenuGroupMenu.ToLower().Contains(sMenuGroupID));

                    if (!string.IsNullOrEmpty(sMenuParentID))
                        query = query.Where(p => p.dMenuParentID.ToUpper().Contains(sMenuParentID));

                    if (!string.IsNullOrEmpty(sDescription))
                        query = query.Where(p => p.dDescription.ToUpper().Contains(sDescription));

                    if (!string.IsNullOrEmpty(sMenuURL))
                        query = query.Where(p => p.dMenuURL.ToUpper().Contains(sMenuURL));

                    if (!string.IsNullOrEmpty(sMenuIMG))
                        query = query.Where(p => p.dMenuIMG.ToUpper().Contains(sMenuIMG));

                    if (!string.IsNullOrEmpty(sMenuLevel))
                        query = query.Where(p => p.dMenuLevel.ToUpper().Contains(sMenuLevel));

                    if (!string.IsNullOrEmpty(sMenuListNo))
                        query = query.Where(p => p.dMenuListNo.ToUpper().Contains(sMenuListNo));

                    if (!string.IsNullOrEmpty(sMenuControlType))
                        query = query.Where(p => p.dMenuControlType.ToUpper().Contains(sMenuControlType));


                    if (query != null)
                    {
                        pModel.sSearchData = query.ToList();
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public MenuViewModel_Detail Get(string pMenuID)
        {
            MenuViewModel_Detail model = new MenuViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pMenuID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MENU
                                     where v.MEU_ROW_ID.Equals(pMenuID)

                                     select new
                                     {
                                         dMenuRowID = v.MEU_ROW_ID,
                                         dMenuGroupMenu = v.MEU_GROUP_MENU,
                                         dMenuParentID = v.MEU_PARENT_ID,
                                         dDescription = v.LNG_DESCRIPTION,
                                         dMenuURL = v.MEU_URL,
                                         dMenuIMG = v.MEU_IMG,
                                         dMenuLevel = v.MEU_LEVEL,
                                         dMenuListNo = v.MEU_LIST_NO,
                                         dMenuControlType = v.MEU_CONTROL_TYPE,
                                         dActive = v.MEU_ACTIVE,
                                         dUrlDirect = v.MEU_URL_DIRECT
                                     });

                        if (query != null)
                        {
                            model.Control = new List<MenuViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                if (i == 1)
                                {
                                    model.MEU_ROW_ID = g.dMenuRowID;
                                    model.MEU_GROUP_MENU = g.dMenuGroupMenu;
                                    model.MEU_PARENT_ID = g.dMenuParentID;
                                    model.LNG_DESCRIPTION = g.dDescription;
                                    model.MEU_URL = g.dMenuURL;
                                    model.MEU_IMG = g.dMenuIMG;
                                    model.MEU_LEVEL = g.dMenuLevel;
                                    model.MEU_LIST_NO = g.dMenuListNo;
                                    model.MEU_CONTROL_TYPE = g.dMenuControlType;
                                    model.MEU_ACTIVE = g.dActive;
                                    model.MEU_URL_DIRECT = g.dUrlDirect;

                                    model.MEU_PARENT_ID_CURRENT = g.dMenuParentID;
                                    model.MEU_PARENT_DETAIL_CURRENT = g.dDescription;

                                    //model.Menu_ParentDetail = GetParentDetail(g.dMenuParentID);
                                }

                                //if (string.IsNullOrEmpty(g.dMenuRowID) == false)
                                //{

                                //    model.Control.Add(new MenuViewModel_Control { RowID = g.dMenuRowID, Action = g.dActive, GroupMenu = g.dMenuGroupMenu, ParentID = g.dMenuParentID, Description = g.dDescription, URL = g.dMenuURL, Img = g.dMenuIMG, Level = g.dMenuLevel, ListNo = g.dMenuListNo, ControlType = g.dMenuControlType, Active = g.dActive, UrlDirect = g.dUrlDirect });

                                //    i++;

                                //    //List<MenuDetail.MenuRootObject> menuRoot = new List<MenuDetail.MenuRootObject>();
                                //    //menuRoot.Add(new MenuDetail.MenuRootObject() { menu_id = g.dMenuRowID,  group_menu = g.dMenuGroupMenu, parent_id = g.dMenuParentID, lng_description = g.dDescription, menu_url = g.dMenuURL, menu_img = g.dMenuIMG, menu_level = g.dMenuLevel, menu_list_no = g.dMenuListNo, menu_control_type = g.dMenuControlType, menu_active = g.dActive, menu_url_direct = g.dUrlDirect });
                                //}
                            }
                        }
                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetAllMenu()
        {
            string json = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    List<MENU> menuList = new List<MENU>();

                    var allMenu = (from m in context.MENU
                                   select m).ToList();

                    List<MenuDetail.MenuRootObject> menuRoot = new List<MenuDetail.MenuRootObject>();

                    if (allMenu != null)
                    {
                        var mainMenu = allMenu.Where(item => int.Parse(item.MEU_LEVEL).Equals(1)).OrderBy(x => int.Parse(x.MEU_LIST_NO));

                        if (mainMenu != null)
                        {
                            int iRunningNo = 1;
                            string strRunningNo = "";
                            foreach (var item in mainMenu)
                            {

                                MenuDetail.MenuRootObject nDetail = new MenuDetail.MenuRootObject();

                                strRunningNo = iRunningNo.ToString();

                                nDetail.menu_id = item.MEU_ROW_ID;
                                nDetail.text = strRunningNo + ". " + item.LNG_DESCRIPTION;
                                nDetail.tags = new List<string> { item.MEU_CONTROL_TYPE == "MENU" ? "M" : "F", item.MEU_ACTIVE };
                                nDetail.backColor = item.MEU_CONTROL_TYPE == "MENU" ? "" : "#E0E9F8";
                                //nDetail.icon = item.MEU_CONTROL_TYPE == "MENU" ? "" : "glyphicon glyphicon-flash";

                                nDetail.nodes = GetSubMenu(allMenu, item.MEU_ROW_ID, strRunningNo);
                                menuRoot.Add(nDetail);

                                //menuRoot.Add(new MenuDetail.MenuRootObject() { menu_id = item.MEU_ROW_ID, text = RunningNo.ToString() + ". " + item.LNG_DESCRIPTION, nodes = lstDetail, tags = new List<string> {  item.MEU_CONTROL_TYPE == "MENU" ? "M" : "F", item.MEU_ACTIVE }, backColor = item.MEU_CONTROL_TYPE == "MENU" ? "" : "#E0E9F8" });
                                //
                                iRunningNo++;
                            }
                        }
                    }
                    json = JSonConvertUtil.modelToJson(menuRoot);
                }
                return json.Replace("[]", "null");
            }
            catch (Exception ex)
            {
                return json;
            }
        }

        public static List<MenuDetail.Node> GetSubMenu(List<MENU> menuLst, string MeuRowID, string RunningNo)
        {
            List<MenuDetail.Node> listDetail = new List<MenuDetail.Node>();
            MenuDetail.Node nDetail = new MenuDetail.Node();
            try
            {

                var query = (from m in menuLst
                             where m.MEU_PARENT_ID.Equals(MeuRowID)
                             select m).ToList();

                if (query != null)
                {
                    int i = 1;
                    int iCountAllSub = query.Count;
                    if (iCountAllSub == 0)
                        return null;

                    string strCo = string.Empty;

                    foreach (var iSub in query.OrderBy(s => int.Parse(s.MEU_LIST_NO)))
                    {
                        strCo = RunningNo + "." + i.ToString();

                        nDetail = new MenuDetail.Node();
                        nDetail.menu_id = iSub.MEU_ROW_ID;
                        nDetail.text = strCo + " " + iSub.LNG_DESCRIPTION;
                        nDetail.tags = new List<string> { iSub.MEU_CONTROL_TYPE == "MENU" ? "M" : "F", iSub.MEU_ACTIVE };
                        nDetail.backColor = iSub.MEU_CONTROL_TYPE == "MENU" ? "" : "#E0E9F8";
                        //nDetail.icon = iSub.MEU_CONTROL_TYPE == "MENU" ? "" : "glyphicon glyphicon-flash";
                        nDetail.nodes = GetSubMenu(menuLst, nDetail.menu_id, strCo);
                        listDetail.Add(nDetail);
                        i++;
                    }
                }
                else
                {
                    listDetail = null;
                }


                return listDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //public static string GetAllMenu()
        //{
        //    string json = "";
        //    try
        //    {
        //        using (EntityCPAIEngine context = new EntityCPAIEngine())
        //        {
        //            List<MENU> menuList = new List<MENU>();

        //            var allMenu = (from m in context.MENU
        //                           select m).ToList();

        //            List<MenuDetail.MenuRootObject> menuRoot = new List<MenuDetail.MenuRootObject>();

        //            if (allMenu != null)
        //            {
        //                var mainMenu = allMenu.Where(item => item.MEU_PARENT_ID.Equals("#")).OrderBy(x => int.Parse(x.MEU_LIST_NO));

        //                if (mainMenu != null)
        //                {
        //                    //int iMax = 1;
        //                    int i = 1;
        //                    foreach (var item in mainMenu)
        //                    {

        //                        MenuDetail.Node nDetail = new MenuDetail.Node();
        //                        List<MenuDetail.Node> lstDetail = new List<MenuDetail.Node>();
        //                        int refCountAllSub = 0;

        //                        lstDetail = GetSubMenu(allMenu, item.MEU_ROW_ID, item.MEU_ROW_ID,i,ref refCountAllSub);

        //                        //var index = menuRoot.FindIndex(a => a.text == item.LNG_DESCRIPTION);
        //                        //menuRoot[index].nodes.Add(lstDetail);


        //                        menuRoot.Add(new MenuDetail.MenuRootObject() { menu_id = item.MEU_ROW_ID, text = i.ToString() + ". " + item.LNG_DESCRIPTION, nodes = lstDetail, tags= new List<string> { item.MEU_ACTIVE }  });
        //                        i++;
        //                    }
        //                }
        //            }
        //            json = JSonConvertUtil.modelToJson(menuRoot);
        //        }
        //        return json.Replace("[]","null");
        //    }
        //    catch (Exception ex)
        //    {
        //        return json;
        //    }
        //}

        //public static List<MenuDetail.Node> GetSubMenu(List<MENU> menuLst, string rowID, string parentID,int Count,ref int CountAllSub)
        //{
        //    string json = "";
        //    List<MenuDetail.Node> listDetail = new List<MenuDetail.Node>();
        //    MenuDetail.Node nDetail = new MenuDetail.Node();

        //    try
        //    {
        //        using (EntityCPAIEngine context = new EntityCPAIEngine())
        //        {
        //            //int iCountAllSub = 0;
        //            var query = (from m in menuLst
        //                         where m.MEU_PARENT_ID.Equals(parentID)
        //                         select m).ToList();

        //            if (query != null)
        //            {
        //                int i = 1;

        //                string strCo = string.Empty;
        //                foreach (var iSub in query.OrderBy(s => s.MEU_LIST_NO))
        //                {
        //                    CountAllSub = query.Count;
        //                    strCo = Count.ToString() + "." + i.ToString() + " ";
        //                    nDetail = new MenuDetail.Node();
        //                    nDetail.menu_id = iSub.MEU_ROW_ID;
        //                    //nDetail.group_menu = iSub.MEU_GROUP_MENU;
        //                    //nDetail.parent_id = iSub.MEU_PARENT_ID;
        //                    //nDetail.lng_description = iSub.LNG_DESCRIPTION;
        //                    //nDetail.menu_url = iSub.MEU_URL;
        //                    //nDetail.menu_img = iSub.MEU_IMG;
        //                    //nDetail.menu_level = iSub.MEU_LEVEL;
        //                    //nDetail.menu_list_no = iSub.MEU_LIST_NO;
        //                    //nDetail.menu_control_type = iSub.MEU_CONTROL_TYPE;
        //                    //nDetail.menu_action = iSub.MEU_ACTIVE;
        //                    //nDetail.menu_url_direct = iSub.MEU_URL_DIRECT;
        //                    nDetail.text = strCo + iSub.LNG_DESCRIPTION;
        //                    //nDetail.parent_detail = GetParentDetail(iSub.MEU_PARENT_ID);
        //                    ////nDetail.nodes = new List<MenuDetail.Node2>();
        //                    nDetail.tags = new List<string> { iSub.MEU_ACTIVE };

        //                    listDetail.Add(nDetail);

        //                    var querySub = (from m in menuLst
        //                                    where m.MEU_PARENT_ID.Equals(iSub.MEU_ROW_ID)
        //                                    select m).ToList();
        //                    i++;
        //                    if (querySub != null)
        //                    {
        //                        int iCountSub = 0;
        //                        int i2 = 1;
        //                        string strCo2 = string.Empty;
        //                        nDetail.nodes = new List<MenuDetail.Node>();

        //                        foreach (var iSub2 in querySub.OrderBy(s => s.MEU_LIST_NO))
        //                        {
        //                            strCo2 = strCo.Replace(" ", "") + "." + i2.ToString() + " ";
        //                            MenuDetail.Node nSubDetail = new MenuDetail.Node();
        //                            nSubDetail.menu_id = iSub2.MEU_ROW_ID;
        //                            //nSubDetail.group_menu = iSub2.MEU_GROUP_MENU;
        //                            //nSubDetail.parent_id = iSub2.MEU_PARENT_ID;
        //                            //nSubDetail.lng_description = iSub2.LNG_DESCRIPTION;
        //                            //nSubDetail.menu_url = iSub2.MEU_URL;
        //                            //nSubDetail.menu_img = iSub2.MEU_IMG;
        //                            //nSubDetail.menu_level = iSub2.MEU_LEVEL;
        //                            //nSubDetail.menu_list_no = iSub2.MEU_LIST_NO;
        //                            //nSubDetail.menu_control_type = iSub2.MEU_CONTROL_TYPE;
        //                            //nSubDetail.menu_action = iSub2.MEU_ACTIVE;
        //                            //nSubDetail.menu_url_direct = iSub2.MEU_URL_DIRECT;
        //                            nSubDetail.text = strCo2 + iSub2.LNG_DESCRIPTION;
        //                            //nSubDetail.parent_detail = GetParentDetail(iSub.MEU_PARENT_ID);
        //                            nSubDetail.tags = new List<string> { iSub2.MEU_ACTIVE };
        //                            nDetail.nodes.Add(nSubDetail);
        //                            i2++;

        //                            iCountSub = querySub.Count;
        //                        }
        //                        //nDetail.tags = iCountSub.ToString();
        //                        //CountAllSub = CountAllSub + iCountSub;

        //                    }
        //                }
        //            }
        //        }
        //        return listDetail;
        //    }
        //    catch (Exception ex)
        //    {
        //        return listDetail;
        //    }
        //}

        public static string GetGroupMenu(string sParentID)
        {
            string sGroupMenu = string.Empty;

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var allMenu = (from m in context.MENU
                                   where m.MEU_ROW_ID.Equals(sParentID)
                                   select m).ToList();

                    if (allMenu != null)
                    {
                        foreach (var item in allMenu)
                        {
                            sGroupMenu = item.MEU_GROUP_MENU;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return sGroupMenu;
        }

        public static string GetControlTypeMenu(string sParentID)
        {
            string sGroupMenu = string.Empty;

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var allMenu = (from m in context.MENU
                                   where m.MEU_ROW_ID.Equals(sParentID)
                                   select m).ToList();

                    if (allMenu != null)
                    {
                        foreach (var item in allMenu)
                        {
                            sGroupMenu = item.MEU_CONTROL_TYPE;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return sGroupMenu;
        }

        public static string GetLevelMenu(string sParentID, string sMenuType)
        {
            string sLevelMenu = string.Empty;

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    List<MENU> menuList = new List<MENU>();

                    var allMenu = (from m in context.MENU
                                   where m.MEU_ROW_ID.Equals(sParentID)
                                   select m).ToList();

                    if (allMenu != null)
                    {
                        foreach (var item in allMenu)
                        {
                            if (!string.IsNullOrEmpty(item.MEU_LEVEL))
                            {
                                int iLevel = Convert.ToInt32(item.MEU_LEVEL);
                                if (sMenuType == "MENU")
                                {
                                    iLevel++;
                                } 
                                sLevelMenu = iLevel.ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sLevelMenu;
        }

        public static string GetMaxListNoMenu(string sParentID)
        {
            string sMaxListNoMenu = "1";

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var allMenu = (from m in context.MENU
                                   select m).ToList();

                    if (sParentID == "#")
                    {
                        allMenu = (from m in allMenu
                                   where int.Parse(m.MEU_LEVEL).Equals(1)
                                   select m).ToList();
                    }
                    else
                    {
                        allMenu = (from m in context.MENU
                                   where m.MEU_PARENT_ID.Equals(sParentID)
                                   select m).ToList();
                    }


                    if (allMenu != null)
                    {
                        if (allMenu.Count > 0)
                        {
                            var mainMenu = allMenu.Max(x => int.Parse(x.MEU_LIST_NO));
                            int iMaxList = Convert.ToInt32(mainMenu) + 1;
                            sMaxListNoMenu = iMaxList.ToString();
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sMaxListNoMenu;
        }

        public static List<SelectListItem> GetDDListNo(string sParentID)
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MENU
                                 select v).ToList();

                    if (sParentID != "#")
                    {
                        query = (from v in query
                                 where v.MEU_PARENT_ID.Equals(sParentID)
                                 select v).ToList();
                    }
                    else
                    {
                        query = (from v in query
                                 where int.Parse(v.MEU_LEVEL).Equals(1)
                                 select v).ToList();
                    }

                    if (query != null)
                    {
                        var iCount = query.Count();
                        for (int i = 1; i <= iCount; i++)
                        {
                            rtn.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
                        }
                    }
                }
                return rtn;
            }
            catch (Exception ex)
            {

                return rtn;
            }
        }

        public static string GetParentDetail(string sID)
        {
            string sParentDetail = string.Empty;

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var allMenu = (from m in context.MENU
                                   where m.MEU_ROW_ID.Equals(sID)
                                   select m).ToList();

                    if (allMenu != null)
                    {
                        foreach (var item in allMenu)
                        {
                            sParentDetail = item.LNG_DESCRIPTION;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return sParentDetail;
        }


        // string format (MENU)
        private static string pstrCanNotLoadFormat = "<li><a href=\"#\">Can't Load Menu Please contact Admin!!!</a></li>";
        private static string pstrMainMenuFormat = "<li><a href=\"{0}\" {1} class=\"dropdown-toggle\" data-toggle=\"dropdown\">{2}<span class=\"caret\"></span></a></li>";
        private static string pstrMainMenuWithSubMenuFormat = "<li><a href=\"{0}\" {1} class=\"dropdown-toggle\" data-toggle=\"dropdown\">{2}<span class=\"caret\"></span></a><ul class=\"dropdown-menu\">{3}</ul></li>";
        private static string pstrSubMenuFormat = "<li><a href=\"{0}\" {1}>{2}</a></li>";
        private static string pstrSubMenuWithSubMenuFormat = "<li class=\"dropdown-submenu\"><a href=\"{0}\" {1}>{2}</a><ul class=\"dropdown-menu\">{3}</ul>";
        private static ShareFn _FN = new ShareFn();

        public static string GenMenu(UserModel pUser)
        {
            string htmlMenu = "";
            if (pUser != null)
            {
                if (pUser.MenuPermission.Count > 0)
                {
                    StringBuilder _html = new StringBuilder();
                    List<MenuPermission> _lstMenu = pUser.MenuPermission;

                    var mainMenu = _lstMenu.Where(item => int.Parse(item.MEU_LEVEL).Equals(1) && item.MEU_CONTROL_TYPE.Equals("MENU")).OrderBy(x => int.Parse(x.MEU_LIST_NO));

                    if (mainMenu != null)
                    {
                        foreach (var item in mainMenu)
                        {
                            string parentURL = GetMenuURL(item.MEU_ROWID, item.MEU_URL, item.MEU_URL_DIRECT); //GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
                            string direct = checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";
                            if (item.MENU_CHILD.Count > 0)
                            {
                                var subMenu = GetSubMenu(item.MENU_CHILD);
                                _html.Append(string.Format(pstrMainMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
                            }
                            else
                            {
                                _html.Append(string.Format(pstrMainMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
                            }
                        }
                    }

                    htmlMenu = _html.ToString();
                }
                else
                {
                    htmlMenu = pstrCanNotLoadFormat;
                }

            }
            else
            {
                htmlMenu = pstrCanNotLoadFormat;
            }

            return htmlMenu;
           
        }

        public static string GetSubMenu(List<MenuPermission> menuLst)
        {
            StringBuilder _html = new StringBuilder();

            try
            {
                if (menuLst != null)
                {
                    var query = menuLst.OrderBy(s => int.Parse(s.MEU_LIST_NO));

                    foreach (var item in query)
                    {
                        //string parentURL = GetURL(iSub.MEU_URL, iSub.MEU_URL_DIRECT, iSub.LNG_DESCRIPTION);
                        string parentURL = GetMenuURL(item.MEU_ROWID, item.MEU_URL, item.MEU_URL_DIRECT); //GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
                        string direct = checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";

                        if (item.MENU_CHILD.Count > 0)
                        {
                            var subMenu = GetSubMenu(item.MENU_CHILD);
                            _html.Append(string.Format(pstrSubMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
                        }
                        else
                        {
                            _html.Append(string.Format(pstrSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
                        }
                    }
                }
                else
                {
                    _html.Append("");
                }


                return _html.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static bool checkURLDirect(string MEU_URL, string MEU_URL_DIRECT)
        {
            var check = false;
            if ((String.IsNullOrEmpty(MEU_URL_DIRECT) != true || Convert.ToString(MEU_URL_DIRECT) != "#") && Convert.ToString(MEU_URL) == "REDIRECT")
            {
                // New windows go to MEU_URL_DIRECT
                check = true;

            }
            return check;
        }

        public static string GetURL(string strURL, string strURLDirect)
        {

            string parentURL = "#";

            if (checkURLDirect(strURL, strURLDirect) == true)
            {
                // New windows go to MEU_URL_DIRECT
                if (strURLDirect != "#" && !string.IsNullOrEmpty(strURLDirect))
                {
                    parentURL = strURLDirect;
                }

            }
            else
            {
                if (strURL != "#")
                {
                    parentURL = _FN.GetSiteRootUrl(strURL);
                }
            }
            return parentURL;
        }

        public static string GetMenuURL(string pMenuID, string pMenuURL, string pMenuURLDirect)
        {

            string URL = "#";

            if (checkURLDirect(pMenuURL, pMenuURLDirect) == true)
            {
                // New windows go to MEU_URL_DIRECT
                if (pMenuURLDirect != "#" && !string.IsNullOrEmpty(pMenuURLDirect))
                {
                    URL = pMenuURLDirect;
                }

            }
            else
            {
                if (pMenuURL != "#" && !string.IsNullOrEmpty(pMenuURL) && !pMenuURL.ToUpper().Contains("web/DIRECTPAGE.aspx".ToUpper()))
                {
                    URL = _FN.GetSiteRootUrl("CPAIMVC/Menu/MenuURL?menuID=" + pMenuID);
                }
                else
                {
                    URL = _FN.GetSiteRootUrl(pMenuURL);
                }
            }

            return URL;
        }

        public static List<string> GetActionByMenuID(string pMenuID)
        {
            List<string> lstrAction = new List<string>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var actionMenu = (from m in context.MENU.Where(p=>p.MEU_CONTROL_TYPE.Equals("FIELD"))
                                   where m.MEU_PARENT_ID.Equals(pMenuID)
                                   select m).ToList();

                    if (actionMenu != null)
                    {
                        foreach (var item in actionMenu)
                        {
                            lstrAction.Add(item.LNG_DESCRIPTION);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return lstrAction;
        }
    }
}