﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data;
using System.IO;
using OfficeOpenXml;
using Newtonsoft.Json.Linq;
using com.pttict.sap.Interface.sap.accrual.post;
using com.pttict.sap.Interface.Service;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class PCFPurchaseEntryServiceModel
    {
        #region Data functions

        public ReturnValue Search(ref PCFPurchaseEntryViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sCompany = String.IsNullOrEmpty(pModel.sCompany) ? "" : pModel.sCompany;
                    var sTripNo = String.IsNullOrEmpty(pModel.sTripNo) ? "" : pModel.sTripNo;
                    var sPoDate = String.IsNullOrEmpty(pModel.sPoDate) ? "" : pModel.sPoDate;
                    var sProduct = String.IsNullOrEmpty(pModel.sProduct) ? "" : pModel.sProduct;
                    var sPoNo = String.IsNullOrEmpty(pModel.sPoNo) ? "" : pModel.sPoNo;
                    var sGrDate = String.IsNullOrEmpty(pModel.sGrDate) ? "" : pModel.sGrDate;
                    var sSupplier = String.IsNullOrEmpty(pModel.sSupplier) ? "" : pModel.sSupplier;

                    var query = (from h in context.PCF_HEADER
                                 where h.PHE_BOOKING_RELEASE == "Y" && (h.PHE_CHANNEL == "VESSEL" || h.PHE_CHANNEL == "TRAIN") && h.PHE_STATUS == "ACTIVE"
                                 join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO into viewA
                                 from va in viewA.DefaultIfEmpty()
                                 join o in context.OUTTURN_FROM_SAP on h.PHE_TRIP_NO equals o.TRIP_NO into viewB
                                 from vb in viewB.DefaultIfEmpty()
                                 join v in context.MT_VEHICLE on h.PHE_VESSEL equals v.VEH_ID into viewC
                                 from vc in viewC.DefaultIfEmpty()
                                 join mt in context.MT_MATERIALS on va.PMA_MET_NUM equals mt.MET_NUM into viewD
                                 from vd in viewD.DefaultIfEmpty()
                                 join s in context.MT_VENDOR on va.PMA_SUPPLIER equals s.VND_ACC_NUM_VENDOR into viewE
                                 from ve in viewE.DefaultIfEmpty()
                                 join t in context.PCF_MATERIAL_PRICE on h.PHE_TRIP_NO equals t.PMP_TRIP_NO into viewF
                                 from vf in viewF.DefaultIfEmpty()
                                 select new
                                 {
                                     dTripNo = h.PHE_TRIP_NO,
                                     dMetNum = va.PMA_MET_NUM,
                                     dVessel = h.PHE_CHANNEL,
                                     dVesselType = vc.VEH_TYPE,
                                     dPoNo = va.PMA_PO_NO,
                                     dProduct = vd.MET_MAT_DES_ENGLISH,
                                     dSupplier = ve.VND_NAME1,
                                     dSupplierNo = ve.VND_ACC_NUM_VENDOR,
                                     dVolumeBBL = va.PMA_VOLUME_BBL,
                                     dVolumeMT = va.PMA_VOLUME_MT,
                                     dPurchaseType = va.PMA_PURCHASE_TYPE,
                                     dCompanyCode = h.PHE_COMPANY_CODE,
                                     dCurrency = vf.PMP_CURRENCY,
                                     dMatItemNo = va.PMA_ITEM_NO
                                 });

                    #region Filter Data                    

                    if (!string.IsNullOrEmpty(sCompany) && sCompany != "-- Select --")
                    {
                        query = query.Where(p => p.dCompanyCode.Contains(sCompany));
                    }

                    if (!string.IsNullOrEmpty(sProduct) && sProduct != "-- Select --")
                    {
                        query = query.Where(p => p.dProduct.Contains(sProduct));
                    }

                    if (!string.IsNullOrEmpty(sTripNo))
                    {
                        query = query.Where(p => p.dTripNo.ToLower().Contains(sTripNo.Trim().ToLower()));

                    }

                    //if (!string.IsNullOrEmpty(sPoNo))
                    //{
                    //    query = query.Where(p => p.dPoNo.Contains(sPoNo));
                    //}

                    #endregion

                    if (query != null && query.ToList().Count() > 0)
                    {
                        query = query.Distinct();
                        var query1 = query.ToList();
                        var query2 = query1.GroupBy(x => new { x.dTripNo, x.dMetNum }).Select(grp => grp.First()).ToList();
                        pModel.sSearchData = new List<PCFPurchaseEntryViewModel_SearchData>();
                        foreach (var item in query2)
                        {
                            string FIDoc = "";
                            var queryfi = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO == item.dTripNo && z.PPE_MET_NUM == item.dMetNum && z.PPE_ITEM_NO == 10).SingleOrDefault();
                            if (queryfi != null)
                            {
                                FIDoc = queryfi.PPE_SAP_FI_DOC_NO;
                            }
                            else
                            {

                                FIDoc = "";
                            }
                            pModel.sSearchData.Add(new PCFPurchaseEntryViewModel_SearchData
                            {
                                TripNo = String.IsNullOrEmpty(item.dTripNo) ? "" : item.dTripNo,
                                MetNum = String.IsNullOrEmpty(item.dMetNum) ? "" : item.dMetNum,
                                //Vessel = String.IsNullOrEmpty(item.dVesselType) ? "N/A" : item.dVesselType,
                                Vessel = String.IsNullOrEmpty(item.dVessel) ? "N/A" : item.dVessel,
                                PoNo = String.IsNullOrEmpty(item.dPoNo) ? "" : item.dPoNo,
                                Product = String.IsNullOrEmpty(item.dProduct) ? "" : item.dProduct,
                                Supplier = String.IsNullOrEmpty(item.dSupplier) ? "" : item.dSupplier,
                                SupplierNo = String.IsNullOrEmpty(item.dSupplier) ? "" : item.dSupplierNo,
                                VolumeBBL = (item.dVolumeBBL == null) ? "0" : item.dVolumeBBL.Value.ToString("#,##0.0000"),
                                VolumeMT = (item.dVolumeMT == null) ? "0" : item.dVolumeMT.Value.ToString("#,##0.0000"),
                                PurchaseType = (item.dPurchaseType == null) ? "N/A" : GetPurchaseType(item.dPurchaseType.ToString()),
                                CompanyCode = String.IsNullOrEmpty(item.dCompanyCode) ? "" : item.dCompanyCode,
                                Currency = String.IsNullOrEmpty(item.dCurrency) ? "" : item.dCurrency,
                                MatItemNo = item.dMatItemNo.ToString(),
                                FIDoc = FIDoc
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue Calculate(PCFPurchaseEntryViewModel_SearchData model)
        {
            ReturnValue result = new ReturnValue();
            string errorMessage = "";
            bool somethingWrong = false;
            if (model.sOutturn == null || model.sOutturn.Count() == 0)
            {
                model.sOutturn = new List<PCFPurchaseEntryViewModel_Outtern>();
                return null;
            }

            foreach (var outturn in model.sOutturn)
            {
                if (outturn.Select)
                {
                    outturn.ROE = Convert.ToDecimal(outturn.ROE).ToString();
                    try
                    {
                        #region Calculate Amount USD, Amount THB and VAT
                        //-----------------------------------USD
                        if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "bbl")
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FOBPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FreightPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FOBPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FreightPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                        }
                        else if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "mt")
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FOBPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FreightPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FOBPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FreightPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FOBPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FreightPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FOBPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FreightPrice)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                        }
                        //-----------------------------THB
                        if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "bbl")
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "usd")
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FOBPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FreightPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "usd")
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FOBPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FreightPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                        }
                        else if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "mt")
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "usd")
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FOBPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FreightPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "usd")
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FOBPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FreightPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "usd")
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FOBPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FreightPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "usd")
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FOBPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FreightPrice)) * Convert.ToDecimal(outturn.ROE)).ToString();
                                }
                                else
                                {
                                    outturn.FOBAmountTHB = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FOBPrice))).ToString();
                                    outturn.FreightAmountTHB = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.FreightPrice))).ToString();
                                }
                            }
                        }

                        outturn.CnFPrice = Math.Round(Convert.ToDecimal(outturn.FOBPrice) + Convert.ToDecimal(outturn.FreightPrice), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        outturn.FOBAmountUSD = Math.Round(Convert.ToDecimal(outturn.FOBAmountUSD), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        outturn.FreightAmountUSD = Math.Round(Convert.ToDecimal(outturn.FreightAmountUSD), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        outturn.CnFAmountUSD = Math.Round(Convert.ToDecimal(Convert.ToDecimal(outturn.FOBAmountUSD) + Convert.ToDecimal(outturn.FreightAmountUSD)), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");

                        outturn.FOBAmountTHB = Math.Round(Convert.ToDecimal(outturn.FOBAmountTHB), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        //outturn.FOBAmountTHB = Math.Round((Convert.ToDecimal(outturn.FOBAmountUSD) * Convert.ToDecimal(outturn.ROE)), 4).ToString("#,##0.0000");
                        outturn.FreightAmountTHB = Math.Round(Convert.ToDecimal(outturn.FreightAmountTHB), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        //outturn.FreightAmountTHB = Math.Round((Convert.ToDecimal(outturn.FreightAmountUSD) * Convert.ToDecimal(outturn.ROE)), 4).ToString("#,##0.0000");

                        outturn.CnFAmountTHB = Math.Round(Convert.ToDecimal(Convert.ToDecimal(outturn.FOBAmountTHB) + Convert.ToDecimal(outturn.FreightAmountTHB)), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");

                        outturn.CnFVat7 = Math.Round(((Convert.ToDecimal(outturn.CnFAmountTHB)) * Convert.ToDecimal(0.07)), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        //Math.Round(((Convert.ToDecimal(outturn.CnFAmountUSD) * Convert.ToDecimal(outturn.ROE)) * Convert.ToDecimal(0.07)), 4).ToString();

                        outturn.CnFTotalAmountUSD = Math.Round(Convert.ToDecimal(outturn.FOBAmountUSD), 4, MidpointRounding.AwayFromZero).ToString("#,##0.0000");
                        //outturn.CnFAmountUSD;
                        outturn.CnFTotalAmountTHB = (Convert.ToDecimal(outturn.FOBAmountTHB) + Convert.ToDecimal(outturn.CnFVat7)).ToString("#,##0.0000");
                        //(Convert.ToDecimal(outturn.CnFAmountTHB) + Convert.ToDecimal(outturn.CnFVat7)).ToString("#,##0.0000");
                        outturn.ROE = Convert.ToDecimal(outturn.ROE).ToString("#,##0.0000");

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        somethingWrong = true;
                        errorMessage = ex.Message + " : " + ex.ToString();
                    }
                }
            }

            if (somethingWrong)
            {
                result.Status = false;
                result.Message = "ERROR - " + errorMessage;
            }
            else
            {
                result.Status = true;
                result.Message = "Done without any problems";
            }

            return result;
        }

        public PCFPurchaseEntryViewModel_SearchData CleansingModel(PCFPurchaseEntryViewModel_SearchData model)
        {
            return null;
        }

        public PCFPurchaseEntryViewModel_SearchData FilterOnlySavedRecord(PCFPurchaseEntryViewModel_SearchData model)
        {
            try
            {
                List<int> removedList = new List<int>();
                for (int i = 0; i < model.sOutturn.Count(); i++)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        string tripNo = model.TripNo;
                        decimal itemNo = Convert.ToDecimal(model.sOutturn[i].ItemNo);
                        decimal matItemBo;
                        try
                        {
                            matItemBo = Convert.ToDecimal(GetMatItemNo(tripNo, itemNo.ToString()));
                        }
                        catch
                        {
                            matItemBo = 0;
                        }

                        var query = (from r in context.PCF_PURCHASE_ENTRY_DAILY
                                     where r.MAT_ITEM_NO == matItemBo && r.TRIP_NO == tripNo && r.ITEM_NO == itemNo
                                     select new
                                     {
                                         dItemNo = r.ITEM_NO
                                     });

                        if (query == null || query.ToList().Count() == 0)
                        {
                            removedList.Add(i + 1);
                        }
                    }
                }

                foreach (var index in removedList)
                {
                    model.sOutturn.RemoveAll(x => removedList.Contains(Convert.ToInt32(x.ItemNo)));
                }
            }
            catch (Exception ex)
            {

            }

            return model;
        }

        #endregion

        public ReturnValue SendToSAP(string jsonConfig, PCFPurchaseEntryViewModel_SearchData model, string accrualType, string reverseDate)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            string JsonD = MasterData.GetJsonMasterSetting("PIT_ACCRUAL_POSTING_CONFIG");
            var accrualSetting = GetAccrualSettings();
            var companyCode = model.CompanyCode;
            var metNum = model.MetNum;
            var sortField = GetSortFieldFromSupplier(model.SupplierNo);
            var io = GetIOFromMetNum(metNum, companyCode);
            var freightVendor = GetVendorFromTripNo(model.TripNo);
            int numberOfSuccess = 0;
            int numberOfFail = 0;

            reverseDate = reverseDate.Split('/')[2] + "-" +
                            reverseDate.Split('/')[1] + "-" +
                            reverseDate.Split('/')[0];
            PCFPurchaseEntryViewModel_SearchData TempData = new PCFPurchaseEntryViewModel_SearchData();
            TempData.sOutturn = new List<PCFPurchaseEntryViewModel_Outtern>();
            var js = new JavaScriptSerializer();
            TempData = js.Deserialize<PCFPurchaseEntryViewModel_SearchData>(js.Serialize(model));
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                foreach (var outturn in TempData.sOutturn)
                {
                    string theItemNo = outturn.ItemNo;
                    if (theItemNo == "")
                    {
                        theItemNo = "0";
                    }
                    Decimal itemNoToCompate = Convert.ToDecimal(theItemNo);

                    var queryForSapFiDoc = (from p in context.PCF_PURCHASE_ENTRY_DAILY
                                            where p.TRIP_NO == model.TripNo && p.ITEM_NO == itemNoToCompate
                                            select new
                                            {
                                                dBlQtyBbl = p.BL_VOLUME_BBL,
                                                dBlQtyMt = p.BL_VOLUME_MT,
                                                dBlQtyMl = p.BL_VOLUME_L30,

                                                dQtyBbl = p.GR_VOLUME_BBL,
                                                dQtyMt = p.GR_VOLUME_MT,
                                                dQtyMl = p.GR_VOLUME_L30,

                                                dFOBPrice = p.FOB_PRICE,
                                                dFreightPrice = p.FRT_PRICE,
                                                dCnFPrice = p.CF_PRICE,
                                                dROE = p.ROE,
                                                dFOBAmountUSD = p.FOB_AMOUNT_USD,
                                                dFOBAmountTHB = p.FOB_AMOUNT_THB,
                                                dFreightAmountUSD = p.FRT_AMOUNT_USD,
                                                dFreightAmountTHB = p.FRT_AMOUNT_THB,
                                                dCnFAmountUSD = p.CF_AMOUNT_USD,
                                                dCnFAmountTHB = p.CF_AMOUNT_THB,
                                                dCnFVat7 = p.CF_VAT,
                                                dCnFTotalAmountUSD = p.CF_TOTAL_USD,
                                                dCnFTotalAmountTHB = p.CF_TOTAL_THB,

                                                dVolumeUnit = p.CAL_VOLUME_UNIT,
                                                dInvoiceFigure = p.CAL_INVOICE_FIGURE,
                                                dPriceUnit = p.CAL_PRICE_UNIT,

                                                dMatItmeNo = p.MAT_ITEM_NO,
                                                dItemNo = p.ITEM_NO,
                                                dSapFiDocNo = p.SAP_FI_DOC_NO,
                                                dSapFiDocStatus = p.SAP_FI_DOC_STATUS,
                                                dLastUpdate = p.UPDATED_DATE,
                                                dUpdatedBy = p.UPDATED_BY
                                            });

                    if (queryForSapFiDoc != null && queryForSapFiDoc.ToList().Count() > 0)
                    {
                        outturn.VolumeUnit = queryForSapFiDoc.ToList()[0].dVolumeUnit;
                        outturn.InvoiceFigure = queryForSapFiDoc.ToList()[0].dInvoiceFigure;
                        outturn.PriceUnit = queryForSapFiDoc.ToList()[0].dPriceUnit;

                        outturn.MatItemNo = queryForSapFiDoc.ToList()[0].dMatItmeNo.ToString();
                        outturn.SAPFIDoc = queryForSapFiDoc.ToList()[0].dSapFiDocNo;
                        outturn.SAPFIDocStatus = queryForSapFiDoc.ToList()[0].dSapFiDocStatus;
                        
                        outturn.BlQtyBbl = (queryForSapFiDoc.ToList()[0].dBlQtyBbl ?? 0m).ToString("#,##0.0000");
                        outturn.BlQtyMt = (queryForSapFiDoc.ToList()[0].dBlQtyMt ?? 0m).ToString("#,##0.0000");
                        outturn.BlQtyMl = (queryForSapFiDoc.ToList()[0].dBlQtyMl ?? 0m).ToString("#,##0.0000");

                        outturn.QtyBbl = (queryForSapFiDoc.ToList()[0].dQtyBbl ?? 0m).ToString("#,##0.0000");
                        outturn.QtyMt = (queryForSapFiDoc.ToList()[0].dQtyMt ?? 0m).ToString("#,##0.0000");
                        outturn.QtyMl = (queryForSapFiDoc.ToList()[0].dQtyMl ?? 0m).ToString("#,##0.0000");


                        outturn.FOBPrice = (queryForSapFiDoc.ToList()[0].dFOBPrice ?? 0m).ToString("#,##0.0000");
                        outturn.FreightPrice = (queryForSapFiDoc.ToList()[0].dFreightPrice ?? 0m).ToString("#,##0.0000");
                        outturn.CnFPrice = (queryForSapFiDoc.ToList()[0].dCnFPrice ?? 0m).ToString("#,##0.0000");
                        outturn.ROE = (queryForSapFiDoc.ToList()[0].dROE ?? 0m).ToString("#,##0.0000");
                        outturn.FOBAmountUSD = (queryForSapFiDoc.ToList()[0].dFOBAmountUSD ?? 0m).ToString("#,##0.0000");
                        outturn.FOBAmountTHB = (queryForSapFiDoc.ToList()[0].dFOBAmountTHB ?? 0m).ToString("#,##0.0000");
                        outturn.FreightAmountUSD = (queryForSapFiDoc.ToList()[0].dFreightAmountUSD ?? 0m).ToString("#,##0.0000");
                        outturn.FreightAmountTHB = (queryForSapFiDoc.ToList()[0].dFreightAmountTHB ?? 0m).ToString("#,##0.0000");
                        outturn.CnFAmountUSD = (queryForSapFiDoc.ToList()[0].dCnFAmountUSD ?? 0m).ToString("#,##0.0000");
                        outturn.CnFAmountTHB = (queryForSapFiDoc.ToList()[0].dCnFAmountTHB ?? 0m).ToString("#,##0.0000");
                        outturn.CnFVat7 = (queryForSapFiDoc.ToList()[0].dCnFVat7).ToString("#,##0.0000");
                        outturn.CnFTotalAmountUSD = (queryForSapFiDoc.ToList()[0].dCnFTotalAmountUSD ?? 0m).ToString("#,##0.0000");
                        outturn.CnFTotalAmountTHB = (queryForSapFiDoc.ToList()[0].dCnFTotalAmountTHB ?? 0m).ToString("#,##0.0000");

                        

                        string updateDate = "";
                        if (queryForSapFiDoc.ToList()[0].dLastUpdate != null)
                        {
                            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                            updateDate = (queryForSapFiDoc.ToList()[0].dLastUpdate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                        }
                        outturn.UpdatedDate = updateDate;
                        outturn.UpdatedBy = queryForSapFiDoc.ToList()[0].dUpdatedBy;
                    }
             
                }
            }

            foreach (var outturn in TempData.sOutturn)
            {
                if (outturn.Select)
                {
                    try
                    {
                        //bool isFreight = false;
                        decimal frePrice = 0m;
                        decimal fobPrice = 0m;
                        if (!string.IsNullOrEmpty(outturn.PriceUnit))
                        {
                            if(outturn.PriceUnit.ToLower() == "usd")
                            {
                                if (!string.IsNullOrEmpty(outturn.FOBAmountUSD) && outturn.FOBAmountUSD.Trim() != "0")
                                {
                                    decimal number;
                                    if (decimal.TryParse(outturn.FOBAmountUSD, out number))
                                    {
                                        //isFreight = true;
                                        fobPrice = number;
                                    }

                                }
                                if (!string.IsNullOrEmpty(outturn.FreightAmountUSD) && outturn.FreightAmountUSD.Trim() != "0")
                                {
                                    decimal number;
                                    if (decimal.TryParse(outturn.FreightAmountUSD, out number))
                                    {
                                        //isFreight = true;
                                        frePrice = number;
                                    }

                                }
                            }
                            else if (outturn.PriceUnit.ToLower() == "thb")
                            {
                                if (!string.IsNullOrEmpty(outturn.FOBAmountTHB) && outturn.FOBAmountTHB.Trim() != "0")
                                {
                                    decimal number;
                                    if (decimal.TryParse(outturn.FOBAmountTHB, out number))
                                    {
                                        //isFreight = true;
                                        fobPrice = number;
                                    }

                                }
                                if (!string.IsNullOrEmpty(outturn.FreightAmountTHB) && outturn.FreightAmountTHB.Trim() != "0")
                                {
                                    decimal number;
                                    if (decimal.TryParse(outturn.FreightAmountTHB, out number))
                                    {
                                        //isFreight = true;
                                        frePrice = number;
                                    }

                                }
                            }
                        }
                        
                        //Type A
                        if (accrualType.ToLower() == "a")
                        {
                            string sapFiDoc = "";
                            rtn.Message += "Running Date = " + outturn.RunningDate + ", ";

                            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                            com.pttict.sap.Interface.sap.accrual.create.BAPIACHE09 DocumentHeader = new com.pttict.sap.Interface.sap.accrual.create.BAPIACHE09();
                            List<com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09> AccountGl = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09>();
                            List<com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09> CurrencyAmount = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09>();
                            List<com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX> Extension2 = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX>();
                            List<com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING> zfi_GL_MAPPING = new List<com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING>();

                            string metMatDesEnglish = GetMetMatDesEnglishFromMetNum(metNum);
                            string fisPeriod = (DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month < 10) ? "0" + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString(cultureinfo) : DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString(cultureinfo);
                            string headerTxtSuffix = "REC." + GetMonthName(DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month) + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year;
                            int avaiableCharRangeForHeaderTxt = 25 - (headerTxtSuffix.Length + 1);
                            string freightCurrency = "";
                            bool isFreight = IsFreightAvailable(model.TripNo, out freightCurrency);

                            #region DocumentHeader
                            var documentHeaderFromConfig = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.DOCUMENTHEADER
                                .Where(x => x.ACCRUE_TYPE.ToLower() == accrualType.ToLower() && x.COMP_CODE == companyCode);
                            if (documentHeaderFromConfig.ToList().Count() == 0)
                            {
                                return null;
                            }
                            var docHeader = documentHeaderFromConfig.ToList()[0];

                            DocumentHeader.OBJ_TYPE = docHeader.OBJ_TYPE;
                            DocumentHeader.OBJ_KEY = docHeader.OBJ_KEY;
                            DocumentHeader.BUS_ACT = docHeader.BUS_ACT;
                            DocumentHeader.USERNAME = docHeader.USERNAME;
                            DocumentHeader.HEADER_TXT = (metMatDesEnglish.Length <= avaiableCharRangeForHeaderTxt)
                                ? metMatDesEnglish + " " + headerTxtSuffix
                                : metMatDesEnglish.Substring(0, avaiableCharRangeForHeaderTxt) + " " + headerTxtSuffix;
                            DocumentHeader.COMP_CODE = companyCode;
                            DocumentHeader.DOC_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd", cultureinfo);
                            DocumentHeader.PSTNG_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd", cultureinfo);
                            DocumentHeader.FISC_YEAR = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year.ToString(cultureinfo);
                            DocumentHeader.FIS_PERIOD = fisPeriod;
                            DocumentHeader.DOC_TYPE = docHeader.DOC_TYPE;
                            DocumentHeader.REF_DOC_NO_LONG = model.TripNo + "." + outturn.ItemNo;
                            DocumentHeader.REASON_REV = docHeader.REASON_REV;

                            rtn.Message += "HEADER_TXT = " + DocumentHeader.HEADER_TXT + ", ";
                            rtn.Message += "DOC_DATE = " + DocumentHeader.DOC_DATE + ", ";
                            rtn.Message += "PSTNG_DATE = " + DocumentHeader.PSTNG_DATE + ", ";
                            rtn.Message += "FIS_PERIOD = " + DocumentHeader.FIS_PERIOD + "\r\n";
                            #endregion

                            #region AccountGL
                            var accountGlNotNull = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL
                                .Where(x => x.COMP_CODE == companyCode);
                            if (accountGlNotNull.ToList().Count() == 0)
                            {
                                return null;
                            }
                            AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000001",
                                GL_ACCOUNT = accountGlNotNull.ToList()[0].GLACCOUNT,
                                ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = accountGlNotNull.ToList()[0].COSTCENTER,
                                ORDERID = io
                            });
                            AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000002",
                                GL_ACCOUNT = "",
                                ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = "",
                                ORDERID = ""
                            });

                            if (isFreight)
                            {
                                AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                                {
                                    ITEMNO_ACC = "0000000003",
                                    GL_ACCOUNT = accountGlNotNull.ToList()[1].GLACCOUNT,
                                    ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                    REF_KEY_3 = sortField,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = freightVendor.PadLeft(10, '0'),
                                    ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                    COSTCENTER = accountGlNotNull.ToList()[0].COSTCENTER,
                                    ORDERID = io
                                });
                                AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                                {
                                    ITEMNO_ACC = "0000000004",
                                    GL_ACCOUNT = "",
                                    ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                    REF_KEY_3 = sortField,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = freightVendor.PadLeft(10, '0'),
                                    ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                    COSTCENTER = "",
                                    ORDERID = ""
                                });
                            }
                            #endregion

                            #region CurrencyAmount
                            //var doccur = Convert.ToDecimal((outturn.PriceUnit.ToLower() == "usd") ? outturn.CnFAmountUSD : outturn.CnFAmountTHB);
                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000001",
                                CURR_TYPE = "00",
                                CURRENCY = outturn.PriceUnit,//model.PriceUnit,
                                AMT_DOCCUR = fobPrice,//doccur,
                                AMT_DOCCURSpecified = true
                            });
                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000002",
                                CURR_TYPE = "00",
                                CURRENCY = outturn.PriceUnit,
                                AMT_DOCCUR = fobPrice * -1 ,//doccur * -1,
                                AMT_DOCCURSpecified = true
                            });

                            if (isFreight)
                            {
                                //var freCurr = Convert.ToDecimal((outturn.PriceUnit.ToLower() == "usd") ? outturn.FreightAmountUSD : outturn.FreightAmountTHB);
                                CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                                {
                                    ITEMNO_ACC = "0000000003",
                                    CURR_TYPE = "00",
                                    CURRENCY = outturn.PriceUnit,
                                    AMT_DOCCUR = frePrice ,
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                                {
                                    ITEMNO_ACC = "0000000004",
                                    CURR_TYPE = "00",
                                    CURRENCY = outturn.PriceUnit,
                                    AMT_DOCCUR = frePrice * -1,
                                    AMT_DOCCURSpecified = true
                                });
                            }
                            #endregion

                            #region Extension2

                            var extension2ForVolume = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Volume" && x.COMP_CODE == companyCode);
                            if (extension2ForVolume.ToList().Count() == 0)
                            {
                                return null;
                            }
                            string volumnUnit = outturn.VolumeUnit.ToUpper();
                            string volumn = "";
                            if (outturn.InvoiceFigure.Trim().ToLower() == "outturn")
                            {
                                if (volumnUnit == "BBL") volumn = outturn.QtyBbl;
                                else if (volumnUnit == "MT") volumn = outturn.QtyMt;
                                else if (volumnUnit == "L30") volumn = outturn.QtyMl;
                            }
                            else
                            {
                                if (volumnUnit == "BBL") volumn = outturn.BlQtyBbl;
                                else if (volumnUnit == "MT") volumn = outturn.BlQtyMt;
                                else if (volumnUnit == "L30") volumn = outturn.BlQtyMl;
                            }
                            volumn = volumn.Replace(",", "");
                            //volumn = Convert.ToInt64(Convert.ToDecimal(volumn)).ToString();
                            volumn = (Convert.ToDecimal(volumn)).ToString("###0.000");

                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //VOLUME
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForVolume.ToList()[0].VALUEPART1,
                                VALUEPART2 = volumn
                            });
                            var extension2ForUOM = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "UOM" && x.COMP_CODE == companyCode);
                            if (extension2ForUOM.ToList().Count() == 0)
                            {
                                return null;
                            }

                            //if (!isFreight)
                            //{
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForUOM.ToList()[0].VALUEPART1,
                                VALUEPART2 = outturn.VolumeUnit
                            });
                            //}

                            var extension2ForBP = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Business Place" && x.COMP_CODE == companyCode);
                            if (extension2ForBP.ToList().Count() == 0)
                            {
                                return null;
                            }
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                            {
                                STRUCTURE = "0000000002",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });

                            if (isFreight)
                            {
                                Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                                {
                                    STRUCTURE = "0000000003",
                                    VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1 //TODO fix it
                                });
                                Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                                {
                                    STRUCTURE = "0000000004",
                                    VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1 //TODO fix it
                                });
                            }
                            #endregion

                            #region ZFI_GL-MAPPING
                            var zfiGlMapping = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING
                                .Where(x => x.COMP_CODE == companyCode);
                            if (zfiGlMapping.ToList().Count() == 0)
                            {
                                return null;
                            }
                            zfi_GL_MAPPING.Add(new com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING
                            {
                                ITEMNO_ACC = "0000000002",
                                TRAN_IND = zfiGlMapping.ToList()[0].TRAN_IND
                            });

                            if (isFreight)
                            {
                                zfi_GL_MAPPING.Add(new com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING
                                {
                                    ITEMNO_ACC = "0000000004",
                                    TRAN_IND = zfiGlMapping.ToList()[1].TRAN_IND
                                });
                            }
                            #endregion

                            AccrualService accrualService = new AccrualService();
                            sapFiDoc = accrualService.Create(jsonConfig, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING, reverseDate);

                            if (!String.IsNullOrEmpty(sapFiDoc))
                            {
                                numberOfSuccess++;
                                var rtn2 = UpdateSapFiDocNo(model.TripNo, outturn.MatItemNo, outturn.ItemNo, sapFiDoc, "A");
                                if (!rtn2.Status)
                                {
                                    rtn.Message = "Send to SAP successfully but got th problem when updating SAP Fi Doc No";
                                }
                                var modalDaily = model.sOutturn.Where(p => p.ItemNo == outturn.ItemNo).ToList();
                                if (modalDaily.Count > 0)
                                {
                                    modalDaily.FirstOrDefault().SAPFIDoc = sapFiDoc.Length >= 10 ? sapFiDoc.Substring(0, 10) : sapFiDoc;
                                    modalDaily.FirstOrDefault().SAPFIDocStatus = "A";
                                    modalDaily.FirstOrDefault().UpdatedBy = Const.User.Name;
                                    modalDaily.FirstOrDefault().UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy", cultureinfo);
                                }
                                //outturn.SAPFIDoc = sapFiDoc;
                                //outturn.SAPFIDocStatus = "A";
                                //outturn.UpdatedBy = Const.User.Name;
                                //outturn.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy", cultureinfo);
                            }
                            else
                            {
                                numberOfFail++;
                            }
                        }
                        else //Type C
                        {
                            string sapFiDoc = "";
                            rtn.Message += "Running Date = " + outturn.RunningDate + ", ";

                            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                            BAPIACHE09 DocumentHeader = new BAPIACHE09();
                            List<BAPIACGL09> AccountGl = new List<BAPIACGL09>();
                            List<BAPIACCR09> CurrencyAmount = new List<BAPIACCR09>();
                            List<BAPIPAREX> Extension2 = new List<BAPIPAREX>();
                            List<ZFI_GL_MAPPING> zfi_GL_MAPPING = new List<ZFI_GL_MAPPING>();

                            string metMatDesEnglish = GetMetMatDesEnglishFromMetNum(metNum);
                            string fisPeriod = (DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month < 10) ? "0" + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString(cultureinfo) : DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString(cultureinfo);
                            string headerTxtSuffix = "REC." + GetMonthName(DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month) + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year;
                            int avaiableCharRangeForHeaderTxt = 25 - (headerTxtSuffix.Length + 1);
                            string freightCurrency = "";
                            bool isFreight = IsFreightAvailable(model.TripNo, out freightCurrency);

                            #region DocumentHeader
                            var documentHeaderFromConfig = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.DOCUMENTHEADER
                                .Where(x => x.ACCRUE_TYPE.ToLower() == accrualType.ToLower() && x.COMP_CODE == companyCode);
                            if (documentHeaderFromConfig.ToList().Count() == 0)
                            {
                                return null;
                            }
                            var docHeader = documentHeaderFromConfig.ToList()[0];

                            DocumentHeader.OBJ_TYPE = docHeader.OBJ_TYPE;
                            DocumentHeader.OBJ_KEY = docHeader.OBJ_KEY;
                            DocumentHeader.BUS_ACT = docHeader.BUS_ACT;
                            DocumentHeader.USERNAME = docHeader.USERNAME;
                            DocumentHeader.HEADER_TXT = (metMatDesEnglish.Length <= avaiableCharRangeForHeaderTxt)
                                ? metMatDesEnglish + " " + headerTxtSuffix
                                : metMatDesEnglish.Substring(0, avaiableCharRangeForHeaderTxt) + " " + headerTxtSuffix;
                            DocumentHeader.COMP_CODE = companyCode;
                            DocumentHeader.DOC_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd", cultureinfo);
                            DocumentHeader.PSTNG_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd", cultureinfo);
                            DocumentHeader.FISC_YEAR = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year.ToString(cultureinfo);
                            DocumentHeader.FIS_PERIOD = fisPeriod;
                            DocumentHeader.DOC_TYPE = docHeader.DOC_TYPE;
                            DocumentHeader.REF_DOC_NO_LONG = model.TripNo + "." + outturn.ItemNo;
                            DocumentHeader.REASON_REV = docHeader.REASON_REV;

                            rtn.Message += "HEADER_TXT = " + DocumentHeader.HEADER_TXT + ", ";
                            rtn.Message += "DOC_DATE = " + DocumentHeader.DOC_DATE + ", ";
                            rtn.Message += "PSTNG_DATE = " + DocumentHeader.PSTNG_DATE + ", ";
                            rtn.Message += "FIS_PERIOD = " + DocumentHeader.FIS_PERIOD + "\r\n";
                            #endregion

                            #region AccountGL
                            var accountGlNotNull = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL
                                .Where(x => x.COMP_CODE == companyCode);
                            if (accountGlNotNull.ToList().Count() == 0)
                            {
                                return null;
                            }
                            AccountGl.Add(new BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000001",
                                GL_ACCOUNT = accountGlNotNull.ToList()[0].GLACCOUNT,
                                ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = accountGlNotNull.ToList()[0].COSTCENTER,
                                ORDERID = io
                            });
                            AccountGl.Add(new BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000002",
                                GL_ACCOUNT = "",
                                ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = "",
                                ORDERID = ""
                            });

                            if (isFreight)
                            {
                                AccountGl.Add(new BAPIACGL09
                                {
                                    ITEMNO_ACC = "0000000003",
                                    GL_ACCOUNT = accountGlNotNull.ToList()[1].GLACCOUNT,
                                    ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                    REF_KEY_3 = sortField,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = freightVendor.PadLeft(10, '0'),
                                    ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                    COSTCENTER = accountGlNotNull.ToList()[0].COSTCENTER,
                                    ORDERID = io
                                });
                                AccountGl.Add(new BAPIACGL09
                                {
                                    ITEMNO_ACC = "0000000004",
                                    GL_ACCOUNT = "",
                                    ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                    REF_KEY_3 = sortField,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = freightVendor.PadLeft(10, '0'),
                                    ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                    COSTCENTER = "",
                                    ORDERID = ""
                                });
                            }
                            #endregion

                            #region CurrencyAmount
                            var doccur = Convert.ToDecimal((outturn.PriceUnit.ToLower() == "usd") ? outturn.CnFAmountUSD : outturn.CnFAmountTHB);
                            CurrencyAmount.Add(new BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000001",
                                CURR_TYPE = "00",
                                CURRENCY = outturn.PriceUnit,
                                AMT_DOCCUR = fobPrice,//doccur,
                                AMT_DOCCURSpecified = true
                            });
                            CurrencyAmount.Add(new BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000002",
                                CURR_TYPE = "00",
                                CURRENCY = outturn.PriceUnit,
                                AMT_DOCCUR = fobPrice * -1,//doccur * -1,
                                AMT_DOCCURSpecified = true
                            });

                            if (isFreight)
                            {
                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = "0000000003",
                                    CURR_TYPE = "00",
                                    CURRENCY = outturn.PriceUnit,
                                    AMT_DOCCUR = frePrice,//doccur,
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = "0000000004",
                                    CURR_TYPE = "00",
                                    CURRENCY = outturn.PriceUnit,
                                    AMT_DOCCUR = frePrice * -1 ,//doccur * -1,
                                    AMT_DOCCURSpecified = true
                                });
                            }
                            #endregion

                            #region Extension2

                            var extension2ForVolume = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Volume" && x.COMP_CODE == companyCode);
                            if (extension2ForVolume.ToList().Count() == 0)
                            {
                                return null;
                            }
                            string volumnUnit = outturn.VolumeUnit.ToUpper();
                            string volumn = "";
                            if(outturn.InvoiceFigure.Trim().ToLower() == "outturn")
                            {
                                if (volumnUnit == "BBL") volumn = outturn.QtyBbl;
                                else if (volumnUnit == "MT") volumn = outturn.QtyMt;
                                else if (volumnUnit == "L30") volumn = outturn.QtyMl;
                            }
                            else
                            {
                                if (volumnUnit == "BBL") volumn = outturn.BlQtyBbl;
                                else if (volumnUnit == "MT") volumn = outturn.BlQtyMt;
                                else if (volumnUnit == "L30") volumn = outturn.BlQtyMl;
                            }
                            
                            volumn = volumn.Replace(",", "");
                            //volumn = Convert.ToInt64(Convert.ToDecimal(volumn)).ToString();
                            volumn = (Convert.ToDecimal(volumn)).ToString("###0.000");
                            Extension2.Add(new BAPIPAREX //VOLUME
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForVolume.ToList()[0].VALUEPART1,
                                VALUEPART2 = volumn
                            });
                            var extension2ForUOM = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "UOM" && x.COMP_CODE == companyCode);
                            if (extension2ForUOM.ToList().Count() == 0)
                            {
                                return null;
                            }

                            //if (!isFreight)
                            //{
                            Extension2.Add(new BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForUOM.ToList()[0].VALUEPART1,
                                VALUEPART2 = outturn.VolumeUnit
                            });
                            //}

                            var extension2ForBP = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Business Place" && x.COMP_CODE == companyCode);
                            if (extension2ForBP.ToList().Count() == 0)
                            {
                                return null;
                            }
                            Extension2.Add(new BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });
                            Extension2.Add(new BAPIPAREX
                            {
                                STRUCTURE = "0000000002",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });

                            if (isFreight)
                            {
                                Extension2.Add(new BAPIPAREX
                                {
                                    STRUCTURE = "0000000003",
                                    VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1 //TODO fix it
                                });
                                Extension2.Add(new BAPIPAREX
                                {
                                    STRUCTURE = "0000000004",
                                    VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1 //TODO fix it
                                });
                            }
                            #endregion

                            #region ZFI_GL-MAPPING
                            var zfiGlMapping = accrualSetting.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING
                                .Where(x => x.COMP_CODE == companyCode);
                            if (zfiGlMapping.ToList().Count() == 0)
                            {
                                return null;
                            }
                            zfi_GL_MAPPING.Add(new ZFI_GL_MAPPING
                            {
                                ITEMNO_ACC = "0000000002",
                                TRAN_IND = zfiGlMapping.ToList()[0].TRAN_IND
                            });

                            if (isFreight)
                            {
                                zfi_GL_MAPPING.Add(new ZFI_GL_MAPPING
                                {
                                    ITEMNO_ACC = "0000000004",
                                    TRAN_IND = zfiGlMapping.ToList()[1].TRAN_IND
                                });
                            }
                            #endregion

                            AccrualService accrualService = new AccrualService();
                            sapFiDoc = accrualService.Post(jsonConfig, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING);

                            if (!String.IsNullOrEmpty(sapFiDoc))
                            {
                                numberOfSuccess++;
                                string matItemNo = outturn.MatItemNo;// GetMatItemNo(model.TripNo, outturn.ItemNo);
                                if (matItemNo == "")
                                {
                                    matItemNo = "10";
                                }

                                var rtn2 = UpdateSapFiDocNo(model.TripNo, matItemNo, outturn.ItemNo, sapFiDoc, "C");
                                if (!rtn2.Status)
                                {
                                    rtn.Message = "Send to SAP successfully but got th problem when updating SAP Fi Doc No";
                                }
                                if (!string.IsNullOrEmpty(sapFiDoc))
                                {
                                    var modalDaily = model.sOutturn.Where(p => p.ItemNo == outturn.ItemNo).ToList();
                                    if(modalDaily.Count > 0)
                                    {
                                        modalDaily.FirstOrDefault().SAPFIDoc = sapFiDoc.Length >= 10 ? sapFiDoc.Substring(0,10) : sapFiDoc;
                                        modalDaily.FirstOrDefault().SAPFIDocStatus = "C";
                                        modalDaily.FirstOrDefault().UpdatedBy = Const.User.Name;
                                        modalDaily.FirstOrDefault().UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy", cultureinfo);
                                    }
                                    //outturn.SAPFIDoc = sapFiDoc;
                                    //outturn.SAPFIDocStatus = "C";
                                    //outturn.UpdatedBy = Const.User.Name;
                                    //outturn.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy", cultureinfo);
                                }
                            }
                            else
                            {
                                numberOfFail++;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        numberOfFail++;
                    }
                }
            }

            rtn.Message = "Send to SAP completed. Success = " + numberOfSuccess + ". Fail = " + numberOfFail + ".";
            return rtn;
        }

        #region Get functions

        public ReturnValue GetOutturnData(PCFPurchaseEntryViewModel_SearchData pModel)
        {
            System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("en-US");
            string format = "dd/MM/yyyy";
            ReturnValue rtn = new ReturnValue();
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    #region OUTTURN_FROM_SAP

                    var query = (from o in context.OUTTURN_FROM_SAP
                                 where o.TRIP_NO == pModel.TripNo
                                 join p in context.PCF_MATERIAL_PRICE on pModel.TripNo equals p.PMP_TRIP_NO into viewA
                                 from va in viewA.DefaultIfEmpty()
                                 join f in context.PCF_FREIGHT on pModel.TripNo equals f.PFR_TRIP_NO into viewB
                                 from vb in viewB.DefaultIfEmpty()
                                 select new
                                 {
                                     dBlDate = o.BE_DATE,
                                     dBlQtyBbl = o.BL_QTY_BBL,
                                     dBlQtyMt = o.BL_QTY_MT,
                                     dBlQtyMl = o.BL_QTY_ML,
                                     dPostingDate = o.POSTING_DATE,
                                     dQtyBbl = o.QTY_BBL,
                                     dQtyMt = o.QTY_MT,
                                     dQtyMl = o.QTY_ML,
                                     dFOBPrice = va.PMP_TOTAL_PRICE,
                                     dPOFAmount = vb.PFR_AMOUNT
                                 });

                    if (query != null && query.Count() != 0)
                    {
                        pModel.sOutturn = new List<PCFPurchaseEntryViewModel_Outtern>();
                        string blDate = GetBLDateFromTripNo(pModel.TripNo);
                        int year = DateTime.ParseExact(blDate, "dd/MM/yyyy", cultureinfo).Year;
                        int month = DateTime.ParseExact(blDate, "dd/MM/yyyy", cultureinfo).Month;
                        int numberOfDays = DateTime.DaysInMonth(year, month);

                        for (var i = 1; i <= numberOfDays; i++)
                        {
                            pModel.sOutturn.Add(new PCFPurchaseEntryViewModel_Outtern
                            {
                                BLDate = "",
                                BlQtyBbl = "",
                                BlQtyMt = "",
                                BlQtyMl = "",
                                PostingDate = "",
                                RunningDate = i.ToString("00") + "/" + month.ToString("00") + "/" + year.ToString("0000"),
                                QtyBbl = "",
                                QtyMt = "",
                                QtyMl = "",
                                FOBPrice = "",
                                FreightPrice = "",
                                CnFPrice = "",
                                ROE = "",
                                FOBAmountUSD = "",
                                FOBAmountTHB = "",
                                FreightAmountUSD = "",
                                FreightAmountTHB = "",
                                CnFAmountUSD = "",
                                CnFAmountTHB = "",
                                CnFVat7 = "",
                                CnFTotalAmountUSD = "",
                                CnFTotalAmountTHB = "",
                                MatItemNo = ""
                            });
                        }

                        #region Calculate ROE

                        var roe = 35.11;
                        int devideNum = 0;
                        decimal sumValue = 0;

                        decimal startNum = 0;
                        decimal endNum = 0;
                        if (pModel.sOutturn.Count > 0)
                        {
                            DateTime startDate = DateTime.ParseExact(pModel.sOutturn[0].RunningDate, format, provider);
                            DateTime endDate = DateTime.ParseExact(pModel.sOutturn[pModel.sOutturn.Count - 1].RunningDate, format, provider);

                            if (endDate.Month < DateTime.Now.Month)
                            {
                                startNum = Convert.ToDecimal(startDate.ToString("yyyyMMdd", provider));
                                endNum = Convert.ToDecimal(endDate.ToString("yyyyMMdd", provider));
                            }
                            else
                            {
                                startNum = Convert.ToDecimal(startDate.ToString("yyyyMMdd", provider));
                                int numDev = 0;
                                if (DateTime.Now.DayOfWeek.ToString().ToUpper() == "MONDAY")
                                {
                                    numDev = 1;
                                }
                                else if (DateTime.Now.DayOfWeek.ToString().ToUpper() == "TUESDAY")
                                {
                                    numDev = 2;
                                }
                                else if (DateTime.Now.DayOfWeek.ToString().ToUpper() == "WEDNESDAY")
                                {
                                    numDev = 3;
                                }
                                else if (DateTime.Now.DayOfWeek.ToString().ToUpper() == "THURSDAY")
                                {
                                    numDev = 4;
                                }
                                else if (DateTime.Now.DayOfWeek.ToString().ToUpper() == "FRIDAY")
                                {
                                    numDev = 5;
                                }
                                else if (DateTime.Now.DayOfWeek.ToString().ToUpper() == "SATURDAY")
                                {
                                    numDev = 6;
                                }
                                endNum = Convert.ToDecimal(DateTime.Now.AddDays(-numDev).ToString("yyyyMMdd", provider));
                            }

                        }

                        //var queryROE = (from r in context.MKT_TRN_FX_B.AsEnumerable()
                        //                where r.T_FXB_VALTYPE == "M" && r.T_FXB_CUR == "USD" && r.T_FXB_ENABLED == "T"
                        //                && Convert.ToDecimal(r.T_FXB_VALDATE) >= startNum && Convert.ToDecimal(r.T_FXB_VALDATE) <= endNum
                        //                join s in context.MKT_TRN_FX_H on r.T_FXB_FXHID_FK equals s.T_FXH_ID into viewD
                        //                from vdl in viewD.DefaultIfEmpty()
                        //                where vdl.T_FXH_BNKCODE == "BOT"
                        //                select new
                        //                {
                        //                    dFXBValue = r.T_FXB_VALUE1,
                        //                    dValDate = r.T_FXB_VALDATE
                        //                });

                        var queryROE = (from r in context.MKT_TRN_FX_B.AsEnumerable()
                                        where r.T_FXB_VALTYPE == "M" && r.T_FXB_CUR == "USD" && r.T_FXB_ENABLED == "T"
                                        && Convert.ToDecimal(r.T_FXB_VALDATE) >= startNum && Convert.ToDecimal(r.T_FXB_VALDATE) <= endNum
                                        join s in context.MKT_TRN_FX_H.Where(p=>p.T_FXH_BNKCODE == "BOT") on r.T_FXB_FXHID_FK equals s.T_FXH_ID into viewD
                                        from vdl in viewD.DefaultIfEmpty()
                                        select new
                                        {
                                            dFXBValue = r.T_FXB_VALUE1,
                                            dValDate = r.T_FXB_VALDATE
                                        });

                        if (queryROE != null && queryROE.ToList().Count() > 0)
                        {
                            foreach (var item in queryROE)
                            {
                                if (item.dFXBValue != null)
                                {
                                    sumValue += Convert.ToDecimal(item.dFXBValue);
                                    devideNum++;
                                }
                            }

                            if (devideNum != 0)
                            {
                                roe = (Double)Math.Round(Convert.ToDecimal(sumValue / devideNum), 4, MidpointRounding.AwayFromZero);
                            }
                        }

                        #endregion


                        foreach (var item in query)
                        {
                            #region BL Date
                            string blDateToShow = "";
                            try
                            {
                                if (item.dBlDate != null)
                                    blDateToShow = (item.dBlDate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                                else
                                    blDateToShow = "";
                            }
                            catch
                            {
                                if (item.dBlDate != null)
                                    blDateToShow = (item.dBlDate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                                else
                                    blDateToShow = "";
                            }
                            #endregion

                            #region GR Date
                            string postingDateToShow = "";
                            try
                            {
                                if (item.dPostingDate != null)
                                    postingDateToShow = (item.dPostingDate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                                else
                                    postingDateToShow = "";
                            }
                            catch
                            {
                                if (item.dPostingDate != null)
                                    postingDateToShow = (item.dPostingDate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                                else
                                    postingDateToShow = "";
                            }
                            #endregion

                            if (!String.IsNullOrEmpty(blDateToShow))
                            {
                                var record = pModel.sOutturn.Where(x => DateTime.ParseExact(x.RunningDate, "dd/MM/yyyy", cultureinfo).Date == DateTime.ParseExact(blDateToShow, "dd/MM/yyyy", cultureinfo).Date)
                                                                        .OrderBy(x => x.BLDate).ThenBy(x => x.PostingDate).FirstOrDefault();
                                var record2 = pModel.sOutturn.Where(x => DateTime.ParseExact(x.RunningDate, "dd/MM/yyyy", cultureinfo).Date == DateTime.ParseExact(blDateToShow, "dd/MM/yyyy", cultureinfo).Date)
                                                                        .OrderBy(x => x.BLDate).ThenBy(x => x.PostingDate).ToList();

                                int index = pModel.sOutturn.IndexOf(record);

                                if (record != null)
                                {
                                    #region Calculate Amount & VAT

                                    decimal? fobPrice = GetFOBPrice(pModel.TripNo);
                                    decimal? freightPrice = GetFreightPrice(pModel.TripNo);
                                    decimal? cnfPrice = fobPrice + freightPrice;
                                    decimal? fobAmountUSD = 0;
                                    decimal? fobAmountTHB = 0;
                                    decimal? freightAmountUSD = 0;
                                    decimal? freightAmountTHB = 0;
                                    decimal? cnfAmountUSD = 0;
                                    decimal? cnfAmountTHB = 0;
                                    decimal? cnfVat7 = 0;
                                    decimal? cnfTotalAmountUSD = 0;
                                    decimal? cnfTotalAmountTHB = 0;

                                    if (String.IsNullOrEmpty(pModel.VolumeUnit) || pModel.VolumeUnit.ToLower() == "bbl")
                                    {
                                        if (String.IsNullOrEmpty(pModel.InvoiceFigure) || pModel.InvoiceFigure.ToLower() == "bl")
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                fobAmountUSD = (item.dBlQtyBbl * fobPrice) / Convert.ToDecimal(roe);
                                                freightAmountUSD = (item.dBlQtyBbl * freightPrice) / Convert.ToDecimal(roe);
                                            }
                                            else
                                            {
                                                fobAmountUSD = (item.dBlQtyBbl * fobPrice);
                                                freightAmountUSD = (item.dBlQtyBbl * freightPrice);
                                            }
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                fobAmountUSD = (item.dQtyBbl * fobPrice) / Convert.ToDecimal(roe);
                                                freightAmountUSD = (item.dQtyBbl * freightPrice) / Convert.ToDecimal(roe);
                                            }
                                            else
                                            {
                                                fobAmountUSD = (item.dQtyBbl * fobPrice);
                                                freightAmountUSD = (item.dQtyBbl * freightPrice);
                                            }
                                        }
                                    }
                                    else if (String.IsNullOrEmpty(pModel.VolumeUnit) || pModel.VolumeUnit.ToLower() == "mt")
                                    {
                                        if (String.IsNullOrEmpty(pModel.InvoiceFigure) || pModel.InvoiceFigure.ToLower() == "bl")
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                fobAmountUSD = (item.dBlQtyMt * fobPrice) / Convert.ToDecimal(roe);
                                                freightAmountUSD = (item.dBlQtyMt * freightPrice) / Convert.ToDecimal(roe);
                                            }
                                            else
                                            {
                                                fobAmountUSD = (item.dBlQtyMt * fobPrice);
                                                freightAmountUSD = (item.dBlQtyMt * freightPrice);
                                            }
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                fobAmountUSD = (item.dQtyMt * fobPrice) / Convert.ToDecimal(roe);
                                                freightAmountUSD = (item.dQtyMt * freightPrice) / Convert.ToDecimal(roe);
                                            }
                                            else
                                            {
                                                fobAmountUSD = (item.dQtyMt * fobPrice);
                                                freightAmountUSD = (item.dQtyMt * freightPrice);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(pModel.InvoiceFigure) || pModel.InvoiceFigure.ToLower() == "bl")
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                fobAmountUSD = (item.dBlQtyMl * fobPrice) / Convert.ToDecimal(roe);
                                                freightAmountUSD = (item.dBlQtyMl * freightPrice) / Convert.ToDecimal(roe);
                                            }
                                            else
                                            {
                                                fobAmountUSD = (item.dBlQtyMl * fobPrice);
                                                freightAmountUSD = (item.dBlQtyMl * freightPrice);
                                            }
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                fobAmountUSD = (item.dQtyMl * fobPrice) / Convert.ToDecimal(roe);
                                                freightAmountUSD = (item.dQtyMl * freightPrice) / Convert.ToDecimal(roe);
                                            }
                                            else
                                            {
                                                fobAmountUSD = (item.dQtyMl * fobPrice);
                                                freightAmountUSD = (item.dQtyMl * freightPrice);
                                            }
                                        }
                                    }
                                    roe = (Double)Math.Round(Convert.ToDecimal(roe), 4, MidpointRounding.AwayFromZero);
                                    fobAmountTHB = fobAmountUSD * Convert.ToDecimal(roe);
                                    freightAmountTHB = freightAmountUSD * Convert.ToDecimal(roe);
                                    fobAmountUSD = Math.Round(Convert.ToDecimal(fobAmountUSD), 4, MidpointRounding.AwayFromZero);
                                    fobAmountTHB = Math.Round(Convert.ToDecimal(fobAmountTHB), 4, MidpointRounding.AwayFromZero);
                                    freightAmountUSD = Math.Round(Convert.ToDecimal(freightAmountUSD), 4, MidpointRounding.AwayFromZero);
                                    freightAmountTHB = Math.Round(Convert.ToDecimal(freightAmountTHB), 4, MidpointRounding.AwayFromZero);
                                    cnfAmountUSD = Math.Round(Convert.ToDecimal(fobAmountUSD + freightAmountUSD), 4, MidpointRounding.AwayFromZero);
                                    cnfAmountTHB = Math.Round(Convert.ToDecimal(fobAmountTHB + freightAmountTHB), 4, MidpointRounding.AwayFromZero);
                                    cnfVat7 = cnfAmountTHB * Convert.ToDecimal(0.07);
                                    cnfTotalAmountUSD = cnfAmountUSD;
                                    cnfTotalAmountTHB = cnfAmountTHB + cnfVat7;

                                    cnfVat7 = Math.Round(Convert.ToDecimal(cnfVat7), 4, MidpointRounding.AwayFromZero);
                                    cnfTotalAmountUSD = Math.Round(Convert.ToDecimal(cnfTotalAmountUSD), 4, MidpointRounding.AwayFromZero);
                                    cnfTotalAmountTHB = Math.Round(Convert.ToDecimal(cnfTotalAmountTHB), 4, MidpointRounding.AwayFromZero);

                                    #endregion

                                    #region Fill data to the valid record

                                    if (String.IsNullOrEmpty(record.PostingDate))
                                    {
                                        record.BLDate = blDateToShow;
                                        record.BlQtyBbl = item.dBlQtyBbl.Value.ToString("#,##0.0000");
                                        record.BlQtyMt = item.dBlQtyMt.Value.ToString("#,##0.0000");
                                        record.BlQtyMl = item.dBlQtyMl.Value.ToString("#,##0.0000");
                                        record.PostingDate = postingDateToShow;
                                        record.QtyBbl = item.dQtyBbl.Value.ToString("#,##0.0000");
                                        record.QtyMt = item.dQtyMt.Value.ToString("#,##0.0000");
                                        record.QtyMl = item.dQtyMl.Value.ToString("#,##0.0000");
                                        record.FOBPrice = (fobPrice == null) ? "" : fobPrice.Value.ToString("#,##0.0000");
                                        record.FreightPrice = (freightPrice == null) ? "" : freightPrice.Value.ToString("#,##0.0000");
                                        record.CnFPrice = (cnfPrice == null) ? "" : cnfPrice.Value.ToString("#,##0.0000");
                                        record.ROE = roe.ToString("#,##0.0000");
                                        record.FOBAmountUSD = fobAmountUSD.Value.ToString("#,##0.0000");
                                        record.FOBAmountTHB = fobAmountTHB.Value.ToString("#,##0.0000");
                                        record.FreightAmountUSD = freightAmountUSD.Value.ToString("#,##0.0000");
                                        record.FreightAmountTHB = freightAmountTHB.Value.ToString("#,##0.0000");
                                        record.CnFAmountUSD = cnfAmountUSD.Value.ToString("#,##0.0000");
                                        record.CnFAmountTHB = cnfAmountTHB.Value.ToString("#,##0.0000");
                                        record.CnFVat7 = cnfVat7.Value.ToString("#,##0.0000");
                                        record.CnFTotalAmountUSD = cnfTotalAmountUSD.Value.ToString("#,##0.0000");
                                        record.CnFTotalAmountTHB = cnfTotalAmountTHB.Value.ToString("#,##0.0000");
                                        record.Valid = true;
                                    }
                                    else
                                    {
                                        bool foundExistingPostingDate = false;
                                        foreach (var item2 in record2)
                                        {
                                            if (DateTime.ParseExact(item2.PostingDate, "dd/MM/yyyy", cultureinfo) == item.dPostingDate)
                                            {
                                                foundExistingPostingDate = true;
                                            }
                                        }

                                        if (foundExistingPostingDate)
                                        {
                                            record.BlQtyBbl = (((String.IsNullOrEmpty(record.BlQtyBbl)) ? 0 : Convert.ToDecimal(record.BlQtyBbl)) + Convert.ToDecimal(item.dBlQtyBbl)).ToString("#,##0.0000");
                                            record.BlQtyMt = (((String.IsNullOrEmpty(record.BlQtyMt)) ? 0 : Convert.ToDecimal(record.BlQtyMt)) + Convert.ToDecimal(item.dBlQtyMt)).ToString("#,##0.0000");
                                            record.BlQtyMl = (((String.IsNullOrEmpty(record.BlQtyMl)) ? 0 : Convert.ToDecimal(record.BlQtyMl)) + Convert.ToDecimal(item.dBlQtyMl)).ToString("#,##0.0000");
                                            record.QtyBbl = (((String.IsNullOrEmpty(record.QtyBbl)) ? 0 : Convert.ToDecimal(record.QtyBbl)) + Convert.ToDecimal(item.dQtyBbl)).ToString("#,##0.0000");
                                            record.QtyMt = (((String.IsNullOrEmpty(record.QtyMt)) ? 0 : Convert.ToDecimal(record.QtyMt)) + Convert.ToDecimal(item.dQtyMt)).ToString("#,##0.0000");
                                            record.QtyMl = (((String.IsNullOrEmpty(record.QtyMl)) ? 0 : Convert.ToDecimal(record.QtyMl)) + Convert.ToDecimal(item.dQtyMl)).ToString("#,##0.0000");
                                            record.FOBAmountUSD = (((String.IsNullOrEmpty(record.FOBAmountUSD)) ? 0 : Convert.ToDecimal(record.FOBAmountUSD)) + Convert.ToDecimal(fobAmountUSD)).ToString("#,##0.0000");
                                            record.FOBAmountTHB = (((String.IsNullOrEmpty(record.FOBAmountTHB)) ? 0 : Convert.ToDecimal(record.FOBAmountTHB)) + Convert.ToDecimal(fobAmountTHB)).ToString("#,##0.0000");
                                            record.FreightAmountUSD = (((String.IsNullOrEmpty(record.FreightAmountUSD)) ? 0 : Convert.ToDecimal(record.FreightAmountUSD)) + Convert.ToDecimal(freightAmountUSD)).ToString("#,##0.0000");
                                            record.FreightAmountTHB = (((String.IsNullOrEmpty(record.FreightAmountTHB)) ? 0 : Convert.ToDecimal(record.FreightAmountTHB)) + Convert.ToDecimal(freightAmountTHB)).ToString("#,##0.0000");
                                            record.CnFAmountUSD = (((String.IsNullOrEmpty(record.CnFAmountUSD)) ? 0 : Convert.ToDecimal(record.CnFAmountUSD)) + Convert.ToDecimal(cnfAmountUSD)).ToString("#,##0.0000");
                                            record.CnFAmountTHB = (((String.IsNullOrEmpty(record.CnFAmountTHB)) ? 0 : Convert.ToDecimal(record.CnFAmountTHB)) + Convert.ToDecimal(cnfAmountTHB)).ToString("#,##0.0000");
                                            record.CnFTotalAmountUSD = (((String.IsNullOrEmpty(record.CnFTotalAmountUSD)) ? 0 : Convert.ToDecimal(record.CnFTotalAmountUSD)) + Convert.ToDecimal(cnfTotalAmountUSD)).ToString("#,##0.0000");
                                            record.CnFTotalAmountTHB = (((String.IsNullOrEmpty(record.CnFTotalAmountTHB)) ? 0 : Convert.ToDecimal(record.CnFTotalAmountTHB)) + Convert.ToDecimal(cnfTotalAmountTHB)).ToString("#,##0.0000");
                                        }
                                        else //Found existing BL Date but difference Posting Date, insert the new record
                                        {
                                            pModel.sOutturn.Insert(index + 1, new PCFPurchaseEntryViewModel_Outtern
                                            {
                                                BLDate = blDateToShow,
                                                BlQtyBbl = item.dBlQtyBbl.Value.ToString("#,##0.0000"),
                                                BlQtyMt = item.dBlQtyMt.Value.ToString("#,##0.0000"),
                                                BlQtyMl = item.dBlQtyMl.Value.ToString("#,##0.0000"),
                                                PostingDate = postingDateToShow,
                                                RunningDate = blDateToShow,
                                                QtyBbl = item.dQtyBbl.Value.ToString("#,##0.0000"),
                                                QtyMt = item.dQtyMt.Value.ToString("#,##0.0000"),
                                                QtyMl = item.dQtyMl.Value.ToString("#,##0.0000"),
                                                FOBPrice = (fobPrice == null) ? "" : fobPrice.Value.ToString("#,##0.0000"),
                                                FreightPrice = (freightPrice == null) ? "" : freightPrice.Value.ToString("#,##0.0000"),
                                                CnFPrice = (cnfPrice == null) ? "" : cnfPrice.Value.ToString("#,##0.0000"),
                                                ROE = roe.ToString("#,##0.0000"),
                                                FOBAmountUSD = fobAmountUSD.Value.ToString("#,##0.0000"),
                                                FOBAmountTHB = fobAmountTHB.Value.ToString("#,##0.0000"),
                                                FreightAmountUSD = freightAmountUSD.Value.ToString("#,##0.0000"),
                                                FreightAmountTHB = freightAmountTHB.Value.ToString("#,##0.0000"),
                                                CnFAmountUSD = cnfAmountUSD.Value.ToString("#,##0.0000"),
                                                CnFAmountTHB = cnfAmountTHB.Value.ToString("#,##0.0000"),
                                                CnFVat7 = cnfVat7.Value.ToString("#,##0.0000"),
                                                CnFTotalAmountUSD = cnfTotalAmountUSD.Value.ToString("#,##0.0000"),
                                                CnFTotalAmountTHB = cnfTotalAmountTHB.Value.ToString("#,##0.0000"),
                                                Valid = true
                                            });
                                        }
                                    }

                                    #endregion

                                }

                            }

                        }
                        int itemNo = 1;
                        foreach (var outturn in pModel.sOutturn)
                        {
                            outturn.ItemNo = itemNo.ToString();
                            itemNo++;
                        }

                        foreach (var outturn in pModel.sOutturn)
                        {
                            //outturn.MatItemNo = GetMatItemNo(pModel.TripNo, outturn.ItemNo);
                            //string matItemNo = outturn.MatItemNo;
                            //if (matItemNo == "")
                            //{
                            //    matItemNo = "0";
                            //}
                            //Decimal matItemNoToCompare = Convert.ToDecimal(matItemNo);

                            string theItemNo = outturn.ItemNo;
                            if (theItemNo == "")
                            {
                                theItemNo = "0";
                            }
                            Decimal itemNoToCompate = Convert.ToDecimal(theItemNo);

                            var queryForSapFiDoc = (from p in context.PCF_PURCHASE_ENTRY_DAILY
                                                    where p.TRIP_NO == pModel.TripNo && p.ITEM_NO == itemNoToCompate
                                                    select new
                                                    {
                                                        dBlQtyBbl = p.BL_VOLUME_BBL,
                                                        dBlQtyMt = p.BL_VOLUME_MT,
                                                        dBlQtyMl = p.BL_VOLUME_L30,

                                                        dQtyBbl = p.GR_VOLUME_BBL,
                                                        dQtyMt = p.GR_VOLUME_MT,
                                                        dQtyMl = p.GR_VOLUME_L30,

                                                        dFOBPrice = p.FOB_PRICE,
                                                        dFreightPrice = p.FRT_PRICE,
                                                        dCnFPrice = p.CF_PRICE,
                                                        dROE = p.ROE,
                                                        dFOBAmountUSD = p.FOB_AMOUNT_USD,
                                                        dFOBAmountTHB = p.FOB_AMOUNT_THB,
                                                        dFreightAmountUSD = p.FRT_AMOUNT_USD,
                                                        dFreightAmountTHB = p.FRT_AMOUNT_THB,
                                                        dCnFAmountUSD = p.CF_AMOUNT_USD,
                                                        dCnFAmountTHB = p.CF_AMOUNT_THB,
                                                        dCnFVat7 = p.CF_VAT,
                                                        dCnFTotalAmountUSD = p.CF_TOTAL_USD,
                                                        dCnFTotalAmountTHB = p.CF_TOTAL_THB,

                                                        dMatItmeNo = p.MAT_ITEM_NO,
                                                        dItemNo = p.ITEM_NO,
                                                        dSapFiDocNo = p.SAP_FI_DOC_NO,
                                                        dSapFiDocStatus = p.SAP_FI_DOC_STATUS,
                                                        dLastUpdate = p.UPDATED_DATE,
                                                        dUpdatedBy = p.UPDATED_BY
                                                    });
                            if (queryForSapFiDoc != null && queryForSapFiDoc.ToList().Count() > 0)
                            {
                                outturn.MatItemNo = queryForSapFiDoc.ToList()[0].dMatItmeNo.ToString();
                                outturn.SAPFIDoc = queryForSapFiDoc.ToList()[0].dSapFiDocNo;
                                outturn.SAPFIDocStatus = queryForSapFiDoc.ToList()[0].dSapFiDocStatus;
                                if (outturn.SAPFIDocStatus == "C")
                                {
                                    outturn.BlQtyBbl = (queryForSapFiDoc.ToList()[0].dBlQtyBbl ?? 0m).ToString("#,##0.0000");
                                    outturn.BlQtyMt = (queryForSapFiDoc.ToList()[0].dBlQtyMt ?? 0m).ToString("#,##0.0000");
                                    outturn.BlQtyMl = (queryForSapFiDoc.ToList()[0].dBlQtyMl ?? 0m).ToString("#,##0.0000");

                                    outturn.QtyBbl = (queryForSapFiDoc.ToList()[0].dQtyBbl ?? 0m).ToString("#,##0.0000");
                                    outturn.QtyMt = (queryForSapFiDoc.ToList()[0].dQtyMt ?? 0m).ToString("#,##0.0000");
                                    outturn.QtyMl = (queryForSapFiDoc.ToList()[0].dQtyMl ?? 0m).ToString("#,##0.0000");


                                    outturn.FOBPrice = (queryForSapFiDoc.ToList()[0].dFOBPrice ?? 0m).ToString("#,##0.0000");
                                    outturn.FreightPrice = (queryForSapFiDoc.ToList()[0].dFreightPrice ?? 0m).ToString("#,##0.0000");
                                    outturn.CnFPrice = (queryForSapFiDoc.ToList()[0].dCnFPrice ?? 0m).ToString("#,##0.0000");
                                    outturn.ROE = (queryForSapFiDoc.ToList()[0].dROE ?? 0m).ToString("#,##0.0000");
                                    outturn.FOBAmountUSD = (queryForSapFiDoc.ToList()[0].dFOBAmountUSD ?? 0m).ToString("#,##0.0000");
                                    outturn.FOBAmountTHB = (queryForSapFiDoc.ToList()[0].dFOBAmountTHB ?? 0m).ToString("#,##0.0000");
                                    outturn.FreightAmountUSD = (queryForSapFiDoc.ToList()[0].dFreightAmountUSD ?? 0m).ToString("#,##0.0000");
                                    outturn.FreightAmountTHB = (queryForSapFiDoc.ToList()[0].dFreightAmountTHB ?? 0m).ToString("#,##0.0000");
                                    outturn.CnFAmountUSD = (queryForSapFiDoc.ToList()[0].dCnFAmountUSD ?? 0m).ToString("#,##0.0000");
                                    outturn.CnFAmountTHB = (queryForSapFiDoc.ToList()[0].dCnFAmountTHB ?? 0m).ToString("#,##0.0000");
                                    outturn.CnFVat7 = (queryForSapFiDoc.ToList()[0].dCnFVat7).ToString("#,##0.0000");
                                    outturn.CnFTotalAmountUSD = (queryForSapFiDoc.ToList()[0].dCnFTotalAmountUSD ?? 0m).ToString("#,##0.0000");
                                    outturn.CnFTotalAmountTHB = (queryForSapFiDoc.ToList()[0].dCnFTotalAmountTHB ?? 0m).ToString("#,##0.0000");
                                }
                                string updateDate = "";
                                if (queryForSapFiDoc.ToList()[0].dLastUpdate != null)
                                {
                                    updateDate = (queryForSapFiDoc.ToList()[0].dLastUpdate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                                }
                                outturn.UpdatedDate = updateDate;
                                outturn.UpdatedBy = queryForSapFiDoc.ToList()[0].dUpdatedBy;
                            }

                        }

                        rtn.Status = true;
                        rtn.Message = "GetOutturnData() succeed";
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "No outturn data.";
                    }

                    #endregion

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return rtn;
        }

        public void GetPcfPurchaseEntryDaily(PCFPurchaseEntryViewModel_SearchData pModel)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                var queryPPE = (from r in context.PCF_PURCHASE_ENTRY_DAILY
                                where r.TRIP_NO == pModel.TripNo
                                select new
                                {
                                    dBLDate = r.BL_DATE,
                                    dGRDate = r.GR_DATE,
                                    dFOBPRICE = r.FOB_PRICE,
                                    dFRTPRICE = r.FRT_PRICE,
                                    dPrice = r.CF_PRICE,
                                    dROE = r.ROE,
                                    dAmountUSD = r.CF_AMOUNT_USD,
                                    dAmountTHB = r.CF_AMOUNT_THB,
                                    dVAT = r.CF_VAT,
                                    dTotalUSD = r.CF_TOTAL_USD,
                                    dTotalTHB = r.CF_TOTAL_THB
                                });
                if (queryPPE != null && queryPPE.Count() != 0)
                {
                    foreach (var item in queryPPE)
                    {
                        foreach (var outturn in pModel.sOutturn)
                        {
                            if (outturn.Valid)
                            {
                                DateTime blDate = DateTime.ParseExact(outturn.BLDate, "dd/MM/yyyy", cultureinfo);
                                DateTime grDate = DateTime.ParseExact(outturn.PostingDate, "dd/MM/yyyy", cultureinfo);
                                DateTime blDateTemp = DateTime.Now.AddYears(-200);
                                if (item.dBLDate != null)
                                    blDateTemp = (item.dBLDate ?? DateTime.Now);

                                DateTime grDateTemp = DateTime.Now.AddYears(-200);
                                if (item.dGRDate != null)
                                    grDateTemp = (item.dGRDate ?? DateTime.Now);

                                if (blDate.Date == blDateTemp.Date && grDate.Date == grDateTemp.Date)
                                {
                                    outturn.CnFPrice = item.dPrice.Value.ToString("#,##0.0000");
                                    outturn.ROE = item.dROE.Value.ToString("#,##0.0000");
                                    outturn.FOBPrice = item.dFOBPRICE.ToString();
                                    outturn.FreightPrice = item.dFRTPRICE.ToString();
                                    outturn.CnFAmountUSD = item.dAmountUSD.Value.ToString("#,##0.0000");
                                    outturn.CnFAmountTHB = item.dAmountTHB.Value.ToString("#,##0.0000");
                                    outturn.CnFVat7 = item.dVAT.ToString("#,##0.0000");
                                    outturn.CnFTotalAmountUSD = item.dTotalUSD.Value.ToString("#,##0.0000");
                                    outturn.CnFTotalAmountTHB = item.dTotalTHB.Value.ToString("#,##0.0000");

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        public string GetBLDateFromTripNo(string tripNo)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from o in context.OUTTURN_FROM_SAP
                             where o.TRIP_NO == tripNo
                             select new
                             {
                                 dBlDate = o.BE_DATE
                             });

                if (query != null && query.Count() != 0)
                {
                    if (query.ToList()[0].dBlDate != null)
                        return (query.ToList()[0].dBlDate ?? DateTime.Now).ToString("dd/MM/yyyy", cultureinfo);
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
        }

        public decimal? GetFOBPrice(string tripNo)
        {
            decimal tempPrice = 0;
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from p in context.PCF_MATERIAL_PRICE
                             where p.PMP_TRIP_NO == tripNo
                             group p.PMP_ITEM_NO by p.PMP_MAT_ITEM_NO into g
                             select new
                             {
                                 dKey = g.Key,
                                 dList = g.ToList()
                             });
                var itemNos = query.ToList();
                var maxItemNo = itemNos[0].dList.Max();

                var query2 = (from p in context.PCF_MATERIAL_PRICE
                              where p.PMP_TRIP_NO == tripNo && p.PMP_ITEM_NO == maxItemNo
                              select new
                              {
                                  dTotalPrice = p.PMP_TOTAL_PRICE
                              });

                if (query2.ToList()[0] != null)
                    if (query2.ToList()[0].dTotalPrice != null)
                    {
                        tempPrice = query2.ToList()[0].dTotalPrice.Value;
                    }
                return Math.Round(tempPrice, 4, MidpointRounding.AwayFromZero);
            }
        }

        public decimal? GetFreightPrice(string tripNo)
        {
            decimal? sumVolumeBBL = 0;
            decimal? amount = 0;
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from p in context.PCF_MATERIAL
                             where p.PMA_TRIP_NO == tripNo
                             select new
                             {
                                 dVolumeBBL = p.PMA_VOLUME_BBL
                             });
                if (query != null)
                {
                    var volumeBBLs = query.ToList();
                    sumVolumeBBL = volumeBBLs.Sum(item => item.dVolumeBBL);
                }
                else
                {
                    return null;
                }

                var query2 = (from f in context.PCF_FREIGHT
                              where f.PFR_TRIP_NO == tripNo
                              select new
                              {
                                  dAmount = f.PFR_AMOUNT
                              });
                if (query2 != null && query2.ToList().Count() > 0)
                {
                    amount = query2.ToList()[0].dAmount;
                }
                else
                {
                    return 0;
                }

                return amount / sumVolumeBBL;
            }
        }

        public PCFAccrualSetting GetAccrualSettings()
        {
            string JsonD = MasterData.GetJsonMasterSetting("CIP_ACCRUAL_POSTING_CONFIG");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);

            PCFAccrualSetting myDeserializedObjList = (PCFAccrualSetting)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(PCFAccrualSetting));
            return myDeserializedObjList;
        }

        public string GetCompanyCode(string tripNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from c in context.PCF_HEADER
                                 where c.PHE_TRIP_NO == tripNo
                                 select new
                                 {
                                     dCompanyCode = c.PHE_COMPANY_CODE
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        return query.ToList()[0].dCompanyCode;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetSortFieldFromSupplier(string accNumVendor)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_VENDOR
                                 where p.VND_ACC_NUM_VENDOR.Contains(accNumVendor)
                                 select new
                                 {
                                     dSortField = p.VND_SORT_FIELD
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        return query.ToList()[0].dSortField;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetIOFromMetNum(string metNum, string companyCode)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_MATERIALS
                                 where p.MET_NUM == metNum
                                 select new
                                 {
                                     dIO = p.MET_IO,
                                     dIO_TLB = p.MET_IO_TLB
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        if (companyCode == "1100")
                            return query.ToList()[0].dIO;
                        else
                            return query.ToList()[0].dIO_TLB;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMetMatDesEnglishFromMetNum(string metNum)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_MATERIALS
                                 where p.MET_NUM == metNum
                                 select new
                                 {
                                     dMetMatDesEnglish = p.MET_MAT_DES_ENGLISH
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        return query.ToList()[0].dMetMatDesEnglish;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMonthName(int month)
        {
            switch (month)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "Jun";
                case 7: return "Jul";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "";
            }
        }

        public string GetVendorFromTripNo(string tripNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PCF_FREIGHT
                                 where p.PFR_TRIP_NO == tripNo
                                 select new
                                 {
                                     dSupplier = p.PFR_SUPPLIER
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        return query.ToList()[0].dSupplier;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMatItemNo(string tripNo, string itemNo)
        {
            try
            {
                var itemNoToCompare = Convert.ToDecimal(itemNo);
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PCF_MATERIAL
                                 where p.PMA_TRIP_NO == tripNo && p.PMA_ITEM_NO == itemNoToCompare
                                 select new
                                 {
                                     dMatItemNo = p.PMA_PO_MAT_ITEM
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        return query.ToList()[0].dMatItemNo.ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string GetPurchaseType(string code)
        {
            try
            {

                var _CIPConfig = CrudeImportPlanServiceModel.getCIPConfig("CIP_IMPORT_PLAN");
                if (_CIPConfig != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var purchaseType = js.Serialize(_CIPConfig.PCF_MT_PURCHASE_TYPE);
                    var dataList = (List<PCF_MT_PURCHASE_TYPE>)Newtonsoft.Json.JsonConvert.DeserializeObject(purchaseType, typeof(List<PCF_MT_PURCHASE_TYPE>));
                    var result = dataList.Where(x => x.CODE == code);

                    if (result != null && result.ToList().Count() != 0)
                    {
                        return result.ToList()[0].NAME;
                    }
                    else
                    {
                        return "N/A";
                    }
                }
                else
                {
                    return "N/A";
                }
            }
            catch (Exception ex)
            {
                return "N/A";
            }
        }

        #endregion

        #region DB functions

        public ReturnValue Save(PCFPurchaseEntryViewModel_SearchData model)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            bool haveSelectedItems = false;
            bool problemWhenSave = false;

            Calculate(model);

            ReturnValue result = new ReturnValue();
            if (model.sOutturn == null || model.sOutturn.Count() == 0)
            {
                model.sOutturn = new List<PCFPurchaseEntryViewModel_Outtern>();
                result.Status = false;
                result.Message = "Empty Outturn";
                return result;
            }

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                PCF_PURCHASE_ENTRY_DAILY_DAL ppeDal = new PCF_PURCHASE_ENTRY_DAILY_DAL();
                PCF_PURCHASE_ENTRY_DAILY ped = new PCF_PURCHASE_ENTRY_DAILY();
                PIT_PURCHASE_ENTRY ppe = new PIT_PURCHASE_ENTRY();
                PRICING_TO_SAP_DAL ptsDal = new PRICING_TO_SAP_DAL();
                PRICING_TO_SAP pts = new PRICING_TO_SAP();

                foreach (var m in model.sOutturn)
                {
                    if (m.Select)
                    {
                        haveSelectedItems = true;

                        try
                        {

                            #region Save to PCF_PURCHASE_ENTRY_DAILY

                            try
                            {
                                ped.TRIP_NO = model.TripNo;
                                ped.ITEM_NO = Convert.ToDecimal(m.ItemNo);
                                ped.MAT_ITEM_NO = Decimal.Parse(model.MatItemNo);
                                ped.PO_DATE = DateTime.ParseExact(m.BLDate, "dd/MM/yyyy", cultureinfo);
                                ped.BL_DATE = DateTime.ParseExact(m.BLDate, "dd/MM/yyyy", cultureinfo);
                                ped.BL_VOLUME_BBL = Convert.ToDecimal(m.BlQtyBbl);
                                ped.BL_VOLUME_MT = Convert.ToDecimal(m.BlQtyMt);
                                ped.BL_VOLUME_L30 = Convert.ToDecimal(m.BlQtyMl);
                                ped.GR_DATE = DateTime.ParseExact(m.PostingDate, "dd/MM/yyyy", cultureinfo);
                                ped.GR_VOLUME_BBL = Convert.ToDecimal(m.QtyBbl);
                                ped.GR_VOLUME_MT = Convert.ToDecimal(m.QtyMt);
                                ped.GR_VOLUME_L30 = Convert.ToDecimal(m.QtyMl);
                                ped.FOB_PRICE = Convert.ToDecimal(m.FOBPrice);
                                ped.FRT_PRICE = Convert.ToDecimal(m.FreightPrice);
                                ped.CF_PRICE = Convert.ToDecimal(m.CnFPrice);
                                ped.ROE = Convert.ToDecimal(m.ROE);
                                ped.FOB_AMOUNT_USD = Convert.ToDecimal(m.FOBAmountUSD);
                                ped.FOB_AMOUNT_THB = Convert.ToDecimal(m.FOBAmountTHB);
                                ped.FRT_AMOUNT_USD = Convert.ToDecimal(m.FreightAmountUSD);
                                ped.FRT_AMOUNT_THB = Convert.ToDecimal(m.FreightAmountTHB);
                                ped.CF_AMOUNT_USD = Convert.ToDecimal(m.CnFAmountUSD);
                                ped.CF_AMOUNT_THB = Convert.ToDecimal(m.CnFAmountTHB);
                                ped.CF_VAT = Convert.ToDecimal(m.CnFVat7);
                                ped.CF_TOTAL_USD = Convert.ToDecimal(m.CnFTotalAmountUSD);
                                ped.CF_TOTAL_THB = Convert.ToDecimal(m.CnFTotalAmountTHB);
                                ped.CAL_VOLUME_UNIT = model.VolumeUnit;
                                ped.CAL_INVOICE_FIGURE = model.InvoiceFigure;
                                ped.CAL_PRICE_UNIT = model.PriceUnit;
                                ped.CREATED_DATE = DateTime.Now;
                                ped.CREATED_BY = Const.User.Name;
                                ped.UPDATED_DATE = DateTime.Now;
                                ped.UPDATED_BY = Const.User.Name;
                                ppeDal.Save(ped);

                                m.UpdatedBy = Const.User.Name;
                                m.UpdatedDate = DateTime.Now.ToString("dd/MM/yyyy", cultureinfo);
                            }
                            catch (Exception ex)
                            {

                            }

                            #endregion

                            #region Save to PRICING_TO_SAP

                            try
                            {
                                string volumnToUse = "";

                                if (model.InvoiceFigure != "Outturn") //B/L
                                {
                                    if (model.VolumeUnit.ToLower() == "bbl")
                                    {
                                        volumnToUse = m.BlQtyBbl;
                                    }
                                    else if (model.VolumeUnit.ToLower() == "mt")
                                    {
                                        volumnToUse = m.BlQtyMt;
                                    }
                                    else
                                    {
                                        volumnToUse = m.BlQtyMl;
                                    }
                                }
                                else
                                {
                                    if (model.VolumeUnit.ToLower() == "bbl")
                                    {
                                        volumnToUse = m.QtyBbl;
                                    }
                                    else if (model.VolumeUnit.ToLower() == "mt")
                                    {
                                        volumnToUse = m.QtyMt;
                                    }
                                    else
                                    {
                                        volumnToUse = m.QtyMl;
                                    }
                                }

                                pts.TRANS_TYPE = "C";
                                pts.COM_CODE = GetCompanyCode(model.TripNo);
                                pts.TRIP_NO = model.TripNo;
                                pts.CREATE_DATE = DateTime.Now;
                                try
                                {
                                    pts.CMCS_ETA = DateTime.ParseExact(m.RunningDate, "dd/MM/yyyy", cultureinfo);
                                }
                                catch
                                {
                                    pts.CMCS_ETA = DateTime.Now;
                                }
                                pts.MAT_NUM = model.MetNum;
                                pts.PARTLY_ORDER = Convert.ToDecimal(m.ItemNo);
                                string supplierNo = Convert.ToInt32(model.SupplierNo).ToString();
                                pts.VENDOR_NO = supplierNo;
                                pts.PLANT = "1200";
                                pts.GROUP_LOCATION = "REFINERY";
                                pts.EXCHANGE_RATE = Convert.ToDecimal(m.ROE);
                                pts.BASE_PRICE = Convert.ToDecimal(m.CnFPrice);
                                pts.PREMIUM_OSP = 0;
                                pts.PREMIUM_MARKET = 0;
                                pts.OTHER_COST = 0;
                                pts.FREIGHT_PRICE = 0;
                                pts.INSURANCE_PRICE = 0;
                                pts.TOTAL_AMOUNT_THB = Convert.ToDecimal(m.CnFAmountTHB);
                                pts.TOTAL_AMOUNT_USD = Convert.ToDecimal(m.CnFAmountUSD);
                                pts.QTY = Convert.ToDecimal(volumnToUse);
                                pts.QTY_UNIT = model.VolumeUnit;
                                pts.CURRENCY = model.PriceUnit;
                                //pts.CURRENCY = model.PitPurchaseEntry.CalPriceUnit;
                                pts.SAP_RECEIVED_DATE = null;
                                pts.STATUS = null;
                                pts.GROUP_DATA = 1;
                                pts.FLAG_PLANNING = "";
                                try
                                {
                                    pts.DUE_DATE = DateTime.ParseExact(model.sOutturn[model.sOutturn.Count() - 1].RunningDate, "dd/MM/yyyy", cultureinfo);
                                }
                                catch
                                {
                                    pts.DUE_DATE = DateTime.Now;
                                }
                                pts.CODE = 111;
                                pts.REMARK = null;
                                ptsDal.Save(pts);

                                using (EntityCPAIEngine context2 = new EntityCPAIEngine())
                                {
                                    context2.Database.ExecuteSqlCommand("CALL P_PIT_INSERT_PRICE_0('" + model.TripNo + "')");
                                }
                            }
                            catch (Exception ex)
                            {
                                result.Message = ex.Message;
                            }

                            #endregion

                        }
                        catch (Exception ex)
                        {
                            problemWhenSave = true;
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(m.SAPFIDoc) && string.IsNullOrEmpty(m.SAPFIDocStatus))
                        {
                            // Remove from pit_purchase_entry_daily if not-ticked item already existed in the table
                            ped.TRIP_NO = model.TripNo;
                            ped.ITEM_NO = Convert.ToDecimal(m.ItemNo);

                            ppeDal.Remove(ped);
                        }
                    }
                }
            }

            if (haveSelectedItems)
            {
                if (problemWhenSave)
                {
                    result.Status = false;
                    result.Message = "Some error(s) occurred when saving the data";
                }
                else
                {
                    result.Status = true;
                    result.Message = "Save completed";

                }
            }
            else
            {
                result.Status = true;
                result.Message = "Please choose the items to save";
            }

            using (EntityCPAIEngine context2 = new EntityCPAIEngine())
            {
                context2.Database.ExecuteSqlCommand("CALL p_pcf_train_insert_price_0('" + model.TripNo + "')");
            }

            return result;
        }

        public ReturnValue UpdateSapFiDocNo(string tripNo, string matItemNo, string itemNo, string sapFiDocNo, string type)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                PCF_PURCHASE_ENTRY_DAILY_DAL ppeDal = new PCF_PURCHASE_ENTRY_DAILY_DAL();
                PCF_PURCHASE_ENTRY_DAILY ppe = new PCF_PURCHASE_ENTRY_DAILY();

                try
                {
                    ppeDal.UpdateSapFiDocNo(tripNo, matItemNo, itemNo, sapFiDocNo, Const.User.Name, type);

                    rtn.Status = true;
                    rtn.Message = "Update SAP Fi Doc No successfully";
                    return rtn;
                }
                catch (Exception ex)
                {
                    rtn.Status = false;
                    rtn.Message = "Error: " + ex.Message;
                    return rtn;
                }

            }
        }

        #endregion

        #region Export to Excel functions

        public string ExportToExcel(PCFPurchaseEntryViewModel_SearchData model)
        {
            System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("en-US");
            DataTable dtData = new DataTable();
            string msg = "";

            String xFileName = "Temp_PCF_Purchase_Entry_Report";
            String lFileName = ".xlsx";
            String path = System.Web.HttpContext.Current.Server.MapPath(@"..\..\Areas\CPAIMVC\Report\ExcelTemplate\");
            String newFilename = xFileName.Replace("Temp_", "") + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss", provider) + lFileName;
            String newExcelFileNameFullPath = System.Web.HttpContext.Current.Server.MapPath(@"..\..\Areas\CPAIMVC\Report\Temp\") + newFilename;

            string filePath = "c:\\temp\\";
            FileInfo excelFile = new FileInfo(filePath);

            try
            {
                System.IO.File.Copy(path + "Temp_PCF_Purchase_Entry_Report" + lFileName, newExcelFileNameFullPath, true);
            }
            catch (Exception ex)
            {
                msg = "error: 1_" + ex.Message.ToString();
                goto Next_Line_Exit;
            }

            xFileName = newFilename;
            int irows = 0;

            try
            {
                ExcelPackage excelApp = new ExcelPackage();
                ExcelWorksheet excelSheets;

                try
                {
                    excelApp = new ExcelPackage(new System.IO.FileInfo(newExcelFileNameFullPath));
                    excelSheets = (ExcelWorksheet)excelApp.Workbook.Worksheets[1];

                    irows = 2;
                    excelSheets.Cells[irows, 1].Value = "Trip No : " + model.TripNo;

                    Double bl_volume_bbl = 0;
                    Double bl_volume_mt = 0;
                    Double bl_volume_l30 = 0;
                    Double gr_volume_bbl = 0;
                    Double gr_volume_mt = 0;
                    Double gr_volume_l30 = 0;

                    Double foa_usd = 0;
                    Double foa_thb = 0;
                    Double fra_usd = 0;
                    Double fra_thb = 0;
                    Double cfa_usd = 0;
                    Double cfa_thb = 0;
                    Double cf_vat_thb = 0;
                    Double cf_total_usd = 0;
                    Double cf_total_thb = 0;

                    irows = 6;
                    int runningNo = 0;

                    for (int i = 0; i < model.sOutturn.Count(); i++)
                    {
                        if (!model.sOutturn[i].Valid)
                        {
                            continue;
                        }

                        irows += 1;
                        try
                        {
                            CopyRowTo(excelSheets, excelSheets, 6, irows);
                        }
                        catch { }

                        #region "detail"  
                        excelSheets.Cells[irows, 1].Value = (runningNo + 1).ToString() + "";
                        excelSheets.Cells[irows, 2].Value = model.sOutturn[i].RunningDate.ToString() + "";
                        excelSheets.Cells[irows, 3].Value = model.sOutturn[i].BlQtyBbl.ToString() + "";
                        excelSheets.Cells[irows, 4].Value = model.sOutturn[i].BlQtyMt.ToString() + "";
                        excelSheets.Cells[irows, 5].Value = model.sOutturn[i].BlQtyMl.ToString() + "";
                        excelSheets.Cells[irows, 6].Value = model.sOutturn[i].PostingDate.ToString() + "";
                        excelSheets.Cells[irows, 7].Value = model.sOutturn[i].QtyBbl.ToString().ToString() + "";
                        excelSheets.Cells[irows, 8].Value = model.sOutturn[i].QtyMt.ToString().ToString() + "";
                        excelSheets.Cells[irows, 9].Value = model.sOutturn[i].QtyMl.ToString().ToString() + "";
                        excelSheets.Cells[irows, 10].Value = model.sOutturn[i].FOBPrice.ToString().ToString() + "";
                        excelSheets.Cells[irows, 11].Value = model.sOutturn[i].FreightPrice.ToString().ToString() + "";
                        excelSheets.Cells[irows, 12].Value = model.sOutturn[i].CnFPrice.ToString().ToString() + "";
                        excelSheets.Cells[irows, 13].Value = model.sOutturn[i].ROE.ToString().ToString() + "";
                        excelSheets.Cells[irows, 14].Value = model.sOutturn[i].FOBAmountUSD.ToString().ToString() + "";
                        excelSheets.Cells[irows, 15].Value = model.sOutturn[i].FOBAmountTHB.ToString().ToString() + "";
                        excelSheets.Cells[irows, 16].Value = model.sOutturn[i].FreightAmountUSD.ToString().ToString() + "";
                        excelSheets.Cells[irows, 17].Value = model.sOutturn[i].FreightAmountTHB.ToString().ToString() + "";
                        excelSheets.Cells[irows, 18].Value = model.sOutturn[i].CnFAmountUSD.ToString().ToString() + "";
                        excelSheets.Cells[irows, 19].Value = model.sOutturn[i].CnFAmountTHB.ToString().ToString() + "";
                        excelSheets.Cells[irows, 20].Value = model.sOutturn[i].CnFVat7.ToString().ToString() + "";
                        excelSheets.Cells[irows, 21].Value = model.sOutturn[i].CnFTotalAmountUSD.ToString().ToString() + "";
                        excelSheets.Cells[irows, 22].Value = model.sOutturn[i].CnFTotalAmountTHB.ToString().ToString() + "";
                        excelSheets.Cells[irows, 23].Value = String.IsNullOrEmpty(model.sOutturn[i].SAPFIDoc) ? "" : model.sOutturn[i].SAPFIDoc.ToString().ToString() + "";
                        excelSheets.Cells[irows, 24].Value = String.IsNullOrEmpty(model.sOutturn[i].SAPFIDocStatus) ? "" : model.sOutturn[i].SAPFIDocStatus.ToString().ToString() + "";
                        excelSheets.Cells[irows, 25].Value = String.IsNullOrEmpty(model.sOutturn[i].UpdatedDate) ? "" : model.sOutturn[i].UpdatedDate.ToString().ToString() + "";
                        excelSheets.Cells[irows, 26].Value = String.IsNullOrEmpty(model.sOutturn[i].UpdatedBy) ? "" : model.sOutturn[i].UpdatedBy.ToString().ToString() + "";

                        bl_volume_bbl += CnDoubleRe0(model.sOutturn[i].BlQtyBbl.ToString().ToString().Replace(",", ""));
                        bl_volume_l30 += CnDoubleRe0(model.sOutturn[i].BlQtyMl.ToString().ToString().Replace(",", ""));
                        bl_volume_mt += CnDoubleRe0(model.sOutturn[i].BlQtyMt.ToString().ToString().Replace(",", ""));
                        gr_volume_bbl += CnDoubleRe0(model.sOutturn[i].QtyBbl.ToString().ToString().Replace(",", ""));
                        gr_volume_mt += CnDoubleRe0(model.sOutturn[i].QtyMt.ToString().ToString().Replace(",", ""));
                        gr_volume_l30 += CnDoubleRe0(model.sOutturn[i].QtyMl.ToString().ToString().Replace(",", ""));

                        foa_usd += CnDoubleRe0(model.sOutturn[i].FOBAmountUSD.ToString().ToString().Replace(",", ""));
                        foa_thb += CnDoubleRe0(model.sOutturn[i].FOBAmountTHB.ToString().ToString().Replace(",", ""));
                        fra_usd += CnDoubleRe0(model.sOutturn[i].FreightAmountUSD.ToString().ToString().Replace(",", ""));
                        fra_thb += CnDoubleRe0(model.sOutturn[i].FreightAmountTHB.ToString().ToString().Replace(",", ""));
                        cfa_usd += CnDoubleRe0(model.sOutturn[i].CnFAmountUSD.ToString().ToString().Replace(",", ""));
                        cfa_thb += CnDoubleRe0(model.sOutturn[i].CnFAmountTHB.ToString().ToString().Replace(",", ""));
                        cf_vat_thb += CnDoubleRe0(model.sOutturn[i].CnFVat7.ToString().ToString().Replace(",", ""));
                        cf_total_usd += CnDoubleRe0(model.sOutturn[i].CnFTotalAmountUSD.ToString().ToString().Replace(",", ""));
                        cf_total_thb += CnDoubleRe0(model.sOutturn[i].CnFTotalAmountTHB.ToString().ToString().Replace(",", ""));
                        #endregion "detail"       

                        runningNo++;

                    }

                    #region "footer"
                    irows += 1;
                    excelSheets.Cells[irows, 3].Value = bl_volume_bbl.ToString("#,##0.000");
                    excelSheets.Cells[irows, 4].Value = bl_volume_l30.ToString("#,##0.000");
                    excelSheets.Cells[irows, 5].Value = bl_volume_mt.ToString("#,##0.000");
                    excelSheets.Cells[irows, 6].Value = "";
                    excelSheets.Cells[irows, 7].Value = gr_volume_bbl.ToString("#,##0.000");
                    excelSheets.Cells[irows, 8].Value = gr_volume_mt.ToString("#,##0.000");
                    excelSheets.Cells[irows, 9].Value = gr_volume_l30.ToString("#,##0.000");
                    excelSheets.Cells[irows, 10].Value = "";
                    excelSheets.Cells[irows, 11].Value = "";
                    excelSheets.Cells[irows, 12].Value = "";
                    excelSheets.Cells[irows, 13].Value = "";
                    excelSheets.Cells[irows, 14].Value = foa_usd.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 15].Value = foa_thb.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 16].Value = fra_usd.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 17].Value = fra_thb.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 18].Value = cfa_usd.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 19].Value = cfa_thb.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 20].Value = cf_vat_thb.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 21].Value = cf_total_usd.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 22].Value = cf_total_thb.ToString("#,##0.0000");
                    excelSheets.Cells[irows, 23].Value = "";
                    excelSheets.Cells[irows, 24].Value = "";
                    excelSheets.Cells[irows, 25].Value = "";
                    excelSheets.Cells[irows, 26].Value = "";
                    #endregion

                    excelSheets.DeleteRow(6, 1, true);

                    excelApp.Save();

                    excelApp.Dispose();
                    excelApp = null;

                }
                catch (Exception ex)
                {
                    msg = "error: 2_" + ex.Message.ToString(); goto Next_Line_Exit;
                }
            }
            catch (Exception ex)
            {
                msg = "error: 3_" + ex.Message.ToString(); goto Next_Line_Exit;
            }
            msg = newExcelFileNameFullPath;

            Next_Line_Exit:
            return msg;
        }

        private Double CnDoubleRe0(Object boject_value)
        {
            try
            {
                return Convert.ToDouble(boject_value);
            }
            catch { return 0; }
        }

        private void CopyRowTo(ExcelWorksheet input, ExcelWorksheet output, int row_in, int row_out)
        {
            input.InsertRow(row_out, 1, row_in);
            ExcelRange rng1 = input.SelectedRange[row_in, 1, row_in, 26];
            ExcelRange rng2 = input.SelectedRange[row_out, 1, row_out, 26];
            rng1.Copy(rng2);
        }

        #endregion

        public List<SelectListItem> GetCompany()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PIT_PO_HEADER
                                 join c in context.MT_COMPANY on p.COMPANY_CODE equals c.MCO_COMPANY_CODE into viewC
                                 from vdc in viewC.DefaultIfEmpty()
                                 where vdc.MCO_COMPANY_CODE == "1100" || vdc.MCO_COMPANY_CODE == "1400"
                                 select new
                                 {
                                     dCompanyCode = p.COMPANY_CODE,
                                     dCompanyName = vdc.MCO_SHORT_NAME
                                 }).Distinct();
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.dCompanyName, Value = item.dCompanyCode });
                        }
                    }
                }
                return selectList;
            }
            catch
            {
                return null;
            }
        }

        public List<SelectListItem> GetProduct()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
                return serviceModel.GetFeedStockPCF();
            }
            catch
            {
                return null;
            }
        }

        public bool IsFreightAvailable(string tripNo, out string currency)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from f in context.PCF_FREIGHT
                                 where f.PFR_TRIP_NO == tripNo
                                 select new
                                 {
                                     dCurrency = f.PFR_CURRENCY
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        currency = query.ToList()[0].dCurrency;
                        return true;
                    }
                    else
                    {
                        currency = "";
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                currency = "";
                return false;
            }
        }

        public string GetPlantFromPoItem(string poNo, string poItem)
        {
            var poItemCondition = Convert.ToDecimal(poItem);
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PIT_PO_ITEM
                                 where p.PO_NO == poNo && p.PO_ITEM == poItemCondition
                                 select new
                                 {
                                     dPlant = p.PLANT
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dPlant;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }
    }

    public class PCFAccrualSetting
    {
        public CIP_ACCRUAL_POSTING_CONFIG CIP_ACCRUAL_POSTING_CONFIG { get; set; }
    }

    public class CIP_ACCRUAL_POSTING_CONFIG
    {
        public List<DOCUMENTHEADER> DOCUMENTHEADER { get; set; }
        public List<ACCOUNTGL> ACCOUNTGL { get; set; }
        public List<EXTENSION2> EXTENSION2 { get; set; }
        public List<ZFI_GL_MAPPING2> ZFI_GL_MAPPING { get; set; }
        public List<SPVENDER> SP_VENDER { get; set; }
    }
}