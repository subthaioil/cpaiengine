﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class PortServiceModel
    {
        public ReturnValue Add(ref PortViewModel_Detail portViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                if (portViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(portViewDetailModel.PortName))
                {
                    rtn.Message = "Port Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (portViewDetailModel.Control == null)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (portViewDetailModel.Control.Count == 0)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (portViewDetailModel.Control.Count >= 0)
                {
                    var systemEmpty = portViewDetailModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = portViewDetailModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = portViewDetailModel.Control.GroupBy(x => x.System.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_PORT_DAL dal = new MT_PORT_DAL();
                MT_PORT ent = new MT_PORT();

                MT_PORT_CONTROL_DAL dalCtr = new MT_PORT_CONTROL_DAL();
                MT_PORT_CONTROL entCtr = new MT_PORT_CONTROL();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var portID = Convert.ToDecimal(ShareFn.GenerateCodeByDate(""));
                            ent.MLP_LOADING_PORT_ID = Convert.ToDecimal(portID);
                            ent.MLP_LOADING_PORT_NAME = portViewDetailModel.PortName.Trim();
                            ent.MLP_REMARK = portViewDetailModel.Remark;
                            ent.MLP_CREATE_TYPE = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            ent.MLP_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.MLP_CREATED_BY = pUser;
                            ent.MLP_CREATED_DATE = now;
                            ent.MLP_UPDATED_BY = pUser;
                            ent.MLP_UPDATED_DATE = now;

                            dal.Save(ent, context);

                            foreach (var item in portViewDetailModel.Control)
                            {
                                entCtr = new MT_PORT_CONTROL();
                                entCtr.MMP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCtr.MMP_FK_PORT = Convert.ToDecimal(portID);
                                entCtr.MMP_SYSTEM = String.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                entCtr.MMP_STATUS = item.Status;
                                entCtr.MMP_CREATED_BY = pUser;
                                entCtr.MMP_CREATED_DATE = now;
                                entCtr.MMP_UPDATED_BY = pUser;
                                entCtr.MMP_UPDATED_DATE = now;

                                dalCtr.Save(entCtr, context);
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;

                            portViewDetailModel.PortID = portID.ToString(); // Return ID
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(PortViewModel_Detail portViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (portViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(portViewDetailModel.PortID))
                {
                    rtn.Message = "Vendor Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(portViewDetailModel.CreateType))
                {
                    rtn.Message = "Create Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(portViewDetailModel.PortName) && portViewDetailModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Vendor Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                if (portViewDetailModel.Control == null)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (portViewDetailModel.Control.Count == 0)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (portViewDetailModel.Control.Count >= 0)
                {
                    var systemEmpty = portViewDetailModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = portViewDetailModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = portViewDetailModel.Control.GroupBy(x => x.System.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }


                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_PORT_DAL dal = new MT_PORT_DAL();
                MT_PORT ent = new MT_PORT();

                MT_PORT_CONTROL_DAL dalCtr = new MT_PORT_CONTROL_DAL();
                MT_PORT_CONTROL entCtr = new MT_PORT_CONTROL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            // Update data in MT_VENDOR
                            ent.MLP_LOADING_PORT_ID = Convert.ToDecimal(portViewDetailModel.PortID);
                            ent.MLP_LOADING_PORT_NAME = portViewDetailModel.PortName;
                            ent.MLP_REMARK = portViewDetailModel.Remark;
                            ent.MLP_CREATE_TYPE = portViewDetailModel.CreateType;
                            ent.MLP_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.MLP_UPDATED_BY = pUser;
                            ent.MLP_UPDATED_BY = now.ToString();

                            if (portViewDetailModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                                dal.Update(ent, context);                   // CPAI : Update all
                            else
                                dal.UpdateStatus(ent, context);             // SAP : Update status

                            // Insert data to MT_VENDOR_CONTROL
                            foreach (var item in portViewDetailModel.Control)
                            {
                                if (string.IsNullOrEmpty(item.RowID))
                                {
                                    // Add
                                    entCtr = new MT_PORT_CONTROL();
                                    entCtr.MMP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MMP_FK_PORT = Convert.ToDecimal(portViewDetailModel.PortID);
                                    entCtr.MMP_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MMP_STATUS = item.Status;
                                    entCtr.MMP_CREATED_BY = pUser;
                                    entCtr.MMP_CREATED_DATE = now;
                                    entCtr.MMP_UPDATED_BY = pUser;
                                    entCtr.MMP_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_PORT_CONTROL();
                                    entCtr.MMP_ROW_ID = item.RowID;
                                    entCtr.MMP_FK_PORT = Convert.ToDecimal(portViewDetailModel.PortID);
                                    entCtr.MMP_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MMP_STATUS = item.Status;
                                    entCtr.MMP_UPDATED_BY = pUser;
                                    entCtr.MMP_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref PortViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sCode = String.IsNullOrEmpty(pModel.sPortID) ? "" : pModel.sPortID.ToUpper();
                    var sName = String.IsNullOrEmpty(pModel.sPortName) ? "" : pModel.sPortName.ToLower();
                    var sCreateType = String.IsNullOrEmpty(pModel.sCreateType) ? "" : pModel.sCreateType.ToUpper();
                    var sSystem = String.IsNullOrEmpty(pModel.sSystem) ? "" : pModel.sSystem.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();

                    var query = (from v in context.MT_PORT
                                 join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT into view
                                 from vc in view.DefaultIfEmpty()
                                 orderby new { v.MLP_LOADING_PORT_ID, vc.MMP_SYSTEM }
                                 select new PortViewModel_SearchData
                                 {
                                     dPortID = v.MLP_LOADING_PORT_ID.ToString()
                                     ,
                                     dPortName = v.MLP_LOADING_PORT_NAME
                                     ,
                                     dRemark = v.MLP_REMARK
                                     ,
                                     dCreateType = String.IsNullOrEmpty(v.MLP_CREATE_TYPE) ? "SAP" : v.MLP_CREATE_TYPE
                                     ,
                                     dSystem = vc.MMP_SYSTEM
                                     ,
                                     dStatus = vc.MMP_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sCode))
                        query = query.Where(p => p.dPortID.ToUpper().Contains(sCode.Trim()));

                    if (!string.IsNullOrEmpty(sName))
                        query = query.Where(p => p.dPortName.ToLower().Contains(sName.ToLower().Trim()));

                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Equals(sCreateType));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dSystem.ToUpper().Equals(sSystem));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));


                    if (query != null)
                    {
                        pModel.sSearchData = new List<PortViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new PortViewModel_SearchData
                                                    {
                                                        dPortID = g.dPortID,
                                                        dPortName = g.dPortName,
                                                        dRemark = g.dRemark,
                                                        dCreateType = g.dCreateType,
                                                        dSystem = g.dSystem,
                                                        dStatus = g.dStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public PortViewModel_Detail Get(string pPortID)
        {
            PortViewModel_Detail model = new PortViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pPortID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var id = Convert.ToDecimal(pPortID);
                        var query = (from v in context.MT_PORT
                                     where v.MLP_LOADING_PORT_ID == id
                                     join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT into view
                                     from vc in view.DefaultIfEmpty()
                                     orderby new { vc.MMP_SYSTEM }
                                     select new
                                     {
                                         dPortID = v.MLP_LOADING_PORT_ID
                                         ,
                                         dPortName = v.MLP_LOADING_PORT_NAME
                                         ,
                                         dCreateType = String.IsNullOrEmpty(v.MLP_CREATE_TYPE) ? "SAP" : v.MLP_CREATE_TYPE
                                         ,
                                         dRemark = v.MLP_REMARK
                                         ,
                                         dStatus = v.MLP_STATUS
                                         ,
                                         dCtrlRowID = vc.MMP_ROW_ID
                                         ,
                                         dCtrlSystem = vc.MMP_SYSTEM
                                         ,
                                         dCtrlStatus = vc.MMP_STATUS
                                     });

                        if (query != null)
                        {
                            model.Control = new List<PortViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                model.CreateType = g.dCreateType;
                                model.PortID = g.dPortID.ToString();
                                model.PortName = g.dPortName;
                                model.Remark = g.dRemark;
                                if(!string.IsNullOrEmpty(g.dCtrlRowID))
                                    model.Control.Add(new PortViewModel_Control { RowID = g.dCtrlRowID, Order = i.ToString(), System = g.dCtrlSystem, Status = g.dCtrlStatus });

                                i++;
                            }
                        }
                    }

                    if (model.Control.Count == 0)
                    {
                        model.Control.Add(new PortViewModel_Control { RowID = "", Order = "1", System = "", Status = CPAIConstantUtil.ACTIVE });
                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_PORT_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MMP_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MMP_SYSTEM }
                                    select new { System = d.MMP_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetSystemForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_PORT_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MMP_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MMP_SYSTEM }
                                    select new { System = d.MMP_SYSTEM.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getPortDDLJson(string System)
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_PORT
                            join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT
                            where vc.MMP_SYSTEM.ToUpper() == System.ToUpper()
                            && vc.MMP_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                            select new
                            {
                                Id = v.MLP_LOADING_PORT_ID
                            ,
                                Name = v.MLP_LOADING_PORT_NAME
                            ,
                                System = vc.MMP_SYSTEM
                            ,
                                Status = vc.MMP_STATUS
                            };

                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.Id, text = p.Name }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }
    }
}