﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using log4net;
using System.Web.Mvc;
using com.pttict.sap.Interface.Service;
using com.pttict.engine.utility;
using Newtonsoft.Json.Linq;
using com.pttict.sap.Interface.sap.accrual.post;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Globalization;
using System.Data.SqlClient;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class PurchaseEntryServiceModel
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Data functions

        public ReturnValue Search(ref PurchaseEntryViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sCompany = String.IsNullOrEmpty(pModel.sCompany) ? "" : pModel.sCompany;
                    var sTripNo = String.IsNullOrEmpty(pModel.sTripNo) ? "" : pModel.sTripNo;
                    var sPoDate = String.IsNullOrEmpty(pModel.sPoDate) ? "" : pModel.sPoDate;
                    var sProduct = String.IsNullOrEmpty(pModel.sProduct) ? "" : pModel.sProduct;
                    var sPoNo = String.IsNullOrEmpty(pModel.sPoNo) ? "" : pModel.sPoNo;
                    var sGrDate = String.IsNullOrEmpty(pModel.sGrDate) ? "" : pModel.sGrDate;
                    var sSupplier = String.IsNullOrEmpty(pModel.sSupplier) ? "" : pModel.sSupplier;

                    //getMax Date
                    var qryMaxDate = (from t in context.PIT_PO_PRICE
                                      where t.INVOICE_STATUS == "AC"
                                      group t by new { t.PO_NO, t.PO_ITEM } into view
                                      from g in view
                                      select new { g.PO_NO, g.PO_ITEM, MAX_Date = view.Max(e => e.PO_DATE) }).Distinct();

                    var qryMax = (from post in qryMaxDate
                                  join meta in context.PIT_PO_PRICE
                                  on new { ColA = post.PO_NO, ColB = post.PO_ITEM, ColC = post.MAX_Date, ColD = "AC" }
                                                                equals new { ColA = meta.PO_NO, ColB = meta.PO_ITEM, ColC = meta.PO_DATE, ColD = meta.INVOICE_STATUS }
                                                                into view
                                  from g in view.DefaultIfEmpty()
                                  select new
                                  {
                                      PO_NO = post.PO_NO,
                                      PO_ITEM = post.PO_ITEM,
                                      MAX_Date = post.MAX_Date,
                                      UPDATED_DATE = g.UPDATED_DATE,
                                      UNIT_PRICE = g.UNIT_PRICE
                                  }).Distinct();

                    //var query = from t in queryTemp
                    //            join i in qryMax on new { ColA = t.dPoNo, ColB = t.dPoItem } equals new { ColA = i.PO_NO, ColB = i.PO_ITEM } into viewD
                    //            from p in viewD.DefaultIfEmpty()
                    //            select new
                    //            {
                    //                dTripNo = t.dTripNo,
                    //                dPoNo = t.dPoNo,
                    //                dPoItem = t.dPoItem,
                    //                dPoDate = t.dPoDate,
                    //                dProduct = t.dProduct,
                    //                dProductNum = t.dProductNum,
                    //                dSuplierNo = t.dSuplierNo,
                    //                dSupplier = t.dSupplier,
                    //                dIncoterms = t.dIncoterms,
                    //                dVolume = t.dVolume,
                    //                dVolumeUnit = t.dVolumeUnit,
                    //                dPrice = t.dPrice,
                    //                dTotal = t.dTotal,
                    //                dCurrency = t.dCurrency,
                    //                dCompanyCode = t.dCompanyCode,
                    //                dGRDate = t.dGRDate,
                    //                dUpdatePrice = p.UNIT_PRICE,
                    //                dLastUpdate = p.UPDATED_DATE
                    //            };
                    var query = (from r in context.PIT_PO_HEADER
                                 join i in context.PIT_PO_ITEM on r.PO_NO equals i.PO_NO into viewD
                                 from vdl in viewD.DefaultIfEmpty()
                                 join j in context.MT_VENDOR on vdl.ACC_NUM_VENDOR equals j.VND_ACC_NUM_VENDOR into viewS
                                 from vds in viewS.DefaultIfEmpty()
                                 join k in context.MT_MATERIALS on vdl.MET_NUM equals k.MET_NUM into viewT
                                 from vdt in viewT.DefaultIfEmpty()
                                 join l in context.OUTTURN_FROM_SAP on vdl.TRIP_NO equals l.TRIP_NO into viewL
                                 from vdu in viewL.DefaultIfEmpty()
                                     //join p in context.PIT_PO_PRICE on new { vdl.PO_NO, vdl.PO_ITEM } equals new { p.PO_NO, p.PO_ITEM } into viewP
                                     //from vdp in viewP.DefaultIfEmpty()
                                 //join i in qryMax on new { ColA = r.PO_NO, ColB = vdl.PO_ITEM } equals new { ColA = i.PO_NO, ColB = i.PO_ITEM } into viewP
                                 //from p in viewP.DefaultIfEmpty()
                                 select new
                                 {
                                     dTripNo = vdl.TRIP_NO,
                                     dPoNo = r.PO_NO,
                                     dPoItem = vdl.PO_ITEM,
                                     dPoDate = r.PO_DATE,
                                     dProduct = vdt.MET_MAT_DES_ENGLISH,
                                     dProductNum = vdl.MET_NUM,
                                     dSuplierNo = vdl.ACC_NUM_VENDOR,
                                     dSupplier = vds.VND_NAME1,
                                     dIncoterms = r.INCOTERMS,
                                     dVolume = vdl.VOLUME,
                                     dVolumeUnit = vdl.VOLUME_UNIT,
                                     dPrice = vdl.UNIT_PRICE,
                                     dTotal = vdl.VOLUME * vdl.UNIT_PRICE,
                                     dCurrency = vdl.CURRENCY,
                                     dCompanyCode = r.COMPANY_CODE,
                                     dGRDate = vdu.POSTING_DATE,
                                     dUpdatePrice = 0,
                                     dLastUpdate = ""
                                 });
                    

                    var queryVendor = (from v in context.MT_VENDOR
                                       select v);
                    Vendors vendors = new Vendors();
                    vendors.Vendor = new List<ViewModels.Vendor>();
                    if (queryVendor != null)
                    {
                        foreach (var vendor in queryVendor)
                        {
                            vendors.Vendor.Add(new ViewModels.Vendor
                            {
                                VendorNo = vendor.VND_ACC_NUM_VENDOR,
                                VendorName = vendor.VND_NAME1
                            });
                        }
                    }

                    #region Filter Data                    

                    if (!string.IsNullOrEmpty(sCompany) && sCompany != "-- Select --")
                    {
                        query = query.Where(p => p.dCompanyCode.Contains(sCompany));
                    }

                    if (!string.IsNullOrEmpty(sTripNo))
                    {
                        query = query.Where(p => p.dTripNo.ToLower().Contains(sTripNo.Trim().ToLower()));
                    }

                    if (!string.IsNullOrEmpty(sPoDate))
                    {
                        sPoDate = sPoDate.Replace(" to ", "|");
                        var fromDate = DateTime.ParseExact(sPoDate.Split('|')[0], "dd/MM/yyyy", cultureinfo);
                        var toDate = DateTime.ParseExact(sPoDate.Split('|')[1], "dd/MM/yyyy", cultureinfo);

                        if (fromDate != null && toDate != null)
                        {
                            query = query.Where(p => fromDate <= p.dPoDate && toDate >= p.dPoDate);
                        }
                    }

                    if (!string.IsNullOrEmpty(sGrDate))
                    {
                        sGrDate = sGrDate.Replace(" to ", "|");
                        var fromDate = DateTime.ParseExact(sGrDate.Split('|')[0], "dd/MM/yyyy", cultureinfo);
                        var toDate = DateTime.ParseExact(sGrDate.Split('|')[1], "dd/MM/yyyy", cultureinfo);

                        if (fromDate != null && toDate != null)
                        {
                            query = query.Where(p => fromDate <= p.dGRDate && toDate >= p.dGRDate);
                        }
                    }

                    if (!string.IsNullOrEmpty(sProduct) && sProduct != "-- Select --")
                    {
                        query = query.Where(p => p.dProductNum.ToLower().Contains(sProduct.ToLower()));
                    }

                    if (!string.IsNullOrEmpty(sPoNo))
                    {
                        query = query.Where(p => p.dPoNo.ToLower().Contains(sPoNo.Trim().ToLower()));
                    }

                    if (!string.IsNullOrEmpty(sSupplier) && sSupplier != "-- Select --")
                    {
                        query = query.Where(p => p.dSuplierNo.ToLower().Contains(sSupplier.ToLower()));
                    }

                    #endregion

                    List<string> tripNoPoNo = new List<string>();

                    if (query != null)
                    {
                        pModel.sSearchData = new List<PurchaseEntryViewModel_SearchData>();
                        foreach (var item in query)
                        {
                            if (tripNoPoNo.Contains(item.dTripNo + item.dPoNo))
                            {
                                continue;
                            }
                            else
                            {
                                tripNoPoNo.Add(item.dTripNo + item.dPoNo);
                            }

                            string supplierName = "";
                            foreach (var vend in vendors.Vendor)
                            {
                                try
                                {
                                    if (Convert.ToInt32(item.dSuplierNo) == Convert.ToInt32(vend.VendorNo))
                                    {
                                        supplierName = vend.VendorName;
                                        break;
                                    }
                                }
                                catch { }
                            }

                            string unitPrice = "";
                            string lastUpdate = "";
                            if(qryMax != null && qryMax.Count() > 0)
                            {
                                //foreach(var p in qryMax)
                                //{
                                //    if(p.PO_NO == item.dPoNo && p.PO_ITEM == item.dPoItem)
                                //    {
                                //        unitPrice = p.UNIT_PRICE.ToString();
                                //        lastUpdate = p.UPDATED_DATE.ToString("dd/MM/yyyy", cultureinfo);
                                //    }
                                //}
                                var temp = qryMax.Where(p => p.PO_NO == item.dPoNo && p.PO_ITEM == item.dPoItem).ToList();
                                if (temp.Count() > 0)
                                {
                                    unitPrice = temp[0].UNIT_PRICE.ToString();
                                    lastUpdate = temp[0].UPDATED_DATE.ToString("dd/MM/yyyy", cultureinfo);
                                }
                            }
                            pModel.sSearchData.Add(new PurchaseEntryViewModel_SearchData
                            {
                                TripNo = String.IsNullOrEmpty(item.dTripNo) ? "" : item.dTripNo,
                                PoNo = String.IsNullOrEmpty(item.dPoNo) ? "" : item.dPoNo,
                                PoItem = (item.dPoItem == 0) ? "" : item.dPoItem.ToString(),
                                PoDate = item.dPoDate.Value.ToString("dd/MM/yyyy", cultureinfo),
                                Product = String.IsNullOrEmpty(item.dProduct) ? "" : item.dProduct,
                                Supplier = supplierName,
                                SupplierNo = item.dSuplierNo,
                                Incoterms = String.IsNullOrEmpty(item.dIncoterms) ? "" : item.dIncoterms,
                                Volume = (item.dVolume == null) ? "" : item.dVolume.Value.ToString("#,##0.0000"),
                                VolumeUnit = String.IsNullOrEmpty(item.dVolumeUnit) ? "" : item.dVolumeUnit,
                                Price = (item.dPrice == null) ? "" : item.dPrice.Value.ToString("#,##0.0000"),
                                Total = (item.dTotal == null) ? "" : item.dTotal.Value.ToString("#,##0.0000"),
                                Currency = String.IsNullOrEmpty(item.dCurrency) ? "" : item.dCurrency,
                                MetNum = item.dProductNum,
                                UpdatePrice = unitPrice,
                                LastUpdate = lastUpdate
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                return rtn;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ReturnValue Calculate(PurchaseEntryViewModel_SearchData model)
        {
            ReturnValue result = new ReturnValue();
            bool somethingWrong = false;
            if (model.sOutturn == null || model.sOutturn.Count() == 0)
            {
                model.sOutturn = new List<PurchaseEntryViewModel_Outtern>();
                return null;
            }
            foreach (var outturn in model.sOutturn)
            {
                if (outturn.Select)
                {
                    outturn.ROE = Convert.ToDecimal(outturn.ROE).ToString();
                    try
                    {
                        #region Calculate Amount USD, Amount THB and VAT
                        if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "bbl")
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                else
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.Price))).ToString();
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                else
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.Price))).ToString();
                            }
                        }
                        else if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "mt")
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                else
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.Price))).ToString();
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                else
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.Price))).ToString();
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                else
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.Price))).ToString();
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                                else
                                    outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.Price))).ToString();
                            }
                        }
                        var amountUSDforCalculate = Convert.ToDecimal(outturn.AmountUSD);
                        outturn.AmountUSD = Math.Round(Convert.ToDecimal(outturn.AmountUSD), 4).ToString("#,###.0000");
                        outturn.Vat7 = Math.Round(((amountUSDforCalculate * Convert.ToDecimal(outturn.ROE)) * Convert.ToDecimal(0.07)), 4).ToString();
                        //outturn.Vat7 = Math.Round(((Convert.ToDecimal(outturn.AmountUSD) * Convert.ToDecimal(outturn.ROE)) * Convert.ToDecimal(0.07)), 4).ToString();
                        //outturn.AmountTHB = Math.Round((Convert.ToDecimal(outturn.AmountUSD) * Convert.ToDecimal(outturn.ROE)), 4).ToString("#,###.0000");
                        outturn.AmountTHB = Math.Round((amountUSDforCalculate * Convert.ToDecimal(outturn.ROE)), 4).ToString("#,###.0000");
                        outturn.TotalAmountUSD = outturn.AmountUSD;
                        outturn.TotalAmountTHB = (Convert.ToDecimal(outturn.AmountTHB) + Convert.ToDecimal(outturn.Vat7)).ToString("#,###.0000");
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        somethingWrong = true;
                    }
                }
            }

            if (somethingWrong)
                result.Message = "Done but something wrong happenned!";
            else
                result.Message = "Done without any problems";
            result.Status = true;
            return result;
        }

        public ReturnValue CalculateAll(PurchaseEntryViewModel_SearchData model)
        {
            ReturnValue result = new ReturnValue();
            bool somethingWrong = false;
            if (model.sOutturn == null || model.sOutturn.Count() == 0)
            {
                model.sOutturn = new List<PurchaseEntryViewModel_Outtern>();
                return null;
            }
            foreach (var outturn in model.sOutturn)
            {
                try
                {
                    outturn.ROE = Convert.ToDecimal(outturn.ROE).ToString();

                    #region Calculate Amount USD, Amount THB and VAT
                    if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "bbl")
                    {
                        if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                        {
                            if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                            else
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyBbl) * Convert.ToDecimal(outturn.Price))).ToString();
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                            else
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyBbl) * Convert.ToDecimal(outturn.Price))).ToString();
                        }
                    }
                    else if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "mt")
                    {
                        if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                        {
                            if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                            else
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMt) * Convert.ToDecimal(outturn.Price))).ToString();
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                            else
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMt) * Convert.ToDecimal(outturn.Price))).ToString();
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                        {
                            if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                            else
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.BlQtyMl) * Convert.ToDecimal(outturn.Price))).ToString();
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.Price)) / Convert.ToDecimal(outturn.ROE)).ToString();
                            else
                                outturn.AmountUSD = ((Convert.ToDecimal(outturn.QtyMl) * Convert.ToDecimal(outturn.Price))).ToString();
                        }
                    }
                    var amountUSDforCalculate = Convert.ToDecimal(outturn.AmountUSD);
                    outturn.AmountUSD = Math.Round(Convert.ToDecimal(outturn.AmountUSD), 4).ToString("#,###.0000");
                    outturn.Vat7 = Math.Round(((amountUSDforCalculate * Convert.ToDecimal(outturn.ROE)) * Convert.ToDecimal(0.07)), 4).ToString();
                    //outturn.Vat7 = Math.Round(((Convert.ToDecimal(outturn.AmountUSD) * Convert.ToDecimal(outturn.ROE)) * Convert.ToDecimal(0.07)), 4).ToString();
                    //outturn.AmountTHB = Math.Round((Convert.ToDecimal(outturn.AmountUSD) * Convert.ToDecimal(outturn.ROE)), 4).ToString("#,###.0000");
                    outturn.AmountTHB = Math.Round((amountUSDforCalculate * Convert.ToDecimal(outturn.ROE)), 4).ToString("#,###.0000");
                    outturn.TotalAmountUSD = outturn.AmountUSD;
                    outturn.TotalAmountTHB = (Convert.ToDecimal(outturn.AmountTHB) + Convert.ToDecimal(outturn.Vat7)).ToString("#,###.0000");
                    #endregion
                }
                catch (Exception ex)
                {
                    somethingWrong = true;
                }
            }

            if (somethingWrong)
                result.Message = "Done but something wrong happenned!";
            else
                result.Message = "Done without any problems";
            result.Status = true;
            return result;
        }

        public PurchaseEntryViewModel_SearchData CleansingModel(PurchaseEntryViewModel_SearchData model)
        {
            model.Price = Double.Parse(model.Price, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
            model.Total = Double.Parse(model.Total, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
            model.Volume = Double.Parse(model.Volume, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();

            foreach (var outturn in model.sOutturn)
            {
                if (outturn.Select)
                {
                    outturn.AmountTHB = Double.Parse(outturn.AmountTHB, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.AmountUSD = Double.Parse(outturn.AmountUSD, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.BlQtyBbl = Double.Parse(outturn.BlQtyBbl, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.BlQtyMl = Double.Parse(outturn.BlQtyMl, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.BlQtyMt = Double.Parse(outturn.BlQtyMt, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.QtyBbl = Double.Parse(outturn.QtyBbl, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.QtyMl = Double.Parse(outturn.QtyMl, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.QtyMt = Double.Parse(outturn.QtyMt, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.Price = Double.Parse(outturn.Price, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.ROE = Double.Parse(outturn.ROE, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.Vat7 = Double.Parse(outturn.Vat7, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.TotalAmountTHB = Double.Parse(outturn.TotalAmountTHB, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();
                    outturn.TotalAmountUSD = Double.Parse(outturn.TotalAmountUSD, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint).ToString();

                }
            }

            return model;
        }

        public PurchaseEntryViewModel_SearchData FilterOnlySavedRecord(PurchaseEntryViewModel_SearchData model)
        {
            try
            {
                List<int> removedList = new List<int>();
                for (int i = 0; i < model.sOutturn.Count(); i++)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        decimal poItem = Convert.ToDecimal(model.PoItem);
                        decimal itemNo = Convert.ToDecimal(model.sOutturn[i].ItemNo);

                        var query = (from r in context.PIT_PURCHASE_ENTRY
                                     where r.PO_NO == model.PoNo && r.PO_ITEM == poItem && r.ITEM_NO == itemNo
                                     select new
                                     {
                                         dPoNo = r.PO_NO
                                     });

                        if (query == null || query.ToList().Count() == 0)
                        {
                            removedList.Add(i + 1);
                        }
                    }
                }

                foreach (var index in removedList)
                {
                    model.sOutturn.RemoveAll(x => removedList.Contains(Convert.ToInt32(x.ItemNo)));
                }
            }
            catch (Exception ex)
            {

            }

            return model;
        }

        #endregion

        #region DB functions

        public ReturnValue Save(PurchaseEntryViewModel_SearchData model)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            Calculate(model);

            bool somethingWrong = false;
            ReturnValue result = new ReturnValue();
            if (model.sOutturn == null || model.sOutturn.Count() == 0)
            {
                model.sOutturn = new List<PurchaseEntryViewModel_Outtern>();
                result.Status = false;
                result.Message = "Empty Outturn";
                return result;
            }
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    PIT_PURCHASE_ENTRY_DAL ppeDal = new PIT_PURCHASE_ENTRY_DAL();
                    PIT_PURCHASE_ENTRY ppe = new PIT_PURCHASE_ENTRY();
                    PRICING_TO_SAP_DAL ptsDal = new PRICING_TO_SAP_DAL();
                    PRICING_TO_SAP pts = new PRICING_TO_SAP();
                    DateTime timeStamp = DateTime.Now;

                    foreach (var m in model.sOutturn)
                    {
                        if (m.Select)
                        {
                            #region Save to PIT_PURCHASE_ENTRY

                            try
                            {
                                ppe.PO_NO = model.PoNo;
                                ppe.PO_ITEM = Convert.ToDecimal(model.PoItem);
                                ppe.PO_DATE = DateTime.ParseExact(model.PoDate, "dd/MM/yyyy", cultureinfo);
                                ppe.ITEM_NO = Convert.ToDecimal(m.ItemNo);
                                ppe.BL_DATE = DateTime.ParseExact(m.BLDate, "dd/MM/yyyy", cultureinfo);
                                ppe.BL_VOLUME_BBL = Convert.ToDecimal(m.BlQtyBbl);
                                ppe.BL_VOLUME_MT = Convert.ToDecimal(m.BlQtyMt);
                                ppe.BL_VOLUME_L30 = Convert.ToDecimal(m.BlQtyMl);
                                ppe.GR_DATE = DateTime.ParseExact(m.PostingDate, "dd/MM/yyyy", cultureinfo);
                                ppe.GR_VOLUME_BBL = Convert.ToDecimal(m.QtyBbl);
                                ppe.GR_VOLUME_MT = Convert.ToDecimal(m.QtyMt);
                                ppe.GR_VOLUME_L30 = Convert.ToDecimal(m.QtyMl);
                                ppe.PRICE = Convert.ToDecimal(m.Price);
                                ppe.ROE = Convert.ToDecimal(m.ROE);
                                ppe.AMOUNT_USD = Convert.ToDecimal(m.AmountUSD);
                                ppe.AMOUNT_THB = Convert.ToDecimal(m.AmountTHB);
                                ppe.VAT = Convert.ToDecimal(m.Vat7);
                                ppe.TOTAL_USD = Convert.ToDecimal(m.TotalAmountUSD);
                                ppe.TOTAL_THB = Convert.ToDecimal(m.TotalAmountTHB);
                                ppe.CAL_VOLUME_UNIT = model.VolumeUnit;
                                ppe.CAL_INVOICE_FIGURE = model.InvoiceFigure;
                                ppe.CAL_PRICE_UNIT = model.Currency;
                                ppe.CREATED_DATE = timeStamp;
                                ppe.CREATED_BY = Const.User.Name;
                                ppe.UPDATED_DATE = timeStamp;
                                ppe.UPDATED_BY = Const.User.Name;
                                m.UpdatedDate = timeStamp.ToString("dd/MM/yyyy HH:mm:ss");
                                m.UpdatedBy = Const.User.Name;

                                ppeDal.Save(ppe);
                            }
                            catch (Exception ex)
                            {
                                somethingWrong = true;
                            }

                            #endregion

                            #region Save to PRICING_TO_SAP

                            try
                            {
                                string volumnToUse = "";
                                if (model.VolumeUnit.ToLower() == "bbl")
                                {
                                    volumnToUse = m.BlQtyBbl;
                                }
                                else if (model.VolumeUnit.ToLower() == "mt")
                                {
                                    volumnToUse = m.BlQtyMt;
                                }
                                else
                                {
                                    volumnToUse = m.BlQtyMl;
                                }

                                pts.TRANS_TYPE = "C";
                                pts.COM_CODE = GetCompanyCodeFromPoNo(model.PoNo);
                                pts.TRIP_NO = model.TripNo;
                                pts.CREATE_DATE = timeStamp;
                                try
                                {
                                    pts.CMCS_ETA = DateTime.ParseExact(m.PostingDate, "dd/MM/yyyy", cultureinfo);
                                }
                                catch
                                {
                                    pts.CMCS_ETA = DateTime.Now;
                                }
                                pts.MAT_NUM = model.MetNum;
                                pts.PARTLY_ORDER = Convert.ToDecimal(m.ItemNo);
                                string supplierNo = Convert.ToInt32(model.SupplierNo).ToString();
                                pts.VENDOR_NO = supplierNo;
                                pts.PLANT = GetPlantFromPoItem(model.PoNo, model.PoItem);
                                pts.GROUP_LOCATION = "REFINERY";
                                pts.EXCHANGE_RATE = Convert.ToDecimal(m.ROE);
                                pts.BASE_PRICE = Convert.ToDecimal(m.Price);
                                pts.PREMIUM_OSP = 0;
                                pts.PREMIUM_MARKET = 0;
                                pts.OTHER_COST = 0;
                                pts.FREIGHT_PRICE = 0;
                                pts.INSURANCE_PRICE = 0;
                                pts.TOTAL_AMOUNT_THB = Convert.ToDecimal(m.AmountTHB);
                                pts.TOTAL_AMOUNT_USD = Convert.ToDecimal(m.AmountUSD);
                                pts.QTY = Convert.ToDecimal(volumnToUse);
                                pts.QTY_UNIT = model.VolumeUnit;
                                pts.CURRENCY = model.PriceUnit;
                                //pts.CURRENCY = model.PitPurchaseEntry.CalPriceUnit;
                                pts.SAP_RECEIVED_DATE = null;
                                pts.STATUS = null;
                                pts.GROUP_DATA = 1;
                                pts.FLAG_PLANNING = "";
                                try
                                {
                                    pts.DUE_DATE = DateTime.ParseExact(model.sOutturn[model.sOutturn.Count() - 1].RunningDate, "dd/MM/yyyy", cultureinfo);
                                }
                                catch
                                {
                                    pts.DUE_DATE = timeStamp;
                                }
                                pts.CODE = 111;
                                pts.REMARK = null;
                                ptsDal.Save(pts);
                            }
                            catch (Exception ex)
                            {
                                somethingWrong = true;
                                result.Message = ex.Message;
                            }

                            #endregion
                        }
                        else
                        {
                            // Remove from pit-purchase_entry if not-ticked item already existed in the table
                            ppe.PO_NO = model.PoNo;
                            ppe.PO_ITEM = Convert.ToDecimal(model.PoItem);
                            ppe.PO_DATE = DateTime.ParseExact(model.PoDate, "dd/MM/yyyy", cultureinfo);
                            ppe.ITEM_NO = Convert.ToDecimal(m.ItemNo);

                            ppeDal.Remove(ppe);
                        }
                    }
                }
            }

            using (EntityCPAIEngine context2 = new EntityCPAIEngine())
            {
                context2.Database.ExecuteSqlCommand("CALL P_PIT_INSERT_PRICE_0('" + model.TripNo + "')");
            }

            result.Status = true;
            return result;
        }

        public ReturnValue UpdateSapFiDocNo(string poNo, string poItem, string itemNo, string sapFiDocNo, string type)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                PIT_PURCHASE_ENTRY_DAL ppeDal = new PIT_PURCHASE_ENTRY_DAL();
                PIT_PURCHASE_ENTRY ppe = new PIT_PURCHASE_ENTRY();

                try
                {
                    ppeDal.UpdateSapFiDocNo(poNo, poItem, itemNo, sapFiDocNo, Const.User.Name, type);

                    rtn.Status = true;
                    rtn.Message = "Update SAP Fi Doc No successfully";
                    return rtn;
                }
                catch (Exception ex)
                {
                    rtn.Status = false;
                    rtn.Message = "Error: " + ex.Message;
                    return rtn;
                }

            }
        }

        #endregion

        #region SAP functions

        public ReturnValue
            SendToSAP(string jsonConfig, PurchaseEntryViewModel_SearchData model, string accrualType, string reverseDate)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            string JsonD = MasterData.GetJsonMasterSetting("PIT_ACCRUAL_POSTING_CONFIG");
            var accrualSetting = GetAccrualSettings();
            var companyCode = GetCompanyCodeFromPoNo(model.PoNo);
            var metNum = GetMetNumFromPoItem(model.PoNo, model.PoItem);
            var sortField = GetSortFieldFromSupplier(model.SupplierNo);
            var io = GetIOFromMetNum(metNum, companyCode);

            reverseDate = reverseDate.Split('/')[2] + "-" +
                            reverseDate.Split('/')[1] + "-" +
                            reverseDate.Split('/')[0];

            foreach (var outturn in model.sOutturn)
            {
                if (outturn.Select)
                {
                    try
                    {

                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            decimal poItem = Convert.ToDecimal(model.PoItem ?? "0");
                            decimal itemNo = Convert.ToDecimal(outturn.ItemNo ?? "0");

                            var queryPrice = (from r in context.PIT_PURCHASE_ENTRY
                                              where r.PO_NO == model.PoNo && r.PO_ITEM == poItem && r.ITEM_NO == itemNo
                                              select r);

                            if (queryPrice != null || queryPrice.ToList().Count() > 0)
                            {
                                if (queryPrice.ToList()[0].PRICE != null)
                                {
                                    outturn.Price = (queryPrice.ToList()[0].PRICE ?? 0).ToString("#,##0.0000");
                                }
                                if (queryPrice.ToList()[0].AMOUNT_THB != null)
                                {
                                    outturn.AmountTHB = (queryPrice.ToList()[0].AMOUNT_THB ?? 0).ToString("#,##0.0000");
                                }
                                if (queryPrice.ToList()[0].AMOUNT_USD != null)
                                {
                                    outturn.AmountUSD = (queryPrice.ToList()[0].AMOUNT_USD ?? 0).ToString("#,##0.0000");
                                }

                            }
                        }

                        //Type A
                        if (accrualType.ToLower() == "a")
                        {
                            string sapFiDoc = "";
                            rtn.Message += "Running Date = " + outturn.RunningDate + ", ";

                            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                            com.pttict.sap.Interface.sap.accrual.create.BAPIACHE09 DocumentHeader = new com.pttict.sap.Interface.sap.accrual.create.BAPIACHE09();
                            List<com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09> AccountGl = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09>();
                            List<com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09> CurrencyAmount = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09>();
                            List<com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX> Extension2 = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX>();
                            List<com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING> zfi_GL_MAPPING = new List<com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING>();

                            string metMatDesEnglish = GetMetMatDesEnglishFromMetNum(metNum);
                            string fisPeriod = (DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month < 10) ? "0" + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString() : DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString();
                            string headerTxtSuffix = "REC." + GetMonthName(DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month) + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year;
                            int avaiableCharRangeForHeaderTxt = 25 - (headerTxtSuffix.Length + 1);


                            #region DocumentHeader
                            var documentHeaderFromConfig = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.DOCUMENTHEADER
                                .Where(x => x.ACCRUE_TYPE.ToLower() == accrualType.ToLower() && x.COMP_CODE == companyCode);
                            if (documentHeaderFromConfig.ToList().Count() == 0)
                            {
                                return null;
                            }
                            var docHeader = documentHeaderFromConfig.ToList()[0];

                            DocumentHeader.OBJ_TYPE = docHeader.OBJ_TYPE;
                            DocumentHeader.OBJ_KEY = docHeader.OBJ_KEY;
                            DocumentHeader.BUS_ACT = docHeader.BUS_ACT;
                            DocumentHeader.USERNAME = docHeader.USERNAME;
                            DocumentHeader.HEADER_TXT = (metMatDesEnglish.Length <= avaiableCharRangeForHeaderTxt)
                                ? metMatDesEnglish + " " + headerTxtSuffix
                                : metMatDesEnglish.Substring(0, avaiableCharRangeForHeaderTxt) + " " + headerTxtSuffix;
                            DocumentHeader.COMP_CODE = companyCode;
                            DocumentHeader.DOC_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                            DocumentHeader.PSTNG_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                            DocumentHeader.FISC_YEAR = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year.ToString();
                            DocumentHeader.FIS_PERIOD = fisPeriod;
                            DocumentHeader.DOC_TYPE = docHeader.DOC_TYPE;
                            DocumentHeader.REF_DOC_NO_LONG = model.TripNo + "." + outturn.ItemNo;
                            DocumentHeader.REASON_REV = docHeader.REASON_REV;

                            rtn.Message += "HEADER_TXT = " + DocumentHeader.HEADER_TXT + ", ";
                            rtn.Message += "DOC_DATE = " + DocumentHeader.DOC_DATE + ", ";
                            rtn.Message += "PSTNG_DATE = " + DocumentHeader.PSTNG_DATE + ", ";
                            rtn.Message += "FIS_PERIOD = " + DocumentHeader.FIS_PERIOD + "\r\n";
                            #endregion

                            #region AccountGL
                            var accountGlNotNull = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.ACCOUNTGL
                                .Where(x => x.COMP_CODE == companyCode);
                            if (accountGlNotNull.ToList().Count() == 0)
                            {
                                return null;
                            }
                            AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000001",
                                GL_ACCOUNT = accountGlNotNull.ToList()[0].GLACCOUNT,
                                ITEM_TEXT = DocumentHeader.HEADER_TXT,
                                //ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = accountGlNotNull.ToList()[0].COSTCENTER,
                                ORDERID = io
                            });
                            AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000002",
                                GL_ACCOUNT = "",
                                ITEM_TEXT = DocumentHeader.HEADER_TXT,
                                //ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = "",
                                ORDERID = ""
                            });
                            #endregion

                            #region CurrencyAmount
                            var doccur = Convert.ToDecimal((model.Currency.ToLower() == "usd") ? outturn.AmountUSD : outturn.AmountTHB);
                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000001",
                                CURR_TYPE = "00",
                                CURRENCY = model.Currency,
                                AMT_DOCCUR = doccur,
                                AMT_DOCCURSpecified = true
                            });
                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000002",
                                CURR_TYPE = "00",
                                CURRENCY = model.Currency,
                                AMT_DOCCUR = doccur * -1,
                                AMT_DOCCURSpecified = true
                            });

                            #endregion

                            #region Extension2

                            var extension2ForVolume = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Volume" && x.COMP_CODE == companyCode);
                            if (extension2ForVolume.ToList().Count() == 0)
                            {
                                return null;
                            }
                            string volumnUnit = model.VolumeUnit.ToUpper();
                            string volumn = "";
                            if (volumnUnit == "BBL") volumn = outturn.BlQtyBbl;
                            else if (volumnUnit == "MT") volumn = outturn.BlQtyMt;
                            else if (volumnUnit == "L30") volumn = outturn.BlQtyMl;

                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //VOLUME
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForVolume.ToList()[0].VALUEPART1,
                                VALUEPART2 = volumn
                            });
                            var extension2ForUOM = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "UOM" && x.COMP_CODE == companyCode);
                            if (extension2ForUOM.ToList().Count() == 0)
                            {
                                return null;
                            }
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForUOM.ToList()[0].VALUEPART1,
                                VALUEPART2 = model.VolumeUnit
                            });
                            var extension2ForBP = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Business Place" && x.COMP_CODE == companyCode);
                            if (extension2ForBP.ToList().Count() == 0)
                            {
                                return null;
                            }
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX
                            {
                                STRUCTURE = "0000000002",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });
                            #endregion

                            #region ZFI_GL-MAPPING
                            var zfiGlMapping = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING
                                .Where(x => x.COMP_CODE == companyCode);
                            if (zfiGlMapping.ToList().Count() == 0)
                            {
                                return null;
                            }
                            zfi_GL_MAPPING.Add(new com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING
                            {
                                ITEMNO_ACC = "0000000002",
                                TRAN_IND = zfiGlMapping.ToList()[0].TRAN_IND
                            });
                            #endregion

                            AccrualService accrualService = new AccrualService();
                            try
                            {
                                sapFiDoc = accrualService.Create(jsonConfig, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING, reverseDate);
                            }
                            catch (Exception ex)
                            {

                            }

                            rtn.Status = true;

                            if (rtn.Status)
                            {
                                var rtn2 = UpdateSapFiDocNo(model.PoNo, model.PoItem, outturn.ItemNo, sapFiDoc, "A");
                                if (!rtn2.Status)
                                {
                                    //rtn.Message = "Send to SAP successfully but got th problem when updating SAP Fi Doc No";
                                }
                            }
                        }
                        else //Type C
                        {
                            string sapFiDoc = "";
                            rtn.Message += "Running Date = " + outturn.RunningDate + ", ";

                            #region Variables

                            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                            BAPIACHE09 DocumentHeader = new BAPIACHE09();
                            List<BAPIACGL09> AccountGl = new List<BAPIACGL09>();
                            List<BAPIACCR09> CurrencyAmount = new List<BAPIACCR09>();
                            List<BAPIPAREX> Extension2 = new List<BAPIPAREX>();
                            List<ZFI_GL_MAPPING> zfi_GL_MAPPING = new List<ZFI_GL_MAPPING>();

                            string metMatDesEnglish = GetMetMatDesEnglishFromMetNum(metNum);
                            string fisPeriod = (DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month < 10) ? "0" + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString() : DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month.ToString();
                            string headerTxtSuffix = "REC." + GetMonthName(DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Month) + DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year;
                            int avaiableCharRangeForHeaderTxt = 25 - (headerTxtSuffix.Length + 1);

                            #endregion

                            #region DocumentHeader
                            var documentHeaderFromConfig = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.DOCUMENTHEADER
                                .Where(x => x.ACCRUE_TYPE.ToLower() == accrualType.ToLower() && x.COMP_CODE == companyCode);
                            if (documentHeaderFromConfig.ToList().Count() == 0)
                            {
                                return null;
                            }
                            var docHeader = documentHeaderFromConfig.ToList()[0];

                            DocumentHeader.OBJ_TYPE = docHeader.OBJ_TYPE;
                            DocumentHeader.OBJ_KEY = docHeader.OBJ_KEY;
                            DocumentHeader.BUS_ACT = docHeader.BUS_ACT;
                            DocumentHeader.USERNAME = docHeader.USERNAME;
                            DocumentHeader.HEADER_TXT = (metMatDesEnglish.Length <= avaiableCharRangeForHeaderTxt)
                                ? metMatDesEnglish + " " + headerTxtSuffix
                                : metMatDesEnglish.Substring(0, avaiableCharRangeForHeaderTxt) + " " + headerTxtSuffix;
                            DocumentHeader.COMP_CODE = companyCode;
                            DocumentHeader.DOC_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                            DocumentHeader.PSTNG_DATE = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                            DocumentHeader.FISC_YEAR = DateTime.ParseExact(outturn.RunningDate, "dd/MM/yyyy", cultureinfo).Year.ToString();
                            DocumentHeader.FIS_PERIOD = fisPeriod;
                            DocumentHeader.DOC_TYPE = docHeader.DOC_TYPE;
                            DocumentHeader.REF_DOC_NO_LONG = model.TripNo + "." + outturn.ItemNo;
                            DocumentHeader.REASON_REV = docHeader.REASON_REV;

                            rtn.Message += "HEADER_TXT = " + DocumentHeader.HEADER_TXT + ", ";
                            rtn.Message += "DOC_DATE = " + DocumentHeader.DOC_DATE + ", ";
                            rtn.Message += "PSTNG_DATE = " + DocumentHeader.PSTNG_DATE + ", ";
                            rtn.Message += "FIS_PERIOD = " + DocumentHeader.FIS_PERIOD + "\r\n";
                            #endregion

                            #region AccountGL
                            var accountGlNotNull = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.ACCOUNTGL
                                .Where(x => x.COMP_CODE == companyCode);
                            if (accountGlNotNull.ToList().Count() == 0)
                            {
                                return null;
                            }
                            AccountGl.Add(new BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000001",
                                GL_ACCOUNT = accountGlNotNull.ToList()[0].GLACCOUNT,
                                ITEM_TEXT = DocumentHeader.HEADER_TXT,
                                //ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = accountGlNotNull.ToList()[0].COSTCENTER,
                                ORDERID = io
                            });
                            AccountGl.Add(new BAPIACGL09
                            {
                                ITEMNO_ACC = "0000000002",
                                GL_ACCOUNT = "",
                                ITEM_TEXT = DocumentHeader.HEADER_TXT,
                                //ITEM_TEXT = (metMatDesEnglish.Length > 50) ? metMatDesEnglish.Substring(0, 50) : metMatDesEnglish,
                                REF_KEY_3 = sortField,
                                COMP_CODE = companyCode,
                                VENDOR_NO = model.SupplierNo.PadLeft(10, '0'),
                                ALLOC_NMBR = model.TripNo + "." + outturn.ItemNo,
                                COSTCENTER = "",
                                ORDERID = ""
                            });
                            #endregion

                            #region CurrencyAmount
                            var doccur = Convert.ToDecimal((model.Currency.ToLower() == "usd") ? outturn.AmountUSD : outturn.AmountTHB);
                            CurrencyAmount.Add(new BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000001",
                                CURR_TYPE = "00",
                                CURRENCY = model.Currency,
                                AMT_DOCCUR = doccur,
                                AMT_DOCCURSpecified = true
                            });
                            CurrencyAmount.Add(new BAPIACCR09
                            {
                                ITEMNO_ACC = "0000000002",
                                CURR_TYPE = "00",
                                CURRENCY = model.Currency,
                                AMT_DOCCUR = doccur * -1,
                                AMT_DOCCURSpecified = true
                            });

                            #endregion

                            #region Extension2

                            var extension2ForVolume = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Volume" && x.COMP_CODE == companyCode);
                            if (extension2ForVolume.ToList().Count() == 0)
                            {
                                return null;
                            }
                            string volumnUnit = model.VolumeUnit.ToUpper();
                            string volumn = "";
                            if (volumnUnit == "BBL") volumn = outturn.BlQtyBbl;
                            else if (volumnUnit == "MT") volumn = outturn.BlQtyMt;
                            else if (volumnUnit == "L30") volumn = outturn.BlQtyMl;

                            Extension2.Add(new BAPIPAREX //VOLUME
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForVolume.ToList()[0].VALUEPART1,
                                VALUEPART2 = volumn
                            });
                            var extension2ForUOM = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "UOM" && x.COMP_CODE == companyCode);
                            if (extension2ForUOM.ToList().Count() == 0)
                            {
                                return null;
                            }
                            Extension2.Add(new BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForUOM.ToList()[0].VALUEPART1,
                                VALUEPART2 = model.VolumeUnit
                            });
                            var extension2ForBP = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.EXTENSION2
                                .Where(x => x.DATA_FOR == "Business Place" && x.COMP_CODE == companyCode);
                            if (extension2ForBP.ToList().Count() == 0)
                            {
                                return null;
                            }
                            Extension2.Add(new BAPIPAREX
                            {
                                STRUCTURE = "0000000001",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });
                            Extension2.Add(new BAPIPAREX
                            {
                                STRUCTURE = "0000000002",
                                VALUEPART1 = extension2ForBP.ToList()[0].VALUEPART1
                            });
                            #endregion

                            #region ZFI_GL-MAPPING
                            var zfiGlMapping = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING
                                .Where(x => x.COMP_CODE == companyCode);
                            if (zfiGlMapping.ToList().Count() == 0)
                            {
                                return null;
                            }
                            zfi_GL_MAPPING.Add(new ZFI_GL_MAPPING
                            {
                                ITEMNO_ACC = "0000000002",
                                TRAN_IND = zfiGlMapping.ToList()[0].TRAN_IND
                            });
                            #endregion

                            AccrualService accrualService = new AccrualService();
                            try
                            {
                                sapFiDoc = accrualService.Post(jsonConfig, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING);
                            }
                            catch (Exception ex)
                            {

                            }

                            rtn.Status = true;

                            if (rtn.Status)
                            {
                                var rtn2 = UpdateSapFiDocNo(model.PoNo, model.PoItem, outturn.ItemNo, sapFiDoc, "C");
                                if (!rtn2.Status)
                                {
                                    rtn.Status = false;

                                    //rtn.Message = "Send to SAP successfully but got the problem when updating SAP Fi Doc No";
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //rtn.Message += "Error for " + outturn.RunningDate + ": " + ex.Message + "\n";
                        rtn.Status = false;
                    }
                }
            }

            return rtn;
        }

        #endregion

        #region Get functions

        public ReturnValue GetOutturnData(PurchaseEntryViewModel_SearchData pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    #region Calculate ROE

                    var roe = 35.11;
                    int devideNum = 0;
                    decimal sumValue = 0;

                    var queryROE = (from r in context.MKT_TRN_FX_B
                                    where r.T_FXB_VALTYPE == "M" && r.T_FXB_CUR == "USD" && r.T_FXB_ENABLED == "T"
                                    join s in context.MKT_TRN_FX_H on r.T_FXB_FXHID_FK equals s.T_FXH_ID into viewD
                                    from vdl in viewD.DefaultIfEmpty()
                                    where vdl.T_FXH_BNKCODE == "BOT"
                                    select new
                                    {
                                        dFXBValue = r.T_FXB_VALUE1,
                                        dValDate = r.T_FXB_VALDATE
                                    });

                    if (queryROE != null)
                    {
                        int todayMonth = DateTime.Now.Month;
                        DateTime toDay = DateTime.Now;

                        int poMonth = DateTime.ParseExact(pModel.PoDate, "dd/MM/yyyy", cultureinfo).Month;
                        DateTime? fromDate = DateTime.ParseExact("01" + "/" + poMonth.ToString().PadLeft(2, '0') + "/" + DateTime.ParseExact(pModel.PoDate, "dd/MM/yyyy", cultureinfo).Year, "dd/MM/yyyy", cultureinfo);
                        DateTime? toDate = null;

                        if (poMonth == todayMonth) // Use 1 to last Sunday
                        {
                            bool foundSunday = false;
                            DateTime currentCalDate = DateTime.Now;
                            while (!foundSunday)
                            {
                                if (currentCalDate.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    foundSunday = true;
                                    toDate = currentCalDate;
                                }
                                else
                                {
                                    //minusDay = minusDay - 1;
                                    currentCalDate = currentCalDate.AddDays(-1);
                                }
                            }
                        }
                        else // Use all days in month
                        {
                            int tempYear = DateTime.ParseExact(pModel.PoDate, "dd/MM/yyyy", cultureinfo).Year;
                            int tempDay = DateTime.DaysInMonth(tempYear, poMonth);
                            toDate = DateTime.ParseExact(tempDay.ToString("00") + "/" + poMonth.ToString("00") + "/" + tempYear, "dd/MM/yyyy", cultureinfo);
                        }

                        foreach (var item in queryROE)
                        {
                            DateTime currentValDate = DateTime.ParseExact(item.dValDate, "yyyyMMdd", cultureinfo);
                            if (item.dFXBValue != null && (currentValDate >= fromDate && currentValDate <= toDate))
                            {
                                sumValue += Convert.ToDecimal(item.dFXBValue);
                                devideNum++;
                            }
                        }

                        if (devideNum != 0)
                        {
                            roe = Math.Round(Convert.ToDouble(sumValue / devideNum), 4);
                        }
                        else
                        {
                            roe = 0;
                        }
                    }

                    #endregion

                    #region PIT_PURCHASE_ENTRY

                    var queryPPE = (from r in context.PIT_PURCHASE_ENTRY
                                    where r.PO_NO == pModel.PoNo
                                    select new
                                    {
                                        dCalVolumeUnit = r.CAL_VOLUME_UNIT,
                                        dCalInvoiceFigure = r.CAL_INVOICE_FIGURE,
                                        dCalPriceUnit = r.CAL_PRICE_UNIT
                                    });

                    pModel.PitPurchaseEntry = new PurchaseEntryViewModel_PitPurchaseEntry();
                    if (queryPPE != null && queryPPE.Count() != 0)
                    {
                        foreach (var item in queryPPE)
                        {
                            pModel.PitPurchaseEntry = new PurchaseEntryViewModel_PitPurchaseEntry
                            {
                                CalVolumeUnit = item.dCalVolumeUnit,
                                CalInvoiceFigure = item.dCalInvoiceFigure,
                                CalPriceUnit = item.dCalPriceUnit
                            };
                            break;
                        }
                    }

                    #endregion

                    #region OUTTURN_FROM_SAP

                    var poDate = DateTime.ParseExact(pModel.PoDate, "dd/MM/yyyy", cultureinfo);

                    var query = from j in (from r in context.OUTTURN_FROM_SAP
                                           where r.TRIP_NO == pModel.TripNo
                                           join i in context.PIT_PO_ITEM on r.TRIP_NO equals i.TRIP_NO
                                           select new
                                           {
                                               dBlDate = r.BE_DATE ?? DateTime.Now,
                                               dBlQtyBbl = r.BL_QTY_BBL,
                                               dBlQtyMt = r.BL_QTY_MT,
                                               dBlQtyMl = r.BL_QTY_ML,
                                               dPostingDate = r.POSTING_DATE,
                                               dQtyBbl = r.QTY_BBL,
                                               dQtyMt = r.QTY_MT,
                                               dQtyMl = r.QTY_ML,
                                               dPrice = "",
                                               dInvoiceStatus = "",
                                               dPoNo = i.PO_NO,
                                               dPoItem = i.PO_ITEM,
                                               dDocDate = r.DOC_DATE ?? DateTime.Now
                                           })
                                join p in context.PIT_PO_PRICE on new { ColA = j.dPoNo, ColB = j.dBlDate } equals new { ColA = p.PO_NO, ColB = p.PO_DATE }
                                //where vel.PO_DATE == poDate
                                select new
                                {
                                    dBlDate = j.dBlDate,
                                    dBlQtyBbl = j.dBlQtyBbl,
                                    dBlQtyMt = j.dBlQtyMt,
                                    dBlQtyMl = j.dBlQtyMl,
                                    dPostingDate = j.dPostingDate,
                                    dQtyBbl = j.dQtyBbl,
                                    dQtyMt = j.dQtyMt,
                                    dQtyMl = j.dQtyMl,
                                    dPrice = p.UNIT_PRICE,
                                    dInvoiceStatus = p.INVOICE_STATUS,
                                    dPoNo = p.PO_NO,
                                    dPoItem = p.PO_ITEM
                                };


                    pModel.sOutturn = new List<PurchaseEntryViewModel_Outtern>();
                    int year = DateTime.ParseExact(pModel.PoDate, "dd/MM/yyyy", cultureinfo).Year;
                    int month = DateTime.ParseExact(pModel.PoDate, "dd/MM/yyyy", cultureinfo).Month;
                    int numberOfDays = DateTime.DaysInMonth(year, month);
                    query = query.Where(x => x.dInvoiceStatus.ToLower() == "ac");

                    //Cleansing data (FINAL or PRO)
                    //var finalItems = query.Where(x => x.dInvoiceStatus.ToLower() == "ac");
                    //List<DateTime?> blDateList = new List<DateTime?>();
                    //List<string> poNoList = new List<string>();
                    //List<decimal> poItemList = new List<decimal>();
                    //foreach(var finalItem in finalItems)
                    //{
                    //    blDateList.Add(finalItem.dBlDate);
                    //    poNoList.Add(finalItem.dPoNo);
                    //    poItemList.Add(finalItem.dPoItem);
                    //}
                    //var proItems = query.Where(x => !blDateList.Contains(x.dBlDate) && !poNoList.Contains(x.dPoNo) &&
                    //                                !poItemList.Contains(x.dPoItem) && x.dInvoiceStatus.ToLower() == "pro");
                    //query = finalItems.Concat(proItems);

                    if (query != null && query.Count() != 0)
                    {
                        #region Create the blank record
                        for (var i = 1; i <= numberOfDays; i++)
                        {
                            pModel.sOutturn.Add(new PurchaseEntryViewModel_Outtern
                            {
                                BLDate = "",
                                BlQtyBbl = "",
                                BlQtyMt = "",
                                BlQtyMl = "",
                                PostingDate = "",
                                RunningDate = i.ToString("00") + "/" + month.ToString("00") + "/" + year.ToString("0000"),
                                QtyBbl = "",
                                QtyMt = "",
                                QtyMl = "",
                                Price = "",
                                ROE = "",
                                AmountUSD = "",
                                AmountTHB = "",
                                Vat7 = "",
                                TotalAmountUSD = "",
                                TotalAmountTHB = ""
                            });
                        }
                        #endregion

                        foreach (var item in query)
                        {
                            #region BL Date
                            //var blDateFromDB = item.dBlDate.Value.ToString("dd/MM/yyyy");
                            string blDateToShow = "";
                            try
                            {
                                blDateToShow = item.dBlDate.ToString("dd/MM/yyyy", cultureinfo);
                            }
                            catch
                            {
                                try
                                {
                                    //blDateToShow = item.dBlDate.Value.ToString("MM/dd/yyyy", cultureinfo);
                                }
                                catch
                                {
                                    blDateToShow = item.dBlDate.ToString();
                                }

                                blDateToShow = item.dBlDate.ToString();
                            }
                            #endregion

                            #region GR Date
                            string postingDateToShow = "";
                            try
                            {
                                postingDateToShow = item.dPostingDate.Value.ToString("dd/MM/yyyy", cultureinfo);
                            }
                            catch
                            {
                                try
                                {
                                    postingDateToShow = item.dPostingDate.Value.ToString("MM/dd/yyyy", cultureinfo);
                                }
                                catch
                                {
                                    postingDateToShow = item.dPostingDate.ToString();
                                }

                                postingDateToShow = item.dPostingDate.ToString();
                            }
                            #endregion

                            if (!String.IsNullOrEmpty(blDateToShow))
                            {
                                //var record = pModel.sOutturn.Where(x => DateTime.ParseExact(x.RunningDate, "dd/MM/yyyy", cultureinfo).Date == DateTime.ParseExact(blDateToShow, "dd/MM/yyyy", cultureinfo).Date).OrderBy(x => x.BLDate).ThenBy(x => x.PostingDate).FirstOrDefault();
                                var record = pModel.sOutturn.Where(x => DateTime.ParseExact(x.RunningDate, "dd/MM/yyyy", cultureinfo).Date == DateTime.ParseExact(blDateToShow, "dd/MM/yyyy", cultureinfo).Date).FirstOrDefault();

                                int index = pModel.sOutturn.IndexOf(record);

                                if (record != null)
                                {

                                    #region Calculate Amount and VAT

                                    decimal? price = Math.Round(Convert.ToDecimal(item.dPrice), 4);
                                    decimal? amountUSD = 0;
                                    decimal? amountTHB = 0;
                                    decimal? vat7 = 0;
                                    decimal? totalAmountUSD = 0;
                                    decimal? totalAmountTHB = 0;

                                    #region Simulate ROE
                                    if (pModel.SimulateROE)
                                    {
                                        if (pModel.SimulateDataName == "FOB Price")
                                        {
                                            if (!String.IsNullOrEmpty(pModel.SimulateValue))
                                            {
                                                try
                                                {
                                                    price = Convert.ToDecimal(pModel.SimulateValue);
                                                }
                                                catch { }
                                            }
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(pModel.SimulateValue))
                                            {
                                                try
                                                {
                                                    roe = Convert.ToDouble(pModel.SimulateValue);
                                                }
                                                catch { }
                                            }
                                        }
                                    }
                                    #endregion

                                    if (String.IsNullOrEmpty(pModel.VolumeUnit) || pModel.VolumeUnit.ToLower() == "bbl")
                                    {
                                        if (String.IsNullOrEmpty(pModel.InvoiceFigure) || pModel.InvoiceFigure.ToLower() == "bl")
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                if (roe == 0)
                                                {
                                                    amountUSD = 0;
                                                }
                                                else
                                                {
                                                    amountUSD = (item.dBlQtyBbl * price) / Convert.ToDecimal(roe);
                                                }
                                            }
                                            else
                                                amountUSD = (item.dBlQtyBbl * price);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                if (roe == 0)
                                                {
                                                    amountUSD = 0;
                                                }
                                                else
                                                {
                                                    amountUSD = (item.dQtyBbl * price) / Convert.ToDecimal(roe);
                                                }
                                            }
                                            else
                                            {
                                                amountUSD = (item.dQtyBbl * price);
                                            }
                                        }
                                    }
                                    else if (String.IsNullOrEmpty(pModel.VolumeUnit) || pModel.VolumeUnit.ToLower() == "mt")
                                    {
                                        if (String.IsNullOrEmpty(pModel.InvoiceFigure) || pModel.InvoiceFigure.ToLower() == "bl")
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                if (roe == 0)
                                                {
                                                    amountUSD = 0;
                                                }
                                                else
                                                {
                                                    amountUSD = (item.dBlQtyMt * price) / Convert.ToDecimal(roe);
                                                }
                                            }
                                            else
                                            {
                                                amountUSD = (item.dBlQtyMt * price);
                                            }
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                if (roe == 0)
                                                {
                                                    amountUSD = 0;
                                                }
                                                else
                                                {
                                                    amountUSD = (item.dQtyMt * price) / Convert.ToDecimal(roe);
                                                }
                                            }
                                            else
                                            {
                                                amountUSD = (item.dQtyMt * price);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(pModel.InvoiceFigure) || pModel.InvoiceFigure.ToLower() == "bl")
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                if (roe == 0)
                                                {
                                                    amountUSD = 0;
                                                }
                                                else
                                                {
                                                    amountUSD = (item.dBlQtyMl * price) / Convert.ToDecimal(roe);
                                                }
                                            }
                                            else
                                            {
                                                amountUSD = (item.dBlQtyMl * price);
                                            }
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(pModel.PriceUnit) || pModel.PriceUnit.ToLower() == "thb")
                                            {
                                                if (roe == 0)
                                                {
                                                    amountUSD = 0;
                                                }
                                                else
                                                {
                                                    amountUSD = (item.dQtyMl * price) / Convert.ToDecimal(roe);
                                                }
                                            }
                                            else
                                            {
                                                amountUSD = (item.dQtyMl * price);
                                            }
                                        }
                                    }
                                    roe = Math.Round(Convert.ToDouble(roe), 4);
                                    amountTHB = amountUSD * Convert.ToDecimal(roe);
                                    amountUSD = Math.Round(Convert.ToDecimal(amountUSD), 4);
                                    amountTHB = Math.Round(Convert.ToDecimal(amountTHB), 4);
                                    vat7 = amountTHB * Convert.ToDecimal(0.07);
                                    totalAmountUSD = amountUSD;
                                    totalAmountTHB = amountTHB + vat7;

                                    vat7 = Math.Round(Convert.ToDecimal(vat7), 4);
                                    totalAmountUSD = Math.Round(Convert.ToDecimal(totalAmountUSD), 4);
                                    totalAmountTHB = Math.Round(Convert.ToDecimal(totalAmountTHB), 4);

                                    #endregion

                                    #region Fill data to the valid records

                                    if (String.IsNullOrEmpty(record.PostingDate))
                                    {
                                        //record.BLDate = (item.dBlDate != null) ? item.dBlDate.Value.ToString("dd/MM/yyyy") : "";
                                        record.BLDate = blDateToShow;
                                        record.BlQtyBbl = item.dBlQtyBbl.Value.ToString("#,##0.0000");
                                        record.BlQtyMt = item.dBlQtyMt.Value.ToString("#,##0.0000");
                                        record.BlQtyMl = item.dBlQtyMl.Value.ToString("#,##0.0000");
                                        //record.PostingDate = (item.dPostingDate != null) ? item.dPostingDate.Value.ToString("dd/MM/yyyy") : "";
                                        record.PostingDate = postingDateToShow;
                                        record.QtyBbl = item.dQtyBbl.Value.ToString("#,##0.0000");
                                        record.QtyMt = item.dQtyMt.Value.ToString("#,##0.0000");
                                        record.QtyMl = item.dQtyMl.Value.ToString("#,##0.0000");
                                        record.Price = (item.dPrice == null) ? "" : item.dPrice.Value.ToString("#,##0.0000");
                                        record.ROE = roe.ToString("#,##0.0000");
                                        record.AmountUSD = amountUSD.Value.ToString("#,##0.0000");
                                        record.AmountTHB = amountTHB.Value.ToString("#,##0.0000");
                                        record.Vat7 = vat7.Value.ToString("#,##0.0000");
                                        record.TotalAmountUSD = totalAmountUSD.Value.ToString("#,##0.0000");
                                        record.TotalAmountTHB = totalAmountTHB.Value.ToString("#,##0.0000");
                                        record.Valid = true;
                                    }
                                    else
                                    {
                                        if (DateTime.ParseExact(record.PostingDate, "dd/MM/yyyy", cultureinfo).Date == item.dPostingDate.Value.Date) //Found existing BL Date, plus the data to the existing record
                                        {
                                            record.BlQtyBbl = (((String.IsNullOrEmpty(record.BlQtyBbl)) ? 0 : Convert.ToDecimal(record.BlQtyBbl)) + Convert.ToDecimal(item.dBlQtyBbl)).ToString("#,##0.0000");
                                            record.BlQtyMt = (((String.IsNullOrEmpty(record.BlQtyMt)) ? 0 : Convert.ToDecimal(record.BlQtyMt)) + Convert.ToDecimal(item.dBlQtyMt)).ToString("#,##0.0000");
                                            record.BlQtyMl = (((String.IsNullOrEmpty(record.BlQtyMl)) ? 0 : Convert.ToDecimal(record.BlQtyMl)) + Convert.ToDecimal(item.dBlQtyMl)).ToString("#,##0.0000");
                                            record.QtyBbl = (((String.IsNullOrEmpty(record.QtyBbl)) ? 0 : Convert.ToDecimal(record.QtyBbl)) + Convert.ToDecimal(item.dQtyBbl)).ToString("#,##0.0000");
                                            record.QtyMt = (((String.IsNullOrEmpty(record.QtyMt)) ? 0 : Convert.ToDecimal(record.QtyMt)) + Convert.ToDecimal(item.dQtyMt)).ToString("#,##0.0000");
                                            record.QtyMl = (((String.IsNullOrEmpty(record.QtyMl)) ? 0 : Convert.ToDecimal(record.QtyMl)) + Convert.ToDecimal(item.dQtyMl)).ToString("#,##0.0000");
                                            record.AmountUSD = (((String.IsNullOrEmpty(record.AmountUSD)) ? 0 : Convert.ToDecimal(record.AmountUSD)) + Convert.ToDecimal(amountUSD)).ToString("#,##0.0000");
                                            record.AmountTHB = (((String.IsNullOrEmpty(record.AmountTHB)) ? 0 : Convert.ToDecimal(record.AmountTHB)) + Convert.ToDecimal(amountTHB)).ToString("#,##0.0000");
                                            record.TotalAmountUSD = (((String.IsNullOrEmpty(record.TotalAmountUSD)) ? 0 : Convert.ToDecimal(record.TotalAmountUSD)) + Convert.ToDecimal(totalAmountUSD)).ToString("#,##0.0000");
                                            record.TotalAmountTHB = (((String.IsNullOrEmpty(record.TotalAmountTHB)) ? 0 : Convert.ToDecimal(record.TotalAmountTHB)) + Convert.ToDecimal(totalAmountTHB)).ToString("#,##0.0000");
                                        }
                                        else //Found existing BL Date but difference Posting Date, insert the new record
                                        {
                                            pModel.sOutturn.Insert(index + 1, new PurchaseEntryViewModel_Outtern
                                            {
                                                //BLDate = (item.dBlDate != null) ? item.dBlDate.Value.ToString("dd/MM/yyyy") : "",
                                                BLDate = blDateToShow,
                                                BlQtyBbl = item.dBlQtyBbl.Value.ToString("#,##0.0000"),
                                                BlQtyMt = item.dBlQtyMt.Value.ToString("#,##0.0000"),
                                                BlQtyMl = item.dBlQtyMl.Value.ToString("#,##0.0000"),
                                                PostingDate = postingDateToShow,
                                                RunningDate = blDateToShow,
                                                QtyBbl = item.dQtyBbl.Value.ToString("#,##0.0000"),
                                                QtyMt = item.dQtyMt.Value.ToString("#,##0.0000"),
                                                QtyMl = item.dQtyMl.Value.ToString("#,##0.0000"),
                                                Price = (item.dPrice == null) ? "" : item.dPrice.Value.ToString("#,##0.0000"),
                                                ROE = roe.ToString("#,##0.0000"),
                                                AmountUSD = amountUSD.Value.ToString("#,##0.0000"),
                                                AmountTHB = amountTHB.Value.ToString("#,##0.0000"),
                                                Vat7 = vat7.Value.ToString("#,##0.0000"),
                                                TotalAmountUSD = totalAmountUSD.Value.ToString("#,##0.0000"),
                                                TotalAmountTHB = totalAmountTHB.Value.ToString("#,##0.0000"),
                                                Valid = true
                                            });
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }

                        pModel.sOutturn = pModel.sOutturn.OrderBy(x => x.RunningDate).ThenBy(x => x.PostingDate).ToList();

                        int itemNo = 1;
                        foreach (var outturn in pModel.sOutturn)
                        {
                            outturn.ItemNo = itemNo.ToString();
                            itemNo++;
                        }

                        //Restore the value in PIT_PURCHASE_ENTRY to table (only for the record which already send to SAP)
                        decimal poItem = Convert.ToDecimal(pModel.PoItem);
                        foreach (var outturn in pModel.sOutturn.Where(x => !String.IsNullOrEmpty(x.BLDate) && !String.IsNullOrEmpty(x.PostingDate)))
                        {
                            decimal outturnItemNo = Convert.ToDecimal(outturn.ItemNo);
                            var queryExistingPPE = (from p in context.PIT_PURCHASE_ENTRY
                                                    where pModel.PoNo == p.PO_NO
                                                    && poItem == p.PO_ITEM
                                                    && outturnItemNo == p.ITEM_NO
                                                    && (!String.IsNullOrEmpty(p.SAP_FI_DOC_NO) || p.SAP_FI_DOC_STATUS.ToLower() == "reversed")
                                                    select new
                                                    {
                                                        dPrice = p.PRICE,
                                                        dROE = p.ROE,
                                                        dAmountUSD = p.AMOUNT_USD,
                                                        dAmountTHB = p.AMOUNT_THB,
                                                        dVat = p.VAT,
                                                        dTotalAmountUSD = p.TOTAL_USD,
                                                        dTotalAmountTHB = p.TOTAL_THB
                                                    });
                            if (queryExistingPPE != null && queryExistingPPE.ToList().Count > 0)
                            {
                                outturn.Price = queryExistingPPE.ToList()[0].dPrice.Value.ToString("#,##0.0000");
                                outturn.ROE = queryExistingPPE.ToList()[0].dROE.Value.ToString("#,##0.0000");
                                outturn.AmountUSD = queryExistingPPE.ToList()[0].dAmountUSD.Value.ToString("#,##0.0000");
                                outturn.AmountTHB = queryExistingPPE.ToList()[0].dAmountTHB.Value.ToString("#,##0.0000");
                                outturn.Vat7 = queryExistingPPE.ToList()[0].dVat.Value.ToString("#,##0.0000");
                                outturn.TotalAmountUSD = queryExistingPPE.ToList()[0].dTotalAmountUSD.Value.ToString("#,##0.0000");
                                outturn.TotalAmountTHB = queryExistingPPE.ToList()[0].dTotalAmountTHB.Value.ToString("#,##0.0000");
                            }
                        }


                        var queryForSapFiDoc = (from p in context.PIT_PURCHASE_ENTRY
                                                where p.PO_NO == pModel.PoNo && p.PO_ITEM == poItem
                                                select new
                                                {
                                                    dItemNo = p.ITEM_NO,
                                                    dSapFiDocNo = p.SAP_FI_DOC_NO,
                                                    dSapFiDocStatus = p.SAP_FI_DOC_STATUS,
                                                    dLastUpdate = p.UPDATED_DATE,
                                                    dUpdatedBy = p.UPDATED_BY
                                                });
                        if (queryForSapFiDoc != null && queryForSapFiDoc.Count() != 0)
                        {
                            foreach (var outturn in pModel.sOutturn)
                            {
                                decimal currentItemNo = Convert.ToDecimal(outturn.ItemNo);
                                var record = queryForSapFiDoc.Where(x => x.dItemNo == currentItemNo);
                                if (record.Count() != 0)
                                {
                                    outturn.SAPFIDoc = record.ToList()[0].dSapFiDocNo;
                                    outturn.SAPFIDocStatus = record.ToList()[0].dSapFiDocStatus;
                                    outturn.UpdatedDate = record.ToList()[0].dLastUpdate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                                    outturn.UpdatedBy = record.ToList()[0].dUpdatedBy;

                                    continue;
                                }
                            }
                        }
                    }

                    #endregion                        
                }
            }
            catch (Exception ex)
            {
                pModel.ExceptionMessage = ex.Message;
            }

            return null;
        }

        public void GetPitPurchaseEntry(PurchaseEntryViewModel_SearchData pModel)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var queryPPE = (from r in context.PIT_PURCHASE_ENTRY
                                where r.PO_NO == pModel.PoNo
                                select new
                                {
                                    dBLDate = r.BL_DATE,
                                    dGRDate = r.GR_DATE,
                                    dPrice = r.PRICE,
                                    dROE = r.ROE,
                                    dAmountUSD = r.AMOUNT_USD,
                                    dAmountTHB = r.AMOUNT_THB,
                                    dVAT = r.VAT,
                                    dTotalUSD = r.TOTAL_USD,
                                    dTotalTHB = r.TOTAL_THB
                                });
                if (queryPPE != null && queryPPE.Count() != 0)
                {
                    foreach (var item in queryPPE)
                    {
                        foreach (var outturn in pModel.sOutturn)
                        {
                            if (outturn.Valid)
                            {
                                DateTime blDate = DateTime.ParseExact(outturn.BLDate, "dd/MM/yyyy", cultureinfo);
                                DateTime grDate = DateTime.ParseExact(outturn.PostingDate, "dd/MM/yyyy", cultureinfo);

                                if (blDate.Date == item.dBLDate.Value.Date && grDate.Date == item.dGRDate.Value.Date)
                                {
                                    outturn.Price = item.dPrice.Value.ToString("#,##0.0000");
                                    outturn.ROE = item.dROE.Value.ToString("#,##0.0000");
                                    outturn.AmountUSD = item.dAmountUSD.Value.ToString("#,##0.0000");
                                    outturn.AmountTHB = item.dAmountTHB.Value.ToString("#,##0.0000");
                                    outturn.Vat7 = item.dVAT.Value.ToString("#,##0.0000");
                                    outturn.TotalAmountUSD = item.dTotalUSD.Value.ToString("#,##0.0000");
                                    outturn.TotalAmountTHB = item.dTotalTHB.Value.ToString("#,##0.0000");

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        public List<SelectListItem> GetProduct()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                PurchaseOrderServiceModel serviceModel = new PurchaseOrderServiceModel();
                return serviceModel.GetFeedStockPIT();
            }
            catch
            {
                return null;
            }
        }

        public List<SelectListItem> GetCompany()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PIT_PO_HEADER
                                 join c in context.MT_COMPANY on p.COMPANY_CODE equals c.MCO_COMPANY_CODE into viewC
                                 from vdc in viewC.DefaultIfEmpty()
                                 where vdc.MCO_COMPANY_CODE == "1100" || vdc.MCO_COMPANY_CODE == "1400"
                                 select new
                                 {
                                     dCompanyCode = p.COMPANY_CODE,
                                     dCompanyName = vdc.MCO_SHORT_NAME
                                 }).Distinct();
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.dCompanyName, Value = item.dCompanyCode });
                        }
                    }
                }
                return selectList;
            }
            catch
            {
                return null;
            }
        }

        public List<SelectListItem> GetSupplier()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PIT_PO_ITEM
                                 select new
                                 {
                                     dSupplier = p.ACC_NUM_VENDOR
                                 }).Distinct();
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.dSupplier, Value = item.dSupplier });
                        }
                    }
                }
                return selectList;
            }
            catch
            {
                return null;
            }
        }

        public string GetCompanyCodeFromPoNo(string poNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from c in context.PIT_PO_HEADER
                                 where c.PO_NO == poNo
                                 select new
                                 {
                                     dCompanyCode = c.COMPANY_CODE
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dCompanyCode;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetPlantFromPoItem(string poNo, string poItem)
        {
            var poItemCondition = Convert.ToDecimal(poItem);
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PIT_PO_ITEM
                                 where p.PO_NO == poNo && p.PO_ITEM == poItemCondition
                                 select new
                                 {
                                     dPlant = p.PLANT
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dPlant;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMetNumFromPoItem(string poNo, string poItem)
        {
            var poItemCondition = Convert.ToDecimal(poItem);
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PIT_PO_ITEM
                                 where p.PO_NO == poNo && p.PO_ITEM == poItemCondition
                                 select new
                                 {
                                     dMetNum = p.MET_NUM
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dMetNum;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetSortFieldFromSupplier(string accNumVendor)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_VENDOR
                                 where p.VND_ACC_NUM_VENDOR.Contains(accNumVendor)
                                 select new
                                 {
                                     dSortField = p.VND_SORT_FIELD
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dSortField;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetIOFromMetNum(string metNum, string companyCode)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_MATERIALS
                                 where p.MET_NUM == metNum
                                 select new
                                 {
                                     dIO = p.MET_IO,
                                     dIO_TLB = p.MET_IO_TLB
                                 });
                    if (query != null)
                    {
                        if (companyCode == "1100")
                            return query.ToList()[0].dIO;
                        else
                            return query.ToList()[0].dIO_TLB;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMetMatDesEnglishFromMetNum(string metNum)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_MATERIALS
                                 where p.MET_NUM == metNum
                                 select new
                                 {
                                     dMetMatDesEnglish = p.MET_MAT_DES_ENGLISH
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dMetMatDesEnglish;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public AccrualSetting GetAccrualSettings()
        {
            string JsonD = MasterData.GetJsonMasterSetting("PIT_ACCRUAL_POSTING_CONFIG");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);

            AccrualSetting myDeserializedObjList = (AccrualSetting)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(AccrualSetting));
            return myDeserializedObjList;
        }

        public string GetMonthName(int month)
        {
            switch (month)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "Jun";
                case 7: return "Jul";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "";
            }
        }

        public void ReGetROEFromDB(PurchaseEntryViewModel_SearchData model)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                decimal poItem = Convert.ToDecimal(model.PoItem);
                var query = (from r in context.PIT_PURCHASE_ENTRY
                             where r.PO_NO == model.PoNo && r.PO_ITEM == poItem
                             select new
                             {
                                 dItemNo = r.ITEM_NO,
                                 dROE = r.ROE
                             });

                if (query != null && query.ToList().Count() > 0)
                {
                    foreach (var item in query)
                    {
                        foreach (var itemModel in model.sOutturn)
                        {
                            if (Convert.ToDecimal(itemModel.ItemNo) == item.dItemNo)
                            {
                                itemModel.ROE = item.dROE.Value.ToString("#,##0.0000");
                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Export to Excel functions

        public string ExportToExcel(PurchaseEntryViewModel_SearchData model)
        {
            try
            {
                DataTable dtData = new DataTable();
                string msg = "";

                String xFileName = "Temp_Purchase_Entry_Report";
                String lFileName = ".xlsx";
                String path = System.Web.HttpContext.Current.Server.MapPath(@"..\..\Areas\CPAIMVC\Report\ExcelTemplate\");
                String newFilename = xFileName.Replace("Temp_", "") + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss").ToString() + lFileName;
                String newExcelFileNameFullPath = System.Web.HttpContext.Current.Server.MapPath(@"..\..\Areas\CPAIMVC\Report\Temp\") + newFilename;

                try
                {
                    System.IO.File.Copy(path + "Temp_Purchase_Entry_Report" + lFileName, newExcelFileNameFullPath, true);
                }
                catch (Exception ex)
                {
                    msg = "error: 1_" + ex.Message.ToString();
                    goto Next_Line_Exit;
                }

                xFileName = newFilename;
                int irows = 0;

                try
                {

                    ExcelPackage excelApp = new ExcelPackage();
                    ExcelWorksheet excelSheets;

                    try
                    {
                        excelApp = new ExcelPackage(new System.IO.FileInfo(newExcelFileNameFullPath));
                        excelSheets = (ExcelWorksheet)excelApp.Workbook.Worksheets[1];

                        irows = 2;
                        excelSheets.Cells[irows, 1].Value = "Trip No : " + model.TripNo;

                        Double bl_volume_bbl = 0;
                        Double bl_volume_mt = 0;
                        Double bl_volume_l30 = 0;
                        Double gr_volume_bbl = 0;
                        Double gr_volume_mt = 0;
                        Double gr_volume_l30 = 0;

                        Double cf_usd = 0;
                        Double cf_thb = 0;
                        Double cf_vat_thb = 0;
                        Double cf_total_usd = 0;
                        Double cf_total_thb = 0;

                        irows = 6;

                        for (int i = 0; i < model.sOutturn.Count(); i++)
                        {
                            irows += 1;
                            try
                            {
                                CopyRowTo(excelSheets, excelSheets, 6, irows);
                            }
                            catch { }

                            #region "detail"  
                            excelSheets.Cells[irows, 1].Value = (i + 1).ToString() + "";
                            excelSheets.Cells[irows, 2].Value = String.IsNullOrEmpty(model.sOutturn[i].RunningDate) ? "" : model.sOutturn[i].RunningDate.ToString() + "";
                            excelSheets.Cells[irows, 3].Value = String.IsNullOrEmpty(model.sOutturn[i].BlQtyBbl) ? "" : model.sOutturn[i].BlQtyBbl.ToString() + "";
                            excelSheets.Cells[irows, 4].Value = String.IsNullOrEmpty(model.sOutturn[i].BlQtyMt) ? "" : model.sOutturn[i].BlQtyMt.ToString() + "";
                            excelSheets.Cells[irows, 5].Value = String.IsNullOrEmpty(model.sOutturn[i].BlQtyMl) ? "" : model.sOutturn[i].BlQtyMl.ToString() + "";
                            excelSheets.Cells[irows, 6].Value = String.IsNullOrEmpty(model.sOutturn[i].PostingDate) ? "" : model.sOutturn[i].PostingDate.ToString() + "";
                            excelSheets.Cells[irows, 7].Value = String.IsNullOrEmpty(model.sOutturn[i].QtyBbl) ? "" : model.sOutturn[i].QtyBbl.ToString().ToString() + "";
                            excelSheets.Cells[irows, 8].Value = String.IsNullOrEmpty(model.sOutturn[i].QtyMt) ? "" : model.sOutturn[i].QtyMt.ToString().ToString() + "";
                            excelSheets.Cells[irows, 9].Value = String.IsNullOrEmpty(model.sOutturn[i].QtyMl) ? "" : model.sOutturn[i].QtyMl.ToString().ToString() + "";
                            excelSheets.Cells[irows, 10].Value = String.IsNullOrEmpty(model.sOutturn[i].Price) ? "" : model.sOutturn[i].Price.ToString().ToString() + "";
                            excelSheets.Cells[irows, 11].Value = String.IsNullOrEmpty(model.sOutturn[i].ROE) ? "" : model.sOutturn[i].ROE.ToString().ToString() + "";
                            excelSheets.Cells[irows, 12].Value = String.IsNullOrEmpty(model.sOutturn[i].AmountUSD) ? "" : model.sOutturn[i].AmountUSD.ToString().ToString() + "";
                            excelSheets.Cells[irows, 13].Value = String.IsNullOrEmpty(model.sOutturn[i].AmountTHB) ? "" : model.sOutturn[i].AmountTHB.ToString().ToString() + "";
                            excelSheets.Cells[irows, 14].Value = String.IsNullOrEmpty(model.sOutturn[i].Vat7) ? "" : model.sOutturn[i].Vat7.ToString().ToString() + "";
                            excelSheets.Cells[irows, 15].Value = String.IsNullOrEmpty(model.sOutturn[i].TotalAmountUSD) ? "" : model.sOutturn[i].TotalAmountUSD.ToString().ToString() + "";
                            excelSheets.Cells[irows, 16].Value = String.IsNullOrEmpty(model.sOutturn[i].TotalAmountTHB) ? "" : model.sOutturn[i].TotalAmountTHB.ToString().ToString() + "";
                            excelSheets.Cells[irows, 17].Value = String.IsNullOrEmpty(model.sOutturn[i].SAPFIDoc) ? "" : model.sOutturn[i].SAPFIDoc.ToString().ToString() + "";
                            excelSheets.Cells[irows, 18].Value = String.IsNullOrEmpty(model.sOutturn[i].SAPFIDocStatus) ? "" : model.sOutturn[i].SAPFIDocStatus.ToString().ToString() + "";
                            excelSheets.Cells[irows, 19].Value = String.IsNullOrEmpty(model.sOutturn[i].UpdatedDate) ? "" : model.sOutturn[i].UpdatedDate.ToString().ToString() + "";
                            excelSheets.Cells[irows, 20].Value = String.IsNullOrEmpty(model.sOutturn[i].UpdatedBy) ? "" : model.sOutturn[i].UpdatedBy.ToString().ToString() + "";

                            bl_volume_bbl += CnDoubleRe0(model.sOutturn[i].BlQtyBbl.ToString().Replace(",", ""));
                            bl_volume_l30 += CnDoubleRe0(model.sOutturn[i].BlQtyMl.ToString().Replace(",", ""));
                            bl_volume_mt += CnDoubleRe0(model.sOutturn[i].BlQtyMt.ToString().Replace(",", ""));
                            gr_volume_bbl += CnDoubleRe0(model.sOutturn[i].QtyBbl.ToString().Replace(",", ""));
                            gr_volume_mt += CnDoubleRe0(model.sOutturn[i].QtyMt.ToString().Replace(",", ""));
                            gr_volume_l30 += CnDoubleRe0(model.sOutturn[i].QtyMl.ToString().Replace(",", ""));

                            cf_usd += CnDoubleRe0(model.sOutturn[i].AmountUSD.ToString().Replace(",", ""));
                            cf_thb += CnDoubleRe0(model.sOutturn[i].AmountTHB.ToString().Replace(",", ""));
                            cf_vat_thb += CnDoubleRe0(model.sOutturn[i].Vat7.ToString().Replace(",", ""));
                            cf_total_usd += CnDoubleRe0(model.sOutturn[i].TotalAmountUSD.ToString().Replace(",", ""));
                            cf_total_thb += CnDoubleRe0(model.sOutturn[i].TotalAmountTHB.ToString().Replace(",", ""));
                            #endregion "detail"                        

                        }

                        #region "footer"
                        irows += 1;
                        excelSheets.Cells[irows, 3].Value = bl_volume_bbl.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 4].Value = bl_volume_mt.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 5].Value = bl_volume_l30.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 6].Value = "";
                        excelSheets.Cells[irows, 7].Value = gr_volume_bbl.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 8].Value = gr_volume_mt.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 9].Value = gr_volume_l30.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 10].Value = "";
                        excelSheets.Cells[irows, 11].Value = "";
                        //excelSheets.Cells[irows, 12].Value = "";
                        //excelSheets.Cells[irows, 13].Value = "";
                        excelSheets.Cells[irows, 12].Value = cf_usd.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 13].Value = cf_thb.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 14].Value = cf_vat_thb.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 15].Value = cf_total_usd.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 16].Value = cf_total_thb.ToString("#,##0.0000");
                        excelSheets.Cells[irows, 17].Value = "";
                        excelSheets.Cells[irows, 18].Value = "";
                        excelSheets.Cells[irows, 19].Value = "";
                        excelSheets.Cells[irows, 20].Value = "";
                        #endregion

                        excelSheets.DeleteRow(6, 1, true);

                        excelApp.Save();

                        excelApp.Dispose();
                        excelApp = null;

                    }
                    catch (Exception ex)
                    {
                        msg = "error: 2_" + ex.Message.ToString(); goto Next_Line_Exit;
                    }
                }
                catch (Exception ex)
                {
                    msg = "error: 3_" + ex.Message.ToString(); goto Next_Line_Exit;
                }
                msg = newExcelFileNameFullPath;

                Next_Line_Exit:
                return msg;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Double CnDoubleRe0(Object boject_value)
        {
            try
            {
                return Convert.ToDouble(boject_value);
            }
            catch { return 0; }
        }

        private void CopyRowTo(ExcelWorksheet input, ExcelWorksheet output, int row_in, int row_out)
        {
            input.InsertRow(row_out, 1, row_in);
            ExcelRange rng1 = input.SelectedRange[row_in, 1, row_in, 26];
            ExcelRange rng2 = input.SelectedRange[row_out, 1, row_out, 26];
            rng1.Copy(rng2);
        }

        #endregion
    }

    public class AccrualSetting
    {
        public PIT_ACCRUAL_POSTING_CONFIG PIT_ACCRUAL_POSTING_CONFIG { get; set; }
    }

    public class PIT_ACCRUAL_POSTING_CONFIG
    {
        public List<DOCUMENTHEADER> DOCUMENTHEADER { get; set; }
        public List<ACCOUNTGL> ACCOUNTGL { get; set; }
        public List<EXTENSION2> EXTENSION2 { get; set; }
        public List<ZFI_GL_MAPPING2> ZFI_GL_MAPPING { get; set; }
    }

    public class DOCUMENTHEADER
    {
        public string ACCRUE_TYPE { get; set; }
        public string BUS_ACT { get; set; }
        public string COMP_CODE { get; set; }
        public string DOC_TYPE { get; set; }
        public string OBJ_KEY { get; set; }
        public string OBJ_TYPE { get; set; }
        public string USERNAME { get; set; }
        public string REASON_REV { get; set; }
        public string REVERSE_DATE { get; set; }
    }

    public class ACCOUNTGL
    {
        public string DATA_FOR { get; set; }
        public string COMP_CODE { get; set; }
        public string COSTCENTER { get; set; }
        public string GLACCOUNT { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SUPPLIER_CODE { get; set; }
    }

    public class EXTENSION2
    {
        public string DATA_FOR { get; set; }
        public string COMP_CODE { get; set; }
        public string VALUEPART1 { get; set; }
        public string VALUEPART2 { get; set; }
    }

    public class ZFI_GL_MAPPING2
    {
        public string DATA_FOR { get; set; }
        public string COMP_CODE { get; set; }
        public string TRAN_IND { get; set; }
    }

    public class SPVENDER
    {
        public string VENDER_CODE { get; set; }
        public string ITEM_NO_TXT { get; set; }
    }
}