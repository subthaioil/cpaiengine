﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using ProjectCPAIEngine.DAL.DALPCF;
using com.pttict.engine.utility;
using OfficeOpenXml;
using System.Data;
using System.IO;
using com.pttict.sap.Interface.sap.accrual.post;
using com.pttict.sap.Interface.Service;
using com.pttict.downstream.common.utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class PurchaseEntryVesselServiceModel : BasicBean
    {
        //ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ReturnValue getMaterialsVessel(ref PurchaseEntryVesselViewModel pModel,string TripNo,string MetNum)
        {
            PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();
            ReturnValue rtn = new ReturnValue();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";
            #region Checking requred conditions

            if (String.IsNullOrEmpty(TripNo))
            {
                rtn.Message = "Trip No. is empty.";
                rtn.Status = false;
                return rtn;
            }
            else if (String.IsNullOrEmpty(MetNum))
            {
                rtn.Message = "Crude Type is empty.";
                rtn.Status = false;
                return rtn;
            }
            #endregion

            #region "Query form DB

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var query_HEADER = (from phe in context.PCF_HEADER
                                        join otr in context.OUTTURN_FROM_SAP on phe.PHE_TRIP_NO equals otr.TRIP_NO
                                        join mtm in context.MT_MATERIALS on otr.MAT_NUM equals mtm.MET_NUM
                                        join mtv in context.MT_VEHICLE on phe.PHE_VESSEL equals mtv.VEH_ID
                                        where phe.PHE_TRIP_NO.ToUpper().Equals(TripNo.ToUpper()) &&
                                            otr.MAT_NUM == MetNum
                                        select new
                                        {
                                            TripNo = phe.PHE_TRIP_NO,
                                            Vessel = phe.PHE_VESSEL,
                                            MetNum = MetNum,
                                            txtVeh = mtv != null ? mtv.VEH_VEH_TEXT : phe.PHE_VESSEL,
                                            txtMet = mtm != null ? mtm.MET_MAT_DES_ENGLISH : MetNum,
                                            PostingDate = otr.POSTING_DATE,
                                            BLQtyML = otr.BL_QTY_ML,
                                            BLQtyMT = otr.BL_QTY_MT,
                                            BLQtyBBL = otr.BL_QTY_BBL,
                                            QtyML = otr.QTY_ML,
                                            QtyMT = otr.QTY_MT,
                                            QtyBBL = otr.QTY_BBL,
                                            CompanyCode = phe.PHE_COMPANY_CODE
                                        }).ToList();

                    var query_MATERIAL = (from pma in context.PCF_MATERIAL
                                          join pmp in context.PCF_MATERIAL_PRICE on pma.PMA_TRIP_NO equals pmp.PMP_TRIP_NO
                                          join ctv in context.PCF_CUSTOMS_VAT on pma.PMA_TRIP_NO equals ctv.TRIP_NO
                                          join ven in context.MT_VENDOR on pma.PMA_SUPPLIER equals ven.VND_ACC_NUM_VENDOR
                                          where pma.PMA_TRIP_NO.ToUpper().Equals(TripNo.ToUpper()) &&
                                          pma.PMA_MET_NUM == MetNum &&
                                          pma.PMA_ITEM_NO == pmp.PMP_MAT_ITEM_NO &&
                                          pma.PMA_ITEM_NO == ctv.MAT_ITEM_NO
                                          select new
                                          {
                                              BLDate = pma.PMA_BL_DATE,
                                              DueDate = pma.PMA_DUE_DATE,
                                              Supplier = pma.PMA_SUPPLIER,
                                              TxtSupplier = ven.VND_NAME1,
                                              VolumeBBL = pma.PMA_VOLUME_BBL,
                                              VolumeML = pma.PMA_VOLUME_ML,
                                              VolumeMT = pma.PMA_VOLUME_MT,
                                              MatItemNo = pma.PMA_ITEM_NO,
                                              SupplierNo = pma.PMA_SUPPLIER,
                                              Currency = pmp.PMP_CURRENCY,
                                              CheckStatus = pmp.PMP_PRICE_STATUS == "A" ? 1 : pmp.PMP_PRICE_STATUS == "P" ? 2 : 3,
                                              PriceUnitUSD = pmp.PMP_BASE_PRICE,
                                              FxAgreement = pmp.PMP_FX_AGREEMENT,
                                              ItemNo = pmp.PMP_ITEM_NO,
                                              PriceStatus = pmp.PMP_PRICE_STATUS,
                                              TotalPrice = pmp.PMP_TOTAL_PRICE,
                                              INS_RATE =ctv.INSURANCE_RATE,
                                              InvoiceNo = pmp.PMP_INVOICE == null ? "No" : pmp.PMP_INVOICE,
                                              ROE = ctv.ROE,
                                              VatImportDuty = ctv.IMPORT_DUTY
                                          }).OrderBy(p => p.CheckStatus).OrderBy(p => p.MatItemNo).ToList();
                    //.OrderByDescending(a => a.PriceStatus == "A").OrderByDescending(a => a.PriceStatus == "P")
                    var query_F = (from fed in context.PCF_FREIGHT
                                         join fedt in context.PCF_MT_FREIGHT_TYPE on fed.PFR_FREIGHT_SUB_TYPE equals fedt.PMF_CODE
                                         join van in context.MT_VENDOR on fed.PFR_SUPPLIER equals van.VND_ACC_NUM_VENDOR into vanj
                                         from van in vanj.DefaultIfEmpty()
                                         where fed.PFR_TRIP_NO == TripNo
                                         select new
                                         {
                                             fed,
                                             fedname = fedt.PMF_NAME,
                                             vanname = van == null ? "Unknow" : van.VND_NAME1
                                         });
                    var query_FREIGHT = query_F.Count() != 0 ? query_F.ToList().OrderBy(z => z.fed.PFR_ITEM_NO) : null;

                    string JsonMaster_PTT = MasterData.GetJsonMasterSetting("PCF_PTT_AGREEMENT");
                    PCF_PTT_AGREEMENT dataList = (PCF_PTT_AGREEMENT)Newtonsoft.Json.JsonConvert.DeserializeObject(JsonMaster_PTT.ToString(), typeof(PCF_PTT_AGREEMENT));

                    decimal total_All = Convert.ToDecimal(context.PCF_MATERIAL.Where(a => a.PMA_TRIP_NO == TripNo).Select(a => a.PMA_VOLUME_MT).Sum());
                    double total_Not_All = Convert.ToDouble(context.PCF_MATERIAL.Where(a => a.PMA_TRIP_NO == TripNo && a.PMA_MET_NUM != MetNum).Select(a => a.PMA_VOLUME_MT).Sum());
                    decimal total_Met = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeMT).Sum());
                    decimal total_BL_MT = Convert.ToDecimal(query_HEADER.Select(a => a.BLQtyMT).Sum());
                    decimal total_BL_ML = Convert.ToDecimal(query_HEADER.Select(a => a.BLQtyML).Sum());
                    decimal total_BL_BBL = Convert.ToDecimal(query_HEADER.Select(a => a.BLQtyBBL).Sum());
                    decimal total_OT_MT = Convert.ToDecimal(query_HEADER.Select(a => a.QtyMT).Sum());
                    decimal total_OT_ML = Convert.ToDecimal(query_HEADER.Select(a => a.QtyML).Sum());
                    decimal total_OT_BBL = Convert.ToDecimal(query_HEADER.Select(a => a.QtyBBL).Sum());
                    decimal total_VOL_MT = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeMT).Sum());
                    decimal total_VOL_ML = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeML).Sum());
                    decimal total_VOL_BBL = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeBBL).Sum());
                    double total_IMPORT_DUTY = Convert.ToDouble(query_MATERIAL.Select(a => a.VatImportDuty).Sum());

                    var SumNetTotalVolumeBBL = 0.0;
                    var SumNetTotalPriceBBLUSD = 0.0;
                    var SumNetTotalPriceBBLTHB = 0.0;
                    var SumNetTotalVolumeML = 0.0;
                    var SumNetTotalPriceMLUSD = 0.0;
                    var SumNetTotalPriceMLTHB = 0.0;
                    var SumNetTotalVolumeMT = 0.0;
                    var SumNetTotalPriceMTUSD = 0.0;
                    var SumNetTotalPriceMTTHB = 0.0;
                    string PostingDate = "";

                    if (query_HEADER.Count() != 0)
                    {
                        string INS_RATE = query_MATERIAL[0].INS_RATE.ToString();
                        string Currency = query_MATERIAL[0].Currency;
                        string SupplierNo = query_MATERIAL[0].SupplierNo;
                        string BLDate = Convert.ToDateTime((query_MATERIAL.OrderBy(p => p.BLDate).ToList()[0].BLDate).ToString()).ToString(format, provider);
                        string DueDate = Convert.ToDateTime((query_MATERIAL.OrderBy(p => p.DueDate).ToList()[0].DueDate).ToString()).ToString(format, provider);
                        string FrtROE = GetRateExchange(DateTime.ParseExact(BLDate, format, provider), context);
                        decimal DDutyROE = Convert.ToDecimal(query_MATERIAL.Sum(a => a.ROE)) / query_MATERIAL.Count();
                        string SDutyROE = String.IsNullOrEmpty(DDutyROE.ToString()) ? "1" : Double.Parse(DDutyROE.ToString()).ToString("#,##0.0000");
                        int ItemNo = 10;
                        int index = 0;
                        foreach (var item in query_HEADER)
                        {
                            string FOBName = "";
                            if (index == 0)
                            {
                                PostingDate = Convert.ToDateTime(item.PostingDate.ToString()).ToString(format, provider);
                                pModel.PriceList.Add(new PEV_Price
                                {
                                    sTripNo = item.TripNo,
                                    sItemNo = ItemNo.ToString(),
                                    sBLDate = BLDate,
                                    sMetNum = item.MetNum,
                                    sTxtMet = item.txtMet,
                                    sDueDate = DueDate,
                                    Currency = Currency,
                                    sVesselName = item.Vessel,
                                    sTxtVesselName = item.txtVeh,
                                    sPostingDate = PostingDate,
                                    sCalVolumeUnit = "",
                                    sCalInvFigure = "",
                                    CompanyCode = item.CompanyCode,
                                    SupplierNo = SupplierNo,
                                    RemarkImportDutyROE = SDutyROE,
                                    INS_DF = INS_RATE,
                                    FOBFRTTotalUSD = " ",
                                    FOBFRTTotalTHB = " ",
                                    FRTTotalTHB = " ",
                                    FRTTotalUSD = " ",
                                    FOBTotalUSD = " ",
                                    FOBROE = " ",
                                    FOBTotalTHB = " ",
                                    FOBPriceUSD = " ",
                                    FRTPriceUSD = " ",
                                    INSPriceUSD = " ",
                                    INSTotalUSD = " ",
                                    StampDutyTotalUSD = " ",
                                    LossGain = " ",
                                    MRCInsTotalUSD = " ",
                                    MRCInsTotalTHB = " ",
                                    TotalCIFPriceUSD = " ",
                                    TotalCIFTotalUSD = " ",
                                    TotalCIFTotalTHB = " ",
                                    RemarkFOBUSD = " ",
                                    RemarkFOBTHB = " ",
                                    RemarkFRTUSD = " ",
                                    RemarkFRTTHB = " ",
                                    RemarkINSUSD = " ",
                                    RemarkINSTHB = " ",
                                    RemarkTotalUSD = " ",
                                    RemarkTotalTHB = " ",
                                    InsStampDutyRate = " ",
                                    HTotalBBL = total_VOL_BBL.ToString(),
                                    HTotalML = total_VOL_ML.ToString(),
                                    HTotalMT = total_VOL_MT.ToString(),
                                    RemarkImportDutyTHB = total_IMPORT_DUTY.ToString("#,##0.0000"),
                                    RemarkImportDutyUSD = (total_IMPORT_DUTY / Double.Parse(String.IsNullOrEmpty(SDutyROE) ? "0" : SDutyROE)).ToString("#,##0.0000")
                                });
                                pModel.PriceList[index].QuantityList = new List<PEV_Quantity>();

                                pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                                {
                                    QuantityUnit = "ML",
                                    QtyBL = String.IsNullOrEmpty(total_BL_ML.ToString()) ? "0" : Double.Parse(total_BL_ML.ToString()).ToString("#,##0.0000"),
                                    QtyOutTurn = String.IsNullOrEmpty(total_OT_ML.ToString()) ? "0" : Double.Parse(total_OT_ML.ToString()).ToString("#,##0.0000")
                                });
                                pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                                {
                                    QuantityUnit = "MT",
                                    QtyBL = String.IsNullOrEmpty(total_BL_MT.ToString()) ? "0" : Double.Parse(total_BL_MT.ToString()).ToString("#,##0.0000"),
                                    QtyOutTurn = String.IsNullOrEmpty(total_OT_MT.ToString()) ? "0" : Double.Parse(total_OT_MT.ToString()).ToString("#,##0.0000")
                                });
                                pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                                {
                                    QuantityUnit = "BBL",
                                    QtyBL = String.IsNullOrEmpty(total_BL_BBL.ToString()) ? "0" : Double.Parse(total_BL_BBL.ToString()).ToString("#,##0.0000"),
                                    QtyOutTurn = String.IsNullOrEmpty(total_OT_BBL.ToString()) ? "0" : Double.Parse(total_OT_BBL.ToString()).ToString("#,##0.0000")
                                });

                                pModel.PriceList[index].FreightList = new List<PEV_Freight>();
                                if (query_FREIGHT != null)
                                {
                                    foreach (var item_frt in query_FREIGHT)
                                    {
                                        decimal result_price = Math.Round((Convert.ToDecimal(item_frt.fed.PFR_AMOUNT) * total_Met) / total_All, 2, MidpointRounding.AwayFromZero);
                                        pModel.PriceList[index].FreightList.Add(new PEV_Freight
                                        {
                                            FRTSupplier = item_frt.fed.PFR_SUPPLIER,
                                            FRTSubItem = item_frt.fed.PFR_FREIGHT_SUB_TYPE.ToString(),
                                            FRTSupplierTxt = item_frt.fedname + " (" + item_frt.vanname + ")",
                                            FRTItemNo = item_frt.fed.PFR_ITEM_NO.ToString(),
                                            FRTTotalUSD = result_price.ToString(),
                                            FRTROE = FrtROE,
                                            FRTTotalTHB = " "
                                        });
                                    }
                                }

                                SumNetTotalVolumeBBL = 0.0;
                                SumNetTotalPriceBBLUSD = 0.0;
                                SumNetTotalPriceBBLTHB = 0.0;
                                SumNetTotalVolumeML = 0.0;
                                SumNetTotalPriceMLUSD = 0.0;
                                SumNetTotalPriceMLTHB = 0.0;
                                SumNetTotalVolumeMT = 0.0;
                                SumNetTotalPriceMTUSD = 0.0;
                                SumNetTotalPriceMTTHB = 0.0;

                                pModel.PriceList[index].PriceSupplierList = new List<PEV_PriceSupplier>();
                                foreach (var item_supplier in query_MATERIAL)
                                {
                                    FOBName = FOBName + item_supplier.TxtSupplier + " ,";
                                    string Sup_BLDate = Convert.ToDateTime(item_supplier.BLDate.ToString()).ToString(format, provider);
                                    string Sup_ROE = GetRateExchange(DateTime.ParseExact(Sup_BLDate, format, provider), context);
                                    Sup_ROE = item_supplier.SupplierNo == dataList.PTT_VENDER.ToString() ? String.IsNullOrEmpty(item_supplier.FxAgreement.ToString()) ? Double.Parse(Sup_ROE).ToString("#,##0.0000") : Double.Parse((item_supplier.FxAgreement).ToString()).ToString("#,##0.0000") : Double.Parse(Sup_ROE).ToString("#,##0.0000");
                                    decimal VolBBL = String.IsNullOrEmpty(item_supplier.VolumeBBL.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeBBL);
                                    decimal VolML = String.IsNullOrEmpty(item_supplier.VolumeML.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeML);
                                    decimal VolMT = String.IsNullOrEmpty(item_supplier.VolumeMT.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeMT);
                                    decimal test = total_VOL_BBL != 0 ? total_VOL_BBL == Convert.ToDecimal(total_BL_BBL) ? VolBBL : (Convert.ToDecimal(total_BL_BBL) * VolBBL) / total_VOL_BBL : VolBBL;
                                    VolBBL = total_VOL_BBL != 0 ? total_VOL_BBL == Convert.ToDecimal(total_BL_BBL) ? VolBBL : (Convert.ToDecimal(total_BL_BBL) * VolBBL) / total_VOL_BBL : VolBBL;
                                    VolML = total_VOL_ML != 0 ? total_VOL_ML == Convert.ToDecimal(total_BL_ML) ? VolML : (Convert.ToDecimal(item.BLQtyML) * VolML) / total_VOL_ML : VolML;
                                    VolMT = total_VOL_MT != 0 ? total_VOL_MT == Convert.ToDecimal(total_BL_MT) ? VolMT : (Convert.ToDecimal(item.BLQtyMT) * VolMT) / total_VOL_MT : VolMT;
                                    string PriceUsd = Double.Parse(((item_supplier.VolumeBBL == null ? 0 : item_supplier.VolumeBBL) * (item_supplier.PriceUnitUSD == null ? 0 : item_supplier.PriceUnitUSD)).ToString()).ToString("#,##0.0000");
                                    string PriceThb = (Double.Parse(String.IsNullOrEmpty(PriceUsd) ? "0" : PriceUsd) * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE)).ToString("#,##0.0000");
                                    string UnitUsd = Double.Parse(String.IsNullOrEmpty(item_supplier.TotalPrice.ToString()) ? "0" : item_supplier.TotalPrice.ToString()).ToString("#,##0.0000");
                                    string Vol_BBL = Double.Parse(VolBBL.ToString()).ToString("#,##0.0000");
                                    string Vol_Ml = Double.Parse(VolML.ToString()).ToString("#,##0.0000");
                                    string Vol_Mt = Double.Parse(VolMT.ToString()).ToString("#,##0.0000");

                                    SumNetTotalVolumeBBL += Double.Parse(String.IsNullOrEmpty(Vol_BBL) ? "0" : Vol_BBL);
                                    SumNetTotalPriceBBLUSD += Double.Parse(String.IsNullOrEmpty(PriceUsd) ? "0" : PriceUsd);
                                    SumNetTotalPriceBBLTHB += Double.Parse(String.IsNullOrEmpty(PriceThb) ? "0" : PriceThb);
                                    SumNetTotalVolumeML += Double.Parse(String.IsNullOrEmpty(Vol_Ml) ? "0" : Vol_Ml);
                                    SumNetTotalPriceMLUSD += SumNetTotalVolumeML * Double.Parse(String.IsNullOrEmpty(UnitUsd) ? "0" : UnitUsd);
                                    SumNetTotalPriceMLTHB += SumNetTotalPriceMLUSD * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE);
                                    SumNetTotalVolumeMT += Double.Parse(String.IsNullOrEmpty(Vol_Mt) ? "0" : Vol_Mt);
                                    SumNetTotalPriceMTUSD += SumNetTotalVolumeMT * Double.Parse(String.IsNullOrEmpty(UnitUsd) ? "0" : UnitUsd);
                                    SumNetTotalPriceMTTHB += SumNetTotalPriceMTUSD * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE);

                                    pModel.PriceList[index].PriceSupplierList.Add(
                                        new PEV_PriceSupplier
                                        {
                                            txtSupplierName = item_supplier.TxtSupplier,
                                            SupplierName = item_supplier.Supplier,
                                            VolumeBBL = Vol_BBL,
                                            //OriginalVolumeBBL = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeBBL.ToString()) ? "0" : item_supplier.VolumeBBL.ToString()).ToString("#,##0.0000"),
                                            OriginalVolumeBBL = Vol_BBL,
                                            VolumeML = Vol_Ml,
                                            OriginalVolumeML = Vol_Ml,
                                            //OriginalVolumeML = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeML.ToString()) ? "0" : item_supplier.VolumeML.ToString()).ToString("#,##0.0000"),
                                            VolumeMT = Vol_Mt,
                                            OriginalVolumeMT = Vol_Mt,
                                            //OriginalVolumeMT = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeMT.ToString()) ? "0" : item_supplier.VolumeMT.ToString()).ToString("#,##0.0000"),
                                            PriceUnitUSD = UnitUsd,
                                            PriceTotalUSD = PriceUsd,
                                            ROE = Sup_ROE,
                                            PriceTotalTHB = PriceThb,
                                            NetTotalVolumeBBL = "",
                                            NetTotalPriceBBLUSD = "",
                                            NetTotalPriceBBLTHB = "",
                                            NetTotalVolumeML = "",
                                            NetTotalPriceMLUSD = "",
                                            NetTotalPriceMLTHB = "",
                                            NetTotalVolumeMT = "",
                                            NetTotalPriceMTUSD = "",
                                            NetTotalPriceMTTHB = "",
                                            MatItemNo = item_supplier.MatItemNo.ToString(),
                                            PriceStatus = item_supplier.PriceStatus,
                                            InvoiceNo = item_supplier.InvoiceNo,
                                            HVolumeBBL = Vol_BBL,
                                            HVolumeML = Vol_Ml,
                                            HVolumeMT = Vol_Mt
                                        });
                                }
                                FOBName = FOBName.Remove(FOBName.Length - 1);
                                pModel.PriceList[index].FOBSupplier = FOBName;
                                pModel.PriceList[index].NetTotalVolumeBBL = SumNetTotalVolumeBBL.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceBBLUSD = SumNetTotalPriceBBLUSD.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceBBLTHB = SumNetTotalPriceBBLTHB.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalVolumeML = SumNetTotalVolumeML.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMLUSD = SumNetTotalPriceMLUSD.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMLTHB = SumNetTotalPriceMLTHB.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalVolumeMT = SumNetTotalVolumeMT.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMTUSD = SumNetTotalPriceMTUSD.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMTTHB = SumNetTotalPriceMTTHB.ToString("#,##0.0000");
                                if (query_HEADER.Count() == 1)
                                {
                                    string json = new JavaScriptSerializer().Serialize(pModel.PriceList[0]);
                                    var temp_model1 = JSonConvertUtil.jsonToModel<PEV_Price>(json);
                                    var temp_model2 = JSonConvertUtil.jsonToModel<PEV_Price>(json);
                                    pModel.PriceList.Add(temp_model1);
                                    pModel.PriceList[1].sItemNo = "20";
                                    pModel.PriceList.Add(temp_model2);
                                    pModel.PriceList[2].sItemNo = "30";
                                    break;
                                }
                                ItemNo += 10;
                                index++;
                            }
                            FOBName = "";

                            PostingDate = Convert.ToDateTime(item.PostingDate.ToString()).ToString(format, provider);
                            pModel.PriceList.Add(new PEV_Price
                            {
                                sTripNo = item.TripNo,
                                sItemNo = ItemNo.ToString(),
                                sBLDate = BLDate,
                                sMetNum = item.MetNum,
                                sTxtMet = item.txtMet,
                                sDueDate = DueDate,
                                Currency = Currency,
                                sVesselName = item.Vessel,
                                sTxtVesselName = item.txtVeh,
                                sPostingDate = PostingDate,
                                sCalVolumeUnit = "",
                                sCalInvFigure = "",
                                CompanyCode = item.CompanyCode,
                                SupplierNo = SupplierNo,
                                RemarkImportDutyROE = SDutyROE,
                                INS_DF = INS_RATE,
                                FOBFRTTotalUSD = " ",
                                FOBFRTTotalTHB = " ",
                                FRTTotalTHB = " ",
                                FRTTotalUSD = " ",
                                FOBTotalUSD = " ",
                                FOBROE = " ",
                                FOBTotalTHB = " ",
                                FOBPriceUSD = " ",
                                FRTPriceUSD = " ",
                                INSPriceUSD = " ",
                                INSTotalUSD = " ",
                                StampDutyTotalUSD = " ",
                                LossGain = " ",
                                MRCInsTotalUSD = " ",
                                MRCInsTotalTHB = " ",
                                TotalCIFPriceUSD = " ",
                                TotalCIFTotalUSD = " ",
                                TotalCIFTotalTHB = " ",
                                RemarkFOBUSD = " ",
                                RemarkFOBTHB = " ",
                                RemarkFRTUSD = " ",
                                RemarkFRTTHB = " ",
                                RemarkINSUSD = " ",
                                RemarkINSTHB = " ",
                                RemarkTotalUSD = " ",
                                RemarkTotalTHB = " ",
                                InsStampDutyRate = " ",
                                HTotalBBL = item.BLQtyBBL.ToString(),
                                HTotalML = item.BLQtyML.ToString(),
                                HTotalMT = item.BLQtyMT.ToString(),
                                RemarkImportDutyTHB = total_IMPORT_DUTY.ToString("#,##0.0000"),
                                RemarkImportDutyUSD = (total_IMPORT_DUTY / Double.Parse(String.IsNullOrEmpty(SDutyROE) ? "0" : SDutyROE)).ToString("#,##0.0000")
                            });
                            pModel.PriceList[index].QuantityList = new List<PEV_Quantity>();

                            pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                            {
                                QuantityUnit = "ML",
                                QtyBL = String.IsNullOrEmpty(item.BLQtyML.ToString()) ? "0" : Double.Parse(item.BLQtyML.ToString()).ToString("#,##0.0000"),
                                QtyOutTurn = String.IsNullOrEmpty(item.QtyML.ToString()) ? "0" : Double.Parse(item.QtyML.ToString()).ToString("#,##0.0000")
                            });
                            pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                            {
                                QuantityUnit = "MT",
                                QtyBL = String.IsNullOrEmpty(item.BLQtyMT.ToString()) ? "0" : Double.Parse(item.BLQtyMT.ToString()).ToString("#,##0.0000"),
                                QtyOutTurn = String.IsNullOrEmpty(item.QtyMT.ToString()) ? "0" : Double.Parse(item.QtyMT.ToString()).ToString("#,##0.0000")
                            });
                            pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                            {
                                QuantityUnit = "BBL",
                                QtyBL = String.IsNullOrEmpty(item.BLQtyBBL.ToString()) ? "0" : Double.Parse(item.BLQtyBBL.ToString()).ToString("#,##0.0000"),
                                QtyOutTurn = String.IsNullOrEmpty(item.QtyBBL.ToString()) ? "0" : Double.Parse(item.QtyBBL.ToString()).ToString("#,##0.0000")
                            });

                            SumNetTotalVolumeBBL = 0.0;
                            SumNetTotalPriceBBLUSD = 0.0;
                            SumNetTotalPriceBBLTHB = 0.0;
                            SumNetTotalVolumeML = 0.0;
                            SumNetTotalPriceMLUSD = 0.0;
                            SumNetTotalPriceMLTHB = 0.0;
                            SumNetTotalVolumeMT = 0.0;
                            SumNetTotalPriceMTUSD = 0.0;
                            SumNetTotalPriceMTTHB = 0.0;
                            double totalMT = 0.0;

                            pModel.PriceList[index].PriceSupplierList = new List<PEV_PriceSupplier>();
                            foreach (var item_supplier in query_MATERIAL)
                            {
                                FOBName = FOBName + item_supplier.TxtSupplier + " ,";
                                string Sup_BLDate = Convert.ToDateTime(item_supplier.BLDate.ToString()).ToString(format, provider);
                                string Sup_ROE = GetRateExchange(DateTime.ParseExact(Sup_BLDate, format, provider), context);
                                Sup_ROE = item_supplier.SupplierNo == dataList.PTT_VENDER.ToString() ? String.IsNullOrEmpty(item_supplier.FxAgreement.ToString()) ? Double.Parse(Sup_ROE).ToString("#,##0.0000") : Double.Parse((item_supplier.FxAgreement).ToString()).ToString("#,##0.0000") : Double.Parse(Sup_ROE).ToString("#,##0.0000");
                                decimal VolBBL = String.IsNullOrEmpty(item_supplier.VolumeBBL.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeBBL);
                                decimal VolML = String.IsNullOrEmpty(item_supplier.VolumeML.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeML);
                                decimal VolMT = String.IsNullOrEmpty(item_supplier.VolumeMT.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeMT);
                                VolBBL = total_VOL_BBL != 0 ? total_VOL_BBL == Convert.ToDecimal(item.BLQtyBBL) ? VolBBL : (Convert.ToDecimal(item.BLQtyBBL) * VolBBL) / total_VOL_BBL : VolBBL;
                                VolML = total_VOL_ML != 0 ? total_VOL_ML == Convert.ToDecimal(item.BLQtyML) ? VolML : (Convert.ToDecimal(item.BLQtyML) * VolML) / total_VOL_ML : VolML;
                                VolMT = total_VOL_MT != 0 ? total_VOL_MT == Convert.ToDecimal(item.BLQtyMT) ? VolMT : (Convert.ToDecimal(item.BLQtyMT) * VolMT) / total_VOL_MT : VolMT;
                                totalMT += Double.Parse(VolMT.ToString());
                                string PriceUsd = Double.Parse(((item_supplier.VolumeBBL == null ? 0 : item_supplier.VolumeBBL) * (item_supplier.PriceUnitUSD == null ? 0 : item_supplier.PriceUnitUSD)).ToString()).ToString("#,##0.0000");
                                string PriceThb = (Double.Parse(String.IsNullOrEmpty(PriceUsd) ? "0" : PriceUsd) * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE)).ToString("#,##0.0000");
                                string UnitUsd = Double.Parse(String.IsNullOrEmpty(item_supplier.TotalPrice.ToString()) ? "0" : item_supplier.TotalPrice.ToString()).ToString("#,##0.0000");
                                string Vol_BBL = Double.Parse(VolBBL.ToString()).ToString("#,##0.0000");
                                string Vol_Ml = Double.Parse(VolML.ToString()).ToString("#,##0.0000");
                                string Vol_Mt = Double.Parse(VolMT.ToString()).ToString("#,##0.0000");

                                SumNetTotalVolumeBBL += Double.Parse(String.IsNullOrEmpty(Vol_BBL) ? "0" : Vol_BBL);
                                SumNetTotalPriceBBLUSD += Double.Parse(String.IsNullOrEmpty(PriceUsd) ? "0" : PriceUsd);
                                SumNetTotalPriceBBLTHB += Double.Parse(String.IsNullOrEmpty(PriceThb) ? "0" : PriceThb);
                                SumNetTotalVolumeML += Double.Parse(String.IsNullOrEmpty(Vol_Ml) ? "0" : Vol_Ml);
                                SumNetTotalPriceMLUSD += SumNetTotalVolumeML * Double.Parse(String.IsNullOrEmpty(UnitUsd) ? "0" : UnitUsd);
                                SumNetTotalPriceMLTHB += SumNetTotalPriceMLUSD * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE);
                                SumNetTotalVolumeMT += Double.Parse(String.IsNullOrEmpty(Vol_Mt) ? "0" : Vol_Mt);
                                SumNetTotalPriceMTUSD += SumNetTotalVolumeMT * Double.Parse(String.IsNullOrEmpty(UnitUsd) ? "0" : UnitUsd);
                                SumNetTotalPriceMTTHB += SumNetTotalPriceMTUSD * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE);

                                pModel.PriceList[index].PriceSupplierList.Add(
                                    new PEV_PriceSupplier
                                    {
                                        txtSupplierName = item_supplier.TxtSupplier,
                                        SupplierName = item_supplier.Supplier,
                                        VolumeBBL = Vol_BBL,
                                        OriginalVolumeBBL = Vol_BBL,
                                        //OriginalVolumeBBL = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeBBL.ToString()) ? "0" : item_supplier.VolumeBBL.ToString()).ToString("#,##0.0000"),
                                        VolumeML = Vol_Ml,
                                        OriginalVolumeML = Vol_Ml,
                                        //OriginalVolumeML = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeML.ToString()) ? "0" : item_supplier.VolumeML.ToString()).ToString("#,##0.0000"),
                                        VolumeMT = Vol_Mt,
                                        OriginalVolumeMT = Vol_Mt,
                                        //OriginalVolumeMT = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeMT.ToString()) ? "0" : item_supplier.VolumeMT.ToString()).ToString("#,##0.0000"),
                                        PriceUnitUSD = UnitUsd,
                                        PriceTotalUSD = PriceUsd,
                                        ROE = Sup_ROE,
                                        PriceTotalTHB = PriceThb,
                                        NetTotalVolumeBBL = "",
                                        NetTotalPriceBBLUSD = "",
                                        NetTotalPriceBBLTHB = "",
                                        NetTotalVolumeML = "",
                                        NetTotalPriceMLUSD = "",
                                        NetTotalPriceMLTHB = "",
                                        NetTotalVolumeMT = "",
                                        NetTotalPriceMTUSD = "",
                                        NetTotalPriceMTTHB = "",
                                        MatItemNo = item_supplier.MatItemNo.ToString(),
                                        PriceStatus = item_supplier.PriceStatus,
                                        InvoiceNo = item_supplier.InvoiceNo
                                    });
                            }
                            FOBName = FOBName.Remove(FOBName.Length - 1);
                            pModel.PriceList[index].FOBSupplier = FOBName;
                            pModel.PriceList[index].NetTotalVolumeBBL = SumNetTotalVolumeBBL.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalPriceBBLUSD = SumNetTotalPriceBBLUSD.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalPriceBBLTHB = SumNetTotalPriceBBLTHB.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalVolumeML = SumNetTotalVolumeML.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalPriceMLUSD = SumNetTotalPriceMLUSD.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalPriceMLTHB = SumNetTotalPriceMLTHB.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalVolumeMT = SumNetTotalVolumeMT.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalPriceMTUSD = SumNetTotalPriceMTUSD.ToString("#,##0.0000");
                            pModel.PriceList[index].NetTotalPriceMTTHB = SumNetTotalPriceMTTHB.ToString("#,##0.0000");


                            double total_MET = total_Not_All + Math.Round(totalMT, 3, MidpointRounding.AwayFromZero);
                            pModel.PriceList[index].FreightList = new List<PEV_Freight>();
                            if (query_FREIGHT != null)
                            {
                                foreach (var item_frt in query_FREIGHT)
                                {
                                    decimal result_price = Math.Round(Convert.ToDecimal((Double.Parse(item_frt.fed.PFR_AMOUNT.ToString()) * Double.Parse(totalMT.ToString())) / total_MET), 2, MidpointRounding.AwayFromZero);
                                    pModel.PriceList[index].FreightList.Add(new PEV_Freight
                                    {
                                        FRTSupplier = item_frt.fed.PFR_SUPPLIER,
                                        FRTSubItem = item_frt.fed.PFR_FREIGHT_SUB_TYPE.ToString(),
                                        FRTSupplierTxt = item_frt.fedname + " (" + item_frt.vanname + ")",
                                        FRTItemNo = item_frt.fed.PFR_ITEM_NO.ToString(),
                                        FRTTotalUSD = result_price.ToString(),
                                        FRTROE = FrtROE,
                                        FRTTotalTHB = " "
                                    });
                                }
                            }
                            ItemNo += 10;
                            index++;
                        }
                        pModel.bPartly = false;
                        pModel.bPartlyComplete = false;
                        pModel.sCalInvFigure = "";
                        pModel.sCalVolumeUnit = "";
                        pModel.SAPDocumentDate = DateTime.Now.ToString("dd/MM/yyyy");
                        pModel.SAPPostingDate = DateTime.Now.ToString("dd/MM/yyyy");
                        pModel.SAPReverseDateForTypeA = "01/" + DateTime.Now.AddMonths(1).ToString("MM") + "/" + DateTime.Now.AddMonths(1).ToString("yyyy");

                        rtn = null;
                    }
                    else
                    {
                        //serviceModel.setInitialModel(ref pModel);
                        rtn.Message = "Data not found.";
                        rtn.Status = false;
                    }
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                    var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                    log.Error("Error PurchaseEntryVessel LineNumber : " + LineNumber + " => " + ex.Message);
                }
                rtn = null;
            }
            #endregion
            return rtn;
        }

        public ReturnValue SaveData(ref PurchaseEntryVesselViewModel pModel, string pUser,  string FiDoc = "")
        {
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            PurchaseEntryVesselViewModel dModel = new PurchaseEntryVesselViewModel();
            PURCHASE_ENTRY_VESSEL_DAL dalPPE = new PURCHASE_ENTRY_VESSEL_DAL();
            ReturnValue rtn = new ReturnValue();

            if (pModel == null)
            {
                rtn.Message = "Model is null.";
                rtn.Status = false;
                return rtn;
            }
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            foreach (var item in pModel.PriceList)
                            {
                                PCF_PURCHASE_ENTRY entPPE = new PCF_PURCHASE_ENTRY();

                                string TripNo = item.sTripNo;
                                string MetNum = item.sMetNum;

                                decimal ItemNo = Decimal.Parse(item.sItemNo);

                                entPPE.PPE_TRIP_NO = item.sTripNo;
                                entPPE.PPE_BL_DATE = (String.IsNullOrEmpty(item.sBLDate)) ? (DateTime?)null : DateTime.ParseExact(item.sBLDate, format, provider);
                                entPPE.PPE_GR_DATE = (String.IsNullOrEmpty(item.sPostingDate)) ? (DateTime?)null : entPPE.PPE_GR_DATE = DateTime.ParseExact(item.sPostingDate, format, provider);
                                entPPE.PPE_MET_NUM = item.sMetNum;

                                if (item.QuantityList != null)
                                {
                                    entPPE.PPE_BL_VOLUME_ML = (String.IsNullOrEmpty(item.QuantityList[0].QtyBL)) ? (decimal?)null : Decimal.Parse(item.QuantityList[0].QtyBL);
                                    entPPE.PPE_BL_VOLUME_MT = (String.IsNullOrEmpty(item.QuantityList[1].QtyBL)) ? (decimal?)null : Decimal.Parse(item.QuantityList[1].QtyBL);
                                    entPPE.PPE_BL_VOLUME_BBL = (String.IsNullOrEmpty(item.QuantityList[2].QtyBL)) ? (decimal?)null : Decimal.Parse(item.QuantityList[2].QtyBL);
                                    entPPE.PPE_GR_VOLUME_ML = (String.IsNullOrEmpty(item.QuantityList[0].QtyOutTurn)) ? (decimal?)null : Decimal.Parse(item.QuantityList[0].QtyOutTurn);
                                    entPPE.PPE_GR_VOLUME_MT = (String.IsNullOrEmpty(item.QuantityList[1].QtyOutTurn)) ? (decimal?)null : Decimal.Parse(item.QuantityList[1].QtyOutTurn);
                                    entPPE.PPE_GR_VOLUME_BBL = (String.IsNullOrEmpty(item.QuantityList[2].QtyOutTurn)) ? (decimal?)null : Decimal.Parse(item.QuantityList[2].QtyOutTurn);
                                }
                                else
                                {
                                    entPPE.PPE_BL_VOLUME_ML = null;
                                    entPPE.PPE_BL_VOLUME_MT = null;
                                    entPPE.PPE_BL_VOLUME_BBL = null;
                                    entPPE.PPE_GR_VOLUME_ML = null;
                                    entPPE.PPE_GR_VOLUME_MT = null;
                                    entPPE.PPE_GR_VOLUME_BBL = null;
                                }
                                entPPE.PPE_CAL_INVOICE_FIGURE = item.sCalInvFigure;
                                entPPE.PPE_CAL_VOLUME_UNIT = item.sCalVolumeUnit;

                                entPPE.PPE_FOB_PRICE_USD = (String.IsNullOrEmpty(item.FOBPriceUSD)) ? null : entPPE.PPE_FOB_PRICE_USD = Decimal.Parse(item.FOBPriceUSD);

                                entPPE.PPE_ITEM_NO = Decimal.Parse(item.sItemNo);
                                entPPE.PPE_PARTLY = pModel.bPartly ? "Y" : "";
                                entPPE.PPE_PARTLY_COMPLETE = pModel.bPartlyComplete ? "Y" : "";
                                entPPE.PPE_FOB_PRICE_USD = (String.IsNullOrEmpty(item.FOBPriceUSD)) ? (decimal?)null : Decimal.Parse(item.FOBPriceUSD);
                                entPPE.PPE_FOB_SUPPLIER = item.FOBSupplier;
                                entPPE.PPE_FOB_TOTAL_USD = (String.IsNullOrEmpty(item.FOBTotalUSD)) ? (decimal?)null : Decimal.Parse(item.FOBTotalUSD);
                                entPPE.PPE_FOB_ROE = (String.IsNullOrEmpty(item.FOBROE)) ? (decimal?)null : Decimal.Parse(item.FOBROE);
                                entPPE.PPE_FOB_TOTAL_THB = (String.IsNullOrEmpty(item.FOBTotalTHB)) ? (decimal?)null : Decimal.Parse(item.FOBTotalTHB);
                                entPPE.PPE_FRT_PRICE_USD = (String.IsNullOrEmpty(item.FRTPriceUSD)) ? (decimal?)null : item.FRTPriceUSD == "NaN.00000" ? (decimal?)null : Decimal.Parse(item.FRTPriceUSD);
                                entPPE.PPE_FRT_TOTAL_USD = (String.IsNullOrEmpty(item.FRTTotalUSD)) ? (decimal?)null : Decimal.Parse(item.FRTTotalUSD);
                                entPPE.PPE_FRT_TOTAL_THB = (String.IsNullOrEmpty(item.FRTTotalTHB)) ? (decimal?)null : Decimal.Parse(item.FRTTotalTHB);
                                entPPE.PPE_FOB_FRT_TOTAL_USD = (String.IsNullOrEmpty(item.FOBFRTTotalUSD)) ? (decimal?)null : Decimal.Parse(item.FOBFRTTotalUSD);
                                entPPE.PPE_FOB_FRT_TOTAL_THB = (String.IsNullOrEmpty(item.FOBFRTTotalTHB)) ? (decimal?)null : Decimal.Parse(item.FOBFRTTotalTHB);
                                entPPE.PPE_INS_PRICE_USD = (String.IsNullOrEmpty(item.INSPriceUSD)) ? (decimal?)null : Decimal.Parse(item.INSPriceUSD);
                                entPPE.PPE_INS_TOTAL_USD = (String.IsNullOrEmpty(item.INSTotalUSD)) ? (decimal?)null : Decimal.Parse(item.INSTotalUSD);
                                entPPE.PPE_INS_RATE = (String.IsNullOrEmpty(item.InsuranceRate)) ? (decimal?)null : Decimal.Parse(item.InsuranceRate);
                                entPPE.PPE_STAMP_DUTY_TOTAL_USD = (String.IsNullOrEmpty(item.StampDutyTotalUSD)) ? (decimal?)null : Decimal.Parse(item.StampDutyTotalUSD);
                                entPPE.PPE_INS_STAMP_DUTY_RATE = (String.IsNullOrEmpty(item.InsStampDutyRate)) ? (decimal?)null : Decimal.Parse(item.InsStampDutyRate);
                                entPPE.PPE_LOSS_GAIN = (String.IsNullOrEmpty(item.LossGain)) ? (decimal?)null : Decimal.Parse(item.LossGain);
                                entPPE.PPE_MRC_INS_TOTAL_USD = (String.IsNullOrEmpty(item.MRCInsTotalUSD)) ? (decimal?)null : Decimal.Parse(item.MRCInsTotalUSD);
                                entPPE.PPE_MRC_INS_ROE = (String.IsNullOrEmpty(item.MRCInsROE)) ? (decimal?)null : Decimal.Parse(item.MRCInsROE);
                                entPPE.PPE_MRC_INS_TOTAL_THB = (String.IsNullOrEmpty(item.MRCInsTotalTHB)) ? (decimal?)null : Decimal.Parse(item.MRCInsTotalTHB);
                                entPPE.PPE_TOTAL_CIF_PRICE_USD = (String.IsNullOrEmpty(item.TotalCIFPriceUSD)) ? (decimal?)null : Decimal.Parse(item.TotalCIFPriceUSD);
                                entPPE.PPE_TOTAL_CIF_TOTAL_USD = (String.IsNullOrEmpty(item.TotalCIFTotalUSD)) ? (decimal?)null : Decimal.Parse(item.TotalCIFTotalUSD);
                                entPPE.PPE_TOTAL_CIF_TOTAL_THB = (String.IsNullOrEmpty(item.TotalCIFTotalTHB)) ? (decimal?)null : Decimal.Parse(item.TotalCIFTotalTHB);
                                entPPE.PPE_REMARK_FOB_USD = (String.IsNullOrEmpty(item.RemarkFOBUSD)) ? (decimal?)null : Decimal.Parse(item.RemarkFOBUSD);
                                entPPE.PPE_REMARK_FOB_THB = (String.IsNullOrEmpty(item.RemarkFOBTHB)) ? (decimal?)null : Decimal.Parse(item.RemarkFOBTHB);
                                entPPE.PPE_REMARK_FRT_USD = (String.IsNullOrEmpty(item.RemarkFRTUSD)) ? (decimal?)null : Decimal.Parse(item.RemarkFRTUSD);
                                entPPE.PPE_REMARK_FRT_THB = (String.IsNullOrEmpty(item.RemarkFRTTHB)) ? (decimal?)null : Decimal.Parse(item.RemarkFRTTHB);
                                entPPE.PPE_REMARK_INS_USD = (String.IsNullOrEmpty(item.RemarkINSUSD)) ? (decimal?)null : Decimal.Parse(item.RemarkINSUSD);
                                entPPE.PPE_REMARK_INS_THB = (String.IsNullOrEmpty(item.RemarkINSTHB)) ? (decimal?)null : Decimal.Parse(item.RemarkINSTHB);
                                entPPE.PPE_REMARK_IMPORT_DUTY_USD = (String.IsNullOrEmpty(item.RemarkImportDutyUSD)) ? (decimal?)null : Decimal.Parse(item.RemarkImportDutyUSD);
                                entPPE.PPE_REMARK_IMPORT_DUTY_ROE = (String.IsNullOrEmpty(item.RemarkImportDutyROE)) ? (decimal?)null : Decimal.Parse(item.RemarkImportDutyROE);
                                entPPE.PPE_REMARK_IMPORT_DUTY_THB = (String.IsNullOrEmpty(item.RemarkImportDutyTHB)) ? (decimal?)null : Decimal.Parse(item.RemarkImportDutyTHB);
                                entPPE.PPE_REMARK_TOTAL_USD = (String.IsNullOrEmpty(item.RemarkTotalUSD)) ? (decimal?)null : Decimal.Parse(item.RemarkTotalUSD);
                                entPPE.PPE_REMARK_TOTAL_THB = (String.IsNullOrEmpty(item.RemarkTotalTHB)) ? (decimal?)null : Decimal.Parse(item.RemarkTotalTHB);

                                entPPE.PPE_CREATED_BY = Const.User.Name;
                                entPPE.PPE_CREATED_DATE = DateTime.Now;
                                var queryFI = context.PCF_PURCHASE_ENTRY.Where(a => a.PPE_TRIP_NO == TripNo && a.PPE_MET_NUM == MetNum && a.PPE_ITEM_NO == ItemNo).SingleOrDefault();
                                if (queryFI != null)
                                {
                                    entPPE.PPE_DOCUMENT_DATE = String.IsNullOrEmpty(item.FiDoc) ? queryFI.PPE_DOCUMENT_DATE : DateTime.ParseExact(pModel.SAPDocumentDate, format, provider);
                                    entPPE.PPE_POSTING_DATE = String.IsNullOrEmpty(item.FiDoc) ? queryFI.PPE_POSTING_DATE : DateTime.ParseExact(pModel.SAPPostingDate, format, provider);
                                    entPPE.PPE_ACCRUAL_TYPE = String.IsNullOrEmpty(item.FiDoc) ? queryFI.PPE_ACCRUAL_TYPE : pModel.SAPAccuralType;
                                    entPPE.PPE_SAP_FI_DOC_STATUS = String.IsNullOrEmpty(item.FiDoc) ? queryFI.PPE_SAP_FI_DOC_STATUS : "Success";
                                }
                                else
                                {
                                    entPPE.PPE_DOCUMENT_DATE = String.IsNullOrEmpty(item.FiDoc) ? (DateTime?)null : DateTime.ParseExact(pModel.SAPDocumentDate, format, provider);
                                    entPPE.PPE_POSTING_DATE = String.IsNullOrEmpty(item.FiDoc) ? (DateTime?)null : DateTime.ParseExact(pModel.SAPPostingDate, format, provider);
                                    entPPE.PPE_ACCRUAL_TYPE = String.IsNullOrEmpty(item.FiDoc) ? "" : pModel.SAPAccuralType;
                                    //entPPE.PPE_ACCRUAL_TYPE = "";
                                    entPPE.PPE_SAP_FI_DOC_STATUS = String.IsNullOrEmpty(item.FiDoc) ? "" : "Success";
                                }
                                entPPE.PPE_REVERSE_DATE_FOR_TYPE_A = String.IsNullOrEmpty(pModel.SAPReverseDateForTypeA) ? (DateTime?)null : DateTime.ParseExact(pModel.SAPReverseDateForTypeA, format, provider);
                                
                                if (!String.IsNullOrEmpty(item.FiDoc))
                                {
                                    var queryFi = context.PCF_PURCHASE_ENTRY.Where(a => a.PPE_TRIP_NO == TripNo && a.PPE_MET_NUM == MetNum && a.PPE_ITEM_NO == ItemNo).SingleOrDefault();
                                    string sFiDoc = queryFi.PPE_SAP_FI_DOC_NO;
                                    if (queryFi != null && !String.IsNullOrEmpty(sFiDoc))
                                    {
                                        PCF_REVERSED_FI_DOC PRF = new PCF_REVERSED_FI_DOC();
                                        PRF.TRIP_NO = TripNo;
                                        PRF.ITEM_NO = ItemNo;
                                        PRF.MET_NUM = MetNum;
                                        PRF.SAP_FI_DOC_NO = queryFi.PPE_SAP_FI_DOC_NO;
                                        PRF.REVERSAL_REASON = "02";
                                        PRF.CREATED_BY = pUser;
                                        PRF.CREATED_DATE = DateTime.Now;
                                        dalPPE.InsertReversedFiDoc(PRF, context);
                                    }
                                    item.FiDoc = (FiDoc.Length > 10) ? FiDoc.Substring(0, 10) : FiDoc;
                                }
                                var query_if = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO == item.sTripNo && z.PPE_MET_NUM == item.sMetNum && z.PPE_ITEM_NO == ItemNo).Select(z => z.PPE_SAP_FI_DOC_NO).FirstOrDefault();
                                entPPE.PPE_SAP_FI_DOC_NO = String.IsNullOrEmpty(item.FiDoc) ? String.IsNullOrEmpty(query_if) ? "" : query_if : item.FiDoc;
                                
                                entPPE.PPE_SAP_ERROR_MESSAGE = "";

                                var queryif_pcf = context.PCF_PURCHASE_ENTRY.Find(TripNo, MetNum, ItemNo);
                                if (queryif_pcf == null)
                                {
                                    dalPPE.InsertPPE(entPPE);
                                }
                                else
                                {
                                    dalPPE.UpdatePPE(entPPE);
                                }
                                if (item.FreightList != null)
                                {
                                    foreach (var item_frt in item.FreightList)
                                    {
                                        PCF_PURCHASE_ENTRY_FREIGHT entPPF = new PCF_PURCHASE_ENTRY_FREIGHT();
                                        entPPF.TRIP_NO = item.sTripNo;
                                        entPPF.MET_NUM = item.sMetNum;
                                        entPPF.ITEM_NO = Decimal.Parse(item.sItemNo);
                                        entPPF.FREIGHT_ITEM_NO = Decimal.Parse(item_frt.FRTItemNo);
                                        entPPF.FRT_ROE = Decimal.Parse(item_frt.FRTROE);
                                        entPPF.FRT_TOTAL_USD = Decimal.Parse(item_frt.FRTTotalUSD);
                                        entPPF.FRT_TOTAL_THB = Decimal.Parse(item_frt.FRTTotalTHB);

                                        Decimal FrtItemNo = Decimal.Parse(item_frt.FRTItemNo);
                                        var queryif = context.PCF_PURCHASE_ENTRY_FREIGHT.Where(z => z.TRIP_NO == TripNo && z.MET_NUM == MetNum && z.ITEM_NO == ItemNo && z.FREIGHT_ITEM_NO == FrtItemNo).ToList();
                                        if (queryif.Count() != 0)
                                        {
                                            dalPPE.UpdatePPF(entPPF);
                                        }
                                        else
                                        {
                                            dalPPE.InsertPPF(entPPF);
                                        }
                                    }
                                }

                                if (item.PriceSupplierList != null)
                                {
                                    foreach (var item_sup in item.PriceSupplierList)
                                    {
                                        PCF_PURCHASE_ENTRY_ITEM entPPI = new PCF_PURCHASE_ENTRY_ITEM();
                                        entPPI.PPI_TRIP_NO = item.sTripNo;
                                        entPPI.PPI_MET_NUM = item.sMetNum;
                                        entPPI.PPI_ENTRY_ITEM_NO = entPPE.PPE_ITEM_NO;
                                        entPPI.PPI_MAT_ITEM_NO = Decimal.Parse(item_sup.MatItemNo);
                                        if (String.IsNullOrEmpty(item_sup.VolumeBBL)) { entPPI.PPI_VOLUME_BBL = null; } else { entPPI.PPI_VOLUME_BBL = Decimal.Parse(item_sup.VolumeBBL); }
                                        if (String.IsNullOrEmpty(item_sup.PriceUnitUSD)) { entPPI.PPI_UNIT_PRICE = null; } else { entPPI.PPI_UNIT_PRICE = Decimal.Parse(item_sup.PriceUnitUSD); }
                                        if (String.IsNullOrEmpty(item_sup.PriceTotalUSD)) { entPPI.PPI_TOTAL_USD = null; } else { entPPI.PPI_TOTAL_USD = Decimal.Parse(item_sup.PriceTotalUSD); }
                                        if (String.IsNullOrEmpty(item_sup.ROE)) { entPPI.PPI_ROE = null; } else { entPPI.PPI_ROE = Decimal.Parse(item_sup.ROE); }
                                        if (String.IsNullOrEmpty(item_sup.PriceTotalTHB)) { entPPI.PPI_TOTAL_THB = null; } else { entPPI.PPI_TOTAL_THB = Decimal.Parse(item_sup.PriceTotalTHB); }

                                        var queryif = context.PCF_PURCHASE_ENTRY.Where(a => a.PPE_TRIP_NO == TripNo && a.PPE_MET_NUM == MetNum && a.PPE_ITEM_NO == ItemNo).ToList();
                                        if (queryif.Count() != 0)
                                        {
                                            dalPPE.UpdatePPI(entPPI);
                                        }
                                        else
                                        {
                                            dalPPE.InsertPPI(entPPI);
                                        }
                                    }
                                }
                                if (!pModel.bPartly)
                                {
                                    break;
                                }
                            }
                            log.Info("Save Success");
                            //dbContextTransaction.Rollback();
                            dbContextTransaction.Commit();
                            if (!String.IsNullOrEmpty(FiDoc))
                            {
                                //context.Database.ExecuteSqlCommand("CALL p_pcf_insert_pricing_vessel_ac ('" + pModel.PriceList[0].sTripNo + "','" + pModel.PriceList[0].sMetNum + "')");
                                context.Database.ExecuteSqlCommand("CALL P_PCF_VESSEL_INSERT_PRICE ('" + pModel.PriceList[0].sTripNo + "','" + pModel.PriceList[0].sMetNum + "','ACCOUNTING')");
                            }
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch(Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                            var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                            log.Error("Error PurchaseEntryVessel LineNumber : " + LineNumber + " => " + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                log.Error("Error PurchaseEntryVessel LineNumber : " + LineNumber + " => " + ex.Message);
            }

            return rtn;
        }

        public static List<SelectListItem> getCIPConfigInsuranceRate(PurchaseEntryVesselViewModel pModel)
        {
            string JsonD = MasterData.GetJsonMasterSetting("CIP_IMPORT_PLAN");
            JObject json = JObject.Parse(JsonD);
            GlobalConfig dataList = (GlobalConfig)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfig));
            List<SelectListItem> dList = new List<SelectListItem>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string sTripNo = pModel.PriceList[0].sTripNo;
                string sMetNo = pModel.PriceList[0].sMetNum;
                var queryIns = context.PCF_CUSTOMS_VAT.Where(a => a.TRIP_NO == sTripNo).Select(b => b.INSURANCE_RATE).Distinct().ToList();

                foreach (var item in dataList.PCF_MT_INSURANCE_RATE)
                {
                    //dList.Add(new SelectListItem { Value = item.Code.ToString(), Text = item.Name.ToString() });
                    dList.Add(new SelectListItem { Value = item.Code.ToString(), Text = item.Code.ToString() });
                }
            }
            return dList;
        }

        public static List<SelectListItem> getItemNo(string pTripNo, string pMetNum)
        {
            List<SelectListItem> lstItemNo = new List<SelectListItem>();
            using (var context = new EntityCPAIEngine())
            {
                var qryPEV = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO.ToUpper().Equals(pTripNo.ToUpper()) && z.PPE_MET_NUM.ToUpper().Equals(pMetNum.ToUpper()));

                if (qryPEV != null && qryPEV.ToList().Count() > 0)
                {
                    foreach (var item in qryPEV)
                    {
                        lstItemNo.Add(new SelectListItem { Value = item.PPE_ITEM_NO.ToString(), Text = item.PPE_ITEM_NO.ToString() });
                    }
                }
                else
                {
                    lstItemNo.Add(new SelectListItem { Value = "", Text = "" });
                }
            }

            return lstItemNo;
        }

        //public ReturnValue getDataForEdit(ref PurchaseEntryVesselViewModel pModel)
        //{
        //    CultureInfo provider = new CultureInfo("en-US");
        //    string format = "dd/MM/yyyy";
        //    ReturnValue rtn = new ReturnValue();

        //    try
        //    {
        //        using (var context = new EntityCPAIEngine())
        //        {
        //            string sTripNo = pModel.sTripNo;
        //            string sMetNum = pModel.sMetNum;
        //            Int32 nItemNo = String.IsNullOrEmpty(pModel.sItemNo) ? 0 : Int32.Parse(pModel.sItemNo);

        //            var qryPEV = (from ppe in context.PCF_PURCHASE_ENTRY
        //                          join ppi in context.PCF_PURCHASE_ENTRY_ITEM on ppe.PPE_TRIP_NO equals ppi.PPI_TRIP_NO
        //                          join phe in context.PCF_HEADER on ppe.PPE_TRIP_NO equals phe.PHE_TRIP_NO
        //                          join pma in context.PCF_MATERIAL on phe.PHE_TRIP_NO equals pma.PMA_TRIP_NO
        //                          join pmp in context.PCF_MATERIAL_PRICE on pma.PMA_TRIP_NO equals pmp.PMP_TRIP_NO
        //                          join sup in context.MT_VENDOR on pma.PMA_SUPPLIER equals sup.VND_ACC_NUM_VENDOR

        //                          where (ppe.PPE_MET_NUM == ppi.PPI_MET_NUM)
        //                          && (ppe.PPE_ITEM_NO == ppi.PPI_ENTRY_ITEM_NO)
        //                          && (ppi.PPI_MET_NUM == pma.PMA_MET_NUM)
        //                          && (ppe.PPE_ITEM_NO == pma.PMA_ITEM_NO)
        //                          && (pma.PMA_ITEM_NO == pmp.PMP_MAT_ITEM_NO)
        //                          && (ppe.PPE_TRIP_NO.Equals(sTripNo))
        //                          && (ppi.PPI_MET_NUM.Equals(sMetNum))
        //                          && (ppi.PPI_ENTRY_ITEM_NO == nItemNo)
        //                          select new
        //                          {
        //                              ppe,
        //                              ppi,
        //                              VesselName = phe.PHE_VESSEL,
        //                              DueDate = pmp.PMP_DUE_DATE,
        //                              Supplier = sup.VND_NAME1,
        //                              VolumeML = pma.PMA_VOLUME_ML,
        //                              VolumeMT = pma.PMA_VOLUME_MT,
        //                              LastedUpdate = pmp.PMP_UPDATED_DATE,
        //                              PriceStatus = pmp.PMP_PRICE_STATUS,
        //                              InvoiceNo = ppe.PPE_SAP_FI_DOC_NO
        //                          }).AsEnumerable();

        //            if (qryPEV.ToList().Count > 0)
        //            {
        //                pModel.sTripNo = qryPEV.ToList()[0].ppe.PPE_TRIP_NO;
        //                pModel.sBLDate = String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_BL_DATE.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].ppe.PPE_BL_DATE.ToString()).ToString(format, provider);
        //                pModel.sLastedUpdate = String.IsNullOrEmpty(qryPEV.ToList()[0].LastedUpdate.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].LastedUpdate.ToString()).ToString("dd/MM/yyyy HH:mm:ss", provider);
        //                pModel.sMetNum = qryPEV.ToList()[0].ppe.PPE_MET_NUM;
        //                pModel.sDueDate = String.IsNullOrEmpty(qryPEV.ToList()[0].DueDate.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].DueDate.ToString()).ToString(format, provider);
        //                pModel.sVesselName = qryPEV.ToList()[0].VesselName;
        //                pModel.sPostingDate = String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_GR_DATE.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].ppe.PPE_GR_DATE.ToString()).ToString(format, provider);
        //                pModel.sCalInvFigure = qryPEV.ToList()[0].ppe.PPE_CAL_INVOICE_FIGURE;
        //                pModel.sCalVolumeUnit = qryPEV.ToList()[0].ppe.PPE_CAL_VOLUME_UNIT;
        //                if (String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_PARTLY)) { qryPEV.ToList()[0].ppe.PPE_PARTLY = String.Empty; }
        //                if (String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_PARTLY_COMPLETE)) { qryPEV.ToList()[0].ppe.PPE_PARTLY_COMPLETE = String.Empty; }
        //                pModel.bPartly = qryPEV.ToList()[0].ppe.PPE_PARTLY.ToUpper() == "Y" ? true : false;
        //                pModel.bPartlyComplete = qryPEV.ToList()[0].ppe.PPE_PARTLY_COMPLETE.ToUpper() == "Y" ? true : false;

        //                if (pModel.QuantityList != null) { pModel.QuantityList = null; }
        //                pModel.QuantityList = new List<PEV_Quantity>();
        //                pModel.QuantityList.Add(new PEV_Quantity { QuantityUnit = "ML", QtyBL = qryPEV.ToList()[0].ppe.PPE_BL_VOLUME_ML.ToString(), QtyOutTurn = qryPEV.ToList()[0].ppe.PPE_GR_VOLUME_ML.ToString() });
        //                pModel.QuantityList.Add(new PEV_Quantity { QuantityUnit = "MT", QtyBL = qryPEV.ToList()[0].ppe.PPE_BL_VOLUME_MT.ToString(), QtyOutTurn = qryPEV.ToList()[0].ppe.PPE_GR_VOLUME_MT.ToString() });
        //                pModel.QuantityList.Add(new PEV_Quantity { QuantityUnit = "BBL", QtyBL = qryPEV.ToList()[0].ppe.PPE_BL_VOLUME_BBL.ToString(), QtyOutTurn = qryPEV.ToList()[0].ppe.PPE_GR_VOLUME_BBL.ToString() });

        //                pModel.PriceList[0].FOBPriceUSD = qryPEV.ToList()[0].ppe.PPE_FOB_PRICE_USD.ToString();
        //                pModel.PriceList[0].FOBSupplier = qryPEV.ToList()[0].ppe.PPE_FOB_SUPPLIER;
        //                pModel.PriceList[0].FOBTotalUSD = qryPEV.ToList()[0].ppe.PPE_FOB_TOTAL_USD.ToString();
        //                pModel.PriceList[0].FOBROE = qryPEV.ToList()[0].ppe.PPE_FOB_ROE.ToString();
        //                pModel.PriceList[0].FOBTotalTHB = qryPEV.ToList()[0].ppe.PPE_FOB_TOTAL_THB.ToString();
        //                pModel.PriceList[0].FRTPriceUSD = qryPEV.ToList()[0].ppe.PPE_FRT_PRICE_USD.ToString();
        //                pModel.PriceList[0].FRTTotalUSD = qryPEV.ToList()[0].ppe.PPE_FRT_TOTAL_USD.ToString();
        //                pModel.PriceList[0].FRTROE = qryPEV.ToList()[0].ppe.PPE_FRT_ROE.ToString();
        //                pModel.PriceList[0].FRTTotalTHB = qryPEV.ToList()[0].ppe.PPE_FRT_TOTAL_THB.ToString();
        //                pModel.PriceList[0].FOBFRTTotalUSD = qryPEV.ToList()[0].ppe.PPE_FOB_FRT_TOTAL_USD.ToString();
        //                pModel.PriceList[0].FOBFRTTotalTHB = qryPEV.ToList()[0].ppe.PPE_FOB_FRT_TOTAL_THB.ToString();
        //                pModel.PriceList[0].INSPriceUSD = qryPEV.ToList()[0].ppe.PPE_INS_PRICE_USD.ToString();
        //                pModel.PriceList[0].INSTotalUSD = qryPEV.ToList()[0].ppe.PPE_INS_TOTAL_USD.ToString();
        //                pModel.PriceList[0].InsuranceRate = qryPEV.ToList()[0].ppe.PPE_INS_RATE.ToString();
        //                pModel.PriceList[0].StampDutyTotalUSD = qryPEV.ToList()[0].ppe.PPE_STAMP_DUTY_TOTAL_USD.ToString();
        //                pModel.PriceList[0].InsStampDutyRate = qryPEV.ToList()[0].ppe.PPE_INS_STAMP_DUTY_RATE.ToString();
        //                pModel.PriceList[0].LossGain = qryPEV.ToList()[0].ppe.PPE_LOSS_GAIN.ToString();
        //                pModel.PriceList[0].MRCInsTotalUSD = qryPEV.ToList()[0].ppe.PPE_MRC_INS_TOTAL_USD.ToString();
        //                pModel.PriceList[0].MRCInsROE = qryPEV.ToList()[0].ppe.PPE_MRC_INS_ROE.ToString();
        //                pModel.PriceList[0].MRCInsTotalTHB = qryPEV.ToList()[0].ppe.PPE_MRC_INS_TOTAL_THB.ToString();
        //                pModel.PriceList[0].TotalCIFPriceUSD = qryPEV.ToList()[0].ppe.PPE_TOTAL_CIF_PRICE_USD.ToString();
        //                pModel.PriceList[0].TotalCIFTotalUSD = qryPEV.ToList()[0].ppe.PPE_TOTAL_CIF_TOTAL_USD.ToString();
        //                pModel.PriceList[0].TotalCIFTotalTHB = qryPEV.ToList()[0].ppe.PPE_TOTAL_CIF_TOTAL_THB.ToString();
        //                pModel.PriceList[0].RemarkFOBUSD = qryPEV.ToList()[0].ppe.PPE_REMARK_FOB_USD.ToString();
        //                pModel.PriceList[0].RemarkFOBTHB = qryPEV.ToList()[0].ppe.PPE_REMARK_FOB_THB.ToString();
        //                pModel.PriceList[0].RemarkFRTUSD = qryPEV.ToList()[0].ppe.PPE_REMARK_FRT_USD.ToString();
        //                pModel.PriceList[0].RemarkFRTTHB = qryPEV.ToList()[0].ppe.PPE_REMARK_FRT_THB.ToString();
        //                pModel.PriceList[0].RemarkINSUSD = qryPEV.ToList()[0].ppe.PPE_REMARK_INS_USD.ToString();
        //                pModel.PriceList[0].RemarkINSTHB = qryPEV.ToList()[0].ppe.PPE_REMARK_INS_THB.ToString();
        //                pModel.PriceList[0].RemarkImportDutyUSD = qryPEV.ToList()[0].ppe.PPE_REMARK_IMPORT_DUTY_USD.ToString();
        //                pModel.PriceList[0].RemarkImportDutyTHB = qryPEV.ToList()[0].ppe.PPE_REMARK_IMPORT_DUTY_THB.ToString();
        //                pModel.PriceList[0].RemarkImportDutyROE = qryPEV.ToList()[0].ppe.PPE_REMARK_IMPORT_DUTY_ROE.ToString();
        //                pModel.PriceList[0].RemarkTotalUSD = qryPEV.ToList()[0].ppe.PPE_REMARK_TOTAL_USD.ToString();
        //                pModel.PriceList[0].RemarkTotalTHB = qryPEV.ToList()[0].ppe.PPE_REMARK_TOTAL_THB.ToString();

        //                if (pModel.PriceSupplierList != null) { pModel.PriceSupplierList = null; }
        //                pModel.PriceSupplierList = new List<PEV_PriceSupplier>();

        //                Double nNetTotalVolume = 0, nNetTotalPriceUSD = 0, nNetTotalPriceTHB = 0;

        //                foreach (var tmp in qryPEV.ToList())
        //                {
        //                    pModel.PriceSupplierList.Add(
        //                    new PEV_PriceSupplier
        //                    {
        //                        SupplierName = tmp.Supplier,
        //                        VolumeBBL = tmp.ppi.PPI_VOLUME_BBL.ToString(),
        //                        VolumeML = tmp.ppi.PPI_VOLUME_BBL.ToString(),
        //                        VolumeMT = tmp.ppi.PPI_VOLUME_BBL.ToString(),
        //                        PriceUnitUSD = tmp.ppi.PPI_UNIT_PRICE.ToString(),
        //                        PriceTotalUSD = tmp.ppi.PPI_TOTAL_USD.ToString(),
        //                        ROE = tmp.ppi.PPI_ROE.ToString(),
        //                        PriceTotalTHB = tmp.ppi.PPI_TOTAL_THB.ToString(),
        //                        PriceStatus = tmp.PriceStatus,
        //                        InvoiceNo = tmp.InvoiceNo
        //                    });
        //                    nNetTotalVolume += Double.Parse(String.IsNullOrEmpty(tmp.ppi.PPI_VOLUME_BBL.ToString()) ? "0" : tmp.ppi.PPI_VOLUME_BBL.ToString());
        //                    nNetTotalPriceUSD += Double.Parse(String.IsNullOrEmpty(tmp.ppi.PPI_TOTAL_USD.ToString()) ? "0" : tmp.ppi.PPI_TOTAL_USD.ToString());
        //                    nNetTotalPriceTHB += Double.Parse(String.IsNullOrEmpty(tmp.ppi.PPI_TOTAL_THB.ToString()) ? "0" : tmp.ppi.PPI_TOTAL_THB.ToString());
        //                }
        //                pModel.PriceSupplierList.ToList()[0].NetTotalVolumeBBL = nNetTotalVolume.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalPriceBBLUSD = nNetTotalPriceUSD.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalPriceBBLTHB = nNetTotalPriceTHB.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalVolumeML = nNetTotalVolume.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalPriceMLUSD = nNetTotalPriceUSD.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalPriceMLTHB = nNetTotalPriceTHB.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalVolumeMT = nNetTotalVolume.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalPriceMTUSD = nNetTotalPriceUSD.ToString();
        //                pModel.PriceSupplierList.ToList()[0].NetTotalPriceMTTHB = nNetTotalPriceTHB.ToString();

        //                var qryPEV10 = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO == sTripNo && z.PPE_MET_NUM == sMetNum && z.PPE_ITEM_NO == 10);
        //                if (qryPEV10 != null)
        //                {
        //                    for (int k = 0; k < pModel.PriceSupplierList.ToList().Count; k++)
        //                    {
        //                        pModel.PriceSupplierList.ToList()[k].OriginalVolumeBBL = qryPEV10.ToList()[0].PPE_BL_VOLUME_BBL.ToString();
        //                        pModel.PriceSupplierList.ToList()[k].OriginalVolumeML = qryPEV10.ToList()[0].PPE_BL_VOLUME_ML.ToString();
        //                        pModel.PriceSupplierList.ToList()[k].OriginalVolumeMT = qryPEV10.ToList()[0].PPE_BL_VOLUME_MT.ToString();
        //                    }
        //                }

        //                pModel.SAPDocumentDate = String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_DOCUMENT_DATE.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].ppe.PPE_DOCUMENT_DATE.ToString()).ToString(format, provider);
        //                pModel.SAPPostingDate = String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_POSTING_DATE.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].ppe.PPE_POSTING_DATE.ToString()).ToString(format, provider);
        //                pModel.SAPReverseDateForTypeA = String.IsNullOrEmpty(qryPEV.ToList()[0].ppe.PPE_REVERSE_DATE_FOR_TYPE_A.ToString()) ? "" : Convert.ToDateTime(qryPEV.ToList()[0].ppe.PPE_REVERSE_DATE_FOR_TYPE_A.ToString()).ToString(format, provider);

        //            }
        //            else
        //            {
        //                PurchaseEntryVesselServiceModel serviceModel = new PurchaseEntryVesselServiceModel();
        //                serviceModel.setInitialModel(ref pModel);
        //                //pModel.sTripNo = sTripNo;
        //                //pModel.sMetNum = sMetNum;
        //                //pModel.sItemNo = nItemNo.ToString();
        //            }

        //            rtn = null;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        rtn.Message = ex.Message;
        //        rtn.Status = false;
        //    }

        //    return rtn;
        //}

        public List<PCF_MT_STORAGELOCATION> getStorageLocation()
        {
            string strJSON = "{{ \"COMPANY_CODE\":\"\",\"CODE\":\"\",\"NAME\":\"\"}}";
            string JsonD = MasterData.GetJsonGlobalConfig("CIP_IMPORT_PLAN");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigStorageLocation dataList = (GlobalConfigStorageLocation)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigStorageLocation));

            return dataList.PCF_MT_STORAGE_LOCATION;
        }

        //public string ExportToExcel(PurchaseEntryVesselViewModel model)
        //{
        //    //DataTable dtData = new DataTable();
        //    string msg = "";

        //    String xFileName = "Temp_PCF_Purchase_Entry_Vessel_Report";
        //    String lFileName = ".xlsx";
        //    String path = System.Web.HttpContext.Current.Server.MapPath(@"..\..\Areas\CPAIMVC\Report\ExcelTemplate\");
        //    String newFilename = xFileName.Replace("Temp_", "") + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss").ToString() + lFileName;
        //    String newExcelFileNameFullPath = System.Web.HttpContext.Current.Server.MapPath(@"..\..\Areas\CPAIMVC\Report\Temp\") + newFilename;

        //    //string filePath = "c:\\temp\\";
        //    //FileInfo excelFile = new FileInfo(filePath);

        //    try
        //    {
        //        System.IO.File.Copy(path + "Temp_PCF_Purchase_Entry_Vessel_Report" + lFileName, newExcelFileNameFullPath, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        msg = "error: 1_" + ex.Message.ToString();
        //        goto Next_Line_Exit;
        //    }

        //    xFileName = newFilename;
        //    int irows = 0;

        //    try
        //    {
        //        ExcelPackage excelApp = new ExcelPackage();
        //        ExcelWorksheet excelSheets;

        //        try
        //        {
        //            excelApp = new ExcelPackage(new System.IO.FileInfo(newExcelFileNameFullPath));
        //            excelSheets = (ExcelWorksheet)excelApp.Workbook.Worksheets[1];

        //            //Trip No. / BL Date/Lasted Update
        //            excelSheets.Cells[3, 3].Value = model.sTripNo;
        //            excelSheets.Cells[3, 5].Value = model.sBLDate;
        //            excelSheets.Cells[3, 6].Value = model.sLastedUpdate;

        //            //Crude Type / Due Date
        //            excelSheets.Cells[4, 3].Value = model.sMetNum;
        //            excelSheets.Cells[4, 5].Value = model.sDueDate;

        //            //Vessel / Receipt Date
        //            excelSheets.Cells[5, 3].Value = model.sVesselName;
        //            excelSheets.Cells[5, 5].Value = model.sPostingDate;

        //            //Quantity , B/L , Outturn
        //            //Unit ML
        //            excelSheets.Cells[7, 1].Value = model.QuantityList.ToList()[0].QuantityUnit;
        //            excelSheets.Cells[7, 2].Value = model.QuantityList.ToList()[0].QtyBL;
        //            excelSheets.Cells[7, 3].Value = model.QuantityList.ToList()[0].QtyOutTurn;
        //            //Unit MT
        //            excelSheets.Cells[8, 1].Value = model.QuantityList.ToList()[1].QuantityUnit;
        //            excelSheets.Cells[8, 2].Value = model.QuantityList.ToList()[1].QtyBL;
        //            excelSheets.Cells[8, 3].Value = model.QuantityList.ToList()[1].QtyOutTurn;
        //            //Unit BBL
        //            excelSheets.Cells[9, 1].Value = model.QuantityList.ToList()[2].QuantityUnit;
        //            excelSheets.Cells[9, 2].Value = model.QuantityList.ToList()[2].QtyBL;
        //            excelSheets.Cells[9, 3].Value = model.QuantityList.ToList()[2].QtyOutTurn;

        //            //FOB Unit Price USD
        //            excelSheets.Cells[12, 2].Value = model.PriceList[0].FOBPriceUSD;
        //            //FOB Supplier
        //            excelSheets.Cells[12, 3].Value = model.PriceList[0].FOBSupplier;
        //            //FOB Total USD
        //            excelSheets.Cells[12, 5].Value = model.PriceList[0].FOBTotalUSD;
        //            //FOB ROE
        //            excelSheets.Cells[12, 6].Value = model.PriceList[0].FOBROE;
        //            //FOB Total THB
        //            excelSheets.Cells[12, 7].Value = model.PriceList[0].FOBTotalTHB;

        //            //FRT Price USD
        //            excelSheets.Cells[13, 2].Value = model.PriceList[0].FRTPriceUSD;
        //            //FRT Total USD
        //            excelSheets.Cells[13, 5].Value = model.PriceList[0].FRTTotalUSD;
        //            //FRT ROE
        //            excelSheets.Cells[13, 6].Value = model.PriceList[0].FRTROE;
        //            //FRT Total THB
        //            excelSheets.Cells[13, 7].Value = model.PriceList[0].FRTTotalTHB;

        //            //FOB FRT Total USD
        //            excelSheets.Cells[14, 5].Value = model.PriceList[0].FOBFRTTotalUSD;
        //            //FOB FRT Total THB
        //            excelSheets.Cells[14, 7].Value = model.PriceList[0].FOBFRTTotalTHB;

        //            //INS Price USD
        //            excelSheets.Cells[15, 2].Value = model.PriceList[0].INSPriceUSD;
        //            //INS Total USD
        //            excelSheets.Cells[15, 5].Value = model.PriceList[0].INSTotalUSD;

        //            //Insurance Rate
        //            excelSheets.Cells[16, 1].Value = model.PriceList[0].InsuranceRate;
        //            //Stamp Duty Total USD
        //            excelSheets.Cells[16, 5].Value = model.PriceList[0].StampDutyTotalUSD;

        //            //Insurance Rate
        //            excelSheets.Cells[17, 1].Value = model.PriceList[0].InsStampDutyRate;

        //            //Loss Gain
        //            excelSheets.Cells[18, 2].Value = model.PriceList[0].LossGain;
        //            //MRC INS Total USD
        //            excelSheets.Cells[18, 5].Value = model.PriceList[0].MRCInsTotalUSD;
        //            //MRC INS ROE
        //            excelSheets.Cells[18, 6].Value = model.PriceList[0].MRCInsROE;
        //            //MRC INS Total THB
        //            excelSheets.Cells[18, 7].Value = model.PriceList[0].MRCInsTotalTHB;

        //            //Total CIF Price USD
        //            excelSheets.Cells[19, 2].Value = model.PriceList[0].TotalCIFPriceUSD;
        //            //Total CIF Total USD
        //            excelSheets.Cells[19, 5].Value = model.PriceList[0].TotalCIFTotalUSD;
        //            //MRC INS Total THB
        //            excelSheets.Cells[19, 7].Value = model.PriceList[0].TotalCIFTotalTHB;

        //            //Remark FOB USD
        //            excelSheets.Cells[20, 5].Value = model.PriceList[0].RemarkFOBUSD;
        //            //Remark FOB THB
        //            excelSheets.Cells[20, 7].Value = model.PriceList[0].RemarkFOBTHB;

        //            //Remark FRT USD
        //            excelSheets.Cells[21, 5].Value = model.PriceList[0].RemarkFRTUSD;
        //            //Remark FRT THB
        //            excelSheets.Cells[21, 7].Value = model.PriceList[0].RemarkFRTTHB;

        //            //Remark INS USD
        //            excelSheets.Cells[22, 5].Value = model.PriceList[0].RemarkINSUSD;
        //            //Remark INS THB
        //            excelSheets.Cells[22, 7].Value = model.PriceList[0].RemarkINSTHB;

        //            //Remark Import Duty USD
        //            excelSheets.Cells[23, 5].Value = model.PriceList[0].RemarkImportDutyUSD;
        //            //Remark Import Duty ROE
        //            excelSheets.Cells[23, 6].Value = model.PriceList[0].RemarkImportDutyROE;
        //            //Remark Import Duty THB
        //            excelSheets.Cells[23, 7].Value = model.PriceList[0].RemarkImportDutyTHB;

        //            //Remark Total USD
        //            excelSheets.Cells[24, 5].Value = model.PriceList[0].RemarkTotalUSD;
        //            //Remark Total THB
        //            excelSheets.Cells[24, 7].Value = model.PriceList[0].RemarkTotalTHB;

        //            irows = 25;

        //            for (int i = 0; i < model.PriceSupplierList.ToList().Count; i++)
        //            {
        //                irows += 1;
        //                try
        //                {
        //                    CopyRowTo(excelSheets, excelSheets, 25, irows);
        //                }
        //                catch { }

        //                #region "detail"
        //                excelSheets.Cells[irows, 1].Value = model.PriceSupplierList[i].SupplierName + "";
        //                excelSheets.Cells[irows, 2].Value = model.PriceSupplierList[i].VolumeBBL + "";
        //                excelSheets.Cells[irows, 3].Value = model.PriceSupplierList[i].PriceUnitUSD + "";
        //                excelSheets.Cells[irows, 4].Value = model.PriceSupplierList[i].PriceStatus + "";
        //                excelSheets.Cells[irows, 5].Value = model.PriceSupplierList[i].PriceTotalUSD + "";
        //                excelSheets.Cells[irows, 6].Value = model.PriceSupplierList[i].ROE + "";
        //                excelSheets.Cells[irows, 7].Value = model.PriceSupplierList[i].PriceTotalTHB + "";
        //                excelSheets.Cells[irows, 8].Value = model.PriceSupplierList[i].InvoiceNo + "";
        //                #endregion "detail"
        //            }

        //            #region "footer"
        //            irows += 1;
        //            excelSheets.Cells[irows, 2].Value = model.PriceSupplierList[0].NetTotalVolumeBBL + "";
        //            excelSheets.Cells[irows, 5].Value = model.PriceSupplierList[0].NetTotalPriceBBLUSD + "";
        //            excelSheets.Cells[irows, 7].Value = model.PriceSupplierList[0].NetTotalPriceBBLUSD + "";
        //            #endregion

        //            excelSheets.DeleteRow(25, 1, true);

        //            excelApp.Save();

        //            excelApp.Dispose();
        //            excelApp = null;

        //        }
        //        catch (Exception ex)
        //        {
        //            msg = "error: 2_" + ex.Message.ToString(); goto Next_Line_Exit;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        msg = "error: 3_" + ex.Message.ToString(); goto Next_Line_Exit;
        //    }
        //    msg = newExcelFileNameFullPath;

        //    Next_Line_Exit:
        //    return msg;
        //}

        //private Double CnDoubleRe0(Object boject_value)
        //{
        //    try
        //    {
        //        return Convert.ToDouble(boject_value);
        //    }
        //    catch { return 0; }
        //}

        private void CopyRowTo(ExcelWorksheet input, ExcelWorksheet output, int row_in, int row_out)
        {
            input.InsertRow(row_out, 1, row_in);
            ExcelRange rng1 = input.SelectedRange[row_in, 1, row_in, 8];
            ExcelRange rng2 = input.SelectedRange[row_out, 1, row_out, 8];
            rng1.Copy(rng2);
        }

        public void setInitialModel(ref PurchaseEntryVesselViewModel model)
        {
            if (model.PriceList == null) { model.PriceList = new List<PEV_Price>(); }
            if (model.ddl_InsuranceRate == null)
            {
                model.ddl_InsuranceRate = new List<SelectListItem>();
                model.ddl_InsuranceRate = PurchaseEntryVesselServiceModel.getCIPConfigInsuranceRate(model);
            }
            if (model.ddl_ItemNo == null) { model.ddl_ItemNo = new List<SelectListItem>(); }
            model.ddl_ItemNo.Add(new SelectListItem { Value = "", Text = "" });
            if (model.QuantityList == null) { model.QuantityList = new List<PEV_Quantity>(); }
            model.QuantityList.Add(new PEV_Quantity { QuantityUnit = "", QtyBL = "0", QtyOutTurn = "0" });
            if (model.PriceSupplierList == null) { model.PriceSupplierList = new List<PEV_PriceSupplier>(); }
            model.PriceSupplierList.Add(new PEV_PriceSupplier
            {
                SupplierName = "",
                VolumeBBL = "0",
                VolumeML = "0",
                VolumeMT = "0",
                PriceStatus = "",
                ROE = "0",
                PriceUnitUSD = "0",
                PriceTotalUSD = "0",
                PriceTotalTHB = "0",
                OriginalVolumeBBL = "0",
                OriginalVolumeML = "0",
                OriginalVolumeMT = "0",
                NetTotalPriceBBLTHB = "0",
                NetTotalPriceBBLUSD = "0",
                NetTotalVolumeBBL = "0",
                NetTotalVolumeML = "0",
                NetTotalPriceMLUSD = "0",
                NetTotalPriceMLTHB = "0",
                NetTotalVolumeMT = "0",
                NetTotalPriceMTUSD = "0",
                NetTotalPriceMTTHB = "0",
                InvoiceNo = "",
                MatItemNo = ""
            });
            model.PriceList[0].FOBFRTTotalTHB = "0";
            model.PriceList[0].FOBFRTTotalUSD = "0";
            model.PriceList[0].FOBPriceUSD = "0";
            model.PriceList[0].FOBROE = "0";
            model.PriceList[0].FOBSupplier = "";
            model.PriceList[0].FOBTotalTHB = "0";
            model.PriceList[0].FOBTotalUSD = "0";
            model.PriceList[0].FRTPriceUSD = "0";
            model.PriceList[0].FRTROE = "0";
            model.PriceList[0].FRTTotalTHB = "0";
            model.PriceList[0].FRTTotalUSD = "0";
            model.PriceList[0].INSPriceUSD = "0";
            model.PriceList[0].InsStampDutyRate = "0";
            model.PriceList[0].INSTotalUSD = "0";
            model.PriceList[0].InsuranceRate = "0";
            model.PriceList[0].LossGain = "0";
            model.PriceList[0].MRCInsROE = "0";
            model.PriceList[0].MRCInsTotalTHB = "0";
            model.PriceList[0].MRCInsTotalUSD = "0";
            model.PriceList[0].RemarkFOBTHB = "0";
            model.PriceList[0].RemarkFOBUSD = "0";
            model.PriceList[0].RemarkFRTTHB = "0";
            model.PriceList[0].RemarkFRTUSD = "0";
            model.PriceList[0].RemarkImportDutyROE = "0";
            model.PriceList[0].RemarkImportDutyTHB = "0";
            model.PriceList[0].RemarkImportDutyUSD = "0";
            model.PriceList[0].RemarkINSTHB = "0";
            model.PriceList[0].RemarkINSUSD = "0";
            model.PriceList[0].RemarkTotalTHB = "0";
            model.PriceList[0].RemarkTotalUSD = "0";
            model.PriceList[0].StampDutyTotalUSD = "0";
            model.PriceList[0].TotalCIFPriceUSD = "0";
            model.PriceList[0].TotalCIFTotalTHB = "0";
            model.PriceList[0].TotalCIFTotalUSD = "0";
            model.bPartly = false;
            model.bPartlyComplete = false;
            model.isEdit = false;
            model.SAPAccuralType = "";
            model.SAPDocumentDate = "";
            model.SAPPostingDate = "";
            model.SAPReverseDateForTypeA = "";
            //model.sBLDate = "";
            model.sCalInvFigure = "";
            model.sCalVolumeUnit = "";
            //model.sDueDate = "";
            //model.sItemNo = "0";
            //model.sLastedUpdate = "";
            //model.sMetNum = "";
            model.sPostingDate = "";
            //model.sTripNo = "";
            //model.sVesselName = "";
        }

        public ReturnValue SendToSAP(string jsonConfig, PurchaseEntryVesselViewModel model, string accrualType, string reverseDate, string lbUserName,int num)
        {
            ReturnValue rtn = new ReturnValue();
            rtn.Status = true;
            //string JsonD = MasterData.GetJsonMasterSetting("PIT_ACCRUAL_POSTING_CONFIG");
            PurchaseEntryServiceModel sm = new PurchaseEntryServiceModel();
            var accrualSetting = sm.GetAccrualSettings();
            var accrualSettingCIP = GetAccrualSettingsCIP();
            var companyCode = model.PEV_Price.CompanyCode;
            var metNum = model.PEV_Price.sMetNum;
            var item_no = Decimal.Parse(model.PEV_Price.sItemNo);
            var trip_no = model.PEV_Price.sTripNo;
            var io = GetIOFromMetNum(metNum, companyCode);
            string sapFiDoc = "";

            #region "Vessel send to SAP"
            try
            {
                if (accrualType.ToLower() == "c")
                {
                    System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                    BAPIACHE09 DocumentHeader = new BAPIACHE09();
                    List<BAPIACGL09> AccountGl = new List<BAPIACGL09>();
                    List<BAPIACCR09> CurrencyAmount = new List<BAPIACCR09>();
                    List<BAPIPAREX> Extension2 = new List<BAPIPAREX>();
                    List<ZFI_GL_MAPPING> zfi_GL_MAPPING = new List<ZFI_GL_MAPPING>();
                    string Month = "";
                    string Year = "";
                    using (var context = new EntityCPAIEngine())
                    {
                        var query = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO == trip_no && z.PPE_MET_NUM == metNum && z.PPE_ITEM_NO == item_no).SingleOrDefault();
                        Month = query.PPE_GR_DATE.Value.ToString("MMM", cultureinfo);
                        Year = query.PPE_GR_DATE.Value.ToString("yy", cultureinfo);
                    }

                    string metMatDesEnglish = GetMetMatDesEnglishFromMetNum(metNum);
                    string freightCurrency = "";
                    int running_num = 1;
                    string item_txt = "";
                    string running_str = "";
                    bool isFreight = IsFreightAvailable("", out freightCurrency);
                    string fisPeriod = (DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Month < 10) ? "0" + DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Month.ToString() : DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Month.ToString();
                    var configEx = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2.Where(x => x.COMP_CODE == companyCode);

                    #region DocumentHeader
                    var documentHeaderFromConfig = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.DOCUMENTHEADER
                        .Where(x => x.ACCRUE_TYPE.ToLower() == accrualType.ToLower() && x.COMP_CODE == companyCode);
                    if ((documentHeaderFromConfig == null) || (documentHeaderFromConfig.ToList().Count() == 0))
                    {
                        return null;
                    }
                    var docHeader = documentHeaderFromConfig.ToList()[0];

                    DocumentHeader.OBJ_TYPE = docHeader.OBJ_TYPE;
                    DocumentHeader.OBJ_KEY = docHeader.OBJ_KEY;
                    DocumentHeader.BUS_ACT = docHeader.BUS_ACT;
                    DocumentHeader.USERNAME = docHeader.USERNAME;
                    DocumentHeader.HEADER_TXT = metMatDesEnglish + " REC." + Month.ToUpper() + Year;
                    //DocumentHeader.HEADER_TXT = (metMatDesEnglish.Length <= avaiableCharRangeForHeaderTxt)
                    //    ? metMatDesEnglish + " " + headerTxtSuffix
                    //    : metMatDesEnglish.Substring(0, avaiableCharRangeForHeaderTxt) + " " + headerTxtSuffix;
                    DocumentHeader.COMP_CODE = companyCode;
                    DocumentHeader.DOC_DATE = DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                    DocumentHeader.PSTNG_DATE = DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                    DocumentHeader.FISC_YEAR = DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Year.ToString();
                    DocumentHeader.FIS_PERIOD = fisPeriod;
                    DocumentHeader.DOC_TYPE = docHeader.DOC_TYPE;
                    DocumentHeader.REF_DOC_NO_LONG = "";
                    DocumentHeader.REASON_REV = docHeader.REASON_REV;

                    rtn.Message += "HEADER_TXT = " + DocumentHeader.HEADER_TXT + ", ";
                    rtn.Message += "DOC_DATE = " + DocumentHeader.DOC_DATE + ", ";
                    rtn.Message += "PSTNG_DATE = " + DocumentHeader.PSTNG_DATE + ", ";
                    rtn.Message += "FIS_PERIOD = " + DocumentHeader.FIS_PERIOD + "\r\n";
                    #endregion

                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        #region Crude
                            foreach (var item in model.PEV_Price.PriceSupplierList)
                            {
                                var sortFieldCu = GetSortFieldFromSupplier(item.SupplierName);
                                var configGlM = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Material");
                                var configFi = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING.Where(x => x.COMP_CODE == companyCode);
                                decimal ItemNo = Convert.ToDecimal(item.MatItemNo);
                                var formulaMet = (context.PCF_MATERIAL.Where(p => p.PMA_TRIP_NO.Equals(trip_no) && p.PMA_MET_NUM.Equals(metNum) && p.PMA_ITEM_NO.Equals(ItemNo))).ToList()[0].PMA_FORMULA;
                                if (configGlM.ToList().Count() == 0) { return null; }
                                if (configEx.ToList().Count() == 0) { return null; }
                                if (configFi.ToList().Count() == 0) { return null; }
                                running_str = running_num.ToString().PadLeft(10, '0');
                                string volumnUnit = model.PEV_Price.sCalVolumeUnit.ToUpper();
                                string volumn = "";
                                item_txt = "CRU/" + item.txtSupplierName + "/" + model.PEV_Price.sTxtVesselName + "/" + formulaMet;
                                item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;
                                if (volumnUnit == "BBL") volumn = item.VolumeBBL;
                                else if (volumnUnit == "MT") volumn = item.VolumeMT;
                                else if (volumnUnit == "L30") volumn = item.VolumeML;
                                volumn = Convert.ToInt64(Convert.ToDecimal(volumn.Replace(",", ""))).ToString();

                                #region Debits
                                AccountGl.Add(new BAPIACGL09
                                {
                                    ITEMNO_ACC = running_str,
                                    GL_ACCOUNT = configGlM.ToList()[0].GLACCOUNT,
                                    ITEM_TEXT = item_txt,
                                    REF_KEY_3 = sortFieldCu,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = item.SupplierName.PadLeft(10, '0'),
                                    ALLOC_NMBR = trip_no,
                                    COSTCENTER = configGlM.ToList()[0].COSTCENTER,
                                    ORDERID = io
                                });

                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "00",
                                    CURRENCY = "USD",
                                    AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalUSD),
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "10",
                                    CURRENCY = "THB",
                                    AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalTHB),
                                    AMT_DOCCURSpecified = true
                                });
                                Extension2.Add(new BAPIPAREX //VOLUME
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Volume").ToList()[0].VALUEPART1,
                                    VALUEPART2 = volumn
                                });
                                Extension2.Add(new BAPIPAREX //UOM
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "UOM").ToList()[0].VALUEPART1,
                                    VALUEPART2 = volumnUnit
                                });
                                Extension2.Add(new BAPIPAREX //Business Place
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                                });
                                running_num++;
                                #endregion

                                running_str = running_num.ToString().PadLeft(10, '0');
                                item_txt = "CRU/" + item.txtSupplierName + "/" + model.PEV_Price.sTxtVesselName + "/" + formulaMet;
                                item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;

                                #region Credit
                                AccountGl.Add(new BAPIACGL09
                                {
                                    ITEMNO_ACC = running_str,
                                    ITEM_TEXT = item_txt,
                                    REF_KEY_3 = sortFieldCu,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = item.SupplierName.PadLeft(10, '0'),
                                    ALLOC_NMBR = trip_no
                                });

                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "00",
                                    CURRENCY = "USD",
                                    AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalUSD) * -1,
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "10",
                                    CURRENCY = "THB",
                                    AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalTHB) * -1,
                                    AMT_DOCCURSpecified = true
                                });

                                Extension2.Add(new BAPIPAREX //Business Place
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                                });

                                zfi_GL_MAPPING.Add(new ZFI_GL_MAPPING
                                {
                                    ITEMNO_ACC = running_str,
                                    TRAN_IND = configFi.ToList()[0].TRAN_IND
                                });
                                running_num++;
                                #endregion
                            }
                        #endregion

                        #region Freight

                        var configGlF = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Freight");
                        var configZfi = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Freight");
                        if (model.PEV_Price.FreightList != null)
                        {
                            foreach (var item in model.PEV_Price.FreightList)
                            {
                                //var formulaMet = (context.PCF_MATERIAL.Where(p => p.PMA_TRIP_NO.Equals(model.sTripNo) && p.PMA_MET_NUM.Equals(metNum))).ToList()[0].PMA_FORMULA;
                                var sp_vender = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.SP_VENDER.Where(x => x.VENDER_CODE == item.FRTSupplier).FirstOrDefault();
                                string[] words = item.FRTSupplierTxt.Split('('); ;
                                item_txt = sp_vender == null ? "FRI/" + model.PEV_Price.sTxtVesselName + "/" + words[0] : sp_vender.ITEM_NO_TXT + "/" + "FRI/" + model.PEV_Price.sTxtVesselName + "/" + words[0];
                                var sortFieldFe = GetSortFieldFromSupplier(item.FRTSupplier);
                                running_str = running_num.ToString().PadLeft(10, '0');
                                string volumnUnit = model.PEV_Price.sCalVolumeUnit.ToUpper();
                                string volumn = "";
                                if (volumnUnit == "BBL") volumn = model.PEV_Price.QuantityList[2].QtyBL;
                                else if (volumnUnit == "MT") volumn = model.PEV_Price.QuantityList[1].QtyBL;
                                else if (volumnUnit == "L30") volumn = model.PEV_Price.QuantityList[0].QtyBL;
                                item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;
                                AccountGl.Add(new BAPIACGL09
                                {
                                    ITEMNO_ACC = running_str,
                                    GL_ACCOUNT = configGlF.ToList()[0].GLACCOUNT,
                                    ITEM_TEXT = item_txt,
                                    REF_KEY_3 = sortFieldFe,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = item.FRTSupplier.PadLeft(10, '0'),
                                    ALLOC_NMBR = trip_no,
                                    COSTCENTER = configGlF.ToList()[0].COSTCENTER,
                                    ORDERID = io
                                });

                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "00",
                                    CURRENCY = "USD",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalUSD),
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "10",
                                    CURRENCY = "THB",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalTHB),
                                    AMT_DOCCURSpecified = true
                                });
                                Extension2.Add(new BAPIPAREX //Business Place
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                                });

                                running_num++;
                                running_str = running_num.ToString().PadLeft(10, '0');

                                AccountGl.Add(new BAPIACGL09
                                {
                                    ITEMNO_ACC = running_str,
                                    ITEM_TEXT = item_txt,
                                    REF_KEY_3 = sortFieldFe,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = item.FRTSupplier.PadLeft(10, '0'),
                                    ALLOC_NMBR = trip_no
                                });

                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "00",
                                    CURRENCY = "USD",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalUSD) * -1,
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "10",
                                    CURRENCY = "THB",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalTHB) * -1,
                                    AMT_DOCCURSpecified = true
                                });
                                Extension2.Add(new BAPIPAREX //Business Place
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                                });

                                zfi_GL_MAPPING.Add(new ZFI_GL_MAPPING
                                {
                                    ITEMNO_ACC = running_str,
                                    TRAN_IND = configZfi.ToList()[0].TRAN_IND
                                });

                                running_num++;
                            }
                        }

                        #endregion

                        #region Insurance
                        var configIn = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Insurance");
                        var configZIn = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Insurance");
                        running_str = running_num.ToString().PadLeft(10, '0');
                        var formulaMetIn = (context.PCF_MATERIAL.Where(p => p.PMA_TRIP_NO.Equals(trip_no) && p.PMA_MET_NUM.Equals(metNum))).ToList()[0].PMA_FORMULA;
                        item_txt = "INS/" + model.PEV_Price.sTxtVesselName + "/" + formulaMetIn;
                        item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;
                        AccountGl.Add(new BAPIACGL09
                        {
                            ITEMNO_ACC = running_str,
                            GL_ACCOUNT = configIn.ToList()[0].GLACCOUNT,
                            ITEM_TEXT = item_txt,
                            REF_KEY_3 = configIn.ToList()[0].SUPPLIER_NAME,
                            COMP_CODE = companyCode,
                            VENDOR_NO = configIn.ToList()[0].SUPPLIER_CODE.PadLeft(10, '0'),
                            ALLOC_NMBR = trip_no,
                            COSTCENTER = configGlF.ToList()[0].COSTCENTER,
                            ORDERID = io
                        });

                        CurrencyAmount.Add(new BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "00",
                            CURRENCY = "USD",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalUSD),
                            AMT_DOCCURSpecified = true
                        });
                        CurrencyAmount.Add(new BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "10",
                            CURRENCY = "THB",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalTHB),
                            AMT_DOCCURSpecified = true
                        });
                        Extension2.Add(new BAPIPAREX //Business Place
                        {
                            STRUCTURE = running_str,
                            VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                        });

                        running_num++;
                        running_str = running_num.ToString().PadLeft(10, '0');
                        item_txt = "INS/" + model.PEV_Price.sTxtVesselName + "/" + formulaMetIn;
                        item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;

                        AccountGl.Add(new BAPIACGL09
                        {
                            ITEMNO_ACC = running_str,
                            ITEM_TEXT = item_txt,
                            REF_KEY_3 = configIn.ToList()[0].SUPPLIER_NAME,
                            COMP_CODE = companyCode,
                            VENDOR_NO = configIn.ToList()[0].SUPPLIER_CODE.PadLeft(10, '0'),
                            ALLOC_NMBR = trip_no
                        });

                        CurrencyAmount.Add(new BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "00",
                            CURRENCY = "USD",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalUSD) * -1,
                            AMT_DOCCURSpecified = true
                        });
                        CurrencyAmount.Add(new BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "10",
                            CURRENCY = "THB",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalTHB) * -1,
                            AMT_DOCCURSpecified = true
                        });
                        Extension2.Add(new BAPIPAREX //Business Place
                        {
                            STRUCTURE = running_str,
                            VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                        });

                        zfi_GL_MAPPING.Add(new ZFI_GL_MAPPING
                        {
                            ITEMNO_ACC = running_str,
                            TRAN_IND = configZIn.ToList()[0].TRAN_IND
                        });
                        #endregion

                    }
                    AccrualService accrualService = new AccrualService();
                        sapFiDoc = accrualService.Post(jsonConfig, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING);
                    if (!String.IsNullOrEmpty(sapFiDoc))
                    {
                        model.PriceList[num].FiDoc = sapFiDoc;
                        rtn = SaveData(ref model, lbUserName, sapFiDoc);
                        rtn.Message = "Send To SAP Complete Fi Doc : " + sapFiDoc;
                        rtn.Status = true;
                    }
                    else
                    {
                        rtn.Message = "Fail";
                        rtn.Status = false;
                    }
                }
                else // Type A
                {
                    System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                    com.pttict.sap.Interface.sap.accrual.create.BAPIACHE09 DocumentHeader = new com.pttict.sap.Interface.sap.accrual.create.BAPIACHE09();
                    List<com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09> AccountGl = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09>();
                    List<com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09> CurrencyAmount = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09>();
                    List<com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX> Extension2 = new List<com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX>();
                    List<com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING> zfi_GL_MAPPING = new List<com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING>();

                    string metMatDesEnglish = GetMetMatDesEnglishFromMetNum(metNum);
                    string freightCurrency = "";
                    string item_txt = "";
                    int running_num = 1;
                    string running_str = "";
                    bool isFreight = IsFreightAvailable("", out freightCurrency);
                    string fisPeriod = (DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Month < 10) ? "0" + DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Month.ToString() : DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Month.ToString();
                    var configEx = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.EXTENSION2.Where(x => x.COMP_CODE == companyCode);
                    string Month = "";
                    string Year = "";
                    using (var context = new EntityCPAIEngine())
                    {
                        var query = context.PCF_PURCHASE_ENTRY.Where(z => z.PPE_TRIP_NO == trip_no && z.PPE_MET_NUM == metNum && z.PPE_ITEM_NO == item_no).SingleOrDefault();
                        Month = query.PPE_GR_DATE.Value.ToString("MMM", cultureinfo);
                        Year = query.PPE_GR_DATE.Value.ToString("yy", cultureinfo);
                    }
                    #region DocumentHeader
                    var documentHeaderFromConfig = accrualSetting.PIT_ACCRUAL_POSTING_CONFIG.DOCUMENTHEADER
                            .Where(x => x.ACCRUE_TYPE.ToLower() == accrualType.ToLower() && x.COMP_CODE == companyCode);
                    if ((documentHeaderFromConfig == null) || (documentHeaderFromConfig.ToList().Count() == 0))
                    {
                        return null;
                    }
                    var docHeader = documentHeaderFromConfig.ToList()[0];

                    DocumentHeader.OBJ_TYPE = docHeader.OBJ_TYPE;
                    DocumentHeader.OBJ_KEY = docHeader.OBJ_KEY;
                    DocumentHeader.BUS_ACT = docHeader.BUS_ACT;
                    DocumentHeader.USERNAME = docHeader.USERNAME;
                    DocumentHeader.HEADER_TXT = metMatDesEnglish + " REC." + Month + Year;
                    //DocumentHeader.HEADER_TXT = (metMatDesEnglish.Length <= avaiableCharRangeForHeaderTxt)
                    //    ? metMatDesEnglish + " " + headerTxtSuffix
                    //    : metMatDesEnglish.Substring(0, avaiableCharRangeForHeaderTxt) + " " + headerTxtSuffix;
                    DocumentHeader.COMP_CODE = companyCode;
                    DocumentHeader.DOC_DATE = DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                    DocumentHeader.PSTNG_DATE = DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");
                    DocumentHeader.FISC_YEAR = DateTime.ParseExact(model.SAPDocumentDate, "dd/MM/yyyy", cultureinfo).Year.ToString();
                    DocumentHeader.FIS_PERIOD = fisPeriod;
                    DocumentHeader.DOC_TYPE = docHeader.DOC_TYPE;
                    DocumentHeader.REF_DOC_NO_LONG = trip_no;
                    DocumentHeader.REASON_REV = docHeader.REASON_REV;
                    reverseDate = DateTime.ParseExact(reverseDate, "dd/MM/yyyy", cultureinfo).ToString("yyyy-MM-dd");

                        rtn.Message += "HEADER_TXT = " + DocumentHeader.HEADER_TXT + ", ";
                        rtn.Message += "DOC_DATE = " + DocumentHeader.DOC_DATE + ", ";
                        rtn.Message += "PSTNG_DATE = " + DocumentHeader.PSTNG_DATE + ", ";
                        rtn.Message += "FIS_PERIOD = " + DocumentHeader.FIS_PERIOD + "\r\n";
                    #endregion

                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        #region Crude
                        foreach (var item in model.PEV_Price.PriceSupplierList)
                        {
                            var sortFieldCu = GetSortFieldFromSupplier(item.SupplierName);
                            var configGlM = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Material");
                            var configFi = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING.Where(x => x.COMP_CODE == companyCode);
                            if (configGlM.ToList().Count() == 0) { return null; }
                            if (configEx.ToList().Count() == 0) { return null; }
                            if (configFi.ToList().Count() == 0) { return null; }
                            decimal ItemNo = Convert.ToDecimal(item.MatItemNo);
                            var formulaMet = (context.PCF_MATERIAL.Where(p => p.PMA_TRIP_NO.Equals(trip_no) && p.PMA_MET_NUM.Equals(metNum) && p.PMA_ITEM_NO.Equals(ItemNo))).ToList()[0].PMA_FORMULA;
                            running_str = running_num.ToString().PadLeft(10, '0');
                            string volumnUnit = model.PEV_Price.sCalVolumeUnit.ToUpper();
                            string volumn = "";
                            item_txt = "CRU/" + item.txtSupplierName + "/" + model.PEV_Price.sTxtVesselName + "/" + formulaMet;
                            item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;
                            if (volumnUnit == "BBL") volumn = item.VolumeBBL;
                            else if (volumnUnit == "MT") volumn = item.VolumeMT;
                            else if (volumnUnit == "L30") volumn = model.PEV_Price.QuantityList[0].QtyBL;
                            volumn = Convert.ToInt64(Convert.ToDecimal(volumn.Replace(",", ""))).ToString();

                            #region Debits
                            AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                            {
                                ITEMNO_ACC = running_str,
                                GL_ACCOUNT = configGlM.ToList()[0].GLACCOUNT,
                                ITEM_TEXT = item_txt,
                                REF_KEY_3 = sortFieldCu,
                                COMP_CODE = companyCode,
                                VENDOR_NO = item.SupplierName.PadLeft(10, '0'),
                                ALLOC_NMBR = trip_no,
                                COSTCENTER = configGlM.ToList()[0].COSTCENTER,
                                ORDERID = io
                            });

                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = running_str,
                                CURR_TYPE = "00",
                                CURRENCY = "USD",
                                AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalUSD),
                                AMT_DOCCURSpecified = true
                            });
                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = running_str,
                                CURR_TYPE = "10",
                                CURRENCY = "THB",
                                AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalTHB),
                                AMT_DOCCURSpecified = true
                            });
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //VOLUME
                            {
                                STRUCTURE = running_str,
                                VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Volume").ToList()[0].VALUEPART1,
                                VALUEPART2 = volumn
                            });
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //UOM
                            {
                                STRUCTURE = running_str,
                                VALUEPART1 = configEx.Where(c => c.DATA_FOR == "UOM").ToList()[0].VALUEPART1,
                                VALUEPART2 = volumnUnit
                            });
                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //Business Place
                            {
                                STRUCTURE = running_str,
                                VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                            });
                            running_num++;
                            #endregion

                            running_str = running_num.ToString().PadLeft(10, '0');
                            item_txt = "CRU/" + item.txtSupplierName + "/" + model.PEV_Price.sTxtVesselName + "/" + formulaMet;
                            item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;

                            #region Credit
                            AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                            {
                                ITEMNO_ACC = running_str,
                                ITEM_TEXT = item_txt,
                                REF_KEY_3 = sortFieldCu,
                                COMP_CODE = companyCode,
                                VENDOR_NO = item.SupplierName.PadLeft(10, '0'),
                                ALLOC_NMBR = trip_no
                            });

                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = running_str,
                                CURR_TYPE = "00",
                                CURRENCY = "USD",
                                AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalUSD) * -1,
                                AMT_DOCCURSpecified = true
                            });
                            CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                            {
                                ITEMNO_ACC = running_str,
                                CURR_TYPE = "10",
                                CURRENCY = "THB",
                                AMT_DOCCUR = Convert.ToDecimal(item.PriceTotalTHB) * -1,
                                AMT_DOCCURSpecified = true
                            });

                            Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //Business Place
                            {
                                STRUCTURE = running_str,
                                VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                            });

                            zfi_GL_MAPPING.Add(new com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING
                            {
                                ITEMNO_ACC = running_str,
                                TRAN_IND = configFi.ToList()[0].TRAN_IND
                            });
                            running_num++;
                            #endregion
                        }
                        #endregion

                        #region Freight
                        var configGlF = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Freight");
                        var configZfi = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Freight");
                        if (model.PEV_Price.FreightList != null)
                        {
                            foreach (var item in model.PEV_Price.FreightList)
                            {
                                //var formulaMet = (context.PCF_MATERIAL.Where(p => p.PMA_TRIP_NO.Equals(model.sTripNo) && p.PMA_MET_NUM.Equals(metNum))).ToList()[0].PMA_FORMULA;
                                var sp_vender = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.SP_VENDER.Where(x => x.VENDER_CODE == item.FRTSupplier).FirstOrDefault();
                                string[] words = item.FRTSupplierTxt.Split('('); ;
                                item_txt = sp_vender == null ? "FRI/" + model.PEV_Price.sTxtVesselName + "/" + words[0] : sp_vender.ITEM_NO_TXT + "/" + "FRI/" + model.PEV_Price.sTxtVesselName + "/" + words[0];
                                var sortFieldF = GetSortFieldFromSupplier(item.FRTSupplier);
                                running_str = running_num.ToString().PadLeft(10, '0');
                                string volumnUnit = model.PEV_Price.sCalVolumeUnit.ToUpper();
                                string volumn = "";
                                if (volumnUnit == "BBL") volumn = model.PEV_Price.QuantityList[2].QtyBL;
                                else if (volumnUnit == "MT") volumn = model.PEV_Price.QuantityList[1].QtyBL;
                                else if (volumnUnit == "L30") volumn = model.PEV_Price.QuantityList[0].QtyBL;
                                //item_txt = "FRI/" + model.sTxtVesselName + "/" +;
                                item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;
                                AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                                {
                                    ITEMNO_ACC = running_str,
                                    GL_ACCOUNT = configGlF.ToList()[0].GLACCOUNT,
                                    ITEM_TEXT = item_txt,
                                    REF_KEY_3 = sortFieldF,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = item.FRTSupplier.PadLeft(10, '0'),
                                    ALLOC_NMBR = trip_no,
                                    COSTCENTER = configGlF.ToList()[0].COSTCENTER,
                                    ORDERID = io
                                });

                                CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "00",
                                    CURRENCY = "USD",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalUSD),
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "10",
                                    CURRENCY = "THB",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalTHB),
                                    AMT_DOCCURSpecified = true
                                });
                                Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //Business Place
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                                });

                                running_num++;
                                running_str = running_num.ToString().PadLeft(10, '0');

                                AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                                {
                                    ITEMNO_ACC = running_str,
                                    ITEM_TEXT = item_txt,
                                    REF_KEY_3 = sortFieldF,
                                    COMP_CODE = companyCode,
                                    VENDOR_NO = item.FRTSupplier.PadLeft(10, '0'),
                                    ALLOC_NMBR = trip_no
                                });

                                CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "00",
                                    CURRENCY = "USD",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalUSD) * -1,
                                    AMT_DOCCURSpecified = true
                                });
                                CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                                {
                                    ITEMNO_ACC = running_str,
                                    CURR_TYPE = "10",
                                    CURRENCY = "THB",
                                    AMT_DOCCUR = Convert.ToDecimal(item.FRTTotalTHB) * -1,
                                    AMT_DOCCURSpecified = true
                                });
                                Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //Business Place
                                {
                                    STRUCTURE = running_str,
                                    VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                                });

                                zfi_GL_MAPPING.Add(new com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING
                                {
                                    ITEMNO_ACC = running_str,
                                    TRAN_IND = configZfi.ToList()[0].TRAN_IND
                                });

                                running_num++;
                            }
                        }

                        #endregion

                        #region Insurance
                        var configZIn = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ZFI_GL_MAPPING.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Insurance");
                        var configIn = accrualSettingCIP.CIP_ACCRUAL_POSTING_CONFIG.ACCOUNTGL.Where(x => x.COMP_CODE == companyCode && x.DATA_FOR == "Insurance");
                        running_str = running_num.ToString().PadLeft(10, '0');
                        var formulaMetIn = (context.PCF_MATERIAL.Where(p => p.PMA_TRIP_NO.Equals(trip_no) && p.PMA_MET_NUM.Equals(metNum))).ToList()[0].PMA_FORMULA;
                        item_txt = "INS/" + model.PEV_Price.sTxtVesselName + "/" + formulaMetIn;
                        item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;
                        AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                        {
                            ITEMNO_ACC = running_str,
                            GL_ACCOUNT = configIn.ToList()[0].GLACCOUNT,
                            ITEM_TEXT = item_txt,
                            REF_KEY_3 = configIn.ToList()[0].SUPPLIER_NAME,
                            COMP_CODE = companyCode,
                            VENDOR_NO = configIn.ToList()[0].SUPPLIER_CODE.PadLeft(10, '0'),
                            ALLOC_NMBR = trip_no,
                            COSTCENTER = configGlF.ToList()[0].COSTCENTER,
                            ORDERID = io
                        });

                        CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "00",
                            CURRENCY = "USD",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalUSD),
                            AMT_DOCCURSpecified = true
                        });
                        CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "10",
                            CURRENCY = "THB",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalTHB),
                            AMT_DOCCURSpecified = true
                        });
                        Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //Business Place
                        {
                            STRUCTURE = running_str,
                            VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                        });

                        running_num++;
                        running_str = running_num.ToString().PadLeft(10, '0');
                        item_txt = "INS/" + model.PEV_Price.sTxtVesselName + "/" + formulaMetIn;
                        item_txt = (item_txt.Length > 50) ? item_txt.Substring(0, 50) : item_txt;

                        AccountGl.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACGL09
                        {
                            ITEMNO_ACC = running_str,
                            ITEM_TEXT = item_txt,
                            REF_KEY_3 = configIn.ToList()[0].SUPPLIER_NAME,
                            COMP_CODE = companyCode,
                            VENDOR_NO = configIn.ToList()[0].SUPPLIER_CODE.PadLeft(10, '0'),
                            ALLOC_NMBR = trip_no
                        });

                        CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "00",
                            CURRENCY = "USD",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalUSD) * -1,
                            AMT_DOCCURSpecified = true
                        });
                        CurrencyAmount.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIACCR09
                        {
                            ITEMNO_ACC = running_str,
                            CURR_TYPE = "10",
                            CURRENCY = "THB",
                            AMT_DOCCUR = Convert.ToDecimal(model.PEV_Price.MRCInsTotalTHB) * -1,
                            AMT_DOCCURSpecified = true
                        });
                        Extension2.Add(new com.pttict.sap.Interface.sap.accrual.create.BAPIPAREX //Business Place
                        {
                            STRUCTURE = running_str,
                            VALUEPART1 = configEx.Where(c => c.DATA_FOR == "Business Place").ToList()[0].VALUEPART1,
                        });

                        zfi_GL_MAPPING.Add(new com.pttict.sap.Interface.sap.accrual.create.ZFI_GL_MAPPING
                        {
                            ITEMNO_ACC = running_str,
                            TRAN_IND = configZIn.ToList()[0].TRAN_IND
                        });
                        #endregion

                    }

                    AccrualService accrualService = new AccrualService();
                    sapFiDoc = accrualService.Create(jsonConfig, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING, reverseDate);
                    if (!String.IsNullOrEmpty(sapFiDoc))
                    {
                        if (sapFiDoc != "$")
                        {
                            model.PriceList[num].FiDoc = sapFiDoc;
                            rtn = SaveData(ref model, lbUserName, sapFiDoc);
                            rtn.Message = "Send To SAP Complete Fi Doc : " + sapFiDoc;
                            rtn.Status = true;
                        }
                        else
                        {
                            rtn.Message = "Fail";
                            rtn.Status = false;
                        }

                    }
                    else
                    {
                        rtn.Message = "Fail";
                        rtn.Status = false;
                    }
                }
                }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                log.Error("Error PurchaseEntryVessel LineNumber : " + LineNumber + " => " + ex.Message);
            }

            #endregion

            return rtn;
        }

        public AccrualVesselSetting GetAccrualSettings()
        {
            string JsonD = MasterData.GetJsonMasterSetting("CIP_ACCRUAL_POSTING_CONFIG");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);

            AccrualVesselSetting myDeserializedObjList = (AccrualVesselSetting)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(AccrualVesselSetting));
            return myDeserializedObjList;
        }

        public PCFAccrualSetting GetAccrualSettingsCIP()
        {
            string JsonD = MasterData.GetJsonMasterSetting("CIP_ACCRUAL_POSTING_CONFIG");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);

            PCFAccrualSetting myDeserializedObjList = (PCFAccrualSetting)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(PCFAccrualSetting));
            return myDeserializedObjList;
        }

        public string GetSortFieldFromSupplier(string accNumVendor)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_VENDOR
                                 where p.VND_ACC_NUM_VENDOR.Contains(accNumVendor)
                                 select new
                                 {
                                     dSortField = p.VND_SORT_FIELD
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dSortField;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetIOFromMetNum(string metNum, string companyCode)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_MATERIALS
                                 where p.MET_NUM == metNum
                                 select new
                                 {
                                     dIO = p.MET_IO,
                                     dIO_TLB = p.MET_IO_TLB
                                 });
                    if (query != null)
                    {
                        if (companyCode == "1100")
                            return query.ToList()[0].dIO;
                        else
                            return query.ToList()[0].dIO_TLB;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetVendorFromTripNo(string tripNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.PCF_FREIGHT
                                 where p.PFR_TRIP_NO == tripNo
                                 select new
                                 {
                                     dSupplier = p.PFR_SUPPLIER
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dSupplier;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMetMatDesEnglishFromMetNum(string metNum)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from p in context.MT_MATERIALS
                                 where p.MET_NUM == metNum
                                 select new
                                 {
                                     dMetMatDesEnglish = p.MET_MAT_DES_ENGLISH
                                 });
                    if (query != null)
                    {
                        return query.ToList()[0].dMetMatDesEnglish;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        public string GetMonthName(int month)
        {
            switch (month)
            {
                case 1: return "Jan";
                case 2: return "Feb";
                case 3: return "Mar";
                case 4: return "Apr";
                case 5: return "May";
                case 6: return "Jun";
                case 7: return "Jul";
                case 8: return "Aug";
                case 9: return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "";
            }
        }

        public bool IsFreightAvailable(string tripNo, out string currency)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from f in context.PCF_FREIGHT
                                 where f.PFR_TRIP_NO == tripNo
                                 select new
                                 {
                                     dCurrency = f.PFR_CURRENCY
                                 });
                    if (query != null && query.ToList().Count > 0)
                    {
                        currency = query.ToList()[0].dCurrency;
                        return true;
                    }
                    else
                    {
                        currency = "";
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                currency = "";
                return false;
            }
        }

        public string GetRateExchange(DateTime date,EntityCPAIEngine context)
        {
            CultureInfo provider = new CultureInfo("en-US");
            var tempFx = (from h in context.MKT_TRN_FX_H
                          join b in context.MKT_TRN_FX_B on  h.T_FXH_ID equals b.T_FXB_FXHID_FK
                          where ( h.T_FXH_BNKCODE == "BOT" &&
                          b.T_FXB_ENABLED == "T" &&
                          b.T_FXB_CUR == "USD" &&
                          b.T_FXB_VALTYPE == "M")
                          select b).OrderByDescending(p => p.T_FXB_VALDATE);

            var queryFx = from d in tempFx.AsEnumerable()
                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(date.ToString("yyyyMMdd", provider))
                          select d;
            var test = queryFx.ToList()[0];
            string ROE = queryFx.ToList()[0].T_FXB_VALUE1.ToString();
            return ROE;
        }

    }

    public class AccrualVesselSetting
    {
        public CIP_VESSEL_ACCRUAL_POSTING_CONFIG CIP_VESSEL_ACCRUAL_POSTING_CONFIG { get; set; }
    }

    public class CIP_VESSEL_ACCRUAL_POSTING_CONFIG
    {
        public List<DOCUMENTHEADER> DOCUMENTHEADER { get; set; }
        public List<ACCOUNTGL> ACCOUNTGL { get; set; }
        public List<EXTENSION2> EXTENSION2 { get; set; }
        public List<ZFI_GL_MAPPING2> ZFI_GL_MAPPING { get; set; }
    }
}
