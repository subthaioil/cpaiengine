﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class PurchaseOrderServiceModel
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //This function is not complete yet, due to we don't allow user to add PO directly
        public ReturnValue Add(ref PurchaseOrderViewModel_Detail purchaseOrderViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                #region Check the valid fields

                if (purchaseOrderViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.PoDate))
                {
                    rtn.Message = "Purchase Order Date must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.CompanyCode))
                {
                    rtn.Message = "Company Code must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.PurchasingGroup))
                {
                    rtn.Message = "Purchasing Group must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.Currency))
                {
                    rtn.Message = "Currency must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.FXBot))
                {
                    rtn.Message = "FX Bot must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.Incoterms))
                {
                    rtn.Message = "INCO Terms must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.OriginCountry))
                {
                    rtn.Message = "Origin Country must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.CargoNo))
                {
                    rtn.Message = "Cargo No. must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.SoNo))
                {
                    rtn.Message = "SO NO. must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.CreateDate))
                {
                    rtn.Message = "Created Date must not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(purchaseOrderViewDetailModel.CreateBy))
                {
                    rtn.Message = "Created By must not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                #endregion

                PIT_PO_HEADER_DAL dal = new PIT_PO_HEADER_DAL();
                PIT_PO_HEADER ent = new PIT_PO_HEADER();

                PIT_PO_ITEM_DAL dalItem = new PIT_PO_ITEM_DAL();
                PIT_PO_ITEM entItem = new PIT_PO_ITEM();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.PO_NO = purchaseOrderViewDetailModel.PoNo;
                            ent.COMPANY_CODE = purchaseOrderViewDetailModel.CompanyCode;
                            ent.ACC_NUM_VENDOR = purchaseOrderViewDetailModel.AccNumVendor;
                            ent.PURCHASING_GROUP = purchaseOrderViewDetailModel.PurchasingGroup;
                            ent.CURRENCY = purchaseOrderViewDetailModel.Currency;
                            ent.FX_BOT = Convert.ToDecimal(purchaseOrderViewDetailModel.FXBot);
                            ent.PO_DATE = Convert.ToDateTime(purchaseOrderViewDetailModel.PoDate);
                            ent.INCOTERMS = purchaseOrderViewDetailModel.Incoterms;
                            ent.ORIGIN_COUNTRY = purchaseOrderViewDetailModel.OriginCountry;
                            ent.CARGO_NO = purchaseOrderViewDetailModel.CargoNo;
                            ent.SO_NO = purchaseOrderViewDetailModel.SoNo;
                            ent.CREATED_DATE = now;
                            ent.CREATED_BY = pUser;
                            ent.UPDATED_DATE = now;
                            ent.UPDATED_BY = pUser;

                            //dal.Save(ent, context);

                            foreach (var item in purchaseOrderViewDetailModel.Control)
                            {
                                entItem = new PIT_PO_ITEM();

                                //entItem.PO_ITEM = Convert.ToDecimal(poItem);
                                entItem.PO_NO = purchaseOrderViewDetailModel.PoNo;
                                entItem.SHORT_TEXT = item.ShortText;
                                entItem.MET_NUM = item.MetNum;
                                entItem.PLANT = item.Plant;
                                entItem.TRIP_NO = item.TripNo;
                                entItem.VOLUME = Convert.ToDecimal(item.Volume);
                                entItem.VOLUME_UNIT = item.VolumeUnit;
                                entItem.UNIT_PRICE = Convert.ToDecimal(item.UnitPrice);
                                entItem.CURRENCY = item.Currency;
                                entItem.ACC_NUM_VENDOR = item.Vendor;
                                entItem.TRAN_ID = item.TranId;
                                entItem.CARGO_NO = item.CargoNo;
                                entItem.SO_USERNAME = item.SOUserName;
                                entItem.UPDATED_DATE = now;
                                entItem.UPDATED_BY = pUser;

                                dalItem.Save(entItem, context);
                            }
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(PurchaseOrderViewModel_Detail purchaseOrderViewDetailModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                #region Check the valid fields

                if (purchaseOrderViewDetailModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }

                #endregion

                PIT_PO_HEADER_DAL dal = new PIT_PO_HEADER_DAL();
                PIT_PO_HEADER ent = new PIT_PO_HEADER();

                PIT_PO_ITEM_DAL dalItem = new PIT_PO_ITEM_DAL();
                PIT_PO_ITEM entItem = new PIT_PO_ITEM();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            // Update data in PIT_PO_HEADER
                            ent.PO_NO = purchaseOrderViewDetailModel.PoNo;

                            #region Comment area reserved for add new PO (if possible)

                            //ent.COMPANY_CODE = purchaseOrderViewDetailModel.CompanyCode;
                            //ent.ACC_NUM_VENDOR = purchaseOrderViewDetailModel.AccNumVendor;
                            //ent.PURCHASING_GROUP = purchaseOrderViewDetailModel.PurchasingGroup;
                            //ent.CURRENCY = purchaseOrderViewDetailModel.Currency;
                            //ent.FX_BOT = Convert.ToDecimal(purchaseOrderViewDetailModel.FXBot);
                            //ent.PO_DATE = Convert.ToDateTime(purchaseOrderViewDetailModel.PoDate);
                            //ent.INCOTERMS = purchaseOrderViewDetailModel.Incoterms;
                            //ent.ORIGIN_COUNTRY = purchaseOrderViewDetailModel.OriginCountry;
                            //ent.CARGO_NO = purchaseOrderViewDetailModel.CargoNo;
                            //ent.SO_NO = purchaseOrderViewDetailModel.SoNo;

                            #endregion
                           
                            ent.UPDATED_DATE = now;
                            ent.UPDATED_BY = pUser;

                            dal.Update(ent, context);
                            
                            foreach (var item in purchaseOrderViewDetailModel.Control)
                            {
                                if(string.IsNullOrEmpty(item.PoItem))
                                {
                                    #region Comment area reserved for and new item (ifpossible)

                                    //Add new
                                    //entItem = new PIT_PO_ITEM();
                                    //var poItem = Convert.ToInt32(purchaseOrderViewDetailModel.Control.Max(x => x.PoItem)) + 1;

                                    //entItem.PO_ITEM = Convert.ToDecimal(poItem);
                                    //entItem.FK_PO_NO = purchaseOrderViewDetailModel.PoNo;
                                    //entItem.SHORT_TEXT = item.ShortText;
                                    //entItem.MET_NUM = item.MetNum;
                                    //entItem.PLANT = item.Plant;
                                    //entItem.STORAGE_LOCATION = item.StorageLocation;
                                    //entItem.TRIP_NO = item.TripNo;
                                    //entItem.VOLUME = Convert.ToDecimal(item.Volume);
                                    //entItem.VOLUME_UNIT = item.VolumeUnit;
                                    //entItem.UNIT_PRICE = Convert.ToDecimal(item.UnitPrice);
                                    //entItem.CURRENCY = item.Currency;
                                    //entItem.ACC_NUM_VENDOR = item.Vendor;
                                    //entItem.TRAN_ID = item.TranId;
                                    //entItem.CARGO_NO = item.CargoNo;
                                    //entItem.SO_USERNAME = item.SOUserName;

                                    //dalItem.Save(entItem, context);

                                    #endregion
                                }
                                else
                                {
                                    // Update data in PIT_PO_ITEM
                                    entItem = new PIT_PO_ITEM();

                                    #region Comment area reserved for update other field for item (if possible)

                                    //entItem.SHORT_TEXT = item.ShortText;
                                    //entItem.MET_NUM = item.MetNum;
                                    //entItem.PLANT = item.Plant;
                                    //entItem.STORAGE_LOCATION = item.StorageLocation;
                                    //entItem.TRIP_NO = item.TripNo;
                                    //entItem.ACC_NUM_VENDOR = item.Vendor;
                                    //entItem.TRAN_ID = item.TranId;
                                    //entItem.CARGO_NO = item.CargoNo;
                                    //entItem.SO_USERNAME = item.SOUserName;

                                    #endregion

                                    entItem.PO_ITEM = Convert.ToDecimal(item.PoItem);
                                    entItem.PO_NO = item.FKPoNo;
                                    entItem.VOLUME = Convert.ToDecimal(item.Volume);
                                    entItem.VOLUME_UNIT = item.VolumeUnit;
                                    entItem.UNIT_PRICE = Convert.ToDecimal(item.UnitPrice);
                                    entItem.CURRENCY = item.Currency;
                                    entItem.UPDATED_DATE = now;
                                    entItem.UPDATED_BY = pUser;

                                    dalItem.Update(entItem, context);
                                }
                            }



                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }
        
        public ReturnValue Search(ref PurchaseOrderViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sPoNo = String.IsNullOrEmpty(pModel.sPoNo) ? "" : pModel.sPoNo.ToUpper();
                    var sSoNo = String.IsNullOrEmpty(pModel.sSoNo) ? "" : pModel.sSoNo.ToUpper();
                    var sFeedStock = String.IsNullOrEmpty(pModel.sFeedStock) ? "" : pModel.sFeedStock.ToUpper();
                    var sSupplier = String.IsNullOrEmpty(pModel.sSupplier) ? "" : pModel.sSupplier.ToUpper();
                    var sCompany = String.IsNullOrEmpty(pModel.sCompany) ? "" : pModel.sCompany.ToUpper();
                    var sPoDate = String.IsNullOrEmpty(pModel.sPoDate) ? "" : pModel.sPoDate;
                    
                    var query = (from r in context.PIT_PO_HEADER
                                 where r.IS_DELETE != "TRUE" && (r.COMPANY_CODE == "1100" || r.COMPANY_CODE == "1400")
                                 join i in context.PIT_PO_ITEM on r.PO_NO equals i.PO_NO into viewD
                                 from vdl in viewD.DefaultIfEmpty()
                                 join c in context.MT_MATERIALS on vdl.MET_NUM equals c.MET_NUM into viewCom
                                 from vdcl in viewCom.DefaultIfEmpty()                                 
                                 join co in context.MT_COMPANY on r.COMPANY_CODE equals co.MCO_COMPANY_CODE into viewCompany
                                 from comp in viewCompany.DefaultIfEmpty()
                                 select new 
                                 {
                                     dPoNo = r.PO_NO,
                                     dTripNo = vdl.TRIP_NO,
                                     dFeedstock = vdcl.MET_MAT_DES_ENGLISH,
                                     dFeedstockNo = vdcl.MET_NUM,
                                     dSupplierNo = r.ACC_NUM_VENDOR,
                                     dIncoterms = r.INCOTERMS,  
                                     dVolume = vdl.VOLUME,
                                     dVolumeUnit = vdl.VOLUME_UNIT,
                                     dPrice = vdl.UNIT_PRICE,
                                     dTotalAmount = ((vdl.VOLUME??0) * (vdl.UNIT_PRICE??0)),
                                     dCurrency = vdl.CURRENCY,
                                     dCreatedDate = (r.CREATED_DATE),
                                     dCreatedBy = r.CREATED_BY,
                                     dLastUpdated = (r.UPDATED_DATE),
                                     dUpdatedBy = r.UPDATED_BY,
                                     dCompany = comp.MCO_COMPANY_CODE,
                                     dCompanyName = comp.MCO_SHORT_NAME,
                                     dPoDate = r.PO_DATE,
                                     dSoNo = r.SO_NO
                                 });

                    var queryVendor = (from v in context.MT_VENDOR
                                       select v);
                    Vendors vendors = new Vendors();
                    vendors.Vendor = new List<ViewModels.Vendor>();
                    if(queryVendor != null)
                    {
                        foreach(var vendor in queryVendor)
                        {
                            vendors.Vendor.Add(new ViewModels.Vendor
                            {
                                VendorNo = vendor.VND_ACC_NUM_VENDOR,
                                VendorName = vendor.VND_NAME1
                            });
                        }
                    }

                    var companyGlobalConfig = PurchaseOrderServiceModel.GetSetting("FORMULA_PRICING_TYPE").MT_COMPANY;
                    List<string> companyCodes = new List<string>();
                    foreach(var company in companyGlobalConfig)
                    {
                        companyCodes.Add(company.CODE);
                    }
                    query = query.Where(x => companyCodes.Contains(x.dCompany.Trim()));

                    #region Filter Data

                    if (!string.IsNullOrEmpty(sPoNo))
                    {
                        query = query.Where(p => p.dPoNo.Contains(sPoNo.Trim()));
                    }

                    if (!string.IsNullOrEmpty(sSoNo))
                    {
                        query = query.Where(p => p.dSoNo.Contains(sSoNo.Trim()));
                    }

                    if (!string.IsNullOrEmpty(sFeedStock))
                    {
                        query = query.Where(p => p.dFeedstockNo.ToLower().Contains(sFeedStock.Trim().ToLower()));
                    }

                    if (!string.IsNullOrEmpty(sSupplier))
                    {
                        query = query.Where(p => p.dSupplierNo.ToLower().Contains(sSupplier.Trim().ToLower()));
                    }

                    if (!string.IsNullOrEmpty(sCompany))
                    {
                        query = query.Where(p => p.dCompany.Contains(sCompany.Trim()));
                    }

                    if (!string.IsNullOrEmpty(sPoDate))
                    {                        
                        sPoDate = sPoDate.Replace(" to ", "|");
                        //var fromDate = Convert.ToDateTime(sPoDate.Split('|')[0]);
                        var fromDate = DateTime.ParseExact(sPoDate.Split('|')[0], "dd/MM/yyyy", cultureinfo);
                        //var toDate = Convert.ToDateTime(sPoDate.Split('|')[1]);
                        var toDate = DateTime.ParseExact(sPoDate.Split('|')[1], "dd/MM/yyyy", cultureinfo);

                        if (fromDate != null && toDate != null)
                        {
                            query = query.Where(p => fromDate <= p.dPoDate && toDate >= p.dPoDate);
                            //query = query.Where(p => fromDate <= DateTime.ParseExact(p.dPoDate.ToString(), "dd/MM/yyyy", cultureinfo) && toDate >= DateTime.ParseExact(p.dPoDate.ToString(), "dd/MM/yyyy", cultureinfo));
                        }                        
                    }

                    #endregion

                    if (query != null)
                    {
                        pModel.sSearchData = new List<PurchaseOrderViewModel_SearchData>();
                        int runningNo = 1;
                        foreach (var item in query)
                        {
                            string supplierName = "";
                            foreach(var vend in vendors.Vendor)
                            {
                                try
                                {
                                    if (Convert.ToInt32(item.dSupplierNo) == Convert.ToInt32(vend.VendorNo))
                                    {
                                        supplierName = vend.VendorName;
                                        break;
                                    }
                                }
                                catch { }
                            }

                            pModel.sSearchData.Add(new PurchaseOrderViewModel_SearchData
                            {
                                dRunningNo = runningNo.ToString(),
                                dCompanyName = string.IsNullOrEmpty(item.dCompanyName) ? "" : item.dCompanyName,
                                dTripNo = string.IsNullOrEmpty(item.dTripNo) ? "" : item.dTripNo,
                                dFeedstock = string.IsNullOrEmpty(item.dFeedstock) ? "" : item.dFeedstock,
                                dSupplier = supplierName,
                                dIncoterms = string.IsNullOrEmpty(item.dIncoterms) ? "" : item.dIncoterms,
                                dVolume = item.dVolume.ToString(),  
                                dVolumeUnit = string.IsNullOrEmpty(item.dVolumeUnit) ? "" : item.dVolumeUnit,
                                dPrice = item.dPrice.ToString(),
                                dTotalAmount = item.dTotalAmount.ToString(),
                                dCurrency = string.IsNullOrEmpty(item.dCurrency) ? "" : item.dCurrency,
                                dSapPONO = string.IsNullOrEmpty(item.dPoNo) ? "" : item.dPoNo,
                                dLastUpdated = item.dLastUpdated.ToString("dd/MM/yyyy", cultureinfo),
                                dUpdatedBy = string.IsNullOrEmpty(item.dUpdatedBy) ? "" : item.dUpdatedBy,
                                dPoDate = item.dPoDate.Value.ToString("dd/MM/yyyy", cultureinfo),
                                dSoNo = string.IsNullOrEmpty(item.dSoNo) ? "" : item.dSoNo                                
                            });

                            runningNo++;
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {                
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public void MarkForDeletion(string poNo)
        {
            PIT_PO_HEADER_DAL dal = new PIT_PO_HEADER_DAL();
            PIT_PO_HEADER ent = new PIT_PO_HEADER();

            using (var context = new EntityCPAIEngine())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    ent.PO_NO = poNo;
                    ent.IS_DELETE = "TRUE";
                    ent.UPDATED_DATE = DateTime.Now;
                    ent.UPDATED_BY = Const.User.Name;
                    dal.Update(ent, context);
                    dbContextTransaction.Commit();
                }
            }
        }

        public List<SelectListItem> GetFeedStockPIT()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            using (var context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_MATERIALS_CONTROL
                             where v.MMC_SYSTEM.Equals("PIT")
                             join vd in context.MT_MATERIALS on v.MMC_FK_MATRIALS equals vd.MET_NUM into feedStock
                             from fs in feedStock.DefaultIfEmpty()
                             select new
                             {
                                 dFeedStockNo = fs.MET_NUM,
                                 dFeedStock = fs.MET_MAT_DES_ENGLISH
                             }).OrderBy(p=>p.dFeedStock);
                if (query != null)
                {
                    foreach (var f in query)
                    {
                        list.Add(new SelectListItem { Text = f.dFeedStock, Value = f.dFeedStockNo });
                    }
                }
            }

            return list;
        }

        public List<SelectListItem> GetFeedStockPCF()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            using (var context = new EntityCPAIEngine())
            {
                var query = (from v in context.PCF_MATERIAL 
                             where v.PMA_MET_NUM.StartsWith("Y")
                             join vd in context.MT_MATERIALS on v.PMA_MET_NUM equals vd.MET_NUM into feedStock
                             from fs in feedStock.DefaultIfEmpty()
                             select new
                             {
                                 dFeedStockNo = fs.MET_NUM,
                                 dFeedStock = fs.MET_MAT_DES_ENGLISH
                             });
                if (query != null)
                {
                    foreach (var f in query.Distinct())
                    {
                        list.Add(new SelectListItem { Text = f.dFeedStock, Value = f.dFeedStockNo });
                    }
                }
            }

            return list;
        }

        public PurchaseOrderViewModel_Detail Get(string poNo)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            PurchaseOrderViewModel_Detail model = new PurchaseOrderViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(poNo) == false)
                {
                    string accNumVendor = GetAccNumVendorWithoutZeroPrefix(poNo);
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.PIT_PO_HEADER
                                     where v.PO_NO.Equals(poNo)
                                     join vd in context.MT_VENDOR on accNumVendor equals vd.VND_ACC_NUM_VENDOR into vendor
                                     from vnd in vendor.DefaultIfEmpty()
                                     join vc in context.MT_COMPANY on v.COMPANY_CODE equals vc.MCO_COMPANY_CODE into company
                                     from vcc in company.DefaultIfEmpty()
                                     select new
                                     {
                                         dPoNo = v.PO_NO,
                                         dCompanyCode = v.COMPANY_CODE,     
                                         dCompanyName = vcc.MCO_SHORT_NAME,                                   
                                         dAccVendor = vnd.VND_NAME1,
                                         dAccNumVerndor = v.ACC_NUM_VENDOR,
                                         dPurchasingGroup = v.PURCHASING_GROUP,
                                         dCurrency = v.CURRENCY,
                                         dFxBot = v.FX_BOT,
                                         dPoDate = v.PO_DATE,
                                         dIncoTerms = v.INCOTERMS,
                                         dOriginCountry = v.ORIGIN_COUNTRY,
                                         dCargoNo = v.CARGO_NO,
                                         dSoNo = v.SO_NO,
                                         dCreatedDate = v.CREATED_DATE,
                                         dCreatedBy = v.CREATED_BY
                                         
                                     });

                        if (query != null)
                        {
                            foreach (var g in query)
                            {
                                model.PoNo = g.dPoNo;
                                model.CompanyCode = g.dCompanyCode;
                                model.CompanyName = g.dCompanyName;
                                model.AccVendor = g.dAccVendor;
                                model.AccNumVendor = g.dAccNumVerndor;
                                model.PurchasingGroup = g.dPurchasingGroup;
                                model.Currency = g.dCurrency;
                                model.FXBot = g.dFxBot.ToString();
                                model.PoDate = g.dPoDate.ToString();
                                model.Incoterms = g.dIncoTerms;
                                model.OriginCountry = g.dOriginCountry;
                                model.CargoNo = g.dCargoNo;
                                model.SoNo = g.dSoNo;
                                model.CreateDate = g.dCreatedDate.ToString("dd/MM/yyyy", cultureinfo);
                                model.CreateBy = g.dCreatedBy;
                                
                            }
                        }
                        
                        model.Control = new List<PurchaseOrderViewModel_Control>();                        
                        var queryItem = (from v in context.PIT_PO_ITEM
                                         where v.PO_NO.Equals(poNo)
                                         join p in context.MT_PLANT on v.PLANT equals p.MPL_PLANT into plant
                                         from plnt in plant.DefaultIfEmpty()
                                         join vd in context.MT_VENDOR on accNumVendor equals vd.VND_ACC_NUM_VENDOR into vendor
                                         from vnd in vendor.DefaultIfEmpty()
                                         join vm in context.MT_MATERIALS on v.MET_NUM equals vm.MET_NUM into material
                                         from vnm in material.DefaultIfEmpty()
                                         select new
                                         {
                                             dPoItem = v.PO_ITEM,
                                             dFkPoNo = v.PO_NO,
                                             dShortText = v.SHORT_TEXT,
                                             dMetNum = vnm.MET_MAT_DES_ENGLISH,
                                             dPlant = plnt.MPL_PLANT,
                                             dTripNo = v.TRIP_NO,
                                             dVolumn = v.VOLUME,
                                             dVolumnUnit = v.VOLUME_UNIT,
                                             dUnitPrice = v.UNIT_PRICE,
                                             dCurrency = v.CURRENCY,
                                             dAccVendor = vnd.VND_NAME1,
                                             dAccNumVendor = v.ACC_NUM_VENDOR,
                                             dTranId = v.TRAN_ID,
                                             dCargoNo = v.CARGO_NO,
                                             dSoUserName = v.SO_USERNAME,
                                             dTotalAmount = ((v.VOLUME ?? 0) * (v.UNIT_PRICE ?? 0))
                                         });
                        if(queryItem != null)
                        {
                            foreach(var g in queryItem)
                            {                                
                                PurchaseOrderViewModel_Control control = new PurchaseOrderViewModel_Control()
                                {
                                    PoItem = g.dPoItem.ToString(),
                                    FKPoNo = g.dFkPoNo,
                                    ShortText = g.dShortText,
                                    MetNum = g.dMetNum,
                                    Plant = g.dPlant,
                                    //StorageLocation = (storageLocation.Count() == 0) ? "" : storageLocation[0].NAME,
                                    //StorageLocationNo = (storageLocation.Count() == 0) ? "" :storageLocation[0].CODE,
                                    TripNo = g.dTripNo,
                                    Volume = g.dVolumn.ToString(),
                                    VolumeUnit = g.dVolumnUnit,
                                    UnitPrice = g.dUnitPrice.ToString(),
                                    Currency = g.dCurrency,
                                    Vendor = g.dAccVendor,
                                    VendorNo = g.dAccNumVendor,
                                    TranId = g.dTranId,
                                    CargoNo = g.dCargoNo,
                                    SOUserName = g.dSoUserName,
                                    TotalAmount = g.dTotalAmount.ToString()
                                };

                                model.Control.Add(control);
                                
                            }
                        }

                    }
                    

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetAccNumVendorWithoutZeroPrefix(string poNo)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.PIT_PO_HEADER
                             where v.PO_NO.Equals(poNo)
                             select new
                             {
                                 dAccNumVerndor = v.ACC_NUM_VENDOR
                             });
                if(query == null)
                {
                    return null;
                }
                string accNumVendor = Convert.ToInt32(query.ToList()[0].dAccNumVerndor).ToString();
                return accNumVendor;
            }            
        }

        public static CIP_Config GetSetting(string TypeConfig)
        {
            string JsonD = MasterData.GetJsonMasterSetting(TypeConfig);
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);

            CIP_Config myDeserializedObjList = (CIP_Config)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(CIP_Config));
            return myDeserializedObjList;
        }
    }
}