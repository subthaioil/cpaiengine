﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using ProjectCPAIEngine.DAL.DALPCF;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class ReverseVatFIDocumentServiceModel
    {
        public CustomVatViewModel SearchCustomVat(string document)
        {
            CustomVatViewModel result = new CustomVatViewModel();            
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";
            int no;

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {               
                var query = (from h in context.PCF_HEADER
                             join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO
                             join cv in context.PCF_CUSTOMS_VAT on new {ColA = m.PMA_TRIP_NO, ColB= m.PMA_ITEM_NO} equals new {ColA = cv.TRIP_NO, ColB = cv.MAT_ITEM_NO } into cv1
                             from cv in cv1.DefaultIfEmpty()
                             join ve in context.MT_VEHICLE on h.PHE_VESSEL equals ve.VEH_ID                        
                             join s in context.MT_VENDOR on m.PMA_SUPPLIER equals s.VND_ACC_NUM_VENDOR                            
                             join ma in context.MT_MATERIALS on m.PMA_MET_NUM equals ma.MET_NUM
                             where (cv.SAP_FI_DOC.ToUpper().Contains(document.ToUpper()) )//|| String.IsNullOrEmpty(document))                             
                             select new
                             {
                                 dTripNo = h.PHE_TRIP_NO,
                                 dVessel = ve.VEH_VEH_TEXT,
                                 dCrude = ma.MET_MAT_DES_ENGLISH,
                                 dSupplier = s.VND_NAME1,
                                 dBLDate = m.PMA_BL_DATE,
                                 dDueDate = m.PMA_DUE_DATE,
                                 dQtyBBL = m.PMA_VOLUME_BBL,
                                 dQtyMT = m.PMA_VOLUME_MT,
                                 dQtyML = m.PMA_VOLUME_ML,
                                 dIncoTerms = m.PMA_INCOTERMS,
                                 dPONo = m.PMA_PO_NO,
                                 dFIDocVAT = cv.SAP_FI_DOC,
                                 dInvoiceDocMIRO = cv.SAP_FI_DOC_INVOICE,
                                 dFIDocMIRO = cv.SAP_INVOICE_DOC,
                                 dCustomPrice = cv.CUSTOMS_PRICE,
                                 dFreightAmount = cv.FREIGHT_AMOUNT,
                                 dMatItemNo = m.PMA_ITEM_NO,
                                 dMetNum = m.PMA_MET_NUM,
                                 dVendorNo = s.VND_ACC_NUM_VENDOR,
                                 dVesselNo = ve.VEH_VEHICLE,
                                 dCompanyCode = h.PHE_COMPANY_CODE,
                                 dROE = cv.ROE,
                                 dFOB = cv.FOB,
                                 dFRT = cv.FRT,
                                 dCFR = cv.CFR,
                                 dINS = cv.INS,
                                 dCIF = cv.CIF_SUM_INS,
                                 dPremium = cv.PREMIUM,
                                 dTotalUSD100 = cv.TOTAL_USD_100,
                                 dBaht100 = cv.BAHT_100,
                                 dBaht105 = cv.BAHT_105,
                                 dVat = cv.VAT,
                                 dTaxBase = cv.TAX_BASE,
                                 dCorrectVAT = cv.CORRECT_VAT,
                                 dImportDuty = cv.IMPORT_DUTY,
                                 dExciseTax = cv.EXCISE_TAX,
                                 dMunicipalTax = cv.MUNICIPAL_TAX,
                                 dOilFuelFund = cv.OIL_FUEL_FUND,
                                 dDepositOfImportDuty = cv.DEPOSIT_OF_IMPORT_DUTY,
                                 dDepositOfExciseTax = cv.DEPOSIT_OF_EXCISE_TAX,
                                 dDepositOfMunicipalTax = cv.DEPOSIT_OF_MUNICIPAL_TAX,
                                 dEnergyOilFuelFund = cv.ENERY_OIL_FUEL_FUND
                             });
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (query != null)
                {
                    no = 0;
                    result.CustomVat_Calc_Total = new CustomVatViewModel_Calc_Total();                 
                    result.CustomVat_Calc = new List<CustomVatViewModel_Calc>();
                    foreach (var i in query)
                    {
                        no++;
                        result.sTripNo = i.dTripNo;
                        result.sMatItemNo = string.IsNullOrEmpty(i.dMatItemNo.ToString()) ? "0" : Double.Parse(i.dMatItemNo.ToString()).ToString("#,##0.0000");
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        result.CustomVat_Calc_Total.totalQtyBBL += (decimal?)i.dQtyBBL ?? 0;
                        result.CustomVat_Calc_Total.totalQtyMT += (decimal?)i.dQtyMT ?? 0;
                        result.CustomVat_Calc_Total.totalQtyML += (decimal?)i.dQtyML ?? 0;
                        result.CustomVat_Calc_Total.totalFOB += (decimal?)i.dFOB ?? 0;
                        result.CustomVat_Calc_Total.totalFRT += (decimal?)i.dFRT ?? 0;
                        result.CustomVat_Calc_Total.totalCFR += (decimal?)i.dCFR ?? 0;
                        result.CustomVat_Calc_Total.totalINS += (decimal?)i.dINS ?? 0;
                        result.CustomVat_Calc_Total.totalCIF += (decimal?)i.dCIF ?? 0;
                        result.CustomVat_Calc_Total.totalPremium += (decimal?)i.dPremium ?? 0;
                        result.CustomVat_Calc_Total.totalTotalUSD100 += (decimal?)i.dTotalUSD100 ?? 0;
                        result.CustomVat_Calc_Total.totalBaht100 += (decimal?)i.dBaht100 ?? 0;
                        result.CustomVat_Calc_Total.totalBaht105 += (decimal?)i.dBaht105 ?? 0;
                        result.CustomVat_Calc_Total.totalVAT += (decimal?)i.dVat ?? 0;
                        result.CustomVat_Calc_Total.totalTaxBase += (decimal?)i.dTaxBase ?? 0;
                        result.CustomVat_Calc_Total.totalCorrectVAT += (decimal?)i.dCorrectVAT ?? 0;
                        result.CustomVat_Calc_Total.totalImportDuty += (decimal?)i.dImportDuty ?? 0;
                        result.CustomVat_Calc_Total.totalExciseTax += (decimal?)i.dExciseTax ?? 0;
                        result.CustomVat_Calc_Total.totalMunicipalTax += (decimal?)i.dMunicipalTax ?? 0;
                        result.CustomVat_Calc_Total.totalOilFuelFund += (decimal?)i.dOilFuelFund ?? 0;
                        result.CustomVat_Calc_Total.totalDepositOfImportDuty += (decimal?)i.dDepositOfImportDuty ?? 0;
                        result.CustomVat_Calc_Total.totalDepositOfExciseTax += (decimal?)i.dDepositOfExciseTax ?? 0;
                        result.CustomVat_Calc_Total.totalDepositOfMunicipalTax += (decimal?)i.dDepositOfMunicipalTax ?? 0;
                        result.CustomVat_Calc_Total.totalEnergyOilFuelFund += (decimal?)i.dEnergyOilFuelFund ?? 0;
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        result.CustomVat_Calc.Add(new CustomVatViewModel_Calc
                        {
                            No = no.ToString(),
                            PoNo = string.IsNullOrEmpty(i.dPONo) ? string.Empty : i.dPONo,
                            CrudeSupplier = i.dCrude + " " + i.dSupplier,
                            ROE = string.IsNullOrEmpty(i.dROE.ToString()) ? "0" : Double.Parse(i.dROE.ToString()).ToString("#,##0.0000"),
                            QtyBBL = string.IsNullOrEmpty(i.dQtyBBL.ToString()) ? "0" : Double.Parse(i.dQtyBBL.ToString()).ToString("#,##0.0000"),
                            QtyMT = string.IsNullOrEmpty(i.dQtyMT.ToString()) ? "0" : Double.Parse(i.dQtyMT.ToString()).ToString("#,##0.0000"),
                            QtyML = string.IsNullOrEmpty(i.dQtyML.ToString()) ? "0" : Double.Parse(i.dQtyML.ToString()).ToString("#,##0.0000"),
                            CustomsPrice = string.IsNullOrEmpty(i.dCustomPrice.ToString()) ? "0" : Double.Parse(i.dCustomPrice.ToString()).ToString("#,##0.0000"),
                            FOB = string.IsNullOrEmpty(i.dFOB.ToString()) ? "0" : Double.Parse(i.dFOB.ToString()).ToString("#,##0.0000"),
                            FRT = string.IsNullOrEmpty(i.dFRT.ToString()) ? "0" : Double.Parse(i.dFRT.ToString()).ToString("#,##0.0000"),
                            CFR = string.IsNullOrEmpty(i.dCFR.ToString()) ? "0" : Double.Parse(i.dCFR.ToString()).ToString("#,##0.0000"),
                            INS = string.IsNullOrEmpty(i.dINS.ToString()) ? "0" : Double.Parse(i.dINS.ToString()).ToString("#,##0.0000"),
                            CIF = string.IsNullOrEmpty(i.dCIF.ToString()) ? "0" : Double.Parse(i.dCIF.ToString()).ToString("#,##0.0000"),
                            Premium = string.IsNullOrEmpty(i.dPremium.ToString()) ? "0" : Double.Parse(i.dPremium.ToString()).ToString("#,##0.0000"),
                            TotalUSD100 = string.IsNullOrEmpty(i.dTotalUSD100.ToString()) ? "0" : Double.Parse(i.dTotalUSD100.ToString()).ToString("#,##0.0000"),
                            Baht100 = string.IsNullOrEmpty(i.dBaht100.ToString()) ? "0" : Double.Parse(i.dBaht100.ToString()).ToString("#,##0.0000"),
                            Baht105 = string.IsNullOrEmpty(i.dBaht105.ToString()) ? "0" : Double.Parse(i.dBaht105.ToString()).ToString("#,##0.0000"),
                            VAT = string.IsNullOrEmpty(i.dVat.ToString()) ? "0" : Double.Parse(i.dVat.ToString()).ToString("#,##0.0000"),
                            TaxBase = string.IsNullOrEmpty(i.dTaxBase.ToString()) ? "0" : Double.Parse(i.dTaxBase.ToString()).ToString("#,##0.0000"),
                            CorrectVAT = string.IsNullOrEmpty(i.dCorrectVAT.ToString()) ? "0" : Double.Parse(i.dCorrectVAT.ToString()).ToString("#,##0.0000"),
                            ImportDuty = string.IsNullOrEmpty(i.dImportDuty.ToString()) ? "0" : Double.Parse(i.dImportDuty.ToString()).ToString("#,##0.0000"),
                            ExciseTax = string.IsNullOrEmpty(i.dExciseTax.ToString()) ? "0" : Double.Parse(i.dExciseTax.ToString()).ToString("#,##0.0000"),
                            MunicipalTax = string.IsNullOrEmpty(i.dMunicipalTax.ToString()) ? "0" : Double.Parse(i.dMunicipalTax.ToString()).ToString("#,##0.0000"),
                            OilFuelFund = string.IsNullOrEmpty(i.dOilFuelFund.ToString()) ? "0" : Double.Parse(i.dOilFuelFund.ToString()).ToString("#,##0.0000"),
                            DepositOfImportDuty = string.IsNullOrEmpty(i.dDepositOfImportDuty.ToString()) ? "0" : Double.Parse(i.dDepositOfImportDuty.ToString()).ToString("#,##0.0000"),
                            DepositOfExciseTax = string.IsNullOrEmpty(i.dDepositOfExciseTax.ToString()) ? "0" : Double.Parse(i.dDepositOfExciseTax.ToString()).ToString("#,##0.0000"),
                            DepositOfMunicipalTax = string.IsNullOrEmpty(i.dDepositOfMunicipalTax.ToString()) ? "0" : Double.Parse(i.dDepositOfMunicipalTax.ToString()).ToString("#,##0.0000"),
                            EnergyOilFuelFund = string.IsNullOrEmpty(i.dEnergyOilFuelFund.ToString()) ? "0" : Double.Parse(i.dEnergyOilFuelFund.ToString()).ToString("#,##0.0000")                            
                        });
                    }
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalCorrectVAT;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalImportDuty;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalExciseTax;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalMunicipalTax;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalOilFuelFund;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalDepositOfImportDuty;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalDepositOfExciseTax;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalDepositOfMunicipalTax;
                    result.CustomVat_Calc_Total.totalAll += result.CustomVat_Calc_Total.totalEnergyOilFuelFund;
                }
            }

            return result;
        }

        public void UpdateTables(CustomVatViewModel data, string sapFIDoc, string sapReverseDocNo, ReverseFIDocumentResult result)
        {
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                string userName = Const.User.Name;                
                PCF_VAT_REVERSED_FI_DOC entReversed = new PCF_VAT_REVERSED_FI_DOC();
                
                entReversed.COMPANY_CODE = result.CompanyCode;
                entReversed.TRIP_NO = data.sTripNo;                
                entReversed.MAT_ITEM_NO = Convert.ToDecimal(data.sMatItemNo);
                entReversed.SAP_FI_DOC_NO = sapFIDoc;
                entReversed.SAP_REVERSED_DOC_NO = sapReverseDocNo;
                entReversed.REVERSAL_REASON = "01";
                entReversed.CREATED_DATE = DateTime.Now;
                entReversed.CREATED_BY = userName;
                entReversed.UPDATED_DATE = DateTime.Now;
                entReversed.UPDATED_BY = userName;

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    CUSTOMS_VAT_DAL dalCustoms = new CUSTOMS_VAT_DAL();
                    dalCustoms.Update(sapFIDoc, context);
                    
                    PCF_VAT_REVERSED_FI_DOC_DAL dalReversed = new PCF_VAT_REVERSED_FI_DOC_DAL();
                    dalReversed.Save(entReversed);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
