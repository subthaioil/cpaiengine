﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class RoleServiceModel
    {
        private string ParentMenuFormat = "{{ \"text\": \"  {0}\" " +
                                        ",\"color\": \"#428bca\" " +
                                        ",\"backColor\": \"{1}\" " +
                                        ",\"href\": \"#node-{2}\" " +
                                        ",\"selectable\": \"false\" " +
                                        ",\"state\": {{ " +
                                        " \"expanded\": \"true\" " +
                                        " {3} " +
                                        "}} " +
                                        ",\"nodes\": {4} " +
                                        "}}";

        //private string ParentMenuCheckedFormat = "{{ \"text\": \"  {0}\" " +
        //                                ",\"color\": \"#428bca\" " +
        //                                ",\"backColor': \"{1}\" " +
        //                                ",\"href\": \"#node-{2}\" " +
        //                                ",\"selectable\": \"false\" " +
        //                                ",\"state\": {{ " +
        //                                " \"expanded\": \"true\" " +
        //                                ",\"checked\": \"true\" " +
        //                                "}} " +
        //                                ",\"nodes\": {3} " +
        //                                "}}";

        private string SubMenuFormat = "{{ \"text\": \"   {0}\" " +
                                       ", \"color\": \"#428bca\" " +
                                       ", \"backColor\": \"{1}\" " +
                                       ", \"href\": \"#node-{2}\" " +
                                        ",\"state\": {{ " +
                                        " \"expanded\": \"true\" " +
                                        " {3} " +
                                        "}} " +
                                       ", \"nodes\": {4} " +
                                       "}}";

        //private string SubMenuCheckedFormat = "{{ \"text\": \"  {0}\" " +
        //                           ",\"color\": \"#428bca\" " +
        //                           ",\"backColor\": \"{1}\" " +
        //                           ",\"href\": \"#node-{2}\" " +
        //                           ",\"state\": {{ " +
        //                           " \"expanded\": \"true\" " +
        //                           ",\"checked\": \"true\" " +
        //                           "}}" +
        //                           ",\"nodes\": {3} " +
        //                           "}}";

        public ReturnValue Add(RoleViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.RolName))
                {
                    rtn.Message = "Role Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (string.IsNullOrEmpty(pModel.RolType))
                //{
                //    rtn.Message = "Role Type should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                else if (string.IsNullOrEmpty(pModel.RolDesc))
                {
                    rtn.Message = "Role Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.RolStatus))
                {
                    rtn.Message = "Role Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pModel.MenuTreeID))
                {
                    rtn.Message = "Please add Role menu";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.MenuTreeID.SplitWord("|").Count() == 0)
                {
                    rtn.Message = "Please add Role menu";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (pModel.RoleMenu.Count >= 0)
                //{
                //    var roleEmpty = pModel.RoleMenu.Any(x => string.IsNullOrWhiteSpace(x.MenuID));
                //    if (roleEmpty == true)
                //    {
                //        rtn.Message = "MenuID should not be empty";
                //        rtn.Status = false;
                //        return rtn;
                //    }

                //    var menuEmpty = pModel.RoleMenu.Any(x => string.IsNullOrWhiteSpace(x.MenuLevel));
                //    if (menuEmpty == true)
                //    {
                //        rtn.Message = "MenuLevel should not be empty";
                //        rtn.Status = false;
                //        return rtn;
                //    }

                //}

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                ROLE_DAL dal = new ROLE_DAL();
                ROLE ent = new ROLE();

                ROLE_MENU_DAL dalCtr = new ROLE_MENU_DAL();
                ROLE_MENU entCtr = new ROLE_MENU();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var roleCode = ShareFn.GenerateCodeByDate("CPAI");
                            ent.ROL_ROW_ID = roleCode;
                            ent.ROL_NAME = pModel.RolName.Trim();
                            ent.ROL_TYPE = "1";//pModel.RolType.Trim();
                            ent.ROL_STATUS = pModel.RolStatus; //CPAIConstantUtil.ACTIVE;
                            ent.ROL_DESCRIPTION = pModel.RolDesc.Trim();

                            ent.ROL_CREATED_BY = pUser;
                            ent.ROL_CREATED_DATE = now;
                            ent.ROL_UPDATED_BY = pUser;
                            ent.ROL_UPDATED_DATE = now;

                            ent.ROLE_MENU = new List<ROLE_MENU>();
                            ent.USER_ROLE = new List<USER_ROLE>();

                            dal.Save(ent, context);

                            foreach (var item in pModel.MenuTreeID.SplitWord("|").ToList())
                            {
                                if (item != "")
                                {
                                    entCtr = new ROLE_MENU();
                                    entCtr.RMN_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.RMN_FK_MENU = (item.Replace("&", "")).Replace("&", "");
                                    entCtr.RMN_FK_ROLE = ent.ROL_ROW_ID;
                                    entCtr.RMN_CREATED_BY = pUser;
                                    entCtr.RMN_CREATED_DATE = now;
                                    entCtr.RMN_UPDATED_BY = pUser;
                                    entCtr.RMN_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }

                            }
                           

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(RoleViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.RolName))
                {
                    rtn.Message = "Role Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (string.IsNullOrEmpty(pModel.RolType))
                //{
                //    rtn.Message = "Role Type should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                else if (string.IsNullOrEmpty(pModel.RolDesc))
                {
                    rtn.Message = "Role Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.RolStatus))
                {
                    rtn.Message = "Role Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty( pModel.MenuTreeID))
                {
                    rtn.Message = "Please add Role menu";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.MenuTreeID.SplitWord("|").Count() == 0)
                {
                    rtn.Message = "Please add Role menu";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (pModel.RoleMenu.Count >= 0)
                //{
                //    var roleEmpty = pModel.RoleMenu.Any(x => string.IsNullOrWhiteSpace(x.Role));
                //    if (roleEmpty == true)
                //    {
                //        rtn.Message = "Role should not be empty";
                //        rtn.Status = false;
                //        return rtn;
                //    }

                //    var menuEmpty = pModel.RoleMenu.Any(x => string.IsNullOrWhiteSpace(x.Menu));
                //    if (menuEmpty == true)
                //    {
                //        rtn.Message = "Menu should not be empty";
                //        rtn.Status = false;
                //        return rtn;
                //    }

                //}

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                ROLE_DAL dal = new ROLE_DAL();
                ROLE ent = new ROLE();

                ROLE_MENU_DAL dalCtr = new ROLE_MENU_DAL();
                ROLE_MENU entCtr = new ROLE_MENU();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //var roleCode = ShareFn.GenerateCodeByDate("CPAI");
                            ent.ROL_ROW_ID = pModel.RolID;
                            ent.ROL_NAME = pModel.RolName.Trim();
                            //ent.ROL_TYPE = pModel.RolType.Trim();
                            ent.ROL_STATUS = pModel.RolStatus; //CPAIConstantUtil.ACTIVE;
                            ent.ROL_DESCRIPTION = pModel.RolDesc.Trim();
                            ent.ROL_UPDATED_BY = pUser;
                            ent.ROL_UPDATED_DATE = now;

                            dal.Update(ent, context);
                            dalCtr.DeleteAll(pModel.RolID, context);

                            foreach (var item in pModel.MenuTreeID.SplitWord("|").ToList())
                            {
                                if(item != "")
                                {
                                    entCtr = new ROLE_MENU();
                                    entCtr.RMN_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.RMN_FK_MENU = (item.Replace("&","")).Replace("&", "");
                                    entCtr.RMN_FK_ROLE = pModel.RolID;
                                    entCtr.RMN_CREATED_BY = pUser;
                                    entCtr.RMN_CREATED_DATE = now;
                                    entCtr.RMN_UPDATED_BY = pUser;
                                    entCtr.RMN_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref RoleViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sName = String.IsNullOrEmpty(pModel.sRoleName) ? "" : pModel.sRoleName.Trim();
                    var sDesc = String.IsNullOrEmpty(pModel.sRoleDescription) ? "" : pModel.sRoleDescription.Trim();
                    var sStatus = String.IsNullOrEmpty(pModel.sRoleStatus) ? "" : pModel.sRoleStatus.Trim();

                    var query = (from v in context.ROLE
                                 orderby new { v.ROL_NAME }
                                 select new RoleViewModel_SeachData
                                 {
                                     dRolID = v.ROL_ROW_ID
                                     ,
                                     dRolName = v.ROL_NAME
                                     ,
                                     dRolDesc = v.ROL_DESCRIPTION
                                     ,
                                     dRolStatus = v.ROL_STATUS

                                 });

                    if (!string.IsNullOrEmpty(sName))
                        query = query.Where(p => p.dRolID.Trim().ToUpper().Contains(sName.Trim().ToUpper()));

                    if (!string.IsNullOrEmpty(sDesc))
                        query = query.Where(p => p.dRolDesc.ToUpper().Contains(sDesc.Trim().ToUpper()));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dRolStatus.ToUpper().Contains(sStatus.Trim().ToUpper()));

                    if (query != null)
                    {
                        pModel.sSearchData = new List<RoleViewModel_SeachData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new RoleViewModel_SeachData
                                                    {
                                                        dRolID = g.dRolID,
                                                        dRolName = g.dRolName,
                                                        dRolDesc = g.dRolDesc,
                                                        dRolStatus = g.dRolStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public RoleViewModel_Detail Get(string pRoleID)
        {

            StringBuilder _html = new StringBuilder();
            RoleViewModel_Detail model = new RoleViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pRoleID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MENU
                                         // where v.ToUpper().Equals(pRoleID.ToUpper())
                                     join vc in context.ROLE_MENU.Where(o => o.RMN_FK_ROLE == pRoleID)
                                        on v.MEU_ROW_ID equals vc.RMN_FK_MENU into view
                                     from vc in view.DefaultIfEmpty()
                                     join r in context.ROLE.Where(o => o.ROL_ROW_ID == pRoleID)
                                        on vc.RMN_FK_ROLE equals r.ROL_ROW_ID into role
                                     from r in role.DefaultIfEmpty()
                                     select new RoleViewModel_MenuWithRole
                                     {
                                         dMenuID = v.MEU_ROW_ID
                                         ,
                                         dMenuGroup = v.MEU_GROUP_MENU
                                         ,
                                         dMenuParentID = v.MEU_PARENT_ID
                                         ,
                                         dMenuLevel = v.MEU_LEVEL
                                         ,
                                         dMenuListNo = v.MEU_LIST_NO
                                         ,
                                         dMenuDesc = v.LNG_DESCRIPTION
                                         ,
                                         dMenuType = v.MEU_CONTROL_TYPE
                                         ,
                                         dRoleID = r.ROL_ROW_ID
                                         ,
                                         dRoleName = r.ROL_NAME
                                         ,
                                         dRoleDesc = r.ROL_DESCRIPTION
                                         ,
                                         dRoleStatus = r.ROL_STATUS
                                         ,
                                         dRoleMenuID = vc.RMN_ROW_ID

                                     }).ToList();

                        if (query != null)
                        {
                            foreach (var g in query.Where(p => !String.IsNullOrEmpty(p.dRoleID)).ToList())
                            {
                                model.RolID = g.dRoleID;
                                model.RolName = g.dRoleName;
                                model.RolDesc = g.dRoleDesc;
                                model.RolStatus = g.dRoleStatus;
                                model.BoolStatus = g.dRoleStatus == "ACTIVE" ? true : false;
                                break;
                            }

                            var MenuLevel1 = query.Where(p => p.dMenuLevel == "1").ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                            if (MenuLevel1.Count > 0)
                            {
                                int jCount = 0;
                                foreach (var j in MenuLevel1)
                                {
                                    if (!String.IsNullOrEmpty(j.dRoleID))
                                        model.MenuTreeID += (model.MenuTreeID != "" ? "|&" + j.dMenuID + "&" : "&" + j.dMenuID + "&");

                                    var qSubMenu = query.Where(p => p.dMenuParentID == j.dMenuID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                                    //var MenuFormat = !String.IsNullOrEmpty(j.dRoleID) ? ParentMenuCheckedFormat : ParentMenuFormat;
                                    var strMenu = "";
                                    if (qSubMenu.Count > 0)
                                    {
                                        var strSubMenu = GetSubMenu(j.dMenuID, query, ref model);
                                        strMenu = String.Format(ParentMenuFormat, j.dMenuDesc, GetColor(j.dMenuType), j.dMenuID, GetChecked(j.dRoleID), "["+ strSubMenu + "]");
                                    }
                                    else
                                    {
                                        strMenu = String.Format(ParentMenuFormat, j.dMenuDesc, GetColor(j.dMenuType), j.dMenuID, GetChecked(j.dRoleID), "null");
                                    }

                                    if(jCount < MenuLevel1.Count-1)
                                        _html.Append(strMenu +",");
                                    else
                                        _html.Append(strMenu);

                                    jCount++;
                                }
                            }

                            string menuJSON = "[" + _html.ToString() + "]";

                            model.MenuTree = menuJSON;
                            //model.MenuTree = MenuServiceModel.GetAllMenu();
                            model.RoleMenu = new List<RoleViewModel_Menu>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                RoleViewModel_Menu menu = new RoleViewModel_Menu();
                                menu.RoleMenuID = g.dRoleMenuID;
                                menu.MenuID = g.dMenuID;
                                menu.MenuGroup = g.dMenuGroup;
                                menu.MenuParentID = g.dMenuParentID;
                                menu.MenuLevel = g.dMenuLevel;
                                menu.MenuListNo = g.dMenuListNo;
                                menu.MenuDesc = g.dMenuDesc;

                                model.RoleMenu.Add(menu);

                                i++;
                            }
                        }
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public string GetSubMenu(string MenuParentID, List<RoleViewModel_MenuWithRole> menuLst, ref RoleViewModel_Detail model)
        {
            StringBuilder _html = new StringBuilder();

            try
            {
                if (menuLst != null)
                {
                    var SubMenu = menuLst.Where(p => p.dMenuParentID == MenuParentID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                    if(SubMenu.Count > 0)
                    {
                        int kCount = 0;
                        foreach (var k in SubMenu)
                        {
                            if (!String.IsNullOrEmpty(k.dRoleID))
                                model.MenuTreeID += (model.MenuTreeID != "" ? "|&" + k.dMenuID + "&" : "&" + k.dMenuID + "&");

                            var qSubMenu = menuLst.Where(p => p.dMenuParentID == k.dMenuID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)).ToList();

                            //var MenuFormat = !String.IsNullOrEmpty(k.dRoleID) ? SubMenuCheckedFormat : SubMenuFormat;
                            var strMenu = "";
                            if (qSubMenu.Count > 0)
                            {
                                var strSubMenu = GetSubMenu(k.dMenuID, menuLst, ref model);
                                strMenu = String.Format(SubMenuFormat, k.dMenuDesc, GetColor(k.dMenuType), k.dMenuID, GetChecked(k.dRoleID), "[" + strSubMenu + "]");
                            }
                            else
                            {
                                strMenu = String.Format(ParentMenuFormat, k.dMenuDesc, GetColor(k.dMenuType), k.dMenuID, GetChecked(k.dRoleID), "null");
                            }

                            if (kCount < SubMenu.Count - 1)
                                _html.Append(strMenu + ",");
                            else
                                _html.Append(strMenu);

                            kCount++;
                        }
                    }
                    else
                    {
                        _html.Append("");
                    }
                }
                   
                return _html.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string GetColor(string MenuType)
        {
            if(MenuType == "MENU")
            {
                return "undefined";
            }
            else
            {
                return "#E0E9F8";
            }
        }

        public string GetChecked(string RoleID)
        {
            if(!string.IsNullOrEmpty(RoleID))
            {
                return ",\"checked\": \"true\" ";
            }
            else
            {
                return "";
            }
        }

        public bool CheckMenuURLDulpicate()
        {
            return false;
        }

        //public RoleViewModel_Detail Get(string pRoleID)
        //{
        //    RoleViewModel_Detail model = new RoleViewModel_Detail();
        //    try
        //    {
        //        if (String.IsNullOrEmpty(pRoleID) == false)
        //        {
        //            using (EntityCPAIEngine context = new EntityCPAIEngine())
        //            {
        //                var query = (from v in context.MENU
        //                                 // where v.ToUpper().Equals(pRoleID.ToUpper())
        //                             join vc in context.ROLE_MENU.Where(o => o.RMN_FK_ROLE == pRoleID)
        //                                on v.MEU_ROW_ID equals vc.RMN_FK_MENU into view
        //                             from vc in view.DefaultIfEmpty()
        //                             join r in context.ROLE.Where(o => o.ROL_ROW_ID == pRoleID)
        //                                on vc.RMN_FK_ROLE equals r.ROL_ROW_ID into role
        //                             from r in role.DefaultIfEmpty()
        //                             select new
        //                             {
        //                                 dMenuID = v.MEU_ROW_ID
        //                                 ,
        //                                 dMenuGroup = v.MEU_GROUP_MENU
        //                                 ,
        //                                 dMenuParentID = v.MEU_PARENT_ID
        //                                 ,
        //                                 dMenuLevel = v.MEU_LEVEL
        //                                 ,
        //                                 dMenuListNo = v.MEU_LIST_NO
        //                                 ,
        //                                 dMenuDesc = v.LNG_DESCRIPTION
        //                                 ,
        //                                 dRoleID = r.ROL_ROW_ID
        //                                 ,
        //                                 dRoleName = r.ROL_NAME
        //                                 ,
        //                                 dRoleDesc = r.ROL_DESCRIPTION
        //                                 ,
        //                                 dRoleStatus = r.ROL_STATUS
        //                                 ,
        //                                 dRoleMenuID = vc.RMN_ROW_ID

        //                             });

        //                if (query != null)
        //                {
        //                    foreach (var g in query.Where(p => !String.IsNullOrEmpty(p.dRoleID)).ToList())
        //                    {
        //                        model.RolID = g.dRoleID;
        //                        model.RolName = g.dRoleName;
        //                        model.RolDesc = g.dRoleDesc;
        //                        model.RolStatus = g.dRoleStatus;
        //                        model.BoolStatus = g.dRoleStatus == "ACTIVE" ? true : false;
        //                        break;
        //                    }

        //                    string parentMenu = "";
        //                    string childMenu = "";
        //                    string subchildMenu = "";
        //                    foreach (var j in query.Where(p => p.dMenuLevel == "1").ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)))
        //                    {
        //                        if (!String.IsNullOrEmpty(j.dRoleID))
        //                            model.MenuTreeID += (model.MenuTreeID != "" ? "|&" + j.dMenuID + "&" : "&" + j.dMenuID + "&");

        //                        childMenu = "";
        //                        foreach (var k in query.Where(p => p.dMenuParentID == j.dMenuID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)))
        //                        {
        //                            if (!String.IsNullOrEmpty(k.dRoleID))
        //                                model.MenuTreeID += (model.MenuTreeID != "" ? "|&" + k.dMenuID + "&" : "&" + k.dMenuID + "&");

        //                            subchildMenu = "";
        //                            foreach (var l in query.Where(p => p.dMenuParentID == k.dMenuID).ToList().OrderBy(x => int.Parse(x.dMenuLevel)).ThenBy(y => int.Parse(y.dMenuListNo)))
        //                            {
        //                                if (!String.IsNullOrEmpty(l.dRoleID))
        //                                    model.MenuTreeID += (model.MenuTreeID != "" ? "|&" + l.dMenuID + "&" : "&" + l.dMenuID + "&");
        //                                if (subchildMenu != "")
        //                                {
        //                                    subchildMenu += ",";
        //                                }
        //                                subchildMenu += "{ \"text\": \"  " + l.dMenuDesc + "\"" +

        //                                ",\"color\": \"#428bca\"" +
        //                                ",\"backColor\": \"#E0E9F8\"" +
        //                                ",\"href\": \"#node-" + l.dMenuID + "\"" +
        //                                //",\"selectable\": \"true\"" +
        //                                (!String.IsNullOrEmpty(l.dRoleID) ?
        //                                ",\"state\": { " +
        //                                "\"checked\": \"true\"" +
        //                                //",\"disabled\": \"true\"" +
        //                                //",\"expanded\": \"false\"" +
        //                                //",\"selected\": \"true\"" +
        //                                "}" : "") +
        //                                //",\"tags\": [\"available\"]" +
        //                                ",\"nodes\": null" +
        //                                "}";
        //                            }

        //                            if (subchildMenu != "")
        //                            {
        //                                if (childMenu != "")
        //                                {
        //                                    childMenu += ",";
        //                                }
        //                                childMenu += "{ \"text\": \"  " + k.dMenuDesc + "\"" +

        //                                ",\"color\": \"#428bca\"" +
        //                                ",\"backColor\": \"undefined\"" +
        //                                ",\"href\": \"#node-" + k.dMenuID + "\"" +
        //                                //",\"selectable\": \"false\"" +
        //                                ",\"state\": { " +
        //                                "\"expanded\": \"false\"" +
        //                                (!String.IsNullOrEmpty(k.dRoleID) ?
        //                                ",\"checked\": \"true\"" : "") +
        //                                //",\"expanded\": \"false\"" +
        //                                //",\"selected\": \"true\"" +
        //                                "}" +
        //                                //",\"tags\": [\"available\"]" +
        //                                ",\"nodes\": " + (subchildMenu == "" ? "null" : " [" + subchildMenu + "]") +
        //                                "}";
        //                            }
        //                            else
        //                            {
        //                                if (childMenu != "")
        //                                {
        //                                    childMenu += ",";
        //                                }
        //                                childMenu += "{ \"text\": \"  " + k.dMenuDesc + "\"" +

        //                                ",\"color\": \"#428bca\"" +
        //                                ",\"backColor\": \"undefined\"" +
        //                                ",\"href\": \"#node-" + k.dMenuID + "\"" +
        //                                //",\"selectable\": \"false\"" +
        //                                 ",\"state\": { " +
        //                                "\"expanded\": \"false\"" +
        //                                (!String.IsNullOrEmpty(k.dRoleID) ?
        //                                ",\"checked\": \"true\"" : "") +
        //                                //",\"expanded\": \"false\"" +
        //                                //",\"selected\": \"true\"" +
        //                                "}" +
        //                                //",\"tags\": [\"available\"]" +
        //                                ",\"nodes\": null" +
        //                                "}";
        //                            }

        //                        }
        //                        if (parentMenu != "")
        //                        {
        //                            parentMenu += ",";
        //                        }
        //                        parentMenu += "{ \"text\": \"  " + j.dMenuGroup + "\"" +
        //                                //",\"icon\": \"glyphicon glyphicon-bookmark\"" +
        //                                //",\"collapseIcon\": \"glyphicon glyphicon-chevron-down\"" +
        //                                //",\"expandIcon\": \"glyphicon glyphicon-chevron-right\"" +
        //                                //",\"selectedIcon\": \"glyphicon glyphicon-bookmark\"" +
        //                                ",\"color\": \"#428bca\"" +
        //                                ",\"backColor\": \"undefined\"" +
        //                                ",\"href\": \"#node-" + j.dMenuID + "\"" +
        //                                ",\"selectable\": \"false\"" +
        //                                 ",\"state\": { " +
        //                                "\"expanded\": \"true\"" +
        //                                (!String.IsNullOrEmpty(j.dRoleID) ?
        //                                ",\"checked\": \"true\"" : "") +
        //                                //",\"disabled\": \"false\"" +
        //                                //",\"selected\": \"true\"" +
        //                                "}" +
        //                                //",\"tags\": [\"available\"]" +
        //                                ",\"nodes\": " + (childMenu == "" ? "null" : " [" + childMenu + "]") +
        //                                "}";
        //                    }
        //                    string menuJSON = "[" + parentMenu + "]";

        //                    model.MenuTree = menuJSON;
        //                    //model.MenuTree = "[{ \"text\": \"" + "test" + "\"," +
        //                    //            "\"icon\": \"glyphicon glyphicon-unchecked\"," +
        //                    //            "\"selectedIcon\": \"glyphicon glyphicon-unchecked\"," +
        //                    //            "\"color\": \"#000000\"," +
        //                    //            "\"backColor\": \"#FFFFFF\"," +
        //                    //            "\"href\": \"#node-111\"," +
        //                    //            "\"selectable\": \"true\"," +
        //                    //            "\"state\": { " +
        //                    //            "\"checked\": \"true\"," +
        //                    //            "\"disabled\": \"false\"," +
        //                    //            "\"expanded\": \"true\"," +
        //                    //            "\"selected\": \"true\"" +
        //                    //            "}," +
        //                    //            "\"tags\": [\"available\"]" +
        //                    //            "}]";
        //                    //model.MenuTree = "[{ \"text\": \"Parent 2\" }]";
        //                    //model.MenuTree = "[{\"text\": \"Parent 1\", \"nodes\": [{\"text\": \"Child 1\",\"nodes\": [{ \"text\": \"Grandchild 1\" }," +
        //                    //            "{ \"text\": \"Grandchild 2\" } ] }, { \"text\": \"Child 2\" } ] }, { \"text\": \"Parent 2\" }," +
        //                    //            "{  \"text\": \"Parent 3\" }]";
        //                    model.RoleMenu = new List<RoleViewModel_Menu>();
        //                    int i = 1;
        //                    foreach (var g in query)
        //                    {

        //                        //string jsonXML = "";
        //                        //jsonXML = "{\"counter_txn\": [" + bodyXml + "]}";

        //                        RoleViewModel_Menu menu = new RoleViewModel_Menu();
        //                        menu.RoleMenuID = g.dRoleMenuID;
        //                        menu.MenuID = g.dMenuID;
        //                        menu.MenuGroup = g.dMenuGroup;
        //                        menu.MenuParentID = g.dMenuParentID;
        //                        menu.MenuLevel = g.dMenuLevel;
        //                        menu.MenuListNo = g.dMenuListNo;
        //                        menu.MenuDesc = g.dMenuDesc;

        //                        model.RoleMenu.Add(menu);

        //                        i++;
        //                    }
        //                }
        //            }

        //            return model;
        //        }
        //        else
        //        {
        //            return null;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }


        //}

        public List<ROLE> getAllRoles()
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.ROLE
                                 where v.ROL_STATUS == "ACTIVE"
                                 orderby v.ROL_NAME
                                 select v).ToList();

                    return query;

                }
            } catch (Exception ex)
            {
                return null;
            }
        }
    }
}
