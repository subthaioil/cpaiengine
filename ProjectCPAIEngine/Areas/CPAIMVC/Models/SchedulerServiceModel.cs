﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.DALFreight;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Model;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class SchedulerServiceModel
    {

        public static string getTransactionCMMTByID(string id)
        {
            SchRootObject rootObj = new SchRootObject();
            rootObj.sch_detail = new SchDetail();
            CPAI_VESSEL_SCHEDULE_DAL dataDal = new CPAI_VESSEL_SCHEDULE_DAL();
            CPAI_VESSEL_SCHEDULE data = dataDal.GetByID(id);
            if (data != null)
            {
                rootObj.sch_detail.vessel = data.VSD_FK_VEHICLE;
                rootObj.sch_detail.freight = data.VSD_FK_FREIGHT;
                rootObj.sch_detail.vendor = data.VSD_FK_CUST;
                rootObj.sch_detail.vendor_name = MT_CUST_DETAIL_DAL.GetCUSTDetailName(data.VSD_FK_CUST);
                rootObj.sch_detail.schedule_type = data.VSD_SCH_TYPE;
                rootObj.sch_detail.date_start = (data.VSD_DATE_START == DateTime.MinValue || data.VSD_DATE_START == null) ? "" : Convert.ToDateTime(data.VSD_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.date_end = (data.VSD_DATE_END == DateTime.MinValue || data.VSD_DATE_END == null) ? "" : Convert.ToDateTime(data.VSD_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.laycan_from = (data.VSD_LAYCAN_FROM == DateTime.MinValue || data.VSD_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.VSD_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.laycan_to = (data.VSD_LAYCAN_TO == DateTime.MinValue || data.VSD_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.VSD_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.note = data.VSD_NOTE;
                rootObj.sch_detail.tentative = data.VSD_TENTATIVE_FLAG == "Y" ? true : false;
                rootObj.sch_detail.schedule_type = data.VSD_SCH_TYPE;
                rootObj.sch_detail.detail = data.VSD_DETAIL;
                rootObj.sch_detail.color = data.VSD_COLOR;


                rootObj.sch_detail.sch_load_ports = new List<SchLoadPorts>();
                rootObj.sch_detail.sch_dis_ports = new List<SchDisPorts>();
                rootObj.sch_detail.sch_cargo = new List<SchCargo>();
                List<CPAI_VESSEL_SCHEDULE_PORT> port = new CPAI_VESSEL_SCHEDULE_PORT_DAL().GetPortByDataID(id);
                if (port != null)
                {
                    for (int i = 0; i < port.Count; i++)
                    {
                        if (port[i].VSP_PORT_TYPE.ToUpper() == "L")
                        {
                            SchLoadPorts obj_port = new SchLoadPorts();
                            obj_port.order = port[i].VSP_ORDER_PORT;
                            obj_port.port = port[i].VSP_PORT_NAME;
                            rootObj.sch_detail.sch_load_ports.Add(obj_port);
                        }
                        else
                        {
                            SchDisPorts obj_port = new SchDisPorts();
                            obj_port.order = port[i].VSP_ORDER_PORT;
                            obj_port.port = port[i].VSP_PORT_NAME;
                            rootObj.sch_detail.sch_dis_ports.Add(obj_port);
                        }
                    }
                }

                List<CPAI_VESSEL_SCHEDULE_CARGO> cargo = new CPAI_VESSEL_SCHEDULE_CARGO_DAL().GetCargoByDataID(id);
                if (cargo != null)
                {
                    for (int i = 0; i < cargo.Count; i++)
                    {
                        SchCargo obj_cargo = new SchCargo();
                        obj_cargo.order = cargo[i].VSCG_ORDER;
                        obj_cargo.cargo = cargo[i].VSCG_FK_MT_MATERIALS;
                        obj_cargo.qty = Convert.ToDecimal(cargo[i].VSCG_QUANTITY).ToString("#,##0");
                        obj_cargo.tolerance = cargo[i].VSCG_TOLERANCE;
                        obj_cargo.unit = cargo[i].VSCG_UNIT;
                        obj_cargo.others = cargo[i].VSCG_OTHERS;
                        rootObj.sch_detail.sch_cargo.Add(obj_cargo);
                    }
                }
                rootObj.sch_activitys = new List<SchActivity>();
                List<CPAI_VESSEL_SCHEDULE_ACTIVITYS> dataAct = new CPAI_VESSEL_SCHEDULE_ACTIVITYS_DAL().GetSchedulerActivityByDataID(id);
                using (var context = new EntityCPAIEngine())
                {
                    var _schs = (from a in dataAct
                                 join b in context.MT_VESSEL_ACTIVITY on a.VAS_FK_VESSEL_ACTIVITY equals b.MVA_ROW_ID into bb
                                 from b in bb.DefaultIfEmpty()
                                 join c in context.MT_VESSEL_ACTIVITY_CONTROL on new { FK_VESSEL_ACTIVITY = a.VAS_FK_VESSEL_ACTIVITY, MAC_SYSTEM = "CPAI", MAC_TYPE = "CMMT" } equals new { FK_VESSEL_ACTIVITY = c.MAC_FK_VESSEL_ACTIVITY, MAC_SYSTEM = c.MAC_SYSTEM, MAC_TYPE = c.MAC_TYPE } into cc
                                 from c in cc.DefaultIfEmpty()
                                 where a.VAS_FK_VESSEL_SCHEDULE == id.ToUpper() //&& c.MAC_SYSTEM.ToUpper() == "CPAI" && c.MAC_TYPE.ToUpper() == "CMMT"
                                 select new
                                 {
                                     VAS_ROW_ID = a.VAS_ROW_ID,
                                     VAS_FK_VESSEL_ACTIVITY = a.VAS_FK_VESSEL_ACTIVITY,
                                     VAS_ORDER = a.VAS_ORDER,
                                     VAS_START_DATE = a.VAS_START_DATE,
                                     VAS_END_DATE = a.VAS_END_DATE,
                                     VAS_ACTIVITY_OTHER = a.VAS_ACTIVITY_OTHER,
                                     VAS_NOTE = a.VAS_NOTE,
                                     MVA_ROW_ID = b == null ? null : b.MVA_ROW_ID,
                                     MVA_CODE = b == null ? null : b.MVA_CODE,
                                     MVA_KEY = b == null ? null : b.MVA_KEY,
                                     MVA_STANDARD_PHRASE_EN = b == null ? null : b.MVA_STANDARD_PHRASE_EN,
                                     MVA_REF = b == null ? null : b.MVA_REF,
                                     MAC_SYSTEM = c == null ? null : c.MAC_SYSTEM,
                                     MAC_TYPE = c == null ? null : c.MAC_TYPE,
                                     MAC_COLOR_CODE = c == null ? null : c.MAC_COLOR_CODE,
                                     MAC_STATUS = c == null ? null : c.MAC_STATUS
                                 });


                    var result = _schs.OrderBy(o => o.VAS_ORDER).ToList();
                    if (result != null)
                    {
                        result = result.Distinct().ToList();
                        for (int i = 0; i < result.Count; i++)
                        {
                            var MVA_ROW_ID = result[i].MVA_ROW_ID;
                            var MVA_CODE = result[i].MVA_CODE;
                            var MVA_STANDARD_PHRASE_EN = result[i].MVA_STANDARD_PHRASE_EN;
                            var MVA_REF = Convert.ToString(result[i].MVA_REF) == "Y" ? "Y" : "N";
                            var MAC_COLOR_CODE = result[i].MAC_COLOR_CODE;

                            var VAS_ACTIVITY = MVA_REF + "|" + MAC_COLOR_CODE + "|" + MVA_ROW_ID;
                            //var VAS_ACTIVITY_NAME = MVA_CODE + " | " + MVA_STANDARD_PHRASE_EN;
                            var VAS_ACTIVITY_NAME = MVA_STANDARD_PHRASE_EN;
                            var VAS_ACTIVITY_COLOR = MAC_COLOR_CODE;

                            if (string.IsNullOrEmpty(MVA_ROW_ID))
                            {
                                VAS_ACTIVITY = "N|" + CPAIConstantUtil.COLOR_DEFUALT + "|";
                                VAS_ACTIVITY_NAME = "Other";
                                VAS_ACTIVITY_COLOR = CPAIConstantUtil.COLOR_DEFUALT;
                            }
                            SchActivity obj_Act = new SchActivity();
                            obj_Act.order = result[i].VAS_ORDER;
                            obj_Act.activity = VAS_ACTIVITY;
                            obj_Act.activity_name = VAS_ACTIVITY_NAME;
                            obj_Act.activity_color = VAS_ACTIVITY_COLOR;
                            obj_Act.start_date = Convert.ToDateTime(result[i].VAS_START_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            obj_Act.date = Convert.ToDateTime(result[i].VAS_END_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            obj_Act.explanationAttach = GetAttactSingleFileVessel(result[i].VAS_ROW_ID, "EXT");
                            obj_Act.other_activity = result[i].VAS_ACTIVITY_OTHER;
                            obj_Act.act_note = result[i].VAS_NOTE;
                            rootObj.sch_activitys.Add(obj_Act);
                        }
                    }
                }




                //if (dataAct != null)
                //{
                //    for (int i = 0; i < dataAct.Count; i++)
                //    {
                //        SchActivity obj_Act = new SchActivity();
                //        obj_Act.activity = dataAct[i].VAS_FK_VESSEL_ACTIVITY;
                //        obj_Act.order = dataAct[i].VAS_ORDER;
                //        obj_Act.start_date = Convert.ToDateTime(dataAct[i].VAS_START_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                //        obj_Act.date = Convert.ToDateTime(dataAct[i].VAS_END_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                //        obj_Act.other_activity = dataAct[i].VAS_ACTIVITY_OTHER;
                //        obj_Act.explanationAttach = GetAttactFileVessel(dataAct[i].VAS_ROW_ID, "EXT");
                //        obj_Act.act_note = dataAct[i].VAS_NOTE;
                //        rootObj.sch_activitys.Add(obj_Act);
                //    }
                //}
            }
            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getTransactionCMMTActByID(string id)
        {
            SchRootObject rootObj = new SchRootObject();
            rootObj.sch_detail = new SchDetail();
            rootObj.sch_activitys = new List<SchActivity>();

            CPAI_VESSEL_SCHEDULE_DAL dataDal = new CPAI_VESSEL_SCHEDULE_DAL();
            CPAI_VESSEL_SCHEDULE data = dataDal.GetByID(id);
            List<CPAI_VESSEL_SCHEDULE_ACTIVITYS> dataAct = new CPAI_VESSEL_SCHEDULE_ACTIVITYS_DAL().GetSchedulerActivityByDataID(id);

            if (data != null)
            {
                rootObj.sch_detail.vessel = data.VSD_FK_VEHICLE;
                rootObj.sch_detail.freight = data.VSD_FK_FREIGHT;
                rootObj.sch_detail.vendor = data.VSD_FK_CUST;
                rootObj.sch_detail.schedule_type = data.VSD_SCH_TYPE;
                rootObj.sch_detail.date_start = (data.VSD_DATE_START == DateTime.MinValue || data.VSD_DATE_START == null) ? "" : Convert.ToDateTime(data.VSD_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.date_end = (data.VSD_DATE_END == DateTime.MinValue || data.VSD_DATE_END == null) ? "" : Convert.ToDateTime(data.VSD_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.laycan_from = (data.VSD_LAYCAN_FROM == DateTime.MinValue || data.VSD_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.VSD_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.laycan_to = (data.VSD_LAYCAN_TO == DateTime.MinValue || data.VSD_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.VSD_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.sch_detail.note = data.VSD_NOTE;
                rootObj.sch_detail.tentative = data.VSD_TENTATIVE_FLAG == "Y" ? true : false;
                rootObj.sch_detail.schedule_type = data.VSD_SCH_TYPE;
                rootObj.sch_detail.detail = data.VSD_DETAIL;
                rootObj.sch_detail.color = data.VSD_COLOR;

                rootObj.sch_detail.sch_load_ports = new List<SchLoadPorts>();
                rootObj.sch_detail.sch_dis_ports = new List<SchDisPorts>();
                rootObj.sch_detail.sch_cargo = new List<SchCargo>();
            }


            if (dataAct != null)
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _schs = (from a in dataAct
                                 join b in context.MT_VESSEL_ACTIVITY on a.VAS_FK_VESSEL_ACTIVITY equals b.MVA_ROW_ID into bb
                                 from b in bb.DefaultIfEmpty()
                                 join c in context.MT_VESSEL_ACTIVITY_CONTROL on new { FK_VESSEL_ACTIVITY = a.VAS_FK_VESSEL_ACTIVITY, MAC_SYSTEM = "CPAI", MAC_TYPE = "CMMT" } equals new { FK_VESSEL_ACTIVITY = c.MAC_FK_VESSEL_ACTIVITY, MAC_SYSTEM = c.MAC_SYSTEM, MAC_TYPE = c.MAC_TYPE } into cc
                                 from c in cc.DefaultIfEmpty()
                                 where a.VAS_FK_VESSEL_SCHEDULE == id.ToUpper() //&& c.MAC_SYSTEM.ToUpper() == "CPAI" && c.MAC_TYPE.ToUpper() == "CMMT"
                                 select new
                                 {
                                     VAS_ROW_ID = a.VAS_ROW_ID,
                                     VAS_FK_VESSEL_ACTIVITY = a.VAS_FK_VESSEL_ACTIVITY,
                                     VAS_ORDER = a.VAS_ORDER,
                                     VAS_START_DATE = a.VAS_START_DATE,
                                     VAS_END_DATE = a.VAS_END_DATE,
                                     VAS_ACTIVITY_OTHER = a.VAS_ACTIVITY_OTHER,
                                     VAS_NOTE = a.VAS_NOTE,
                                     MVA_ROW_ID = b == null ? null : b.MVA_ROW_ID,
                                     MVA_CODE = b == null ? null : b.MVA_CODE,
                                     MVA_KEY = b == null ? null : b.MVA_KEY,
                                     MVA_STANDARD_PHRASE_EN = b == null ? null : b.MVA_STANDARD_PHRASE_EN,
                                     MVA_REF = b == null ? null : b.MVA_REF,
                                     MAC_SYSTEM = c == null ? null : c.MAC_SYSTEM,
                                     MAC_TYPE = c == null ? null : c.MAC_TYPE,
                                     MAC_COLOR_CODE = c == null ? null : c.MAC_COLOR_CODE,
                                     MAC_STATUS = c == null ? null : c.MAC_STATUS
                                 });


                    var result = _schs.OrderBy(o => o.VAS_ORDER).ToList();
                    if (result != null)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            var MVA_ROW_ID = result[i].MVA_ROW_ID;
                            var MVA_CODE = result[i].MVA_CODE;
                            var MVA_STANDARD_PHRASE_EN = result[i].MVA_STANDARD_PHRASE_EN;
                            var MVA_REF = Convert.ToString(result[i].MVA_REF) == "Y" ? "Y" : "N";
                            var MAC_COLOR_CODE = result[i].MAC_COLOR_CODE;

                            var VAS_ACTIVITY = MVA_REF + "|" + MAC_COLOR_CODE + "|" + MVA_ROW_ID;
                            //var VAS_ACTIVITY_NAME = MVA_CODE + " | " + MVA_STANDARD_PHRASE_EN;
                            var VAS_ACTIVITY_NAME = MVA_STANDARD_PHRASE_EN;
                            var VAS_ACTIVITY_COLOR = MAC_COLOR_CODE;

                            if (string.IsNullOrEmpty(MVA_ROW_ID))
                            {
                                VAS_ACTIVITY = "N|" + CPAIConstantUtil.COLOR_DEFUALT + "|";
                                VAS_ACTIVITY_NAME = "Other";
                                VAS_ACTIVITY_COLOR = CPAIConstantUtil.COLOR_DEFUALT;
                            }
                            SchActivity obj_Act = new SchActivity();
                            obj_Act.order = result[i].VAS_ORDER;
                            obj_Act.activity = VAS_ACTIVITY;
                            obj_Act.activity_name = VAS_ACTIVITY_NAME;
                            obj_Act.activity_color = VAS_ACTIVITY_COLOR;
                            obj_Act.start_date = Convert.ToDateTime(result[i].VAS_START_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            obj_Act.date = Convert.ToDateTime(result[i].VAS_END_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            obj_Act.explanationAttach = GetAttactSingleFileVessel(result[i].VAS_ROW_ID, "EXT");
                            obj_Act.other_activity = result[i].VAS_ACTIVITY_OTHER;
                            obj_Act.act_note = result[i].VAS_NOTE;
                            rootObj.sch_activitys.Add(obj_Act);
                        }
                    }
                }
                //for (int i = 0; i < dataAct.Count; i++)
                //{
                //    SchActivity obj_Act = new SchActivity();
                //    obj_Act.activity = dataAct[i].VAS_FK_VESSEL_ACTIVITY;
                //    obj_Act.order = dataAct[i].VAS_ORDER;
                //    obj_Act.start_date = Convert.ToDateTime(dataAct[i].VAS_START_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                //    obj_Act.date = Convert.ToDateTime(dataAct[i].VAS_END_DATE).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                //    obj_Act.other_activity = dataAct[i].VAS_ACTIVITY_OTHER;
                //    obj_Act.explanationAttach = GetAttactFileVessel(dataAct[i].VAS_ROW_ID, "EXT");
                //    obj_Act.act_note = dataAct[i].VAS_NOTE;
                //    rootObj.sch_activitys.Add(obj_Act);
                //}
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string GetAttactFileVessel(string ACTID, string Type = "EXT")
        {
            string strExplanationAttach = "";
            CPAI_VESSEL_SCHEDULE_ATTACH_DAL fileDal = new CPAI_VESSEL_SCHEDULE_ATTACH_DAL();
            List<CPAI_VESSEL_SCHEDULE_ATTACH> extFile = fileDal.GetFileByDataID(ACTID, Type);
            foreach (var item in extFile)
            {
                strExplanationAttach += item.VSAF_PATH + ":" + item.VSAF_INFO + "|";
            }
            return strExplanationAttach;
        }

        public static string GetAttactSingleFileVessel(string ACTID, string Type = "EXT")
        {
            string strExplanationAttach = "";
            CPAI_VESSEL_SCHEDULE_ATTACH_DAL fileDal = new CPAI_VESSEL_SCHEDULE_ATTACH_DAL();
            CPAI_VESSEL_SCHEDULE_ATTACH extFile = fileDal.GetFileByDataID(ACTID, Type).FirstOrDefault();
            if (extFile != null)
            {
                strExplanationAttach = extFile.VSAF_PATH;
            }
            return strExplanationAttach;
        }

        public static string getTransactionCMCSByID(string id)
        {
            SchsRootObject rootObj = new SchsRootObject();
            rootObj.schs_detail = new schsDetail();
            CPAI_VESSEL_SCH_S_DAL dataDal = new CPAI_VESSEL_SCH_S_DAL();

            CPAI_VESSEL_SCH_S data = dataDal.GetSchedulerByDataID(id);
            if (data != null)
            {
                rootObj.schs_detail.vessel = data.VSDS_FK_VEHICLE;
                rootObj.schs_detail.tc = data.VSDS_TC;
                rootObj.schs_detail.date_start = Convert.ToDateTime(data.VSDS_DATE_START).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                if(data.VSDS_DATE_END != null)
                {
                    rootObj.schs_detail.date_end = Convert.ToDateTime(data.VSDS_DATE_END).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                }
                
                rootObj.schs_detail.note = data.VSDS_NOTE;

                rootObj.schs_detail.schs_load_ports = new List<Model.SchsLoadPorts>();
                rootObj.schs_detail.schs_dis_ports = new List<Model.SchsDisPorts>();
                List<CPAI_VESSEL_SCH_S_PORT> port = new CPAI_VESSEL_SCH_S_PORT_DAL().GetPortByDataID(id);
                if (port != null)
                {
                    for (int i = 0; i < port.Count; i++)
                    {
                        if (port[i].VSPS_PORT_TYPE.ToUpper() == "L")
                        {
                            Model.SchsLoadPorts obj_port = new Model.SchsLoadPorts();
                            obj_port.order = port[i].VSPS_ORDER_PORT;
                            obj_port.port = port[i].VSPS_FK_PORT.ToString();
                            List<SchsCrude> schs_crude_list = new List<SchsCrude>();
                            List<CPAI_VESSEL_SCH_S_PRODUCTS> newProduct = new CPAI_VESSEL_SCH_S_PRODUCTS_DAL().GetProductByDataID(port[i].VSPS_ROW_ID);
                            if (newProduct != null)
                            {
                                for (int j = 0; j < newProduct.Count; j++)
                                {
                                    SchsCrude obj_crude = new SchsCrude();
                                    obj_crude.crude = newProduct[j].VSPS_FK_MATERIAL;
                                    obj_crude.qty = newProduct[j].VSPS_QTY;
                                    if (newProduct[j].VSPS_NOMINATE_DATE_START != null && newProduct[j].VSPS_NOMINATE_DATE_END != null)
                                    {
                                        obj_crude.nominate_date_start = Convert.ToDateTime(newProduct[j].VSPS_NOMINATE_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        obj_crude.nominate_date_end = Convert.ToDateTime(newProduct[j].VSPS_NOMINATE_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        obj_crude.nominate_date_start = null;
                                        obj_crude.nominate_date_end = null;
                                    }
                                    if (newProduct[j].VSPS_RECEIVED_DATE_START != null && newProduct[j].VSPS_RECEIVED_DATE_END != null)
                                    {
                                        obj_crude.received_date_start = Convert.ToDateTime(newProduct[j].VSPS_RECEIVED_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                        obj_crude.received_date_end = Convert.ToDateTime(newProduct[j].VSPS_RECEIVED_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        obj_crude.received_date_start = null;
                                        obj_crude.received_date_end = null;
                                    }
                                    //obj_crude.nominate_date_start = Convert.ToDateTime(newProduct[j].VSPS_NOMINATE_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    //obj_crude.nominate_date_end = Convert.ToDateTime(newProduct[j].VSPS_NOMINATE_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    //obj_crude.received_date_start = Convert.ToDateTime(newProduct[j].VSPS_RECEIVED_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    //obj_crude.received_date_end = Convert.ToDateTime(newProduct[j].VSPS_RECEIVED_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    schs_crude_list.Add(obj_crude);
                                }
                            }
                            obj_port.schs_crude = schs_crude_list;
                            rootObj.schs_detail.schs_load_ports.Add(obj_port);
                        }
                        else
                        {
                            Model.SchsDisPorts obj_port = new Model.SchsDisPorts();
                            obj_port.order = port[i].VSPS_ORDER_PORT;
                            obj_port.port = port[i].VSPS_FK_PORT.ToString();
                            rootObj.schs_detail.schs_dis_ports.Add(obj_port);
                        }
                    }
                }
            }
            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getTransactionCMCSActByID(string id)
        {
            SchsRootObject rootObj = new SchsRootObject();
            rootObj.schs_activitys = new List<SchsActivity>();
            List<CPAI_VESSEL_SCH_S_ACTIVITYS> dataAct = new CPAI_VESSEL_SCH_S_ACTIVITYS_DAL().GetActivityByDataID(id);
            if (dataAct != null)
            {
                foreach (var item in dataAct)
                {
                    SchsActivity obj_act = new SchsActivity();
                    obj_act.order = item.VASS_ORDER;
                    obj_act.activity = item.VASS_FK_VESSEL_ACTIVITY;
                    obj_act.activity_color = VesselActivityServiceModel.GetStatus_Color_RowID(item.VASS_FK_VESSEL_ACTIVITY, ConstantPrm.SYSTEMTYPE.CRUDE).Split('|')[1];
                    obj_act.date_start = Convert.ToDateTime(item.VASS_DATE_START).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                    if (item.VASS_DATE_END != null)
                    {
                        obj_act.date_end = Convert.ToDateTime(item.VASS_DATE_END).ToString("dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                    } 
                    
                    obj_act.@ref = item.VASS_REF;
                    obj_act.portName = item.VASS_PORT;
                    rootObj.schs_activitys.Add(obj_act);
                }
            }
            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getFreightCalculationByDocNo(string DocNo)
        {

            CPAI_FREIGHT_DATA obj_freight = new FREIGHT_DATA_DAL().GetFreightDataByDocNo(DocNo);
            SchFreight fght = new SchFreight();
            if (obj_freight != null)
            {
                List<FUNCTION_TRANSACTION> obj_trans = new FunctionTransactionDAL().findByTransactionId(obj_freight.FDA_ROW_ID);
                if (obj_trans != null)
                {
                    foreach (var item in obj_trans)
                    {
                        fght.row_id = item.FTX_TRANS_ID.Encrypt();
                        fght.req_trans_id = item.FTX_REQ_TRANS.Encrypt();
                    }
                }
            }
            return new JavaScriptSerializer().Serialize(fght);
        }

        public static string saveSPNote(string month_year, string note, string user)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    try
                    {
                        CPAI_VESSEL_SCHEDULE_SP_NOTE_DAL noteDal = new CPAI_VESSEL_SCHEDULE_SP_NOTE_DAL();
                        CPAI_VESSEL_SCHEDULE_SP_NOTE sp_note = new CPAI_VESSEL_SCHEDULE_SP_NOTE();
                        sp_note.VSSP_ROW_ID = Guid.NewGuid().ToString("N");
                        sp_note.VSSP_MONTH_YEAR = month_year;
                        sp_note.VSSP_FLAG = "N";
                        sp_note.VSSP_NOTE = note;
                        sp_note.VSSP_CREATED = DateTime.Now;
                        sp_note.VSSP_CREATED_BY = user;
                        sp_note.VSSP_UPDATED = DateTime.Now;
                        sp_note.VSSP_UPDATED_BY = user;

                        noteDal.UpdateAllFlags(month_year, context);
                        noteDal.Save(sp_note, context);
                         
                        DateTime dtNow = DateTime.Now;
                        CPAI_VESSEL_SCHEDULE_DAL scheDAL = new CPAI_VESSEL_SCHEDULE_DAL();
                        CPAI_VESSEL_SCHEDULE dataSCHE = new CPAI_VESSEL_SCHEDULE();
                        //dataSCHE = context.CPAI_VESSEL_SCHEDULE.SingleOrDefault(a => a.VSD_ROW_ID == TransID);
                        //if (dataSCHE != null)
                        //{
                        //    dataSCHE.VSD_UPDATED_DATE = dtNow;
                        //    dataSCHE.VSD_UPDATED_BY = user;
                        //    scheDAL.Update(dataSCHE, context);
                        //}
                    }
                    catch (Exception ex)
                    {
                        return "F";
                    }
                }
            }
            catch (Exception ex)
            {
                return "F";
            }

            return "T";
        }

        public static string getSPNote(string month_year)
        {
            CPAI_VESSEL_SCHEDULE_SP_NOTE_DAL noteDal = new CPAI_VESSEL_SCHEDULE_SP_NOTE_DAL();
            CPAI_VESSEL_SCHEDULE_SP_NOTE sp_note = noteDal.GetLastest(month_year);
            return sp_note.VSSP_NOTE;
        }

        public static SchDetail getScheduler(string id)
        {
            using (var context = new EntityCPAIEngine())
            {
                var _schs = context.CPAI_VESSEL_SCHEDULE.Where(x => x.VSD_ROW_ID.ToUpper() == id.ToUpper()).FirstOrDefault();
                SchDetail data = new SchDetail();
                data.schedule_type = _schs.VSD_SCH_TYPE;
                data.schedule_type_detail = _schs.VSD_DETAIL;
                data.vendor_name = new MT_CUST_CONTROL_DAL().GetShotName(_schs.VSD_FK_CUST);
                data.date_start = Convert.ToDateTime(_schs.VSD_DATE_START).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if(_schs.VSD_DATE_END != null)
                {
                    data.date_end = Convert.ToDateTime(_schs.VSD_DATE_END).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                    data.date_end = "-";
                }                
                data.color = _schs.VSD_COLOR;
                data.tentative = _schs.VSD_TENTATIVE_FLAG == "Y" ? true : false;
                data.note = _schs.VSD_NOTE;
                data.UpdateBy = _schs.VSD_UPDATED_BY;
                data.updateDate = _schs.VSD_UPDATED_DATE.ToString("dd/MM/yyyy HH:mm");
                return data;
            }
        }

        public static string getSchedulerNameColorById(string id, bool type)
        {
            using (var context = new EntityCPAIEngine())
            {
                string data = "";
                var _schs = context.CPAI_VESSEL_SCHEDULE.Where(x => x.VSD_ROW_ID.ToUpper() == id.ToUpper()).FirstOrDefault();
                if (type)
                {
                    if (!string.IsNullOrEmpty(_schs.VSD_DETAIL))
                        data = _schs.VSD_DETAIL;

                }
                else
                {
                    if (!string.IsNullOrEmpty(_schs.VSD_DETAIL))
                        data = _schs.VSD_COLOR;
                }
                return data;
            }
        }

        public static List<SchCargo> getCargoList(string id)
        {
            List<CPAI_VESSEL_SCHEDULE_CARGO> list_cargo = new CPAI_VESSEL_SCHEDULE_CARGO_DAL().GetCargoByDataID(id).OrderBy(o => o.VSCG_ORDER).ToList();
            List<SchCargo> cargo = new List<SchCargo>();
            foreach (var v in list_cargo)
            {
                cargo.Add(new SchCargo
                {
                    order = v.VSCG_ORDER,
                    cargo = v.VSCG_FK_MT_MATERIALS,
                    qty = v.VSCG_QUANTITY,
                    unit = v.VSCG_UNIT,
                    others = v.VSCG_OTHERS
                });
            }
            return cargo;
        }

        public static List<SchLoadPorts> getLoadPortList(string id)
        {
            List<CPAI_VESSEL_SCHEDULE_PORT> list_port = new CPAI_VESSEL_SCHEDULE_PORT_DAL().GetPortByDataID(id).Where(x => x.VSP_PORT_TYPE == "L").ToList().OrderBy(o => o.VSP_ORDER_PORT).ToList();
            List<SchLoadPorts> port = new List<SchLoadPorts>();
            foreach (var v in list_port)
            {
                port.Add(new SchLoadPorts
                {
                    order = v.VSP_ORDER_PORT,
                    port = v.VSP_PORT_NAME
                });
            }
            return port;
        }

        public static List<SchDisPorts> getDischargePortList(string id)
        {
            List<CPAI_VESSEL_SCHEDULE_PORT> list_port = new CPAI_VESSEL_SCHEDULE_PORT_DAL().GetPortByDataID(id).Where(x => x.VSP_PORT_TYPE == "D").ToList().OrderBy(o => o.VSP_ORDER_PORT).ToList();
            List<SchDisPorts> port = new List<SchDisPorts>();
            foreach (var v in list_port)
            {
                port.Add(new SchDisPorts
                {
                    order = v.VSP_ORDER_PORT,
                    port = v.VSP_PORT_NAME
                });
            }
            return port;
        }

        public static string GetJsonExcel(string month_year_from, string month_year_to)
        {

            RootObjectExcel rootObjExcel = new RootObjectExcel();
            rootObjExcel.sch_vessel_excel = new List<SchVesselExcel>();
            rootObjExcel.sch_charterer_excel = new List<SchChartererExcel>();

            CPAI_VESSEL_SCHEDULE_DAL dataDal = new CPAI_VESSEL_SCHEDULE_DAL();
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(CPAIConstantUtil.PROJECT_NAME_SPACE, CPAIConstantUtil.ACTIVE, "SCH_P").OrderBy(x => x.VEH_ID).ToList().OrderBy(o => o.VEH_ID).ToList();

            var monthyearfrom = ShareFn.ConvertStringDateFormatToDatetime(month_year_from);
            var monthyearto = ShareFn.ConvertStringDateFormatToDatetime(month_year_to);

            List<CPAI_VESSEL_SCHEDULE> data = dataDal.GetDataForJsonExcel(monthyearfrom, monthyearto).OrderBy(o => o.VSD_FK_VEHICLE).ToList();

            foreach (var v in mt_vehicle)
            {
                SchVesselExcel vessel = new SchVesselExcel();
                vessel.sch_activity_excel = new List<SchActivityExcel>();
                vessel.vessel = string.Format("{0} ({1})", v.VEH_VEH_TEXT, v.VEH_VEHICLE);

                var ttt = data.Where(w => w.VSD_FK_VEHICLE == v.VEH_ID).ToList();
                foreach (var t in ttt)
                {
                    DateTime date_time_from = Convert.ToDateTime(t.VSD_DATE_START);
                    DateTime date_time_to = Convert.ToDateTime(t.VSD_DATE_END);

                    string tNameTemp = "";
                    int numberOfDays = (date_time_to - date_time_from).Days;
                    if (numberOfDays <= 2)
                    {
                        tNameTemp = string.Format("{0} - {1}", Convert.ToDateTime(t.VSD_LAYCAN_FROM).ToString("dd/MM", System.Globalization.CultureInfo.InvariantCulture), Convert.ToDateTime(t.VSD_LAYCAN_TO).ToString("dd/MM", System.Globalization.CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        tNameTemp = string.Format("{0} - {1}", Convert.ToDateTime(t.VSD_LAYCAN_FROM).ToString("dd MMM", System.Globalization.CultureInfo.InvariantCulture), Convert.ToDateTime(t.VSD_LAYCAN_TO).ToString("dd MMM", System.Globalization.CultureInfo.InvariantCulture)) + "\n" + new MT_CUST_CONTROL_DAL().GetShotName(t.VSD_FK_CUST);
                    }

                    string t_color = "#FFFFFF";
                    if (t.VSD_SCH_TYPE.ToUpper() == "SCHEDULE")
                    {
                        string t_color_temp = GetColorCust(t.VSD_FK_CUST);
                        if (!string.IsNullOrEmpty(t_color_temp))
                        {
                            if (t.VSD_TENTATIVE_FLAG == null || t.VSD_TENTATIVE_FLAG == "N")
                            {
                                t_color = t_color_temp;
                            }
                            else
                            {
                                t_color = "#848484";
                            }
                        }
                    }
                    else
                    {
                        t_color = t.VSD_COLOR;
                        tNameTemp = "";
                    }

                    string date_start_temp = "";
                    string date_end_temp = "";

                    if (date_time_from.ToString("MM/yyy") == month_year_from.Substring(3) && date_time_to.ToString("MM/yyy") == month_year_to.Substring(3))
                    {
                        date_start_temp = date_time_from.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        date_end_temp = date_time_to.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        if (date_time_from < ShareFn.ConvertStringDateFormatToDatetime(month_year_from) && date_time_to > ShareFn.ConvertStringDateFormatToDatetime(month_year_to))
                        {
                            date_start_temp = month_year_from;
                            date_end_temp = month_year_to;
                        }
                        else if (date_time_from <= ShareFn.ConvertStringDateFormatToDatetime(month_year_to) && date_time_to > ShareFn.ConvertStringDateFormatToDatetime(month_year_to))
                        {
                            date_start_temp = date_time_from.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            date_end_temp = month_year_to;
                        }
                        else if (date_time_to < ShareFn.ConvertStringDateFormatToDatetime(month_year_to))
                        {
                            date_start_temp = month_year_from;
                            date_end_temp = date_time_to.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        }
                    }

                    SchActivityExcel act = new SchActivityExcel();
                    act.name = tNameTemp;
                    act.color = t_color;
                    act.date_start = date_start_temp;
                    act.date_end = date_end_temp;
                    vessel.sch_activity_excel.Add(act);
                }
                rootObjExcel.sch_vessel_excel.Add(vessel);
            }

            SchChartererExcel cht0 = new SchChartererExcel();
            cht0.color = "#FFFF00";
            cht0.name = "Tank Cleaning";
            rootObjExcel.sch_charterer_excel.Add(cht0);

            SchChartererExcel cht1 = new SchChartererExcel();
            cht1.color = "#FF0000";
            cht1.name = "Maintenance";
            rootObjExcel.sch_charterer_excel.Add(cht1);

            SchChartererExcel cht2 = new SchChartererExcel();
            cht2.color = "#000000";
            cht2.name = "Off-Hire";
            rootObjExcel.sch_charterer_excel.Add(cht2);

            var vessel_group = data.OrderBy(o => o.VSD_FK_CUST).GroupBy(g => g.VSD_FK_CUST).ToList();
            foreach (var rt in vessel_group)
            {
                if (rt.Key != null)
                {
                    SchChartererExcel cht = new SchChartererExcel();
                    cht.color = GetColorCust(rt.Key);
                    cht.name = new MT_CUST_CONTROL_DAL().GetShotName(rt.Key);
                    rootObjExcel.sch_charterer_excel.Add(cht);
                }
            }

            var vessel_other = data.Where(v => v.VSD_SCH_TYPE == "Others").ToList();
            foreach (var rt in vessel_other)
            {
                SchChartererExcel cht = new SchChartererExcel();
                cht.color = rt.VSD_COLOR;
                cht.name = rt.VSD_DETAIL;
                rootObjExcel.sch_charterer_excel.Add(cht);
            }

            rootObjExcel.note = getSchedulerDetail(month_year_from.SplitWord("/")[1] + "_" + month_year_from.SplitWord("/")[2]);
            rootObjExcel.sp_note = getSPNote(int.Parse(month_year_from.SplitWord("/")[1]) + "/" + int.Parse(month_year_from.SplitWord("/")[2]));

            return new JavaScriptSerializer().Serialize(rootObjExcel);
        }

        public static string GetJsonActivityExcel(string trans_id)
        {
            RootObjectActivityExcel act = new RootObjectActivityExcel();
            act.sch_act_time_line_excel = new List<SchActTimeLineExcel>();

            using (var context = new EntityCPAIEngine())
            {
                var query_main = (from s in context.CPAI_VESSEL_SCHEDULE
                                  join v in context.MT_VEHICLE on s.VSD_FK_VEHICLE equals v.VEH_ID
                                  join c in context.MT_CUST_DETAIL on s.VSD_FK_CUST equals c.MCD_ROW_ID
                                  where s.VSD_ROW_ID == trans_id
                                  select new
                                  {
                                      VESSEL_ID = s.VSD_FK_VEHICLE,
                                      VESSEL_NAME = v.VEH_VEH_TEXT,
                                      CHARTER_ID = s.VSD_FK_CUST,
                                      CHARTER_NAME = c.MCD_NAME_1,
                                      CHARTER_COLOR = "",
                                      PERIOD_FROM = s.VSD_DATE_START,
                                      PERIOD_TO = s.VSD_DATE_END,
                                      LAYCAN_FROM = s.VSD_LAYCAN_FROM,
                                      LAYCAN_TO = s.VSD_LAYCAN_TO
                                  }).FirstOrDefault();

                var query_sub = (from s in context.CPAI_VESSEL_SCHEDULE_ACTIVITYS
                                 join a in context.MT_VESSEL_ACTIVITY on s.VAS_FK_VESSEL_ACTIVITY equals a.MVA_ROW_ID
                                 join c in context.MT_VESSEL_ACTIVITY_CONTROL on a.MVA_ROW_ID equals c.MAC_ROW_ID
                                 where s.VAS_FK_VESSEL_SCHEDULE == trans_id
                                 select new
                                 {
                                     ACT_ID = s.VAS_ROW_ID,
                                     ACT_NAME = a.MVA_STANDARD_PHRASE_EN,
                                     ACT_COLOR = c.MAC_COLOR_CODE,
                                     ACT_START = s.VAS_START_DATE,
                                     ACT_END = s.VAS_END_DATE
                                 }).ToList();

                act.vessel_id = query_main.VESSEL_ID;
                act.vessel_name = query_main.VESSEL_NAME;
                act.charterer_id = query_main.CHARTER_ID;
                act.charterer_name = query_main.CHARTER_NAME;
                act.charterer_color = query_main.CHARTER_COLOR;
                act.period = ShareFn.ConvertDateTimeToDateStringFormat(query_main.PERIOD_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(query_main.PERIOD_TO, "dd/MM/yyyy");
                act.laycan = ShareFn.ConvertDateTimeToDateStringFormat(query_main.LAYCAN_FROM, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(query_main.LAYCAN_TO, "dd/MM/yyyy");

                foreach (var v in query_sub)
                {
                    act.sch_act_time_line_excel.Add(new SchActTimeLineExcel
                    {
                        act_id = v.ACT_ID,
                        act_name = v.ACT_NAME,
                        act_color = v.ACT_COLOR,
                        time_start = ShareFn.ConvertDateTimeToDateStringFormat(v.ACT_START, "dd/MM/yyyy HH:mm"),
                        time_end = ShareFn.ConvertDateTimeToDateStringFormat(v.ACT_END, "dd/MM/yyyy HH:mm")
                    });
                }
                return new JavaScriptSerializer().Serialize(act);
            }
        }

        public static string GetColorCust(string cust_id)
        {
            var color = string.Empty;

            MT_CUST_CONTROL_DAL service = new MT_CUST_CONTROL_DAL();
            color = service.GetColor(cust_id);
            return color;
        }

        public static string getSchedulerDetail(string month)
        {
            string tNote = "";

            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(CPAIConstantUtil.PROJECT_NAME_SPACE, CPAIConstantUtil.ACTIVE, "SCH_P").OrderBy(x => x.VEH_ID).ToList().OrderBy(o => o.VEH_ID).ToList();

            int n_month = int.Parse(month.SplitWord("_")[0]);
            int n_year = int.Parse(month.SplitWord("_")[1]);

            var monthyearfrom = new DateTime(n_year, n_month, 1);
            var monthyearto = new DateTime(n_year, n_month, DateTime.DaysInMonth(n_year, n_month));

            List<CPAI_VESSEL_SCHEDULE> data = new CPAI_VESSEL_SCHEDULE_DAL().GetDataForJsonExcel(monthyearfrom, monthyearto).OrderBy(o => o.VSD_FK_VEHICLE).ToList().OrderBy(o => o.VSD_DATE_START).ToList();

            foreach (var v in mt_vehicle)
            {
                tNote += "\n" + v.VEH_VEHICLE + "\n";

                var ttt = data.Where(w => w.VSD_FK_VEHICLE == v.VEH_ID).ToList();
                foreach (var t in ttt)
                {
                    if (t.VSD_SCH_TYPE.ToUpper() == "SCHEDULE")
                    {
                        tNote += string.Format("{0} - {1}", Convert.ToDateTime(t.VSD_LAYCAN_FROM).ToString("dd MMM", System.Globalization.CultureInfo.InvariantCulture), Convert.ToDateTime(t.VSD_LAYCAN_TO).ToString("dd MMM", System.Globalization.CultureInfo.InvariantCulture));
                        tNote += " : " + new MT_CUST_CONTROL_DAL().GetShotName(t.VSD_FK_CUST) + ", ";

                        var cargo = getCargoList(t.VSD_ROW_ID);
                        var load_port = getLoadPortList(t.VSD_ROW_ID);
                        var dis_port = getDischargePortList(t.VSD_ROW_ID);

                        for (int i = 0; i < cargo.Count; i++)
                        {
                            if (i == (cargo.Count - 1))
                            {
                                tNote += MaterialsServiceModel.GetName(cargo[i].cargo) + " " + Convert.ToDecimal(cargo[i].qty).ToString("#,##0") + " " + (cargo[i].unit == "OTHER" ? cargo[i].others : cargo[i].unit) + ", "; ;
                            }
                            else
                            {
                                tNote += MaterialsServiceModel.GetName(cargo[i].cargo) + " " + Convert.ToDecimal(cargo[i].qty).ToString("#,##0") + " " + (cargo[i].unit == "OTHER" ? cargo[i].others : cargo[i].unit) + " and ";
                            }
                        }

                        for (int i = 0; i < load_port.Count; i++)
                        {

                            if (i == (load_port.Count - 1))
                            {
                                tNote += load_port[i].port + " - ";
                            }
                            else
                            {
                                tNote += load_port[i].port + " and ";
                            }
                        }

                        for (int i = 0; i < dis_port.Count; i++)
                        {

                            if (i == (dis_port.Count - 1))
                            {
                                tNote += dis_port[i].port + " ";
                            }
                            else
                            {
                                tNote += dis_port[i].port + " and ";
                            }
                        }

                        tNote += (string.IsNullOrEmpty(t.VSD_NOTE) ? "" : string.Format("({0})", t.VSD_NOTE)) + "\n";
                    }
                    else
                    {
                        tNote += string.Format("{0} - {1}", Convert.ToDateTime(t.VSD_DATE_START).ToString("dd MMM", System.Globalization.CultureInfo.InvariantCulture), Convert.ToDateTime(t.VSD_DATE_END).ToString("dd MMM", System.Globalization.CultureInfo.InvariantCulture));
                        tNote += " : " + (t.VSD_SCH_TYPE == "Others" ? t.VSD_DETAIL : t.VSD_SCH_TYPE);
                        tNote += (string.IsNullOrEmpty(t.VSD_NOTE) ? "" : string.Format(", ({0})", t.VSD_NOTE)) + "\n";
                    }
                }
            }

            return tNote;
        }

        public static string getAttFileByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL fileDal = new CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL();
            List<CPAI_VESSEL_SCHEDULE_M_ATTACH> extFile = fileDal.GetFileByDataID(id);
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].VSMF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        public static int getCountAttachFile(string trans_id)
        {
            CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL attach = new CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL();
            return attach.GetFileByDataID(trans_id).Count;
        }
    }
}