﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALPCF;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class SwapServiceModel
    {
        CultureInfo provider = new CultureInfo("en-US");
        string format = "dd/MM/yyyy";

        public ReturnValue SearchData(ref SwapViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var sTripNo = !string.IsNullOrEmpty(pModel.Swap_Search.sTripNo) ? pModel.Swap_Search.sTripNo.ToUpper() : string.Empty;
                    var sProduce = !string.IsNullOrEmpty(pModel.Swap_Search.sProduct) ? pModel.Swap_Search.sProduct.ToUpper() : string.Empty;
                    var sCustomer = !string.IsNullOrEmpty(pModel.Swap_Search.sCustomer) ? pModel.Swap_Search.sCustomer.ToUpper() : string.Empty;
                    DateTime? sDeliveryDate_Start = null;
                    DateTime? sDeliveryDate_End = null;

                    if (!string.IsNullOrEmpty(pModel.Swap_Search.sDeliveryDate))
                    {
                        string[] sDeliveryDate = pModel.Swap_Search.sDeliveryDate.Split(new[] { " to " }, StringSplitOptions.None);
                        sDeliveryDate_Start = DateTime.ParseExact(sDeliveryDate[0], format, provider);
                        sDeliveryDate_End = DateTime.ParseExact(sDeliveryDate[1], format, provider);
                    }

                    var query = (from sw in context.SWAP_CRUDE
                                 join mat_value in context.MT_MATERIALS on sw.SWC_CRUDE_TYPE_ID equals mat_value.MET_NUM into mat_join
                                 from mat in mat_join.DefaultIfEmpty()
                                 join veh_value in context.MT_VEHICLE on sw.SWC_VESSEL_ID equals veh_value.VEH_ID into veh_join
                                 from veh in veh_join.DefaultIfEmpty()
                                 where ((!sDeliveryDate_Start.HasValue) || (sw.SWC_DELIVERY_DATE >= sDeliveryDate_Start))
                                 && ((!sDeliveryDate_End.HasValue) || (sw.SWC_DELIVERY_DATE <= sDeliveryDate_End))
                                 && ((sw.SWC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                 && ((sw.SWC_CRUDE_TYPE_ID.ToUpper().Equals(sProduce.ToUpper())) || (String.IsNullOrEmpty(sProduce)))
                                 && ((sw.SWC_SOLDTO.ToUpper().Equals(sCustomer.ToUpper())) || (String.IsNullOrEmpty(sCustomer)))
                                 select new
                                 {
                                     sw,
                                     mat,
                                     veh,
                                     sols = (from sol in context.MT_CUST_DETAIL
                                            where sol.MCD_FK_CUS == sw.SWC_SOLDTO && sol.MCD_NATION == "I"
                                            select sol),
                                     shps = (from shp in context.MT_CUST_DETAIL
                                            where shp.MCD_FK_CUS == sw.SWC_SHIPTO && shp.MCD_NATION == "I"
                                            select shp),
                                     sp_pros = (from sp in context.SWAP_PRICE
                                               where sp.SWP_TRIP_NO == sw.SWC_TRIP_NO &&
                                                     sp.SWP_ITEM_NO == sw.SWC_ITEM_NO &&
                                                     sp.SWP_CRUDE_TYPE_ID == sw.SWC_CRUDE_TYPE_ID &&
                                                     sp.SWP_TYPE == "1"
                                               orderby sp.SWP_NUM   
                                               select sp),
                                     sp_acts = (from sp in context.SWAP_PRICE
                                               where sp.SWP_TRIP_NO == sw.SWC_TRIP_NO &&
                                                     sp.SWP_ITEM_NO == sw.SWC_ITEM_NO &&
                                                     sp.SWP_CRUDE_TYPE_ID == sw.SWC_CRUDE_TYPE_ID &&
                                                     sp.SWP_TYPE == "2"
                                               orderby sp.SWP_NUM
                                               select sp)
                                 });

                    if (query != null)
                    {
                        pModel.Swap_Search.SearchData = new List<SwapViewModel_SearchData>();
                        foreach (var item in query.ToList())
                        {
                            var sp_pro = item.sp_pros != null && item.sp_pros.ToList() != null ? item.sp_pros.ToList().LastOrDefault() : null;
                            var sp_act = item.sp_acts != null && item.sp_acts.ToList() != null ? item.sp_acts.ToList().LastOrDefault() : null;
                            var sol = item.sols != null && item.sols.ToList() != null ? item.sols.ToList().LastOrDefault() : null;
                            var shp = item.shps != null && item.shps.ToList() != null ? item.shps.ToList().LastOrDefault() : null;
                            SwapViewModel_SearchData temp = new SwapViewModel_SearchData();
                            temp.dTripNo = item.sw != null ? item.sw.SWC_TRIP_NO : "";
                            temp.dItemNo = item.sw != null ? item.sw.SWC_ITEM_NO : "";
                            temp.dCrudeTypeID = item.sw != null ? item.sw.SWC_CRUDE_TYPE_ID : "";
                            temp.dVessel = item.veh != null ? item.veh.VEH_VEHICLE : "";
                            temp.dCustomer = sol != null ? sol.MCD_NAME_1 : "";
                            temp.dShipTo = shp != null ? shp.MCD_NAME_1 : "";
                            temp.dDeliveryDate = item.sw != null && item.sw.SWC_DELIVERY_DATE.HasValue ? item.sw.SWC_DELIVERY_DATE.Value.ToString(format, provider) : "";
                            temp.dPlant = item.sw != null ? item.sw.SWC_PLANT : "";
                            temp.dProduct = item.mat != null ? item.mat.MET_MAT_DES_ENGLISH : "";
                            temp.dVolumeBBL = (item.sw != null && item.sw.SWC_VOLUME_BBL.HasValue ? item.sw.SWC_VOLUME_BBL.Value : Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dVolumeMT = (item.sw != null && item.sw.SWC_VOLUME_MT.HasValue ? item.sw.SWC_VOLUME_MT.Value : Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dVolumeLITE = (item.sw != null && item.sw.SWC_VOLUME_LITE.HasValue ? item.sw.SWC_VOLUME_LITE.Value : Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dPONo = item.sw != null ? item.sw.SWC_PO_NO : "";
                            temp.dSONo = item.sw != null ? item.sw.SWC_SALEORDER : "";
                            temp.dPONo = item.sw != null ? item.sw.SWC_DO_NO : "";
                            temp.dMemo = item.sw != null ? item.sw.SWC_MEMO : "";
                            temp.dProPrice = (sp_pro != null && sp_pro.SWP_PRICE.HasValue ? sp_pro.SWP_PRICE.Value : Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dActurePrice = (sp_act != null && sp_act.SWP_PRICE.HasValue ? sp_act.SWP_PRICE.Value : Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dProUnit = sp_pro != null ? sp_pro.SWP_UNIT_ID : "";
                            temp.dActureUnit = sp_act != null ? sp_act.SWP_UNIT_ID : "";
                            pModel.Swap_Search.SearchData.Add(temp);
                        }
                        rtn.Status = true;
                    }
                    else
                    {
                        pModel.Swap_Search.SearchData = new List<SwapViewModel_SearchData>();
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                catch (Exception ex)
                {
                    rtn.Status = false;
                    rtn.Message = ex.Message;
                }
            }
            return rtn;
        }

        public ReturnValue SaveData(ref SwapViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            SWAP_CRUDE_DAL dal = new SWAP_CRUDE_DAL();
                            if (pModel.Swap_Detail != null)
                            {
                                foreach(var item in pModel.Swap_Detail)
                                {
                                    SWAP_CRUDE ent = new SWAP_CRUDE();
                                    List<SWAP_PRICE> ent_details = new List<SWAP_PRICE>();
                                    ent.SWC_TRIP_NO = item.trip_no;
                                    ent.SWC_ITEM_NO = item.item_no;
                                    ent.SWC_PO_NO = item.po_no;
                                    ent.SWC_VESSEL_ID = item.vessel_id;
                                    ent.SWC_SOLDTO = item.sold_to;
                                    ent.SWC_SHIPTO = item.ship_to;
                                    ent.SWC_PLANT = item.plant;
                                    ent.SWC_CRUDE_TYPE_ID = item.crude_type_id;
                                    ent.SWC_PRICE_PER = item.price_per;
                                    ent.SWC_UNIT_ID = item.unit_id;
                                    ent.SWC_UNIT_TOTAL = item.unit_total;
                                    ent.SWC_TYPE = item.type;
                                    ent.SWC_STATUS = item.status;
                                    ent.SWC_RELEASE = item.release;
                                    ent.SWC_TANK = item.tank;
                                    ent.SWC_PRICE_RELEASE = item.price_release;
                                    ent.SWC_REMARK = item.remark;
                                    ent.SWC_DISTRI_CHANN = item.distri_chann;
                                    ent.SWC_DATE_SAP = item.date_sap;
                                    ent.SWC_SALEORDER = item.sale_order;
                                    ent.SWC_MEMO = item.memo;
                                    ent.SWC_FI_DOC = item.fi_doc;
                                    ent.SWC_DO_NO = item.do_no;
                                    ent.SWC_STATUS_TO_SAP = item.status_to_sap;
                                    ent.SWC_SEQ = item.seq;
                                    ent.SWC_INCOTERM_TYPE = item.incoterm_type;
                                    ent.SWC_STORAGE_LOCATION = item.storage_location;
                                    ent.SWC_SUPPLIER = item.supplier;
                                    ent.SWC_DISTRI_CHANN_SAP = item.distri_chann_sap;
                                    ent.SWC_VAT_NONVAT = item.vat_nonvat;
                                    ent.SWC_UPDATE_DO = item.update_do;
                                    ent.SWC_UPDATE_GI = item.update_gi;
                                    ent.SWC_FI_DOC_REVERSE = item.fi_doc_reverse;
                                    if (String.IsNullOrEmpty(item.delivery_date)) { ent.SWC_DELIVERY_DATE = null; } else { ent.SWC_DELIVERY_DATE = DateTime.ParseExact(item.delivery_date, format, provider); }
                                    if (String.IsNullOrEmpty(item.volumn_bbl)) { ent.SWC_VOLUME_BBL = null; } else { ent.SWC_VOLUME_BBL = Decimal.Parse(item.volumn_bbl); }
                                    if (String.IsNullOrEmpty(item.volumn_mt)) { ent.SWC_VOLUME_MT = null; } else { ent.SWC_VOLUME_MT = Decimal.Parse(item.volumn_mt); }
                                    if (String.IsNullOrEmpty(item.volumn_lite)) { ent.SWC_VOLUME_LITE = null; } else { ent.SWC_VOLUME_LITE = Decimal.Parse(item.volumn_lite); }
                                    if (String.IsNullOrEmpty(item.outturn_bbl)) { ent.SWC_OUTTURN_BBL = null; } else { ent.SWC_OUTTURN_BBL = Decimal.Parse(item.outturn_bbl); }
                                    if (String.IsNullOrEmpty(item.outturn_lite)) { ent.SWC_OUTTURN_LITE = null; } else { ent.SWC_OUTTURN_LITE = Decimal.Parse(item.outturn_lite); }
                                    if (String.IsNullOrEmpty(item.outturn_mt)) { ent.SWC_OUTTURN_MT = null; } else { ent.SWC_OUTTURN_MT = Decimal.Parse(item.outturn_mt); }
                                    if (String.IsNullOrEmpty(item.price)) { ent.SWC_PRICE = null; } else { ent.SWC_PRICE = Decimal.Parse(item.price); }
                                    if (String.IsNullOrEmpty(item.exchange_rate)) { ent.SWC_EXCHANGE_RATE = null; } else { ent.SWC_EXCHANGE_RATE = Decimal.Parse(item.exchange_rate); }
                                    if (String.IsNullOrEmpty(item.total)) { ent.SWC_TOTAL = null; } else { ent.SWC_TOTAL = Decimal.Parse(item.total); }
                                    if (String.IsNullOrEmpty(item.temp)) { ent.SWC_TEMP = null; } else { ent.SWC_TEMP = Decimal.Parse(item.temp); }
                                    if (String.IsNullOrEmpty(item.density)) { ent.SWC_DENSITY = null; } else { ent.SWC_DENSITY = Decimal.Parse(item.density); }

                                    foreach (var pModelDetail in item.prices)
                                    {
                                        SWAP_PRICE ent_detail = new SWAP_PRICE();
                                        ent_detail.SWP_TRIP_NO = pModelDetail.trip_no;
                                        ent_detail.SWP_ITEM_NO = pModelDetail.item_no;
                                        ent_detail.SWP_CRUDE_TYPE_ID = pModelDetail.crude_type_id;
                                        ent_detail.SWP_TYPE = pModelDetail.type;
                                        ent_detail.SWP_UNIT_ID = pModelDetail.unit_id;
                                        ent_detail.SWP_RELEASE = pModelDetail.release;
                                        ent_detail.SWP_UNIT_INV = pModelDetail.unit_inv;
                                        ent_detail.SWP_MEMO = pModelDetail.memo;
                                        ent_detail.SWP_NUM = Decimal.Parse(pModelDetail.num);
                                        if (String.IsNullOrEmpty(pModelDetail.price)) { ent_detail.SWP_PRICE = null; } else { ent_detail.SWP_PRICE = Decimal.Parse(pModelDetail.price); }
                                        if (String.IsNullOrEmpty(pModelDetail.date_price)) { ent_detail.SWP_DATE_PRICE = null; } else { ent_detail.SWP_DATE_PRICE = DateTime.ParseExact(pModelDetail.date_price, format, provider); }
                                        if (String.IsNullOrEmpty(pModelDetail.invoice)) { ent_detail.SWP_INVOICE = null; } else { ent_detail.SWP_INVOICE = Decimal.Parse(pModelDetail.invoice); }
                                        ent_details.Add(ent_detail);
                                    }

                                    var dal_exist = dal.Get(ent, context);
                                    if (dal_exist == null)
                                    {
                                        ent.SWC_CREATE_DATE = DateTime.Now.Date;
                                        ent.SWC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");
                                        ent.SWC_CREATE_BY = Const.User.UserName;
                                        dal.Save(ent, ent_details, context);

                                    }
                                    else
                                    {
                                        ent.SWC_LAST_MODIFY_DATE = DateTime.Now.Date;
                                        ent.SWC_LAST_MODIFY_TIME = DateTime.Now.ToString("HH:mm:ss");
                                        ent.SWC_LAST_MODIFY_BY = Const.User.UserName;
                                        dal.Update(ent, ent_details, context);
                                    }
                                }
                                
                            }
                            
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue LoadData(ref SwapViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    SWAP_CRUDE_DAL dal = new SWAP_CRUDE_DAL();
                    foreach(var item in pModel.Swap_Detail)
                    {
                        SWAP_CRUDE ent = new SWAP_CRUDE();
                        List<SWAP_PRICE> ent_details = new List<SWAP_PRICE>();
                        //SWAP_CRUDE tmp = new SWAP_CRUDE();
                        ent.SWC_TRIP_NO = item.trip_no;
                        ent.SWC_ITEM_NO = item.item_no;
                        ent.SWC_CRUDE_TYPE_ID = item.crude_type_id;
                        //ent.Add(tmp);
                        dal.Load(ref ent, ref ent_details, context);

                        if (ent != null)
                        {
                            item.trip_no = ent.SWC_TRIP_NO;
                            item.item_no = ent.SWC_ITEM_NO;
                            item.po_no = ent.SWC_PO_NO;
                            item.vessel_id = ent.SWC_VESSEL_ID;
                            item.sold_to = ent.SWC_SOLDTO;
                            item.ship_to = ent.SWC_SHIPTO;
                            item.delivery_date = ent.SWC_DELIVERY_DATE.HasValue ? ent.SWC_DELIVERY_DATE.Value.ToString(format, provider) : "";
                            item.plant = ent.SWC_PLANT;
                            item.crude_type_id = ent.SWC_CRUDE_TYPE_ID;
                            item.volumn_bbl = (ent.SWC_VOLUME_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.volumn_mt = (ent.SWC_VOLUME_MT ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.volumn_lite = (ent.SWC_VOLUME_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.outturn_bbl = (ent.SWC_OUTTURN_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.outturn_mt = (ent.SWC_OUTTURN_MT ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.outturn_lite = (ent.SWC_OUTTURN_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.price_per = ent.SWC_PRICE_PER;
                            item.price = (ent.SWC_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.unit_id = ent.SWC_UNIT_ID;
                            item.exchange_rate = (ent.SWC_EXCHANGE_RATE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.total = (ent.SWC_TOTAL ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.unit_total = ent.SWC_UNIT_TOTAL;
                            item.type = ent.SWC_TYPE;
                            item.create_date = ent.SWC_CREATE_DATE.HasValue ? ent.SWC_CREATE_DATE.Value.ToString(format, provider) : "";
                            item.create_time = ent.SWC_CREATE_TIME;
                            item.create_by = ent.SWC_CREATE_BY;
                            item.last_modify_date = ent.SWC_LAST_MODIFY_DATE.HasValue ? ent.SWC_LAST_MODIFY_DATE.Value.ToString(format, provider) : "";
                            item.last_modify_time = ent.SWC_LAST_MODIFY_TIME;
                            item.last_modify_by = ent.SWC_LAST_MODIFY_BY;
                            item.status = ent.SWC_STATUS;
                            item.release = ent.SWC_RELEASE;
                            item.tank = ent.SWC_TANK;
                            item.price_release = ent.SWC_PRICE_RELEASE;
                            item.remark = ent.SWC_REMARK;
                            item.distri_chann = ent.SWC_DISTRI_CHANN;
                            item.date_sap = ent.SWC_DATE_SAP;
                            item.sale_order = ent.SWC_SALEORDER;
                            item.memo = ent.SWC_MEMO;
                            item.fi_doc = ent.SWC_FI_DOC;
                            item.do_no = ent.SWC_DO_NO;
                            item.status_to_sap = ent.SWC_STATUS_TO_SAP;
                            item.seq = ent.SWC_SEQ;
                            item.incoterm_type = ent.SWC_INCOTERM_TYPE;
                            item.temp = (ent.SWC_TEMP ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.density = (ent.SWC_DENSITY ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            item.storage_location = ent.SWC_STORAGE_LOCATION;
                            item.supplier = ent.SWC_SUPPLIER;
                            item.distri_chann_sap = ent.SWC_DISTRI_CHANN_SAP;
                            item.vat_nonvat = ent.SWC_VAT_NONVAT;
                            item.update_do = ent.SWC_UPDATE_DO;
                            item.update_gi = ent.SWC_UPDATE_GI;
                            item.fi_doc_reverse = ent.SWC_FI_DOC_REVERSE;
                            item.prices = new List<SwapViewModel_Price>();
                            foreach (var ent_detail in ent_details)
                            {
                                SwapViewModel_Price pModelDetail = new SwapViewModel_Price();
                                pModelDetail.trip_no = ent_detail.SWP_TRIP_NO;
                                pModelDetail.item_no = ent_detail.SWP_ITEM_NO;
                                pModelDetail.crude_type_id = ent_detail.SWP_CRUDE_TYPE_ID;
                                pModelDetail.num = ent_detail.SWP_NUM.ToString();
                                pModelDetail.type = ent_detail.SWP_TYPE;
                                pModelDetail.price = (ent_detail.SWP_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                                pModelDetail.date_price = ent_detail.SWP_DATE_PRICE.HasValue ? ent_detail.SWP_DATE_PRICE.Value.ToString(format, provider) : "";
                                pModelDetail.unit_id = ent_detail.SWP_UNIT_ID;
                                pModelDetail.release = ent_detail.SWP_RELEASE;
                                pModelDetail.invoice = (ent_detail.SWP_INVOICE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                                pModelDetail.unit_inv = ent_detail.SWP_UNIT_INV;
                                pModelDetail.memo = ent_detail.SWP_MEMO;
                                item.prices.Add(pModelDetail);
                            }
                            rtn.Status = true;
                            rtn.Message = string.Empty;
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    rtn.Status = false;
                    rtn.Message = ex.Message;
                }
            }
            return rtn;
        }

        public ReturnValue ImportData(ref SwapViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var sTrip_no = !string.IsNullOrEmpty(pModel.Swap_Import.sTrip_no) ? pModel.Swap_Import.sTrip_no.ToUpper() : string.Empty;
                    var sCrude_type_id = !string.IsNullOrEmpty(pModel.Swap_Import.sCrude_type_id) ? pModel.Swap_Import.sCrude_type_id.ToUpper() : string.Empty;

                    var swaps =  (from sw in context.SWAP_CRUDE select sw).ToList();
                    var query = (from h in context.PCF_HEADER
                                 join m_value in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m_value.PMA_TRIP_NO into m_join
                                 from m in m_join
                                 join mt_value in context.MT_MATERIALS on m.PMA_MET_NUM equals mt_value.MET_NUM into mt_join
                                 from mt in mt_join.DefaultIfEmpty()
                                 join vh_value in context.MT_VEHICLE on h.PHE_VESSEL equals vh_value.VEH_ID into vh_join
                                 from vh in vh_join.DefaultIfEmpty()
                                 join sp_value in context.MT_VENDOR on m.PMA_SUPPLIER equals sp_value.VND_ACC_NUM_VENDOR into sp_join
                                 from sp in sp_join.DefaultIfEmpty()
                                 where h.PHE_STATUS == "ACTIVE"
                                    && ((h.PHE_TRIP_NO.ToUpper().Contains(sTrip_no.ToUpper())) || String.IsNullOrEmpty(sTrip_no))
                                    && ((m.PMA_MET_NUM.ToUpper().Equals(sCrude_type_id.ToUpper())) || (String.IsNullOrEmpty(sCrude_type_id)))
                                 select new
                                 {
                                     h,
                                     m,
                                     mt,
                                     vh,
                                     sp
                                 }).ToList();

                    if (query != null)
                    {
                        pModel.Swap_Import = pModel.Swap_Import ?? new SwapViewModel_Import();
                        pModel.Swap_Import.ImportData = new List<SwapViewModel_ImportData>();
                        foreach (var item in query)
                        {
                            if (swaps == null || !swaps.Where(swap => swap.SWC_TRIP_NO == item.h.PHE_TRIP_NO
                                                           && swap.SWC_ITEM_NO == item.m.PMA_ITEM_NO.ToString() 
                                                           && swap.SWC_CRUDE_TYPE_ID == item.m.PMA_MET_NUM).Any())
                            {
                                SwapViewModel_ImportData temp = new SwapViewModel_ImportData();
                                temp.dTrip_no = item.h != null ? item.h.PHE_TRIP_NO : string.Empty;
                                temp.dVessel_id = item.h != null ? item.h.PHE_VESSEL : string.Empty;
                                temp.dVessel_name = item.vh != null ? item.vh.VEH_VEH_TEXT : string.Empty;
                                temp.dItem_no = item.m != null ? item.m.PMA_ITEM_NO.ToString("0") : string.Empty;
                                temp.dCrude_type_id = item.m != null ? item.m.PMA_MET_NUM : string.Empty;
                                temp.dCrude_name = item.mt != null ? item.mt.MET_MAT_DES_ENGLISH : string.Empty;
                                temp.dSupplier_id = item.m != null ? item.m.PMA_SUPPLIER : string.Empty;
                                temp.dSupplier_name = item.m != null ? item.sp.VND_NAME1 : string.Empty;
                                temp.dBl_date = item.m != null && item.m.PMA_BL_DATE.HasValue ? item.m.PMA_BL_DATE.Value.ToString(format, provider) : string.Empty;
                                temp.dVolumn_bbl = item.m != null && item.m.PMA_VOLUME_BBL.HasValue ? item.m.PMA_VOLUME_BBL.ToString() : Convert.ToDecimal(0).ToString("#,##0.0000");
                                temp.dVolumn_mt = item.m != null && item.m.PMA_VOLUME_ML.HasValue ? item.m.PMA_VOLUME_ML.ToString() : Convert.ToDecimal(0).ToString("#,##0.0000");
                                pModel.Swap_Import.ImportData.Add(temp);
                            }             
                        }
                        rtn.Status = true;
                    }
                    else
                    {
                        pModel.Swap_Import.ImportData = new List<SwapViewModel_ImportData>();
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                catch (Exception ex)
                {
                    rtn.Status = false;
                    rtn.Message = ex.Message;
                }
            }
            return rtn;
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
            }
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static List<SelectListItem> getVehicle(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(PROJECT_NAME_SPACE, ACTIVE, type).OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), mt_vehicle.Select(x => x.VEH_ID).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getCustomer(string type = "PCF", bool isOptional = false, string message = "")
        {
            List<MT_CUST_DETAIL> mt_cust_detail = CustDetailDAL.GetCustDetail(PROJECT_NAME_SPACE, ACTIVE, type);
            return insertSelectListValue(mt_cust_detail.Select(x => x.MCD_NAME_1).ToList(), mt_cust_detail.Select(x => x.MCD_FK_CUS).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterial(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterialAll().OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getPlant(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_PLANT> mt_plant = MT_PLANT_DAL.GetPlant().ToList();
            return insertSelectListValue(mt_plant.Select(x => x.MPL_PLANT).ToList(), mt_plant.Select(x => x.MPL_PLANT).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getUnit()
        {
            string JsonD = MasterData.GetJsonMasterSetting("PCF_CLEAR_LINE");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);
            GlobalConfigCL dataList = (GlobalConfigCL)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigCL));
            List<SelectListItem> dList = new List<SelectListItem>();
            foreach (var item in dataList.PCF_UNIT_CAL)
            {
                dList.Add(new SelectListItem { Value = item.Code, Text = item.Name });
            }
            return dList;
        }

        public static List<SelectListItem> getUnitPrice()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "US4", Text = "US4" });
            dList.Add(new SelectListItem { Value = "TH4", Text = "TH4" });

            return dList;
        }

        public static List<SelectListItem> getUnitTotal()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "THB", Text = "THB" });
            dList.Add(new SelectListItem { Value = "USD", Text = "USD" });

            return dList;
        }

        public static List<SelectListItem> getPriceStatus()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "1", Text = "Provisional Price" });
            dList.Add(new SelectListItem { Value = "2", Text = "Final Price" });

            return dList;
        }

        public static List<SelectListItem> getUnitInvoice()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "THB", Text = "THB" });

            return dList;
        }

        public static List<SelectListItem> getSaleUnitType()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "BBL", Text = "BBL" });
            dList.Add(new SelectListItem { Value = "MT", Text = "MT" });
            dList.Add(new SelectListItem { Value = "L30", Text = "L30" });

            return dList;
        }

        public static List<SelectListItem> getInvoiceFigureType()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "B/L", Text = "B/L" });
            dList.Add(new SelectListItem { Value = "Outturn", Text = "Outturn" });

            return dList;
        }

    }
}