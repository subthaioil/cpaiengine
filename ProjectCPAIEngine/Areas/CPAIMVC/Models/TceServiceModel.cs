﻿using System;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class TceServiceModel
    {
        public static string getTransactionByID(string id)
        {
            TceRootObject rootObj = new TceRootObject();

            rootObj.tce_detail = new TceDetail();
         

            CPAI_TCE_WS_DAL dataDal = new CPAI_TCE_WS_DAL();
            CPAI_TCE_WS data = dataDal.GetByID(id);
            if (data != null)
            {
                rootObj.tce_detail.voy_no = data.CTW_VOY_NO;
                rootObj.tce_detail.chi_data_id = data.CTW_FK_CHI_DATA;
                rootObj.tce_detail.ship_name = data.CTW_FK_VEHICLE;
                rootObj.tce_detail.ship_broker = data.CTW_FK_VENDOR;
                rootObj.tce_detail.ship_owner = data.CTW_OWNER;
                rootObj.tce_detail.dem = data.CTW_DEM;
                rootObj.tce_detail.flat_rate = data.CTW_FLAT_RATE;
                rootObj.tce_detail.extra_cost = data.CTW_EXTRA_COST;
                rootObj.tce_detail.laycan_load_from = (data.CTW_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.CTW_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_detail.laycan_load_to = (data.CTW_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.CTW_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_detail.top_fixture_value = data.CTW_FIXTURE_VALUE;
                rootObj.tce_detail.date_fixture = data.CTW_FIXTURE_DATE; //(data.CTW_FIXTURE_DATE == null) ? "" : Convert.ToDateTime(data.CTW_FIXTURE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_detail.td = data.CTW_TD_TYPE;
                rootObj.tce_detail.min_load = data.CTW_MIN_LOAD;
                rootObj.tce_detail.reason = data.CTW_REASON;
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getTransactionIDByCherterinID(string id)
        {
            string rtn = "";

            CPAI_TCE_WS_DAL dataDal = new CPAI_TCE_WS_DAL();
            CPAI_TCE_WS data = dataDal.GetByCharterinID(id);
            if (data != null)
            {
                rtn = data.CTW_ROW_ID;
            }

            return rtn;
        }

        public static string getFNTransactionIDByCherterinID(string id)
        {
            string rtn = "";

            string CTW_ROW_ID = getTransactionIDByCherterinID(id);
            FunctionTransactionDAL dataDal = new FunctionTransactionDAL();
            List<FUNCTION_TRANSACTION> data = dataDal.findByTransactionId(CTW_ROW_ID);
            if (data != null)
            {
                rtn = data.FirstOrDefault().FTX_ROW_ID;
            }

            return rtn;
        }
    }
}
