﻿using log4net;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class UnitServiceModel
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public ReturnValue Add(UnitViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                MT_UNIT_DAL dal = new MT_UNIT_DAL();
                MT_UNIT ent = new MT_UNIT();


                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if(string.IsNullOrEmpty(pModel.Order))
                {
                    rtn.Message = "Order should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if(string.IsNullOrEmpty(pModel.UnitName))
                {
                    rtn.Message = "Unit name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if(pModel.UnitName != null)
                {
                    bool hasData = dal.ChkUnitByName(pModel.UnitName);
                    if (hasData == false)
                    {
                        rtn.Message = "Unit name is duplicate.";
                        rtn.Status = false;
                        return rtn;
                    }
                }
                else if (string.IsNullOrEmpty(pModel.UnitStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (pModel.BoolProcessConfrim != null)
                {
                    pModel.UnitProcessConfirm = pModel.BoolProcessConfrim == true ? "Y" : "N";
                }
                else
                {
                    pModel.UnitProcessConfirm = "N";
                }
               

                DateTime now = DateTime.Now;
                try
                {
                    var Code = ShareFn.GenerateCodeByDate("CPAI");
                    // ent.MUN_ROW_ID = pModel.UnitCode;
                    ent.MUN_ROW_ID = Code;
                    ent.MUN_NAME = pModel.UnitName;
                    ent.MUN_ORDER = pModel.Order;
                    ent.MUN_STATUS = pModel.UnitStatus;
                    ent.MUN_QUESTION_FLAG = pModel.UnitProcessConfirm;                    
                    ent.MUN_USER_GROUP = pModel.UserGroup;
                    ent.MUN_CREATED_BY = pUser;
                    ent.MUN_CREATED_DATE = now;
                    ent.MUN_UPDATED_BY = pUser;
                    ent.MUN_UPDATED_DATE = now;

                    pModel.UnitCode = Code;
                    dal.Save(ent);
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch(Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch(Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }


        public ReturnValue Search(ref UnitViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sUnitCode = pModel.sUnitCode == null ? "" : pModel.sUnitCode.ToUpper();
                    var sUnitName = pModel.sUnitName == null ? "" : pModel.sUnitName.ToUpper();
                    var sUnitStatus = pModel.sUnitStatus == null ? "" : pModel.sUnitStatus.ToUpper();

                   

                    var query = (from u in context.MT_UNIT
                         
                                 select new UnitViewModel_SeachData
                                 {
                                     dUnitCode = u.MUN_ROW_ID,
                                     dUnitName = u.MUN_NAME,
                                     dUnitStatus = u.MUN_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sUnitCode))
                        query = query.Where(p => p.dUnitCode.ToUpper().Contains(sUnitCode));

                    if (!string.IsNullOrEmpty(sUnitName))
                        query = query.Where(p => p.dUnitName.ToUpper().Contains(sUnitName));

                    if (!string.IsNullOrEmpty(sUnitStatus))
                        query = query.Where(p => p.dUnitStatus.ToUpper().Equals(sUnitStatus));

                   

                    if (query != null)
                    {

                        //pModel.sSearchData = query.ToList();                       
                        pModel.sSearchData = new List<UnitViewModel_SeachData>();
                        foreach (var item in query.OrderBy(c=>c.dUnitName).ToList())
                        {
                            pModel.sSearchData.Add(new UnitViewModel_SeachData
                            {
                                dUnitCode = string.IsNullOrEmpty(item.dUnitCode) ? "" : item.dUnitCode,
                                dUnitName = string.IsNullOrEmpty(item.dUnitName) ? "" : item.dUnitName,
                                dUnitStatus = string.IsNullOrEmpty(item.dUnitStatus) ? "" : item.dUnitStatus
                            });
                        }
                       
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }


        public UnitViewModel_Detail Get(string pUnitCode)
        {
            UnitViewModel_Detail model = new UnitViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pUnitCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = context.MT_UNIT.Where(c => c.MUN_ROW_ID.ToUpper() == pUnitCode.ToUpper());
                        var areaName = string.Empty;
                        if (query.FirstOrDefault().MUN_FK_MT_AREA != null)
                        {
                            areaName = context.MT_AREA.Where(c => c.MAR_ROW_ID == query.FirstOrDefault().MUN_FK_MT_AREA).SingleOrDefault().MAR_NAME;
                        }
                        
                        if (query != null)
                        {
                            foreach (var g in query)
                            {
                                //model.CreateType = g;
                                model.UnitCode = g.MUN_ROW_ID;
                                model.Order = g.MUN_ORDER;
                                model.UnitName = g.MUN_NAME;
                                model.UserGroup = g.MUN_USER_GROUP;
                                model.UnitStatus = g.MUN_STATUS;
                                model.UnitProcessConfirm = g.MUN_QUESTION_FLAG == "Y" ? "YES" : "NO";
                                model.BoolStatus = g.MUN_STATUS == "ACTIVE" ? true : false;
                                model.BoolProcessConfrim = g.MUN_QUESTION_FLAG == "Y" ? true : false;
                                model.AreaName = areaName != null ? areaName : null;
                            }
                        }

                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public ReturnValue Edit(UnitViewModel_Detail pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            try
            {
                MT_UNIT_DAL dal = new MT_UNIT_DAL();
                MT_UNIT ent = new MT_UNIT();

                var boolStatus = pModel.BoolStatus;
                if (boolStatus != null)
                {
                    pModel.UnitStatus = pModel.BoolStatus == true ? "ACTIVE" : "INACTIVE";
                }
                else
                {
                    pModel.UnitStatus = "INACTIVE";
                }

                if(pModel.BoolProcessConfrim != null)
                {
                    pModel.UnitProcessConfirm = pModel.BoolProcessConfrim == true ? "Y" : "N";
                }
                else
                {
                    pModel.UnitProcessConfirm = "N";
                }


                if (pModel == null)
                {
                    rtn.Message = "Model is not null.";
                    rtn.Status = false;
                }
                else if (string.IsNullOrEmpty(pModel.UnitCode))
                {
                    rtn.Message = "Unit code should not be empty.";
                }
                else if (string.IsNullOrEmpty(pModel.UnitName))
                {
                    rtn.Message = "Unit name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (!string.IsNullOrEmpty(pModel.UnitName))
                {
                    bool hasData = dal.ChkUnitName(pModel.UnitCode, pModel.UnitName);
                    if (hasData == false)
                    {
                        rtn.Message = "Unit name is duplicate.";
                        rtn.Status = false;
                        return rtn;
                    }
                }
                else if (string.IsNullOrEmpty(pModel.UnitStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
               

                try
                { 
                    ent.MUN_ROW_ID = pModel.UnitCode;
                    ent.MUN_NAME = pModel.UnitName.Trim();
                    ent.MUN_STATUS = pModel.UnitStatus;
                    ent.MUN_QUESTION_FLAG = pModel.UnitProcessConfirm;
                    ent.MUN_ORDER = pModel.Order;
                    ent.MUN_USER_GROUP = pModel.UserGroup;
                    ent.MUN_UPDATED_BY = pUser;
                    ent.MUN_UPDATED_DATE = DateTime.Now;

                    dal.Update(ent);

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {                 
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            
            return rtn;
        }

        /// <summary>
        /// All Unit status is ACTIVE
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<SelectListItem> getUnitDDL(string status = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_UNIT
                                 //where d.MUN_FK_MT_AREA == null
                             orderby new { d.MUN_NAME }
                             select new { UnitCode = d.MUN_ROW_ID, UnitName = d.MUN_NAME.ToUpper() }).Distinct().ToList().OrderBy(c=>c.UnitName);

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UnitCode, Text = item.UnitName });
                    }
                }
            }
            return rtn;
        }


        /// <summary>
        /// Unit MUN_FK_MT_AREA = null and MUN_FK_MT_AREA = AreaID
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        public static List<SelectListItem> getUnitByArea(string area)
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_UNIT
                             where (d.MUN_FK_MT_AREA == null && d.MUN_STATUS == "ACTIVE") || d.MUN_FK_MT_AREA == area
                             orderby new { d.MUN_NAME }
                             select new { UnitCode = d.MUN_ROW_ID, UnitName = d.MUN_NAME.ToUpper() }).Distinct().ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UnitCode, Text = item.UnitName });
                    }
                }
            }
            return rtn;
        }


        /// <summary>
        /// Only unit is free from Area
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> getFreeUnit()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from d in context.MT_UNIT
                             where (d.MUN_FK_MT_AREA == null && d.MUN_STATUS == "ACTIVE")/* && d.MUN_STATUS =="ACTIVE"*/
                             orderby new { d.MUN_NAME }
                             select new { UnitCode = d.MUN_ROW_ID, UnitName = d.MUN_NAME.ToUpper() }).Distinct().ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UnitCode, Text = item.UnitName });
                    }
                }
            }
            return rtn;
        }


        /// <summary>
        /// Only Unit is free from Oper
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> getFreeUnitFromOper()
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from u in context.MT_UNIT
                         join o in context.MT_OPER_UNIT on u.MUN_ROW_ID equals o.MOU_UNIT into g
                         from c in g.DefaultIfEmpty()
                         where c.MOU_UNIT == null && u.MUN_STATUS == "ACTIVE"
                         orderby new {u.MUN_NAME}
                         select new { UnitCode = u.MUN_ROW_ID, UnitName = u.MUN_NAME.ToUpper() }).Distinct().ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UnitCode, Text = item.UnitName });
                    }
                }
            }
            return rtn;
        }




        public static List<SelectListItem> getFreeUnitFromOper2(string operid)
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from u in context.MT_UNIT
                             join o in context.MT_OPER_UNIT on u.MUN_ROW_ID equals o.MOU_UNIT into g
                             from c in g.DefaultIfEmpty()
                             where (c.MOU_UNIT == null && u.MUN_STATUS == "ACTIVE")|| c.MOU_ROW_ID == operid
                             orderby new { u.MUN_NAME }
                             select new { UnitCode = u.MUN_ROW_ID, UnitName = u.MUN_NAME.ToUpper() }).Distinct().ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UnitCode, Text = item.UnitName });
                    }
                }
            }
            return rtn;
        }


        public bool IsUsed(string unitCode)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = context.MT_UNIT.Where(c => c.MUN_ROW_ID == unitCode && c.MUN_FK_MT_AREA != null).SingleOrDefault();
                    if (query!=null)
                    {
                        var area = context.MT_AREA.Where(c => c.MAR_ROW_ID == query.MUN_FK_MT_AREA).SingleOrDefault().MAR_NAME;
                    }
                   
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}