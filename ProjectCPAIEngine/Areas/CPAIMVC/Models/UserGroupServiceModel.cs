﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class UserGroupServiceModel
    {
        public static List<SelectListItem> getUserGroupDDL(string Status = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {         
                var query = (from d in context.CPAI_USER_GROUP
                            orderby new { d.USG_USER_GROUP }
                            select new { UserGroup = d.USG_USER_GROUP.ToUpper() }).Distinct().ToList();



                //var selectQuery = query.ToList();
                if (query != null)
                {
                    foreach(var item in query)
                    {
                        rtn.Add(new SelectListItem { Value = item.UserGroup, Text = item.UserGroup });
                    }
                }
            }


           return rtn;
        }
    }
}