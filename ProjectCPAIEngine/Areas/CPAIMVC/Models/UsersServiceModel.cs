﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.PIS;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class UsersServiceModel
    {
        public ReturnValue Add(UsersViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.UsersLogin))
                {
                    rtn.Message = "User Login should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.ADLogin))
                {
                    rtn.Message = "AD Login should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                /*
                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system and type";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.UserGroupSystem));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }
                    
                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }
                    
                }
            */


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                USERS_DAL dal = new USERS_DAL();
                USERS ent = new USERS();

                CPAI_USER_GROUP_DAL dalCtr = new CPAI_USER_GROUP_DAL();
                CPAI_USER_GROUP entCtr = new CPAI_USER_GROUP();

                USERS_ROLE_DAL dalRole = new USERS_ROLE_DAL();
                USER_ROLE entRole = new USER_ROLE();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            // Update data in USERS
                            var genID = ShareFn.GenerateCodeByDate("");
                            ent.USR_ROW_ID = genID;
                            ent.USR_TITLE_EN = pModel.TitleEN;
                            ent.USR_TITLE_COUNTRY = pModel.TitleTH;
                            ent.USR_FIRST_NAME_EN = pModel.FirstNameEN;
                            ent.USR_FIRST_NAME_COUNTRY = pModel.FirstNameTH;
                            ent.USR_LAST_NAME_EN = pModel.LastNameEN;
                            ent.USR_LAST_NAME_COUNTRY = pModel.LastNameTH;
                            ent.USR_LOGIN = pModel.UsersLogin;
                            ent.USR_PASSWORD = Cryptography.GetMD5(pModel.Password);
                            ent.USR_AD_LOGIN_FLAG = pModel.ADLogin;
                            ent.USR_COMPANY = pModel.Company;
                            ent.USR_EMPLOYEE_ID = pModel.EmployeeID;
                            ent.USR_EMAIL = pModel.Email;
                            ent.USR_SYSTEM = pModel.System;
                            ent.USR_STATUS = pModel.Status;
                            ent.USR_MOBILE_FLAG = pModel.IsMobileFlag ? "Y" : "N";
                            ent.USR_NOTI_FLAG = pModel.IsNotiFlag ? "Y" : "N";
                            ent.USR_EMAIL_FLAG = pModel.IsEmailFlag ? "Y" : "N";
                            ent.USR_SMS_FLAG = pModel.IsSMSFlag ? "Y" : "N";
                            ent.USR_MOBILE_MENU = pModel.MobileMenu;
                            ent.USR_WEB_MENU = pModel.WebMenu;
                            ent.USR_CREATED_DATE = now;
                            ent.USR_CREATED_BY = pUser;
                            ent.USR_UPDATED_DATE = now;
                            ent.USR_UPDATED_BY = pUser;

                            dal.Save(ent, context);

                            // Insert data to CPAI_USERS_GROUP

                            if (!string.IsNullOrEmpty(pModel.Role))
                            {
                                entRole = new USER_ROLE();
                                entRole.URO_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entRole.URO_FK_ROLE = pModel.Role;
                                entRole.URO_FK_USER = ent.USR_ROW_ID;

                                entRole.URO_CREATED_BY = pUser;
                                entRole.URO_CREATED_DATE = now;
                                entRole.URO_UPDATED_BY = pUser;
                                entRole.URO_UPDATED_DATE = now;

                                dalRole.Save(entRole, context);
                            }
                            if (pModel.Control != null)
                            {
                                foreach (var item in pModel.Control)
                                {
                                    //if (string.IsNullOrEmpty(item.RowID))
                                    //{
                                    // Add
                                    if (string.IsNullOrEmpty(item.UserGroup) != true || string.IsNullOrEmpty(item.UserGroupSystem) != true)
                                    {
                                        entCtr = new CPAI_USER_GROUP();
                                        entCtr.USG_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        entCtr.USG_FK_USERS = genID;
                                        entCtr.USG_USER_GROUP = string.IsNullOrEmpty(item.UserGroup) ? "-" : item.UserGroup;
                                        entCtr.USG_USER_SYSTEM = string.IsNullOrEmpty(item.UserGroupSystem) ? "-" : item.UserGroupSystem;
                                        entCtr.USG_USED_FLAG = "Y";
                                        entCtr.USG_STATUS = "ACTIVE";
                                        entCtr.USG_CREATED_BY = pUser;
                                        entCtr.USG_CREATED_DATE = now;
                                        entCtr.USG_UPDATED_BY = pUser;
                                        entCtr.USG_UPDATED_DATE = now;
                                        entCtr.USG_AFFECT_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.UserAffectDate);
                                        entCtr.USG_END_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.UserEndDate);

                                        dalCtr.Save(entCtr, context);
                                    }

                                    //}
                                    //else
                                    //{
                                    //    // Update
                                    //    entCtr = new CPAI_USER_GROUP();
                                    //    entCtr.USG_ROW_ID = item.RowID;
                                    //    entCtr.USG_FK_USERS = pModel.UsersID;
                                    //    entCtr.USG_USER_GROUP = string.IsNullOrEmpty(item.UserGroup) ? "-" : item.UserGroup;
                                    //    entCtr.USG_USER_SYSTEM = string.IsNullOrEmpty(item.UserGroupSystem) ? "-" : item.UserGroupSystem;
                                    //    entCtr.USG_USED_FLAG = item.UsedFlag;
                                    //    entCtr.USG_STATUS = item.Status;
                                    //    entCtr.USG_UPDATED_BY = pUser;
                                    //    entCtr.USG_UPDATED_DATE = now;

                                    //    dalCtr.Update(entCtr, context);
                                    //}
                                }
                            }


                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(UsersViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.UsersLogin))
                {
                    rtn.Message = "User Login should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                /*
                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system and type";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.UserGroupSystem));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }
                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }
                }
                */

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                USERS_DAL userDAL = new USERS_DAL();
                USERS userDetail = new USERS();

                CPAI_USER_GROUP_DAL uGroupDAL = new CPAI_USER_GROUP_DAL();
                CPAI_USER_GROUP uGroupDetail = new CPAI_USER_GROUP();

                USERS_ROLE_DAL dalRole = new USERS_ROLE_DAL();
                USER_ROLE entRole = new USER_ROLE();

                DateTime now = DateTime.Now;


                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            userDetail.USR_ROW_ID = pModel.UsersID;
                            userDetail.USR_TITLE_EN = pModel.TitleEN;
                            userDetail.USR_TITLE_COUNTRY = pModel.TitleTH;
                            userDetail.USR_FIRST_NAME_EN = pModel.FirstNameEN;
                            userDetail.USR_FIRST_NAME_COUNTRY = pModel.FirstNameTH;
                            userDetail.USR_LAST_NAME_EN = pModel.LastNameEN;
                            userDetail.USR_LAST_NAME_COUNTRY = pModel.LastNameTH;
                            userDetail.USR_AD_LOGIN_FLAG = pModel.ADLogin;
                            userDetail.USR_LOGIN = pModel.UsersLogin;
                            userDetail.USR_PASSWORD = Cryptography.GetMD5(pModel.Password);
                            userDetail.USR_COMPANY = pModel.Company;
                            userDetail.USR_EMPLOYEE_ID = pModel.EmployeeID;
                            userDetail.USR_EMAIL = pModel.Email;
                            userDetail.USR_SYSTEM = pModel.System;
                            userDetail.USR_STATUS = pModel.Status;
                            userDetail.USR_MOBILE_FLAG = pModel.IsMobileFlag ? "Y" : "N";
                            userDetail.USR_NOTI_FLAG = pModel.IsNotiFlag ? "Y" : "N";
                            userDetail.USR_EMAIL_FLAG = pModel.IsEmailFlag ? "Y" : "N";
                            userDetail.USR_SMS_FLAG = pModel.IsSMSFlag ? "Y" : "N";
                            userDetail.USR_MOBILE_MENU = pModel.MobileMenu;
                            userDetail.USR_WEB_MENU = pModel.WebMenu;
                            userDetail.USR_UPDATED_BY = pUser;
                            userDetail.USR_UPDATED_DATE = now;
                            userDAL.Update(userDetail, context);

                            dalRole.Delete(pModel.UsersID, context);
                            if (!string.IsNullOrEmpty(pModel.Role))
                            {
                                entRole = new USER_ROLE();
                                entRole.URO_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entRole.URO_FK_ROLE = pModel.Role;
                                entRole.URO_FK_USER = pModel.UsersID;

                                entRole.URO_CREATED_BY = pUser;
                                entRole.URO_CREATED_DATE = now;
                                entRole.URO_UPDATED_BY = pUser;
                                entRole.URO_UPDATED_DATE = now;

                                dalRole.Save(entRole, context);
                            }

                            uGroupDAL.DeleteByUserID(pModel.UsersID, context);
                            if (pModel.Control != null)
                            {
                                int countUsg = 0;
                                foreach (var item in pModel.Control)
                                {
                                    var Code = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("usg"), countUsg.ToString("000"));
                                    //Edit
                                    if (string.IsNullOrEmpty(item.UserGroup) != true || string.IsNullOrEmpty(item.UserGroupSystem) != true)
                                    {
                                        uGroupDetail = new CPAI_USER_GROUP();
                                        uGroupDetail.USG_ROW_ID = Code;
                                        uGroupDetail.USG_FK_USERS = pModel.UsersID;
                                        uGroupDetail.USG_USER_GROUP = string.IsNullOrEmpty(item.UserGroup) ? "-" : item.UserGroup;
                                        uGroupDetail.USG_USER_SYSTEM = string.IsNullOrEmpty(item.UserGroupSystem) ? "-" : item.UserGroupSystem;
                                        uGroupDetail.USG_USED_FLAG = "Y";
                                        uGroupDetail.USG_STATUS = "ACTIVE";
                                        uGroupDetail.USG_CREATED_BY = pUser;
                                        uGroupDetail.USG_CREATED_DATE = now;
                                        uGroupDetail.USG_UPDATED_BY = pUser;
                                        uGroupDetail.USG_UPDATED_DATE = now;
                                        uGroupDetail.USG_AFFECT_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.UserAffectDate);
                                        uGroupDetail.USG_END_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.UserEndDate);
                                        uGroupDetail.USG_DELEGATE_FLAG = (uGroupDetail.USG_AFFECT_DATE == null || uGroupDetail.USG_END_DATE == null) ? null : item.UsedDelegateFlag;
                                        uGroupDAL.Save(uGroupDetail, context);
                                    }
                                    countUsg++;

                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref UsersViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sUsersID = pModel.sUsersID == null ? "" : pModel.sUsersID.ToUpper();
                    var sFirstNameEN = pModel.sFirstNameEN == null ? "" : pModel.sFirstNameEN.ToUpper();
                    var sLastNameEN = pModel.sLastNameEN == null ? "" : pModel.sLastNameEN.ToUpper();
                    var sUsersLogin = pModel.sUsersLogin == null ? "" : pModel.sUsersLogin.ToUpper();
                    var sSectionHead = pModel.sSectionHead == null ? "" : pModel.sSectionHead.ToUpper();
                    var sCompany = pModel.sCompany == null ? "" : pModel.sCompany.ToUpper();
                    var sADLogin = pModel.sADLogin == null ? "" : pModel.sADLogin.ToUpper();
                    var sStatus = pModel.sStatus == null ? "" : pModel.sStatus.ToUpper();

                    var query = (from v in context.USERS
                                 select new UsersViewModel_SearchData
                                 {
                                     dUsersID = v.USR_ROW_ID,
                                     dTitleEN = v.USR_TITLE_EN,
                                     dTitleTH = v.USR_TITLE_COUNTRY,
                                     dFirstNameEN = v.USR_FIRST_NAME_EN,
                                     dLastNameEN = v.USR_LAST_NAME_EN,
                                     dUsersLogin = v.USR_LOGIN,
                                     dSectionHead = v.USR_SECTION_HEAD,
                                     dCompany = v.USR_COMPANY,
                                     dADLogin = v.USR_LOGIN,
                                     dStatus = v.USR_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sUsersID))
                        query = query.Where(p => p.dUsersID.ToUpper().Contains(sUsersID));

                    if (!string.IsNullOrEmpty(sFirstNameEN))
                        query = query.Where(p => p.dFirstNameEN.ToUpper().Contains(sFirstNameEN));

                    if (!string.IsNullOrEmpty(sLastNameEN))
                        query = query.Where(p => p.dLastNameEN.ToUpper().Contains(sLastNameEN));

                    if (!string.IsNullOrEmpty(sUsersLogin))
                        query = query.Where(p => p.dUsersLogin.ToUpper().Contains(sUsersLogin));

                    if (!string.IsNullOrEmpty(sSectionHead))
                        query = query.Where(p => p.dSectionHead.ToUpper().Contains(sSectionHead));

                    if (!string.IsNullOrEmpty(sCompany))
                        query = query.Where(p => p.dCompany.ToUpper().Contains(sCompany));

                    if (!string.IsNullOrEmpty(sADLogin))
                        query = query.Where(p => p.dADLogin.ToUpper().Contains(sADLogin));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Contains(sStatus));


                    if (query != null)
                    {

                        pModel.sSearchData = query.ToList();
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public UsersViewModel_Detail Get(string pUsername)
        {
            UsersViewModel_Detail model = new UsersViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pUsername) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.USERS
                                     where v.USR_LOGIN.ToUpper().Equals(pUsername.ToUpper())
                                     join vc in context.CPAI_USER_GROUP on v.USR_ROW_ID equals vc.USG_FK_USERS into view
                                     from vc in view.DefaultIfEmpty()
                                     join r in context.USER_ROLE on v.USR_ROW_ID equals r.URO_FK_USER into RoleData
                                     from r in RoleData.DefaultIfEmpty()
                                     select new
                                     {
                                         dUsersID = v.USR_ROW_ID,
                                         dTitleEN = v.USR_TITLE_EN,
                                         dTitleTH = v.USR_TITLE_COUNTRY,
                                         dFirstNameEN = v.USR_FIRST_NAME_EN,
                                         dLastNameEN = v.USR_LAST_NAME_EN,
                                         dFirstNameTH = v.USR_FIRST_NAME_COUNTRY,
                                         dLastNameTH = v.USR_LAST_NAME_COUNTRY,
                                         dPassword = v.USR_PASSWORD,
                                         dUsersLogin = v.USR_LOGIN,
                                         dSectionHead = v.USR_SECTION_HEAD,
                                         dCompany = v.USR_COMPANY,
                                         dADLogin = v.USR_AD_LOGIN_FLAG,
                                         dSystem = v.USR_SYSTEM,
                                         dStatus = v.USR_STATUS,
                                         dEmployeeID = v.USR_EMPLOYEE_ID,
                                         dEmail = v.USR_EMAIL,
                                         dUserRowID = vc.USG_ROW_ID,
                                         dUserGroup = vc.USG_USER_GROUP,
                                         dUserGroupSystem = vc.USG_USER_SYSTEM,
                                         dUserRole = r.URO_FK_ROLE,
                                         dMobileFlag = v.USR_MOBILE_FLAG,
                                         dNotiFlag = v.USR_NOTI_FLAG,
                                         dEmailFlag = v.USR_EMAIL_FLAG,
                                         dSMSFlag = v.USR_SMS_FLAG,
                                         dMobileMenu = v.USR_MOBILE_MENU,
                                         dWebMenu = v.USR_WEB_MENU,
                                         dAffectDate = vc.USG_AFFECT_DATE,
                                         dEndDate = vc.USG_END_DATE,
                                         dDelegateFlag = vc.USG_DELEGATE_FLAG
                                     });

                        if (query != null)
                        {
                            model.Control = new List<UsersViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                if (i == 1)
                                {
                                    model.UsersID = g.dUsersID;
                                    model.TitleEN = g.dTitleEN;
                                    model.TitleTH = g.dTitleTH;
                                    model.FirstNameEN = g.dFirstNameEN;
                                    model.LastNameEN = g.dLastNameEN;
                                    model.FirstNameTH = g.dFirstNameTH;
                                    model.LastNameTH = g.dLastNameTH;
                                    model.Password = g.dPassword;
                                    model.UsersLogin = g.dUsersLogin;
                                    model.Company = g.dCompany;
                                    model.ADLogin = g.dADLogin;
                                    model.System = g.dSystem;
                                    model.Status = g.dStatus;
                                    model.EmployeeID = g.dEmployeeID;
                                    model.Email = g.dEmail;
                                    model.Role = g.dUserRole;
                                    model.MobileFlag = g.dMobileFlag == "Y" ? "true" : "false";
                                    model.IsMobileFlag = g.dMobileFlag == "Y" ? true : false;
                                    model.NotiFlag = g.dNotiFlag == "Y" ? "true" : "false";
                                    model.IsNotiFlag = g.dNotiFlag == "Y" ? true : false;
                                    model.EmailFlag = g.dEmailFlag == "Y" ? "true" : "false";
                                    model.IsEmailFlag = g.dEmailFlag == "Y" ? true : false;
                                    model.SMSFlag = g.dSMSFlag == "Y" ? "true" : "false";
                                    model.IsSMSFlag = g.dSMSFlag == "Y" ? true : false;
                                    model.MobileMenu = g.dMobileMenu;
                                    model.WebMenu = g.dWebMenu;
                                }

                                if (string.IsNullOrEmpty(g.dUsersID) == false)
                                {
                                    model.Control.Add(new UsersViewModel_Control
                                    {
                                        RowID = g.dUserRowID,
                                        Order = i.ToString(),
                                        UserGroup = g.dUserGroup,
                                        UserGroupSystem = g.dUserGroupSystem,
                                        UserAffectEndDate = (g.dAffectDate == null) ? null : ShareFn.ConvertDateTimeToDateStringFormat(g.dAffectDate, "dd/MM/yyyy") + " to " + ShareFn.ConvertDateTimeToDateStringFormat(g.dEndDate, "dd/MM/yyyy"),
                                        UsedDelegateFlag = g.dDelegateFlag
                                    });

                                    i++;
                                }
                            }
                        }
                    }
                    //var test=     GetMenu("ADMIN");
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public ReturnValue Delegate(List<string> listUsersID, UsersViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (listUsersID == null || listUsersID.Count() < 1)
                {
                    rtn.Message = "UserList should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                USERS_DAL dal = new USERS_DAL();
                USERS ent = new USERS();

                CPAI_USER_GROUP_DAL dalCtr = new CPAI_USER_GROUP_DAL();
                CPAI_USER_GROUP entCtr = new CPAI_USER_GROUP();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            if (pModel.Control != null)
                            {
                                foreach (string userID in listUsersID)
                                {
                                    foreach (var item in pModel.Control)
                                    {
                                        if (string.IsNullOrEmpty(item.UserGroup) != true || string.IsNullOrEmpty(item.UserGroupSystem) != true)
                                        {
                                            if (!string.IsNullOrEmpty(item.UserAffectDate) && !string.IsNullOrEmpty(item.UserEndDate))
                                            {
                                                dalCtr.DeleteByUserIDAndSystemGroup(userID, item.UserGroupSystem, item.UserGroup, context);
                                                string getUserID = Get(pUser).UsersID;
                                                var userGroupList = GetUserGroupByUser(item.UserGroupSystem, getUserID);
                                                foreach (var itemDetail in userGroupList)
                                                {

                                                    entCtr = new CPAI_USER_GROUP();
                                                    entCtr.USG_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                    entCtr.USG_FK_USERS = userID;
                                                    entCtr.USG_USER_GROUP = string.IsNullOrEmpty(itemDetail.Value) ? "-" : itemDetail.Value;
                                                    entCtr.USG_USER_SYSTEM = string.IsNullOrEmpty(item.UserGroupSystem) ? "-" : item.UserGroupSystem;
                                                    entCtr.USG_USED_FLAG = "Y";
                                                    entCtr.USG_STATUS = "ACTIVE";
                                                    entCtr.USG_CREATED_BY = pUser;
                                                    entCtr.USG_CREATED_DATE = now;
                                                    entCtr.USG_UPDATED_BY = pUser;
                                                    entCtr.USG_UPDATED_DATE = now;
                                                    entCtr.USG_AFFECT_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.UserAffectDate);
                                                    entCtr.USG_END_DATE = ShareFn.ConvertStringDateFormatToDatetime(item.UserEndDate);
                                                    entCtr.USG_DELEGATE_FLAG = "Y";
                                                    entCtr.USG_FK_DELEGATE_BY = getUserID;
                                                    dalCtr.Save(entCtr, context);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public static string GetUserADJSON(string pName = "")
        {
            string json = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    List<VW_PERSONAL_ALL> personList = VW_PERSON.GetAllUser();

                    if (!string.IsNullOrEmpty(pName))
                    {

                        var query = (from v in personList
                                     select v);


                        if (!string.IsNullOrEmpty(pName))
                            query = query.Where(q => q.ENName.StartsWith(pName));

                        if (query != null)
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            json = js.Serialize(query);
                        }
                    }
                    else
                    {
                        var query = (from v in personList
                                     select v.ENName);

                        if (!string.IsNullOrEmpty(pName))
                            query = query.Where(q => q.StartsWith(pName));


                        if (query != null)
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            json = js.Serialize(query);
                        }
                    }
                }

                //test
                //UsersViewModel_Detail test_detail = new UsersViewModel_Detail();
                //test_detail = GetUserADDetail("Chaichan Jaisawat");// Jaisawat


                return json;
            }
            catch (Exception ex)
            {
                return json;
            }
        }

        public static string GetMenu(string userRole)
        {
            string json = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    List<MENU> menuList = new List<MENU>();

                    var allMenu = (from rl in context.ROLE
                                   where rl.ROL_NAME.Equals(userRole)
                                   join rm in context.ROLE_MENU on rl.ROL_ROW_ID equals rm.RMN_FK_ROLE into viewR
                                   from r in viewR.DefaultIfEmpty()
                                   join m in context.MENU on r.RMN_FK_MENU equals m.MEU_ROW_ID into viewM
                                   from vm in viewM.DefaultIfEmpty()
                                       //where vm.MEU_ACTIVE.Equals("ACTIVE") && vm.MEU_PARENT_ID.Equals("#") && vm.MEU_CONTROL_TYPE.Equals("MENU")
                                   where vm.MEU_ACTIVE.Equals("ACTIVE")
                                   select vm).ToList();

                    List<MenuDetail.MenuRootObject> menuRoot = new List<MenuDetail.MenuRootObject>();

                    if (allMenu != null)
                    {
                        var mainMenu = allMenu.Where(item => item.MEU_PARENT_ID.Equals("#"));

                        if (mainMenu != null)
                        {
                            //int iMax = 1;
                            foreach (var item in mainMenu.OrderBy(x => int.Parse(x.MEU_LEVEL)).ThenBy(y => int.Parse(y.MEU_LIST_NO)))
                            {

                                MenuDetail.Node nDetail = new MenuDetail.Node();
                                List<MenuDetail.Node> lstDetail = new List<MenuDetail.Node>();
                                lstDetail = GetSubMenu(allMenu, item.MEU_ROW_ID, item.MEU_ROW_ID);

                                //var index = menuRoot.FindIndex(a => a.text == item.LNG_DESCRIPTION);
                                //menuRoot[index].nodes.Add(lstDetail);

                                menuRoot.Add(new MenuDetail.MenuRootObject() { menu_id = item.MEU_ROW_ID, text = item.LNG_DESCRIPTION, nodes = lstDetail, tags = new List<string>(new string[] { "M" }) });
                            }
                        }
                    }
                    json = JSonConvertUtil.modelToJson(menuRoot);


                }
                return json;
            }
            catch (Exception ex)
            {
                return json;
            }
        }
        public static List<MenuDetail.Node> GetSubMenu(List<MENU> menuLst, string rowID, string parentID)
        {
            string json = "";
            List<MenuDetail.Node> listDetail = new List<MenuDetail.Node>();
            MenuDetail.Node nDetail = new MenuDetail.Node();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    List<MENU> menuList = new List<MENU>();

                    var query = (from m in menuLst
                                 where m.MEU_PARENT_ID.Equals(parentID)
                                 select m).ToList();

                    if (query != null)
                    {

                        foreach (var iSub in query.OrderBy(x => int.Parse(x.MEU_LEVEL)).ThenBy(y => int.Parse(y.MEU_LIST_NO)))
                        {
                            nDetail = new MenuDetail.Node();
                            nDetail.text = iSub.LNG_DESCRIPTION;
                            nDetail.menu_id = iSub.MEU_ROW_ID;
                            nDetail.tags = new List<string>(new string[] { "M" });
                            //nDetail.nodes = new List<MenuDetail.Node>();
                            listDetail.Add(nDetail);

                            var querySub = (from m in menuLst
                                            where m.MEU_PARENT_ID.Equals(iSub.MEU_ROW_ID)
                                            select m).ToList();

                            if (querySub != null)
                            {
                                if (querySub.Count > 0) nDetail.nodes = new List<MenuDetail.Node>();
                                foreach (var iSub2 in querySub.OrderBy(s => s.MEU_LIST_NO))
                                {
                                    MenuDetail.Node nSubDetail = new MenuDetail.Node();
                                    nSubDetail.text = iSub2.LNG_DESCRIPTION;
                                    nSubDetail.menu_id = iSub2.MEU_ROW_ID;
                                    nSubDetail.tags = new List<string>(new string[] { "F" });
                                    nSubDetail.backColor = "#E0E9F8";
                                    nDetail.nodes.Add(nSubDetail);
                                }
                            }
                        }
                    }
                }
                return listDetail;
            }
            catch (Exception ex)
            {
                return listDetail;
            }
        }

        public List<SelectListItem> GetUserGroupByUser(string system, string userID)
        {
            List<SelectListItem> groupList = new List<SelectListItem>();
            UsersServiceModel serviceModel = new UsersServiceModel();
            groupList = DropdownServiceModel.getDelegateGroup(userID, system);
            return groupList;
        }

        public static UsersViewModel_Detail GetUserADDetail(string Fullname)
        {
            UsersViewModel_Detail model = new UsersViewModel_Detail();
            List<VW_PERSONAL_ALL> personList = VW_PERSON.GetUserbyFullName(Fullname);

            foreach (var item in personList)
            {
                model.UsersID = item.USERID;
                model.TitleEN = item.ENTITLE;
                model.TitleTH = item.THTITLE;
                model.FirstNameEN = item.ENFIRSTNAME;
                model.FirstNameTH = item.THFIRSTNAME;
                model.LastNameEN = item.ENLASTNAME;
                model.LastNameTH = item.THLASTNAME;
                model.ADLogin = "Y";
                model.Company = item.CompCode;
                model.EmployeeID = item.EMPLOYEEID;
                model.Email = item.EMAIL;
            }
            return model;


        }

        public static string SetUserFlag(string username, string flag, string type)
        {
            UsersServiceModel userService = new UsersServiceModel();
            UsersViewModel_Detail model = userService.Get(username.ToUpper());
            string json = "";
            string jsonXML = "{\"data\": []}";

            if (model != null)
            {
                USERS_DAL userDAL = new USERS_DAL();
                USERS user = new USERS();

                using (var context = new EntityCPAIEngine())
                {
                    user.USR_ROW_ID = model.UsersID;
                    user.USR_UPDATED_BY = model.UsersLogin;
                    user.USR_UPDATED_DATE = DateTime.Now;

                    if (type == CPAIConstantUtil.MOBILE)
                    {
                        user.USR_MOBILE_FLAG = flag;
                        userDAL.UpdateMobileFlag(user, context);
                    }
                    else if (type == CPAIConstantUtil.SMS)
                    {
                        user.USR_SMS_FLAG = flag;
                        userDAL.UpdateSMSFlag(user, context);
                    }
                    else if (type == CPAIConstantUtil.EMAIL)
                    {
                        user.USR_EMAIL = flag;
                        userDAL.UpdateEmailFlag(user, context);
                    }
                    else if (type == CPAIConstantUtil.NOTI)
                    {
                        user.USR_NOTI_FLAG = flag;
                        userDAL.UpdateNotiFlag(user, context);
                    }
                }

                model = userService.Get(username.ToUpper());

                if (model != null)
                {
                    string mobileFlag = (model.IsMobileFlag ? "Y" : "N");
                    string smsFlag = (model.IsSMSFlag ? "Y" : "N");
                    string emailFlag = (model.IsEmailFlag ? "Y" : "N");
                    string notiFlag = (model.IsNotiFlag ? "Y" : "N");

                    if (type == CPAIConstantUtil.MOBILE)
                    {
                        if (flag != mobileFlag)
                        {
                            return "update_failed";
                        }
                    }
                    else if (type == CPAIConstantUtil.SMS)
                    {
                        if (flag != smsFlag)
                        {
                            return "update_failed";
                        }
                    }
                    else if (type == CPAIConstantUtil.EMAIL)
                    {
                        if (flag != emailFlag)
                        {
                            return "update_failed";
                        }
                    }
                    else if (type == CPAIConstantUtil.NOTI)
                    {
                        if (flag != notiFlag)
                        {
                            return "update_failed";
                        }
                    }

                    json = "{\"key\":\"MOBILE\",\"value\":\"" + mobileFlag + "\"},";
                    json += "{\"key\":\"SMS\",\"value\":\"" + smsFlag + "\"},";
                    json += "{\"key\":\"EMAIL\",\"value\":\"" + emailFlag + "\"},";
                    json += "{\"key\":\"NOTI\",\"value\":\"" + notiFlag + "\"}";
                }
                else
                {
                    return "user_not_found";
                }

                jsonXML = "{\"data\":[" + json + "]}";
            }
            else
            {
                return "user_not_found";
            }

            return jsonXML;
        }
    }
}