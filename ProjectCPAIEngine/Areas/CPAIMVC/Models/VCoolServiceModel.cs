﻿using com.pttict.engine.dal.Entity;
using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.DALVCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class VCoolServiceModel
    {
        public static string getTransactionByID(string id)
        {
            VcoRootObject rootObj = new VcoRootObject();
            rootObj.requester_info = new VcoRequesterInfo();
            rootObj.crude_info = new VcoCrudeInfo();
            rootObj.comment = new VcoComment();
            rootObj.date_history = new VcoEtaDateHistory();
            rootObj.crude_compare = new VcoCrudeYieldComparison();
            rootObj.crude_compare.yield = new List<VcoCrudeYield>();
            rootObj.crude_compare.comparison_json = new List<VcoCrudeYieldJson>();

            VCO_DATA_DAL dataDal = new VCO_DATA_DAL();
            VCO_DATA data = dataDal.GetByID(id);


            VCO_COMMENT_DAL commentDal = new VCO_COMMENT_DAL();
            VCO_COMMENT dataComment = commentDal.GetByDataID(data.VCDA_ROW_ID);


            VCO_ETA_DATE_HISTORY_DAL historyDal = new VCO_ETA_DATE_HISTORY_DAL();
            List<VCO_ETA_DATE_HISTORY> lstHistory = historyDal.GetAllByID(data.VCDA_ROW_ID);

            if (data != null)
            {
                VCO_DATA former_data = dataDal.GetFormerByCrudeAndCountry(data.VCDA_CRUDE_NAME, data.VCDA_ORIGIN, data.VCDA_CREATED);

                rootObj.requester_info.date_purchase = (data.VCDA_DATE_PURCHASE == DateTime.MinValue || data.VCDA_DATE_PURCHASE == null) ? "" : Convert.ToDateTime(data.VCDA_DATE_PURCHASE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.requester_info.purchase_no = data.VCDA_PURCHASE_NO != null ? data.VCDA_PURCHASE_NO : "";
                rootObj.requester_info.name = data.VCDA_CREATED_BY;
                rootObj.requester_info.workflow_priority = data.VCDA_PRIORITY != null ? data.VCDA_PRIORITY : "";
                rootObj.requester_info.workflow_status = data.VCDA_STATUS != null ? data.VCDA_STATUS : "";
                rootObj.requester_info.workflow_status_description = getWorkflowStatusDescription(rootObj.requester_info.workflow_status);
                
                rootObj.requester_info.area_unit = data.VCDA_AREA_UNIT != null ? data.VCDA_AREA_UNIT : "";
                rootObj.requester_info.status_sc = data.VCDA_SC_STATUS != null ? data.VCDA_SC_STATUS : "";
                rootObj.requester_info.status_sc_description = getWorkflowStatusDescription(rootObj.requester_info.status_sc);
                rootObj.requester_info.status_tn = data.VCDA_TN_STATUS != null ? data.VCDA_TN_STATUS : "";
                rootObj.requester_info.status_tn_description = getWorkflowStatusDescription(rootObj.requester_info.status_tn);

                rootObj.crude_info.tpc_plan_month = data.VCDA_TPC_PLAN_MONTH != null ? data.VCDA_TPC_PLAN_MONTH : "";
                rootObj.crude_info.tpc_plan_year = data.VCDA_TPC_PLAN_YEAR != null ? data.VCDA_TPC_PLAN_YEAR : "";
                rootObj.crude_info.purchased_by = data.VCDA_PURCHASED_BY != null ? data.VCDA_PURCHASED_BY : "";
                rootObj.crude_info.crude_name = data.VCDA_CRUDE_NAME != null ? data.VCDA_CRUDE_NAME : "";
                rootObj.crude_info.origin = data.VCDA_ORIGIN != null ? data.VCDA_ORIGIN : "";
                rootObj.crude_info.purchase_date = (data.VCDA_PURCHASE_DATE == DateTime.MinValue || data.VCDA_PURCHASE_DATE == null) ? "" : Convert.ToDateTime(data.VCDA_PURCHASE_DATE).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.benchmark_price = data.VCDA_BENCHMARK_PRICE != null ? data.VCDA_BENCHMARK_PRICE : "";
                rootObj.crude_info.final_benchmark_price = data.VCDA_FINAL_BENCHMARK_PRICE != null ? data.VCDA_FINAL_BENCHMARK_PRICE : "";
                rootObj.crude_info.incoterm = data.VCDA_INCOTERM != null ? data.VCDA_INCOTERM : "";
                rootObj.crude_info.final_incoterm = data.VCDA_FINAL_INCOTERM != null ? data.VCDA_FINAL_INCOTERM : "";
                rootObj.crude_info.quantity_kbbl_max = data.VCDA_QUANTITY_KBBL_MAX != null ? data.VCDA_QUANTITY_KBBL_MAX : "";
                rootObj.crude_info.loading_date_from = (data.VCDA_LOADING_DATE_FROM == DateTime.MinValue || data.VCDA_LOADING_DATE_FROM == null) ? "" : Convert.ToDateTime(data.VCDA_LOADING_DATE_FROM).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.loading_date_to = (data.VCDA_LOADING_DATE_TO == DateTime.MinValue || data.VCDA_LOADING_DATE_TO == null) ? "" : Convert.ToDateTime(data.VCDA_LOADING_DATE_TO).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.crude_ref = data.VCDA_CRUDE_NAME != null ? data.VCDA_CRUDE_NAME : "";
                rootObj.crude_info.supplier = data.VCDA_SUPPLIER != null ? data.VCDA_SUPPLIER : "";
                rootObj.crude_info.purchase_type = data.VCDA_PURCHASE_TYPE != null ? data.VCDA_PURCHASE_TYPE : "";
                rootObj.crude_info.purchase_method = data.VCDA_PURCHASE_METHOD != null ? data.VCDA_PURCHASE_METHOD : "";
                rootObj.crude_info.discharging_date_from = (data.VCDA_DISCHARGING_DATE_FROM == DateTime.MinValue || data.VCDA_DISCHARGING_DATE_FROM == null) ? "" : Convert.ToDateTime(data.VCDA_DISCHARGING_DATE_FROM).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.discharging_date_to = (data.VCDA_DISCHARGING_DATE_TO == DateTime.MinValue || data.VCDA_DISCHARGING_DATE_TO == null) ? "" : Convert.ToDateTime(data.VCDA_DISCHARGING_DATE_TO).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.premium = data.VCDA_PREMIUM != null ? data.VCDA_PREMIUM : "";
                rootObj.crude_info.final_premium = data.VCDA_FINAL_PREMIUM != null ? data.VCDA_FINAL_PREMIUM : "";
                rootObj.crude_info.formula_price = data.VCDA_FORMULA_PRICE != null ? data.VCDA_FORMULA_PRICE : "";
                rootObj.crude_info.quantity_kt_max = data.VCDA_QUANTITY_KT_MAX != null ? data.VCDA_QUANTITY_KT_MAX : "";
                rootObj.crude_info.revised_date_from = (data.VCDA_SCSC_REVISED_DATE_FROM == DateTime.MinValue || data.VCDA_SCSC_REVISED_DATE_FROM == null) ? "" : Convert.ToDateTime(data.VCDA_SCSC_REVISED_DATE_FROM).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.revised_date_to = (data.VCDA_SCSC_REVISED_DATE_TO == DateTime.MinValue || data.VCDA_SCSC_REVISED_DATE_TO == null) ? "" : Convert.ToDateTime(data.VCDA_SCSC_REVISED_DATE_TO).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.crude_info.premium_maximum = data.VCDA_PREMIUM_MAXIMUM != null ? data.VCDA_PREMIUM_MAXIMUM : "";
                rootObj.crude_info.purchase_result = data.VCDA_PURCHASE_RESULT != null ? data.VCDA_PURCHASE_RESULT : "";

                if (former_data != null)
                {
                    rootObj.crude_info.former_formula_price = former_data.VCDA_FORMULA_PRICE != null ? data.VCDA_FORMULA_PRICE : "";
                    rootObj.crude_info.former_quantity_kt_max = former_data.VCDA_QUANTITY_KT_MAX != null ? data.VCDA_QUANTITY_KT_MAX : "";
                }
                else
                {
                    rootObj.crude_info.former_formula_price = "";
                    rootObj.crude_info.former_quantity_kt_max = "";
                }
                rootObj.crude_info.propcessing_period_form = (data.VCDA_PROCESSING_DATE_FROM == DateTime.MinValue || data.VCDA_PROCESSING_DATE_FROM == null) ? "" : Convert.ToDateTime(data.VCDA_PROCESSING_DATE_FROM).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.crude_info.propcessing_period_to = (data.VCDA_PROCESSING_DATE_TO == DateTime.MinValue || data.VCDA_PROCESSING_DATE_TO == null) ? "" : Convert.ToDateTime(data.VCDA_PROCESSING_DATE_TO).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (data.VCDA_COMPARISON_JSON != null)
                    rootObj.crude_compare.comparison_json = new JavaScriptSerializer().Deserialize<List<VcoCrudeYieldJson>>(data.VCDA_COMPARISON_JSON);
            }

            if (dataComment != null)
            {
                rootObj.comment.reason_for_purchasing = dataComment.VCCO_REASON_FOR_PURCHASING ?? "";
                rootObj.comment.lp_run_note = dataComment.VCCO_LP_RUN_NOTE ?? "";
                rootObj.comment.lp_run_summary = dataComment.VCCO_LP_RUN_SUMMARY ?? "";
                rootObj.comment.lp_run_summary_mobile = dataComment.VCCO_LP_RUN_SUMMARY_MOBILE ?? "";
                rootObj.comment.lp_run_attach_file = dataComment.VCCO_LP_RUN_ATTACH_FILE ?? "";
                rootObj.comment.scep = dataComment.VCCO_SCEP ?? "";
                rootObj.comment.scsc = dataComment.VCCO_SCSC ?? "";
                rootObj.comment.tn = dataComment.VCCO_TN ?? "";
                rootObj.comment.scvp = dataComment.VCCO_SCVP ?? "";
                rootObj.comment.cmcs_request = dataComment.VCCO_CMCS_REQUEST ?? "";
                rootObj.comment.cmcs_propose_discharge = dataComment.VCCO_CMCS_PROPOSE_DISCHARGE ?? "";
                rootObj.comment.cmcs_propose_final = dataComment.VCCO_CMCS_PROPOSE_FINAL ?? "";
                rootObj.comment.evpc = dataComment.VCCO_EVPC ?? "";
                rootObj.comment.tnvp = dataComment.VCCO_TNVP ?? "";
                rootObj.comment.cmvp = dataComment.VCCO_CMVP ?? "";
                rootObj.comment.cmvp_flag = dataComment.VCCO_CMVP_FLAG ?? "";
                rootObj.comment.scvp_flag = dataComment.VCCO_SCVP_FLAG ?? "";
                rootObj.comment.tnvp_flag = dataComment.VCCO_TNVP_FLAG ?? "";
                rootObj.comment.cmcs_note = dataComment.VCCO_CMCS_NOTE ?? "";
                rootObj.comment.scsc_agree_flag = dataComment.VCCO_SCSC_AGREE_FLAG ?? "";
            }

            VCO_CRUDE_COMPARISON_DAL data_compare_dal = new VCO_CRUDE_COMPARISON_DAL();
            List<VCO_CRUDE_COMPARISON> data_compare = data_compare_dal.GetByDataID(data.VCDA_ROW_ID);

            if (data_compare != null)
            {

                foreach (var item in data_compare)
                {
                    VcoCrudeYield dataCompare = new VcoCrudeYield();
                    dataCompare.order = item.VCCP_ORDER;
                    dataCompare.yield = item.VCCP_YIELD;
                    dataCompare.base_value = item.VCCP_BASE_VALUE;
                    dataCompare.compare_crude = item.VCCP_COMPARE_CRUDE;
                    dataCompare.compare_value = item.VCCP_COMPARE_VALUE;
                    dataCompare.delta_value = item.VCCP_DELTA_VALUE;
                    rootObj.crude_compare.yield.Add(dataCompare);
                }

            }

            if (lstHistory != null)
            {
                rootObj.date_history.history = new List<DateHistory>();
                var listHistory = lstHistory.OrderBy(d => d.VCDH_CREATED).ToList();
                for (int i = 0; i < listHistory.Count; i++)
                {
                    DateHistory date_history = new DateHistory();
                    date_history.order = i.ToString();
                    date_history.user_group = listHistory[i].VCDH_USER_GROUP;
                    if (listHistory[i].VCDH_DATE_FROM == null || listHistory[i].VCDH_DATE_FROM == DateTime.MinValue || listHistory[i].VCDH_DATE_TO == null || listHistory[i].VCDH_DATE_TO == DateTime.MinValue)
                    {
                        date_history.date_history = "";
                    }
                    else
                    {
                        date_history.date_history = Convert.ToDateTime(listHistory[i].VCDH_DATE_FROM).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture) + " to " + Convert.ToDateTime(listHistory[i].VCDH_DATE_TO).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    }

                    rootObj.date_history.history.Add(date_history);
                }
            }
            return new JavaScriptSerializer().Serialize(rootObj);

        }

        public static string getWorkflowStatusDescription(string workflow_status)
        {
            var workflow_description = workflow_status;
            if (workflow_status == CPAIConstantUtil.STATUS_WAITING_CHECK_IMPACT)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_CHECK_IMPACT;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_VP_APPROVE_TANK;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_VP_CHECK_IMPACT)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_VP_CHECK_IMPACT;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_VP_APPROVE_TANK;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_VP_APPROVE_TANK;
            }
            // ADD NEW CHANGE REQUEST 6 STATUS
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_CHECK_TANK)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_CHECK_TANK;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_APPROVE_TANK;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_CONFIRM_PRICE;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_APPROVE_PRICE)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_APPROVE_PRICE;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_APPROVE_SUMMARY)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_APPROVE_SUMMARY;
            }
            else if (workflow_status == CPAIConstantUtil.STATUS_WAITING_APPROVE_RUN_LP)
            {
                workflow_description = CPAIConstantUtil.DESCRIPTION_WAITING_APPROVE_RUN_LP;
            }
            return workflow_description;
        }
        public static List<string> getUserGroup(string user)
        {
            UserGroupDAL ugDAL = new UserGroupDAL();
            List<CPAI_USER_GROUP> ug = ugDAL.getUserList(user, ConstantPrm.SYSTEM.VCOOL);
            if (ug != null)
            {
                List<string> groups = new List<string>();
                foreach (var group in ug)
                {
                    groups.Add(group.USG_USER_GROUP.ToUpper());
                }
                return groups;
            }
            else
            {
                return null;
            }
        }

        public string getParentByTrandID(string tranID)
        {
            FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
            FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(tranID);
            return ft.FTX_PARENT_TRX_ID;
        }

        public List<VCoolReportResult> GetVCoolReport(VCoolReportCriteria criteria)
        {
            try
            {
                var cultureInfo = new CultureInfo("en-US");
                var isSearchTPCMonthRange = false;
                Expression<Func<VCO_DATA, bool>> predicate = GetVCODataPredicate(criteria, cultureInfo, out isSearchTPCMonthRange);

                var monthDictionary = Enumerable.Range(0, 12).ToDictionary(i => cultureInfo.DateTimeFormat.AbbreviatedMonthNames[i], i => cultureInfo.DateTimeFormat.MonthNames[i]);
                var getTPCMonthYear = new Func<string, string, string>((month, year) =>
                {
                    var longMonth = !string.IsNullOrEmpty(month) && monthDictionary.ContainsKey(month) ? monthDictionary[month] : month;
                    return string.Format("{0}-{1}", longMonth, year);
                });

                var result = new VCO_DATA_DAL().SearchVCODataExpression(predicate);
                if (isSearchTPCMonthRange)
                {
                    var monthYearFrom = FunctionTransactionDAL.GetMonthYearNo(criteria.TPCMonthFrom, criteria.TPCYearFrom);
                    var monthYearTo = FunctionTransactionDAL.GetMonthYearNo(criteria.TPCMonthTo, criteria.TPCYearTo);
                    result = result.Where(c =>
                    {
                        var tpcMonthYearNo = FunctionTransactionDAL.GetMonthYearNo(c.VCDA_TPC_PLAN_MONTH, c.VCDA_TPC_PLAN_YEAR);
                        return (monthYearFrom <= tpcMonthYearNo) && (tpcMonthYearNo <= monthYearTo);
                    }).ToList();
                }

                var report = result.Select(c => new VCoolReportResult
                {
                    ActualPrice = c.VCDA_FINAL_PREMIUM,
                    ActualPurchase = c.VCDA_PURCHASE_RESULT,
                    Country = c.VCDA_ORIGIN,
                    CrudeName = c.VCDA_CRUDE_NAME,
                    ProposedPrice = c.VCDA_PREMIUM,
                    PurchaseDate = c.VCDA_DATE_PURCHASE,
                    Supplier = c.VCDA_FK_SUPPLIER,
                    SupplierImport = c.VCDA_SUPPLIER,
                    TPCMonthYear = getTPCMonthYear(c.VCDA_TPC_PLAN_MONTH, c.VCDA_TPC_PLAN_YEAR),
                    TransactionID = c.VCDA_ROW_ID,
                    Volumn = c.VCDA_QUANTITY_KT_MAX
                }).ToList();
                return report;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private Expression<Func<VCO_DATA, bool>> GetVCODataPredicate(VCoolReportCriteria criteria, CultureInfo cultureInfo, out bool isSearchTPCMonthRange)
        {
            isSearchTPCMonthRange = false;
            Expression<Func<VCO_DATA, bool>> predicate = null;

            #region Build Predicate Expression

            #region Common Fields
            if (!string.IsNullOrEmpty(criteria.ActualPurchase))
            {
                predicate = c => c.VCDA_PURCHASE_RESULT.Equals(criteria.ActualPurchase, StringComparison.OrdinalIgnoreCase);
            }
            if (!string.IsNullOrEmpty(criteria.Country))
            {
                Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_ORIGIN.ToUpper().Equals(criteria.Country.ToUpper(), StringComparison.OrdinalIgnoreCase);
                predicate = predicate == null ? expression : predicate.And(expression);
            }
            if (!string.IsNullOrEmpty(criteria.CrudeName))
            {
                Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_CRUDE_NAME.ToUpper().Equals(criteria.CrudeName.ToUpper(), StringComparison.OrdinalIgnoreCase);
                predicate = predicate == null ? expression : predicate.And(expression);
            }
            if (!string.IsNullOrEmpty(criteria.Supplier))
            {
                /*****criteria.Supplier is Id of supplier*****/
                /*****VCDA_FK_SUPPLIER is supplier in the system as Id, VCDA_SUPPLIER is supplier from import as Text*****/
                var supplierList = VendorDAL.GetVendorPIT(CPAIConstantUtil.PROJECT_NAME_PIT, CPAIConstantUtil.ACTIVE).ToDictionary(c => c.VND_ACC_NUM_VENDOR, c => c.VND_NAME1);
                if (supplierList.ContainsKey(criteria.Supplier))
                {
                    var supplierName = supplierList[criteria.Supplier];
                    Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_FK_SUPPLIER.Equals(criteria.Supplier, StringComparison.OrdinalIgnoreCase) || c.VCDA_SUPPLIER.Equals(supplierName, StringComparison.OrdinalIgnoreCase);
                    predicate = predicate == null ? expression : predicate.And(expression);
                }
                else
                {
                    Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_FK_SUPPLIER.Equals(criteria.Supplier, StringComparison.OrdinalIgnoreCase);
                    predicate = predicate == null ? expression : predicate.And(expression);
                }
            }
            #endregion

            #region Date Fields
            if (!string.IsNullOrEmpty(criteria.DischargingDates))
            {
                var dates = GetDatePair(criteria.DischargingDates, cultureInfo);
                if (dates != null && dates.Item1 != DateTime.MinValue && dates.Item2 != DateTime.MinValue)
                {
                    var dateFrom = dates.Item1.Date;
                    var dateTo = dates.Item2.Date.AddDays(1).AddSeconds(-1);
                    Expression<Func<VCO_DATA, bool>> expression = c => DbFunctions.TruncateTime(c.VCDA_DISCHARGING_DATE_FROM) >= dateFrom && DbFunctions.TruncateTime(c.VCDA_DISCHARGING_DATE_TO) <= dateTo;
                    predicate = predicate == null ? expression : predicate.And(expression);
                }
            }
            if (!string.IsNullOrEmpty(criteria.LoadingDates))
            {
                var dates = GetDatePair(criteria.LoadingDates, cultureInfo);
                if (dates != null && dates.Item1 != DateTime.MinValue && dates.Item2 != DateTime.MinValue)
                {
                    var dateFrom = dates.Item1.Date;
                    var dateTo = dates.Item2.Date.AddDays(1).AddSeconds(-1);
                    Expression<Func<VCO_DATA, bool>> expression = c => DbFunctions.TruncateTime(c.VCDA_LOADING_DATE_FROM) >= dateFrom && DbFunctions.TruncateTime(c.VCDA_LOADING_DATE_TO) <= dateTo;
                    predicate = predicate == null ? expression : predicate.And(expression);
                }
            }

            if (!string.IsNullOrEmpty(criteria.TPCMonthFrom))
            {
                if (!string.IsNullOrEmpty(criteria.TPCYearFrom) && !string.IsNullOrEmpty(criteria.TPCMonthTo) && !string.IsNullOrEmpty(criteria.TPCYearTo))
                {
                    /*****Search by TPCMonthFrom-TPCYearFrom and TPCMonthTo-TPCYearTo*****/
                    //Convert Month-Year string to number for search by range
                    var monthYearFrom = FunctionTransactionDAL.GetMonthYearNo(criteria.TPCMonthFrom, criteria.TPCYearFrom);
                    var monthYearTo = FunctionTransactionDAL.GetMonthYearNo(criteria.TPCMonthTo, criteria.TPCYearTo);
                    if (monthYearFrom > 0 && monthYearTo > 0)
                    {
                        //Filter after selected
                        isSearchTPCMonthRange = true;
                    }
                }
                else if (string.IsNullOrEmpty(criteria.TPCYearFrom))
                {
                    /*****Search by TPCMonthFrom only*****/
                    Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_TPC_PLAN_MONTH.Equals(criteria.TPCMonthFrom, StringComparison.OrdinalIgnoreCase);
                    predicate = predicate == null ? expression : predicate.And(expression);
                }
                else
                {
                    /*****Search by TPCMonthFrom and TPCYearFrom*****/
                    Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_TPC_PLAN_MONTH.Equals(criteria.TPCMonthFrom, StringComparison.OrdinalIgnoreCase) && c.VCDA_TPC_PLAN_YEAR.Equals(criteria.TPCYearFrom);
                    predicate = predicate == null ? expression : predicate.And(expression);
                }
            }
            else if (!string.IsNullOrEmpty(criteria.TPCYearFrom))
            {
                /*****Search by TPCYearFrom only*****/
                Expression<Func<VCO_DATA, bool>> expression = c => c.VCDA_TPC_PLAN_YEAR.Equals(criteria.TPCYearFrom);
                predicate = predicate == null ? expression : predicate.And(expression);
            }
            #endregion

            #endregion

            return predicate;
        }
        
        /// <summary>
        /// Convert date string pair to DateTime
        /// </summary>
        /// <param name="dateString">"08-Sep-2017 to 11-Oct-2017"</param>
        /// <returns>Pair of date</returns>
        internal static Tuple<DateTime, DateTime> GetDatePair(string dateString, CultureInfo cultureInfo)
        {
            var pattern = string.Format(@"(?<day>(?<=\s|^)\d{{2}})[-/](?<month>{0})[-/](?<year>\d{{4}}(?=\s|$))", string.Join("|", cultureInfo.DateTimeFormat.AbbreviatedMonthNames.Take(12)));
            var result = new Regex(pattern, RegexOptions.IgnoreCase).Matches(dateString);

            var dates = result.OfType<Match>().Where(c => c.Success).Select(match => {
                var date = DateTime.MinValue;
                var isdate = DateTime.TryParseExact(match.Value, CoolViewModel.FORMAT_DATE, cultureInfo, DateTimeStyles.None, out date);
                return isdate ? date : DateTime.MinValue;
            }).ToArray();
            return (dates.Length == 2) ? new Tuple<DateTime, DateTime>(dates[0], dates[1]) : null;
        }
    }
}