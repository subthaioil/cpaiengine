﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class VehicleServiceModel
    {
        public ReturnValue Add(ref VehicleViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_VEHICLE))
                {
                    rtn.Message = "VEH NAME should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_TYPE))
                {
                    rtn.Message = "VEH TYPE should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_OWNER))
                {
                    rtn.Message = "OWNER should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_LANGUAGE))
                {
                    rtn.Message = "LANGUAGE should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_VEH_TEXT))
                {
                    rtn.Message = "VEH TEXT should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_GROUP))
                {
                    rtn.Message = "GROUP should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Type));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper() + x.Type.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }

                }

                MT_VEHICLE_DAL dal = new MT_VEHICLE_DAL();
                MT_VEHICLE ent = new MT_VEHICLE();

                MT_VEHICLE_CONTROL_DAL dalCtr = new MT_VEHICLE_CONTROL_DAL();
                MT_VEHICLE_CONTROL entCtr = new MT_VEHICLE_CONTROL();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            ShareFn _FN = new ShareFn();

                            var VEH_ID = ShareFn.GenerateCodeByDate("VEH");
                            ent.VEH_ID = VEH_ID;
                            ent.VEH_VEHICLE = pModel.VEH_VEHICLE.Trim();
                            ent.VEH_TYPE = pModel.VEH_TYPE.Trim();
                            ent.VEH_OWNER = pModel.VEH_OWNER.Trim();
                            ent.VEH_LANGUAGE = pModel.VEH_LANGUAGE.Trim();
                            ent.VEH_VEH_TEXT = pModel.VEH_VEH_TEXT.Trim();
                            ent.VEH_GROUP = pModel.VEH_GROUP.Trim();

                            // Show
                            ent.VEH_AGE = pModel.VEH_AGE;
                            ent.VHE_DWT = pModel.VHE_DWT;
                            ent.VHE_CAPACITY = pModel.VHE_CAPACITY;
                            ent.VHE_MAX_DRAFT = pModel.VHE_MAX_DRAFT;
                            ent.VHE_COATING = pModel.VHE_COATING;
                            ent.VHE_FULL_BALLAST = pModel.VHE_FULL_BALLAST;
                            ent.VHE_FULL_LADEN = pModel.VHE_FULL_LADEN;
                            ent.VHE_ECO_BALLAST = pModel.VHE_ECO_BALLAST;
                            ent.VHE_ECO_LADEN = pModel.VHE_ECO_LADEN;

                            // Optional
                            ent.VEH_NRTUS = Convert.ToDecimal(pModel.VEH_NRTUS);
                            ent.VEH_VOL_UOM = pModel.VEH_VOL_UOM;
                            ent.VEH_WGT_UOM = pModel.VEH_WGT_UOM;
                            ent.VEH_DIM_UOM = pModel.VEH_DIM_UOM;
                            ent.VEH_MAXWGT = Convert.ToDecimal(pModel.VEH_MAXWGT);
                            ent.VEH_UNLWGT = Convert.ToDecimal(pModel.VEH_UNLWGT);
                            ent.VEH_MAXVOL = Convert.ToDecimal(pModel.VEH_MAXVOL);
                            ent.VEH_HEIGHT = Convert.ToDecimal(pModel.VEH_HEIGHT);
                            ent.VEH_WIDTH = Convert.ToDecimal(pModel.VEH_WIDTH);
                            ent.VEH_LENGTH = Convert.ToDecimal(pModel.VEH_LENGTH);
                            ent.VEH_DEPOT = pModel.VEH_DEPOT;
                            ent.VEH_NAME_EFF_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_NAME_EFF_DATE);
                            ent.VEH_CLF_EFDT = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_CLF_EFDT);
                            ent.VEH_REG_CNTRY = pModel.VEH_REG_CNTRY;
                            ent.VEH_REG_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_REG_DATE);
                            ent.VEH_REG_OWNER = pModel.VEH_REG_OWNER;
                            ent.VEH_OWN_FLAG = pModel.VEH_OWN_FLAG;
                            ent.VEH_BALLCAP = Convert.ToDecimal(pModel.VEH_BALLCAP);
                            ent.VEH_FL_BNK_CAPA = Convert.ToDecimal(pModel.VEH_FL_BNK_CAPA);
                            ent.VEH_DSL_BNK_CAPA = Convert.ToDecimal(pModel.VEH_DSL_BNK_CAPA);
                            ent.VEH_CLASS_GRP = pModel.VEH_CLASS_GRP;
                            ent.VEH_DRAFT = Convert.ToDecimal(pModel.VEH_DRAFT);
                            ent.VEH_POINT = pModel.VEH_POINT;
                            ent.VEH_CRE_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_CRE_DATE);
                            ent.VEH_CRE_NAME = pModel.VEH_CRE_NAME;
                            ent.VEH_CHA_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_CHA_DATE);
                            ent.VEH_CHA_NAME = pModel.VEH_CHA_NAME;
                            ent.VEH_IMO_NUMBER = pModel.VEH_IMO_NUMBER;
                            ent.VEH_CLASS_SOCIETY = pModel.VEH_CLASS_SOCIETY;
                            ent.VEH_INSERT_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_INSERT_DATE);
                            ent.VEH_VESSEL_CATEGORY = pModel.VEH_VESSEL_CATEGORY;
                            ent.VEH_BUILT = pModel.VEH_BUILT;

                            ent.VHE_CREATE_TYPE = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            ent.VEH_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.VEH_CREATED_BY = pUser;
                            ent.VEH_CREATED_DATE = now;
                            ent.VEH_UPDATED_BY = pUser;
                            ent.VEH_UPDATED_DATE = now;

                            dal.Save(ent, context);

                            foreach (var item in pModel.Control)
                            {
                                entCtr = new MT_VEHICLE_CONTROL();
                                entCtr.MMC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCtr.MCC_FK_VEHICLE = VEH_ID;
                                entCtr.MCC_SYSTEM = String.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                entCtr.MCC_TYPE = String.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                entCtr.MCC_STATUS = item.Status;
                                entCtr.MCC_CREATED_BY = pUser;
                                entCtr.MCC_CREATED_DATE = now;
                                entCtr.MCC_UPDATED_BY = pUser;
                                entCtr.MCC_UPDATED_DATE = now;

                                dalCtr.Save(entCtr, context);
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;

                            pModel.VEH_ID = VEH_ID; // Return ID
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(VehicleViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VHE_CREATE_TYPE))
                {
                    rtn.Message = "CREATE TYPE should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_ID))
                {
                    rtn.Message = "VEH ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_VEHICLE))
                {
                    rtn.Message = "VEH NAME should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_TYPE))
                {
                    rtn.Message = "VEH TYPE should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_OWNER))
                {
                    rtn.Message = "OWNER should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_LANGUAGE))
                {
                    rtn.Message = "LANGUAGE should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_VEH_TEXT))
                {
                    rtn.Message = "VEH TEXT should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VEH_GROUP))
                {
                    rtn.Message = "GROUP should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add controller";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Type));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper() + x.Type.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }

                }

                MT_VEHICLE_DAL dal = new MT_VEHICLE_DAL();
                MT_VEHICLE ent = new MT_VEHICLE();

                MT_VEHICLE_CONTROL_DAL dalCtr = new MT_VEHICLE_CONTROL_DAL();
                MT_VEHICLE_CONTROL entCtr = new MT_VEHICLE_CONTROL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            if (pModel.VHE_CREATE_TYPE.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                            {
                                // Update data in MT_VEHICLE
                                ent.VEH_ID = pModel.VEH_ID;
                                ent.VEH_VEHICLE = pModel.VEH_VEHICLE.Trim();
                                ent.VEH_TYPE = pModel.VEH_TYPE.Trim();
                                ent.VEH_OWNER = pModel.VEH_OWNER.Trim();
                                ent.VEH_LANGUAGE = pModel.VEH_LANGUAGE.Trim();
                                ent.VEH_VEH_TEXT = pModel.VEH_VEH_TEXT.Trim();
                                ent.VEH_GROUP = pModel.VEH_GROUP.Trim();

                                // Show
                                ent.VEH_AGE = pModel.VEH_AGE;
                                ent.VHE_DWT = pModel.VHE_DWT;
                                ent.VHE_CAPACITY = pModel.VHE_CAPACITY;
                                ent.VHE_MAX_DRAFT = pModel.VHE_MAX_DRAFT;
                                ent.VHE_COATING = pModel.VHE_COATING;
                                ent.VHE_FULL_BALLAST = pModel.VHE_FULL_BALLAST;
                                ent.VHE_FULL_LADEN = pModel.VHE_FULL_LADEN;
                                ent.VHE_ECO_BALLAST = pModel.VHE_ECO_BALLAST;
                                ent.VHE_ECO_LADEN = pModel.VHE_ECO_LADEN;

                                // Optional
                                ent.VEH_NRTUS = Convert.ToDecimal(pModel.VEH_NRTUS);
                                ent.VEH_VOL_UOM = pModel.VEH_VOL_UOM;
                                ent.VEH_WGT_UOM = pModel.VEH_WGT_UOM;
                                ent.VEH_DIM_UOM = pModel.VEH_DIM_UOM;
                                ent.VEH_MAXWGT = Convert.ToDecimal(pModel.VEH_MAXWGT);
                                ent.VEH_UNLWGT = Convert.ToDecimal(pModel.VEH_UNLWGT);
                                ent.VEH_MAXVOL = Convert.ToDecimal(pModel.VEH_MAXVOL);
                                ent.VEH_HEIGHT = Convert.ToDecimal(pModel.VEH_HEIGHT);
                                ent.VEH_WIDTH = Convert.ToDecimal(pModel.VEH_WIDTH);
                                ent.VEH_LENGTH = Convert.ToDecimal(pModel.VEH_LENGTH);
                                ent.VEH_DEPOT = pModel.VEH_DEPOT;
                                ent.VEH_NAME_EFF_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_NAME_EFF_DATE);
                                ent.VEH_CLF_EFDT = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_CLF_EFDT);
                                ent.VEH_REG_CNTRY = pModel.VEH_REG_CNTRY;
                                ent.VEH_REG_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_REG_DATE);
                                ent.VEH_REG_OWNER = pModel.VEH_REG_OWNER;
                                ent.VEH_OWN_FLAG = pModel.VEH_OWN_FLAG;
                                ent.VEH_BALLCAP = Convert.ToDecimal(pModel.VEH_BALLCAP);
                                ent.VEH_FL_BNK_CAPA = Convert.ToDecimal(pModel.VEH_FL_BNK_CAPA);
                                ent.VEH_DSL_BNK_CAPA = Convert.ToDecimal(pModel.VEH_DSL_BNK_CAPA);
                                ent.VEH_CLASS_GRP = pModel.VEH_CLASS_GRP;
                                ent.VEH_DRAFT = Convert.ToDecimal(pModel.VEH_DRAFT);
                                ent.VEH_POINT = pModel.VEH_POINT;
                                ent.VEH_CRE_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_CRE_DATE);
                                ent.VEH_CRE_NAME = pModel.VEH_CRE_NAME;
                                ent.VEH_CHA_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_CHA_DATE);
                                ent.VEH_CHA_NAME = pModel.VEH_CHA_NAME;
                                ent.VEH_IMO_NUMBER = pModel.VEH_IMO_NUMBER;
                                ent.VEH_CLASS_SOCIETY = pModel.VEH_CLASS_SOCIETY;
                                ent.VEH_INSERT_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.VEH_INSERT_DATE);
                                ent.VEH_VESSEL_CATEGORY = pModel.VEH_VESSEL_CATEGORY;
                                ent.VEH_BUILT = pModel.VEH_BUILT;

                                ent.VHE_CREATE_TYPE = CPAIConstantUtil.PROJECT_NAME_SPACE;
                                ent.VEH_STATUS = CPAIConstantUtil.ACTIVE;
                                //ent.VEH_CREATED_BY = pUser;
                                //ent.VEH_CREATED_DATE = now;
                                ent.VEH_UPDATED_BY = pUser;
                                ent.VEH_UPDATED_DATE = now;

                                dal.Update(ent, context);                   // CPAI : Update all
                            } 
                           

                            // Insert data to MT_VEHICLE_CONTROL
                            foreach (var item in pModel.Control)
                            {
                                if (string.IsNullOrEmpty(item.RowID))
                                {
                                    // Add
                                    entCtr = new MT_VEHICLE_CONTROL();
                                    entCtr.MMC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MCC_FK_VEHICLE = pModel.VEH_ID;
                                    entCtr.MCC_SYSTEM = String.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MCC_TYPE = String.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                    entCtr.MCC_STATUS = item.Status;
                                    entCtr.MCC_CREATED_BY = pUser;
                                    entCtr.MCC_CREATED_DATE = now;
                                    entCtr.MCC_UPDATED_BY = pUser;
                                    entCtr.MCC_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_VEHICLE_CONTROL();
                                    entCtr.MMC_ROW_ID = item.RowID;
                                    entCtr.MCC_FK_VEHICLE = pModel.VEH_ID;
                                    entCtr.MCC_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MCC_TYPE = string.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                    entCtr.MCC_STATUS = item.Status;
                                    entCtr.MCC_UPDATED_BY = pUser;
                                    entCtr.MCC_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref VehicleViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sVehicleID = String.IsNullOrEmpty(pModel.sVehicleID) ? "" : pModel.sVehicleID.ToUpper();
                    var sVehicleName = String.IsNullOrEmpty(pModel.sVehicleName) ? "" : pModel.sVehicleName.ToLower();
                    var sVehicleText = String.IsNullOrEmpty(pModel.sVehicleText) ? "" : pModel.sVehicleText.ToLower();
                    var sCreateType = String.IsNullOrEmpty(pModel.sCreateType) ? "" : pModel.sCreateType.ToUpper();
                    var sSystem = String.IsNullOrEmpty(pModel.sSystem) ? "" : pModel.sSystem.ToUpper();
                    var sType = String.IsNullOrEmpty(pModel.sType) ? "" : pModel.sType.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();

                    var sDateFrom = String.IsNullOrEmpty(pModel.sCreateDate) ? "" : pModel.sCreateDate.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.sCreateDate) ? "" : pModel.sCreateDate.ToUpper().Substring(14, 10);

                    var query = (from v in context.MT_VEHICLE
                                 join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE into view
                                 from vc in view.DefaultIfEmpty()
                                 orderby new { v.VEH_VEHICLE, vc.MCC_SYSTEM, vc.MCC_TYPE }
                                 select new
                                 {
                                     dCreateDate = vc.MCC_CREATED_DATE == null ? DateTime.MinValue : vc.MCC_CREATED_DATE
                                     ,
                                     dVehicleID = v.VEH_ID
                                     ,
                                     dVehicleName = v.VEH_VEHICLE
                                     ,
                                     dVehicleText = v.VEH_VEH_TEXT
                                     ,
                                     dCreateType = String.IsNullOrEmpty(v.VHE_CREATE_TYPE) ? "SAP" : v.VHE_CREATE_TYPE
                                     ,
                                     dSystem = vc.MCC_SYSTEM
                                     ,
                                     dType = vc.MCC_TYPE
                                     ,
                                     dStatus = vc.MCC_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sVehicleID))
                        query = query.Where(p => p.dVehicleID.ToUpper().Contains(sVehicleID.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sVehicleName))
                        query = query.Where(p => p.dVehicleName.ToUpper().Contains(sVehicleName.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sVehicleText))
                        query = query.Where(p => p.dVehicleText.ToUpper().Contains(sVehicleText.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Equals(sCreateType.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dSystem.ToUpper().Equals(sSystem.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sType))
                        query = query.Where(p => p.dType.ToUpper().Equals(sType.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus.ToUpper().Trim()));

                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dCreateDate >= sDate && p.dCreateDate <= eDate);
                    }

                    if (query != null)
                    {
                        pModel.sSearchData = new List<VehicleViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new VehicleViewModel_SearchData
                                                    {
                                                        dCreateDate = g.dCreateDate == null || g.dCreateDate == DateTime.MinValue ? "" : g.dCreateDate.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture),
                                                        dVehicleID = g.dVehicleID,
                                                        dVehicleName = g.dVehicleName,
                                                        dVehicleText = g.dVehicleText,
                                                        dCreateType =  g.dCreateType,
                                                        dSystem = g.dSystem,
                                                        dType = g.dType,
                                                        dStatus = g.dStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public VehicleViewModel_Detail Get(string pVEH_ID)
        {
            ShareFn _FN = new Utilities.ShareFn();
            VehicleViewModel_Detail model = new VehicleViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pVEH_ID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VEHICLE
                                     where v.VEH_ID.ToUpper().Equals(pVEH_ID.ToUpper())
                                     select v).FirstOrDefault();

                        if (query != null)
                        {

                            model.VEH_ID = query.VEH_ID;
                            model.VEH_VEHICLE = query.VEH_VEHICLE;
                            model.VEH_TYPE = query.VEH_TYPE;
                            model.VEH_OWNER = query.VEH_OWNER;
                            model.VEH_LANGUAGE = query.VEH_LANGUAGE;
                            model.VEH_VEH_TEXT = query.VEH_VEH_TEXT;
                            model.VEH_GROUP = query.VEH_GROUP;

                            // Show
                            model.VEH_AGE = query.VEH_AGE;
                            model.VHE_DWT = query.VHE_DWT;
                            model.VHE_CAPACITY = query.VHE_CAPACITY;
                            model.VHE_MAX_DRAFT = query.VHE_MAX_DRAFT;
                            model.VHE_COATING = query.VHE_COATING;
                            model.VHE_FULL_BALLAST = query.VHE_FULL_BALLAST;
                            model.VHE_FULL_LADEN = query.VHE_FULL_LADEN;
                            model.VHE_ECO_BALLAST = query.VHE_ECO_BALLAST;
                            model.VHE_ECO_LADEN = query.VHE_ECO_LADEN;

                            // Optional
                            model.VEH_NRTUS = Convert.ToString(query.VEH_NRTUS);
                            model.VEH_VOL_UOM = query.VEH_VOL_UOM;
                            model.VEH_WGT_UOM = query.VEH_WGT_UOM;
                            model.VEH_DIM_UOM = query.VEH_DIM_UOM;
                            model.VEH_MAXWGT = Convert.ToString(query.VEH_MAXWGT);
                            model.VEH_UNLWGT = Convert.ToString(query.VEH_UNLWGT);
                            model.VEH_MAXVOL = Convert.ToString(query.VEH_MAXVOL);
                            model.VEH_HEIGHT = Convert.ToString(query.VEH_HEIGHT);
                            model.VEH_WIDTH = Convert.ToString(query.VEH_WIDTH);
                            model.VEH_LENGTH = Convert.ToString(query.VEH_LENGTH);
                            model.VEH_DEPOT = query.VEH_DEPOT;
                            model.VEH_NAME_EFF_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_NAME_EFF_DATE, "dd/MM/yyyy");
                            model.VEH_CLF_EFDT = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_CLF_EFDT, "dd/MM/yyyy");
                            model.VEH_REG_CNTRY = query.VEH_REG_CNTRY;
                            model.VEH_REG_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_REG_DATE, "dd/MM/yyyy");
                            model.VEH_REG_OWNER = query.VEH_REG_OWNER;
                            model.VEH_OWN_FLAG = query.VEH_OWN_FLAG;
                            model.VEH_BALLCAP = Convert.ToString(query.VEH_BALLCAP);
                            model.VEH_FL_BNK_CAPA = Convert.ToString(query.VEH_FL_BNK_CAPA);
                            model.VEH_DSL_BNK_CAPA = Convert.ToString(query.VEH_DSL_BNK_CAPA);
                            model.VEH_CLASS_GRP = query.VEH_CLASS_GRP;
                            model.VEH_DRAFT = Convert.ToString(query.VEH_DRAFT);
                            model.VEH_POINT = query.VEH_POINT;
                            model.VEH_CRE_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_CRE_DATE, "dd/MM/yyyy");
                            model.VEH_CRE_NAME = query.VEH_CRE_NAME;
                            model.VEH_CHA_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_CHA_DATE, "dd/MM/yyyy");
                            model.VEH_CHA_NAME = query.VEH_CHA_NAME;
                            model.VEH_IMO_NUMBER = query.VEH_IMO_NUMBER;
                            model.VEH_CLASS_SOCIETY = query.VEH_CLASS_SOCIETY;
                            model.VEH_INSERT_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_INSERT_DATE, "dd/MM/yyyy");
                            model.VEH_VESSEL_CATEGORY = query.VEH_VESSEL_CATEGORY;
                            model.VEH_BUILT = query.VEH_BUILT;

                            model.VHE_CREATE_TYPE = String.IsNullOrEmpty(query.VHE_CREATE_TYPE) == true ? "SAP" : query.VHE_CREATE_TYPE;

                            model.VEH_STATUS = query.VEH_STATUS;
                            model.VEH_CREATED_BY = query.VEH_CREATED_BY;
                            model.VEH_CREATED_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_CREATED_DATE, "dd/MM/yyyy");
                            model.VEH_UPDATED_BY = query.VEH_UPDATED_BY;
                            model.VEH_UPDATED_DATE = ShareFn.ConvertDateTimeToDateStringFormat(query.VEH_UPDATED_DATE, "dd/MM/yyyy");

                            var queryControl = (from v in context.MT_VEHICLE_CONTROL
                                                 where v.MCC_FK_VEHICLE.ToUpper().Equals(pVEH_ID.ToUpper())
                                                 select v);
                            model.Control = new List<VehicleViewModel_Control>();
                            foreach (var g in queryControl)
                            {
                                model.Control.Add(new VehicleViewModel_Control { RowID = g.MMC_ROW_ID, System = g.MCC_SYSTEM, Type = g.MCC_TYPE, Status = g.MCC_STATUS });
                            }
                            if (model.Control.Count == 0)
                                model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "", Type="", Status = CPAIConstantUtil.ACTIVE });
                        }
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetName(string pVendorID)
        {

            try
            {
                if (String.IsNullOrEmpty(pVendorID) == false)
                {
                   
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VEHICLE
                                    join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE
                                    where v.VEH_ID.ToUpper() == pVendorID.ToUpper()
                                    select v.VEH_VEH_TEXT).FirstOrDefault();
                       

                        return query == null ? "" : query;

                    }

                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetNameInitial(string pVendorID)
        {
            try
            {
                if (String.IsNullOrEmpty(pVendorID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VEHICLE                                     
                                     where v.VEH_ID.ToUpper() == pVendorID.ToUpper()
                                     select v.VEH_VEHICLE).FirstOrDefault();

                        return query == null ? "" : query;
                    }
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetVehTypeJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.VEH_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.VEH_TYPE }
                                    select new { VehType = d.VEH_TYPE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.VehType));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetVehOwnerJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.VEH_OWNER.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.VEH_OWNER }
                                    select new { data = d.VEH_OWNER.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.data));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetVehGroupJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.VEH_GROUP.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.VEH_GROUP }
                                    select new { data = d.VEH_GROUP.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.data));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetCtrlSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCC_SYSTEM }
                                    select new { CtrlSystem = d.MCC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.CtrlSystem));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetCtrlTypeJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCC_TYPE }
                                    select new { CtrlType = d.MCC_TYPE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.CtrlType));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> GetCtrlSystemForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE_CONTROL.Where(q => q.MCC_STATUS.Equals(CPAIConstantUtil.ACTIVE))
                    select v);

                    
                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCC_SYSTEM }
                                    select new { Value = d.MCC_SYSTEM.ToUpper(), Text = d.MCC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        foreach(var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
                        }
                           
                        }

                    }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }
          
        }

        public static List<SelectListItem> GetCtrlTypeForDDL()
        {

            List<SelectListItem> selectList = new List<SelectListItem>();

            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VEHICLE_CONTROL.Where(q => q.MCC_STATUS.Equals(CPAIConstantUtil.ACTIVE))
                                 select v);


                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCC_TYPE }
                                    select new { Value = d.MCC_TYPE.ToUpper(), Text = d.MCC_TYPE.ToUpper() }).Distinct().ToArray();

                        foreach (var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
                        }

                    }

                }

                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }

        }

        public static string getVehicleDDLJson(string System, string Type)
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_VEHICLE
                             join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE into view
                             from vc in view.DefaultIfEmpty()
                             where vc.MCC_SYSTEM.ToUpper() == System.ToUpper()
                                && vc.MCC_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                             orderby new { v.VEH_VEHICLE, vc.MCC_SYSTEM, vc.MCC_TYPE }
                             select new
                             {
                                 dCreateDate = vc.MCC_CREATED_DATE
                                 ,
                                 dVehicleID = v.VEH_ID
                                 ,
                                 dVehicleName = v.VEH_VEHICLE
                                 ,
                                 dVehicleText = v.VEH_VEH_TEXT
                                 ,
                                 dCreateType = v.VHE_CREATE_TYPE
                                 ,
                                 dSystem = vc.MCC_SYSTEM
                                 ,
                                 dType = vc.MCC_TYPE
                                 ,
                                 dStatus = vc.MCC_STATUS
                             });

                if (!string.IsNullOrEmpty(Type))
                {
                    string[] aryType = Type.ToUpper().Split('|');
                    query = query.Where(x => aryType.Contains(x.dType.ToUpper().Trim()));
                }
               

                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.dVehicleID, text = p.dVehicleText }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }

     
    }
}