﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class Vendor
    {
        public VendorCode vendor_code { get; set; }
        public string vendor_name { get; set; }
        public Country country { get; set; }
        public string contract_address { get; set; }
        public Status status { get; set; }
    }

    public enum VendorCode
    {
        Test1,
        Test2,
        Test3
    }

    public enum Country
    {
        TH,
        KW
    }

    public enum Status
    {
        ACTIVE,
        INACTIVE
    }
}