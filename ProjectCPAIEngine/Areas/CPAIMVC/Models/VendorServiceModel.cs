﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class VendorServiceModel
    {
        public ReturnValue Add(ref VendorViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VendorName))
                {
                    rtn.Message = "Vendor Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CountryKey))
                {
                    rtn.Message = "Country Key should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Type));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    for (int i = 0; i < pModel.Control.Count; i++)
                    {
                        if (string.IsNullOrEmpty(pModel.Control[i].Color))
                        {
                            pModel.Control[i].Color = CPAIConstantUtil.COLOR_DEFUALT;
                        }
                    }
                    var colorEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Color));
                    if (colorEmpty == true)
                    {
                        rtn.Message = "Color should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper() + x.Type.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                    //var systemDuplicate = pModel.Control.GroupBy(x => x.System.ToUpper()).Any(g => g.Count() > 1);
                    //if (systemDuplicate == true)
                    //{
                    //    rtn.Message = "System is duplicate";
                    //    rtn.Status = false;
                    //    return rtn;
                    //}

                    //var typeDuplicate = pModel.Control.GroupBy(x => x.Type.ToUpper()).Any(g => g.Count() > 1);

                    //if (typeDuplicate == true)
                    //{
                    //    rtn.Message = "Type is duplicate";
                    //    rtn.Status = false;
                    //    return rtn;
                    //}


                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_VENDOR_DAL dal = new MT_VENDOR_DAL();
                MT_VENDOR ent = new MT_VENDOR();

                MT_VENDOR_CONTROL_DAL dalCtr = new MT_VENDOR_CONTROL_DAL();
                MT_VENDOR_CONTROL entCtr = new MT_VENDOR_CONTROL();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var vendorCode = ShareFn.GenerateCodeByDate("VND");
                            ent.VND_ACC_NUM_VENDOR = vendorCode;
                            ent.VND_NAME1 = pModel.VendorName.Trim();
                            ent.VND_BROKER_COMMISSION = pModel.Commission;
                            ent.VND_COUNTRY_KEY = pModel.CountryKey;
                            ent.VND_CREATE_TYPE = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            ent.VND_CONTRACT = String.IsNullOrEmpty(pModel.Contract) ? "" : pModel.Contract.Trim();
                            ent.VND_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.VND_CREATED_BY = pUser;
                            ent.VND_CREATED_DATE = now;
                            ent.VND_UPDATED_BY = pUser;
                            ent.VND_UPDATED_DATE = now;

                            dal.Save(ent, context);

                            foreach (var item in pModel.Control)
                            {
                                entCtr = new MT_VENDOR_CONTROL();
                                entCtr.MVC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCtr.MVC_FK_VENDOR = vendorCode;
                                entCtr.MVC_SYSTEM = String.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                entCtr.MVC_TYPE = String.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                entCtr.MVC_NAME = String.IsNullOrEmpty(item.Name) ? "" : item.Name.Trim().ToUpper();
                                entCtr.MVC_COLOR_CODE = item.Color;
                                entCtr.MVC_STATUS = item.Status;
                                entCtr.MVC_CREATED_BY = pUser;
                                entCtr.MVC_CREATED_DATE = now;
                                entCtr.MVC_UPDATED_BY = pUser;
                                entCtr.MVC_UPDATED_DATE = now;

                                dalCtr.Save(entCtr, context);
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;

                            pModel.VendorCode = vendorCode; // Return Code

                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(VendorViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VendorCode))
                {
                    rtn.Message = "Vendor Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CreateType))
                {
                    rtn.Message = "Create Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VendorName) && pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Vendor Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CountryKey) && pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Country Key should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Type));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }
                    for (int i = 0; i < pModel.Control.Count; i++)
                    {
                        if (string.IsNullOrEmpty(pModel.Control[i].Color))
                        {
                            pModel.Control[i].Color = CPAIConstantUtil.COLOR_DEFUALT;
                        }
                    }
                    var colorEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Color));
                    if (colorEmpty == true)
                    {
                        rtn.Message = "Color should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper() + x.Type.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                    //var systemDuplicate = pModel.Control.GroupBy(x => x.System.ToUpper()).Any(g => g.Count() > 1);
                    //if (systemDuplicate == true)
                    //{
                    //    rtn.Message = "System is duplicate";
                    //    rtn.Status = false;
                    //    return rtn;
                    //}

                    //var typeDuplicate = pModel.Control.GroupBy(x => x.Type.ToUpper()).Any(g => g.Count() > 1);

                    //if (typeDuplicate == true)
                    //{
                    //    rtn.Message = "Type is duplicate";
                    //    rtn.Status = false;
                    //    return rtn;
                    //}


                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_VENDOR_DAL dal = new MT_VENDOR_DAL();
                MT_VENDOR ent = new MT_VENDOR();

                MT_VENDOR_CONTROL_DAL dalCtr = new MT_VENDOR_CONTROL_DAL();
                MT_VENDOR_CONTROL entCtr = new MT_VENDOR_CONTROL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            // Update data in MT_VENDOR
                            ent.VND_ACC_NUM_VENDOR = pModel.VendorCode;
                            ent.VND_NAME1 = pModel.VendorName;
                            ent.VND_BROKER_COMMISSION = pModel.Commission;
                            ent.VND_COUNTRY_KEY = pModel.CountryKey;
                            ent.VND_CREATE_TYPE = pModel.CreateType;
                            ent.VND_CONTRACT = pModel.Contract;
                            ent.VND_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.VND_UPDATED_BY = pUser;
                            ent.VND_UPDATED_DATE = now;

                            if (pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                                dal.Update(ent, context);                   // CPAI : Update all
                            else
                                dal.UpdateStatus(ent, context);             // SAP : Update status

                            // Insert data to MT_VENDOR_CONTROL
                            foreach (var item in pModel.Control)
                            {
                                if (string.IsNullOrEmpty(item.RowID))
                                {
                                    // Add
                                    entCtr = new MT_VENDOR_CONTROL();
                                    entCtr.MVC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MVC_FK_VENDOR = pModel.VendorCode;
                                    entCtr.MVC_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MVC_TYPE = string.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                    entCtr.MVC_NAME = string.IsNullOrEmpty(item.Name) ? "" : item.Name.Trim().ToUpper();
                                    entCtr.MVC_COLOR_CODE = item.Color;
                                    entCtr.MVC_STATUS = item.Status;
                                    entCtr.MVC_CREATED_BY = pUser;
                                    entCtr.MVC_CREATED_DATE = now;
                                    entCtr.MVC_UPDATED_BY = pUser;
                                    entCtr.MVC_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_VENDOR_CONTROL();
                                    entCtr.MVC_ROW_ID = item.RowID;
                                    entCtr.MVC_FK_VENDOR = pModel.VendorCode;
                                    entCtr.MVC_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MVC_TYPE = string.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                    entCtr.MVC_NAME = string.IsNullOrEmpty(item.Name) ? "" : item.Name.Trim().ToUpper();
                                    entCtr.MVC_COLOR_CODE = item.Color;
                                    entCtr.MVC_STATUS = item.Status;
                                    entCtr.MVC_UPDATED_BY = pUser;
                                    entCtr.MVC_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref VendorViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sCode = String.IsNullOrEmpty(pModel.sVendorCode) ? "" : pModel.sVendorCode.ToUpper();
                    var sName = String.IsNullOrEmpty(pModel.sVendorName) ? "" : pModel.sVendorName.ToLower();
                    var sCommission = String.IsNullOrEmpty(pModel.sCommission) ? "" : pModel.sCommission.ToLower();
                    var sCountry = String.IsNullOrEmpty(pModel.sCountry) ? "" : pModel.sCountry.ToUpper();
                    var sCreateType = String.IsNullOrEmpty(pModel.sCreateType) ? "" : pModel.sCreateType.ToUpper();
                    var sSystem = String.IsNullOrEmpty(pModel.sSystem) ? "" : pModel.sSystem.ToUpper();
                    var sType = String.IsNullOrEmpty(pModel.sType) ? "" : pModel.sType.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();

                    var query = (from v in context.MT_VENDOR
                                 join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR into view
                                 from vc in view.DefaultIfEmpty()
                                 orderby new { v.VND_ACC_NUM_VENDOR, vc.MVC_SYSTEM, vc.MVC_TYPE }
                                 select new VendorViewModel_SeachData
                                 {
                                     dVendorCode = v.VND_ACC_NUM_VENDOR
                                     ,
                                     dVendorName = v.VND_NAME1
                                     ,
                                     dCommission = v.VND_BROKER_COMMISSION
                                     ,
                                     dCountry = v.VND_COUNTRY_KEY
                                     ,
                                     dCreateType = String.IsNullOrEmpty(v.VND_CREATE_TYPE) ? "SAP" : v.VND_CREATE_TYPE
                                     ,
                                     dSystem = vc.MVC_SYSTEM
                                     ,
                                     dType = vc.MVC_TYPE
                                     ,
                                     dStatus = vc.MVC_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sCode))
                        query = query.Where(p => p.dVendorCode.ToUpper().Contains(sCode.Trim()));

                    if (!string.IsNullOrEmpty(sName))
                        query = query.Where(p => p.dVendorName.ToLower().Contains(sName.Trim()));

                    if (!string.IsNullOrEmpty(sCommission))
                        query = query.Where(p => p.dCommission.ToLower().Contains(sCommission.Trim()));

                    if (!string.IsNullOrEmpty(sCountry))
                        query = query.Where(p => p.dCountry.ToUpper().Equals(sCountry));

                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Equals(sCreateType));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dSystem.ToUpper().Equals(sSystem));

                    if (!string.IsNullOrEmpty(sType))
                        query = query.Where(p => p.dType.ToUpper().Equals(sType));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));


                    if (query != null)
                    {
                        pModel.sSearchData = new List<VendorViewModel_SeachData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new VendorViewModel_SeachData
                                                    {
                                                        dVendorCode = g.dVendorCode,
                                                        dVendorName = g.dVendorName,
                                                        dCommission = g.dCommission,
                                                        dCountry = g.dCountry,
                                                        dCreateType = g.dCreateType,
                                                        dSystem = g.dSystem,
                                                        dType = g.dType,
                                                        dStatus = g.dStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public VendorViewModel_Detail Get(string pVendorCode)
        {
            VendorViewModel_Detail model = new VendorViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pVendorCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VENDOR
                                     where v.VND_ACC_NUM_VENDOR.ToUpper().Equals(pVendorCode.ToUpper())
                                     join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR into view
                                     from vc in view.DefaultIfEmpty()
                                     orderby new { vc.MVC_SYSTEM, vc.MVC_TYPE }
                                     select new
                                     {
                                         dVendorCode = v.VND_ACC_NUM_VENDOR
                                         ,
                                         dVendorName = v.VND_NAME1
                                         ,
                                         dCommission = v.VND_BROKER_COMMISSION
                                         ,
                                         dCountry = v.VND_COUNTRY_KEY
                                         ,
                                         dCreateType = String.IsNullOrEmpty(v.VND_CREATE_TYPE) ? "SAP" : v.VND_CREATE_TYPE
                                         ,
                                         dContact = v.VND_CONTRACT
                                         ,
                                         dStatus = v.VND_STATUS
                                         ,
                                         dCtrlRowID = vc.MVC_ROW_ID
                                         ,
                                         dCtrlSystem = vc.MVC_SYSTEM
                                         ,
                                         dCtrlType = vc.MVC_TYPE
                                         ,
                                         dCtrlName = vc.MVC_NAME
                                         ,
                                         dCtrlColor = vc.MVC_COLOR_CODE
                                         ,
                                         dCtrlStatus = vc.MVC_STATUS
                                     });

                        if (query != null)
                        {
                            model.Control = new List<VendorViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                model.CreateType = g.dCreateType;
                                model.VendorCode = g.dVendorCode;
                                model.VendorName = g.dVendorName;
                                model.Commission = g.dCommission;
                                model.CountryKey = g.dCountry;
                                model.Contract = g.dContact;
                                if (!string.IsNullOrEmpty(g.dCtrlRowID))
                                    model.Control.Add(new VendorViewModel_Control { RowID = g.dCtrlRowID, Order = i.ToString(), System = g.dCtrlSystem, Type = g.dCtrlType, Name = g.dCtrlName, Color = g.dCtrlColor, Status = g.dCtrlStatus });

                                i++;
                            }
                        }
                    }

                    if (model.Control.Count == 0)
                    {
                        model.Control.Add(new VendorViewModel_Control { RowID = "", Order = "1", System = "", Type = "", Name = "", Color = CPAIConstantUtil.COLOR_DEFUALT, Status = CPAIConstantUtil.ACTIVE });
                    }
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VENDOR_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MVC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MVC_SYSTEM }
                                    select new { System = d.MVC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetSystemForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VENDOR_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MVC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MVC_SYSTEM }
                                    select new { System = d.MVC_SYSTEM.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetTypeJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VENDOR_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MVC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MVC_TYPE }
                                    select new { Type = d.MVC_TYPE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Type));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetTypeForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VENDOR_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MVC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MVC_TYPE }
                                    select new { Type = d.MVC_TYPE.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.Type))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.Type, Text = item.Type });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetName(string pVendorCode)
        {

            try
            {
                if (String.IsNullOrEmpty(pVendorCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VENDOR
                                     where v.VND_ACC_NUM_VENDOR.ToUpper().Equals(pVendorCode.ToUpper())
                                     select v.VND_NAME1).FirstOrDefault();

                        return query == null ? "" : query;

                    }

                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string getVendorDDLJson(string System, string Type)
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == System.ToUpper()
                            && vc.MVC_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                            select new
                            {
                                Num = v.VND_ACC_NUM_VENDOR
                            ,
                                Name = v.VND_NAME1
                            ,
                                Commission = v.VND_BROKER_COMMISSION
                            ,
                                Country = v.VND_COUNTRY_KEY
                            ,
                                System = vc.MVC_SYSTEM
                            ,
                                Type = vc.MVC_TYPE
                            ,
                                Color = vc.MVC_COLOR_CODE
                            ,
                                Status = vc.MVC_STATUS
                            };

                if (!string.IsNullOrEmpty(Type))
                {
                    string[] aryType = Type.ToUpper().Split('|');
                    query = query.Where(x => aryType.Contains(x.Type.ToUpper().Trim()));
                }



                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.Num, text = p.Name }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }

        public static List<SelectListItem> getVendorDDL(string System, string Type, string Status = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            select new
                            {
                                Num = v.VND_ACC_NUM_VENDOR
                            ,
                                Name = v.VND_NAME1
                            ,
                                Commission = v.VND_BROKER_COMMISSION
                            ,
                                Country = v.VND_COUNTRY_KEY
                            ,
                                System = vc.MVC_SYSTEM
                            ,
                                Type = vc.MVC_TYPE
                            ,
                                Color = vc.MVC_COLOR_CODE
                            ,
                                Status = vc.MVC_STATUS
                            };

                if (!string.IsNullOrEmpty(System))
                    query = query.Where(p => p.System.ToUpper().Equals(System.ToUpper()));

                if (!string.IsNullOrEmpty(Type))
                    query = query.Where(p => p.Type.ToUpper().Equals(Type.ToUpper()));

                if (!string.IsNullOrEmpty(Status))
                    query = query.Where(p => p.Status.ToUpper().Equals(Status.ToUpper()));

                if (query != null)
                {

                    var data = (from p in query
                                select new { value = p.Num, text = p.Name }).Distinct().ToArray();

                    foreach (var item in data.OrderBy(p => p.text))
                    {
                        rtn.Add(new SelectListItem { Value = item.value, Text = item.text });
                    }
                }

            }

            return rtn;
        }

        public static List<VendorViewModel_SeachData> getVendorControl(string System, string Type, string Status = "ACTIVE")
        {
            List<VendorViewModel_SeachData> rtn = new List<VendorViewModel_SeachData>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            select new
                            {
                                ID = v.VND_ACC_NUM_VENDOR
                            ,
                                Name = vc.MVC_NAME
                            ,
                                System = vc.MVC_SYSTEM
                            ,
                                Type = vc.MVC_TYPE
                            ,
                                Status = vc.MVC_STATUS
                            };

                if (!string.IsNullOrEmpty(System))
                    query = query.Where(p => p.System.ToUpper().Equals(System.ToUpper()));

                if (!string.IsNullOrEmpty(Type))
                    query = query.Where(p => p.Type.ToUpper().Equals(Type.ToUpper()));

                if (!string.IsNullOrEmpty(Status))
                    query = query.Where(p => p.Status.ToUpper().Equals(Status.ToUpper()));

                if (query != null)
                {
                    foreach (var g in query)
                    {
                        rtn.Add(
                            new VendorViewModel_SeachData
                            {
                                dVendorCode = g.ID,
                                dVendorName = g.Name,
                                dSystem = g.System,
                                dType = g.Type,
                                dStatus = g.Status
                            });
                    }
                }

            }

            return rtn;
        }

    }
}
