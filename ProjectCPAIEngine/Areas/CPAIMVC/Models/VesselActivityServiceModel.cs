﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class VesselActivityServiceModel
    {
        public ReturnValue Add(ref VesselActivityViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VACode))
                {
                    rtn.Message = "Vessel Activity Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VAStdPhraseEN))
                {
                    rtn.Message = "Standard Phrase EN should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VAStdPhraseTH))
                {
                    rtn.Message = "Standard Phrase TH should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VAKey))
                {
                    rtn.Message = "Vessel Activity Key should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACSystem));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACType));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var colorEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACColor));
                    if (colorEmpty == true)
                    {
                        rtn.Message = "Color should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACStatus));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.VACSystem.Trim().ToUpper() + x.VACType.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }

                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_VESSEL_ACTIVITY_DAL dal = new MT_VESSEL_ACTIVITY_DAL();
                MT_VESSEL_ACTIVITY ent = new MT_VESSEL_ACTIVITY();

                MT_VESSEL_ACTIVITY_CONTROL_DAL dalCtr = new MT_VESSEL_ACTIVITY_CONTROL_DAL();
                MT_VESSEL_ACTIVITY_CONTROL entCtr = new MT_VESSEL_ACTIVITY_CONTROL();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var RowID = ShareFn.GenerateCodeByDate("MVA");
                            
                            ent.MVA_ROW_ID = RowID;
                            ent.MVA_CODE = pModel.VACode;
                            ent.MVA_STANDARD_PHRASE_EN = pModel.VAStdPhraseEN;
                            ent.MVA_STANDARD_PHRASE_TH = pModel.VAStdPhraseTH;
                            ent.MVA_KEY = pModel.VAKey;
                            ent.MVA_STD_TIME = pModel.VAStdTime;
                            ent.MVA_MIN_USE = pModel.VAMinUseFlag ? "Y" : "N";
                            ent.MVA_REF = pModel.VARefFlag ? "Y" : "N";

                            ent.MVA_CREATED_BY = pUser;
                            ent.MVA_CREATED_DATE = now;
                            ent.MVA_UPDATED_BY = pUser;
                            ent.MVA_UPDATED_DATE = now;

                            dal.Save(ent, context);

                            foreach (var item in pModel.Control)
                            {
                                entCtr = new MT_VESSEL_ACTIVITY_CONTROL();
                                entCtr.MAC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCtr.MAC_FK_VESSEL_ACTIVITY = RowID;
                                entCtr.MAC_SYSTEM = String.IsNullOrEmpty(item.VACSystem) ? "" : item.VACSystem.Trim().ToUpper();
                                entCtr.MAC_TYPE = String.IsNullOrEmpty(item.VACType) ? "" : item.VACType.Trim().ToUpper();
                                entCtr.MAC_COLOR_CODE = item.VACColor;
                                entCtr.MAC_STATUS = item.VACStatus;
                                entCtr.MAC_CREATED_BY = pUser;
                                entCtr.MAC_CREATED_DATE = now;
                                entCtr.MAC_UPDATED_BY = pUser;
                                entCtr.MAC_UPDATED_DATE = now;

                                dalCtr.Save(entCtr, context);
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;

                            pModel.VARowID = RowID; // Return ID
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(VesselActivityViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VARowID))
                {
                    rtn.Message = "Vessel Activity Row ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VACode))
                {
                    rtn.Message = "Vessel Activity Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VAStdPhraseEN))
                {
                    rtn.Message = "Standard Phrase EN should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VAStdPhraseTH))
                {
                    rtn.Message = "Standard Phrase TH should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.VAKey))
                {
                    rtn.Message = "Vessel Activity Key should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACSystem));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACType));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var colorEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACColor));
                    if (colorEmpty == true)
                    {
                        rtn.Message = "Color should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.VACStatus));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.VACSystem.Trim().ToUpper() + x.VACType.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }


                MT_VESSEL_ACTIVITY_DAL dal = new MT_VESSEL_ACTIVITY_DAL();
                MT_VESSEL_ACTIVITY ent = new MT_VESSEL_ACTIVITY();

                MT_VESSEL_ACTIVITY_CONTROL_DAL dalCtr = new MT_VESSEL_ACTIVITY_CONTROL_DAL();
                MT_VESSEL_ACTIVITY_CONTROL entCtr = new MT_VESSEL_ACTIVITY_CONTROL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            // Update data in MT_VESSEL_ACTIVITY
                            ent.MVA_ROW_ID = pModel.VARowID;
                            ent.MVA_CODE = pModel.VACode;
                            ent.MVA_STANDARD_PHRASE_EN = pModel.VAStdPhraseEN;
                            ent.MVA_STANDARD_PHRASE_TH = pModel.VAStdPhraseTH;
                            ent.MVA_KEY = pModel.VAKey;
                            ent.MVA_STD_TIME = pModel.VAStdTime;
                            ent.MVA_MIN_USE = pModel.VAMinUseFlag ? "Y" : "N";
                            ent.MVA_REF = pModel.VARefFlag ? "Y" : "N";

                            ent.MVA_UPDATED_BY = pUser;
                            ent.MVA_UPDATED_DATE = now;
                           
                            dal.Update(ent, context);
                           
                            // Insert data to MT_VESSEL_ACTIVITY_CONTROL
                            foreach (var item in pModel.Control)
                            {
                                if (string.IsNullOrEmpty(item.VACRowID))
                                {
                                    // Add
                                    entCtr = new MT_VESSEL_ACTIVITY_CONTROL();
                                    entCtr.MAC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MAC_FK_VESSEL_ACTIVITY = pModel.VARowID;
                                    entCtr.MAC_SYSTEM = string.IsNullOrEmpty(item.VACSystem) ? "" : item.VACSystem.Trim().ToUpper();
                                    entCtr.MAC_TYPE = string.IsNullOrEmpty(item.VACType) ? "" : item.VACType.Trim().ToUpper();
                                    entCtr.MAC_COLOR_CODE = item.VACColor;
                                    entCtr.MAC_STATUS = item.VACStatus;
                                    entCtr.MAC_CREATED_BY = pUser;
                                    entCtr.MAC_CREATED_DATE = now;
                                    entCtr.MAC_UPDATED_BY = pUser;
                                    entCtr.MAC_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_VESSEL_ACTIVITY_CONTROL();
                                    entCtr.MAC_ROW_ID = item.VACRowID;
                                    entCtr.MAC_FK_VESSEL_ACTIVITY = pModel.VARowID;
                                    entCtr.MAC_SYSTEM = string.IsNullOrEmpty(item.VACSystem) ? "" : item.VACSystem.Trim().ToUpper();
                                    entCtr.MAC_TYPE = string.IsNullOrEmpty(item.VACType) ? "" : item.VACType.Trim().ToUpper();
                                    entCtr.MAC_COLOR_CODE = item.VACColor;
                                    entCtr.MAC_STATUS = item.VACStatus;
                                    entCtr.MAC_UPDATED_BY = pUser;
                                    entCtr.MAC_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref VesselActivityViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sCode = String.IsNullOrEmpty(pModel.sVACode) ? "" : pModel.sVACode.ToUpper();
                    var sNameEN = String.IsNullOrEmpty(pModel.sVAStdPhraseEN) ? "" : pModel.sVAStdPhraseEN.ToLower();
                    var sNameTH = String.IsNullOrEmpty(pModel.sVAStdPhraseTH) ? "" : pModel.sVAStdPhraseTH;
                    var sVAKey = String.IsNullOrEmpty(pModel.sVAKey) ? "" : pModel.sVAKey.ToUpper();
                    
                    var sSystem = String.IsNullOrEmpty(pModel.sVACSystem) ? "" : pModel.sVACSystem.ToUpper();
                    var sType = String.IsNullOrEmpty(pModel.sVACType) ? "" : pModel.sVACType.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sVACStatus) ? "" : pModel.sVACStatus.ToUpper();

                    var query = (from v in context.MT_VESSEL_ACTIVITY
                                 join vc in context.MT_VESSEL_ACTIVITY_CONTROL on v.MVA_ROW_ID equals vc.MAC_FK_VESSEL_ACTIVITY into view
                                 from vc in view.DefaultIfEmpty()
                                 orderby new { v.MVA_STANDARD_PHRASE_EN, vc.MAC_SYSTEM, vc.MAC_TYPE }
                                 select new VesselActivityViewModel_SeachData
                                 {
                                     dVARowID = v.MVA_ROW_ID
                                     ,
                                     dVACode = v.MVA_CODE
                                     ,
                                     dVAStdPhraseEN = v.MVA_STANDARD_PHRASE_EN
                                     ,
                                     dVAStdPhraseTH = v.MVA_STANDARD_PHRASE_TH
                                     ,
                                     dVAKey = v.MVA_KEY
                                     ,
                                     dVACSystem = vc.MAC_SYSTEM
                                     ,
                                     dVACType = vc.MAC_TYPE
                                     ,
                                     dVACStatus = vc.MAC_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sCode))
                        query = query.Where(p => p.dVACode.ToUpper().Contains(sCode.Trim()));

                    if (!string.IsNullOrEmpty(sNameEN))
                        query = query.Where(p => p.dVAStdPhraseEN.ToLower().Contains(sNameEN.Trim()));

                    if (!string.IsNullOrEmpty(sNameTH))
                        query = query.Where(p => p.dVAStdPhraseTH.ToLower().Contains(sNameTH.Trim()));

                    if (!string.IsNullOrEmpty(sVAKey))
                        query = query.Where(p => p.dVAKey.ToUpper().Contains(sVAKey));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dVACSystem.ToUpper().Contains(sSystem));

                    if (!string.IsNullOrEmpty(sType))
                        query = query.Where(p => p.dVACType.ToUpper().Contains(sType));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dVACStatus.ToUpper().Equals(sStatus));


                    if (query != null)
                    {
                        pModel.sSearchData = new List<VesselActivityViewModel_SeachData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new VesselActivityViewModel_SeachData
                                                    {
                                                        dVARowID = g.dVARowID,
                                                        dVACode = g.dVACode,
                                                        dVAStdPhraseEN = g.dVAStdPhraseEN,
                                                        dVAStdPhraseTH = g.dVAStdPhraseTH,
                                                        dVAKey = g.dVAKey,
                                                        dVACSystem = g.dVACSystem,
                                                        dVACType = g.dVACType,
                                                        dVACStatus = g.dVACStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public VesselActivityViewModel_Detail Get(string pVARowID)
        {
            VesselActivityViewModel_Detail model = new VesselActivityViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pVARowID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VESSEL_ACTIVITY
                                     where v.MVA_ROW_ID.ToUpper().Equals(pVARowID.ToUpper())
                                     join vc in context.MT_VESSEL_ACTIVITY_CONTROL on v.MVA_ROW_ID equals vc.MAC_FK_VESSEL_ACTIVITY into view
                                     from vc in view.DefaultIfEmpty()
                                     orderby new { vc.MAC_SYSTEM, vc.MAC_TYPE }
                                     select new
                                     {
                                         dVARowID = v.MVA_ROW_ID
                                        ,
                                         dVACode = v.MVA_CODE
                                        ,
                                         dVAStdPhraseEN = v.MVA_STANDARD_PHRASE_EN
                                        ,
                                         dVAStdPhraseTH = v.MVA_STANDARD_PHRASE_TH
                                        ,
                                         dVAKey = v.MVA_KEY
                                        ,
                                         dVAStdTime = v.MVA_STD_TIME
                                        ,
                                         dVAMinUse = v.MVA_MIN_USE
                                        ,
                                         dVARef = v.MVA_REF
                                        ,
                                         dVACRowID = vc.MAC_ROW_ID
                                        ,
                                         dVACSystem = vc.MAC_SYSTEM
                                        ,
                                         dVACType = vc.MAC_TYPE
                                        ,
                                         dVACColor = vc.MAC_COLOR_CODE
                                        ,
                                         dVACStatus = vc.MAC_STATUS
                                     });

                        if (query != null)
                        {
                            model.Control = new List<VesselActivityViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                model.VARowID = g.dVARowID;
                                model.VACode = g.dVACode;
                                model.VAStdPhraseEN = g.dVAStdPhraseEN;
                                model.VAStdPhraseTH = g.dVAStdPhraseTH;
                                model.VAKey = g.dVAKey;
                                model.VAStdTime = g.dVAStdTime;
                                model.VAMinUseFlag = g.dVAMinUse == "Y" ? true : false;
                                model.VARefFlag = g.dVARef == "Y" ? true : false ;
                                if (!string.IsNullOrEmpty(g.dVACRowID))
                                    model.Control.Add(new VesselActivityViewModel_Control { VACRowID = g.dVACRowID, VACOrder = i.ToString(), VACSystem = g.dVACSystem, VACType = g.dVACType, VACColor = g.dVACColor, VACStatus = g.dVACStatus });
                                i++;
                            }
                        }
                    }

                    if (model.Control.Count < 1)
                    {
                        model.Control.Add(new VesselActivityViewModel_Control { VACRowID = "", VACOrder = "1", VACSystem = "", VACType = "", VACColor = CPAIConstantUtil.COLOR_DEFUALT, VACStatus = CPAIConstantUtil.ACTIVE });
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetName(string pActivityID, string pUserType)
        {

            try
            {
                if (String.IsNullOrEmpty(pActivityID) == false && String.IsNullOrEmpty(pUserType) == false)
                {
                    string pActivityType = Convert.ToString(pUserType) == "VESSEL" ? "CMMT" : "CMCS";
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VESSEL_ACTIVITY
                                     join vc in context.MT_VESSEL_ACTIVITY_CONTROL on v.MVA_ROW_ID equals vc.MAC_FK_VESSEL_ACTIVITY
                                     where v.MVA_ROW_ID.ToUpper() == pActivityID.ToUpper()
                                     && vc.MAC_TYPE.ToUpper() == pActivityType.ToUpper()
                                     select new { v.MVA_CODE, v.MVA_STANDARD_PHRASE_EN }).FirstOrDefault();


                        return query == null ? "" : query.MVA_CODE + " | "+ query.MVA_STANDARD_PHRASE_EN;

                    }

                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetStatus_Color_RowID(string pActivityID, string pUserType)
        {

            try
            {
                if (String.IsNullOrEmpty(pActivityID) == false && String.IsNullOrEmpty(pUserType) == false)
                {
                    string pActivityType = Convert.ToString(pUserType) == "VESSEL" ? "CMMT" : "CMCS";
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_VESSEL_ACTIVITY
                                     join vc in context.MT_VESSEL_ACTIVITY_CONTROL on v.MVA_ROW_ID equals vc.MAC_FK_VESSEL_ACTIVITY
                                     where v.MVA_ROW_ID.ToUpper() == pActivityID.ToUpper()
                                     && vc.MAC_TYPE.ToUpper() == pActivityType.ToUpper()
                                     select new { v.MVA_REF, vc.MAC_COLOR_CODE, v.MVA_ROW_ID }).FirstOrDefault();

                        if (query == null)
                            return "";

                        var strRef = Convert.ToString(query.MVA_REF) == "Y" ? "Y" : "N";
                        var strColor = string.IsNullOrEmpty(query.MAC_COLOR_CODE) == true ? CPAIConstantUtil.COLOR_DEFUALT : query.MAC_COLOR_CODE;
                        return query == null ? "" : strRef + "|" + strColor + "|"+ query.MVA_ROW_ID;

                    }

                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static List<SelectListItem> GetActivityForDDL(string pUserType)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();    
            if (pUserType == "VESSEL")
            {
                List<VesselActivityViewModel_Schedule> data = GetVesselActivityControl(pUserType, ACTIVE).OrderBy(x => x.act_Name).OrderBy(x => x.act_Code).ToList();
                for (int i = 0; i < data.Count; i++)
                {
                    string text = data[i].act_Name;
                    var Ref = Convert.ToString(data[i].act_RefStatus) == "Y" ? "Y" : "N";
                    var Color = string.IsNullOrEmpty(data[i].act_Color) == true ? COLOR_DEFUALT : data[i].act_Color;
                    string value = Ref + "|" + Color + "|" + data[i].act_RowID;
                    selectList.Add(new SelectListItem { Text = text, Value = value });
                }
            } else
            {
                List<VesselActivityViewModel_Schedule> data = GetVesselActivityControl(pUserType, ACTIVE).OrderBy(x => x.act_Name).OrderBy(x => x.act_Code).ToList();
                for (int i = 0; i < data.Count; i++)
                {
                    string text = data[i].act_Code + " | " + data[i].act_Name;
                    var Ref = Convert.ToString(data[i].act_RefStatus) == "Y" ? "Y" : "N";
                    var Color = string.IsNullOrEmpty(data[i].act_Color) == true ? COLOR_DEFUALT : data[i].act_Color;
                    string value = Ref + "|" + Color + "|" + data[i].act_RowID;
                    selectList.Add(new SelectListItem { Text = text, Value = value });
                }
            }
            return selectList;
        }

        // Get Data  
        public static List<VesselActivityViewModel_Schedule> GetVesselActivityControl(string pUserType, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string pActivityType = Convert.ToString(pUserType) == "VESSEL" ? "CMMT" : "CMCS";
                var query = from v in context.MT_VESSEL_ACTIVITY
                            join vc in context.MT_VESSEL_ACTIVITY_CONTROL on v.MVA_ROW_ID equals vc.MAC_FK_VESSEL_ACTIVITY
                            where
                            vc.MAC_SYSTEM.ToUpper() == "CPAI"
                            && vc.MAC_TYPE.ToUpper() == pActivityType
                            && vc.MAC_STATUS.ToUpper() == status
                            select new VesselActivityViewModel_Schedule
                            {
                                act_RowID = v.MVA_ROW_ID,
                                act_Code = v.MVA_CODE,
                                act_Key = v.MVA_KEY,
                                act_Name = v.MVA_STANDARD_PHRASE_EN,
                                act_System = vc.MAC_SYSTEM,
                                act_Type = vc.MAC_TYPE,
                                act_Color = vc.MAC_COLOR_CODE,
                                act_RefStatus = v.MVA_REF,
                                act_Status = vc.MAC_STATUS
                                
                            };
                return query.ToList();
            }
        }

        public static string GetSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VESSEL_ACTIVITY_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MAC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MAC_SYSTEM }
                                    select new { System = d.MAC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetSystemForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VESSEL_ACTIVITY_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MAC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MAC_SYSTEM }
                                    select new { System = d.MAC_SYSTEM.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetTypeJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VESSEL_ACTIVITY_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MAC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MAC_TYPE }
                                    select new { Type = d.MAC_TYPE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Type));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetTypeForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VESSEL_ACTIVITY_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MAC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MAC_TYPE }
                                    select new { Type = d.MAC_TYPE.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.Type))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.Type, Text = item.Type });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetVACodeJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VESSEL_ACTIVITY
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MVA_CODE }
                                    select new { Value = d.MVA_CODE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetVAKeyJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_VESSEL_ACTIVITY
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MVA_KEY }
                                    select new { Value = d.MVA_KEY.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> GetVesselActivityList(string pUserType)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            List<VesselActivityViewModel_Schedule> data = GetVesselActivityControl(pUserType, ACTIVE).OrderBy(x => x.act_Name).OrderBy(x => x.act_Code).ToList();
            for (int i = 0; i < data.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = data[i].act_Name, Value = data[i].act_RowID });
            }
            return selectList;
        }
    }
}