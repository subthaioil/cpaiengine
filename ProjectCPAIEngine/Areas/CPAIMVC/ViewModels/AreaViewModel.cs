﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class AreaViewModel
    {
        public AreaViewModel_Search area_Search { get; set; }
        public AreaViewModel_Detail area_Detail { get; set; }

        public List<SelectListItem> ddl_areaStatus { get; set; }
        public List<SelectListItem> ddl_Unit { get; set; }
        public List<SelectListItem> ddl_FreeUnit { get; set; }
    }

    public class AreaViewModel_Search
    {
        public string sAreaCode { get; set; }
        public string sAreaName { get; set; }
        public string sAreaStatus { get; set; }

        public List<AreaViewModel_SeachData> sSearchData { get; set; }
    }


    public class AreaViewModel_SeachData
    {
        public string dAreaCode { get; set; }
        public string dAreaName { get; set; }
        public string dAreaStatus { get; set; }
    }

    public class AreaViewModel_Detail
    {
        public string AreaCode { get; set; }
        public string Order { get; set; }
        public string AreaName { get; set; }
        public string AreaStatus { get; set; }
        public bool BoolStatus { get; set; }
        public List<AreaViewModel_Unit> Unit { get; set; }
    }


    public class AreaViewModel_Unit
    {
        public string Code { get; set; }
        public string Order { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public bool BoolStatus { get; set; }
        public string CreateType { get; set; }
        public string UserGroup { get; set; }
    }
}