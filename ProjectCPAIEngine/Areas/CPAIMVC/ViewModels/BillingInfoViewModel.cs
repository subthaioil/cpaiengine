﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class BillingInfoViewModel
    {
        public string BillingDate { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedDate { get; set; }
        public string Company { get; set; }
    }
}