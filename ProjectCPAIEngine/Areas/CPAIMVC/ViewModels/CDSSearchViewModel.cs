﻿using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CDSSearchViewModel
    {
        public List<CDSEncrypt> CDSTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> userList { get; set; }

        public List<SelectListItem> feedstock { get; set; }
        public List<SelectListItem> product { get; set; }
        public List<SelectListItem> supplier { get; set; }
        public List<SelectListItem> vessels { get; set; }

        public string search_User { get; set; }
        public string search_Product { get; set; }
        public string search_Supplier { get; set; }
        public string search_Stock { get; set; }
        public string search_DatePurchase { get; set; }
        public string vessel { get; set; }

    }

    public class CDSViewModel
    {
    }
}