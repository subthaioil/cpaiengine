﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CamTemplateViewModel
    {
        public CamTemplateViewModel_Search CamTemplate_Search { get; set; }
        public CamTemplateViewModel_Detail CamTemplate_Detail { get; set; }

        public List<SelectListItem> ddl_data_field { get; set; }
        public List<SelectListItem> ddl_crude { get; set; }
        public List<SelectListItem> ddl_country { get; set; }
        //public DDL_country ddl_country { get; set; }
        public List<SelectListItem> ddl_Assay { get; set; }

        
    }

    public class DDL_country
    {
        public List<SelectListItem> country_by_crude { get; set; }
    }

    public class CamTemplateViewModel_Search
    {
        public string sCamCode { get; set; }
        public string sVersion { get; set; }

        public List<CamTemplateViewModel_SearchData> sSearchData { get; set; }
    }


    public class CamTemplateViewModel_SearchData
    {
        public string dCamCode { get; set; }
        public string dVersion { get; set; }
        public DateTime dUpdatedDate { get; set; }
        public string dUpdatedBy { get; set; }
        public string dReason { get; set; }
    }

    public class CamTemplateViewModel_Detail
    {
        public string CamCode { get; set; }
        public string Version { get; set; }
        public CamTemplateViewModel_JSON JSON { get; set; }
        public string Reason { get; set; }
    }

    public class CamTemplateViewModel_JSON
    {
        public string spiral_header_column { get; set; }
        public string spiral_data_column { get; set; }
        public string compared_spiral_data_column { get; set; }
        public string test_method_column { get; set; }
        public string source_column { get; set; }
        public string constraint_column { get; set; }
        public string remarks_column { get; set; }
        public string component_header_column { get; set; }
        public string g_font_color { get; set; }
        public string g_font_size { get; set; }
        public string g_number_format { get; set; }
        public string g_border_style { get; set; }
        public string g_freeze_row { get; set; }
        public string g_freeze_col { get; set; }

        public string note_explan_row { get; set; }
        public string note_explan_column { get; set; }
        public string explan_from { get; set; }
        public string explan_to { get; set; }
        public string compare_clude { get; set; }

        //public string border_top_style { get; set; }
        //public string border_top_color { get; set; }
        //public string border_bottom_style { get; set; }
        //public string border_bottom_color { get; set; }
        //public string border_left_style { get; set; }
        //public string border_left_color { get; set; }
        //public string border_right_style { get; set; }
        //public string border_right_color { get; set; }
        public List<CamTemplateViewModel_ColumnWidth> ColumnWidth { get; set; }
        public List<CamTemplateViewModel_Config> Config { get; set; }
        public List<CamTemplateViewModel_Ref> Reference { get; set; }
        public List<CamTemplateViewModel_Fix> Fix { get; set; }
        public List<CamTemplateViewModel_Spiral> Spiral { get; set; }
        public List<CamTemplateViewModel_Data> Data { get; set; }
        public CamTemplateViewModel_Spec Ranking { get; set; }
        public CamTemplateViewModel_Compared_Spec Compared_Ranking { get; set; }
    }

    public class CamTemplateViewModel_ColumnWidth {

        public string column { get; set; }
        public string width { get; set; }
    }

    public class CamTemplateViewModel_Config
    {
        public string order { get; set; }
        public string row_from { get; set; }
        public string row_to { get; set; }
        public string col_from { get; set; }
        public string col_to { get; set; }
        public string hidden { get; set; }
        public string merge { get; set; }
        public string font_bold { get; set; }
        public string font_color { get; set; }
        public string font_size { get; set; }
        public string number_format { get; set; }
        public string indent { get; set; }
        public string center { get; set; }
        public string freeze_row { get; set; }
        public string freeze_col { get; set; }
        public string border_style { get; set; }
        public string border_color { get; set; }
        public string bg_color { get; set; }
        public string alignment { get; set; }
        public string font_italic { get; set; }
        public string font_underline { get; set; }
        public string font_textwrap { get; set; }
        public string font_horizontal_alignment { get; set; }
        public string font_vertical_alignment { get; set; }
        public string border_top_style { get; set; }
        public string border_top_color { get; set; }
        public string border_bottom_style { get; set; }
        public string border_bottom_color { get; set; }
        public string border_left_style { get; set; }
        public string border_left_color { get; set; }
        public string border_right_style { get; set; }
        public string border_right_color { get; set; }

    }

    public class CamTemplateViewModel_Ref
    {
        public string order { get; set; }
        public string crude { get; set; }
        public string country { get; set; }
        public string AssayRef{ get; set; }
        public string col { get; set; }
        public string header { get; set; }
    }

    public class CamTemplateViewModel_Fix
    {
        public string row_from { get; set; }
        public string row_to { get; set; }
        public string col_from { get; set; }
        public string col_to { get; set; }
        public string value { get; set; }
    }

    public class CamTemplateViewModel_Spiral
    {
        public string Component { get; set; }
        public string Row_from { get; set; }
        public string Row_to { get; set; }
        public objProperty ObjProperty { get; set; }
    }

    public class CamTemplateViewModel_Data
    {
        public string row { get; set; }
        public string field { get; set; }
        public string valueFromExcel { get; set; }
    }

    public class CamTemplateViewModel_Spec
    {
        public string min_col { get; set; }
        public string min_range { get; set; }
        public string max_col { get; set; }
        public string max_range { get; set; }
    }

    public class CamTemplateViewModel_Compared_Spec
    {
        public string min_col { get; set; }
        public string min_range { get; set; }
        public string max_col { get; set; }
        public string max_range { get; set; }
    }

    public class objProperty
    {
        public List<Property> Property { get; set; }
    }

    public class Property
    {
        public string txtProperty { get; set; }
        public string txtUnit { get; set; }
        public string txtFrom { get; set; }
        public string txtTo { get; set; }
        public string txtValueExcel { get; set; }
    }

  
}