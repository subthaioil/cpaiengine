﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CharterInViewModel
    {
        public ChiEvaluating chi_evaluating { get; set; }
        public ChiCargoDetail chi_cargo_detail { get; set; }

        public ChiCargoDetail chi_cargo_detail_TripNo { get; set; }

        public ChiCharteringMethod chi_chartering_method { get; set; }
        public ChiNegotiationSummary chi_negotiation_summary { get; set; }
        public ChiProposedForApprove chi_proposed_for_approve { get; set; }
        public string chi_reason { get; set; }
        public string chi_note { get; set; }
        public List<ApproveItem> approve_items { get; set; }
        public string explanationAttach { get; set; }
        public string explanationAttachTrader { get; set; }

        public ChitEvaluating chit_evaluating { get; set; }
        public ChitCharteringMethod chit_chartering_method { get; set; }
        public ChitNegotiationSummary chit_negotiation_summary { get; set; }
        public ChitProposedForApprove chit_proposed_for_approve { get; set; }
        public string chit_reason { get; set; }
        public string chit_note { get; set; }

        public List<SelectListItem> vessel { get; set; }
        public List<SelectListItem> broker { get; set; }
        public List<SelectListItem> pricesTerm { get; set; }
        public List<SelectListItem> vehicle { get; set; }
        public List<SelectListItem> vendor { get; set; }
        public List<SelectListItem> materials { get; set; }
        public List<SelectListItem> portName { get; set; }
        public List<SelectListItem> freight { get; set; }
        public List<SelectListItem> vesseltype { get; set; }
        public string ev_tempLaycan { get; set; }
        public string pa_tempLaycan { get; set; }
        public List<VesselSize> vesselSize { get; set; }

        public string laycan_full { get; set; }
        public string loading_month_full { get; set; }
        public List<SelectListItem> freight_type_list { get; set; }
        public List<SelectListItem> product_list { get; set; }
        public List<SelectListItem> broker_list { get; set; }
        public List<SelectListItem> vessel_list { get; set; }
        public List<SelectListItem> pricesTerm_list { get; set; }
        public List<SelectListItem> quantity_list { get; set; }
        public List<SelectListItem> type_list { get; set; }

        public List<SelectListItem> market_ref { get; set; }

        public List<SelectListItem> cargo_Unit { get; set; }
        public List<SelectListItem> cargo_list { get; set; }
        public List<SelectListItem> charterer_list { get; set; }
        public List<SelectListItem> bss_list { get; set; }
        public List<SelectListItem> NegoUnit { get; set; }
        public List<SelectListItem> cust { get; set; }

        public List<SelectListItem> ddlCharter_For { get; set; }

        public ChitDetail chit_detail { get; set; }

        public List<ChitListDropDownKey> portTypeLoad { get; set; }
        public List<ChitListDropDownKey> portTypeDisCharge { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string pageStatus { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public string BSSDataTmp { get; set; } 

        public string checkSelectCargo { get; set; }
        public string tripNumber { get; set; }
        public List<SelectListItem> tripNumberList { get; set; }
        public List<LoadingMonth> LoadingMonthList { get; set; }

        public List<string> groupNameList { get; set; }

        public List<SelectListItem> by_offer { get; set; }
    }

    public class LoadingMonth
    {
        public string tripNumber { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }
}