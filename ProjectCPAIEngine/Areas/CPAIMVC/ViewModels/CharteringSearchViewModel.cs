﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CharteringSearchViewModel
    {
        public List<CharteringEncrypt> CharteringTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> userList { get; set; }


        public List<SelectListItem> vessel { get; set; }
        public List<SelectListItem> broker { get; set; }
        public List<SelectListItem> charterer { get; set; }
        public List<SelectListItem> created_by { get; set; }

        public string sDate { get; set; }
        public string sVessel { get; set; }
        public string sLaycan { get; set; }
        public string sBroker { get; set; }
        public string sCharterer { get; set; }
        public string sOwner { get; set; }
        public string sCreateBy { get; set; }
        public string sCreateBySelected { get; set; }
    }
}