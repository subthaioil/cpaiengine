﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class ClearLineViewModel
    {
        public SOClearLineViewModel_Search SOClearLine_Search { get; set; }
        public ClearLineViewModel_Search ClearLine_Search { get; set; }
        public ClearLineDetailViewModel ClearLineDetail { get; set; }
        public List<SelectListItem> ddl_Vessel { get; set; }
        public string json_Vessel { get; set; }
        public List<SelectListItem> ddl_Crude { get; set; }
        public string json_Crude { get; set; }
        public List<SelectListItem> ddl_Customer { get; set; }
        public string json_Customer { get; set; }
        public List<SelectListItem> ddl_Plant { get; set; }
        public string json_Plant { get; set; }
        public List<SelectListItem> ddl_Unit { get; set; }
        public string json_Unit { get; set; }
        public List<SelectListItem> ddl_UnitPrice { get; set; }
        public string json_UnitPrice { get; set; }
        public List<SelectListItem> ddl_UnitINV { get; set; }
        public string json_UnitINV { get; set; }
        public List<SelectListItem> ddl_UnitTotal { get; set; }
        public string json_UnitTotal{ get; set; }
        public List<SelectListItem> ddl_PriceStatus { get; set; }
        public List<SelectListItem> ddl_SaleUnitType { get; set; }
        public List<SelectListItem> ddl_InvoiceFigureType { get; set; }
        public List<SelectListItem> ddl_Company { get; set; }
        public string Mode { get; set; }
    }

    public class ClearLineViewModel_Search
    {
        public string sTripNo { get; set; }
        public string sVesselName { get; set; }
        public string sDeliveryDate { get; set; }
        public string sCrude { get; set; }
        public string sCustomer { get; set; }                               
        public List<ClearLineViewModel_SearchData> SearchData { get; set; }
        
    }

    public class ClearLineViewModel_SearchData
    {
        public string dTripNo { get; set; }
        public string dCustomer { get; set; }
        public string dCustomerCode { get; set; }
        public string dDeliveryDate { get; set; }
        public string dCrudeType { get; set; }
        public string dVessel { get; set; }         
        public string dPrice { get; set; }        
        public string dPONo { get; set; }
        public string dFIDoc { get; set; }

        public string dVolumeBBL { get; set; }
        public string dVolumeMT { get; set; }
        public string dVolumeLitres { get; set; }
        public string dOuternBBL { get; set; }
        public string dOuternMT { get; set; }
        public string dOuternLitres { get; set; }

        //SO Create
        public string dPlant { get; set; }
        public string dProduct { get; set; }
        public string dProductCode { get; set; }
        public string dAmount { get; set; }
        public string dUS4 { get; set; }
        public string dExchangeRate { get; set; }
        public string dSaleOrder { get; set; }
        public string dMEMONo { get; set; }
        public string dDO { get; set; }
        public string dSaleUnitType { get; set; }
        public string dInvoiceFigureType { get; set; }
        public string dCompany { get; set; }

        
    }

    public class ClearLineDetailViewModel
    {
        public string sTripNo { get; set; }
        public string sCompany { get; set; }
        public string sVesselName { get; set; }
        public string sCustomer { get; set; }
        public string sDeliveryDate { get; set; }
        public string sCrude { get; set; }
        public string sPlant { get; set; }
        public string sDueDate { get; set; }
        public string sSaleUnitType { get; set; }
        public string sVolumeBBL { get; set; }
        public string sVolumeLitres { get; set; }
        public string sVolumeMT { get; set; }
        public string sOutturnBBL { get; set; }
        public string sInvoiceFigureType { get; set; }
        public string sOutturnLitres { get; set; }
        public string sOutturnMT { get; set; }
        public string sPONo { get; set; }
        public string sExchangeRate { get; set; }
        public string sTotalAmount { get; set; }
        public string sTotalUnit { get; set; }
        public string sRemark { get; set; }
        public string sPricePer { get; set; }
        public string sPrice { get; set; }
        public string sUnitID { get; set; }
        public string sType { get; set; } 
        public string sStatus { get; set; }
        public string sRelease { get; set; }
        public string sTank { get; set; }
        public string sSaleOrder { get; set; }
        public string sPriceRelease { get; set; } 
        public string sDistriChann { get; set; }
        public string sTableName { get; set; }
        public string sMemo { get; set; }
        public string sDateSAP { get; set; }
        public string sFIDoc { get; set; }
        public string sDONo { get; set; }
        public string sStatusToSAP { get; set; }
        public string sFIDocReverse { get; set; }
        public string sIncoTermType { get; set; }
        public string sTemp { get; set; }
        public string sDensity { get; set; }
        public string sStorageLocation { get; set; }
        public string sUpdateDO { get; set; }
        public string sUpdateGI { get; set; }
        public List<PriceViewModel> PriceList { get; set; }
        public string sCreateDate { get; set; }
        public string sCreateBy { get; set; }
        public string sLastModifyDate { get; set; }
        public string sLastModifyBy { get; set; }
        public string sCreateTime { get; set; }
        public string sLastModifyTime { get; set; }
        public string sPriceStatus { get; set; }
    }

    public class PriceViewModel
    {
        public string sPriceType { get; set; }  // P=Provisional , F=Final , A=Actual
        public string sDueDate { get; set; }
        public string sPrice { get; set; }
        public string sPriceUnit { get; set; }
        public string sInvoice { get; set; }
        public string sInvUnit { get; set; } 
        public string sTripNo { get; set;}
        public string sNum { get; set; }
        public string sRelease { get; set; }
        public string sMemo { get; set; }
    }

    public class GlobalConfigCL
    {
        public List<UnitCal> PCF_UNIT_CAL { get; set; }

        public string DISTRIBUTION_CHANEL { get; set; }
        public string SHIPPING_POINT { get; set; }
        public string DIVISION { get; set; }
        public string DATE_TYPE { get; set; }
        public string ITM_NUMBER { get; set; }
    }

    public class UnitCal
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

}