﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CoolCAMComparisonViewModel
    {
        public List<CoolCAMComparisonEncrypt> CoolCAMComparisonTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> areaList { get; set; }
        public List<string> unitList { get; set; }
        public List<SelectListItem> country { get; set; }
        public List<SelectListItem> crudeCategories { get; set; }
        public List<SelectListItem> crudeKerogen { get; set; }
        public List<SelectListItem> crudeCharacteristic { get; set; }
        public List<SelectListItem> crudeMaturity { get; set; }
        public List<SelectListItem> area { get; set; }
        public List<SelectListItem> unit { get; set; }

        public string sPurchaseNo { get; set; }
        public string sAssayRef { get; set; }
        public string sCountry { get; set; }
        public string sCrudeName { get; set; }
        public string sCrudeCategories { get; set; }
        public string sCrudeKerogen { get; set; }
        public string sCrudeCharacteristic { get; set; }
        public string sCrudeMaturity { get; set; }
        public string sArea { get; set; }
        public string sUnit { get; set; }
        

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }


        public List<Crude> crude { get; set; }
        public string rowData { get; set; }
        public string fileName { get; set; }
        public string fileUrl { get; set; }
        public string hide_row_start { get; set; }
        public string hide_row_end { get; set; }
        public string hide_column_start { get; set; }
        public string hide_column_end { get; set; }
        public string footer_row_start { get; set; }
        public string footer_row_end { get; set; }
        public string footer_column_start { get; set; }
        public string footer_column_end { get; set; }
        public string crude_column { get; set; }
        public string score_row { get; set; }

    }
    public class Crude
    {
        public string crude_name { get; set; }
        public string crude_assay_ref_no { get; set; }
        public string crude_country { get; set; }

    }
}