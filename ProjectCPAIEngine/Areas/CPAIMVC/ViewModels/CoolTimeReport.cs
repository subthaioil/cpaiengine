﻿using System.Collections.Generic;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CoolTimeReport
    {
        public string period_month_from { get; set; }
        public string period_month_to { get; set; }
        public string period_year_from { get; set; }
        public string period_year_to { get; set; }
        public string crude_name { get; set; }
        public string file_name { get; set; }
        public string file_path { get; set; }

        public CoolTimeReportData coolTimeReportData { get; set; }
    }

    public class CoolTimeReportData
    {
        public string timeReportTitle { get; set; }
        public double timeA_sumAvg { get; set; }
        public double timeB_sumAvg { get; set; }
        public double timeC_sumAvg { get; set; }
        public double timeD_sumAvg { get; set; }
        public double timeTotal_sumAvg { get; set; }
        public List<NumberOfCrude> numberOfCrude { get; set; }
        public List<WorkingTime> workingTime { get; set; }
        public int columnCount { get; set; }
        public List<ExpertWorkingTime> expertWorkingTime { get; set; }
    }

    public class NumberOfCrude
    {
        public string month { get; set; }
        public string numberOfCam { get; set; }
        public double avgWorkingTime { get; set; }
    }

    public class WorkingTime
    {
        public string month { get; set; }
        public double timeA_avg { get; set; }
        public double timeB_avg { get; set; }
        public double timeC_avg { get; set; }
        public double timeD_avg { get; set; }
        public double timeTotal_avg { get; set; }
        public List<WorkTimeDetail> workTimeDetail { get; set; }
    }

    public class WorkTimeDetail
    {
        public string crudeName { get; set; }
        public double timeA { get; set; }
        public double timeB { get; set; }
        public double timeC { get; set; }
        public double timeD { get; set; }
        public double timeTotal { get; set; }
    }

    public class ExpertWorkingTime
    {
        public string month { get; set; }
        public string crudeKey { get; set; }
        public string crudeName { get; set; }
        public List<ExpertWorkingTimeDetail> expertWorkTimeDetail { get; set; }
    }

    public class ExpertWorkingTimeDetail
    {
        public string crudeKey { get; set; }
        public string crudeName { get; set; }
        public string columnName { get; set; }
        public double timeValue { get; set; }
        public double timeValueSH { get; set; }
    }
}