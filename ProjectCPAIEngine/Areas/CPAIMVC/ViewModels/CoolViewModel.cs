﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CoolViewModel
    {
        public static string FORMAT_DATE = "dd-MMM-yyyy";
        public static string FORMAT_DATETIME = "dd-MMM-yyyy HH:mm";
        public CooData data { get; set; }
        public CooExperts experts { get; set; }
        public string support_doc_file { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        public string comment_draft_cam { get; set; }
        public string comment_final_cam { get; set; }
        public string note { get; set; }

        //Expert comment
        public CoexAreas areas { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public string approvalDay { get; set; }
        public string assay_file_path { get; set; }
        public List<SelectListItem> ddlPriority { get; set; }
        public List<SelectListItem> ddlAssayFrom { get; set; }
        public List<SelectListItem> ddlCrudeCategories { get; set; }
        public List<SelectListItem> ddlCrudeKerogen { get; set; }
        public List<SelectListItem> ddlCrudeCharacteristic { get; set; }
        public List<SelectListItem> ddlCrudeMaturity { get; set; }
        public List<SelectListItem> ddlCrudeReference { get; set; }
        public List<SelectListItem> ddlFileSupportingDocumentType { get; set; }

        public List<UnitScore> unit_score { get; set; }
    }

    public class UnitScore
    {
        public string unit_key { get; set; }
        public List<string> score { get; set; }
    }

    public class CoolSearchViewModel
    {
        public List<CoolGroupEncrypt> CoolGroupTransaction { get; set; }
        public List<CoolEncrypt> CoolTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> userList { get; set; }
        public List<string> crudeList { get; set; }
        public List<string> countryList { get; set; }

        public List<SelectListItem> country { get; set; }
        public List<SelectListItem> crude { get; set; }
        public List<SelectListItem> crudeCategories { get; set; }
        public List<SelectListItem> crudeKerogen { get; set; }
        public List<SelectListItem> crudeCharacteristic { get; set; }
        public List<SelectListItem> crudeMaturity { get; set; } 
        public List<SelectListItem> camStatus { get; set; }
        public List<SelectListItem> orderBy { get; set; }

        public string sAssayRef { get; set; }
        public string sAssayDate { get; set; }
        public string sCountry { get; set; }
        public string sCrudeName { get; set; }
        public string sCrudeCategories { get; set; }
        public string sCrudeKerogen { get; set; }
        public string sCrudeCharacteristic { get; set; }
        public string sCrudeMaturity { get; set; }
        public string sCAMStatus { get; set; }
        public string sOrderBy { get; set; }


        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
    }
}