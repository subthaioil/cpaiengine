﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CreditViewModel
    {
        public CreditViewModel_Search creditViewModel_Search { get; set; }
        public CreditViewModel_Detail creditViewModel_Detail { get; set; }

        public class CreditViewModel_Search
        {
            public List<CreditViewModel_SearchData> sSearchData { get; set; }
        }

        public class CreditViewModel_SearchData
        {
            public string dOrderID { get; set; }
            public string dUnderlying { get; set; }
            public string dCropPlan { get; set; }
            public string dProduction { get; set; }
            public string dUnitPrice { get; set; }
            public string dUnitVolume { get; set; }
            public string dActiveDate { get; set; }
        }

        public class CreditViewModel_Detail
        {
            public string hmcc_name { get; set; }
            public string hmcc_value { get; set; }
            public string hmcc_desc { get; set; }
        }
    }
}