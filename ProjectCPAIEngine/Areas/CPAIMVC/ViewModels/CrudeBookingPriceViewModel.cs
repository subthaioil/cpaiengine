﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CrudeBookingPriceViewModel
    {
        public CustomerViewModel_Seach cust_Search { get; set; }
        public CustomerViewModel_Detail cust_Detail { get; set; }

        public List<SelectListItem> ddl_Country { get; set; }
        public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_Nation { get; set; }
        public List<SelectListItem> ddl_Broker { get; set; }

        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_Type { get; set; }
        public List<SelectListItem> ddl_Color { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }

        public string cust_SystemJSON { get; set; }
        public string cust_TypeJSON { get; set; }
    }

    public class CrudeBookingPriceViewModel_Seach
    {
        public string sCustCode { get; set; }
        public string sCustName { get; set; }
        public string sCompany { get; set; }
        public string sNation { get; set; }
        public string sCountry { get; set; }
        public string sCreateType { get; set; }
        
        public string sSystem { get; set; }
        public string sType { get; set; }
        public string sStatus { get; set; }

        public List<CrudeBookingPriceViewModel_SeachData> sSearchData { get; set; }
    }

    public class CrudeBookingPriceViewModel_SeachData
    {
        public string dCustCtrlID { get; set; } // Not Display
        public string dCustCode { get; set; }
        public string dCustName { get; set; }
        public string dCompany { get; set; }
        public string dCompanyName { get; set; }
        public string dNation { get; set; }
        public string dCountry { get; set; }
        public string dCreateType { get; set; }

        public string dSystem { get; set; }
        public string dType { get; set; }
        public string dStatus { get; set; }
    }
    
}