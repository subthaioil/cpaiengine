﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CrudeImportPlanViewModel
    {
        public CrudeImportPlanViewModel_Seach crude_Search { get; set; }
        public CrudeImportPlanViewModel_Detail crude_Detail { get; set; }


        public CrudeApproveFormViewModel_Seach crude_approve_Search { get; set; }
        public CrudeApproveFormViewModel_Detail crude_approve_detail { get; set; }

        public FreightViewModel freightAddData { get; set; }
        public SurveyorViewModel surveyorAddData { get; set; }
        public OtherViewModel otherAddData { get; set; }



        public List<SelectListItem> ddl_CrudeName { get; set; }
        public string json_Crude { get; set; }
        public string json_CrudeAuto { get; set; }

        public List<SelectListItem> ddl_Supplier { get; set; }
        public string json_Supplier { get; set; }
        public string json_SupplierAuto { get; set; }

        public string json_VendorCrude { get; set; }
        public string json_VendorFreight { get; set; }
        public string json_VendorSurveyor { get; set; }
        public string json_VendorOther { get; set; }
        public string json_VendorBroker { get; set; }

        public List<SelectListItem> ddl_Incoterm { get; set; }
        public string json_Incoterm { get; set; }

        public List<SelectListItem> ddl_Vehicle { get; set; }
        public string json_Vehicle { get; set; }
        public string json_VehicleAuto { get; set; }

        public List<SelectListItem> ddl_LoadingPort { get; set; }
        public string json_LoadingPort { get; set; }
        public string json_LoadingPortAuto { get; set; }

        public string json_approveSearch { get; set; }
        public string json_month { get; set; }
        public string json_year { get; set; }
        public string json_PriceStatus { get; set; }
        public string json_BLStatus { get; set; }

        public List<SelectListItem> ddl_BLStatus { get; set; }

        public List<SelectListItem> ddl_Year { get; set; }
        public List<SelectListItem> ddl_Month { get; set; }
        public List<SelectListItem> ddl_PriceStatus { get; set; }

        public List<SelectListItem> ddl_PurchaseType { get; set; }
        public string json_PurchaseType { get; set; }
        public List<SelectListItem> ddl_StorageLocation { get; set; }
        public string json_StorageLocation { get; set; }
        public List<SelectListItem> ddl_Currency { get; set; }
        public string json_Currency { get; set; }

        public List<SelectListItem> ddl_SurveyorType { get; set; }
        public string json_SurveyorType { get; set; }
        public string json_SurveyorConfig { get; set; }

        public List<SelectListItem> ddl_OtherCostType { get; set; }
        public string json_OtherCostType { get; set; }
        public string json_OtherCostTypeConfig { get; set; }

        public List<SelectListItem> ddl_FreightType { get; set; }
        public string json_FreightType { get; set; }
        public string json_FreightTypeConfig { get; set; }

        public string json_CharteringType { get; set; }

        public string json_AuthorizePage { get; set; }
        public string json_Channel { get; set; }
        public List<SelectListItem> ddl_Channel { get; set; }
        public List<SelectListItem> ddl_UnitCal { get; set; }
        public string json_InsuranceRate { get; set; }
        public string json_UnitCal { get; set; }
        public string json_Result { get; set; }
        public string TripNoSelect { get; set; }
        public string FormLoadSTS { get; set; }
        public string PosScrollTop { get; set; }
        public string Popup_TripNoSelect { get; set; }
        public string Popup_ItemNoSelect { get; set; }
        public string Popup_RowIndexSelect { get; set; }
        public string json_purgroup { get; set; }
        public string json_SearchType { get; set; }
        public List<PCF_PO_SURVEYOR_PUR_GROUP> PurGroup { get; set; }
        public List<SelectListItem> ddl_HolidayType { get; set; }
        public string json_HolidayType { get; set; }
        public string MsgAlert { get; set; }
        public string userRole { get; set; }
        public string AvalivalYearMonth { get; set; }
        public string AvalivalYear { get; set; }
        //public List<SelectListItem> ddlCountry { get; set; }
        public string json_Country { get; set; }
        public string json_freight_prod { get; set; }

        public string auto_showPopup { get; set; }

        public PriceDataDetailViewModel PriceDetail { get; set; }


    }

    public class PCFMobDetail
    {
        public DateTime date { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal ProductValue { get; set; }
    }

    public class PriceDataDetailViewModel
    {
        public string PCFLastUpdate { get; set; }
        public string PriceLastUpdate { get; set; }
        public string BKKBankHoliday { get; set; }
        public string NKBankHoliday { get; set; }
        public string Monthly1 { get; set; }
        public string Monthly2 { get; set; }
        public string Daily1 { get; set; }
        public string Daily1_Hidden { get; set; }
        public string Daily2 { get; set; }
        public List<PriceDailyDetail> PriceDaily { get; set; }
        public List<PriceMonthlyDetail> PriceMonthly { get; set; }
    }

    public class PriceDailyDetail
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Price1 { get; set; }
        public string Price2 { get; set; }
    }

    public class PriceMonthlyDetail
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Price1 { get; set; }
        public string Price2 { get; set; }
    }

    public class CrudeImportPlanViewModel_Seach
    {
        //public string sArraivalDate { get; set; }
        //public string sLoadingDate { get; set; }
        public string sCrudeName { get; set; }
        public string sSupplier { get; set; }
        public string sIncoterm { get; set; }
        public string sExportSTS { get; set; }

        public List<CrudeImportPlanViewModel_Detail> sSearchData { get; set; }
    }

    public class CrudeApproveFormViewModel_Seach
    {
        public string sArraivalDate { get; set; }
        public string sLoadingDate { get; set; }
        public string sCrudeName { get; set; }
        public string sSupplier { get; set; }
        public string sIncoterm { get; set; }
        public string sType { get; set; }

        public List<CrudeApproveFormViewModel_SearchData> sSearchData { get; set; }
    }

    public class CrudeApproveFormViewModel_SearchData
    {
        public string refID { get; set; }
        public string refTripNo { get; set; }
        public string ArraivalDate { get; set; }
        public string ArraivalMonth { get; set; }
        public string ArraivalYear { get; set; }
        public string LoadingDate { get; set; }
        public string CrudeName { get; set; }
        public string SupplierName { get; set; }
        public string Price { get; set; }
        public string Incoterm { get; set; }
        public string QTYKbbl { get; set; }
        public string QTYKKT { get; set; }
        public string QTYUnit { get; set; }
        public string DischargeLayCan { get; set; }
        public string PurchaseNo { get; set; }
        public string LoadingDateFromTo { get; set; }
        public string Formula { get; set; }
        public string Tolerance { get; set; }
        public string GT_C { get; set; }
        public string ContractType { get; set; }
        public string Origin { get; set; }
        public string com_code { get; set; }
        public string CrudeCode { get; set; }
        public string SupplierCode { get; set; }
        public string paymentTerm { get; set; }
        public string bechmark { get; set; }
        public string PricingPeriod { get; set; }
        public string Offer { get; set; }
        public string Density { get; set; }
        public string ArraivalDateNum { get; set; }
        public string LoadingDateNum { get; set; }
        public string PerformanceBond { get; set; }
    }

    public class CrudeApproveFormViewModel_Detail
    {
        public List<HeaderViewModel> dDetail_Header { get; set; }
        public List<MaterialViewModel> dDetail_Material { get; set; }
        public List<CrudeImportPlanViewModel_Detail> dDetail { get; set; }
        public List<FreightViewModel> dDetail_Freight { get; set; }

        public List<SurveyorViewModel> dDetail_Surveyor { get; set; }
        public List<SurveyorMatViewModel> dDetail_SurveyorMat { get; set; }

        public List<OtherViewModel> dDetail_Other { get; set; }

        public List<CustomsViewModel> dDetail_Custom { get; set; }
        public List<PriceToSapViewModel> dDetail_PriceToSap { get; set; }

        public List<FileViewModel> dDetail_MaterialFile { get; set; }
        public List<FileViewModel> dDetail_OtherFile { get; set; }
        public List<FileViewModel> dDetail_SurveyorFile { get; set; }
        public List<FileViewModel> dDetail_FreightFile { get; set; }
        public List<FileViewModel> dDetail_CustomsFile { get; set; }

        public List<DailyPriceViewModel> dDetail_DailyPrice { get; set; }
    }

    public class PriceToSapViewModel
    {
        public string TRANS_TYPE { get; set; }
        public string COM_CODE { get; set; }
        public string TRIP_NO { get; set; }
        public string CREATE_DATE { get; set; }
        public string CMCS_ETA { get; set; }
        public string MAT_NUM { get; set; }
        public string PARTLY_ORDER { get; set; }
        public string VENDOR_NO { get; set; }
        public string PLANT { get; set; }
        public string GROUP_LOCATION { get; set; }
        public string EXCHANGE_RATE { get; set; }
        public string BASE_PRICE { get; set; }
        public string PREMIUM_OSP { get; set; }
        public string PREMIUM_MARKET { get; set; }
        public string OTHER_COST { get; set; }
        public string FREIGHT_PRICE { get; set; }
        public string INSURANCE_PRICE { get; set; }
        public string TOTAL_AMOUNT_THB { get; set; }
        public string TOTAL_AMOUNT_USD { get; set; }
        public string QTY { get; set; }
        public string QTY_UNIT { get; set; }
        public string CURRENCY { get; set; }
        public string SAP_RECEIVED_DATE { get; set; }
        public string STATUS { get; set; }
        public string GROUP_DATA { get; set; }
        public string FLAG_PLANNING { get; set; }
        public string DUE_DATE { get; set; }
        public string CODE { get; set; }
        public string REMARK { get; set; }
        public string DATA_FROM { get; set; }


    }

    public class HeaderViewModel
    {
        public string PHE_TRIP_NO { get; set; }
        public string PHE_MAX_ITEMNO { get; set; }
        public string PHE_COMPANY_CODE { get; set; }
        public string PHE_ARRIVAL_YEAR { get; set; }
        public string PHE_ARRIVAL_MONTH { get; set; }
        public string PHE_ACTUAL_ETA_FROM { get; set; }
        public string PHE_ACTUAL_ETA_TO { get; set; }
        public string PHE_VESSEL { get; set; }
        public string PHE_VESSEL_TEXT { get; set; }
        public string PHE_CREATED_DATE { get; set; }
        public string PHE_CREATED_BY { get; set; }
        public string PHE_UPDATED_DATE { get; set; }
        public string PHE_UPDATED_BY { get; set; }
        public string PHE_CDA_ROW_ID { get; set; }
        public string PHE_PLANNING_RELEASE { get; set; }
        public string PHE_BOOKING_RELEASE { get; set; }
        public string PHE_STATUS { get; set; }
        public string PHE_CHANNEL { get; set; }
        public string PHE_SPOT_NAME { get; set; }
        public string PHE_INDB_STATUS { get; set; }
        public string PHE_ACTION { get; set; }
        public string PHE_SAVE_STS { get; set; }
        public string PHE_SAVE_MSG { get; set; }
        public Boolean PHE_IS_CHECK { get; set; }
        public string PHE_MAT_TYPE { get; set; }
        public string PHE_EXPORT_STS { get; set; }
        public string PHE_LOADING_MONTH { get; set; }
        public string PHE_CHARTERING_NO { get; set; }
    }

    public class MaterialViewModel
    {
        public string PMA_TRIP_NO { get; set; }
        public string PMA_ITEM_NO { get; set; }
        public string PMA_MAX_PRICE_ITEM_NO { get; set; }
        public string PMA_MET_NUM { get; set; }
        public string PMA_MET_NAME { get; set; }
        public string PMA_SUPPLIER { get; set; }
        public string PMA_SUPPLIER_SEARCHTERM { get; set; }
        public string PMA_FORMULA { get; set; }
        public string PMA_CONTRACT_TYPE { get; set; }
        public string PMA_INCOTERMS { get; set; }
        public string PMA_GT_C { get; set; }
        public string PMA_PURCHASE_TYPE { get; set; }
        public string PMA_STORAGE_LOCATION { get; set; }
        public string PMA_ORIGIN_COUNTRY { get; set; }
        public string PMA_VOLUME_BBL { get; set; }
        public string PMA_VOLUME_KBBL { get; set; }
        public string PMA_VOLUME_MT { get; set; }
        public string PMA_VOLUME_ML { get; set; }
        public string PMA_VOLUME_L15 { get; set; }
        public string PMA_VOLUME_LTON { get; set; }
        public string PMA_VOLUME_KG { get; set; }
        public string PMA_PURCHASE_UNIT { get; set; }
        public string PMA_TOLERANCE { get; set; }
        public string PMA_NOMINATED_FROM { get; set; }
        public string PMA_NOMINATED_TO { get; set; }
        public string PMA_RECEIVED_FROM { get; set; }
        public string PMA_RECEIVED_TO { get; set; }
        public string PMA_BL_DATE { get; set; }
        public string PMA_COMPLETE_TIME { get; set; }
        public string PMA_DUE_DATE { get; set; }
        public string PMA_BSW { get; set; }
        public string PMA_API { get; set; }
        public string PMA_DENSITY { get; set; }
        public string PMA_TEMP { get; set; }
        public string PMA_REMARK { get; set; }
        public string PMA_MEMO_NO { get; set; }
        public string PMA_PO_NO { get; set; }
        public string PMA_LOADING_PORT { get; set; }
        public string PMA_LOADING_DATE_FROM { get; set; }
        public string PMA_LOADING_DATE_TO { get; set; }
        public string PMA_DISCHARGE_LAYCAN_FROM { get; set; }
        public string PMA_DISCHARGE_LAYCAN_TO { get; set; }
        public string PMA_CREATED_DATE { get; set; }
        public string PMA_CREATED_BY { get; set; }
        public string PMA_UPDATED_DATE { get; set; }
        public string PMA_UPDATED_BY { get; set; }
        public string PMA_GIT_NO { get; set; }
        public string PMA_STATUS { get; set; }
        public string PMA_PO_MAT_ITEM { get; set; }
        public string PMA_PO_FREIGHT_ITEM { get; set; }
        public string PMA_PO_INSURANCE_ITEM { get; set; }
        public string PMA_PO_CUSTOMS_ITEM { get; set; }
        public string PMA_MAT_DOC { get; set; }
        public string PMA_BL_STATUS { get; set; }
        public string PMA_PAYMENTTERM { get; set; }
        public string PMA_BECHMARK { get; set; }
        public string PMA_HOLIDAY { get; set; }
        public string CST_INVOICE_STATUS { get; set; }
        public string IS_DELETE { get; set; }
        public string RET_STS { get; set; }
        public string RET_MSG { get; set; }

        public string CST_PRICE { get; set; }
        public string CST_FREIGHT_AMOUNT { get; set; }
        public string CST_INSURANCE_AMOUNT { get; set; }
        public string CST_INSURANCE_RATE { get; set; }

        public string GROSS_QTY_BBL { get; set; }
        public string GROSS_QTY_MT { get; set; }
        public string GROSS_QTY_ML { get; set; }

        public string FREIGHT_PRICE { get; set; }
        public string INSURANCE_PRICE { get; set; }
        public string IMPORTDUTY_PRICE { get; set; }
        public string PO_CURRENCY { get; set; }
        public string CST_BL_DATE { get; set; }
        public string CST_UNIT { get; set; }
        public string PMA_INVOICE_NO { get; set; }
        public string PMA_CDA_ROW_ID { get; set; }
        public string PMA_LOADING_PORT2 { get; set; }
        public string PMA_PER_BOND { get; set; }
        //public string FREIGHT_PRICE_TMP { get; set; }
        //public string INSURANCE_PRICE_TMP { get; set; }
        //public string IMPORTDUTY_PRICE_TMP { get; set; }
        //public string PO_CURRENCY_TMP { get; set; }


    }

    public class CrudeImportPlanViewModel_Detail
    {
        public string TripStatus { get; set; }
        public string LoadingMonth { get; set; }
        public string ArrivalMonth { get; set; }
        public string TripNo { get; set; }
        public string MatItemNo { get; set; }
        public string MatPriceItemNo { get; set; }
        public string Vessel { get; set; }
        public string Crude { get; set; }
        public string Supplier { get; set; }
        public string FormulaPrice { get; set; }
        public string Incoterm { get; set; }
        public string PriceStatus { get; set; }
        public string BLDate { get; set; }
        public string DuePaidDate { get; set; }
        public string BLQuantityBBL { get; set; }
        public string ProActual { get; set; }
        public string TotalAmtUSDBAHT { get; set; }
        public string FXPeriodDate { get; set; }
        public string ExAgreement { get; set; }
        public string ExBOT { get; set; }
        public string TotalInBaht { get; set; }
        public string CertifiedDate { get; set; }
        public string ApprovedDate { get; set; }
        public string QTYKBBL { get; set; }
        public string QTYKT { get; set; }

        public string Tolerance { get; set; }
        public string LoadingPort { get; set; }
        public string LDNominated { get; set; }
        public string LDReceived { get; set; }
        public string DischargeLayCan { get; set; }
        public string ActualETA { get; set; }
        public string GT_C { get; set; }
        public string BLReceived { get; set; }
        public string PlanningRelease { get; set; }
        public string BookingRelease { get; set; }
        public string ExportStatus { get; set; }


        public string i_CrudeCode { get; set; }
        public string i_SupplierCode { get; set; }
        public string h_com_code { get; set; }
        public string h_CDA_ROW_ID { get; set; }
        public string h_refID { get; set; }
        public string ID { get; set; }
        public string h_TripNo { get; set; }
        public string h_ItemNo { get; set; }
        public string h_ArraivalDate { get; set; }
        public string h_ArraivalDateNum { get; set; }
        public string h_Year { get; set; }
        public string h_Month { get; set; }
        public string h_Supplier { get; set; }
        public string h_FormulaPirce { get; set; }
        public string h_Incoterm { get; set; }
        public string h_GT_C { get; set; }
        public string h_Tolerance { get; set; }
        public string h_Channel { get; set; }
        public string h_Vessel { get; set; }
        public string h_VesselText { get; set; }
        public string h_VesselName { get; set; }
        public string h_MET_NUM { get; set; }
        public string h_CrudeName { get; set; }
        public string h_ContractType { get; set; }
        public string h_PurchaseType { get; set; }
        public string h_StorageLocation { get; set; }
        public string h_OriginCountry { get; set; }
        public string h_PurchaseUnit { get; set; }
        public string h_NorminateDate { get; set; }
        public string h_ReceiveDate { get; set; }
        public string h_BLDate { get; set; }
        public string h_CompleteTime { get; set; }
        public string h_DueDate { get; set; }
        public string h_BSW { get; set; }
        public string h_API { get; set; }
        public string h_Density { get; set; }
        public string h_Temp { get; set; }
        public string h_Remark { get; set; }
        public string h_MemoNo { get; set; }
        public string h_PONo { get; set; }
        public string h_LoadPort { get; set; }
        public string h_LoadPortText { get; set; }
        public string h_LoadPort2 { get; set; }
        public string h_LoadPortText2 { get; set; }
        public string h_LodingDate { get; set; }
        public string h_LodingDateCFR { get; set; }
        public string h_DischargeLayCanDate { get; set; }
        public string h_GitNo { get; set; }
        public string h_QTY_Kbbl { get; set; }
        public string h_QTY_BBL { get; set; }
        public string h_QTY_MT { get; set; }
        public string h_QTY_ML { get; set; }
        public string h_QTY_L15 { get; set; }
        public string h_QTY_KG { get; set; }
        public string h_QTY_LTON { get; set; }
        public string h_QTY_KT { get; set; }
        public string h_Actual_ETA { get; set; }

        public string i_ItemNo { get; set; }
        public string i_SubItemNo { get; set; }
        public string i_PriceStatus { get; set; }
        public string i_Formula { get; set; }
        public string i_BasePrice { get; set; }
        public string i_BP_Rounding { get; set; }
        public string i_OSP_PremiumDiscount { get; set; }
        public string i_OSP_Rounding { get; set; }
        public string i_TradingPremiumDiscount { get; set; }
        public string i_TRD_ROUNDING { get; set; }
        public string i_OTHER_COST { get; set; }
        public string i_OC_ROUNDING { get; set; }
        public string i_TOTAL_PRICE { get; set; }
        public string i_TP_ROUNDING { get; set; }
        public string i_TOTAL_AMOUNT { get; set; }
        public string i_TA_ROUNDING { get; set; }
        public string i_CURRENCY { get; set; }
        public string i_FX_AGREEMENT { get; set; }
        public string i_FXA_ROUNDING { get; set; }
        public string i_FX_BOT { get; set; }
        public string i_FXB_ROUNDING { get; set; }
        public string i_TOTAL_AMOUNT_THB { get; set; }
        public string i_CERTIFIED_DATE { get; set; }
        public string i_APPROVED_DATE { get; set; }
        public string i_PREPAYMENT { get; set; }
        public string i_PREPAYMENT_CURRENCY { get; set; }
        public string i_INVOICE { get; set; }
        public string i_BLStatus { get; set; }
        public string i_RecordStatus { get; set; }
        public string i_MatPrice { get; set; }
        public string i_PricePeriod { get; set; }
        public string i_PriceFor { get; set; }
        public string PMP_PHET_VOL { get; set; }

        public string IS_DELETE { get; set; }
        public Boolean i_IS_CHECK { get; set; }

        public string PONoCrude { get; set; }
        public string MemoNo { get; set; }
        public string INVOICE_NO { get; set; }
        public string PriceUpdateDate { get; set; }
        public string UnitPriceDigit { get; set; }
        public string PerformanceBond { get; set; }
        public string CalTotalPrice { get; set; }
        public string OSP_MONTH_PRICE { get; set; }

        //public List<FreightViewModel> freightDetail { get; set; }
        //public List<SurveyorViewModel> surveyorDetail { get; set; }
        //public List<OtherViewModel> otherDetail { get; set; }
    }

    public class FileViewModel
    {
        public string PAF_TRIP_NO { get; set; }
        public string PAF_FILE_FOR { get; set; }
        public string PAF_ITEM_NO { get; set; }
        public string PAF_FILE_NAME { get; set; }
        public string PAF_PATH { get; set; }
        public string PAF_NO { get; set; }
        public string PAF_DESCRIPTION { get; set; }
        public string PAF_DESC_TEMP { get; set; }
        public string PAF_UPLOAD_DATE { get; set; }
        public string PAF_RED_ID { get; set; }
        public string PAF_STATUS { get; set; }
        public string PAF_INDB_STS { get; set; }
    }


    public class FreightViewModel
    {
        public string FR_RefId { get; set; }
        public string FR_TripNo { get; set; }
        public string FR_ItemNo { get; set; }
        public string FR_CharteringType { get; set; }
        public string FR_CharteringTypeText { get; set; }
        public string FR_CostType { get; set; }
        public string FR_CostTypeText { get; set; }
        public string FR_Owner { get; set; }
        public string FR_OwnerText { get; set; }
        public string FR_Broker { get; set; }
        public string FR_BrokerText { get; set; }
        public string FR_Product { get; set; }
        public string FR_ProductText { get; set; }
        public string FR_Port { get; set; }
        public string FR_PortText { get; set; }
        public string FR_Freight { get; set; }
        public string FR_FreightType { get; set; }
        public string FR_FreightTypeText { get; set; }
        public string FR_Amt { get; set; }
        public string FR_AmtUnit { get; set; }
        public string FR_AmtUnitText { get; set; }
        public string FR_CoAmt { get; set; }
        public string FR_CoAmtUnit { get; set; }
        public string FR_CoAmtUnitText { get; set; }
        public string FR_DueDate { get; set; }
        public string FR_CerDate { get; set; }
        public string FR_ApproveDate { get; set; }
        public string FR_LayTime { get; set; }
        public string FR_WorldScale { get; set; }
        public string FR_MinLoadable { get; set; }
        public string FR_Demurrange { get; set; }
        public string FR_WarRiskPremium { get; set; }
        public string FR_Price { get; set; }
        public string FR_PriceUnit { get; set; }
        public string FR_PriceUnitText { get; set; }
        public string FR_MemoNo { get; set; }
        public string FR_PONo { get; set; }
        public string FR_ExcAgree { get; set; }
        public string FR_ExcBOT { get; set; }
        public string FR_ExcTotal { get; set; }
        public string FR_TotalUnit { get; set; }
        public string FR_TotalUnitText { get; set; }
        public string FR_ExcCertiDate { get; set; }
        public string FR_ExcApproveDate { get; set; }
        public string FR_InvoiceSTS { get; set; }
        public string FR_InvoiceNo { get; set; }
        public string FR_Volume { get; set; }
        public string FR_VolumeUnit { get; set; }
        public string FR_PeriodDate { get; set; }
        public string FR_Remark { get; set; }
        public string IS_DELETE { get; set; }
        public string RET_STS { get; set; }
        public string RET_MSG { get; set; }

        public string FR_MatCodeList { get; set; }
        public string FR_MatNameList { get; set; }


    }
    public class SurveyorViewModel
    {
        public string Sur_RefId { get; set; }
        public string Sur_TripNo { get; set; }
        public string Sur_ItemNo { get; set; }
        public string Sur_MatNum { get; set; }
        public string Sur_PO { get; set; }
        public string Sur_Type { get; set; }
        public string Sur_TypeText { get; set; }
        public string Sur_Vessel { get; set; }
        public string Sur_VesselText { get; set; }
        public string Sur_Surveyor { get; set; }
        public string Sur_SurveyorText { get; set; }
        public string Sur_Crude { get; set; }
        public string Sur_CrudeText { get; set; }
        public string Sur_Amount { get; set; }
        public string Sur_AmountUnit { get; set; }
        public string Sur_AmountUnitText { get; set; }
        public string Sur_CostSharing { get; set; }
        public string Sur_LoadDate { get; set; }
        public string Sur_LoadPort { get; set; }
        public string Sur_LoadPortText { get; set; }
        public string Sur_DischargeDate { get; set; }
        public string Sur_DiscPort { get; set; }
        public string Sur_DiscPortText { get; set; }
        public string Sur_InvoiceSTS { get; set; }
        public string Sur_Remark { get; set; }
        public string Sur_SurMat { get; set; }
        public string IS_DELETE { get; set; }
        public string RET_STS { get; set; }
        public string RET_MSG { get; set; }
        public Boolean i_IS_CHECK { get; set; }
        public string INVOICE_NO { get; set; }

        public List<SurveyorCrdude> crudeList { get; set; }
    }
    public class SurveyorMatViewModel
    {
        public string PSM_TRIP_NO { get; set; }
        public string PSM_ITEM_NO { get; set; }
        public string PSM_MET_NUM { get; set; }
    }
    public class SurveyorCrdude
    {
        public string TripNo { get; set; }
        public string Item { get; set; }
        public string MetNum { get; set; }
    }
    public class OtherViewModel
    {
        public string OC_RefId { get; set; }
        public string OC_TripNo { get; set; }
        public string OC_ItemNo { get; set; }
        public string OC_Type { get; set; }
        public string OC_TypeText { get; set; }
        public string OC_Crude { get; set; }
        public string OC_CrudeText { get; set; }
        public string OC_Vendor { get; set; }
        public string OC_VendorText { get; set; }
        public string OC_Port { get; set; }
        public string OC_PortText { get; set; }
        public string OC_OperationDate { get; set; }
        public string OC_Price { get; set; }
        public string OC_Amount { get; set; }
        public string OC_AmountUnit { get; set; }
        public string OC_AmountUnitText { get; set; }
        public string OC_Demurrage { get; set; }
        public string OC_FXAgree { get; set; }
        public string OC_FXBOT { get; set; }
        public string OC_Total { get; set; }
        public string OC_DueDate { get; set; }
        public string OC_CerDate { get; set; }
        public string OC_ApprDate { get; set; }
        public string OC_InvoiceSTS { get; set; }
        public string OC_Remark { get; set; }
        public string OC_MemoNo { get; set; }
        public string IS_DELETE { get; set; }
        public string RET_STS { get; set; }
        public string RET_MSG { get; set; }
        public string INVOICE_NO { get; set; }
    }

    public class CustomsViewModel
    {
        public string CT_TripNo { get; set; }
        public string CT_MatItemNo { get; set; }
        public string CT_QtyBBL { get; set; }
        public string CT_QtyMT { get; set; }
        public string CT_QtyML { get; set; }
        public string CT_BLPurchaseDate { get; set; }
        public string CT_DuePaidDate { get; set; }
        public string CT_InsRate { get; set; }
        public string CT_Price { get; set; }
        public string CT_FreightAmt { get; set; }
        public string CT_InsAmt { get; set; }
    }

    public class FreightTypeViewModel
    {
        public decimal PMF_CODE { get; set; }
        public string PMF_NAME { get; set; }
        public string PMF_CHARTERING_TYPE { get; set; }
        public string PMF_DATA_FROM_CHARTERING { get; set; }
        public string PMF_SUPPLIER { get; set; }
        public string PMF_SUPPLIER_DISPLAY { get; set; }
        public string PMF_BROKER { get; set; }
        public string PMF_BROKER_DISPLAY { get; set; }
        public string PMF_CHARTERING_TYPE2 { get; set; }
        public string PMF_CHARTERING_TYPE_DISPLAY { get; set; }
        public string PMF_LAYTIME { get; set; }
        public string PMF_LAYTIME_DISPLAY { get; set; }
        public string PMF_FREIGHT_SUB_TYPE { get; set; }
        public string PMF_FREIGHT_SUB_TYPE_DISPLAY { get; set; }
        public string PMF_PRODUCT { get; set; }
        public string PMF_PRODUCT_DISPLAY { get; set; }
        public string PMF_PORT { get; set; }
        public string PMF_PORT_DISPLAY { get; set; }
        public string PMF_PERIOD_FROM { get; set; }
        public string PMF_PERIOD_FROM_DISPLAY { get; set; }
        public string PMF_PERIOD_TO { get; set; }
        public string PMF_PERIOD_TO_DISPLAY { get; set; }
        public string PMF_VOLUME { get; set; }
        public string PMF_VOLUME_DISPLAY { get; set; }
        public string PMF_WS { get; set; }
        public string PMF_WS_DISPLAY { get; set; }
        public string PMF_MIN_LOAD { get; set; }
        public string PMF_MIN_LOAD_DISPLAY { get; set; }
        public string PMF_DEMURRAGE { get; set; }
        public string PMF_DEMURRAGE_DISPLAY { get; set; }
        public string PMF_WRP { get; set; }
        public string PMF_WRP_DISPLAY { get; set; }
        public string PMF_PRICE { get; set; }
        public string PMF_PRICE_DISPLAY { get; set; }
        public string PMF_TOTAL { get; set; }
        public string PMF_TOTAL_DISPLAY { get; set; }
        public string PMF_TOTAL_1 { get; set; }
        public string PMF_TOTAL_1_DISPLAY { get; set; }
        public string PMF_DUE_DATE { get; set; }
        public string PMF_DUE_DATE_DISPLAY { get; set; }
        public string PMF_CERTIFIED_DATE { get; set; }
        public string PMF_CERTIFIED_DATE_DISPLAY { get; set; }
        public string PMF_APPROVED_DATE { get; set; }
        public string PMF_APPROVED_DATE_DISPLAY { get; set; }
        public string PMF_REMARK { get; set; }
        public string PMF_REMARK_DISPLAY { get; set; }
        public Nullable<System.DateTime> PMF_CREATE_DATE { get; set; }
        public string PMF_CREATE_BY { get; set; }
        public Nullable<System.DateTime> PMF_UPDATED_DATE { get; set; }
        public string PMF_UPDATED_BY { get; set; }
        public string PMF_COST_TYPE { get; set; }
        public string PMF_COST_TYPE_DISPLAY { get; set; }



    }
    public class SurveyorTypeViewModel
    {
        public decimal PMS_CODE { get; set; }
        public string PMS_NAME { get; set; }
        public string PMS_LOADING_DATE { get; set; }
        public string PMS_LOADING_DATE_DISPLAY { get; set; }
        public string PMS_LOADING_PORT { get; set; }
        public string PMS_LOADING_PORT_DISPLAY { get; set; }
        public string PMS_DISCHARGING_DATE { get; set; }
        public string PMS_DISCHARGING_DATE_DISPLAY { get; set; }
        public string PMS_DISCHARGING_PORT { get; set; }
        public string PMS_DISCHARGING_PORT_DISPLAY { get; set; }
        public string PMS_VESSEL { get; set; }
        public string PMS_VESSEL_DISPLAY { get; set; }
        public string PMS_MATERIAL { get; set; }
        public string PMS_MATERIAL_DISPLAY { get; set; }
        public string PMS_SURVEYOR { get; set; }
        public string PMS_SURVEYOR_DISPLAY { get; set; }
        public string PMS_TOTAL { get; set; }
        public string PMS_TOTAL_DISPLAY { get; set; }
        public string PMS_COST_SHARING { get; set; }
        public string PMS_COST_SHARING_DISPLAY { get; set; }
        public string PMS_REMARK { get; set; }
        public string PMS_REMARK_DISPLAY { get; set; }
        public Nullable<System.DateTime> PMS_CREATE_DATE { get; set; }
        public string PMS_CREATE_BY { get; set; }
        public Nullable<System.DateTime> PMS_UPDATED_DATE { get; set; }
        public string PMS_UPDATED_BY { get; set; }
    }


    [Serializable]
    public class PCF_MATER_TYPE
    {
        public List<SurveyorTypeViewModel> SurveyorTypeViewModel { get; set; }
        public List<FreightTypeViewModel> FreightTypeViewModel { get; set; }
    }

    [Serializable]
    public class PCFHolidayFormulaModel
    {
        public List<PCF_MATERIAL_DC> PCF_MATERIAL_DC { get; set; }
        public List<PCF_CRUDE_DC_BANK> PCF_CRUDE_DC_BANK { get; set; }
        public List<PCF_HOLIDAY_FORMULA> PCF_HOLIDAY_FORMULA { get; set; }
    }

    public class PCF_MATERIAL_DC
    {
        //{ "DC": "LOCAL", "MAT_CODE": "Phet" },
        public string DC { get; set; }
        public string MAT_CODE { get; set; }
    }

    public class PCF_MT_MATERIAL
    {
        public string MET_NUM { get; set; }
        public string MET_PRODUCT_DENSITY { get; set; }
    }

    public class PCF_HOLIDAY_FORMULA
    {
        //{ "CRUDE_DC": "IMPORT", "BANK_TYPE": "UK", "CURRENCY": "USD", "DAY_START": "0", "DAY_END": "1", "HOLIDAY_ADJ": "1" },
        public string CRUDE_DC { get; set; }
        public string BANK_TYPE { get; set; }
        public string CURRENCY { get; set; }
        public int DAY_START { get; set; }
        public int DAY_END { get; set; }
        public string HOLIDAY_ADJ { get; set; }
    }

    public class PCF_CRUDE_DC_BANK {
        public string CRUDE_DC { get; set; }
        public string BANK_TYPE { get; set; }
        public string CURRENCY { get; set; }
    }

    [Serializable]
    public class CIP_Config
    {
        public List<FORMULA_PRICING> FORMULA_PRICING { get; set; }
        public List<FORMULA_SAP_PRICING_STRUCTURE> FORMULA_SAP_PRICING_STRUCTURE { get; set; }
        public List<PCF_MT_PURCHASE_TYPE> PCF_MT_PURCHASE_TYPE { get; set; }
        public List<PCF_MT_STORAGE_LOCATION> PCF_MT_STORAGE_LOCATION { get; set; }
        public List<PCF_MT_STORAGE_LOCATION_DETAIL> PCF_MT_STORAGE_LOCATION_DETAIL { get; set; }
        public List<PCF_MT_PRICE_STATUS> PCF_MT_PRICE_STATUS { get; set; }
        public List<PCF_MT_CHARTERING_TYPE> PCF_MT_CHARTERING_TYPE { get; set; }
        public List<PCF_MT_FREIGHT_TYPE> PCF_MT_FREIGHT_TYPE { get; set; }
        public List<PCF_MT_PO_SURVEYOR_TYPE> PCF_MT_PO_SURVEYOR_TYPE { get; set; }
        public List<CIP_MT_COMPANY> MT_COMPANY { get; set; }
        public List<CIP_CURRENCY> CURRENCY { get; set; }
        public List<PCF_MONTH> PCF_MONTH { get; set; }
        public List<PCF_MT_OTHER_COST_TYPE> PCF_MT_OTHER_COST_TYPE { get; set; }
        public List<PCF_CHANNEL> PCF_CHANNEL { get; set; }
        public List<PCF_MT_INSURANCE_RATE> PCF_MT_INSURANCE_RATE { get; set; }
        public List<PCF_UNIT_CAL> PCF_UNIT_CAL { get; set; }
        public List<PCF_PO_SURVEYOR_PUR_GROUP> PCF_PO_SURVEYOR_PUR_GROUP { get; set; }
        public List<PCF_SEARCH_TYPE> PCF_SEARCH_TYPE { get; set; }
        public List<PCF_FREIGHT_PRODUCT> PCF_FREIGHT_PRODUCT { get; set; }
    }

    [Serializable]
    public class CIP2_Config
    {
        public List<InsRate> PCF_MT_INSURANCE_RATE { get; set; }


    }

    [Serializable]
    public class PCF_Authen
    {
        public List<FIELD_AUTHEN> PLAN { get; set; }
        public List<FIELD_AUTHEN> BOOK { get; set; }
        public List<FIELD_AUTHEN> ALL { get; set; }

    }

    public class FIELD_AUTHEN
    {
        public string FIELD_MODEL { get; set; }
        public string DISPLAY_NAME { get; set; }
        public string ENABLED { get; set; }
        public string DISPLAY { get; set; }
    }

    public class PCF_PO_SURVEYOR_PUR_GROUP
    {
        public string ROLE { get; set; }
        public string VALUE { get; set; }
    }

    public class InsRate
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class CIP_CURRENCY
    {
        public string CODE { get; set; }
    }
    public class CIP_MT_COMPANY
    {
        public string CODE { get; set; }
    }
    public class PCF_MT_OTHER_COST_TYPE
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string ATE_SAP_MEMO { get; set; }
    }
    public class PCF_MT_PO_SURVEYOR_TYPE
    {
        public string NAME { get; set; }
        public string LOADING_PORT { get; set; }
        public string LOADING_DATE { get; set; }
        public string DISCHARGING_PORT { get; set; }
        public string DISCHARGING_DATE { get; set; }
    }
    public class PCF_MT_FREIGHT_TYPE
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string CHARTERING_TYPE { get; set; }
    }
    public class PCF_SEARCH_TYPE
    {
        public string value { get; set; }
        public string text { get; set; }
    }
    public class PCF_FREIGHT_PRODUCT
    {
        public string value { get; set; }
        public string text { get; set; }
    }
    public class PCF_MT_CHARTERING_TYPE
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }
    public class PCF_MT_PRICE_STATUS
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }
    public class PCF_MT_STORAGE_LOCATION
    {
        public string COMPANY_CODE { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
    }
    public class PCF_MT_STORAGE_LOCATION_DETAIL
    {
        public string COMPANY_CODE { get; set; }
        public string PURCHASE_TYPE { get; set; }
        public string INCOTERMS { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
    }
    public class PCF_MT_PURCHASE_TYPE
    {
        public string COMPANY_CODE { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
    }
    public class FORMULA_SAP_PRICING_STRUCTURE
    {
        public string code { get; set; }
        public string description { get; set; }
    }

    public class FORMULA_PRICING
    {
        public string code { get; set; }
        public string type { get; set; }

    }
    public class PCF_MONTH
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class PCF_CHANNEL
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class PCF_MT_INSURANCE_RATE
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class PCF_UNIT_CAL
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    [Serializable]
    public class CIP_Currency
    {
        public List<CURRENCY> CURRENCY { get; set; }
    }
    public class CURRENCY
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class ResultFromMtSapMemoMapping
    {
        public string MetNum { get; set; }
        public string CostName { get; set; }
        public string PlanningType { get; set; }
        public string PlanningGroup { get; set; }
    }

    [Serializable]
    public class CalPricingAllViewModel
    {
        public List<CalPricingAll> PricingDataViewModel { get; set; }
    }

    public class CalPricingAll
    {
        public string rowIndex { get; set; }
        public string benchMark { get; set; }
        public string matNum { get; set; }
        public string priceStatus { get; set; }
        public string pricePeriod { get; set; }
        public string crudePhetVol { get; set; }
        public string priceVol { get; set; }
        public string retsts { get; set; }
        public string retmsg { get; set; }
    }


    public class DailyPriceOutturnViewModel
    {
        public string TRIP_NO { get; set; }
        public string MAT_NUM { get; set; }
        public string BL_DATE { get; set; }
        public string YYYYMMDD { get; set; }
        public Decimal QTY_BBL { get; set; }
    }

    public class DailyPriceViewModel
    {
        public string TRIP_NO { get; set; }
        public string MAT_NUM { get; set; }
        public string INVOICE_NO { get; set; }
        public string BL_DATE { get; set; }
        public string YYYYMMDD { get; set; }
        public Nullable<Decimal> PRO_PRICE { get; set; }
        public Nullable<Decimal> FINAL_PRICE { get; set; }
        public Nullable<Decimal> DIFF_PRICE { get; set; }
        public Nullable<Decimal> OUTTURN_QTY { get; set; }
        public Nullable<Decimal> P1_FX_RATE { get; set; }
        public Nullable<Decimal> P1_UNIT_PRICE { get; set; }
        public Nullable<Decimal> P1_TOTAL_BF_VAT { get; set; }
        public Nullable<Decimal> P1_VAT { get; set; }
        public Nullable<Decimal> P1_TOTAL_AF_VAT { get; set; }
        public Nullable<Decimal> P2_FX_RATE { get; set; }
        public Nullable<Decimal> P2_UNIT_PRICE { get; set; }
        public Nullable<Decimal> P2_DIFF_UNITPRICE { get; set; }
        public Nullable<Decimal> P2_ADJUST_PRICE { get; set; }
        public Nullable<Decimal> P2_TOTAL_DIFF { get; set; }
        public string P2_DIFF_DOCNO { get; set; }
        public Nullable<Decimal> F1_FX_RATE { get; set; }
        public Nullable<Decimal> F1_UNIT_PRICE { get; set; }
        public Nullable<Decimal> F1_TOTAL_BF_VAT { get; set; }
        public Nullable<Decimal> F1_VAT { get; set; }
        public Nullable<Decimal> F1_TOTAL_AF_VAT { get; set; }
        public Nullable<Decimal> F2_FX_RATE { get; set; }
        public Nullable<Decimal> F2_TOTAL_DIFF1 { get; set; }
        public Nullable<Decimal> F2_TOTAL_DIFF2 { get; set; }
        public Nullable<Decimal> F2_TOTAL_DIFF3 { get; set; }


    }

    public class ResultModel {
        public string TYPE { get; set; }
        public string MSG_ID { get; set; }
        public string MSG_TXT { get; set; }
        public string VALUE { get; set; }
        public string VALUE2 { get; set; }
        public string VALUE3 { get; set; }
    }

    public class CharteringOutbound
    {
        public string VesselCode { get; set; }
        public string VesselName { get; set; }
        public string RoundType { get; set; }
        public string RoundVol { get; set; }
        public string FlatRate { get; set; }
        public string LayTime { get; set; }
        public string FreightType { get; set; }
        public string Total { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string PurchaseNo { get; set; }
         
    }

    public class WSDataModel
    {
        public string FreightType { get; set; }
        public string VesselCode { get; set; }
        public string VesselName { get; set; }
        public string FlatRate { get; set; }
        public string Laytime { get; set; }
        public string MinLoad { get; set; }
        public string Dem { get; set; }
        public string WarRisk { get; set; }
        public string WS { get; set; }
        public string Total { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string PurchaseNo { get; set; }

    }



}