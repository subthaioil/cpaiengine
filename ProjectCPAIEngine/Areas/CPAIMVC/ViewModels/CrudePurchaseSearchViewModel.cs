﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CrudePurchaseSearchViewModel
    {
        public List<CrudePurchaseEncrypt> CrudePurchaseTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> userList { get; set; }

        public List<SelectListItem> feedstock { get; set; }
        public List<SelectListItem> product { get; set; }
        public List<SelectListItem> supplier { get; set; }

        public string search_User { get; set; }
        public string search_Product { get; set; }
        public string search_Supplier { get; set; }
        public string search_Stock { get; set; }
        public string search_DatePurchase { get; set; }
    }

    public class CrudePurchaseSearchBondViewModel
    {
        public List<CrudePurchaseViewModel_SearchBond> CrudePurchases { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> userList { get; set; }

        public List<SelectListItem> Suppliers { get; set; }
        public List<SelectListItem> Statuses { get; set; }
        public List<SelectListItem> Crudes { get; set; }
        public string Search_datePurchase {get;set;}
        public string Search_crudes { get; set; }
        public string Search_suppliers { get; set; }
        public string Search_statuses { get; set; }
        public string Search_createdByUser { get; set; }
    }
}