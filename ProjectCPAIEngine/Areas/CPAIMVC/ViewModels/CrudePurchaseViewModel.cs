﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectCPAIEngine.Model;
using System.Web.Mvc;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CrudePurchaseViewModel
    {
        public string date_purchase { get; set; }
        public string plan_month { get; set; }
        public string plan_year { get; set; }
        public string feedstock { get; set; }
        public string feedstock_others { get; set; }
        public string for_feedstock { get; set; }
        public string crude_id { get; set; }
        public string crude_name { get; set; }
        public string crude_name_others { get; set; }
        public string origin_id { get; set; }
        public string origin { get; set; }
        public string term { get; set; }
        public string term_others { get; set; }
        public string purchase { get; set; }
        public string explanation { get; set; }
        public string explanationAttach { get; set; }
        public List<CdpOffersItem> offers_items { get; set; }
        public List<CdpCompetOffersItem> compet_offers_items { get; set; }
        public CdpContractPeriod contract_period { get; set; }
        public CdpLoadingPeriod loading_period { get; set; }
        public CdpDischargingPeriod discharging_period { get; set; }
        public string other_condition { get; set; }
        public CdpPaymentTerms payment_terms { get; set; }
        public CdpProposal proposal { get; set; }
        public string notes { get; set; }
        public List<ApproveItem> approve_items { get; set; }
        public string reason { get; set; }
        public string gtc { get; set; }
        public string requested_by { get; set; }
        public string supply_source { get; set; }
        public string performance_bond { get; set; }
        public string bond_documents { get; set; }
        public string offered_by_ptt { get; set; }
        public string ptt_id { get; set; }

        public string contract { get; set; }
        public string loading { get; set; }
        public string discharging { get; set; }
        public string price_period { get; set; }
        public string nrdoDay { get; set; }
        public string crude_name_dummy { get; set; }
        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public List<SelectListItem> ddlForFeedstock{ get; set; }
        public List<SelectListItem> ddlFeedstock { get; set; }
        public List<SelectListItem> ddlHiddenFeedstock { get; set; }
        public List<SelectListItem> ddlHiddenFeedstock2 { get; set; }
        public List<SelectListItem> ddlTerm { get; set; }
        public List<SelectListItem> ddlPurchase { get; set; }
        public List<SelectListItem> ddlIncoterms { get; set; }
        public List<SelectListItem> ddlBenchmarkPrice { get; set; }
        public List<SelectListItem> ddlPaymentTerms { get; set; }
        public List<SelectListItem> ddlProposalReason { get; set; }
        public List<SelectListItem> ddlCrude { get; set; }
        public List<SelectListItem> ddlCountry { get; set; }
        public List<SelectListItem> ddlSupplier { get; set; }
        public List<SelectListItem> ddlFormula_incoterms { get; set; }
        public List<SelectListItem> ddlCrudeCountry { get; set; }
        public List<SelectListItem> ddlToleranceType { get; set; }
        public List<SelectListItem> ddlToleranceOption { get; set; }
        public List<SelectListItem> ddlSupplySource { get; set; }
        public string supplier_bond_rates { get; set; }
        public List<PERFORMANCEBOND> ListPERFORMANCEBOND { get; set; }
        public string supplier_New_Name { get; set; }
        public string saveAndPreview { get; set; }
        public string select_New_Name { get; set; }

        public string reasonPTToffer { get; set; }
        public string offerVia { get; set; }
        public string reasonOfferVia { get; set; }
        public string offerDate { get; set; }
        public List<SelectListItem> ddlOfferVia { get; set; }





        public string CrudeSale_Optimization { get; set; }
        public string CrudeSale_Ref { get; set; }
    }

    public class PERFORMANCEBOND
    {
        public string SUPPLIERCODE { get; set; }
        public string BOND { get; set; }
    }

    public class CrudePurchaseReportViewModel
    {
        public List<SelectListItem> ddlFeedStock { get; set; }
        public List<SelectListItem> ddlCrude { get; set; }
        public string FeedStockJSON { get; set; }
        public string CrudeJSON { get; set; }
        public string SupplierJSON { get; set; }
        public string ExportTo { get; set; }    // PDF, EXCEL
        public string ActionMSG { get; set; }
        public CrudePurchaseReportViewModel_Search cdp_Search { get; set; }
    }

    public class CrudePurchaseReportViewModel_Search
    {
        public string sLoadingPeriodDate { get; set; }
        public string sDischargingPeriodDate { get; set; }
        public string sFeedStock { get; set; }
        public string sCrudeName { get; set; }
        public string sSupplier { get; set; }
        public string dMarginTotal { get; set; }
        public List<CrudePurchaseReportViewModel_SearchData> sSearchData { get; set; }
    }

    public class CrudePurchaseReportViewModel_SearchData
    {
        public string dLoadingPeriodDate { get; set; }
        public string dDischargingPeriodDate { get; set; }
        public string dCrudeName { get; set; }
        public string dOrigin { get; set; }
        public string dFeedStock { get; set; }
        public string dFor { get; set; }
        public string dSupplier { get; set; }
        public string dContactPerson { get; set; }
        public string dQuantity { get; set; }
        public string dIncoterms { get; set; }
        public string dOffer { get; set; }
        public string dMarketSource { get; set; }
        public string dLatestLP { get; set; }
        public string dBechmarkPrice { get; set; }
        public string dRankRound { get; set; }
        public string dMarginLP { get; set; }
        public string dMargin { get; set; }
    }

    public class CrudePurchaseReportViewModel_Search_PIT
    {

        private bool _Status;
        public bool Status
        {
            get { return _Status; }
            set { _Status = value; }
        }


        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private int? _ReturnIdentity = null;
        public int? ReturnIdentity
        {
            get { return _ReturnIdentity; }
            set { _ReturnIdentity = value; }
        }
        public List<CrudePurchaseReportViewModel_SearchData_PIT> sSearchData { get; set; }
    }
    public class CrudePurchaseReportViewModel_SearchData_PIT
    {
        public string dLoadingPeriodDate { get; set; }
        public string dDischargingPeriodDate { get; set; }
        public string dContractDate { get; set; }
        public string dPricingPeriodDate { get; set; }
        public string dCrudeId { get; set; }
        public string dCrudeName { get; set; }
        public string dCrudeOther { get; set; }
        public string dTerms { get; set; }
        public string dTermsOthers { get; set; }
        public string dOrigin { get; set; }
        public string dFeedStock { get; set; }
        public string dFeedStockOther { get; set; }
        public string dFor { get; set; }
        public string dGtc { get; set; }
        public string dPaymentTerms { get; set; }
        public string dSubPaymentTerms { get; set; }
        public string dPaymentTermsDetail { get; set; }
        public string dPaymentTermsOthers { get; set; }
        public string dTolerance { get; set; }
        public string dToleranceType { get; set; }
        public string dToleranceOption { get; set; }
        public string dSupplierId { get; set; }
        public string dSupplier { get; set; }
        public string dContactPerson { get; set; }
        public string dQuantity { get; set; }
        public string dIncoterms { get; set; }
        public string dOffer { get; set; }
        public string dOfferRowID { get; set; }
        public string dMarketSource { get; set; }
        public string dLatestLP { get; set; }
        public string dBechmarkPrice { get; set; }
        public string dRankRound { get; set; }
        public string dMarginLP { get; set; }
        public string dMargin { get; set; }
        public string dRowId { get; set; }
        public string dPurchaseNo { get; set; }
        public string dQuantityUnit { get; set; }
        public string dDensity { get; set; }
        public string dPerBond { get; set; }

    }

    public class CrudePurchaseViewModel_SearchBond
    {
        public string CrudePurchaseRowId;
        public string TripNo;
        public string PurchaseDate;
        public string Crude;
        public string SupplierId;
        public string SupplierName;
        public string Quantity;
        public string Margin;
        public string CreatedBy;
        public string BlDate;
        public string Status;
        public string Bond;
        public string Bond_fileUpload;
        public List<PerformanceBondDocument> BondDocuments;

        /*public List<SelectListItem> ddlSuppliers { get; set; }
        public List<SelectListItem> ddlStatuses { get; set; }
        public List<SelectListItem> ddlCrudeNames { get; set; }*/

        public struct PerformanceBondDocument
        {
            public string FileName;
            public string FilePath;
        }
    }

}