﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CrudeRoutesViewModel
    {
        public CrudeRoutesViewModel_Seach crude_Search { get; set; }
        public CrudeRoutesViewModel_Detail crude_Detail { get; set; }

        public List<SelectListItem> ddl_Status { get; set; }
    }
  
    public class CrudeRoutesViewModel_Seach
    {
        public string sDateFromTo { get; set; }     // Display (dd/MM/yyyy to dd/MM/yyyy)  
        public string sStatus { get; set; }
        public List<CrudeRoutesViewModel_SeachData> sSearchData { get; set; }
    }

    public class CrudeRoutesViewModel_SeachData
    {
        public string dDate { get; set; }           // Display (dd/MM/yyyy)
        public string dDateOrder { get; set; }      // Not Display (yyyyMMdd)
        public string dTD2 { get; set; }
        public string dTD3 { get; set; }
        public string dTD15 { get; set; }
        public string dStatus { get; set; }

    }

    public class CrudeRoutesViewModel_Detail
    {
        public string Date { get; set; }    // Display (dd/MM/yyyy)
        public string TD2 { get; set; }
        public string TD3 { get; set; }
        public string TD15 { get; set; }
        public string Status { get; set; }
        public bool BoolStatus { get; set; }
    }

  
}