﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{

    public class CustomVatViewModel
    {
        public CustomVatViewModel_Search CustomVat_Search { get; set; }
        public CustomVatViewModel_Calc_Total CustomVat_Calc_Total { get; set; }
        public List<CustomVatViewModel_Calc> CustomVat_Calc { get; set; }
        public List<GLAccountVATViewModel> GLAccountVAT { get; set; }
        public List<VATFactorViewModel> VATFactorList { get; set; }

        public List<SelectListItem> ddl_Vessel { get; set; }
        public string json_Vessel { get; set; }
        public List<SelectListItem> ddl_Crude { get; set; }
        public string json_Crude { get; set; }
        public List<SelectListItem> ddl_Supplier { get; set; }
        public string json_Supplier { get; set; }

        public List<SelectListItem> ddl_InsuranceRate { get; set; }
       
        public string SenMailResult { get; set; }
        public string sTripNo { get; set; }
        public string sMatItemNo { get; set; }
        public string sROE { get; set; }
        public string sInsuranceRate { get; set; }
        public string sDocumentDate { get; set; }
        public string sPostingDate { get; set; }
        public string sCalculationUnit { get; set; }
        public string sTotalAll { get; set; }
        public string sTotalImportDuty { get; set; }

        public VatPercent nVatPercentages { get; set; }
    }

    public class CustomVatSavedData {
        public string TripNo { get; set; }
        public string MatItemNo { get; set; }
        public string TaxBase { get; set; }
        public string CorrectVat { get; set; }
        public string ImportDuty { get; set; }
        public string ExciseTax { get; set; }
        public string MunicipalTax { get; set; }
        public string OilFuelFund { get; set; }
        public string DepositOfImportDuty { get; set; }
        public string DepositOfExciseTax { get; set; }
        public string DepositOfMunicipalTax { get; set; }
        public string EnergyOilFuelFund { get; set; }
    }

    public class CustomVatViewModel_Search
    {
        public string sBLDate { get; set; }
        public string sDueDate { get; set; }
        public string sTripNo { get; set; }
        public string sVessel { get; set; }
        public string sCrude { get; set; }
        public string sSupplier { get; set; }
        public List<CustomVatViewModel_SearchData> sSearchData { get; set; }
    }

    public class CustomVatViewModel_SearchData
    {
        public string dTripNo { get; set; }
        public string dVessel { get; set; }
        public string dCrude { get; set; }
        public string dSupplier { get; set; }
        public string dBLDate { get; set; }
        public string dDueDate { get; set; }
        public string dQtyBBL { get; set; }
        public string dQtyMT { get; set; }
        public string dQtyML { get; set; }
        public string dIncoTerms { get; set; }
        public string dPONo { get; set; }
        public string dFIDocVAT { get; set; }
        public string dInvoiceDocMIRO { get; set; }
        public string dFIDocMIRO { get; set; }
        public string dCustomPrice { get; set; }
        public string dFreightAmount { get; set; }
        public string dMatItemNo { get; set; }
        public string dMetNum { get; set; }
        public string dVendorNo { get; set; }
        public string dVesselNo { get; set; }
        public string dCompanyCode { get; set; }
        public string dActual_from { get; set; }
        public string dActual_to { get; set; }
    }

    public class CustomVatViewModel_Calc_Total
    {
        public decimal totalQtyBBL { get; set; }
        public decimal totalQtyMT { get; set; }
        public decimal totalQtyML { get; set; }
        public decimal totalFOB { get; set; }
        public decimal totalFRT { get; set; }
        public decimal totalCFR { get; set; }
        public decimal totalINS { get; set; }
        public decimal totalCIF { get; set; }
        public decimal totalPremium { get; set; }
        public decimal totalTotalUSD100 { get; set; }
        public decimal totalBaht100 { get; set; }
        public decimal totalBaht105 { get; set; }
        public decimal totalVAT { get; set; }
        public decimal totalTaxBase { get; set; }
        public decimal totalCorrectVAT { get; set; }
        public decimal totalImportDuty { get; set; }
        public decimal totalExciseTax { get; set; }
        public decimal totalMunicipalTax { get; set; }
        public decimal totalOilFuelFund { get; set; }
        public decimal totalDepositOfImportDuty { get; set; }
        public decimal totalDepositOfExciseTax { get; set; }
        public decimal totalDepositOfMunicipalTax { get; set; }
        public decimal totalEnergyOilFuelFund { get; set; }
        public decimal totalAll { get; set; }

        public CustomVatViewModel_Calc_Total()
        {
            totalQtyBBL = 0;
            totalQtyMT = 0;
            totalQtyML = 0;
            totalFOB = 0;
            totalFRT = 0;
            totalCFR = 0;
            totalINS = 0;
            totalCIF = 0;
            totalPremium = 0;
            totalTotalUSD100 = 0;
            totalBaht100 = 0;
            totalBaht105 = 0;
            totalVAT = 0;
            totalTaxBase = 0;
            totalCorrectVAT = 0;
            totalImportDuty = 0;
            totalExciseTax = 0;
            totalMunicipalTax = 0;
            totalOilFuelFund = 0;
            totalDepositOfImportDuty = 0;
            totalDepositOfExciseTax = 0;
            totalDepositOfMunicipalTax = 0;
            totalEnergyOilFuelFund = 0;
            totalAll = 0;
        }
    }

    public class CustomVatViewModel_Calc
    {
      
        public string No { get; set; }
        public string PoNo { get; set; }
        public string CrudeSupplier { get; set; }
        public string ROE { get; set; }
        public string QtyBBL { get; set; }
        public string QtyMT { get; set; }
        public string QtyML { get; set; }
        public string CustomsPrice { get; set; }
        public string FOB { get; set; }
        public string FRT { get; set; }
        public string CFR { get; set; }
        public string INS { get; set; }
        public string CIF { get; set; }
        public string Premium { get; set; }
        public string TotalUSD100 { get; set; }
        public string Baht100 { get; set; }
        public string Baht105 { get; set; }
        public string VAT { get; set; }
        public string TaxBase { get; set; }
        public string CorrectVAT { get; set; }
        public string ImportDuty { get; set; }
        public string ExciseTax { get; set; }
        public string MunicipalTax { get; set; }
        public string OilFuelFund { get; set; }
        public string DepositOfImportDuty { get; set; }
        public string DepositOfExciseTax { get; set; }
        public string DepositOfMunicipalTax { get; set; }
        public string EnergyOilFuelFund { get; set; }
        public string MatItemNo { get; set; }
        public string TripNo { get; set; }
        public string MetNum { get; set; }
        public string MatItemName { get; set; }
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
        public string VesselNo { get; set; }
        public string VesselName { get; set; }
        public string CompanyCode { get; set; }
        public string FIDocVAT { get; set; }
        public string FIDocVATStatus { get; set; }
        public string FIDocVATMessage { get; set; }
        public string FIDocMIRO { get; set; }
        public string FIDocMIROStatus { get; set; }
        public string FIDocMIROMessage { get; set; }
        public string InvoiceDocMIRO { get; set; }
        public string InvoiceDocMIROStatus { get; set; }
        public string InvoiceDocMIROMessage { get; set; }
        public string sActual_from { get; set; }
        public string sActual_to { get; set; }
    }

    public class GLAccountVATViewModel
    {
        
        public string Type { get; set; }
        public string GLAccount { get; set; }
        public string TaxCodeVat { get; set; }
        public string ValuePart1 { get; set; }
        public string TaxCodeMIRO { get; set; }
        public string Currency { get; set; }
    }

    public class VATFactorViewModel
    {
        public string MetNum { get; set; }
        public string ImportDutyF1 { get; set; }
        public string ImportDutyF1Const { get; set; }
        public string ImportDutyF2 { get; set; }
        public string ImportDutyF2Const { get; set; }
        public string ExciseTax { get; set; }
        public string ExciseTaxConst { get; set; }
        public string MunicipalTax { get; set; }
        public string MunicipalTaxConst { get; set; }
        public string DepositImportDuty { get; set; }
        public string DepositImportDutyConst { get; set; }
        public string DepositExciseTax { get; set; }
        public string DepositExciseTaxConst { get; set; }
        public string DepositMunicipalTax { get; set; }
        public string DepositMunicipalTaxConst { get; set; }
    }

    [Serializable]
    public class GlobalConfig
    {
        public List<InsuranceRate> PCF_MT_INSURANCE_RATE { get; set; }
    }

    public class VatPercent
    {
        public VatPercent()
        {
            try
            {
                string strJSON = "{\"PercentagesTaxBaseVAT\":0.00,\"PercentagesVAT\":0.00}";

                string JsonD = DAL.DALMaster.MasterData.GetJsonMasterSetting("CIP_VAT_PERCENT");
                Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
                PercentagesTaxBaseVAT = Decimal.Parse(json.GetValue("PercentagesTaxBaseVAT").ToString());
                PercentagesVAT = decimal.Parse(json.GetValue("PercentagesVAT").ToString());
            }
            catch (Exception)
            {
                PercentagesTaxBaseVAT = 0;
                PercentagesVAT = 0;
            }
        }
        public decimal PercentagesTaxBaseVAT {get;set; }
        public decimal PercentagesVAT {get;set; }
    }

    public class InsuranceRate
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}