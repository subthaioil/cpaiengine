﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CustomerViewModel
    {
        public CustomerViewModel_Seach cust_Search { get; set; }
        public CustomerViewModel_Detail cust_Detail { get; set; }

        public List<SelectListItem> ddl_Country { get; set; }
        public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_Nation { get; set; }
        public List<SelectListItem> ddl_Broker { get; set; }

        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_Type { get; set; }
        public List<SelectListItem> ddl_Color { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }

        public string cust_SystemJSON { get; set; }
        public string cust_TypeJSON { get; set; }
    }

    public class CustomerViewModel_Seach
    {
        public string sCustCode { get; set; }
        public string sCustName { get; set; }
        public string sCompany { get; set; }
        public string sNation { get; set; }
        public string sCountry { get; set; }
        public string sCreateType { get; set; }
        
        public string sSystem { get; set; }
        public string sType { get; set; }
        public string sStatus { get; set; }

        public List<CustomerViewModel_SeachData> sSearchData { get; set; }
    }

    public class CustomerViewModel_SeachData
    {
        public string dCustCtrlID { get; set; } // Not Display
        public string dCustCode { get; set; }
        public string dCustName { get; set; }
        public string dCompany { get; set; }
        public string dCompanyName { get; set; }
        public string dNation { get; set; }
        public string dCountry { get; set; }
        public string dCreateType { get; set; }

        public string dSystem { get; set; }
        public string dType { get; set; }
        public string dStatus { get; set; }
    }

    public class CustomerViewModel_Detail
    {
        public string CreateType { get; set; }

        public string CustDetailID { get; set; } // Not Display
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string Company { get; set; }
        public string Nation { get; set; }
        public string CountryKey { get; set; }

        public string CustPaymentTermID { get; set; } // Not Display
        public string PaymentTerm { get; set; }
        public string AddressCommission { get; set; }
        public string BrokerName { get; set; }
        public string BrokerCommission { get; set; }
        public string WithodingTax { get; set; }

        public List<CustomerViewModel_Control> Control { get; set; }
        public List<CustomerViewModel_Broker> Broker { get; set; }
    }

    public class CustomerViewModel_Control
    {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string System { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }
        public string Status { get; set; }
    }

    public class CustomerViewModel_Broker {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string Vendor { get; set; }
        public string Status { get; set; }
        public string Default { get; set; }
    }
}