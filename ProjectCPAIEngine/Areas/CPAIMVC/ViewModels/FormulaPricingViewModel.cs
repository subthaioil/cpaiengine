﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class FormulaPricingViewModel
    {
        public FormulaPricingViewModel_Search for_Search { get; set; }
        public FormulaPricingViewModel_Detail for_Detail { get; set; }

        public List<SelectListItem> ddl_Material { get; set; }
        public List<SelectListItem> ddl_PriceStatus { get; set; }
        public List<SelectListItem> ddl_PriceStructure { get; set; }
        public List<SelectListItem> ddl_PriceVariable { get; set; }
        public List<SelectListItem> ddl_PriceType { get; set; }
        public List<SelectListItem> ddl_PriceMOPS { get; set; }

    }


    public class FormulaPricingViewModel_Search
    {
        public string sFormulaName { get; set; }
        public string sMaterial { get; set; }
        public string sPriceStatus { get; set; }
        public List<FormulaPricingViewModel_SearchData> sSearchData { get; set; }
    }

    public class FormulaPricingViewModel_SearchData
    {
        public string dFormulaCode { get; set; }
        public string dFormulaName { get; set; }
        public string dMaterial { get; set; }
        public string dPriceStatus { get; set; }
    }

    public class FormulaPricingViewModel_Detail
    {
        public string FormulaCode { get; set; }
        public string FormulaName { get; set; }
        public string Material { get; set; }
        public string PriceStatus { get; set; }
        public List<FormulaPricingViewModel_DetailVariable> dDetailVariable { get; set; }
    }

    public class FormulaPricingViewModel_DetailVariable
    {
        public string fos_code { get; set; }
        public string PriceStructureCode { get; set; }
        public string PriceStructure { get; set; }
        public string VariableCode { get; set; }
        public string VariableDesc { get; set; }
        public string VariableDescKey { get; set; }
        public string Test { get; set; }
    }

    public class FormulaConditionItemList
    {
        public List<FormulaPricingCondition> ConditionItem { get; set; }
        //public FormulaTier TierList  { get; set; }
        public List<FormulaPricingTier> TierItem { get; set; }
        public List<ResultTemp> result { get; set; }
        public double MainValue { get; set; }
    }
    public class ResultTemp
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }

    //public class FormulaTier
    //{
    //    public List<FormulaPricingTier> TierItem { get; set; }

    //}

    public class FormulaPricingCondition
    {
        public string Symbol { get; set; }
        public string Variable1 { get; set; }
        public string Round1 { get; set; }
        public string Variable2 { get; set; }
        public string Round2 { get; set; }
        public string ResultOut { get; set; }
        public string ResultOut_Round { get; set; }

    }
    public class FormulaPricingTier
    {
        public string Return_Value { get; set; }
        public string Compare_name { get; set; }
        public List<TierCondition> Compare_with { get; set; }
        public List<FormulaPricingCondition> ConditionItem { get; set; }

    }
    public class TierCondition
    {
        public string Compare_Symbol { get; set; }
        public string Compare_Value { get; set; }
        
    }

    public class FormulaPricingDetail
    {
        public string MET_NUM { get; set; }
        public string PRICE_STATUS { get; set; }
        public string DAILY_1 { get; set; }
        public string DAILY_2 { get; set; }
        public string DAILY_3 { get; set; }
        public string DAILY_4 { get; set; }
        public string DAILY_5 { get; set; }
        public string DAILY_6 { get; set; }
        public string DAILY_7 { get; set; }
        public string MONTHLY_1 { get; set; }
        public string MONTHLY_2 { get; set; }
        public string MONTHLY_3 { get; set; }
        public string MONTHLY_4 { get; set; }
        public string MONTHLY_5 { get; set; }
        public string MONTHLY_6 { get; set; }
        public string MONTHLY_7 { get; set; }

        public string BP_PRICING_PERIOD_FROM { get; set; }
        public string BP_PRICING_PERIOD_TO { get; set; }
        public string OSP_PRICING_PERIOD { get; set; }
        public string BASE_PRICE { get; set; }
        public string BASE_PRICE_TEXT { get; set; }
        public string OSP_PREMIUM_DISCOUNT { get; set; }
        public string OSP_PREMIUM_DISCOUNT_TEXT { get; set; }
        public string TRADING_PREMIUM_DISCOUNT { get; set; }
        public string TRADING_PREMIUM_DISCOUNT_TEXT { get; set; }
        public string OTHER_COST { get; set; }
        public string OTHER_COST_TEXT { get; set; }
        public string BASE_PRICE_PERIOD { get; set; }
        public string TRADING_PRICE_PERIOD { get; set; }
        public string OTHER_PRICE_PERIOD { get; set; }
        
    }
    
}