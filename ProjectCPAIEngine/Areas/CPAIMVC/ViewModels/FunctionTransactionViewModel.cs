﻿
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class FnTranViewModel
    {
        public List<FN_TRANSACTION> FunctionTransaction { get; set; }

        public List<SelectListItem> lstlstStatus { get; set; }
        public List<SelectListItem> lstFunction { get; set; }
        public string CAppID { get; set; }
        public string CReqTranID { get; set; }
        public string CTranID { get; set; }
        public string CFunctionID { get; set; }
        public string CFromDate { get; set; }
        public string CToDate { get; set; }
        public string CStatus { get; set; }
        public string CIndex1 { get; set; }
        public string CIndex2 { get; set; }
        public string CIndex3 { get; set; }
        public string CIndex4 { get; set; }
        public string CIndex5 { get; set; }
        public string CIndex6 { get; set; }
        public string CIndex7 { get; set; }
        public string CIndex8 { get; set; }
        public string CIndex9 { get; set; }
        public string CIndex10 { get; set; }
        public string CIndex11 { get; set; }
        public string CIndex12 { get; set; }
        public string CIndex13 { get; set; }
        public string CIndex14 { get; set; }
        public string CIndex15 { get; set; }
        public string CIndex16 { get; set; }
        public string CIndex17 { get; set; }
        public string CIndex18 { get; set; }
        public string CIndex19 { get; set; }
        public string CIndex20 { get; set; }
    }

    public class FnTranDetail
    {
        public FN_TRANSACTION FnTranItemDetail { get; set; }
        public List<FN_Activity> ActivityTran { get; set; }
        public List<FN_LOG> TranLog { get; set; }
        public List<ExthenVal> ExtenTran { get; set; }    
        public string EventMSG { get; set; }    
    }

    public class FN_TRANSACTION
    {
        public string FTX_ACCUM_RETRY { get; set; }
        public string FTX_APP_ID { get; set; }
        public string FTX_CREATED { get; set; }
        public string FTX_CREATED_BY { get; set; }
        public string FTX_CURRENT_CODE { get; set; }
        public string FTX_CURRENT_DESC { get; set; }
        public string FTX_CURRENT_NAME_SPACE { get; set; }
        public string FTX_CURRENT_STATE { get; set; }
        public string FTX_FK_FUNCTION { get; set; }
        public string FTX_FK_FUNCTIONNAME { get; set; }
        public string FTX_FWD_RESP_CODE_FLAG { get; set; }
        public string FTX_INDEX1 { get; set; }
        public string FTX_INDEX10 { get; set; }
        public string FTX_INDEX11 { get; set; }
        public string FTX_INDEX12 { get; set; }
        public string FTX_INDEX13 { get; set; }
        public string FTX_INDEX14 { get; set; }
        public string FTX_INDEX15 { get; set; }
        public string FTX_INDEX16 { get; set; }
        public string FTX_INDEX17 { get; set; }
        public string FTX_INDEX18 { get; set; }
        public string FTX_INDEX19 { get; set; }
        public string FTX_INDEX2 { get; set; }
        public string FTX_INDEX20 { get; set; }
        public string FTX_INDEX3 { get; set; }
        public string FTX_INDEX4 { get; set; }
        public string FTX_INDEX5 { get; set; }
        public string FTX_INDEX6 { get; set; }
        public string FTX_INDEX7 { get; set; }
        public string FTX_INDEX8 { get; set; }
        public string FTX_INDEX9 { get; set; }
        public string FTX_NEXT_STATE { get; set; }
        public string FTX_PARENT_ACT_ID { get; set; }
        public string FTX_PARENT_TRX_ID { get; set; }
        public DateTime FTX_REQUEST_DATE { get; set; }
        public string FTX_REQ_TRANS { get; set; }
        public DateTime FTX_RESPONSE_DATE { get; set; }
        public string FTX_RETRY_COUNT { get; set; }
        public string FTX_RETURN_CODE { get; set; }
        public string FTX_RETURN_DESC { get; set; }
        public string FTX_RETURN_MESSAGE { get; set; }
        public string FTX_RETURN_NAME_SPACE { get; set; }
        public string FTX_RETURN_STATUS { get; set; }
        public string FTX_ROW_ID { get; set; }
        public string FTX_STATE_ACTION { get; set; }
        public string FTX_TRANS_ID { get; set; }
        public DateTime FTX_UPDATED { get; set; }
        public string FTX_UPDATED_BY { get; set; }
        public DateTime? FTX_WAKEUP { get; set; }
    }

    public class ExthenVal
    {
        public string ExtenParam { get; set; }
        public string ExtenVal { get; set; }
    }

    public class FN_Activity
    {
        public string FAC_CLASS { get; set; }
        public string FAC_CREATED { get; set; }
        public string FAC_CREATED_BY { get; set; }
        public string FAC_FK_FUNC_TRX { get; set; }
        public string FAC_NAME_SPACE { get; set; }
        public string FAC_NO_REPORT_FLAG { get; set; }
        public string FAC_PARALLEL_FLAG { get; set; }
        public string FAC_RECEIVE_DATE { get; set; }
        public string FAC_RETURN_CODE { get; set; }
        public string FAC_RETURN_DESC { get; set; }
        public string FAC_ROW_ID { get; set; }
        public string FAC_SEND_DATE { get; set; }
        public string FAC_STATE_NAME { get; set; }
        public string FAC_UPDATED { get; set; }
        public string FAC_UPDATED_BY { get; set; }
    }

    public class FN_LOG
    {
        public string TXL_CREATED { get; set; }
        public string TXL_CREATED_BY { get; set; }
        public string TXL_DATA { get; set; }
        public string TXL_DATA_DATE { get; set; }
        public string TXL_DATA_TYPE { get; set; }
        public string TXL_ENCRYPT_FLAG { get; set; }
        public string TXL_GROUP { get; set; }
        public string TXL_ROW_ID { get; set; }
        public string TXL_TABLE { get; set; }
        public string TXL_TABLE_ROW_ID { get; set; }
        public string TXL_UPDATED { get; set; }
        public string TXL_UPDATED_BY { get; set; }
    }
}