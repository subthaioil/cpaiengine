﻿using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class GraphBoardViewModel
    {
        public List<GraphBoardFromDate> GraphDate { get; set; }
        public List<SelectListItem> lstVoyNo { get; set; }
        public List<SelectListItem> lstTD { get; set; }
        public List<SelectListItem> lstSTATUS { get; set; }
        public List<SelectListItem> lstVessel { get; set; }
        public List<SelectListItem> lstVesselSearch { get; set; }
        public List<SelectListItem> lstCharterIn { get; set; }
        public List<string> Xdesx { get; set; }
        public List<TDData> TDData { get; set; }
        public string ActionMSG { get; set; }
        public string MinYAxis { get; set; }
        public string datePurchaseField { get; set; }
        public string selectTD { get; set; }

        public string TotalAvg { get; set; }
        public string TotalSum { get; set; }


    }
    public class GraphBoardFromDate
    {
        public bool showInLegend { get; set; }
        public string name { get; set; }
        public string color { get; set; }
        public bool visible { get; set; }
        public Marker marker { get; set; }
        public Tooltip tooltip { get; set; }    
        public List<List<double?>> data { get; set; }
    }
  
    public class Tooltip
    {
        public string headerFormat { get; set; }
        public string pointFormat { get; set; }
    }


public class Marker
    {
        public string symbol { get; set; }
        public bool enabled { get; set; }
        public string fillColor { get; set; }
        public int lineWidth { get; set; }
        public string lineColor { get; set; }
        public string color { get; set; }
    }
    public class TDData
    {
        public string Status { get; set; }
        public string chi_data_id { get; set; }
        public string DateTD { get; set; }
        public string TD2 { get; set; }
        public string TD3 { get; set; }
        public string TD15 { get; set; }
        public string TOPFix { get; set; }
        public string ShipName { get; set; }
        public string ShipBroker { get; set; }
        public string ShipOwner { get; set; }
        public string Dem { get; set; }
        public string Flatrate { get; set; }
        public string Deviation { get; set; }
        public string LaycanLoad { get; set; }
        public string Target20Day { get; set; }
        public string AvgWS3day { get; set; }
        public string WSSaveing { get; set; }
        public string SaveIn { get; set; }
        public string ParamVal { get; set; }
        public string DateOrder { get; set; }
        public string TD { get; set; }
    }

    public class TCEMKData
    {
        public string Status { get; set; }
        public string TranID { get; set; }
        public string purchase_no { get; set; }
        public string TceDate { get; set; }
        public string TD { get; set; }
        public string VesselName { get; set; }
        public string VoyNo { get; set; }
        public string Month { get; set; }
        public string TCRate { get; set; }
        public string OperatingDay { get; set; }
        public string FOMT { get; set; }
        public string GOMT { get; set; }
        public string TimeCharterCost { get; set; }
        public List<TceMkLoadPort> PortCharge { get; set; }
        public List<TceMkDisPort> DischangePort { get; set; }
        public string BunkerUseFO { get; set; }
        public string BunkerUseGO { get; set; }
        public string totalfrieghtCost { get; set; }
        public string FlatRate { get; set; }
        public string WSBITR { get; set; }
        public string TotalMarketCost { get; set; }
        public string Saving_LossFromMarket { get; set; }
        public string CharterOut { get; set; }
        public string SpotVariableCost { get; set; }
        public string Demurrage { get; set; }
        public string TCEBenefitDay { get; set; }
        public string TCEBenefitVoy { get; set; }
        public string TCEDay { get; set; }
        public string CreateBy { get; set; }
        public string ParamVal { get; set; }
        public string DateOrder { get; set; }
    }


    public class FixtureViewModel
    {
        public bool PageMode { get; set; }
        public string Action { get; set; }
        public bool GenButton { get; set; }
        public List<SelectListItem> lstVoyNo { get; set; }
        public List<SelectListItem> lstTD { get; set; }
        public List<SelectListItem> lstShipName { get; set; }
        public List<SelectListItem> lstShipBroker { get; set; }
        public TceDetail TceDetail { get; set; }
        public string chi_data_no { get; set; }
        public string sOnSubjectDate { get; set; }
        public string FixtureStatus { get; set; }

    }
    
}