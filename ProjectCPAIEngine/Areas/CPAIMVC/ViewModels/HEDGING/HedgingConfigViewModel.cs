﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingConfigViewModel
    {
        public List<SelectListItem> ddlCreditRate {get;set;}

        public string reason { get; set; }
        public List<HedgingConfig_NewData> HedgingConfig_NewData { get; set; }
        public List<HedgingConfig_OldData> HedgingConfig_OldData { get; set;}
    }

    public class HedgingConfig_OldData
    {
        public string oRowID { get; set; }
        public string oName { get; set; }
        public string oValue { get; set; }
        public string oDesc { get; set; }
    }

    public class HedgingConfig_NewData
    {
        public string nRowID { get; set; }
        public string nName { get; set; }
        public string nValue { get; set; }
        public string nDesc { get; set; }
        public string IsChange { get; set; }
    }
}