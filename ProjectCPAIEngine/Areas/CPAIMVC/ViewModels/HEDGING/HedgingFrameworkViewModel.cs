﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingFrameworkViewModel
    {
        public List<SelectListItem> ddlUnderlying { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }

        public List<SelectListItem> ddlHegingType { get; set; }
        public List<SelectListItem> ddlType { get; set; }
        public List<SelectListItem> ddlValueType { get; set; }
        public List<SelectListItem> ddlVolumeUnit { get; set; }

        public string pageMode { get; set; }

        public HedgingFrameworkViewModel_Search hedgingFramework_Search { get; set; }
        public HedgingFrameworkViewModel_Detail hedgingFramework_Detail { get; set; }        
    }

    public class HedgingFrameworkViewModel_Search
    {
        public string sOrder { get; set; }
        public string sActiveDate { get; set; }
        public string sUnderlying { get; set; }
        public string sStatus { get; set; }
        
        public List<HedgingFrameworkViewModel_SearchData> sSearchData { get; set; }
    }

    public class HedgingFrameworkViewModel_SearchData
    {
        public string dOrderID { get; set; }
        public string dActiveDate { get; set; }
        public string dUnderlying { get; set; }
        public string dStatus { get; set; }
        public string dDescription { get; set; }
        public string dCreateDate { get; set; }
        public string dCreateBy { get; set; }
        public string dCopy { get; set; }        
    }

    public class HedgingFrameworkViewModel_Detail
    {
        public string OrderID { get; set; }
        public string ActiveDate { get; set; }
        public string Underlying { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string CancelReamark { get; set; }
        public string IsChange { get; set; }

        public List<HedgingFrameworkViewModel_DetailData> DetailData { get; set; }
    }

    public class HedgingFrameworkViewModel_DetailData
    {        
        public string RowID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Order { get; set; }
        public string HedgeType { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string ValueType { get; set; }
        public string Volume { get; set; }
        public string VolumeUnit { get; set; }
        public string Price1 { get; set; }
        public string Price1Remark { get; set; }
        public string Price2 { get; set; }
        public string Price2Remark { get; set; }
        public string Price3 { get; set; }
        public string Price3Remark { get; set; }
        public string PriceUnit { get; set; }
        public string CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}
