﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingManagementCounterpartyViewModel
    {
        public List<SelectListItem> ddlCreditRate { get; set; }
        public List<SelectListItem> ddlActive { get; set; }
        public List<SelectListItem> ddlCounterParty { get; set; }
        public List<SelectListItem> ddlCreditRating { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }

        public HedgingManagementCounterpartyViewModel_Search Search { get; set; }
        public HedgingManagementCounterpartyViewModel_Detail HMC_Detail { get; set; }

        public string pageMode { get; set; }
    }

    public class HedgingManagementCounterpartyViewModel_Search
    {
        public string sCouterParty { get; set; }
        public string sCreditRating { get; set; }
        public string sStatus { get; set; }

        public List<HedgingManagementCounterpartyViewModel_Detail> Detail { get; set; }
    }

    #region Detail
    public class HedgingManagementCounterpartyViewModel_Detail
    {
        public string HMC_ROW_ID { get; set; }//HMC_ROW_ID
        public string CouterPartyID { get; set; }//HMC_FK_VENDOR
        public string CouterPartyName { get; set; }//HMC_FK_VENDOR
        public string FileID { get; set; }//HMC_FK_HEDG_CDS_FILE
        public string CreditRating { get; set; }//HMC_CREDIT_RATING
        public string CreditLimit { get; set; }//HMC_CREDIT_LIMIT
        public string Status { get; set; }//HMC_STATUS
        public string Reason { get; set; }//HMC_REASON
        public string Log_Reason { get; set; }
        public string SCDS { get; set; }
        public string CreateDate { get; set; }//HMC_CREATED_DATE
        public string CreateBy { get; set; }//HMC_CREATED_BY
        public string UpdateDate { get; set; }//HMC_UPDATED_DATE
        public string UpdateBy { get; set; }//HMC_UPDATED_BY

        public List<HedgingManagementCounterpartyViewModel_DetailCds> DetailCds { get; set; }
        public List<HedgingManagementCounterpartyViewModel_DetailCreditLimit> DetailCreditLimit { get; set; }
        public List<HedgingManagementCounterpartyViewModel_DetailStatus> DetailStatus { get; set; }
    }

    public class HedgingManagementCounterpartyViewModel_DetailCds
    {
        public string HMCD_ROW_ID { get; set; }//HMCD_ROW_ID
        public string CouterPartyID { get; set; }//HMCD_FK_HEDG_CP
        public string FileID { get; set; }//HMCD_FK_HEDG_CDS_FILE
        public string Date { get; set; }//HMCD_DATE
        public string Cds { get; set; }//HMCD_CDS
        public string Status { get; set; }//HMCD_STATUS
        public string CreateDate { get; set; }//HMC_CREATED_DATE
        public string CreateBy { get; set; }//HMC_CREATED_BY
        public string UpdateDate { get; set; }//HMC_UPDATED_DATE
        public string UpdateBy { get; set; }//HMC_UPDATED_BY
    }

    public class HedgingManagementCounterpartyViewModel_DetailCreditLimit
    {
        public string HMCL_ROW_ID { get; set; }//HMCL_ROW_ID
        public string CouterPartyID { get; set; }//HMCL_FK_HEDG_CP
        public string DateStart { get; set; }//HMCL_DATE_START
        public string DateEnd { get; set; }//HMCL_DATE_END
        public string CreditLimit { get; set; }//HMCL_CREDIT_LIMIT
        public string Status { get; set; }//HMCL_STATUS
        public string CreateDate { get; set; }//HMCL_CREATED_DATE
        public string CreateBy { get; set; }//HMCL_CREATED_BY
        public string UpdateDate { get; set; }//HMCL_UPDATED_DATE
        public string UpdateBy { get; set; }//HMCL_UPDATED_BY

        public string dDate { get; set; }//HMCL_DATE_START+HMCL_DATE_END
    }

    public class HedgingManagementCounterpartyViewModel_DetailStatus
    {
        public string HMCS_ROW_ID { get; set; }//HMCS_ROW_ID
        public string CouterPartyID { get; set; }//HMCS_FK_HEDG_CP
        public string Status { get; set; }//HMCS_STATUS
        public string Reason { get; set; }//HMCS_REASON
        public string CreateDate { get; set; }//HMCS_CREATED_DATE
        public string CreateBy { get; set; }//HMCS_CREATED_BY
        public string UpdateDate { get; set; }//HMCS_CREATED_BY
        public string UpdateBy { get; set; }//HMCS_UPDATED_BY
    }
    #endregion

    public class HedgingManagementCounterpartyViewModel_CdsMax
    {
        public string VendorID { get; set; }
        public string Date { get; set; }
    }

    public class CounterPartyTemp
    {
        public string VendorID { get; set; }
        public string Name { get; set; }
        public string Rating { get; set; }
        public string Date { get; set; }
        public string Value { get; set; }
        public string Count { get; set; }
    }
}
