﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingSteeringCommitteeViewModel
    {
        public List<SelectListItem> ddlUnderlying { get; set; }
        public List<SelectListItem> ddlCropPlan { get; set; }
        public List<SelectListItem> ddlActiveDate_FromMonth { get; set; }
        public List<SelectListItem> ddlActiveDate_FromYear { get; set; }
        public List<SelectListItem> ddlActiveDate_ToMonth { get; set; }
        public List<SelectListItem> ddlActiveDate_ToYear { get; set; }

        public HedgingSteeringCommitteeViewModel_Search search { get; set; }
        public HedgingSteeringCommitteeViewModel_DetailList detailList { get; set; }
    }

    public class HedgingSteeringCommitteeViewModel_Search
    {
        public string sOrderID { get; set; }
        public string sUnderlying { get; set; }
        public string sCropPlan { get; set; }
        public string sActiveDate { get; set; }
        public string sActiveDate_FromMonth { get; set; }
        public string sActiveDate_FromYear { get; set; }
        public string sActiveDate_ToMonth { get; set; }
        public string sActiveDate_ToYear { get; set; }

        public List<HedgingSteeringCommitteeViewModel_SearchData> SearchData { get; set; }
    }

    public class HedgingSteeringCommitteeViewModel_SearchData
    {
        public string dOrderID { get; set; }
        public string dUnderlying { get; set; }
        public string dUnderlyingName { get; set; }
        public string dCropPlan { get; set; }        
        public string dProduction { get; set; }
        public string dUnitPrice { get; set; }
        public string dUnitVolume { get; set; }
        public string dActiveDate { get; set; }

        public List<HedgingSteeringCommitteeViewModel_DetailData> DetailData { get; set; }
    }

    public class HedgingSteeringCommitteeViewModel_DetailList
    {
        public string _pageMode { get; set; }
        public string _funcID { get; set; }
        public string _rowID { get; set; }

        public string Underlying { get; set; }
        public string CropPlan { get; set; }
        public string UnitPrice { get; set; }
        public string UnitVolume { get; set; }

        public List<HedgingSteeringCommitteeViewModel_Detail> detail { get; set; }
    }

    public class HedgingSteeringCommitteeViewModel_Detail
    {
        public string OrderID { get; set; }         
        public string Production { get; set; }
        public string UnitPrice { get; set; }
        public string UnitVolume { get; set; }
        public string ActiveDate { get; set; }
        public string ActiveDate_FromMonth { get; set; }
        public string ActiveDate_FromYear { get; set; }
        public string ActiveDate_ToMonth { get; set; }
        public string ActiveDate_ToYear { get; set; }

        public List<HedgingSteeringCommitteeViewModel_DetailData> DetailData { get; set; }
    }

    public class HedgingSteeringCommitteeViewModel_DetailData
    {
        public string OrderID { get; set; }
        public string RowID { get; set; }
        public string Order { get; set; }
        public string Price { get; set; }
        public string Volume { get; set; }
    }
}
