﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingTicketViewModel
    {
        public HedgingTicketViewModel_Search HedgingTicket_Search { get; set; }

        // JSON Engine 
        public TradingTicket trading_ticket { get; set; }
        public DealDetail deal_detail { get; set; }
        // -----

        public DealDetail oldDeal_detail { get; set; }
        public HedgingTarget hedging_target { get; set; }       
        public SearchDeal search_deal { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }
        public List<SelectListItem> ddlDealType { get; set; }
        public List<SelectListItem> ddlTool { get; set; }
        public List<SelectListItem> ddlUnderlying { get; set; }
        public List<SelectListItem> ddlVS { get; set; }
        public List<SelectListItem> ddlCounterParty { get; set; }
        public List<string> dealList = new List<string>();

        public string systemType { get; set; }
        public string taskID { get; set; }
        public string reqID { get; set; }
        public string tranID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public string reason { get; set; }
        public string action { get; set; }
        public string next_status { get; set; } 
        public string system { get; set; } 

        public string htmlPDF { get; set; }

        public HedgingTicketViewModel()
        {
            search_deal = new SearchDeal();
            deal_detail = new DealDetail();
            deal_detail.hedging_execution = new HedgingExecution();
            deal_detail.hedging_target = new List<HedgingTarget>();
            deal_detail.hedging_chart = new HedgingChart();
            hedging_target = new HedgingTarget();
            trading_ticket = new TradingTicket();
            oldDeal_detail = new DealDetail();
            oldDeal_detail.hedging_execution = new HedgingExecution();
            oldDeal_detail.hedging_target = new List<HedgingTarget>();
            oldDeal_detail.hedging_chart = new HedgingChart();

            ddlStatus = DropdownServiceModel.getStatus(false, string.Empty, DropdownServiceModel.HedgeStatus.Ticket);
            ddlDealType = DropdownServiceModel.getDealType(false, string.Empty);
            ddlTool = DropdownServiceModel.getTool(false, string.Empty);
            ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            ddlVS = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            ddlCounterParty = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, "HG_CP_OTH");

            search_deal.deal_type = DropdownServiceModel.getDealType();
            search_deal.tool = DropdownServiceModel.getTool();
            search_deal.hegingType = DropdownServiceModel.getHedgeType(false, string.Empty);
            search_deal.underlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            search_deal.vS = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
        }

        public class SearchDeal
        {            
            public string deal_no { get; set; }
            public string deal_date { get; set; }
            public List<SelectListItem> deal_type { get; set; }
            public List<SelectListItem> tool { get; set; }
            public List<SelectListItem> underlying { get; set; }
            public List<SelectListItem> vS { get; set; }
            public List<SearchDealList> search_deal_list { get; set; }
            public List<SelectListItem> hegingType { get; set; }
            public string sDate_start { get; set; }
            public string sDate_to { get; set; }
            public string sDeal_type { get; set; }
            public string sTool { get; set; }
            public string sUnderlying { get; set; }
            public string sVS { get; set; }
            public string sHedgeType { get; set; }
        }

        public class SearchDealList
        {
            public string deal_id { get; set; }
            public string deal_no { get; set; }
            public string deal_date { get; set; }
            public string deal_type { get; set; }
            public string tool { get; set; }
            public string underlying { get; set; }
            public string annual_id { get; set; }
            public string hedging_type { get; set; }
            public string date_active { get; set; }
        }


        public class TradingTicket
        {
            public string ticket_no { get; set; }
            public string trader { get; set; }
            public string ticket_date { get; set; }
            public string status { get; set; }
            public string revision { get; set; }
            public string note { get; set; }
            public string reason { get; set; }
        }

        public class HedgingTarget
        {
            public string order { get; set; }
            public string tenor { get; set; }
            public string price { get; set; }
            public string volume { get; set; }
            public string remaining { get; set; }
        }

        public class HedgingChart
        {
            public string limit { get; set; }
            public string target { get; set; }
            public string hedged_vol { get; set; }
        }

        public class BidPrice
        {
            public string order { get; set; }
            public string counter_parties_id { get; set; }
            public string counter_parties_name { get; set; }
            public string price { get; set; }
        }

        public class HedgingExecutionData
        {
            public string order { get; set; }
            public string tenor { get; set; }
            public string deal_no { get; set; }
            public string deal_id { get; set; }
            public string deal_date { get; set; }
            public string counter_parties_id { get; set; }
            public string counter_parties_name { get; set; }
            public string price { get; set; }
            public string volume { get; set; }
            public List<BidPrice> bid_price { get; set; }
            public string record_status { get; set; }
            public string deal_status { get; set; }
        }

        public class HedgingPositionData
        {
            public string order { get; set; }
            public string tenor { get; set; }
            public string price { get; set; }
            public string volume { get; set; }
        }

        public class HedgingPosition
        {
            public string price_unit { get; set; }
            public string volume_unit { get; set; }
            public List<HedgingPositionData> hedging_position_data { get; set; }
        }

        public class HedgingExecution
        {
            public string price_unit { get; set; }
            public string volume_mnt_unit { get; set; }
            public List<HedgingExecutionData> hedging_execution_data { get; set; }
            public HedgingPosition hedging_position { get; set; }
        }

        public class DealDetail
        {
            public string deal_type { get; set; }
            public string underlying_id { get; set; }
            public string underlying_name { get; set; }
            public string underlying_vs_id { get; set; }
            public string underlying_vs_name { get; set; }
            public string tool_id { get; set; }
            public string tool_name { get; set; }
            public string hedg_type_name { get; set; }
            public string annual_detail_id { get; set; }
            public List<HedgingTarget> hedging_target { get; set; }
            public HedgingChart hedging_chart { get; set; }
            public HedgingExecution hedging_execution { get; set; }
            public string[] removed_deals { get; set; }
        } 

    }

    public class HedgingTicketViewModel_Search
    {
        public string systemType { get; set; }

        public string sTicketNo { get; set; }
        public string sTicketDate { get; set; }
        public string sDealNo { get; set; }
        public string sStatus { get; set; }
        public string sDealType { get; set; }
        public string sTool { get; set; }
        public string sUnderlying { get; set; }
        public string sVS { get; set; }
        public string sTrader { get; set; }
        public string sCounterParty { get; set; }
        public string sStatusSelected { get; set; }

        public List<HedgingTicketViewModel_SearchData> sSearchData { get; set; }        
    }

    public class HedgingTicketViewModel_SearchData
    {
        public string hTransactionIdEncrypted { get; set; }
        public string hTransactionRequestEncrypted { get; set; }
        public string hSystemEncrypted { get; set; }
        public string hTypeEncrypted { get; set; } 
        public string hTicketNoEncrypted { get; set; }
        
        public string dDate { get; set; }
        public string dStatus { get; set; }
        public string dTicketNo { get; set; }
        public string dDealNo { get; set; }
        public string dDealType { get; set; }
        public string dTool { get; set; }
        public string dUnderlying { get; set; }
        public string dTrader { get; set; }        

        public List<HedgingTicketViewModel_Detail> detail { get; set; }
    }

    public class HedgingTicketViewModel_Detail
    {
        public string hTransactionIdEncrypted { get; set; }
        public string hTransactionRequestEncrypted { get; set; }
        public string hSystemEncrypted { get; set; }
        public string hTypeEncrypted { get; set; } 
        public string hDealNoEncrypted { get; set; }

        public string ref_ticket_no { get; set; }
        public string deal_no { get; set; }
        public string counterParty { get; set; }
    }    
}