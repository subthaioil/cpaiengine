﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeAnnualViewModel
    {
        public static string FORMAT_DATE = "dd/MM/yyyy";
        public static string FORMAT_DATETIME = "dd/MM/yyyy HH:mm";

        // MAIN PROPERTIES //
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string note { get; set; }
        public string attach_file { get; set; }

        public string update_by { get; set; }
        public string update_date { get; set; }
        public string status { get; set; }
        public string version { get; set; }

        public HedgAnnualSubmitNote hedge_annual_submit_note { get; set; } = new HedgAnnualSubmitNote();
        public List<HedgeAnnualTypeViewModel> hedge_annual_types { get; set; } = new List<HedgeAnnualTypeViewModel>();

        // DROPDOWN LIST PROPERTIES //
        public List<SelectListItem> ddlTrading { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeType { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeProduct { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeUnitPrice { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeUnitVolume { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeMinMax { get; set; } = new List<SelectListItem>();

        // INTERFACE PROPERTIES //
        public string hedge_type { get; set; }
        public string action { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }

    }

    public class HedgeAnnualSubmitNoteViewModel {
        public string subject { get; set; }
        public string ref_no { get; set; }
        public string note { get; set; }
        public string attach_file { get; set; }
    }

    public class HedgeAnnualTypeViewModel {
        public string hedge_type { get; set; }
        public string hedge_type_name { get; set; }
        public string hedge_type_order { get; set; }
        public List<HedgeAnnualTypeDetailViewModel> hedge_annual_type_details { get; set; } = new List<HedgeAnnualTypeDetailViewModel>();
    }

    public class HedgeAnnualTypeDetailViewModel {
        public string key { get; set; }
        public string order { get; set; }
        public string product_front { get; set; }
        public string product_back { get; set; }
        public string title { get; set; }
        public string approved_date { get; set; }
        public string unit_price { get; set; }
        public string unit_volume { get; set; }
        public string year { get; set; }
        public string min_max { get; set; }
        public string price_year { get; set; }
        public string price_Q1 { get; set; }
        public string price_Q2 { get; set; }
        public string price_Q3 { get; set; }
        public string price_Q4 { get; set; }
        public string prod_volume { get; set; }
        public string limit_volume { get; set; }
        public string approved_volume { get; set; }
        public string note { get; set; }

        public string action { get; set; } = Utilities.ConstantPrm.ACTION.VIEW;
    }



    public class HedgeAnnualSearchViewModel
    {
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string update_date { get; set; }
        public string update_by { get; set; }
        public string status { get; set; }
        public List<HedgeAnnualViewModel> hedge_annuals { get; set; } = new List<HedgeAnnualViewModel>();
        public List<HedgeAnnualEncrypt> HedgeAnnualTransaction { get; set; }


        public List<SelectListItem> ddlTrading { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlStatus { get; set; } = new List<SelectListItem>();

    }

    public class HedgeAnnualHistoryViewModel
    {
        public string tran_id { get; set; }
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string version { get; set; }
        public string ref_no { get; set; }
        public string subject { get; set; }
        public string note { get; set; }
        public string update_date { get; set; }
        public string update_by { get; set; }
    }

    public class HedgeAnnualProductHistoryViewModel
    {
        public string product_front { get; set; }
        public string product_back { get; set; }
        public string title { get; set; }
        public string version { get; set; }
        public string approved_date { get; set; }
        public string unit_price { get; set; }
        public string unit_vol { get; set; }
        public string year { get; set; }
        public string min_max { get; set; }
        public string price_year { get; set; }
        public string price_q1 { get; set; }
        public string price_q2 { get; set; }
        public string price_q3 { get; set; }
        public string price_q4 { get; set; }
        public string prod_vol { get; set; }
        public string limit_vol { get; set; }
        public string approved_vol { get; set; }
        public string note { get; set; }
    }
    
}