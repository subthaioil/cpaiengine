﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeCDSImportViewModel
    {
        public HedgeCDSImportViewModel_Search cds_search { get; set; }
    }

    public class HedgeCDSImportViewModel_Search
    {
        public int sLimitRow { get; set; }
        public List<HedgeCDSImport_SearchData> sSearchData { get; set; }
    }

    public class HedgeCDSImport_SearchData
    {
        public string dID { get; set; }
        public string dFileName { get; set; }
        public string dFilePath { get; set; }
        public string dResult { get; set; }
        public string dNote { get; set; }
        public DateTime? dImportDate { get; set; }
        public string dImportBy { get; set; }
        public DateTime? dUpdateDate { get; set; }
        public string dUpdateBy { get; set; }

        public string dImportDateString { get; set; }
    }
}