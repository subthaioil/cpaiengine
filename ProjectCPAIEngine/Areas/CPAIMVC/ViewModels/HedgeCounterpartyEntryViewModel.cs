﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeCounterpartyEntryViewModel
    {
        public HedgeCounterpartyEntry_Detail cpe_Detail { get; set; }
        public History cpe_History { get; set; }
        public HedgeCounterpartyEntry_Search cpe_search { get; set; }

        public List<SelectListItem> ddl_counterparty { get; set; }
        public List<SelectListItem> ddl_creditRating { get; set; }
        public List<SelectListItem> ddl_countDate { get; set; }
        public List<SelectListItem> ddl_MasterDDL { get; set; }
        public List<SelectListItem> ddl_CounterpartyMapping { get; set; }
    }



    public class HedgeCounterpartyEntry_Detail
    {
        public string pageMode { get; set; }
        public string row_id { get; set; }
        public string counterParty { get; set; }
        public string statustrading { get; set; }
        public string counterPartyName { get; set; }
        public string counterPartyFullName { get; set; }
        public string mappingCDS { get; set; }
        public string latestCDS { get; set; }
        public string latestCDS_value { get; set; }
        public string creditRating { get; set; }
        public string creditRating_NAFlag { get; set; }
        public string creditRating_NAFlag_h { get; set; }
        public string refer_CreditRating { get; set; }
        public string creditLimit { get; set; }
        public string creditLimit_UpdateFlag { get; set; }
        public string paymentDay { get; set;}
        public string countDate { get; set; }
        public string roundingDecimal { get; set; }
        public string status_roundingDecimal { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public string version { get; set; }
       
        public List<Credit_Limit_Specify_Period> creditLimitSpecifyPeriod { get; set; }
        public List<CDS> cds { get; set; }
        public List<History> history { get; set; }
    }


    public class HedgeCounterpartyEntry_Search
    {
        public string scpeCounterparty { get; set; }
        public string scpeCreditRating { get; set; }
        public string scpeCreditLimit { get; set; }
        public string scpeCDS { get; set; }
        public string scpeStatus { get; set; }

        public List<HedgeCounterpartyEntry_SearchData> searchData { get; set; }
    }


    public class Credit_Limit_Specify_Period
    {       
        public string credit_limit_code { get; set; }
        public string period { get; set; }
        public string credit_amount { get; set; }
        public string note { get; set; }
        public string updated_date { get; set; }
        public string updated_by { get; set; }
        public string status { get; set; }
    }

    public class CDS
    {
        public string date { get; set; }
        public string value { get; set; }
        public string updated_by { get; set; }
        public string updated_date { get; set; }
    }

    public class History
    {
        public string history_id { get; set; }
        public string version { get; set; }
        public string by { get; set; }
        public string date { get; set; }
        public string reason { get; set; }
        public List<History_Log> history_log { get; set; }

        //public List<History_Log> history_log { get; set; } = new List<History_Log>();
    }

    public class History_Log
    {
        public string order { get; set; }
        public string title { get; set; }
        public string action { get; set; }
        public string history_note { get; set; }
    }


    public class HedgeCounterpartyEntry_SearchData
    {
        public string dEntryCode { get; set; }
        public string dCounterparty { get; set; }
        public string dCreditRatingId { get; set; }
        public string dCreditRating { get; set; }
        public string dCreditLimit { get; set; }
        public string dCDS { get; set; }
        public string dStatusTrad { get; set; }
        public string dStatus { get; set; }
        public string dReason { get; set; }
        public DateTime? dUpdatedDate { get; set; }
        public string dUpdatedBy { get; set; }
    }
}