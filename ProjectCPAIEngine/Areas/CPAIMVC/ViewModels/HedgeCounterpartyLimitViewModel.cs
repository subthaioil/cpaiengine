﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeCounterpartyLimitViewModel
    {
        public string ID { get; set; }
        public string CreditRatingID { get; set; }
        public string CreditRatingDesc { get; set; }
        public string SptradePercent { get; set; }
        public string SptradePercentDesc { get; set; }
        public string LimitOverPercent { get; set; }
        public string LimitOverPercentDesc { get; set; }
        public string LimitOverPercentEmail { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public List<CounterpartyCDS> CounterpartyCDS { get; set; }
        public List<string> oldSelectList { get; set; }
        public List<SearchEmailData> EmailList { get; set; }

        public List<SelectListItem> ddlCreditRating { get; set; }
        public List<SelectListItem> ddlCdsType { get; set; }
        public List<SelectListItem> ddlCdsAction { get; set; }

        public string UserGroup { get; set; }
        public string UserSystem { get; set; }
        public string UserFullNameEN { get; set; }
        public string UserFullNameTH { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public List<SelectListItem> ddlUserGroup { get; set; }
        public List<SelectListItem> ddlSystem { get; set; }

        public HedgeCounterpartyLimitViewModel()
        {
            oldSelectList = new List<string>();
        }
    }

    public class CounterpartyCDS
    {
        public string ID { get; set; }
        public string Order { get; set; }
        public string CreditLimitID { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Day { get; set; }
        public string Action { get; set; }
        public string Desc { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }

    public class SearchEmailData
    {
        public string UserGroup { get; set; }
        public string UserSystem { get; set; }
        public string UserFullNameEN { get; set; }
        public string UserFullNameTH { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string Select { get; set; }
    }
}