﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeFormularViewModel
    {
        public List<Formular> Formular_Detail { get; set; }
        public List<SelectListItem> ddl_MasterDDL { get; set; }
        public List<SelectListItem> ddl_Operation { get; set; }
        public List<SelectListItem> ddl_Variable { get; set; }
    }

    public class Formular
    {
        public string Row_Id { get; set; }
        public string OrderFormular { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Formular_Expression> Formular_Exp { get; set; } = new List<Formular_Expression>();
        public string Expression { get; set; }
        public string Status { get; set; }
    }


    public class Formular_Expression
    {
        public string Row_idEX { get; set; }
        public string Formular_IdEX { get; set; }
        public string OrderEX { get; set; }
        public string TypeEX { get; set; }
        public string ValueEx { get; set; }
    }
}