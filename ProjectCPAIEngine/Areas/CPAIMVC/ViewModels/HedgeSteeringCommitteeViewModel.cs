﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeSteeringCommitteeSearchViewModel
    {
        public string reference_no { get; set; }
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string hedge_type { get; set; }
        public string intrument { get; set; }
        public string exceed_annual_fw { get; set; }
        public string tenor_from { get; set; }
        public string tenor_to { get; set; }
        public string package_name { get; set; }
        public string update_date { get; set; }
        public string update_by { get; set; }
        public string status { get; set; }

        public List<HedgeSteeringCommitteeEncrypt> HedgeSteeringCommitteeTransaction { get; set; } = new List<HedgeSteeringCommitteeEncrypt>();
        public List<SelectListItem> ddlTrading { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeType { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlStatus { get; set; } = new List<SelectListItem>();

    }

    public class HedgeSteeringCommitteeViewModel
    {
        public static string FORMAT_DATE = "dd/MM/yyyy";
        public static string FORMAT_DATETIME = "dd/MM/yyyy HH:mm";

        public string reference_no { get; set; }
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string tenor_from { get; set; }
        public string tenor_to { get; set; }
        public string template { get; set; }        // NORMAL|TERMINATE|RESTRUCTURE

        public string status { get; set; }          // ACTIVE|INACTIVE 
        public string hedge_type { get; set; }      
        public string m1_m { get; set; }          // M-1|M
        public string time_spread { get; set; }     // Y|N
        public string consecutive { get; set; }     // Y|N
        public string total_volume { get; set; }
        public string unit_price { get; set; }
        public string unit_volume { get; set; }

        public string note { get; set; }
        public string attach_file { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public HedgeSteeringCommitteeChoiceViewModel hedge_steering_committee_main_choice { get; set; } = new HedgeSteeringCommitteeChoiceViewModel();
        public HedgeSteeringCommitteeChoiceViewModel hedge_steering_committee_extend_choice { get; set; } = new HedgeSteeringCommitteeChoiceViewModel();
        public HedgeSteeringCommitteeToolViewModel hedge_steering_committee_tool { get; set; } = new HedgeSteeringCommitteeToolViewModel();
        public HedgeSteeringCommitteeSubmitNoteViewModel hedge_steering_committee_submit_note { get; set; } = new HedgeSteeringCommitteeSubmitNoteViewModel();

        public List<SelectListItem> ddlHedgeType { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlTrading { get; set; } = new List<SelectListItem>();
    }

    public class HedgeSteeringCommitteeSubmitNoteViewModel
    {
        public string subject { get; set; }
        public string approved_date { get; set; }
        public string ref_no { get; set; }
        public string note { get; set; }
        public string attach_file { get; set; }
    }

    public class HedgeSteeringCommitteeChoiceViewModel
    {
        public string hedge_steering_committee_choice_detail_name { get; set; }
        public string unit_price { get; set; }
        public string unit_volume { get; set; }
        public string main_volume { get; set; }
        public List<HedgeSteeringCommitteeChoiceDetailViewModel> hedge_steering_committee_choice_details { get; set; } = new List<HedgeSteeringCommitteeChoiceDetailViewModel>();

        public List<SelectListItem> ddlHedgeTitle { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeProduct { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeUnitPrice { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeUnitVolume { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeMinMax { get; set; } = new List<SelectListItem>();
    }

    public class HedgeSteeringCommitteeChoiceDetailViewModel {
        public string name { get; set; }
        public string order { get; set; }

        public string package_name { get; set; } 
        public string min_max { get; set; }            // BUY|SELL
        public string underlying_type { get; set; } = "N"; // NONE|SUBTRACT|DIVIDE
        public string avg_af_price_primary { get; set; }
        public string avg_af_price_secondary { get; set; }
        public string net_af_price { get; set; }

        public List<HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel> hedge_steering_committee_choice_underlying_primaries { get; set; } = new List<HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel>();
        public List<HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel> hedge_steering_committee_choice_underlying_secondaries { get; set; } = new List<HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel>();

        public List<SelectListItem> ddlMinMax { get; set; } = new List<SelectListItem>();
    }

    public class HedgeSteeringCommitteeChoiceDetailUnderlyingViewModel
    {
        public string key { get; set; }
        public string order { get; set; }
        public string af_frame { get; set; }
        public string product_front { get; set; }
        public string product_back { get; set; }
        public string weight { get; set; }
        public string weight_volume { get; set; }
        public string af_price { get; set; }
        public string unit_price { get; set; }
        public string unit_volume { get; set; }
        public string min_max { get; set; }

        public string action { get; set; }
    }

    public class HedgeSteeringCommitteeToolViewModel {
        public string hedge_steering_committee_tool_detail { get; set; }
        public string hedge_steering_committee_tool_detail_code { get; set; }
        public string hedge_steering_committee_tool_detail_name { get; set; }

        public string unit_price { get; set; }
        public string unit_volume { get; set; }
        public string total_extend_volume { get; set; }
        public List<HedgeSteeringCommitteeToolDetailViewModel> hedge_steering_committee_tool_details { get; set; } = new List<HedgeSteeringCommitteeToolDetailViewModel>();

        public List<SelectListItem> ddlHedgeUnitPrice { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeUnitVolume { get; set; } = new List<SelectListItem>();
    }

    public class HedgeSteeringCommitteeToolDetailViewModel {
        public string hedge_tool_detail { get; set; }
        public string hedge_tool_detail_code { get; set; }
        public string hedge_tool_detail_name { get; set; }
        public string hedge_tool_detail_order { get; set; }

        public string feature_target_redemption { get; set; }
        public string feature_swaption { get; set; }
        public string feature_extendible { get; set; }
        public string feature_capped { get; set; }
        public string feature_knock_in_out { get; set; }

        public string net_premium { get; set; }
        public string use_formula { get; set; }
        public string expression { get; set; }

        public HedgeSteeringCommitteeToolDetailExtendibleViewModel hedge_steering_committee_tool_detail_extendible { get; set; } = new HedgeSteeringCommitteeToolDetailExtendibleViewModel();
        public HedgeSteeringCommitteeToolDetailTargetRedemptionViewModel hedge_steering_committee_tool_detail_target_redemption { get; set; } = new HedgeSteeringCommitteeToolDetailTargetRedemptionViewModel();
        public HedgeSteeringCommitteeToolDetailKnockInOutViewModel hedge_steering_committee_tool_detail_knock_in_out { get; set; } = new HedgeSteeringCommitteeToolDetailKnockInOutViewModel();
        public HedgeSteeringCommitteeToolDetailCappedViewModel hedge_steering_committee_tool_detail_capped { get; set; } = new HedgeSteeringCommitteeToolDetailCappedViewModel();
        public HedgeSteeringCommitteeToolDetailSwaptionViewModel hedge_steering_committee_tool_detail_swaption { get; set; } = new HedgeSteeringCommitteeToolDetailSwaptionViewModel();
        public List<HedgeSteeringCommitteeToolDetailNetPremiumViewModel> hedge_steering_committee_tool_detail_net_premiums { get; set; } = new List<HedgeSteeringCommitteeToolDetailNetPremiumViewModel>();
        public List<HedgeSteeringCommitteeToolDetailFormulaViewModel> hedge_steering_committee_tool_detail_formulas { get; set; } = new List<HedgeSteeringCommitteeToolDetailFormulaViewModel>();
    }

    public class HedgeSteeringCommitteeToolDetailNetPremiumViewModel {
        public string order { get; set; }
        public string option { get; set; }
        public string net_target_price { get; set; }
        public string net_target_unit_price { get; set; }
        public string net_target_unit_volume { get; set; }
        public string return_price { get; set; }
        public string return_unit_price { get; set; }
        public string return_unit_volume { get; set; }
        public string premium_price { get; set; }
        public string premium_unit_price { get; set; }
        public string premium_unit_volume { get; set; }
    }
    public class HedgeSteeringCommitteeToolDetailFormulaViewModel
    {
        public string order { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public string text { get; set; }
    }
    public class HedgeSteeringCommitteeToolDetailTargetRedemptionViewModel
    {
        public string target_redemption { get; set; }
        public string target_redemption_per_unit { get; set; }
    }
    public class HedgeSteeringCommitteeToolDetailKnockInOutViewModel
    {
        public string by { get; set; }
        public string option { get; set; }
        public string activated { get; set; }
        public string activated_operator { get; set; }
        public string hedge_price { get; set; }
        public string deactivated { get; set; }
        public string deactivated_operator { get; set; }
    }
    public class HedgeSteeringCommitteeToolDetailCappedViewModel
    {
        public string capped { get; set; }
    }
    public class HedgeSteeringCommitteeToolDetailSwaptionViewModel
    {
        public string excercise_date { get; set; }
    }
    public class HedgeSteeringCommitteeToolDetailExtendibleViewModel
    {
        public List<HedgeSteeringCommitteeToolDetailExtendibleDetailViewModel> hedge_steering_committee_tool_detail_extendible_details { get; set; } = new List<ViewModels.HedgeSteeringCommitteeToolDetailExtendibleDetailViewModel>();
        
        public List<SelectListItem> ddlHedgeUnitPrice { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddlHedgeUnitVolume { get; set; } = new List<SelectListItem>();
    }
    public class HedgeSteeringCommitteeToolDetailExtendibleDetailViewModel
    {
        public string key { get; set; }
        public string order { get; set; }
        public string extend_period_from { get; set; }
        public string extend_period_to { get; set; }
        public string exercise_date { get; set; }
        public string extend_volume { get; set; }
        public string unit_price { get; set; }
        public string unit_volume { get; set; }

    }
}