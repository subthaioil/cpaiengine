﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingCDSImportViewModel
    {
        public List<SelectListItem> ddlCreditRate { get; set; }
        public List<SelectListItem> ddlActive { get; set; }
        public List<SelectListItem> ddlCounterParty { get; set; }
        public List<SelectListItem> ddlCreditRating { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }

        public HedgingCDSImportViewModel_Search Search { get; set; }
        public HedgingCDSImportViewModel_Detail HMC_Detail { get; set; }

        public string pageMode { get; set; }
    }

    public class HedgingCDSImportViewModel_Search
    {
        public string sCouterParty { get; set; }
        public string sCreditRating { get; set; }
        public string sStatus { get; set; }

        public List<HedgingCDSImportViewModel_Detail> Detail { get; set; }
    }

    #region Detail

    public class HedgingCDSImportViewModel_Detail
    {
        public string HMCP_ROW_ID { get; set; }//HMCP_ROW_ID
        public string CouterPartyID { get; set; }//HMCP_FK_VENDOR
        public string CouterPartyName { get; set; }//HMCP_FULL_NAME
        public string CouterPartyFullName { get; set; }//HMCP_FULL_NAME
        public string MappingCdsCP { get; set; }//HMCP_MAPPING_CDS_CP
        public string LastCdsDate { get; set; }//HMCP_LAST_CDS_DATE
        public string LastCdsValue { get; set; }//HMCP_LAST_CDS_VALUE
        public string CreditRating { get; set; }//HMCP_CREDIT_RATING
        public string CreditRatingRefFlag { get; set; }//HMCP_CREDIT_RATING_REF_FLAG
        public string CreditLimit { get; set; }//HMCP_CREDIT_LIMIT
        public string CreditLimitUpdate { get; set; }//HMCP_CREDIT_LIMIT_UPDATE
        public string PaymentDay { get; set; }//HMCP_PAYMENT_DAY
        public string PaymentDayType { get; set; }//HMCP_PAYMENT_DAY_TYPE
        public string RoundDecimal { get; set; }//HMCP_ROUND_DECIMAL
        public string RoundType { get; set; }//HMCP_ROUND_TYPE
        public string Description { get; set; }//HMCP_DESC
        public string Reason { get; set; }//HMCP_REASON
        public string Status { get; set; }//HMCP_STATUS
        public string CreateDate { get; set; }//HMCP_CREATED_DATE
        public string CreateBy { get; set; }//HMCP_CREATED_BY
        public string UpdateDate { get; set; }//HMCP_UPDATED_DATE
        public string UpdateBy { get; set; }//HMCP_UPDATED_BY
        public string StatusTrade { get; set; }//HMCP_CP_STATUS

        public List<HedgingCDSImportViewModel_DetailCds> DetailCds { get; set; }
        public List<HedgingCDSImportViewModel_DetailCreditLimit> DetailCreditLimit { get; set; }
        public List<HedgingCDSImportViewModel_DetailHistory> DetailHistory { get; set; }
        public List<HedgingCDSImportViewModel_DetailHistoryLog> DetailHistoryLog { get; set; }
    }

    public class HedgingCDSImportViewModel_DetailCds
    {
        public string HMCD_ROW_ID { get; set; }//	HMCD_ROW_ID
        public string HMCD_FK_HEDG_MT_CP { get; set; }//	HMCD_FK_HEDG_MT_CP
        public string FileID { get; set; }//	HMCD_FK_HEDG_MT_CDS_FILE
        public string CdsDate { get; set; }//	HMCD_CDS_DATE
        public string CdsValue { get; set; }//	HMCD_CDS_VALUE
        public string Status { get; set; }//	HMCD_STATUS
        public string CreateDate { get; set; }//	HMCD_CREATED_DATE
        public string CreateBy { get; set; }//	HMCD_CREATED_BY
        public string UpdateDate { get; set; }//	HMCD_UPDATED_DATE
        public string UpdateBy { get; set; }//	HMCD_UPDATED_BY
    }

    public class HedgingCDSImportViewModel_DetailCreditLimit
    {
        public string HMCL_ROW_ID { get; set; }//HMCL_ROW_ID
        public string HMCL_FK_HEDG_MT_CP { get; set; }//	HMCL_FK_HEDG_MT_CP
        public string DateStart { get; set; }//	HMCL_DATE_START
        public string DateEnd { get; set; }//	HMCL_DATE_END
        public string CreditLimit { get; set; }//	HMCL_CREDIT_LIMIT_AMT
        public string Status { get; set; }//	HMCL_STATUS
        public string CreateDate { get; set; }//	HMCL_CREATED_DATE
        public string CreateBy { get; set; }//	HMCL_CREATED_BY
        public string UpdateDate { get; set; }//	HMCL_UPDATED_DATE
        public string UpdateBy { get; set; }//	HMCL_UPDATED_BY

        public string dDate { get; set; }//HMCL_DATE_START+HMCL_DATE_END
    }

    public class HedgingCDSImportViewModel_DetailHistory
    {
        public string HMCH_ROW_ID { get; set; }//	HMCH_ROW_ID
        public string HMCH_FK_HEDG_MT_CP { get; set; }//	HMCH_FK_HEDG_MT_CP
        public string Reason { get; set; }//	HMCH_REASON
        public string CreateDate { get; set; }//	HMCH_CREATED_DATE
        public string CreateBy { get; set; }//	HMCH_CREATED_BY
        public string UpdateDate { get; set; }//	HMCH_UPDATED_DATE
        public string UpdateBy { get; set; }//	HMCH_UPDATED_BY
    }

    public class HedgingCDSImportViewModel_DetailHistoryLog
    {
        public string HMHL_ROW_ID { get; set; }//	HMHL_ROW_ID
        public string HMHL_FK_CP_HISTORY { get; set; }//	HMHL_FK_CP_HISTORY
        public string Order { get; set; }//	HMHL_ORDER
        public string Title { get; set; }//	HMHL_TITLE
        public string Action { get; set; }//	HMHL_ACTION
        public string Note { get; set; }//	HMHL_NOTE
        public string CreateDate { get; set; }//	HMHL_CREATED_DATE
        public string CreateBy { get; set; }//	HMHL_CREATED_BY
        public string UpdateDate { get; set; }//	HMHL_UPDATED_DATE
        public string UpdateBy { get; set; }//	HMHL_UPDATED_BY
    }

    #endregion

    public class HedgingCDSImportViewModel_CdsMax
    {
        public string VendorID { get; set; }
        public string Date { get; set; }
    }

    public class CounterPartyTemp
    {
        public string VendorID { get; set; }
        public string Name { get; set; }
        public string Rating { get; set; }
        public string Date { get; set; }
        public string Value { get; set; }
        public string Count { get; set; }
    }
}
