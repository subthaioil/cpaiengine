﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HolidayViewModel
    {
        public HolidayViewModel_Search holiday_Search { get; set; }
        public HolidayViewModel_SearchData holiday_SearchData { get; set; }
        public HolidayViewModel_Detail holiday_Detail { get; set; }

        public List<SelectListItem> ddl_HolidayType { get; set; }
    }

    public class HolidayViewModel_Search
    {
        public string sHolidayID { get; set; }
        public string sHolidayDesc { get; set; }
        public string sHolidayDate { get; set; }
        public string sHolidayType { get; set; }
        public List<HolidayViewModel_SearchData> sSearchData { get; set; }
    }

    public class HolidayViewModel_SearchData
    {
        public string dHolidayID { get; set; }
        public string dHolidayDesc { get; set; }
        public string dHolidayDate { get; set; }
        public string dHolidayType { get; set; }
        public string dHolidayDateOrder { get; set; }
    }

    public class HolidayViewModel_Detail
    {
        public string HolidayID { get; set; }
        public string HolidayDesc { get; set; }
        public string HolidayDate { get; set; } // dd/MM/yyyy
        public string HolidayType { get; set; }
    }
}