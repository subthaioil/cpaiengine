﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class JettyViewModel
    {
        public JettyViewModel_Search jetty_Search { get; set; }
        public JettyViewModel_Detail jetty_Detail { get; set; }

        public List<SelectListItem> ddl_Country { get; set; }
        public List<SelectListItem> ddl_Vehicle { get; set; }
        public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
        public string jettyIDJSON { get; set; }
    }

    public class JettyViewModel_Search
    {
        public string sJettyID { get; set; }
        public string sJettyName { get; set; }
        public string sType { get; set; }
        public string sCountry { get; set; }
        public string sLocation { get; set; }
        public string sStatus { get; set; }
        public string sCreateType { get; set; }

        public List<JettyViewModel_SearchData> sSearchData { get; set; }
    }

    public class JettyViewModel_SearchData
    {
        public string dJettyID { get; set; }
        public string dJettyName { get; set; }
        public string dJettyChargeID { get; set; }
        public string dType { get; set; }
        public string dCountry { get; set; }
        public string dLocation { get; set; }
        public string dCharge { get; set; }
        public string dStatus { get; set; }
        public string dCreateType { get; set; }
    }

    public class JettyViewModel_Detail
    {
        public string JettyID { get; set; }
        public string JettyChargeID { get; set; }
        public string JettyName { get; set; }
        public string Type { get; set; }
        public string Country { get; set; }
        public string Vehicle { get; set; }
        public string Location { get; set; }
        public string Charge { get; set; }
        public string Status { get; set; }
        public string CreateType { get; set; }
        public bool boolStatus { get; set; }
    }

}