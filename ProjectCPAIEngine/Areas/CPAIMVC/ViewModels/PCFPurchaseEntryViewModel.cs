﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class PCFPurchaseEntryViewModel
    {
        public PCFPurchaseEntryViewModel_Search PCFPurchaseEntry_Search { get; set; }

        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_PoDate { get; set; }
        public List<SelectListItem> ddl_Product { get; set; }
        public List<SelectListItem> ddl_Supplier { get; set; }
    }

    public class PCFPurchaseEntryViewModel_Search
    {
        public string sCompany { get; set; }
        public string sTripNo { get; set; }
        public string sPoDate { get; set; }
        public string sProduct { get; set; }
        public string sPoNo { get; set; }
        public string sGrDate { get; set; }
        public string sSupplier { get; set; }        
        public List<PCFPurchaseEntryViewModel_SearchData> sSearchData { get; set; }
    }

    public class PCFPurchaseEntryViewModel_SearchData
    {
        public string TripNo { get; set; }
        public string Vessel { get; set; }
        public string Crude { get; set; }
        public string PoNo { get; set; }
        public string Product { get; set; }
        public string Supplier { get; set; }
        public string SupplierNo { get; set; }
        public string VolumeBBL { get; set; }
        public string VolumeMT { get; set; }
        public string PurchaseType { get; set; }
        public string MetNum { get; set; }
        public string CompanyCode { get; set; }
        public string Currency { get; set; }

        public string VolumeUnit { get; set; }
        public string InvoiceFigure { get; set; }
        public string PriceUnit { get; set; }
        public string MatItemNo { get; set; }
        public string FIDoc { get; set; }

        public PCFPurchaseEntryViewModel_PitPurchaseEntry PitPurchaseEntry { get; set; }
        public List<PCFPurchaseEntryViewModel_Outtern> sOutturn { get; set; }
    }

    public class PCFPurchaseEntryViewModel_Outtern
    {
        public string BLDate { get; set; }
        public string BlQtyBbl { get; set; }
        public string BlQtyMt { get; set; }
        public string BlQtyMl { get; set; }
        public string PostingDate { get; set; }
        public string ItemNo { get; set; }
        public string RunningDate { get; set; }
        public string QtyBbl { get; set; }
        public string QtyMt { get; set; }
        public string QtyMl { get; set; }
        public string FOBPrice { get; set; }
        public string FreightPrice { get; set; }
        public string CnFPrice { get; set; }
        public string ROE { get; set; }
        public string FOBAmountUSD { get; set; }
        public string FOBAmountTHB { get; set; }
        public string FreightAmountUSD { get; set; }
        public string FreightAmountTHB { get; set; }
        public string CnFAmountUSD { get; set; }
        public string CnFAmountTHB { get; set; }
        public string CnFVat7 { get; set; }
        public string CnFTotalAmountUSD { get; set; }
        public string CnFTotalAmountTHB { get; set; }
        public string SAPFIDoc { get; set; }
        public string SAPFIDocStatus { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool Select { get; set; }
        public bool Valid { get; set; }
        public string MatItemNo { get; set; }

        public string VolumeUnit { get; set; }
        public string InvoiceFigure { get; set; }
        public string PriceUnit { get; set; }
        
    }

    public class PCF_PTT_AGREEMENT
    {
        public int PTT_VENDER { get; set; }
    }

    public class PCFPurchaseEntryViewModel_PitPurchaseEntry
    {
        public string CalVolumeUnit { get; set; }
        public string CalInvoiceFigure { get; set; }
        public string CalPriceUnit { get; set; }
    }
}