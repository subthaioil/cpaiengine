﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class PlantViewModel
    {
        public PlantViewModel_Search plant_Search { get; set; }
        public PlantViewModel_Detail plant_Detail { get; set; }

        public List<SelectListItem> ddl_PlantCompany { get; set; }
        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }

        public string plant_SystemJSON { get; set; }

        public string plantIdJSON { get; set; }
        public string plantNameJSON { get; set; }
    }

    public class PlantViewModel_Search
    {
        public string sCompanyCode { get; set; }
        public string sPlantID { get; set; }
        public string sPlantShortName { get; set; }
        public string sCreateType { get; set; }
        public string sSystem { get; set; }
        public string sStatus { get; set; }
        public List<PlantViewModel_SearchData> sSearchData { get; set; }
    }

    public class PlantViewModel_SearchData
    {
        public string dCompanyCode { get; set; }
        public string dCompanyName { get; set; }
        public string dPlantID { get; set; }
        public string dPlantShortName { get; set; }
        public string dPlantDesc { get; set; }
        public string dCreateType { get; set; }
        public string dSystem { get; set; }
        public string dStatus { get; set; }
    }

    public class PlantViewModel_Detail
    {
        public string CreateType { get; set; }
        public string CompanyCode { get; set; }
        public string PlantID { get; set; }
        public string PlantShortName { get; set; }
        public string PlantDesc { get; set; }
        public List<PlantViewModel_Control> Control { get; set; }
    }

    public class PlantViewModel_Control
    {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string System { get; set; }
        public string Status { get; set; }
    }
}