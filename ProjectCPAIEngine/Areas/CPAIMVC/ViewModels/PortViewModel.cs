﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class PortViewModel
    {
        public PortViewModel_Search port_Search { get; set; }
        public PortViewModel_Detail port_Detail { get; set; }

        public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }

        public string port_SystemJSON { get; set; }
    }

    public class PortViewModel_Search
    {
        public string sPortID { get; set; }
        public string sPortName { get; set; }
        public string sRemark { get; set; }
        public string sCreateType { get; set; }
        public string sSystem { get; set; }
        public string sStatus { get; set; }
        public List<PortViewModel_SearchData> sSearchData { get; set; }
    }

    public class PortViewModel_SearchData
    {
        public string dPortID { get; set; }
        public string dPortName { get; set; }
        public string dRemark { get; set; }
        public string dCreateType { get; set; }
        public string dSystem { get; set; }
        public string dStatus { get; set; }
    }

    public class PortViewModel_Detail
    {
        public string CreateType { get; set; }
        public string PortID { get; set; }
        public string PortName { get; set; }
        public string Remark { get; set; }
        public List<PortViewModel_Control> Control { get; set; }
    }

    public class PortViewModel_Control
    {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string System { get; set; }
        public string Status { get; set; }
    }
}