﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class PurchaseEntryVesselViewModel
    {
        public string sPostingDate { get; set; }
        public string sCalVolumeUnit { get; set; }
        public string sCalInvFigure { get; set; }
        public bool bPartly { get; set; }
        public bool bPartlyComplete { get; set; }
        public string tabPartly { get; set; }
        public string HTotalBBL { get; set; }
        public string HTotalML { get; set; }
        public string HTotalMT { get; set; }
        public string IsSave { get; set; }

        public string SAPDocumentDate { get; set; }
        public string SAPPostingDate { get; set; }
        public string SAPAccuralType { get; set; }
        public string SAPReverseDateForTypeA { get; set; }
        public string CompanyCode { get; set; }
        public string SupplierNo { get; set; }
        public string Currency { get; set; }
        public string TotalAll { get; set; }

        public string INS_DF { get; set; }

        public bool isEdit { get; set; }
        public List<SelectListItem> ddl_ItemNo { get; set; }
        public string sItemNo { get; set; }

        public List<SelectListItem> ddl_InsuranceRate { get; set; }

        public List<PEV_Quantity> QuantityList { get; set; }
        public List<PEV_Freight> FreightList { get; set; }
        public List<PEV_Freight> FreightList1 { get; set; }
        public List<PEV_Freight> FreightList2 { get; set; }
        public List<PEV_Freight> FreightList3 { get; set; }
        public List<PEV_Price> dPriceList { get; set; }
        public PEV_Price PEV_Price { get; set; }
        public List<PEV_Price> PriceList { get; set; }
        public PEV_Price PriceListPartly1 { get; set; }
        public PEV_Price PriceListPartly2 { get; set; }
        public PEV_Price PriceListPartly3 { get; set; }
        public List<PEV_PriceSupplier> PriceSupplierList { get; set; }
        public List<PEV_PriceSupplier> PriceSupplierListPartly1 { get; set; }
        public List<PEV_PriceSupplier> PriceSupplierListPartly2 { get; set; }
        public List<PEV_PriceSupplier> PriceSupplierListPartly3 { get; set; }
        public FED_Calculate Fed_Calculate { get; set; }
    }

    public class FED_Calculate
    {
        public string MET_NUM { get; set; }
        public string Total_Volume_MT { get; set; }
    }

    public class PEV_Quantity
    {
        public string QuantityUnit { get; set; }
        public string QtyBL { get; set; }
        public string QtyOutTurn { get; set; }
    }

    public class PEV_Freight
    {
        public string FRTSupplierTxt { get; set; }
        public string FRTSubItem { get; set; }
        public string FRTItemNo { get; set; }
        public string FRTSupplier { get; set; }
        public string FRTTotalUSD { get; set; }
        public string FRTTotalTHB { get; set; }
        public string FRTROE { get; set; }
    }

    public class PEV_Price
    {
        public string FOBPriceUSD { get; set; }
        public string FOBSupplier { get; set; }
        public string FOBTotalUSD { get; set; }
        public string FOBROE { get; set; }
        public string FOBTotalTHB { get; set; }
        public string FRTPriceUSD { get; set; }
        //public string FRTSupplier { get; set; }
        public string FRTTotalUSD { get; set; }
        public string FRTROE { get; set; }
        public string FRTTotalTHB { get; set; }
        public string FOBFRTTotalUSD { get; set; }
        public string FOBFRTTotalTHB { get; set; }
        public string INSPriceUSD { get; set; }
        public string INSTotalUSD { get; set; }
        public string InsuranceRate { get; set; }
        public string StampDutyTotalUSD { get; set; }
        public string InsStampDutyRate { get; set; }
        public string LossGain { get; set; }
        public string MRCInsTotalUSD { get; set; }
        public string MRCInsROE { get; set; }
        public string MRCInsTotalTHB { get; set; }
        public string TotalCIFPriceUSD { get; set; }
        public string TotalCIFTotalUSD { get; set; }
        public string TotalCIFTotalTHB { get; set; }
        public string RemarkFOBUSD { get; set; }
        public string RemarkFOBTHB { get; set; }
        public string RemarkFRTUSD { get; set; }
        public string RemarkFRTTHB { get; set; }
        public string RemarkINSUSD { get; set; }
        public string RemarkINSTHB { get; set; }
        public string RemarkImportDutyUSD { get; set; }
        public string RemarkImportDutyROE { get; set; }
        public string RemarkImportDutyTHB { get; set; }
        public string RemarkTotalUSD { get; set; }
        public string RemarkTotalTHB { get; set; }
        public string FiDoc { get; set; }

        public string sTripNo { get; set; }
        public string sBLDate { get; set; }
        public string sLastedUpdate { get; set; }
        public string sMetNum { get; set; }
        public string sTxtMet { get; set; }
        public string sDueDate { get; set; }
        public string sVesselName { get; set; }
        public string sTxtVesselName { get; set; }
        public string sPostingDate { get; set; }
        public string sCalVolumeUnit { get; set; }
        public string sCalInvFigure { get; set; }
        public bool bPartly { get; set; }
        public bool bPartlyComplete { get; set; }
        public string tabPartly { get; set; }
        public string HTotalBBL { get; set; }
        public string HTotalML { get; set; }
        public string HTotalMT { get; set; }
        public string SAPDocumentNO { get; set; }
        public string SAPDocumentDate { get; set; }
        public string SAPPostingDate { get; set; }
        public string SAPAccuralType { get; set; }
        public string SAPReverseDateForTypeA { get; set; }
        public string CompanyCode { get; set; }
        public string SupplierNo { get; set; }
        public string Currency { get; set; }
        public string TotalAll { get; set; }

        public string INS_DF { get; set; }

        public string NetTotalVolumeBBL { get; set; }
        public string NetTotalPriceBBLUSD { get; set; }
        public string NetTotalPriceBBLTHB { get; set; }
        public string NetTotalVolumeML { get; set; }
        public string NetTotalPriceMLUSD { get; set; }
        public string NetTotalPriceMLTHB { get; set; }
        public string NetTotalVolumeMT { get; set; }
        public string NetTotalPriceMTUSD { get; set; }
        public string NetTotalPriceMTTHB { get; set; }

        public bool isEdit { get; set; }
        public List<SelectListItem> ddl_ItemNo { get; set; }
        public string sItemNo { get; set; }

        public FED_Calculate Fed_Calculate { get; set; }

        public List<PEV_Quantity> QuantityList { get; set; }

        public List<PEV_Freight> FreightList { get; set; }

        public List<PEV_PriceSupplier> PriceSupplierList { get; set; }
    }

    public class PEV_PriceSupplier
    {
        public string SupplierName { get; set; }
        public string txtSupplierName { get; set; }
        public string VolumeBBL { get; set; }
        public string OriginalVolumeBBL { get; set; }
        public string VolumeML { get; set; }
        public string OriginalVolumeML { get; set; }
        public string VolumeMT { get; set; }
        public string OriginalVolumeMT { get; set; }
        public string PriceUnitUSD { get; set; }
        public string PriceTotalUSD { get; set; }
        public string ROE { get; set; }
        public string PriceTotalTHB { get; set; }
        public string NetTotalVolumeBBL { get; set; }
        public string NetTotalPriceBBLUSD { get; set; }
        public string NetTotalPriceBBLTHB { get; set; }
        public string NetTotalVolumeML { get; set; }
        public string NetTotalPriceMLUSD { get; set; }
        public string NetTotalPriceMLTHB { get; set; }
        public string NetTotalVolumeMT { get; set; }
        public string NetTotalPriceMTUSD { get; set; }
        public string NetTotalPriceMTTHB { get; set; }
        public string MatItemNo { get; set; }
        public string PriceStatus { get; set; }
        public string InvoiceNo { get; set; }
        public string HVolumeBBL { get; set; }
        public string HVolumeML { get; set; }
        public string HVolumeMT { get; set; }
    }

    [Serializable]
    public class GlobalConfigStorageLocation
    {
        public List<PCF_MT_STORAGELOCATION> PCF_MT_STORAGE_LOCATION { get; set; }
    }

    public class PCF_MT_STORAGELOCATION
    {
        public string COMPANY_CODE { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class PurchaseEntryVesselReport
    {
        public string sTripNo { get; set; }
        public string sBLDate { get; set; }
        public string sMetNum { get; set; }
        public string sDueDate { get; set; }
        public string sVesselName { get; set; }
        public string sPostingDate { get; set; }

        public List<PEV_Quantity> QuantityList { get; set; }
        public PEV_Price PriceList { get; set; }
        public List<PEV_PriceSupplier> PriceSupplierList { get; set; }
        public List<PEV_Freight> FreightList { get; set; }

        public PurchaseEntryVesselReport()
        {
            QuantityList = new List<PEV_Quantity>();
            PriceSupplierList = new List<PEV_PriceSupplier>();
            FreightList = new List<PEV_Freight>();
        }
    }
     
}