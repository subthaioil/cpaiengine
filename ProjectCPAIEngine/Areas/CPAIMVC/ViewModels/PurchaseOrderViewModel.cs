﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class PurchaseOrderViewModel
    {
        public PurchaseOrderViewModel_Search PurchaseOrder_Search { get; set; }
        public PurchaseOrderViewModel_Detail PurchaseOrder_Detail { get; set; }

        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_Vendor { get; set; }
        public List<SelectListItem> ddl_PurchasingGroup { get; set; }
        public List<SelectListItem> ddl_Incoterm { get; set; }
        public List<SelectListItem> ddl_StorageLocation { get; set; }
        public List<SelectListItem> ddl_feedStock { get; set; }
        public List<SelectListItem> ddl_Currency { get; set; }
    }

    public class PurchaseOrderViewModel_Search
    {
        public string sPoNo { get; set; }
        public string sSoNo { get; set; }
        public string sTripNO { get; set; }
        public string sFeedStock { get; set; }
        public string sSupplier { get; set; }
        public string sIncoterm { get; set; }
        public string sCompany { get; set; }
        public string sPoDate { get; set; }
        public List<PurchaseOrderViewModel_SearchData> sSearchData { get; set; }
    }

    public class PurchaseOrderViewModel_SearchData
    {
        public string dRunningNo { get; set; }
        public string dCompanyName { get; set; }
        public string dTripNo { get; set; }
        public string dFeedstock { get; set; }
        public string dSupplier { get; set; }
        public string dIncoterms { get; set; }
        public string dVolume { get; set; }
        public string dVolumeUnit { get; set; }
        public string dPrice { get; set; }
        public string dTotalAmount { get; set; }
        public string dCurrency { get; set; }
        public string dSapPONO { get; set; }
        public string dLastUpdated { get; set; }
        public string dUpdatedBy { get; set; }
        public string dPoDate { get; set; }
        public string dSoNo { get; set; }
    }

    public class PurchaseOrderViewModel_Detail
    {
        public string PoNo { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string AccVendor { get; set; }
        public string AccNumVendor { get; set; }
        public string PurchasingGroup { get; set; }
        public string Currency { get; set; }
        public string FXBot { get; set; }
        public string PoDate { get; set; }
        public string Incoterms { get; set; }
        public string OriginCountry { get; set; }
        public string CargoNo { get; set; }
        public string SoNo { get; set; }

        public List<PurchaseOrderViewModel_Control> Control { get; set; }
    }

    public class PurchaseOrderViewModel_Control
    {
        public string FKPoNo { get; set; }
        public string PoItem { get; set; }
        public string ShortText { get; set; }
        public string MetNum { get; set; }
        public string Plant { get; set; }
        //public string StorageLocation { get; set; }
        //public string StorageLocationNo { get; set; }
        public string TripNo { get; set; }
        public string Volume { get; set; }
        public string VolumeUnit { get; set; }
        public string UnitPrice { get; set; }
        public string Currency { get; set; }
        public string Vendor { get; set; }
        public string VendorNo { get; set; }
        public string TranId { get; set; }
        public string CargoNo { get; set; }
        public string SOUserName { get; set; }
        public string TotalAmount { get; set; }
    }

    [Serializable]
    public class VolumnUnits
    {
        public List<VOLUMN_UNIT> VOLUMN_UNIT { get; set; }
    }

    public class VOLUMN_UNIT
    {
        public string NAME { get; set; }
    }

    public class Vendor
    {
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
    }

    public class Vendors
    {
        public List<Vendor> Vendor { get; set; }
    }
}