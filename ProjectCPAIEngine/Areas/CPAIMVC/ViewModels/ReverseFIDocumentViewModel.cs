﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class ReverseFIDocumentViewModel
    {
        public string Document { get; set; }
        public string ComCode { get; set; }
        public string Year { get; set; }

        public List<SelectListItem> ddl_Company { get; set; }
    }

    public class ReverseFIDocumentResult
    {
        public bool Pass { get; set; }
        public string PoNo { get; set; }
        public string SAPReverseDocNo { get; set; }
        public string ResultMessage { get; set; }
        public string CompanyCode { get; set; }

    }
}