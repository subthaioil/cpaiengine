﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class RoleViewModel
    {
        public RoleViewModel_Seach role_Search { get; set; }
        public RoleViewModel_Detail role_Detail { get; set; }

        public List<SelectListItem> ddl_RoleName { get; set; }
        public List<SelectListItem> ddl_RoleDesc { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
    }

    public class RoleViewModel_Seach
    {
        public string sRoleName { get; set; }     // Display (dd/MM/yyyy to dd/MM/yyyy)  
        public string sRoleDescription { get; set; }
        public string sRoleStatus { get; set; }
        public List<RoleViewModel_SeachData> sSearchData { get; set; }
    }

    public class RoleViewModel_SeachData
    {
        public string dRolID { get; set; }
        public string dRolName { get; set; }
        public string dRolStatus { get; set; }
        public string dRolDesc { get; set; }
    }

    public class RoleViewModel_Detail
    {
        public string RolID { get; set; }
        public string RolName { get; set; }
        public string RolType { get; set; }
        public string RolDesc { get; set; }
        public string RolStatus { get; set; }
        public bool BoolStatus { get; set; }

        public string MenuTree { get; set; }
        public string MenuTreeID { get; set; }

        public List<RoleViewModel_Menu> RoleMenu { get; set; }

    }

    public class RoleViewModel_Menu
    {
        public string RoleMenuID { get; set; }
        public string MenuID { get; set; }
        public string MenuGroup { get; set; }
        public string MenuParentID { get; set; }
        public string MenuLevel { get; set; }
        public string MenuListNo { get; set; }
        public string MenuDesc { get; set; }
    }


    public class RoleViewModel_MenuWithRole
    {
        public string dMenuID { get; set; }
        public string dMenuGroup { get; set; }
        public string dMenuParentID { get; set; }
        public string dMenuLevel { get; set; }
        public string dMenuListNo { get; set; }
        public string dMenuDesc { get; set; }
        public string dMenuType { get; set; }
        public string dRoleID { get; set; }
        public string dRoleName { get; set; }
        public string dRoleDesc { get; set; }
        public string dRoleStatus { get; set; }
        public string dRoleMenuID { get; set; }
    }

}