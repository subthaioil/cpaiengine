﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class SOClearLineViewModel
    {
        public SOClearLineViewModel_Search SOClearLine_Search { get; set; }     
        public string selectedTripNo { get; set; }               
        public List<SelectListItem> ddl_Vessel { get; set; }
        public string json_Vessel { get; set; }
        public List<SelectListItem> ddl_Crude { get; set; }
        public string json_Crude { get; set; }
        public List<SelectListItem> ddl_Customer { get; set; }
        public string json_Customer { get; set; }
    }

    public class SOClearLineViewModel_Search
    {
        public string sTripNo { get; set; }
        public string sVesselName { get; set; }
        public string sDeliveryDate { get; set; }
        public string sCrude { get; set; }
        public string sCustomer { get; set; }
        public string sTripNo_Select { get; set; }
        public List<SOClearLineViewModel_SearchData> SearchData { get; set; }

    }

    public class SOClearLineViewModel_SearchData
    {
        public string dTripNo { get; set; }
        public string dCustomer { get; set; }
        public string dDeliveryDate { get; set; }
        public string dPlant { get; set; }
        public string dProduct { get; set; }
        public string dVolumeBBL { get; set; }
        public string dVolumeMT { get; set; }
        public string dVolumeLitres { get; set; }
        public string dPrice { get; set; }
        public string dAmount { get; set; }
        public string dUS4 { get; set; }
        public string dPONo { get; set; }
        public string dExchangeRate { get; set; }
        public string dSaleOrder { get; set; }
        public string dMEMONo { get; set; }
        public string dDO { get; set; }

        public string dSaleUnitType { get; set; }
        public string dInvoiceFigureType { get; set; }

        public string dCompany { get; set; }
    }
        
}