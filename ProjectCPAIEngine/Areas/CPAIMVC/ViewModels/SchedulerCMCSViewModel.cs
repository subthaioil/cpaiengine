﻿using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class SchedulerCMCSViewModel
    {
        public schsDetail schs_detail { get; set; }

        public List<SchsActivity> schs_activitys { get; set; }
        public SchsActivityViewModel schs_activitys_model { get; set; }      // For Schedule Activity view Model
        //search
        public string schs_searchStartEndDate { get; set; }
        public string schs_searchMultiVesselID { get; set; }

        //insert/edit
        public string schs_Transaction_id { get; set; }
        public string schs_Req_transaction_id { get; set; }
        public string schs_Doc_no { get; set; }
        public string schs_Type { get; set; }
        public string schs_Status { get; set; }
        public string schs_tempDateFrom { get; set; }
        public string schs_tempTimeFrom { get; set; }
        public string schs_tempDateTo { get; set; }
        public string schs_tempTimeTo { get; set; }
        public List<SchsLoadPorts> schs_load_ports { get; set; }
        public List<SchsDisPorts> schs_dis_ports { get; set; }

        public List<SchsActivityViewModel_GridValues> schs_activity_grid_model { get; set; }

        //DDL
        public List<SelectListItem> vessel { get; set; }
        public List<SelectListItem> product { get; set; }
        public List<SelectListItem> portName { get; set; }
        public List<SelectListItem> vesselActivity { get; set; }

        public List<SchCMCSTransGridViewModel> schs_cmsc_trans_grid_view_model { get; set; }

        public string schs_cmsc_trans_timeline_json { get; set; }
        public string schs_cmsc_trans_vessel_json { get; set; }
        public List<SchsActivityTimelineListViewModel> activity_info { get; set; }
        public string ButtonCode { get; set; }

        public SchedulerCMCSViewModel()
        {
            activity_info = new List<SchsActivityTimelineListViewModel>();
        }
    }
     
    // Model สำหรับ แสดงข้อมูลใน Grid
    public class SchCMCSTransGridViewModel
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Doc_no_Encrypted { get; set; }
        public string Type_Encrypted { get; set; }

        public string scht_status_Encrypted { get; set; }

        public string scht_Doc_no { get; set; }
        public string scht_Datetime { get; set; }
        public string scht_vasselName { get; set; }
        public string scht_tc { get; set; }
        public string scht_note { get; set; }
        public string scht_reson { get; set; }
        public string scht_status { get; set; }
        public string scht_type { get; set; }
    }

    public class SchTransVesselViewModel
    {
        public string key { get; set; }
        public string label { get; set; }
    }

    public class SchTransTimelineViewModel
    {
        public string text { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string section_id { get; set; }
        public string color { get; set; }
        public string href_edit { get; set; }
        public string href_copy { get; set; }
        public string href_view { get; set; }
    }

    public class SchsLoadPorts
    {
        public string order { get; set; }
        public string port { get; set; }
        public List<SchsCrudeDetail> schs_crude { get; set; }
    }

    public class SchsCrudeDetail
    {
        public string crude { get; set; }
        public string qty { get; set; }
        public string nominate_date { get; set; }
        public string received_date { get; set; }
    }

    public class SchsDisPorts
    {        
        public string order { get; set; } = "";       
        public string port { get; set; } = "";
    }    

    // Model for Schedule Activity View
    public class SchsActivityViewModel
    {
        public string TranID { get; set; }
        public string Type { get; set; }

        public string activity { get; set; }
        public string activity_color { get; set; }
        public string order { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string @ref { get; set; }

        public string sch_activity_timeline_model_json { get; set; }

        public List<SchsActivityViewModel_GridValues> schs_activity_grid_model { get; set; }
    }

    public class SchsActivityTimelineViewModel
    {
        public string text { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string section_id { get; set; }
        public string color { get; set; }
        public string href_edit { get; set; }
        public string href_copy { get; set; }
        public string href_view { get; set; }
        public List<SchsActivityTimelineInfoViewModel> activity_info { get; set; }
    }

    public class SchsActivityTimelineInfoViewModel
    {
        public string activity { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
        public string activity_start { get; set; }
        public string activity_end { get; set; }
        public string activity_color { get; set; }
    }

    public class SchsActivityTimelineListViewModel
    {
        public string tcNumber { get; set; }
        public string activity { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string section_id { get; set; }
        public string color { get; set; }
    }
    
    // Model for Schedule Activity Grid Items 
    public class SchsActivityViewModel_GridValues
    {
        public string act_order { get; set; }       // Order No.
        public string act_code { get; set; }        // DDL
        public string act_color { get; set; }       // DDL
        public string act_date_start { get; set; }
        public string act_date_end { get; set; } // 'dd/MM/yyyy HH:mm'        
        public string act_time { get; set; }
        public string act_ref { get; set; }
        public string act_ref_status { get; set; }  // Not Display
        public string act_portName { get; set; }
    }

    public class SchsObjectViewModel
    {
        public schsDetail schs_detail { get; set; }
        public List<SchsActivity> schs_activitys { get; set; }
    }
    public class Vessel
    {
        public string Vessel_Name { get; set; }
        public List<Operating_Year> Operating_Year { get; set; }
    }
    public class Operating_Year
    {
        public string Year { get; set; }
        public List<Operating_Qtr> Qtr { get; set; }
        public List<Operating_Activity> Activity { get; set; }
    }
    public class Operating_Qtr
    {
        public string Qtr_Name_Year { get; set; }
        public decimal Qtr_Vul { get; set; }
    }
    public class Operating_Activity
    {
        public string Activity_id { get; set; }
        public string Activity_Name { get; set; }
        public List<Operating_Qtr> Qtr { get; set; }
    }

    public class Vessel_Utilization_Report
    {
        public List<SelectListItem> Yearly { get; set; }
        public List<SelectListItem> MonthlyFrom { get; set; }
        public List<SelectListItem> MonthlyTo { get; set; }
        public List<SelectListItem> YearlyFrom { get; set; }
        public List<SelectListItem> YearlyTo { get; set; }
        public List<SelectListItem> YearlyComparsion1 { get; set; }
        public List<SelectListItem> YearlyComparsion2 { get; set; }
        public List<SelectListItem> Vessel { get; set; }
        public Search Search { get; set; }
    }
    public class Search
    {
        public string sVessel { get; set; }
        public string sYearly { get; set; }
        public string sMonthlyFrom { get; set; }
        public string sMonthlyTo { get; set; }
        public string sYearlyFrom { get; set; }
        public string sYearlyTo { get; set; }
        public string sYearlyComparsion1 { get; set; }
        public string sYearlyComparsion2 { get; set; }
        public string VoyageFrom { get; set; }
        public string VoyageTo { get; set; }
    }


}