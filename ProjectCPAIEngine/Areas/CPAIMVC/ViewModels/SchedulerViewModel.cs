﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class SchedulerViewModel
    {
        public SchDetail sch_detail { get; set; }
        public List<SchActivity> sch_activitys { get; set; }
        public string schd_tempLaycan { get; set; }
        public string schd_tempDate { get; set; }
        public string schd_tempSailing { get; set; }
        public string schd_note { get; set; }

        public string schd_currentMonthYear { get; set; }       // Jane 2016
        public string schd_startDate { get; set; }              // 'yyyy-MM-dd'
        public string schd_endDate { get; set; }                // 'yyyy-MM-dd'
        public string schd_searchStartEndDate { get; set; }     // dd/MM/yyyy to dd/MM/yyyy
        public string schd_searchMultiVesselID { get; set; }     // "vessel1|vessel2|vessel3|vessel4"
        public string schd_vendorColorDefualt { get; set; }
        public string schd_vendorColorValue { get; set; }       //#color|vendor_id

        public string schd_dayLaycan { get; set; }          // Laycan day calculate between date [num Day]
        public string schd_daySailing { get; set; }         // Sailing day calculate between date [num Day]

        public string schd_Transaction_id { get; set; }
        public string schd_Req_transaction_id { get; set; }
        public string schd_Doc_no { get; set; }
        public string schd_Type { get; set; }
        public string schd_Status { get; set; }

        // For view (DRAFT,CANCEL) in Grid View
        public List<SchTransGridViewModel> sch_trans_grid_model { get; set; }

        // For view (SUBMIT) in Gantt View
        public List<SchTransGanttViewModel_Data> sch_trans_gantt_model { get; set; }
        public string sch_trans_gantt_model_json { get; set; }

        // For view customer clor in dropdownlist
        public List<SchTransDropdownCustColorViewModel> sch_trans_custcolor_model { get; set; }

        public string sch_trans_list_id { get; set; }

        //public List<SchedulerTransaction> sch_schedule_transaction { get; set; }

        public List<SelectListItem> vessel { get; set; }
        public List<SelectListItem> freight { get; set; }
        public List<SelectListItem> customer { get; set; }
        public List<SelectListItem> cargo_list { get; set; }
        public List<SelectListItem> cargo_unit { get; set; }
        public List<SelectListItem> portName { get; set; }          // For Load Port Field and Discharge port Field
        public List<SelectListItem> vesselActivity { get; set; }    //
        public List<SelectListItem> scheduleType { get; set; }
        public List<SelectListItem> activityType { get; set; }
        public List<SelectListItem> ddl_Color { get; set; }

        //public SchRootObject sch_rootObject { get; set; }         // For Schedule Activity View

        
        public SchActivityViewModel sch_activitys_model { get; set; }      // For Schedule Activity view Model
            
        public string ButtonCode { get; set; }      // For button code

        public string chot_modal_date { get; set; }
        public string chot_modal_vessel { get; set; }
        public string chot_modal_charterer { get; set; }
        public string chot_modal_laycan { get; set; }
        public string chot_modal_laytime { get; set; }
        public string chot_modal_cargo { get; set; }
        public List<SelectListItem> charterer_list { get; set; }

        public bool flagDefult { get; set; }
        public string LatestUpdate { get; set; }
    }

    // Model สำหรับ แสดงข้อมูลใน Grid
    public class SchTransGridViewModel
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Doc_no_Encrypted { get; set; }
        public string Type_Encrypted { get; set; }

        public string scht_status_Encrypted { get; set; }

        public string scht_Doc_no { get; set; }
        public string scht_Datetime { get; set; }
        public string scht_vasselName { get; set; }
        public string scht_userCustomer { get; set; }
        public string scht_note { get; set; }
        public string scht_laycan { get; set; }
        public string scht_status { get; set; }
        public string scht_type { get; set; }
        public string scht_Update_date { get; set; }
        public string scht_Update_by { get; set; }
    }

    // Model สำหรับ แสดงข้อมูลใน GanttView
    public class SchTransGanttViewModel_Data
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<SchTransGanttViewModel_Series> series { get; set; }
    }
    public class SchTransGanttViewModel_Series
    {
        public string name { get; set; }
        public List<SchTransGanttViewModel_Activities> activities { get; set; }
    }
    public class SchTransGanttViewModel_Activities
    {
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string color { get; set; }
    }

    // Model for customer color
    public class SchTransDropdownCustColorViewModel
    {
        public string cust_Code { get; set; }
        public string cust_Name { get; set; }
        public string cust_Color { get; set; }
    }

    // Model for Schedule Activity View
    public class SchActivityViewModel
    {
        public string TranID { get; set; }
        public string Type { get; set; }

        public string schd_startDate { get; set; }              // 'yyyy-MM-dd HH:mm'
        public string schd_endDate { get; set; }                // 'yyyy-MM-dd HH:mm'

        public string schd_calendar_startDate { get; set; }     // 'dd/MM/yyyy'
        public string schd_calendar_endDate { get; set; }       // 'dd/MM/yyyy'

        public List<SchActivityViewModel_GanttSource> sch_activity_gantt_model { get; set; }
        public string sch_activity_gantt_model_json { get; set; }

        public List<SchActivityViewModel_GridValues> sch_activity_grid_model { get; set; }
    }

    // Model for Schedule Activity Gantt
    public class SchActivityViewModel_GanttSource
    {
        public string name { get; set; }
        public List<SchActivityViewModel_GanttValues> values { get; set; }
    }

    // Model for Schedule Activity Gantt
    public class SchActivityViewModel_GanttValues
    {
        public string from { get; set; }
        public string to { get; set; }
        public string label { get; set; }
        public string customClass { get; set; }
    }

  
    // Model for Schedule Activity Grid Items 
    public class SchActivityViewModel_GridValues
    {
        public string act_order { get; set; }       // Order No.
        public string act_code { get; set; }        // DDL
        public string act_color { get; set; }       // DDL
        public string act_date { get; set; }        // 'dd/MM/yyyy HH:mm'        
        public string act_ref { get; set; }
        public string act_ref_status { get; set; }  // Not Display
    }

    //public class SchedulerDetailViewModel
    //{
    //    public List<SelectListItem> vessel { get; set; }
    //    public string sch_laycan { get; set; }
    //    public string sch_date { get; set; }
    //    public string sch_sailing { get; set; }
    //    public List<SelectListItem> freight { get; set; }
    //    public string sch_laytime { get; set; }
    //    public List<SelectListItem> customer { get; set; }
    //    public string sch_note { get; set; }
    //}

    public class SchActivityExcel
    {
        public string name { get; set; }
        public string color { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
    }

    public class SchVesselExcel
    {
        public string vessel { get; set; }
        public List<SchActivityExcel> sch_activity_excel { get; set; }
    }       

    public class SchOtherActivityExcel
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class SchChartererExcel
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    public class RootObjectExcel
    {
        public List<SchVesselExcel> sch_vessel_excel { get; set; }
        public List<SchOtherActivityExcel> sch_other_activity_excel { get; set; }
        public List<SchChartererExcel> sch_charterer_excel { get; set; }
        public string note { get; set; }
        public string sp_note { get; set; }
    }

    public class SchActTimeLineExcel
    {
        public string act_id { get; set; }
        public string act_name { get; set; }
        public string act_color { get; set; }
        public string time_start { get; set; }
        public string time_end { get; set; }
    }

    public class RootObjectActivityExcel
    {
        public string vessel_id { get; set; }
        public string vessel_name { get; set; }
        public string period { get; set; }
        public string laycan { get; set; }
        public string charterer_id { get; set; }
        public string charterer_name { get; set; }
        public string charterer_color { get; set; }
        public List<SchActTimeLineExcel> sch_act_time_line_excel { get; set; }
    }
}