﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class SpecViewModel
    {
        public SpecViewModel_Search spec_Search { get; set; }

        public List<SelectListItem> ddl_Material { get; set; }
        public List<SelectListItem> ddl_Country { get; set; }
        public SpecViewModel_Detail specViewModel_Detail { get; set; }
        public bool? spec_compare { get; set; }
    }


    public class SpecViewModel_Search
    {
        public string sSpecRowId { get; set; }
        //public string sCountry { get; set; }
        //public string sMaterial { get; set; }

        public List<SpecViewModel_SearchData> sSearchData { get; set; }
    }


    public class SpecViewModel_SearchData
    {
        public string dSpecRowId { get; set; }
        public string dVersion { get; set; }  
        public string dReason { get; set; }   
        public DateTime dUpdatedDate { get; set; }
        public string dUpdatedBy { get; set; }  
        public DateTime dCreatedDate { get; set; }

    }




    public class SpecViewModel_Detail
    {
        public string RowId { get; set; }
        //public string Material { get; set; }
        //public string Country { get; set; }
        public string RowVersion { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Reason { get; set; }
        public List<SpecDetail> SpecDetail { get; set; }   
        public List<Spec_Note> SpecNote { get; set; }
    }

    public class SpecViewModel_Detail_Compared
    {
        public string RowId { get; set; }
        //public string Material { get; set; }
        //public string Country { get; set; }
        public string RowVersion { get; set; }
        public string Reason { get; set; }
        public List<ComparedSpecDetail> ComparedSpecDetail { get; set; }
        //compared spec       
        //public List<Spec_Note> SpecNote { get; set; }
    }

    

    public class Spec_Note
    {
        public string RowId { get; set; }
        public string SpecId { get; set; }
        public string Note { get; set; }
        public string Explanation { get; set; }
        public string Image { get; set; } = "";
        public string[] filename { get; set; }
        public string Order { get; set; }
        public int IntOrder { get; set; }
    }

    public class SpecDetail
    {
        public string RowId { get; set; }
        public string Order { get; set; }
        public int IntOrder { get; set; }
        //public int Order { get; set; }
        //public string Material { get; set; }
        public string Component { get; set; }
        public string Property { get; set; }
        public string Unit { get; set; }
        public string TestMethod { get; set; }
        public string Min1 { get; set; }
        public string Min2 { get; set; }
        public string Min3 { get; set; }
        public string Min4 { get; set; }
        public string Min5 { get; set; }
        public string Min6 { get; set; }
        public string Min7 { get; set; }
        public string Min8 { get; set; }
        public string Min9 { get; set; }
        public string Min10 { get; set; }
        public string Max1 { get; set; }
        public string Max2 { get; set; }
        public string Max3 { get; set; }
        public string Max4 { get; set; }
        public string Max5 { get; set; }
        public string Max6 { get; set; }
        public string Max7 { get; set; }
        public string Max8 { get; set; }
        public string Max9 { get; set; }
        public string Max10 { get; set; }

        public string RowVersion { get; set; }
        public string Source { get; set; }
        public string Constraint { get; set; }
        public string Remarks { get; set; }
        
    }


    public class ComparedSpecDetail
    {
        public string RowId { get; set; }
        public string Order { get; set; }
        public int IntOrder { get; set; }
        //public string Material { get; set; }
        public string Component { get; set; }
        public string Property { get; set; }
        public string Unit { get; set; }
        public string TestMethod { get; set; }
        public string Min1 { get; set; }
        public string Min2 { get; set; }
        public string Min3 { get; set; }
        public string Min4 { get; set; }
        public string Min5 { get; set; }
        public string Min6 { get; set; }
        public string Min7 { get; set; }
        public string Min8 { get; set; }
        public string Min9 { get; set; }
        public string Min10 { get; set; }
        public string Max1 { get; set; }
        public string Max2 { get; set; }
        public string Max3 { get; set; }
        public string Max4 { get; set; }
        public string Max5 { get; set; }
        public string Max6 { get; set; }
        public string Max7 { get; set; }
        public string Max8 { get; set; }
        public string Max9 { get; set; }
        public string Max10 { get; set; }

        public string RowVersion { get; set; }
        public string Source { get; set; }
        public string Constraint { get; set; }
        public string Remarks { get; set; }

    }
}