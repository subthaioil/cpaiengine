﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class SwapViewModel
    {
        public enum Mode
        {
            View,
            Create,
            Edit,
            None
        }

        public Mode PageMode { get; set; } = Mode.View;

        public SwapViewModel_Search Swap_Search { get; set; } = new SwapViewModel_Search();
        public List<SwapViewModel_Detail> Swap_Detail { get; set; } = new List<SwapViewModel_Detail>();
        public SwapViewModel_Import Swap_Import { get; set; } = new SwapViewModel_Import();


        public List<SelectListItem> ddl_Vessel { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_Product { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_Customer { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_Material { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_Plant { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_PriceStatus { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_Unit { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_UnitTotal { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_UnitPrice { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_UnitInvoice { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> ddl_CrudeName { get; set; } = new List<SelectListItem>();
        

        public string json_Crude { get; set; }
        public string json_CrudeAuto { get; set; }
    }

    public class SwapViewModel_Search
    {
        public string sTripNo { get; set; }
        public string sDeliveryDate { get; set; }
        public string sProduct { get; set; }
        public string sCustomer { get; set; }
        public string sCrudeName { get; set; }
        public string sDeliveryMonth { get; set; }
        public string sDeliveryYear { get; set; }
        public List<SwapViewModel_SearchData> SearchData { get; set; } = new List<SwapViewModel_SearchData>();
    }

    public class SwapViewModel_SearchData
    {
        public bool dIsSelect { get; set; }
        public string dTripNo { get; set; }
        public string dItemNo { get; set; }
        public string dCrude { get; set; }
        public string dCrudeTypeID { get; set; }
        public string dVessel { get; set; }
        public string dCustomer { get; set; }
        public string dShipTo { get; set; }
        public string dDeliveryDate { get; set; }
        public string dPlant { get; set; }
        public string dProduct { get; set; }
        public string dVolumeBBL { get; set; }
        public string dVolumeMT { get; set; }
        public string dVolumeLITE { get; set; }
        public string dProPrice { get; set; }
        public string dProUnit { get; set; }
        public string dActurePrice { get; set; }
        public string dActureUnit { get; set; }
        public string dPONo { get; set; }
        public string dSONo { get; set; }
        public string dDONo { get; set; }
        public string dMemo { get; set; }
        public string dFormulaPrice { get; set; }
        public string dIncoterm { get; set; }
        public string dQTYBBL { get; set; }
        public string dQTYMT { get; set; }
        public string dPurchaseTripNo { get; set; }
    }

    public class SwapViewModel_Detail
    {
        public string trip_no { get; set; }
        public string item_no { get; set; }
        public string po_no { get; set; }
        public string vessel_id { get; set; }
        public string sold_to { get; set; }
        public string ship_to { get; set; }
        public string delivery_date { get; set; }
        public string plant { get; set; }
        public string crude_type_id { get; set; }
        public string volumn_bbl { get; set; }
        public string volumn_mt { get; set; }
        public string volumn_lite { get; set; }
        public string outturn_bbl { get; set; }
        public string outturn_mt { get; set; }
        public string outturn_lite { get; set; }
        public string price_per { get; set; }
        public string price { get; set; }
        public string unit_id { get; set; }
        public string exchange_rate { get; set; }
        public string total { get; set; }
        public string unit_total { get; set; }
        public string type { get; set; }
        public string create_date { get; set; }
        public string create_time { get; set; }
        public string create_by { get; set; }
        public string last_modify_date { get; set; }
        public string last_modify_time { get; set; }
        public string last_modify_by { get; set; }
        public string status { get; set; }
        public string release { get; set; }
        public string tank { get; set; }
        public string sale_order { get; set; }
        public string price_release { get; set; }
        public string remark { get; set; }
        public string distri_chann { get; set; }
        public string table_name { get; set; }
        public string memo { get; set; }
        public string date_sap { get; set; }
        public string fi_doc { get; set; }
        public string do_no { get; set; }
        public string status_to_sap { get; set; }
        public string seq { get; set; }
        public string incoterm_type { get; set; }
        public string temp { get; set; }
        public string density { get; set; }
        public string storage_location { get; set; }
        public string supplier { get; set; }
        public string distri_chann_sap { get; set; }
        public string vat_nonvat { get; set; }
        public string update_do { get; set; }
        public string update_gi { get; set; }
        public string fi_doc_reverse { get; set; }
        public string price_status { get; set; }
        public List<SwapViewModel_Price> prices { get; set; } = new List<SwapViewModel_Price>();
    }

    public class SwapViewModel_Import
    {
        public string sTrip_no { get; set; }
        public string sCrude_type_id { get; set; }
        public List<SwapViewModel_ImportData> ImportData { get; set; } = new List<SwapViewModel_ImportData>();
    }

    public class SwapViewModel_ImportData {
        public string dTrip_no { get; set; }
        public string dItem_no { get; set; }
        public string dVessel_id { get; set; }
        public string dVessel_name { get; set; }
        public string dCrude_type_id { get; set; }
        public string dCrude_name { get; set; }
        public string dBl_date { get; set; }
        public string dVolumn_bbl { get; set; }
        public string dVolumn_mt { get; set; }
        public string dSupplier_id { get; set; }
        public string dSupplier_name { get; set; }
        public string dMatItemNo { get; set; }
    }

    public class SwapViewModel_Price
    {
        public string trip_no { get; set; }
        public string item_no { get; set; }
        public string crude_type_id { get; set; }
        public string num { get; set; }
        public string type { get; set; }
        public string price { get; set; }
        public string date_price { get; set; }
        public string unit_id { get; set; }
        public string release { get; set; }
        public string invoice { get; set; }
        public string memo { get; set; }
        public string unit_inv { get; set; }
    }
}