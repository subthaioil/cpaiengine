﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class ThroughputViewModel
    {
        public ThroughputViewModel_Search Throughput_Search { get; set; }
        public ThroughoutFeeViewModel_Detail Throughput_Detail { get; set; }

        public ThroughputFeeTransactionViewModel ThroughputFeeTransaction { get; set; }

        public string selectedTripNo { get; set; }

        public List<SelectListItem> ddl_Customer { get; set; }
        public string json_Customer { get; set; }

        public List<SelectListItem> ddl_Plant { get; set; }
        public string json_Plant { get; set; }

        public List<SelectListItem> ddl_Product { get; set; }
        public string json_Product { get; set; }

        public List<SelectListItem> ddl_Vessel { get; set; }
        public string json_Vessel { get; set; }

        public List<SelectListItem> ddl_Unit { get; set; }
        public string json_Unit { get; set; }

        public List<SelectListItem> ddl_Hour { get; set; }

        public List<SelectListItem> ddl_Min { get; set; }

        public List<SelectListItem> ddl_PriceStatus { get; set; }

        public List<SelectListItem> ddl_ExchangeRate_Type { get; set; }
        public string pageMode { get; set; }

        public ThroughputViewModel()
        {
            Throughput_Search = new ThroughputViewModel_Search();
            ThroughputFeeTransaction = new ThroughputFeeTransactionViewModel();
            ddl_Customer = new List<SelectListItem>();
            ddl_Plant = new List<SelectListItem>();
            ddl_Product = new List<SelectListItem>();
            ddl_Vessel = new List<SelectListItem>();
            ddl_Unit = new List<SelectListItem>();
        }

    }

    public class ThroughputViewModel_Search
    {
        public string sTripNo { get; set; }
        public string sDeliveryDate { get; set; }
        public string sPlant { get; set; }
        public string sCustomer { get; set; }
        public string sDeliveryDateFrom { get; set; }
        public string sDeliveryDateTo { get; set; }
        public string sDeliveryMonth { get; set; }
        public string sDeliveryYear { get; set; }

        public List<ThroughputViewModel_SearchData> SearchData { get; set; }
        public ThroughputViewModel_Search()
        {
            SearchData = new List<ThroughputViewModel_SearchData>();
        }

    }

    public class ThroughputViewModel_SearchData
    {
        public string dTripNo { get; set; }
        public string dCustomer { get; set; }
        public string dCompleted { get; set; }
        public string dVessel { get; set; }
        public string dQTYBBL { get; set; }
        public string dQTYMT { get; set; }
        public string dSalesOrder { get; set; }
        public string dMonth { get; set; }
        public string dYear { get; set; }
        public DateTime? dCompD { get; set; } 

        public string dProduct { get; set; }
        public string dDeliveryDate { get; set; }
        public string dPlant { get; set; }        
        public string dVolumeBBL { get; set; }
        public string dVolumeMT { get; set; }        
        public string dTotalAmount { get; set; }
        public string dPrice { get; set; }
        public string dSONo { get; set; }    
        public string txtCustomer { get; set; }   
        public string dLast { get; set; }
        public string dTripNoEncrypt { get; set; }
    }

    public class ThroughputFeeTransactionViewModel
    {
        public string sTripNo { get; set; }
        public string sCustomer { get; set; }
        public string sPlant { get; set; }
        public string sCompleteDate { get; set; }
        public string sRemark { get; set; }
        public ThroughputFeeTransactionDetailViewModel ThroughputFeeItem { get; set; }
        public List<ThroughputFeeTransactionDetailViewModel> ThroughputFeeTransactionList { get; set; }
        public ThroughputFeeTransactionViewModel()
        {
            ThroughputFeeItem = new ThroughputFeeTransactionDetailViewModel();
            ThroughputFeeTransactionList = new List<ThroughputFeeTransactionDetailViewModel>();
        }
    }

    public class ThroughoutFeeViewModel_Detail
    {
        public string dTripNo { get; set; }
        public string dCustomer { get; set; }
        public string dCompleteDate { get; set; }
        public string dHour { get; set; }
        public string dMin { get; set; }
        public string dVesselID { get; set; }
        public string dNetOutturnQTYBBL { get; set; }
        public string dNetOutturnQTYMT { get; set; }
        public string dNetOutturnQTYML { get; set; }
        public string dSalesUnit { get; set; }
        public string dShipTo { get; set; }
        public string dDueDate { get; set; }

        public List<ThroughputFeeViewModel_DetailList> ItemList { get; set; }
    }

    public class ThroughputFeeViewModel_DetailList
    {
        public string iIndex { get; set; }
        public string iPriceStatus { get; set; }
        public string iFixRate { get; set; }
        public string iTotalAmountUSD { get; set; }
        public string iTotalAmountTHB { get; set; }
        public string iCurrency { get; set; }
        public string iExchangeRate { get; set; }
        public string iExchangeRatePeriod { get; set; }
        public string iExchangeRateType { get; set; }
        public string iQtyMT { get; set; }
        public string iQtyML { get; set; }
        public string iQtyBBL { get; set; }
    }

    public class ThroughputFeeTransactionDetailViewModel
    {
        public string dTripNo { get; set; }
        public string dCustomer { get; set; }
        public string dCompleteDate { get; set; }
        public string dVesselID { get; set; }
        public string dNetOutturnQTYBBL { get; set; }
        public string dNetOutturnQTYMT { get; set; }
        public string dNetOutturnQTYML { get; set; }
        public string dSalesUnit { get; set; }
        public string dShipTo { get; set; }
        public string dDueDate { get; set; }

        public string dSEQ { get; set; }
        public string dProduct { get; set; }
        
        public string dVolume { get; set; }
        public string dPricePer { get; set; }
        public string dPricePerText { get; set; }
        public string dPrice { get; set; }
        public string dUnit { get; set; }
        public string dExchangeRate { get; set; }        
        public string dTotalAmount { get; set; }
        public string dPriceType { get; set; }
        public string dIsSelected { get; set; }
        //*****
        
        public string dUnitTotal { get; set; }
        public string dStatus { get; set; }
        public string dRelease { get; set; }
        public string dSaleOrder { get; set; }
        public string dStatusToSAP { get; set; }
        public string dPriceStatus { get; set; }
    }

    public class THROUGHPUT_FEE_CAL_ViewModel
    {
        public decimal input_Exchange_BuyingRate { get; set; }
        public decimal input_Exchange_SellingRate { get; set; }
        public decimal input_Exchange_AverageRate { get; set; }
        public decimal input_Volume_1_1 { get; set; }
        public decimal FixRate1_2 { get; set; }
        public decimal ServiceCharge1_3 { get; set; }
        public decimal BuyingRate1_4 { get; set; }
        public decimal ProServiceFeeBase1_5_3 { get; set; }
        public decimal ProServiceFeeRate1_5_5 { get; set; }
        public decimal FinalServiceFeeRate1_5_6 { get; set; }

        public decimal ServiceFee2_3 { get; set; }
        public decimal Vat2_4 { get; set; }
        public decimal Total2_5 { get; set; }

        public decimal ServiceFeeRate3_3 { get; set; }
        public decimal ServiceFeeContactAgeement3_4 { get; set; }
        public decimal ServiceFeeChargedInTax3_5 { get; set; }
        public decimal DebitNote3_6 { get; set; }


    }
    public class FormulaItemList
    {
        public List<FormulaCondition> ConditionItem { get; set; }
        public List<FormulaTier> FormulaTier { get; set; }
    }
    public class FormulaCondition
    {
        public string Symbol { get; set; }
        public string Variable1 { get; set; }
        public string Round1 { get; set; }
        public string Variable2 { get; set; }
        public string Round2 { get; set; }
        public string ResultOut { get; set; }
        public string ResultOut_Round { get; set; }

    }
    public class FormulaTier
    {
        public string Return_Value { get; set; }
        public string Compare_name { get; set; }
        public List<TierCondition> Compare_with { get; set; }
        public List<FormulaPricingCondition> ConditionItem { get; set; }
    }
    public class ThroughtputSetting
    {
        public THROUGHT_PUT_COMPANY_CONFIG THROUGHT_PUT_COMPANY_CONFIG { get; set; }
    }

    public class THROUGHT_PUT_COMPANY_CONFIG
    {
        public List<DATE_CONFIG> DATE_CONFIG { get; set; }
    }

    public class DATE_CONFIG
    {
        public string COMP_CODE { get; set; }
        public string COMP_DATE { get; set; }
        public string GET_HOLIDAY { get; set; }
        public string IF_MONDAY { get; set; }
    }

}