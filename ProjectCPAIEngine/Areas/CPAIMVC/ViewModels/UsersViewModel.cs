﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class UsersViewModel
    {
        public UsersViewModel_Search users_Search { get; set; }
        public UsersViewModel_Detail users_Detail { get; set; }

        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_TitleEN { get; set; }
        public List<SelectListItem> ddl_TitleTH { get; set; }
        public List<SelectListItem> ddl_ADLogin { get; set; }
        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_UserGroup { get; set; }
        public List<SelectListItem> ddl_UserGroupSystem { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
        public List<SelectListItem> ddl_Role { get; set; }
        public List<SelectListItem> ddl_User { get; set; }

        public string users_UserADJSON { get; set; }
        public string users_SystemJSON { get; set; }
        public string users_UsersGroupJSON { get; set; }
        public string users_UsersGroupSystemJSON { get; set; }
        public string users_UserRoleJSON { get; set; }
        public List<SelectListItem> userList { get; set; }
        public string sUser { get; set; }
    }

    public class UsersViewModel_Search
    {
        public string sUsersID { get; set; }
        public string sFirstNameEN { get; set; }
        public string sLastNameEN { get; set; }
        public string sUsersLogin { get; set; }
        public string sSectionHead { get; set; }
        public string sCompany { get; set; }
        public string sADLogin { get; set; }
        public string sStatus { get; set; }
        public List<UsersViewModel_SearchData> sSearchData { get; set; }
    }

    public class UsersViewModel_SearchData
    {
        public string dUsersID { get; set; }
        public string dTitleEN { get; set; }
        public string dTitleTH { get; set; }
        public string dFirstNameEN { get; set; }
        public string dLastNameEN { get; set; }
        public string dUsersLogin { get; set; }
        public string dSectionHead { get; set; }
        public string dCompany { get; set; }
        public string dADLogin { get; set; }
        public string dStatus { get; set; }
        public string dUserGroup { get; set; }
        public string dUserGroupSystem { get; set; }
        public string dSystem { get; set; }
        public string dEmployeeID { get; set; }
        public string dEmail { get; set; }
    }

    public class UsersViewModel_Detail
    {
        public string UsersID { get; set; }
        public string TitleEN { get; set; }
        public string TitleTH { get; set; }
        public string FirstNameEN { get; set; }
        public string LastNameEN { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public string ADLogin { get; set; }
        public string Password { get; set; }
        public string UsersLogin { get; set; }
        public string Company { get; set; }
        public string EmployeeID { get; set; }
        public string Email { get; set; }
        public string System { get; set; }
        public string Status { get; set; }
        public string UserGroup { get; set; }
        public string UserGroupSystem { get; set; }
        public string UsedFlag { get; set; }
        public string Role { get; set; }

        public string MobileFlag { get; set; }
        public string NotiFlag { get; set; }
        public string EmailFlag { get; set; }
        public string SMSFlag { get; set; }
        public string MobileMenu { get; set; }
        public string WebMenu { get; set; }

        public bool IsMobileFlag { get; set; }
        public bool IsNotiFlag { get; set; }
        public bool IsEmailFlag { get; set; }
        public bool IsSMSFlag { get; set; }

        public List<UsersViewModel_Control> Control { get; set; }
    }

    public class UsersViewModel_Control
    {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string UserGroup { get; set; }
        public string UserGroupSystem { get; set; }
        public string UserAffectEndDate { get; set; }
        public string UserAffectDate { get; set; }
        public string UserEndDate { get; set; }
        public string UsedFlag { get; set; }
        public string UsedDelegateFlag { get; set; }
        public string Status { get; set; }
    }
}