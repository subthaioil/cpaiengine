﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class VCoolTrackingViewModel
    {
        public List<VCoolEncrypt> VCoolTrackingTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> areaList { get; set; }
        public List<string> unitList { get; set; }
        public List<SelectListItem> country { get; set; }
        public List<SelectListItem> crudeCategories { get; set; }
        public List<SelectListItem> crudeKerogen { get; set; }
        public List<SelectListItem> crudeCharacteristic { get; set; }
        public List<SelectListItem> crudeMaturity { get; set; }
        public List<SelectListItem> area { get; set; }
        public List<SelectListItem> unit { get; set; }

        public string purchase_no { get; set; }
        public string sAssayRef { get; set; }
        public string sCountry { get; set; }
        public string sCrudeName { get; set; }
        public string sCrudeCategories { get; set; }
        public string sCrudeKerogen { get; set; }
        public string sCrudeCharacteristic { get; set; }
        public string sCrudeMaturity { get; set; }
        public string sArea { get; set; }
        public string sUnit { get; set; }


        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }

        public List<SelectListItem> ddlCountry { get; set; }
        public List<SelectListItem> ddlCrudeName { get; set; }

        public string tempDischargingDate { get; set; }
        public string tempLoadingDate { get; set; }
        public string tpc_plan_month { get; set; }
        public string tpc_plan_year { get; set; }
        public string tpc_plan_month_from { get; set; }

        public string tpc_plan_month_to { get; set; }
        public string tpc_plan_year_from { get; set; }
        public string tpc_plan_year_to { get; set; }
        public string products { get; set; }
        public string origin { get; set; }
        public string loading_from { get; set; }
        public string loading_to { get; set; }
        public string loading_date { get; set; }
        public string discharging_from { get; set; }
        public string discharging_to { get; set; }
    }
}