﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class VCoolViewModel
    {
        public static string FORMAT_DATE = "dd-MMM-yyyy";
        public static string FORMAT_DATETIME = "dd-MMM-yyyy HH:mm";
        public VcoRequesterInfo requester_info { get; set; }
        public VcoCrudeInfo crude_info { get; set; }
        public VcoComment comment { get; set; }
        public VcoEtaDateHistory date_history { get; set; }
        public VcoCrudeYieldComparison crude_compare { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        
        public List<SelectListItem> ddlCountry { get; set; }
        public List<SelectListItem> ddlPriority { get; set; }
        public List<SelectListItem> ddlCrudeName { get; set; }
        public List<SelectListItem> ddlPurchaseType { get; set; }
        public List<SelectListItem> ddlPurchaseMethod { get; set; }
        public List<SelectListItem> ddlBenchmarkPrice { get; set; }
        public List<SelectListItem> ddlIncoterm { get; set; }
        public List<SelectListItem> ddlSupplier { get; set; }
        public List<SelectListItem> ddlPurchaseResult { get; set; }
        public string tempDischargingDate { get; set; }
        public string tempLoadingDate { get; set; }
        public string tempCrudeYield { get; set; }
        public string tempFinalCAM { get; set; }
        public string tempFinalCAM_Encrypt { get; set; }

        ///LP Run
        public string CrudeYield { get; set; } 
        public List<SelectListItem> SelectedCrudeNameList { get; set; }
        public List<string> Selected { get; set; }
        public List<string> SelectedCrudeName { get; set; }
        public List<VCoolCompareCrude> CooSpiralList { get; set; }
        public string ProcessingPeriodDate { get; set; }

        ///SCSC Check Tank 
        public string PageStatus { get; set; } 
        public string Revise_dischangePeriod { get; set; }  

        //PCA
        public string stateModel { get; set; }

        public VCoolViewModel()
        {
            requester_info = new VcoRequesterInfo();
            crude_info = new VcoCrudeInfo();
            comment = new VcoComment();
            ddlPriority = new List<SelectListItem>();
            ddlCrudeName = new List<SelectListItem>();
            ddlPurchaseType = new List<SelectListItem>();
            ddlPurchaseMethod = new List<SelectListItem>();
            ddlBenchmarkPrice = new List<SelectListItem>();
            ddlIncoterm = new List<SelectListItem>(); 
            SelectedCrudeNameList = new List<SelectListItem>();
            Selected = new List<string>();
            SelectedCrudeName = new List<string>();
            CooSpiralList = new List<VCoolCompareCrude>();
            ddlPurchaseResult = new List<SelectListItem>();
            //crude_compare = new VcoCrudeYieldComparison();
        }
    }

    public class VCoolCompareCrude
    {
        public string header { get; set; }
        public string crude { get; set; }
        public string value { get; set; }
    }

    public class VCoolSearchViewModel
    {
        public string tpc_plan_month { get; set; }
        public string tpc_plan_year { get; set; }
        public string crude_name { get; set; }
        public string origin { get; set; }
        public string loading_from { get; set; }
        public string loading_to { get; set; }
        public string loading_date { get; set; }

        public List<VCoolEncrypt> VCoolTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> userList { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
    }

    public class VCoolReportCriteria
    {
        public string CrudeName { get; set; } = string.Empty;
        public string Country { get; set; } = string.Empty;
        public string Supplier { get; set; } = string.Empty;
        public string ActualPurchase { get; set; } = string.Empty;
        public string LoadingDates { get; set; } = string.Empty;
        public string DischargingDates { get; set; } = string.Empty;

        public string TPCMonthFrom { get; set; } = string.Empty;
        public string TPCYearFrom { get; set; } = string.Empty;
        public string TPCMonthTo { get; set; } = string.Empty;
        public string TPCYearTo { get; set; } = string.Empty;

        internal VCoolReportCriteria Clone()
        {
            var cultureInfo = new System.Globalization.CultureInfo("en-US");
            var abbrMonthDictionary = Enumerable.Range(0, 12).ToDictionary(i => cultureInfo.DateTimeFormat.AbbreviatedMonthNames[i], i => cultureInfo.DateTimeFormat.MonthNames[i]);
            var criteria = (VCoolReportCriteria)MemberwiseClone();
            if (!abbrMonthDictionary.ContainsKey(TPCMonthFrom))
            {
                criteria.TPCMonthFrom = null;
            }
            if (!abbrMonthDictionary.ContainsKey(TPCMonthTo))
            {
                criteria.TPCMonthTo = null;
            }
            var yearFrom = 0;
            var yearTo = 0;
            if (!int.TryParse(TPCYearFrom, out yearFrom))
            {
                criteria.TPCYearFrom = null;
            }
            if (!int.TryParse(TPCYearTo, out yearTo))
            {
                criteria.TPCYearTo = null;
            }
            return criteria;
        }
    }

    public class VCoolReportResult
    {
        public string TransactionID { get; set; } = string.Empty;
        public string CrudeName { get; set; } = string.Empty;
        public string Country { get; set; } = string.Empty;
        public string Supplier { get; set; } = string.Empty;
        public string SupplierImport { get; set; } = string.Empty;
        public string TPCMonthYear { get; set; } = string.Empty;
        public string ProposedPrice { get; set; } = string.Empty;
        public string ActualPrice { get; set; } = string.Empty;
        public string Volumn { get; set; } = string.Empty;
        public string ActualPurchase { get; set; } = string.Empty;
        public DateTime? PurchaseDate { get; set; } = null;
    }

    public class VCoolReportViewModel
    {
        public List<SelectListItem> CrudeNames { get; set; }
        public List<SelectListItem> Suppliers { get; set; }
        public List<SelectListItem> ActualPurchases { get; set; }
        public List<SelectListItem> Countries { get; set; }

        public VCoolReportCriteria Criteria { get; set; }
        public List<VCoolReportResult> VCoolReportResults { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;

        public VCoolReportViewModel()
        {
            CrudeNames = new List<SelectListItem>();
            Suppliers = new List<SelectListItem>();
            ActualPurchases = new List<SelectListItem>();
            Countries = new List<SelectListItem>();

            Criteria = new VCoolReportCriteria();
            VCoolReportResults = new List<VCoolReportResult>();
        }
    }
}