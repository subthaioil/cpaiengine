﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class VehicleViewModel
    {
        public VehicleViewModel_Search veh_Search { get; set; }
        public VehicleViewModel_Detail veh_Detail { get; set; }

        public List<SelectListItem> ddl_CreateType { get; set; }    // DropdownServiceModel.getCreateType
        public List<SelectListItem> ddl_Status { get; set; }        // DropdownServiceModel.getMasterStatus

        public List<SelectListItem> ddl_CtrlSystem { get; set; }    // DropdownServiceModel.getVehicleCtrlSystem
        public List<SelectListItem> ddl_CtrlType { get; set; }      // DropdownServiceModel.getVehicleCtrlType
        public List<SelectListItem> ddl_Language { get; set; }      // DropdownServiceModel.getLanguage

        public string veh_TypeJSON { get; set; }
        public string veh_OwnerJSON { get; set; }
        public string veh_GroupJSON { get; set; }

        public string veh_CtrlSystemJSON { get; set; }
        public string veh_CtrlTypeJSON { get; set; }

    }

    public class VehicleViewModel_Search
    {
        public string sVehicleID { get; set; }
        public string sVehicleName { get; set; }
        public string sVehicleText { get; set; }
        public string sCreateType { get; set; }
        public string sSystem { get; set; }
        public string sType { get; set; }
        public string sStatus { get; set; }
        public string sCreateDate { get; set; }
        public List<VehicleViewModel_SearchData> sSearchData { get; set; }
    }

    public class VehicleViewModel_SearchData
    {
        public string dCreateDate { get; set; }
        public string dVehicleID { get; set; }
        public string dVehicleName { get; set; }
        public string dVehicleText { get; set; }
        public string dCreateType { get; set; }
        public string dSystem { get; set; }
        public string dType { get; set; }
        public string dStatus { get; set; }
    }

    public class VehicleViewModel_Detail
    {
        public string VHE_CREATE_TYPE { get; set; } // Readonly
        public string VEH_ID { get; set; }          // Auto running
        // Require
        public string VEH_VEHICLE { get; set; }
        public string VEH_TYPE { get; set; }
        public string VEH_OWNER { get; set; }
        public string VEH_LANGUAGE { get; set; }
        public string VEH_VEH_TEXT { get; set; }
        public string VEH_GROUP { get; set; }
        // Show
        public string VEH_AGE { get; set; }
        public string VHE_DWT { get; set; }
        public string VHE_CAPACITY { get; set; }
        public string VHE_MAX_DRAFT { get; set; }
        public string VHE_COATING { get; set; }
        public string VHE_FULL_BALLAST { get; set; }
        public string VHE_FULL_LADEN { get; set; }
        public string VHE_ECO_BALLAST { get; set; }
        public string VHE_ECO_LADEN { get; set; }
        // Optional
        public string VEH_NRTUS { get; set; }
        public string VEH_VOL_UOM { get; set; }
        public string VEH_WGT_UOM { get; set; }
        public string VEH_DIM_UOM { get; set; }
        public string VEH_MAXWGT { get; set; }
        public string VEH_UNLWGT { get; set; }
        public string VEH_MAXVOL { get; set; }
        public string VEH_HEIGHT { get; set; }
        public string VEH_WIDTH { get; set; }
        public string VEH_LENGTH { get; set; }
        public string VEH_DEPOT { get; set; }
        public string VEH_NAME_EFF_DATE { get; set; }
        public string VEH_CLF_EFDT { get; set; }
        public string VEH_REG_CNTRY { get; set; }
        public string VEH_REG_DATE { get; set; }
        public string VEH_REG_OWNER { get; set; }
        public string VEH_OWN_FLAG { get; set; }
        public string VEH_BALLCAP { get; set; }
        public string VEH_FL_BNK_CAPA { get; set; }
        public string VEH_DSL_BNK_CAPA { get; set; }
        public string VEH_CLASS_GRP { get; set; }
        public string VEH_DRAFT { get; set; }
        public string VEH_POINT { get; set; }
        public string VEH_CRE_DATE { get; set; }
        public string VEH_CRE_NAME { get; set; }
        public string VEH_CHA_DATE { get; set; }
        public string VEH_CHA_NAME { get; set; }
        public string VEH_IMO_NUMBER { get; set; }
        public string VEH_CLASS_SOCIETY { get; set; }
        public string VEH_INSERT_DATE { get; set; }        
        public string VEH_VESSEL_CATEGORY { get; set; }
        public string VEH_BUILT { get; set; }

        // Not show
        public string VEH_STATUS { get; set; }
        public string VEH_CREATED_BY { get; set; }
        public string VEH_CREATED_DATE { get; set; }
        public string VEH_UPDATED_BY { get; set; }
        public string VEH_UPDATED_DATE { get; set; }

        public List<VehicleViewModel_Control> Control { get; set; }
    }

    public class VehicleViewModel_Control
    {
        public string RowID { get; set; }
        public string System { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
    }
}