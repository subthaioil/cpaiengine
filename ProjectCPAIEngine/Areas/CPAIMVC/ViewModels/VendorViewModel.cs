﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{

    public class VendorViewModel
    {
        public VendorViewModel_Seach vnd_Search { get; set; }
        public VendorViewModel_Detail vnd_Detail { get; set; }

        public List<SelectListItem> ddl_Country { get; set; }
        public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_Type { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
        public List<SelectListItem> ddl_Color { get; set; }

        public string vnd_SystemJSON { get; set; }
        public string vnd_TypeJSON { get; set; }
    }

    public class VendorViewModel_Seach
    {
        public string sVendorCode {get;set;}
        public string sVendorName { get; set; }
        public string sCommission { get; set; }
        public string sCountry { get; set; }
        public string sCreateType { get; set; }
        public string sSystem { get; set; }
        public string sType { get; set; }
        public string sStatus { get; set; }
        public List<VendorViewModel_SeachData> sSearchData { get; set; }
    }

    public class VendorViewModel_SeachData
    {
        public string dVendorCode { get; set; }
        public string dVendorName { get; set; }
        public string dCommission { get; set; }
        public string dCountry { get; set; }
        public string dCreateType { get; set; }
        public string dSystem { get; set; }
        public string dType { get; set; }
        public string dStatus { get; set; }
    }

    public class VendorViewModel_Detail
    {
        public string CreateType { get; set; } 
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string Commission { get; set; }
        public string CountryKey { get; set; }
        public string Contract { get; set; }
        public List<VendorViewModel_Control> Control { get; set; }
    }

    public class VendorViewModel_Control
    {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string System { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public string Status { get; set; }
    }

}