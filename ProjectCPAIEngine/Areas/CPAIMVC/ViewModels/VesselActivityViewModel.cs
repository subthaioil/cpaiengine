﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class VesselActivityViewModel
    {
        public VesselActivityViewModel_Seach mva_Search { get; set; }
        public VesselActivityViewModel_Detail mva_Detail { get; set; }

        public List<SelectListItem> ddl_System { get; set; }
        public List<SelectListItem> ddl_Type { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
        public List<SelectListItem> ddl_Color { get; set; }

        public string mva_SystemJSON { get; set; }
        public string mva_TypeJSON { get; set; }

        public string mva_CodeJSON { get; set; }
        public string mva_KeyJSON { get; set; }

    }

    public class VesselActivityViewModel_Seach
    {
        public string sVACode { get; set; }
        public string sVAStdPhraseEN { get; set; }
        public string sVAStdPhraseTH { get; set; }
        public string sVAKey { get; set; }
        public string sVACSystem { get; set; }
        public string sVACType { get; set; }
        public string sVACStatus { get; set; }
        public List<VesselActivityViewModel_SeachData> sSearchData { get; set; }
    }

    public class VesselActivityViewModel_SeachData
    {
        public string dVARowID { get; set; }
        public string dVACode { get; set; }
        public string dVAStdPhraseEN { get; set; }
        public string dVAStdPhraseTH { get; set; }
        public string dVAKey { get; set; }
        public string dVACSystem { get; set; }
        public string dVACType { get; set; }
        public string dVACStatus { get; set; }
    }

    public class VesselActivityViewModel_Detail
    {
        public string VARowID { get; set; }
        public string VACode { get; set; }
        public string VAStdPhraseEN { get; set; }
        public string VAStdPhraseTH { get; set; }
        public string VAKey { get; set; }
        public string VAStdTime { get; set; }
        //public string VAMinUse { get; set; }
        //public string VARef { get; set; }
        public bool VAMinUseFlag { get; set; }
        public bool VARefFlag { get; set; }
        public List<VesselActivityViewModel_Control> Control { get; set; }
    }

    public class VesselActivityViewModel_Schedule
    {
        public string act_RowID { get; set; }
        public string act_Code { get; set; }
        public string act_Key { get; set; }
        public string act_Name { get; set; }
        public string act_System { get; set; }
        public string act_Type { get; set; }
        public string act_Color { get; set; }
        public string act_RefStatus { get; set; }
        public string act_Status { get; set; }
    }

    public class VesselActivityViewModel_Control
    {
        public string VACRowID { get; set; }
        public string VACOrder { get; set; }
        public string VACSystem { get; set; }
        public string VACColor { get; set; }
        public string VACType { get; set; }
        public string VACStatus { get; set; }
    }
}