webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"loading\">\n  <div class=\"sk-cube-grid\">\n    <div class=\"sk-cube sk-cube1\"></div>\n    <div class=\"sk-cube sk-cube2\"></div>\n    <div class=\"sk-cube sk-cube3\"></div>\n    <div class=\"sk-cube sk-cube4\"></div>\n    <div class=\"sk-cube sk-cube5\"></div>\n    <div class=\"sk-cube sk-cube6\"></div>\n    <div class=\"sk-cube sk-cube7\"></div>ื\n    <div class=\"sk-cube sk-cube8\"></div>\n    <div class=\"sk-cube sk-cube9\"></div>\n  </div>\n</div>\n\n<router-outlet></router-outlet>\n\n<!--<div class=\"main-content\">-->\n  <!--<div class=\"main-content-area\">-->\n    <!--<div class=\"container body-content\">-->\n      <!--<router-outlet></router-outlet>-->\n    <!--</div>-->\n  <!--</div>-->\n<!--</div>-->\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: []
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pipes__ = __webpack_require__("../../../../../src/app/pipes/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__directives__ = __webpack_require__("../../../../../src/app/directives/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__containers_think_think_component__ = __webpack_require__("../../../../../src/app/containers/think/think.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__containers_playground_playground_component__ = __webpack_require__("../../../../../src/app/containers/playground/playground.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_input_time_input_time_component__ = __webpack_require__("../../../../../src/app/components/input-time/input-time.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_input_date_input_date_range_component__ = __webpack_require__("../../../../../src/app/components/input-date/input-date-range.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_input_date_input_date_component__ = __webpack_require__("../../../../../src/app/components/input-date/input-date.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_file_upload_file_upload_component__ = __webpack_require__("../../../../../src/app/components/file-upload/file-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_freetext_freetext_component__ = __webpack_require__("../../../../../src/app/components/freetext/freetext.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_input_suggestion_input_suggestion_component__ = __webpack_require__("../../../../../src/app/components/input-suggestion/input-suggestion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_shared_table_component__ = __webpack_require__("../../../../../src/app/components/_shared/table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__containers_demurrage_form_demurrage_form_component__ = __webpack_require__("../../../../../src/app/containers/demurrage-form/demurrage-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_demurrage_material_demurrage_material_component__ = __webpack_require__("../../../../../src/app/components/demurrage-material/demurrage-material.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_demurrage_time_summary_demurrage_time_summary_component__ = __webpack_require__("../../../../../src/app/components/demurrage-time-summary/demurrage-time-summary.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_demurrage_port_demurrage_port_component__ = __webpack_require__("../../../../../src/app/components/demurrage-port/demurrage-port.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_demurrage_value_usd_summary_demurrage_value_usd_summary_component__ = __webpack_require__("../../../../../src/app/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_input_date_time_input_date_time_component__ = __webpack_require__("../../../../../src/app/components/input-date-time/input-date-time.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_input_show_date_diff_input_show_date_diff_component__ = __webpack_require__("../../../../../src/app/components/input-show-date-diff/input-show-date-diff.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__components_cip_reference_cip_reference_component__ = __webpack_require__("../../../../../src/app/components/cip-reference/cip-reference.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_deduction_modal_deduction_modal_component__ = __webpack_require__("../../../../../src/app/components/deduction-modal/deduction-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__components_show_text_and_hrs_show_text_and_hrs_component__ = __webpack_require__("../../../../../src/app/components/show-text-and-hrs/show-text-and-hrs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__components_shared_show_table_col_text_and_hrs_component__ = __webpack_require__("../../../../../src/app/components/_shared/show-table-col-text-and-hrs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pipes_moment_pipe__ = __webpack_require__("../../../../../src/app/pipes/moment.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__components_input_hrs_input_hrs_component__ = __webpack_require__("../../../../../src/app/components/input-hrs/input-hrs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__components_input_suggestion_input_start_suggestion_component__ = __webpack_require__("../../../../../src/app/components/input-suggestion/input-start-suggestion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__guards_pending_changes_guard__ = __webpack_require__("../../../../../src/app/guards/pending-changes.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__components_input_suggestion_input_suggestion_ui_component__ = __webpack_require__("../../../../../src/app/components/input-suggestion/input-suggestion-ui.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// Routing Module































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* AppComponent */]
            ].concat(__WEBPACK_IMPORTED_MODULE_10__pipes__["b" /* pipes */], __WEBPACK_IMPORTED_MODULE_11__directives__["a" /* directives */], [
                __WEBPACK_IMPORTED_MODULE_13__containers_think_think_component__["a" /* ThinkComponent */],
                __WEBPACK_IMPORTED_MODULE_14__containers_playground_playground_component__["a" /* PlaygroundComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_input_time_input_time_component__["a" /* InputTimeComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_input_date_input_date_range_component__["a" /* InputDateRangeComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_input_date_input_date_component__["a" /* InputDateComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_file_upload_file_upload_component__["a" /* FileUploadComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_freetext_freetext_component__["a" /* FreetextComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_input_suggestion_input_suggestion_component__["a" /* InputSuggestionComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_shared_table_component__["a" /* TableComponent */],
                __WEBPACK_IMPORTED_MODULE_22__containers_demurrage_form_demurrage_form_component__["a" /* DemurrageFormComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_demurrage_material_demurrage_material_component__["a" /* DemurrageMaterialComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_demurrage_time_summary_demurrage_time_summary_component__["a" /* DemurrageTimeSummaryComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_demurrage_port_demurrage_port_component__["a" /* DemurragePortComponent */],
                __WEBPACK_IMPORTED_MODULE_26__components_demurrage_value_usd_summary_demurrage_value_usd_summary_component__["a" /* DemurrageValueUsdSummaryComponent */],
                __WEBPACK_IMPORTED_MODULE_27__components_input_date_time_input_date_time_component__["a" /* InputDateTimeComponent */],
                __WEBPACK_IMPORTED_MODULE_28__components_input_show_date_diff_input_show_date_diff_component__["a" /* InputShowDateDiffComponent */],
                __WEBPACK_IMPORTED_MODULE_29__components_cip_reference_cip_reference_component__["a" /* CipReferenceComponent */],
                __WEBPACK_IMPORTED_MODULE_30__components_deduction_modal_deduction_modal_component__["a" /* DeductionModalComponent */],
                __WEBPACK_IMPORTED_MODULE_31__components_show_text_and_hrs_show_text_and_hrs_component__["a" /* ShowTextAndHrsComponent */],
                __WEBPACK_IMPORTED_MODULE_32__components_shared_show_table_col_text_and_hrs_component__["a" /* ShowTableColTextAndHrsComponent */],
                __WEBPACK_IMPORTED_MODULE_33__pipes_moment_pipe__["a" /* MomentPipe */],
                __WEBPACK_IMPORTED_MODULE_34__components_input_hrs_input_hrs_component__["a" /* InputHrsComponent */],
                __WEBPACK_IMPORTED_MODULE_35__components_input_suggestion_input_start_suggestion_component__["a" /* InputStartSuggestionComponent */],
                __WEBPACK_IMPORTED_MODULE_37__components_input_suggestion_input_suggestion_ui_component__["a" /* InputSuggestionUiComponent */],
            ]),
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_7__app_routing__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["a" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["b" /* TypeaheadModule */].forRoot(),
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_http__["e" /* RequestOptions */], useClass: __WEBPACK_IMPORTED_MODULE_8__common__["c" /* CustomRequestOptions */] }
            ].concat(__WEBPACK_IMPORTED_MODULE_10__pipes__["b" /* pipes */], __WEBPACK_IMPORTED_MODULE_9__services__["c" /* services */], [
                __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DecimalPipe */],
                __WEBPACK_IMPORTED_MODULE_36__guards_pending_changes_guard__["a" /* PendingChangesGuard */],
                __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */],
            ]),
            bootstrap: [__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* AppComponent */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_21__components_shared_table_component__["a" /* TableComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__containers_think_think_component__ = __webpack_require__("../../../../../src/app/containers/think/think.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__containers_playground_playground_component__ = __webpack_require__("../../../../../src/app/containers/playground/playground.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__containers_demurrage_form_demurrage_form_component__ = __webpack_require__("../../../../../src/app/containers/demurrage-form/demurrage-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__guards_pending_changes_guard__ = __webpack_require__("../../../../../src/app/guards/pending-changes.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        canDeactivate: [__WEBPACK_IMPORTED_MODULE_5__guards_pending_changes_guard__["a" /* PendingChangesGuard */]],
        path: ':user_group/:req_transaction_id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_4__containers_demurrage_form_demurrage_form_component__["a" /* DemurrageFormComponent */],
        data: {
            title: ''
        },
    },
    {
        canDeactivate: [__WEBPACK_IMPORTED_MODULE_5__guards_pending_changes_guard__["a" /* PendingChangesGuard */]],
        path: ':user_group/:req_transaction_id/:transaction_id/edit/:is_duplicate',
        component: __WEBPACK_IMPORTED_MODULE_4__containers_demurrage_form_demurrage_form_component__["a" /* DemurrageFormComponent */],
        data: {
            title: ''
        },
    },
    {
        canDeactivate: [__WEBPACK_IMPORTED_MODULE_5__guards_pending_changes_guard__["a" /* PendingChangesGuard */]],
        path: ':user_group',
        component: __WEBPACK_IMPORTED_MODULE_4__containers_demurrage_form_demurrage_form_component__["a" /* DemurrageFormComponent */],
        data: {
            title: ''
        },
    },
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__containers_think_think_component__["a" /* ThinkComponent */],
        data: {
            title: ''
        },
    },
    {
        path: 'the/great/bank/playground',
        component: __WEBPACK_IMPORTED_MODULE_3__containers_playground_playground_component__["a" /* PlaygroundComponent */],
        data: {
            title: ''
        },
    },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forRoot(routes, { useHash: true })],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/common/button.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Button; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var Button = (function () {
    function Button() {
    }
    Button.getMessage = function (btn_name) {
        var message = "";
        switch (btn_name) {
            case "CANCEL":
                message = "Reason for cancel : The transaction cannot be reused.";
                break;
            case "REJECT":
                message = "Reason for reject : The transaction can be resubmit and approve again";
                break;
            case "APPROVE":
                message = "Reason for approve";
        }
        return message;
    };
    Button.getDefaultButton = function () {
        return [
            {
                "name": "SAVE DRAFT",
                "page_url": null,
                "call_xml": null
            },
            {
                "name": "SUBMIT",
                "page_url": null,
                "call_xml": null
            },
            {
                "name": "NO_PDF",
                "page_url": null,
                "call_xml": null
            },
        ];
    };
    Button.getCancelMessage = function () {
        return Button.getMessage('CANCEL');
    };
    Button.getRejectMessage = function () {
        return Button.getMessage('REJECT');
    };
    Button.getApproveMessage = function () {
        return Button.getMessage('APPROVE');
    };
    return Button;
}());



/***/ }),

/***/ "../../../../../src/app/common/constant.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BASE_API; });
var BASE_API = base_api.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');


/***/ }),

/***/ "../../../../../src/app/common/custom-request-options.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomRequestOptions; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var CustomRequestOptions = (function (_super) {
    __extends(CustomRequestOptions, _super);
    function CustomRequestOptions() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
        });
        return _this;
    }
    CustomRequestOptions = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], CustomRequestOptions);
    return CustomRequestOptions;
}(__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* BaseRequestOptions */]));



/***/ }),

/***/ "../../../../../src/app/common/daf-helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAFHelper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);


/**
 * Created by chusri on 25/11/2017 AD.
 */
var DAFHelper = (function () {
    function DAFHelper() {
    }
    DAFHelper.updatedModel = function (model, is_duplicate) {
        console.log("is_duplicate: " + is_duplicate);
        if (is_duplicate) {
            delete model.REQ_TRAN_ID;
            delete model.TRAN_ID;
            delete model.DDA_ROW_ID;
            delete model.DDA_FORM_ID;
            delete model.DDA_STATUS;
            delete model.DDA_REASON;
        }
        return model;
    };
    DAFHelper.CalculateTime = function (model) {
        // let result = Utility.clone(model);
        model.DDA_LAYTIME_CONTRACT_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetSumDiffHrsString(+model.DDA_LAYTIME_CONTRACT_HRS, 0);
        model.DAF_PORT = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.DAF_PORT, function (e) {
            // Origin
            e.DPT_RT_ORIGIN_CLAIM_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_ORIGIN_CLAIM_T_START, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            e.DPT_RT_ORIGIN_CLAIM_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_ORIGIN_CLAIM_T_START, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            // Top
            e.DPT_RT_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_TOP_REVIEWED_T_START, e.DPT_RT_TOP_REVIEWED_T_STOP);
            e.DPT_RT_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_TOP_REVIEWED_T_START, e.DPT_RT_TOP_REVIEWED_T_STOP);
            // Settle
            e.DPT_RT_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_SETTLED_T_START, e.DPT_RT_SETTLED_T_STOP);
            e.DPT_RT_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_SETTLED_T_START, e.DPT_RT_SETTLED_T_STOP);
            // saving
            e.DPT_RT_SAVING_T_START_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_SETTLED_T_START, e.DPT_RT_ORIGIN_CLAIM_T_START);
            e.DPT_RT_SAVING_T_START_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_SETTLED_T_START, e.DPT_RT_ORIGIN_CLAIM_T_START);
            e.DPT_RT_SAVING_T_STOP_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_SETTLED_T_STOP, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            e.DPT_RT_SAVING_T_STOP_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_SETTLED_T_STOP, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            e.DPT_RT_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDiffHrsString(e.DPT_RT_SAVING_T_START_HRS, e.DPT_RT_SAVING_T_STOP_HRS);
            e.DPT_RT_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDiffHrsHours(e.DPT_RT_SAVING_T_START_HRS, e.DPT_RT_SAVING_T_STOP_HRS);
            e.DAF_DEDUCTION_ACTIVITY = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                if (f.DDD_TYPE === 'fixed') {
                    f.DDD_OWNER_DEDUCTION_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetHoursFromText(f.DDD_OWNER_DEDUCTION_T_TEXT);
                    f.DDD_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetHoursFromText(f.DDD_TOP_REVIEWED_T_TEXT);
                    f.DDD_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetHoursFromText(f.DDD_SETTLED_T_TEXT);
                }
                else {
                    f.DDD_OWNER_DEDUCTION_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(f.DDD_OWNER_DEDUCTION_T_START, f.DDD_OWNER_DEDUCTION_T_STOP);
                    f.DDD_OWNER_DEDUCTION_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(f.DDD_OWNER_DEDUCTION_T_START, f.DDD_OWNER_DEDUCTION_T_STOP);
                    f.DDD_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(f.DDD_TOP_REVIEWED_T_START, f.DDD_TOP_REVIEWED_T_STOP);
                    f.DDD_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(f.DDD_TOP_REVIEWED_T_START, f.DDD_TOP_REVIEWED_T_STOP);
                    f.DDD_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(f.DDD_SETTLED_T_START, f.DDD_SETTLED_T_STOP);
                    f.DDD_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(f.DDD_SETTLED_T_START, f.DDD_SETTLED_T_STOP);
                }
                f.DDD_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsText(f.DDD_SETTLED_T_HRS, f.DDD_OWNER_DEDUCTION_T_HRS);
                f.DDD_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(f.DDD_SETTLED_T_HRS, f.DDD_OWNER_DEDUCTION_T_HRS);
                return f;
            });
            e.DPT_D_OWNER_DEDUCTION_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_OWNER_DEDUCTION_T_HRS || 0;
            });
            e.DPT_D_OWNER_DEDUCTION_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_OWNER_DEDUCTION_T_HRS);
            e.DPT_D_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_TOP_REVIEWED_T_HRS || 0;
            });
            e.DPT_D_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_TOP_REVIEWED_T_HRS);
            e.DPT_D_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_SETTLED_T_HRS || 0;
            });
            e.DPT_D_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_SETTLED_T_HRS);
            e.DPT_D_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_SAVING_T_HRS || 0;
            });
            e.DPT_D_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_SAVING_T_HRS);
            e.DPT_NTS_CLAIM_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_RT_ORIGIN_CLAIM_T_HRS, e.DPT_D_OWNER_DEDUCTION_T_HRS);
            e.DPT_NTS_CLAIM_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_CLAIM_T_HRS);
            e.DPT_NTS_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_RT_TOP_REVIEWED_T_HRS, e.DPT_D_TOP_REVIEWED_T_HRS);
            e.DPT_NTS_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_TOP_REVIEWED_T_HRS);
            e.DPT_NTS_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_RT_SETTLED_T_HRS, e.DPT_D_SETTLED_T_HRS);
            e.DPT_NTS_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_SETTLED_T_HRS);
            e.DPT_NTS_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_NTS_SETTLED_T_HRS, e.DPT_NTS_CLAIM_T_HRS);
            e.DPT_NTS_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_SAVING_T_HRS);
            return e;
        });
        // model = DAFHelper.SetValuePairForTextAndHours(model, 'DDA_TRT_ORIGIN_CLAIM');
        model.DDA_TRT_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_ORIGIN_CLAIM_T_HRS || 0;
        });
        model.DDA_TRT_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_ORIGIN_CLAIM_HRS);
        model.DDA_TRT_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_TOP_REVIEWED_T_HRS || 0;
        });
        model.DDA_TRT_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_TOP_REVIEWED_HRS);
        model.DDA_TRT_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_SETTLED_T_HRS || 0;
        });
        model.DDA_TRT_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_SETTLED_HRS);
        model.DDA_TRT_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_SAVING_T_HRS || 0;
        });
        model.DDA_TRT_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_SAVING_HRS);
        model.DDA_TDT_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_OWNER_DEDUCTION_T_HRS || 0;
        });
        model.DDA_TDT_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_ORIGIN_CLAIM_HRS);
        model.DDA_TDT_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_TOP_REVIEWED_T_HRS || 0;
        });
        model.DDA_TDT_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_TOP_REVIEWED_HRS);
        model.DDA_TDT_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_SETTLED_T_HRS || 0;
        });
        model.DDA_TDT_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_SETTLED_HRS);
        model.DDA_TDT_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_SAVING_T_HRS || 0;
        });
        model.DDA_TDT_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_SAVING_HRS);
        model.DDA_NTS_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TRT_ORIGIN_CLAIM_HRS, model.DDA_TDT_ORIGIN_CLAIM_HRS);
        model.DDA_NTS_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_ORIGIN_CLAIM_HRS);
        model.DDA_NTS_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TRT_TOP_REVIEWED_HRS, model.DDA_TDT_TOP_REVIEWED_HRS);
        model.DDA_NTS_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_TOP_REVIEWED_HRS);
        model.DDA_NTS_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TRT_SETTLED_HRS, model.DDA_TDT_SETTLED_HRS);
        model.DDA_NTS_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_SETTLED_HRS);
        model.DDA_NTS_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_SETTLED_HRS, model.DDA_NTS_ORIGIN_CLAIM_HRS);
        model.DDA_NTS_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_SAVING_HRS);
        model.DDA_LY_ORIGIN_CLAIM_HRS = model.DDA_LAYTIME_CONTRACT_HRS;
        model.DDA_LY_ORIGIN_CLAIM_TIME_TEXT = model.DDA_LAYTIME_CONTRACT_TEXT;
        model.DDA_LY_TOP_REVIEWED_HRS = model.DDA_LAYTIME_CONTRACT_HRS;
        model.DDA_LY_TOP_REVIEWED_TIME_TEXT = model.DDA_LAYTIME_CONTRACT_TEXT;
        model.DDA_LY_SETTLED_HRS = model.DDA_LAYTIME_CONTRACT_HRS;
        model.DDA_LY_SETTLED_TIME_TEXT = model.DDA_LAYTIME_CONTRACT_TEXT;
        model.DDA_LY_SAVING_HRS = 0;
        model.DDA_LY_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(0);
        model.DDA_TOD_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_ORIGIN_CLAIM_HRS, model.DDA_LY_ORIGIN_CLAIM_HRS);
        model.DDA_TOD_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_ORIGIN_CLAIM_HRS);
        model.DDA_TOD_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_TOP_REVIEWED_HRS, model.DDA_LY_TOP_REVIEWED_HRS);
        model.DDA_TOD_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_TOP_REVIEWED_HRS);
        model.DDA_TOD_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_SETTLED_HRS, model.DDA_LY_SETTLED_HRS);
        model.DDA_TOD_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_SETTLED_HRS);
        model.DDA_TOD_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TOD_SETTLED_HRS, model.DDA_TOD_ORIGIN_CLAIM_HRS);
        model.DDA_TOD_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_SAVING_HRS);
        model.DDA_TIME_ON_DEM_HRS = model.DDA_TOD_SETTLED_HRS;
        model.DDA_TIME_ON_DEM_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TIME_ON_DEM_HRS);
        model = DAFHelper.CalValue(model);
        return model;
    };
    DAFHelper.CalGross = function (hrs, pdpr, fraction) {
        var phpr = (pdpr || 0) / 24;
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["round"]((hrs * phpr), fraction);
        // return (hrs * phpr);
    };
    DAFHelper.CalAddressCom = function (value, address_com, fraction) {
        value = value || 0;
        address_com = address_com || 0;
        var result = value * address_com / 100;
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["round"]((result), fraction);
        // return (result);
    };
    DAFHelper.CalSharingToTop = function (value, percentage, fraction) {
        value = value || 0;
        percentage = percentage || 0;
        var result = value * percentage / 100;
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["round"]((result), fraction);
        // return result;
    };
    DAFHelper.CalValue = function (model) {
        model.DDA_GD_ORIGIN_CLAIM_VALUE = DAFHelper.CalGross(model.DDA_TOD_ORIGIN_CLAIM_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_GD_TOP_REVIEWED_VALUE = DAFHelper.CalGross(model.DDA_TOD_TOP_REVIEWED_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_GD_SETTLED_VALUE = DAFHelper.CalGross(model.DDA_TOD_SETTLED_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_GD_SAVING_VALUE = DAFHelper.CalGross(model.DDA_TOD_SAVING_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_AC_ORIGIN_CLAIM_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_ORIGIN_CLAIM_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_AC_TOP_REVIEWED_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_TOP_REVIEWED_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_AC_SETTLED_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_SETTLED_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_AC_SAVING_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_SAVING_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_STD_ORIGIN_CLAIM_VALUE = model.DDA_GD_ORIGIN_CLAIM_VALUE - model.DDA_AC_ORIGIN_CLAIM_VALUE;
        model.DDA_STD_TOP_REVIEWED_VALUE = model.DDA_GD_TOP_REVIEWED_VALUE - model.DDA_AC_TOP_REVIEWED_VALUE;
        model.DDA_STD_SETTLED_VALUE = model.DDA_GD_SETTLED_VALUE - model.DDA_AC_SETTLED_VALUE;
        model.DDA_STD_SAVING_VALUE = model.DDA_GD_SAVING_VALUE - model.DDA_AC_SAVING_VALUE;
        model.DDA_DST_ORIGIN_CLAIM_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_ORIGIN_CLAIM_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_DST_TOP_REVIEWED_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_TOP_REVIEWED_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_DST_SETTLED_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_SETTLED_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_DST_SAVING_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_SAVING_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_STD_ORIGIN_CLAIM_VALUE = model.DDA_STD_ORIGIN_CLAIM_VALUE || 0;
        model.DDA_DST_ORIGIN_CLAIM_VALUE = model.DDA_DST_ORIGIN_CLAIM_VALUE || 0;
        model.DDA_STD_TOP_REVIEWED_VALUE = model.DDA_STD_TOP_REVIEWED_VALUE || 0;
        model.DDA_DST_TOP_REVIEWED_VALUE = model.DDA_DST_TOP_REVIEWED_VALUE || 0;
        model.DDA_STD_SETTLED_VALUE = model.DDA_STD_SETTLED_VALUE || 0;
        model.DDA_DST_SETTLED_VALUE = model.DDA_DST_SETTLED_VALUE || 0;
        model.DDA_DST_SAVING_VALUE = model.DDA_DST_SAVING_VALUE || 0;
        model.DDA_STD_SAVING_VALUE = model.DDA_STD_SAVING_VALUE || 0;
        model.DDA_ND_ORIGIN_CLAIM_VALUE = +model.DDA_STD_ORIGIN_CLAIM_VALUE - +model.DDA_DST_ORIGIN_CLAIM_VALUE;
        model.DDA_ND_TOP_REVIEWED_VALUE = +model.DDA_STD_TOP_REVIEWED_VALUE - +model.DDA_DST_TOP_REVIEWED_VALUE;
        model.DDA_ND_SETTLED_VALUE = +model.DDA_STD_SETTLED_VALUE - +model.DDA_DST_SETTLED_VALUE;
        model.DDA_ND_SAVING_VALUE = +model.DDA_STD_SAVING_VALUE - +model.DDA_DST_SAVING_VALUE;
        model.DDA_GD_SETTLED_VALUE = model.DDA_GD_SETTLED_VALUE || 0;
        if (model.DDA_IS_BROKER_COMMISSION === 'Y') {
            model.DDA_BROKER_COMMISSION_PERCEN = model.DDA_BROKER_COMMISSION_PERCEN || null;
        }
        if (model.DDA_GD_SETTLED_VALUE && model.DDA_BROKER_COMMISSION_PERCEN) {
            // model.DDA_BROKER_COMMISSION_VALUE = _.round(model.DDA_GD_SETTLED_VALUE * model.DDA_BROKER_COMMISSION_PERCEN / 100, model.DDA_DECIMAL);
            model.DDA_BROKER_COMMISSION_VALUE = model.DDA_GD_SETTLED_VALUE * model.DDA_BROKER_COMMISSION_PERCEN / 100;
        }
        else {
            model.DDA_BROKER_COMMISSION_VALUE = 0;
        }
        model.DDA_NET_PAYMENT_DEM_VALUE = __WEBPACK_IMPORTED_MODULE_1_lodash__["clone"](model.DDA_ND_SETTLED_VALUE);
        model.DDA_NET_PAYMENT_DEM_VALUE_OV = model.DDA_IS_OVERRIDE_NET_PAYMENT == 'Y' ? model.DDA_NET_PAYMENT_DEM_VALUE_OV : __WEBPACK_IMPORTED_MODULE_1_lodash__["clone"](model.DDA_NET_PAYMENT_DEM_VALUE);
        return model;
    };
    return DAFHelper;
}());



/***/ }),

/***/ "../../../../../src/app/common/date-helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateHelper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DateHelper = (function () {
    function DateHelper() {
    }
    DateHelper_1 = DateHelper;
    DateHelper.DateRangeStringToDate = function (date_range_string) {
        return null;
    };
    DateHelper.DateStringToDate = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(date_string, DateHelper_1.from_format).toDate();
        // let date = new Date()  // this creates a date object in the user's timezone
        // date.setDate(Number(date_string.slice(0, 2)))
        // date.setMonth((NumberparseInt(date_string.slice(3, 5)) - 1))  // months a indexed at 0 in js
        // date.setYear(Number(date_string.slice(6, 10)))
        return date;
    };
    DateHelper.DateRangeStringToCsharpString = function (date_string) {
        var d = date_string.split(" to ");
        var date_start = d[0] && __WEBPACK_IMPORTED_MODULE_1_moment_moment__(d[0], DateHelper_1.from_format).format(DateHelper_1.to_format);
        var date_end = d[1] && __WEBPACK_IMPORTED_MODULE_1_moment_moment__(d[1], DateHelper_1.from_format).format(DateHelper_1.to_format);
        if (date_start == "Invalid date" || date_end == "Invalid date") {
            date_start = null;
            date_end = null;
        }
        return {
            date_start: date_start,
            date_end: date_end,
        };
    };
    DateHelper.DateStringToCsharpString = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(date_string, DateHelper_1.from_format).format(DateHelper_1.to_format);
        if (date == "Invalid date") {
            return null;
        }
        return date;
    };
    DateHelper.DateStringToCsharpWithMinuteString = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(date_string, DateHelper_1.from_format).format(DateHelper_1.to_format) + ' ' + __WEBPACK_IMPORTED_MODULE_1_moment_moment__().format('HH:mm:ss');
        if (date == "Invalid date") {
            return null;
        }
        // console.log(date);
        return date;
    };
    DateHelper.DateDotNetToCsharpString = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(date_string).format(DateHelper_1.to_format);
        if (date == "Invalid date") {
            return null;
        }
        return date;
    };
    DateHelper.DateToString = function (date) {
        var date_str = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(date).format(DateHelper_1.from_format);
        // console.log(date_str);
        if (date_str == "Invalid date") {
            date_str = "";
        }
        return date_str;
    };
    DateHelper.GetDiffHrsText = function (hours0, hours1) {
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"](DateHelper_1.GetDiffHrsHours(hours0, hours1), 'hours');
        return DateHelper_1.DurationToString(duration);
    };
    DateHelper.GetTextFromHours = function (hours) {
        // hours = _.round(hours, 4);
        hours = hours;
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"](hours, 'hours');
        return DateHelper_1.DurationToString(duration);
    };
    DateHelper.GetDiffHrsHours = function (hours0, hours1) {
        // return _.round(Math.abs(+hours0 - +hours1), 4);
        return Math.abs(+hours0 - +hours1);
    };
    DateHelper.GetSumDiffHrsString = function (hours0, hours1) {
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"]((hours0 + hours1), 'hours');
        return DateHelper_1.DurationToString(duration);
    };
    DateHelper.GetDiffDiffHrsString = function (hours0, hours1) {
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"]((hours0 - hours1), 'hours');
        return DateHelper_1.DurationToString(duration);
    };
    DateHelper.GetDiffDiffHrsHours = function (hours0, hours1) {
        return Math.abs(hours0 - hours1);
    };
    DateHelper.GetSumDiffHrsHours = function (hours0, hours1) {
        // let duration = moment.duration((hours0 + hours1), 'hours');
        // let days = _.floor(Math.abs(duration.asDays()), 0);
        // let hours = _.floor(Math.abs(duration.hours()), 0);
        // let minutes = _.floor(Math.abs(duration.minutes()), 0);
        // let message = days + " Days ";
        // message += hours + " Hrs ";
        // message += minutes + " Mins ";
        return (hours0 + hours1);
    };
    DateHelper.GetDiffDateString = function (from_str, to_str) {
        var from = (from_str && __WEBPACK_IMPORTED_MODULE_1_moment_moment__(from_str)) || __WEBPACK_IMPORTED_MODULE_1_moment_moment__(__WEBPACK_IMPORTED_MODULE_1_moment_moment__().format('YYYY-MM-DD HH:mm:00'));
        var to = (to_str && __WEBPACK_IMPORTED_MODULE_1_moment_moment__(to_str)) || __WEBPACK_IMPORTED_MODULE_1_moment_moment__(__WEBPACK_IMPORTED_MODULE_1_moment_moment__().format('YYYY-MM-DD HH:mm:00'));
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"](to.diff(from));
        return DateHelper_1.DurationToString(duration);
    };
    DateHelper.GetDiffHours = function (from_str, to_str) {
        var from = (from_str && __WEBPACK_IMPORTED_MODULE_1_moment_moment__(from_str)) || __WEBPACK_IMPORTED_MODULE_1_moment_moment__(__WEBPACK_IMPORTED_MODULE_1_moment_moment__().format('YYYY-MM-DD HH:mm:00'));
        var to = (to_str && __WEBPACK_IMPORTED_MODULE_1_moment_moment__(to_str)) || __WEBPACK_IMPORTED_MODULE_1_moment_moment__(__WEBPACK_IMPORTED_MODULE_1_moment_moment__().format('YYYY-MM-DD HH:mm:00'));
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"](to.diff(from));
        // return _.round(Math.abs(duration.asHours()), 4);
        return Math.abs(duration.asHours());
    };
    DateHelper.DurationToString = function (duration) {
        var days = __WEBPACK_IMPORTED_MODULE_2_lodash__["floor"](Math.abs(duration.asDays()), 0) || 0;
        var hours = __WEBPACK_IMPORTED_MODULE_2_lodash__["floor"](Math.abs(duration.hours()), 0) || 0;
        var minutes = __WEBPACK_IMPORTED_MODULE_2_lodash__["floor"](Math.abs(duration.minutes()), 0) || 0;
        // const second = _.round(Math.abs(duration.seconds()), 4) || 0;
        var second = Math.abs(duration.seconds()) || 0;
        if (second >= 30) {
            minutes = minutes + 1;
        }
        var message = days + ' Days ';
        message += hours + ' Hrs ';
        message += minutes + ' Mins ';
        return message;
    };
    DateHelper.GetNumberFromTextByType = function (text, type) {
        text = text || '';
        type = type || '';
        var factor = text.replace(' Days ', ',').replace(' Hrs ', ',').replace(' Mins', ',');
        var extracts = factor.split(',');
        switch (type) {
            case 'days':
                return (extracts && extracts.length > 0 && extracts[0]) || 0;
            case 'hrs':
                return (extracts && extracts.length > 1 && extracts[1]) || 0;
            case 'mins':
                return (extracts && extracts.length > 2 && extracts[2]) || 0;
        }
        return 0;
    };
    DateHelper.SetNumberToTextByType = function (text, type, value) {
        text = text || '';
        type = type || '';
        var factor = text.replace(' Days ', ',').replace(' Hrs ', ',').replace(' Mins', ',');
        var extracts = factor.split(',');
        var days = (extracts && extracts.length > 0 && extracts[0]) || 0;
        var hrs = (extracts && extracts.length > 1 && extracts[1]) || 0;
        var mins = (extracts && extracts.length > 2 && extracts[2]) || 0;
        switch (type) {
            case 'days':
                days = value || 0;
                break;
            case 'hrs':
                hrs = value || 0;
                break;
            case 'mins':
                mins = value || 0;
                break;
        }
        var message = days + ' Days ';
        message += hrs + ' Hrs ';
        message += mins + ' Mins';
        return message;
    };
    DateHelper.GetHoursFromText = function (text) {
        text = text || '';
        var factor = text.replace(' Days ', ',').replace(' Hrs ', ',').replace(' Mins', ',');
        var extracts = factor.split(',');
        var days = (extracts && extracts.length > 0 && extracts[0]) || 0;
        var hrs = (extracts && extracts.length > 1 && extracts[1]) || 0;
        var mins = (extracts && extracts.length > 2 && extracts[2]) || 0;
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"]({
            seconds: 0,
            minutes: mins,
            hours: hrs,
            days: days,
        });
        // return _.round(Math.abs(duration.asHours()), 4);
        return Math.abs(duration.asHours());
    };
    DateHelper.from_format = 'DD/MM/YYYY';
    DateHelper.to_format = 'YYYY-MM-DD';
    DateHelper = DateHelper_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], DateHelper);
    return DateHelper;
    var DateHelper_1;
}());

// var reformat = function (viewValue, from_format, to_format) {
//         console.log('viewValue', viewValue);
//         from_format = from_format || 'DD-MM-YYYY';
//         to_format = to_format || 'YYYY-MM-DD';
//         // var regex = /\d{2}-\d{2}-\d{4} to \d{2}-\d{2}-\d{4}/g;
//         // var result = regex.test(viewValue);
//         // if (!result) {
//         //   from_format = 'YYYY-MM-DD';
//         // }
//         // var dates = viewValue.split(" to ");
//         // var start_date = moment(dates[0], from_format).format(to_format);     // false (not a real month)
//         // var end_date = moment(dates[1], from_format).format(to_format);     // false (not a real month)
//         // new_date = start_date + " to " + end_date;
//         var date = moment(viewValue, from_format).format(to_format);
//         return date;
//       }
//       var reformat = function (viewValue, from_format, to_format) {
//         console.log('viewValue', viewValue);
//         from_format = from_format || 'DD-MM-YYYY';
//         to_format = to_format || 'YYYY-MM-DD';
//         // var regex = /\d{2}-\d{2}-\d{4} to \d{2}-\d{2}-\d{4}/g;
//         // var result = regex.test(viewValue);
//         // if (!result) {
//         //   from_format = 'YYYY-MM-DD';
//         // }
//         var dates = viewValue.split(" to ");
//         var start_date = moment(dates[0], from_format).format(to_format);     // false (not a real month)
//         var end_date = moment(dates[1], from_format).format(to_format);     // false (not a real month)
//         new_date = start_date + " to " + end_date;
//         return new_date;
//       }


/***/ }),

/***/ "../../../../../src/app/common/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__constant__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__utility__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__custom_request_options__ = __webpack_require__("../../../../../src/app/common/custom-request-options.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__custom_request_options__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_3__date_helper__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__button__ = __webpack_require__("../../../../../src/app/common/button.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_4__button__["a"]; });







/***/ }),

/***/ "../../../../../src/app/common/permission-store.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PermissionStore; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);

var PermissionStore = (function () {
    function PermissionStore() {
    }
    // public static IsCreator() : boolean {
    //   return !PermissionStore.IsBothRoles() && PermissionStore.IsRole('creator');
    // }
    // public static IsApprover() : boolean {
    //   return !PermissionStore.IsBothRoles() && PermissionStore.IsRole('approver');
    // }
    // public static IsBothRoles() : boolean {
    //   return PermissionStore.IsRole('approver') && PermissionStore.IsRole('creator');
    // }
    // public static IsAdmin() : boolean {
    //   return PermissionStore.IsRole('admin');
    // }
    // public static GetRole() : [string] {
    //   const roles_raw: string = localStorage.getItem('roles') || "";
    //   const roles: any = roles_raw.split(',');
    //   return roles;
    // }
    // public static IsRole(target: string) : boolean {
    //   let result: boolean = false;
    //   let roles: any = PermissionStore.GetRole();
    //   switch (target) {
    //     case "creator":
    //     case "creater":
    //     case "is_creater":
    //       result = _.includes(roles, 'Procurement User');
    //       break;
    //     case "approver":
    //     case "is_approver":
    //       result = _.includes(roles, 'Procurement Approver');
    //       break;
    //     case "admin":
    //       result = _.includes(roles, 'Admin');
    //       break;
    //     default:
    //       break;
    //   }
    //   return result;
    // }
    // public static IsDraft(model: any) :boolean {
    //   return model.STATE == 'DRAFT';
    // }
    // public static IsNoState(model: any) :boolean {
    //   return model.STATE == '' || model.STATE == null;
    // }
    // public static IsDenied(model: any) :boolean {
    //   return model.STATE == 'Denied';
    // }
    // public static IsRevising(model: any) :boolean {
    //   return model.STATE == 'Revising';
    // }
    // public static IsWaitingForAprove(model: any) : boolean {
    //   return model.STATE == 'Waiting for Approval';
    // }
    // public static IsCurrentUser(model: any) : boolean {
    //   let result = true;
    //   let username = localStorage.getItem('current_user');
    //   let select_history = _.find(model.FLOW_HISTORY, (e) => {
    //     e.UPDATED_BY = e.UPDATED_BY || "";
    //     return e.UPDATED_BY.toLowerCase() == username.toLowerCase();
    //   });
    //   console.log('select_history', select_history);
    //   if (select_history) {
    //     result = result && false;
    //   }
    //   return result;
    // }
    // public static IsAbleToComment(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsRole('approver');
    //   result = result && PermissionStore.IsWaitingForAprove(model);
    //   result = result && PermissionStore.IsCurrentUser(model);
    //   result = result && !PermissionStore.IsDocumentOwner(model);
    //   return result;
    // }
    // public static IsAbleToViewComment(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsDisabled(model);
    //   result = result || PermissionStore.IsRevising(model);
    //   return result;
    // }
    // public static IsDisabled(model: any) : boolean {
    //   // !(model.STATE == 'DRAFT' || model.STATE == 'Denied' || model.STATE == 'Revising' || model.STATE == '' ||  model.STATE == null );
    //   let result = true;
    //   result = result && !PermissionStore.IsDraft(model);
    //   result = result && !PermissionStore.IsRevising(model);
    //   // result = result && PermissionStore.IsDenied(model);
    //   // result = result && PermissionStore.IsWaitingForAprove(model);
    //   result = result && !PermissionStore.IsNoState(model);
    //   return result;
    // }
    // public static IsAbleToApproveOrReject(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsDisabled(model);
    //   result = result && PermissionStore.IsAbleToComment(model);
    //   return result;
    // }
    // public static IsDocumentOwner(model: any) : boolean {
    //   let username = localStorage.getItem('current_user');
    //   return model.CREATED_BY == username;
    // }
    // public static IsAbleToEdit(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsDisabled(model);
    //   result = result && PermissionStore.IsDocumentOwner(model);
    //   result = result && !PermissionStore.IsWaitingForAprove(model);
    //   return result;
    // }
    PermissionStore.CheckButtonText = function (button_list, target) {
        var index = __WEBPACK_IMPORTED_MODULE_0_lodash__["findIndex"](button_list, function (e) {
            return e.name == target;
        });
        return index != -1;
    };
    PermissionStore.IsAbleToSaveDraft = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'SAVE DRAFT');
        // if(!button_list || button_list.length == 0) {
        //   return true;
        // }
        return result;
    };
    PermissionStore.IsAbleToSubmit = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'SUBMIT');
        // result = result || PermissionStore.IsAbleToSaveDraft(button_list);
        return result;
    };
    PermissionStore.IsAbleToVerify = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'VERIFY');
        return result;
    };
    PermissionStore.IsAbleToEndorse = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'ENDORSE');
        return result;
    };
    PermissionStore.IsAbleToApprove = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'APPROVE');
        return result;
    };
    PermissionStore.IsAbleToCancel = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'CANCEL');
        return result;
    };
    PermissionStore.IsAbleToReject = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'REJECT');
        return result;
    };
    PermissionStore.IsAbleToGenPDF = function (button_list) {
        var result = true;
        result = result && !PermissionStore.CheckButtonText(button_list, 'NO_PDF');
        return result;
    };
    return PermissionStore;
}());



/***/ }),

/***/ "../../../../../src/app/common/utility.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utility; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Utility = (function () {
    function Utility() {
    }
    Utility.GetFileUrl = function (path) {
        path = path || '';
        path = path.replace('~', '');
        return __WEBPACK_IMPORTED_MODULE_2__constant__["a" /* BASE_API */] + "../" + path;
    };
    Utility.prototype.getTextByID = function (id, master) {
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](master, function (elem) {
            return elem.ID === id;
        });
        return s && s.VALUE;
    };
    Utility.getTextByID = function (id, master) {
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](master, function (elem) {
            return elem.ID === id;
        });
        return (s && s.VALUE) || '';
    };
    Utility.clone = function (source) {
        if (!source) {
            return null;
        }
        return JSON.parse(JSON.stringify(source));
    };
    Utility = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], Utility);
    return Utility;
}());



/***/ }),

/***/ "../../../../../src/app/components/_shared/show-table-col-text-and-hrs.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowTableColTextAndHrsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowTableColTextAndHrsComponent = (function () {
    function ShowTableColTextAndHrsComponent() {
        this.className = '';
        this.need_br = false;
        this.text_key = '';
        this.hrs_key = '';
    }
    ShowTableColTextAndHrsComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(ShowTableColTextAndHrsComponent.prototype, "setKey", {
        set: function (k) {
            this.text_key = k + '_TIME_TEXT';
            this.hrs_key = k + '_HRS';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], ShowTableColTextAndHrsComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowTableColTextAndHrsComponent.prototype, "className", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], ShowTableColTextAndHrsComponent.prototype, "need_br", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('key'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], ShowTableColTextAndHrsComponent.prototype, "setKey", null);
    ShowTableColTextAndHrsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-show-table-col-text-and-hrs',
            template: "\n    <ptx-show-text-and-hrs\n      [className]=\"className\"\n      [text]=\"model[text_key]\"\n      [hrs]=\"model[hrs_key]\"\n    ></ptx-show-text-and-hrs>\n  ",
            // template: `<td>
            //   <ptx-show-text-and-hrs
            //     [className]="className"
            //     [text]="model[text_key]"
            //     [hrs]="model[hrs_key]"
            //   ></ptx-show-text-and-hrs>
            //   <br *ngIf="need_br">
            // </td>`,
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], ShowTableColTextAndHrsComponent);
    return ShowTableColTextAndHrsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/_shared/table.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive mt-15 mb-30 pb-30\">\n  <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n    <tbody>\n      <tr>\n        <td rowspan=\"3\" class=\"rotate bg-navy ptx-w-40 pl-0 pr-0 text-white\"><div><span>Running Time</span></div></td>\n        <td class=\"text-bold text-center ptx-w-200\">Load Port Name</td>\n        <td class=\"text-bold text-center ptx-w-400\">Activity</td>\n        <td class=\"text-bold text-center ptx-w-320\">Original Claim</td>\n        <td class=\"text-bold text-center ptx-w-320\">TOP Reviewed</td>\n        <td class=\"text-bold text-center ptx-w-320\">Settled</td>\n        <td class=\"text-bold text-center ptx-w-320\">Saving</td>\n        <td rowspan=\"7\" class=\"ptx-w-50 text-center\">\n          <a class=\"btn-icon red\" href=\"javascript:void(0)\" (click)=\"removePort($event);\">\n            <i class=\"fa fa-times\" aria-hidden=\"true\"></i>\n          </a>\n        </td>\n      </tr>\n      <tr class=\"border-bottom-0\">\n        <td class=\"text-center\" valign=\"top\">\n          <input type=\"text\" class=\"form-control\" value=\"SRC Terminal\">\n        </td>\n        <td>\n          <div class=\"row mb-5\">\n            <div class=\"col-xs-4 pl-0\">\n              <input type=\"text\" class=\"form-control text-center text-sm\" value=\"Time Start\" disabled>\n            </div>\n            <div class=\"col-xs-8\">\n              <input type=\"text\" class=\"form-control text-center text-sm\" value=\"NOR+6\">\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-4 pl-0\">\n              <input type=\"text\" class=\"form-control text-center text-sm\" value=\"Time Stop\" disabled>\n            </div>\n            <div class=\"col-xs-8\">\n              <input type=\"text\" class=\"form-control text-center text-sm\" value=\"Hose Disconnect\">\n            </div>\n          </div>\n        </td>\n\n        <td *ngFor=\"let item of (modelKeys.runningTime); let i=index; let odd=odd; trackBy: trackById\" [class.odd]=\"odd\" >\n          <div *ngIf=\"item != 'saving'\">\n            <div class=\"row mb-5\">\n              <div class=\"col-xs-8\">\n                <ptx-input-date\n                  [date]=\"model.runningTime[item].startDate\"\n                  (modelChange)=\"triggerUpdate($event, 'runningTime.'+item+''); model.runningTime[item].startDate = $event;\"\n                  [disabled]=\"false\"\n                ></ptx-input-date>\n              </div>\n              <div class=\"col-xs-4 pl-0\">\n                <div class=\"form-inline\">\n                  <input type=\"text\" class=\"form-control text-center text-sm ptx-w-40\"\n                    [(ngModel)]=\"model.runningTime[item].startTime.hours\"\n                    pattern=\"[01]\\d|2[0-3]\" value=\"\">\n                </div>\n                <div class=\"text-inline\"> : </div>\n                <div class=\"form-inline\">\n                  <input type=\"text\" class=\"form-control text-center text-sm ptx-w-40\"\n                    [(ngModel)]=\"model.runningTime[item].startTime.mins\"\n                    pattern=\"[0-5]\\d\" value=\"\">\n                </div>\n              </div>\n            </div>\n            <div class=\"row mb-5\">\n              <div class=\"col-xs-8\">\n                <ptx-input-date\n                  [date]=\"model.runningTime[item].endDate\"\n                  (modelChange)=\"model.runningTime[item].endDate = $event;\"\n                  [disabled]=\"false\"\n                ></ptx-input-date>\n              </div>\n              <div class=\"col-xs-4 pl-0\">\n                <div class=\"form-inline\">\n                  <input type=\"text\" class=\"form-control text-center text-sm ptx-w-40\"\n                    [(ngModel)]=\"model.runningTime[item].endTime.hours\"\n                    pattern=\"[01]\\d|2[0-3]\" value=\"\">\n                </div>\n                <div class=\"text-inline\"> : </div>\n                <div class=\"form-inline\">\n                  <input type=\"text\" class=\"form-control text-center text-sm ptx-w-40\"\n                    [(ngModel)]=\"model.runningTime[item].endTime.mins\"\n                    pattern=\"[0-5]\\d\" value=\"\">\n                </div>\n              </div>\n            </div>\n            <hr>\n            <div class=\"row\">\n              <div class=\"col-xs-12\">\n                <input type=\"text\" class=\"form-control text-center text-sm ptx-w-100 m-zero-auto\"\n                    [(ngModel)]=\"model.runningTime[item].netHours\"\n                    value=\"\">\n              </div>\n            </div>\n          </div>\n          <div *ngIf=\"item == 'saving'\">\n            <div class=\"row mb-5\">\n              <div class=\"col-xs-8\">\n                <input type=\"text\" class=\"form-control text-center text-sm\" value=\"3 Days  16 Hrs  23 Mins\" disabled>\n              </div>\n              <div class=\"col-xs-4 pl-0\">\n                <input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled>\n              </div>\n            </div>\n            <div class=\"row\">\n              <div class=\"col-xs-8\">\n                <input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled>\n              </div>\n              <div class=\"col-xs-4 pl-0\">\n                <input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled>\n              </div>\n            </div>\n          </div>\n        </td>\n      </tr>\n      <tr class=\"border-top-0\">\n        <td class=\"text-center\">\n          Total Running Time\n        </td>\n        <td>\n        </td>\n        <td class=\"text-bold\" *ngFor=\"let item of (modelKeys.runningTime); let i=index; let odd=odd; trackBy: trackById\" [class.odd]=\"odd\">\n          <div class=\"row\" *ngIf=\"item != 'saving'\">\n            <div class=\"col-xs-8\">\n              <input ptxShowDateDiff\n                [startDate]=\"model.runningTime[item].startDate\"\n                [endDate]=\"model.runningTime[item].endDate\"\n                [startTimeHours]=\"model.runningTime[item].startTime.hours\"\n                [startTimeMins]=\"model.runningTime[item].startTime.mins\"\n                [endTimeHours]=\"model.runningTime[item].endTime.hours\"\n                [endTimeMins]=\"model.runningTime[item].endTime.mins\"\n                [netHours]=\"model.runningTime[item].netHours\"\n                type=\"text\" class=\"form-control text-center text-sm\" value=\"\" disabled>\n            </div>\n            <div class=\"col-xs-4 pl-0\">\n              <input ptxShowDateDiff\n                [startDate]=\"model.runningTime[item].startDate\"\n                [endDate]=\"model.runningTime[item].endDate\"\n                [startTimeHours]=\"model.runningTime[item].startTime.hours\"\n                [startTimeMins]=\"model.runningTime[item].startTime.mins\"\n                [endTimeHours]=\"model.runningTime[item].endTime.hours\"\n                [endTimeMins]=\"model.runningTime[item].endTime.mins\"\n                [netHours]=\"model.runningTime[item].netHours\"\n                [onlyHours]=\"true\"\n                type=\"text\" class=\"form-control text-center text-sm\" value=\"\" disabled>\n            </div>\n          </div>\n          <div class=\"row\" *ngIf=\"item == 'saving'\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n      </tr>\n\n      <tr>\n        <td rowspan=\"3\" class=\"rotate bg-red ptx-w-40 pl-0 pr-0 text-white\"><div><span>Deduction</span></div></td>\n        <td class=\"text-bold text-center ptx-w-200\">Load Port Name</td>\n        <td class=\"text-bold text-center ptx-w-400\">Activity</td>\n        <td class=\"text-bold text-center ptx-w-300\">Original Claim</td>\n        <td class=\"text-bold text-center ptx-w-300\">TOP Reviewed</td>\n        <td class=\"text-bold text-center ptx-w-300\">Settled</td>\n        <td class=\"text-bold text-center ptx-w-300\">Saving</td>\n      </tr>\n      <tr class=\"border-bottom-0\">\n        <td class=\"text-center\" valign=\"top\">\n          <input type=\"text\" class=\"form-control\" value=\"SRC Terminal\" disabled>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\">\n            <input type=\"text\" class=\"form-control text-left text-sm\" value=\"1. Shifting\" disabled>\n          </div>\n          <div class=\"row mb-5\">\n            <input type=\"text\" class=\"form-control text-left text-sm\" value=\"2. Internal stipping vessel's tanks into SL-P\" disabled>\n          </div>\n          <div class=\"row mb-5\">\n            <input type=\"text\" class=\"form-control text-left text-sm\" value=\"3. Average discharging pressure rate\" disabled>\n          </div>\n          <div class=\"row mt-15 text-center\"><a class=\"btn btn-default btn-sm\">Manage Activity</a></div>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n          <div class=\"row mb-5\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n          <div class=\"row mb-5\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n          <div class=\"row mb-5\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-60\" value=\"0\" disabled></div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n          <div class=\"row mb-5\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n      </tr>\n      <tr class=\"border-top-0\">\n        <td class=\"text-center\">\n          Total Deduction Time\n        </td>\n        <td>\n\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n      </tr>\n      <tr *ngIf=\"_isLast\">\n        <td class=\"border-left-0\"></td>\n        <td colspan=\"2\" class=\"border-right-0 text-left\">Net Time Spent</td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n            <div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>\n          </div>\n        </td>\n      </tr>\n\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/_shared/table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableComponent = (function () {
    function TableComponent(_element) {
        this.model = {
            runningTime: {
                originalClaim: {
                    startDate: new Date(),
                    startTime: { hours: '00', mins: '00' },
                    endDate: new Date(),
                    endTime: { hours: '00', mins: '00' },
                    netHours: ''
                },
                topReviewed: {
                    startDate: new Date(),
                    startTime: { hours: '00', mins: '00' },
                    endDate: new Date(),
                    endTime: { hours: '00', mins: '00' },
                    netHours: ''
                },
                settled: {
                    startDate: new Date(),
                    startTime: { hours: '00', mins: '00' },
                    endDate: new Date(),
                    endTime: { hours: '00', mins: '00' },
                    netHours: ''
                },
                saving: {
                    startDate: new Date(),
                    startTime: { hours: '00', mins: '00' },
                    endDate: new Date(),
                    endTime: { hours: '00', mins: '00' },
                    netHours: ''
                }
            },
            deduction: {}
        };
        this.modelKeys = {};
        this._idx = -1;
        this._isLast = false;
    }
    TableComponent.prototype.ngOnInit = function () {
        // setTimeout(() => {
        //   this.model = "ssssss";
        // }, 2000);
        //this.log();
        var keys = {};
        for (var key in this.model) {
            var tmp = [];
            var keyName = key;
            for (var k in this.model[key]) {
                tmp.push(k);
            }
            keys[keyName] = tmp;
        }
        this.modelKeys = keys;
    };
    TableComponent.prototype.log = function () {
        console.log(this.model);
    };
    TableComponent.prototype.triggerUpdate = function (value, modelObj) {
        var arr = modelObj.split('.');
        this.model[arr[0]][arr[1]]['netHours'] = '';
        //console.log('TRIGGER', value, modelObj, this.model[arr[0]][arr[1]]);
    };
    TableComponent.prototype.removePort = function (evt) {
        if (!confirm('Are you sure you want to remove this port ?')) {
            return;
        }
        console.log(this._ref);
        this._ref.destroy();
    };
    TableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-table',
            template: __webpack_require__("../../../../../src/app/components/_shared/table.component.html"),
            styles: [],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], TableComponent);
    return TableComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/cip-reference/cip-reference.component.html":
/***/ (function(module, exports) {

module.exports = "<button class=\"btn btn-success btn-sm text-light\" [disabled]=\"disabled\" (click)=\"modal.show()\">Search</button>\n\n\n<ng-template [ngIf]=\"IsCMMT()\">\n  <div bsModal #modal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h3 class=\"modal-title pull-left\">Search</h3>\n          <div class=\"exit\" (click)=\"modal.hide()\" role=\"button\" tabindex=\"0\"></div>\n          <!-- <button type=\"button\" class=\"close pull-right\" (click)=\"modal.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button> -->\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"row\">\n            <div class=\"col-lg-10 col-lg-offset-1\">\n              <div class=\"form-group\">\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Document No</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.DOC_NO\">\n                      </div>\n                  </div>\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Vessel Name</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.VESSEL_NAME\">\n                      </div>\n                  </div>\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Charterer</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.CHARTERER\">\n                      </div>\n                  </div>\n              </div>\n              <div class=\"form-group\">\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Laycan</label>\n                      <div class=\"col-md-7\">\n                        <ptx-input-date-range\n                            [disabled]=\"false\"\n                            [start_date]=\"from\"\n                            [end_date]=\"to\"\n                            (modelChange)=\"model.LAYCAN_FROM = $event.start_date;model.LAYCAN_TO = $event.end_date;\"\n                        >\n                        </ptx-input-date-range>\n                      </div>\n                  </div>\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Create By</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.CREATED_BY\">\n                      </div>\n                  </div>\n              </div>\n              <div class=\"text-center\" id=\"buttonDiv\">\n                <button class=\"btn btn-success\" (click)=\"DoSearch()\"> Search </button>\n              </div>\n            </div>\n          </div>\n          <br />\n          <div class=\"table-responsive\">\n            <table id=\"ptxDataTable\" class=\"tble table-hover table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">\n            </table>\n          </div>\n        </div>\n        <!--<div class=\"modal-footer\">-->\n          <!--n-->\n        <!--</div>-->\n      </div>\n    </div>\n  </div>\n</ng-template>\n\n<ng-template [ngIf]=\"!IsCMMT()\">\n  <div bsModal #modal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h3 class=\"modal-title pull-left\">Search</h3>\n          <div class=\"exit\" (click)=\"modal.hide()\" role=\"button\" tabindex=\"0\"></div>\n          <!-- <button type=\"button\" class=\"close pull-right\" (click)=\"modal.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button> -->\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"row\">\n            <div class=\"col-lg-10 col-lg-offset-1\">\n              <div class=\"form-group\">\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Trip No</label>\n                  <div class=\"col-md-7\">\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.DOC_NO\">\n                  </div>\n                </div>\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Vessel Name</label>\n                  <div class=\"col-md-7\">\n                    <input type=\"text\" class=\"form-control\"  [(ngModel)]=\"model.VESSEL_NAME\">\n                  </div>\n                </div>\n                <!--<div class=\"col-md-4\">-->\n                  <!--<label class=\"col-md-3 control-label\">Supplier</label>-->\n                  <!--<div class=\"col-md-7\">-->\n                    <!--<input type=\"text\" class=\"form-control\"  [(ngModel)]=\"model.SUPPLIER_NAME\">-->\n                  <!--</div>-->\n                <!--</div>-->\n              </div>\n              <div class=\"form-group\">\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Arrival Date</label>\n                  <div class=\"col-md-7\">\n                    <ptx-input-date-range\n                      [disabled]=\"false\"\n                      [start_date]=\"model.DISCHARGE_FROM\"\n                      [end_date]=\"model.DISCHARGE_TO\"\n                      (modelChange)=\"model.DISCHARGE_FROM = $event.start_date;model.DISCHARGE_TO = $event.end_date;\"\n                    >\n                    </ptx-input-date-range>\n                  </div>\n                </div>\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Loading Date</label>\n                  <div class=\"col-md-7\">\n                    <ptx-input-date-range\n                      [disabled]=\"false\"\n                      [start_date]=\"model.LOADING_FROM\"\n                      [end_date]=\"model.LOADING_TO\"\n                      (modelChange)=\"model.LOADING_FROM = $event.start_date;model.LOADING_TO = $event.end_date;\"\n                    >\n                    </ptx-input-date-range>\n                  </div>\n                </div>\n              </div>\n              <div class=\"text-center\" id=\"buttonDiv\">\n                <button class=\"btn btn-success\" (click)=\"DoSearch()\"> Search </button>\n              </div>\n            </div>\n          </div>\n          <br />\n          <div class=\"table-responsive\">\n            <table id=\"ptxDataTable\" class=\"tble table-hover table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">\n            </table>\n          </div>\n        </div>\n        <!--<div class=\"modal-footer\">-->\n        <!--n-->\n        <!--</div>-->\n      </div>\n    </div>\n  </div>\n</ng-template>\n"

/***/ }),

/***/ "../../../../../src/app/components/cip-reference/cip-reference.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CipReferenceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_cip_reference_service__ = __webpack_require__("../../../../../src/app/services/cip-reference.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_cho_reference_service__ = __webpack_require__("../../../../../src/app/services/cho-reference.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CipReferenceComponent = (function () {
    function CipReferenceComponent(el, cipReferenceService, choReferenceService) {
        this.el = el;
        this.cipReferenceService = cipReferenceService;
        this.choReferenceService = choReferenceService;
        this.config = {
            animated: true,
            keyboard: true,
            backdrop: true,
            ignoreBackdropClick: true
        };
        this.model = {};
        this.user_group = "";
        this.disabled = false;
        this.onSelectData = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.onSearchCIPRef = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    CipReferenceComponent.prototype.ngOnInit = function () {
        this.updateTable();
        // const context = this;
        // $(() => {
        //   const ptxDataTable = $(context.el.nativeElement).find("#ptxDataTable").DataTable({
        //     "data": context.data_set,
        //     "ordering": false,
        //     "bSort": false,
        //     "dom": '<"toolbar">rtip',
        //     "columnDefs": [
        //       {"className": "dt-center", "targets": "_all"}
        //     ],
        //     "columns": [
        //         {
        //           "title": "Trip No.",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Vessel Name",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Supplier",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Crude Name",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Incoterms",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Arrival Date",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Loading Date",
        //           "data": "id"
        //         },
        //     ],
        //   });
        //
        //   $(context.el.nativeElement).find("#ptxDataTable tbody tr").on("click", function(event){
        //     const select_data = ptxDataTable.row(this).data();
        //     context.onSelectData.emit(select_data);
        //     context.modal.hide();
        //   });
        //
        //   this.table = ptxDataTable;
        // });
    };
    CipReferenceComponent.prototype.IsCMMT = function () {
        return this.user_group.toUpperCase() === 'CMMT';
    };
    CipReferenceComponent.prototype.DoSearch = function () {
        var context = this;
        var model = context.model;
        console.log(model);
        var dialog = bootbox.dialog({
            title: 'Processing.....',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
        });
        dialog.init(function () {
            if (context.user_group.toUpperCase() === 'CMMT') {
                context.choReferenceService.search(model).subscribe(function (res) {
                    context.data_set = res;
                    context.updateTable();
                    // this.table.draw();
                    console.log(context.data_set);
                    dialog.modal('hide');
                }, function (error) {
                    console.log(error);
                    dialog.modal('hide');
                });
            }
            else if (context.user_group.toUpperCase() === 'CMCS') {
                context.cipReferenceService.getCIPCrudeData(model).subscribe(function (res) {
                    context.data_set = res;
                    context.updateTable();
                    // this.table.draw();
                    dialog.modal('hide');
                    console.log(context.data_set);
                }, function (error) {
                    console.log(error);
                    console.log(context.data_set);
                    dialog.modal('hide');
                });
            }
            else if (context.user_group.toUpperCase() === 'CMPS') {
                context.cipReferenceService.getCIPProductData(model).subscribe(function (res) {
                    context.data_set = res;
                    context.updateTable();
                    // this.table.draw();
                    dialog.modal('hide');
                    console.log(context.data_set);
                }, function (error) {
                    console.log(error);
                    console.log(context.data_set);
                    dialog.modal('hide');
                });
            }
            // setTimeout(function(){
            // dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');
            // }, 3000);
        });
    };
    CipReferenceComponent.prototype.updateTable = function () {
        var context = this;
        if (this.table) {
            this.table.destroy();
        }
        this.table = $(context.el.nativeElement).find("#ptxDataTable").DataTable({
            "data": context.data_set,
            "ordering": false,
            "bSort": true,
            "dom": '<"toolbar">rtip',
            "columnDefs": [
                { "className": "dt-center", "targets": "_all" }
            ],
            "columns": context.MappingColumn(),
        });
        // $(context.el.nativeElement).find("#ptxDataTable tbody tr").on("click", function(event){
        //   const select_data = context.table.row(this).data();
        //   context.onSelectData.emit(select_data);
        //   context.modal.hide();
        // });
        $(context.el.nativeElement).find('#ptxDataTable tbody').on('click', 'tr', function (event) {
            var select_data = context.table.row(this).data();
            context.onSelectData.emit(select_data);
            context.modal.hide();
        });
    };
    CipReferenceComponent.prototype.MappingColumn = function () {
        var result;
        if (this.IsCMMT()) {
            result = this.MappingCMMT();
        }
        else {
            result = this.MappingOther();
        }
        return result;
    };
    CipReferenceComponent.prototype.MappingCMMT = function () {
        return [
            {
                'title': 'Date',
                'data': 'SHOW_DOC_DATE'
            },
            {
                'title': 'Documnent No.',
                'data': 'TRIP_NO'
            },
            {
                'title': 'Vessel Name',
                'data': 'VESSEL_NAME'
            },
            {
                'title': 'Laycan',
                'data': 'SHOW_LAYCAN_DATE'
            },
            {
                'title': 'Charterer',
                'data': 'CUSTOMER_NAME'
            },
            {
                'title': 'Created By',
                'data': 'CREATED_BY'
            }
        ];
    };
    CipReferenceComponent.prototype.MappingOther = function () {
        return [
            {
                'title': 'Trip No.',
                'data': 'TRIP_NO'
            },
            {
                'title': 'Vessel Name',
                'data': 'VESSEL_NAME'
            },
            // {
            //   'title': 'Supplier',
            //   'data': 'SUPPLIER_NAME'
            // },
            {
                'title': 'Crude Name',
                'data': 'CRUDE_NAMES'
            },
            {
                'title': 'Incoterms',
                'data': 'INCOTERMS'
            },
            {
                'title': 'Arrival Date',
                'data': 'SHOW_ACTUAL_ETA'
            },
            {
                'title': 'Loading Date',
                'data': 'SHOW_LOADING_LAYCAN'
            },
            {
                'title': 'Created By',
                'data': 'CREATED_BY'
            }
        ];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */])
    ], CipReferenceComponent.prototype, "modal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CipReferenceComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], CipReferenceComponent.prototype, "user_group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], CipReferenceComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], CipReferenceComponent.prototype, "data_set", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CipReferenceComponent.prototype, "onSelectData", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CipReferenceComponent.prototype, "onSearchCIPRef", void 0);
    CipReferenceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-cip-reference',
            template: __webpack_require__("../../../../../src/app/components/cip-reference/cip-reference.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_2__services_cip_reference_service__["a" /* CipReferenceService */],
            __WEBPACK_IMPORTED_MODULE_3__services_cho_reference_service__["a" /* ChoReferenceService */]])
    ], CipReferenceComponent);
    return CipReferenceComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/deduction-modal/deduction-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"openModalWithClass(template)\">Open modal with custom css class</button>-->\n<div class=\"row mt-15 text-center\"><a class=\"btn btn-default btn-sm\" (click)=\"lgModal.show()\">Manage Activity</a></div>\n\n\n<div bsModal #lgModal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h3 class=\"modal-title pull-left\">Deduction</h3>\n        <div class=\"exit\" (click)=\"lgModal.hide()\" role=\"button\" tabindex=\"0\"></div>\n        <!--<button type=\"button\" class=\"close pull-right\" (click)=\"lgModal.hide()\" aria-label=\"Close\">-->\n          <!--<span aria-hidden=\"true\">&times;</span>-->\n        <!--</button>-->\n      </div>\n      <div class=\"modal-body\">\n        <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n          <tbody>\n            <tr>\n              <td class=\"text-bold text-center ptx-w-300\">Activity</td>\n              <td class=\"text-bold text-center ptx-w-100\"></td>\n              <td class=\"text-bold text-center ptx-w-300\">Owner Deduction</td>\n              <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n              <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n              <td class=\"text-bold text-center ptx-w-300\">Settled</td>\n              <td class=\"ptx-w-50 text-center\"></td>\n            </tr>\n            <ng-template ngFor let-e [ngForOf]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY\" let-j=\"index\">\n              <tr *ngIf=\"e.DDD_TYPE == 'fixed'\">\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <input type=\"text\"\n                           class=\"form-control\"\n                           [disabled]=\"model.IS_DISABLED\"\n                           [(ngModel)]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_ACTIVITY\"\n                    >\n                  </div>\n                </td>\n                <td valign=\"top\" >\n                </td>\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'days')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_OWNER_DEDUCTION_T_TEXT', 'days', $event);HandleCopyChange(index, j, 'origin');\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Day</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'hrs')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_OWNER_DEDUCTION_T_TEXT', 'hrs', $event);HandleCopyChange(index, j, 'origin');\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Hrs</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'mins')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_OWNER_DEDUCTION_T_TEXT', 'mins', $event);HandleCopyChange(index, j, 'origin');\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline\">Mins</div>\n                  </div>\n                </td>\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'days')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_TOP_REVIEWED_T_TEXT', 'days', $event);HandleCopyChange(index, j, 'top');OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Day</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'hrs')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_TOP_REVIEWED_T_TEXT', 'hrs', $event);HandleCopyChange(index, j, 'top');OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Hrs</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'mins')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_TOP_REVIEWED_T_TEXT', 'mins', $event);HandleCopyChange(index, j, 'top');OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline\">Mins</div>\n                  </div>\n                </td>\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'days')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_SETTLED_T_TEXT', 'days', $event);OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Day</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'hrs')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_SETTLED_T_TEXT', 'hrs', $event);OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Hrs</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'mins')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_SETTLED_T_TEXT', 'mins', $event);OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline\">Mins</div>\n                  </div>\n                </td>\n                <td class=\"ptx-w-50 text-center\">\n                  <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"RemoveRow(j)\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </td>\n              </tr>\n              <tr *ngIf=\"e.DDD_TYPE == 'interval'\">\n                <td valign=\"top\" >\n                  <input type=\"text\"\n                         class=\"form-control\"\n                         [disabled]=\"model.IS_DISABLED\"\n                         [(ngModel)]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_ACTIVITY\"\n                  >\n                </td>\n                <td>\n                  <div class=\"row mb-5\">\n                    <div class=\"col-xs-12 pl-0\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm\"\n                             value=\"Start\"\n                             disabled>\n                    </div>\n                  </div>\n                  <div class=\"row mb-5\">\n                    <div class=\"col-xs-12 pl-0\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm\"\n                             value=\"Stop\"\n                             disabled>\n                    </div>\n                  </div>\n                </td>\n                <td>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_START\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_START = $event; HandleCopyChange(index, j, 'origin'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_STOP\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_STOP = $event; HandleCopyChange(index, j, 'origin'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                </td>\n                <td>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START = $event; HandleCopyChange(index, j, 'top'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP = $event; HandleCopyChange(index, j, 'top'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                </td>\n                <td>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_START\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_START = $event; HandleCopyChange(index, j, 'settled'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_STOP\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_STOP = $event; HandleCopyChange(index, j, 'settled'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                </td>\n                <td class=\"ptx-w-50 text-center\">\n                  <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"RemoveRow(j)\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </td>\n              </tr>\n            </ng-template>\n            <!--<tr class=\"border-bottom-0 border-top-0\">-->\n              <!--<td valign=\"top\">-->\n              <!--</td>-->\n            <!--</tr>-->\n            <!--<tr class=\"border-top-0\">-->\n              <!--<td valign=\"top\">-->\n              <!--</td>-->\n            <!--</tr>-->\n            <!--<tr>-->\n              <!--<td valign=\"top\">-->\n              <!--</td>-->\n            <!--</tr>-->\n          </tbody>\n        </table>\n        <div class=\"wrapper-button text-left\">\n          <button class=\"btn btn-primary btn-xs\" (click)=\"DoAddFixedTime(index, j)\"> Add Fixed Time </button>\n          <button class=\"btn btn-primary btn-xs\" (click)=\"DoAddInterValTime(index, j)\"> Add Time Interval</button>\n        </div>\n        <div class=\"wrapper-button\" id=\"buttonDiv\">\n          <button class=\"btn btn-success\" (click)=\"OnDone();lgModal.hide()\"> Done </button>\n          <!--<button class=\"btn btn-default\" (click)=\"lgModal.hide()\"> Cancel </button>-->\n        </div>\n      </div>\n      <!--<div class=\"modal-footer text-center\">-->\n      <!--n-->\n      <!--</div>-->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/deduction-modal/deduction-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeductionModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_deduction_activity__ = __webpack_require__("../../../../../src/app/models/daf-deduction-activity.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__ = __webpack_require__("../../../../../src/app/common/daf-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DeductionModalComponent = (function () {
    function DeductionModalComponent(modalService) {
        this.modalService = modalService;
        this.model = new __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */]();
        this.index = 0;
        this.config = {
            animated: true,
            keyboard: true,
            backdrop: true,
            ignoreBackdropClick: true
        };
    }
    DeductionModalComponent.prototype.HandleCopyChange = function (index, j, type) {
        if (type === 'origin') {
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_START);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_STOP);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_HRS);
            this.HandleCopyChange(index, j, 'top');
        }
        else if (type === 'top') {
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_START = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_STOP = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_HRS);
        }
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    DeductionModalComponent.prototype.DoAddFixedTime = function (index, j) {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY.push(new __WEBPACK_IMPORTED_MODULE_3__models_daf_deduction_activity__["a" /* DAF_DEDUCTION_ACTIVITY */]('fixed'));
    };
    DeductionModalComponent.prototype.DoAddInterValTime = function (index, j) {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY.push(new __WEBPACK_IMPORTED_MODULE_3__models_daf_deduction_activity__["a" /* DAF_DEDUCTION_ACTIVITY */]('interval'));
    };
    DeductionModalComponent.prototype.GetText = function (text, type) {
        return __WEBPACK_IMPORTED_MODULE_4__common_date_helper__["a" /* DateHelper */].GetNumberFromTextByType(text, type);
    };
    DeductionModalComponent.prototype.SetText = function (index, j, key, type, value) {
        var old = this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j][key];
        this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j][key] = __WEBPACK_IMPORTED_MODULE_4__common_date_helper__["a" /* DateHelper */].SetNumberToTextByType(old, type, value);
        this.OnModelChange();
    };
    DeductionModalComponent.prototype.OnModelChange = function () {
        console.log('sss');
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    DeductionModalComponent.prototype.RemoveRow = function (j) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var target = this.model.DAF_PORT[this.index].DAF_DEDUCTION_ACTIVITY[j];
        this.model.DAF_PORT[this.index].DAF_DEDUCTION_ACTIVITY = __WEBPACK_IMPORTED_MODULE_6_lodash__["reject"](this.model.DAF_PORT[this.index].DAF_DEDUCTION_ACTIVITY, function (e) {
            return e == target;
        });
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    DeductionModalComponent.prototype.openModal = function (template) {
        this.modalRef = this.modalService.show(template, this.config);
    };
    DeductionModalComponent.prototype.openModalWithClass = function (template) {
        this.modalRef = this.modalService.show(template, Object.assign({}, this.config, { class: 'gray modal-fullscreen' }));
    };
    DeductionModalComponent.prototype.OnDone = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */])
    ], DeductionModalComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], DeductionModalComponent.prototype, "index", void 0);
    DeductionModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-deduction-modal',
            template: __webpack_require__("../../../../../src/app/components/deduction-modal/deduction-modal.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* BsModalService */]])
    ], DeductionModalComponent);
    return DeductionModalComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/demurrage-material/demurrage-material.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-4 col-sm-2\">\n  <div class=\"form-group\">\n    <label class=\"col-xs-12 control-label pt-0\">\n      <div class=\"text-inline\">Type of</div>\n      <div class=\"form-inline\">\n        <select class=\"form-control\"\n                [disabled]=\"model.IS_DISABLED\"\n                [(ngModel)]=\"model.DDA_MATERIAL_TYPE\"\n        >\n          <option value=\"Crude & F/S\"> Crude & F/S </option>\n          <option value=\"Product\"> Product </option>\n        </select>\n      </div>\n    </label>\n  </div>\n</div>\n\n<div class=\"col-xs-6\">\n  <!--<div class=\"form-group\">-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<select class=\"form-control\">-->\n        <!--<option value=\"\"> Product </option>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0 text-right\">-->\n      <!--<div class=\"text text-black\">BL Date</div>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<ptx-input-date-->\n        <!--[date]=\"model.DOC_DATE\"-->\n        <!--(modelChange)=\"model.DOC_DATE = $event;\"-->\n        <!--[disabled]=\"true\"-->\n      <!--&gt;</ptx-input-date>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0\">-->\n\n    <!--</div>-->\n  <!--</div>-->\n  <!--<div class=\"form-group\">-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<select class=\"form-control\">-->\n        <!--<option value=\"\"> Product </option>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0 text-right\">-->\n      <!--<div class=\"text text-black\">BL Date</div>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<ptx-input-date-->\n        <!--[date]=\"model.DOC_DATE\"-->\n        <!--(modelChange)=\"model.DOC_DATE = $event;\"-->\n        <!--[disabled]=\"true\"-->\n      <!--&gt;</ptx-input-date>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0\">-->\n      <!--<a class=\"btn-icon red\" href=\"javascript:void(0)\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></a>-->\n    <!--</div>-->\n  <!--</div>-->\n  <!--<div class=\"form-group\">-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<select class=\"form-control\">-->\n        <!--<option value=\"\"> Product </option>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0 text-right\">-->\n      <!--<div class=\"text text-black\">BL Date</div>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<ptx-input-date-->\n        <!--[date]=\"model.DOC_DATE\"-->\n        <!--(modelChange)=\"model.DOC_DATE = $event;\"-->\n        <!--[disabled]=\"true\"-->\n      <!--&gt;</ptx-input-date>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0\">-->\n      <!--<a class=\"btn-icon red\" href=\"javascript:void(0)\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></a>-->\n    <!--</div>-->\n  <!--</div>-->\n\n  <div class=\"form-group\" *ngFor=\"let elem of model.DAF_MATERIAL; let index = index;\">\n    <div class=\"col-xs-4\">\n      <ptx-input-suggestion\n        [model]=\"model.DAF_MATERIAL[index].DMT_MET_NUM\"\n        [data_list]=\"master.MATERIALS\"\n        [is_disabled]=\"model.IS_DISABLED\"\n        [show_field]=\"'VALUE'\"\n        select_field=\"ID\"\n        (ModelChanged)=\"model.DAF_MATERIAL[index].DMT_MET_NUM = $event;\"\n      >\n      </ptx-input-suggestion>\n    </div>\n    <div class=\"col-xs-2 p-0 text-right\">\n      <div class=\"text text-black\">B/L Date</div>\n    </div>\n    <div class=\"col-xs-4\">\n      <ptx-input-date\n        [date]=\"model.DAF_MATERIAL[index].DMT_BL_DATE\"\n        (modelChange)=\"model.DAF_MATERIAL[index].DMT_BL_DATE = $event;\"\n        [disabled]=\"model.IS_DISABLED\"\n      ></ptx-input-date>\n    </div>\n    <div class=\"col-xs-2 p-0\">\n      <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"DoRemove(index, elem)\">\n        <span class=\"glyphicon glyphicon-trash\"></span>\n      </a>\n    </div>\n  </div>\n  <div class=\"form-group\">\n    <div class=\"col-xs-12\">\n      <button class=\"btn btn-success btn-sm\" (click)=\"DoAdd()\">Add</button>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/demurrage-material/demurrage-material.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageMaterialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_material__ = __webpack_require__("../../../../../src/app/models/daf-material.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_master__ = __webpack_require__("../../../../../src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DemurrageMaterialComponent = (function () {
    function DemurrageMaterialComponent(utility) {
        this.utility = utility;
    }
    DemurrageMaterialComponent.prototype.ngOnInit = function () {
    };
    DemurrageMaterialComponent.prototype.DoAdd = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var mat = new __WEBPACK_IMPORTED_MODULE_2__models_daf_material__["a" /* DAF_MATERIAL */]();
        this.model.DAF_MATERIAL.push(mat);
    };
    DemurrageMaterialComponent.prototype.DoRemove = function (index, e) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var met_num = e && e.DMT_MET_NUM;
        console.log(met_num);
        var mat = this.utility.getTextByID(met_num, this.master.MATERIALS);
        console.log(mat);
        var popup_content = "Are you sure you want to remove this product/crude?<br /><br /><b>Product/Crude Name:</b> " + mat;
        __WEBPACK_IMPORTED_MODULE_4_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            var mat = context.model.DAF_MATERIAL[index];
            context.model.DAF_MATERIAL = __WEBPACK_IMPORTED_MODULE_5_lodash__["reject"](context.model.DAF_MATERIAL, function (e) {
                return e === mat;
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], DemurrageMaterialComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__models_daf_master__["a" /* DAFMaster */])
    ], DemurrageMaterialComponent.prototype, "master", void 0);
    DemurrageMaterialComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-material',
            template: __webpack_require__("../../../../../src/app/components/demurrage-material/demurrage-material.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */]])
    ], DemurrageMaterialComponent);
    return DemurrageMaterialComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/demurrage-port/demurrage-port.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"p-0 col-xs-12 mb-30\">\n  <div class=\"text-bold\"> {{ title }}</div>\n  <div *ngFor=\"let e of model.DAF_PORT|portsFilter:type\" class=\"table-responsive mt-15 mb-30 pb-30\">\n    <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n      <tbody>\n      <tr>\n        <td rowspan=\"3\" class=\"rotate bg-navy ptx-w-40 pl-0 pr-0 text-white\"><div><span>Running Time</span></div></td>\n        <td class=\"text-bold text-center ptx-w-150\">{{ title }} Name</td>\n        <td class=\"text-bold text-center ptx-w-250\">Activity (Free text)</td>\n        <td class=\"text-bold text-center ptx-w-320\">Original Claim/Original Time Spent</td>\n        <td class=\"text-bold text-center ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n        <td class=\"text-bold text-center ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n        <td class=\"text-bold text-center ptx-w-320\">Settled</td>\n        <td class=\"text-bold text-center ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Original)</td>\n        <td rowspan=\"7\" class=\"ptx-w-50 text-center\">\n          <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"removePort(e)\">\n            <span class=\"glyphicon glyphicon-trash\"></span>\n          </a>\n          <!--<a class=\"btn-icon red\" href=\"javascript:void(0)\" (click)=\"removePort.emit($event);\">-->\n            <!--<i class=\"fa fa-times\" aria-hidden=\"true\"></i>-->\n          <!--</a>-->\n        </td>\n      </tr>\n      <tr class=\"border-bottom-0\">\n        <td class=\"text-center\" valign=\"top\">\n          <ptx-input-suggestion\n            *ngIf=\"IsCMCS()\"\n            [model]=\"e.DPT_LOAD_PORT\"\n            [data_list]=\"master.PORTS\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT = $event;\"\n          >\n          </ptx-input-suggestion>\n          <ptx-input-suggestion\n            *ngIf=\"!IsCMCS() && type === 'load'\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_L\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion>\n          <ptx-input-suggestion\n            *ngIf=\"!IsCMCS() && type === 'discharge'\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_D\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion>\n        </td>\n        <td>\n          <div class=\"row mb-5\">\n            <div class=\"col-xs-3 pl-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm\"\n                     value=\"Start\"\n                     disabled>\n            </div>\n            <div class=\"col-xs-9\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_T_START_ACTIVITY\"\n              >\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-3 pl-0\">\n              <input type=\"text\" class=\"form-control text-center text-sm\" value=\"Stop\" disabled>\n            </div>\n            <div class=\"col-xs-9\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_T_STOP_ACTIVITY\"\n              >\n            </div>\n          </div>\n        </td>\n        <!--origin-->\n        <td>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_START\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_START = $event; HandleCopyChange(e, 'origin'); OnModelChange();\"\n          ></ptx-input-date-time>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_STOP\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_STOP = $event; HandleCopyChange(e, 'origin'); OnModelChange();\"\n          ></ptx-input-date-time>\n        </td>\n        <!--top review-->\n        <td>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START = $event; HandleCopyChange(e, 'top'); OnModelChange();\"\n          ></ptx-input-date-time>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP = $event; HandleCopyChange(e, 'top'); OnModelChange();\"\n          ></ptx-input-date-time>\n        </td>\n        <!--Settled review-->\n        <td>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_START\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_START = $event; HandleCopyChange(e, 'settled'); OnModelChange();\"\n          ></ptx-input-date-time>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_STOP\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_STOP = $event; HandleCopyChange(e, 'settled'); OnModelChange();\"\n          ></ptx-input-date-time>\n        </td>\n        <!--saving-->\n        <td *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row mb-5\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_TEXT\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_HRS)\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n          </div>\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_TEXT\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_HRS)\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <tr class=\"border-top-0\">\n        <td class=\"text-center\">\n          Total Running Time\n        </td>\n        <td>\n        </td>\n        <!--origin-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--top review-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--Settled review-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--saving-->\n        <td class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n      </tr>\n\n      <!--deduction-->\n      <tr>\n        <td rowspan=\"3\" class=\"rotate bg-red ptx-w-40 pl-0 pr-0 text-white\"><div><span>Deduction</span></div></td>\n        <td class=\"text-bold text-center ptx-w-200\">{{title}} Name</td>\n        <td class=\"text-bold text-center ptx-w-400\">Activity</td>\n        <td class=\"text-bold text-center ptx-w-300\">Owner Deduction</td>\n        <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n        <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n        <td class=\"text-bold text-center ptx-w-300\">Settled</td>\n        <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Owner)</td>\n      </tr>\n      <tr class=\"border-bottom-0\">\n        <td class=\"text-center\" valign=\"top\">\n          <!--<ptx-input-suggestion-->\n            <!--[model]=\"e.DPT_LOAD_PORT\"-->\n            <!--[data_list]=\"master.PORTS\"-->\n            <!--[is_disabled]=\"true\"-->\n            <!--[show_field]=\"'VALUE'\"-->\n            <!--select_field=\"ID\"-->\n            <!--(ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT = $event;\"-->\n          <!--&gt;-->\n          <!--</ptx-input-suggestion>-->\n          <ptx-input-suggestion\n            *ngIf=\"IsCMCS()\"\n            [model]=\"e.DPT_LOAD_PORT\"\n            [data_list]=\"master.PORTS\"\n            [is_disabled]=\"true\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT = $event;\"\n          >\n          </ptx-input-suggestion>\n          <ptx-input-suggestion\n            *ngIf=\"!IsCMCS() && type === 'load'\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_L\"\n            [is_disabled]=\"true\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion>\n          <ptx-input-suggestion\n            *ngIf=\"!IsCMCS() && type === 'discharge'\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_D\"\n            [is_disabled]=\"true\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <input type=\"text\"\n                   class=\"form-control text-left text-sm\"\n                   [ngModel]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_ACTIVITY\"\n                   disabled>\n          </div>\n          <ptx-deduction-modal\n            [model]=\"model\"\n            [index]=\"GetIndex(e)\"\n          >\n          </ptx-deduction-modal>\n        </td>\n\n        <!--Owner-->\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'days')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'hrs')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'mins')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n\n        <!--top review-->\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'days')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'hrs')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'mins')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n\n        <!--settle-->\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'days')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'hrs')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'mins')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n\n        <!--saving-->\n        <td valign=\"top\" class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row  mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <tr class=\"border-top-0\">\n        <td class=\"text-center\">\n          Total Deduction Time\n        </td>\n        <td>\n\n        </td>\n        <!--Owner-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--top-review-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n\n        <!--settle-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--saving-->\n        <td valign=\"top\" class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <tr>\n        <td class=\"border-left-0\"></td>\n        <td colspan=\"2\" class=\"border-right-0 text-left\">Net Time Spent</td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n        <td class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row mb-5\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <!--<tr>-->\n        <!--<td class=\"border-left-0\"></td>-->\n        <!--<td colspan=\"2\" class=\"border-right-0 text-left\">Net Time Spent</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n      <!--</tr>-->\n      </tbody>\n    </table>\n  </div>\n  <div class=\"mb-30\">\n    <button class=\"btn btn-success m-0\" (click)=\"addLoadPort()\">\n      {{ button_title }}\n    </button>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/demurrage-port/demurrage-port.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurragePortComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_master__ = __webpack_require__("../../../../../src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_port__ = __webpack_require__("../../../../../src/app/models/daf-port.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DemurragePortComponent = (function () {
    function DemurragePortComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_2__models_daf_master__["a" /* DAFMaster */]();
        this.title = '';
        this.button_title = '';
        this.type = '';
        this.user_group = '';
        this.handleModelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    DemurragePortComponent.prototype.ngOnInit = function () {
    };
    DemurragePortComponent.prototype.GetIndex = function (elem) {
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["findIndex"](this.model.DAF_PORT, function (e) {
            return elem === e;
        });
    };
    DemurragePortComponent.prototype.addLoadPort = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.DAF_PORT.push(new __WEBPACK_IMPORTED_MODULE_3__models_daf_port__["a" /* DAF_PORT */](this.type));
    };
    DemurragePortComponent.prototype.OnModelChange = function () {
        this.handleModelChange.emit();
        // this.model = DAFHelper.CalculateTime(this.model);
    };
    DemurragePortComponent.prototype.HandleCopyChange = function (e, type) {
        if (type === 'origin') {
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_START);
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_STOP);
            this.HandleCopyChange(e, 'top');
        }
        else if (type === 'top') {
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_SETTLED_T_START = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START);
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_SETTLED_T_STOP = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP);
        }
    };
    DemurragePortComponent.prototype.GetText = function (text, type) {
        return __WEBPACK_IMPORTED_MODULE_7__common_date_helper__["a" /* DateHelper */].GetNumberFromTextByType(text, type);
    };
    DemurragePortComponent.prototype.IsCMCS = function () {
        return this.user_group === 'CMCS';
    };
    DemurragePortComponent.prototype.ShowHrs = function (hrs) {
        hrs = hrs || 0;
        // const r = this.decimalPipe.transform(hrs, '1.4-4');
        // return `${r} Hrs`;
        return "" + hrs;
    };
    DemurragePortComponent.prototype.removePort = function (e) {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var id = e && e.DPT_LOAD_PORT;
        var name = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].getTextByID(id, this.master.PORTS);
        console.log(name);
        var popup_content = "Are you sure you want to remove this port?<br /><br /><b>Port Name:</b> " + name;
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.DAF_PORT = __WEBPACK_IMPORTED_MODULE_4_lodash__["reject"](context.model.DAF_PORT, function (o) {
                return o === e;
            });
            _this.handleModelChange.emit();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], DemurragePortComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_daf_master__["a" /* DAFMaster */])
    ], DemurragePortComponent.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "button_title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "type", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "user_group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], DemurragePortComponent.prototype, "handleModelChange", void 0);
    DemurragePortComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-port',
            template: __webpack_require__("../../../../../src/app/components/demurrage-port/demurrage-port.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__angular_common__["d" /* DecimalPipe */]])
    ], DemurragePortComponent);
    return DemurragePortComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/demurrage-time-summary/demurrage-time-summary.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"text-bold mt-30 pt-30\">Demurrage Time Summary</div>\n<div class=\"table-responsive mt-15 mb-30 pb-30\">\n  <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n    <thead>\n    <tr>\n      <td class=\"text-bold ptx-w-200\"></td>\n      <td class=\"text-bold ptx-w-320\">Original Claim/Original Time Spent</td>\n      <td class=\"text-bold ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n      <td class=\"text-bold ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n      <td class=\"text-bold ptx-w-320\">Settled</td>\n      <td class=\"text-bold ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Original) </td>\n    </tr>\n    </thead>\n    <tbody>\n    <tr>\n      <td class=\"text-left\">Total Running Time <br>({{ GetPortLabel() }})</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TRT_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TRT_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TRT_SETTLED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n          key=\"DDA_TRT_SAVING\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Total Deduction Time <br>({{ GetPortLabel() }})</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TDT_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TDT_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TDT_SETTLED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n          key=\"DDA_TDT_SAVING\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Net Time Spent</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_NTS_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_NTS_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_NTS_SETTLED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n          key=\"DDA_NTS_SAVING\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Laytime Contract</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_LY_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_LY_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        key=\"DDA_LY_SETTLED\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n        key=\"DDA_LY_SAVING\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Time On Demurrage</td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_ORIGIN_CLAIM\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_TOP_REVIEWED\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_SETTLED\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_SAVING\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/demurrage-time-summary/demurrage-time-summary.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageTimeSummaryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_master__ = __webpack_require__("../../../../../src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DemurrageTimeSummaryComponent = (function () {
    function DemurrageTimeSummaryComponent() {
        this.master = new __WEBPACK_IMPORTED_MODULE_1__models_daf_master__["a" /* DAFMaster */]();
        this.model = new __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */]();
    }
    DemurrageTimeSummaryComponent.prototype.ngOnInit = function () {
    };
    DemurrageTimeSummaryComponent.prototype.GetPortLabel = function () {
        var result = '';
        var is_load_port = false;
        var is_discharge_port = false;
        __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](this.model.DAF_PORT, function (e) {
            is_load_port = is_load_port || e.DPT_TYPE === 'load';
            is_discharge_port = is_discharge_port || e.DPT_TYPE === 'discharge';
        });
        if (is_load_port && is_discharge_port) {
            result += 'Load Port + Discharge Port';
        }
        else if (is_load_port) {
            result += 'Load Port';
        }
        else if (is_discharge_port) {
            result += 'Discharge Port';
        }
        return result;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_master__["a" /* DAFMaster */])
    ], DemurrageTimeSummaryComponent.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */])
    ], DemurrageTimeSummaryComponent.prototype, "model", void 0);
    DemurrageTimeSummaryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-time-summary',
            template: __webpack_require__("../../../../../src/app/components/demurrage-time-summary/demurrage-time-summary.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], DemurrageTimeSummaryComponent);
    return DemurrageTimeSummaryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"p-0 col-xs-12 mb-30\">\n  <div class=\"text-bold\">Demurrage Value ({{ model.DDA_CURRENCY_UNIT }}) Summary</div>\n  <div class=\"form-horizontal mt-30 mb-15\">\n    <div class=\"row\">\n      <div class=\"col-xs-12 col-sm-6\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Demurrage Rate ({{ model.DDA_CURRENCY_UNIT }} PDPR)</label>\n          <div class=\"col-xs-6\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [padding]=\"0\"\n                   [ngModel]=\"model.DDA_DEMURAGE_RATE\"\n                   disabled>\n          </div>\n          <!--<div class=\"col-xs-1\">-->\n            <!--<div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>-->\n          <!--</div>-->\n        </div>\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Address Commission ({{ model.DDA_CURRENCY_UNIT }})</label>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_ADDRESS_COMMISSION_N\"\n                     name=\"DDA_IS_ADDRESS_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_ADDRESS_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'N'\"\n              >\n              <label for=\"DDA_IS_ADDRESS_COMMISSION_N\">No</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_ADDRESS_COMMISSION_Y\"\n                     name=\"DDA_IS_ADDRESS_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_ADDRESS_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'Y'\"\n              >\n              <label for=\"DDA_IS_ADDRESS_COMMISSION_Y\">Yes</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [max]=\"100\"\n                   [disabled]=\"model.DDA_IS_ADDRESS_COMMISSION == 'N' || model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_ADDRESS_COMMISSION_PERCEN\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">%</div>\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n          <label class=\"col-xs-5 control-label\">Broker Commission ({{ model.DDA_CURRENCY_UNIT }})</label>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_BROKER_COMMISSION_N\"\n                     name=\"DDA_IS_BROKER_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_BROKER_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'N'\"\n              >\n              <label for=\"DDA_IS_BROKER_COMMISSION_N\">No</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_BROKER_COMMISSION_Y\"\n                     name=\"DDA_IS_BROKER_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_BROKER_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'Y'\"\n              >\n              <label for=\"DDA_IS_BROKER_COMMISSION_Y\">Yes</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <!--<input type=\"text\"-->\n                   <!--class=\"form-control text-center\"-->\n                   <!--ptxRestrictNumber-->\n                   <!--[fraction]=\"2\"-->\n                   <!--[max]=\"100\"-->\n                   <!--[disabled]=\"model.DDA_IS_BROKER_COMMISSION == 'N' || model.IS_DISABLED\"-->\n                   <!--[(ngModel)]=\"model.DDA_BROKER_COMMISSION_PERCEN\"-->\n                   <!--(ngModelChange)=\"onUpdateCal()\"-->\n                   <!--(ngModelChange)=\"OnModelChange();\"-->\n            <!--&gt;-->\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [max]=\"100\"\n                   [disabled]=\"model.DDA_IS_BROKER_COMMISSION == 'N' || model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_BROKER_COMMISSION_PERCEN\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">%</div>\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n          <label class=\"col-xs-5 control-label\">Demurrage Sharing to {{ GetCompanyName(model.DDA_FOR_COMPANY) }} ({{ model.DDA_CURRENCY_UNIT }})</label>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_SHARING_TO_TOP_N\"\n                     name=\"DDA_IS_SHARING_TO_TOP\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_SHARING_TO_TOP\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'N'\"\n              >\n              <label for=\"DDA_IS_SHARING_TO_TOP_N\">No</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_SHARING_TO_TOP_Y\"\n                     name=\"DDA_IS_SHARING_TO_TOP\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_SHARING_TO_TOP\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'Y'\"\n              >\n              <label for=\"DDA_IS_SHARING_TO_TOP_Y\">Yes</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [max]=\"100\"\n                   [disabled]=\"model.DDA_IS_SHARING_TO_TOP == 'N' || model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_SHARING_TO_TOP_PERCEN\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">%</div>\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"false\">\n          <label class=\"col-xs-5 control-label\">Decimal</label>\n          <div class=\"col-xs-6\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"0\"\n                   [max]=\"7\"\n                   [disabled]=\"model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_DECIMAL\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">Digit</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <table class=\"mb-30 pb-30 table table-bordered plutonyx-table-2 text-center\">\n    <thead>\n    <tr>\n      <td class=\"text-bold\" width=\"30%\"></td>\n      <td class=\"text-bold\" width=\"17.5%\">Original Claim/Original Time Spent</td>\n      <td class=\"text-bold\" width=\"17.5%\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n      <td class=\"text-bold\" width=\"17.5%\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n      <td class=\"text-bold\" width=\"17.5%\">Settled</td>\n      <td class=\"text-bold\" width=\"17.5%\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Original)</td>\n    </tr>\n    </thead>\n    <tbody>\n    <tr *ngIf=\"model.DDA_IS_ADDRESS_COMMISSION === 'Y' || model.DDA_IS_SHARING_TO_TOP === 'Y'\">\n      <td class=\"text-left\">Gross Demurrage ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_GD_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_GD_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_GD_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_GD_SAVING_VALUE ) }}</td>\n    </tr>\n    <tr *ngIf=\"model.DDA_IS_ADDRESS_COMMISSION === 'Y'\">\n      <td class=\"text-left\">Less Address Commission ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_AC_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_AC_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_AC_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_AC_SAVING_VALUE) }}</td>\n    </tr>\n    <tr class=\"bg-blue\" *ngIf=\"model.DDA_IS_ADDRESS_COMMISSION === 'Y' && model.DDA_IS_SHARING_TO_TOP === 'Y'\">\n      <td class=\"text-left\">Sub Total Demurrage ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_STD_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_STD_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_STD_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_STD_SAVING_VALUE) }}</td>\n    </tr>\n    <tr *ngIf=\"model.DDA_IS_SHARING_TO_TOP === 'Y'\">\n      <td class=\"text-left\">Demurrage Sharing to TOP ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_DST_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_DST_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_DST_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_DST_SAVING_VALUE) }}</td>\n    </tr>\n    <tr class=\"bg-yellow\">\n      <td class=\"text-left\">Net Demurrage ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_ND_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_ND_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_ND_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_ND_SAVING_VALUE) }}</td>\n    </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageValueUsdSummaryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_daf_helper__ = __webpack_require__("../../../../../src/app/common/daf-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_master__ = __webpack_require__("../../../../../src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DemurrageValueUsdSummaryComponent = (function () {
    function DemurrageValueUsdSummaryComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_3__models_daf_master__["a" /* DAFMaster */]();
    }
    DemurrageValueUsdSummaryComponent.prototype.ngOnInit = function () {
    };
    DemurrageValueUsdSummaryComponent.prototype.onUpdateCal = function () {
        var DDA_IS_BROKER_COMMISSION = this.model.DDA_IS_BROKER_COMMISSION === 'Y';
        var DDA_IS_ADDRESS_COMMISSION = this.model.DDA_IS_ADDRESS_COMMISSION === 'Y';
        var DDA_IS_SHARING_TO_TOP = this.model.DDA_IS_SHARING_TO_TOP === 'Y';
        if (!DDA_IS_BROKER_COMMISSION) {
            this.model.DDA_BROKER_COMMISSION_PERCEN = null;
        }
        if (!DDA_IS_ADDRESS_COMMISSION) {
            this.model.DDA_ADDRESS_COMMISSION_PERCEN = null;
        }
        if (!DDA_IS_SHARING_TO_TOP) {
            this.model.DDA_SHARING_TO_TOP_PERCEN = null;
        }
        this.OnModelChange();
    };
    DemurrageValueUsdSummaryComponent.prototype.ShowNumber = function (num) {
        num = num || 0;
        var r = this.decimalPipe.transform(num, '1.2-2');
        return "" + r;
    };
    DemurrageValueUsdSummaryComponent.prototype.GetCompanyName = function (comcode) {
        return __WEBPACK_IMPORTED_MODULE_4__common_utility__["a" /* Utility */].getTextByID(comcode, this.master.COMPANY);
    };
    DemurrageValueUsdSummaryComponent.prototype.OnModelChange = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_2__common_daf_helper__["a" /* DAFHelper */].CalValue(this.model);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], DemurrageValueUsdSummaryComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__models_daf_master__["a" /* DAFMaster */])
    ], DemurrageValueUsdSummaryComponent.prototype, "master", void 0);
    DemurrageValueUsdSummaryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-value-usd-summary',
            template: __webpack_require__("../../../../../src/app/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DecimalPipe */]])
    ], DemurrageValueUsdSummaryComponent);
    return DemurrageValueUsdSummaryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/file-upload/file-upload.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ is_file_upload | json }}</pre> -->\n<!-- <div class=\"col-xs-12\" *ngIf=\"is_disabled\">\n  <div class=\"form-group\">\n    <ng-content></ng-content>\n    <label class=\"btn btn-xs btn-select-file\" [ngClass]=\"{'m-progress':is_file_upload}\">\n        view attached file\n    </label>\n  </div>\n</div> -->\n<div class=\"col-xs-12\" *ngIf=\"!is_disabled\">\n  <div class=\"form-group\">\n    <ng-content></ng-content>\n    <label class=\"btn btn-default\" [ngClass]=\"{'m-progress':is_file_upload}\">\n      <!-- <label class=\"btn btn-xs btn-select-file\" [ngClass]=\"{'m-progress':is_file_upload}\"> -->\n      Select File\n      <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span>\n      <input [disabled]=\"is_disabled\" name=\"ss\" type=\"file\" (change)=\"FileChangeEvent($event, filterBy)\" style=\"display: none;\" multiple>\n    </label>\n    <b>{{ desc }}</b>\n    <!-- <b>PDF, Word, Excel, Image, Outlook</b> -->\n  </div>\n</div>\n\n<div *ngFor=\"let elem of model|attachmentFilter:filterBy\" class=\"col-md-12\">\n  <div class=\"col-md-6\">\n    <!-- <div *ngFor=\"let elem of model\" class=\"uploadifyQueueItem ExplanAttach\"> -->\n    <div class=\"uploadifyQueueItem ExplanAttach\">\n      <div class=\"cancel\" *ngIf=\"!is_disabled\">\n        <a [class.disabled]=\"is_disabled\" (click)=\"RemoveFiles(elem)\"><span class=\"glyphicon glyphicon-remove removeExplanAttach\"></span></a>\n      </div>\n      <a target=\"_blank\" [href]=\"GetFileUrl(elem.DAT_PATH)\">{{ elem.DAT_INFO }}</a>\n    </div>\n  </div>\n  <div class=\"col-md-4\" *ngIf=\"need_caption\" style=\"margin-top: 10px;\">\n    <input type=\"text\" name=\"caption\" class=\"form-control\" [disabled]=\"is_disabled\" [(ngModel)]=\"model[GetIndex(elem)].DAT_CAPTION\" (ngModelChange)=\"handleModelChanged.emit(model)\">\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/file-upload/file-upload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileUploadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_upload_service__ = __webpack_require__("../../../../../src/app/services/upload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FileUploadComponent = (function () {
    function FileUploadComponent(uploadService) {
        this.uploadService = uploadService;
        this.model = [];
        this.fk = "";
        this.is_disabled = false;
        this.filterBy = '';
        this.need_caption = false;
        this.desc = 'PDF, Word, Excel, Image, Outlook';
        this.handleModelChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.is_file_upload = false;
    }
    FileUploadComponent.prototype.ngOnInit = function () {
    };
    FileUploadComponent.prototype.RemoveFiles = function (target) {
        if (this.is_disabled) {
            return null;
        }
        var state = this.model;
        var rejected_images = __WEBPACK_IMPORTED_MODULE_1_lodash__["reject"](state, function (elem) {
            return elem == target;
        });
        this.model = rejected_images;
        this.handleModelChanged.emit(this.model);
    };
    FileUploadComponent.prototype.GetIndex = function (elem) {
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["findIndex"](this.model, function (e) {
            return e == elem;
        });
    };
    FileUploadComponent.prototype.FileChangeEvent = function ($event, type_string) {
        var _this = this;
        var files = $event.target.files;
        this.model = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], this.model);
        __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](files, function (file) {
            _this.is_file_upload = true;
            _this.uploadService.postSingle(file, type_string)
                .subscribe(function (data) {
                // const uid = data.uid;
                // console.log(data.PAT_INFO)
                _this.is_file_upload = false;
                _this.model.push(data);
                _this.handleModelChanged.emit(_this.model);
                $event.target.value = "";
            }, function (error) {
                // console.log(error)
                _this.is_file_upload = false;
                $event.target.value = "";
            });
            // setTimeout(() => {
            //   console.log(file);
            //   const data = {
            //     name: file.name,
            //   };
            //   this.model.push(data);
            //   this.is_file_upload = false;
            // }, 1000);
        });
        console.log(files);
        // let file: File = files[0]
        // this.uploadService.postMemo(file, type_string, this.fk)
        //   .subscribe(
        //       data => {
        //         // const uid = data.uid;
        //         this.model.push(data);
        //         console.log(data)
        //         this.is_file_upload = false;
        //         this.handleModelChanged.emit(this.model);
        //         $event.target.value = "";
        //       },
        //       error => {
        //         console.log(error)
        //         this.is_file_upload = false;
        //         $event.target.value = "";
        //       }
        //   )
    };
    FileUploadComponent.prototype.GetFileUrl = function (path) {
        return __WEBPACK_IMPORTED_MODULE_3__common_utility__["a" /* Utility */].GetFileUrl(path);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FileUploadComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FileUploadComponent.prototype, "fk", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], FileUploadComponent.prototype, "is_disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FileUploadComponent.prototype, "filterBy", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], FileUploadComponent.prototype, "need_caption", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], FileUploadComponent.prototype, "desc", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], FileUploadComponent.prototype, "handleModelChanged", void 0);
    FileUploadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-file-upload',
            template: __webpack_require__("../../../../../src/app/components/file-upload/file-upload.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_upload_service__["a" /* UploadService */]])
    ], FileUploadComponent);
    return FileUploadComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/freetext/freetext.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FreetextComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FreetextComponent = (function () {
    function FreetextComponent(el) {
        this.el = el;
        this.elementId = "";
        // @Input() model: any = "";
        this.disabled = false;
        // @Output() HandleStateChange: EventEmitter<any> = new EventEmitter<any>();
        this.HandleStateChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.base_api = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */];
        this.insideElementId = "";
        this.htmlContent = "";
    }
    FreetextComponent.prototype.ngOnInit = function () {
    };
    FreetextComponent.prototype.ngAfterViewInit = function () {
        this.insideElementId = "ptx-freetext #" + this.elementId;
        console.log(this.insideElementId);
        tinymce.init({
            selector: this.insideElementId,
            theme: 'modern',
            plugins: ['textcolor colorpicker'],
            skin_url: '../../Content/daf/assets/skins/lightgray',
            height: 200,
            menubar: false,
            toolbar: " undo redo | bold italic underline | numlist | forecolor | removeformat | spellchecker",
            statusbar: true,
            image_advtab: true,
            paste_data_images: true,
            readonly: this.disabled,
            branding: false,
            setup: this.tinyMCESetup.bind(this),
        });
    };
    FreetextComponent.prototype.ngOnChanges = function (e) {
        // if (e.model.currentValue !== e.model.previousValue) {
        //   // $(`#${this.elementId}`).html(e.model.currentValue);
        //   if(tinymce.get(this.elementId) && e.model.currentValue) {
        //     tinymce.get(this.elementId).setContent(e.model.currentValue);
        //     // tinymce.get(this.elementId).focus();
        //     // tinymce.activeEditor.setContent(e.model.currentValue, {format: 'raw'});
        //   }
        if (e.disabled && e.disabled.currentValue) {
            tinymce.get(this.elementId).getBody().setAttribute('contenteditable', false);
        }
        // }
    };
    FreetextComponent.prototype.tinyMCESetup = function (e) {
        e.on('keyup change', this.tinyMCEOnKeyup.bind(this));
        // e.on('change', this.tinyMCEOnChange.bind(this));
    };
    // tinyMCEOnChange(e) {
    //   let content = tinymce.get(this.elementId).getContent();
    //   console.log(`tinyMCEOnChange`, content);
    //   // this.HandleStateChange.emit(content);
    // }
    FreetextComponent.prototype.tinyMCEOnKeyup = function (e) {
        var content = tinymce.get(this.elementId).getContent();
        // console.log(`tinyMCEOnKeyup`, content);
        this.HandleStateChange.emit(content);
    };
    FreetextComponent.prototype.ngOnDestroy = function () {
        //destroy cloned elements
        if (tinymce.get(this.elementId)) {
            tinymce.get(this.elementId).remove();
        }
        // tinymce.get(this.elementId).remove();
        // let elem = document.getElementById(this.elementId);
        // elem.parentElement.removeChild(elem);
    };
    Object.defineProperty(FreetextComponent.prototype, "model", {
        set: function (c) {
            c = c || "";
            if (tinymce.get(this.elementId)) {
                tinymce.get(this.elementId).setContent(c);
            }
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], FreetextComponent.prototype, "elementId", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], FreetextComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", Object)
    ], FreetextComponent.prototype, "HandleStateChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('model'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], FreetextComponent.prototype, "model", null);
    FreetextComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-freetext',
            template: "<textarea id=\"{{elementId}}\" class=\"form-control\" [disabled]=\"disabled\"></textarea>",
            styleUrls: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], FreetextComponent);
    return FreetextComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-date-time/input-date-time.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row mb-5\">\n  <div class=\"col-xs-5\">\n    <ptx-input-date\n      [date]=\"model.date\"\n      (modelChange)=\"model.date = $event; DoHandleModelChange();\"\n      [disabled]=\"disabled\"\n    ></ptx-input-date>\n  </div>\n  <div class=\"col-xs-4 pl-0\">\n    <ptx-input-time\n      [hour]=\"model.hour\"\n      [minute]=\"model.minute\"\n      [disabled]=\"disabled\"\n      (HandleStateChange)=\"model.hour = $event.hour; model.minute = $event.minute; DoHandleModelChange();\"\n    ></ptx-input-time>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/input-date-time/input-date-time.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDateTimeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputDateTimeComponent = (function () {
    function InputDateTimeComponent() {
        this.input = '';
        this.disabled = false;
        this.model = {
            date: '',
            hour: '00',
            minute: '00',
        };
        this.handleModelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    InputDateTimeComponent.prototype.ngOnInit = function () {
    };
    InputDateTimeComponent.prototype.DoHandleModelChange = function () {
        var date = this.model.date;
        date += ' ' + this.pad(this.model.hour, 2) + ':' + this.pad(this.model.minute, 2) + ':00';
        // console.log(date);
        this.handleModelChange.emit(date);
    };
    Object.defineProperty(InputDateTimeComponent.prototype, "setModel", {
        set: function (e) {
            if (e) {
                var full_date = __WEBPACK_IMPORTED_MODULE_1_moment__(e).format('YYYY-MM-DD HH:mm');
                if (full_date == "Invalid date") {
                    full_date = '';
                }
                else {
                    this.model.hour = __WEBPACK_IMPORTED_MODULE_1_moment__(e).format('HH');
                    this.model.minute = __WEBPACK_IMPORTED_MODULE_1_moment__(e).format('mm');
                    this.model.date = __WEBPACK_IMPORTED_MODULE_1_moment__(e).format('YYYY-MM-DD');
                }
            }
            // console.log('input', e, full_date);
            // this.model.date =
        },
        enumerable: true,
        configurable: true
    });
    InputDateTimeComponent.prototype.pad = function (n, width) {
        var z = '0';
        n = n + '';
        n = n * 1;
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputDateTimeComponent.prototype, "input", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputDateTimeComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputDateTimeComponent.prototype, "handleModelChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('input'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], InputDateTimeComponent.prototype, "setModel", null);
    InputDateTimeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-date-time',
            template: __webpack_require__("../../../../../src/app/components/input-date-time/input-date-time.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], InputDateTimeComponent);
    return InputDateTimeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-date/input-date-range.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDateRangeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputDateRangeComponent = (function () {
    function InputDateRangeComponent(el) {
        this.el = el;
        // @Input() private model: string = null;
        this.start_date = null;
        this.end_date = null;
        this.disabled = false;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.show_model = "";
    }
    InputDateRangeComponent.prototype.onChange = function (e) {
        // console.log(e);
    };
    InputDateRangeComponent.prototype.ngOnInit = function () {
    };
    InputDateRangeComponent.prototype.OnChangeUpdate = function (p) {
        if (!p) {
            // console.log(p)
            this.modelChange.emit({
                start_date: '',
                end_date: '',
            });
        }
    };
    InputDateRangeComponent.prototype.ngAfterViewInit = function () {
        var context = this;
        // console.log($(context.el.nativeElement).find('.input-date-picker'));
        $(context.el.nativeElement).find('.input-date-picker').dateRangePicker({
            autoClose: true,
            singleDate: false,
            showShortcuts: false,
            format: "DD/MM/YYYY",
        }).bind('datepicker-change', function (event, obj) {
            // console.log(obj)
            var start_date = __WEBPACK_IMPORTED_MODULE_1__common__["d" /* DateHelper */].DateStringToCsharpString(obj.date1);
            var end_date = __WEBPACK_IMPORTED_MODULE_1__common__["d" /* DateHelper */].DateStringToCsharpString(obj.date2);
            // context.model = Date√Helper.DateStringToDate(obj.value);
            // context.show_model = obj.value;
            context.modelChange.emit({
                start_date: start_date,
                end_date: end_date,
            });
            // DateHelper
        });
    };
    InputDateRangeComponent.prototype.normalizeDate = function (start_date, end_date) {
        // console.log(start_date)
        // console.log(end_date)
        if (!start_date || start_date == "") {
            return "";
        }
        var start_date_str = __WEBPACK_IMPORTED_MODULE_1__common__["d" /* DateHelper */].DateToString(start_date);
        var end_date_str = __WEBPACK_IMPORTED_MODULE_1__common__["d" /* DateHelper */].DateToString(end_date);
        if (!start_date_str || start_date_str == "") {
            return "";
        }
        return start_date_str + " TO " + end_date_str;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputDateRangeComponent.prototype, "start_date", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputDateRangeComponent.prototype, "end_date", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputDateRangeComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputDateRangeComponent.prototype, "modelChange", void 0);
    InputDateRangeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-date-range',
            template: "<input\n    type=\"text\"\n    name=\"date\"\n    class=\"form-control essential input-date-picker daterange\"\n    [ngModel]=\"normalizeDate(start_date, end_date)\"\n    (ngModelChange)=\"OnChangeUpdate($event)\"\n    [disabled]=\"disabled\"\n  >",
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], InputDateRangeComponent);
    return InputDateRangeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-date/input-date.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import * as $ from 'jquery';
var InputDateComponent = (function () {
    function InputDateComponent(el) {
        this.el = el;
        this.date = null;
        this.disabled = false;
        this.needtime = false;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.show_date = "";
    }
    InputDateComponent.prototype.onChange = function (e) {
        console.log(e);
    };
    InputDateComponent.prototype.ngOnInit = function () {
        var context = this;
        $(context.el.nativeElement).find('.input-date-picker').dateRangePicker({
            autoClose: true,
            singleDate: true,
            showShortcuts: false,
            format: 'DD/MM/YYYY',
        }).bind('datepicker-change', function (event, obj) {
            // console.log(obj)
            // context.date = DateHelper.DateStringToDate(obj.value);
            context.show_date = obj.value;
            // if (context.needtime) {
            //   context.modelChange.emit(DateHelper.DateStringToCsharpWithMinuteString(obj.value));
            // } else {
            context.modelChange.emit(__WEBPACK_IMPORTED_MODULE_1__common__["d" /* DateHelper */].DateStringToCsharpString(obj.value));
            // }
            // DateHelper
        });
    };
    InputDateComponent.prototype.normalizeDate = function (date) {
        // console.log(`date`, date);
        var date_str = '';
        if (date) {
            date_str = __WEBPACK_IMPORTED_MODULE_1__common__["d" /* DateHelper */].DateToString(date);
            if (date_str == 'Invalid date') {
                date_str = '';
            }
        }
        // console.log(`date`, date_str);
        return date_str;
    };
    InputDateComponent.prototype.OnChangeUpdate = function (p) {
        if (!p) {
            // console.log(p)
            this.modelChange.emit('');
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputDateComponent.prototype, "date", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputDateComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputDateComponent.prototype, "needtime", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputDateComponent.prototype, "modelChange", void 0);
    InputDateComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-date',
            template: "<input\n    type=\"text\"\n    name=\"date\"\n    class=\"form-control essential input-date-picker daterange\"\n    [ngModel]=\"normalizeDate(date)\"\n    (ngModelChange)=\"OnChangeUpdate($event)\"\n    [disabled]=\"disabled\"\n  >",
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], InputDateComponent);
    return InputDateComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-hrs/input-hrs.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<input type=\"text\"-->\n   <!--class=\"form-control text-center text-sm\"-->\n   <!--[ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_HRS)\"-->\n   <!--disabled-->\n<!--&gt;-->\n<input type=\"text\"\n       class=\"form-control text-inline text-center text-sm\"\n       [ngClass]=\"className\"\n       [ngModel]=\"ShowHrs(hrs)\"\n       disabled\n>\n<div class=\"text-inline mr-10\">Hrs</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/input-hrs/input-hrs.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputHrsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputHrsComponent = (function () {
    function InputHrsComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.hrs = 0;
        this.className = '';
    }
    InputHrsComponent.prototype.ngOnInit = function () {
    };
    InputHrsComponent.prototype.ShowHrs = function (hrs) {
        var r = this.decimalPipe.transform(hrs, '1.10-10');
        return r;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], InputHrsComponent.prototype, "hrs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputHrsComponent.prototype, "className", void 0);
    InputHrsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-hrs',
            template: __webpack_require__("../../../../../src/app/components/input-hrs/input-hrs.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["d" /* DecimalPipe */]])
    ], InputHrsComponent);
    return InputHrsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-show-date-diff/input-show-date-diff.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"show_date\" disabled>\n"

/***/ }),

/***/ "../../../../../src/app/components/input-show-date-diff/input-show-date-diff.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputShowDateDiffComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import moment = require("moment");


var InputShowDateDiffComponent = (function () {
    function InputShowDateDiffComponent(el) {
        this.el = el;
        this.only_hour = false;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.show_date = '';
    }
    InputShowDateDiffComponent.prototype.ngOnInit = function () {
    };
    InputShowDateDiffComponent.prototype.ngAfterViewInit = function () {
        $(this.el.nativeElement).find('input').change(function (e) {
            console.log(e);
        });
        // this.cdr.detectChanges();
    };
    Object.defineProperty(InputShowDateDiffComponent.prototype, "model", {
        set: function (val) {
            var from = (val.start && __WEBPACK_IMPORTED_MODULE_2_moment_moment__(val.start)) || __WEBPACK_IMPORTED_MODULE_2_moment_moment__();
            var to = (val.end && __WEBPACK_IMPORTED_MODULE_2_moment_moment__(val.end)) || __WEBPACK_IMPORTED_MODULE_2_moment_moment__();
            var duration = __WEBPACK_IMPORTED_MODULE_2_moment_moment__["duration"](to.diff(from));
            var days = __WEBPACK_IMPORTED_MODULE_1_lodash__["floor"](Math.abs(duration.asDays()), 0);
            var hours = __WEBPACK_IMPORTED_MODULE_1_lodash__["floor"](Math.abs(duration.hours()), 0);
            var minutes = __WEBPACK_IMPORTED_MODULE_1_lodash__["floor"](Math.abs(duration.minutes()), 0);
            var message = days + " Days ";
            message += hours + " Hrs ";
            message += minutes + " Mins ";
            this.show_date = message;
            // this.cdr.detectChanges();
            this.modelChange.emit(message);
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputShowDateDiffComponent.prototype, "only_hour", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputShowDateDiffComponent.prototype, "modelChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputShowDateDiffComponent.prototype, "show_date", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('model'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], InputShowDateDiffComponent.prototype, "model", null);
    InputShowDateDiffComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-show-date-diff',
            template: __webpack_require__("../../../../../src/app/components/input-show-date-diff/input-show-date-diff.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], InputShowDateDiffComponent);
    return InputShowDateDiffComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-suggestion/input-start-suggestion.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputStartSuggestionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputStartSuggestionComponent = (function () {
    function InputStartSuggestionComponent() {
        this.show_field = 'TEXT';
        this.class_name = '';
        this.ModelChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    InputStartSuggestionComponent.prototype.ngOnInit = function () {
    };
    InputStartSuggestionComponent.prototype.show = function (model) {
        var _this = this;
        // return Utility.getTextByID(model, this.data_list);
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list, function (e) {
            return e[_this.select_field] == model;
        });
        return (s && s[this.show_field]);
    };
    InputStartSuggestionComponent.prototype.OnSelected = function (model) {
        var _this = this;
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list, function (e) {
            return e[_this.show_field] == model;
        });
        this.ModelChanged.emit((s && s.ID));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputStartSuggestionComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputStartSuggestionComponent.prototype, "data_list", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputStartSuggestionComponent.prototype, "is_disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputStartSuggestionComponent.prototype, "show_field", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputStartSuggestionComponent.prototype, "select_field", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputStartSuggestionComponent.prototype, "class_name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputStartSuggestionComponent.prototype, "ModelChanged", void 0);
    InputStartSuggestionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-input-start-suggestion',
            template: "\n    <input\n      [ngModel]=\"show(model)\"\n      (ngModelChange)=\"OnSelected($event)\"\n      [typeahead]=\"data_list\"\n      [disabled]=\"is_disabled\"\n      placeholder=\"\"\n      [typeaheadOptionField]=\"show_field\"\n      [typeaheadMinLength]=\"0\"\n      typeaheadWaitMs=\"300\"\n      [ngClass]=\"class_name\"\n      class=\"form-control\">\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], InputStartSuggestionComponent);
    return InputStartSuggestionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-suggestion/input-suggestion-ui.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputSuggestionUiComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputSuggestionUiComponent = (function () {
    function InputSuggestionUiComponent(el) {
        this.el = el;
        this.id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        this.show_field = 'TEXT';
        this.class_name = '';
        this.is_start_with = true;
        this.ModelChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    InputSuggestionUiComponent.prototype.ngOnInit = function () {
        // $(this.el.nativeElement).find('#' + this.id).val(this.id);
    };
    InputSuggestionUiComponent.prototype.ngAfterViewInit = function () {
    };
    //
    InputSuggestionUiComponent.prototype.show = function (model) {
        var _this = this;
        // return Utility.getTextByID(model, this.data_list);
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list_clone, function (e) {
            return e[_this.select_field] === model;
        });
        return (s && s[this.show_field]);
    };
    InputSuggestionUiComponent.prototype.OnSelected = function (model) {
        var _this = this;
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list_clone, function (e) {
            return e[_this.show_field] === model;
        });
        this.ModelChanged.emit((s && s.ID));
    };
    Object.defineProperty(InputSuggestionUiComponent.prototype, "dataList", {
        set: function (c) {
            c = c || [];
            this.data_list_clone = c;
            this.load();
        },
        enumerable: true,
        configurable: true
    });
    InputSuggestionUiComponent.prototype.load = function () {
        var context = this;
        var data = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](context.data_list_clone, function (e) {
            return e[context.show_field];
        });
        console.log(data);
        $(function () {
            console.log("#" + context.id);
            $("#" + context.id).autocomplete({
                source: function (request, response) {
                    var results = [];
                    if (context.is_start_with) {
                        results = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](data, function (item) {
                            return __WEBPACK_IMPORTED_MODULE_1_lodash__["startsWith"](item.toLowerCase(), request.term.toLowerCase());
                        });
                    }
                    else {
                        results = $.ui.autocomplete.filter(data, request.term);
                    }
                    response(results.slice(0, 15));
                },
                delay: 0,
                minLength: 0,
                select: function (event, ui) {
                    var select_label;
                    if (ui.item) {
                        select_label = ui.item.label || '';
                    }
                    else {
                        select_label = $(this).val() || '';
                    }
                    context.OnSelected(select_label);
                    // const selected = _.find(context.data_list_clone, (e) => {
                    //   return e[context.show_field].toLowerCase() === select_label.toLowerCase();
                    // });
                    // console.log(selected[context.select_field]);
                },
                change: function (event, ui) {
                    if (ui.item) {
                        return;
                    }
                    var value = $(this).val() || '';
                    var valueLowerCase = value.toLowerCase();
                    var valid = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](context.data_list_clone, function (e) {
                        return e[context.show_field].toLowerCase() === valueLowerCase;
                    });
                    if (valid) {
                        return;
                    }
                    $(this).val('');
                    $(this).autocomplete('instance').term = '';
                    context.ModelChanged.emit(null);
                },
            }).click(function () {
                $(this).autocomplete('search');
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputSuggestionUiComponent.prototype, "id", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionUiComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionUiComponent.prototype, "is_disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputSuggestionUiComponent.prototype, "show_field", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionUiComponent.prototype, "select_field", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputSuggestionUiComponent.prototype, "class_name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputSuggestionUiComponent.prototype, "is_start_with", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputSuggestionUiComponent.prototype, "ModelChanged", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('data_list'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], InputSuggestionUiComponent.prototype, "dataList", null);
    InputSuggestionUiComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-suggestion-ui',
            template: "\n    <input\n      [id]=\"id\"\n      [ngClass]=\"class_name\"\n      [ngModel]=\"show(model)\"\n      class=\"form-control\">\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], InputSuggestionUiComponent);
    return InputSuggestionUiComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-suggestion/input-suggestion.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputSuggestionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputSuggestionComponent = (function () {
    function InputSuggestionComponent() {
        this.show_field = 'TEXT';
        this.class_name = '';
        this.ModelChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    InputSuggestionComponent.prototype.ngOnInit = function () {
    };
    InputSuggestionComponent.prototype.show = function (model) {
        var _this = this;
        // return Utility.getTextByID(model, this.data_list);
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list, function (e) {
            return e[_this.select_field] == model;
        });
        return (s && s[this.show_field]);
    };
    InputSuggestionComponent.prototype.OnSelected = function (model) {
        var _this = this;
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list, function (e) {
            return e[_this.show_field] == model;
        });
        this.ModelChanged.emit((s && s.ID));
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionComponent.prototype, "data_list", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionComponent.prototype, "is_disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputSuggestionComponent.prototype, "show_field", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], InputSuggestionComponent.prototype, "select_field", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputSuggestionComponent.prototype, "class_name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], InputSuggestionComponent.prototype, "ModelChanged", void 0);
    InputSuggestionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-suggestion',
            template: "\n    <input\n      [ngModel]=\"show(model)\"\n      (ngModelChange)=\"OnSelected($event)\"\n      [typeahead]=\"data_list\"\n      [disabled]=\"is_disabled\"\n      placeholder=\"\"\n      [typeaheadOptionField]=\"show_field\"\n      [typeaheadMinLength]=\"0\"\n      typeaheadWaitMs=\"300\"\n      [ngClass]=\"class_name\"\n      class=\"form-control\">\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], InputSuggestionComponent);
    return InputSuggestionComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/input-time/input-time.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-inline\">\n  <select class=\"form-control text-center\"\n          [(ngModel)]=\"hour\"\n          [disabled]=\"disabled\"\n          (ngModelChange)=\"OnModelChange()\"\n  >\n    <option value=\"00\">00</option>\n    <option value=\"01\">01</option>\n    <option value=\"02\">02</option>\n    <option value=\"03\">03</option>\n    <option value=\"04\">04</option>\n    <option value=\"05\">05</option>\n    <option value=\"06\">06</option>\n    <option value=\"07\">07</option>\n    <option value=\"08\">08</option>\n    <option value=\"09\">09</option>\n    <option value=\"10\">10</option>\n    <option value=\"11\">11</option>\n    <option value=\"12\">12</option>\n    <option value=\"13\">13</option>\n    <option value=\"14\">14</option>\n    <option value=\"15\">15</option>\n    <option value=\"16\">16</option>\n    <option value=\"17\">17</option>\n    <option value=\"18\">18</option>\n    <option value=\"19\">19</option>\n    <option value=\"20\">20</option>\n    <option value=\"21\">21</option>\n    <option value=\"22\">22</option>\n    <option value=\"23\">23</option>\n  </select>\n  <!--<input-->\n    <!--type=\"text\"-->\n    <!--class=\"form-control text-center text-sm ptx-w-40\"-->\n    <!--ptxRestrictNumber-->\n    <!--min=\"0\"-->\n    <!--max=\"23\"-->\n    <!--fraction=\"0\"-->\n    <!--padding=\"2\"-->\n    <!--[(ngModel)]=\"hour\"-->\n    <!--[disabled]=\"disabled\"-->\n    <!--(ngModelChange)=\"OnModelChange()\"-->\n  <!--/>-->\n</div>\n<div class=\"text-inline mr-10\">Hrs</div>\n<div class=\"form-inline\">\n  <select class=\"form-control text-center\"\n          [(ngModel)]=\"minute\"\n          [disabled]=\"disabled\"\n          (ngModelChange)=\"OnModelChange()\"\n  >\n    <option value=\"00\">00</option>\n    <option value=\"01\">01</option>\n    <option value=\"02\">02</option>\n    <option value=\"03\">03</option>\n    <option value=\"04\">04</option>\n    <option value=\"05\">05</option>\n    <option value=\"06\">06</option>\n    <option value=\"07\">07</option>\n    <option value=\"08\">08</option>\n    <option value=\"09\">09</option>\n    <option value=\"10\">10</option>\n    <option value=\"11\">11</option>\n    <option value=\"12\">12</option>\n    <option value=\"13\">13</option>\n    <option value=\"14\">14</option>\n    <option value=\"15\">15</option>\n    <option value=\"16\">16</option>\n    <option value=\"17\">17</option>\n    <option value=\"18\">18</option>\n    <option value=\"19\">19</option>\n    <option value=\"20\">20</option>\n    <option value=\"21\">21</option>\n    <option value=\"22\">22</option>\n    <option value=\"23\">23</option>\n    <option value=\"24\">24</option>\n    <option value=\"25\">25</option>\n    <option value=\"26\">26</option>\n    <option value=\"27\">27</option>\n    <option value=\"28\">28</option>\n    <option value=\"29\">29</option>\n    <option value=\"30\">30</option>\n    <option value=\"31\">31</option>\n    <option value=\"32\">32</option>\n    <option value=\"33\">33</option>\n    <option value=\"34\">34</option>\n    <option value=\"35\">35</option>\n    <option value=\"36\">36</option>\n    <option value=\"37\">37</option>\n    <option value=\"38\">38</option>\n    <option value=\"39\">39</option>\n    <option value=\"40\">40</option>\n    <option value=\"41\">41</option>\n    <option value=\"42\">42</option>\n    <option value=\"43\">43</option>\n    <option value=\"44\">44</option>\n    <option value=\"45\">45</option>\n    <option value=\"46\">46</option>\n    <option value=\"47\">47</option>\n    <option value=\"48\">48</option>\n    <option value=\"49\">49</option>\n    <option value=\"50\">50</option>\n    <option value=\"51\">51</option>\n    <option value=\"52\">52</option>\n    <option value=\"53\">53</option>\n    <option value=\"54\">54</option>\n    <option value=\"55\">55</option>\n    <option value=\"56\">56</option>\n    <option value=\"57\">57</option>\n    <option value=\"58\">58</option>\n    <option value=\"59\">59</option>\n  </select>\n  <!--<input-->\n    <!--type=\"text\"-->\n    <!--class=\"form-control text-center text-sm ptx-w-40\"-->\n    <!--ptxRestrictNumber-->\n    <!--[disabled]=\"disabled\"-->\n    <!--min=\"0\"-->\n    <!--max=\"59\"-->\n    <!--fraction=\"0\"-->\n    <!--padding=\"2\"-->\n    <!--[(ngModel)]=\"minute\"-->\n    <!--(ngModelChange)=\"OnModelChange()\"-->\n  <!--/>-->\n</div>\n<div class=\"text-inline\">Mins</div>\n<!--<div class=\"form-inline\">-->\n  <!--<input-->\n    <!--type=\"text\"-->\n    <!--class=\"form-control text-center text-sm ptx-w-40\"-->\n    <!--ptxRestrictNumber-->\n    <!--min=\"0\"-->\n    <!--max=\"23\"-->\n    <!--fraction=\"0\"-->\n    <!--padding=\"2\"-->\n    <!--[(ngModel)]=\"hour\"-->\n    <!--[disabled]=\"disabled\"-->\n    <!--(ngModelChange)=\"OnModelChange()\"-->\n  <!--/>-->\n<!--</div>-->\n<!--<div class=\"text-inline\"> : </div>-->\n<!--<div class=\"form-inline\">-->\n  <!--<input-->\n    <!--type=\"text\"-->\n    <!--class=\"form-control text-center text-sm ptx-w-40\"-->\n    <!--ptxRestrictNumber-->\n    <!--[disabled]=\"disabled\"-->\n    <!--min=\"0\"-->\n    <!--max=\"59\"-->\n    <!--fraction=\"0\"-->\n    <!--padding=\"2\"-->\n    <!--[(ngModel)]=\"minute\"-->\n    <!--(ngModelChange)=\"OnModelChange()\"-->\n  <!--/>-->\n<!--</div>-->\n<!--&lt;!&ndash;<input type=\"text\" class=\"form-control text-center text-sm ptx-w-40\" value=\"06\"></div>&ndash;&gt;-->\n<!--&lt;!&ndash;<div class=\"text-inline\"> : </div>&ndash;&gt;-->\n<!--&lt;!&ndash;<div class=\"form-inline\"><input type=\"text\" class=\"form-control text-center text-sm ptx-w-40\" value=\"10\"></div>&ndash;&gt;-->\n"

/***/ }),

/***/ "../../../../../src/app/components/input-time/input-time.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputTimeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InputTimeComponent = (function () {
    function InputTimeComponent() {
        this.hour = '00';
        this.minute = '00';
        this.disabled = false;
        this.HandleStateChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    InputTimeComponent.prototype.ngOnInit = function () {
    };
    InputTimeComponent.prototype.log = function (e) {
        console.log(e);
    };
    InputTimeComponent.prototype.OnModelChange = function () {
        this.HandleStateChange.emit({
            hour: this.hour,
            minute: this.minute,
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputTimeComponent.prototype, "hour", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], InputTimeComponent.prototype, "minute", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], InputTimeComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", Object)
    ], InputTimeComponent.prototype, "HandleStateChange", void 0);
    InputTimeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-input-time',
            template: __webpack_require__("../../../../../src/app/components/input-time/input-time.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], InputTimeComponent);
    return InputTimeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/show-text-and-hrs/show-text-and-hrs.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-xs-7\">\n    <input type=\"text\"\n           class=\"form-control text-center text-sm\"\n           [ngClass]=\"className\"\n           [ngModel]=\"text\"\n           disabled\n    >\n  </div>\n  <div class=\"col-xs-3 pl-0\">\n    <ptx-input-hrs\n      [hrs]=\"hrs\"\n      [className]=\"className\"\n    ></ptx-input-hrs>\n    <!--<input type=\"text\"-->\n           <!--class=\"form-control text-center text-sm\"-->\n           <!--[ngClass]=\"className\"-->\n           <!--[ngModel]=\"ShowHrs(hrs)\"-->\n           <!--disabled-->\n    <!--&gt;-->\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/show-text-and-hrs/show-text-and-hrs.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowTextAndHrsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowTextAndHrsComponent = (function () {
    function ShowTextAndHrsComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.text = '';
        this.hrs = 0;
        this.className = '';
    }
    ShowTextAndHrsComponent.prototype.ngOnInit = function () {
    };
    ShowTextAndHrsComponent.prototype.ShowHrs = function (hrs) {
        hrs = hrs || 0;
        var r = this.decimalPipe.transform(hrs, '1.4-4');
        return r + " Hrs";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowTextAndHrsComponent.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], ShowTextAndHrsComponent.prototype, "hrs", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowTextAndHrsComponent.prototype, "className", void 0);
    ShowTextAndHrsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-show-text-and-hrs',
            template: __webpack_require__("../../../../../src/app/components/show-text-and-hrs/show-text-and-hrs.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common__["d" /* DecimalPipe */]])
    ], ShowTextAndHrsComponent);
    return ShowTextAndHrsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/containers/demurrage-form/demurrage-form.component.html":
/***/ (function(module, exports) {

module.exports = " <!--<pre>{{ model | json }}</pre>-->\n<div class=\"wrapper-detail\">\n  <h2 class=\"first-title\">\n    <span class=\"note\">Demurrage Approval Form </span> Please fill in form as relevant\n  </h2>\n  <div class=\"form-horizontal mb-30\">\n    <div class=\"row\">\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Form ID</label>\n          <div class=\"col-xs-5 pr-10\">\n            <input\n              class=\"form-control\"\n              [(ngModel)]=\"model.DDA_FORM_ID\"\n              type=\"text\"\n              [disabled]=\"true\"\n            >\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Create Date</label>\n          <div class=\"col-xs-5 pr-10\">\n            <ptx-input-date\n              [needtime]=\"true\"\n              [date]=\"model.DDA_DOC_DATE\"\n              (modelChange)=\"model.DDA_DOC_DATE = $event;\"\n              [disabled]=\"model.IS_DISABLED\"\n            ></ptx-input-date>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">For</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_FOR_COMPANY\"\n                    (ngModelChange)=\"OnChangeCompany(model.DDA_FOR_COMPANY)\"\n            >\n              <option value=\"\">-- Select --</option>\n              <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.COMPANY\">{{ elem.VALUE }}</option>\n            </select>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Type of Transaction</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_TYPE_OF_TRANSECTION\"\n            >\n              <option value=\"\">-- select --</option>\n              <option value=\"Sale\">Sale</option>\n              <option value=\"Purchase\">Purchase</option>\n            </select>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Incoterms</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_INCOTERM\"\n            >\n              <option value=\"\">-- Select --</option>\n              <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.INCOTERMS\">{{ elem.VALUE }}</option>\n            </select>\n            <!--<ptx-input-suggestion-->\n              <!--[model]=\"model.DDA_INCOTERM\"-->\n              <!--[data_list]=\"master.INCOTERMS\"-->\n              <!--[is_disabled]=\"model.IS_DISABLED\"-->\n              <!--[show_field]=\"'VALUE'\"-->\n              <!--select_field=\"ID\"-->\n              <!--(ModelChanged)=\"model.DDA_INCOTERM = $event;\"-->\n            <!--&gt;-->\n            <!--</ptx-input-suggestion>-->\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Demurrage</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_DEMURAGE_TYPE\"\n                    (ngModelChange)=\"onUpdateDemType()\"\n            >\n              <option value=\"\">-- select --</option>\n              <option value=\"Paid\">Paid</option>\n              <option value=\"Received\">Received</option>\n            </select>\n          </div>\n        </div>\n      </div>\n    </div>\n    <ng-template [ngIf]=\"IsNeedReferrence()\">\n      <h2 class=\"second-title\">\n        Reference\n      </h2>\n      <div class=\"form-horizontal mb-30\">\n\n        <div class=\"row\">\n          <div class=\"col-xs-12 col-sm-4\">\n            <div class=\"form-group\">\n              <label class=\"col-xs-5 control-label\">Trip No./Document No.</label>\n              <div class=\"col-xs-5\">\n                <input type=\"text\"\n                       class=\"form-control\"\n                       [ngModel]=\"model.DDA_REF_DOC_NO\"\n                       [disabled]=\"model.IS_DISABLED\"\n                 >\n              </div>\n              <div class=\"col-xs-2 p-0\">\n                <ptx-cip-reference\n                  [user_group]=\"model.DDA_USER_GROUP\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [data_set] = \"cip_data\"\n                  (onSelectData)=\"OnCIPSelectData($event)\"\n                  (onSearchCIPRef)=\"OnSearchCIPRef($event)\"\n                ></ptx-cip-reference>\n                <!--<button class=\"btn btn-success btn-sm text-light\">Search</button>-->\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </ng-template>\n    <h2 class=\"second-title\">\n      Details\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-4\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-5 control-label\">Counterparty</label>\n            <div class=\"col-xs-7\">\n              <div class=\"radio\">\n                <input id=\"ship_owner\"\n                       [disabled]=\"model.IS_DISABLED\"\n                       name=\"DDA_COUNTERPARTY_TYPE\"\n                       type=\"radio\"\n                       [(ngModel)]=\"model.DDA_COUNTERPARTY_TYPE\"\n                       [value]=\"'Ship Owner'\"\n                >\n                <label for=\"ship_owner\">Ship Owner</label>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-5\">\n              <input type=\"text\"\n                     class=\"form-control\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_SHIP_OWNER\"\n              >\n            </div>\n            <div class=\"col-xs-2 p-0 text-center\">\n              <div class=\"text text-black\">via Broker</div>\n            </div>\n            <div class=\"col-xs-5\">\n              <ptx-input-suggestion\n                [model]=\"model.DDA_BROKER\"\n                [data_list]=\"master.BROKERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_BROKER = $event;\"\n              >\n              </ptx-input-suggestion>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-4\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-5 control-label\"></label>\n            <div class=\"col-xs-7\">\n              <div class=\"radio\">\n                <input id=\"supplier\"\n                       name=\"DDA_COUNTERPARTY_TYPE\"\n                       type=\"radio\"\n                       [disabled]=\"model.IS_DISABLED\"\n                       [(ngModel)]=\"model.DDA_COUNTERPARTY_TYPE\"\n                       [value]=\"'Supplier as charterer'\"\n                >\n                <label for=\"supplier\">Supplier as charterer</label>\n                <!--<input id=\"radiobox1\" name=\"test1\"  type=\"radio\">-->\n                <!--<label for=\"radiobox1\">Supplier as charterer</label>-->\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-12\">\n              <ptx-input-suggestion\n                [model]=\"model.DDA_SUPPLIER\"\n                [data_list]=\"master.BROKERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_SUPPLIER = $event;\"\n              >\n              </ptx-input-suggestion>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-4\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-5 control-label\"></label>\n            <div class=\"col-xs-7\">\n              <div class=\"radio\">\n                <input id=\"customer\"\n                       [disabled]=\"model.IS_DISABLED\"\n                       name=\"DDA_COUNTERPARTY_TYPE\"\n                       type=\"radio\"\n                       [(ngModel)]=\"model.DDA_COUNTERPARTY_TYPE\"\n                       [value]=\"'Customer'\"\n                >\n                <label for=\"customer\">Customer</label>\n                <!--<input id=\"radiobox1\" name=\"test1\"  type=\"radio\">-->\n                <!--<label for=\"radiobox1\">Customer</label>-->\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-5\">\n              <ptx-input-suggestion\n                [model]=\"model.DDA_CUSTOMER\"\n                [data_list]=\"master.CUSTOMERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_CUSTOMER = $event;\"\n              >\n              </ptx-input-suggestion>\n            </div>\n            <div class=\"col-xs-2 p-0 text-center\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n              <div class=\"text text-black\">via Broker</div>\n            </div>\n            <div class=\"col-xs-5\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n              <ptx-input-suggestion\n                [model]=\"model.DDA_CUSTOMER_BROKER\"\n                [data_list]=\"master.BROKERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_CUSTOMER_BROKER = $event;\"\n              >\n              </ptx-input-suggestion>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">GT&amp;C or CP Type</label>\n            <div class=\"col-xs-7\">\n              <textarea class=\"form-control\"\n                        rows=\"4\"\n                        style=\"min-height: auto; padding-top: 7px;\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.DDA_GT_C\"\n              ></textarea>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Laytime Contract</label>\n            <div class=\"col-xs-6\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [padding]=\"0\"\n                     [fraction]=\"4\"\n                     [(ngModel)]=\"model.DDA_LAYTIME_CONTRACT_HRS\"\n                     (ngModelChange)=\"OnModelChange();\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [(ngModel)]=\"model.DDA_LAYTIME_CONTRACT_TEXT\"\n                     disabled\n              >\n            </div>\n            <div class=\"col-xs-2 p-0\">\n              <div class=\"text text-black\">Hrs</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Vessel Name</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-suggestion\n                [model]=\"model.DDA_VESSEL\"\n                [data_list]=\"master.VESSELS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_VESSEL = $event;\"\n              >\n              </ptx-input-suggestion>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-4\"></div>\n            <div class=\"col-xs-6\">\n              <div class=\"form-inline\" *ngIf=\"IsCMLAGroup() || IsCMMTGroup()\">\n                <input type=\"text\"\n                       class=\"form-control\"\n                       placeholder=\"MT/HOURS\"\n                       style=\"width: 49%;\"\n                       ptxRestrictNumber\n                       [(ngModel)]=\"model.DDA_LAYTIME_QUANTITY_RATE\"\n                       (ngModelChange)=\"doUpdateLaytime()\"\n                       [disabled]=\"model.IS_DISABLED\"\n                >\n                <input type=\"text\"\n                       class=\"form-control\"\n                       placeholder=\"Quantity\"\n                       style=\"width: 49%;\"\n                       ptxRestrictNumber\n                       [(ngModel)]=\"model.DDA_LAYTIME_QUANTITY\"\n                       (ngModelChange)=\"doUpdateLaytime()\"\n                       [disabled]=\"model.IS_DISABLED\"\n                >\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">CP Date/Contract Date</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date\n                [date]=\"model.DDA_CONTRACT_DATE\"\n                (modelChange)=\"model.DDA_CONTRACT_DATE = $event;\"\n                [disabled]=\"model.IS_DISABLED\"\n              ></ptx-input-date>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Currency Unit</label>\n            <div class=\"col-xs-6\">\n              <select class=\"form-control\"\n                      [(ngModel)]=\"model.DDA_CURRENCY_UNIT\"\n                      [disabled]=\"model.IS_DISABLED\"\n              >\n                <option [value]=\"elem.ID\" *ngFor=\"let elem of master.DAF_CURRENCY_UNIT\">{{ elem.VALUE }}</option>\n              </select>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n            <label class=\"col-xs-4 control-label\">Agreed Loading Laycan</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date-range\n                [disabled]=\"model.IS_DISABLED\"\n                [start_date]=\"model.DDA_AGREED_LOADING_LAYCAN_FROM\"\n                [end_date]=\"model.DDA_AGREED_LOADING_LAYCAN_TO\"\n                (modelChange)=\"model.DDA_AGREED_LOADING_LAYCAN_FROM = $event.start_date;model.DDA_AGREED_LOADING_LAYCAN_TO = $event.end_date;\"\n              ></ptx-input-date-range>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Demurrage Rate</label>\n            <div class=\"col-xs-6\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"2\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_DEMURAGE_RATE\"\n                     (ngModelChange)=\"OnModelChange()\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n            <div class=\"col-xs-2 p-0\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }} PDPR</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group mb-15\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n            <label class=\"col-xs-4 control-label\">Agreed Discharging Laycan</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date-range\n                [disabled]=\"model.IS_DISABLED\"\n                [start_date]=\"model.DDA_AGREED_DC_LAYCAN_FROM\"\n                [end_date]=\"model.DDA_AGREED_DC_LAYCAN_TO\"\n                (modelChange)=\"model.DDA_AGREED_DC_LAYCAN_FROM = $event.start_date;model.DDA_AGREED_DC_LAYCAN_TO = $event.end_date;\"\n              ></ptx-input-date-range>\n            </div>\n          </div>\n          <div class=\"form-group \" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n            <label class=\"col-xs-4 control-label\">Agreed Laycan</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date-range\n                [disabled]=\"model.IS_DISABLED\"\n                [start_date]=\"model.DDA_AGREED_LAYCAN_FROM\"\n                [end_date]=\"model.DDA_AGREED_LAYCAN_TO\"\n                (modelChange)=\"model.DDA_AGREED_LAYCAN_FROM = $event.start_date;model.DDA_AGREED_LAYCAN_TO = $event.end_date;\"\n              ></ptx-input-date-range>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n        </div>\n      </div>\n      <div class=\"row mt-10\">\n        <ptx-demurrage-material\n          [model]=\"model\"\n          [master]=\"master\"\n        >\n        </ptx-demurrage-material>\n        <div class=\"col-xs-12 col-sm-4\"></div>\n      </div>\n      <div class=\"row mt-10\">\n        <div class=\"col-xs-12\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-2 control-label\">Sailing Route</label>\n            <div class=\"col-xs-8\">\n              <input type=\"text\"\n                     class=\"form-control\"\n                     [(ngModel)]=\"model.DDA_SAILING_ROUTE\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <h2 class=\"second-title\">\n      Calculation\n    </h2>\n\n    <ptx-demurrage-port\n      [user_group]=\"user_group\"\n      title=\"Load Port\"\n      button_title=\"Add Load Port\"\n      type=\"load\"\n      [model]=\"model\"\n      [master]=\"master\"\n      (handleModelChange)=\"OnModelChange();\"\n    ></ptx-demurrage-port>\n\n    <ptx-demurrage-port\n      [user_group]=\"user_group\"\n      title=\"Discharge Port\"\n      button_title=\"Add Discharge Port\"\n      type=\"discharge\"\n      [model]=\"model\"\n      [master]=\"master\"\n      (handleModelChange)=\"OnModelChange();\"\n    ></ptx-demurrage-port>\n\n    <ptx-demurrage-time-summary\n      [model]=\"model\"\n      [master]=\"master\"\n    >\n    </ptx-demurrage-time-summary>\n\n    <ptx-demurrage-value-usd-summary\n      [model]=\"model\"\n      [master]=\"master\"\n    ></ptx-demurrage-value-usd-summary>\n\n    <h2 class=\"second-title\">\n      Explanation\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n      <ptx-freetext\n        *ngIf=\"true\"\n        [disabled]=\"model.IS_DISABLED_NOTE\"\n        [elementId]=\"'DDA_EXPLANATION'\"\n        [model]=\"model.DDA_EXPLANATION\"\n        (HandleStateChange)=\"model.DDA_EXPLANATION = $event\">\n      </ptx-freetext>\n    </div>\n\n    <h2 class=\"second-title\">\n      Proposal for approval\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Demurrage</label>\n            <div class=\"col-xs-4 pr-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [ngModel]=\"ShowValue(model.DDA_FOR_COMPANY)\"\n                     disabled>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-2 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Paid to</label>\n            <label class=\"col-xs-2 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Received From</label>\n            <div class=\"col-xs-10 pr-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [ngModel]=\"ShowCharterer()\"\n                     disabled>\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row\">\n        <div class=\"col-xs-12 mb-15 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Vessel</label>\n            <div class=\"col-xs-4 pr-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [ngModel]=\"ShowValueVessel(model.DDA_VESSEL)\"\n                     disabled>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Time on Demurrage</label>\n            <div class=\"col-xs-4 p-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-bold\"\n                     [ngModel]=\"model.DDA_TIME_ON_DEM_TEXT\"\n                     disabled>\n            </div>\n            <div class=\"col-xs-2\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     [ngModel]=\"ShowHrs(model.DDA_TIME_ON_DEM_HRS)\"\n                     disabled\n              >\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center mb-15\"-->\n                     <!--ptxRestrictNumber-->\n                     <!--[fraction]=\"4\"-->\n                     <!--[padding]=\"0\"-->\n                     <!--[(ngModel)]=\"model.DDA_TIME_ON_DEM_HRS\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            </div>\n            <div class=\"col-xs-1\">\n              <div class=\"text text-black\">Hrs</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <!--<label class=\"col-xs-4 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Net receiving demurrage value</label>-->\n            <!--<label class=\"col-xs-4 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Net receiving demurrage value</label>-->\n            <label class=\"col-xs-4 control-label\">Demurrage {{ GetSettled() }}</label>\n            <div class=\"col-xs-4\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_NET_PAYMENT_DEM_VALUE\"\n                     (ngModelChange)=\"onUpdateExchange()\"\n                     [disabled]=\"model.IS_DISABLED || true\"\n              >\n            </div>\n            <div class=\"col-xs-1\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n            <!--<div class=\"col-xs-2\">-->\n              <!--<div class=\"form-group\">-->\n                <!--<div class=\"col-xs-12\">-->\n                  <!--<button class=\"btn btn-success btn-sm\" (click)=\"DoRecal()\" >Re - Calculate</button>-->\n                <!--</div>-->\n              <!--</div>-->\n            <!--</div>-->\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Demurrage {{ GetSettled() }} <br> (Manual input)</label>\n            <div class=\"col-xs-4\">\n            <input type=\"text\"\n                   class=\"form-control text-center mb-15\"\n                   ptxRestrictNumber\n                   [fraction]=\"model.DDA_DECIMAL\"\n                   [padding]=\"0\"\n                   [(ngModel)]=\"model.DDA_NET_PAYMENT_DEM_VALUE_OV\"\n                   (ngModelChange)=\"onUpdateNetPayment();onUpdateExchange()\"\n                   [disabled]=\"model.IS_DISABLED\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>\n          </div>\n        </div>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"model.DDA_CURRENCY_UNIT != 'THB'\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Exchange rate - For settlement in Thai Baht</label>\n            <div class=\"col-xs-4 p-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"2\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_EXCHANGE_RATE\"\n                     (ngModelChange)=\"onUpdateExchange()\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n            <div class=\"col-xs-2\">\n              <div class=\"text text-black\">THB/{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\" *ngIf=\"model.DDA_SETTLEMENT_AMOUNT_THB !== null\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Settlement amount</label>\n            <div class=\"col-xs-4\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [ngModel]=\"model.DDA_SETTLEMENT_AMOUNT_THB\"\n                     disabled\n              >\n            </div>\n            <div class=\"col-xs-2\">\n              <div class=\"text text-black\">THB</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"model.DDA_IS_BROKER_COMMISSION === 'Y'\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Broker Commission</label>\n            <!-- <label class=\"col-xs-4 control-label\">Broker Commission<br/>(TOP/TM paid to broker)</label> -->\n            <div class=\"col-xs-4 p-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_BROKER_COMMISSION_VALUE\"\n                     disabled\n              >\n            </div>\n            <div class=\"col-xs-2\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <h2 class=\"second-title\">\n      Reference Attachment\n    </h2>\n    <div class=\"form-horizontal mb-30 pb-30\">\n      <ptx-file-upload\n        btnClass=\"text-left\"\n        [model]=\"model.DAF_ATTACH_FILE\"\n        [is_disabled]=\"model.IS_DISABLED\"\n        [need_caption]=\"false\"\n        [desc]=\"'PDF, Word, Excel, Image, Outlook'\"\n        filterBy=\"BMS\"\n        (handleModelChanged)=\"model.DAF_ATTACH_FILE = $event\"\n      >\n      </ptx-file-upload>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-xs-12\">\n        <div class=\"form-group\">\n          <label class=\"control-label\">Reason for Approval/Reject</label>\n          <textarea\n            class=\"form-control inline-textarea\"\n            [disabled]=\"true\"\n            [(ngModel)]=\"model.DDA_REASON\"></textarea>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"wrapper-button\" id=\"buttonDiv\">\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft(model)\"> SAVE DRAFT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit(model)\"> SAVE & SUBMIT </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify(model)\"> VERIFY </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse(model)\"> ENDORSE </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved(model)\"> APPROVE </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject(model)\"> REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel(model)\"> CANCEL </button>\n      <button class=\"btn btn-default green\" (click)=\"DoGenerateEXCEL(model)\"> Export to Excel  </button>\n      <button class=\"btn btn-default\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>\n      <!--<button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>-->\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/demurrage-form/demurrage-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_daf_data__ = __webpack_require__("../../../../../src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_daf_master__ = __webpack_require__("../../../../../src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_permission_store__ = __webpack_require__("../../../../../src/app/common/permission-store.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_daf_service__ = __webpack_require__("../../../../../src/app/services/daf.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__ = __webpack_require__("../../../../../src/app/common/daf-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_cip_reference_service__ = __webpack_require__("../../../../../src/app/services/cip-reference.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__models_daf_material__ = __webpack_require__("../../../../../src/app/models/daf-material.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__models_daf_port__ = __webpack_require__("../../../../../src/app/models/daf-port.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var DemurrageFormComponent = (function () {
    function DemurrageFormComponent(route, router, loaderService, masterService, dafService, cipReferenceService, decimalPipe, componentFactoryResolver) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.dafService = dafService;
        this.cipReferenceService = cipReferenceService;
        this.decimalPipe = decimalPipe;
        this.componentFactoryResolver = componentFactoryResolver;
        this.model = new __WEBPACK_IMPORTED_MODULE_4__models_daf_data__["a" /* DAF_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_5__models_daf_master__["a" /* DAFMaster */]();
        this.is_duplicate = false;
        this.cip_data = new Array();
    }
    DemurrageFormComponent.prototype.canDeactivate = function () {
        // insert logic to check if there are pending changes here;
        // returning true will navigate without confirmation
        // returning false will show a confirm alert before navigating away
    };
    // @HostListener allows us to also guard against browser refresh, close, etc.
    DemurrageFormComponent.prototype.unloadNotification = function ($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = 'This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)';
        }
    };
    DemurrageFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['req_transaction_id'];
            _this.transaction_id = params['transaction_id'];
            _this.user_group = params['user_group'];
            _this.is_duplicate = params['is_duplicate'];
            _this.model.DDA_USER_GROUP = _this.user_group.toUpperCase();
            // TODO:
            // this.model.DDA_DEMURAGE_TYPE = 'Received';
            // this.model.DDA_INCOTERM = 'FOB';
            // this.model.DDA_TYPE_OF_TRANSECTION = 'Purchase';
            //
            // this.model.DDA_DEMURAGE_TYPE = 'Received';
            // this.model.DDA_INCOTERM = 'CFR';
            // this.model.DDA_TYPE_OF_TRANSECTION = 'Purchase';
            //
            // this.model.DDA_DEMURAGE_TYPE = 'Paid';
            // this.model.DDA_INCOTERM = 'FOB';
            // this.model.DDA_TYPE_OF_TRANSECTION = 'Sale';
            //
            // this.model.DDA_DEMURAGE_TYPE = 'Paid';
            // this.model.DDA_INCOTERM = 'CFR';
            // this.model.DDA_TYPE_OF_TRANSECTION = 'Purchase';
            _this.load();
        });
    };
    DemurrageFormComponent.prototype.ShowValueVessel = function (id) {
        return __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].getTextByID(id, this.master.VESSELS);
    };
    DemurrageFormComponent.prototype.ShowValue = function (id) {
        return __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].getTextByID(id, this.master.COMPANY);
    };
    DemurrageFormComponent.prototype.ShowCharterer = function () {
        var result = '';
        var counter_party_type = this.model.DDA_COUNTERPARTY_TYPE;
        counter_party_type = counter_party_type && counter_party_type.toLowerCase();
        switch (counter_party_type) {
            case 'ship owner':
                var DDA_SHIP_OWNER = this.model.DDA_SHIP_OWNER || '';
                if (DDA_SHIP_OWNER !== '') {
                    result += DDA_SHIP_OWNER + ' (Ship Owner)';
                }
                var broker = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].getTextByID(this.model.DDA_BROKER, this.master.BROKERS);
                if (broker !== '') {
                    result += ' via ' + broker + ' (Broker)';
                }
                break;
            case "supplier as charterer":
                var vendor = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].getTextByID(this.model.DDA_SUPPLIER, this.master.BROKERS);
                if (vendor !== '') {
                    result = vendor + ' (Supplier as charterer)';
                }
                break;
            case "customer":
                var customer = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].getTextByID(this.model.DDA_CUSTOMER, this.master.CUSTOMERS);
                if (customer !== '') {
                    result = customer + ' (Customer)';
                }
                var customer_broker = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].getTextByID(this.model.DDA_CUSTOMER_BROKER, this.master.BROKERS);
                if (customer_broker !== '') {
                    result += ' via ' + customer_broker + ' (Broker)';
                }
                break;
        }
        // console.log(`ShowCharterer ${result}`);
        return result;
    };
    DemurrageFormComponent.prototype.load = function () {
        var _this = this;
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = res;
            _this.master.REAL_CUSTOMER = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(_this.master.CUSTOMERS);
            if (_this.req_transaction_id) {
                _this.dafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    if (_this.is_duplicate) {
                        _this.model.Buttons = __WEBPACK_IMPORTED_MODULE_8__common__["b" /* Button */].getDefaultButton();
                        _this.model.IS_DISABLED = false;
                        _this.model.DDA_REASON = '';
                        _this.model.DDA_FORM_ID = '';
                        _this.model.DDA_DOC_DATE = __WEBPACK_IMPORTED_MODULE_11_moment_moment__().format('YYYY-MM-DD HH:mm:ss');
                    }
                    _this.model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].CalculateTime(_this.model);
                    _this.model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].CalValue(_this.model);
                    _this.OnChangeCompany(_this.model.DDA_FOR_COMPANY);
                    // this.model.SHOW_COUTER_PARTY = this.ShowCharterer();
                    _this.OverrideGroupValue(_this.model);
                    _this.loaderService.display(false);
                }, function (err) {
                    console.log(err);
                    _this.loaderService.display(false);
                });
            }
            else {
                _this.model.Buttons = __WEBPACK_IMPORTED_MODULE_8__common__["b" /* Button */].getDefaultButton();
                if (_this.user_group.toUpperCase() === 'CMMT') {
                    _this.model.DDA_DEMURAGE_TYPE = 'Received';
                }
                _this.loaderService.display(false);
            }
            _this.OverrideGroupValue(_this.model);
        }, function (error) {
            console.log(error);
        });
    };
    DemurrageFormComponent.prototype.OverrideGroupValue = function (model) {
        switch (this.user_group.toUpperCase()) {
            case 'CMPS':
            case 'CMLA':
                model.DDA_MATERIAL_TYPE = 'Product';
                break;
            case 'CMMT':
                model.DDA_MATERIAL_TYPE = 'Product';
                break;
            default:
                model.DDA_MATERIAL_TYPE = 'Crude & F/S';
                break;
        }
        return model;
    };
    DemurrageFormComponent.prototype.ShowHrs = function (hrs) {
        var r = this.decimalPipe.transform(hrs, '1.10-10');
        return r;
    };
    DemurrageFormComponent.prototype.IsNeedReferrence = function () {
        var result = this.user_group.toUpperCase() !== 'CMLA';
        return result;
    };
    DemurrageFormComponent.prototype.IsCMLAGroup = function () {
        return this.user_group.toUpperCase() === 'CMLA';
    };
    DemurrageFormComponent.prototype.IsCMCSGroup = function () {
        return this.user_group.toUpperCase() === 'CMCS';
    };
    DemurrageFormComponent.prototype.ISCMPSGroup = function () {
        return this.user_group.toUpperCase() === 'CMPS';
    };
    DemurrageFormComponent.prototype.IsCMMTGroup = function () {
        return this.user_group.toUpperCase() === 'CMMT';
    };
    DemurrageFormComponent.prototype.OnModelChange = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
        // this.model.SHOW_COUTER_PARTY = this.ShowCharterer();
        // this.model = DAFHelper.CalValue(this.model);
    };
    DemurrageFormComponent.prototype.GetSettled = function () {
        // if (this.model.DDA_DEMURAGE_TYPE === 'Received' && this.model.DDA_COUNTERPARTY_TYPE === 'Supplier as charterer') {
        //   return 'Sharing Settled';
        // } else if (this.model.DDA_DEMURAGE_TYPE === 'Received' && this.model.DDA_COUNTERPARTY_TYPE === 'Customer') {
        //   return 'Received Settled';
        // } else
        if (this.model.DDA_DEMURAGE_TYPE === 'Received') {
            return 'Received Settled';
        }
        else {
            return 'Paid Settled';
        }
        // return 'Settled';
    };
    DemurrageFormComponent.prototype.log = function () {
        console.log(this.model);
    };
    DemurrageFormComponent.prototype.GetCustomers = function (company) {
        company = company || "";
        // console
        var customers = __WEBPACK_IMPORTED_MODULE_10_lodash__["filter"](this.master.REAL_CUSTOMER, function (e) {
            return e.COMPANY_CODE && e.COMPANY_CODE.trim() == company.trim();
        });
        customers = __WEBPACK_IMPORTED_MODULE_10_lodash__["uniqBy"](customers, 'ID');
        console.log(company, customers);
        // console.log(company, customers);
        // console.log(company, customers);
        return customers;
    };
    DemurrageFormComponent.prototype.OnChangeCompany = function (d) {
        // this.master.CUSTOMERS = _.
        this.master.CUSTOMERS = this.GetCustomers(this.model.DDA_FOR_COMPANY);
        console.log(d);
    };
    DemurrageFormComponent.prototype.AddMaterials = function (e) {
        var _this = this;
        if (e && e.CIP_MATERIALS) {
            this.model.DAF_MATERIAL = [];
            __WEBPACK_IMPORTED_MODULE_10_lodash__["each"](e.CIP_MATERIALS, function (f) {
                var mat = new __WEBPACK_IMPORTED_MODULE_13__models_daf_material__["a" /* DAF_MATERIAL */]();
                mat.DMT_MET_NUM = f.MET_NUM;
                mat.DMT_BL_DATE = f.BL_DATE;
                _this.model.DAF_MATERIAL.push(mat);
            });
        }
    };
    DemurrageFormComponent.prototype.OnCIPSelectData = function (e) {
        var _this = this;
        console.log(e);
        this.model.DDA_REF_DOC_NO = e && e.TRIP_NO;
        // this.model.DDA_AGREED_LAYCAN_FROM = e && e.LAYCAN_FROM;
        // this.model.DDA_AGREED_LAYCAN_TO = e && e.LAYCAN_TO;
        this.model.DDA_VESSEL = e && e.VESSEL_CODE;
        console.log(this.IsCMCSGroup());
        if (this.IsCMCSGroup() || this.ISCMPSGroup()) {
            this.model.DDA_COUNTERPARTY_TYPE = '';
            this.model.DDA_SHIP_OWNER = null;
            this.model.DDA_SUPPLIER = null;
            this.model.DDA_BROKER = null;
            this.model.DDA_VESSEL = null;
            this.model.DDA_DEMURAGE_RATE = null;
            this.model.DDA_LAYTIME_CONTRACT_HRS = null;
            this.model.DAF_MATERIAL = [];
            var is_cfr = __WEBPACK_IMPORTED_MODULE_10_lodash__["includes"](['CFR', 'DDP', 'DDU', 'DEG', 'DES', 'CIF', 'CIP', 'CPT', 'DAF', 'DAP'], this.model.DDA_INCOTERM);
            var is_fob = !is_cfr; //_.includes(['FOB'], this.model.DDA_INCOTERM);
            if (this.model.DDA_DEMURAGE_TYPE === 'Paid' && is_fob && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                console.log('ss');
                this.model.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
                this.model.DDA_SHIP_OWNER = e.SUPPLIER_NAME;
                this.model.DDA_BROKER = e.BROKER_CODE;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT && e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
                // this.model.DDA_SHIP_OWNER = e.
                console.log(e);
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Paid' && is_fob && this.model.DDA_TYPE_OF_TRANSECTION === 'Sale') {
                console.log('ss');
                this.model.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
                this.model.DDA_SHIP_OWNER = e.SUPPLIER_NAME;
                this.model.DDA_BROKER = e.BROKER_CODE;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT && e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
                // this.model.DDA_SHIP_OWNER = e.
                console.log(e);
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Received' && is_cfr && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                this.model.DDA_COUNTERPARTY_TYPE = 'Supplier as charterer';
                this.model.DDA_SUPPLIER = e.SUPPLIER_MAT_CODE;
                this.model.DDA_GT_C = e.GT_C;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_AGREED_LAYCAN_FROM = e.DISCHARGE_LAYCAN_FROM;
                this.model.DDA_AGREED_LAYCAN_TO = e.DISCHARGE_LAYCAN_TO;
                // this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                // this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Received' && is_fob && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                this.model.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
                this.model.DDA_SHIP_OWNER = e.SUPPLIER_NAME;
                this.model.DDA_BROKER = e.BROKER_CODE;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT && e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Paid' && is_cfr && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                this.model.DDA_COUNTERPARTY_TYPE = 'Supplier as charterer';
                this.model.DDA_SUPPLIER = e.SUPPLIER_MAT_CODE;
                this.model.DDA_GT_C = e.GT_C;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_AGREED_DC_LAYCAN_FROM = e.DISCHARGE_LAYCAN_FROM;
                this.model.DDA_AGREED_DC_LAYCAN_TO = e.DISCHARGE_LAYCAN_TO;
                // this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                // this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
            }
            return;
        }
        else if (this.IsCMMTGroup()) {
            this.model.DDA_COUNTERPARTY_TYPE = '';
            this.model.DDA_SHIP_OWNER = null;
            this.model.DDA_SUPPLIER = null;
            this.model.DDA_BROKER = null;
            this.model.DDA_VESSEL = null;
            this.model.DDA_DEMURAGE_RATE = null;
            this.model.DDA_LAYTIME_CONTRACT_HRS = null;
            this.model.DAF_MATERIAL = [];
            this.model.DAF_PORT = [];
            this.model.DDA_VESSEL = e && e.VESSEL_CODE;
            this.model.DDA_CUSTOMER = e && e.CUSTOMER_CODE;
            this.model.DDA_CUSTOMER_BROKER = e && e.BROKER_CODE;
            this.model.DDA_COUNTERPARTY_TYPE = 'Customer';
            this.model.DDA_AGREED_LAYCAN_FROM = e && e.LAYCAN_FROM;
            this.model.DDA_AGREED_LAYCAN_TO = e && e.LAYCAN_TO;
            this.model.DDA_CONTRACT_DATE = e && e.CP_DATE;
            this.model.DDA_LAYTIME_CONTRACT_HRS = e && e.LAYTIME;
            this.model.DDA_DEMURAGE_RATE = e && e.DEMURRAGE;
            if (e && e.CHO_CARGOS) {
                this.model.DAF_MATERIAL = [];
                __WEBPACK_IMPORTED_MODULE_10_lodash__["each"](e.CHO_CARGOS, function (f) {
                    var mat = new __WEBPACK_IMPORTED_MODULE_13__models_daf_material__["a" /* DAF_MATERIAL */]();
                    mat.DMT_MET_NUM = f.MAT_NUM;
                    // mat.DMT_BL_DATE = f.;
                    _this.model.DAF_MATERIAL.push(mat);
                });
            }
            if (e && e.CHO_PORTS_D) {
                __WEBPACK_IMPORTED_MODULE_10_lodash__["each"](e.CHO_PORTS_D, function (f) {
                    var port = new __WEBPACK_IMPORTED_MODULE_14__models_daf_port__["a" /* DAF_PORT */]('discharge');
                    port.DPT_LOAD_PORT_FK_JETTY = f.PORT_ID;
                    _this.model.DAF_PORT.push(port);
                });
            }
            if (e && e.CHO_PORTS_L) {
                __WEBPACK_IMPORTED_MODULE_10_lodash__["each"](e.CHO_PORTS_L, function (f) {
                    var port = new __WEBPACK_IMPORTED_MODULE_14__models_daf_port__["a" /* DAF_PORT */]('load');
                    port.DPT_LOAD_PORT_FK_JETTY = f.PORT_ID;
                    _this.model.DAF_PORT.push(port);
                });
            }
            this.OnModelChange();
            return;
        }
        this.model.DDA_SUPPLIER = e && e.SUPPLIER_CODE;
        this.model.DDA_COUNTERPARTY_TYPE = 'Supplier as charterer';
        this.model.DDA_AGREED_LOADING_LAYCAN_FROM = e && e.LOADING_DATE_FROM;
        this.model.DDA_AGREED_LOADING_LAYCAN_TO = e && e.LOADING_DATE_TO;
        var mat = new __WEBPACK_IMPORTED_MODULE_13__models_daf_material__["a" /* DAF_MATERIAL */]();
        if (e && e.CRUDE_NAMES) {
            this.model.DAF_MATERIAL = [];
            this.model.DAF_MATERIAL.push(mat);
        }
    };
    DemurrageFormComponent.prototype.onUpdateDemType = function () {
        if (this.model.DDA_DEMURAGE_TYPE === 'Paid') {
            this.model.DDA_IS_BROKER_COMMISSION = 'N';
            this.model.DDA_IS_SHARING_TO_TOP = 'N';
            this.model.DDA_BROKER_COMMISSION_VALUE = 0;
        }
    };
    DemurrageFormComponent.prototype.onUpdateExchange = function () {
        var DDA_EXCHANGE_RATE = this.model.DDA_EXCHANGE_RATE;
        var DDA_NET_PAYMENT_DEM_VALUE = this.model.DDA_NET_PAYMENT_DEM_VALUE_OV;
        // DDA_EXCHANGE_RATE = DDA_EXCHANGE_RATE || '';
        console.log(DDA_EXCHANGE_RATE, DDA_EXCHANGE_RATE);
        if (DDA_EXCHANGE_RATE == null || DDA_EXCHANGE_RATE.toString() === '') {
            this.model.DDA_SETTLEMENT_AMOUNT_THB = null;
        }
        else {
            DDA_EXCHANGE_RATE = DDA_EXCHANGE_RATE || 0;
            DDA_NET_PAYMENT_DEM_VALUE = DDA_NET_PAYMENT_DEM_VALUE || 0;
            this.model.DDA_SETTLEMENT_AMOUNT_THB = __WEBPACK_IMPORTED_MODULE_10_lodash__["round"](+DDA_EXCHANGE_RATE * +DDA_NET_PAYMENT_DEM_VALUE, 2);
        }
        return this.model.DDA_SETTLEMENT_AMOUNT_THB;
    };
    DemurrageFormComponent.prototype.onUpdateNetPayment = function () {
        this.model.DDA_IS_OVERRIDE_NET_PAYMENT = 'Y';
    };
    DemurrageFormComponent.prototype.DoRecal = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
        this.model.DDA_IS_OVERRIDE_NET_PAYMENT = 'N';
        this.onUpdateExchange();
    };
    DemurrageFormComponent.prototype.doUpdateLaytime = function () {
        var DDA_LAYTIME_QUANTITY = this.model.DDA_LAYTIME_QUANTITY;
        var DDA_LAYTIME_QUANTITY_RATE = this.model.DDA_LAYTIME_QUANTITY_RATE;
        if (DDA_LAYTIME_QUANTITY && DDA_LAYTIME_QUANTITY_RATE) {
            this.model.DDA_LAYTIME_CONTRACT_HRS = __WEBPACK_IMPORTED_MODULE_10_lodash__["round"](DDA_LAYTIME_QUANTITY / DDA_LAYTIME_QUANTITY_RATE, 4);
            this.model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
        }
    };
    //
    // CalSettleMent(): number {
    //   let DDA_EXCHANGE_RATE = this.model.DDA_EXCHANGE_RATE;
    //   let DDA_NET_PAYMENT_DEM_VALUE = this.model.DDA_NET_PAYMENT_DEM_VALUE;
    //
    //   if(DDA_EXCHANGE_RATE === null || DDA_EXCHANGE_RATE === '') {
    //     this.model.DDA_SETTLEMENT_AMOUNT_THB = null;
    //   } else {
    //     DDA_EXCHANGE_RATE = DDA_EXCHANGE_RATE || 0;
    //     DDA_NET_PAYMENT_DEM_VALUE = DDA_NET_PAYMENT_DEM_VALUE || 0;
    //     this.model.DDA_SETTLEMENT_AMOUNT_THB = +DDA_EXCHANGE_RATE * +DDA_NET_PAYMENT_DEM_VALUE;
    //   }
    //
    //   return this.model.DDA_SETTLEMENT_AMOUNT_THB;
    // }
    DemurrageFormComponent.prototype.OnSearchCIPRef = function (criteria) {
        var _this = this;
        // var dialog = bootbox.dialog({
        //     title: 'Processing.....',
        //     message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
        // });
        // dialog.init(function(){
        this.cipReferenceService.search({}).subscribe(function (res) {
            _this.cip_data = res;
            console.log(_this.cip_data);
        }, function (error) {
            console.log(error);
        });
        // setTimeout(function(){
        //     // dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');
        // }, 3000);
        // });
    };
    DemurrageFormComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    DemurrageFormComponent.prototype.DoSubmit = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                _this.req_transaction_id = res.req_transaction_id;
                _this.transaction_id = res.transaction_id;
                context.dafService.Submit(model).subscribe(function (res2) {
                    __WEBPACK_IMPORTED_MODULE_3_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        context.load();
                        context.router.navigate(["/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    DemurrageFormComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    context.load();
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.dafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    DemurrageFormComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    context.load();
                    // context.router.navigate(['/cmps/search'])
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.dafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    DemurrageFormComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_8__common__["b" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.DDA_REASON = result;
                        _this.dafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                context.load();
                                // window.location.href = `${BASE_API}/../../`;
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    DemurrageFormComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_8__common__["b" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.DDA_REASON = result;
                        _this.dafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                context.load();
                                // window.location.href = `../../web/MainBoards.aspx`;
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    DemurrageFormComponent.prototype.DoApproved = function () {
        var _this = this;
        // const context = this;
        // const model = Utility.clone(this.model);
        // swal({
        //     title: "Approve",
        //     text: "Are you sure you want to approve?",
        //     type: "info",
        //     showCancelButton: true,
        //     closeOnConfirm: false,
        //     showLoaderOnConfirm: true,
        //   },
        //   () => {
        //     context.dafService.Approve(model).subscribe(
        //       (res) => {
        //         swal({title:"Approved!", type: "success"}, () => {
        //           window.location.href = `${BASE_API}/../../`;
        //         });
        //       },
        //       (err) => {
        //         swal("Something went wrong!", "submit error");
        //       }
        //     );
        //   });
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_8__common__["b" /* Button */].getApproveMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    result = '-';
                }
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Approve.", function () { });
                    }
                }
                else {
                    var loading_3 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_3.init(function () {
                        model.DDA_REASON = result;
                        _this.dafService.Approve(model).subscribe(function (res) {
                            loading_3.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
                                title: "Approved",
                                text: "",
                                type: "success"
                            }, function () {
                                context.load();
                                // window.location.href = `../../web/MainBoards.aspx`;
                            });
                        }, function (err) {
                            loading_3.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    DemurrageFormComponent.prototype.DoGeneratePDF = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(context.model);
        if (model.DDA_STATUS === '' || !model.DDA_STATUS || model.DDA_STATUS === 'DRAFT') {
            model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
            __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
                title: "Save",
                text: "Are you sure you want to save?",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: true,
            }, function () {
                context.dafService.SaveDraft(model).subscribe(function (res) {
                    console.log(res);
                    context.model.TRAN_ID = res.transaction_id;
                    model.TRAN_ID = res.transaction_id;
                    _this.dafService.GeneratePDF(model).subscribe(function (res2) {
                        window.open(__WEBPACK_IMPORTED_MODULE_8__common__["a" /* BASE_API */] + "/../../web/report/tmpfile/" + res2, '_blank');
                        context.router.navigate(["/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    }, function (error) {
                        context.router.navigate(["/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "save error");
                });
            });
        }
        else {
            this.dafService.GeneratePDF(model).subscribe(function (res) {
                window.open(__WEBPACK_IMPORTED_MODULE_8__common__["a" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            });
        }
    };
    DemurrageFormComponent.prototype.DoGenerateEXCEL = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_8__common__["e" /* Utility */].clone(this.model);
        if (model.DDA_STATUS === '' || !model.DDA_STATUS || model.DDA_STATUS === 'DRAFT') {
            model = __WEBPACK_IMPORTED_MODULE_9__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
            __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
                title: "Save",
                text: "Are you sure you want to save?",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: true,
            }, function () {
                context.dafService.SaveDraft(model).subscribe(function (res) {
                    console.log(res);
                    context.model.TRAN_ID = res.transaction_id;
                    model.TRAN_ID = res.transaction_id;
                    _this.dafService.GenExcel(model).subscribe(function (res2) {
                        window.open(res2);
                        context.router.navigate(["/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    }, function (error) {
                        context.router.navigate(["/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_3_sweetalert__("Something went wrong!", "save error");
                });
            });
        }
        else {
            this.dafService.GenExcel(model).subscribe(function (res) {
                window.open(res);
                // window.open(`${BASE_API}/../../web/report/tmpfile/${res}`, '_blank');
            });
        }
        // this.dafService.GenExcel(model).subscribe(
        //   (res) => {
        //     console.log(res);
        //     window.open(res);
        //     // window.open(`${BASE_API}/../../web/report/tmpfile/${res}`, '_blank');
        //   }
        // )
    };
    DemurrageFormComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToReject(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_6__common_permission_store__["a" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('window:beforeunload', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], DemurrageFormComponent.prototype, "unloadNotification", null);
    DemurrageFormComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-demurrage-form',
            template: __webpack_require__("../../../../../src/app/containers/demurrage-form/demurrage-form.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services__["a" /* LoaderService */],
            __WEBPACK_IMPORTED_MODULE_2__services__["b" /* MasterService */],
            __WEBPACK_IMPORTED_MODULE_7__services_daf_service__["a" /* DafService */],
            __WEBPACK_IMPORTED_MODULE_12__services_cip_reference_service__["a" /* CipReferenceService */],
            __WEBPACK_IMPORTED_MODULE_15__angular_common__["d" /* DecimalPipe */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* ComponentFactoryResolver */]])
    ], DemurrageFormComponent);
    return DemurrageFormComponent;
}());



/***/ }),

/***/ "../../../../../src/app/containers/playground/playground.component.html":
/***/ (function(module, exports) {

module.exports = "<pre>{{ model | json }}</pre>\n<div class=\"wrapper-detail\">\n  <div class=\"row\">\n    <div class=\"col-xs-2 p-0 text-center\">\n      <div class=\"text text-black\">via Broker</div>\n    </div>\n    <div class=\"col-xs-5\">\n      <ptx-input-suggestion-ui\n        [data_list]=\"master.CUSTOMERS\"\n        [show_field]=\"'VALUE'\"\n        select_field=\"ID\"\n        [model]=\"model.SELECT_CUSTOMER\"\n        (ModelChanged)=\"model.SELECT_CUSTOMER = $event\"\n      ></ptx-input-suggestion-ui>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-xs-2 p-0 text-center\">\n      <div class=\"text text-black\">via Broker</div>\n    </div>\n    <div class=\"col-xs-5\">\n      <ptx-input-suggestion-ui\n        [data_list]=\"master.CUSTOMERS\"\n        [show_field]=\"'VALUE'\"\n        select_field=\"ID\"\n        [is_start_with]=\"false\"\n        [model]=\"model.SELECT_CUSTOMER\"\n        (ModelChanged)=\"model.SELECT_CUSTOMER = $event\"\n      ></ptx-input-suggestion-ui>\n    </div>\n  </div>\n\n  <h2 class=\"first-title\">\n    <span class=\"note\">Demurrage Approval Form </span> Please fill in form as relevant\n  </h2>\n  <br/>\n\n  <ptx-input-date\n    [disabled]=\"false\"\n    [date]=\"model.date\"\n    (modelChange)=\"model.date = $event\"\n  ></ptx-input-date>\n  <br/>\n\n\n  <ptx-input-date-range\n    [disabled]=\"false\"\n    [start_date]=\"model.date_from\"\n    [end_date]=\"model.date_to\"\n    (modelChange)=\"model.date_from = $event.start_date;model.date_to = $event.end_date;\"\n  ></ptx-input-date-range>\n  <br/>\n\n  <ptx-input-time\n    [hour]=\"model.hour\"\n    [minute]=\"model.minute\"\n    (HandleStateChange)=\"model.hour = $event.hour; model.minute = $event.minute\"\n  >\n  </ptx-input-time>\n\n  <br/>\n\n  <input\n    type=\"text\"\n    class=\"form-control\"\n    ptxRestrictNumber\n    padding=\"0\"\n    fraction=\"2\"\n    [(ngModel)]=\"model.number\"\n  />\n\n  <br/>\n\n  <ptx-freetext\n    *ngIf=\"true\"\n    [disabled]=\"false\"\n    [elementId]=\"'my_text'\"\n    [model]=\"model.my_text\"\n    (HandleStateChange)=\"model.my_text = $event\">\n  </ptx-freetext>\n  <br/>\n\n  <ptx-input-suggestion\n    [is_disabled]=\"false\"\n    [model]=\"model.sugester\"\n    [data_list]=\"model.dropdownlist\"\n    [show_field]=\"'VALUE'\"\n    [select_field]=\"'ID'\"\n    (ModelChanged)=\"model.sugester = $event;\"\n  >\n  </ptx-input-suggestion>\n  <br/>\n  <br/>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/playground/playground.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlaygroundComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_master__ = __webpack_require__("../../../../../src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PlaygroundComponent = (function () {
    function PlaygroundComponent(masterService) {
        this.masterService = masterService;
        this.model = {
            hour: '00',
            minute: '00',
            dropdownlist: [{
                    ID: 'ddd',
                    VALUE: 'ssss',
                }],
            SELECT_CUSTOMER: '0000100092',
        };
        this.master = new __WEBPACK_IMPORTED_MODULE_1__models_daf_master__["a" /* DAFMaster */]();
    }
    PlaygroundComponent.prototype.GetCustomers = function (company) {
        company = company || '';
        // console
        var customers = __WEBPACK_IMPORTED_MODULE_4_lodash__["filter"](this.master.REAL_CUSTOMER, function (e) {
            return e.COMPANY_CODE && e.COMPANY_CODE.trim() === company.trim();
        });
        customers = __WEBPACK_IMPORTED_MODULE_4_lodash__["uniqBy"](customers, 'ID');
        console.log(company, customers);
        // console.log(company, customers);
        // console.log(company, customers);
        return customers;
    };
    PlaygroundComponent.prototype.OnChangeCompany = function (d) {
        // this.master.CUSTOMERS = _.
        this.master.CUSTOMERS = this.GetCustomers(d);
        console.log(d);
    };
    PlaygroundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.masterService.getList().subscribe(function (res) {
            _this.master = res;
            _this.master.REAL_CUSTOMER = __WEBPACK_IMPORTED_MODULE_2__common_utility__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            console.log(_this.master.REAL_CUSTOMER);
            setTimeout(function () {
                _this.model.SELECT_CUSTOMER = '0000000022';
                _this.OnChangeCompany('1100');
            }, 2000);
        });
    };
    PlaygroundComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-playground',
            template: __webpack_require__("../../../../../src/app/containers/playground/playground.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__services_master_service__["a" /* MasterService */]])
    ], PlaygroundComponent);
    return PlaygroundComponent;
}());



/***/ }),

/***/ "../../../../../src/app/containers/think/think.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model | json }}</pre> -->\n<div class=\"wrapper-detail\">\n\t<h2 class=\"first-title\">\n\t\t<span class=\"note\">Demurrage Approval Form </span> Please fill in form as relevant\n\t</h2>\n\t<div class=\"form-horizontal mb-30\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-xs-5 control-label\">Form ID</label>\n\t\t\t\t\t<div class=\"col-xs-5 pr-10\">\n\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" disabled>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-xs-5 control-label\">Create Date</label>\n\t\t\t\t\t<div class=\"col-xs-5 pr-10\">\n\t\t\t\t\t\t<ptx-input-date\n\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t              [disabled]=\"true\"\n\t\t\t            ></ptx-input-date>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-xs-5 control-label\">For</label>\n\t\t\t\t\t<div class=\"col-xs-5 pr-10\">\n\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t            </select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-xs-5 control-label\">Type of Transaction</label>\n\t\t\t\t\t<div class=\"col-xs-5 pr-10\">\n\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t            </select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-xs-5 control-label\">Incoterms</label>\n\t\t\t\t\t<div class=\"col-xs-5 pr-10\">\n\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t            </select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t<label class=\"col-xs-5 control-label\">Demurrage</label>\n\t\t\t\t\t<div class=\"col-xs-5 pr-10\">\n\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t            </select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\n\t\t<h2 class=\"second-title\">\n\t\t\tReference\n\t\t</h2>\n\t\t<div class=\"form-horizontal mb-30\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Trip No./Document No.</label>\n\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" disabled>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0\">\n\t\t\t\t\t\t\t<button class=\"btn btn-success btn-sm text-light\">Search</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<h2 class=\"second-title\">\n\t\t\tClaims Details\n\t\t</h2>\n\t\t<div class=\"form-horizontal mb-30\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Counterparty</label>\n\t\t\t\t\t\t<div class=\"col-xs-7\">\n\t\t\t\t\t\t\t<div class=\"radio\">\n\t                            <input id=\"radiobox1\" name=\"test1\"  type=\"radio\" checked>\n\t                            <label for=\"radiobox1\">Ship Owner</label>\n\t                        </div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0 text-center\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">via Broker</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t\t            </select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-5 control-label\"></label>\n\t\t\t\t\t\t<div class=\"col-xs-7\">\n\t\t\t\t\t\t\t<div class=\"radio\">\n\t                            <input id=\"radiobox1\" name=\"test1\"  type=\"radio\">\n\t                            <label for=\"radiobox1\">Supplier as charterer</label>\n\t                        </div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t\t            </select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-5 control-label\"></label>\n\t\t\t\t\t\t<div class=\"col-xs-7\">\n\t\t\t\t\t\t\t<div class=\"radio\">\n\t                            <input id=\"radiobox1\" name=\"test1\"  type=\"radio\">\n\t                            <label for=\"radiobox1\">Customer</label>\n\t                        </div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-5\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t\t            </select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"form-horizontal mb-30\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">GT&C or CP Type</label>\n\t\t\t\t\t\t<div class=\"col-xs-7\">\n\t\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" style=\"min-height: auto; padding-top: 7px;\"></textarea>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Laytime - Contract</label>\n\t\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center mb-15\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" disabled>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">Hrs.</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Vessel Name</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\">-- select --</option>\n\t\t\t\t            </select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">CP Date/Contract Date</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Currency Unit</label>\n\t\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\"> USD </option>\n\t\t\t\t            </select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Agreed Loading Laycan</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Demurrage Rate (PDPR)</label>\n\t\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">USD</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group mb-15\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Agreed Discharging Laycan</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group \">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Agreed Laycan</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row mt-10\">\n\t\t\t\t<div class=\"col-xs-4 col-sm-2\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-12 control-label pt-0\">\n\t\t\t\t\t\t\t<div class=\"text-inline\">Type of</div>\n\t\t\t\t\t\t\t<div class=\"form-inline\">\n\t\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t\t              <option value=\"\"> Product </option>\n\t\t\t\t\t            </select>\n\t\t\t\t\t        </div>\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\"> Product </option>\n\t\t\t\t            </select>\n\t\t\t\t        </div>\n\t\t\t\t        <div class=\"col-xs-2 p-0 text-right\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">BL Date</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0\">\n\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\"> Product </option>\n\t\t\t\t            </select>\n\t\t\t\t        </div>\n\t\t\t\t        <div class=\"col-xs-2 p-0 text-right\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">BL Date</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0\">\n\t\t\t\t\t\t\t<a class=\"btn-icon red\" href=\"javascript:void(0)\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<select class=\"form-control\">\n\t\t\t\t              <option value=\"\"> Product </option>\n\t\t\t\t            </select>\n\t\t\t\t        </div>\n\t\t\t\t        <div class=\"col-xs-2 p-0 text-right\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">BL Date</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<ptx-input-date\n\t\t\t\t              [date]=\"model.DOC_DATE\"\n\t\t\t\t              (modelChange)=\"model.DOC_DATE = $event;\"\n\t\t\t\t              [disabled]=\"true\"\n\t\t\t\t            ></ptx-input-date>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2 p-0\">\n\t\t\t\t\t\t\t<a class=\"btn-icon red\" href=\"javascript:void(0)\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-xs-12\">\n\t\t\t\t\t\t\t<button class=\"btn btn-success btn-sm\">Add</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-4\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row mt-10\">\n\t\t\t\t<div class=\"col-xs-12\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-2 control-label\">Sailing Route</label>\n\t\t\t\t\t\t<div class=\"col-xs-10\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<h2 class=\"second-title\">\n\t\t\tCalculation\n\t\t</h2>\n\t\t<div class=\"p-0 col-xs-12 mb-30\">\n\t\t\t<div class=\"text-bold\">Load Port</div>\n\t\t\t<!-- <app-table></app-table> -->\n      <ng-template #containerAppTable></ng-template>\n\t\t\t<div class=\"text-bold\">Discharge Port</div>\n\t\t\t<div class=\"mt-15\"><button class=\"btn btn-success m-0\">Add Discharge Port</button></div>\n\n\n\t\t\t<div class=\"text-bold mt-30 pt-30\">Demurrage Time Summary</div>\n\t\t\t<div class=\"table-responsive mt-15 mb-30 pb-30\">\n\t\t\t\t<table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n\t\t\t\t\t<thead>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td class=\"text-bold ptx-w-400\"></td>\n\t\t\t\t\t\t\t<td class=\"text-bold ptx-w-300\">Original Claim</td>\n\t\t\t\t\t\t\t<td class=\"text-bold ptx-w-300\">TOP Reviewed</td>\n\t\t\t\t\t\t\t<td class=\"text-bold ptx-w-300\">Settled</td>\n\t\t\t\t\t\t\t<td class=\"text-bold ptx-w-300\">Saving</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</thead>\n\t\t\t\t\t<tbody>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td class=\"text-left\">Total Running Time (Load Port + Discharge Port)</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td class=\"text-left\">Total Deduction Time (Load Port + Discharge Port)</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td class=\"text-left\">Net Time Spent</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td class=\"text-left\">Laytime</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr class=\"bg-yellow\">\n\t\t\t\t\t\t\t<td class=\"text-left\">Time On Demurrage</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>\n\t\t\t\t\t\t\t\t\t<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm\" value=\"64.3833 Hrs\" disabled></div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</tbody>\n\t\t\t\t</table>\n\t\t\t</div>\n\n\n\t\t\t<div class=\"text-bold\">Demurrage Value (USD) Summary</div>\n\t\t\t<div class=\"form-horizontal mt-30 mb-15\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Demurrage Rate (PDPR)</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"15,000.00\" disabled>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div class=\"text text-black\">USD</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Address Commission (USD)</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<div class=\"radio\">\n\t\t                            <input id=\"radiobox3\" name=\"test3\"  type=\"radio\">\n\t\t                            <label for=\"radiobox3\">No</label>\n\t\t                        </div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t                        <div class=\"radio\">\n\t\t                            <input id=\"radiobox3\" name=\"test3\"  type=\"radio\" checked>\n\t\t                            <label for=\"radiobox3\">Yes</label>\n\t\t                        </div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"2.5\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div class=\"text text-black\">%</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Broker Commission (USD)</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<div class=\"radio\">\n\t\t                            <input id=\"radiobox4\" name=\"test4\"  type=\"radio\">\n\t\t                            <label for=\"radiobox4\">No</label>\n\t\t                        </div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t                        <div class=\"radio\">\n\t\t                            <input id=\"radiobox4\" name=\"test4\"  type=\"radio\" checked>\n\t\t                            <label for=\"radiobox4\">Yes</label>\n\t\t                        </div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"1\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div class=\"text text-black\">%</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Demurrage Sharing to TOP (USD)</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<div class=\"radio\">\n\t\t                            <input id=\"radiobox5\" name=\"test5\"  type=\"radio\">\n\t\t                            <label for=\"radiobox5\">No</label>\n\t\t                        </div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t                        <div class=\"radio\">\n\t\t                            <input id=\"radiobox5\" name=\"test5\"  type=\"radio\" checked>\n\t\t                            <label for=\"radiobox5\">Yes</label>\n\t\t                        </div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"50\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div class=\"text text-black\">%</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Decimal</label>\n\t\t\t\t\t\t\t<div class=\"col-xs-6\">\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"2\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t\t<div class=\"text text-black\">Digit</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<table class=\"mb-30 pb-30 table table-bordered plutonyx-table-2 text-center\">\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<td class=\"text-bold\" width=\"30%\"></td>\n\t\t\t\t\t\t<td class=\"text-bold\" width=\"17.5%\">Original Claim</td>\n\t\t\t\t\t\t<td class=\"text-bold\" width=\"17.5%\">TOP Reviewed</td>\n\t\t\t\t\t\t<td class=\"text-bold\" width=\"17.5%\">Settled</td>\n\t\t\t\t\t\t<td class=\"text-bold\" width=\"17.5%\">Saving</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<td class=\"text-left\">Gross Demurrage (USD)</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<td class=\"text-left\">Address Commission (USD)</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr class=\"bg-blue\">\n\t\t\t\t\t\t<td class=\"text-left\">Sub Total Demurrage (USD)</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<td class=\"text-left\">Demurrage Sharing to TOP (USD)</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t</tr>\n\t\t\t\t\t<tr class=\"bg-yellow\">\n\t\t\t\t\t\t<td class=\"text-left\">Net Demurrage (USD)</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t\t<td>x,xxx.xx</td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\t\t</div>\n\n\n\t\t<h2 class=\"second-title\">\n\t\t\tExplanation\n\t\t</h2>\n\t\t<div class=\"form-horizontal mb-30\">\n\t\t\t<ptx-freetext\n\t            [elementId]=\"Explanation\"\n\t            [disabled]=\"false\"\n\t        ></ptx-freetext>\n\t\t</div>\n\n\t\t<h2 class=\"second-title\">\n\t\t\tProposal\n\t\t</h2>\n\t\t<div class=\"form-horizontal mb-30\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Demurrage</label>\n\t\t\t\t\t\t<div class=\"col-xs-4 pr-0\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"Paid\" disabled>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Time on Demurrage</label>\n\t\t\t\t\t\t<div class=\"col-xs-4 p-0\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center text-bold\" value=\"1 Days  1 Hrs  19 Mins\" disabled>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"25.3166\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-1\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">Hrs</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Net payment demurrage value</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"xx,xxx.xx\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">USD</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">Broker Commission</label>\n\t\t\t\t\t\t<div class=\"col-xs-4 p-0\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" value=\"xx,xxx.xx\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">USD</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-4 control-label\">For settlement in Thai Baht - Exchange rate</label>\n\t\t\t\t\t\t<div class=\"col-xs-4 p-0\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">THB/USD</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-xs-12 col-sm-6\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<label class=\"col-xs-5 control-label\">Settlement amount</label>\n\t\t\t\t\t\t<div class=\"col-xs-4\">\n\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control text-center\" disabled>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"col-xs-2\">\n\t\t\t\t\t\t\t<div class=\"text text-black\">THB</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<h2 class=\"second-title\">\n\t\t\tReference Attachment\n\t\t</h2>\n\t\t<div class=\"form-horizontal mb-30 pb-30\">\n\t\t\t<ptx-file-upload\n\t            [model]=\"model.MEMO_ATTACHMENT\"\n\t            [is_disabled]=\"is_disabled\"\n\t            [fk]=\"model.MEMO_DATA_ID\"\n\t            (handleModelChanged)=\"model.MEMO_ATTACHMENT = $event\"\n\t        ></ptx-file-upload>\n\t\t</div>\n\n\t\t<div class=\"wrapper-button\">\n            <button class=\"btn btn-default\">Save Draft</button>\n            <button class=\"btn btn-success\">Save & Submit</button>\n            <button class=\"btn btn-primary\">Export to Excel</button>\n        </div>\n\t</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/think/think.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThinkComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_components_shared_table_component__ = __webpack_require__("../../../../../src/app/components/_shared/table.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import * as bootbox from 'bootbox';
var ThinkComponent = (function () {
    function ThinkComponent(route, router, masterService, componentFactoryResolver) {
        this.route = route;
        this.router = router;
        this.masterService = masterService;
        this.componentFactoryResolver = componentFactoryResolver;
        this.portTableComponents = [];
        this.portTableComponentsClass = __WEBPACK_IMPORTED_MODULE_3_app_components_shared_table_component__["a" /* TableComponent */];
        this.model = {
            DOC_DATE_START: "",
            DOC_DATE_END: "",
            DOC_NO: "",
            SUBJECT: "",
            STATUS: "",
            CREATED_BY: "",
            PR_NO: "",
        };
    }
    ThinkComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            // this.req_transaction_id = params['id'];
            // this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    ThinkComponent.prototype.log = function () {
        console.log(this.model);
    };
    ThinkComponent.prototype.load = function () {
        var _self = this;
        this.masterService.getList().subscribe(function (res) {
            //console.log(res.BROKERS);
            //console.log(res.INCOTERMS);
            _self.addComponent(_self.portTableComponentsClass);
        }, function (error) {
            console.log(error);
        });
    };
    ThinkComponent.prototype.addComponent = function (componentClass) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentClass);
        var component = this.container.createComponent(componentFactory);
        // Push the component so that we can keep track of which components are created
        this.portTableComponents.push(component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChild */])('containerAppTable', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewContainerRef */] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewContainerRef */])
    ], ThinkComponent.prototype, "container", void 0);
    ThinkComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-think',
            template: __webpack_require__("../../../../../src/app/containers/think/think.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_2__services__["b" /* MasterService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* ComponentFactoryResolver */]])
    ], ThinkComponent);
    return ThinkComponent;
}());



/***/ }),

/***/ "../../../../../src/app/directives/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return directives; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__restrict_number_directive__ = __webpack_require__("../../../../../src/app/directives/restrict-number.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__show_date_diff_directive__ = __webpack_require__("../../../../../src/app/directives/show-date-diff.directive.ts");
/* unused harmony reexport RestrictNumberDirective */
/* unused harmony reexport ShowDateDiffDirective */



var directives = [
    __WEBPACK_IMPORTED_MODULE_0__restrict_number_directive__["a" /* RestrictNumberDirective */],
    __WEBPACK_IMPORTED_MODULE_1__show_date_diff_directive__["a" /* ShowDateDiffDirective */]
];


/***/ }),

/***/ "../../../../../src/app/directives/restrict-number.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestrictNumberDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes__ = __webpack_require__("../../../../../src/app/pipes/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RestrictNumberDirective = (function () {
    function RestrictNumberDirective(elementRef, currencyPipe) {
        this.elementRef = elementRef;
        this.currencyPipe = currencyPipe;
        this.lastest_value = 0;
        // @HostBinding('fraction')
        this.fraction = 2;
        this.ngModelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        // private padding = '000000';
        this.decimal = '.';
        this.thousands = ',';
        this.onLoadCheck = true;
        this.el = this.elementRef.nativeElement;
    }
    RestrictNumberDirective.prototype.isNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        var key = event.key;
        if (key && (key == 'Subtract' || key == 'Del')) {
            return true;
        }
        var regex = /[0-9,\.-]+$/g;
        if (this.fraction == 0) {
            regex = /[0-9,-]+$/g;
        }
        // var regex = /[0-9\.]+$/g
        return regex.test(key);
        // if (key == '.') {
        //   return true;
        // }
        // // 61 // -
        // // 45 // -
        // // 43 // +
        // if (charCode == 61 || charCode == 45 || charCode == 43){
        //   return true;
        // }
        // if (charCode > 31 && (charCode < 48 || charCode > 57)){
        //   return false;
        // }
        // return true;
    };
    RestrictNumberDirective.prototype.ngOnInit = function () {
        this.el.value = this.currencyPipe.transform(this.ngModel, this.fraction);
    };
    RestrictNumberDirective.prototype.ngDoCheck = function (e) {
        if (this.onLoadCheck) {
            // console.log(this.el.value)
            if (this.el.value) {
                // if(this.fraction == 0) {
                //   this.el.value = this.el.value || "0";
                //   console.log('b _.round', this.el.value);
                //   this.el.value = _.round(+this.el.value, 0);
                //   console.log('a _.round', this.el.value);
                // }
                this.el.value = this.currencyPipe.transform(this.el.value, this.fraction);
            }
        }
        // this.el.value = this.currencyPipe.transform(this.el.value);
    };
    RestrictNumberDirective.prototype.ngOnChanges = function (e) {
        // this.el.value = this.currencyPipe.transform(this.el.value);
    };
    // @HostListener("keypress", ["$event.target.value"])
    // onChange(value) {
    //   console.log('keypress')
    //   var plain_number = value.replace(/[^\d|\-+|\.+]/g, '');
    //   this.el.value = this.parse(plain_number); // opossite of transform
    // }
    RestrictNumberDirective.prototype.onkeypress = function (e) {
        var event = e || window.event;
        if (event) {
            return this.isNumberKey(event);
        }
    };
    RestrictNumberDirective.prototype.onFocus = function (value) {
        this.onLoadCheck = false;
        this.el.value = this.currencyPipe.parse(value, this.fraction); // opossite of transform
        this.lastest_value = +value;
        // var plain_number = value.replace(/[^\d|\-+|\.+]/g, '');
        // this.el.value = this.parse(value); // opossite of transform
        // this.ngModelChange.emit(plain_number);
    };
    RestrictNumberDirective.prototype.onBlur = function (value) {
        // console.log('onBlur', value)
        // var plain_number = this.el.value.replace(/[^\d|\-+|\.+]/g, '');
        // this.el.value = plain_number;
        // console.log(+value, +this.max, this.lastest_value)
        if (this.max && +value > +this.max) {
            this.el.value = this.currencyPipe.transform('0', this.fraction);
            if (this.padding) {
                this.ngModelChange.emit(this.pad(this.el.value, this.padding));
            }
            else {
                this.ngModelChange.emit(this.el.value);
            }
            // this.ngModel = 0;
        }
        else {
            this.el.value = this.currencyPipe.transform(value, this.fraction);
        }
        if (this.padding) {
            this.el.value = this.pad(this.el.value, this.padding);
        }
        // this.el.value = this.currencyPipe.transform(value, this.fraction);
        // var plain_number = value.replace(/[^\d|\-+|\.+]/g, '');
        // this.el.value = this.transform(value);
        // this.ngModelChange.emit(plain_number);
    };
    RestrictNumberDirective.prototype.pad = function (n, width) {
        var z = '0';
        n = n + '';
        n = n * 1;
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], RestrictNumberDirective.prototype, "ngModel", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], RestrictNumberDirective.prototype, "options", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('min'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], RestrictNumberDirective.prototype, "min", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('max'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], RestrictNumberDirective.prototype, "max", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], RestrictNumberDirective.prototype, "padding", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], RestrictNumberDirective.prototype, "fraction", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", Object)
    ], RestrictNumberDirective.prototype, "ngModelChange", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('keypress'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], RestrictNumberDirective.prototype, "onkeypress", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])("focus", ["$event.target.value"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], RestrictNumberDirective.prototype, "onFocus", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])("blur", ["$event.target.value"]),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], RestrictNumberDirective.prototype, "onBlur", null);
    RestrictNumberDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[ptxRestrictNumber]',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_1__pipes__["a" /* CurrencyPipe */]])
    ], RestrictNumberDirective);
    return RestrictNumberDirective;
}());



/***/ }),

/***/ "../../../../../src/app/directives/show-date-diff.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowDateDiffDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowDateDiffDirective = (function () {
    function ShowDateDiffDirective(el) {
        this.el = el;
    }
    ShowDateDiffDirective.prototype.ngOnInit = function () {
        this.calculateDateDiff();
    };
    ShowDateDiffDirective.prototype.ngOnChanges = function (changes) {
        //console.log('Changes', changes);
        this.startDate = changes.startDate ? new Date(changes.startDate.currentValue) : this.startDate;
        this.endDate = changes.endDate ? new Date(changes.endDate.currentValue) : this.endDate;
        this.startTimeHours = changes.startTimeHours ? (changes.startTimeHours.currentValue) : this.startTimeHours;
        this.startTimeMins = changes.startTimeMins ? (changes.startTimeMins.currentValue) : this.startTimeMins;
        this.endTimeHours = changes.endTimeHours ? (changes.endTimeHours.currentValue) : this.endTimeHours;
        this.endTimeMins = changes.endTimeMins ? (changes.endTimeMins.currentValue) : this.endTimeMins;
        this.netHours = changes.netHours ? (changes.netHours.currentValue) : this.netHours;
        this.calculateDateDiff();
    };
    ShowDateDiffDirective.prototype.calculateDateDiff = function () {
        // console.log('fired calculateDateDiff');
        // console.log('StartDate:', this.startDate);
        // console.log('EndDate:', this.endDate);
        var startTime = this.startTimeHours + ":" + this.startTimeMins;
        var endTime = this.endTimeHours + ":" + this.endTimeMins;
        var dateStart = this.startDate.toLocaleDateString() + startTime;
        var dateEnd = this.endDate.toLocaleDateString() + endTime;
        var endM = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(dateEnd, "MM-DD-YYYY HH:mm");
        var startM = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(dateStart, "MM-DD-YYYY HH:mm");
        var duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"](endM.diff(startM));
        var hours = endM.diff(startM, 'hours', true).toString();
        var message = "";
        if (this.netHours) {
            duration = __WEBPACK_IMPORTED_MODULE_1_moment_moment__["duration"](parseInt(this.netHours), 'hours');
            hours = this.netHours;
        }
        message = duration.years() ? duration.years() + " Years " : "";
        message += duration.months() ? +duration.months() + " Months " : "";
        message += duration.days() ? +duration.days() + " Days " : "";
        message += duration.hours() ? +duration.hours() + " Hrs " : "";
        message += duration.minutes() ? +duration.minutes() + " Mins " : "";
        if (this.onlyHours) {
            this.el.nativeElement.value = hours ? hours + " Hrs" : "0 Hrs";
        }
        else {
            this.el.nativeElement.value = message ? message : "0 Days";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowDateDiffDirective.prototype, "result", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ShowDateDiffDirective.prototype, "startDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowDateDiffDirective.prototype, "startTimeHours", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowDateDiffDirective.prototype, "startTimeMins", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ShowDateDiffDirective.prototype, "endDate", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowDateDiffDirective.prototype, "endTimeHours", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowDateDiffDirective.prototype, "endTimeMins", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowDateDiffDirective.prototype, "netHours", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], ShowDateDiffDirective.prototype, "onlyHours", void 0);
    ShowDateDiffDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: '[ptxShowDateDiff]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], ShowDateDiffDirective);
    return ShowDateDiffDirective;
}());



/***/ }),

/***/ "../../../../../src/app/guards/pending-changes.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PendingChangesGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PendingChangesGuard = (function () {
    function PendingChangesGuard() {
    }
    PendingChangesGuard.prototype.canDeactivate = function (component) {
        // if there are no pending changes, just allow deactivation; else confirm first
        return component.canDeactivate() ?
            true :
            // NOTE: this warning message will only be shown when navigating elsewhere within your angular app;
            // when navigating away from your angular app, the browser will show a generic warning message
            // see http://stackoverflow.com/a/42207299/7307355
            confirm('WARNING: You have unsaved changes. Press Cancel to go back and save these changes, or OK to lose these changes.');
    };
    PendingChangesGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], PendingChangesGuard);
    return PendingChangesGuard;
}());



/***/ }),

/***/ "../../../../../src/app/models/daf-data.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_DATA; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment__);
/**
 * Created by chusri on 23/11/2017 AD.
 */

var DAF_DATA = (function () {
    function DAF_DATA() {
        this.IS_DISABLED = false;
        this.DDA_FOR_COMPANY = '';
        this.DDA_TYPE_OF_TRANSECTION = '';
        this.DDA_DEMURAGE_TYPE = '';
        this.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
        this.SHOW_COUTER_PARTY = '';
        this.DAF_ATTACH_FILE = new Array();
        this.DAF_MATERIAL = new Array();
        this.DAF_PORT = new Array();
        this.DDA_DOC_DATE = __WEBPACK_IMPORTED_MODULE_0_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.DDA_CURRENCY_UNIT = 'USD';
        this.DDA_IS_ADDRESS_COMMISSION = 'N';
        this.DDA_IS_BROKER_COMMISSION = 'N';
        this.DDA_IS_SHARING_TO_TOP = 'N';
        this.DDA_DECIMAL = 2;
        this.DDA_MATERIAL_TYPE = 'Crude & F/S';
        this.DDA_INCOTERM = '';
        this.DDA_DEMURAGE_TYPE = 'Paid';
        this.DDA_SETTLEMENT_AMOUNT_THB = null;
        this.DDA_IS_OVERRIDE_NET_PAYMENT = 'N';
        // this.DAF_PORT.push(new DAF_PORT('load'));
        // this.DAF_PORT.push(new DAF_PORT('discharge'));
        // this = Helper.Deserialize(a);
    }
    return DAF_DATA;
}());



/***/ }),

/***/ "../../../../../src/app/models/daf-deduction-activity.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_DEDUCTION_ACTIVITY; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var DAF_DEDUCTION_ACTIVITY = (function () {
    function DAF_DEDUCTION_ACTIVITY(type) {
        this.DDD_TYPE = type;
    }
    return DAF_DEDUCTION_ACTIVITY;
}());



/***/ }),

/***/ "../../../../../src/app/models/daf-master.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAFMaster; });
var DAFMaster = (function () {
    function DAFMaster() {
    }
    return DAFMaster;
}());



/***/ }),

/***/ "../../../../../src/app/models/daf-material.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_MATERIAL; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var DAF_MATERIAL = (function () {
    function DAF_MATERIAL() {
    }
    return DAF_MATERIAL;
}());



/***/ }),

/***/ "../../../../../src/app/models/daf-port.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_PORT; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var DAF_PORT = (function () {
    function DAF_PORT(type) {
        this.DPT_RT_ORIGIN_CLAIM_T_TEXT = '';
        this.DAF_DEDUCTION_ACTIVITY = new Array();
        this.DPT_TYPE = type;
        this.DPT_RT_T_START_ACTIVITY = 'NOR+6';
        this.DPT_RT_T_STOP_ACTIVITY = 'HOSE DISCONNECTED';
        // this.DPT_RT_ORIGIN_CLAIM_T_START = moment();
        // this.DPT_RT_ORIGIN_CLAIM_T_STOP = moment();
    }
    return DAF_PORT;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/attachment-filter.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttachmentFilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var AttachmentFilterPipe = (function () {
    function AttachmentFilterPipe() {
    }
    AttachmentFilterPipe.prototype.transform = function (value, type_string) {
        var result_list = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](value, function (o) { return true; /*return o.DAT_TYPE == type_string;*/ });
        // let result_list = [];
        // switch (type_string) {
        //   case "IS_OTHER":
        //     result_list = _.filter(value, function(o) { return o.IS_OTHER == "Y"; });
        //     break;
        //   // case "main":
        //   //   result_list = _.filter(value, function(o) { return o.IS_OTHER == "N"; });
        //   //   break;
        //   default:
        //     result_list = value;
        //     break;
        // }
        return result_list;
    };
    AttachmentFilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'attachmentFilter',
            pure: false,
        })
    ], AttachmentFilterPipe);
    return AttachmentFilterPipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/create-list.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateListPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CreateListPipe = (function () {
    function CreateListPipe() {
    }
    CreateListPipe.prototype.transform = function (value, args) {
        return null;
    };
    CreateListPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'createList'
        })
    ], CreateListPipe);
    return CreateListPipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/currency.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CurrencyPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PADDING = "000000";
var CurrencyPipe = (function () {
    function CurrencyPipe() {
        // TODO comes from configuration settings
        this.PREFIX = '';
        this.DECIMAL_SEPARATOR = ".";
        this.THOUSANDS_SEPARATOR = ",";
        this.SUFFIX = '';
    }
    CurrencyPipe.prototype.transform = function (value, fractionSize) {
        if (fractionSize === void 0) { fractionSize = 2; }
        var _a = (value || "").toString()
            .split("."), integer = _a[0], _b = _a[1], fraction = _b === void 0 ? "" : _b;
        fraction = fractionSize > 0
            ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
            : "";
        integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.THOUSANDS_SEPARATOR);
        if (integer == "") {
            return "";
        }
        return this.PREFIX + integer + fraction + this.SUFFIX;
    };
    CurrencyPipe.prototype.parse = function (value, fractionSize) {
        if (fractionSize === void 0) { fractionSize = 2; }
        var _a = (value || "").replace(this.PREFIX, "")
            .replace(this.SUFFIX, "")
            .split(this.DECIMAL_SEPARATOR), integer = _a[0], _b = _a[1], fraction = _b === void 0 ? "" : _b;
        integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");
        fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
            ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
            : "";
        if (integer == "") {
            return "";
        }
        return integer + fraction;
    };
    CurrencyPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'currency'
        }),
        __metadata("design:paramtypes", [])
    ], CurrencyPipe);
    return CurrencyPipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/datex.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatexPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DatexPipe = (function () {
    function DatexPipe() {
    }
    DatexPipe.prototype.transform = function (value, format) {
        if (format === void 0) { format = ""; }
        // Try and parse the passed value.
        var momentDate = __WEBPACK_IMPORTED_MODULE_1_moment__(value);
        // If momentร didn't understand the value, return it unformatted.
        if (!momentDate.isValid())
            return value;
        // Otherwise, return the date formatted as requested.
        return momentDate.format(format);
    };
    DatexPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'datex'
        })
    ], DatexPipe);
    return DatexPipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return pipes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__currency_pipe__ = __webpack_require__("../../../../../src/app/pipes/currency.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__create_list_pipe__ = __webpack_require__("../../../../../src/app/pipes/create-list.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__datex_pipe__ = __webpack_require__("../../../../../src/app/pipes/datex.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__attachment_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/attachment-filter.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ports_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/ports-filter.pipe.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__currency_pipe__["a"]; });
/* unused harmony reexport CreateListPipe */
/* unused harmony reexport DatexPipe */
/* unused harmony reexport AttachmentFilterPipe */
/* unused harmony reexport PortsFilterPipe */





var pipes = [
    __WEBPACK_IMPORTED_MODULE_0__currency_pipe__["a" /* CurrencyPipe */],
    __WEBPACK_IMPORTED_MODULE_1__create_list_pipe__["a" /* CreateListPipe */],
    __WEBPACK_IMPORTED_MODULE_2__datex_pipe__["a" /* DatexPipe */],
    // DomReviewListPipe,
    __WEBPACK_IMPORTED_MODULE_3__attachment_filter_pipe__["a" /* AttachmentFilterPipe */],
    __WEBPACK_IMPORTED_MODULE_4__ports_filter_pipe__["a" /* PortsFilterPipe */],
];



/***/ }),

/***/ "../../../../../src/app/pipes/moment.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MomentPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MomentPipe = (function () {
    function MomentPipe() {
    }
    MomentPipe.prototype.transform = function (value, args) {
        return null;
    };
    MomentPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'moment'
        })
    ], MomentPipe);
    return MomentPipe;
}());



/***/ }),

/***/ "../../../../../src/app/pipes/ports-filter.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PortsFilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PortsFilterPipe = (function () {
    function PortsFilterPipe() {
    }
    PortsFilterPipe.prototype.transform = function (value, type) {
        var result_list = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](value, function (o) {
            return o.DPT_TYPE === type;
        });
        return result_list;
    };
    PortsFilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'portsFilter',
            pure: false,
        })
    ], PortsFilterPipe);
    return PortsFilterPipe;
}());



/***/ }),

/***/ "../../../../../src/app/services/cho-reference.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChoReferenceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChoReferenceService = (function () {
    function ChoReferenceService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "DAFMaster";
    }
    ChoReferenceService.prototype.search = function (data) {
        return this.http
            .post(this.resource + "/GetCHOData", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    ChoReferenceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]])
    ], ChoReferenceService);
    return ChoReferenceService;
}());



/***/ }),

/***/ "../../../../../src/app/services/cip-reference.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CipReferenceService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CipReferenceService = (function () {
    function CipReferenceService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "DAFMaster";
    }
    CipReferenceService.prototype.search = function (data) {
        return this.http
            .post(this.resource + "/GetCIPData", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    CipReferenceService.prototype.getCIPCrudeData = function (data) {
        return this.http
            .post(this.resource + "/GetCIPCrudeData", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    CipReferenceService.prototype.getCIPProductData = function (data) {
        return this.http
            .post(this.resource + "/getCIPProductData", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    CipReferenceService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]])
    ], CipReferenceService);
    return CipReferenceService;
}());



/***/ }),

/***/ "../../../../../src/app/services/daf.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DafService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_throw__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/throw.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DafService = (function () {
    function DafService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "DafService";
    }
    DafService.prototype.getList = function () {
        return this.http
            .get(this.resource + "/GetListTransaction")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.get = function (id) {
        return this.http
            .get(this.resource + "/" + id)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.update = function (id, data) {
        return this.http
            .put(this.resource + "/" + id, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.post = function (data) {
        return this.http
            .post(this.resource + "/DomesticSaleCreate", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.put = function (data) {
        return this.http
            .put("" + this.resource, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.SearchTransaction = function (data) {
        return this.http
            .post(this.resource + "/SearchTransaction", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.GetSearchCriteria = function () {
        return this.http
            .get(this.resource + "/GetSearchCriteria")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.GetByTransactionId = function (TRAN_ID) {
        return this.http
            .post(this.resource + "/GetByTransactionId", {
            TRAN_ID: TRAN_ID,
        })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.GetButtons = function () {
        return this.http
            .get(this.resource + "/GetButtons")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.SaveDraft = function (data) {
        return this.http
            .post(this.resource + "/SaveDraft", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.Submit = function (data) {
        return this.http
            .post(this.resource + "/Submit", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.Verify = function (data) {
        return this.http
            .post(this.resource + "/Verify", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.Endorse = function (data) {
        return this.http
            .post(this.resource + "/Endorse", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.Approve = function (data) {
        return this.http
            .post(this.resource + "/Approve", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.GeneratePDF = function (data) {
        return this.http
            .get(this.resource + "/GeneratePDF/" + data.TRAN_ID)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.GenExcel = function (data) {
        return this.http
            .get(this.resource + "/GenExcel/" + data.TRAN_ID)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.Reject = function (data) {
        return this.http
            .post(this.resource + "/Reject", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.Cancel = function (data) {
        return this.http
            .post(this.resource + "/Cancel", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService.prototype.playgroundGet = function () {
        return this.http
            .get('../../Content/daf/assets/data/inter.json')
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    DafService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]])
    ], DafService);
    return DafService;
}());



/***/ }),

/***/ "../../../../../src/app/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return services; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daf_service__ = __webpack_require__("../../../../../src/app/services/daf.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loader_service__ = __webpack_require__("../../../../../src/app/services/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__upload_service__ = __webpack_require__("../../../../../src/app/services/upload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cip_reference_service__ = __webpack_require__("../../../../../src/app/services/cip-reference.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cho_reference_service__ = __webpack_require__("../../../../../src/app/services/cho-reference.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__master_service__["a"]; });
/* unused harmony reexport DafService */
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__loader_service__["a"]; });
/* unused harmony reexport UploadService */
/* unused harmony reexport CipReferenceService */
/* unused harmony reexport ChoReferenceService */






var services = [
    __WEBPACK_IMPORTED_MODULE_0__master_service__["a" /* MasterService */],
    __WEBPACK_IMPORTED_MODULE_1__daf_service__["a" /* DafService */],
    __WEBPACK_IMPORTED_MODULE_2__loader_service__["a" /* LoaderService */],
    __WEBPACK_IMPORTED_MODULE_3__upload_service__["a" /* UploadService */],
    __WEBPACK_IMPORTED_MODULE_4__cip_reference_service__["a" /* CipReferenceService */],
    __WEBPACK_IMPORTED_MODULE_5__cho_reference_service__["a" /* ChoReferenceService */],
];



/***/ }),

/***/ "../../../../../src/app/services/loader.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoaderService = (function () {
    function LoaderService() {
        this.loaderStatus = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["a" /* BehaviorSubject */](false);
    }
    LoaderService.prototype.displayLoader = function (value) {
        this.loaderStatus.next(value);
    };
    LoaderService.prototype.display = function (value) {
        this.loaderStatus.next(value);
    };
    LoaderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], LoaderService);
    return LoaderService;
}());



/***/ }),

/***/ "../../../../../src/app/services/master.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MasterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MasterService = (function () {
    function MasterService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "DAFMaster";
    }
    MasterService.prototype.getList = function () {
        return this.http
            .get("" + this.resource)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    MasterService.prototype.getFX = function (data) {
        return this.http
            .post(this.resource + "/Post_FX", {
            DATE_FROM: data.PBD_FX_DATE_FROM,
            DATE_TO: data.PBD_FX_DATE_TO,
        })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    MasterService.prototype.getHSFO = function (data) {
        return this.http
            .post(this.resource + "/Post_HSFO", {
            DATE_FROM: data.PBD_HSFO_DATE_FROM,
            DATE_TO: data.PBD_HSFO_DATE_TO,
        })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    MasterService.prototype.get = function (id) {
        return this.http
            .get(this.resource + "/" + id)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('suServer error'); });
    };
    MasterService.prototype.update = function (id, data) {
        return this.http
            .put(this.resource + "/" + id, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    MasterService.prototype.post = function (data) {
        return this.http
            .post("" + this.resource, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    MasterService.prototype.put = function (data) {
        return this.http
            .put("" + this.resource, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    MasterService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]])
    ], MasterService);
    return MasterService;
}());



/***/ }),

/***/ "../../../../../src/app/services/upload.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UploadService = (function () {
    function UploadService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common__["a" /* BASE_API */] + "DafService";
    }
    UploadService.prototype.postSingle = function (file, type_string) {
        var formData = new FormData();
        formData.append('file', file, file.name);
        // let headers = new Headers()
        //headers.append('Content-Type', 'json');
        //headers.append('Accept', 'application/json');
        // let options = new RequestOptions({ headers: headers });
        return this.http
            .post(this.resource + "/FileUpload?Type=DAF|F10000080&PAT_TYPE=" + type_string, formData)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error); });
    };
    // public postSingle(file: File) : Observable<any> {
    //   let formData: FormData = new FormData();
    //   formData.append('uploadFile', file, file.name);
    //   // let headers = new Headers()
    //   //headers.append('Content-Type', 'json');
    //   //headers.append('Accept', 'application/json');
    //   // let options = new RequestOptions({ headers: headers });
    //   return this.http
    //     .post(`${BASE_API}api/Upload/anywhere`, formData)
    //     .map(res => res.json())
    //     .catch(error => Observable.throw(error));
    // }
    UploadService.prototype.post = function (event) {
        var fileList = event.target.files;
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData = new FormData();
            formData.append('uploadFile', file, file.name);
            // let headers = new Headers()
            //headers.append('Content-Type', 'json');
            //headers.append('Accept', 'application/json');
            // let options = new RequestOptions({ headers: headers });
            return this.http
                .post(__WEBPACK_IMPORTED_MODULE_1__common__["a" /* BASE_API */] + "api/Upload", formData)
                .map(function (res) { return res.json(); })
                .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw(error); });
        }
    };
    UploadService.prototype.getList = function () {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_1__common__["a" /* BASE_API */] + "api/blob")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    UploadService.prototype.get = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_1__common__["a" /* BASE_API */] + "api/blob/" + id)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["a" /* Observable */].throw('Server error'); });
    };
    UploadService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]])
    ], UploadService);
    return UploadService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_16" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map