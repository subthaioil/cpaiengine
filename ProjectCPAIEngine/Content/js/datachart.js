var ganttData = [
	{ 
		id: 1, name: "Phubai Pattra 1", 
		series: [
			{ name: "Phubai Pattra 1 (8,706.56 m3)", 
			    activities: [ 
					 { title: 'Domestics PX 5 KT (by SMPC)<br/><span class="textday">01 Jul - 17 Jul</span><div class="c-mark">C</div><div class="m-mark">M</div><div class="o-mark">O</div><div class="b-mark">B</div><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color:#47668a;"><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu" style="background-color:#47668a;"><li><a href="#"><img src="images/icon-pencil.png" /></a></li><li><a href="#"><img src="images/icon_view_list.png" /></a></li></ul></div>',
					   start: new Date(2016,06,01), 
					   end: new Date(2016,06,17),
					   color: "#6592c6" }
				]},
		]
	}, 
	{
		id: 2, name: "Phubai Pattra 2",
		series: [
			{ name: "Phubai Pattra 2 (8,720.43 m3)", 
			    activities: [ 
					 { title: 'Import MTBE from 1 or 2 ports SG to TOP Sriracha ... (ETA Sriracha)<br/><span class="textday">08 Jul - 23 Jul</span><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color:#69547b;"><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu" style="background-color:#69547b;"><li><a href="#"><img src="images/icon-flag.png" /></a></li><li><a href="#"><img src="images/icon-pencil.png" /></a></li><li><a href="#"><img src="images/icon-remove.png" /></a></li></ul></div>',
					   start: new Date(2016,06,08), 
					   end: new Date(2016,06,23),
					   color: "#9679b0" }
				]},
		]
	},
	{
		id: 3, name: "Phubai Pattra 4",
		series: [
			{ name: "Phubai Pattra 4 (2,593.321 m3)", 
			    activities: [ 
					 { title: 'Domestics PX 5 KT (by SMPC)<br/><span class="textday">03 Jul - 18 Jul</span><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color:#8f4041;"><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu" style="background-color:#8f4041;"><li><a href="#"><img src="images/icon-flag.png" /></a></li><li><a href="#"><img src="images/icon-pencil.png" /></a></li><li><a href="#"><img src="images/icon-remove.png" /></a></li></ul></div>',
					   start: new Date(2016,06,03), 
					   end: new Date(2016,06,18),
					   color: "#cd5b5d" }
				]},
		]
	},
	{
		id: 4, name: "Phubai Pattra 5",
		series: [
			{ name: "Phubai Pattra 5 (3,200 m3)", 
			    activities: [ 
					 { title: 'Domestics PX 5 KT (by SMPC)<br/><span class="textday">11 Jul - 26 Jul</span><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color:#8a6d4c;"><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu" style="background-color:#8a6d4c;"><li><a href="#"><img src="images/icon-flag.png" /></a></li><li><a href="#"><img src="images/icon-pencil.png" /></a></li><li><a href="#"><img src="images/icon-remove.png" /></a></li></ul></div>',
					   start: new Date(2016,06,11), 
					   end: new Date(2016,06,26),
					   color: "#c69c6d" }
				]},
		]
	},
	{
		id: 5, name: "Phubai Amara 1",
		series: [
			{ name: "Phubai Amara 1 (37,785 m3)", 
			    activities: [ 
					 { title: 'Import MTBE from 1 or 2 ports SG (ETA Sriracha)<br/><span class="textday">04 Jul - 15 Jul</span><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color:#428072;"><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu" style="background-color:#428072;"><li><a href="#"><img src="images/icon-flag.png" /></a></li><li><a href="#"><img src="images/icon-pencil.png" /></a></li><li><a href="#"><img src="images/icon-remove.png" /></a></li></ul></div>',
					   start: new Date(2016,06,04), 
					   end: new Date(2016,06,15),
					   color: "#5fb8a3" },
					 
					 { title: 'Domestics PX 5 KT (by SMPC)<br/><span class="textday">16 Jul - 23 Jul</span><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color:#47668a;"><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu" style="background-color:#47668a;"><li><a href="#"><img src="images/icon-flag.png" /></a></li><li><a href="#"><img src="images/icon-pencil.png" /></a></li><li><a href="#"><img src="images/icon-remove.png" /></a></li></ul></div>',
					   start: new Date(2016,06,16), 
					   end: new Date(2016,06,23),
					   color: "#6592c6" }
				]},
		]
	},
	];