var randomScalingFactor = function(){ return Math.round(Math.random()*1000)};
	
var lineChartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July", "a", "b", "c", "d", "e", "f", "f1", "f2", "f3", "f4", "f5", "6", "7", "8", "9", "0", "f11"],
    datasets: [
        {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [randomScalingFactor(), null, randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        },
        {
            label: "My Second dataset",
            fillColor: "rgba(48, 164, 255, 0.2)",
            strokeColor: "rgba(48, 164, 255, 1)",
            pointColor: "rgba(48, 164, 255, 1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(48, 164, 255, 1)",
            data: [randomScalingFactor(), null, randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()]
        }
        ,
        {
            label: "My Third dataset",
            fillColor: "rgba(255, 66, 66, 0.2)",
            strokeColor: "rgba(255, 66, 66, 1)",
            pointColor: "rgba(255, 66, 66, 1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(48, 164, 255, 1)",
            data: [null, 500]
        }
        ,
        {
            label: "My Third dataset",
            fillColor: "rgba(255, 66, 66, 0.2)",
            strokeColor: "rgba(255, 66, 66, 1)",
            pointColor: "rgba(255, 66, 66, 1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(48, 164, 255, 1)",
            data: [null, null, null, null, null, null, 300]
        },
    {
        label: "My Third dataset",
        fillColor: "rgba(255, 66, 66, 0.2)",
        strokeColor: "rgba(255, 66, 66, 1)",
        pointColor: "rgba(255, 66, 66, 1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(48, 164, 255, 1)",
        data: [null, null, null, null, null, null, null, null, null, 10]
    }
    ]

}


	
	