﻿function getRootWebSitePath() {
    var _location = document.location.toString();
    var applicationNameIndex = _location.indexOf('/', _location.indexOf('://') + 3);
    var applicationName = _location.substring(0, applicationNameIndex) + '/';
    var webFolderIndex = _location.indexOf('/', _location.indexOf(applicationName) + applicationName.length);
    var webFolderFullPath = _location.substring(0, webFolderIndex).toString().replace('/CPAIMVC', '');
    return webFolderFullPath;
}

var type_ajax = '';
var pgname_ajax = '';

function addAttachFile(type, pgname) {
    type_ajax = type;
    pgname_ajax = pgname;
    $("#upload_MultiFile").click();
}

function addExplanAttachFile(type, pgname) {
    type_ajax = type;
    pgname_ajax = pgname;
    $("#Explanupload_MultiFile").click();
}

function addExplanAttachFileTrader(type, pgname) {
    type_ajax = type;
    pgname_ajax = pgname;
    $("#Explanupload_MultiFile_trader").click();
}

$('#Explanupload_MultiFile_trader').change(function (e) {
    var files = e.target.files;
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            for (var x = 0; x < files.length; x++) {
                var data = new FormData();
                data.append("file" + x, files[x]);
                $.ajax({
                    type: "POST",
                    url: getRootWebSitePath() + "/Web/Api/UploadFileAPI.aspx?Type=" + type_ajax + "|" + pgname_ajax,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        var index = response.indexOf('<!DOCTYPE html>');
                        var json = response.substring(0, index);
                        var data = JSON.parse(json.trim());
                        $('#ExplanfilesUploaded_trader').append(MakeUploadTagExplanTrader(data.file_path, ''));
                        DeleteFileEventSetExplanTrader();
                    },
                    error: function (event, queueID, fileObj, errorObj) {
                        alert(errorObj.type + ' Error: ' + errorObj.info);
                        if (fileObj.size > 4194304) { alert('File size too large.'); }
                        else { alert('Someting wrong.'); }
                    }
                });
                data = null;
            }
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
    $(this).val('');
});

$('#upload_MultiFile').change(function (e) {//
    var files = e.target.files;
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            for (var x = 0; x < files.length; x++) {
                var data = new FormData();
                data.append("file" + x, files[x]);
                $.ajax({
                    type: "POST",
                    url: getRootWebSitePath() + "/Web/Api/UploadFileAPI.aspx?Type=" + type_ajax + "|" + pgname_ajax,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        var index = response.indexOf('<!DOCTYPE html>');
                        var json = response.substring(0, index);
                        var data = JSON.parse(json.trim());
                        $('#filesUploaded').append(MakeUploadTag(data.file_path));//
                        DeleteFileEventSet();//
                    },
                    error: function (event, queueID, fileObj, errorObj) {
                        alert(errorObj.type + ' Error: ' + errorObj.info);
                        if (fileObj.size > 4194304) { alert('File size too large.'); }
                        else { alert('Someting wrong.'); }
                    }
                });
                data = null;
            }
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
    $(this).val('');
});

$('#Explanupload_MultiFile').change(function (e) {
    var files = e.target.files;
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            for (var x = 0; x < files.length; x++) {
                var data = new FormData();
                data.append("file" + x, files[x]);
                $.ajax({
                    type: "POST",
                    url: getRootWebSitePath() + "/Web/Api/UploadFileAPI.aspx?Type=" + type_ajax + "|" + pgname_ajax,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (response) {
                        var index = response.indexOf('<!DOCTYPE html>');
                        var json = response.substring(0, index);
                        var data = JSON.parse(json.trim());
                        $('#ExplanfilesUploaded').append(MakeUploadTagExplan(data.file_path, ''));
                        DeleteFileEventSetExplan();
                    },
                    error: function (event, queueID, fileObj, errorObj) {
                        alert(errorObj.type + ' Error: ' + errorObj.info);
                        if (fileObj.size > 4194304) { alert('File size too large.'); }
                        else { alert('Someting wrong.'); }
                    }
                });
                data = null;
            }
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
    $(this).val('');
});


function SaveFileUpload() {
    $('#hdfFileUpload').val('');
    var FileVal = "";
    $('.FileAttach').each(function () {
        FileVal += $(this).attr('value') + "|";
    });
    $('#hdfFileUpload').val(FileVal);
    
    $('#hdfExplanFileUploadTrader').val('');
    $('#hdfExplanFileUpload').val('');
    var FileValExplanTrader = "";
    var FileValExplan = "";
    $('.ExplanAttach').each(function () {
        var txtDesc = $(this).closest('tr').find('.txtUpload').val();
        FileValExplan += $(this).attr('value') + ":" + txtDesc + "|";
    });
    $('.ExplanAttachTrader').each(function () {
        var txtDesc = $(this).closest('tr').find('.txtUpload').val();
        FileValExplanTrader += $(this).attr('value') + ":" + txtDesc + "|";
    });
    $('#hdfExplanFileUpload').val(FileValExplan);
    $('#hdfExplanFileUploadTrader').val(FileValExplanTrader);
}

function LoadFileUpload() {
    $('.removeAttach').each(function () {
        $(this).parent().parent().parent().remove();
    });
    $('.removeExplanAttach').each(function () {
        $(this).parent().parent().parent().parent().remove();
    });
    $('.removeExplanAttachTrader').each(function () {
        $(this).parent().parent().parent().parent().remove();
    });

    if ($('#hdfFileUpload').val()) {
        var ItemFile = $('#hdfFileUpload').val().split("|");
        for (var i = 0; i < ItemFile.length; i++) {
            if (ItemFile[i] != "") {
                $('#filesUploaded').append(MakeUploadTag(ItemFile[i]));
            }
        }
        DeleteFileEventSet();
    }
    if ($('#hdfExplanFileUpload').val()) {
        var ItemFile = $('#hdfExplanFileUpload').val().split("|");
        for (var i = 0; i < ItemFile.length; i++) {
            if (ItemFile[i] != "") {
                var splitVal = ItemFile[i].split(":");
                $('#ExplanfilesUploaded').append(MakeUploadTagExplan(splitVal[0], splitVal[1]));
            }
        }
        DeleteFileEventSetExplan();
    }
    if ($('#hdfExplanFileUploadTrader').val()) {
        var ItemFile = $('#hdfExplanFileUploadTrader').val().split("|");
        for (var i = 0; i < ItemFile.length; i++) {
            if (ItemFile[i] != "") {
                var splitVal = ItemFile[i].split(":");
                $('#ExplanfilesUploaded_trader').append(MakeUploadTagExplan(splitVal[0], splitVal[1]));
            }
        }
        DeleteFileEventSetExplanTrader();
    }
}

function DeleteFileEventSetExplanTrader() {
    $('.glyphicon-remove').click(function () {
        $(this).parent().parent().parent().parent().remove();
    });
}

function DeleteFileEventSetExplan() {
    $('.glyphicon-remove').click(function () {
        $(this).parent().parent().parent().parent().remove();
    });
}

function DeleteFileEventSet() {
    $('.glyphicon-remove').click(function () {
        $(this).parent().parent().parent().remove();
    });
}