#!/bin/bash
yarn build
cd dist/
sed -i -e 's/__webpack_require__.p = \"\"\;/__webpack_require__.p = \"..\/..\/Content\/paf\/\"\;/g' inline.bundle.js
sed -i -e 's/\/assets/..\/..\/Content\/paf\/assets/g' inline.bundle.js
sed -i -e 's/\/assets/..\/..\/Content\/paf\/assets/g' scripts.bundle.js
sed -i -e 's/\/assets/..\/..\/Content\/paf\/assets/g' styles.bundle.js
sed -i -e 's/\/assets/..\/..\/Content\/paf\/assets/g' main.bundle.js

# rm -rf assets
# rm -rf index.html

mkdir -p /Volumes/CPAI/PAF/Content/paf/
mkdir -p /Volumes/CPAI-1/PAF/Content/paf/
cp -rf * /Volumes/CPAI/PAF/Content/paf/
cp -rf * /Volumes/CPAI/CHR/Content/paf/
cp -rf * /Volumes/CPAI-1/PAF/Content/paf/
cp -rf * /Volumes/CPAI-1/CHR/Content/paf/
