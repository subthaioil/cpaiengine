webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pipes__ = __webpack_require__("../../../../../src/app/pipes/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__routes__ = __webpack_require__("../../../../../src/app/routes/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__directives_restrict_number_directive__ = __webpack_require__("../../../../../src/app/directives/restrict-number.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__containers_app_app_component__ = __webpack_require__("../../../../../src/app/containers/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__containers_cmps_cmps_dom_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-dom.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_freetext_freetext_component__ = __webpack_require__("../../../../../src/app/components/freetext/freetext.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__containers_not_found_not_found_component__ = __webpack_require__("../../../../../src/app/containers/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__containers_think_think_component__ = __webpack_require__("../../../../../src/app/containers/think/think.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_input_date_input_date_component__ = __webpack_require__("../../../../../src/app/components/input-date/input-date.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__containers_cmps_cmps_inter_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-inter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__containers_cmps_cmps_import_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-import.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__containers_cmps_cmps_search_list_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-search-list.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_input_suggestion_input_suggestion_component__ = __webpack_require__("../../../../../src/app/components/input-suggestion/input-suggestion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__containers_cmla_cmla_form_1_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-form-1.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__containers_cmla_cmla_form_2_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-form-2.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__containers_cmla_cmla_import_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-import.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_input_currency_input_currency_component__ = __webpack_require__("../../../../../src/app/components/input-currency/input-currency.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_file_upload_file_upload_component__ = __webpack_require__("../../../../../src/app/components/file-upload/file-upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_status_square_status_square_component__ = __webpack_require__("../../../../../src/app/components/status-square/status-square.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_datepicker_datepicker_component__ = __webpack_require__("../../../../../src/app/components/datepicker/datepicker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_datepicker_daterangepicker_component__ = __webpack_require__("../../../../../src/app/components/datepicker/daterangepicker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__components_loading_div_loading_div_component__ = __webpack_require__("../../../../../src/app/components/loading-div/loading-div.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_input_decimal_input_decimal_component__ = __webpack_require__("../../../../../src/app/components/input-decimal/input-decimal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pipes_currency_pipe__ = __webpack_require__("../../../../../src/app/pipes/currency.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__components_sugester_sugester_component__ = __webpack_require__("../../../../../src/app/components/sugester/sugester.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__components_shared_form_shared_form_component__ = __webpack_require__("../../../../../src/app/components/shared-form/shared-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pipes_create_list_pipe__ = __webpack_require__("../../../../../src/app/pipes/create-list.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pipes_dom_review_list_pipe__ = __webpack_require__("../../../../../src/app/pipes/dom-review-list.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pipes_attachment_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/attachment-filter.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__components_new_freetext_new_freetext_component__ = __webpack_require__("../../../../../src/app/components/new-freetext/new-freetext.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__components_cmla_cmla_form_one_cmla_form_one_component__ = __webpack_require__("../../../../../src/app/components/cmla/cmla-form-one/cmla-form-one.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__components_cmla_cmla_form_one_cmla_calculate_table_component__ = __webpack_require__("../../../../../src/app/components/cmla/cmla-form-one/cmla-calculate-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_input_float_input_float_component__ = __webpack_require__("../../../../../src/app/components/input-float/input-float.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__components_input_quantity_input_quantity_component__ = __webpack_require__("../../../../../src/app/components/input-quantity/input-quantity.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pipes_cmla_cal_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/cmla-cal-filter.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__containers_search_search_component__ = __webpack_require__("../../../../../src/app/containers/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__containers_cmla_cmla_bitumen_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-bitumen.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__common_custom_request_options__ = __webpack_require__("../../../../../src/app/common/custom-request-options.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pipes_allocations_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/allocations-filter.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__components_datatable_datatable_component__ = __webpack_require__("../../../../../src/app/components/datatable/datatable.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__containers_demurrage_demurrage_form_component__ = __webpack_require__("../../../../../src/app/containers/demurrage/demurrage-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__containers_cmps_cmps_other_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-other.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_11__containers_app_app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_12__containers_cmps_cmps_dom_component__["a" /* CmpsDomComponent */],
            __WEBPACK_IMPORTED_MODULE_13__components_freetext_freetext_component__["a" /* FreetextComponent */],
            __WEBPACK_IMPORTED_MODULE_14__containers_not_found_not_found_component__["a" /* NotFoundComponent */],
            __WEBPACK_IMPORTED_MODULE_15__containers_think_think_component__["a" /* ThinkComponent */],
            __WEBPACK_IMPORTED_MODULE_16__components_input_date_input_date_component__["a" /* InputDateComponent */],
            __WEBPACK_IMPORTED_MODULE_25__components_file_upload_file_upload_component__["a" /* FileUploadComponent */],
            __WEBPACK_IMPORTED_MODULE_17__containers_cmps_cmps_inter_component__["a" /* CmpsInterComponent */],
            __WEBPACK_IMPORTED_MODULE_18__containers_cmps_cmps_import_component__["a" /* CmpsImportComponent */],
            __WEBPACK_IMPORTED_MODULE_19__containers_cmps_cmps_search_list_component__["a" /* CmpsSearchListComponent */],
            __WEBPACK_IMPORTED_MODULE_20__components_input_suggestion_input_suggestion_component__["a" /* InputSuggestionComponent */],
            __WEBPACK_IMPORTED_MODULE_21__containers_cmla_cmla_form_1_component__["a" /* CmlaForm1Component */],
            __WEBPACK_IMPORTED_MODULE_22__containers_cmla_cmla_form_2_component__["a" /* CmlaForm2Component */],
            __WEBPACK_IMPORTED_MODULE_23__containers_cmla_cmla_import_component__["a" /* CmlaImportComponent */],
            __WEBPACK_IMPORTED_MODULE_24__components_input_currency_input_currency_component__["a" /* InputCurrencyComponent */],
            __WEBPACK_IMPORTED_MODULE_25__components_file_upload_file_upload_component__["a" /* FileUploadComponent */],
            __WEBPACK_IMPORTED_MODULE_26__components_status_square_status_square_component__["a" /* StatusSquareComponent */],
            __WEBPACK_IMPORTED_MODULE_27__components_datepicker_datepicker_component__["a" /* DatepickerComponent */],
            __WEBPACK_IMPORTED_MODULE_28__components_datepicker_daterangepicker_component__["a" /* DaterangepickerComponent */],
            __WEBPACK_IMPORTED_MODULE_29__components_loading_div_loading_div_component__["a" /* LoadingDivComponent */],
            __WEBPACK_IMPORTED_MODULE_30__components_input_decimal_input_decimal_component__["a" /* InputDecimalComponent */],
            __WEBPACK_IMPORTED_MODULE_31__pipes_currency_pipe__["a" /* CurrencyPipe */],
            __WEBPACK_IMPORTED_MODULE_10__directives_restrict_number_directive__["a" /* RestrictNumberDirective */],
            __WEBPACK_IMPORTED_MODULE_32__components_sugester_sugester_component__["a" /* SugesterComponent */],
            __WEBPACK_IMPORTED_MODULE_33__components_shared_form_shared_form_component__["a" /* SharedFormComponent */],
            __WEBPACK_IMPORTED_MODULE_34__pipes_create_list_pipe__["a" /* CreateListPipe */],
            __WEBPACK_IMPORTED_MODULE_35__pipes_dom_review_list_pipe__["a" /* DomReviewListPipe */],
            __WEBPACK_IMPORTED_MODULE_36__pipes_attachment_filter_pipe__["a" /* AttachmentFilterPipe */],
            __WEBPACK_IMPORTED_MODULE_37__components_new_freetext_new_freetext_component__["a" /* NewFreetextComponent */],
            __WEBPACK_IMPORTED_MODULE_38__components_cmla_cmla_form_one_cmla_form_one_component__["a" /* CmlaFormOneComponent */],
            __WEBPACK_IMPORTED_MODULE_40__components_input_float_input_float_component__["a" /* InputFloatComponent */],
            __WEBPACK_IMPORTED_MODULE_41__components_input_quantity_input_quantity_component__["a" /* InputQuantityComponent */],
            __WEBPACK_IMPORTED_MODULE_39__components_cmla_cmla_form_one_cmla_calculate_table_component__["a" /* CmlaCalculateTableComponent */],
            __WEBPACK_IMPORTED_MODULE_42__pipes_cmla_cal_filter_pipe__["a" /* CmlaCalFilterPipe */],
            __WEBPACK_IMPORTED_MODULE_43__containers_search_search_component__["a" /* SearchComponent */],
            __WEBPACK_IMPORTED_MODULE_8__pipes__["a" /* DatexPipe */],
            __WEBPACK_IMPORTED_MODULE_44__containers_cmla_cmla_bitumen_component__["a" /* CmlaBitumenComponent */],
            __WEBPACK_IMPORTED_MODULE_46__pipes_allocations_filter_pipe__["a" /* AllocationsFilterPipe */],
            __WEBPACK_IMPORTED_MODULE_47__components_datatable_datatable_component__["a" /* DatatableComponent */],
            __WEBPACK_IMPORTED_MODULE_48__containers_demurrage_demurrage_form_component__["a" /* DemurrageFormComponent */],
            __WEBPACK_IMPORTED_MODULE_49__containers_cmps_cmps_other_component__["a" /* CmpsOtherComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_9__routes__["a" /* routes */], { useHash: true }),
            // Select2Module,
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["a" /* ModalModule */],
            __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["b" /* TypeaheadModule */].forRoot(),
        ],
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* RequestOptions */], useClass: __WEBPACK_IMPORTED_MODULE_45__common_custom_request_options__["a" /* CustomRequestOptions */] }
        ].concat(__WEBPACK_IMPORTED_MODULE_7__services__["a" /* services */], __WEBPACK_IMPORTED_MODULE_8__pipes__["b" /* pipes */], [
            __WEBPACK_IMPORTED_MODULE_5__angular_common__["a" /* DecimalPipe */],
        ]),
        bootstrap: [__WEBPACK_IMPORTED_MODULE_11__containers_app_app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/common/button.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Button; });
var Button = (function () {
    function Button() {
    }
    Button.getMessage = function (btn_name) {
        var message = "";
        switch (btn_name) {
            case "CANCEL":
                message = "Reason for cancel : The transaction cannot be reused.";
                break;
            case "REJECT":
                message = "Reason for reject : The transaction can be resubmit and approve again";
                break;
            case "APPROVE":
                message = "Reason for approve";
        }
        return message;
    };
    Button.getCancelMessage = function () {
        return Button.getMessage('CANCEL');
    };
    Button.getRejectMessage = function () {
        return Button.getMessage('REJECT');
    };
    Button.getApproveMessage = function () {
        return Button.getMessage('APPROVE');
    };
    return Button;
}());

//# sourceMappingURL=button.js.map

/***/ }),

/***/ "../../../../../src/app/common/constant.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BASE_API; });
var BASE_API = base_api.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
//# sourceMappingURL=constant.js.map

/***/ }),

/***/ "../../../../../src/app/common/custom-request-options.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomRequestOptions; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var CustomRequestOptions = (function (_super) {
    __extends(CustomRequestOptions, _super);
    function CustomRequestOptions() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Headers */]({
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT'
        });
        return _this;
    }
    return CustomRequestOptions;
}(__WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* BaseRequestOptions */]));
CustomRequestOptions = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])()
], CustomRequestOptions);

//# sourceMappingURL=custom-request-options.js.map

/***/ }),

/***/ "../../../../../src/app/common/date-helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateHelper; });

var DateHelper = (function () {
    function DateHelper() {
    }
    DateHelper.DateRangeStringToDate = function (date_range_string) {
        return null;
    };
    DateHelper.DateStringToDate = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(date_string, DateHelper.from_format).toDate();
        // let date = new Date()  // this creates a date object in the user's timezone
        // date.setDate(Number(date_string.slice(0, 2)))
        // date.setMonth((NumberparseInt(date_string.slice(3, 5)) - 1))  // months a indexed at 0 in js
        // date.setYear(Number(date_string.slice(6, 10)))
        return date;
    };
    DateHelper.DateRangeStringToCsharpString = function (date_string) {
        var d = date_string.split(" to ");
        var date_start = d[0] && __WEBPACK_IMPORTED_MODULE_0_moment_moment__(d[0], DateHelper.from_format).format(DateHelper.to_format);
        var date_end = d[1] && __WEBPACK_IMPORTED_MODULE_0_moment_moment__(d[1], DateHelper.from_format).format(DateHelper.to_format);
        if (date_start == "Invalid date" || date_end == "Invalid date") {
            date_start = null;
            date_end = null;
        }
        return {
            date_start: date_start,
            date_end: date_end,
        };
    };
    DateHelper.DateStringToCsharpString = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(date_string, DateHelper.from_format).format(DateHelper.to_format);
        if (date == "Invalid date") {
            return null;
        }
        return date;
    };
    DateHelper.DateDotNetToCsharpString = function (date_string) {
        var date = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(date_string).format(DateHelper.to_format);
        if (date == "Invalid date") {
            return null;
        }
        return date;
    };
    DateHelper.DateToString = function (date) {
        var date_str = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(date).format(DateHelper.from_format);
        if (date_str == "Invalid date") {
            date_str = "";
        }
        return date_str;
    };
    return DateHelper;
}());

DateHelper.from_format = 'DD/MM/YYYY';
DateHelper.to_format = 'YYYY-MM-DD';
// var reformat = function (viewValue, from_format, to_format) {
//         console.log('viewValue', viewValue);
//         from_format = from_format || 'DD-MM-YYYY';
//         to_format = to_format || 'YYYY-MM-DD';
//         // var regex = /\d{2}-\d{2}-\d{4} to \d{2}-\d{2}-\d{4}/g;
//         // var result = regex.test(viewValue);
//         // if (!result) {
//         //   from_format = 'YYYY-MM-DD';
//         // }
//         // var dates = viewValue.split(" to ");
//         // var start_date = moment(dates[0], from_format).format(to_format);     // false (not a real month)
//         // var end_date = moment(dates[1], from_format).format(to_format);     // false (not a real month)
//         // new_date = start_date + " to " + end_date;
//         var date = moment(viewValue, from_format).format(to_format);
//         return date;
//       }
//       var reformat = function (viewValue, from_format, to_format) {
//         console.log('viewValue', viewValue);
//         from_format = from_format || 'DD-MM-YYYY';
//         to_format = to_format || 'YYYY-MM-DD';
//         // var regex = /\d{2}-\d{2}-\d{4} to \d{2}-\d{2}-\d{4}/g;
//         // var result = regex.test(viewValue);
//         // if (!result) {
//         //   from_format = 'YYYY-MM-DD';
//         // }
//         var dates = viewValue.split(" to ");
//         var start_date = moment(dates[0], from_format).format(to_format);     // false (not a real month)
//         var end_date = moment(dates[1], from_format).format(to_format);     // false (not a real month)
//         new_date = start_date + " to " + end_date;
//         return new_date;
//       }
//# sourceMappingURL=date-helper.js.map

/***/ }),

/***/ "../../../../../src/app/common/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__validate_helper__ = __webpack_require__("../../../../../src/app/common/validate-helper.ts");
/* unused harmony reexport ValidateHelper */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__utility__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__permission_store__ = __webpack_require__("../../../../../src/app/common/permission-store.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_2__permission_store__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__paf_store__ = __webpack_require__("../../../../../src/app/common/paf-store.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_3__paf_store__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_4__date_helper__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_5__constant__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__button__ = __webpack_require__("../../../../../src/app/common/button.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_6__button__["a"]; });







//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/common/paf-store.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2____ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PAFStore; });



var PAFStore = (function () {
    function PAFStore() {
    }
    PAFStore.showProducts = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.master.PRODUCTS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    PAFStore.prototype.normalizeDate = function (model) {
        var date_str = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateToString(model);
        if (date_str == "Invalid date") {
            date_str = "";
        }
        return date_str;
    };
    PAFStore.CheckRealyDisabled = function (old_val, buttons, status) {
        if (status != "DRAFT") {
            return old_val;
        }
        var has_draft_btn = __WEBPACK_IMPORTED_MODULE_1_lodash__["findIndex"](buttons, function (e) {
            return e.name == "SAVE DRAFT";
        }) != -1;
        return has_draft_btn ? false : true;
    };
    PAFStore.UpdateModel = function (model) {
        if (model && model.PAF_BTM_DETAIL && model.PAF_BTM_DETAIL.PBD_FX_DATE_FROM && model.PAF_BTM_DETAIL.PBD_FX_DATE_TO) {
            model.PAF_BTM_DETAIL.PBD_FX_DATE_FROM = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(model.PAF_BTM_DETAIL.PBD_FX_DATE_FROM).format('YYYY-MM-DD');
            model.PAF_BTM_DETAIL.PBD_FX_DATE_TO = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(model.PAF_BTM_DETAIL.PBD_FX_DATE_TO).format('YYYY-MM-DD');
        }
        if (model && model.PAF_BTM_DETAIL && model.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM && model.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO) {
            model.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(model.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM).format('YYYY-MM-DD');
            model.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO = __WEBPACK_IMPORTED_MODULE_0_moment_moment__(model.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO).format('YYYY-MM-DD');
        }
        // if(!model.PAF_PROPOSAL_ITEMS || model.PAF_PROPOSAL_ITEMS.length == 0) {
        //   model.PAF_PROPOSAL_ITEMS = [{}];
        // }
        model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_PAYMENT_ITEMS, function (e) {
            for (var i = 0; i < 5; ++i) {
                try {
                    e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                }
                catch (e) {
                    e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                }
            }
            return e;
        });
        model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], model.PAF_PROPOSAL_ITEMS);
        model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_PROPOSAL_ITEMS, function (e) {
            // console.log(model.PAF_PROPOSAL_ITEMS)
            e.PAF_PROPOSAL_OTC_ITEMS = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].FillArray(e.PAF_PROPOSAL_OTC_ITEMS, {}, 4);
            e.PAF_PROPOSAL_REASON_ITEMS = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].FillArray(e.PAF_PROPOSAL_REASON_ITEMS, {}, 3);
            // for (var i = 0; i < 3; ++i) {
            //   try
            //   {
            //     e.PAF_PROPOSAL_OTC_ITEMS[i] = e.PAF_PROPOSAL_OTC_ITEMS[i] || {};
            //   }
            //   catch (e)
            //   {
            //     e.PAF_PROPOSAL_OTC_ITEMS.push({});
            //   }
            // }
            // for (var i = 0; i < 2; ++i) {
            //   try
            //   {
            //     e.PAF_PROPOSAL_REASON_ITEMS[i] = e.PAF_PROPOSAL_REASON_ITEMS[i] || {};
            //   }
            //   catch (e)
            //   {
            //     e.PAF_PROPOSAL_REASON_ITEMS.push({});
            //     // e.PAF_PROPOSAL_REASON_ITEMS[i] = {};
            //   }
            // }
            return e;
        });
        var max_round_len = 0;
        for (var i = 0; i < model.PAF_BIDDING_ITEMS.length; ++i) {
            var round_count = model.PAF_BIDDING_ITEMS[i].PAF_ROUND.length;
            max_round_len = max_round_len < round_count ? round_count : max_round_len;
        }
        model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_BIDDING_ITEMS, function (e) {
            e.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(__WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].FillArray(e.PAF_ROUND, {}, max_round_len));
            e.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], e.PAF_BIDDING_ITEMS_BENCHMARK);
            if (e.PAF_BIDDING_ITEMS_BENCHMARK.length == 0) {
                e.PAF_BIDDING_ITEMS_BENCHMARK = [{}];
            }
            e.SELECTED = __WEBPACK_IMPORTED_MODULE_1_lodash__["findIndex"](model.PAF_PAYMENT_ITEMS, function (ee) {
                return ee.PPI_FK_CUSTOMER == e.PBI_FK_CUSTOMER;
            }) != -1;
            e.PBI_QUANTITY_FLOAT = e.PBI_QUANTITY_FLOAT || '';
            e.PBI_QUANTITY_FLOAT = e.PBI_QUANTITY_FLOAT.replace('?', '±');
            e.PBI_QUANTITY_FLOAT = e.PBI_QUANTITY_FLOAT.replace('+/-', '±');
            return e;
        });
        // model.PAF_REVIEW_ITEMS = _.extend([], model.PAF_REVIEW_ITEMS);
        // if(!model.PAF_REVIEW_ITEMS || model.PAF_REVIEW_ITEMS.length == 0) {
        //   model.PAF_REVIEW_ITEMS = [{}];
        // }
        model.PAF_REVIEW_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_REVIEW_ITEMS, function (e) {
            if (!e.PAF_REVIEW_ITEMS_DETAIL || e.PAF_REVIEW_ITEMS_DETAIL.length == 0) {
                e.PAF_REVIEW_ITEMS_DETAIL = [{}];
            }
            return e;
        });
        model.PDA_CONTRACT_DATE_FROM = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateDotNetToCsharpString(model.PDA_CONTRACT_DATE_FROM);
        model.PDA_CONTRACT_DATE_TO = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateDotNetToCsharpString(model.PDA_CONTRACT_DATE_TO);
        model.PDA_LOADING_DATE_FROM = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateDotNetToCsharpString(model.PDA_LOADING_DATE_FROM);
        model.PDA_LOADING_DATE_TO = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateDotNetToCsharpString(model.PDA_LOADING_DATE_TO);
        model.PDA_DISCHARGING_DATE_FROM = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateDotNetToCsharpString(model.PDA_DISCHARGING_DATE_FROM);
        model.PDA_DISCHARGING_DATE_TO = __WEBPACK_IMPORTED_MODULE_2____["b" /* DateHelper */].DateDotNetToCsharpString(model.PDA_DISCHARGING_DATE_TO);
        return model;
    };
    PAFStore.HandleChangeCustomer = function (customer, model) {
        console.log('selected customer', customer);
        model.PAF_REVIEW_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_REVIEW_ITEMS, function (elem) {
            elem.PRI_FK_CUSTOMER = customer.ID;
            elem.SHOW_CUSTOMER = customer.VALUE;
            return elem;
        });
        model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_BIDDING_ITEMS, function (elem) {
            elem.PRI_FK_CUSTOMER = customer.ID;
            elem.SHOW_CUSTOMER = customer.VALUE;
            return elem;
        });
        return model;
    };
    PAFStore.RemovePAFReviewItems = function (i, j, review_items) {
        var is_first = j == 0;
        var target;
        if (is_first) {
            var target_1 = review_items[i];
            review_items = __WEBPACK_IMPORTED_MODULE_1_lodash__["reject"](review_items, function (e) { return e == target_1; });
        }
        else {
            var target_2 = review_items[i].PAF_REVIEW_ITEMS_DETAIL[j];
            review_items[i].PAF_REVIEW_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_1_lodash__["reject"](review_items[i].PAF_REVIEW_ITEMS_DETAIL, function (e) { return e == target_2; });
        }
        return review_items;
    };
    PAFStore.GetPAFReviewItemsSummary = function (review_items, master, is_cmps_dom) {
        var customer;
        var product;
        var tier_amount;
        var customer_id = review_items && review_items.PRI_FK_CUSTOMER;
        var product_id = review_items && review_items.PRI_FK_MATERIAL;
        customer = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(customer_id, master.CUSTOMERS);
        product = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(product_id, master.PRODUCTS_DOM);
        tier_amount = review_items && review_items.PAF_REVIEW_ITEMS_DETAIL && review_items.PAF_REVIEW_ITEMS_DETAIL.length;
        tier_amount = tier_amount;
        customer = customer || "";
        product = product || "";
        return {
            customer: customer,
            product: product,
            tier_amount: tier_amount,
        };
    };
    PAFStore.GetPAFBidderItemsSummary = function (PAF_BIDDING_ITEMS, master, is_cmps_dom) {
        var customer;
        var product;
        var customer_id = PAF_BIDDING_ITEMS && PAF_BIDDING_ITEMS.PBI_FK_CUSTOMER;
        var product_id = PAF_BIDDING_ITEMS && PAF_BIDDING_ITEMS.PBI_FK_MATERIAL;
        customer = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(customer_id, master.CUSTOMERS);
        product = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(product_id, master.PRODUCTS);
        if (is_cmps_dom) {
            product = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(product_id, master.PRODUCTS_DOM);
        }
        customer = customer || "";
        product = product || "";
        return {
            customer: customer,
            product: product,
        };
    };
    PAFStore.GetPAFBidderItemsSummaryCMLA = function (review_items, CUSTOMERS, PRODUCTS) {
        // console.log(review_items, CUSTOMERS, PRODUCTS);
        var customer;
        var product;
        var customer_id = review_items && review_items.PRI_FK_CUSTOMER;
        var product_id = review_items && review_items.PRI_FK_MATERIAL;
        customer = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(customer_id, CUSTOMERS);
        product = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].getTextByID(product_id, PRODUCTS);
        customer = customer || "";
        product = product || "";
        return {
            customer: customer,
            product: product,
        };
    };
    PAFStore.prototype.IsNeedFreight = function (record, master, is_cmps_import) {
        var result = false;
        var incoterm = record && record.PBI_INCOTERM;
        if (incoterm) {
            var incoterm_object = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](master.INCOTERMS, function (e) {
                return e.ID == incoterm;
            });
            result = ((incoterm_object && incoterm_object.INCLUDED_FREIGHT) || false);
        }
        return result;
        // if(is_cmps_import) {
        //   return !result;
        // } else {
        //   // console.log(result)
        //   return result;
        // }
    };
    PAFStore.UpdateInterTable = function (criteria, bidding_items, show_bidding_items) {
        var result = [];
        var new_bidding_items = [];
        // let count = bidding_items.length || 1;
        var count = (show_bidding_items.length || 1);
        // let count = ((bidding_items.length || 0) / (criteria.products.length || 1)) || 1;
        // console.log(count)
        if (show_bidding_items.length != count) {
            // let num = Math.abs(show_bidding_items.length - count);
            // result = _.times(count, _.constant({
            //   SELECTED: false,
            //   PAF_ROUND: [{
            //   }],
            // }));
        }
        result = show_bidding_items;
        // console.log(`criteria.products.length`, criteria.products.length);
        result = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](result, function (e) {
            // console.log(`e.PRODUCTS.length`, e.PRODUCTS.length);
            // console.log(`criteria.products.length`, criteria.products.length);
            // if(e.PRODUCTS.length != criteria.products.length)  {
            var last_round = __WEBPACK_IMPORTED_MODULE_1_lodash__["last"](e.PAF_ROUND);
            e.IS_FOB = e.PBI_INCOTERM == 'FOB';
            // console.log(`last_round`,last_round);
            // console.log(`last_round`,e.PAF_ROUND);
            // if(e.IS_FOB) {
            //   e.PBI_FREIGHT_PRICE = null;
            // }
            for (var i = 0; i < criteria.products.length; i++) {
                try {
                    e.PRODUCTS[i].PBI_FK_MATERIAL = criteria.products[i].PBI_FK_MATERIAL;
                    e.PRODUCTS[i].PBI_QUANTITY_UNIT = criteria.products[i].PBI_QUANTITY_UNIT;
                    // if(e.IS_FOB) {
                    //   e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED;
                    // } else {
                    //   e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED - e.PBI_FREIGHT_PRICE;
                    // }
                    // e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = e.PRODUCTS[i].SHOW_CONVERT_TO_FOB || 0;
                }
                catch (e) {
                    e.PRODUCTS = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], e.PRODUCTS);
                    e.PRODUCTS.push({
                        PBI_FK_MATERIAL: criteria.products[i].PBI_FK_MATERIAL,
                        PBI_QUANTITY_UNIT: criteria.products[i].PBI_QUANTITY_UNIT,
                    });
                }
            }
            // }
            return e;
        });
        for (var j = 0; j < criteria.products.length; j++) {
            var source = result && result.length && result[0].PRODUCTS[j];
            for (var i = 0; i < result.length; i++) {
                result[i].PRODUCTS[j].PBI_MARKET_PRICE = source.PBI_MARKET_PRICE;
                result[i].PRODUCTS[j].PBI_LATEST_LP_PLAN_PRICE_FIXED = source.PBI_LATEST_LP_PLAN_PRICE_FIXED;
                result[i].PRODUCTS[j].PBI_BENCHMARK_PRICE = source.PBI_BENCHMARK_PRICE;
                result[i].PRODUCTS[j].PBI_QUANTITY_UNIT = source.PBI_QUANTITY_UNIT;
                result[i].PRODUCTS[j].PBI_AWARDED_QUANTITY_MIN = source.PBI_AWARDED_QUANTITY_MIN;
                result[i].PRODUCTS[j].PBI_AWARDED_QUANTITY_MAX = source.PBI_AWARDED_QUANTITY_MAX;
            }
        }
        // console.log(`Products`, criteria.products);
        // let result = Utility.clone(bidding_items);
        // result = _.map(result, (e) => {
        //   e.PRODUCTS =  _.extend([], e.PRODUCTS);
        //   if(e.PRODUCTS.length != criteria.products.length)  {
        //     for (var i = 0; i < criteria.products.length; i++) {
        //       try {
        //         e.PRODUCTS[i].PBI_FK_MATERIAL = criteria.products[i].PBI_FK_MATERIAL;
        //       }
        //       catch (e)
        //       {
        //         e.PRODUCTS.push({
        //           PBI_FK_MATERIAL: criteria.products[i].PBI_FK_MATERIAL,
        //         });
        //       }
        //     }
        //   }
        //   return e;
        // });
        // console.log(`UpdateInterTable`, result);
        // console.log(`UpdateInterTable`, s);
        return {
            PAF_BIDDING_ITEMS: new_bidding_items,
            SHOW_PAF_BIDDING_ITEMS: result,
        };
    };
    PAFStore.ReverseUpdateInterTable = function (criteria, bidding_items, show_bidding_items) {
        var result = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(show_bidding_items);
        // console.log(bidding_items);
        // let s = _.chain(result)
        // .groupBy('PBI_FK_CUSTOMER')
        // .value();
        console.log("UpdateInterTable", criteria.products);
        return show_bidding_items;
    };
    PAFStore.GetCustomers = function (company) {
        company = company || "";
        // console
        var customers = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](this.master.REAL_CUSTOMERS_INTER, function (e) {
            return e.COMPANY_CODE && e.COMPANY_CODE.trim() == company.trim();
        });
        customers = __WEBPACK_IMPORTED_MODULE_1_lodash__["uniqBy"](customers, 'ID');
        console.log(company, customers);
        // console.log(company, customers);
        // console.log(company, customers);
        return customers;
    };
    PAFStore.AdjustInterModelBeforeSave = function (criteria) {
        // console.log(criteria);
        var result = [];
        var count_product = +criteria.products.length;
        var count_customer = +criteria.PAF_BIDDING_ITEMS.length;
        console.log(count_product * count_customer);
        result = __WEBPACK_IMPORTED_MODULE_1_lodash__["times"](count_product * count_customer, __WEBPACK_IMPORTED_MODULE_1_lodash__["constant"]({}));
        console.log(count_product * count_customer);
        var index = 0;
        for (var i = 0; i < count_customer; ++i) {
            for (var j = 0; j < count_product; ++j) {
                result[index] = {
                    PBI_FK_CUSTOMER: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_FK_CUSTOMER),
                    PBI_AWARDED_BIDDER: criteria.PAF_BIDDING_ITEMS[i].SELECTED ? "Y" : "N",
                    PBI_CONTACT_PERSON: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_CONTACT_PERSON),
                    PBI_PRICING_PERIOD: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_PRICING_PERIOD),
                    PBI_INCOTERM: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_INCOTERM),
                    PBI_COUNTRY: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_COUNTRY),
                    PBI_FREIGHT_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_FREIGHT_PRICE),
                    PBI_NOTE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_NOTE),
                    PBI_FK_MATERIAL: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_FK_MATERIAL),
                    PBI_QUANTITY_UNIT: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_UNIT),
                    PBI_QUANTITY_MIN: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_MIN),
                    PBI_QUANTITY_MAX: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_MAX),
                    SHOW_CONVERT_TO_FOB: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].SHOW_CONVERT_TO_FOB),
                    PBI_MARKET_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_MARKET_PRICE),
                    PBI_LATEST_LP_PLAN_PRICE_FIXED: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_LATEST_LP_PLAN_PRICE_FIXED),
                    PBI_BENCHMARK_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_BENCHMARK_PRICE),
                    PBI_AWARDED_QUANTITY_MIN: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_AWARDED_QUANTITY_MIN),
                    PBI_AWARDED_QUANTITY_MAX: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_AWARDED_QUANTITY_MAX),
                    PBI_PORT: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_PORT),
                    PBI_QUANTITY_TOLERANCE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_TOLERANCE),
                    PBI_QUANTITY_TOLERANCE_OPTION: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_TOLERANCE_OPTION),
                    PAF_ROUND: [],
                };
                var PAF_ROUND = criteria.PAF_BIDDING_ITEMS[i].PAF_ROUND;
                for (var k = 0; k < PAF_ROUND.length; ++k) {
                    console.log(PAF_ROUND[k].PRODUCTS[j]);
                    result[index].PAF_ROUND.push({
                        PRN_COUNT: (k + 1),
                        PRN_FIXED: PAF_ROUND[k].PRODUCTS[j].PRN_FIXED
                    });
                }
                index++;
            }
        }
        return result;
    };
    PAFStore.AdjustImportModelBeforeSave = function (criteria) {
        var result = [];
        var count_product = +criteria.products.length;
        var count_customer = +criteria.PAF_BIDDING_ITEMS.length;
        console.log(count_product * count_customer);
        result = __WEBPACK_IMPORTED_MODULE_1_lodash__["times"](count_product * count_customer, __WEBPACK_IMPORTED_MODULE_1_lodash__["constant"]({}));
        console.log(count_product * count_customer);
        var index = 0;
        for (var i = 0; i < count_customer; ++i) {
            for (var j = 0; j < count_product; ++j) {
                result[index] = {
                    PBI_FK_VENDOR: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_FK_CUSTOMER),
                    PBI_AWARDED_BIDDER: criteria.PAF_BIDDING_ITEMS[i].SELECTED ? "Y" : "N",
                    PBI_CONTACT_PERSON: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_CONTACT_PERSON),
                    PBI_PRICING_PERIOD: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_PRICING_PERIOD),
                    PBI_INCOTERM: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_INCOTERM),
                    PBI_COUNTRY: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_COUNTRY),
                    PBI_FREIGHT_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_FREIGHT_PRICE),
                    PBI_NOTE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_NOTE),
                    PBI_FK_MATERIAL: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_FK_MATERIAL),
                    PBI_QUANTITY_UNIT: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_UNIT),
                    PBI_QUANTITY_MIN: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_MIN),
                    PBI_QUANTITY_MAX: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_MAX),
                    SHOW_CONVERT_TO_FOB: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].SHOW_CONVERT_TO_FOB),
                    PBI_MARKET_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_MARKET_PRICE),
                    PBI_LATEST_LP_PLAN_PRICE_FIXED: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_LATEST_LP_PLAN_PRICE_FIXED),
                    PBI_BENCHMARK_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_BENCHMARK_PRICE),
                    PBI_AWARDED_QUANTITY_MIN: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_AWARDED_QUANTITY_MIN),
                    PBI_AWARDED_QUANTITY_MAX: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_AWARDED_QUANTITY_MAX),
                    PBI_PORT: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PBI_PORT),
                    PBI_QUANTITY_TOLERANCE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_TOLERANCE),
                    PBI_QUANTITY_TOLERANCE_OPTION: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria.PAF_BIDDING_ITEMS[i].PRODUCTS[j].PBI_QUANTITY_TOLERANCE_OPTION),
                    PAF_ROUND: [],
                };
                var PAF_ROUND = criteria.PAF_BIDDING_ITEMS[i].PAF_ROUND;
                for (var k = 0; k < PAF_ROUND.length; ++k) {
                    console.log(PAF_ROUND[k].PRODUCTS[j]);
                    result[index].PAF_ROUND.push({
                        PRN_COUNT: (k + 1),
                        PRN_FIXED: PAF_ROUND[k].PRODUCTS[j].PRN_FIXED
                    });
                }
                index++;
            }
        }
        return result;
    };
    // public static LoadInterModelToCiteriaOld(criteria, PAF_BIDDING_ITEMS: any) : any {
    //   let new_criteria = Utility.clone(criteria);
    //   let NEW_PAF_BIDDING_ITEMS = [];
    //   let ordered = _.orderBy(PAF_BIDDING_ITEMS, ['PBI_ROW_ID'], ['asc']);
    //   // let ordered = _.orderBy(PAF_BIDDING_ITEMS, ['PBI_FK_MATERIAL'], ['asc']);
    //   let grouped = _.groupBy(ordered, 'PBI_FK_CUSTOMER');
    //   let keys = _.keys(grouped);
    //   console.log(`keys`,PAF_BIDDING_ITEMS)
    //   if(keys) {
    //     new_criteria.products = _.map(grouped[keys[0]], (e) => {
    //       return Utility.clone({
    //         PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
    //         PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
    //       })
    //     });
    //   }
    //   for (var i = 0; i < keys.length; ++i) {
    //     let item: any = {};
    //     let count_product = grouped[keys[i]].length;
    //     item = {
    //       PBI_FK_CUSTOMER: Utility.clone(grouped[keys[i]][0].PBI_FK_CUSTOMER),
    //       PBI_CONTACT_PERSON: Utility.clone(grouped[keys[i]][0].PBI_CONTACT_PERSON),
    //       PBI_PRICING_PERIOD: Utility.clone(grouped[keys[i]][0].PBI_PRICING_PERIOD),
    //       PBI_INCOTERM: Utility.clone(grouped[keys[i]][0].PBI_INCOTERM),
    //       IS_FOB: Utility.clone(grouped[keys[i]][0].PBI_INCOTERM == "FOB"),
    //       PBI_COUNTRY: Utility.clone(grouped[keys[i]][0].PBI_COUNTRY),
    //       PBI_FREIGHT_PRICE: Utility.clone(grouped[keys[i]][0].PBI_FREIGHT_PRICE),
    //       PBI_NOTE: Utility.clone(grouped[keys[i]][0].PBI_NOTE),
    //       PBI_PORT: Utility.clone(grouped[keys[i]][0].PBI_PORT),
    //       PAF_ROUND: [{
    //         PRODUCTS: _.times(count_product, _.constant({}))
    //       }],
    //       PRODUCTS: _.map(grouped[keys[i]], (e) => {
    //         const factor = (e.PBI_QUANTITY_TOLERANCE && e.PBI_QUANTITY_TOLERANCE.split(" ")) || []
    //         const prefix = factor.length && factor[0].trim();
    //         const suffix = factor.length && factor[1].trim();
    //         return {
    //           PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
    //           PBI_QUANTITY_MIN: e.PBI_QUANTITY_MIN,
    //           PBI_QUANTITY_MAX: e.PBI_QUANTITY_MAX,
    //           SHOW_CONVERT_TO_FOB: e.SHOW_CONVERT_TO_FOB,
    //           PBI_MARKET_PRICE: e.PBI_MARKET_PRICE,
    //           PBI_LATEST_LP_PLAN_PRICE_FIXED: e.PBI_LATEST_LP_PLAN_PRICE_FIXED,
    //           PBI_BENCHMARK_PRICE: e.PBI_BENCHMARK_PRICE,
    //           PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
    //           PBI_AWARDED_QUANTITY_MIN: e.PBI_AWARDED_QUANTITY_MIN,
    //           PBI_AWARDED_QUANTITY_MAX: e.PBI_AWARDED_QUANTITY_MAX,
    //           PBI_QUANTITY_TOLERANCE_PREFIX: prefix,
    //           PBI_QUANTITY_TOLERANCE_SUFFIX: suffix,
    //           PBI_QUANTITY_TOLERANCE_OPTION: e.PBI_QUANTITY_TOLERANCE_OPTION || "",
    //           PBI_QUANTITY_TOLERANCE: e.PBI_QUANTITY_TOLERANCE,
    //         }
    //       }),
    //     }
    //     let rounds = grouped[keys[i]][0].PAF_ROUND;
    //     for (var k = 0; k < rounds.length; ++k) {
    //       for (var l = 0; l < count_product; ++l) {
    //         item.PAF_ROUND[k] = _.extend({}, item.PAF_ROUND[k]);
    //         item.PAF_ROUND[k].PRODUCTS = _.extend([], item.PAF_ROUND[k].PRODUCTS);
    //         item.PAF_ROUND[k].PRODUCTS[l] = _.extend({}, item.PAF_ROUND[k].PRODUCTS[l]);
    //         item.PAF_ROUND[k].PRODUCTS[l].PRN_FIXED = grouped[keys[i]][l].PAF_ROUND[k].PRN_FIXED;
    //       }
    //     }
    //     NEW_PAF_BIDDING_ITEMS.push(Utility.clone(item))
    //   }
    //   console.log(`grouped`, grouped);
    //   console.log(`NEW_PAF_BIDDING_ITEMS`, NEW_PAF_BIDDING_ITEMS);
    //   console.log(`NEW_PAF_BIDDING_ITEMS`, NEW_PAF_BIDDING_ITEMS);
    //   // new_criteria.PAF_BIDDING_ITEMS =
    //   // return new_criteria;
    //   new_criteria.PAF_BIDDING_ITEMS = NEW_PAF_BIDDING_ITEMS;
    //   new_criteria.PAF_BIDDING_ITEMS = _.map(new_criteria.PAF_BIDDING_ITEMS, (e) => {
    //     e.IS_FOB = e.PBI_INCOTERM == 'FOB';
    //     let last_round = _.last(e.PAF_ROUND);
    //     for (var i = 0; i < e.PRODUCTS.length; i++) {
    //       if(e.IS_FOB) {
    //         e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED;
    //       } else {
    //         e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED - e.PBI_FREIGHT_PRICE;
    //       }
    //     }
    //     return e;
    //   });
    //   return new_criteria;
    // }
    PAFStore.LoadInterModelToCiteria = function (criteria, PAF_BIDDING_ITEMS) {
        var new_criteria = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria);
        var NEW_PAF_BIDDING_ITEMS = [];
        // let ordered = _.orderBy(PAF_BIDDING_ITEMS, ['PBI_FK_MATERIAL'], ['asc']);
        var grouped = __WEBPACK_IMPORTED_MODULE_1_lodash__["groupBy"](PAF_BIDDING_ITEMS, 'PBI_FK_CUSTOMER');
        console.log(grouped);
        var keys = __WEBPACK_IMPORTED_MODULE_1_lodash__["keys"](grouped);
        if (keys) {
            new_criteria.products = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](grouped[keys[0]], function (e) {
                return __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone({
                    PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
                    PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
                });
            });
        }
        for (var i = 0; i < keys.length; ++i) {
            // debugger;
            var item = {};
            var count_product = grouped[keys[i]].length;
            item = {
                PBI_FK_CUSTOMER: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_FK_CUSTOMER),
                PBI_CONTACT_PERSON: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_CONTACT_PERSON),
                PBI_PRICING_PERIOD: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_PRICING_PERIOD),
                PBI_INCOTERM: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_INCOTERM),
                IS_FOB: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_INCOTERM == "FOB"),
                PBI_COUNTRY: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_COUNTRY),
                PBI_FREIGHT_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_FREIGHT_PRICE),
                PBI_NOTE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_NOTE),
                PBI_PORT: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_PORT),
                PAF_ROUND: [{
                        PRODUCTS: __WEBPACK_IMPORTED_MODULE_1_lodash__["times"](count_product, __WEBPACK_IMPORTED_MODULE_1_lodash__["constant"]({}))
                    }],
                PRODUCTS: __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](grouped[keys[i]], function (e) {
                    var factor = (e.PBI_QUANTITY_TOLERANCE && e.PBI_QUANTITY_TOLERANCE.split(" ")) || [];
                    var prefix = factor.length && factor[0].trim();
                    var suffix = factor.length && factor[1].trim();
                    return {
                        PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
                        PBI_QUANTITY_MIN: e.PBI_QUANTITY_MIN,
                        PBI_QUANTITY_MAX: e.PBI_QUANTITY_MAX,
                        SHOW_CONVERT_TO_FOB: e.SHOW_CONVERT_TO_FOB,
                        PBI_MARKET_PRICE: e.PBI_MARKET_PRICE,
                        PBI_LATEST_LP_PLAN_PRICE_FIXED: e.PBI_LATEST_LP_PLAN_PRICE_FIXED,
                        PBI_BENCHMARK_PRICE: e.PBI_BENCHMARK_PRICE,
                        PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
                        PBI_AWARDED_QUANTITY_MIN: e.PBI_AWARDED_QUANTITY_MIN,
                        PBI_AWARDED_QUANTITY_MAX: e.PBI_AWARDED_QUANTITY_MAX,
                        PBI_QUANTITY_TOLERANCE: e.PBI_QUANTITY_TOLERANCE,
                        PBI_QUANTITY_TOLERANCE_PREFIX: prefix,
                        PBI_QUANTITY_TOLERANCE_SUFFIX: suffix,
                        PBI_QUANTITY_TOLERANCE_OPTION: e.PBI_QUANTITY_TOLERANCE_OPTION || "",
                    };
                }),
            };
            var rounds = grouped[keys[i]][0].PAF_ROUND;
            for (var k = 0; k < rounds.length; ++k) {
                for (var l = 0; l < count_product; ++l) {
                    item.PAF_ROUND[k] = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]({}, item.PAF_ROUND[k]);
                    item.PAF_ROUND[k].PRODUCTS = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], item.PAF_ROUND[k].PRODUCTS);
                    item.PAF_ROUND[k].PRODUCTS[l] = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]({}, item.PAF_ROUND[k].PRODUCTS[l]);
                    item.PAF_ROUND[k].PRODUCTS[l].PRN_FIXED = grouped[keys[i]][l].PAF_ROUND[k].PRN_FIXED;
                }
            }
            NEW_PAF_BIDDING_ITEMS.push(__WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(item));
        }
        console.log("grouped", grouped);
        console.log("NEW_PAF_BIDDING_ITEMS", NEW_PAF_BIDDING_ITEMS);
        console.log("NEW_PAF_BIDDING_ITEMS", NEW_PAF_BIDDING_ITEMS);
        // new_criteria.PAF_BIDDING_ITEMS =
        // return new_criteria;
        new_criteria.PAF_BIDDING_ITEMS = NEW_PAF_BIDDING_ITEMS;
        new_criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](new_criteria.PAF_BIDDING_ITEMS, function (e) {
            e.IS_FOB = e.PBI_INCOTERM == 'FOB';
            var last_round = __WEBPACK_IMPORTED_MODULE_1_lodash__["last"](e.PAF_ROUND);
            for (var i = 0; i < e.PRODUCTS.length; i++) {
                if (e.IS_FOB) {
                    e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED;
                }
                else {
                    e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED - e.PBI_FREIGHT_PRICE;
                }
            }
            return e;
        });
        return new_criteria;
    };
    PAFStore.LoadImportModelToCiteria = function (criteria, PAF_BIDDING_ITEMS) {
        // let new_criteria = Utility.clone(criteria);
        // let NEW_PAF_BIDDING_ITEMS = [];
        // // let ordered = _.orderBy(PAF_BIDDING_ITEMS, ['PBI_FK_MATERIAL'], ['asc']);
        // // let ordered = _.orderBy(PAF_BIDDING_ITEMS, ['PBI_ROW_ID'], ['asc']);
        // // let grouped = _.groupBy(ordered, 'PBI_FK_VENDOR');
        // let grouped = _.groupBy(ordered, 'PBI_FK_VENDOR');
        var new_criteria = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(criteria);
        var NEW_PAF_BIDDING_ITEMS = [];
        // let ordered = _.orderBy(PAF_BIDDING_ITEMS, ['PBI_FK_MATERIAL'], ['asc']);
        var grouped = __WEBPACK_IMPORTED_MODULE_1_lodash__["groupBy"](PAF_BIDDING_ITEMS, 'PBI_FK_VENDOR');
        console.log(grouped);
        var keys = __WEBPACK_IMPORTED_MODULE_1_lodash__["keys"](grouped);
        if (keys) {
            new_criteria.products = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](grouped[keys[0]], function (e) {
                return __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone({
                    PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
                    PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
                });
            });
        }
        // console.log(grouped)
        // let keys = _.keys(grouped);
        // if(keys) {
        //   new_criteria.products = _.map(grouped[keys[0]], (e) => {
        //     return Utility.clone({
        //       PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
        //       PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
        //     })
        //   });
        // }
        for (var i = 0; i < keys.length; ++i) {
            var item = {};
            var count_product = grouped[keys[i]].length;
            item = {
                PBI_FK_CUSTOMER: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_FK_VENDOR),
                PBI_CONTACT_PERSON: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_CONTACT_PERSON),
                PBI_PRICING_PERIOD: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_PRICING_PERIOD),
                PBI_INCOTERM: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_INCOTERM),
                IS_FOB: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_INCOTERM == "FOB"),
                PBI_COUNTRY: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_COUNTRY),
                PBI_FREIGHT_PRICE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_FREIGHT_PRICE),
                PBI_NOTE: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_NOTE),
                PBI_PORT: __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(grouped[keys[i]][0].PBI_PORT),
                PAF_ROUND: [{
                        PRODUCTS: __WEBPACK_IMPORTED_MODULE_1_lodash__["times"](count_product, __WEBPACK_IMPORTED_MODULE_1_lodash__["constant"]({}))
                    }],
                PRODUCTS: __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](grouped[keys[i]], function (e) {
                    var factor = (e.PBI_QUANTITY_TOLERANCE && e.PBI_QUANTITY_TOLERANCE.split(" ")) || [];
                    var prefix = factor.length && factor[0].trim();
                    var suffix = factor.length && factor[1].trim();
                    return {
                        PBI_FK_MATERIAL: e.PBI_FK_MATERIAL,
                        PBI_QUANTITY_MIN: e.PBI_QUANTITY_MIN,
                        PBI_QUANTITY_MAX: e.PBI_QUANTITY_MAX,
                        SHOW_CONVERT_TO_FOB: e.SHOW_CONVERT_TO_FOB,
                        PBI_MARKET_PRICE: e.PBI_MARKET_PRICE,
                        PBI_LATEST_LP_PLAN_PRICE_FIXED: e.PBI_LATEST_LP_PLAN_PRICE_FIXED,
                        PBI_BENCHMARK_PRICE: e.PBI_BENCHMARK_PRICE,
                        PBI_QUANTITY_UNIT: e.PBI_QUANTITY_UNIT,
                        PBI_AWARDED_QUANTITY_MIN: e.PBI_AWARDED_QUANTITY_MIN,
                        PBI_AWARDED_QUANTITY_MAX: e.PBI_AWARDED_QUANTITY_MAX,
                        PBI_QUANTITY_TOLERANCE: e.PBI_QUANTITY_TOLERANCE,
                        PBI_QUANTITY_TOLERANCE_PREFIX: prefix,
                        PBI_QUANTITY_TOLERANCE_SUFFIX: suffix,
                        PBI_QUANTITY_TOLERANCE_OPTION: e.PBI_QUANTITY_TOLERANCE_OPTION || "",
                    };
                }),
            };
            var rounds = grouped[keys[i]][0].PAF_ROUND;
            for (var k = 0; k < rounds.length; ++k) {
                for (var l = 0; l < count_product; ++l) {
                    item.PAF_ROUND[k] = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]({}, item.PAF_ROUND[k]);
                    item.PAF_ROUND[k].PRODUCTS = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], item.PAF_ROUND[k].PRODUCTS);
                    item.PAF_ROUND[k].PRODUCTS[l] = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]({}, item.PAF_ROUND[k].PRODUCTS[l]);
                    item.PAF_ROUND[k].PRODUCTS[l].PRN_FIXED = grouped[keys[i]][l].PAF_ROUND[k].PRN_FIXED;
                }
            }
            NEW_PAF_BIDDING_ITEMS.push(__WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(item));
        }
        console.log("grouped", grouped);
        console.log("NEW_PAF_BIDDING_ITEMS", NEW_PAF_BIDDING_ITEMS);
        console.log("NEW_PAF_BIDDING_ITEMS", NEW_PAF_BIDDING_ITEMS);
        // new_criteria.PAF_BIDDING_ITEMS =
        // return new_criteria;
        new_criteria.PAF_BIDDING_ITEMS = NEW_PAF_BIDDING_ITEMS;
        new_criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](new_criteria.PAF_BIDDING_ITEMS, function (e) {
            e.IS_FOB = e.PBI_INCOTERM == 'FOB';
            var last_round = __WEBPACK_IMPORTED_MODULE_1_lodash__["last"](e.PAF_ROUND);
            for (var i = 0; i < e.PRODUCTS.length; i++) {
                if (e.IS_FOB) {
                    e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED;
                }
                else {
                    e.PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round.PRODUCTS[i].PRN_FIXED - e.PBI_FREIGHT_PRICE;
                }
            }
            return e;
        });
        return new_criteria;
    };
    PAFStore.UpdateSELECTED = function (PAF_BIDDING_ITEMS, PAF_PAYMENT_ITEMS) {
        PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](PAF_BIDDING_ITEMS, function (e) {
            e.SELECTED = __WEBPACK_IMPORTED_MODULE_1_lodash__["findIndex"](PAF_PAYMENT_ITEMS, function (ee) {
                return ee.PPI_FK_CUSTOMER == e.PBI_FK_CUSTOMER;
            }) != -1;
            return e;
        });
        return PAF_BIDDING_ITEMS;
    };
    PAFStore.NormalizeQuantity = function (value, quantity_unit, density) {
        // console.log(value, quantity_unit, density)
        density = +density || 0;
        value = +value || 0;
        var result = 0;
        // console.log("bank_debug",quantity_unit )
        switch (quantity_unit) {
            case "KT":
                result = (value * 1000 * 6.29) / density;
                break;
            case "KBBL":
                result = (value * 1000);
                break;
            case "MML":
                result = (value) / 0.158984;
                break;
            default:
                // code...
                break;
        }
        // console.log(`density`, density);
        // console.log(`NormalizeQuantity`, quantity_unit ,value, result, density);
        return result || 0;
    };
    PAFStore.CalculateBetterThanMarketPrice = function (materials, model) {
        return 0;
    };
    PAFStore.CalculateBetterThanLPPlanPrice = function (materials, model) {
        return 0;
    };
    PAFStore.NormalizeModelCMLAIMPORT = function (model) {
        model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_BIDDING_ITEMS, function (e) {
            e.PBI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(e.PBI_FK_CUSTOMER);
            e.PBI_FK_CUSTOMER = null;
            return e;
        });
        model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_PAYMENT_ITEMS, function (e) {
            e.PPI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(e.PPI_FK_CUSTOMER);
            e.PPI_FK_CUSTOMER = null;
            return e;
        });
        model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_PROPOSAL_ITEMS, function (e) {
            e.PSI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(e.PSI_FK_CUSTOMER);
            e.PSI_FK_CUSTOMER = null;
            return e;
        });
        return model;
    };
    PAFStore.LoadModelCMLA = function (model) {
        model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_BIDDING_ITEMS, function (e) {
            e.PBI_FK_CUSTOMER = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(e.PBI_FK_VENDOR);
            e.PBI_FK_VENDOR = null;
            return e;
        });
        model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_PAYMENT_ITEMS, function (e) {
            e.PPI_FK_CUSTOMER = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(e.PPI_FK_VENDOR);
            e.PPI_FK_VENDOR = null;
            return e;
        });
        model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.PAF_PROPOSAL_ITEMS, function (e) {
            e.PSI_FK_CUSTOMER = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(e.PSI_FK_VENDOR);
            e.PSI_FK_VENDOR = null;
            return e;
        });
        return model;
    };
    PAFStore.Form2BenchMark = function (PAF_BIDDING_ITEMS) {
        console.log(PAF_BIDDING_ITEMS);
        if (PAF_BIDDING_ITEMS && PAF_BIDDING_ITEMS.length > 0 && PAF_BIDDING_ITEMS && PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK && PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK.length > 0) {
            for (var i = 0; i < PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK.length; ++i) {
                var source = PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[i];
                for (var j = 0; j < PAF_BIDDING_ITEMS.length; ++j) {
                    // console.log(j, i)
                    if (j == 0) {
                        continue;
                    }
                    PAF_BIDDING_ITEMS[j].PAF_BIDDING_ITEMS_BENCHMARK[i] = __WEBPACK_IMPORTED_MODULE_2____["a" /* Utility */].clone(source);
                }
                // PAF_BIDDING_ITEMS = _.map(PAF_BIDDING_ITEMS, (e) => {
                //   e.PAF_BIDDING_ITEMS_BENCHMARK[i] = Utility.clone(source);
                //   return e;
                // });
            }
            // _.each(PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK, (PAF_BIDDING_ITEMS_BENCHMARK, i) => {
            //   let source = Utility.clone(PAF_BIDDING_ITEMS_BENCHMARK);
            // });
        }
        return PAF_BIDDING_ITEMS;
    };
    return PAFStore;
}());

PAFStore.master = {};
//# sourceMappingURL=paf-store.js.map

/***/ }),

/***/ "../../../../../src/app/common/permission-store.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PermissionStore; });

var PermissionStore = (function () {
    function PermissionStore() {
    }
    // public static IsCreator() : boolean {
    //   return !PermissionStore.IsBothRoles() && PermissionStore.IsRole('creator');
    // }
    // public static IsApprover() : boolean {
    //   return !PermissionStore.IsBothRoles() && PermissionStore.IsRole('approver');
    // }
    // public static IsBothRoles() : boolean {
    //   return PermissionStore.IsRole('approver') && PermissionStore.IsRole('creator');
    // }
    // public static IsAdmin() : boolean {
    //   return PermissionStore.IsRole('admin');
    // }
    // public static GetRole() : [string] {
    //   const roles_raw: string = localStorage.getItem('roles') || "";
    //   const roles: any = roles_raw.split(',');
    //   return roles;
    // }
    // public static IsRole(target: string) : boolean {
    //   let result: boolean = false;
    //   let roles: any = PermissionStore.GetRole();
    //   switch (target) {
    //     case "creator":
    //     case "creater":
    //     case "is_creater":
    //       result = _.includes(roles, 'Procurement User');
    //       break;
    //     case "approver":
    //     case "is_approver":
    //       result = _.includes(roles, 'Procurement Approver');
    //       break;
    //     case "admin":
    //       result = _.includes(roles, 'Admin');
    //       break;
    //     default:
    //       break;
    //   }
    //   return result;
    // }
    // public static IsDraft(model: any) :boolean {
    //   return model.STATE == 'DRAFT';
    // }
    // public static IsNoState(model: any) :boolean {
    //   return model.STATE == '' || model.STATE == null;
    // }
    // public static IsDenied(model: any) :boolean {
    //   return model.STATE == 'Denied';
    // }
    // public static IsRevising(model: any) :boolean {
    //   return model.STATE == 'Revising';
    // }
    // public static IsWaitingForAprove(model: any) : boolean {
    //   return model.STATE == 'Waiting for Approval';
    // }
    // public static IsCurrentUser(model: any) : boolean {
    //   let result = true;
    //   let username = localStorage.getItem('current_user');
    //   let select_history = _.find(model.FLOW_HISTORY, (e) => {
    //     e.UPDATED_BY = e.UPDATED_BY || "";
    //     return e.UPDATED_BY.toLowerCase() == username.toLowerCase();
    //   });
    //   console.log('select_history', select_history);
    //   if (select_history) {
    //     result = result && false;
    //   }
    //   return result;
    // }
    // public static IsAbleToComment(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsRole('approver');
    //   result = result && PermissionStore.IsWaitingForAprove(model);
    //   result = result && PermissionStore.IsCurrentUser(model);
    //   result = result && !PermissionStore.IsDocumentOwner(model);
    //   return result;
    // }
    // public static IsAbleToViewComment(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsDisabled(model);
    //   result = result || PermissionStore.IsRevising(model);
    //   return result;
    // }
    // public static IsDisabled(model: any) : boolean {
    //   // !(model.STATE == 'DRAFT' || model.STATE == 'Denied' || model.STATE == 'Revising' || model.STATE == '' ||  model.STATE == null );
    //   let result = true;
    //   result = result && !PermissionStore.IsDraft(model);
    //   result = result && !PermissionStore.IsRevising(model);
    //   // result = result && PermissionStore.IsDenied(model);
    //   // result = result && PermissionStore.IsWaitingForAprove(model);
    //   result = result && !PermissionStore.IsNoState(model);
    //   return result;
    // }
    // public static IsAbleToApproveOrReject(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsDisabled(model);
    //   result = result && PermissionStore.IsAbleToComment(model);
    //   return result;
    // }
    // public static IsDocumentOwner(model: any) : boolean {
    //   let username = localStorage.getItem('current_user');
    //   return model.CREATED_BY == username;
    // }
    // public static IsAbleToEdit(model: any) : boolean {
    //   let result = true;
    //   result = result && PermissionStore.IsDisabled(model);
    //   result = result && PermissionStore.IsDocumentOwner(model);
    //   result = result && !PermissionStore.IsWaitingForAprove(model);
    //   return result;
    // }
    PermissionStore.CheckButtonText = function (button_list, target) {
        var index = __WEBPACK_IMPORTED_MODULE_0_lodash__["findIndex"](button_list, function (e) {
            return e.name == target;
        });
        return index != -1;
    };
    PermissionStore.IsAbleToSaveDraft = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'SAVE DRAFT');
        // if(!button_list || button_list.length == 0) {
        //   return true;
        // }
        return result;
    };
    PermissionStore.IsAbleToSubmit = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'SUBMIT');
        // result = result || PermissionStore.IsAbleToSaveDraft(button_list);
        return result;
    };
    PermissionStore.IsAbleToVerify = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'VERIFY');
        return result;
    };
    PermissionStore.IsAbleToEndorse = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'ENDORSE');
        return result;
    };
    PermissionStore.IsAbleToApprove = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'APPROVE');
        return result;
    };
    PermissionStore.IsAbleToCancel = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'CANCEL');
        return result;
    };
    PermissionStore.IsAbleToReject = function (button_list) {
        var result = true;
        result = result && PermissionStore.CheckButtonText(button_list, 'REJECT');
        return result;
    };
    PermissionStore.IsAbleToGenPDF = function (button_list) {
        var result = true;
        result = result && !PermissionStore.CheckButtonText(button_list, 'NO_PDF');
        return result;
    };
    return PermissionStore;
}());

//# sourceMappingURL=permission-store.js.map

/***/ }),

/***/ "../../../../../src/app/common/utility.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* unused harmony reexport DateHelper */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Utility; });




var Utility = (function () {
    function Utility() {
    }
    Utility.IsRevising = function (model) {
        var result = false;
        result = result || model.STATE == 'Revising';
        return result;
    };
    Utility.IsDenied = function (model) {
        var result = model.STATE == 'Denied';
        result = result || model.STATE == 'Revising';
        return result;
    };
    Utility.IsDone = function (model) {
        var result = model.STATE == 'Approved';
        result = result || model.STATE == 'Cancelled';
        result = result || model.STATE == 'Denied';
        return result;
    };
    Utility.IsShowComment = function (model, IS_EXECUTIVE, IS_VIEW) {
        var result = IS_EXECUTIVE;
        result = result && !Utility.IsDone(model);
        result = result && !Utility.IsDenied(model);
        var username = localStorage.getItem('current_user');
        var select_history = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](model.FLOW_HISTORY, function (e) {
            e.UPDATED_BY = e.UPDATED_BY || "";
            return e.UPDATED_BY.toLowerCase() == username.toLowerCase();
        });
        if (select_history && select_history.length > 0) {
            result = result && false;
        }
        return result;
    };
    Utility.GetToDo = function (value, criteria) {
        return __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](value, function (elem) {
            var ft = elem.FLOW_TRANSACTION && elem.FLOW_TRANSACTION.length > 0 && elem.FLOW_TRANSACTION[0];
            var current_user = ft.CUSTOM_FIELD;
            if (!Utility.ISCurrentID(current_user)) {
                return false;
            }
            // const fth = Utility.GetOrderedFlowHistoryIncludedSubmit(elem.FLOW_TRANSACTION);
            // const cc = _.filter(fth, (e) => {
            //   let re = e.CUSTOM_FIELD;
            //   return Utility.ISCurrentID(re);
            // });
            // console.log(cc);
            // if(cc.length > 0) {
            //   return false;
            // }
            return (criteria.indexOf(elem.STATE) > -1);
        });
    };
    Utility.NormalizeFlowTransaction = function (old_trans) {
        var select_flow = old_trans && old_trans.length > 0 && old_trans[0];
        var result = [];
        var trans = Utility.clone(select_flow);
        var history_trans = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], trans.FLOW_TRANSACTION_HISTORY);
        for (var i = 0; i < history_trans.length; ++i) {
            if (history_trans[i].ACTION == "Rejected") {
                result = [];
            }
            else {
                result.push(history_trans[i]);
            }
        }
        return result;
    };
    Utility.GetOrderedFlowHistoryIncludedSubmit = function (old_trans) {
        var select_flow = old_trans && old_trans.length > 0 && old_trans[0];
        var trans = Utility.clone(select_flow);
        var history_trans = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], trans.FLOW_TRANSACTION_HISTORY);
        history_trans.push(trans);
        // history_trans = _.sortBy(history_trans, (o) => { return moment(o.UPDATED_DATE).valueOf(); }).reverse();
        history_trans = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](history_trans, function (e) {
            e.orderDate = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(e.UPDATED_DATE).valueOf();
            return e;
        });
        // history_trans = _.sortBy(history_trans, (o) => { return o.orderDate });
        history_trans = __WEBPACK_IMPORTED_MODULE_2_lodash__["orderBy"](history_trans, ['orderDate'], ['asc']);
        // console.log('history_trans', history_trans);
        // history_trans = _.reject(history_trans, (e) => {
        //   return e.ACTION == "Submit";
        // })
        // console.log(`history_tranz`, history_trans);
        return history_trans;
    };
    Utility.GetOrderedFlowHistory = function (old_trans) {
        // const select_flow = old_trans && old_trans.length > 0 && old_trans[0];
        // const trans = Utility.clone(select_flow);
        // let history_trans = _.extend([], trans.FLOW_TRANSACTION_HISTORY);
        // history_trans.push(trans);
        // // history_trans = _.sortBy(history_trans, (o) => { return moment(o.UPDATED_DATE).valueOf(); }).reverse();
        // history_trans = _.sortBy(history_trans, (o) => { return moment(o.UPDATED_DATE).valueOf(); });
        var history_trans = Utility.GetOrderedFlowHistoryIncludedSubmit(old_trans);
        history_trans = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](history_trans, function (e) {
            return e.ACTION == "Submit";
        });
        // console.log(`history_tranz`, history_trans);
        return history_trans;
    };
    Utility.UpdatRejectedTransactions = function (old_trans) {
        var result = [];
        var history_trans = Utility.GetOrderedFlowHistory(old_trans);
        for (var i = 0; i < history_trans.length; ++i) {
            var e = {
                UPDATED_BY: history_trans[i].UPDATED_BY,
                REJECTED_REASON: null,
                COMMENT: history_trans[i].COMMENT,
                UPDATED_DATE: history_trans[i].UPDATED_DATE,
                ACTION: history_trans[i].ACTION,
            };
            result.push(e);
            if (history_trans[i].ACTION == "Rejected") {
                break;
            }
        }
        var cross_check = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](result, ['ACTION', 'Rejected']);
        if (!cross_check) {
            return [];
        }
        return result;
    };
    Utility.UpdateTransactions = function (old_trans) {
        var history_trans = Utility.GetOrderedFlowHistory(old_trans);
        var result = [];
        for (var i = 0; i < history_trans.length; ++i) {
            var e = {
                UPDATED_BY: history_trans[i].UPDATED_BY,
                REJECTED_REASON: null,
                // REJECTED_REASON: history_trans[i].REJECTED_REASON,
                COMMENT: history_trans[i].COMMENT,
                UPDATED_DATE: history_trans[i].UPDATED_DATE,
                ACTION: history_trans[i].ACTION,
            };
            result.push(e);
            if (history_trans[i].ACTION == "Rejected") {
                result = [];
            }
        }
        return result;
    };
    Utility.NormalizeApproverList = function (approver_list) {
        // let result = [];
        // const result = _.map(approver_list, (elem) => {
        //   return elem.SHOW_STATUS == "Rejected";
        // });
        var state_before = "";
        for (var i = 0; i < approver_list.length; ++i) {
            // console.log(state_before);
            if (state_before == "CURRENT" || state_before == null) {
                approver_list[i].SHOW_STATUS = null;
            }
            state_before = approver_list[i].SHOW_STATUS;
        }
        // if(filtered.length > 0) {
        //   return _.map(approver_list, (elem) => {
        //     elem.SHOW_STATUS = null;
        //     return elem;
        //   });
        // }
        return approver_list;
    };
    Utility.ISCurrentID = function (target) {
        var user = localStorage.getItem('current_user');
        // console.log(user);
        target = target || "";
        user = user || "";
        return __WEBPACK_IMPORTED_MODULE_2_lodash__["includes"](target.toLowerCase(), user.toLowerCase());
        // return target.toLowerCase() == user.toLowerCase()
    };
    Utility.GetFileUrl = function (path) {
        path = path || '';
        path = path.replace('~', '');
        return __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* BASE_API */] + "../" + path;
    };
    Utility.stringToNumber = function (a) {
        a = a || 0;
        return +a;
    };
    Utility.NormalizePRType = function (pr_type) {
        if (pr_type.toLowerCase() == "material") {
            return "Material";
        }
        if (pr_type.toLowerCase() == "service") {
            return "Service";
        }
        if (pr_type.toLowerCase() == "type1" || pr_type.toLowerCase() == "type2") {
            return "Material";
        }
        else {
            return "Service";
        }
    };
    Utility.GetDownloadBlobUrl = function (blob_id) {
        if (!blob_id) {
            return null;
        }
        return __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* BASE_API */] + "/blob/download/" + blob_id;
    };
    Utility.GetBlobUrl = function (blob_id) {
        if (!blob_id) {
            return null;
        }
        if (blob_id.startsWith('~/Upload')) {
            return null;
        }
        return __WEBPACK_IMPORTED_MODULE_0__constant__["a" /* BASE_API */] + "/blob/render/" + blob_id;
    };
    Utility.GetImageUrl = function (blob_id) {
        return Utility.GetBlobUrl(blob_id);
    };
    Utility.GetThumbnail = function (blobs) {
        blobs = blobs || [];
        var blob_id = blobs && blobs.length && blobs[0];
        return Utility.GetBlobUrl(blob_id) || 'http://placehold.it/500x500/c6d500/fff';
    };
    Utility.clone = function (source) {
        if (!source) {
            return null;
        }
        return JSON.parse(JSON.stringify(source));
    };
    Utility.NormalizeStatus = function (status) {
        status = status.toString();
        // console.log(status)
        switch (status) {
            case "1":
                status = 'enabled';
                break;
            case "0":
                status = 'disabled';
                break;
            default:
                // code...
                break;
        }
        return status;
    };
    Utility.GetStatus = function (status) {
        status = Utility.NormalizeStatus(status);
        var html = '';
        switch (status) {
            case "enabled":
                html = '<span class="label label-fit label-success">Enabled</span>';
                break;
            case "disabled":
                html = '<span class="label label-fit label-default">Disabled</span>';
                break;
            default:
                // code...
                break;
        }
        return html;
    };
    Utility.getValueByID = function (id, key, master) {
        var s = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](master, function (elem) {
            return elem.ID == id;
        });
        return s && s[key];
    };
    Utility.getTextByID = function (id, master) {
        var s = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](master, function (elem) {
            return elem.ID == id;
        });
        return s && s.VALUE;
    };
    Utility.NormalizeDate = function (date) {
        var formated_date = __WEBPACK_IMPORTED_MODULE_1_moment_moment__(date).format("DD/MM/YYYY");
        console.log('NormalizeDate', date, formated_date);
        if (formated_date == 'Invalid date') {
            return '';
        }
        else {
            return formated_date;
        }
    };
    Utility.NormalizeModel = function (target_model, master) {
        var model = Utility.clone(target_model);
        model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](target_model.PAF_BIDDING_ITEMS, function (e) {
            e.PBI_QUANTITY_FLOAT = e.PBI_QUANTITY_FLOAT && e.PBI_QUANTITY_FLOAT.replace('?', '+/-');
            e.PBI_QUANTITY_FLOAT = e.PBI_QUANTITY_FLOAT && e.PBI_QUANTITY_FLOAT.replace('±', '+/-');
            e.PRD_EXISTING_QTY_FLOAT = e.PRD_EXISTING_QTY_FLOAT && e.PRD_EXISTING_QTY_FLOAT.replace('?', '+/-');
            e.PRD_EXISTING_QTY_FLOAT = e.PRD_EXISTING_QTY_FLOAT && e.PRD_EXISTING_QTY_FLOAT.replace('±', '+/-');
            e.PRD_CUS_PROPOSAL_QTY_FLOAT = e.PRD_CUS_PROPOSAL_QTY_FLOAT && e.PRD_CUS_PROPOSAL_QTY_FLOAT.replace('?', '+/-');
            e.PRD_CUS_PROPOSAL_QTY_FLOAT = e.PRD_CUS_PROPOSAL_QTY_FLOAT && e.PRD_CUS_PROPOSAL_QTY_FLOAT.replace('±', '+/-');
            e.PRD_OUR_PROPOSAL_QTY_FLOAT = e.PRD_OUR_PROPOSAL_QTY_FLOAT && e.PRD_OUR_PROPOSAL_QTY_FLOAT.replace('?', '+/-');
            e.PRD_OUR_PROPOSAL_QTY_FLOAT = e.PRD_OUR_PROPOSAL_QTY_FLOAT && e.PRD_OUR_PROPOSAL_QTY_FLOAT.replace('±', '+/-');
            return e;
        });
        return model;
    };
    Utility.DeNormalizeModel = function (target_model, master) {
        var model = Utility.clone(target_model);
        return model;
    };
    Utility.FillArray = function (arr, val, length) {
        arr = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], arr);
        var oldLength = arr.length;
        arr.length = length;
        return __WEBPACK_IMPORTED_MODULE_2_lodash__["fill"](arr, val, oldLength);
    };
    Utility.AdjustPAFModel = function (model) {
        var result = Utility.clone(model);
        result.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](result.PAF_PAYMENT_ITEMS, function (e) {
            e.PAF_PAYMENT_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](e.PAF_PAYMENT_ITEMS_DETAIL, function (f) {
                return f.PPD_PAYMENT_TYPE;
            });
            return e;
        });
        result.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](result.PAF_PROPOSAL_ITEMS, function (e) {
            e.PAF_PROPOSAL_REASON_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](e.PAF_PROPOSAL_REASON_ITEMS, function (f) {
                return f.PSR_REASON;
            });
            e.PAF_PROPOSAL_OTC_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](e.PAF_PROPOSAL_OTC_ITEMS, function (f) {
                return f.PSO_OTHER_TERM_COND;
            });
            return e;
        });
        return result;
    };
    Utility.GetRoundText = function (num) {
        num = num + "";
        var result = "";
        switch (num) {
            case "1":
                result = "1st";
                break;
            case "2":
                result = "2nd";
                break;
            case "3":
                result = "3rd";
                break;
            default:
                result = num + "th";
                break;
        }
        return result;
    };
    return Utility;
}());

//# sourceMappingURL=utility.js.map

/***/ }),

/***/ "../../../../../src/app/common/validate-helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ValidateHelper */
var ValidateHelper = (function () {
    function ValidateHelper() {
    }
    ValidateHelper.empty = function (s) {
        console.log(s);
        if (!s) {
            return false;
        }
        return true;
    };
    ValidateHelper.BasicInfo = function (model) {
        var validated = true;
        validated = validated && ValidateHelper.empty(model.COMPANY_CODE);
        validated = validated && ValidateHelper.empty(model.SUBJECT);
        validated = validated && ValidateHelper.empty(model.CUSTODIAN);
        validated = validated && ValidateHelper.empty(model.DELIVERY_DATE);
        console.log("Validate BasicInfo:", validated);
        return validated && this.AdvanceInfo(model);
    };
    ValidateHelper.AdvanceInfo = function (model) {
        var validated = true;
        if (model.IS_CHANGE_REQUEST == 'Y') {
            validated = validated && ValidateHelper.MemoList(model.MEMO_BIDDER);
            validated = validated && ValidateHelper.Awarded(model);
            validated = validated && ValidateHelper.Change(model);
            return validated;
        }
        console.log("model.MEMO_SOURCING_METHOD", model);
        switch (model.MEMO_SOURCING_METHOD) {
            case "1":
                validated = validated && ValidateHelper.SingleSource(model);
                break;
            case "2":
                validated = validated && ValidateHelper.PriceCompare(model);
                break;
            case "3":
                validated = validated && ValidateHelper.Tendering(model);
                break;
            case "4":
                validated = validated && ValidateHelper.Tendering(model);
                break;
            default:
                validated = false;
                break;
        }
        console.log("Validate AdvanceInfo:", validated);
        return validated;
    };
    ValidateHelper.MemoList = function (memo_bidder) {
        var validated = true;
        for (var i = memo_bidder.length - 1; i >= 0; i--) {
            validated = validated && ValidateHelper.empty(memo_bidder[i].BIDDER_NAME);
        }
        return validated;
    };
    ValidateHelper.SingleSource = function (model) {
        var validated = ValidateHelper.empty(model.SINGLE_SOURCE_REASON);
        console.log("Validate SingleSource:", validated);
        validated = validated && ValidateHelper.empty(model.SINGLE_SOURCE_EXPLANATION);
        console.log("Validate SingleSource:", validated);
        validated = validated && ValidateHelper.MemoList(model.MEMO_BIDDER);
        console.log("Validate SingleSource:", validated);
        validated = validated && ValidateHelper.Awarded(model);
        console.log("Validate SingleSource:", validated);
        return validated;
    };
    ValidateHelper.PriceCompare = function (model) {
        var validated = true;
        for (var i = model.MEMO_BIDDER.length - 1; i >= 0; i--) {
            // validated = validated && ValidateHelper.empty(model.MEMO_BIDDER[i].EVALUATED_PRICE)
        }
        validated = validated && ValidateHelper.empty(model.HISTORICAL_PRICE);
        validated = validated && ValidateHelper.MemoList(model.MEMO_BIDDER);
        validated = validated && ValidateHelper.Awarded(model);
        return validated;
    };
    ValidateHelper.Tendering = function (model) {
        var validated = true;
        // validated = validated && ValidateHelper.empty(model.TECHNICAL_ACCEPTANCE_CRITERIA);
        for (var i = model.MEMO_BIDDER.length - 1; i >= 0; i--) {
            // validated = validated && ValidateHelper.empty(model.MEMO_BIDDER[i].EVALUATED_PRICE)
        }
        validated = validated && ValidateHelper.empty(model.HISTORICAL_PRICE);
        validated = validated && ValidateHelper.MemoList(model.MEMO_BIDDER);
        validated = validated && ValidateHelper.Awarded(model);
        validated = validated && ValidateHelper.empty(model.TECHNICAL_ACCEPTANCE_CRITERIA);
        // validated = validated && ValidateHelper.TechnicalScore(model.MEMO_BIDDER);
        console.log("Validate Tendering:", validated);
        if (model.IS_LOWEST_EVALUATED_PRICE == 'N') {
            validated = validated && ValidateHelper.empty(model.EVALUATION_CRITERIA_TECHNICAL);
            validated = validated && ValidateHelper.empty(model.EVALUATION_CRITERIA_COMMERCIAL);
            console.log("Validate Tendering Lowes:", validated);
        }
        return validated;
    };
    ValidateHelper.TechnicalScore = function (MEMO_BIDDER) {
        var validated = true;
        for (var i = MEMO_BIDDER.length - 1; i >= 0; i--) {
            validated = validated && ValidateHelper.empty(MEMO_BIDDER[i].TECHNICAL_SCORE);
        }
        return validated;
    };
    ValidateHelper.Change = function (model) {
        var validated = true;
        validated = validated && ValidateHelper.empty(model.MAIN_CONTRACT_NO);
        validated = validated && ValidateHelper.empty(model.CONTRACT_NAME);
        validated = validated && ValidateHelper.empty(model.ORIGINAL_CONTRACT_VALUE);
        validated = validated && ValidateHelper.empty(model.VARIATION_NO);
        validated = validated && ValidateHelper.empty(model.VARIATION_DESC);
        validated = validated && ValidateHelper.empty(model.CHANGED_REASON);
        validated = validated && ValidateHelper.empty(model.ACCUMULATED_CHANGED_VALUE);
        return validated;
    };
    ValidateHelper.Awarded = function (model) {
        var validated = ValidateHelper.empty(model.AWARDED_BIDDER);
        validated = validated && ValidateHelper.empty(model.INITIAL_PROPOSAL_PRICE);
        validated = validated && ValidateHelper.empty(model.FINAL_NEGOTIATED_PRICE);
        validated = validated && ValidateHelper.empty(model.THB_FINAL_NEGOTIATED_PRICE);
        return validated;
    };
    return ValidateHelper;
}());

//# sourceMappingURL=validate-helper.js.map

/***/ }),

/***/ "../../../../../src/app/components/cmla/cmla-form-one/cmla-calculate-table.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row no-margin\">\n  <div class=\"col-xs-2\">\n    <div class=\"form-group\">\n      <label class=\"col-md-12 control-label\"></label>\n    </div>\n  </div>\n  <div class=\"col-xs-10\">\n    <!-- <pre>{{criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)] | json}}</pre> -->\n    <table class=\"table table-bordered\">\n        <thead>\n            <tr>\n                <th>Product Name</th>\n                <th>Quantity</th>\n                <th>Unit</th>\n                <th *ngIf=\"is_cmla_form_1\">Final Price</th>\n                <th>Better than LP Plan Price (USD/MT)</th>\n                <th *ngIf=\"is_cmla_form_1\">Better than Benchmark Price (USD/MT)</th>\n            </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let p of model.PAF_BIDDING_ITEMS|cmlaCalFilter:proposal.PSI_FK_CUSTOMER ;let i = index;\">\n              <td width=\"15%\"> {{ ShowProductName(p.PBI_FK_MATERIAL) }} </td>\n              <td width=\"15%\" class=\"text-center\">\n                {{ ShowQuantity(p) }}\n              </td>\n              <td width=\"5%\"> {{ p.PBI_QUANTITY_UNIT }} </td>\n              <td *ngIf=\"is_cmla_form_1\" width=\"20%\"> {{ GetLastRound(p) }} </td>\n              <td width=\"20%\"> {{ CalculateBetterThanLPPlanPrice(p) }} </td>\n              <td *ngIf=\"is_cmla_form_1\" width=\"20%\"> {{ CalculateBetterThanBenchmarkPrice(p) }} </td>\n            </tr>\n        </tbody>\n    </table>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/cmla/cmla-form-one/cmla-calculate-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaCalculateTableComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CmlaCalculateTableComponent = (function () {
    // @Input() criteria: any;
    // @Input() title: any;
    function CmlaCalculateTableComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.is_cmla_form_1 = false;
        this.is_cmla_form_2 = false;
        this.is_cmla_import = false;
    }
    CmlaCalculateTableComponent.prototype.ngOnInit = function () {
    };
    CmlaCalculateTableComponent.prototype.ShowProductName = function (id) {
        // console.log(id);
        return __WEBPACK_IMPORTED_MODULE_3__common__["a" /* Utility */].getValueByID(id, 'VALUE', this.master.PRODUCTS);
    };
    CmlaCalculateTableComponent.prototype.GetFactor = function (PAF_ROUND) {
        var last_round = __WEBPACK_IMPORTED_MODULE_2_lodash__["last"](__WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](PAF_ROUND, function (e) {
            return e.PRN_FIXED || e.PRN_FLOAT;
        }));
        // console.log(PAF_ROUND, last_round);
        var fixed = last_round && last_round.PRN_FIXED;
        var float = last_round && last_round.PRN_FLOAT;
        var is_fixed = false;
        if (fixed) {
            is_fixed = true;
        }
        var is_float = false;
        if (float) {
            is_float = true;
        }
        return {
            fixed: fixed,
            float: float,
            is_fixed: is_fixed,
            is_float: is_float,
        };
    };
    CmlaCalculateTableComponent.prototype.GetNumberFromFloat = function (float) {
        var factor = float && float.split('|');
        var prefix = factor && factor.length > 0 && factor[0];
        var signed = factor && factor.length > 1 && factor[1];
        var real_number = factor && factor.length > 2 && factor[2];
        var num = +("" + signed + real_number);
        return [
            prefix,
            num,
        ];
    };
    CmlaCalculateTableComponent.prototype.ShowQuantity = function (p) {
        return (p && p.PBI_QUANTITY_FLOAT && p.PBI_QUANTITY_FLOAT.replace('+/-', '±')) || '';
    };
    CmlaCalculateTableComponent.prototype.GetLastRound = function (p) {
        var _a = this.GetFactor(p.PAF_ROUND), fixed = _a.fixed, float = _a.float, is_fixed = _a.is_fixed, is_float = _a.is_float;
        if (float) {
            var _b = this.GetNumberFromFloat(float), prefix = _b[0], num = _b[1];
            var signed = num >= 0 ? '+' : '-';
            num = Math.abs(num);
            var num_str = this.decimalPipe.transform(num, '1.2-2');
            float = prefix + " " + signed + " " + num;
            // let prefix = float_l && float_l.length > 0 && float_l[0];
            // let signed = float_l && float_l.length > 0 && float_l[0];
        }
        return fixed || float || 0;
    };
    // public CalculateBetterThanLPPlanPrice(index: number) : string {
    CmlaCalculateTableComponent.prototype.CalculateBetterThanLPPlanPrice = function (p) {
        // console.log(p)
        var _a = this.GetFactor(p.PAF_ROUND), fixed = _a.fixed, float = _a.float, is_fixed = _a.is_fixed, is_float = _a.is_float;
        var lp_float = p.PBI_LATEST_LP_PLAN_PRICE_FLOAT;
        var lp_fixed = p.PBI_LATEST_LP_PLAN_PRICE_FIXED;
        // console.log(fixed, float, is_fixed, is_float, lp_float)
        if (is_fixed && is_float) {
            return 'n/a';
        }
        // if (lp_fixed && float) {
        //   return 'n/a';
        // } else if (lp_float && fixed) {
        //   return 'n/a';
        // }
        if (float) {
            var signed = '+';
            var _b = this.GetNumberFromFloat(float), round_prefix = _b[0], round_num = _b[1];
            var _c = this.GetNumberFromFloat(lp_float), lp_prefix = _c[0], lp_num = _c[1];
            if (lp_prefix != round_prefix) {
                return 'n/a';
            }
            var final_num = +round_num - +lp_num;
            if (final_num < 0) {
                // final_num *= -1;
                signed = '-';
            }
            var final_num_str = this.decimalPipe.transform(final_num, '1.2-2');
            return "" + final_num_str;
            // return `${round_prefix} ${signed} ${final_num_str}`;
        }
        else {
            // console.log(+fixed, +lp_fixed, +fixed - +lp_fixed);
            var final_num = +fixed - +lp_fixed;
            var final_num_str = this.decimalPipe.transform(final_num, '1.2-2');
            return "" + final_num_str;
        }
    };
    CmlaCalculateTableComponent.prototype.CalculateBetterThanBenchmarkPrice = function (p) {
        var _a = this.GetFactor(p.PAF_ROUND), fixed = _a.fixed, float = _a.float, is_fixed = _a.is_fixed, is_float = _a.is_float;
        var bm_float = p.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE;
        // console.log(fixed, float, is_fixed, is_float, bm_float)
        if (is_fixed && is_float) {
            return 'n/a';
        }
        if (is_float) {
            return 'n/a';
        }
        var final_num = +fixed - +bm_float;
        var final_num_str = this.decimalPipe.transform(final_num, '1.2-2');
        return "" + final_num_str;
    };
    return CmlaCalculateTableComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaCalculateTableComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaCalculateTableComponent.prototype, "master", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaCalculateTableComponent.prototype, "proposal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], CmlaCalculateTableComponent.prototype, "is_cmla_form_1", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], CmlaCalculateTableComponent.prototype, "is_cmla_form_2", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], CmlaCalculateTableComponent.prototype, "is_cmla_import", void 0);
CmlaCalculateTableComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-cmla-calculate-table',
        template: __webpack_require__("../../../../../src/app/components/cmla/cmla-form-one/cmla-calculate-table.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */]) === "function" && _a || Object])
], CmlaCalculateTableComponent);

var _a;
//# sourceMappingURL=cmla-calculate-table.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/cmla/cmla-form-one/cmla-form-one.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model.PAF_BIDDING_ITEMS | json }}</pre> -->\n<div class=\"row\" *ngIf=\"(is_cmla_form_1 || is_cmla_form_2)\">\n  <div class=\"col-xs-12\">\n    <div class=\"checkbox\">\n      <input\n        id=\"PDA_INCLUDED_REVIEW_TABLE\"\n        type=\"checkbox\"\n        [disabled]=\"model.IS_DISABLED\"\n        [ngModel]=\"model.PDA_INCLUDED_REVIEW_TABLE == 'Y'\"\n        (ngModelChange)=\"model.PDA_INCLUDED_REVIEW_TABLE = $event ? 'Y': 'N'; HandleModelUpdate()\"\n      >\n      <!-- <input id=\"PDA_INCLUDED_REVIEW_TABLE\" type=\"checkbox\" [(ngModel)]=\"model.PDA_INCLUDED_REVIEW_TABLE\"> -->\n      <label for=\"PDA_INCLUDED_REVIEW_TABLE\">\n        Applied for \"Customers request to review the existing price\"\n      </label>\n    </div>\n  </div>\n</div>\n<ng-template [ngIf]=\"(is_cmla_form_1 || is_cmla_form_2) && model.PDA_INCLUDED_REVIEW_TABLE == 'Y'\">\n  <div class=\"row\">\n    <div class=\"col-xs-12\">\n      <div class=\"table-responsive\">\n        <table id=\"\" class=\"table table-bordered plutonyx-table\">\n          <thead>\n            <tr>\n              <th rowspan=\"4\"> Customer </th>\n              <th rowspan=\"4\">Contact Person</th>\n              <th rowspan=\"4\">Product</th>\n              <th rowspan=\"4\">Unit</th>\n              <th colspan=\"9\">\n                Contract\n                <div style=\"display: inline-block; min-width: 100px; margin-left: 10px;\">\n                  <ptx-sugester\n                    [model]=\"model.PDA_PRICE_UNIT\"\n                    [data_list]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    [select_field]=\"'ID'\"\n                    (ModelChanged)=\"model.PDA_PRICE_UNIT = $event;  HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </div>\n              </th>\n              <th rowspan=\"4\" width=\"50px\">Note</th>\n              <th rowspan=\"4\" width=\"50px\"></th>\n            </tr>\n            <tr>\n              <th colspan=\"3\">Existing</th>\n              <th colspan=\"3\">Customer Proposal</th>\n              <th colspan=\"3\">Our Proposal</th>\n            </tr>\n            <tr>\n              <th width=\"25%\" rowspan=\"2\">Quantity</th>\n              <th nowrap colspan=\"2\">Price or Formula</th>\n              <th width=\"25%\" rowspan=\"2\">Quantity</th>\n              <th nowrap colspan=\"2\">Price or Formula</th>\n              <th width=\"25%\" rowspan=\"2\">Quantity</th>\n              <th nowrap colspan=\"2\">Price or Formula</th>\n            </tr>\n            <tr>\n              <th>Float</th>\n              <th>Fixed</th>\n              <th>Float</th>\n              <th>Fixed</th>\n              <th>Float</th>\n              <th>Fixed</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngIf=\"model.PAF_REVIEW_ITEMS.length == 0\">\n              <td colspan=\"14\" class=\"text-center\">No Data</td>\n            </tr>\n            <ng-template ngFor let-row [ngForOf]=\"model.PAF_REVIEW_ITEMS\" let-i=\"index\">\n              <tr>\n                <td class=\"min-300\">\n                  <ptx-sugester\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_CUSTOMER\"\n                    [data_list]=\"master.CUSTOMERS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    [select_field]=\"'ID'\"\n                    (ModelChanged)=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_CUSTOMER = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    type=\"text\"\n                    class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PRI_CONTACT_PERSON\"\n                    (ngModelChange)=\"HandleModelUpdate();\"\n                  />\n                </td>\n                <td class=\"min-200\">\n                  <ptx-sugester\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_MATERIAL\"\n                    [data_list]=\"master.PRODUCTS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_MATERIAL = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </td>\n                <td class=\"min-100\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_REVIEW_ITEMS[i].PRI_QUANTITY_UNIT\"\n                      [data_list]=\"master.QUANTITY_UNITS\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"model.PAF_REVIEW_ITEMS[i].PRI_QUANTITY_UNIT = $event; HandleModelUpdate();\"\n                    >\n                    </ptx-sugester>\n                </td>\n                <td class=\"min-400\">\n                  <ptx-input-quantity\n                    [disabled]=\"model.IS_DISABLED\"\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_EXISTING_QTY_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_EXISTING_QTY_FLOAT = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-input-quantity>\n                </td>\n                <td class=\"min-400\">\n                  <ptx-input-float\n                    [class_name]=\"'ptx-w-200'\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [data_list]=\"master.FORMULA\"\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_EXISTING_PRICE_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_EXISTING_PRICE_FLOAT = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-input-float>\n                </td>\n                <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_EXISTING_PRICE_FIXED\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                </td>\n                <td class=\"min-300\">\n                  <ptx-input-quantity\n                    [disabled]=\"model.IS_DISABLED\"\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_CUS_PROPOSAL_QTY_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_CUS_PROPOSAL_QTY_FLOAT = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-input-quantity>\n                </td>\n                <td class=\"min-400\">\n                  <ptx-input-float\n                    [class_name]=\"'ptx-w-200'\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [data_list]=\"master.FORMULA\"\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_CUS_PROPOSAL_PRICE_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_CUS_PROPOSAL_PRICE_FLOAT = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-input-float>\n                </td>\n                <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_CUS_PROPOSAL_PRICE_FIXED\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                </td>\n                <td class=\"min-300\">\n                  <ptx-input-quantity\n                    [disabled]=\"model.IS_DISABLED\"\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_OUR_PROPOSAL_QTY_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_OUR_PROPOSAL_QTY_FLOAT = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-input-quantity>\n                </td>\n                <td class=\"min-400\">\n                  <ptx-input-float\n                    [class_name]=\"'ptx-w-200'\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [data_list]=\"master.FORMULA\"\n                    [model]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_OUR_PROPOSAL_PRICE_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_OUR_PROPOSAL_PRICE_FLOAT = $event; HandleModelUpdate();\"\n                  >\n                  </ptx-input-float>\n                </td>\n                <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_OUR_PROPOSAL_PRICE_FIXED\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                </td>\n                <td class=\"min-200\">\n                  <input type=\"text\"\n                    class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[0].PRD_NOTE\"\n                  />\n                </td>\n                <td>\n                  <a (click)=\"RemoveCustomerRequest.emit(row)\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n                </td>\n              </tr>\n            </ng-template>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <button class=\"btn btn-success\" (click)=\"AddCustomerRequest.emit()\"> Add Customer</button>\n    </div>\n  </div>\n  <br/>\n</ng-template>\n<div class=\"row\">\n  <div class=\"col-xs-12\">\n    <div class=\"checkbox\" *ngIf=\"!is_cmla_import\">\n      <input\n\n        id=\"PDA_INCLUDED_BIDDING_TABLE\"\n        type=\"checkbox\"\n        [disabled]=\"is_cmla_import || model.IS_DISABLED\"\n        [ngModel]=\"model.PDA_INCLUDED_BIDDING_TABLE == 'Y'\"\n        (ngModelChange)=\"model.PDA_INCLUDED_BIDDING_TABLE = $event ? 'Y': 'N'\"\n      >\n      <label for=\"PDA_INCLUDED_BIDDING_TABLE\">\n         Applied for \"Product price bidding\"\n      </label>\n    </div>\n    <h5 class=\"second-title\" *ngIf=\"is_cmla_import\">Applied for \"Product price bidding\"</h5>\n  </div>\n</div>\n<!-- <pre>{{ model.PAF_BIDDING_ITEMS | json }}</pre> -->\n<ng-template [ngIf]=\"true && model.PDA_INCLUDED_BIDDING_TABLE == 'Y'\">\n  <div class=\"row\">\n    <div class=\"col-xs-12\">\n      <div class=\"table-responsive\">\n        <table id=\"\" class=\"table table-bordered plutonyx-table\">\n          <thead>\n            <tr>\n              <th rowspan=\"3\"></th>\n              <th rowspan=\"3\" *ngIf=\"!is_cmla_import\"> Customer </th>\n              <th rowspan=\"3\" *ngIf=\"is_cmla_import\"> Supplier </th>\n              <th rowspan=\"3\">Contact Person</th>\n              <th rowspan=\"3\">Product</th>\n              <th rowspan=\"3\">Unit</th>\n              <th rowspan=\"3\">Quantity</th>\n              <ng-template [ngIf]=\"is_cmla_form_2\">\n                <th rowspan=\"3\">Destination</th>\n              </ng-template>\n              <th [colSpan]=\"GetBidPriceColSpan()\">\n                <!-- <div class=\"col-md-2 col-md-offset-3\"> -->\n                Bid price or formula\n                <!-- </div> -->\n                <div style=\"display: inline-block; min-width: 100px; margin-left: 10px;\">\n                  <ptx-sugester\n                    [model]=\"model.PDA_PRICE_UNIT\"\n                    [data_list]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    [select_field]=\"'ID'\"\n                    (ModelChanged)=\"model.PDA_PRICE_UNIT = $event;  HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </div>\n              </th>\n              <th rowspan=\"3\" width=\"50px\">Note</th>\n              <th rowspan=\"3\" width=\"50px\"></th>\n            </tr>\n            <tr>\n              <ng-template [ngIf]=\"is_cmla_form_2 || is_cmla_import\">\n                <th rowspan=\"2\">Packaging</th>\n                <ng-template [ngIf]=\"is_cmla_import\">\n                  <th rowspan=\"2\" style=\"min-width: 100px;\">Incoterm</th>\n                </ng-template>\n                <ng-template [ngIf]=\"is_cmla_form_2\">\n                  <th rowspan=\"2\" colspan=\"2\" style=\"min-width: 100px;\">Incoterm</th>\n                </ng-template>\n              </ng-template>\n              <!-- <ng-template [ngIf]=\"is_cmla_form_2\">\n                <th rowspan=\"2\">Destination (Port)</th>\n              </ng-template> -->\n              <th [colSpan]=\"2\" *ngFor=\"let round of model.PAF_BIDDING_ITEMS[0].PAF_ROUND;let round_index = index\">\n                {{GetRoundText(round_index + 1)}} Bid\n                <ng-template [ngIf]=\"round_index != 0\">\n                  <!-- <br/> -->\n                  <a (click)=\"RemoveRound.emit(round_index)\">\n                    <span class=\"ptx-delete-round glyphicon glyphicon-trash\"></span>\n                  </a>\n                </ng-template>\n              </th>\n              <ng-template [ngIf]=\"is_cmla_import\">\n                <th rowspan=\"2\">Freight Cost</th>\n                <th rowspan=\"2\">Benefit above break even price</th>\n              </ng-template>\n              <ng-template [ngIf]=\"is_cmla_form_1\">\n                <th rowspan=\"2\">Market price</th>\n                <th colspan=\"2\">Latest LP Plan Price</th>\n                <th nowrap>Benchmark Price</th>\n              </ng-template>\n              <ng-template [ngIf]=\"is_cmla_form_2\">\n                <th [colSpan]=\"GetBenchMarkColSpan()\">Market price</th>\n                <th colspan=\"2\">Latest LP Plan Price</th>\n                <!-- <th rowspan=\"2\">Term Price</th> -->\n                <!-- <th rowspan=\"2\">Import Price (CFR)</th> -->\n              </ng-template>\n            </tr>\n            <tr>\n              <ng-template ngFor let-row [ngForOf]=\"model.PAF_BIDDING_ITEMS[0].PAF_ROUND\">\n                <th>Float</th>\n                <th>Fixed</th>\n              </ng-template>\n              <ng-template [ngIf]=\"is_cmla_form_1\">\n                <th>Float</th>\n                <th>Fixed</th>\n                <th>\n                  <input\n                    type=\"text\"\n                    class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME\"\n                    (ngModelChange)=\"HandleModelUpdate();\"\n                    >\n                </th>\n              </ng-template>\n              <ng-template [ngIf]=\"is_cmla_form_2\">\n                <ng-template ngFor let-b [ngForOf]=\"model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK\" let-b_i=\"index\">\n                  <th>\n                    <div class=\"form-inline\">\n                      <input\n                        type=\"text\"\n                        class=\"form-control ptx-input-with-remove\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[b_i].PBB_NAME\"\n                        (ngModelChange)=\"HandleModelUpdate();UpdatePBBName();HandleModelUpdate();\"\n                      >\n                      <a *ngIf=\"b_i != 0\" (click)=\"RemoveBenchmark.emit(b_i)\">\n                        <span class=\"ptx-delete-round glyphicon glyphicon-trash\"></span>\n                      </a>\n                    </div>\n                  </th>\n                </ng-template>\n                <!-- <th>{{ model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME }}</th> -->\n                <th>Float</th>\n                <th>Fixed</th>\n              </ng-template>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngIf=\"model.PAF_BIDDING_ITEMS.length == 0\">\n              <td colspan=\"16\" class=\"text-center\">No Data</td>\n            </tr>\n            <ng-template ngFor let-row [ngForOf]=\"model.PAF_BIDDING_ITEMS\" let-i=\"index\">\n              <tr>\n                <td>\n                  <div class=\"checkbox\">\n                    <input\n                      id=\"other_{{i}}\"\n                      type=\"checkbox\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [ngModel]=\"GetSelected(i)\"\n                      (ngModelChange)=\"SetSelected($event, i);OnSelectForAward.emit()\">\n                    <label for=\"other_{{i}}\"></label>\n                  </div>\n                </td>\n                <td class=\"min-300\">\n                  <ptx-sugester\n                    [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_FK_CUSTOMER\"\n                    [data_list]=\"master.CUSTOMERS\"\n                    [is_disabled]=\"model.IS_DISABLED || GetSelected(i)\"\n                    [show_field]=\"'VALUE'\"\n                    [select_field]=\"'ID'\"\n                    (ModelChanged)=\"model.PAF_BIDDING_ITEMS[i].PBI_FK_CUSTOMER = $event;HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    type=\"text\"\n                    class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_CONTACT_PERSON\"\n                    (ngModelChange)=\"HandleModelUpdate();\"\n                  />\n                </td>\n                <td class=\"min-200\">\n                  <ptx-sugester\n                    [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_FK_MATERIAL\"\n                    [data_list]=\"master.PRODUCTS\"\n                    [is_disabled]=\"(is_cmla_import && i != 0) || model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.PAF_BIDDING_ITEMS[i].PBI_FK_MATERIAL = $event;UpdateCMLAFirstRow();HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </td>\n                <td class=\"min-100\">\n                  <ptx-sugester\n                    [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_QUANTITY_UNIT\"\n                    [data_list]=\"master.QUANTITY_UNITS\"\n                    [is_disabled]=\"(is_cmla_import && i != 0) || model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.PAF_BIDDING_ITEMS[i].PBI_QUANTITY_UNIT = $event;UpdateCMLAFirstRow();HandleModelUpdate();\"\n                  >\n                  </ptx-sugester>\n                </td>\n                <td class=\"min-300\">\n                  <ptx-input-quantity\n                    [disabled]=\"model.IS_DISABLED\"\n                    [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_QUANTITY_FLOAT\"\n                    (HandleModelUpdate)=\"model.PAF_BIDDING_ITEMS[i].PBI_QUANTITY_FLOAT = $event;HandleModelUpdate();\"\n                  >\n                  </ptx-input-quantity>\n                </td>\n                <ng-template [ngIf]=\"is_cmla_form_2\">\n                  <td class=\"min-200\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_COUNTRY\"\n                      [data_list]=\"master.COUNTRY\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"model.PAF_BIDDING_ITEMS[i].PBI_COUNTRY = $event;\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                </ng-template>\n                <ng-template [ngIf]=\"is_cmla_form_2 || is_cmla_import\">\n                  <td class=\"min-100\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_FK_PACKAGING\"\n                      [data_list]=\"master.PAF_CMLA_PACKAGING\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      [select_field]=\"'ID'\"\n                      (ModelChanged)=\"model.PAF_BIDDING_ITEMS[i].PBI_FK_PACKAGING = $event;HandleModelUpdate();\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td class=\"min-100\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_INCOTERM\"\n                      [data_list]=\"master.INCOTERMS\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"model.PAF_BIDDING_ITEMS[i].PBI_INCOTERM = $event;HandleModelUpdate();\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <ng-template [ngIf]=\"is_cmla_form_2\">\n                    <td class=\"min-100\">\n                      <input\n                        type=\"text\"\n                        class=\"form-control\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_PORT\"\n                        (ngModelChange)=\"HandleModelUpdate();\"\n                      />\n                    </td>\n                  </ng-template>\n                </ng-template>\n                <ng-template [ngIf]=\"is_cmla_form_2\">\n                  <!-- <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_PORT\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td> -->\n                </ng-template>\n                <ng-template ngFor let-row [ngForOf]=\"model.PAF_BIDDING_ITEMS[0].PAF_ROUND\" let-j=\"index\">\n                  <td class=\"min-400\">\n                    <ptx-input-float\n                      [class_name]=\"'ptx-w-200'\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [data_list]=\"master.FORMULA\"\n                      [model]=\"model.PAF_BIDDING_ITEMS[i].PAF_ROUND[j].PRN_FLOAT\"\n                      (HandleModelUpdate)=\"model.PAF_BIDDING_ITEMS[i].PAF_ROUND[j].PRN_FLOAT = $event; HandleModelUpdate();\"\n                    >\n                    </ptx-input-float>\n                    <!-- <div class=\"form-inline\">\n                      <div class=\"form-group\" style=\"width: 80px;\">\n                        <select class=\"form-control\" style=\"width: 90%\">\n                          <option>FOBK</option>\n                          <option>FOBK2</option>\n                        </select>\n                      </div>\n                      <div class=\"form-group\" style=\"width: 80px;\">\n                        <select class=\"form-control\" style=\"width: 90%\">\n                          <option>-</option>\n                          <option>+</option>\n                        </select>\n                      </div>\n                      <div class=\"form-group\"><input type=\"text\" class=\"form-control\" style=\"width: 50px;\" /></div>\n                    </div> -->\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PAF_ROUND[j].PRN_FIXED\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td>\n                </ng-template>\n                <ng-template [ngIf]=\"is_cmla_import\">\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_FREIGHT_PRICE\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_BENEFIT\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td>\n                </ng-template>\n                <ng-template [ngIf]=\"is_cmla_form_1\">\n                  <td class=\"min-200\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_MARKET_PRICE_FLOAT\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td>\n                  <td class=\"min-400\">\n                    <ptx-input-float\n                      [class_name]=\"'ptx-w-200'\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [data_list]=\"master.FORMULA\"\n                      [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_LATEST_LP_PLAN_PRICE_FLOAT\"\n                      (HandleModelUpdate)=\"model.PAF_BIDDING_ITEMS[i].PBI_LATEST_LP_PLAN_PRICE_FLOAT = $event; HandleModelUpdate();\"\n                    >\n                    </ptx-input-float>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_LATEST_LP_PLAN_PRICE_FIXED\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE\"\n                      (ngModelChange)=\"HandleModelUpdate();\"\n                    />\n                  </td>\n                </ng-template>\n                <ng-template [ngIf]=\"is_cmla_form_2\">\n                  <ng-template ngFor let-b [ngForOf]=\"model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK\" let-b_i=\"index\">\n                    <td class=\"min-200\">\n                      <input\n                        type=\"text\"\n                        class=\"form-control\"\n                        ptxRestrictNumber\n                        [disabled]=\"i != 0 || model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PAF_BIDDING_ITEMS_BENCHMARK[b_i].PBB_VALUE\"\n                        (ngModelChange)=\"UpdateFirstRow(i);HandleModelUpdate();\"\n                      />\n                    </td>\n                  </ng-template>\n                  <!-- <td class=\"min-200\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE\"\n                      (ngModelChange)=\"UpdateFirstRow(i);HandleModelUpdate();\"\n                    />\n                  </td> -->\n                  <td class=\"min-400\">\n                    <ptx-input-float\n                      [class_name]=\"'ptx-w-200'\"\n                      [disabled]=\"i != 0 || model.IS_DISABLED\"\n                      [data_list]=\"master.FORMULA\"\n                      [model]=\"model.PAF_BIDDING_ITEMS[i].PBI_LATEST_LP_PLAN_PRICE_FLOAT\"\n                      (HandleModelUpdate)=\"UpdateFirstRow(i);model.PAF_BIDDING_ITEMS[i].PBI_LATEST_LP_PLAN_PRICE_FLOAT = $event; HandleModelUpdate();\"\n                    >\n                    </ptx-input-float>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"i != 0 || model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_LATEST_LP_PLAN_PRICE_FIXED\"\n                      (ngModelChange)=\"UpdateFirstRow(i);HandleModelUpdate();\"\n                    />\n                  </td>\n                  <!-- <td class=\"min-200\">\n                    <div class=\"input-group\">\n                      <input\n                        type=\"text\"\n                        ptxRestrictNumber\n                        class=\"form-control\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_TERMS_PRICE_MIN\"\n                        (ngModelChange)=\"HandleModelUpdate();\"\n                      />\n                      <span class=\"input-group-addon\">-</span>\n                      <input\n                        type=\"text\"\n                        ptxRestrictNumber\n                        class=\"form-control\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_TERMS_PRICE_MAX\"\n                        (ngModelChange)=\"HandleModelUpdate();\"\n                      />\n                    </div>\n                  </td>\n                  <td class=\"min-200\">\n                    <div class=\"input-group\">\n                      <input\n                        type=\"text\"\n                        ptxRestrictNumber\n                        class=\"form-control\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_IMPORT_PRICE_MIN\"\n                        (ngModelChange)=\"HandleModelUpdate();\"\n                      />\n                      <span class=\"input-group-addon\">-</span>\n                      <input\n                        type=\"text\"\n                        ptxRestrictNumber\n                        class=\"form-control\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_IMPORT_PRICE_MAX\"\n                        (ngModelChange)=\"HandleModelUpdate();\"\n                      />\n                    </div>\n                  </td> -->\n                </ng-template>\n                <td class=\"min-200\">\n                  <input\n                    type=\"text\"\n                    class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_BIDDING_ITEMS[i].PBI_NOTE\"\n                    (ngModelChange)=\"HandleModelUpdate();\"\n                  />\n                </td>\n                <td>\n                  <a (click)=\"RemoveBidder.emit(row)\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n                </td>\n              </tr>\n            </ng-template>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <button class=\"btn btn-success\" (click)=\"AddBidder.emit()\" *ngIf=\"!is_cmla_import\"> Add Customer</button>\n      <button class=\"btn btn-success\" (click)=\"AddBidder.emit()\" *ngIf=\"is_cmla_import\"> Add Supplier</button>\n      <button class=\"btn btn-primary\" (click)=\"AddBidRound.emit()\"> Add Bid Round</button>\n      <button *ngIf=\"is_cmla_form_2\" class=\"btn btn-default\" (click)=\"AddBenchmark.emit()\"> Add Market price</button>\n    </div>\n  </div>\n  <br />\n</ng-template>\n"

/***/ }),

/***/ "../../../../../src/app/components/cmla/cmla-form-one/cmla-form-one.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaFormOneComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CmlaFormOneComponent = (function () {
    function CmlaFormOneComponent() {
        this.is_cmla_form_1 = false;
        this.is_cmla_form_2 = false;
        this.is_cmla_import = false;
        this.AddCustomerRequest = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.AddBidder = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.AddBidRound = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.AddBenchmark = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.RemoveRound = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.RemoveBidder = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.RemoveBenchmark = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.RemoveCustomerRequest = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.OnSelectForAward = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.OnModelUpdate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    CmlaFormOneComponent.prototype.ngOnInit = function () {
    };
    CmlaFormOneComponent.prototype.SetSelected = function (e, index) {
        this.model.PAF_BIDDING_ITEMS[index].PBI_AWARDED_BIDDER = e ? 'Y' : 'N';
    };
    CmlaFormOneComponent.prototype.GetSelected = function (index) {
        // if (this.is_cmps_inter) {
        //   return this.criteria.PAF_BIDDING_ITEMS[index].SELECTED;
        // }
        // console.log(index)
        return this.model.PAF_BIDDING_ITEMS[index].PBI_AWARDED_BIDDER == 'Y';
    };
    CmlaFormOneComponent.prototype.UpdateCMLAFirstRow = function () {
        if (this.is_cmla_import) {
            var target_1 = this.model.PAF_BIDDING_ITEMS[0];
            this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](this.model.PAF_BIDDING_ITEMS, function (e) {
                e.PBI_FK_MATERIAL = __WEBPACK_IMPORTED_MODULE_2__common__["a" /* Utility */].clone(target_1.PBI_FK_MATERIAL);
                e.PBI_QUANTITY_UNIT = __WEBPACK_IMPORTED_MODULE_2__common__["a" /* Utility */].clone(target_1.PBI_QUANTITY_UNIT);
                e.PBI_FREIGHT_PRICE = __WEBPACK_IMPORTED_MODULE_2__common__["a" /* Utility */].clone(target_1.PBI_FREIGHT_PRICE);
                e.PBI_BENEFIT = __WEBPACK_IMPORTED_MODULE_2__common__["a" /* Utility */].clone(target_1.PBI_BENEFIT);
                return e;
            });
        }
    };
    CmlaFormOneComponent.prototype.GetRoundText = function (num) {
        return __WEBPACK_IMPORTED_MODULE_2__common__["a" /* Utility */].GetRoundText(num);
    };
    CmlaFormOneComponent.prototype.HandleModelUpdate = function () {
        this.OnModelUpdate.emit(this.model);
    };
    CmlaFormOneComponent.prototype.UpdateFirstRow = function (i) {
        var _this = this;
        this.OnModelUpdate.emit(this.model);
        if (i == 0) {
            var PBI_MARKET_PRICE_1 = this.model.PAF_BIDDING_ITEMS[0].PBI_MARKET_PRICE;
            var PBI_LATEST_LP_PLAN_PRICE_FLOAT_1 = this.model.PAF_BIDDING_ITEMS[0].PBI_LATEST_LP_PLAN_PRICE_FLOAT;
            var PBI_LATEST_LP_PLAN_PRICE_FIXED_1 = this.model.PAF_BIDDING_ITEMS[0].PBI_LATEST_LP_PLAN_PRICE_FIXED;
            // console.log(PBI_LATEST_LP_PLAN_PRICE_FLOAT);
            var PBB_VALUE_1 = this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE;
            var PBB_NAME_1 = this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME;
            this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](this.model.PAF_BIDDING_ITEMS, function (e) {
                e.PBI_MARKET_PRICE = PBI_MARKET_PRICE_1;
                e.PBI_LATEST_LP_PLAN_PRICE_FLOAT = PBI_LATEST_LP_PLAN_PRICE_FLOAT_1;
                e.PBI_LATEST_LP_PLAN_PRICE_FIXED = PBI_LATEST_LP_PLAN_PRICE_FIXED_1;
                if (!_this.is_cmla_form_2) {
                    e.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME = PBB_NAME_1;
                    e.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE = PBB_VALUE_1;
                }
                return e;
            });
            if (this.is_cmla_form_2) {
                this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2__common__["c" /* PAFStore */].Form2BenchMark(this.model.PAF_BIDDING_ITEMS);
            }
            this.OnModelUpdate.emit(this.model);
        }
    };
    CmlaFormOneComponent.prototype.UpdatePBBName = function () {
        for (var i = 0; i < this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK.length; i++) {
            var PBB_NAME = this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[i].PBB_NAME;
            for (var j = 0; j < this.model.PAF_BIDDING_ITEMS.length; ++j) {
                this.model.PAF_BIDDING_ITEMS[j].PAF_BIDDING_ITEMS_BENCHMARK[i].PBB_NAME = PBB_NAME;
            }
        }
    };
    CmlaFormOneComponent.prototype.GetBidPriceColSpan = function () {
        var bids = this.model.PAF_BIDDING_ITEMS;
        var round = bids && bids.length > 0 && bids[0] && bids[0].PAF_ROUND && bids[0].PAF_ROUND.length;
        round = +round || 0;
        var other_factor = 0;
        if (this.is_cmla_form_2) {
            other_factor += 2; //package, incoterm, destination, term-price, import-price
            if (this.GetBenchMarkColSpan() != 1) {
                other_factor += this.GetBenchMarkColSpan() - 1;
            }
        }
        return (round * 2) + 4 + +other_factor;
    };
    CmlaFormOneComponent.prototype.GetBenchMarkColSpan = function () {
        var bids = this.model.PAF_BIDDING_ITEMS;
        var count = bids && bids.length > 0 && bids[0] && bids[0].PAF_BIDDING_ITEMS_BENCHMARK && bids[0].PAF_BIDDING_ITEMS_BENCHMARK.length;
        count = +count || 0;
        if (count == 1) {
            return 1;
        }
        return (count); // +1 for other column
    };
    return CmlaFormOneComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], CmlaFormOneComponent.prototype, "is_cmla_form_1", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], CmlaFormOneComponent.prototype, "is_cmla_form_2", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], CmlaFormOneComponent.prototype, "is_cmla_import", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaFormOneComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaFormOneComponent.prototype, "master", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaFormOneComponent.prototype, "criteria", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], CmlaFormOneComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], CmlaFormOneComponent.prototype, "AddCustomerRequest", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _b || Object)
], CmlaFormOneComponent.prototype, "AddBidder", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _c || Object)
], CmlaFormOneComponent.prototype, "AddBidRound", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _d || Object)
], CmlaFormOneComponent.prototype, "AddBenchmark", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _e || Object)
], CmlaFormOneComponent.prototype, "RemoveRound", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _f || Object)
], CmlaFormOneComponent.prototype, "RemoveBidder", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _g || Object)
], CmlaFormOneComponent.prototype, "RemoveBenchmark", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _h || Object)
], CmlaFormOneComponent.prototype, "RemoveCustomerRequest", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _j || Object)
], CmlaFormOneComponent.prototype, "OnSelectForAward", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _k || Object)
], CmlaFormOneComponent.prototype, "OnModelUpdate", void 0);
CmlaFormOneComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-cmla-form-one',
        template: __webpack_require__("../../../../../src/app/components/cmla/cmla-form-one/cmla-form-one.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [])
], CmlaFormOneComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=cmla-form-one.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/datatable/datatable.component.html":
/***/ (function(module, exports) {

module.exports = "<table id=\"ptxDataTable\" class=\"table table-hover table-striped table-bordered\" cellspacing=\"0\">\n  <thead>\n    <tr>\n      <th>Name</th>\n      <th>Position</th>\n      <th>Office</th>\n      <th>Age</th>\n      <th>Start date</th>\n      <th>Salary</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Tiger Nixon</td>\n      <td>System Architect</td>\n      <td>Edinburgh</td>\n      <td>61</td>\n      <td>2011/04/25</td>\n      <td>$320,800</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Garrett Winters</td>\n      <td>Accountant</td>\n      <td>Tokyo</td>\n      <td>63</td>\n      <td>2011/07/25</td>\n      <td>$170,750</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Ashton Cox</td>\n      <td>Junior Technical Author</td>\n      <td>San Francisco</td>\n      <td>66</td>\n      <td>2009/01/12</td>\n      <td>$86,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Cedric Kelly</td>\n      <td>Senior Javascript Developer</td>\n      <td>Edinburgh</td>\n      <td>22</td>\n      <td>2012/03/29</td>\n      <td>$433,060</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Airi Satou</td>\n      <td>Accountant</td>\n      <td>Tokyo</td>\n      <td>33</td>\n      <td>2008/11/28</td>\n      <td>$162,700</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Brielle Williamson</td>\n      <td>Integration Specialist</td>\n      <td>New York</td>\n      <td>61</td>\n      <td>2012/12/02</td>\n      <td>$372,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Herrod Chandler</td>\n      <td>Sales Assistant</td>\n      <td>San Francisco</td>\n      <td>59</td>\n      <td>2012/08/06</td>\n      <td>$137,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Rhona Davidson</td>\n      <td>Integration Specialist</td>\n      <td>Tokyo</td>\n      <td>55</td>\n      <td>2010/10/14</td>\n      <td>$327,900</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Colleen Hurst</td>\n      <td>Javascript Developer</td>\n      <td>San Francisco</td>\n      <td>39</td>\n      <td>2009/09/15</td>\n      <td>$205,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Sonya Frost</td>\n      <td>Software Engineer</td>\n      <td>Edinburgh</td>\n      <td>23</td>\n      <td>2008/12/13</td>\n      <td>$103,600</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Jena Gaines</td>\n      <td>Office Manager</td>\n      <td>London</td>\n      <td>30</td>\n      <td>2008/12/19</td>\n      <td>$90,560</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Quinn Flynn</td>\n      <td>Support Lead</td>\n      <td>Edinburgh</td>\n      <td>22</td>\n      <td>2013/03/03</td>\n      <td>$342,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Charde Marshall</td>\n      <td>Regional Director</td>\n      <td>San Francisco</td>\n      <td>36</td>\n      <td>2008/10/16</td>\n      <td>$470,600</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Haley Kennedy</td>\n      <td>Senior Marketing Designer</td>\n      <td>London</td>\n      <td>43</td>\n      <td>2012/12/18</td>\n      <td>$313,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Tatyana Fitzpatrick</td>\n      <td>Regional Director</td>\n      <td>London</td>\n      <td>19</td>\n      <td>2010/03/17</td>\n      <td>$385,750</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Michael Silva</td>\n      <td>Marketing Designer</td>\n      <td>London</td>\n      <td>66</td>\n      <td>2012/11/27</td>\n      <td>$198,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Paul Byrd</td>\n      <td>Chief Financial Officer (CFO)</td>\n      <td>New York</td>\n      <td>64</td>\n      <td>2010/06/09</td>\n      <td>$725,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Gloria Little</td>\n      <td>Systems Administrator</td>\n      <td>New York</td>\n      <td>59</td>\n      <td>2009/04/10</td>\n      <td>$237,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Bradley Greer</td>\n      <td>Software Engineer</td>\n      <td>London</td>\n      <td>41</td>\n      <td>2012/10/13</td>\n      <td>$132,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Dai Rios</td>\n      <td>Personnel Lead</td>\n      <td>Edinburgh</td>\n      <td>35</td>\n      <td>2012/09/26</td>\n      <td>$217,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Jenette Caldwell</td>\n      <td>Development Lead</td>\n      <td>New York</td>\n      <td>30</td>\n      <td>2011/09/03</td>\n      <td>$345,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Yuri Berry</td>\n      <td>Chief Marketing Officer (CMO)</td>\n      <td>New York</td>\n      <td>40</td>\n      <td>2009/06/25</td>\n      <td>$675,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Caesar Vance</td>\n      <td>Pre-Sales Support</td>\n      <td>New York</td>\n      <td>21</td>\n      <td>2011/12/12</td>\n      <td>$106,450</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Doris Wilder</td>\n      <td>Sales Assistant</td>\n      <td>Sidney</td>\n      <td>23</td>\n      <td>2010/09/20</td>\n      <td>$85,600</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Angelica Ramos</td>\n      <td>Chief Executive Officer (CEO)</td>\n      <td>London</td>\n      <td>47</td>\n      <td>2009/10/09</td>\n      <td>$1,200,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Gavin Joyce</td>\n      <td>Developer</td>\n      <td>Edinburgh</td>\n      <td>42</td>\n      <td>2010/12/22</td>\n      <td>$92,575</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Jennifer Chang</td>\n      <td>Regional Director</td>\n      <td>Singapore</td>\n      <td>28</td>\n      <td>2010/11/14</td>\n      <td>$357,650</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Brenden Wagner</td>\n      <td>Software Engineer</td>\n      <td>San Francisco</td>\n      <td>28</td>\n      <td>2011/06/07</td>\n      <td>$206,850</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Fiona Green</td>\n      <td>Chief Operating Officer (COO)</td>\n      <td>San Francisco</td>\n      <td>48</td>\n      <td>2010/03/11</td>\n      <td>$850,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Shou Itou</td>\n      <td>Regional Marketing</td>\n      <td>Tokyo</td>\n      <td>20</td>\n      <td>2011/08/14</td>\n      <td>$163,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Michelle House</td>\n      <td>Integration Specialist</td>\n      <td>Sidney</td>\n      <td>37</td>\n      <td>2011/06/02</td>\n      <td>$95,400</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Suki Burks</td>\n      <td>Developer</td>\n      <td>London</td>\n      <td>53</td>\n      <td>2009/10/22</td>\n      <td>$114,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Prescott Bartlett</td>\n      <td>Technical Author</td>\n      <td>London</td>\n      <td>27</td>\n      <td>2011/05/07</td>\n      <td>$145,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Gavin Cortez</td>\n      <td>Team Leader</td>\n      <td>San Francisco</td>\n      <td>22</td>\n      <td>2008/10/26</td>\n      <td>$235,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Martena Mccray</td>\n      <td>Post-Sales support</td>\n      <td>Edinburgh</td>\n      <td>46</td>\n      <td>2011/03/09</td>\n      <td>$324,050</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Unity Butler</td>\n      <td>Marketing Designer</td>\n      <td>San Francisco</td>\n      <td>47</td>\n      <td>2009/12/09</td>\n      <td>$85,675</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Howard Hatfield</td>\n      <td>Office Manager</td>\n      <td>San Francisco</td>\n      <td>51</td>\n      <td>2008/12/16</td>\n      <td>$164,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Hope Fuentes</td>\n      <td>Secretary</td>\n      <td>San Francisco</td>\n      <td>41</td>\n      <td>2010/02/12</td>\n      <td>$109,850</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Vivian Harrell</td>\n      <td>Financial Controller</td>\n      <td>San Francisco</td>\n      <td>62</td>\n      <td>2009/02/14</td>\n      <td>$452,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Timothy Mooney</td>\n      <td>Office Manager</td>\n      <td>London</td>\n      <td>37</td>\n      <td>2008/12/11</td>\n      <td>$136,200</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Jackson Bradshaw</td>\n      <td>Director</td>\n      <td>New York</td>\n      <td>65</td>\n      <td>2008/09/26</td>\n      <td>$645,750</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Olivia Liang</td>\n      <td>Support Engineer</td>\n      <td>Singapore</td>\n      <td>64</td>\n      <td>2011/02/03</td>\n      <td>$234,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Bruno Nash</td>\n      <td>Software Engineer</td>\n      <td>London</td>\n      <td>38</td>\n      <td>2011/05/03</td>\n      <td>$163,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Sakura Yamamoto</td>\n      <td>Support Engineer</td>\n      <td>Tokyo</td>\n      <td>37</td>\n      <td>2009/08/19</td>\n      <td>$139,575</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Thor Walton</td>\n      <td>Developer</td>\n      <td>New York</td>\n      <td>61</td>\n      <td>2013/08/11</td>\n      <td>$98,540</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Finn Camacho</td>\n      <td>Support Engineer</td>\n      <td>San Francisco</td>\n      <td>47</td>\n      <td>2009/07/07</td>\n      <td>$87,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Serge Baldwin</td>\n      <td>Data Coordinator</td>\n      <td>Singapore</td>\n      <td>64</td>\n      <td>2012/04/09</td>\n      <td>$138,575</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Zenaida Frank</td>\n      <td>Software Engineer</td>\n      <td>New York</td>\n      <td>63</td>\n      <td>2010/01/04</td>\n      <td>$125,250</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Zorita Serrano</td>\n      <td>Software Engineer</td>\n      <td>San Francisco</td>\n      <td>56</td>\n      <td>2012/06/01</td>\n      <td>$115,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Jennifer Acosta</td>\n      <td>Junior Javascript Developer</td>\n      <td>Edinburgh</td>\n      <td>43</td>\n      <td>2013/02/01</td>\n      <td>$75,650</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Cara Stevens</td>\n      <td>Sales Assistant</td>\n      <td>New York</td>\n      <td>46</td>\n      <td>2011/12/06</td>\n      <td>$145,600</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Hermione Butler</td>\n      <td>Regional Director</td>\n      <td>London</td>\n      <td>47</td>\n      <td>2011/03/21</td>\n      <td>$356,250</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Lael Greer</td>\n      <td>Systems Administrator</td>\n      <td>London</td>\n      <td>21</td>\n      <td>2009/02/27</td>\n      <td>$103,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Jonas Alexander</td>\n      <td>Developer</td>\n      <td>San Francisco</td>\n      <td>30</td>\n      <td>2010/07/14</td>\n      <td>$86,500</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Shad Decker</td>\n      <td>Regional Director</td>\n      <td>Edinburgh</td>\n      <td>51</td>\n      <td>2008/11/13</td>\n      <td>$183,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Michael Bruce</td>\n      <td>Javascript Developer</td>\n      <td>Singapore</td>\n      <td>29</td>\n      <td>2011/06/27</td>\n      <td>$183,000</td>\n    </tr>\n    <tr class=\"drafts\" style=\"cursor: pointer;\">\n      <td>Donna Snider</td>\n      <td>Customer Support</td>\n      <td>New York</td>\n      <td>27</td>\n      <td>2011/01/25</td>\n      <td>$112,000</td>\n    </tr>\n  </tbody>\n</table>\n"

/***/ }),

/***/ "../../../../../src/app/components/datatable/datatable.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatatableComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DatatableComponent = (function () {
    function DatatableComponent() {
    }
    DatatableComponent.prototype.ngOnInit = function () {
        $(function () {
            $("#ptxDataTable").DataTable({
                "ordering": false,
                "bSort": false,
                "columns": [
                    { "data": "first_name" },
                    { "data": "last_name" },
                    { "data": "position" },
                    { "data": "office" },
                    { "data": "start_date" },
                    { "data": "salary" }
                ],
            });
        });
    };
    return DatatableComponent;
}());
DatatableComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-datatable',
        template: __webpack_require__("../../../../../src/app/components/datatable/datatable.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [])
], DatatableComponent);

//# sourceMappingURL=datatable.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/datepicker/datepicker.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DatepickerComponent = (function () {
    function DatepickerComponent(el) {
        this.el = el;
        this.model = null;
        this.disabled = false;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.show_model = "";
    }
    DatepickerComponent.prototype.onChange = function (e) {
        console.log(e);
    };
    DatepickerComponent.prototype.ngOnInit = function () {
        var context = this;
        $(context.el.nativeElement).find('.input-date-picker').dateRangePicker({
            autoClose: true,
            singleDate: true,
            showShortcuts: false,
            format: "DD/MM/YYYY",
        }).bind('datepicker-change', function (event, obj) {
            console.log(obj);
            // context.model = DateHelper.DateStringToDate(obj.value);
            context.show_model = obj.value;
            context.modelChange.emit(__WEBPACK_IMPORTED_MODULE_1__common_date_helper__["a" /* DateHelper */].DateStringToCsharpString(obj.value));
            // DateHelper
        });
    };
    DatepickerComponent.prototype.normalizeDate = function (model) {
        var date_str = __WEBPACK_IMPORTED_MODULE_1__common_date_helper__["a" /* DateHelper */].DateToString(model);
        if (date_str == "Invalid date") {
            date_str = "";
        }
        return date_str;
    };
    return DatepickerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], DatepickerComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], DatepickerComponent.prototype, "disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], DatepickerComponent.prototype, "modelChange", void 0);
DatepickerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-datepicker',
        template: "<input\n    type=\"text\"\n    name=\"date\"\n    class=\"form-control essential input-date-picker daterange\"\n    [ngModel]=\"normalizeDate(model)\"\n    [disabled]=\"disabled\"\n  >",
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _b || Object])
], DatepickerComponent);

var _a, _b;
//# sourceMappingURL=datepicker.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/datepicker/daterangepicker.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_date_helper__ = __webpack_require__("../../../../../src/app/common/date-helper.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaterangepickerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DaterangepickerComponent = (function () {
    function DaterangepickerComponent(el) {
        this.el = el;
        // @Input() private model: string = null;
        this.start_date = null;
        this.end_date = null;
        this.disabled = false;
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.show_model = "";
    }
    DaterangepickerComponent.prototype.onChange = function (e) {
        console.log(e);
    };
    DaterangepickerComponent.prototype.ngOnInit = function () {
    };
    DaterangepickerComponent.prototype.OnChangeUpdate = function (p) {
        if (!p) {
            // console.log(p)
            this.modelChange.emit({
                start_date: '',
                end_date: '',
            });
        }
    };
    DaterangepickerComponent.prototype.ngAfterViewInit = function () {
        var context = this;
        console.log($(context.el.nativeElement).find('.input-date-picker'));
        $(context.el.nativeElement).find('.input-date-picker').dateRangePicker({
            autoClose: true,
            singleDate: false,
            showShortcuts: false,
            format: "DD/MM/YYYY",
        }).bind('datepicker-change', function (event, obj) {
            // console.log(obj)
            var start_date = __WEBPACK_IMPORTED_MODULE_1__common_date_helper__["a" /* DateHelper */].DateStringToCsharpString(obj.date1);
            var end_date = __WEBPACK_IMPORTED_MODULE_1__common_date_helper__["a" /* DateHelper */].DateStringToCsharpString(obj.date2);
            // context.model = Date√Helper.DateStringToDate(obj.value);
            // context.show_model = obj.value;
            context.modelChange.emit({
                start_date: start_date,
                end_date: end_date,
            });
            // DateHelper
        });
    };
    DaterangepickerComponent.prototype.normalizeDate = function (start_date, end_date) {
        // console.log(start_date)
        // console.log(end_date)
        if (!start_date || start_date == "") {
            return "";
        }
        var start_date_str = __WEBPACK_IMPORTED_MODULE_1__common_date_helper__["a" /* DateHelper */].DateToString(start_date);
        var end_date_str = __WEBPACK_IMPORTED_MODULE_1__common_date_helper__["a" /* DateHelper */].DateToString(end_date);
        if (!start_date_str || start_date_str == "") {
            return "";
        }
        return start_date_str + " TO " + end_date_str;
    };
    return DaterangepickerComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], DaterangepickerComponent.prototype, "start_date", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], DaterangepickerComponent.prototype, "end_date", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], DaterangepickerComponent.prototype, "disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], DaterangepickerComponent.prototype, "modelChange", void 0);
DaterangepickerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-daterangepicker',
        template: "<input\n    type=\"text\"\n    name=\"date\"\n    class=\"form-control essential input-date-picker daterange\"\n    [ngModel]=\"normalizeDate(start_date, end_date)\"\n    (ngModelChange)=\"OnChangeUpdate($event)\"\n    [disabled]=\"disabled\"\n  >",
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _b || Object])
], DaterangepickerComponent);

var _a, _b;
//# sourceMappingURL=daterangepicker.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/file-upload/file-upload.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ is_file_upload | json }}</pre> -->\n<!-- <div class=\"col-xs-12\" *ngIf=\"is_disabled\">\n  <div class=\"form-group\">\n    <ng-content></ng-content>\n    <label class=\"btn btn-xs btn-select-file\" [ngClass]=\"{'m-progress':is_file_upload}\">\n        view attached file\n    </label>\n  </div>\n</div> -->\n<div class=\"col-xs-12\" *ngIf=\"!is_disabled\">\n  <div class=\"form-group\">\n    <ng-content></ng-content>\n    <label class=\"btn btn-default\" [ngClass]=\"{'m-progress':is_file_upload}\">\n    <!-- <label class=\"btn btn-xs btn-select-file\" [ngClass]=\"{'m-progress':is_file_upload}\"> -->\n        Select File\n        <span class=\"glyphicon glyphicon-upload\" aria-hidden=\"true\"></span>\n        <input [disabled]=\"is_disabled\" name=\"ss\" type=\"file\" (change)=\"FileChangeEvent($event, filterBy)\" style=\"display: none;\" multiple>\n    </label>\n    <b>{{ desc }}</b>\n    <!-- <b>PDF, Word, Excel, Image, Outlook</b> -->\n  </div>\n</div>\n\n<div *ngFor=\"let elem of model|attachmentFilter:filterBy\" class=\"col-md-12\">\n  <div class=\"col-md-6\">\n    <!-- <div *ngFor=\"let elem of model\" class=\"uploadifyQueueItem ExplanAttach\"> -->\n    <div class=\"uploadifyQueueItem ExplanAttach\">\n        <div class=\"cancel\" *ngIf=\"!is_disabled\">\n            <a [class.disabled]=\"is_disabled\" (click)=\"RemoveFiles(elem)\"><span class=\"glyphicon glyphicon-remove removeExplanAttach\"></span></a>\n        </div>\n        <a target=\"_blank\" [href]=\"GetFileUrl(elem.PAT_PATH)\">{{ elem.PAT_INFO }}</a>\n    </div>\n  </div>\n  <div class=\"col-md-4\" *ngIf=\"need_caption\" style=\"margin-top: 10px;\">\n    <input type=\"text\" name=\"caption\" class=\"form-control\" [disabled]=\"is_disabled\" [(ngModel)]=\"model[GetIndex(elem)].PAT_CAPTION\" (ngModelChange)=\"handleModelChanged.emit(model)\">\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/file-upload/file-upload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_upload_service__ = __webpack_require__("../../../../../src/app/services/upload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_utility__ = __webpack_require__("../../../../../src/app/common/utility.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileUploadComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FileUploadComponent = (function () {
    function FileUploadComponent(uploadService) {
        this.uploadService = uploadService;
        this.model = [];
        this.fk = "";
        this.is_disabled = false;
        this.filterBy = '';
        this.need_caption = false;
        this.desc = 'PDF, Word, Excel, Image, Outlook';
        this.handleModelChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.is_file_upload = false;
    }
    FileUploadComponent.prototype.ngOnInit = function () {
    };
    FileUploadComponent.prototype.RemoveFiles = function (target) {
        if (this.is_disabled) {
            return null;
        }
        var state = this.model;
        var rejected_images = __WEBPACK_IMPORTED_MODULE_1_lodash__["reject"](state, function (elem) {
            return elem == target;
        });
        this.model = rejected_images;
        this.handleModelChanged.emit(this.model);
    };
    FileUploadComponent.prototype.GetIndex = function (elem) {
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["findIndex"](this.model, function (e) {
            return e == elem;
        });
    };
    FileUploadComponent.prototype.FileChangeEvent = function ($event, type_string) {
        var _this = this;
        var files = $event.target.files;
        this.model = __WEBPACK_IMPORTED_MODULE_1_lodash__["extend"]([], this.model);
        __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](files, function (file) {
            _this.is_file_upload = true;
            _this.uploadService.postSingle(file, type_string)
                .subscribe(function (data) {
                // const uid = data.uid;
                // console.log(data.PAT_INFO)
                _this.is_file_upload = false;
                _this.model.push(data);
                _this.handleModelChanged.emit(_this.model);
                $event.target.value = "";
            }, function (error) {
                // console.log(error)
                _this.is_file_upload = false;
                $event.target.value = "";
            });
            // setTimeout(() => {
            //   console.log(file);
            //   const data = {
            //     name: file.name,
            //   };
            //   this.model.push(data);
            //   this.is_file_upload = false;
            // }, 1000);
        });
        console.log(files);
        // let file: File = files[0]
        // this.uploadService.postMemo(file, type_string, this.fk)
        //   .subscribe(
        //       data => {
        //         // const uid = data.uid;
        //         this.model.push(data);
        //         console.log(data)
        //         this.is_file_upload = false;
        //         this.handleModelChanged.emit(this.model);
        //         $event.target.value = "";
        //       },
        //       error => {
        //         console.log(error)
        //         this.is_file_upload = false;
        //         $event.target.value = "";
        //       }
        //   )
    };
    FileUploadComponent.prototype.GetFileUrl = function (path) {
        return __WEBPACK_IMPORTED_MODULE_3__common_utility__["a" /* Utility */].GetFileUrl(path);
    };
    return FileUploadComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], FileUploadComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], FileUploadComponent.prototype, "fk", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], FileUploadComponent.prototype, "is_disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], FileUploadComponent.prototype, "filterBy", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], FileUploadComponent.prototype, "need_caption", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], FileUploadComponent.prototype, "desc", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], FileUploadComponent.prototype, "handleModelChanged", void 0);
FileUploadComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-file-upload',
        template: __webpack_require__("../../../../../src/app/components/file-upload/file-upload.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_upload_service__["a" /* UploadService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_upload_service__["a" /* UploadService */]) === "function" && _b || Object])
], FileUploadComponent);

var _a, _b;
//# sourceMappingURL=file-upload.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/freetext/freetext.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FreetextComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FreetextComponent = (function () {
    function FreetextComponent(el) {
        this.el = el;
        this.elementId = "";
        // @Input() model: any = "";
        this.disabled = false;
        // @Output() HandleStateChange: EventEmitter<any> = new EventEmitter<any>();
        this.HandleStateChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.base_api = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */];
        this.insideElementId = "";
        this.htmlContent = "";
    }
    FreetextComponent.prototype.ngOnInit = function () {
    };
    FreetextComponent.prototype.ngAfterViewInit = function () {
        this.insideElementId = "app-freetext #" + this.elementId;
        console.log(this.insideElementId);
        tinymce.init({
            selector: this.insideElementId,
            theme: 'modern',
            plugins: ['textcolor colorpicker'],
            skin_url: '../../Content/paf/assets/skins/lightgray',
            height: 200,
            menubar: false,
            toolbar: " undo redo | bold italic underline | numlist | forecolor | removeformat | spellchecker",
            statusbar: true,
            image_advtab: true,
            paste_data_images: true,
            readonly: this.disabled,
            setup: this.tinyMCESetup.bind(this),
        });
    };
    FreetextComponent.prototype.ngOnChanges = function (e) {
        // if (e.model.currentValue !== e.model.previousValue) {
        //   // $(`#${this.elementId}`).html(e.model.currentValue);
        //   if(tinymce.get(this.elementId) && e.model.currentValue) {
        //     tinymce.get(this.elementId).setContent(e.model.currentValue);
        //     // tinymce.get(this.elementId).focus();
        //     // tinymce.activeEditor.setContent(e.model.currentValue, {format: 'raw'});
        //   }
        if (e.disabled && e.disabled.currentValue) {
            tinymce.get(this.elementId).getBody().setAttribute('contenteditable', false);
        }
        // }
    };
    FreetextComponent.prototype.tinyMCESetup = function (e) {
        e.on('keyup change', this.tinyMCEOnKeyup.bind(this));
        // e.on('change', this.tinyMCEOnChange.bind(this));
    };
    // tinyMCEOnChange(e) {
    //   let content = tinymce.get(this.elementId).getContent();
    //   console.log(`tinyMCEOnChange`, content);
    //   // this.HandleStateChange.emit(content);
    // }
    FreetextComponent.prototype.tinyMCEOnKeyup = function (e) {
        var content = tinymce.get(this.elementId).getContent();
        // console.log(`tinyMCEOnKeyup`, content);
        this.HandleStateChange.emit(content);
    };
    FreetextComponent.prototype.ngOnDestroy = function () {
        //destroy cloned elements
        tinymce.get(this.elementId).remove();
        // let elem = document.getElementById(this.elementId);
        // elem.parentElement.removeChild(elem);
    };
    Object.defineProperty(FreetextComponent.prototype, "model", {
        set: function (c) {
            c = c || "";
            if (tinymce.get(this.elementId)) {
                tinymce.get(this.elementId).setContent(c);
            }
        },
        enumerable: true,
        configurable: true
    });
    return FreetextComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], FreetextComponent.prototype, "elementId", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], FreetextComponent.prototype, "disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", Object)
], FreetextComponent.prototype, "HandleStateChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])('model'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], FreetextComponent.prototype, "model", null);
FreetextComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-freetext',
        template: "<textarea id=\"{{elementId}}\" [disabled]=\"disabled\"></textarea>",
        styleUrls: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _a || Object])
], FreetextComponent);

var _a;
//# sourceMappingURL=freetext.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/input-currency/input-currency.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__ = __webpack_require__("../../../../../src/app/pipes/currency.pipe.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputCurrencyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputCurrencyComponent = (function () {
    function InputCurrencyComponent(elementRef, currencyPipe) {
        this.elementRef = elementRef;
        this.currencyPipe = currencyPipe;
        this.el = this.elementRef.nativeElement;
    }
    InputCurrencyComponent.prototype.ngOnInit = function () {
        this.el.value = this.currencyPipe.transform(this.el.value);
    };
    InputCurrencyComponent.prototype.onFocus = function (value) {
        console.log(value);
        this.el.value = this.currencyPipe.parse(value); // opossite of transform
    };
    InputCurrencyComponent.prototype.onBlur = function (value) {
        console.log(value);
        this.el.value = this.currencyPipe.transform(value);
    };
    InputCurrencyComponent.prototype.onAmountChange = function (d) {
        console.log(d);
    };
    return InputCurrencyComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* HostListener */])("focus", ["$event.target.value"]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], InputCurrencyComponent.prototype, "onFocus", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* HostListener */])("blur", ["$event.target.value"]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], InputCurrencyComponent.prototype, "onBlur", null);
InputCurrencyComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-input-currency',
        template: "<input type=\"text\" \n       type=\"text\" \n       class=\"form-control\"\n       [disabled]=\"is_disabled\"\n       [ngModel]=\"amount\" \n       (ngModelChange)=\"onAmountChange($event)\" \n       currency\n   />",
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__["a" /* CurrencyPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__["a" /* CurrencyPipe */]) === "function" && _b || Object])
], InputCurrencyComponent);

var _a, _b;
//# sourceMappingURL=input-currency.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/input-date/input-date.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDateComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InputDateComponent = (function () {
    function InputDateComponent(el) {
        this.el = el;
        this.model = "";
        // @Input() disabled: boolean = false;
        this.HandleStateChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    InputDateComponent.prototype.ngOnInit = function () {
    };
    InputDateComponent.prototype.ngAfterViewInit = function () {
        $(this.el.nativeElement).find('.input-date-picker').daterangepicker({
            autoClose: true,
            singleDate: true,
            showShortcuts: false,
            locale: {
                format: 'DD/MM/YYYY'
            },
        });
    };
    InputDateComponent.prototype.ngOnDestroy = function () {
    };
    return InputDateComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], InputDateComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], InputDateComponent.prototype, "HandleStateChange", void 0);
InputDateComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-input-date',
        template: "\n    <input type=\"text\" id=\"\" class=\"form-control input-date-picker\"  value=\"\">\n  ",
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _b || Object])
], InputDateComponent);

var _a, _b;
//# sourceMappingURL=input-date.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/input-decimal/input-decimal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__ = __webpack_require__("../../../../../src/app/pipes/currency.pipe.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDecimalComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputDecimalComponent = (function () {
    function InputDecimalComponent(elementRef, currencyPipe) {
        this.elementRef = elementRef;
        this.currencyPipe = currencyPipe;
        this.model = "";
        this.modelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.model_display = "";
        this.el = this.elementRef.nativeElement;
    }
    InputDecimalComponent.prototype.ngOnInit = function () {
    };
    InputDecimalComponent.prototype.ngAfterViewInit = function () {
        var context = this;
        var typingTimer; //timer identifier
        var doneTypingInterval = 1000; //time in ms, 5 second for example
        var $input = $('ptx-input-decimal > input');
        console.log($('ptx-input-decimal > input'));
        //on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });
        //on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });
        //user is "finished typing," do something
        function doneTyping() {
            //do something
            console.log('done', context.model);
        }
    };
    InputDecimalComponent.prototype.UpdateModel = function ($event) {
        this.model = $event;
    };
    InputDecimalComponent.prototype.showModel = function (model) {
        return model;
        // return this.currencyPipe.transform(model);
    };
    return InputDecimalComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], InputDecimalComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], InputDecimalComponent.prototype, "modelChange", void 0);
InputDecimalComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-input-decimal',
        template: "\n    <input\n      class=\"form-control\"\n      type=\"text\"\n      id=\"ss\"\n      name=\"decimal\"\n\n      [ngModel]=\"showModel(model)\"\n      (ngModelChange)=\"UpdateModel($event)\">\n      <pre>{{ model_display | json }}</pre>\n  ",
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__["a" /* CurrencyPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__["a" /* CurrencyPipe */]) === "function" && _c || Object])
], InputDecimalComponent);

var _a, _b, _c;
//# sourceMappingURL=input-decimal.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/input-float/input-float.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-inline ptx-form-inline\">\n  <div class=\"form-group ptx-from-group\">\n    <ptx-sugester\n      [class_name]=\"class_name\"\n      [model]=\"formula\"\n      [data_list]=\"data_list\"\n      [is_disabled]=\"disabled\"\n      [show_field]=\"'VALUE'\"\n      select_field=\"ID\"\n      (ModelChanged)=\"formula = $event; HandleModelChange();\"\n    >\n    </ptx-sugester>\n    <!-- <select class=\"form-control\" style=\"width: 90%\">\n      <option>FOBK</option>\n      <option>FOBK2</option>\n    </select> -->\n  </div>\n  <div class=\"form-group ptx-from-group\">\n    <select\n      class=\"form-control ptx-w-40\"\n      [disabled]=\"disabled\"\n      [(ngModel)]=\"sign\"\n      (ngModelChange)=\"HandleModelChange();\"\n    >\n      <option value=\"+\">+</option>\n      <option value=\"-\">-</option>\n    </select>\n  </div>\n  <div class=\"form-group ptx-from-group\">\n    <input\n      type=\"text\"\n      class=\"form-control ptx-w-100 text-center\"\n      [disabled]=\"disabled\"\n      [(ngModel)]=\"real_number\"\n      (ngModelChange)=\"HandleModelChange();\"\n      ptxRestrictNumber\n    />\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/input-float/input-float.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputFloatComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputFloatComponent = (function () {
    function InputFloatComponent() {
        this.data_list = [];
        this.disabled = false;
        this.class_name = "ptx-w-90";
        this.HandleModelUpdate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.formula = '';
        this.real_number = '';
        this.sign = '+';
        this.model = "";
    }
    InputFloatComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(InputFloatComponent.prototype, "setModel", {
        set: function (c) {
            this.model = c;
            var factor = __WEBPACK_IMPORTED_MODULE_1__common__["a" /* Utility */].clone(this.model && this.model.split('|'));
            this.formula = (factor && factor.length > 0 && factor[0]) || '';
            this.sign = (factor && factor.length > 1 && factor[1]) || '+';
            this.real_number = (factor && factor.length > 2 && factor[2]) || '';
            if (this.formula && (!this.real_number || this.real_number.trim() == "")) {
                this.real_number = '0';
            }
        },
        enumerable: true,
        configurable: true
    });
    InputFloatComponent.prototype.HandleModelChange = function () {
        var real_number = this.real_number || '0';
        if (!this.formula) {
            this.HandleModelUpdate.emit(null);
            return;
        }
        if (this.formula && (!this.real_number || this.real_number.trim() == "")) {
            this.real_number = '0';
        }
        var model = this.formula + "|" + this.sign + "|" + real_number;
        console.log(model);
        this.HandleModelUpdate.emit(model);
    };
    return InputFloatComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], InputFloatComponent.prototype, "data_list", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], InputFloatComponent.prototype, "disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], InputFloatComponent.prototype, "class_name", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], InputFloatComponent.prototype, "HandleModelUpdate", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])('model'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], InputFloatComponent.prototype, "setModel", null);
InputFloatComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-input-float',
        template: __webpack_require__("../../../../../src/app/components/input-float/input-float.component.html"),
    }),
    __metadata("design:paramtypes", [])
], InputFloatComponent);

var _a;
//# sourceMappingURL=input-float.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/input-quantity/input-quantity.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"form-inline ptx-form-inline\">\n  <div class=\"form-group ptx-from-group\">\n    <input\n      type=\"text\"\n      class=\"form-control ptx-w-100 text-center\"\n      [ngModel]=\"Show('EXACT_VAL')\"\n      [disabled]=\"disabled\"\n      ptxRestrictNumber\n      (ngModelChange)=\"HandleModelChage('EXACT_VAL', $event)\"\n    />\n  </div>\n  <div class=\"form-group ptx-from-group\">\n    <span class=\"input-group-addon\">±</span>\n  </div>\n  <div class=\"form-group ptx-from-group\">\n    <input\n      type=\"text\"\n      class=\"form-control ptx-w-100 text-center\"\n      [ngModel]=\"Show('TOLERANCE_VAL')\"\n      [disabled]=\"disabled\"\n      ptxRestrictNumber\n      (ngModelChange)=\"HandleModelChage('TOLERANCE_VAL', $event)\"\n    />\n  </div> %\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/input-quantity/input-quantity.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputQuantityComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InputQuantityComponent = (function () {
    function InputQuantityComponent() {
        this.disabled = false;
        this.HandleModelUpdate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    InputQuantityComponent.prototype.ngOnInit = function () {
    };
    InputQuantityComponent.prototype.Show = function (e) {
        if (!this.model) {
            return '';
        }
        var factor = this.model.split(' ');
        var index = null;
        switch (e) {
            case "EXACT_VAL":
                index = 0;
                break;
            case "TOLERANCE_VAL":
                index = 2;
                break;
            default:
                // code...
                break;
        }
        // console.log(index, factor)
        // factor = _.extend([], factor);
        return (factor && factor.length > index && factor[index] && factor[index]) || '';
    };
    InputQuantityComponent.prototype.HandleModelChage = function (e, val) {
        var factor = __WEBPACK_IMPORTED_MODULE_1__common__["a" /* Utility */].clone(this.model && this.model.split(' '));
        var exact = (factor && factor.length && factor[0]) || 0;
        var tol = (factor && factor.length && factor.length > 2 && factor[2]) || 0;
        switch (e) {
            case "EXACT_VAL":
                exact = val;
                break;
            case "TOLERANCE_VAL":
                tol = val;
                break;
            default:
                // code...
                break;
        }
        exact = exact || 0;
        tol = tol || 0;
        this.HandleModelUpdate.emit(exact + " +/- " + tol);
    };
    return InputQuantityComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], InputQuantityComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], InputQuantityComponent.prototype, "disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], InputQuantityComponent.prototype, "HandleModelUpdate", void 0);
InputQuantityComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-input-quantity',
        template: __webpack_require__("../../../../../src/app/components/input-quantity/input-quantity.component.html"),
    }),
    __metadata("design:paramtypes", [])
], InputQuantityComponent);

var _a;
//# sourceMappingURL=input-quantity.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/input-suggestion/input-suggestion.component.html":
/***/ (function(module, exports) {

module.exports = "<input\n  [ngModel]=\"model\"\n  [typeahead]=\"sources\"\n  [typeaheadMinLength]=\"0\"\n  typeaheadOptionField=\"text\"\n  (typeaheadOnSelect)=\"log($event)\"\n  class=\"form-control\"\n>"

/***/ }),

/***/ "../../../../../src/app/components/input-suggestion/input-suggestion.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputSuggestionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InputSuggestionComponent = (function () {
    function InputSuggestionComponent() {
        this.OnSelected = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    InputSuggestionComponent.prototype.ngOnInit = function () {
    };
    InputSuggestionComponent.prototype.log = function (e) {
        console.log(e);
    };
    return InputSuggestionComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], InputSuggestionComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], InputSuggestionComponent.prototype, "sources", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], InputSuggestionComponent.prototype, "OnSelected", void 0);
InputSuggestionComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'input-suggestion',
        template: __webpack_require__("../../../../../src/app/components/input-suggestion/input-suggestion.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [])
], InputSuggestionComponent);

var _a;
//# sourceMappingURL=input-suggestion.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/loading-div/loading-div.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingDivComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingDivComponent = (function () {
    function LoadingDivComponent() {
    }
    LoadingDivComponent.prototype.ngOnInit = function () {
    };
    return LoadingDivComponent;
}());
LoadingDivComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-loading-div',
        template: "<div class=\"loader\"></div>",
        // templateUrl: './loading-div.component.html',
        styles: ["\n      .loader {\n          border: 10px solid #f3f3f3; /* Light grey */\n          border-top: 10px solid #3498db; /* Blue */\n          border-radius: 50%;\n          width: 50px;\n          height: 50px;\n          animation: spin 2s linear infinite;\n      }\n\n      @keyframes spin {\n          0% { transform: rotate(0deg); }\n          100% { transform: rotate(360deg); }\n      }"
        ]
    }),
    __metadata("design:paramtypes", [])
], LoadingDivComponent);

//# sourceMappingURL=loading-div.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/new-freetext/new-freetext.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewFreetextComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};

var NewFreetextComponent = (function () {
    function NewFreetextComponent(elementRef) {
        this.elementRef = elementRef;
        var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
        var uniqid = randLetter + Date.now();
        this.elementID = 'tinymce' + uniqid;
        this.contentChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    NewFreetextComponent.prototype.ngAfterViewInit = function () {
        //Clone base textarea
        var baseTextArea = this.elementRef.nativeElement.querySelector("#baseTextArea");
        var clonedTextArea = baseTextArea.cloneNode(true);
        clonedTextArea.id = this.elementID;
        var formGroup = this.elementRef.nativeElement.querySelector("#tinyFormGroup");
        formGroup.appendChild(clonedTextArea);
        //Attach tinyMCE to cloned textarea
        tinymce.init({
            mode: 'exact',
            height: 500,
            theme: 'modern',
            skin_url: '../../Content/paf/assets/skins/lightgray',
            plugins: ['textcolor colorpicker'],
            // plugins: [
            //     'advlist autolink lists link image charmap print preview anchor',
            //     'searchreplace visualblocks code fullscreen',
            //     'insertdatetime media table contextmenu paste code'
            // ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            elements: this.elementID,
            setup: this.tinyMCESetup.bind(this)
        });
    };
    NewFreetextComponent.prototype.ngOnDestroy = function () {
        //destroy cloned elements
        tinymce.get(this.elementID).remove();
        var elem = document.getElementById(this.elementID);
        elem.parentElement.removeChild(elem);
    };
    NewFreetextComponent.prototype.tinyMCESetup = function (ed) {
        ed.on('keyup', this.tinyMCEOnKeyup.bind(this));
    };
    NewFreetextComponent.prototype.tinyMCEOnKeyup = function (e) {
        this.contentChanged.emit(tinymce.get(this.elementID).getContent());
    };
    Object.defineProperty(NewFreetextComponent.prototype, "model", {
        set: function (content) {
            this.htmlContent = content;
        },
        enumerable: true,
        configurable: true
    });
    return NewFreetextComponent;
}());
NewFreetextComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-new-freetext',
        template: "<div id=\"tinyFormGroup\" class=\"form-group\">\n    <div class=\"hidden\">\n      <textarea id=\"baseTextArea\">{{htmlContent}}</textarea>\n    </div>\n  </div>",
        inputs: ['model', 'elementId'],
        outputs: ['contentChanged']
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["g" /* Inject */])(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _a || Object])
], NewFreetextComponent);

var _a;
//# sourceMappingURL=new-freetext.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/shared-form/shared-form.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model | json}}</pre> -->\n<!-- <pre>{{ criteria | json}}</pre> -->\n<div class=\"wrapper-detail\">\n  <h2 class=\"first-title\">\n    <span class=\"note\">{{ title }}</span> Please fill in form as relevant\n  </h2>\n  <div class=\"form-horizontal\">\n    <div class=\"row\">\n      <div class=\"col-xs-3\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-3 control-label\">Form Id</label>\n          <div class=\"col-xs-9\">\n            <input class=\"form-control\" [(ngModel)]=\"model.PDA_FORM_ID\" type=\"text\" [disabled]=\"true\">\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-9\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-2 control-label\">Created Date</label>\n          <div class=\"col-xs-3\">\n            <ptx-datepicker\n              [disabled]=\"true\"\n              [model]=\"model.PDA_CREATED\"\n            ></ptx-datepicker>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-xs-3\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-3 control-label\">For</label>\n          <div class=\"col-xs-9\">\n            <select class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_FOR_COMPANY\"\n              (ngModelChange)=\"OnChangeCompany.emit(model.PDA_FOR_COMPANY)\"\n            >\n              <option value=\"\">-- Select --</option>\n              <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.COMPANY\">{{ elem.VALUE }}</option>\n            </select>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-9\">\n        <div class=\"form-group\" *ngIf=\"is_cmla_import\">\n          <label class=\"col-xs-2 control-label\">Type of Purchasing</label>\n          <div class=\"col-xs-3\">\n            <select class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_SUB_TYPE\"\n            >\n              <option value=\"\"> -- Select -- </option>\n              <option value=\"Spot Purchasing\">Spot Purchasing</option>\n              <option value=\"Term Purchasing\">Term Purchasing</option>\n              <option value=\"Others\">Others</option>\n            </select>\n          </div>\n          <div class=\"col-xs-7\">\n            <input\n              class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_TYPE_NOTE\"\n              type=\"text\">\n          </div>\n        </div>\n      <div class=\"col-xs-9\">\n        <div class=\"form-group\" *ngIf=\"!is_cmps_import && !is_cmla_import\">\n          <label class=\"col-xs-2 control-label\">Type of sale</label>\n          <div class=\"col-xs-3\">\n            <select class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_SUB_TYPE\"\n            >\n              <option value=\"\"> -- Select -- </option>\n              <option value=\"Spot sale\">Spot sale</option>\n              <option value=\"Term sale\">Term sale</option>\n              <option value=\"Others\">Others</option>\n            </select>\n          </div>\n          <div class=\"col-xs-7\">\n            <input\n              class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_TYPE_NOTE\"\n              type=\"text\">\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"is_cmps_import && !is_cmla_import\">\n          <label class=\"col-xs-2 control-label\">Type of import</label>\n          <div class=\"col-xs-3\">\n            <select\n              class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_SUB_TYPE\">\n              <option value=\"\"> -- Select -- </option>\n              <option value=\"Spot import\">Spot import</option>\n              <option value=\"Term import\">Term import</option>\n              <option value=\"Others\">Others</option>\n            </select>\n          </div>\n          <div class=\"col-xs-7\">\n            <input\n              class=\"form-control\"\n              [disabled]=\"model.IS_DISABLED\"\n              [(ngModel)]=\"model.PDA_TYPE_NOTE\"\n              type=\"text\">\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-xs-12\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-12 control-label\">Brief Market Situation (as necessary)</label>\n          <div class=\"col-xs-12\">\n            <app-freetext\n              *ngIf=\"true\"\n              [disabled]=\"model.IS_DISABLED_NOTE\"\n              [elementId]=\"'PDA_BRIEF'\"\n              [model]=\"model.PDA_BRIEF\"\n              (HandleStateChange)=\"model.PDA_BRIEF = $event\">\n            </app-freetext>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"row\">\n      <ptx-file-upload\n        btnClass=\"text-left\"\n        [model]=\"model.PAF_ATTACH_FILE\"\n        [is_disabled]=\"model.IS_DISABLED\"\n        [need_caption]=\"true\"\n        [desc]=\"'Image files'\"\n        filterBy=\"BMS\"\n        (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\"\n      >\n      </ptx-file-upload>\n    </div>\n    <br />\n    <div class=\"row\" *ngIf=\"is_cmps_dom\">\n      <div class=\"col-xs-12\">\n        <div class=\"checkbox\">\n          <input\n            id=\"PDA_INCLUDED_REVIEW_TABLE\"\n            type=\"checkbox\"\n            [disabled]=\"model.IS_DISABLED\"\n            [ngModel]=\"model.PDA_INCLUDED_REVIEW_TABLE == 'Y'\"\n            (ngModelChange)=\"model.PDA_INCLUDED_REVIEW_TABLE = $event ? 'Y': 'N'\"\n          >\n          <!-- <input id=\"PDA_INCLUDED_REVIEW_TABLE\" type=\"checkbox\" [(ngModel)]=\"model.PDA_INCLUDED_REVIEW_TABLE\"> -->\n          <label for=\"PDA_INCLUDED_REVIEW_TABLE\">\n            Applied for \"Customers request to review the existing price\"\n          </label>\n        </div>\n      </div>\n    </div>\n  </div>\n<!-- </div> -->\n  <div class=\"form-horizontal\">\n    <ng-template [ngIf]=\"is_cmps_dom && model.PDA_INCLUDED_REVIEW_TABLE == 'Y'\">\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <!-- <div class=\"wrapper-table\"> -->\n          <div class=\"table-responsive\">\n            <table id=\"\" class=\"table table-bordered plutonyx-table\">\n              <thead>\n                <tr>\n                  <th rowspan=\"3\"> Customer </th>\n                  <th rowspan=\"3\">Product</th>\n                  <th rowspan=\"3\">Unit</th>\n                  <th colspan=\"6\">\n                    <div class=\"col-md-2 col-md-offset-3\">Contract</div>\n                    <div class=\"col-md-2\">\n                      <!-- <ptx-sugester\n                        [model]=\"model.PDA_PRICE_UNIT\"\n                        [data_list]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        [is_disabled]=\"false\"\n                        [show_field]=\"'VALUE'\"\n                        [select_field]=\"'ID'\"\n                        (ModelChanged)=\"model.PDA_PRICE_UNIT = $event\"\n                      >\n                      </ptx-sugester> -->\n                      <!-- <input\n                        [(ngModel)]=\"model.PDA_PRICE_UNIT\"\n                        [typeahead]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        [disabled]=\"is_disabled\"\n                        placeholder=\"\"\n                        typeaheadOptionField=\"VALUE\"\n                        [typeaheadMinLength]=\"0\"\n                        class=\"form-control\"> -->\n                      <!-- <select2\n                        *ngIf=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        [data]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        [value]=\"model.PDA_PRICE_UNIT\"\n                        (valueChanged)=\"model.PDA_PRICE_UNIT = $event && $event.value\"\n                      ></select2> -->\n                      <!-- <select class=\"form-control\">\n                        <option>$/BBL</option>\n                      </select> -->\n                    </div>\n                  </th>\n                  <th rowspan=\"3\" width=\"50px\">Note</th>\n                  <th rowspan=\"3\" width=\"50px\"></th>\n                </tr>\n                <tr>\n                  <th colspan=\"2\">Existing</th>\n                  <th colspan=\"2\">Customer Proposal</th>\n                  <th colspan=\"2\">Our Proposal</th>\n                </tr>\n                <tr>\n                  <th width=\"25%\">Quantity</th>\n                  <th nowrap>Price or Formula</th>\n                  <th width=\"25%\">Quantity</th>\n                  <th nowrap>Price or Formula</th>\n                  <th width=\"25%\">Quantity</th>\n                  <th nowrap>Price or Formula</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngIf=\"model.PAF_REVIEW_ITEMS.length == 0\">\n                  <td colspan=\"11\" class=\"text-center\">No Data</td>\n                </tr>\n                <ng-template ngFor let-row [ngForOf]=\"model.PAF_REVIEW_ITEMS\" let-i=\"index\">\n                  <ng-template ngFor let-elem [ngForOf]=\"row.PAF_REVIEW_ITEMS_DETAIL\" let-j=\"index\">\n                    <tr>\n                      <td class=\"min-300\">\n                        <div class=\"col-md-2\" *ngIf=\"j == 0\">\n                          <input\n                            type=\"radio\"\n                            name=\"criteria.TARGET_PRODUCT\"\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"criteria.TARGET_PRODUCT\"\n                            [value]=\"i\"\n                          >\n                        </div>\n                        <div class=\"ptx-col-xs-10\" *ngIf=\"j == 0\">\n                          <ptx-sugester\n                            [model]=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_CUSTOMER\"\n                            [data_list]=\"master.CUSTOMERS\"\n                            [is_disabled]=\"model.IS_DISABLED || model.PAF_REVIEW_ITEMS[i].SELECTED\"\n                            [show_field]=\"'VALUE'\"\n                            [select_field]=\"'ID'\"\n                            (ModelChanged)=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_CUSTOMER = $event\"\n                          >\n                          </ptx-sugester>\n                          <!-- <input\n                            *ngIf=\"j == 0\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_CUSTOMER\"\n                            [typeahead]=\"master.CUSTOMERS\"\n                            [disabled]=\"is_disabled\"\n                            placeholder=\"\"\n                            typeaheadOptionField=\"VALUE\"\n                            [typeaheadMinLength]=\"0\"\n                            class=\"form-control\"> -->\n                        </div>\n                      </td>\n                      <td class=\"min-300\">\n                        <ptx-sugester\n                          *ngIf=\"j == 0\"\n                          [model]=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_MATERIAL\"\n                          [data_list]=\"master.PRODUCTS_DOM\"\n                          [is_disabled]=\"model.IS_DISABLED\"\n                          [show_field]=\"'VALUE'\"\n                          select_field=\"ID\"\n                          (ModelChanged)=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_MATERIAL = $event\"\n                        >\n                        </ptx-sugester>\n                        <!-- <input\n                            *ngIf=\"j == 0\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PRI_FK_MATERIAL\"\n                            [typeahead]=\"master.PRODUCTS\"\n                            [disabled]=\"is_disabled\"\n                            placeholder=\"\"\n                            typeaheadOptionField=\"VALUE\"\n                            [typeaheadMinLength]=\"0\"\n                            class=\"form-control\"> -->\n                      </td>\n                      <td class=\"min-100\">\n                        <ptx-sugester\n                          *ngIf=\"j == 0\"\n                          [model]=\"model.PAF_REVIEW_ITEMS[i].PRI_QUANTITY_UNIT\"\n                          [data_list]=\"master.QUANTITY_UNITS\"\n                          [is_disabled]=\"model.IS_DISABLED\"\n                          [show_field]=\"'VALUE'\"\n                          select_field=\"ID\"\n                          (ModelChanged)=\"model.PAF_REVIEW_ITEMS[i].PRI_QUANTITY_UNIT = $event\"\n                        >\n                        </ptx-sugester>\n                        <!-- <input\n                            *ngIf=\"j == 0\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PRI_QUANTITY_UNIT\"\n                            [typeahead]=\"master.QUANTITY_UNITS\"\n                            [disabled]=\"is_disabled\"\n                            placeholder=\"\"\n                            typeaheadOptionField=\"VALUE\"\n                            [typeaheadMinLength]=\"0\"\n                            class=\"form-control\"> -->\n                      </td>\n                      <td class=\"min-200\">\n                        <div class=\"input-group\">\n                          <input\n                            type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_EXISTING_QUANTITY_MIN\"\n                          />\n                          <span class=\"input-group-addon\">-</span>\n                          <input\n                            type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_EXISTING_QUANTITY_MAX\"/>\n                        </div>\n                      </td>\n                      <td class=\"min-200\">\n                          <input type=\"text\"\n                            class=\"form-control\"\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_EXISTING_PRICE_FLOAT\"/>\n                      </td>\n                      <td class=\"min-200\">\n                        <div class=\"input-group\">\n                          <input type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_CUS_PROPOSAL_QUANTITY_MIN\"/>\n                          <span class=\"input-group-addon\">-</span>\n                          <input type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_CUS_PROPOSAL_QUANTITY_MAX\"/>\n                        </div>\n                      </td>\n                      <td class=\"min-200\">\n                        <input type=\"text\"\n                          class=\"form-control\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_CUS_PROPOSAL_PRICE_FLOAT\"/>\n                      </td>\n                      <td class=\"min-200\">\n                        <div class=\"input-group\">\n                          <input type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_OUR_PROPOSAL_QUANTITY_MIN\"/>\n                          <span class=\"input-group-addon\">-</span>\n                          <input type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_OUR_PROPOSAL_QUANTITY_MAX\"/>\n                        </div>\n                      </td>\n                      <td class=\"min-200\">\n                        <input type=\"text\"\n                          class=\"form-control\"\n                            [disabled]=\"model.IS_DISABLED\"\n                          [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_OUR_PROPOSAL_PRICE_FLOAT\"/>\n                      </td>\n                      <td class=\"min-200\">\n                        <input type=\"text\"\n                          *ngIf=\"j == 0\"\n                          class=\"form-control\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [(ngModel)]=\"model.PAF_REVIEW_ITEMS[i].PAF_REVIEW_ITEMS_DETAIL[j].PRD_NOTE\"/>\n                      </td>\n                      <td>\n                        <a (click)=\"RemovePAFReviewItems(i, j, row)\">\n                          <span class=\"glyphicon glyphicon-trash\"></span>\n                        </a>\n                      </td>\n                    </tr>\n                  </ng-template>\n                </ng-template>\n              </tbody>\n            </table>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <button class=\"btn btn-success\" (click)=\"AddCustomerRequest()\"> Add Customer</button>\n          <button class=\"btn btn-primary\" (click)=\"AddCustomerRequestTier()\"> Add Tier</button>\n        </div>\n      </div>\n      <br/>\n    </ng-template>\n\n    <div class=\"row\" *ngIf=\"is_cmps_dom\">\n      <div class=\"col-xs-12\">\n        <div class=\"checkbox\">\n          <input\n            id=\"PDA_INCLUDED_BIDDING_TABLE\"\n            type=\"checkbox\"\n            [disabled]=\"model.IS_DISABLED\"\n            [ngModel]=\"model.PDA_INCLUDED_BIDDING_TABLE == 'Y'\"\n            (ngModelChange)=\"model.PDA_INCLUDED_BIDDING_TABLE = $event ? 'Y': 'N'\"\n          >\n          <label for=\"PDA_INCLUDED_BIDDING_TABLE\">\n             Applied for approval\n          </label>\n        </div>\n      </div>\n    </div>\n\n    <ng-template [ngIf]=\"is_cmps_dom && model.PDA_INCLUDED_BIDDING_TABLE == 'Y'\">\n      <!-- <pre>{{ model.PAF_BIDDING_ITEMS | json }}</pre> -->\n      <div class=\"row\">\n        <div class=\"col-xs-12\">\n          <!-- <div class=\"wrapper-table\"> -->\n          <!-- <pre>{{ model.PAF_BIDDING_ITEMS |json }}</pre> -->\n          <div class=\"table-responsive\">\n            <table id=\"\" class=\"table table-bordered plutonyx-table\">\n              <thead>\n                <tr>\n                  <th rowspan=\"2\"></th>\n                  <th rowspan=\"2\"> Customer </th>\n                  <th rowspan=\"2\">Contact Person</th>\n                  <th rowspan=\"2\">Product</th>\n                  <th rowspan=\"2\">Unit</th>\n                  <th rowspan=\"2\">Quantity</th>\n                  <th align=\"center\" colspan=\"6\">\n                    <div class=\"col-md-4 col-md-offset-2\">Price or formula</div>\n                    <div class=\"col-xs-2\">\n                    <input\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PDA_PRICE_UNIT\"\n                        [typeahead]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        placeholder=\"\"\n                        typeaheadOptionField=\"VALUE\"\n                        [typeaheadMinLength]=\"0\"\n                        class=\"form-control\">\n                      <!-- <select2\n                        *ngIf=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        [data]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                        [value]=\"model.PDA_PRICE_UNIT\"\n                        (valueChanged)=\"model.PDA_PRICE_UNIT = $event && $event.value\"\n                      ></select2> -->\n                    </div>\n                  </th>\n                  <th rowspan=\"2\">Note</th>\n                  <th rowspan=\"2\"></th>\n                </tr>\n                <tr>\n                  <th nowrap>Pricing Period</th>\n                  <th nowrap>Existing Price</th>\n                  <th nowrap>Export Price</th>\n                  <th nowrap>Delta Price</th>\n                  <th nowrap>Market/Tier Price</th>\n                  <th nowrap>Offer Price</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngIf=\"model.PAF_BIDDING_ITEMS.length == 0\">\n                  <td colspan=\"11\" class=\"text-center\">No Data</td>\n                </tr>\n                <tr *ngFor=\"let row of model.PAF_BIDDING_ITEMS; let index = index;\">\n                  <td>\n                    <div class=\"checkbox\">\n                      <input\n                        id=\"other_{{index}}\"\n                        type=\"checkbox\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [ngModel]=\"GetSelected(index)\"\n                        (ngModelChange)=\"SetSelected($event, index);OnSelectForAward()\">\n                      <label for=\"other_{{index}}\"></label>\n                    </div>\n                  </td>\n                  <td class=\"min-200\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER\"\n                      [data_list]=\"master.CUSTOMERS\"\n                      [is_disabled]=\"model.IS_DISABLED || GetSelected(index)\"\n                      [show_field]=\"'VALUE'\"\n                      [select_field]=\"'ID'\"\n                      (ModelChanged)=\"model.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER = $event\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_CONTACT_PERSON\"/>\n                  </td>\n                  <td class=\"min-300\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BIDDING_ITEMS[index].PBI_FK_MATERIAL\"\n                      [data_list]=\"master.PRODUCTS_DOM\"\n                      [is_disabled]=\"model.IS_DISABLED || GetSelected(index)\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"model.PAF_BIDDING_ITEMS[index].PBI_FK_MATERIAL = $event;\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td class=\"min-100\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BIDDING_ITEMS[index].PBI_QUANTITY_UNIT\"\n                      [data_list]=\"master.QUANTITY_UNITS\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"model.PAF_BIDDING_ITEMS[index].PBI_QUANTITY_UNIT = $event\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_QUANTITY_MIN\"/>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_PRICING_PERIOD\"/>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_EXISTING_PRICE\"/>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_EXPORT_PRICE\"/>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_DELTA_PRICE\"/>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_MARKET_PRICE\"/>\n                  </td>\n                  <td class=\"min-100\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_OFFER_PRICE\"/>\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BIDDING_ITEMS[index].PBI_NOTE\"/>\n                  </td>\n                  <td>\n                    <a (click)=\"RemoveBiddingItemsRequest(row)\">\n                      <span class=\"glyphicon glyphicon-trash\"></span>\n                    </a>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <button class=\"btn btn-success\" (click)=\"AddBidder()\"> Add Customer</button>\n          <!-- <input type=\"submit\" name=\"ctl00$ContentPlaceHolder1$btnAddNewSupplier\" value=\"Add Customer\" onclick=\"CallItemEvent('AddSup');return false;\" id=\"ContentPlaceHolder1_btnAddNewSupplier\" class=\"btn btn-success\"> -->\n        </div>\n        <!-- <div class=\"col-md-6\" style=\"text-align: right;\">\n          <input type=\"submit\" name=\"ctl00$ContentPlaceHolder1$btnAddNewOffer\" value=\"Add New Offer\" onclick=\"CallItemEvent('AddOff');return false;\" id=\"ContentPlaceHolder1_btnAddNewOffer\" class=\"btn btn-primary\">\n          <input type=\"submit\" name=\"ctl00$ContentPlaceHolder1$btnrefresh\" value=\"refresh\" id=\"ContentPlaceHolder1_btnrefresh\" style=\"display: none;\">\n        </div> -->\n      </div>\n      <br />\n    </ng-template>\n    <br />\n    <ng-template [ngIf]=\"is_cmps_inter\">\n      <div class=\"form-horizontal\">\n        <div class=\"row\">\n          <div class=\"col-md-8\">\n              <h3>Product<span style=\"color:red\">*</span></h3>\n              <table class=\"table table-bordered\">\n                  <thead>\n                      <tr>\n                          <th width=\"30%\">Name</th>\n                          <th width=\"20%\">Code</th>\n                          <th width=\"30%\">Description</th>\n                          <th width=\"20%\">Quantity Unit</th>\n                          <th></th>\n                      </tr>\n                  </thead>\n                  <tbody>\n                      <tr *ngFor=\"let product of criteria.products; let index = index;\">\n                        <td>\n                          <ptx-sugester\n                            [model]=\"criteria.products[index].PBI_FK_MATERIAL\"\n                            [data_list]=\"master.PRODUCTS_INTER\"\n                            [is_disabled]=\"model.IS_DISABLED\"\n                            [show_field]=\"'NAME'\"\n                            [select_field]=\"'ID'\"\n                            (ModelChanged)=\"criteria.products[index].PBI_FK_MATERIAL = $event;OnUpdatedProduct();UpdateInterTable();\"\n                          >\n                          </ptx-sugester>\n                        </td>\n                        <td>\n                          <ptx-sugester\n                            [model]=\"criteria.products[index].PBI_FK_MATERIAL\"\n                            [data_list]=\"master.PRODUCTS_INTER\"\n                            [is_disabled]=\"true\"\n                            [show_field]=\"'ID'\"\n                            [select_field]=\"'ID'\"\n                            (ModelChanged)=\"criteria.products[index].PBI_FK_MATERIAL = $event;OnUpdatedProduct();\"\n                          >\n                          </ptx-sugester>\n                        </td>\n                        <td>\n                          <ptx-sugester\n                            [model]=\"criteria.products[index].PBI_FK_MATERIAL\"\n                            [data_list]=\"master.PRODUCTS_INTER\"\n                            [is_disabled]=\"true\"\n                            [show_field]=\"'VALUE'\"\n                            [select_field]=\"'ID'\"\n                            (ModelChanged)=\"criteria.products[index].PBI_FK_MATERIAL = $event;OnUpdatedProduct();\"\n                          >\n                          </ptx-sugester>\n                        </td>\n                        <td>\n                          <ptx-sugester\n                            [model]=\"criteria.products[index].PBI_QUANTITY_UNIT\"\n                            [data_list]=\"master.QUANTITY_UNITS\"\n                            [is_disabled]=\"model.IS_DISABLED\"\n                            [show_field]=\"'VALUE'\"\n                            select_field=\"ID\"\n                            (ModelChanged)=\"criteria.products[index].PBI_QUANTITY_UNIT = $event;UpdateInterTable();\"\n                          >\n                          </ptx-sugester>\n                        </td>\n                        <td>\n                          <a (click)=\"RemoveProductFromCriteria(product)\">\n                            <span class=\"glyphicon glyphicon-trash\"></span>\n                          </a>\n                        </td>\n                      </tr>\n                  </tbody>\n              </table>\n          </div>\n        </div>\n        <div class=\"row\">\n          <button type=\"button\" class=\"btn btn-success\" [disabled]=\"criteria.products.length == 2 || model.IS_DISABLED\" (click)=\"AddNewProduct()\"> Add Product </button>\n          <!-- <button type=\"button\" class=\"btn btn-primary\" [disabled]=\"criteria.products.length == 1\" (click)=\"DelProduct()\"> Delete Product </button> -->\n        </div>\n      </div>\n      <br/>\n      <!-- <pre>{{ criteria.PAF_BIDDING_ITEMS | json}}</pre> -->\n      <!-- <pre>{{ criteria.PAF_BIDDING_ITEMS[0] | json}}</pre> -->\n      <!-- <pre>{{ (criteria.products.length*(4 + model.PAF_BIDDING_ITEMS[0].PAF_ROUND.length)) + 1 | json}}</pre> -->\n      <h4 class=\"second-title\" *ngIf=\"is_cmps_inter\">Applied for \"Product price bidding\"</h4>\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n        <div class=\"fixedTable\">\n          <div class=\"fixedTable-sidebar\">\n            <table id=\"\" class=\"table table-bordered\">\n        <!-- <div style=\"overflow-x: scroll;\">\n            <table id=\"\" class=\"table table-bordered\"> -->\n                <thead>\n                  <tr>\n                      <th class=\"headcol\" rowspan=\"3\"></th>\n                      <th class=\"headcol\" rowspan=\"3\" width=\"15%\" style=\"min-width: 180px;\" *ngIf=\"!is_cmps_import\">Customer</th>\n                      <th class=\"headcol\" rowspan=\"3\" width=\"15%\" style=\"min-width: 180px;\" *ngIf=\"is_cmps_import\">Supplier</th>\n                      <th class=\"headcol\" rowspan=\"3\" width=\"10%\" style=\"min-width: 120px;\">Contact Person</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let row of criteria.PAF_BIDDING_ITEMS; let index = index;\">\n                    <td class=\"fixedcol\">\n                      <div class=\"checkbox\">\n                          <input\n                            id=\"check_got_bid{{index}}\"\n                            type=\"checkbox\"\n                            [disabled]=\"model.IS_DISABLED\"\n                            [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].SELECTED\"\n                            (ngModelChange)=\"OnSelectForAwardInter()\"\n                          >\n                          <label for=\"check_got_bid{{index}}\" style=\"margin-bottom: 0;\"></label>\n                      </div>\n                    </td>\n                    <td class=\"fixedcol\">\n                      <ptx-sugester\n                        [model]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER\"\n                        [data_list]=\"master.CUSTOMERS\"\n                        [is_disabled]=\"model.IS_DISABLED || criteria.PAF_BIDDING_ITEMS[index].SELECTED\"\n                        [show_field]=\"'VALUE'\"\n                        [select_field]=\"'ID'\"\n                        (ModelChanged)=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER = $event; UpdateInterTable();\"\n                      >\n                      </ptx-sugester>\n                    </td>\n                    <!-- <td *ngIf=\"is_cmps_import\">\n                      <ptx-sugester\n                        [model]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FK_VENDOR\"\n                        [data_list]=\"master.VENDOR\"\n                        [is_disabled]=\"false\"\n                        [show_field]=\"'VALUE'\"\n                        [select_field]=\"'ID'\"\n                        (ModelChanged)=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER = $event; UpdateInterTable();\"\n                      >\n                      </ptx-sugester>\n                    </td> -->\n                    <td class=\"fixedcol\">\n                      <input\n                        class=\"text-left form-control\"\n                        type=\"\"\n                        name=\"\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_CONTACT_PERSON\"\n                        (ngModelChange)=\"UpdateInterTable();\"\n                      >\n                    </td>\n                  </tr>\n                </tbody>\n            </table>\n          </div>\n\n          <div style=\"overflow-x: scroll;\">\n            <table id=\"\" class=\"table table-bordered\">\n              <thead>\n                <tr>\n                    <th rowspan=\"2\" [colSpan]=\"criteria.products.length\">Quantity</th>\n                    <th rowspan=\"3\" style=\"min-width: 100px;\">Pricing Period</th>\n                    <th rowspan=\"3\" style=\"min-width: 100px;\">Incoterm</th>\n                    <th rowspan=\"3\" style=\"min-width: 100px;\">Country</th>\n                    <!-- <th *ngIf=\"!is_cmps_import\" [colSpan]=\"(criteria.products.length*(4 + criteria.PAF_BIDDING_ITEMS[0].PAF_ROUND.length)) + 1\"> -->\n                    <th [colSpan]=\"(criteria.products.length*(4 + criteria.PAF_BIDDING_ITEMS[0].PAF_ROUND.length)) + (is_cmps_import?3:2)\">\n                      Bid price or formula\n                      <div style=\"display: inline-block; min-width: 100px; margin-left: 10px;\">\n                        <ptx-sugester\n                          [model]=\"model.PDA_PRICE_UNIT\"\n                          [data_list]=\"master.CURRENCY_PER_VOLUMN_UNITS\"\n                          [is_disabled]=\"model.IS_DISABLED\"\n                          [show_field]=\"'VALUE'\"\n                          [select_field]=\"'ID'\"\n                          (ModelChanged)=\"model.PDA_PRICE_UNIT = $event\"\n                        >\n                        </ptx-sugester>\n                        <!-- <select class=\"form-control\" [(ngModel)]=\"model.PDA_PRICE_UNIT\">\n                          <option>$/BBL</option>\n                        </select> -->\n                      </div>\n                    </th>\n                    <th rowspan=\"3\" width=\"15%\">Note</th>\n                    <th rowspan=\"3\" width=\"15%\"></th>\n                </tr>\n                <tr>\n                    <th [colSpan]=\"criteria.products.length\" *ngFor=\"let round of criteria.PAF_BIDDING_ITEMS[0].PAF_ROUND;let round_index = index\">\n                      <div class=\"inline-block\">{{GetRoundText(round_index + 1)}} Bid</div>\n                      <div class=\"inline-block\">\n                      <ng-template [ngIf]=\"round_index != 0\">\n                        <!-- <br/> -->\n                        <a (click)=\"RemoveRound(round_index)\">\n                          <span class=\"glyphicon glyphicon-trash\"></span>\n                        </a>\n                      </ng-template>\n                      </div>\n                    </th>\n                    <th [rowSpan]=\"2\" [colSpan]=\"1\">Freight</th>\n                    <th [rowSpan]=\"2\" [colSpan]=\"1\" *ngIf=\"is_cmps_import\">Port</th>\n                    <th [colSpan]=\"criteria.products.length\" class=\"min-200\" *ngIf=\"!is_cmps_import\">Converted to FOB</th>\n                    <th [colSpan]=\"criteria.products.length\" class=\"min-200\" *ngIf=\"is_cmps_import\">Converted to CFR</th>\n                    <th [rowSpan]=\"2\" [colSpan]=\"1\">Average</th>\n                    <th [colSpan]=\"criteria.products.length\">Market Price</th>\n                    <th [colSpan]=\"criteria.products.length\" class=\"min-200\" >Latest LP Plan Price</th>\n                    <th [colSpan]=\"criteria.products.length\">Benchmark </th>\n                </tr>\n                <tr>\n                    <th class=\"min-100\" *ngFor=\"let elem of criteria.products\" colspan=\"1\">&nbsp;{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }} {{ (elem.PBI_QUANTITY_UNIT && \"(\" + elem.PBI_QUANTITY_UNIT + \")\") || \"\" }} </th>\n                    <ng-template ngFor let-f [ngForOf]=\"criteria.PAF_BIDDING_ITEMS[0].PAF_ROUND\">\n                      <th width=\"100px\" *ngFor=\"let elem of criteria.products\">{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }}&nbsp;</th>\n                      <!-- <th *ngFor=\"let elem of criteria.products\" style=\"min-width: 150px;\">{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }} ({{ elem.PBI_QUANTITY_UNIT }})</th> -->\n                    </ng-template>\n                    <th *ngFor=\"let elem of criteria.products\" colspan=\"1\">{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }}</th>\n                    <th *ngFor=\"let elem of criteria.products\" colspan=\"1\">{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }}</th>\n                    <th *ngFor=\"let elem of criteria.products\" colspan=\"1\">{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }}</th>\n                    <th *ngFor=\"let elem of criteria.products\" colspan=\"1\">{{ ShowProductNickName(elem.PBI_FK_MATERIAL) }}</th>\n                </tr>\n              </thead>\n              <tbody>\n                <tr *ngFor=\"let row of criteria.PAF_BIDDING_ITEMS; let index = index;\">\n                  <ng-template ngFor let-i=\"index\" let-elem [ngForOf]=\"criteria.products\">\n                    <td class=\"min-200\">\n                      <div class=\"input-group\">\n                        <input\n                          type=\"text\"\n                          ptxRestrictNumber\n                          class=\"form-control\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_QUANTITY_MIN\"\n                          (ngModelChange)=\"log(index);log(i);\"\n                        />\n                        <!-- <input\n                          type=\"text\"\n                          class=\"form-control\"\n                          [ngModel]=\"ShowInterTableElement(criteria.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER, 'PRD_EXISTING_QUANTITY_MIN', ShowProductNickName(elem.PBI_FK_MATERIAL))\"\n                          (ngModelChange)=\"HandleInterChange($event, criteria.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER, 'PRD_EXISTING_QUANTITY_MIN', ShowProductNickName(elem.PBI_FK_MATERIAL));UpdateInterTable();\"\n                        /> -->\n                        <span class=\"input-group-addon\">-</span>\n                        <input\n                          type=\"text\"\n                          ptxRestrictNumber\n                          class=\"form-control\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_QUANTITY_MAX\"\n                          (ngModelChange)=\"log(index);log(i);\"\n                        />\n                        <!-- <input\n                          type=\"text\"\n                          class=\"form-control\"\n                          [ngModel]=\"ShowInterTableElement(criteria.PAF_BIDDING_ITEMS[index].PBI_FK_CUSTOMER, 'PRD_EXISTING_QUANTITY_MAX', ShowProductNickName(elem.PBI_FK_MATERIAL))\"\n                          (ngModelChange)=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[GetProductIndex(ShowProductNickName(elem.PBI_FK_MATERIAL))].PRD_EXISTING_QUANTITY_MIN = $event;UpdateInterTable();\"\n                        /> -->\n                      </div>\n                    </td>\n                  </ng-template>\n                  <td>\n                    <input\n                      class=\"text-left form-control\"\n                      type=\"\"\n                      name=\"\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_PRICING_PERIOD\"></td>\n                  <td>\n                    <ptx-sugester\n                      [model]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_INCOTERM\"\n                      [data_list]=\"master.INCOTERMS\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"criteria.PAF_BIDDING_ITEMS[index].PBI_INCOTERM = $event;UpdateInterTable();IsDisableLoading();\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td>\n                    <ptx-sugester\n                      [model]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_COUNTRY\"\n                      [data_list]=\"master.COUNTRY\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      select_field=\"ID\"\n                      (ModelChanged)=\"criteria.PAF_BIDDING_ITEMS[index].PBI_COUNTRY = $event;\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <!-- <td class=\"min-80\" *ngFor=\"let elem of criteria.products\">\n                    <input class=\"text-right form-control\" type=\"\" name=\"\">\n                  </td> -->\n                  <ng-template ngFor let-j=\"index\" let-e [ngForOf]=\"criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND\">\n                    <td class=\"min-100\" *ngFor=\"let elem of criteria.products;let i = index\">\n                      <input\n                        type=\"text\"\n                        ptxRestrictNumber\n                        class=\"text-right form-control\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND[j].PRODUCTS[i].PRN_FIXED\"\n                        (ngModelChange)=\"UpdateInterTable();\"\n                      />\n                      <!-- <ng-template [ngIf]=\"j != 0\">\n                        <br/>\n                        <a (click)=\"RemoveRound(index, e)\">\n                          <span class=\"glyphicon glyphicon-trash\"></span>\n                        </a>\n                      </ng-template> -->\n                    </td>\n                  </ng-template>\n                  <td class=\"min-100\" [colSpan]=\"1\">\n                    <input\n                      class=\"text-right form-control\"\n                      type=\"\"\n                      name=\"\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED || !IsNeedFreight(criteria.PAF_BIDDING_ITEMS[index])\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                    >\n                    <!-- <input\n                      *ngIf=\"!is_cmps_import\"\n                      class=\"text-right form-control\"\n                      type=\"\"\n                      name=\"\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED || IsNeedFreight(criteria.PAF_BIDDING_ITEMS[index])\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                      >\n                    <input\n                      *ngIf=\"is_cmps_import\"\n                      class=\"text-right form-control\"\n                      type=\"\"\n                      name=\"\"\n                      ptxRestrictNumber\n                      [disabled]=\"model.IS_DISABLED || !IsNeedFreight(criteria.PAF_BIDDING_ITEMS[index])\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                      > -->\n                  </td>\n                  <td class=\"min-100\" [colSpan]=\"1\" *ngIf=\"is_cmps_import\">\n                    <input\n                      class=\"text-left form-control\"\n                      type=\"\"\n                      name=\"\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_PORT\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                      >\n                  </td>\n                  <td class=\"min-100\" *ngFor=\"let elem of criteria.products;let i = index\">\n                    <!-- <input\n                      *ngIf=\"!is_cmps_import\"\n                      type=\"text\"\n                      ptxRestrictNumber\n                      class=\"text-right form-control\"\n                      [disabled]=\"true\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB\"\n                    /> -->\n                    <input\n                      type=\"text\"\n                      ptxRestrictNumber\n                      class=\"text-right form-control\"\n                      [disabled]=\"true\"\n                      [ngModel]=\"CalCulateConvertToFOB(index, i)\"\n                    />\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      type=\"text\"\n                      class=\"text-center form-control\"\n                      [disabled]=\"true\"\n                      [value]=\"CalAverage(criteria.PAF_BIDDING_ITEMS[index].PRODUCTS)\"\n                    />\n                  </td>\n                  <td class=\"min-100\" *ngFor=\"let elem of criteria.products;let i = index\">\n                    <input\n                      type=\"text\"\n                      ptxRestrictNumber\n                      class=\"text-right form-control\"\n                      [disabled]=\"index != 0 || model.IS_DISABLED\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_MARKET_PRICE\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                    />\n                  </td>\n                  <td class=\"min-100\" *ngFor=\"let elem of criteria.products;let i = index\">\n                    <input\n                      type=\"text\"\n                      ptxRestrictNumber\n                      class=\"text-right form-control\"\n                      [disabled]=\"index != 0 || model.IS_DISABLED\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_LATEST_LP_PLAN_PRICE_FIXED\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                    />\n                  </td>\n                  <td class=\"min-150\" *ngFor=\"let elem of criteria.products;let i = index\">\n                    <ptx-sugester\n                      [model]=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_BENCHMARK_PRICE\"\n                      [data_list]=\"master.BENCHMARK\"\n                      [is_disabled]=\"index != 0 || model.IS_DISABLED\"\n                      [show_field]=\"'VALUE'\"\n                      [select_field]=\"'ID'\"\n                      (ModelChanged)=\"criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_BENCHMARK_PRICE = $event; UpdateInterTable();\"\n                      (ngModelChange)=\"UpdateInterTable();\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td class=\"min-150\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[index].PBI_NOTE\"/>\n                  </td>\n                  <td>\n                    <a (click)=\"RemoveBiddingItemsInter(row)\">\n                      <span class=\"glyphicon glyphicon-trash\"></span>\n                    </a>\n                  </td>\n                </tr>\n              </tbody>\n            </table>\n          </div>\n        </div>\n        </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n            <button class=\"btn btn-success\" *ngIf=\"!is_cmps_import\" (click)=\"AddInterBidder()\"> Add Customer</button>\n            <button class=\"btn btn-success\" *ngIf=\"is_cmps_import\" (click)=\"AddInterBidder()\"> Add Supplier</button>\n            <button class=\"btn btn-primary\" (click)=\"AddBidRound()\"> Add Bid Round</button>\n          </div>\n      </div>\n      <br />\n    </ng-template>\n    <!-- <pre>{{ model | json }}</pre> -->\n    <ptx-cmla-form-one\n      *ngIf=\"is_cmla\"\n      [is_cmla_form_1]=\"is_cmla_form_1\"\n      [is_cmla_form_2]=\"is_cmla_form_2\"\n      [is_cmla_import]=\"is_cmla_import\"\n      [model]=\"model\"\n      [master]=\"master\"\n      [criteria]=\"criteria\"\n      (AddCustomerRequest)=\"AddCustomerRequest()\"\n      (AddBidder)=\"AddCMLABidder()\"\n      (AddBidRound)=\"AddBidRoundCMLA()\"\n      (AddBenchmark)=\"AddBenchmarkCMLA()\"\n      (RemoveRound)=\"RemoveRoundCMLA($event)\"\n      (RemoveBidder)=\"RemoveBiddingItemsRequest($event)\"\n      (RemoveBenchmark)=\"RemoveBenchmarkCMLA($event)\"\n      (RemoveCustomerRequest)=\"RemoveCMLAReviewItems($event)\"\n      (OnSelectForAward)=\"OnSelectForAwardCMLA();\"\n      (OnModelUpdate)=\"model = $event\"\n    ></ptx-cmla-form-one>\n  </div>\n<!-- <div class=\"wrapper-detail\"> -->\n  <div class=\"form-horizontal\">\n    <div class=\"row\">\n      <div class=\"form-horizontal\">\n        <div class=\"row\">\n          <div class=\"col-md-6\">\n            <div class=\"form-group\">\n              <label class=\"col-md-3 control-label\">Contract Period :</label>\n              <div class=\"col-md-8\">\n                <ptx-daterangepicker\n                  [disabled]=\"model.IS_DISABLED\"\n                  [start_date]=\"model.PDA_CONTRACT_DATE_FROM\"\n                  [end_date]=\"model.PDA_CONTRACT_DATE_TO\"\n                  (modelChange)=\"model.PDA_CONTRACT_DATE_FROM = $event.start_date;model.PDA_CONTRACT_DATE_TO = $event.end_date;\"\n                ></ptx-daterangepicker>\n                <!-- <app-input-date\n                  [model]=\"model.PDA_CONTRACT_DATE\"\n                  (HandleStateChange)=\"model.PDA_CONTRACT_DATE = $event\"\n                >\n                </app-input-date> -->\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-3 control-label\">Loading Period :</label>\n              <div class=\"col-md-8\">\n                <ptx-daterangepicker\n                  [disabled]=\"model.IS_DISABLED || model.IS_DISABLED_LOADING\"\n                  [start_date]=\"model.PDA_LOADING_DATE_FROM\"\n                  [end_date]=\"model.PDA_LOADING_DATE_TO\"\n                  (modelChange)=\"model.PDA_LOADING_DATE_FROM = $event.start_date;model.PDA_LOADING_DATE_TO = $event.end_date;\"\n                ></ptx-daterangepicker>\n                <!-- <app-input-date\n                  [model]=\"model.PDA_LOADING_DATE\"\n                  (HandleStateChange)=\"model.PDA_LOADING_DATE = $event\"\n                >\n                </app-input-date> -->\n              </div>\n            </div>\n            <div class=\"form-group\" *ngIf=\"!is_cmla\">\n              <label class=\"col-md-3 control-label\">Pricing Period :</label>\n              <div class=\"col-md-8\">\n                <input\n                  type=\"text\"\n                  class=\"form-control\"\n                  [disabled]=\"model.IS_DISABLED || is_cmps_inter\"\n                  [(ngModel)]=\"model.PDA_PRICING_PERIOD\"\n                />\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-3 control-label\">Discharging Period :</label>\n              <div class=\"col-md-8\">\n                <ptx-daterangepicker\n                  [disabled]=\"model.IS_DISABLED || model.IS_DISABLED_DISCHARGING\"\n                  [start_date]=\"model.PDA_DISCHARGING_DATE_FROM\"\n                  [end_date]=\"model.PDA_DISCHARGING_DATE_TO\"\n                  (modelChange)=\"model.PDA_DISCHARGING_DATE_FROM = $event.start_date;model.PDA_DISCHARGING_DATE_TO = $event.end_date;\"\n                ></ptx-daterangepicker>\n                <!-- <app-input-date\n                  [model]=\"model.PDA_DISCHARGING_DATE\"\n                  (HandleStateChange)=\"model.PDA_DISCHARGING_DATE = $event\"\n                >\n                </app-input-date> -->\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-6\">\n          <!-- <div class=\"col-md-6\" style=\"margin-top: -40px;\"> -->\n            <div class=\"form-group\">\n              <label class=\"control-label\">Any other term and special condition</label>\n              <textarea\n                class=\"form-control\"\n                [disabled]=\"model.IS_DISABLED\"\n                [(ngModel)]=\"model.PDA_SPECIAL_TERMS_OR_CONDITION\"></textarea>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <h4 class=\"second-title\" *ngIf=\"model.PAF_PAYMENT_ITEMS.length > 0\">Payment Terms</h4>\n    <div class=\"form-horizontal\" *ngFor=\"let elem of model.PAF_PAYMENT_ITEMS;let index = index;\">\n      <div class=\"row no-margin\">\n        <div class=\"col-xs-2\">\n          <div class=\"form-group\">\n            <label class=\"col-md-12 control-label\" *ngIf=\"!is_cmps_import\">Customer</label>\n            <label class=\"col-md-12 control-label\" *ngIf=\"is_cmps_import\">Supplier</label>\n          </div>\n        </div>\n        <div class=\"col-xs-10\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-3\">\n              <input\n                class=\"form-control\"\n                type=\"text\"\n                [ngModel]=\"ShowCustomer(elem.PPI_FK_CUSTOMER)\"\n                disabled >\n            </div>\n          </div>\n            <div class=\"form-group\">\n                <div class=\"row\">\n                  <div class=\"col-xs-4\">\n                      <div class=\"checkbox\">\n                        <input\n                          id=\"oc{{index}}\"\n                          type=\"checkbox\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [ngModel]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_TYPE == 'Open credit'\"\n                          (ngModelChange)=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_TYPE = $event ? 'Open credit': ''\">\n                        <label for=\"oc{{index}}\">\n                          <div class=\"col-xs-12\">Open credit</div>\n                        </label>\n                      </div>\n                  </div>\n                  <div class=\"col-xs-1\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_TYPE == 'Open credit'\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control form-inline\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      style=\"width: 100%;\" [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_DETAIL\">\n                  </div>\n                  <div class=\"col-xs-1\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_TYPE == 'Open credit'\">days</div>\n                  <div class=\"col-xs-5\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_TYPE == 'Open credit'\">\n                      <select\n                        [disabled]=\"model.IS_DISABLED\"\n                        class=\"form-control\"\n                        style=\"width: 100%; margin-top: -4px;\"\n                        [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 2, 'Open credit')].PPD_PAYMENT_SUB_DETAIL\">\n                        <option [ngValue]=\"e.ID\" *ngFor=\"let e of master.PAF_MT_CREDIT_DETAIL\">{{ e.VALUE }}</option>\n                        <!-- <option [ngFor]=\"let e \">after B/L date (B/L date = day zero)</option> -->\n                      </select>\n                  </div>\n                </div>\n\n                <div class=\"row\">\n                  <div class=\"col-xs-4\">\n                      <div class=\"checkbox\">\n                        <input\n                          id=\"lc_{{index}}\"\n                          type=\"checkbox\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [ngModel]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_TYPE == 'L/C'\"\n                          (ngModelChange)=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_TYPE = $event ? 'L/C': ''\">\n                        <label for=\"lc_{{index}}\">\n                          <div class=\"col-xs-3\">L/C</div>\n                        </label>\n                      </div>\n                  </div>\n                  <div class=\"col-xs-1\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_TYPE == 'L/C'\">\n                    <input\n                      type=\"text\"\n                      class=\"form-control form-inline\"\n                      style=\"width: 100%;\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_DETAIL\">\n                  </div>\n                  <div class=\"col-xs-1\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_TYPE == 'L/C'\">days</div>\n                  <div class=\"col-xs-5\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_TYPE == 'L/C'\">\n                      <select [disabled]=\"model.IS_DISABLED\" class=\"form-control\" style=\"width: 100%; margin-top: -4px;\" [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 1, 'L/C')].PPD_PAYMENT_SUB_DETAIL\">\n                        <option [ngValue]=\"e.ID\" *ngFor=\"let e of master.PAF_MT_CREDIT_DETAIL\">{{ e.VALUE }}</option>\n                        <!-- <option [ngFor]=\"let e \">after B/L date (B/L date = day zero)</option> -->\n                      </select>\n                  </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <div class=\"checkbox\">\n                            <input\n                              id=\"cia_{{index}}\"\n                              type=\"checkbox\"\n                              [disabled]=\"model.IS_DISABLED\"\n                              [ngModel]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 0, 'Cash in advance')].PPD_PAYMENT_TYPE == 'Cash in advance'\"\n                              (ngModelChange)=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 0, 'Cash in advance')].PPD_PAYMENT_TYPE = $event ? 'Cash in advance': ''\">\n                            <label for=\"cia_{{index}}\">\n                                Cash in advance\n                            </label>\n                        </div>\n                    </div>\n                </div>\n\n\n                <ng-template [ngIf]=\"!is_cmla\">\n                  <div class=\"row\">\n                    <div class=\"col-xs-4\">\n                        <div class=\"checkbox\">\n                          <input\n                            id=\"bg{{index}}\"\n                            type=\"checkbox\"\n                            [disabled]=\"model.IS_DISABLED\"\n                            [ngModel]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_TYPE == 'Bank Guarantee'\"\n                            (ngModelChange)=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_TYPE = $event ? 'Bank Guarantee': ''\">\n                          <label for=\"bg{{index}}\">\n                            <div class=\"col-xs-12\">Bank Guarantee</div>\n                          </label>\n                        </div>\n                    </div>\n                    <div class=\"col-xs-1\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_TYPE == 'Bank Guarantee'\">\n                      <input\n                        type=\"text\"\n                        class=\"form-control form-inline\"\n                        style=\"width: 100%;\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_DETAIL\">\n                    </div>\n                    <div class=\"col-xs-1\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_TYPE == 'Bank Guarantee'\">days</div>\n                    <div class=\"col-xs-5\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_TYPE == 'Bank Guarantee'\">\n                        <select\n                          class=\"form-control\"\n                          style=\"width: 100%; margin-top: -4px;\"\n                          [disabled]=\"model.IS_DISABLED\"\n                          [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 3, 'Bank Guarantee')].PPD_PAYMENT_SUB_DETAIL\">\n                          <option [ngValue]=\"e.ID\" *ngFor=\"let e of master.PAF_MT_CREDIT_DETAIL\">{{ e.VALUE }}</option>\n                          <!-- <option [ngFor]=\"let e \">after B/L date (B/L date = day zero)</option> -->\n                        </select>\n                    </div>\n                  </div>\n                </ng-template>\n                <div class=\"col-xs-4\">\n                  <div class=\"checkbox\">\n                    <input\n                      id=\"othes{{index}}\"\n                      type=\"checkbox\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [ngModel]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 4, 'Others')].PPD_PAYMENT_TYPE == 'Others'\"\n                      (ngModelChange)=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 4, 'Others')].PPD_PAYMENT_TYPE = $event ? 'Others': ''\">\n                    <label for=\"othes{{index}}\">\n                      <div class=\"col-xs-3\">Others</div>\n                    </label>\n                  </div>\n                </div>\n                <div class=\"col-xs-8\">\n                  <!-- <div class=\"col-xs-9\" *ngIf=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 4, 'Others')].PPD_PAYMENT_TYPE == 'Others'\"> -->\n                    <input\n                      type=\"text\"\n                      class=\"form-control form-inline\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      style=\"width: 100%;\" [(ngModel)]=\"model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL[GetIndexPaymentItems(index, 4, 'Others')].PPD_PAYMENT_DETAIL\">\n                  </div>\n            </div>\n        </div>\n      </div>\n    </div>\n    <br/>\n    <div class=\"row\" *ngIf=\"is_cmps_dom\">\n      <div class=\"col-xs-12\">\n        <div class=\"form-group\">\n          <label class=\"control-label\">Proposal</label>\n          <app-freetext\n            [disabled]=\"model.IS_DISABLED\"\n            [elementId]=\"'PSI_DETAIL'\"\n            [model]=\"model.PAF_PROPOSAL_ITEMS[0].PSI_DETAIL\"\n            (HandleStateChange)=\"model.PAF_PROPOSAL_ITEMS[0].PSI_DETAIL = $event\">\n          >\n          </app-freetext>\n        </div>\n      </div>\n    </div>\n    <ng-template [ngIf]=\"(is_cmps_inter || is_cmla) && model.PAF_PROPOSAL_ITEMS.length > 0\">\n      <!-- <pre>{{ model.PAF_PROPOSAL_ITEMS | json }}</pre> -->\n      <h4 class=\"second-title\">Proposal</h4>\n      <div class=\"form-horizontal\" *ngFor=\"let elem of model.PAF_PROPOSAL_ITEMS;let index = index;\">\n        <div class=\"row no-margin\">\n          <div class=\"col-xs-2\">\n            <div class=\"form-group\">\n              <label class=\"col-md-12 control-label\">Award to</label>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-12 control-label\">Reason</label>\n            </div>\n          </div>\n          <div class=\"col-xs-5\">\n            <div class=\"form-group\">\n              <div class=\"col-xs-6\">\n                <input\n                  class=\"form-control\"\n                  type=\"text\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [ngModel]=\"ShowCustomer(elem.PSI_FK_CUSTOMER)\" disabled >\n              </div>\n            </div>\n            <div class=\"form-group\">\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <div class=\"checkbox\">\n                            <input\n                              id=\"bsp{{index}}\"\n                              type=\"checkbox\"\n                              [disabled]=\"model.IS_DISABLED\"\n                              [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 0, 'Best price')].PSR_REASON == 'Best price'\"\n                              (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 0, 'Best price')].PSR_REASON = $event ? 'Best price': ''\">\n                            <label for=\"bsp{{index}}\">\n                                Best price\n                            </label>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <div class=\"checkbox\">\n                            <input\n                              id=\"otherreason{{index}}\"\n                              type=\"checkbox\"\n                              [disabled]=\"model.IS_DISABLED\"\n                              [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 1, 'Others')].PSR_REASON == 'Others'\"\n                              (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 1, 'Others')].PSR_REASON = $event ? 'Others': ''\">\n                            <label for=\"otherreason{{index}}\">\n                              <div class=\"col-xs-3\">Others</div>\n                              <div class=\"col-xs-9\" *ngIf=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 1, 'Others')].PSR_REASON == 'Others'\">\n                                <textarea\n                                  [disabled]=\"model.IS_DISABLED\"\n                                  [(ngModel)]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 1, 'Others')].PSR_REASON_DETAIL\"\n                                  class=\"form-control\"\n                                  style=\"width: 100%; min-height: 80px;\">\n                                </textarea>\n                              </div>\n                            </label>\n                        </div>\n                    </div>\n                </div>\n            </div>\n          </div>\n        </div>\n\n        <ng-template [ngIf]=\"is_cmps_inter\">\n          <div class=\"row no-margin\">\n            <div class=\"col-xs-2\">\n              <div class=\"form-group\">\n                <label class=\"col-md-12 control-label\">Other term & Conditions</label>\n              </div>\n            </div>\n            <div class=\"col-xs-5\">\n              <div class=\"form-group\">\n                  <div class=\"row\">\n                      <div class=\"col-xs-12\">\n                          <div class=\"checkbox\">\n                              <input\n                                id=\"gtc{{index}}\"\n                                type=\"checkbox\"\n                                [disabled]=\"model.IS_DISABLED\"\n                                [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 0, 'GT&C as per Thai Oil')].PSO_OTHER_TERM_COND == 'GT&C as per Thai Oil'\"\n                                (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 0, 'GT&C as per Thai Oil')].PSO_OTHER_TERM_COND = $event ? 'GT&C as per Thai Oil': ''\">\n                              <label for=\"gtc{{index}}\">\n                                  GT&C as per Thai Oil\n                              </label>\n                          </div>\n                      </div>\n                  </div>\n                  <div class=\"row\">\n                      <div class=\"col-xs-12\">\n                          <div class=\"checkbox\">\n                              <input\n                                id=\"perlastdeal{{index}}\"\n                                type=\"checkbox\"\n                                [disabled]=\"model.IS_DISABLED\"\n                                [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 1, 'As per last deal done between')].PSO_OTHER_TERM_COND == 'As per last deal done between'\"\n                                (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 1, 'As per last deal done between')].PSO_OTHER_TERM_COND = $event ? 'As per last deal done between': ''; UpdateAsPerLastDeal();\">\n                              <label for=\"perlastdeal{{index}}\">\n                                  <div class=\"col-xs-6\">As per last deal done between</div>\n                                  <div class=\"col-xs-6\" *ngIf=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 1, 'As per last deal done between')].PSO_OTHER_TERM_COND == 'As per last deal done between'\">\n                                    <input\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 1, 'As per last deal done between')].PSO_OTHER_TERM_COND_DETAIL\"\n                                      class=\"form-control\"\n                                      >\n                                  </div>\n                              </label>\n                          </div>\n                      </div>\n                  </div>\n                  <div class=\"row\">\n                      <div class=\"col-xs-12\">\n                          <div class=\"checkbox\">\n                              <input\n                                id=\"others{{index}}\"\n                                type=\"checkbox\"\n                                [disabled]=\"model.IS_DISABLED\"\n                                [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 2, 'Others')].PSO_OTHER_TERM_COND == 'Others'\"\n                                (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 2, 'Others')].PSO_OTHER_TERM_COND = $event ? 'Others': ''\">\n                              <label for=\"others{{index}}\">\n                                  <div class=\"col-xs-3\">Others</div>\n                                  <div class=\"col-xs-9\" *ngIf=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 2, 'Others')].PSO_OTHER_TERM_COND == 'Others'\">\n                                    <textarea\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 2, 'Others')].PSO_OTHER_TERM_COND_DETAIL\"\n                                      class=\"form-control\"\n                                      style=\"width: 100%; min-height: 80px;\">\n                                    </textarea>\n                                  </div>\n                              </label>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"row no-margin\">\n            <div class=\"col-xs-2\">\n              <div class=\"form-group\">\n                <label class=\"col-md-12 control-label\"></label>\n              </div>\n            </div>\n            <div class=\"col-xs-10\">\n              <!-- <pre>{{criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)] | json}}</pre> -->\n              <table class=\"table table-bordered\">\n                  <thead>\n                      <tr>\n                          <th>Product Name</th>\n                          <th>Quantity</th>\n                          <th>Unit</th>\n                          <th>Tolerance</th>\n                          <th>Option</th>\n                          <th>Better than Market Price ({{ model.PDA_PRICE_UNIT }})</th>\n                          <!-- <th>Better than Market Price (USD/BBL)</th> -->\n                          <th>Better than LP Plan Price ({{ model.PDA_PRICE_UNIT }})</th>\n                      </tr>\n                  </thead>\n                  <tbody>\n                      <tr *ngFor=\"let p of criteria.products;let i = index;\">\n                        <td width=\"15%\"> {{ ShowProductNickName(p.PBI_FK_MATERIAL) }} </td>\n                        <td width=\"15%\" class=\"text-center\">\n                          {{ GetAwardQuantity(criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i]) }}\n                          <!-- <div class=\"input-group\">\n                            <input\n                              type=\"text\"\n                              class=\"form-control\"\n                              ptxRestrictNumber\n                              [dis]\n                              [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_AWARDED_QUANTITY_MIN\"\n                              (ngModelChange)=\"log($event)\"\n                            />\n                            <span class=\"input-group-addon\">-</span>\n                            <input\n                              type=\"text\"\n                              class=\"form-control\"\n                              ptxRestrictNumber\n                              [dis]\n                              [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_AWARDED_QUANTITY_MAX\"\n                              (ngModelChange)=\"log($event)\"\n                            />\n                          </div> -->\n                        </td>\n                        <td width=\"5%\"> {{ p.PBI_QUANTITY_UNIT }} </td>\n                        <td width=\"20%\">\n                          <div class=\"form-inline\">\n                            <!-- <div class=\"form-group\"><input type=\"text\" class=\"form-control\" style=\"width: 50px;\" /></div> -->\n                            <!-- <pre>{{ criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_QUANTITY_TOLERANCE_PREFIX | json}}</pre> -->\n                            <div class=\"form-group\" style=\"margin-right: 2px;width: 51px;\">\n                              <select\n                                [disabled]=\"model.IS_DISABLED\"\n                                [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_QUANTITY_TOLERANCE_PREFIX\"\n                                class=\"form-control\" style=\"width: 90%\"\n                                (ngModelChange)=\"updateTolerance(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER), i, 'prefix')\"\n                              >\n                                <option value=\"+\">+</option>\n                                <option value=\"-\">-</option>\n                                <option value=\"+/-\">+/-</option>\n                              </select>\n                            </div>\n                            <div class=\"form-group\" style=\"margin-left: 0px;margin-right: 2px;\">\n                              <input\n                                type=\"text\"\n                                class=\"form-control\"\n                                style=\"width: 70px;\"\n                                [disabled]=\"model.IS_DISABLED\"\n                                [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_QUANTITY_TOLERANCE_SUFFIX\"\n                                (ngModelChange)=\"updateTolerance(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER), i, 'suffix')\"\n                                />\n                            </div> %\n                          </div>\n                        </td>\n                        <td width=\"10%\">\n                          <!-- <div class=\"col-xs-1\"> -->\n                            <select\n                              class=\"form-control\"\n                              style=\"width: 100%\"\n                              [disabled]=\"model.IS_DISABLED\"\n                              [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_QUANTITY_TOLERANCE_OPTION\">\n                              <!-- <option value=\"\">-- Select --</option> -->\n                              <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.PAF_QUANTITY_TOLERANCE_OPTION\">{{ elem.VALUE }}</option>\n                            </select>\n                          <!-- </div> -->\n                        </td>\n                        <td width=\"20%\"> {{ CalculateBetterThanMarketPrice(master.PRODUCTS_INTER, p.PBI_FK_MATERIAL, model.PDA_PRICE_UNIT, criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i], criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].SHOW_CONVERT_TO_FOB, i) | number:'1.2-2' }} </td>\n                        <td width=\"20%\"> {{ CalculateBetterThanLPPlanPrice(master.PRODUCTS_INTER, p.PBI_FK_MATERIAL, model.PDA_PRICE_UNIT, criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i], criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].SHOW_CONVERT_TO_FOB, i) | number:'1.2-2' }} </td>\n                      </tr>\n                      <tr>\n                        <td colspan=\"5\"> Better Value (USD) </td>\n                        <td> {{ CalBetterValueMarket(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)) }} </td>\n                        <td> {{ CalBetterValueLP(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)) }} </td>\n                      </tr>\n                  </tbody>\n              </table>\n            </div>\n          </div>\n        </ng-template>\n\n        <ptx-cmla-calculate-table\n          *ngIf=\"is_cmla_form_1\"\n          [is_cmla_form_1]=\"is_cmla_form_1\"\n          [is_cmla_form_2]=\"is_cmla_form_2\"\n          [proposal]=\"elem\"\n          [model]=\"model\"\n          [master]=\"master\"\n        >\n        </ptx-cmla-calculate-table>\n\n      </div>\n    </ng-template>\n\n    <!-- deprecated -->\n    <ng-template [ngIf]=\"is_cmla_old\">\n      <!-- <pre>{{ model.PAF_PROPOSAL_ITEMS | json }}</pre> -->\n      <h4 class=\"second-title\" *ngIf=\"model.PAF_PROPOSAL_ITEMS.length > 0\">Proposal</h4>\n      <div class=\"form-horizontal\" *ngFor=\"let elem of model.PAF_PROPOSAL_ITEMS;let index = index;\">\n        <div class=\"row no-margin\">\n          <div class=\"col-xs-2\">\n            <div class=\"form-group\">\n              <label class=\"col-md-12 control-label\">Award to</label>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-12 control-label\">Reason</label>\n            </div>\n          </div>\n          <div class=\"col-xs-5\">\n            <div class=\"form-group\">\n              <div class=\"col-xs-6\">\n                <input class=\"form-control\" type=\"text\" [ngModel]=\"ShowCustomer(elem.PSI_FK_CUSTOMER)\" disabled >\n              </div>\n            </div>\n            <div class=\"form-group\">\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <div class=\"checkbox\">\n                            <input\n                              id=\"bsp{{index}}\"\n                              type=\"checkbox\"\n                              [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 0, 'Best price')].PSR_REASON == 'Best price'\"\n                              (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 0, 'Best price')].PSR_REASON = $event ? 'Best price': ''\">\n                            <label for=\"bsp{{index}}\">\n                                Best price\n                            </label>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-xs-12\">\n                        <div class=\"checkbox\">\n                            <input\n                              id=\"otherreason{{index}}\"\n                              type=\"checkbox\"\n                              [ngModel]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 1, 'Others')].PSR_REASON == 'Others'\"\n                              (ngModelChange)=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON', 1, 'Others')].PSR_REASON = $event ? 'Others': ''\">\n                            <label for=\"otherreason{{index}}\">\n                              <div class=\"col-xs-3\">Others</div>\n                              <div class=\"col-xs-9\" *ngIf=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON_DETAIL', 1, 'Others')].PSR_REASON_DETAIL == 'Others'\">\n                                <textarea\n                                  [(ngModel)]=\"model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS[GetIndexOfListItem(model.PAF_PROPOSAL_ITEMS[index].PAF_PROPOSAL_REASON_ITEMS, 'PSR_REASON_DETAIL', 1, 'Others')].PSR_REASON_DETAIL\"\n                                  class=\"form-control\"\n                                  style=\"width: 100%; min-height: 80px;\">\n                                </textarea>\n                              </div>\n                            </label>\n                        </div>\n                    </div>\n                </div>\n            </div>\n          </div>\n\n          <!-- <pre> {{ GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER) | json }} </pre> -->\n          <!-- <pre> {{ criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS |json }} </pre> -->\n          <div class=\"col-xs-5\">\n            <table class=\"table table-bordered\">\n                <thead>\n                    <tr>\n                        <th>Product Name</th>\n                        <th>Quantity</th>\n                        <th>Better than Market Price (USD/BBL)</th>\n                        <th>Better than LP Plan Price (USD/BBL)</th>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let p of criteria.products;let i = index;\">\n                      <td width=\"15%\"> {{ ShowProductNickName(p.PBI_FK_MATERIAL) }} </td>\n                      <td width=\"30%\">\n                        <div class=\"input-group\">\n                          <input\n                            type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_AWARDED_QUANTITY_MIN\"\n                            (ngModelChange)=\"log($event)\"\n                          />\n                          <span class=\"input-group-addon\">-</span>\n                          <input\n                            type=\"text\"\n                            class=\"form-control\"\n                            ptxRestrictNumber\n                            [(ngModel)]=\"criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i].PBI_AWARDED_QUANTITY_MAX\"\n                            (ngModelChange)=\"log($event)\"\n                          />\n                        </div>\n                      </td>\n                      <td> {{ CalculateBetterThanMarketPrice(master.PRODUCTS_INTER, p.PBI_FK_MATERIAL, model.PDA_PRICE_UNIT, criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i], criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PAF_ROUND, i) | number:'1.2-2' }} </td>\n                      <td> {{ CalculateBetterThanLPPlanPrice(master.PRODUCTS_INTER, p.PBI_FK_MATERIAL, model.PDA_PRICE_UNIT, criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PRODUCTS[i], criteria.PAF_BIDDING_ITEMS[GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)].PAF_ROUND, i) | number:'1.2-2' }} </td>\n                    </tr>\n                    <tr>\n                      <td colspan=\"2\"> Better Value (USD) </td>\n                      <td> {{ CalBetterValueMarket(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)) }} </td>\n                      <td> {{ CalBetterValueLP(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER)) }} </td>\n                    </tr>\n                </tbody>\n            </table>\n          </div>\n        </div>\n\n      </div>\n    </ng-template>\n    <!-- end-deprecated -->\n\n    <div class=\"row\">\n      <div class=\"col-xs-12\">\n        <div class=\"form-group\">\n          <label class=\"control-label\">Note</label>\n          <textarea\n            class=\"form-control inline-textarea\"\n            [disabled]=\"model.IS_DISABLED_NOTE\"\n            [(ngModel)]=\"model.PDA_NOTE\"></textarea>\n        </div>\n        <ptx-file-upload\n            btnClass=\"text-left\"\n            [model]=\"model.PAF_ATTACH_FILE\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [filterBy]=\"'REF'\"\n            (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\"\n          >\n          </ptx-file-upload>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-xs-12\">\n        <div class=\"form-group\">\n          <label class=\"control-label\">Reason for Approval/Reject</label>\n          <textarea\n            class=\"form-control inline-textarea\"\n            [disabled]=\"true\"\n            [(ngModel)]=\"model.PDA_REASON\"></textarea>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-horizontal\">\n      <!-- <pre>{{ model | json }}</pre> -->\n    </div>\n    <div class=\"wrapper-button\" id=\"buttonDiv\">\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft.emit(model)\"> SAVE DRAFT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit.emit(model)\"> SAVE & SUBMIT </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify.emit(model)\"> VERIFY </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse.emit(model)\"> ENDORSE </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved.emit(model)\"> APPROVE </button>\n      <!-- <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject.emit(model)\"> REJECT </button> -->\n      <button class=\"btn btn-default\" *ngIf=\"is_cmla && IsAbleToReject(model.Buttons)\" (click)=\"DoReject.emit(model)\"> RECALL/REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"!is_cmla && IsAbleToReject(model.Buttons)\" (click)=\"DoReject.emit(model)\"> REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"is_cmla && IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel.emit(model)\"> DELETE </button>\n      <button class=\"btn btn-default\" *ngIf=\"!is_cmla && IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel.emit(model)\"> CANCEL </button>\n      <!-- <button class=\"btn btn-default\" *ngIf=\"!is_cmla && IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel.emit(model)\"> DELETE/CANCEL </button> -->\n      <!-- <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel.emit(model)\"> CANCEL </button> -->\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF.emit(model)\"> PRINT </button>\n      <!-- <input type=\"submit\" value=\"SAVE DRAFT\" class=\"btn btn-default\" />\n      <input type=\"submit\" value=\"SAVE &amp; PREVIEW\" class=\"btn btn-default\"/>\n      <input type=\"submit\" value=\"SUBMIT\" class=\"btn btn-default\" name=\"action:Submit\"/>\n      <input type=\"submit\" value=\"GENPDF\" id=\"btnGenPDF\" style=\"display: none;\" name=\"action:GenPDF\"/>\n      <input type=\"submit\" value=\"SAVE DRAFT\" id=\"btnSaveDraft\" style=\"display: none;\" name=\"action:SaveDraft\" (click)=\"DoSaveDraft()\" />\n      <input type=\"submit\" value=\"SUBMIT\" id=\"btnSubmit\" style=\"display: none;\" name=\"action:Submit\" /> -->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/shared-form/shared-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SharedFormComponent = (function () {
    function SharedFormComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.PAF_PROPOSAL_REASON_ITEMS = [{}, {}];
        this.PAF_PROPOSAL_OTC_ITEMS = [{}, {}, {}];
        this.is_cmps_dom = false;
        this.is_cmps_inter = false;
        this.is_cmps_import = false;
        this.is_cmla_form_1 = false;
        this.is_cmla_form_2 = false;
        this.is_cmla_form_import = false;
        this.is_cmla_import = false;
        this.is_cmla = false;
        // @Output()
        this.DoSaveDraft = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoSubmit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoVerify = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoEndorse = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoApproved = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoGeneratePDF = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoReject = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.DoCancel = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.OnChangeCompany = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    SharedFormComponent.prototype.ngOnInit = function () {
    };
    SharedFormComponent.prototype.OnUpdatedProduct = function () {
        console.log(this.criteria.products);
    };
    SharedFormComponent.prototype.DelProduct = function (elem) {
        this.criteria.products = this.criteria.products.slice(-1);
        // this.criteria.products = _.reject(this.criteria.products, (e) => {
        //   return elem.ID == e.ID;
        // });
    };
    SharedFormComponent.prototype.OnSelectForAwardCMLA = function () {
        var _this = this;
        var customer_ids = [];
        for (var i = 0; i < this.model.PAF_BIDDING_ITEMS.length; i++) {
            var elem = this.model.PAF_BIDDING_ITEMS[i];
            if (this.model.PAF_BIDDING_ITEMS[i].PBI_AWARDED_BIDDER == 'Y') {
                customer_ids.push(elem.PBI_FK_CUSTOMER);
            }
        }
        this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](this.model.PAF_PAYMENT_ITEMS, function (e) {
            return __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](customer_ids, function (f) {
                return e.PPI_FK_CUSTOMER == f;
            }) != -1;
        });
        this.model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](this.model.PAF_PROPOSAL_ITEMS, function (e) {
            return __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](customer_ids, function (f) {
                return e.PSI_FK_CUSTOMER == f;
            }) != -1;
        });
        customer_ids = __WEBPACK_IMPORTED_MODULE_2_lodash__["uniq"](customer_ids);
        __WEBPACK_IMPORTED_MODULE_2_lodash__["each"](customer_ids, function (elem) {
            var found = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                return e.PPI_FK_CUSTOMER == elem;
            }) != -1;
            if (!found) {
                _this.model.PAF_PAYMENT_ITEMS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                    PPI_FK_CUSTOMER: elem,
                    PAF_PAYMENT_ITEMS_DETAIL: _this.PAF_PAYMENT_ITEM_DETAIL
                }));
            }
            found = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](_this.model.PAF_PROPOSAL_ITEMS, function (e) {
                return e.PSI_FK_CUSTOMER == elem;
            }) != -1;
            if (!found) {
                _this.model.PAF_PROPOSAL_ITEMS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                    PSI_FK_CUSTOMER: elem,
                    PAF_PROPOSAL_REASON_ITEMS: _this.PAF_PROPOSAL_REASON_ITEMS,
                    PAF_PROPOSAL_OTC_ITEMS: _this.PAF_PROPOSAL_OTC_ITEMS,
                }));
            }
        });
        // console.log(this.model.PAF_BIDDING_ITEMS)
        this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_PAYMENT_ITEMS, function (e) {
            var customer = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](_this.master.CUSTOMERS, function (ee) {
                return e.PPI_FK_CUSTOMER == ee.ID;
            });
            var other_index = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](e.PAF_PAYMENT_ITEMS_DETAIL, function (ee) {
                return ee.PPD_PAYMENT_TYPE == 'Others';
            });
            // console.log()
            if (other_index == -1) {
                other_index = 4;
            }
            // console.log(customer);
            // console.log(other_index);
            e.PAF_PAYMENT_ITEMS_DETAIL[other_index] = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]({}, e.PAF_PAYMENT_ITEMS_DETAIL[other_index]);
            e.PAF_PAYMENT_ITEMS_DETAIL[other_index].PPD_PAYMENT_DETAIL = e.PAF_PAYMENT_ITEMS_DETAIL[other_index].PPD_PAYMENT_DETAIL || (customer && customer.PAYMENT_TERM_DETAIL);
            // console.log(e.PAF_PAYMENT_ITEMS_DETAIL[other_index].PPD_PAYMENT_DETAIL);
            return e;
        });
        // if(elem.SELECTED) {
        //   let found = _.findIndex(this.model.PAF_PAYMENT_ITEMS, (e) => {
        //     return e.PPI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
        //   }) != -1;
        //   if( !found ){
        //     this.model.PAF_PAYMENT_ITEMS.push(Utility.clone({
        //       PPI_FK_CUSTOMER: elem.PBI_FK_CUSTOMER,
        //       PAF_PAYMENT_ITEMS_DETAIL: this.PAF_PAYMENT_ITEM_DETAIL
        //     }));
        //   }
        //   found = _.findIndex(this.model.PAF_PROPOSAL_ITEMS, (e) => {
        //     return e.PSI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
        //   }) != -1;
        //   if( !found ){
        //     this.model.PAF_PROPOSAL_ITEMS.push(Utility.clone({
        //       PSI_FK_CUSTOMER: elem.PBI_FK_CUSTOMER,
        //       PAF_PROPOSAL_REASON_ITEMS: this.PAF_PROPOSAL_REASON_ITEMS,
        //       PAF_PROPOSAL_OTC_ITEMS: this.PAF_PROPOSAL_OTC_ITEMS,
        //     }));
        //   }
        // } else {
        //   this.model.PAF_PAYMENT_ITEMS = _.reject(this.model.PAF_PAYMENT_ITEMS, (e) => {
        //     return e.PPI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
        //   });
        //   this.model.PAF_PROPOSAL_ITEMS = _.reject(this.model.PAF_PROPOSAL_ITEMS, (e) => {
        //     return e.PSI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
        //   });
        // }
    };
    SharedFormComponent.prototype.OnSelectForAwardInter = function () {
        var _this = this;
        this.criteria.SHOW_INTER_CALCULATES = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], this.criteria.SHOW_INTER_CALCULATES);
        var pricing_periods = [];
        var _loop_1 = function () {
            var elem = this_1.criteria.PAF_BIDDING_ITEMS[i];
            if (elem.SELECTED) {
                pricing_periods.push(elem.PBI_PRICING_PERIOD);
                this_1.criteria.PAF_BIDDING_ITEMS[i].PRODUCTS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this_1.criteria.PAF_BIDDING_ITEMS[i].PRODUCTS, function (e) {
                    e.PBI_AWARDED_QUANTITY_MIN = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(e.PBI_QUANTITY_MIN);
                    e.PBI_AWARDED_QUANTITY_MAX = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(e.PBI_QUANTITY_MAX);
                    e.PBI_QUANTITY_TOLERANCE_OPTION = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(e.PBI_QUANTITY_TOLERANCE_OPTION) || "Operational Tolerance";
                    e.PBI_QUANTITY_TOLERANCE_PREFIX = e.PBI_QUANTITY_TOLERANCE_PREFIX || '+/-';
                    return e;
                });
                var found = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](this_1.model.PAF_PAYMENT_ITEMS, function (e) {
                    return e.PPI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
                }) != -1;
                if (!found) {
                    this_1.model.PAF_PAYMENT_ITEMS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                        PPI_FK_CUSTOMER: elem.PBI_FK_CUSTOMER,
                        PAF_PAYMENT_ITEMS_DETAIL: this_1.PAF_PAYMENT_ITEM_DETAIL
                    }));
                }
                found = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](this_1.model.PAF_PROPOSAL_ITEMS, function (e) {
                    return e.PSI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
                }) != -1;
                if (!found) {
                    this_1.model.PAF_PROPOSAL_ITEMS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                        PSI_FK_CUSTOMER: elem.PBI_FK_CUSTOMER,
                        PAF_PROPOSAL_REASON_ITEMS: this_1.PAF_PROPOSAL_REASON_ITEMS,
                        PAF_PROPOSAL_OTC_ITEMS: this_1.PAF_PROPOSAL_OTC_ITEMS,
                    }));
                }
                found = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](this_1.criteria.SHOW_INTER_CALCULATES, function (e) {
                    return e.PBI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
                }) != -1;
                if (!found) {
                    this_1.criteria.SHOW_INTER_CALCULATES.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(elem));
                }
            }
            else {
                this_1.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](this_1.model.PAF_PAYMENT_ITEMS, function (e) {
                    return e.PPI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
                });
                this_1.model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](this_1.model.PAF_PROPOSAL_ITEMS, function (e) {
                    return e.PSI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
                });
                this_1.criteria.SHOW_INTER_CALCULATES = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](this_1.criteria.SHOW_INTER_CALCULATES, function (e) {
                    return e.PBI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
                });
                this_1.criteria.PAF_BIDDING_ITEMS[i].PRODUCTS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this_1.criteria.PAF_BIDDING_ITEMS[i].PRODUCTS, function (e) {
                    e.PBI_AWARDED_QUANTITY_MIN = 0;
                    e.PBI_AWARDED_QUANTITY_MAX = 0;
                    e.PBI_QUANTITY_TOLERANCE_PREFIX = e.PBI_QUANTITY_TOLERANCE_PREFIX || '+/-';
                    return e;
                });
            }
        };
        var this_1 = this;
        for (var i = 0; i < this.criteria.PAF_BIDDING_ITEMS.length; i++) {
            _loop_1();
        }
        this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_PAYMENT_ITEMS, function (e) {
            var sap_payment_term = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](_this.master.MT_CUST_PAYMENT_TERM, function (ee) {
                return e.PPI_FK_CUSTOMER == ee.FK_MT_CUST;
            });
            var other_index = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](e.PAF_PAYMENT_ITEMS_DETAIL, function (ee) {
                return ee.PPD_PAYMENT_TYPE == 'Others';
            });
            // console.log()
            if (other_index == -1) {
                other_index = 4;
            }
            console.log(other_index);
            e.PAF_PAYMENT_ITEMS_DETAIL[other_index] = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]({}, e.PAF_PAYMENT_ITEMS_DETAIL[other_index]);
            e.PAF_PAYMENT_ITEMS_DETAIL[other_index].PPD_PAYMENT_DETAIL = e.PAF_PAYMENT_ITEMS_DETAIL[other_index].PPD_PAYMENT_DETAIL || (sap_payment_term && sap_payment_term.VALUE);
            return e;
        });
        // console.log(pricing_periods);
        pricing_periods = __WEBPACK_IMPORTED_MODULE_2_lodash__["uniq"](pricing_periods);
        // console.log(pricing_periods);
        this.model.PDA_PRICING_PERIOD = __WEBPACK_IMPORTED_MODULE_2_lodash__["join"](pricing_periods, ',');
        this.IsDisableLoading();
    };
    SharedFormComponent.prototype.OnSelectForAward = function () {
        var _this = this;
        var customer_ids = [];
        for (var i = 0; i < this.model.PAF_BIDDING_ITEMS.length; i++) {
            var elem = this.model.PAF_BIDDING_ITEMS[i];
            if (elem.PBI_AWARDED_BIDDER == 'Y') {
                customer_ids.push(elem.PBI_FK_CUSTOMER);
            }
            // this.model.PAF_BIDDING_ITEMS[i].PBI_AWARDED_BIDDER = elem.SELECTED ? 'Y': 'N';
        }
        this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](this.model.PAF_PAYMENT_ITEMS, function (e) {
            return __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](customer_ids, function (f) {
                return e.PPI_FK_CUSTOMER == f;
            }) != -1;
        });
        // this.model.PAF_PROPOSAL_ITEMS = _.filter(this.model.PAF_PROPOSAL_ITEMS, (e) => {
        //   return _.findIndex(customer_ids, (f) => {
        //     return e.PSI_FK_CUSTOMER == f;
        //   }) != -1;
        // });
        customer_ids = __WEBPACK_IMPORTED_MODULE_2_lodash__["uniq"](customer_ids);
        __WEBPACK_IMPORTED_MODULE_2_lodash__["each"](customer_ids, function (elem) {
            var found = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                return e.PPI_FK_CUSTOMER == elem;
            }) != -1;
            if (!found) {
                _this.model.PAF_PAYMENT_ITEMS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                    PPI_FK_CUSTOMER: elem,
                    PAF_PAYMENT_ITEMS_DETAIL: _this.PAF_PAYMENT_ITEM_DETAIL
                }));
            }
            // found = _.findIndex(this.model.PAF_PROPOSAL_ITEMS, (e) => {
            //   return e.PSI_FK_CUSTOMER == elem;
            // }) != -1;
            // if( !found ){
            //   this.model.PAF_PROPOSAL_ITEMS.push(Utility.clone({
            //     PSI_FK_CUSTOMER: elem,
            //     PAF_PROPOSAL_REASON_ITEMS: this.PAF_PROPOSAL_REASON_ITEMS,
            //     PAF_PROPOSAL_OTC_ITEMS: this.PAF_PROPOSAL_OTC_ITEMS,
            //   }));
            // }
        });
        // for (var i = 0; i < this.model.PAF_BIDDING_ITEMS.length; i++) {
        //   let elem = this.model.PAF_BIDDING_ITEMS[i];
        //   if(elem.SELECTED) {
        //     let found = _.findIndex(this.model.PAF_PAYMENT_ITEMS, (e) => {
        //       return e.PPI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
        //     }) != -1;
        //     if( !found ){
        //       this.model.PAF_PAYMENT_ITEMS.push(Utility.clone({
        //         PPI_FK_CUSTOMER: elem.PBI_FK_CUSTOMER,
        //         PAF_PAYMENT_ITEMS_DETAIL: this.PAF_PAYMENT_ITEM_DETAIL
        //       }));
        //     }
        //   } else {
        //     this.model.PAF_PAYMENT_ITEMS = _.reject(this.model.PAF_PAYMENT_ITEMS, (e) => {
        //       return e.PPI_FK_CUSTOMER == elem.PBI_FK_CUSTOMER;
        //     })
        //   }
        // }
        // this.model.PAF_PAYMENT_ITEMS = _.map(_.filter(this.model.PAF_BIDDING_ITEMS, (elem) => {
        //   return elem.SELECTED == true;
        // }), (elem) => {
        //   return Utility.clone({
        //     PPI_FK_CUSTOMER: elem.PBI_FK_CUSTOMER,
        //     PAF_PAYMENT_ITEMS_DETAIL: this.PAF_PAYMENT_ITEM_DETAIL
        //   });
        // });
        this.IsDisableLoading();
    };
    SharedFormComponent.prototype.RemoveRound = function (index) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var popup_content = "Are you sure you want to remove this bid round?";
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            // context.model.PAF_REVIEW_ITEMS = PAFStore.RemovePAFReviewItems(i, j, context.model.PAF_REVIEW_ITEMS);
            context.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](context.criteria.PAF_BIDDING_ITEMS, function (elem) {
                elem.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_ROUND);
                elem.PAF_ROUND.splice(index, 1);
                return elem;
            });
        });
        // this.criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND = _.reject(this.criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND, (e) => {
        //   return e == target;
        // });
    };
    SharedFormComponent.prototype.RemoveRoundCMLA = function (index) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var popup_content = "Are you sure you want to remove this bid round?";
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](context.model.PAF_BIDDING_ITEMS, function (elem) {
                elem.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_ROUND);
                elem.PAF_ROUND.splice(index, 1);
                return elem;
            });
        });
    };
    SharedFormComponent.prototype.RemoveBenchmarkCMLA = function (index) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var popup_content = "Are you sure you want to remove this market price?";
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](context.model.PAF_BIDDING_ITEMS, function (elem) {
                elem.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_BIDDING_ITEMS_BENCHMARK);
                elem.PAF_BIDDING_ITEMS_BENCHMARK.splice(index, 1);
                return elem;
            });
        });
    };
    SharedFormComponent.prototype.UpdateAsPerLastDeal = function () {
        var _this = this;
        this.model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_PROPOSAL_ITEMS, function (e) {
            var customer = _this.ShowCustomer(e.PSI_FK_CUSTOMER);
            var index = _this.GetIndexOfListItem(e.PAF_PROPOSAL_OTC_ITEMS, 'PSO_OTHER_TERM_COND', 1, 'As per last deal done between');
            e.PAF_PROPOSAL_OTC_ITEMS[index].PSO_OTHER_TERM_COND_DETAIL = "Thai Oil and " + customer;
            return e;
        });
    };
    SharedFormComponent.prototype.AddNewProduct = function () {
        this.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.criteria.PAF_BIDDING_ITEMS, function (e) {
            e.PRODUCTS = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], e.PRODUCTS);
            e.PRODUCTS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({}));
            e.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](e.PAF_ROUND, function (f) {
                f.PRODUCTS = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], f.PRODUCTS);
                f.PRODUCTS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({}));
                return f;
            });
            return e;
        });
        this.criteria.products.push({
            IS_NEED_PRODUCT: true,
        });
        this.UpdateInterTable();
    };
    SharedFormComponent.prototype.UpdateRound = function () {
        var template_count = this.criteria.PAF_BIDDING_ITEMS[0].PAF_ROUND.length;
        this.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.criteria.PAF_BIDDING_ITEMS, function (elem) {
            elem.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_ROUND);
            elem.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].FillArray(elem.PAF_ROUND, {}, template_count);
            return elem;
        });
    };
    SharedFormComponent.prototype.AddBidRound = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var count_product = this.criteria.products.length;
        this.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.criteria.PAF_BIDDING_ITEMS, function (elem) {
            elem.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_ROUND);
            elem.PAF_ROUND.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                PRODUCTS: __WEBPACK_IMPORTED_MODULE_2_lodash__["times"](count_product, __WEBPACK_IMPORTED_MODULE_2_lodash__["constant"]({})),
            }));
            return elem;
        });
    };
    SharedFormComponent.prototype.AddBenchmarkCMLA = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var count_product = this.criteria.products.length;
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            elem.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_BIDDING_ITEMS_BENCHMARK);
            elem.PAF_BIDDING_ITEMS_BENCHMARK.push({});
            return elem;
        });
    };
    SharedFormComponent.prototype.AddBidRoundCMLA = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var count_product = this.criteria.products.length;
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            elem.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_ROUND);
            elem.PAF_ROUND.push({});
            return elem;
        });
    };
    SharedFormComponent.prototype.AddBidder = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.PAF_BIDDING_ITEMS.push({});
        if (this.is_cmps_inter) {
            this.UpdateRound();
        }
    };
    SharedFormComponent.prototype.GetRoundText = function (num) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].GetRoundText(num);
    };
    SharedFormComponent.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    SharedFormComponent.prototype.SetSelected = function (e, index) {
        this.model.PAF_BIDDING_ITEMS[index].PBI_AWARDED_BIDDER = e ? 'Y' : 'N';
    };
    SharedFormComponent.prototype.GetSelected = function (index) {
        if (this.is_cmps_inter) {
            return this.criteria.PAF_BIDDING_ITEMS[index].SELECTED;
        }
        return this.model.PAF_BIDDING_ITEMS[index].PBI_AWARDED_BIDDER == 'Y';
    };
    SharedFormComponent.prototype.AddCustomerRequest = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var pre_customer = {
            NEED_PRODUCT: true,
            PAF_REVIEW_ITEMS_DETAIL: [{}],
            PRI_QUANTITY_UNIT: 'MT',
        };
        console.log(pre_customer);
        console.log(this.model.PAF_REVIEW_ITEMS);
        // this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
        this.model.PAF_REVIEW_ITEMS.push(pre_customer);
    };
    SharedFormComponent.prototype.AddCustomerRequestTier = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        console.log(this.criteria.TARGET_PRODUCT);
        if (this.criteria.TARGET_PRODUCT != null) {
            this.model.PAF_REVIEW_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], this.model.PAF_REVIEW_ITEMS);
            this.model.PAF_REVIEW_ITEMS[this.criteria.TARGET_PRODUCT].PAF_REVIEW_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], this.model.PAF_REVIEW_ITEMS[this.criteria.TARGET_PRODUCT].PAF_REVIEW_ITEMS_DETAIL);
            this.model.PAF_REVIEW_ITEMS[this.criteria.TARGET_PRODUCT].PAF_REVIEW_ITEMS_DETAIL.push({});
        }
        console.log(this.model.PAF_REVIEW_ITEMS);
    };
    SharedFormComponent.prototype.RemoveBiddingItemsRequest = function (target) {
        if (this.model.IS_DISABLED || target.PBI_AWARDED_BIDDER == 'Y') {
            // if(this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var _a = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].GetPAFBidderItemsSummary(target, this.master, this.is_cmps_dom), customer = _a.customer, product = _a.product;
        var cust_label = 'Customer';
        if (this.is_cmla_import) {
            cust_label = 'Supplier';
        }
        var popup_content = "Are you sure you want to remove this record?<br/><br/>\n      <b>" + cust_label + ":</b> " + customer + "<br/>\n      <b>Product:</b> " + product + "<br/>\n    ";
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            // console.log(context.model.PAF_BIDDING_ITEMS.length)
            // console.log(context.is_cmla)
            if (context.model.PAF_BIDDING_ITEMS.length == 1 && context.is_cmla) {
                context.AddCMLABidder();
            }
            context.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](context.model.PAF_BIDDING_ITEMS, function (elem) {
                return elem == target;
            });
        });
    };
    SharedFormComponent.prototype.RemoveBiddingItemsInter = function (target) {
        if (this.model.IS_DISABLED || target.PBI_AWARDED_BIDDER == 'Y') {
            return;
        }
        var context = this;
        var _a = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].GetPAFBidderItemsSummary(target, this.master, this.is_cmps_dom), customer = _a.customer, product = _a.product;
        var popup_content = "Are you sure you want to remove this record?<br/><br/>\n      <b>Customer:</b> " + customer + "<br/>\n    ";
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](context.criteria.PAF_BIDDING_ITEMS, function (elem) {
                return elem == target;
            });
        });
    };
    SharedFormComponent.prototype.GetIndexOfListItem = function (items, key, i, target) {
        // console.log(items, key, i, target)
        var found_index = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](items, function (o) { return o[key] == target; });
        // console.log(items, key, i, target, found_index);
        // console.log(items, key, i, target, found_index);
        return found_index == -1 ? i : found_index;
    };
    SharedFormComponent.prototype.GetIndexPaymentItems = function (index, i, target) {
        var items = this.model.PAF_PAYMENT_ITEMS[index].PAF_PAYMENT_ITEMS_DETAIL;
        var found_index = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](items, function (o) { return o.PPD_PAYMENT_TYPE == target; });
        // console.log('found_index', found_index)
        // console.log('found_index', items)
        return found_index == -1 ? i : found_index;
    };
    SharedFormComponent.prototype.AddInterBidder = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var count_product = this.criteria.products.length;
        var count_round = this.criteria.PAF_BIDDING_ITEMS[0].PAF_ROUND.length;
        this.criteria.PAF_BIDDING_ITEMS.push(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
            SELECTED: false,
            PAF_ROUND: __WEBPACK_IMPORTED_MODULE_2_lodash__["times"](count_round, __WEBPACK_IMPORTED_MODULE_2_lodash__["constant"](__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone({
                PRODUCTS: __WEBPACK_IMPORTED_MODULE_2_lodash__["times"](count_product, __WEBPACK_IMPORTED_MODULE_2_lodash__["constant"]({})),
            }))),
            PRODUCTS: __WEBPACK_IMPORTED_MODULE_2_lodash__["times"](count_product, __WEBPACK_IMPORTED_MODULE_2_lodash__["constant"]({})),
        }));
        this.UpdateRound();
        this.UpdateInterTable();
    };
    SharedFormComponent.prototype.AddCMLABidder = function () {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        // this.model.PAF_BIDDING_ITEMS.push({})
        var template_count = this.model.PAF_BIDDING_ITEMS[0].PAF_ROUND.length;
        var template_bm_count = this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK.length;
        // this.model.PAF_BIDDING_ITEMS = _.map(this.model.PAF_BIDDING_ITEMS, (elem) => {
        //   elem.PAF_ROUND = _.extend([], elem.PAF_ROUND);
        //   elem.PAF_ROUND = Utility.FillArray(elem.PAF_ROUND, {}, template_count);
        //   elem.PAF_BIDDING_ITEMS_BENCHMARK = Utility.FillArray(elem.PAF_BIDDING_ITEMS_BENCHMARK, {}, template_bm_count);
        //   return elem;
        // });
        var target = {
            PBI_QUANTITY_UNIT: 'MT',
            PAF_ROUND: null,
            PAF_BIDDING_ITEMS_BENCHMARK: null,
        };
        target.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], target.PAF_ROUND);
        target.PAF_ROUND = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].FillArray(target.PAF_ROUND, {}, template_count));
        target.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], target.PAF_BIDDING_ITEMS_BENCHMARK);
        target.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(__WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].FillArray(target.PAF_BIDDING_ITEMS_BENCHMARK, {}, template_bm_count));
        // target.PAF_BIDDING_ITEMS_BENCHMARK = _.extend([], target.PAF_BIDDING_ITEMS_BENCHMARK);
        if (!this.is_cmla_form_2) {
            target.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK));
            target.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](target.PAF_BIDDING_ITEMS_BENCHMARK, function (f) {
                f.PBB_VALUE = 0;
                return f;
            });
        }
        if (this.is_cmla_import) {
            target.PBI_FK_MATERIAL = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[0].PBI_FK_MATERIAL);
            target.PBI_QUANTITY_UNIT = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[0].PBI_QUANTITY_UNIT);
            // target.PBI_FREIGHT_PRICE = Utility.clone(this.model.PAF_BIDDING_ITEMS[0].PBI_FREIGHT_PRICE);
            // target.PBI_BENEFIT = Utility.clone(this.model.PAF_BIDDING_ITEMS[0].PBI_BENEFIT);
        }
        // target.PAF_BIDDING_ITEMS_BENCHMARK = Utility.FillArray(target.PAF_BIDDING_ITEMS_BENCHMARK, {}, template_bm_count);
        // console.log(target.PAF_ROUND, target.PAF_BIDDING_ITEMS_BENCHMARK)
        if (this.is_cmla_form_1) {
            var src_index = this.model.PAF_BIDDING_ITEMS.length - 1;
            if (src_index >= 0) {
                target.PBI_FK_MATERIAL = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PBI_FK_MATERIAL);
                target.PBI_MARKET_PRICE_FLOAT = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PBI_MARKET_PRICE_FLOAT);
                target.PBI_LATEST_LP_PLAN_PRICE_FLOAT = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PBI_LATEST_LP_PLAN_PRICE_FLOAT);
                target.PBI_LATEST_LP_PLAN_PRICE_FIXED = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PBI_LATEST_LP_PLAN_PRICE_FIXED);
                target.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PAF_BIDDING_ITEMS_BENCHMARK);
                // target.PAF_BIDDING_ITEMS_BENCHMARK[0] = {};
                // target.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME = PBB_NAME;
                // target.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE = PBB_VALUE;
            }
        }
        else if (this.is_cmla_form_2) {
            var src_index = this.model.PAF_BIDDING_ITEMS.length - 1;
            if (src_index >= 0) {
                target.PBI_FK_CUSTOMER = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PBI_FK_CUSTOMER);
                target.PBI_FK_MATERIAL = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.model.PAF_BIDDING_ITEMS[src_index].PBI_FK_MATERIAL);
            }
        }
        this.model.PAF_BIDDING_ITEMS.push(target);
        if (this.is_cmla_form_2) {
            this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].Form2BenchMark(this.model.PAF_BIDDING_ITEMS);
        }
        var PBI_MARKET_PRICE_FLOAT = this.model.PAF_BIDDING_ITEMS[0].PBI_MARKET_PRICE_FLOAT;
        var PBI_LATEST_LP_PLAN_PRICE_FLOAT = this.model.PAF_BIDDING_ITEMS[0].PBI_LATEST_LP_PLAN_PRICE_FLOAT;
        var PBI_LATEST_LP_PLAN_PRICE_FIXED = this.model.PAF_BIDDING_ITEMS[0].PBI_LATEST_LP_PLAN_PRICE_FIXED;
        var PBB_VALUE = this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE;
        var PBB_NAME = this.model.PAF_BIDDING_ITEMS[0].PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME;
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            if (!_this.is_cmla_form_1) {
                elem.PBI_MARKET_PRICE_FLOAT = PBI_MARKET_PRICE_FLOAT;
                elem.PBI_LATEST_LP_PLAN_PRICE_FLOAT = PBI_LATEST_LP_PLAN_PRICE_FLOAT;
                elem.PBI_LATEST_LP_PLAN_PRICE_FIXED = PBI_LATEST_LP_PLAN_PRICE_FIXED;
            }
            if (!_this.is_cmla_form_2 && !_this.is_cmla_form_1) {
                elem.PAF_BIDDING_ITEMS_BENCHMARK = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]([], elem.PAF_BIDDING_ITEMS_BENCHMARK);
                elem.PAF_BIDDING_ITEMS_BENCHMARK[0] = {};
                elem.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_NAME = PBB_NAME;
                elem.PAF_BIDDING_ITEMS_BENCHMARK[0].PBB_VALUE = PBB_VALUE;
            }
            return elem;
        });
    };
    SharedFormComponent.prototype.RemoveProductFromCriteria = function (product) {
        if (this.model.IS_DISABLED) {
            return;
        }
        if (this.criteria.products.length > 1) {
            this.criteria.products = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](this.criteria.products, function (e) {
                return e == product;
            });
        }
    };
    SharedFormComponent.prototype.GetProductIndex = function (product_code) {
        var index = -1;
        index = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](this.criteria.products, function (e) {
            return e.PBI_FK_MATERIAL == product_code;
        });
        return index;
    };
    SharedFormComponent.prototype.ShowInterTableElement = function (a, b) {
        return 0;
    };
    // public HandleInterChange(val, customer, material) : void {
    //   // code...
    // }
    SharedFormComponent.prototype.UpdateInterTable = function () {
        var _a = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].UpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS, this.criteria.PAF_BIDDING_ITEMS), PAF_BIDDING_ITEMS = _a.PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS = _a.SHOW_PAF_BIDDING_ITEMS;
        // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
        this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
        // this.model.PAF_BIDDING_ITEMS = PAFStore.UpdateInterMainModel(this.criteria, this.model.PAF_BIDDING_ITEMS);
        // this.criteria.PAF_BIDDING_ITEMS = PAFStore.UpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS);
        // this.model.SHOW_PAF_BIDDING_ITEMS = PAFStore.ReverseUpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS, this.model.SHOW_PAF_BIDDING_ITEMS);
    };
    SharedFormComponent.prototype.ShowNumberInterTable = function (mat, index, key) {
        console.log(mat, index, key);
        if (mat) {
            this.criteria.PAF_BIDDING_ITEMS[index] = __WEBPACK_IMPORTED_MODULE_2_lodash__["extend"]({}, this.criteria.PAF_BIDDING_ITEMS[index]);
            console.log(this.criteria.PAF_BIDDING_ITEMS[index]);
        }
        return 0;
        // this.criteria.PAF_BIDDING_ITEMS[index] = _.extend({}, this.criteria.PAF_BIDDING_ITEMS[index]);
        // this.criteria.PAF_BIDDING_ITEMS[index][mat] = _.extend({}, this.criteria.PAF_BIDDING_ITEMS[index][mat]);
        // this.criteria.PAF_BIDDING_ITEMS[index][mat][key] = this.criteria.PAF_BIDDING_ITEMS[index][mat][key] || 0;
        // return this.criteria.PAF_BIDDING_ITEMS[index][mat][key];
    };
    SharedFormComponent.prototype.log = function (a) {
        console.log(a);
    };
    SharedFormComponent.prototype.updateTolerance = function (index, i, target) {
        // switch (target) {
        //   case "suffix":
        //     // code...
        //     break;
        //   case "prefix"
        //   default:
        //     // code...
        //     break;
        // }
        var prefix = this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_QUANTITY_TOLERANCE_PREFIX || "+/-";
        var postfix = this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_QUANTITY_TOLERANCE_SUFFIX;
        this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_QUANTITY_TOLERANCE = prefix + " " + postfix;
        // updateTolerance(GetIndexBiddingItems(criteria.PAF_BIDDING_ITEMS, elem.PSI_FK_CUSTOMER), i, 'suffix')
        // console.log(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].PBI_QUANTITY_TOLERANCE)
    };
    SharedFormComponent.prototype.GetIndexBiddingItems = function (items, customer) {
        var key = __WEBPACK_IMPORTED_MODULE_2_lodash__["findIndex"](items, function (e) {
            return e.PBI_FK_CUSTOMER == customer;
        });
        // console.log(key, items, customer)
        return key;
    };
    SharedFormComponent.prototype.ShowProductNickName = function (id) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].getValueByID(id, 'NAME', this.master.PRODUCTS_INTER);
    };
    SharedFormComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    SharedFormComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    SharedFormComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    SharedFormComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    SharedFormComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    SharedFormComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    SharedFormComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToReject(button_list);
    };
    SharedFormComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_4__common__["f" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    SharedFormComponent.prototype.CalAverage = function (products) {
        // let result_min = 0;
        // let result_max = 0;
        // let sum_quantity_min = _.reduce(products, (sum, e) => {
        //   return sum += +(e.PBI_QUANTITY_MIN || 0);
        // }, 0);
        // let convert_to_fob_min = _.reduce(products, (sum, e) => {
        //   return sum += (+(e.SHOW_CONVERT_TO_FOB || 0) * +(e.PBI_QUANTITY_MIN || 0));
        // }, 0);
        // let sum_quantity_max = _.reduce(products, (sum, e) => {
        //   return sum += +(e.PBI_QUANTITY_MAX || 0);
        // }, 0);
        // let convert_to_fob_max = _.reduce(products, (sum, e) => {
        //   return sum += (+(e.SHOW_CONVERT_TO_FOB || 0) * +(e.PBI_QUANTITY_MAX || 0));
        // }, 0);
        // if(sum_quantity_min) {
        //   result_min = convert_to_fob_min/sum_quantity_min;
        // }
        // if(sum_quantity_max) {
        //   result_max = convert_to_fob_max/sum_quantity_max;
        // }
        // // console.log({
        // //   sum_quantity_min,
        // //   convert_to_fob_min,
        // //   sum_quantity_max,
        // //   convert_to_fob_max,
        // // })
        // let result_min_str = this.decimalPipe.transform(result_min, '1.2-2');
        // let result_max_str = this.decimalPipe.transform(result_max, '1.2-2');
        // // return `${result_min_str} - ${result_max_str}`;
        var result_max = 0;
        var convert_to_fob_max = __WEBPACK_IMPORTED_MODULE_2_lodash__["reduce"](products, function (sum, e) {
            var SHOW_CONVERT_TO_FOB = +e.SHOW_CONVERT_TO_FOB || 0;
            var PBI_QUANTITY_MAX = +e.PBI_QUANTITY_MAX || 0;
            var result = SHOW_CONVERT_TO_FOB * PBI_QUANTITY_MAX;
            return sum += (result);
        }, 0);
        var sum_quantity_max = __WEBPACK_IMPORTED_MODULE_2_lodash__["reduce"](products, function (sum, e) {
            var PBI_QUANTITY_MAX = +(e.PBI_QUANTITY_MAX || 0);
            var SHOW_CONVERT_TO_FOB = +(e.SHOW_CONVERT_TO_FOB || 0);
            PBI_QUANTITY_MAX = (SHOW_CONVERT_TO_FOB && PBI_QUANTITY_MAX) || 0;
            return sum += PBI_QUANTITY_MAX;
        }, 0);
        if (sum_quantity_max) {
            result_max = convert_to_fob_max / sum_quantity_max;
        }
        var result_max_str = this.decimalPipe.transform(result_max, '1.2-2');
        var convert_to_fobs = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](products, function (e) {
            return e.SHOW_CONVERT_TO_FOB;
        });
        if (__WEBPACK_IMPORTED_MODULE_2_lodash__["compact"](convert_to_fobs).length == 0) {
            result_max_str = "";
        }
        return "" + result_max_str;
    };
    SharedFormComponent.prototype.CalCulateConvertToFOB = function (index, i) {
        var result_str = '';
        var round_group = __WEBPACK_IMPORTED_MODULE_4__common__["a" /* Utility */].clone(this.criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND);
        var round_list = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](round_group, function (e) {
            return e.PRODUCTS[i].PRN_FIXED;
        });
        var last_round_cal = __WEBPACK_IMPORTED_MODULE_2_lodash__["last"](__WEBPACK_IMPORTED_MODULE_2_lodash__["compact"](round_list));
        if (last_round_cal) {
            var is_fob = this.IsNeedFreight(this.criteria.PAF_BIDDING_ITEMS[index]);
            if (this.is_cmps_import) {
                if (is_fob) {
                    this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = +last_round_cal + (+(this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE || 0));
                }
                else {
                    this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE = '';
                    this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = +last_round_cal;
                }
            }
            else {
                // console.log(`SHOW_CONVERT_TO_FOB`, this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB);
                // console.log(`PBI_FREIGHT_PRICE`, this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE);
                // if(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB && this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE)
                // {
                if (is_fob) {
                    this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = +last_round_cal - (+(this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE || 0));
                }
                else {
                    this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE = '';
                    this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = +last_round_cal;
                }
                // }
            }
            result_str = this.decimalPipe.transform(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB, '1.2-2');
        }
        else {
            this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = '';
            result_str = '';
        }
        // console.log(last_round)
        // let last_round = _.last(round);
        // console.log(last_round);
        // // console.log('CalCulateConvertToFOB', this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE)
        // const round = this.criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND;
        // let last_round = _.last(round);
        // let last_round_cal = +(last_round.PRODUCTS[i].PRN_FIXED || 0);
        // let is_fob = this.IsNeedFreight(this.criteria.PAF_BIDDING_ITEMS[index]);
        // if(this.is_cmps_import) {
        //   if(is_fob) {
        //     this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round_cal + (+(this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE || 0));
        //   } else {
        //     this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE = '';
        //     this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round_cal;
        //   }
        // } else {
        //   // debugger;
        //   if(is_fob) {
        //     this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round_cal - (+(this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE || 0));
        //   } else {
        //     this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE = '';
        //     this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round_cal;
        //   }
        //   // if(is_fob) {
        //   //   this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE = '';
        //   //   this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round_cal;
        //   // } else {
        //   //   this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = last_round_cal - (+(this.criteria.PAF_BIDDING_ITEMS[index].PBI_FREIGHT_PRICE || 0));
        //   // }
        // }
        // let last_round_prn_fixed = last_round.PRODUCTS[i].PRN_FIXED;
        // if (!last_round_prn_fixed) {
        //   this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB = null;
        // }
        // let result_str = this.decimalPipe.transform(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB, '1.2-2');
        // // this.CalAverage(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS);
        // result_str = this.decimalPipe.transform(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB, '1.2-2');
        return result_str;
    };
    SharedFormComponent.prototype.CalBetterValueMarket = function (index) {
        var PAF_BIDDING_ITEM = this.criteria.PAF_BIDDING_ITEMS[index];
        var round = this.criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND;
        var unit = this.model.PDA_PRICE_UNIT;
        var products = this.criteria.products;
        var result_min = 0;
        var result_max = 0;
        var result = 0;
        for (var i = 0; i < products.length; ++i) {
            var material = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](this.master.PRODUCTS_INTER, function (e) {
                return e.ID == products[i].PBI_FK_MATERIAL;
            });
            if (!material) {
                break;
            }
            var item = PAF_BIDDING_ITEM.PRODUCTS[i];
            var last_round = __WEBPACK_IMPORTED_MODULE_2_lodash__["last"](round);
            // let better_than_market = +(last_round.PRODUCTS[i].PRN_FIXED || 0) - +(item.PBI_MARKET_PRICE || 0);
            var better_than_market = +(item.SHOW_CONVERT_TO_FOB || 0) - +(item.PBI_MARKET_PRICE || 0);
            if (this.is_cmps_import) {
                better_than_market *= -1;
            }
            // console.log(better_than_market)
            if (unit != 'USD/BBL') {
                better_than_market = better_than_market * (+material.MET_PRODUCT_DENSITY) / (6.29);
            }
            better_than_market = better_than_market || 0;
            result_min += better_than_market * __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].NormalizeQuantity(+item.PBI_AWARDED_QUANTITY_MIN, item.PBI_QUANTITY_UNIT, +material.MET_PRODUCT_DENSITY);
            result_max += better_than_market * __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].NormalizeQuantity(+item.PBI_AWARDED_QUANTITY_MAX, item.PBI_QUANTITY_UNIT, +material.MET_PRODUCT_DENSITY);
            // console.log(result_min)
        }
        // let is_need_bracket_min = false;
        // if (result_min < 0) {
        //   is_need_bracket_min = true;
        // }
        var result_min_str = this.decimalPipe.transform(result_min, '1.2-2');
        var result_max_str = this.decimalPipe.transform(result_max, '1.2-2');
        if (result_min == result_max) {
            return "" + result_max_str;
        }
        result_min_str = result_min < 0 ? "(" + result_min_str + ")" : result_min_str;
        result_max_str = result_max < 0 ? "(" + result_max_str + ")" : result_max_str;
        return result_min_str + " - " + result_max_str;
    };
    SharedFormComponent.prototype.CalBetterValueLP = function (index) {
        var PAF_BIDDING_ITEM = this.criteria.PAF_BIDDING_ITEMS[index];
        var round = this.criteria.PAF_BIDDING_ITEMS[index].PAF_ROUND;
        var unit = this.model.PDA_PRICE_UNIT;
        var products = this.criteria.products;
        var result_min = 0;
        var result_max = 0;
        var result = 0;
        for (var i = 0; i < products.length; ++i) {
            var material = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](this.master.PRODUCTS_INTER, function (e) {
                return e.ID == products[i].PBI_FK_MATERIAL;
            });
            if (!material) {
                break;
            }
            var item = PAF_BIDDING_ITEM.PRODUCTS[i];
            var last_round = __WEBPACK_IMPORTED_MODULE_2_lodash__["last"](round);
            // let better_than_market = +(last_round.PRODUCTS[i].PRN_FIXED || 0) - +(item.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0);
            var better_than_market = +(this.criteria.PAF_BIDDING_ITEMS[index].PRODUCTS[i].SHOW_CONVERT_TO_FOB || 0) - +(item.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0);
            if (this.is_cmps_import) {
                better_than_market *= -1;
            }
            if (unit != 'USD/BBL') {
                material.MET_PRODUCT_DENSITY = material.MET_PRODUCT_DENSITY || 0.1;
                better_than_market = better_than_market * (+material.MET_PRODUCT_DENSITY) / (6.29);
            }
            result_min += better_than_market * __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].NormalizeQuantity(+item.PBI_AWARDED_QUANTITY_MIN, item.PBI_QUANTITY_UNIT, +material.MET_PRODUCT_DENSITY);
            result_max += better_than_market * __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].NormalizeQuantity(+item.PBI_AWARDED_QUANTITY_MAX, item.PBI_QUANTITY_UNIT, +material.MET_PRODUCT_DENSITY);
        }
        var result_min_str = this.decimalPipe.transform(result_min, '1.2-2');
        var result_max_str = this.decimalPipe.transform(result_max, '1.2-2');
        if (result_min == result_max) {
            return "" + result_max_str;
        }
        result_min_str = result_min < 0 ? "(" + result_min_str + ")" : result_min_str;
        result_max_str = result_max < 0 ? "(" + result_max_str + ")" : result_max_str;
        return result_min_str + " - " + result_max_str;
    };
    // public CalculateBetterThanMarketPrice(products, p_index, unit, product, round, i) : number {
    SharedFormComponent.prototype.CalculateBetterThanMarketPrice = function (products, p_index, unit, product, convert_to_fob, i) {
        // console.log({
        //   products, p_index, unit, product, convert_to_fob, i,
        // })
        var result = 0;
        var material = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](products, function (e) {
            return e.ID == p_index;
        });
        // if(material) {
        //   let last_round = _.last(round);
        //   if (unit == 'USD/BBL') {
        //     result = +(last_round.PRODUCTS[i].PRN_FIXED || 0) - +(product.PBI_MARKET_PRICE || 0);
        //   } else {
        //     result = +(last_round.PRODUCTS[i].PRN_FIXED || 0) - +(product.PBI_MARKET_PRICE || 0) * ( +material.MET_PRODUCT_DENSITY) / (6.29);
        //   }
        // }
        // if(material) {
        //   if (unit == 'USD/BBL') {
        //     result = +(convert_to_fob || 0) - +(product.PBI_MARKET_PRICE || 0);
        //   } else {
        //     result = +(convert_to_fob || 0) - +(product.PBI_MARKET_PRICE || 0) * ( +material.MET_PRODUCT_DENSITY) / (6.29);
        //   }
        // }
        result = +(convert_to_fob || 0) - +(product.PBI_MARKET_PRICE || 0);
        if (this.is_cmps_import) {
            result *= -1;
        }
        return result;
    };
    // public CalculateBetterThanLPPlanPrice(products, p_index, unit, product, round, i) : number {
    SharedFormComponent.prototype.CalculateBetterThanLPPlanPrice = function (products, p_index, unit, product, convert_to_fob, i) {
        var result = 0;
        var material = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](products, function (e) {
            return e.ID == p_index;
        });
        // if(material) {
        //   let last_round = _.last(round);
        //   if (unit == 'USD/BBL') {
        //     result = +(last_round.PRODUCTS[i].PRN_FIXED || 0) - +(product.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0);
        //   } else {
        //     result = +(last_round.PRODUCTS[i].PRN_FIXED || 0) - +(product.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0) * ( +material.MET_PRODUCT_DENSITY) / (6.29);
        //   }
        // }
        // if(material) {
        //   if (unit == 'USD/BBL') {
        //     result = +(convert_to_fob || 0) - +(product.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0);
        //   } else {
        //     result = +(convert_to_fob || 0) - +(product.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0) * ( +material.MET_PRODUCT_DENSITY) / (6.29);
        //   }
        // }
        result = +(convert_to_fob || 0) - +(product.PBI_LATEST_LP_PLAN_PRICE_FIXED || 0);
        if (this.is_cmps_import) {
            result *= -1;
        }
        return result;
    };
    SharedFormComponent.prototype.IsNeedFreight = function (record) {
        var result = true;
        var incoterm = record && record.PBI_INCOTERM;
        if (incoterm) {
            var incoterm_object = __WEBPACK_IMPORTED_MODULE_2_lodash__["find"](this.master.INCOTERMS, function (e) {
                return e.ID == incoterm;
            });
            result = ((incoterm_object && incoterm_object.INCLUDED_FREIGHT) || false);
        }
        // console.log(incoterm, result);
        if (this.is_cmps_import) {
            return !result;
        }
        else if (this.is_cmps_inter) {
            // console.log(result)
            return result;
        }
        return false;
    };
    SharedFormComponent.prototype.IsDisableLoading = function () {
        var _this = this;
        if (!this.is_cmps_inter && !this.is_cmps_import) {
            return;
        }
        var result = true;
        var customers = __WEBPACK_IMPORTED_MODULE_2_lodash__["map"](this.model.PAF_PAYMENT_ITEMS, function (e) {
            return e.PPI_FK_CUSTOMER;
        });
        if (customers.length == 0) {
            return;
        }
        var rows = __WEBPACK_IMPORTED_MODULE_2_lodash__["filter"](this.criteria.PAF_BIDDING_ITEMS, function (e) {
            // let is_need = false;
            // if(_.indexOf(customers, e.PBI_FK_CUSTOMER) != -1) {
            //   is_need = this.IsNeedFreight(e);
            // }
            return __WEBPACK_IMPORTED_MODULE_2_lodash__["indexOf"](customers, e.PBI_FK_CUSTOMER) != -1;
        });
        var include_frieght = __WEBPACK_IMPORTED_MODULE_2_lodash__["groupBy"](rows, function (e) {
            return _this.IsNeedFreight(e);
        });
        var keys = __WEBPACK_IMPORTED_MODULE_2_lodash__["keys"](include_frieght);
        if (keys.length == 0 || keys.length > 1) {
            this.model.IS_DISABLED_LOADING = false;
            this.model.IS_DISABLED_DISCHARGING = false;
        }
        else {
            result = keys[0] == 'true';
            // this.model.IS_DISABLED_LOADING = !result;
            if (this.is_cmps_import) {
                this.model.IS_DISABLED_LOADING = !result;
                this.model.IS_DISABLED_DISCHARGING = false;
            }
            else {
                this.model.IS_DISABLED_LOADING = false;
                this.model.IS_DISABLED_DISCHARGING = !result;
            }
        }
        if (this.model.IS_DISABLED_LOADING) {
            this.model.PDA_LOADING_DATE_TO = "";
            this.model.PDA_LOADING_DATE_FROM = "";
        }
        if (this.model.IS_DISABLED_DISCHARGING) {
            this.model.PDA_DISCHARGING_DATE_TO = "";
            this.model.PDA_DISCHARGING_DATE_FROM = "";
        }
        console.log('result', result == false);
        console.log('this.is_cmps_import', this.is_cmps_import);
        console.log('this.model.IS_DISABLED_LOADING', this.model.IS_DISABLED_LOADING);
        console.log('this.model.IS_DISABLED_DISCHARGING', this.model.IS_DISABLED_DISCHARGING);
        // if (include_frieght.length !=  == 0) {
        //   return false;
        // }
    };
    SharedFormComponent.prototype.GetAwardQuantity = function (product) {
        product.PBI_AWARDED_QUANTITY_MIN = product.PBI_QUANTITY_MIN;
        product.PBI_AWARDED_QUANTITY_MAX = product.PBI_QUANTITY_MAX;
        var min = +(product.PBI_AWARDED_QUANTITY_MIN || 0);
        var max = +(product.PBI_AWARDED_QUANTITY_MAX || 0);
        var result_min_str = this.decimalPipe.transform(min, '1.2-2');
        var result_max_str = this.decimalPipe.transform(max, '1.2-2');
        if (min == max) {
            return "" + result_max_str;
        }
        return result_min_str + " - " + result_max_str;
    };
    SharedFormComponent.prototype.RemoveCMLAReviewItems = function (target) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var _a = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].GetPAFBidderItemsSummaryCMLA(target, this.master.CUSTOMERS, this.master.PRODUCTS), customer = _a.customer, product = _a.product;
        var popup_content = "Are you sure you want to remove this record?<br/><br/>\n      <b>Customer:</b> " + customer + "<br/>\n      <b>Product:</b> " + product + "<br/>\n    ";
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_REVIEW_ITEMS = __WEBPACK_IMPORTED_MODULE_2_lodash__["reject"](context.model.PAF_REVIEW_ITEMS, function (e) { return e == target; });
        });
    };
    SharedFormComponent.prototype.RemovePAFReviewItems = function (i, j, target) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var _a = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].GetPAFReviewItemsSummary(target, this.master, this.is_cmps_dom), customer = _a.customer, product = _a.product, tier_amount = _a.tier_amount;
        var tier_no = +j + 1;
        var is_first = +j == 0;
        var popup_content = "Are you sure you want to remove this tier?<br/><br/>\n      <b>Customer:</b> " + customer + "<br/>\n      <b>Product:</b> " + product + "<br/>\n      <b>Tier No.:</b> " + tier_no + "<br/>\n    ";
        if (is_first) {
            popup_content = "Are you sure you want to remove this main tier?<br/>\n        (This customer and product also all tier will be removed)<br/><br/>\n        <b>Customer:</b> " + customer + "<br/>\n        <b>Product:</b> " + product + "<br/>\n        <b>Tier No.:</b> " + tier_no + "<br/>\n      ";
        }
        if (!this.is_cmps_dom) {
            popup_content = "Are you sure you want to remove this main tier?<br/>\n        (This customer and product also all tier will be removed)<br/><br/>\n        <b>Customer:</b> " + customer + "<br/>\n        <b>Product:</b> " + product + "<br/>\n      ";
        }
        __WEBPACK_IMPORTED_MODULE_3_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_REVIEW_ITEMS = __WEBPACK_IMPORTED_MODULE_4__common__["c" /* PAFStore */].RemovePAFReviewItems(i, j, context.model.PAF_REVIEW_ITEMS);
        });
    };
    return SharedFormComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SharedFormComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SharedFormComponent.prototype, "master", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SharedFormComponent.prototype, "criteria", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SharedFormComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmps_dom", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmps_inter", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmps_import", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmla_form_1", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmla_form_2", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmla_form_import", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmla_import", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Boolean)
], SharedFormComponent.prototype, "is_cmla", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], SharedFormComponent.prototype, "DoSaveDraft", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _b || Object)
], SharedFormComponent.prototype, "DoSubmit", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _c || Object)
], SharedFormComponent.prototype, "DoVerify", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _d || Object)
], SharedFormComponent.prototype, "DoEndorse", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _e || Object)
], SharedFormComponent.prototype, "DoApproved", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _f || Object)
], SharedFormComponent.prototype, "DoGeneratePDF", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _g || Object)
], SharedFormComponent.prototype, "DoReject", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _h || Object)
], SharedFormComponent.prototype, "DoCancel", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _j || Object)
], SharedFormComponent.prototype, "OnChangeCompany", void 0);
SharedFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-shared-form',
        template: __webpack_require__("../../../../../src/app/components/shared-form/shared-form.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */]) === "function" && _k || Object])
], SharedFormComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=shared-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/status-square/status-square.component.html":
/***/ (function(module, exports) {

module.exports = "<svg height=\"30\" width=\"30\">\n  <rect *ngIf=\"min > max\" height=\"100%\" rx=\"0\" ry=\"0\" style=\"fill:#00a651;stroke-width:0;opacity:1\" width=\"100%\" x=\"0\" y=\"0\"></rect>\n  <rect *ngIf=\"!(min > max || min < max)\" height=\"100%\" rx=\"0\" ry=\"0\" style=\"fill:#fff600;stroke-width:0;opacity:1\" width=\"100%\" x=\"0\" y=\"0\"></rect>\n  <rect *ngIf=\"min < max\" height=\"100%\" rx=\"0\" ry=\"0\" style=\"fill:#f8020b;stroke-width:0;opacity:1\" width=\"100%\" x=\"0\" y=\"0\"></rect>\n</svg>"

/***/ }),

/***/ "../../../../../src/app/components/status-square/status-square.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusSquareComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StatusSquareComponent = (function () {
    function StatusSquareComponent() {
        this.min = __WEBPACK_IMPORTED_MODULE_1_moment_moment__().toDate();
        this.max = __WEBPACK_IMPORTED_MODULE_1_moment_moment__().toDate();
    }
    StatusSquareComponent.prototype.ngOnInit = function () {
    };
    return StatusSquareComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], StatusSquareComponent.prototype, "min", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], StatusSquareComponent.prototype, "max", void 0);
StatusSquareComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-status-square',
        template: __webpack_require__("../../../../../src/app/components/status-square/status-square.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [])
], StatusSquareComponent);

//# sourceMappingURL=status-square.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/sugester/sugester.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SugesterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SugesterComponent = (function () {
    function SugesterComponent() {
        this.show_field = 'TEXT';
        this.class_name = '';
        this.ModelChanged = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
    }
    SugesterComponent.prototype.ngOnInit = function () {
    };
    SugesterComponent.prototype.show = function (model) {
        var _this = this;
        // return Utility.getTextByID(model, this.data_list);
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list, function (e) {
            return e[_this.select_field] == model;
        });
        return (s && s[this.show_field]);
    };
    SugesterComponent.prototype.OnSelected = function (model) {
        var _this = this;
        var s = __WEBPACK_IMPORTED_MODULE_1_lodash__["find"](this.data_list, function (e) {
            return e[_this.show_field] == model;
        });
        this.ModelChanged.emit((s && s.ID));
    };
    return SugesterComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SugesterComponent.prototype, "model", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SugesterComponent.prototype, "data_list", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SugesterComponent.prototype, "is_disabled", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], SugesterComponent.prototype, "show_field", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], SugesterComponent.prototype, "select_field", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", String)
], SugesterComponent.prototype, "class_name", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]) === "function" && _a || Object)
], SugesterComponent.prototype, "ModelChanged", void 0);
SugesterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-sugester',
        template: "\n  <input\n    [ngModel]=\"show(model)\"\n    (ngModelChange)=\"OnSelected($event)\"\n    [typeahead]=\"data_list\"\n    [disabled]=\"is_disabled\"\n    placeholder=\"\"\n    [typeaheadOptionField]=\"show_field\"\n    [typeaheadMinLength]=\"0\"\n    typeaheadWaitMs=\"300\"\n    [ngClass]=\"class_name\"\n    class=\"form-control\">\n  ",
        styles: []
    }),
    __metadata("design:paramtypes", [])
], SugesterComponent);

var _a;
//# sourceMappingURL=sugester.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"loading\">\n  <div class=\"sk-cube-grid\">\n    <div class=\"sk-cube sk-cube1\"></div>\n    <div class=\"sk-cube sk-cube2\"></div>\n    <div class=\"sk-cube sk-cube3\"></div>\n    <div class=\"sk-cube sk-cube4\"></div>\n    <div class=\"sk-cube sk-cube5\"></div>\n    <div class=\"sk-cube sk-cube6\"></div>\n    <div class=\"sk-cube sk-cube7\"></div>\n    <div class=\"sk-cube sk-cube8\"></div>\n    <div class=\"sk-cube sk-cube9\"></div>\n  </div>\n</div>\n\n<router-outlet></router-outlet>\n\n<!-- <div class=\"main-content\">\n  <div class=\"main-content-area\">\n    <div class=\"container body-content\">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>\n\n -->\n\n"

/***/ }),

/***/ "../../../../../src/app/containers/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/containers/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/containers/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/containers/app/app.component.scss")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-bitumen.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model | json }}</pre> -->\n<div class=\"wrapper-detail\">\n  <h2 class=\"first-title\">\n    <span class=\"note\">Product Approval Form (CMLA-Bitumen)</span> Please fill in form as relevant\n  </h2>\n  <div class=\"form-horizontal\">\n    <div class=\"row\">\n      <div class=\"col-xs-3\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-3 control-label\">Form Id</label>\n          <div class=\"col-xs-9\">\n            <input class=\"form-control\" [(ngModel)]=\"model.PDA_FORM_ID\" type=\"text\" [disabled]=\"true\">\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-9\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-2 control-label\">Created Date</label>\n          <div class=\"col-xs-3\">\n            <ptx-datepicker\n              [disabled]=\"true\"\n              [model]=\"model.PDA_CREATED\"\n            ></ptx-datepicker>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <h4 class=\"second-title\">Balance Volume</h4>\n  <div class=\"row\" *ngFor=\"let elem of model.PAF_BTM_GRADE_ITEMS; let index = index;\">\n    <div class=\"col-xs-12\">\n      <div class=\"table-responsive\">\n        <table class=\"table table-bordered plutonyx-table\">\n          <thead>\n            <tr>\n              <th rowspan=\"2\"></th>\n              <th rowspan=\"2\" class=\"min-200\">Bitumen Grade</th>\n              <th colspan=\"2\">O/S</th>\n              <th rowspan=\"2\" class=\"min-180\">Production</th>\n              <th [colSpan]=\"CalBitumenGradeColSpan(index)\">\n               <div class=\"col-md-4 col-md-offset-2 text-right\">Allocation</div>\n               <div class=\"col-md-4 ptx-w-100\">\n                 <ptx-sugester\n                    [model]=\"model.PAF_BTM_DETAIL.PBD_VOLUME_UNIT\"\n                    [data_list]=\"master.QUANTITY_UNITS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.PAF_BTM_DETAIL.PBD_VOLUME_UNIT = $event;\"\n                  >\n                  </ptx-sugester>\n               </div>\n              </th>\n              <th rowspan=\"2\" class=\"min-180\">C/S</th>\n              <th rowspan=\"3\"></th>\n            </tr>\n            <tr>\n              <th class=\"min-180\">O/S (Actual)</th>\n              <th class=\"min-180\">Safety Stock</th>\n              <th class=\"min-180\">Deferred Volume</th>\n              <th *ngFor=\"let e of model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS;let i = index\" class=\"min-300 pt-0\">\n                <div class=\"inline-block min-200 pl-5 pt-10 mb-10\">\n                  <ptx-sugester\n                    [model]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[i].PBA_FK_CUSTOMER\"\n                    [data_list]=\"master.CUSTOMERS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    [select_field]=\"'ID'\"\n                    (ModelChanged)=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[i].PBA_FK_CUSTOMER = $event;\"\n                  >\n                  </ptx-sugester>\n                </div>\n                <div class=\"inline-block ptx-w-40 pr-5 mb-10\">\n                  <a href=\"javascript:void(0)\" (click)=\"RemoveAllocateItem(index, e)\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n                </div>\n                <input\n                  class=\"form-control text-center\"\n                  type=\"text\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[i].PAV_INFO\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                >\n              </th>\n              <!-- <th class=\"ptx-w-150 pt-0\">\n                <div class=\"inline-block ptx-w-80 pl-5 pt-10 mb-10\">\n                  <select class=\"form-control\">\n                    <option>PTT</option>\n                  </select>\n                </div>\n                <div class=\"inline-block ptx-w-40 pr-5 mb-10\">\n                  <a href=\"#\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n                </div>\n                <input class=\"form-control text-center\" type=\"text\" placeholder=\"Domestic\">\n              </th>\n              <th class=\"ptx-w-150 pt-0\">\n                <div class=\"inline-block ptx-w-80 pl-5 pt-10 mb-10\">\n                  <select class=\"form-control\">\n                    <option>IRPC</option>\n                  </select>\n                </div>\n                <div class=\"inline-block ptx-w-40 pr-5 mb-10\">\n                  <a href=\"#\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n                </div>\n                <input class=\"form-control text-center\" type=\"text\" placeholder=\"Domestic\">\n              </th> -->\n            </tr>\n          </thead>\n          <tbody>\n            <tr>\n              <td rowspan=\"2\" class=\"headcol\">\n                <div class=\"checkbox\">\n                  <div class=\"col-md-2\">\n                    <input\n                      type=\"radio\"\n                      name=\"check_got_bid\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"criteria.SELECT_GRADE\"\n                      [value]=\"index\"\n                    >\n                  </div>\n                    <!-- <input\n                      id=\"check_got_bid{{index}}\"\n                      type=\"checkbox\"\n                    >\n                    <label for=\"check_got_bid{{index}}\"></label> -->\n                </div>\n              </td>\n              <td rowspan=\"2\">\n                <!-- <input class=\"form-control text-center\" value=\"60/70\" type=\"text\"> -->\n                <ptx-sugester\n                  [model]=\"model.PAF_BTM_GRADE_ITEMS[index].PBG_FK_MATERIAL\"\n                  [data_list]=\"master.PAF_CMLA_BITUMEN\"\n                  [is_disabled]=\"model.IS_DISABLED\"\n                  [show_field]=\"'VALUE'\"\n                  select_field=\"ID\"\n                  (ModelChanged)=\"model.PAF_BTM_GRADE_ITEMS[index].PBG_FK_MATERIAL = $event;\"\n                >\n                </ptx-sugester>\n              </td>\n              <td>\n                <input\n                  type=\"text\"\n                  class=\"form-control\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PBG_OS_ACTUAL\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                />\n              </td>\n              <td>\n                <input\n                  type=\"text\"\n                  class=\"form-control\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PBG_SAFETY_STOCK\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                />\n              </td>\n              <td rowspan=\"2\">\n                <input\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  class=\"form-control\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PBG_PRODUCTION\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                />\n              </td>\n              <td rowspan=\"2\">\n                <input\n                  type=\"text\"\n                  class=\"form-control\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PBG_DEFFERED_VOLUME\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                />\n              </td>\n              <ng-template ngFor let-e [ngForOf]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS\" let-i=\"index\">\n                <td>\n                  <input\n                    type=\"text\"\n                    class=\"form-control\"\n                    ptxRestrictNumber\n                    [fraction]=\"0\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[i].PAV_VOLUME\"\n                    (ngModelChange)=\"HandleModelUpdate();\"\n                  />\n                </td>\n              </ng-template>\n              <td rowspan=\"2\">\n                <input\n                  class=\"form-control text-center inline-block ptx-w-200\"\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  [disabled]=\"true\"\n                  [ngModel]=\"CalTotalCS(index)\"\n                >\n              </td>\n              <td rowspan=\"2\">\n                <a href=\"javascript:void(0)\" (click)=\"RemoveBitumenGrade(elem)\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n              </td>\n            </tr>\n            <tr>\n              <td colspan=\"2\" class=\"min-200\">\n                <div class=\"text inline-block\">Total O/S = </div>\n                <input\n                  class=\"form-control text-center inline-block ptx-w-100\"\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  [disabled]=\"true\"\n                  [ngModel]=\"CalTotalOS(index)\"\n                >\n              </td>\n              <td [colSpan]=\"CalBitumenGradeColSpan(index)-1\">\n                <div class=\"text inline-block\">Total Allocation = </div>\n                <input\n                  class=\"form-control text-center inline-block ptx-w-100\"\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [fraction]=\"0\"\n                  [disabled]=\"true\"\n                  [ngModel]=\"CalTotalAllocation(index)\"\n                >\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n  <!-- <div class=\"mt-15\" style=\"overflow-x: scroll;\">\n    <table class=\"table table-bordered\">\n      <thead>\n        <tr>\n          <th rowspan=\"2\"></th>\n          <th rowspan=\"2\" class=\"ptx-w-180\">Bitumen Grade</th>\n          <th colspan=\"2\">O/S</th>\n          <th rowspan=\"2\">Production</th>\n          <th colspan=\"3\">\n           <div class=\"col-md-4 col-md-offset-2\">Allocation</div>\n           <div class=\"col-md-2\">\n             <select class=\"form-control\">\n                <option>MT</option>\n              </select>\n           </div>\n          </th>\n          <th rowspan=\"2\">Production</th>\n          <th rowspan=\"3\"></th>\n        </tr>\n        <tr>\n          <th>O/S (Actual)</th>\n          <th>Safety Stock</th>\n          <th>Deferred<br />Volume</th>\n          <th class=\"ptx-w-150 pt-0\">\n            <div class=\"inline-block ptx-w-80 pl-5 pt-10 mb-10\">\n              <select class=\"form-control\">\n                <option>GPP</option>\n              </select>\n            </div>\n            <div class=\"inline-block ptx-w-40 pr-5 mb-10\">\n              <a href=\"#\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n            </div>\n            <input class=\"form-control text-center\" type=\"text\" placeholder=\"Domestic\">\n          </th>\n          <th class=\"ptx-w-150 pt-0\">\n            <div class=\"inline-block ptx-w-80 pl-5 pt-10 mb-10\">\n              <select class=\"form-control\">\n                <option>IRPC</option>\n              </select>\n            </div>\n            <div class=\"inline-block ptx-w-40 pr-5 mb-10\">\n              <a href=\"#\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n            </div>\n            <input class=\"form-control text-center\" type=\"text\" placeholder=\"Domestic\">\n          </th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td rowspan=\"2\" class=\"headcol\">\n            <div class=\"checkbox\">\n                <input\n                  id=\"check_got_bid{{index}}\"\n                  type=\"checkbox\"\n                >\n                <label for=\"check_got_bid{{index}}\"></label>\n            </div>\n          </td>\n          <td rowspan=\"2\"><input class=\"form-control text-center\" value=\"60/70\" type=\"text\"></td>\n          <td><input class=\"form-control text-center\" type=\"text\"></td>\n          <td><input class=\"form-control text-center\" type=\"text\"></td>\n          <td rowspan=\"2\"><input class=\"form-control text-center\" type=\"text\"></td>\n          <td rowspan=\"2\"><input class=\"form-control text-center\" type=\"text\"></td>\n          <td><input class=\"form-control text-center\" type=\"text\"></td>\n          <td><input class=\"form-control text-center\" type=\"text\"></td>\n          <td rowspan=\"2\"><input class=\"form-control text-center\" type=\"text\" [disabled]=\"true\"></td>\n          <td rowspan=\"2\">\n            <a href=\"#\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n          </td>\n        </tr>\n        <tr>\n          <td colspan=\"2\"><div class=\"text\">Total O/S = </div><input class=\"form-control text-center\" type=\"text\" [disabled]=\"true\"></td>\n          <td colspan=\"2\"><div class=\"text\">Total Allocation = </div><input class=\"form-control text-center\" type=\"text\" [disabled]=\"true\"></td>\n        </tr>\n      </tbody>\n    </table>\n  </div> -->\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <!-- <button class=\"btn btn-success mr-5\" (click)=\"AddBitumenGrade()\"> Add Bitumen Grade</button> -->\n      <button class=\"btn btn-primary\" (click)=\"AddAllocation()\"> Add Allocation</button>\n    </div>\n  </div>\n\n  <div class=\"row mt-45\">\n    <div class=\"col-xs-7\">\n      <h3 class=\"text-bold\">Reference Price</h3>\n      <table class=\"table table-bordered\">\n          <thead>\n            <tr>\n              <th width=\"5%\">No</th>\n              <th width=\"65%\">Assumption Detail</th>\n              <th width=\"15%\">Value</th>\n              <th width=\"15%\">Unit</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr>\n              <td>1</td>\n              <td class=\"text-left\">Argus Price = To use Argus Bitumen at 3rd week of previous loading month</td>\n              <td>\n                <input\n                  class=\"form-control text-right\"\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [disabled]=\"model.IS_DISABLED\"\n                  [(ngModel)]=\"model.PAF_BTM_DETAIL.PBD_ARGUS_PRICE\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                >\n              </td>\n              <td>{{ model.PAF_BTM_DETAIL.PBD_ARGUS_PRICE_UNIT }}</td>\n            </tr>\n            <tr>\n              <td>2</td>\n              <td class=\"text-left\">\n                Estimated FX in previous loading month<br/>\n                <ptx-daterangepicker\n                  [disabled]=\"model.IS_DISABLED\"\n                  [start_date]=\"model.PAF_BTM_DETAIL.PBD_FX_DATE_FROM\"\n                  [end_date]=\"model.PAF_BTM_DETAIL.PBD_FX_DATE_TO\"\n                  (modelChange)=\"model.PAF_BTM_DETAIL.PBD_FX_DATE_FROM = $event.start_date;model.PAF_BTM_DETAIL.PBD_FX_DATE_TO = $event.end_date;loadFX()\"\n                ></ptx-daterangepicker>\n              </td>\n              <td>\n                <input\n                  class=\"form-control text-right\"\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [disabled]=\"true\"\n                  [(ngModel)]=\"model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                >\n                  <!-- <input class=\"form-control\" type=\"text\" value=\"35.13\" [disabled]=\"true\"> -->\n              </td>\n              <td>{{ model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX_UNIT }}</td>\n            </tr>\n            <tr>\n              <td>3</td>\n              <td class=\"text-left\">\n                Avg. HSFO in previous loading month<br/>\n                <ptx-daterangepicker\n                  [disabled]=\"model.IS_DISABLED\"\n                  [start_date]=\"model.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM\"\n                  [end_date]=\"model.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO\"\n                  (modelChange)=\"model.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM = $event.start_date;model.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO = $event.end_date;loadHSFO()\"\n                ></ptx-daterangepicker>\n              </td>\n              <td>\n                <input\n                  class=\"form-control text-right\"\n                  type=\"text\"\n                  ptxRestrictNumber\n                  [disabled]=\"true\"\n                  [(ngModel)]=\"model.PAF_BTM_DETAIL.PBD_AVG_HSFO\"\n                  (ngModelChange)=\"HandleModelUpdate();\"\n                >\n              </td>\n              <td>{{ model.PAF_BTM_DETAIL.PBD_AVG_HSFO_UNIT }}</td>\n            </tr>\n          </tbody>\n      </table>\n    </div>\n    <div class=\"col-xs-4 col-xs-offset-1\">\n      <h3 class=\"text-bold\">Weighted Bitumen Price</h3>\n      <div class=\"row mb-10\">\n        <div class=\"col-xs-6\">Simplan (production plan)</div>\n        <div class=\"col-xs-3 pl-0\">\n          <input\n            class=\"form-control text-right\"\n            type=\"text\"\n            ptxRestrictNumber\n            [disabled]=\"model.IS_DISABLED\"\n            [(ngModel)]=\"model.PAF_BTM_DETAIL.PBD_SIM_PLAN\"\n            (ngModelChange)=\"HandleModelUpdate();\"\n          >\n        </div>\n        <div class=\"col-xs-3 pl-0\">{{ model.PAF_BTM_DETAIL.PBD_SIM_UNIT }}</div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-6\">Corp. Plan</div>\n        <div class=\"col-xs-3 pl-0\">\n          <input\n            class=\"form-control text-right\"\n            type=\"text\"\n            ptxRestrictNumber\n            [disabled]=\"model.IS_DISABLED\"\n            [(ngModel)]=\"model.PAF_BTM_DETAIL.PBD_CROP_PLAN\"\n            (ngModelChange)=\"HandleModelUpdate();\"\n          >\n        </div>\n        <div class=\"col-xs-3 pl-0\">{{ model.PAF_BTM_DETAIL.PBD_CORP_UNIT }}</div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row mt-30\">\n    <div class=\"col-xs-12\">\n      <div class=\"form-group\">\n        <h3 class=\"text-bold\">Brief Market Situation (as necessary)</h3>\n        <div class=\"col-xs-12\">\n          <app-freetext\n            *ngIf=\"true\"\n            [disabled]=\"model.IS_DISABLED_NOTE\"\n            [elementId]=\"'PDA_BRIEF'\"\n            [model]=\"model.PDA_BRIEF\"\n            (HandleStateChange)=\"model.PDA_BRIEF = $event\">\n          </app-freetext>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row mt-10\">\n    <ptx-file-upload\n      btnClass=\"text-left\"\n      [model]=\"model.PAF_ATTACH_FILE\"\n      [is_disabled]=\"model.IS_DISABLED\"\n      [need_caption]=\"true\"\n      [desc]=\"'Image files'\"\n      filterBy=\"BMS\"\n      (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\"\n    >\n    </ptx-file-upload>\n  </div>\n\n  <h4 class=\"second-title mt-45\">Price Calculation</h4>\n\n  <ng-template ngFor let-elem [ngForOf]=\"model.PAF_BTM_GRADE_ITEMS\" let-index=\"index\">\n    <div class=\"row mt-20\">\n      <div class=\"col-xs-6\">\n        <h3 class=\"text-bold mb-10\">Bitumen Grade\n          <input\n            class=\"form-control form-inline text-center ml-10 min-200\"\n            type=\"text\"\n            [disabled]=\"true\"\n            [ngModel]=\"ShowProduct(elem.PBG_FK_MATERIAL)\"\n          >\n        </h3>\n        <table class=\"table table-bordered\">\n            <thead>\n              <tr>\n                <th width=\"5%\">No</th>\n                <th width=\"\">Assumption Detail</th>\n                <th width=\"10%\"></th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let g of model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_GRADE_ASSUME_ITEMS;let i=index\">\n                <td valign=\"middle\">{{ (i + 1) }}</td>\n                <td class=\"text-left\">\n                  <input\n                    class=\"form-control\"\n                    type=\"text\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_GRADE_ASSUME_ITEMS[i].PGA_DETAIL\"\n                  >\n                </td>\n                <td valign=\"middle\">\n                  <a href=\"javascript:void(0)\" (click)=\"RemoveGradeAssume(index, g)\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </td>\n              </tr>\n            </tbody>\n        </table>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <button class=\"btn btn-success\" (click)=\"AddAssumption(index)\"> Add Assumption</button>\n      </div>\n    </div>\n\n    <div class=\"row mt-45\">\n      <div style=\"overflow-x: scroll;\">\n        <table class=\"table table-bordered\">\n          <thead>\n            <tr>\n              <th rowspan=\"2\"></th>\n              <th rowspan=\"2\">Customer</th>\n              <th rowspan=\"2\" class=\"ptx-w-150\">Tier</th>\n              <th rowspan=\"2\">Volume (MT)</th>\n              <th colspan=\"4\">Pricing Structure</th>\n              <th rowspan=\"2\">Comparing to Argus</th>\n              <th rowspan=\"2\"></th>\n            </tr>\n            <tr>\n              <th class=\"ptx-w-180\">Formula</th>\n              <th>USD/MT</th>\n              <th>Baht/MT</th>\n              <th>Value (Baht)</th>\n            </tr>\n          </thead>\n          <tbody>\n            <!-- <tr *ngFor=\"let e of model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS_DETAIL;\">\n              <td class=\"headcol\">\n                <div class=\"col-md-2\">\n                  <input\n                    type=\"radio\"\n                    name=\"check_got_bid\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"criteria.SELECTED_CUSTOMER\"\n                    [value]=\"index\"\n                  >\n                </div>\n              </td>\n              <td>\n                <input class=\"form-control text-center\" value=\"PTT\" type=\"text\" [disabled]=\"true\">\n              </td>\n              <td>\n                <select class=\"form-control\">\n                  <option>Domestic Tier 1</option>\n                </select>\n                  <br />\n                <select class=\"form-control\">\n                  <option>Domestic Tier 2</option>\n                </select>\n                <br />\n                SUM\n              </td>\n              <td>\n                <input class=\"form-control text-center\" type=\"text\" value=\"15,000\"><br />\n                <input class=\"form-control text-center\" type=\"text\" value=\"4,582\"><br />\n                <input class=\"form-control text-center ptx-bg-yellow\" type=\"text\" value=\"19,582\">\n              </td>\n              <td>\n                <input class=\"form-control\" type=\"text\" value=\"Negotiation\"><br />\n                <input class=\"form-control\" type=\"text\" value=\"Argus - 10 = 305 - 45\"><br />\n              </td>\n              <td>\n                <input class=\"form-control text-right\" type=\"text\" value=\"310.00\" [disabled]=\"true\"><br />\n                <input class=\"form-control text-right\" type=\"text\" value=\"260.00\" [disabled]=\"true\"><br />\n                <input class=\"form-control text-right ptx-bg-yellow\" type=\"text\" value=\"570.00\">\n              </td>\n              <td>\n                <input class=\"form-control text-right\" type=\"text\" value=\"310.00\"><br />\n                <input class=\"form-control text-right\" type=\"text\" value=\"260.00\"><br />\n                <input class=\"form-control text-right ptx-bg-yellow\" type=\"text\" value=\"570.00\">\n              </td>\n              <td>\n                <input class=\"form-control text-right\" type=\"text\" value=\"310.00\" [disabled]=\"true\"><br />\n                <input class=\"form-control text-right\" type=\"text\" value=\"260.00\" [disabled]=\"true\"><br />\n                <input class=\"form-control text-right ptx-bg-yellow\" type=\"text\" value=\"570.00\">\n              </td>\n              <td>\n                <input class=\"form-control text-center\" type=\"text\" value=\"Argus + 5\" [disabled]=\"true\"><br />\n                <input class=\"form-control text-center\" type=\"text\" value=\"Argus - 45\" [disabled]=\"true\"><br />\n                <input class=\"form-control text-center ptx-bg-yellow\" type=\"text\" value=\"Argus - 7\">\n              </td>\n              <td>\n                <div class=\"mt-5 mb-20\">\n                  <a href=\"#\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </div>\n                <div class=\"mt-5 mb-20\">\n                  <a href=\"#\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </div>\n              </td>\n            </tr> -->\n            <ng-template ngFor let-e [ngForOf]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS|allocationsFilter\" let-k=\"index\">\n              <ng-template ngFor let-f [ngForOf]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL\" let-l=\"index\">\n                <tr>\n                  <td class=\"headcol\">\n                    <div class=\"col-md-2\" *ngIf=\"l == 0\">\n                      <input\n                        type=\"radio\"\n                        name=\"SELECTED_CUSTOMER\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"criteria.SELECTED_CUSTOMER\"\n                        [value]=\"GetIndexAllocationItems(index, e)\"\n                      >\n                    </div>\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      *ngIf=\"l == 0\"\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      [disabled]=\"true\"\n                      [ngModel]=\"ShowCustomer(e.PBA_FK_CUSTOMER)\"\n                    >\n                  </td>\n                  <td class=\"min-200\">\n                    <ptx-sugester\n                      [model]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_FK_PAF_MT_BTM_TIER\"\n                      [data_list]=\"master.PAF_MT_BTM_TIER\"\n                      [is_disabled]=\"model.IS_DISABLED\"\n                      [show_field]=\"'NAME'\"\n                      [select_field]=\"'ID'\"\n                      (ModelChanged)=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_FK_PAF_MT_BTM_TIER = $event;\"\n                    >\n                    </ptx-sugester>\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      ptxRestrictNumber\n                      [fraction]=\"0\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_TIER_VOLUME\"\n                    >\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_FORMULA\"\n                    >\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      ptxRestrictNumber\n                      [fraction]=\"0\"\n                      [disabled]=\"model.IS_DISABLED\"\n                      [(ngModel)]=\"model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_USD_PER_UNIT\"\n                    >\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      ptxRestrictNumber\n                      [fraction]=\"0\"\n                      [disabled]=\"true\"\n                      [ngModel]=\"CalTHBMT(index, GetIndexAllocationItems(index, e), l)\"\n                    >\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      ptxRestrictNumber\n                      [fraction]=\"0\"\n                      [disabled]=\"true\"\n                      [ngModel]=\"CalTHBValue(index, GetIndexAllocationItems(index, e), l)\"\n                    >\n                    <!-- <input class=\"form-control text-right\" type=\"text\" value=\"310.00\" [disabled]=\"true\"> -->\n                  </td>\n                  <td class=\"min-200\">\n                    <input\n                      class=\"form-control text-center\"\n                      type=\"text\"\n                      [disabled]=\"true\"\n                      [ngModel]=\"CalComparingToArgus(index, GetIndexAllocationItems(index, e), l)\"\n                    >\n                    <!-- <input class=\"form-control text-center\" type=\"text\" value=\"Argus + 5\" [disabled]=\"true\"> -->\n                  </td>\n                  <td>\n                    <a href=\"javascript:void(0)\" class=\"mb-10\" (click)=\"RemoveAllocateItemTier(index, GetIndexAllocationItems(index, e), l, f)\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n                    <!-- <a class=\"mb-10\" href=\"#\">\n                      <span class=\"glyphicon glyphicon-trash\"></span>\n                    </a> -->\n                  </td>\n                </tr>\n              </ng-template>\n              <tr *ngIf=\"ShowSumRow(model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL)\">\n                <td class=\"headcol\">\n                </td>\n                <td class=\"min-200\">\n                </td>\n                <td class=\"min-200\">\n                  SUM\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    class=\"form-control text-center ptx-bg-yellow\"\n                    type=\"text\"\n                    ptxRestrictNumber\n                    [fraction]=\"0\"\n                    [disabled]=\"true\"\n                    [ngModel]=\"CalSummaryVolume(model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL)\"\n                  >\n                </td>\n                <td class=\"min-200\">\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    class=\"form-control text-center ptx-bg-yellow\"\n                    type=\"text\"\n                    ptxRestrictNumber\n                    [fraction]=\"0\"\n                    [disabled]=\"true\"\n                    [ngModel]=\"CalSummaryUSDMT(model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL)\"\n                  >\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    class=\"form-control text-center ptx-bg-yellow\"\n                    type=\"text\"\n                    ptxRestrictNumber\n                    [fraction]=\"0\"\n                    [disabled]=\"true\"\n                    [ngModel]=\"CalSummaryBATHMT(model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL)\"\n                  >\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    class=\"form-control text-center ptx-bg-yellow\"\n                    type=\"text\"\n                    ptxRestrictNumber\n                    [fraction]=\"0\"\n                    [disabled]=\"true\"\n                    [ngModel]=\"CalSummaryBATHValue(model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL)\"\n                  >\n                  <!-- <input class=\"form-control text-right\" type=\"text\" value=\"310.00\" [disabled]=\"true\"> -->\n                </td>\n                <td class=\"min-200\">\n                  <input\n                    class=\"form-control text-center ptx-bg-yellow\"\n                    type=\"text\"\n                    [disabled]=\"true\"\n                    [ngModel]=\"CalSummaryComparingToArgus(model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[GetIndexAllocationItems(index, e)].PAF_BTM_ALLOCATE_ITEMS_DETAIL)\"\n                  >\n                  <!-- <input class=\"form-control text-center\" type=\"text\" value=\"Argus + 5\" [disabled]=\"true\"> -->\n                </td>\n                <td>\n                </td>\n              </tr>\n            </ng-template>\n          </tbody>\n        </table>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <button class=\"btn btn-primary\" (click)=\"AddAllocationTier(index)\"> Add Tier</button>\n        </div>\n      </div>\n    </div>\n\n    <!-- <pre>{{ GetSummaryList(index) | json }}</pre> -->\n    <div class=\"row mt-45\">\n      <table class=\"table table-bordered\">\n        <thead>\n          <tr>\n            <th colspan=\"7\">Summary</th>\n          </tr>\n          <tr>\n            <th rowspan=\"2\">Customer</th>\n            <th rowspan=\"2\">Tier</th>\n            <th rowspan=\"2\">Volume (MT)</th>\n            <th colspan=\"2\">Price</th>\n            <th rowspan=\"2\">Selling Price - Argus Bitumen</th>\n            <th rowspan=\"2\">Selling Price - HSFO Price</th>\n          </tr>\n          <tr>\n            <th>Baht/MT</th>\n            <th>USD/MT</th>\n          </tr>\n        </thead>\n        <tbody>\n          <ng-template ngFor let-e [ngForOf]=\"GetSummaryList(index)\" let-k=\"index\">\n            <ng-template ngFor let-f [ngForOf]=\"e.PAF_BTM_ALLOCATE_ITEMS_DETAIL\" let-l=\"index\">\n            <!-- <tr *ngIf=\"ShowSumRow(e.PAF_BTM_ALLOCATE_ITEMS_DETAIL)\">\n              <td>PTT</td>\n              <td>Domestic</td>\n              <td>19,582</td>\n              <td class=\"text-right\">10,471.00</td>\n              <td class=\"text-right\">298.00</td>\n              <td>-7</td>\n              <td>-3</td>\n            </tr>-->\n              <tr>\n                <td> {{ l == 0 ? e.SHOW_CUSTOMER: '' }} </td>\n                <td> {{ f.TIER_NAME }} </td>\n                <td> {{ f.SHOW_VOLUME | number:'1.0-0' }} </td>\n                <td class=\"text-right\">{{ f.SHOW_BATH_MT | number:'1.0-0' }}</td>\n                <td class=\"text-right\">{{ f.SHOW_USD_MT | number:'1.0-0' }}</td>\n                <td>{{ f.SHOW_ARGUS_DIFF | number:'1.0-0' }}</td>\n                <td>{{ f.SHOW_HSFO_DIFF | number:'1.0-0' }}</td>\n              </tr>\n            </ng-template>\n          </ng-template>\n        </tbody>\n      </table>\n    </div>\n\n    <div class=\"row mt-30\">\n      <div class=\"col-md-12 mb-10\">\n      <h3 class=\"text-bold\">Weighted Price</h3>\n      </div>\n      <div class=\"col-xs-4\">\n        <table class=\"table table-bordered\">\n          <tbody>\n            <tr>\n              <td class=\"text-right\">{{ GetWeightedPriceTHB(index) | number:'1.0-0' }}</td>\n              <td>Baht/MT</td>\n            </tr>\n            <tr>\n              <td class=\"text-right\">{{ GetWeightedPriceUSD(index) | number:'1.0-0' }}</td>\n              <td>USD/MT</td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n      <div class=\"col-xs-3\" style=\"margin-top: 38px;\">\n        <input\n          class=\"form-control\"\n          [ngModel]=\"GetPlanCompare(index)\"\n          type=\"text\"\n          [disabled]=\"true\"\n        >\n      </div>\n      <div class=\"col-xs-4\">\n        <table class=\"table table-bordered\">\n          <thead>\n            <tr>\n              <th>Wgt.Price - Argus</th>\n              <th>Wgt.Price - HSFO</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr>\n              <td>{{ GetWeightedPriceDiffArgus(index) | number:'1.0-0' }}</td>\n              <td>{{ GetWeightedPriceDiffHSFO(index) | number:'1.0-0' }}</td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </ng-template>\n\n\n  <!-- <div class=\"row mt-80\">\n    <div class=\"col-xs-6\">\n      <h3 class=\"text-bold mb-10\">Bitumen Grade <input class=\"form-control form-inline text-center ml-10\" value=\"70/80\" type=\"text\" [disabled]=\"true\"></h3>\n      <table class=\"table table-bordered\">\n          <thead>\n            <tr>\n              <th width=\"5%\">No</th>\n              <th width=\"\">Assumption Detail</th>\n              <th width=\"10%\"></th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr>\n              <td valign=\"middle\"></td>\n              <td class=\"text-left\"><input class=\"form-control\" type=\"text\"></td>\n              <td valign=\"middle\">\n                <a href=\"#\"><span class=\"glyphicon glyphicon-trash\"></span></a>\n              </td>\n            </tr>\n          </tbody>\n      </table>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <button class=\"btn btn-success\"> Add Assumption</button>\n    </div>\n  </div>\n\n  <div class=\"row mt-45\">\n    <div style=\"overflow-x: scroll;\">\n      <table class=\"table table-bordered\">\n        <thead>\n          <tr>\n            <th rowspan=\"2\"></th>\n            <th rowspan=\"2\">Customer</th>\n            <th rowspan=\"2\" class=\"ptx-w-180\">Tier</th>\n            <th rowspan=\"2\">Volume (MT)</th>\n            <th colspan=\"4\">Pricing Structure</th>\n            <th rowspan=\"2\">Comparing to Argus</th>\n            <th rowspan=\"2\"></th>\n          </tr>\n          <tr>\n            <th class=\"ptx-w-180\">Formula</th>\n            <th>USD/MT</th>\n            <th>Baht/MT</th>\n            <th>Value (Baht)</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td class=\"headcol\">\n              <div class=\"checkbox\">\n                  <input\n                    id=\"check_got_bid{{index}}\"\n                    type=\"checkbox\"\n                  >\n                  <label for=\"check_got_bid{{index}}\"></label>\n              </div>\n            </td>\n            <td><input class=\"form-control text-center\" value=\"GPP\" type=\"text\" [disabled]=\"true\"></td>\n            <td>\n              <select class=\"form-control\">\n                    <option>Export Spot</option>\n              </select><br />\n              <select class=\"form-control\">\n                    <option>Export Indochina Spot</option>\n              </select>\n            </td>\n            <td>\n              <input class=\"form-control text-center\" type=\"text\"><br />\n              <input class=\"form-control text-center\" type=\"text\">\n            </td>\n            <td>\n              <input class=\"form-control text-center\" type=\"text\"><br />\n              <input class=\"form-control text-center\" type=\"text\">\n            </td>\n            <td>\n              <input class=\"form-control text-right\" type=\"text\" [disabled]=\"true\"><br />\n              <input class=\"form-control text-right\" type=\"text\" [disabled]=\"true\">\n            </td>\n            <td>\n              <input class=\"form-control text-right\" type=\"text\"><br />\n              <input class=\"form-control text-right\" type=\"text\">\n            </td>\n            <td>\n              <input class=\"form-control text-right\" type=\"text\" [disabled]=\"true\"><br />\n              <input class=\"form-control text-right\" type=\"text\" [disabled]=\"true\">\n            </td>\n            <td>\n              <input class=\"form-control text-center\" type=\"text\" [disabled]=\"true\"><br />\n              <input class=\"form-control text-center\" type=\"text\" [disabled]=\"true\">\n            </td>\n            <td>\n              <div class=\"mt-5 mb-20\">\n                <a href=\"#\">\n                  <span class=\"glyphicon glyphicon-trash\"></span>\n                </a>\n              </div>\n              <div class=\"mt-5 mb-20\">\n                <a href=\"#\">\n                  <span class=\"glyphicon glyphicon-trash\"></span>\n                </a>\n              </div>\n            </td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <button class=\"btn btn-primary\"> Add Tier</button>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row mt-45\">\n    <table class=\"table table-bordered\">\n      <thead>\n        <tr>\n          <th colspan=\"7\">Summary</th>\n        </tr>\n        <tr>\n          <th rowspan=\"2\">Customer</th>\n          <th rowspan=\"2\">Tier</th>\n          <th rowspan=\"2\">Volume (MT)</th>\n          <th colspan=\"2\">Price</th>\n          <th rowspan=\"2\">Selling Price - Argus Bitumen</th>\n          <th rowspan=\"2\">Selling Price - HSFO Price</th>\n        </tr>\n        <tr>\n          <th>Baht/MT</th>\n          <th>USD/MT</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td>PTT</td>\n          <td>Domestic</td>\n          <td>19,582</td>\n          <td class=\"text-right\">10,471.00</td>\n          <td class=\"text-right\">298.00</td>\n          <td>-7</td>\n          <td>-3</td>\n        </tr>\n        <tr>\n          <td>IRPC</td>\n          <td>Export Indochina</td>\n          <td>5,386</td>\n          <td class=\"text-right\">12,471.00</td>\n          <td class=\"text-right\">345.00</td>\n          <td>40</td>\n          <td>45</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n\n  <div class=\"row mt-30\">\n    <div class=\"col-md-12 mb-10\">\n    <h3 class=\"text-bold\">Weighted Price</h3>\n    </div>\n    <div class=\"col-xs-4\">\n      <table class=\"table table-bordered\">\n        <tbody>\n          <tr>\n            <td class=\"text-right\">10,828.00</td>\n            <td>Baht/MT</td>\n          </tr>\n          <tr>\n            <td class=\"text-right\">308.00</td>\n            <td>USD/MT</td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n    <div class=\"col-xs-3\">\n      <input class=\"form-control\" value=\"> Simplan, Corp.Plan\" type=\"text\" [disabled]=\"true\">\n    </div>\n    <div class=\"col-xs-4\">\n      <table class=\"table table-bordered\">\n        <thead>\n          <tr>\n            <th>Wgt.Price - Argus</th>\n            <th>Wgt.Price - HSFO</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr>\n            <td>3</td>\n            <td>8</td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div> -->\n\n\n  <div class=\"row mt-30\">\n    <div class=\"col-xs-12\">\n      <div class=\"form-group\">\n        <label class=\"control-label\">Note</label>\n        <textarea\n          class=\"form-control inline-textarea\"\n          [disabled]=\"model.IS_DISABLED_NOTE\"\n          [(ngModel)]=\"model.PDA_NOTE\"></textarea>\n      </div>\n      <ptx-file-upload\n          btnClass=\"text-left\"\n          [model]=\"model.PAF_ATTACH_FILE\"\n          [is_disabled]=\"model.IS_DISABLED\"\n          [filterBy]=\"'REF'\"\n          (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\"\n        >\n        </ptx-file-upload>\n    </div>\n  </div>\n\n    <div class=\"wrapper-button\" id=\"buttonDiv\">\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft(model)\"> SAVE DRAFT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit(model)\"> SAVE & SUBMIT </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify(model)\"> VERIFY </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse(model)\"> ENDORSE </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved(model)\"> APPROVE </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject(model)\"> REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel(model)\"> CANCEL </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>\n    </div>\n\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-bitumen.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaBitumenComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CmlaBitumenComponent = (function () {
    function CmlaBitumenComponent(route, router, loaderService, masterService, pafService, decimalPipe, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.decimalPipe = decimalPipe;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.criteria = {
            SELECT_GRADE: 0,
            SELECTED_CUSTOMER: 0,
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.PAF_BTM_ALLOCATE_ITEMS = {
            PAF_BTM_ALLOCATE_ITEMS_DETAIL: [{}],
        };
        this.PAF_BTM_GRADE_ITEM = {
            PAF_BTM_ALLOCATE_ITEMS: [__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.PAF_BTM_ALLOCATE_ITEMS)],
            PAF_BTM_GRADE_ASSUME_ITEMS: [{
                    PGA_NO: 1,
                    PGA_DETAIL: "",
                }],
        };
        this.model = {
            PDA_FORM_ID: 'Draft',
            PDA_TEMPLATE: 'CMLA_BITUMEN',
            // PDA_TYPE: 'Type of Import',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_5_moment_moment__().format('YYYY-MM-DD'),
            PAF_BTM_DETAIL: {
                PBD_VOLUME_UNIT: 'MT',
                PBD_ARGUS_PRICE_UNIT: 'USD/MT',
                PBD_ESTIMATED_FX_UNIT: 'Baht/USD',
                PBD_AVG_HSFO_UNIT: 'USD/MT',
                PBD_FX_DATE_FROM: __WEBPACK_IMPORTED_MODULE_5_moment_moment__().startOf('month').format('YYYY-MM-DD'),
                PBD_FX_DATE_TO: __WEBPACK_IMPORTED_MODULE_5_moment_moment__().format('YYYY-MM-DD'),
                PBD_HSFO_DATE_FROM: __WEBPACK_IMPORTED_MODULE_5_moment_moment__().startOf('month').format('YYYY-MM-DD'),
                PBD_HSFO_DATE_TO: __WEBPACK_IMPORTED_MODULE_5_moment_moment__().format('YYYY-MM-DD'),
                PBD_ESTIMATED_FX: 0,
                PBD_AVG_HSFO: 0,
                PBD_SIM_UNIT: 'USD/MT',
                PBD_SIM_PLAN: 0,
                PBD_CROP_PLAN: 0,
                PBD_CORP_UNIT: 'USD/MT',
            },
            PAF_BTM_GRADE_ITEMS: [__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.PAF_BTM_GRADE_ITEM)],
        };
        this.master = {};
    }
    CmlaBitumenComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmlaBitumenComponent.prototype.ngAfterViewInit = function () {
    };
    CmlaBitumenComponent.prototype.ngOnDestroy = function () {
    };
    CmlaBitumenComponent.prototype.HandleModelUpdate = function () {
        // code...
    };
    CmlaBitumenComponent.prototype.AddBitumenGrade = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.PAF_BTM_GRADE_ITEMS.push(__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.PAF_BTM_GRADE_ITEM));
    };
    CmlaBitumenComponent.prototype.AddAllocation = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var index = this.criteria.SELECT_GRADE;
        // if (index) {
        this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS.push(__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.PAF_BTM_ALLOCATE_ITEMS));
        // }
        console.log(index);
    };
    CmlaBitumenComponent.prototype.AddAssumption = function (index) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var PAF_BTM_GRADE_ASSUME_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["last"](this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_GRADE_ASSUME_ITEMS);
        this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_GRADE_ASSUME_ITEMS.push({
            PGA_NO: (PAF_BTM_GRADE_ASSUME_ITEMS && (PAF_BTM_GRADE_ASSUME_ITEMS.PGA_NO + 1)) + 1
        });
    };
    CmlaBitumenComponent.prototype.AddAllocationTier = function (index) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var k = this.criteria.SELECTED_CUSTOMER;
        this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL.push(__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone({}));
        console.log(index);
    };
    CmlaBitumenComponent.prototype.updateBtmGrade = function () {
        this.model.PAF_BTM_GRADE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["map"](this.model.PAF_BTM_GRADE_ITEMS, function (f) {
            f.PAF_BTM_ALLOCATE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["map"](f.PAF_BTM_ALLOCATE_ITEMS, function (e) {
                e.PAF_BTM_ALLOCATE_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], e.PAF_BTM_ALLOCATE_ITEMS_DETAIL);
                if (e.PAF_BTM_ALLOCATE_ITEMS_DETAIL.length == 0) {
                    e.PAF_BTM_ALLOCATE_ITEMS_DETAIL.push(__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone({}));
                }
                return e;
            });
            return f;
        });
    };
    CmlaBitumenComponent.prototype.RemoveAllocateItemTier = function (index, k, tier_num, e) {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var PAF_BTM_ALLOCATE_ITEMS = this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k];
        var customer_id = PAF_BTM_ALLOCATE_ITEMS && PAF_BTM_ALLOCATE_ITEMS.PBA_FK_CUSTOMER;
        console.log(customer_id);
        var customer = this.ShowCustomer(customer_id);
        var tier = __WEBPACK_IMPORTED_MODULE_4_lodash__["find"](this.master.PAF_MT_BTM_TIER, function (f) {
            return f.ID == e.PAD_FK_PAF_MT_BTM_TIER;
        });
        var tier_name = (tier && tier.NAME) || "";
        customer = customer || "";
        var popup_content = "Are you sure you want to remove this tier?<br />\n    <br /><b>Customer:</b> " + customer + "\n    <br /><b>Tier:</b> " + tier_name;
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_4_lodash__["reject"](_this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL, function (elem) {
                return e == elem;
            });
            _this.updateBtmGrade();
        });
    };
    CmlaBitumenComponent.prototype.RemoveAllocateItem = function (index, e) {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var customer_id = e && e.PBA_FK_CUSTOMER;
        var customer = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].getTextByID(customer_id, this.master.CUSTOMERS);
        customer = customer || "";
        var popup_content = "Are you sure you want to remove this allocation?<br /><br /><b>Customer:</b> " + customer;
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["reject"](_this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS, function (elem) {
                return e == elem;
            });
        });
    };
    CmlaBitumenComponent.prototype.RemoveBitumenGrade = function (e) {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var product_id = e && e.PBG_FK_MATERIAL;
        var product = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].getTextByID(product_id, this.master.PAF_CMLA_BITUMEN);
        product = product || "";
        var popup_content = "Are you sure you want to remove this bitumen grade?<br /><br /><b>Bitumen Grade:</b> " + product;
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_BTM_GRADE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["reject"](_this.model.PAF_BTM_GRADE_ITEMS, function (elem) {
                return e == elem;
            });
        });
    };
    CmlaBitumenComponent.prototype.RemoveGradeAssume = function (index, e) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var popup_content = "Are you sure you want to remove this assumption?";
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_GRADE_ASSUME_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["reject"](context.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_GRADE_ASSUME_ITEMS, function (elem) {
                return e == elem;
            });
        });
    };
    CmlaBitumenComponent.prototype.ShowSumRow = function (l) {
        var _this = this;
        var result = true;
        var groups = [];
        // console.log(l)
        groups = __WEBPACK_IMPORTED_MODULE_4_lodash__["map"](l, function (e) {
            var tier = __WEBPACK_IMPORTED_MODULE_4_lodash__["find"](_this.master.PAF_MT_BTM_TIER, function (f) {
                return f.ID == e.PAD_FK_PAF_MT_BTM_TIER;
            });
            return tier && tier.GROUP;
        });
        groups = __WEBPACK_IMPORTED_MODULE_4_lodash__["uniq"](__WEBPACK_IMPORTED_MODULE_4_lodash__["compact"](groups));
        return l.length > 1 && groups.length == 1;
    };
    CmlaBitumenComponent.prototype.CalBitumenGradeColSpan = function (index) {
        var result = 1;
        var PAF_BTM_ALLOCATE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS);
        result += +PAF_BTM_ALLOCATE_ITEMS.length;
        return result;
    };
    // public showCustomer(id: string) : string {
    //   let e = _.find(this.master.CUSTOMERS, (e) => {
    //     return e.ID == id;
    //   });
    //   return e && e.VALUE;
    // }
    CmlaBitumenComponent.prototype.CalTotalOS = function (index) {
        var PAF_BTM_GRADE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], this.model.PAF_BTM_GRADE_ITEMS);
        var PAF_BTM_GRADE_ITEM = PAF_BTM_GRADE_ITEMS && PAF_BTM_GRADE_ITEMS.length > index && PAF_BTM_GRADE_ITEMS[index];
        var PBG_SAFETY_STOCK = (PAF_BTM_GRADE_ITEM && +PAF_BTM_GRADE_ITEM.PBG_SAFETY_STOCK) || 0;
        var PBG_OS_ACTUAL = (PAF_BTM_GRADE_ITEM && +PAF_BTM_GRADE_ITEM.PBG_OS_ACTUAL) || 0;
        return PBG_SAFETY_STOCK + PBG_OS_ACTUAL;
    };
    CmlaBitumenComponent.prototype.CalTotalCS = function (index) {
        var PAF_BTM_GRADE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], this.model.PAF_BTM_GRADE_ITEMS);
        var PAF_BTM_GRADE_ITEM = PAF_BTM_GRADE_ITEMS && PAF_BTM_GRADE_ITEMS.length > index && PAF_BTM_GRADE_ITEMS[index];
        var deffered_volume = (PAF_BTM_GRADE_ITEM && +PAF_BTM_GRADE_ITEM.PBG_DEFFERED_VOLUME) || 0;
        var production = (PAF_BTM_GRADE_ITEM && +PAF_BTM_GRADE_ITEM.PBG_PRODUCTION) || 0;
        var total_cs = this.CalTotalOS(index);
        var total_allocation = this.CalTotalAllocation(index);
        return total_cs + production - deffered_volume - total_allocation;
    };
    CmlaBitumenComponent.prototype.CalTHBMT = function (index, k, l) {
        var PAD_USD_PER_UNIT = 0;
        var PBD_ESTIMATED_FX = 0;
        try {
            PBD_ESTIMATED_FX = +this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX || 0;
            PAD_USD_PER_UNIT = +this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_USD_PER_UNIT;
        }
        catch (e) {
            console.log(e);
        }
        var result = __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](PAD_USD_PER_UNIT * PBD_ESTIMATED_FX);
        return result || 0;
    };
    CmlaBitumenComponent.prototype.CalTHBValue = function (index, k, l) {
        var PAD_USD_PER_UNIT = 0;
        var PAD_TIER_VOLUME = 0;
        var PBD_ESTIMATED_FX = 0;
        // try {
        //   PAD_USD_PER_UNIT = +this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_USD_PER_UNIT;
        //   PAD_TIER_VOLUME = +this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_TIER_VOLUME;
        //   PBD_ESTIMATED_FX = +this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX || 0;
        // } catch(e) {
        //   console.log(e)
        // }
        // PAD_USD_PER_UNIT = PAD_USD_PER_UNIT || 0;
        // PAD_TIER_VOLUME = PAD_TIER_VOLUME || 0;
        // PBD_ESTIMATED_FX = PBD_ESTIMATED_FX || 0;
        var BAHT_MT = this.CalTHBMT(index, k, l);
        try {
            PAD_TIER_VOLUME = +this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_TIER_VOLUME;
        }
        catch (e) {
            console.log(e);
        }
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](PAD_TIER_VOLUME * BAHT_MT) || 0;
    };
    CmlaBitumenComponent.prototype.CalComparingToArgus = function (index, k, l) {
        // let USD_MT = this.(index, k, l);
        var USD_MT = 0;
        var PBD_ARGUS_PRICE = 0;
        try {
            USD_MT = +this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS[k].PAF_BTM_ALLOCATE_ITEMS_DETAIL[l].PAD_USD_PER_UNIT;
            PBD_ARGUS_PRICE = +this.model.PAF_BTM_DETAIL.PBD_ARGUS_PRICE || 0;
            // PBD_ARGUS_PRICE = _.round(PBD_ARGUS_PRICE);
        }
        catch (e) {
            console.log(e);
        }
        USD_MT = +USD_MT || 0;
        PBD_ARGUS_PRICE = +PBD_ARGUS_PRICE || 0;
        var result = +USD_MT - +PBD_ARGUS_PRICE;
        var signed = __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](result) < 0 ? '-' : '+';
        var result_str = this.decimalPipe.transform(Math.abs(result), '1.0');
        return "Argus " + signed + " " + result_str;
    };
    CmlaBitumenComponent.prototype.CalTotalAllocation = function (index) {
        var PAF_BTM_GRADE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], this.model.PAF_BTM_GRADE_ITEMS);
        var PAF_BTM_GRADE_ITEM = PAF_BTM_GRADE_ITEMS && PAF_BTM_GRADE_ITEMS.length > index && PAF_BTM_GRADE_ITEMS[index];
        var PAF_BTM_ALLOCATE_ITEMS = PAF_BTM_GRADE_ITEM && PAF_BTM_GRADE_ITEM.PAF_BTM_ALLOCATE_ITEMS;
        PAF_BTM_ALLOCATE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], PAF_BTM_ALLOCATE_ITEMS);
        var result = __WEBPACK_IMPORTED_MODULE_4_lodash__["sumBy"](PAF_BTM_ALLOCATE_ITEMS, function (e) {
            return +e.PAV_VOLUME || 0;
        });
        return result;
    };
    CmlaBitumenComponent.prototype.CalSummaryVolume = function (e) {
        var result = __WEBPACK_IMPORTED_MODULE_4_lodash__["sumBy"](e, function (l) { return +l.PAD_TIER_VOLUME || 0; });
        // console.log(result);
        return result;
    };
    CmlaBitumenComponent.prototype.CalSummaryUSDMT = function (e) {
        // let PBD_ESTIMATED_FX = 0
        // try {
        //   PBD_ESTIMATED_FX = +this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX || 0;
        // } catch(e){
        //   console.log(e)
        // }
        // let result = this.CalSummaryBATHMT(e)*PBD_ESTIMATED_FX;
        var result = __WEBPACK_IMPORTED_MODULE_4_lodash__["sumBy"](e, function (l) {
            var PAD_USD_PER_UNIT = +l.PAD_USD_PER_UNIT || 0;
            var PAD_TIER_VOLUME = +l.PAD_TIER_VOLUME || 0;
            return (PAD_USD_PER_UNIT * PAD_TIER_VOLUME) || 0;
        });
        result /= this.CalSummaryVolume(e);
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](result);
    };
    CmlaBitumenComponent.prototype.CalSummaryBATHMT = function (e) {
        // let result = this.CalSummaryBATHValue(e)/this.CalSummaryVolume(e);
        // let result = _.sumBy(e, (l) => {
        //   let PAD_USD_PER_UNIT = +l.PAD_USD_PER_UNIT || 0;
        //   let PAD_TIER_VOLUME = +l.PAD_TIER_VOLUME || 0;
        //   return (PAD_USD_PER_UNIT*PAD_TIER_VOLUME) || 0;
        // });
        // result /= this.CalSummaryVolume(e);
        var result = this.CalSummaryUSDMT(e);
        var PBD_ESTIMATED_FX = 0;
        try {
            PBD_ESTIMATED_FX = +this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX || 0;
        }
        catch (e) {
            console.log(e);
        }
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"]((result || 0) * PBD_ESTIMATED_FX);
    };
    CmlaBitumenComponent.prototype.CalSummaryBATHValue = function (e) {
        var PBD_ESTIMATED_FX = 0;
        try {
            PBD_ESTIMATED_FX = +this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX || 0;
        }
        catch (e) {
            console.log(e);
        }
        var result = __WEBPACK_IMPORTED_MODULE_4_lodash__["sumBy"](e, function (l) {
            var PAD_USD_PER_UNIT = +l.PAD_USD_PER_UNIT || 0;
            var PAD_TIER_VOLUME = +l.PAD_TIER_VOLUME || 0;
            return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](PAD_USD_PER_UNIT * PBD_ESTIMATED_FX) * PAD_TIER_VOLUME || 0;
        });
        // console.log(result);
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](result) || 0;
    };
    CmlaBitumenComponent.prototype.CalSummaryComparingToArgus = function (e) {
        var result = 0;
        var PBD_ARGUS_PRICE = 0;
        result = this.CalSummaryUSDMT(e);
        try {
            PBD_ARGUS_PRICE = +this.model.PAF_BTM_DETAIL.PBD_ARGUS_PRICE || 0;
            result -= PBD_ARGUS_PRICE;
        }
        catch (e) {
            console.log(e);
        }
        var signed = result < 0 ? '-' : '+';
        result = __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](result);
        var result_str = this.decimalPipe.transform(Math.abs(result), '1.0');
        return "Argus " + signed + " " + result_str;
    };
    CmlaBitumenComponent.prototype.ShowProduct = function (id) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].getTextByID(id, this.master.PAF_CMLA_BITUMEN);
    };
    CmlaBitumenComponent.prototype.loadFX = function () {
        var _this = this;
        this.masterService.getFX(this.model.PAF_BTM_DETAIL).subscribe(function (res) {
            console.log(res);
            _this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX = res && res.FX;
        });
    };
    CmlaBitumenComponent.prototype.loadHSFO = function () {
        var _this = this;
        this.masterService.getHSFO(this.model.PAF_BTM_DETAIL).subscribe(function (res) {
            console.log(res);
            _this.model.PAF_BTM_DETAIL.PBD_AVG_HSFO = res && res.HSFO;
        });
    };
    CmlaBitumenComponent.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"](_this.master, res);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.CUSTOMERS_BITUMEN);
            _this.master.PRODUCTS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.PRODUCTS_CMLA_IMPORT);
            _this.master.QUANTITY_UNITS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.QUANTITY_UNITS_CMLA);
            _this.master.CURRENCY_PER_VOLUMN_UNITS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.CURRENCY_PER_VOLUMN_UNITS_CMLA);
            __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].master = _this.master;
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.updateBtmGrade();
                    _this.loadFX();
                    _this.loadHSFO();
                    // this.model.PAF_PAYMENT_ITEMS = _.map(this.model.PAF_PAYMENT_ITEMS, (e) => {
                    //   for (var i = 0; i < 5; ++i) {
                    //     try
                    //     {
                    //       e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                    //     }
                    //     catch (e)
                    //     {
                    //       e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                    //     }
                    //   }
                    //   return e;
                    // });
                    // this.master.CUSTOMERS = PAFStore.GetCustomers(this.model.PDA_FOR_COMPANY);
                    // this.model.IS_DISABLED = PAFStore.CheckRealyDisabled(this.model.IS_DISABLED, this.model.Buttons, this.model.PDA_STATUS);
                    // this.model.IS_DISABLED_NOTE = PAFStore.CheckRealyDisabled(this.model.IS_DISABLED_NOTE, this.model.Buttons, this.model.PDA_STATUS)
                    // const { PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS } = PAFStore.UpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS, this.criteria.PAF_BIDDING_ITEMS);
                    // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                    // this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                    // PAFStore.LoadInterModelToCiteria(this.model.PAF_BIDDING_ITEMS);
                    // this.criteria = PAFStore.LoadInterModelToCiteria(this.criteria, this.model.PAF_BIDDING_ITEMS)
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.loadFX();
                _this.loadHSFO();
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
                _this.loaderService.display(false);
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmlaBitumenComponent.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmlaBitumenComponent.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmlaBitumenComponent.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmlaBitumenComponent.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmlaBitumenComponent.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    CmlaBitumenComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        // model = PAFStore.NormalizeModelCMLAIMPORT(model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmla/bitumen/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaBitumenComponent.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].NormalizeModelCMLAIMPORT(model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        window.location.href = "../../web/MainBoards.aspx";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaBitumenComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaBitumenComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaBitumenComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_7__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmlaBitumenComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_7__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    CmlaBitumenComponent.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
            });
        });
    };
    CmlaBitumenComponent.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
        });
    };
    CmlaBitumenComponent.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmlaBitumenComponent.prototype.ShowTierName = function (tier_id) {
        var tier = __WEBPACK_IMPORTED_MODULE_4_lodash__["find"](this.master.PAF_MT_BTM_TIER, function (f) {
            return f.ID == tier_id;
        });
        return (tier && tier.DETAIL) || "";
    };
    CmlaBitumenComponent.prototype.log = function (e) {
        console.log(e);
    };
    CmlaBitumenComponent.prototype.GetIndexAllocationItems = function (index, e) {
        var result = __WEBPACK_IMPORTED_MODULE_4_lodash__["findIndex"](this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS, function (o) {
            return e == o;
        });
        return result;
    };
    CmlaBitumenComponent.prototype.GetSummaryList = function (index) {
        var _this = this;
        // return this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS;
        var result = [];
        // console.log(`aPAF_BTM_ALLOCATE_ITEMS`, this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS);
        var PAF_BTM_ALLOCATE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["filter"](this.model.PAF_BTM_GRADE_ITEMS[index].PAF_BTM_ALLOCATE_ITEMS, function (o) {
            return o.PAV_VOLUME && o.PAV_VOLUME != 0;
        });
        // PAF_BTM_ALLOCATE_ITEMS_DETAIL
        // console.log(`bPAF_BTM_ALLOCATE_ITEMS`, PAF_BTM_ALLOCATE_ITEMS);
        PAF_BTM_ALLOCATE_ITEMS = __WEBPACK_IMPORTED_MODULE_4_lodash__["groupBy"](__WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(PAF_BTM_ALLOCATE_ITEMS), function (e) {
            return e.PBA_FK_CUSTOMER;
        });
        result = __WEBPACK_IMPORTED_MODULE_4_lodash__["map"](PAF_BTM_ALLOCATE_ITEMS, function (e) {
            var elem = {};
            var rows = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]([], e);
            var PBA_FK_CUSTOMER = rows.length > 0 && rows[0] && rows[0].PBA_FK_CUSTOMER;
            var PAF_BTM_ALLOCATE_ITEMS_DETAIL = [];
            for (var i = 0; i < e.length; i++) {
                for (var j = 0; j < e[i].PAF_BTM_ALLOCATE_ITEMS_DETAIL.length; j++) {
                    PAF_BTM_ALLOCATE_ITEMS_DETAIL.push(e[i].PAF_BTM_ALLOCATE_ITEMS_DETAIL[j]);
                }
            }
            PAF_BTM_ALLOCATE_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_4_lodash__["map"](__WEBPACK_IMPORTED_MODULE_4_lodash__["groupBy"](PAF_BTM_ALLOCATE_ITEMS_DETAIL, function (f) {
                var tier = __WEBPACK_IMPORTED_MODULE_4_lodash__["find"](_this.master.PAF_MT_BTM_TIER, function (j) {
                    return j.ID == f.PAD_FK_PAF_MT_BTM_TIER;
                });
                // return f.PAD_FK_PAF_MT_BTM_TIER;
                return tier && tier.GROUP;
            }), function (e) {
                return e;
            });
            // console.log(PAF_BTM_ALLOCATE_ITEMS_DETAIL);
            elem.SHOW_CUSTOMER = _this.ShowCustomer(PBA_FK_CUSTOMER);
            elem.PAF_BTM_ALLOCATE_ITEMS_DETAIL = __WEBPACK_IMPORTED_MODULE_4_lodash__["map"](PAF_BTM_ALLOCATE_ITEMS_DETAIL, function (f) {
                f.TIER_NAME = _this.ShowTierName(f[0].PAD_FK_PAF_MT_BTM_TIER);
                f.SHOW_VOLUME = _this.CalSummaryVolume(f);
                f.SHOW_BATH_MT = _this.CalSummaryBATHMT(f);
                f.SHOW_USD_MT = _this.CalSummaryUSDMT(f);
                f.SHOW_ARGUS_DIFF = f.SHOW_USD_MT - (+_this.model.PAF_BTM_DETAIL.PBD_ARGUS_PRICE || 0);
                f.SHOW_HSFO_DIFF = f.SHOW_USD_MT - (+_this.model.PAF_BTM_DETAIL.PBD_AVG_HSFO || 0);
                f.SHOW_ARGUS_DIFF = __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](f.SHOW_ARGUS_DIFF);
                f.SHOW_HSFO_DIFF = __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](f.SHOW_HSFO_DIFF);
                return f;
            });
            return elem;
        });
        return result;
    };
    CmlaBitumenComponent.prototype.GetWeightedPriceTHB = function (index) {
        // let sum_bath = 0;
        // let sum_volume = 0;
        // let summary_list = this.GetSummaryList(index);
        // for (var i = 0; i < summary_list.length; ++i) {
        //   for (var j = 0; j < summary_list[i].PAF_BTM_ALLOCATE_ITEMS_DETAIL.length; ++j) {
        //     let e = summary_list[i].PAF_BTM_ALLOCATE_ITEMS_DETAIL[j];
        //     sum_bath += (_.round((+e.SHOW_VOLUME || 0)) * _.round((+e.SHOW_BATH_MT || 0)));
        //     sum_volume += _.round((+e.SHOW_VOLUME || 0));
        //     // console.log()
        //   }
        // }
        // return (sum_bath/sum_volume) || 0;
        var sum_bath = 0;
        var w_usd = this.GetWeightedPriceUSD(index);
        var PBD_ESTIMATED_FX = 0;
        try {
            PBD_ESTIMATED_FX = +this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX || 0;
        }
        catch (e) {
            console.log(e);
        }
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](w_usd * PBD_ESTIMATED_FX);
    };
    CmlaBitumenComponent.prototype.GetWeightedPriceUSD = function (index) {
        var sum_usd = 0;
        var sum_volume = 0;
        var summary_list = this.GetSummaryList(index);
        for (var i = 0; i < summary_list.length; ++i) {
            for (var j = 0; j < summary_list[i].PAF_BTM_ALLOCATE_ITEMS_DETAIL.length; ++j) {
                var e = summary_list[i].PAF_BTM_ALLOCATE_ITEMS_DETAIL[j];
                sum_usd += (__WEBPACK_IMPORTED_MODULE_4_lodash__["round"]((+e.SHOW_VOLUME || 0)) * __WEBPACK_IMPORTED_MODULE_4_lodash__["round"]((+e.SHOW_USD_MT || 0)));
                sum_volume += __WEBPACK_IMPORTED_MODULE_4_lodash__["round"]((+e.SHOW_VOLUME || 0));
                // console.log()
            }
        }
        // let bath = this.GetWeightedPriceTHB(index);
        // return _.round(bath*this.model.PAF_BTM_DETAIL.PBD_ESTIMATED_FX);
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](sum_usd / sum_volume) || 0;
    };
    CmlaBitumenComponent.prototype.GetPlanCompare = function (index) {
        var result = "";
        var PBD_SIM_PLAN = this.model.PAF_BTM_DETAIL.PBD_SIM_PLAN;
        var PBD_CROP_PLAN = this.model.PAF_BTM_DETAIL.PBD_CROP_PLAN;
        var usd = this.GetWeightedPriceUSD(index);
        if (usd > PBD_SIM_PLAN && usd > PBD_CROP_PLAN) {
            result = "> Simplan, Corp. Plan";
        }
        else if (usd < PBD_SIM_PLAN && usd < PBD_CROP_PLAN) {
            result = "< Simplan, Corp. Plan";
        }
        else if (usd > PBD_SIM_PLAN && usd < PBD_CROP_PLAN) {
            result = "> Simplan, < Corp. Plan";
        }
        else if (usd < PBD_SIM_PLAN && usd > PBD_CROP_PLAN) {
            result = "< Simplan, > Corp. Plan";
        }
        else if (usd < PBD_SIM_PLAN && usd == PBD_CROP_PLAN) {
            result = "< Simplan, = Corp. Plan";
        }
        else if (usd > PBD_SIM_PLAN && usd == PBD_CROP_PLAN) {
            result = "> Simplan, = Corp. Plan";
        }
        else if (usd == PBD_SIM_PLAN && usd > PBD_CROP_PLAN) {
            result = "= Simplan, > Corp. Plan";
        }
        else if (usd == PBD_SIM_PLAN && usd < PBD_CROP_PLAN) {
            result = "= Simplan, < Corp. Plan";
        }
        else if (usd == PBD_SIM_PLAN && usd == PBD_CROP_PLAN) {
            result = "= Simplan, Corp. Plan";
        }
        return result;
    };
    CmlaBitumenComponent.prototype.GetWeightedPriceDiffArgus = function (index) {
        var usd = this.GetWeightedPriceUSD(index);
        usd -= __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](+this.model.PAF_BTM_DETAIL.PBD_ARGUS_PRICE || 0);
        return usd;
    };
    CmlaBitumenComponent.prototype.GetWeightedPriceDiffHSFO = function (index) {
        var usd = this.GetWeightedPriceUSD(index);
        usd -= __WEBPACK_IMPORTED_MODULE_4_lodash__["round"](+this.model.PAF_BTM_DETAIL.PBD_AVG_HSFO || 0);
        return usd;
    };
    // public GetSummaryTierList(PAF_BTM_ALLOCATE_ITEM: any) : any {
    //   PAF_BTM_ALLOCATE_ITEM = _.groupBy(PAF_BTM_ALLOCATE_ITEM, (e) => {
    //     return e.PAF_BTM_ALLOCATE_ITEMS_DETAIL.PAD_FK_PAF_MT_BTM_TIER
    //     let tier = _.find(this.master.PAF_MT_BTM_TIER, (f) => {
    //       return f.ID == e.PAF_BTM_ALLOCATE_ITEMS_DETAIL.PAD_FK_PAF_MT_BTM_TIER;
    //     });
    //     return (tier && tier.DETAIL) || "";
    //   })
    //   PAD_FK_PAF_MT_BTM_TIER
    //   console.log(e)
    //   return [{}];
    // }
    CmlaBitumenComponent.prototype.OnChangeCompany = function (company) {
        console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].GetCustomers(company);
    };
    CmlaBitumenComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToReject(button_list);
    };
    CmlaBitumenComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    return CmlaBitumenComponent;
}());
CmlaBitumenComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmla-bitumen',
        template: __webpack_require__("../../../../../src/app/containers/cmla/cmla-bitumen.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["e" /* DomesticService */]) === "function" && _g || Object])
], CmlaBitumenComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=cmla-bitumen.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-form-1.component.html":
/***/ (function(module, exports) {

module.exports = "<app-shared-form\n  [title]=\"'Product Approval Form (CMLA - Base Oil & Aromatic)'\"\n  [model]=\"model\"\n  [master]=\"master\"\n  [criteria]=\"criteria\"\n  [is_cmps_dom]=\"false\"\n  [is_cmps_inter]=\"false\"\n  [is_cmla_form_1]=\"true\"\n  [is_cmla_form_2]=\"false\"\n  [is_cmla]=\"true\"\n  (OnChangeCompany)=\"OnChangeCompany($event)\"\n  (DoSaveDraft)=\"DoSaveDraft($event)\"\n  (DoSubmit)=\"DoSubmit($event)\"\n  (DoVerify)=\"DoVerify($event)\"\n  (DoEndorse)=\"DoEndorse($event)\"\n  (DoApproved)=\"DoApproved($event)\"\n  (DoGeneratePDF)=\"DoGeneratePDF($event)\"\n  (DoReject)=\"DoReject($event)\"\n  (DoCancel)=\"DoCancel($event)\"\n>\n</app-shared-form>\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-form-1.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaForm1Component; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CmlaForm1Component = (function () {
    function CmlaForm1Component(route, router, loaderService, masterService, pafService, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.new_model = {};
        this.criteria = {
            products: [{
                    name: "U95",
                    quantity_unit: "MT",
                    quantities: {
                        min: 0,
                        max: 0,
                    }
                }],
            fob: false,
            cfr: false,
            submited: false,
            TARGET_PRODUCT: 0,
            selected_customer: null,
            PAF_BIDDING_ITEMS: [{
                    SELECTED: false,
                    PAF_ROUND: [{}],
                    PRODUCTS: [{}],
                }],
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.model = {
            PDA_FORM_ID: 'Draft',
            PDA_TEMPLATE: 'CMLA_FORM_1',
            PDA_TYPE: 'Type of Import',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            // PDA_CREATED: "/Date(1488880800000)/",
            PDA_FOR_COMPANY: "",
            PDA_SUB_TYPE: "",
            PDA_INCLUDED_REVIEW_TABLE: 'N',
            PDA_INCLUDED_BIDDING_TABLE: 'N',
            PAF_REVIEW_ITEMS: [{
                    NEED_PRODUCT: true,
                    PRI_QUANTITY_UNIT: 'MT',
                    PAF_REVIEW_ITEMS_DETAIL: [{}],
                }],
            PAF_BIDDING_ITEMS: [{
                    PBI_QUANTITY_UNIT: 'MT',
                    PAF_ROUND: [{}],
                    PAF_BIDDING_ITEMS_BENCHMARK: [{}],
                }],
            PAF_PAYMENT_ITEMS: [],
            SHOW_PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PROPOSAL_ITEMS: [],
            PDA_PRICE_UNIT: 'USD/MT',
        };
        this.master = {};
    }
    CmlaForm1Component.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmlaForm1Component.prototype.ngAfterViewInit = function () {
    };
    CmlaForm1Component.prototype.ngOnDestroy = function () {
    };
    CmlaForm1Component.prototype.showProducts = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].showProducts(id);
        // let e = _.find(this.master.PRODUCTS, (e) => {
        //   return e.ID == id;
        // });
        // return e && e.TEXT;
    };
    CmlaForm1Component.prototype.showCustomer = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_3_lodash__["find"](this.master.CUSTOMERS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    CmlaForm1Component.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](_this.master, res);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            _this.master.REAL_CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            _this.master.PRODUCTS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.PRODUCTS_CMLA);
            _this.master.QUANTITY_UNITS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.QUANTITY_UNITS_CMLA);
            _this.master.CURRENCY_PER_VOLUMN_UNITS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CURRENCY_PER_VOLUMN_UNITS_CMLA);
            __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].master = _this.master;
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                        for (var i = 0; i < 5; ++i) {
                            try {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                            }
                            catch (e) {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                            }
                        }
                        return e;
                    });
                    _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(_this.model.PDA_FOR_COMPANY);
                    _this.model.IS_DISABLED = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED, _this.model.Buttons, _this.model.PDA_STATUS);
                    _this.model.IS_DISABLED_NOTE = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED_NOTE, _this.model.Buttons, _this.model.PDA_STATUS);
                    // const { PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS } = PAFStore.UpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS, this.criteria.PAF_BIDDING_ITEMS);
                    // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                    // this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                    // PAFStore.LoadInterModelToCiteria(this.model.PAF_BIDDING_ITEMS);
                    // this.criteria = PAFStore.LoadInterModelToCiteria(this.criteria, this.model.PAF_BIDDING_ITEMS)
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
                _this.loaderService.display(false);
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmlaForm1Component.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmlaForm1Component.prototype.RemoveBiddingItemsRequest = function (target) {
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["reject"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            return elem == target;
        });
    };
    // public AddCustomerRequestTier(): void {
    //   console.log(this.criteria.TARGET_PRODUCT);
    //   if (this.criteria.TARGET_PRODUCT !== null) {
    //     console.log(this.criteria.TARGET_PRODUCT)
    //     const index = this.criteria.TARGET_PRODUCT;
    //     let NEW_PAF_REVIEW_ITEMS = [];
    //     let PAF_REVIEW_ITEMS = this.model.PAF_REVIEW_ITEMS;
    //     // let PAF_REVIEW_ITEMS = Utility.clone(this.model.PAF_REVIEW_ITEMS);
    //     // if (index == PAF_REVIEW_ITEMS.length - 1) {
    //       this.model.PAF_REVIEW_ITEMS.splice(index+1, 0, {
    //         NEED_PRODUCT: false,
    //         PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //         PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //         PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //       });
    //       this.model.PAF_REVIEW_ITEMS_DETAIL.push(index, 0, {})
    //     // }
    //     //  else {
    //     //   let target_index = index;
    //     //   for (var i = index + 1; i < PAF_REVIEW_ITEMS.length; i++) {
    //     //     target_index = i + 1;
    //     //     if (PAF_REVIEW_ITEMS[i].NEED_PRODUCT) {
    //     //       target_index = i;
    //     //       break;
    //     //     }
    //     //   }
    //     //   this.model.PAF_REVIEW_ITEMS.push({
    //     //     NEED_PRODUCT: false,
    //     //     PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //     //     PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //     //     PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //     //   });
    //     //   this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
    //     // }
    //   }
    // }
    CmlaForm1Component.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmlaForm1Component.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmlaForm1Component.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmlaForm1Component.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    CmlaForm1Component.prototype.DoSaveDraft = function () {
        var context = this;
        // const model = Utility.NormalizeModel(context.model, context.master);
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmla/form-1/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaForm1Component.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        window.location.href = "../../web/MainBoards.aspx";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaForm1Component.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaForm1Component.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaForm1Component.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmlaForm1Component.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    CmlaForm1Component.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Approve(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Approved",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // swal({title:"Approved!", type: "success"}, () => {
        //       //   context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmlaForm1Component.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            // swal({
            //   title: "Saved",
            //   text: "",
            //   type: "success"
            // }, () => {
            //   swal({title:"Saved!", type: "success"}, () => {
            //     // context.router.navigate([`/cmla/search`])
            //   });
            // })
        });
    };
    CmlaForm1Component.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmlaForm1Component.prototype.log = function (e) {
        console.log(e);
    };
    CmlaForm1Component.prototype.playground = function () {
        var _this = this;
        this.pafService.playgroundGet().subscribe(function (res) {
            var model = res;
            _this.new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
            _this.new_criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadInterModelToCiteria(_this.criteria, model.PAF_BIDDING_ITEMS);
        });
        // const criteria = {
        //   "products": [
        //     {
        //       "name": "U95",
        //       "quantity_unit": "MT",
        //       "quantities": {
        //         "min": 0,
        //         "max": 0
        //       },
        //       "PBI_FK_MATERIAL": "YUG95",
        //       "PBI_QUANTITY_UNIT": "KBBL"
        //     }
        //   ],
        //   "fob": false,
        //   "cfr": false,
        //   "submited": false,
        //   "TARGET_PRODUCT": 0,
        //   "selected_customer": null,
        //   "PAF_BIDDING_ITEMS": [
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "1"
        //             },
        //             {}
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "1",
        //           "PRD_EXISTING_QUANTITY_MAX": "2",
        //           "SHOW_CONVERT_TO_FOB": "6",
        //           "PBI_MARKET_PRICE": "6",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "6",
        //           "PBI_BENCHMARK_PRICE": "78"
        //         },
        //         {
        //           "PBI_FK_MATERIAL": "YUG95"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "4",
        //       "PBI_INCOTERM": "DEG",
        //       "PBI_COUNTRY": "10000174",
        //       "PBI_FREIGHT_PRICE": "6",
        //       "PBI_NOTE": "6"
        //     },
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "8"
        //             }
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "7",
        //           "PRD_EXISTING_QUANTITY_MAX": "8",
        //           "SHOW_CONVERT_TO_FOB": "8",
        //           "PBI_MARKET_PRICE": "8",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "8",
        //           "PBI_BENCHMARK_PRICE": "77"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "8",
        //       "PBI_INCOTERM": "DDU",
        //       "PBI_COUNTRY": "10000078",
        //       "PBI_FREIGHT_PRICE": "8",
        //       "PBI_NOTE": "8"
        //     }
        //   ]
        // };
        // let new_model = Utility.clone(this.model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(criteria);
        // this.new_model = new_model;
        // console.log(new_model);
        // return new_model;
    };
    CmlaForm1Component.prototype.OnChangeCompany = function (company) {
        console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(company);
    };
    return CmlaForm1Component;
}());
CmlaForm1Component = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmla-form-1',
        template: __webpack_require__("../../../../../src/app/containers/cmla/cmla-form-1.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */]) === "function" && _f || Object])
], CmlaForm1Component);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=cmla-form-1.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-form-2.component.html":
/***/ (function(module, exports) {

module.exports = "<app-shared-form\n  [title]=\"'Product Approval Form (CMLA - Slack Wax)'\"\n  [model]=\"model\"\n  [master]=\"master\"\n  [criteria]=\"criteria\"\n  [is_cmps_dom]=\"false\"\n  [is_cmps_inter]=\"false\"\n  [is_cmla_form_1]=\"false\"\n  [is_cmla_form_2]=\"true\"\n  [is_cmla]=\"true\"\n  (OnChangeCompany)=\"OnChangeCompany($event)\"\n  (DoSaveDraft)=\"DoSaveDraft($event)\"\n  (DoSubmit)=\"DoSubmit($event)\"\n  (DoVerify)=\"DoVerify($event)\"\n  (DoEndorse)=\"DoEndorse($event)\"\n  (DoApproved)=\"DoApproved($event)\"\n  (DoGeneratePDF)=\"DoGeneratePDF($event)\"\n  (DoReject)=\"DoReject($event)\"\n  (DoCancel)=\"DoCancel($event)\"\n>\n</app-shared-form>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-form-2.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaForm2Component; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CmlaForm2Component = (function () {
    function CmlaForm2Component(route, router, loaderService, masterService, pafService, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.new_model = {};
        this.criteria = {
            products: [{
                    name: "U95",
                    quantity_unit: "MT",
                    quantities: {
                        min: 0,
                        max: 0,
                    }
                }],
            fob: false,
            cfr: false,
            submited: false,
            TARGET_PRODUCT: 0,
            selected_customer: null,
            PAF_BIDDING_ITEMS: [{
                    SELECTED: false,
                    PAF_ROUND: [{}],
                    PRODUCTS: [{}],
                }],
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.model = {
            PDA_FORM_ID: 'Draft',
            PDA_TEMPLATE: 'CMLA_FORM_2',
            PDA_TYPE: 'Type of Import',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            // PDA_CREATED: "/Date(1488880800000)/",
            PDA_FOR_COMPANY: "",
            PDA_SUB_TYPE: "",
            PDA_INCLUDED_REVIEW_TABLE: 'N',
            PDA_INCLUDED_BIDDING_TABLE: 'N',
            PAF_REVIEW_ITEMS: [{
                    NEED_PRODUCT: true,
                    PRI_QUANTITY_UNIT: 'MT',
                    PAF_REVIEW_ITEMS_DETAIL: [{}],
                }],
            PAF_BIDDING_ITEMS: [{
                    PBI_QUANTITY_UNIT: 'MT',
                    PAF_ROUND: [{}],
                    PAF_BIDDING_ITEMS_BENCHMARK: [{
                            PBB_NAME: ''
                        }],
                }],
            PAF_PAYMENT_ITEMS: [],
            SHOW_PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PROPOSAL_ITEMS: [],
            PDA_PRICE_UNIT: 'USD/MT',
        };
        this.master = {};
    }
    CmlaForm2Component.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmlaForm2Component.prototype.ngAfterViewInit = function () {
    };
    CmlaForm2Component.prototype.ngOnDestroy = function () {
    };
    CmlaForm2Component.prototype.showProducts = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].showProducts(id);
        // let e = _.find(this.master.PRODUCTS, (e) => {
        //   return e.ID == id;
        // });
        // return e && e.TEXT;
    };
    CmlaForm2Component.prototype.showCustomer = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_3_lodash__["find"](this.master.CUSTOMERS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    CmlaForm2Component.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](_this.master, res);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            _this.master.REAL_CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            _this.master.PRODUCTS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.PRODUCTS_CMLA_FORM_2);
            _this.master.QUANTITY_UNITS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.QUANTITY_UNITS_CMLA);
            _this.master.CURRENCY_PER_VOLUMN_UNITS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CURRENCY_PER_VOLUMN_UNITS_CMLA);
            __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].master = _this.master;
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                        for (var i = 0; i < 5; ++i) {
                            try {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                            }
                            catch (e) {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                            }
                        }
                        return e;
                    });
                    _this.master.CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(_this.model.PDA_FOR_COMPANY);
                    _this.model.IS_DISABLED = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED, _this.model.Buttons, _this.model.PDA_STATUS);
                    _this.model.IS_DISABLED_NOTE = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED_NOTE, _this.model.Buttons, _this.model.PDA_STATUS);
                    // const { PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS } = PAFStore.UpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS, this.criteria.PAF_BIDDING_ITEMS);
                    // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                    // this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                    // PAFStore.LoadInterModelToCiteria(this.model.PAF_BIDDING_ITEMS);
                    // this.criteria = PAFStore.LoadInterModelToCiteria(this.criteria, this.model.PAF_BIDDING_ITEMS)
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
                _this.loaderService.display(false);
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmlaForm2Component.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmlaForm2Component.prototype.RemoveBiddingItemsRequest = function (target) {
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["reject"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            return elem == target;
        });
    };
    // public AddCustomerRequestTier(): void {
    //   console.log(this.criteria.TARGET_PRODUCT);
    //   if (this.criteria.TARGET_PRODUCT !== null) {
    //     console.log(this.criteria.TARGET_PRODUCT)
    //     const index = this.criteria.TARGET_PRODUCT;
    //     let NEW_PAF_REVIEW_ITEMS = [];
    //     let PAF_REVIEW_ITEMS = this.model.PAF_REVIEW_ITEMS;
    //     // let PAF_REVIEW_ITEMS = Utility.clone(this.model.PAF_REVIEW_ITEMS);
    //     // if (index == PAF_REVIEW_ITEMS.length - 1) {
    //       this.model.PAF_REVIEW_ITEMS.splice(index+1, 0, {
    //         NEED_PRODUCT: false,
    //         PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //         PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //         PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //       });
    //       this.model.PAF_REVIEW_ITEMS_DETAIL.push(index, 0, {})
    //     // }
    //     //  else {
    //     //   let target_index = index;
    //     //   for (var i = index + 1; i < PAF_REVIEW_ITEMS.length; i++) {
    //     //     target_index = i + 1;
    //     //     if (PAF_REVIEW_ITEMS[i].NEED_PRODUCT) {
    //     //       target_index = i;
    //     //       break;
    //     //     }
    //     //   }
    //     //   this.model.PAF_REVIEW_ITEMS.push({
    //     //     NEED_PRODUCT: false,
    //     //     PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //     //     PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //     //     PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //     //   });
    //     //   this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
    //     // }
    //   }
    // }
    CmlaForm2Component.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmlaForm2Component.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmlaForm2Component.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmlaForm2Component.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    CmlaForm2Component.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmla/form-2/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaForm2Component.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        window.location.href = "../../web/MainBoards.aspx";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaForm2Component.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaForm2Component.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaForm2Component.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmlaForm2Component.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    CmlaForm2Component.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Approve(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Approved",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // swal({title:"Approved!", type: "success"}, () => {
        //       //   context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmlaForm2Component.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            // window.open(`http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/${res}`, '_blank');
            // swal({
            //   title: "Saved",
            //   text: "",
            //   type: "success"
            // }, () => {
            //   swal({title:"Saved!", type: "success"}, () => {
            //     // context.router.navigate([`/cmla/search`])
            //   });
            // })
        });
    };
    CmlaForm2Component.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmlaForm2Component.prototype.log = function (e) {
        console.log(e);
    };
    CmlaForm2Component.prototype.playground = function () {
        var _this = this;
        this.pafService.playgroundGet().subscribe(function (res) {
            var model = res;
            _this.new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
            _this.new_criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadInterModelToCiteria(_this.criteria, model.PAF_BIDDING_ITEMS);
        });
        // const criteria = {
        //   "products": [
        //     {
        //       "name": "U95",
        //       "quantity_unit": "MT",
        //       "quantities": {
        //         "min": 0,
        //         "max": 0
        //       },
        //       "PBI_FK_MATERIAL": "YUG95",
        //       "PBI_QUANTITY_UNIT": "KBBL"
        //     }
        //   ],
        //   "fob": false,
        //   "cfr": false,
        //   "submited": false,
        //   "TARGET_PRODUCT": 0,
        //   "selected_customer": null,
        //   "PAF_BIDDING_ITEMS": [
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "1"
        //             },
        //             {}
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "1",
        //           "PRD_EXISTING_QUANTITY_MAX": "2",
        //           "SHOW_CONVERT_TO_FOB": "6",
        //           "PBI_MARKET_PRICE": "6",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "6",
        //           "PBI_BENCHMARK_PRICE": "78"
        //         },
        //         {
        //           "PBI_FK_MATERIAL": "YUG95"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "4",
        //       "PBI_INCOTERM": "DEG",
        //       "PBI_COUNTRY": "10000174",
        //       "PBI_FREIGHT_PRICE": "6",
        //       "PBI_NOTE": "6"
        //     },
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "8"
        //             }
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "7",
        //           "PRD_EXISTING_QUANTITY_MAX": "8",
        //           "SHOW_CONVERT_TO_FOB": "8",
        //           "PBI_MARKET_PRICE": "8",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "8",
        //           "PBI_BENCHMARK_PRICE": "77"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "8",
        //       "PBI_INCOTERM": "DDU",
        //       "PBI_COUNTRY": "10000078",
        //       "PBI_FREIGHT_PRICE": "8",
        //       "PBI_NOTE": "8"
        //     }
        //   ]
        // };
        // let new_model = Utility.clone(this.model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(criteria);
        // this.new_model = new_model;
        // console.log(new_model);
        // return new_model;
    };
    CmlaForm2Component.prototype.OnChangeCompany = function (company) {
        // console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(company);
    };
    return CmlaForm2Component;
}());
CmlaForm2Component = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmla-form-2',
        template: __webpack_require__("../../../../../src/app/containers/cmla/cmla-form-2.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */]) === "function" && _f || Object])
], CmlaForm2Component);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=cmla-form-2.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-import.component.html":
/***/ (function(module, exports) {

module.exports = "<app-shared-form\n  [title]=\"'Product Approval Form (CMLA-Import)'\"\n  [model]=\"model\"\n  [master]=\"master\"\n  [criteria]=\"criteria\"\n  [is_cmps_dom]=\"false\"\n  [is_cmps_inter]=\"false\"\n  [is_cmla_form_1]=\"false\"\n  [is_cmla_form_2]=\"false\"\n  [is_cmla_import]=\"true\"\n  [is_cmla]=\"true\"\n  (OnChangeCompany)=\"OnChangeCompany($event)\"\n  (DoSaveDraft)=\"DoSaveDraft($event)\"\n  (DoSubmit)=\"DoSubmit($event)\"\n  (DoVerify)=\"DoVerify($event)\"\n  (DoEndorse)=\"DoEndorse($event)\"\n  (DoApproved)=\"DoApproved($event)\"\n  (DoGeneratePDF)=\"DoGeneratePDF($event)\"\n  (DoReject)=\"DoReject($event)\"\n  (DoCancel)=\"DoCancel($event)\"\n>\n</app-shared-form>\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmla/cmla-import.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaImportComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CmlaImportComponent = (function () {
    function CmlaImportComponent(route, router, loaderService, masterService, pafService, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.new_model = {};
        this.criteria = {
            products: [{
                    name: "U95",
                    quantity_unit: "MT",
                    quantities: {
                        min: 0,
                        max: 0,
                    }
                }],
            fob: false,
            cfr: false,
            submited: false,
            TARGET_PRODUCT: 0,
            selected_customer: null,
            PAF_BIDDING_ITEMS: [{
                    SELECTED: false,
                    PAF_ROUND: [{}],
                    PRODUCTS: [{}],
                }],
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.model = {
            PDA_FORM_ID: 'Draft',
            PDA_TEMPLATE: 'CMLA_IMPORT',
            PDA_TYPE: 'Type of Purchasing',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            // PDA_CREATED: "/Date(1488880800000)/",
            PDA_FOR_COMPANY: "",
            PDA_SUB_TYPE: "",
            PDA_INCLUDED_REVIEW_TABLE: 'Y',
            PDA_INCLUDED_BIDDING_TABLE: 'Y',
            PAF_REVIEW_ITEMS: [{
                    NEED_PRODUCT: true,
                    PRI_QUANTITY_UNIT: 'MT',
                    PAF_REVIEW_ITEMS_DETAIL: [{}],
                }],
            PAF_BIDDING_ITEMS: [{
                    PBI_QUANTITY_UNIT: 'MT',
                    PAF_ROUND: [{}],
                    PAF_BIDDING_ITEMS_BENCHMARK: [{
                            PBB_NAME: 'Others'
                        }],
                }],
            PAF_PAYMENT_ITEMS: [],
            SHOW_PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PROPOSAL_ITEMS: [],
            PDA_PRICE_UNIT: 'USD/MT',
        };
        this.master = {};
    }
    CmlaImportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmlaImportComponent.prototype.ngAfterViewInit = function () {
    };
    CmlaImportComponent.prototype.ngOnDestroy = function () {
    };
    CmlaImportComponent.prototype.showProducts = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].showProducts(id);
        // let e = _.find(this.master.PRODUCTS, (e) => {
        //   return e.ID == id;
        // });
        // return e && e.TEXT;
    };
    CmlaImportComponent.prototype.showCustomer = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_3_lodash__["find"](this.master.CUSTOMERS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    CmlaImportComponent.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](_this.master, res);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.VENDOR);
            _this.master.REAL_CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.VENDOR);
            _this.master.PRODUCTS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.PRODUCTS_CMLA_IMPORT);
            _this.master.QUANTITY_UNITS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.QUANTITY_UNITS_CMLA);
            _this.master.CURRENCY_PER_VOLUMN_UNITS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CURRENCY_PER_VOLUMN_UNITS_CMLA);
            __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].master = _this.master;
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                        for (var i = 0; i < 5; ++i) {
                            try {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                            }
                            catch (e) {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                            }
                        }
                        return e;
                    });
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadModelCMLA(_this.model);
                    _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(_this.model.PDA_FOR_COMPANY);
                    _this.model.IS_DISABLED = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED, _this.model.Buttons, _this.model.PDA_STATUS);
                    _this.model.IS_DISABLED_NOTE = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED_NOTE, _this.model.Buttons, _this.model.PDA_STATUS);
                    // const { PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS } = PAFStore.UpdateInterTable(this.criteria, this.model.PAF_BIDDING_ITEMS, this.criteria.PAF_BIDDING_ITEMS);
                    // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                    // this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                    // PAFStore.LoadInterModelToCiteria(this.model.PAF_BIDDING_ITEMS);
                    // this.criteria = PAFStore.LoadInterModelToCiteria(this.criteria, this.model.PAF_BIDDING_ITEMS)
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
                _this.loaderService.display(false);
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmlaImportComponent.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmlaImportComponent.prototype.RemoveBiddingItemsRequest = function (target) {
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["reject"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            return elem == target;
        });
    };
    // public AddCustomerRequestTier(): void {
    //   console.log(this.criteria.TARGET_PRODUCT);
    //   if (this.criteria.TARGET_PRODUCT !== null) {
    //     console.log(this.criteria.TARGET_PRODUCT)
    //     const index = this.criteria.TARGET_PRODUCT;
    //     let NEW_PAF_REVIEW_ITEMS = [];
    //     let PAF_REVIEW_ITEMS = this.model.PAF_REVIEW_ITEMS;
    //     // let PAF_REVIEW_ITEMS = Utility.clone(this.model.PAF_REVIEW_ITEMS);
    //     // if (index == PAF_REVIEW_ITEMS.length - 1) {
    //       this.model.PAF_REVIEW_ITEMS.splice(index+1, 0, {
    //         NEED_PRODUCT: false,
    //         PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //         PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //         PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //       });
    //       this.model.PAF_REVIEW_ITEMS_DETAIL.push(index, 0, {})
    //     // }
    //     //  else {
    //     //   let target_index = index;
    //     //   for (var i = index + 1; i < PAF_REVIEW_ITEMS.length; i++) {
    //     //     target_index = i + 1;
    //     //     if (PAF_REVIEW_ITEMS[i].NEED_PRODUCT) {
    //     //       target_index = i;
    //     //       break;
    //     //     }
    //     //   }
    //     //   this.model.PAF_REVIEW_ITEMS.push({
    //     //     NEED_PRODUCT: false,
    //     //     PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //     //     PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //     //     PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //     //   });
    //     //   this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
    //     // }
    //   }
    // }
    CmlaImportComponent.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmlaImportComponent.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmlaImportComponent.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmlaImportComponent.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    CmlaImportComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].NormalizeModelCMLAIMPORT(model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmla/import/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaImportComponent.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].NormalizeModelCMLAIMPORT(model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        window.location.href = "../../web/MainBoards.aspx";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmlaImportComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaImportComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmlaImportComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    //Do nothing (cancel button pressed)
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    // $("#hdfNoteAction").val(result);
                    // $('form').attr('target', '_self');
                    // $("#btnMANUAL").click();
                    model.PDA_REASON = result;
                    _this.pafService.Reject(model).subscribe(function (res) {
                        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                            title: "Rejected",
                            text: "",
                            type: "success"
                        }, function () {
                            window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                        });
                    }, function (err) {
                        __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmlaImportComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    //Do nothing (cancel button pressed)
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    model.PDA_REASON = result;
                    _this.pafService.Cancel(model).subscribe(function (res) {
                        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                            title: "Cancelled",
                            text: "",
                            type: "success"
                        }, function () {
                            window.location.href = "../../web/MainBoards.aspx";
                            // window.location.href = `${BASE_API}/../../`;
                        });
                    }, function (err) {
                        __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                    });
                    // LoadingProcess();
                }
            }
        });
        // const context = this;
        // const model = Utility.clone(this.model);
        // this.pafService.Cancel(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Cancelled",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       // swal({title:"Cal!", type: "success"}, () => {
        //         window.location.href = `${BASE_API}/../../`;
        //         // context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmlaImportComponent.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Approve(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Approved",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // swal({title:"Approved!", type: "success"}, () => {
        //       //   context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmlaImportComponent.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            // window.open(`http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/${res}`, '_blank');
            window.open(__WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            // swal({
            //   title: "Saved",
            //   text: "",
            //   type: "success"
            // }, () => {
            //   swal({title:"Saved!", type: "success"}, () => {
            //     // context.router.navigate([`/cmla/search`])
            //   });
            // })
        });
    };
    CmlaImportComponent.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmlaImportComponent.prototype.log = function (e) {
        console.log(e);
    };
    CmlaImportComponent.prototype.playground = function () {
        var _this = this;
        this.pafService.playgroundGet().subscribe(function (res) {
            var model = res;
            _this.new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
            _this.new_criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadInterModelToCiteria(_this.criteria, model.PAF_BIDDING_ITEMS);
        });
        // const criteria = {
        //   "products": [
        //     {
        //       "name": "U95",
        //       "quantity_unit": "MT",
        //       "quantities": {
        //         "min": 0,
        //         "max": 0
        //       },
        //       "PBI_FK_MATERIAL": "YUG95",
        //       "PBI_QUANTITY_UNIT": "KBBL"
        //     }
        //   ],
        //   "fob": false,
        //   "cfr": false,
        //   "submited": false,
        //   "TARGET_PRODUCT": 0,
        //   "selected_customer": null,
        //   "PAF_BIDDING_ITEMS": [
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "1"
        //             },
        //             {}
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "1",
        //           "PRD_EXISTING_QUANTITY_MAX": "2",
        //           "SHOW_CONVERT_TO_FOB": "6",
        //           "PBI_MARKET_PRICE": "6",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "6",
        //           "PBI_BENCHMARK_PRICE": "78"
        //         },
        //         {
        //           "PBI_FK_MATERIAL": "YUG95"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "4",
        //       "PBI_INCOTERM": "DEG",
        //       "PBI_COUNTRY": "10000174",
        //       "PBI_FREIGHT_PRICE": "6",
        //       "PBI_NOTE": "6"
        //     },
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "8"
        //             }
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "7",
        //           "PRD_EXISTING_QUANTITY_MAX": "8",
        //           "SHOW_CONVERT_TO_FOB": "8",
        //           "PBI_MARKET_PRICE": "8",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "8",
        //           "PBI_BENCHMARK_PRICE": "77"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "8",
        //       "PBI_INCOTERM": "DDU",
        //       "PBI_COUNTRY": "10000078",
        //       "PBI_FREIGHT_PRICE": "8",
        //       "PBI_NOTE": "8"
        //     }
        //   ]
        // };
        // let new_model = Utility.clone(this.model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(criteria);
        // this.new_model = new_model;
        // console.log(new_model);
        // return new_model;
    };
    CmlaImportComponent.prototype.OnChangeCompany = function (company) {
        console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(company);
    };
    return CmlaImportComponent;
}());
CmlaImportComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmla-import',
        template: __webpack_require__("../../../../../src/app/containers/cmla/cmla-import.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */]) === "function" && _f || Object])
], CmlaImportComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=cmla-import.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-dom.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model | json}}</pre> -->\n<app-shared-form\n  [title]=\"'Product Approval Form (CMPS - Domestic Sales)'\"\n  [model]=\"model\"\n  [master]=\"master\"\n  [criteria]=\"criteria\"\n  [is_cmps_dom]=\"true\"\n  [is_cmps_inter]=\"false\"\n  (OnChangeCompany)=\"OnChangeCompany($event)\"\n  (DoSaveDraft)=\"DoSaveDraft($event)\"\n  (DoSubmit)=\"DoSubmit($event)\"\n  (DoVerify)=\"DoVerify($event)\"\n  (DoEndorse)=\"DoEndorse($event)\"\n  (DoApproved)=\"DoApproved($event)\"\n  (DoGeneratePDF)=\"DoGeneratePDF($event)\"\n  (DoReject)=\"DoReject($event)\"\n  (DoCancel)=\"DoCancel($event)\"\n>\n</app-shared-form>\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-dom.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmpsDomComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CmpsDomComponent = (function () {
    function CmpsDomComponent(route, router, loaderService, masterService, pafService, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.criteria = {
            products: [{
                    name: "U95",
                    quantity_unit: "MT",
                    quantities: {
                        min: 0,
                        max: 0,
                    }
                }],
            fob: false,
            cfr: false,
            submited: false,
            TARGET_PRODUCT: 0,
            selected_customer: null,
        };
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.model = {
            PDA_TEMPLATE: 'CMPS_DOM_SALE',
            PDA_FORM_ID: 'Draft',
            PDA_TYPE: 'Type of Sale',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            // PDA_CREATED: "/Date(1488880800000)/",
            PDA_FOR_COMPANY: "",
            PDA_SUB_TYPE: "",
            PDA_INCLUDED_REVIEW_TABLE: 'N',
            PDA_INCLUDED_BIDDING_TABLE: 'N',
            IS_DISABLED: false,
            IS_DISABLED_NOTE: false,
            PAF_REVIEW_ITEMS: [{
                    SELECTED: false,
                    NEED_PRODUCT: true,
                    PAF_REVIEW_ITEMS_DETAIL: [{}],
                }],
            PAF_BIDDING_ITEMS: [{}],
            PDA_PRICE_UNIT: "USD/BBL",
            PAF_PAYMENT_ITEMS: [],
            PAF_PROPOSAL_ITEMS: [{
                    PSI_DETAIL: '',
                }],
        };
        this.master = {};
    }
    CmpsDomComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmpsDomComponent.prototype.ngAfterViewInit = function () {
    };
    CmpsDomComponent.prototype.ngOnDestroy = function () {
    };
    CmpsDomComponent.prototype.showProducts = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].showProducts(id);
        // let e = _.find(this.master.PRODUCTS, (e) => {
        //   return e.ID == id;
        // });
        // return e && e.TEXT;
    };
    CmpsDomComponent.prototype.showCustomer = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_3_lodash__["find"](this.master.CUSTOMERS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    CmpsDomComponent.prototype.load = function () {
        var _this = this;
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](_this.master, res);
            _this.master.REAL_CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS_DOM);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS_DOM);
            __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].master = _this.master;
            // this.master.CUSTOMERS_INTER = PAFStore.GetCustomers(this.master.CUSTOMERS_INTER);
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(_this.model.PDA_FOR_COMPANY);
                    _this.model.IS_DISABLED = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED, _this.model.Buttons, _this.model.PDA_STATUS);
                    _this.model.IS_DISABLED_NOTE = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED_NOTE, _this.model.Buttons, _this.model.PDA_STATUS);
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
                _this.loaderService.display(false);
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmpsDomComponent.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmpsDomComponent.prototype.OnChangeCompany = function (company) {
        console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(company);
    };
    // public RemoveBiddingItemsRequest(target) : void {
    //   this.model.PAF_BIDDING_ITEMS = _.reject(this.model.PAF_BIDDING_ITEMS, (elem) => {
    //     return elem == target;
    //   });
    // }
    // public AddCustomerRequestTier(): void {
    //   console.log(this.criteria.TARGET_PRODUCT);
    //   if (this.criteria.TARGET_PRODUCT !== null) {
    //     console.log(this.criteria.TARGET_PRODUCT)
    //     const index = this.criteria.TARGET_PRODUCT;
    //     let NEW_PAF_REVIEW_ITEMS = [];
    //     let PAF_REVIEW_ITEMS = this.model.PAF_REVIEW_ITEMS;
    //     // let PAF_REVIEW_ITEMS = Utility.clone(this.model.PAF_REVIEW_ITEMS);
    //     // if (index == PAF_REVIEW_ITEMS.length - 1) {
    //       this.model.PAF_REVIEW_ITEMS.splice(index+1, 0, {
    //         NEED_PRODUCT: false,
    //         PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //         PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //         PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //       });
    //       this.model.PAF_REVIEW_ITEMS_DETAIL.push(index, 0, {})
    //     // }
    //     //  else {
    //     //   let target_index = index;
    //     //   for (var i = index + 1; i < PAF_REVIEW_ITEMS.length; i++) {
    //     //     target_index = i + 1;
    //     //     if (PAF_REVIEW_ITEMS[i].NEED_PRODUCT) {
    //     //       target_index = i;
    //     //       break;
    //     //     }
    //     //   }
    //     //   this.model.PAF_REVIEW_ITEMS.push({
    //     //     NEED_PRODUCT: false,
    //     //     PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //     //     PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //     //     PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //     //   });
    //     //   this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
    //     // }
    //   }
    // }
    CmpsDomComponent.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmpsDomComponent.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmpsDomComponent.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmpsDomComponent.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    CmpsDomComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        model.PBI_QUANTITY_MAX = model.PBI_QUANTITY_MIN;
        // console.log(model)
        // this.pafService.SaveDraft(model).subscribe(
        //   (res) => {
        //       swal({title:"Saved!", type: "success"}, () => {
        //         context.router.navigate([`/cmps/dom/${res.req_transaction_id}/${res.transaction_id}/edit`])
        //       });
        //   }
        // )
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmps/dom/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsDomComponent.prototype.DoSubmit = function () {
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        var context = this;
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        // context.router.navigate(['/cmps/search'])
                        window.location.href = "../../web/MainBoards.aspx";
                        // window.location.href = `${BASE_API}/../../`;
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsDomComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsDomComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsDomComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    //Do nothing (cancel button pressed)
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    // $("#hdfNoteAction").val(result);
                    // $('form').attr('target', '_self');
                    // $("#btnMANUAL").click();
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmpsDomComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    //Do nothing (cancel button pressed)
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                                // window.location.href = `${BASE_API}/../../`;
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
        // const context = this;
        // const model = Utility.clone(this.model);
        // this.pafService.Cancel(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Cancelled",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       // swal({title:"Cal!", type: "success"}, () => {
        //         window.location.href = `${BASE_API}/../../`;
        //         // context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmpsDomComponent.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Approve(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Approved",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // swal({title:"Approved!", type: "success"}, () => {
        //       //   context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmpsDomComponent.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            // http://tsr-dcpai-app.thaioil.localnet/PAF/CPAIMVC/ApprovalForm/#/cmps/inter/b268708d2cac492d869c6ef7eb33c28c/201708100950250256305/edit
            // http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/PAF201708070940072521.pdf
            // swal({
            //   title: "Saved",
            //   text: "",
            //   type: "success"
            // }, () => {
            //   swal({title:"Saved!", type: "success"}, () => {
            //     context.router.navigate([`/cmps/search`])
            //   });
            // })
        });
    };
    CmpsDomComponent.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmpsDomComponent.prototype.log = function (e) {
        console.log(e);
    };
    return CmpsDomComponent;
}());
CmpsDomComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmps-dom',
        template: __webpack_require__("../../../../../src/app/containers/cmps/cmps-dom.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */]) === "function" && _f || Object])
], CmpsDomComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=cmps-dom.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-import.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre> {{ criteria | json }} </pre> -->\n<!-- <pre> {{ new_model.PAF_BIDDING_ITEMS | json }} </pre> -->\n<!-- <pre> {{ new_criteria | json }} </pre> -->\n<app-shared-form\n  [title]=\"'Product Approval Form (CMPS - International Import)'\"\n  [model]=\"model\"\n  [master]=\"master\"\n  [criteria]=\"criteria\"\n  [is_cmps_dom]=\"false\"\n  [is_cmps_inter]=\"true\"\n  [is_cmps_import]=\"true\"\n  (OnChangeCompany)=\"OnChangeCompany($event)\"\n  (DoSaveDraft)=\"DoSaveDraft($event)\"\n  (DoSubmit)=\"DoSubmit($event)\"\n  (DoVerify)=\"DoVerify($event)\"\n  (DoEndorse)=\"DoEndorse($event)\"\n  (DoApproved)=\"DoApproved($event)\"\n  (DoGeneratePDF)=\"DoGeneratePDF($event)\"\n  (DoReject)=\"DoReject($event)\"\n  (DoCancel)=\"DoCancel($event)\"\n>\n</app-shared-form>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-import.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmpsImportComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CmpsImportComponent = (function () {
    function CmpsImportComponent(route, router, loaderService, masterService, pafService, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.new_model = {};
        this.criteria = {
            products: [{
                    name: "U95",
                    quantity_unit: "MT",
                    quantities: {
                        min: 0,
                        max: 0,
                    }
                }],
            fob: false,
            cfr: false,
            submited: false,
            TARGET_PRODUCT: 0,
            selected_customer: null,
            PAF_BIDDING_ITEMS: [{
                    SELECTED: false,
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }],
                    PRODUCTS: [{}],
                }],
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.model = {
            PDA_TEMPLATE: 'CMPS_IMPORT',
            PDA_TYPE: 'Type of Sale',
            PDA_FORM_ID: 'Draft',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            // PDA_CREATED: "/Date(1488880800000)/",
            PDA_FOR_COMPANY: "",
            PDA_SUB_TYPE: "",
            PDA_INCLUDED_REVIEW_TABLE: 'N',
            PDA_INCLUDED_BIDDING_TABLE: 'N',
            IS_DISABLED: false,
            IS_DISABLED_NOTE: false,
            PAF_REVIEW_ITEMS: [{
                    NEED_PRODUCT: true,
                    PAF_REVIEW_ITEMS_DETAIL: [{}],
                }],
            PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PAYMENT_ITEMS: [],
            SHOW_PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PROPOSAL_ITEMS: [],
            PDA_PRICE_UNIT: 'USD/BBL',
        };
        this.master = {};
    }
    CmpsImportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmpsImportComponent.prototype.ngAfterViewInit = function () {
    };
    CmpsImportComponent.prototype.ngOnDestroy = function () {
    };
    CmpsImportComponent.prototype.showProducts = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].showProducts(id);
        // let e = _.find(this.master.PRODUCTS, (e) => {
        //   return e.ID == id;
        // });
        // return e && e.TEXT;
    };
    CmpsImportComponent.prototype.showCustomer = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_3_lodash__["find"](this.master.CUSTOMERS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    CmpsImportComponent.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](_this.master, res);
            // this.master.REAL_CUSTOMERS_INTER = Utility.clone(this.master.VENDOR);
            // this.master.CUSTOMERS_INTER = Utility.clone(this.master.CUSTOMERS);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.VENDOR);
            _this.master.REAL_CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].master = _this.master;
            // this.master.CUSTOMERS_INTER = PAFStore.GetCustomers(this.master.CUSTOMERS_INTER);
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"]({}, _this.model, res);
                    _this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                        // e.PPI_FK_VENDOR = Utility.clone(e.PPI_FK_CUSTOMER);
                        e.PPI_FK_CUSTOMER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(e.PPI_FK_VENDOR);
                        ;
                        return e;
                    });
                    _this.model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PROPOSAL_ITEMS, function (e) {
                        // e.PSI_FK_VENDOR = Utility.clone(e.PSI_FK_CUSTOMER);
                        e.PSI_FK_CUSTOMER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(e.PSI_FK_VENDOR);
                        ;
                        return e;
                    });
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                        for (var i = 0; i < 5; ++i) {
                            try {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                            }
                            catch (e) {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                            }
                        }
                        return e;
                    });
                    var _a = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateInterTable(_this.criteria, _this.model.PAF_BIDDING_ITEMS, _this.criteria.PAF_BIDDING_ITEMS), PAF_BIDDING_ITEMS = _a.PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS = _a.SHOW_PAF_BIDDING_ITEMS;
                    // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                    _this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                    // PAFStore.LoadInterModelToCiteria(this.model.PAF_BIDDING_ITEMS);
                    // this.criteria = PAFStore.LoadInterModelToCiteria(this.criteria, this.model.PAF_BIDDING_ITEMS)
                    _this.criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadImportModelToCiteria(_this.criteria, _this.model.PAF_BIDDING_ITEMS);
                    console.log(_this.criteria);
                    _this.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateSELECTED(_this.criteria.PAF_BIDDING_ITEMS, _this.model.PAF_PAYMENT_ITEMS);
                    _this.master.CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(_this.model.PDA_FOR_COMPANY);
                    _this.model.IS_DISABLED = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED, _this.model.Buttons, _this.model.PDA_STATUS);
                    _this.model.IS_DISABLED_NOTE = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED_NOTE, _this.model.Buttons, _this.model.PDA_STATUS);
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.loaderService.display(false);
                var _a = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateInterTable(_this.criteria, _this.model.PAF_BIDDING_ITEMS, _this.criteria.PAF_BIDDING_ITEMS), PAF_BIDDING_ITEMS = _a.PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS = _a.SHOW_PAF_BIDDING_ITEMS;
                _this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                _this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmpsImportComponent.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmpsImportComponent.prototype.RemoveBiddingItemsRequest = function (target) {
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["reject"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            return elem == target;
        });
    };
    // public AddCustomerRequestTier(): void {
    //   console.log(this.criteria.TARGET_PRODUCT);
    //   if (this.criteria.TARGET_PRODUCT !== null) {
    //     console.log(this.criteria.TARGET_PRODUCT)
    //     const index = this.criteria.TARGET_PRODUCT;
    //     let NEW_PAF_REVIEW_ITEMS = [];
    //     let PAF_REVIEW_ITEMS = this.model.PAF_REVIEW_ITEMS;
    //     // let PAF_REVIEW_ITEMS = Utility.clone(this.model.PAF_REVIEW_ITEMS);
    //     // if (index == PAF_REVIEW_ITEMS.length - 1) {
    //       this.model.PAF_REVIEW_ITEMS.splice(index+1, 0, {
    //         NEED_PRODUCT: false,
    //         PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //         PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //         PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //       });
    //       this.model.PAF_REVIEW_ITEMS_DETAIL.push(index, 0, {})
    //     // }
    //     //  else {
    //     //   let target_index = index;
    //     //   for (var i = index + 1; i < PAF_REVIEW_ITEMS.length; i++) {
    //     //     target_index = i + 1;
    //     //     if (PAF_REVIEW_ITEMS[i].NEED_PRODUCT) {
    //     //       target_index = i;
    //     //       break;
    //     //     }
    //     //   }
    //     //   this.model.PAF_REVIEW_ITEMS.push({
    //     //     NEED_PRODUCT: false,
    //     //     PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //     //     PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //     //     PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //     //   });
    //     //   this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
    //     // }
    //   }
    // }
    CmpsImportComponent.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmpsImportComponent.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmpsImportComponent.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmpsImportComponent.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    // public DoSaveDraft() : void {
    //   const context = this;
    //   const model = Utility.NormalizeModel(context.model, context.master);
    //   let new_model = Utility.clone(model);
    //   new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(this.criteria);
    //   console.log(new_model)
    //   this.pafService.SaveDraft(new_model).subscribe(
    //     (res) => {
    //       swal({title:"Saved!", type: "success"}, () => {
    //         context.router.navigate([`/cmps/inter/${res.req_transaction_id}/${res.transaction_id}/edit`])
    //       });
    //     }
    //   )
    // }
    CmpsImportComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        var new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(this.criteria);
        new_model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].AdjustImportModelBeforeSave(this.criteria);
        new_model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](new_model.PAF_PAYMENT_ITEMS, function (e) {
            e.PPI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(e.PPI_FK_CUSTOMER);
            e.PPI_FK_CUSTOMER = null;
            return e;
        });
        new_model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](new_model.PAF_PROPOSAL_ITEMS, function (e) {
            e.PSI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(e.PSI_FK_CUSTOMER);
            e.PSI_FK_CUSTOMER = null;
            return e;
        });
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(new_model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmps/import/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsImportComponent.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        var new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(this.criteria);
        new_model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].AdjustImportModelBeforeSave(this.criteria);
        new_model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](new_model.PAF_PAYMENT_ITEMS, function (e) {
            e.PPI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(e.PPI_FK_CUSTOMER);
            e.PPI_FK_CUSTOMER = null;
            return e;
        });
        new_model.PAF_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](new_model.PAF_PROPOSAL_ITEMS, function (e) {
            e.PSI_FK_VENDOR = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(e.PSI_FK_CUSTOMER);
            e.PSI_FK_CUSTOMER = null;
            return e;
        });
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(new_model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        // context.router.navigate(['/cmps/search'])
                        window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsImportComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsImportComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsImportComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmpsImportComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    CmpsImportComponent.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Approve(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Approved",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // swal({title:"Approved!", type: "success"}, () => {
        //       //   context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmpsImportComponent.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            // window.open(`http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/${res}`, '_blank');
            // http://tsr-dcpai-app.thaioil.localnet/PAF/CPAIMVC/ApprovalForm/#/cmps/inter/b268708d2cac492d869c6ef7eb33c28c/201708100950250256305/edit
            // http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/PAF201708070940072521.pdf
            // swal({
            //   title: "Saved",
            //   text: "",
            //   type: "success"
            // }, () => {
            //   swal({title:"Saved!", type: "success"}, () => {
            //     context.router.navigate([`/cmps/search`])
            //   });
            // })
        });
    };
    CmpsImportComponent.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmpsImportComponent.prototype.log = function (e) {
        console.log(e);
    };
    CmpsImportComponent.prototype.playground = function () {
        var _this = this;
        this.pafService.playgroundGet().subscribe(function (res) {
            var model = res;
            _this.new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
            _this.new_criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadInterModelToCiteria(_this.criteria, model.PAF_BIDDING_ITEMS);
        });
        // const criteria = {
        //   "products": [
        //     {
        //       "name": "U95",
        //       "quantity_unit": "MT",
        //       "quantities": {
        //         "min": 0,
        //         "max": 0
        //       },
        //       "PBI_FK_MATERIAL": "YUG95",
        //       "PBI_QUANTITY_UNIT": "KBBL"
        //     }
        //   ],
        //   "fob": false,
        //   "cfr": false,
        //   "submited": false,
        //   "TARGET_PRODUCT": 0,
        //   "selected_customer": null,
        //   "PAF_BIDDING_ITEMS": [
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "1"
        //             },
        //             {}
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "1",
        //           "PRD_EXISTING_QUANTITY_MAX": "2",
        //           "SHOW_CONVERT_TO_FOB": "6",
        //           "PBI_MARKET_PRICE": "6",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "6",
        //           "PBI_BENCHMARK_PRICE": "78"
        //         },
        //         {
        //           "PBI_FK_MATERIAL": "YUG95"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "4",
        //       "PBI_INCOTERM": "DEG",
        //       "PBI_COUNTRY": "10000174",
        //       "PBI_FREIGHT_PRICE": "6",
        //       "PBI_NOTE": "6"
        //     },
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "8"
        //             }
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "7",
        //           "PRD_EXISTING_QUANTITY_MAX": "8",
        //           "SHOW_CONVERT_TO_FOB": "8",
        //           "PBI_MARKET_PRICE": "8",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "8",
        //           "PBI_BENCHMARK_PRICE": "77"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "8",
        //       "PBI_INCOTERM": "DDU",
        //       "PBI_COUNTRY": "10000078",
        //       "PBI_FREIGHT_PRICE": "8",
        //       "PBI_NOTE": "8"
        //     }
        //   ]
        // };
        // let new_model = Utility.clone(this.model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(criteria);
        // this.new_model = new_model;
        // console.log(new_model);
        // return new_model;
    };
    CmpsImportComponent.prototype.OnChangeCompany = function (company) {
        console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(company);
    };
    return CmpsImportComponent;
}());
CmpsImportComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmps-import',
        template: __webpack_require__("../../../../../src/app/containers/cmps/cmps-import.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */]) === "function" && _f || Object])
], CmpsImportComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=cmps-import.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-inter.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre> {{ criteria | json }} </pre> -->\n<!-- <pre> {{ new_model.PAF_BIDDING_ITEMS | json }} </pre> -->\n<!-- <pre> {{ new_criteria | json }} </pre> -->\n<app-shared-form\n  [title]=\"'Product Approval Form (CMPS - International Export)'\"\n  [model]=\"model\"\n  [master]=\"master\"\n  [criteria]=\"criteria\"\n  [is_cmps_dom]=\"false\"\n  [is_cmps_inter]=\"true\"\n  (OnChangeCompany)=\"OnChangeCompany($event)\"\n  (DoSaveDraft)=\"DoSaveDraft($event)\"\n  (DoSubmit)=\"DoSubmit($event)\"\n  (DoVerify)=\"DoVerify($event)\"\n  (DoEndorse)=\"DoEndorse($event)\"\n  (DoApproved)=\"DoApproved($event)\"\n  (DoGeneratePDF)=\"DoGeneratePDF($event)\"\n  (DoReject)=\"DoReject($event)\"\n  (DoCancel)=\"DoCancel($event)\"\n>\n</app-shared-form>\n\n\n\n<!-- <pre>{{ new_model | json }}</pre> -->\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-inter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmpsInterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CmpsInterComponent = (function () {
    function CmpsInterComponent(route, router, loaderService, masterService, pafService, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.new_model = {};
        this.criteria = {
            products: [{
                    name: "U95",
                    quantity_unit: "MT",
                    quantities: {
                        min: 0,
                        max: 0,
                    }
                }],
            fob: false,
            cfr: false,
            submited: false,
            TARGET_PRODUCT: 0,
            selected_customer: null,
            PAF_BIDDING_ITEMS: [{
                    SELECTED: false,
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }],
                    PRODUCTS: [{}],
                }],
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.model = {
            PDA_TEMPLATE: 'CMPS_INTER_SALE',
            PDA_TYPE: 'Type of Sale',
            PDA_FORM_ID: 'Draft',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            // PDA_CREATED: "/Date(1488880800000)/",
            PDA_FOR_COMPANY: "",
            PDA_SUB_TYPE: "",
            PDA_INCLUDED_REVIEW_TABLE: 'N',
            PDA_INCLUDED_BIDDING_TABLE: 'N',
            IS_DISABLED: false,
            IS_DISABLED_NOTE: false,
            PAF_REVIEW_ITEMS: [{
                    NEED_PRODUCT: true,
                    PAF_REVIEW_ITEMS_DETAIL: [{}],
                }],
            PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PAYMENT_ITEMS: [],
            SHOW_PAF_BIDDING_ITEMS: [{
                    PAF_ROUND: [{
                            PRODUCTS: [{}],
                        }]
                }],
            PAF_PROPOSAL_ITEMS: [],
            PDA_PRICE_UNIT: 'USD/BBL',
        };
        this.master = {};
    }
    CmpsInterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmpsInterComponent.prototype.ngAfterViewInit = function () {
    };
    CmpsInterComponent.prototype.ngOnDestroy = function () {
    };
    CmpsInterComponent.prototype.showProducts = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].showProducts(id);
        // let e = _.find(this.master.PRODUCTS, (e) => {
        //   return e.ID == id;
        // });
        // return e && e.TEXT;
    };
    CmpsInterComponent.prototype.showCustomer = function (id) {
        var e = __WEBPACK_IMPORTED_MODULE_3_lodash__["find"](this.master.CUSTOMERS, function (e) {
            return e.ID == id;
        });
        return e && e.VALUE;
    };
    CmpsInterComponent.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"](_this.master, res);
            _this.master.REAL_CUSTOMERS_INTER = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS_INTER);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(_this.master.CUSTOMERS_INTER);
            __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].master = _this.master;
            // this.master.CUSTOMERS_INTER = PAFStore.GetCustomers(this.master.CUSTOMERS_INTER);
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_3_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateModel(_this.model);
                    _this.model.PAF_PAYMENT_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](_this.model.PAF_PAYMENT_ITEMS, function (e) {
                        for (var i = 0; i < 5; ++i) {
                            try {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = e.PAF_PAYMENT_ITEMS_DETAIL[i] || {};
                            }
                            catch (e) {
                                e.PAF_PAYMENT_ITEMS_DETAIL[i] = {};
                            }
                        }
                        return e;
                    });
                    var _a = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateInterTable(_this.criteria, _this.model.PAF_BIDDING_ITEMS, _this.criteria.PAF_BIDDING_ITEMS), PAF_BIDDING_ITEMS = _a.PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS = _a.SHOW_PAF_BIDDING_ITEMS;
                    // this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                    _this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                    // PAFStore.LoadInterModelToCiteria(this.model.PAF_BIDDING_ITEMS);
                    _this.criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadInterModelToCiteria(_this.criteria, _this.model.PAF_BIDDING_ITEMS);
                    // console.log(this.criteria)
                    _this.criteria.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateSELECTED(_this.criteria.PAF_BIDDING_ITEMS, _this.model.PAF_PAYMENT_ITEMS);
                    _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(_this.model.PDA_FOR_COMPANY);
                    _this.model.IS_DISABLED = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED, _this.model.Buttons, _this.model.PDA_STATUS);
                    _this.model.IS_DISABLED_NOTE = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].CheckRealyDisabled(_this.model.IS_DISABLED_NOTE, _this.model.Buttons, _this.model.PDA_STATUS);
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.loaderService.display(false);
                var _a = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].UpdateInterTable(_this.criteria, _this.model.PAF_BIDDING_ITEMS, _this.criteria.PAF_BIDDING_ITEMS), PAF_BIDDING_ITEMS = _a.PAF_BIDDING_ITEMS, SHOW_PAF_BIDDING_ITEMS = _a.SHOW_PAF_BIDDING_ITEMS;
                _this.model.PAF_BIDDING_ITEMS = PAF_BIDDING_ITEMS;
                _this.criteria.PAF_BIDDING_ITEMS = SHOW_PAF_BIDDING_ITEMS;
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmpsInterComponent.prototype.handleChangeCustomer = function (customer) {
        this.model = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].HandleChangeCustomer(customer, this.model);
    };
    CmpsInterComponent.prototype.RemoveBiddingItemsRequest = function (target) {
        this.model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_3_lodash__["reject"](this.model.PAF_BIDDING_ITEMS, function (elem) {
            return elem == target;
        });
    };
    CmpsInterComponent.prototype.OnChangeCompany = function (company) {
        // console.log(company);
        this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].GetCustomers(company);
    };
    // public AddCustomerRequestTier(): void {
    //   console.log(this.criteria.TARGET_PRODUCT);
    //   if (this.criteria.TARGET_PRODUCT !== null) {
    //     console.log(this.criteria.TARGET_PRODUCT)
    //     const index = this.criteria.TARGET_PRODUCT;
    //     let NEW_PAF_REVIEW_ITEMS = [];
    //     let PAF_REVIEW_ITEMS = this.model.PAF_REVIEW_ITEMS;
    //     // let PAF_REVIEW_ITEMS = Utility.clone(this.model.PAF_REVIEW_ITEMS);
    //     // if (index == PAF_REVIEW_ITEMS.length - 1) {
    //       this.model.PAF_REVIEW_ITEMS.splice(index+1, 0, {
    //         NEED_PRODUCT: false,
    //         PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //         PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //         PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //       });
    //       this.model.PAF_REVIEW_ITEMS_DETAIL.push(index, 0, {})
    //     // }
    //     //  else {
    //     //   let target_index = index;
    //     //   for (var i = index + 1; i < PAF_REVIEW_ITEMS.length; i++) {
    //     //     target_index = i + 1;
    //     //     if (PAF_REVIEW_ITEMS[i].NEED_PRODUCT) {
    //     //       target_index = i;
    //     //       break;
    //     //     }
    //     //   }
    //     //   this.model.PAF_REVIEW_ITEMS.push({
    //     //     NEED_PRODUCT: false,
    //     //     PRI_FK_CUSTOMER: PAF_REVIEW_ITEMS[index].PRI_FK_CUSTOMER,
    //     //     PRI_FK_MATERIAL: PAF_REVIEW_ITEMS[index].PRI_FK_MATERIAL,
    //     //     PRI_QUANTITY_UNIT: PAF_REVIEW_ITEMS[index].PRI_QUANTITY_UNIT,
    //     //   });
    //     //   this.model.PAF_REVIEW_ITEMS_DETAIL.push({})
    //     // }
    //   }
    // }
    CmpsInterComponent.prototype.UpdateCustomerIndex = function (index, customer) {
        this.model.PAF_PAYMENT_ITEMS[index].PPI_FK_CUSTOMER = customer;
        // this.model.PAF_PAYMENT_ITEMS_DETAIL[index].
        // this.model.PAF_PROPOSAL_ITEMS[index].
    };
    CmpsInterComponent.prototype.handleModelChange = function (target, value) {
        this.model[target] = value;
        switch (value) {
            case "Spot sale":
                this.model.PDA_TYPE_NOTE = '';
                break;
            case "Term sale":
                this.model.PDA_TYPE_NOTE = '';
                // this.model.PDA_TYPE_NOTE = 'Monthly Term Export : Gasoline 95 and Gasoline 91 in 1H 2017 to Chevron USA Inc (Singapore Branch)';
                break;
            case "Others":
                this.model.PDA_TYPE_NOTE = '';
                break;
            default:
                'code...';
                break;
        }
    };
    CmpsInterComponent.prototype.ShowTypeAHead = function (obj) {
        return obj && obj.TEXT;
    };
    CmpsInterComponent.prototype.GetObjTypeAHead = function (source_list, target) {
        console.log(target);
        return target && target.VALUE;
    };
    // public DoSaveDraft() : void {
    //   const context = this;
    //   const model = Utility.NormalizeModel(context.model, context.master);
    //   let new_model = Utility.clone(model);
    //   new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(this.criteria);
    //   console.log(new_model)
    //   this.pafService.SaveDraft(new_model).subscribe(
    //     (res) => {
    //       swal({title:"Saved!", type: "success"}, () => {
    //         context.router.navigate([`/cmps/inter/${res.req_transaction_id}/${res.transaction_id}/edit`])
    //       });
    //     }
    //   )
    // }
    CmpsInterComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        var new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
        new_model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].AdjustInterModelBeforeSave(this.criteria);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(new_model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmps/inter/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsInterComponent.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        var new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
        new_model.PAF_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].AdjustInterModelBeforeSave(this.criteria);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustImportModelBeforeSave(this.criteria);
        // new_model.PAF_PAYMENT_ITEMS = _.map(new_model.PAF_PAYMENT_ITEMS, (e) => {
        //   e.PPI_FK_VENDOR = Utility.clone(e.PPI_FK_CUSTOMER);
        //   e.PPI_FK_CUSTOMER = null;
        //   return e;
        // });
        // new_model.PAF_PROPOSAL_ITEMS = _.map(new_model.PAF_PROPOSAL_ITEMS, (e) => {
        //   e.PSI_FK_VENDOR = Utility.clone(e.PSI_FK_CUSTOMER);
        //   e.PSI_FK_CUSTOMER = null;
        //   return e;
        // });
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(new_model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        // context.router.navigate(['/cmps/search'])
                        window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsInterComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsInterComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsInterComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmpsInterComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_6__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    CmpsInterComponent.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_5_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Approve(model).subscribe(
        //   (res) => {
        //     swal({
        //       title: "Approved",
        //       text: "",
        //       type: "success"
        //     }, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // swal({title:"Approved!", type: "success"}, () => {
        //       //   context.router.navigate([`/cmps/search`])
        //       // });
        //     })
        //   }
        // )
    };
    CmpsInterComponent.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_6__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            // window.open(`http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/${res}`, '_blank');
            // http://tsr-dcpai-app.thaioil.localnet/PAF/CPAIMVC/ApprovalForm/#/cmps/inter/b268708d2cac492d869c6ef7eb33c28c/201708100950250256305/edit
            // http://tsr-dcpai-app.thaioil.localnet/PAF/web/report/tmpfile/PAF201708070940072521.pdf
            // swal({
            //   title: "Saved",
            //   text: "",
            //   type: "success"
            // }, () => {
            //   swal({title:"Saved!", type: "success"}, () => {
            //     context.router.navigate([`/cmps/search`])
            //   });
            // })
        });
    };
    CmpsInterComponent.prototype.ShowCustomer = function (cus_id) {
        return __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].getTextByID(cus_id, this.master.CUSTOMERS);
    };
    CmpsInterComponent.prototype.log = function (e) {
        console.log(e);
    };
    CmpsInterComponent.prototype.playground = function () {
        var _this = this;
        this.pafService.playgroundGet().subscribe(function (res) {
            var model = res;
            _this.new_model = __WEBPACK_IMPORTED_MODULE_6__common__["a" /* Utility */].clone(model);
            _this.new_criteria = __WEBPACK_IMPORTED_MODULE_6__common__["c" /* PAFStore */].LoadInterModelToCiteria(_this.criteria, model.PAF_BIDDING_ITEMS);
        });
        // const criteria = {
        //   "products": [
        //     {
        //       "name": "U95",
        //       "quantity_unit": "MT",
        //       "quantities": {
        //         "min": 0,
        //         "max": 0
        //       },
        //       "PBI_FK_MATERIAL": "YUG95",
        //       "PBI_QUANTITY_UNIT": "KBBL"
        //     }
        //   ],
        //   "fob": false,
        //   "cfr": false,
        //   "submited": false,
        //   "TARGET_PRODUCT": 0,
        //   "selected_customer": null,
        //   "PAF_BIDDING_ITEMS": [
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "1"
        //             },
        //             {}
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "1",
        //           "PRD_EXISTING_QUANTITY_MAX": "2",
        //           "SHOW_CONVERT_TO_FOB": "6",
        //           "PBI_MARKET_PRICE": "6",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "6",
        //           "PBI_BENCHMARK_PRICE": "78"
        //         },
        //         {
        //           "PBI_FK_MATERIAL": "YUG95"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "4",
        //       "PBI_INCOTERM": "DEG",
        //       "PBI_COUNTRY": "10000174",
        //       "PBI_FREIGHT_PRICE": "6",
        //       "PBI_NOTE": "6"
        //     },
        //     {
        //       "SELECTED": false,
        //       "PAF_ROUND": [
        //         {
        //           "PRODUCTS": [
        //             {
        //               "PRN_FIXED": "8"
        //             }
        //           ]
        //         }
        //       ],
        //       "PRODUCTS": [
        //         {
        //           "PBI_FK_MATERIAL": "YUG95",
        //           "PRD_EXISTING_QUANTITY_MIN": "7",
        //           "PRD_EXISTING_QUANTITY_MAX": "8",
        //           "SHOW_CONVERT_TO_FOB": "8",
        //           "PBI_MARKET_PRICE": "8",
        //           "PBI_LATEST_LP_PLAN_PRICE_FLOAT": "8",
        //           "PBI_BENCHMARK_PRICE": "77"
        //         }
        //       ],
        //       "PBI_FK_CUSTOMER": "0000400001",
        //       "PBI_CONTACT_PERSON": "dsadsa",
        //       "PBI_PRICING_PERIOD": "8",
        //       "PBI_INCOTERM": "DDU",
        //       "PBI_COUNTRY": "10000078",
        //       "PBI_FREIGHT_PRICE": "8",
        //       "PBI_NOTE": "8"
        //     }
        //   ]
        // };
        // let new_model = Utility.clone(this.model);
        // new_model.PAF_BIDDING_ITEMS = PAFStore.AdjustInterModelBeforeSave(criteria);
        // this.new_model = new_model;
        // console.log(new_model);
        // return new_model;
    };
    return CmpsInterComponent;
}());
CmpsInterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmps-inter',
        template: __webpack_require__("../../../../../src/app/containers/cmps/cmps-inter.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["e" /* DomesticService */]) === "function" && _f || Object])
], CmpsInterComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=cmps-inter.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-other.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model | json}}</pre> -->\n<!-- <pre>{{ criteria | json}}</pre> -->\n<div class=\"wrapper-detail\">\n    <h2 class=\"first-title\">\n        <span class=\"note\"> Product Approval Form (CMPS - Other) </span> Please fill in form as relevant\n    </h2>\n    <div class=\"form-horizontal\">\n        <div class=\"row\">\n            <div class=\"col-xs-3\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-3 control-label\">Form Id</label>\n                    <div class=\"col-xs-9\">\n                        <input class=\"form-control\" [(ngModel)]=\"model.PDA_FORM_ID\" type=\"text\" [disabled]=\"true\">\n                    </div>\n                </div>\n        </div>\n            <div class=\"col-xs-9\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-2 control-label\">Created Date</label>\n                    <div class=\"col-xs-3\">\n                        <ptx-datepicker [disabled]=\"true\" [model]=\"model.PDA_CREATED\"></ptx-datepicker>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-xs-3\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-3 control-label\">For</label>\n                    <div class=\"col-xs-9\">\n                        <select class=\"form-control\" [disabled]=\"model.IS_DISABLED\" [(ngModel)]=\"model.PDA_FOR_COMPANY\">\n                          <option value=\"\">-- Select --</option>\n                          <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.COMPANY\">{{ elem.VALUE }}</option>\n                        </select>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-xs-3\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-3 control-label\">Topic</label>\n                    <div class=\"col-xs-9\">\n                      <input type=\"text\" class=\"form-control\" style=\"width: 535px;\" [(ngModel)]=\"model.PDA_TOPIC\" [disabled]=\"model.IS_DISABLED\">\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-xs-12\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-12 control-label\">Brief Market Situation (as necessary)</label>\n                    <div class=\"col-xs-12\">\n                        <app-freetext *ngIf=\"true\" [disabled]=\"model.IS_DISABLED_NOTE\" [elementId]=\"'PDA_BRIEF'\" [model]=\"model.PDA_BRIEF\" (HandleStateChange)=\"model.PDA_BRIEF = $event\">\n                        </app-freetext>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <ptx-file-upload btnClass=\"text-left\" [model]=\"model.PAF_ATTACH_FILE\" [is_disabled]=\"model.IS_DISABLED\" [need_caption]=\"true\" [desc]=\"'Image files'\" filterBy=\"BMS\" (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\">\n            </ptx-file-upload>\n        </div>\n        <br />\n    </div>\n\n    <div class=\"row\">\n        <div class=\"col-xs-12\">\n            <div class=\"form-group\">\n                <label class=\"control-label\">Note</label>\n                <textarea class=\"form-control inline-textarea\" [disabled]=\"model.IS_DISABLED_NOTE\" [(ngModel)]=\"model.PDA_NOTE\"></textarea>\n            </div>\n            <ptx-file-upload btnClass=\"text-left\" [model]=\"model.PAF_ATTACH_FILE\" [is_disabled]=\"model.IS_DISABLED\" [filterBy]=\"'REF'\" (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\">\n            </ptx-file-upload>\n        </div>\n    </div>\n\n    <div class=\"row\">\n        <div class=\"col-xs-12\">\n            <div class=\"form-group\">\n                <label class=\"control-label\">Reason for Approval/Reject</label>\n                <textarea class=\"form-control inline-textarea\" [disabled]=\"true\" [(ngModel)]=\"model.PDA_REASON\"></textarea>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"wrapper-button\" id=\"buttonDiv\">\n        <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft(model)\"> SAVE DRAFT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit(model)\"> SAVE & SUBMIT </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify(model)\"> VERIFY </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse(model)\"> ENDORSE </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved(model)\"> APPROVE </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject(model)\"> REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel(model)\"> CANCEL </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-other.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert__ = __webpack_require__("../../../../sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmpsOtherComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CmpsOtherComponent = (function () {
    function CmpsOtherComponent(route, router, loaderService, masterService, pafService, decimalPipe, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.decimalPipe = decimalPipe;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.criteria = {
            SELECT_GRADE: 0,
            SELECTED_CUSTOMER: 0,
        };
        this.new_criteria = this.criteria;
        this.model = {
            PDA_FORM_ID: 'Draft',
            PDA_TEMPLATE: 'CMPS_OTHER',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_5_moment_moment__().format('YYYY-MM-DD'),
            PDA_FOR_COMPANY: '',
        };
        this.master = {};
    }
    CmpsOtherComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    CmpsOtherComponent.prototype.ngAfterViewInit = function () {
    };
    CmpsOtherComponent.prototype.ngOnDestroy = function () {
    };
    CmpsOtherComponent.prototype.load = function () {
        var _this = this;
        // this.playground();
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"](_this.master, res);
            _this.master.CUSTOMERS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.CUSTOMERS_BITUMEN);
            _this.master.PRODUCTS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.PRODUCTS_CMLA_IMPORT);
            _this.master.QUANTITY_UNITS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.QUANTITY_UNITS_CMLA);
            _this.master.CURRENCY_PER_VOLUMN_UNITS = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(_this.master.CURRENCY_PER_VOLUMN_UNITS_CMLA);
            __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].master = _this.master;
            if (_this.req_transaction_id) {
                _this.pafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_4_lodash__["extend"]({}, _this.model, res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    _this.model = __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].UpdateModel(_this.model);
                }, function (error) {
                    console.log(error);
                });
            }
            else {
                _this.model.Buttons = [
                    {
                        "name": "SAVE DRAFT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "SUBMIT",
                        "page_url": null,
                        "call_xml": null
                    },
                    {
                        "name": "NO_PDF",
                        "page_url": null,
                        "call_xml": null
                    },
                ];
                _this.loaderService.display(false);
            }
        }, function (err) {
            _this.loaderService.display(false);
        });
    };
    CmpsOtherComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].NormalizeModel(context.model, context.master);
        // model = PAFStore.NormalizeModelCMLAIMPORT(model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Save",
            text: "Are you sure you want to save?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Saved!", type: "success" }, function () {
                    context.router.navigate(["/cmps/other/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    // context.router.navigate(['/cmps/search'])
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsOtherComponent.prototype.DoSubmit = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_7__common__["c" /* PAFStore */].NormalizeModelCMLAIMPORT(model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Submit",
            text: "Do you confirm to submit for approving?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                context.pafService.Submit(model).subscribe(function (res) {
                    __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Submitted!", type: "success" }, function () {
                        window.location.href = "../../web/MainBoards.aspx";
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "save error");
            });
        });
    };
    CmpsOtherComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Verify",
            text: "Are you sure you want to verify?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Verified!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsOtherComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Endorse",
            text: "Are you sure you want to endorse?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Endorsed!", type: "success" }, function () {
                    // context.router.navigate(['/cmps/search'])
                    window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../web/MainBoards.aspx";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
            });
        });
        // this.pafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CmpsOtherComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_7__common__["e" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
                                title: "Rejected",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../";
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CmpsOtherComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_7__common__["e" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Cancel.", function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.PDA_REASON = result;
                        _this.pafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
                                title: "Cancelled",
                                text: "",
                                type: "success"
                            }, function () {
                                window.location.href = "../../web/MainBoards.aspx";
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
                        });
                    });
                }
            }
        });
    };
    CmpsOtherComponent.prototype.DoApproved = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_6_sweetalert__({
            title: "Approve",
            text: "Are you sure you want to approve?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.pafService.Approve(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__({ title: "Approved!", type: "success" }, function () {
                    window.location.href = __WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../";
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_6_sweetalert__("Something went wrong!", "submit error");
            });
        });
    };
    CmpsOtherComponent.prototype.DoGeneratePDF = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_7__common__["a" /* Utility */].clone(this.model);
        this.pafService.GeneratePDF(model).subscribe(function (res) {
            window.open(__WEBPACK_IMPORTED_MODULE_7__common__["d" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
        });
    };
    CmpsOtherComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToReject(button_list);
    };
    CmpsOtherComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common__["f" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    return CmpsOtherComponent;
}());
CmpsOtherComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmps-other',
        template: __webpack_require__("../../../../../src/app/containers/cmps/cmps-other.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["e" /* DomesticService */]) === "function" && _g || Object])
], CmpsOtherComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=cmps-other.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-search-list.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{data_list|json}}</pre> -->\n<div class=\"wrapper-detail\">\n  <div>\n    <h1>Search Approval Form</h1>\n  </div>\n  <!-- <div class=\"row\">\n    <div class=\"form-group\">\n      <div class=\"col-md-4\">\n        <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Date</label>\n        <div class=\"col-md-8\">\n          <div class=\"box-date\">\n            <input type=\"text\" class=\"form-control\" name=\"\">\n          </div>\n        </div>\n      </div>\n    </div>\n  </div> -->\n\n  <div class=\"form-group\">\n    <div class=\"col-md-12\">\n      <div class=\"col-md-6 pull-left\">\n        <a class=\"btn btn-success\" routerLink=\"/cmps/dom\" id=\"addBtn\" >Add</a>\n      </div>\n      <div class=\"col-md-6 text-right\">\n        <button type=\"submit\" class=\"btn btn-primary\">Search</button>\n        <button type=\"reset\" value=\"Reset\" class=\"btn btn-default\" onclick=\"resetForm()\">Clear All</button>\n      </div>\n    </div>\n  </div>\n  <br />\n  <div class=\"page-header\">\n    <h1>Search Result</h1>\n  </div>\n  <div class=\"task-box\">\n    <div class=\"wrapper-table collapse in\" id=\"collapseJuly\">\n      <table id=\"example\" class=\"table table-hover table-striped table-bordered\" cellspacing=\"0\">\n        <thead>\n          <tr>\n            <th>Date/Status</th>\n            <th>Task ID</th>\n            <th>Document No</th>\n            <th>Form Type</th>\n            <th>For Company</th>\n            <th>Created By</th>\n          </tr>\n        </thead>\n        <tbody>\n          <!-- <tr class=\"waiting\" data-href=\"http://myspace.com\">\n            <td colspan=\"8\">Waiting for search</td>\n          </tr> -->\n\n          <tr class=\"drafts clickable-row\" routerLink=\"/cmps/{{elem.req_transaction_id}}/{{elem.transaction_id}}/edit\" style=\"cursor: pointer;\" *ngFor=\"let elem of data_list\">\n            <td>{{ elem.created_date }} <span>{{elem.status}}</span></td>\n            <td>{{ elem.transaction_id }}</td>\n            <td>{{ elem.purchase_no }}</td>\n            <td>{{ elem.template_name }}</td>\n            <td>{{ elem.for_company || \"\" }}</td>\n            <td>{{ elem.created_by }}</td>\n          </tr>\n          <!-- <tr class=\"waiting\">\n            <td colspan=\"8\">Data not found</td>\n          </tr> -->\n        </tbody>\n      </table>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/cmps/cmps-search-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmpsSearchListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CmpsSearchListComponent = (function () {
    function CmpsSearchListComponent(route, router, loaderService, masterService, pafService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.data_list = [];
    }
    CmpsSearchListComponent.prototype.ngOnInit = function () {
        this.load();
    };
    CmpsSearchListComponent.prototype.load = function () {
        var _this = this;
        this.loaderService.display(true);
        this.pafService.getList().subscribe(function (res) {
            _this.data_list = res && res.CrudePurchaseTransaction;
            _this.loaderService.display(false);
        }, function (err) {
            console.log(err);
            _this.loaderService.display(false);
        });
    };
    return CmpsSearchListComponent;
}());
CmpsSearchListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-cmps-search-list',
        template: __webpack_require__("../../../../../src/app/containers/cmps/cmps-search-list.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object])
], CmpsSearchListComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=cmps-search-list.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/demurrage/demurrage-form.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{ model | json }}</pre> -->\n<div class=\"wrapper-detail\">\n  <h2 class=\"first-title\">\n    <span class=\"note\">Approval Form (Demurage)</span> Please fill in form as relevant\n  </h2>\n  <div class=\"form-horizontal\">\n    <div class=\"row\">\n      <div class=\"col-xs-3\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-3 control-label\">Form Id</label>\n          <div class=\"col-xs-9\">\n            <input class=\"form-control\" [(ngModel)]=\"model.PDA_FORM_ID\" type=\"text\" [disabled]=\"true\">\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-9\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-2 control-label\">Created Date</label>\n          <div class=\"col-xs-3\">\n            <ptx-datepicker\n              [disabled]=\"true\"\n              [model]=\"model.PDA_CREATED\"\n            ></ptx-datepicker>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row mt-30\">\n    <div class=\"col-xs-12\">\n      <div class=\"form-group\">\n        <label class=\"control-label\">Note</label>\n        <textarea\n          class=\"form-control inline-textarea\"\n          [disabled]=\"model.IS_DISABLED_NOTE\"\n          [(ngModel)]=\"model.PDA_NOTE\"></textarea>\n      </div>\n      <ptx-file-upload\n          btnClass=\"text-left\"\n          [model]=\"model.PAF_ATTACH_FILE\"\n          [is_disabled]=\"model.IS_DISABLED\"\n          [filterBy]=\"'REF'\"\n          (handleModelChanged)=\"model.PAF_ATTACH_FILE = $event\"\n        >\n        </ptx-file-upload>\n    </div>\n  </div>\n\n    <div class=\"wrapper-button\" id=\"buttonDiv\">\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft.emit(model)\"> SAVE DRAFT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit.emit(model)\"> SAVE & SUBMIT </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify.emit(model)\"> VERIFY </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse.emit(model)\"> ENDORSE </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved.emit(model)\"> APPROVE </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject.emit(model)\"> REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel.emit(model)\"> CANCEL </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF.emit(model)\"> PRINT </button>\n    </div>\n\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/demurrage/demurrage-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageFormComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DemurrageFormComponent = (function () {
    function DemurrageFormComponent(route, router, loaderService, masterService, pafService, decimalPipe, domesticService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.decimalPipe = decimalPipe;
        this.domesticService = domesticService;
        this.req_transaction_id = "";
        this.transaction_id = "";
        this.criteria = {
            SELECT_GRADE: 0,
            SELECTED_CUSTOMER: 0,
        };
        this.new_criteria = this.criteria;
        this.PAF_PAYMENT_ITEM_DETAIL = [{}, {}, {}, {}, {}];
        this.PAF_BTM_ALLOCATE_ITEMS = {
            PAF_BTM_ALLOCATE_ITEMS_DETAIL: [{}],
        };
        this.PAF_BTM_GRADE_ITEM = {
            PAF_BTM_ALLOCATE_ITEMS: [__WEBPACK_IMPORTED_MODULE_5__common__["a" /* Utility */].clone(this.PAF_BTM_ALLOCATE_ITEMS)],
            PAF_BTM_GRADE_ASSUME_ITEMS: [{
                    PGA_NO: 1,
                    PGA_DETAIL: "",
                }],
        };
        this.model = {
            PDA_FORM_ID: 'Draft',
            PDA_TEMPLATE: 'CMLA_BITUMEN',
            // PDA_TYPE: 'Type of Import',
            PDA_CREATED: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
            PAF_BTM_DETAIL: {
                PBD_VOLUME_UNIT: 'MT',
                PBD_ARGUS_PRICE_UNIT: 'USD/MT',
                PBD_ESTIMATED_FX_UNIT: 'BATH/USD',
                PBD_AVG_HSFO_UNIT: 'USD/MT',
                PBD_FX_DATE_FROM: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().startOf('month').format('YYYY-MM-DD'),
                PBD_FX_DATE_TO: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
                PBD_HSFO_DATE_FROM: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().startOf('month').format('YYYY-MM-DD'),
                PBD_HSFO_DATE_TO: __WEBPACK_IMPORTED_MODULE_4_moment_moment__().format('YYYY-MM-DD'),
                PBD_ESTIMATED_FX: 0,
                PBD_AVG_HSFO: 0,
                PBD_SIM_UNIT: 'USD/MT',
                PBD_SIM_PLAN: 0,
                PBD_CROP_PLAN: 0,
                PBD_CORP_UNIT: 'USD/MT',
            },
            PAF_BTM_GRADE_ITEMS: [__WEBPACK_IMPORTED_MODULE_5__common__["a" /* Utility */].clone(this.PAF_BTM_GRADE_ITEM)],
        };
        this.master = {};
    }
    DemurrageFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['id'];
            _this.transaction_id = params['transaction_id'];
            _this.load();
        });
    };
    DemurrageFormComponent.prototype.load = function () {
    };
    DemurrageFormComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToReject(button_list);
    };
    DemurrageFormComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_5__common__["f" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    return DemurrageFormComponent;
}());
DemurrageFormComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-demurrage-form',
        template: __webpack_require__("../../../../../src/app/containers/demurrage/demurrage-form.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["d" /* PafService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["a" /* DecimalPipe */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__services__["e" /* DomesticService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services__["e" /* DomesticService */]) === "function" && _g || Object])
], DemurrageFormComponent);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=demurrage-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/not-found/not-found.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  not-found works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/containers/not-found/not-found.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotFoundComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotFoundComponent = (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    return NotFoundComponent;
}());
NotFoundComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-not-found',
        template: __webpack_require__("../../../../../src/app/containers/not-found/not-found.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [])
], NotFoundComponent);

//# sourceMappingURL=not-found.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <pre>{{data_list|json}}</pre> -->\n<div class=\"wrapper-detail\">\n  <div>\n    <h1>Search Approval Form</h1>\n  </div>\n  <div class=\"form\">\n      <div class=\"row\">\n        <div class=\"form-group\">\n          <div class=\"col-md-4\">\n              <label for=\"input-date\" class=\"col-md-3 control-label\">Date Purchase</label>\n              <div class=\"col-md-7\">\n                  <div class=\"box-date\">\n                    <ptx-daterangepicker\n                      [disabled]=\"model.IS_DISABLED\"\n                      [start_date]=\"model.DATE_FROM\"\n                      [end_date]=\"model.DATE_TO\"\n                      (modelChange)=\"model.DATE_FROM = $event.start_date;model.DATE_TO = $event.end_date;\"\n                    ></ptx-daterangepicker>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Customer</label>\n              <div class=\"col-md-7\">\n                <ptx-sugester\n                  [model]=\"model.CUSTOMER_ID\"\n                  [data_list]=\"master.CUSTOMER_LIST\"\n                  [is_disabled]=\"false\"\n                  [show_field]=\"'VALUE'\"\n                  [select_field]=\"'ID'\"\n                  (ModelChanged)=\"model.CUSTOMER_ID = $event\"\n                >\n                </ptx-sugester>\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Vendor</label>\n              <div class=\"col-md-7\">\n                <ptx-sugester\n                  [model]=\"model.VENDOR_ID\"\n                  [data_list]=\"master.VENDOR_LIST\"\n                  [is_disabled]=\"false\"\n                  [show_field]=\"'VALUE'\"\n                  [select_field]=\"'ID'\"\n                  (ModelChanged)=\"model.VENDOR_ID = $event\"\n                >\n                </ptx-sugester>\n              </div>\n          </div>\n        </div>\n        <br/>\n        <div class=\"form-group\">\n          <div class=\"col-md-4\">\n            <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Created by</label>\n            <div class=\"col-md-9\">\n              <ptx-sugester\n                [model]=\"model.CREATED_LIST\"\n                [data_list]=\"master.CREATED_LIST\"\n                [is_disabled]=\"false\"\n                [show_field]=\"'VALUE'\"\n                [select_field]=\"'ID'\"\n                (ModelChanged)=\"model.CREATED_LIST = $event\"\n              >\n              </ptx-sugester>\n            </div>\n          </div>\n        </div>\n      </div>\n  </div>\n  <!-- <div class=\"row\">\n      <div class=\"form-group\">\n          <div class=\"col-md-4\">\n              <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Date Purchase</label>\n              <div class=\"col-md-7\">\n                  <div class=\"box-date\">\n                      <input class=\"form-control input-date-picker\" id=\"date-range-start\" name=\"datePurchaseField\" type=\"text\" value=\"\" />\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Feedstock</label>\n              <div class=\"col-md-7\">\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Name</label>\n              <div class=\"col-md-7\">\n              </div>\n          </div>\n      </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"form-group\">\n          <div class=\"col-md-4\">\n              <label for=\"inputEmail3\" class=\"col-md-3 control-label\">Supplier</label>\n              <div class=\"col-md-7\">\n              </div>\n          </div>\n      </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"form-group\">\n          <div class=\"col-md-12\">\n              <label for=\"inputEmail3\" class=\"col-md-1 control-label\">Created by</label>\n              <div class=\"col-md-10 createByField\">>\n              </div>\n          </div>\n      </div>\n  </div> -->\n  <br />\n  <div class=\"form-group\">\n      <div class=\"col-md-12\">\n          <div class=\"col-md-4 pull-left\">\n              <!-- <a class=\"btn btn-success\" routerLink=\"\" id=\"addBtn\" name=\"addBtn\" role=\"button\">Add</a> -->\n          </div>\n          <div class=\"col-md-4 text-center\">\n              <button type=\"submit\" class=\"btn btn-primary\">Search</button>\n              <button type=\"reset\" value=\"Reset\" class=\"btn btn-default\" onclick=\"resetForm()\">Clear All</button>\n          </div>\n          <div class=\"col-md-4 text-right\">\n              <div id=\"dupBtn\">\n                  <a class=\"btn btn-info\" href=\"#\" id=\"dubCopy\" name=\"dubCopy\" role=\"button\" onclick=\"chkChecked()\">Copy</a>\n              </div>\n          </div>\n      </div>\n  </div>\n\n  <br />\n  <div class=\"page-header\">\n    <h1>Search Result</h1>\n  </div>\n  <div class=\"task-box\">\n    <div class=\"wrapper-table collapse in\" id=\"collapseJuly\">\n      <table id=\"example\" class=\"table table-hover table-striped table-bordered\" cellspacing=\"0\">\n        <thead>\n          <tr>\n            <th>Date/Status</th>\n            <th>Document No</th>\n            <th>Form Type</th>\n            <th>For</th>\n            <th>Awarded Customer/Supplier</th>\n            <th>Created By</th>\n            <th>Updated By</th>\n            <th>Copy</th>\n          </tr>\n        </thead>\n        <tbody>\n          <!-- <tr class=\"waiting\" data-href=\"http://myspace.com\">\n            <td colspan=\"8\">Waiting for search</td>\n          </tr> -->\n\n          <tr class=\"drafts clickable-row\" [routerLink]=\"GetUrl(elem)\" style=\"cursor: pointer;\" *ngFor=\"let elem of data_list;let index = index\">\n            <td>{{ elem.created_date | datex:'MMMM DD YYYY' }} <span>{{elem.status}}</span></td>\n            <td>{{ elem.purchase_no }}</td>\n            <td>{{ elem.template_name }}</td>\n            <td>{{ elem.for_company }}</td>\n            <td>{{ elem.awarded_summary }}</td>\n            <td>{{ elem.created_by }}</td>\n            <td>{{ elem.updated_by }}</td>\n            <td>\n              <input (click)=\"onEvent($event)\" type=\"checkbox\" name=\"dup\" [(ngModel)]=\"data_list[index].SELECTD\" >\n            </td>\n          </tr>\n          <!-- <tr class=\"waiting\">\n            <td colspan=\"8\">Data not found</td>\n          </tr> -->\n        </tbody>\n      </table>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchComponent = (function () {
    function SearchComponent(route, router, loaderService, masterService, pafService) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.pafService = pafService;
        this.model = {};
        this.master = {};
        this.data_list = [];
    }
    SearchComponent.prototype.ngOnInit = function () {
        this.load();
    };
    SearchComponent.prototype.load = function () {
        var _this = this;
        this.loaderService.display(true);
        this.pafService.GetSearchCriteria().subscribe(function (res) {
            _this.master = res;
            _this.pafService.getList().subscribe(function (res) {
                _this.data_list = res && res.PAFTransaction;
                _this.loaderService.display(false);
            }, function (err) {
                console.log(err);
                _this.loaderService.display(false);
            });
        }, function (err) {
            console.log(err);
            _this.loaderService.display(false);
        });
    };
    SearchComponent.prototype.GetUrl = function (_item) {
        var result = "/";
        // result =
        switch (_item.template_id) {
            case "CMPS_DOM_SALE":
                result += "cmps/dom/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                break;
            case "CMPS_INTER_SALE":
                result += "cmps/inter/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                break;
            case "CMPS_IMPORT":
                result += "cmps/import/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                break;
            case "CMLA_FORM_1":
                result += "cmla/form-1/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                break;
            case "CMLA_FORM_2":
                result += "cmla/form-2/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                break;
            case "CMLA_IMPORT":
                result += "cmla/import/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                break;
        }
        return result;
    };
    SearchComponent.prototype.onEvent = function (event, index) {
        event.stopPropagation();
        this.data_list = __WEBPACK_IMPORTED_MODULE_3_lodash__["map"](this.data_list, function (e) {
            e.SELECTED = false;
        });
        this.data_list[index] = true;
    };
    return SearchComponent;
}());
SearchComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'ptx-search',
        template: __webpack_require__("../../../../../src/app/containers/search/search.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["b" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["c" /* MasterService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services__["d" /* PafService */]) === "function" && _e || Object])
], SearchComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=search.component.js.map

/***/ }),

/***/ "../../../../../src/app/containers/think/think.component.html":
/***/ (function(module, exports) {

module.exports = "<ul>\n  <li>\n    <h3><a routerLink=\"/cmla/bitumen\"> CMLA (Bitumen) </a></h3>\n  </li>\n  <!-- <li>\n    <h3><a routerLink=\"/cmps/search\"> CMPS (Search) </a></h3>\n  </li>\n  <li>\n    <h3><a routerLink=\"/cmps/dom\"> CMPS (Domestic) </a></h3>\n  </li>\n  <li>\n    <h3><a routerLink=\"/cmps/inter\"> CMPS (International) </a></h3>\n  </li>\n  <li>\n    <h3><a routerLink=\"/cmps/import\"> CMPS (Import) </a></h3>\n  </li>\n  <li>\n    <h3><a routerLink=\"/cmla/form-1\"> CMLA Form #1 </a></h3>\n  </li>\n  <li>\n    <h3><a routerLink=\"/cmla/form-2\"> CMLA Form #2 </a></h3>\n  </li>\n  <li>\n    <h3><a routerLink=\"/cmla/import\"> CMLA Import </a></h3>\n  </li> -->\n</ul>\n<!--\n<pre>{{ model | json}}</pre>\n <app-freetext\n  [elementId]=\"'ssss'\"\n  [model]=\"model\"\n  (HandleStateChange)=\"model = $event;log()\"\n >\n</app-freetext>\n<button (click)=\"showBootbox()\"></button> -->\n <!-- <app-new-freetext\n  [elementId]=\"'ssss'\"\n  [model]=\"model\"\n  (contentChanged)=\"model = $event\"\n >\n</app-new-freetext> -->\n  <div class=\"page-header\">\n    <h1>Search Result</h1>\n  </div>\n  <div class=\"task-box\">\n    <div class=\"wrapper-table collapse in\" id=\"collapseJuly\">\n<ptx-datatable\n>\n</ptx-datatable>\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/containers/think/think.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThinkComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import * as bootbox from 'bootbox';
var ThinkComponent = (function () {
    function ThinkComponent() {
        this.model = "";
    }
    ThinkComponent.prototype.ngOnInit = function () {
        // setTimeout(() => {
        //   this.model = "ssssss";
        // }, 2000);
    };
    ThinkComponent.prototype.log = function () {
        console.log(this.model);
    };
    ThinkComponent.prototype.showBootbox = function () {
        bootbox.prompt({
            title: "sssss",
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === "") {
                    //Do nothing (cancel button pressed)
                    if (result === "") {
                        bootbox.alert("Please Key Reason for Reject.", function () { });
                    }
                    // $('#EventButton').val("0");
                }
                else {
                    console.log(result);
                    // $("#hdfNoteAction").val(result);
                    // $('form').attr('target', '_self');
                    // $("#btnMANUAL").click();
                    // LoadingProcess();
                }
            }
        });
    };
    return ThinkComponent;
}());
ThinkComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-think',
        template: __webpack_require__("../../../../../src/app/containers/think/think.component.html"),
        styles: []
    }),
    __metadata("design:paramtypes", [])
], ThinkComponent);

//# sourceMappingURL=think.component.js.map

/***/ }),

/***/ "../../../../../src/app/directives/restrict-number.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__ = __webpack_require__("../../../../../src/app/pipes/currency.pipe.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestrictNumberDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RestrictNumberDirective = (function () {
    function RestrictNumberDirective(elementRef, currencyPipe) {
        this.elementRef = elementRef;
        this.currencyPipe = currencyPipe;
        // @HostBinding('fraction')
        this.fraction = 2;
        this.ngModelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* EventEmitter */]();
        this.padding = '000000';
        this.decimal = '.';
        this.thousands = ',';
        this.onLoadCheck = true;
        this.el = this.elementRef.nativeElement;
    }
    RestrictNumberDirective.prototype.isNumberKey = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        var key = event.key;
        // console.log(key)
        if (key && (key == 'Subtract' || key == 'Del')) {
            return true;
        }
        var regex = /[0-9,\.-]+$/g;
        if (this.fraction == 0) {
            regex = /[0-9,-]+$/g;
        }
        // var regex = /[0-9\.]+$/g
        return regex.test(key);
        // if (key == '.') {
        //   return true;
        // }
        // // 61 // -
        // // 45 // -
        // // 43 // +
        // if (charCode == 61 || charCode == 45 || charCode == 43){
        //   return true;
        // }
        // if (charCode > 31 && (charCode < 48 || charCode > 57)){
        //   return false;
        // }
        // return true;
    };
    RestrictNumberDirective.prototype.ngOnInit = function () {
        this.el.value = this.currencyPipe.transform(this.ngModel, this.fraction);
    };
    RestrictNumberDirective.prototype.ngDoCheck = function (e) {
        if (this.onLoadCheck) {
            // console.log(this.el.value)
            if (this.el.value) {
                // if(this.fraction == 0) {
                //   this.el.value = this.el.value || "0";
                //   console.log('b _.round', this.el.value);
                //   this.el.value = _.round(+this.el.value, 0);
                //   console.log('a _.round', this.el.value);
                // }
                this.el.value = this.currencyPipe.transform(this.el.value, this.fraction);
            }
        }
        // this.el.value = this.currencyPipe.transform(this.el.value);
    };
    RestrictNumberDirective.prototype.ngOnChanges = function (e) {
        // this.el.value = this.currencyPipe.transform(this.el.value);
    };
    // @HostListener("keypress", ["$event.target.value"])
    // onChange(value) {
    //   console.log('keypress')
    //   var plain_number = value.replace(/[^\d|\-+|\.+]/g, '');
    //   this.el.value = this.parse(plain_number); // opossite of transform
    // }
    RestrictNumberDirective.prototype.onkeypress = function (e) {
        var event = e || window.event;
        if (event) {
            return this.isNumberKey(event);
        }
    };
    RestrictNumberDirective.prototype.onFocus = function (value) {
        this.onLoadCheck = false;
        this.el.value = this.currencyPipe.parse(value); // opossite of transform
        // var plain_number = value.replace(/[^\d|\-+|\.+]/g, '');
        // this.el.value = this.parse(value); // opossite of transform
        // this.ngModelChange.emit(plain_number);
        // >>>>>>> 918966c8a59f0afe6212c5659c44ba4a8850ac76
    };
    RestrictNumberDirective.prototype.onBlur = function (value) {
        // console.log('onBlur', value)
        // var plain_number = this.el.value.replace(/[^\d|\-+|\.+]/g, '');
        // this.el.value = plain_number;
        this.el.value = this.currencyPipe.transform(value, this.fraction);
        // var plain_number = value.replace(/[^\d|\-+|\.+]/g, '');
        // this.el.value = this.transform(value);
        // this.ngModelChange.emit(plain_number);
    };
    return RestrictNumberDirective;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], RestrictNumberDirective.prototype, "ngModel", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Object)
], RestrictNumberDirective.prototype, "options", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* HostBinding */])('min'),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Number)
], RestrictNumberDirective.prototype, "min", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* HostBinding */])('max'),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Number)
], RestrictNumberDirective.prototype, "max", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Input */])(),
    __metadata("design:type", Number)
], RestrictNumberDirective.prototype, "fraction", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_2" /* Output */])(),
    __metadata("design:type", Object)
], RestrictNumberDirective.prototype, "ngModelChange", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* HostListener */])('keypress'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], RestrictNumberDirective.prototype, "onkeypress", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* HostListener */])("focus", ["$event.target.value"]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], RestrictNumberDirective.prototype, "onFocus", null);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* HostListener */])("blur", ["$event.target.value"]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], RestrictNumberDirective.prototype, "onBlur", null);
RestrictNumberDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["k" /* Directive */])({
        selector: '[ptxRestrictNumber]',
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* ElementRef */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__["a" /* CurrencyPipe */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__pipes_currency_pipe__["a" /* CurrencyPipe */]) === "function" && _b || Object])
], RestrictNumberDirective);

var _a, _b;
//# sourceMappingURL=restrict-number.directive.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/allocations-filter.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllocationsFilterPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var AllocationsFilterPipe = (function () {
    function AllocationsFilterPipe() {
    }
    AllocationsFilterPipe.prototype.transform = function (value, PSI_FK_CUSTOMER) {
        // value = _.map(value, (o) => {
        //   if (o.PAF_BTM_ALLOCATE_ITEMS_DETAIL && o.PAF_BTM_ALLOCATE_ITEMS_DETAIL.length == 0) {
        //     o.PAF_BTM_ALLOCATE_ITEMS_DETAIL.push(Utility.clone({}));
        //   }
        //   return o;
        // })
        var result_list = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](value, function (o) {
            // o.PAF_BTM_ALLOCATE_ITEMS_DETAIL = _.extend([], o.PAF_BTM_ALLOCATE_ITEMS_DETAIL);
            // if (o.PAF_BTM_ALLOCATE_ITEMS_DETAIL && o.PAF_BTM_ALLOCATE_ITEMS_DETAIL.length == 0) {
            //   o.PAF_BTM_ALLOCATE_ITEMS_DETAIL.push(Utility.clone({}));
            // }
            return o.PAV_VOLUME && o.PAV_VOLUME != 0;
        });
        // console.log(`result_list`, result_list)
        return result_list;
        // return value;
    };
    return AllocationsFilterPipe;
}());
AllocationsFilterPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'allocationsFilter',
        pure: false,
    })
], AllocationsFilterPipe);

//# sourceMappingURL=allocations-filter.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/attachment-filter.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttachmentFilterPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var AttachmentFilterPipe = (function () {
    function AttachmentFilterPipe() {
    }
    AttachmentFilterPipe.prototype.transform = function (value, type_string) {
        var result_list = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](value, function (o) { return o.PAT_TYPE == type_string; });
        // let result_list = [];
        // switch (type_string) {
        //   case "IS_OTHER":
        //     result_list = _.filter(value, function(o) { return o.IS_OTHER == "Y"; });
        //     break;
        //   // case "main":
        //   //   result_list = _.filter(value, function(o) { return o.IS_OTHER == "N"; });
        //   //   break;
        //   default:
        //     result_list = value;
        //     break;
        // }
        return result_list;
    };
    return AttachmentFilterPipe;
}());
AttachmentFilterPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'attachmentFilter',
        pure: false,
    })
], AttachmentFilterPipe);

//# sourceMappingURL=attachment-filter.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/cmla-cal-filter.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CmlaCalFilterPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var CmlaCalFilterPipe = (function () {
    function CmlaCalFilterPipe() {
    }
    CmlaCalFilterPipe.prototype.transform = function (value, PSI_FK_CUSTOMER) {
        var result_list = __WEBPACK_IMPORTED_MODULE_1_lodash__["filter"](value, function (o) {
            return o.PBI_AWARDED_BIDDER == 'Y' && o.PBI_FK_CUSTOMER == PSI_FK_CUSTOMER;
        });
        return result_list;
    };
    return CmlaCalFilterPipe;
}());
CmlaCalFilterPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'cmlaCalFilter',
        pure: false,
    })
], CmlaCalFilterPipe);

//# sourceMappingURL=cmla-cal-filter.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/create-list.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateListPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CreateListPipe = (function () {
    function CreateListPipe() {
    }
    CreateListPipe.prototype.transform = function (value, args) {
        return null;
    };
    return CreateListPipe;
}());
CreateListPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'createList'
    })
], CreateListPipe);

//# sourceMappingURL=create-list.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/currency.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CurrencyPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PADDING = "000000";
var CurrencyPipe = (function () {
    function CurrencyPipe() {
        // TODO comes from configuration settings
        this.PREFIX = '';
        this.DECIMAL_SEPARATOR = ".";
        this.THOUSANDS_SEPARATOR = ",";
        this.SUFFIX = '';
    }
    CurrencyPipe.prototype.transform = function (value, fractionSize) {
        if (fractionSize === void 0) { fractionSize = 2; }
        var _a = (value || "").toString()
            .split("."), integer = _a[0], _b = _a[1], fraction = _b === void 0 ? "" : _b;
        fraction = fractionSize > 0
            ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
            : "";
        integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.THOUSANDS_SEPARATOR);
        if (integer == "") {
            return "";
        }
        return this.PREFIX + integer + fraction + this.SUFFIX;
    };
    CurrencyPipe.prototype.parse = function (value, fractionSize) {
        if (fractionSize === void 0) { fractionSize = 2; }
        var _a = (value || "").replace(this.PREFIX, "")
            .replace(this.SUFFIX, "")
            .split(this.DECIMAL_SEPARATOR), integer = _a[0], _b = _a[1], fraction = _b === void 0 ? "" : _b;
        integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, "g"), "");
        fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
            ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
            : "";
        if (integer == "") {
            return "";
        }
        return integer + fraction;
    };
    return CurrencyPipe;
}());
CurrencyPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'currency'
    }),
    __metadata("design:paramtypes", [])
], CurrencyPipe);

//# sourceMappingURL=currency.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/datex.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatexPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DatexPipe = (function () {
    function DatexPipe() {
    }
    DatexPipe.prototype.transform = function (value, format) {
        if (format === void 0) { format = ""; }
        // Try and parse the passed value.
        var momentDate = __WEBPACK_IMPORTED_MODULE_1_moment__(value);
        // If momentร didn't understand the value, return it unformatted.
        if (!momentDate.isValid())
            return value;
        // Otherwise, return the date formatted as requested.
        return momentDate.format(format);
    };
    return DatexPipe;
}());
DatexPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'datex'
    })
], DatexPipe);

//# sourceMappingURL=datex.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/dom-review-list.pipe.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DomReviewListPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DomReviewListPipe = (function () {
    function DomReviewListPipe() {
    }
    DomReviewListPipe.prototype.transform = function (value, args) {
        return null;
    };
    return DomReviewListPipe;
}());
DomReviewListPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Pipe */])({
        name: 'domReviewList'
    })
], DomReviewListPipe);

//# sourceMappingURL=dom-review-list.pipe.js.map

/***/ }),

/***/ "../../../../../src/app/pipes/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__currency_pipe__ = __webpack_require__("../../../../../src/app/pipes/currency.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__create_list_pipe__ = __webpack_require__("../../../../../src/app/pipes/create-list.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__datex_pipe__ = __webpack_require__("../../../../../src/app/pipes/datex.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dom_review_list_pipe__ = __webpack_require__("../../../../../src/app/pipes/dom-review-list.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__attachment_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/attachment-filter.pipe.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__allocations_filter_pipe__ = __webpack_require__("../../../../../src/app/pipes/allocations-filter.pipe.ts");
/* unused harmony reexport CurrencyPipe */
/* unused harmony reexport CreateListPipe */
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_2__datex_pipe__["a"]; });
/* unused harmony reexport DomReviewListPipe */
/* unused harmony reexport AttachmentFilterPipe */
/* unused harmony reexport AllocationsFilterPipe */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return pipes; });






var pipes = [
    __WEBPACK_IMPORTED_MODULE_0__currency_pipe__["a" /* CurrencyPipe */],
    __WEBPACK_IMPORTED_MODULE_1__create_list_pipe__["a" /* CreateListPipe */],
    __WEBPACK_IMPORTED_MODULE_2__datex_pipe__["a" /* DatexPipe */],
    __WEBPACK_IMPORTED_MODULE_3__dom_review_list_pipe__["a" /* DomReviewListPipe */],
    __WEBPACK_IMPORTED_MODULE_4__attachment_filter_pipe__["a" /* AttachmentFilterPipe */],
    __WEBPACK_IMPORTED_MODULE_5__allocations_filter_pipe__["a" /* AllocationsFilterPipe */],
];

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/routes/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__containers_cmps_cmps_dom_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-dom.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__containers_cmps_cmps_inter_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-inter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__containers_cmps_cmps_import_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-import.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__containers_think_think_component__ = __webpack_require__("../../../../../src/app/containers/think/think.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__containers_not_found_not_found_component__ = __webpack_require__("../../../../../src/app/containers/not-found/not-found.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__containers_cmps_cmps_other_component__ = __webpack_require__("../../../../../src/app/containers/cmps/cmps-other.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__containers_cmla_cmla_form_1_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-form-1.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__containers_cmla_cmla_form_2_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-form-2.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__containers_cmla_cmla_import_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-import.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__containers_cmla_cmla_bitumen_component__ = __webpack_require__("../../../../../src/app/containers/cmla/cmla-bitumen.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__containers_search_search_component__ = __webpack_require__("../../../../../src/app/containers/search/search.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });











var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_3__containers_think_think_component__["a" /* ThinkComponent */]
    },
    {
        path: 'search',
        component: __WEBPACK_IMPORTED_MODULE_10__containers_search_search_component__["a" /* SearchComponent */]
    },
    {
        path: 'cmps/:id/view',
        component: __WEBPACK_IMPORTED_MODULE_0__containers_cmps_cmps_dom_component__["a" /* CmpsDomComponent */],
    },
    {
        path: 'cmps/dom/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_0__containers_cmps_cmps_dom_component__["a" /* CmpsDomComponent */],
    },
    {
        path: 'cmps/dom',
        component: __WEBPACK_IMPORTED_MODULE_0__containers_cmps_cmps_dom_component__["a" /* CmpsDomComponent */],
    },
    {
        path: 'cmps/inter/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_1__containers_cmps_cmps_inter_component__["a" /* CmpsInterComponent */],
    },
    {
        path: 'cmps/inter',
        component: __WEBPACK_IMPORTED_MODULE_1__containers_cmps_cmps_inter_component__["a" /* CmpsInterComponent */],
    },
    {
        path: 'cmps/import/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_2__containers_cmps_cmps_import_component__["a" /* CmpsImportComponent */],
    },
    {
        path: 'cmps/import',
        component: __WEBPACK_IMPORTED_MODULE_2__containers_cmps_cmps_import_component__["a" /* CmpsImportComponent */],
    },
    {
        path: 'cmps/other/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_5__containers_cmps_cmps_other_component__["a" /* CmpsOtherComponent */],
    },
    {
        path: 'cmps/other',
        component: __WEBPACK_IMPORTED_MODULE_5__containers_cmps_cmps_other_component__["a" /* CmpsOtherComponent */],
    },
    {
        path: 'cmla/form-1',
        component: __WEBPACK_IMPORTED_MODULE_6__containers_cmla_cmla_form_1_component__["a" /* CmlaForm1Component */],
    },
    {
        path: 'cmla/form-1/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_6__containers_cmla_cmla_form_1_component__["a" /* CmlaForm1Component */],
    },
    {
        path: 'cmla/form-2',
        component: __WEBPACK_IMPORTED_MODULE_7__containers_cmla_cmla_form_2_component__["a" /* CmlaForm2Component */],
    },
    {
        path: 'cmla/form-2/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_7__containers_cmla_cmla_form_2_component__["a" /* CmlaForm2Component */],
    },
    {
        path: 'cmla/import',
        component: __WEBPACK_IMPORTED_MODULE_8__containers_cmla_cmla_import_component__["a" /* CmlaImportComponent */],
    },
    {
        path: 'cmla/import/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_8__containers_cmla_cmla_import_component__["a" /* CmlaImportComponent */],
    },
    {
        path: 'cmla/bitumen',
        component: __WEBPACK_IMPORTED_MODULE_9__containers_cmla_cmla_bitumen_component__["a" /* CmlaBitumenComponent */],
    },
    {
        path: 'cmla/bitumen/:id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_9__containers_cmla_cmla_bitumen_component__["a" /* CmlaBitumenComponent */],
    },
    // {
    //   path: 'demurrage',
    //   component: DemurrageFormComponent,
    // },
    // {
    //   path: 'demurrage/:id/:transaction_id/edit',
    //   component: DemurrageFormComponent,
    // },
    {
        path: '**',
        component: __WEBPACK_IMPORTED_MODULE_4__containers_not_found_not_found_component__["a" /* NotFoundComponent */]
    },
];
// export const routes: Routes = [].concat(
//   {
//     path: '',
//     component: ThinkComponent
//   },
//   cmps_routes,
//   {
//     path: '**',
//     component: NotFoundComponent
//   }
// );
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/services/domestic.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DomesticService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DomesticService = (function () {
    function DomesticService(http) {
        this.http = http;
        // http://localhost:50131/CPAIMVC/DomesticResource/GetButtons
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "DomesticResource";
    }
    DomesticService.prototype.getButtons = function () {
        return this.http
            .get(this.resource + "/GetButtons")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('suServer error'); });
    };
    return DomesticService;
}());
DomesticService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]) === "function" && _a || Object])
], DomesticService);

var _a;
//# sourceMappingURL=domestic.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__paf_service__ = __webpack_require__("../../../../../src/app/services/paf.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__site_service__ = __webpack_require__("../../../../../src/app/services/site.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loader_service__ = __webpack_require__("../../../../../src/app/services/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__domestic_service__ = __webpack_require__("../../../../../src/app/services/domestic.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__upload_service__ = __webpack_require__("../../../../../src/app/services/upload.service.ts");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__master_service__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_1__paf_service__["a"]; });
/* unused harmony reexport SiteService */
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_3__loader_service__["a"]; });
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_4__domestic_service__["a"]; });
/* unused harmony reexport UploadService */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return services; });






var services = [
    __WEBPACK_IMPORTED_MODULE_0__master_service__["a" /* MasterService */],
    __WEBPACK_IMPORTED_MODULE_1__paf_service__["a" /* PafService */],
    __WEBPACK_IMPORTED_MODULE_2__site_service__["a" /* SiteService */],
    __WEBPACK_IMPORTED_MODULE_3__loader_service__["a" /* LoaderService */],
    __WEBPACK_IMPORTED_MODULE_4__domestic_service__["a" /* DomesticService */],
    __WEBPACK_IMPORTED_MODULE_5__upload_service__["a" /* UploadService */],
];

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/services/loader.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoaderService = (function () {
    function LoaderService() {
        this.loaderStatus = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](false);
    }
    LoaderService.prototype.displayLoader = function (value) {
        this.loaderStatus.next(value);
    };
    LoaderService.prototype.display = function (value) {
        this.loaderStatus.next(value);
    };
    return LoaderService;
}());
LoaderService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], LoaderService);

//# sourceMappingURL=loader.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/master.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MasterService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MasterService = (function () {
    function MasterService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "ApprovalMaster";
    }
    MasterService.prototype.getList = function () {
        return this.http
            .get("" + this.resource)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
        // return this.http
        //   .get(`${this.resource}`)
        //   .map( response => response.json() )
        //   .catch((error:any) => Observable.throw('suServer error'));
    };
    MasterService.prototype.getFX = function (data) {
        return this.http
            .post(this.resource + "/Post_FX", {
            DATE_FROM: data.PBD_FX_DATE_FROM,
            DATE_TO: data.PBD_FX_DATE_TO,
        })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    MasterService.prototype.getHSFO = function (data) {
        return this.http
            .post(this.resource + "/Post_HSFO", {
            DATE_FROM: data.PBD_HSFO_DATE_FROM,
            DATE_TO: data.PBD_HSFO_DATE_TO,
        })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    MasterService.prototype.get = function (id) {
        return this.http
            .get(this.resource + "/" + id)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('suServer error'); });
    };
    MasterService.prototype.update = function (id, data) {
        return this.http
            .put(this.resource + "/" + id, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    MasterService.prototype.post = function (data) {
        return this.http
            .post("" + this.resource, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    MasterService.prototype.put = function (data) {
        return this.http
            .put("" + this.resource, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    return MasterService;
}());
MasterService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]) === "function" && _a || Object])
], MasterService);

var _a;
//# sourceMappingURL=master.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/paf.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common__ = __webpack_require__("../../../../../src/app/common/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PafService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PafService = (function () {
    function PafService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "PafService";
    }
    PafService.prototype.getList = function () {
        return this.http
            .get(this.resource + "/GetListTransaction")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.get = function (id) {
        return this.http
            .get(this.resource + "/" + id)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.update = function (id, data) {
        return this.http
            .put(this.resource + "/" + id, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.post = function (data) {
        return this.http
            .post(this.resource + "/DomesticSaleCreate", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.put = function (data) {
        return this.http
            .put("" + this.resource, data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.SearchTransaction = function (data) {
        return this.http
            .post(this.resource + "/SearchTransaction", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.GetSearchCriteria = function () {
        return this.http
            .get(this.resource + "/GetSearchCriteria")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.GetByTransactionId = function (TRAN_ID) {
        return this.http
            .post(this.resource + "/GetByTransactionId", {
            TRAN_ID: TRAN_ID,
        })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.GetButtons = function () {
        return this.http
            .get(this.resource + "/GetButtons")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.SaveDraft = function (data) {
        data = __WEBPACK_IMPORTED_MODULE_2__common__["a" /* Utility */].AdjustPAFModel(data);
        return this.http
            .post(this.resource + "/SaveDraft", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.Submit = function (data) {
        return this.http
            .post(this.resource + "/Submit", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.Verify = function (data) {
        return this.http
            .post(this.resource + "/Verify", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.Endorse = function (data) {
        return this.http
            .post(this.resource + "/Endorse", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.Approve = function (data) {
        return this.http
            .post(this.resource + "/Approve", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.GeneratePDF = function (data) {
        return this.http
            .get(this.resource + "/GeneratePDF/" + data.TRAN_ID)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.Reject = function (data) {
        return this.http
            .post(this.resource + "/Reject", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.Cancel = function (data) {
        return this.http
            .post(this.resource + "/Cancel", data)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    PafService.prototype.playgroundGet = function () {
        return this.http
            .get('../../Content/paf/assets/data/inter.json')
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    return PafService;
}());
PafService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* Http */]) === "function" && _a || Object])
], PafService);

var _a;
//# sourceMappingURL=paf.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/site.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SiteService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SiteService = (function () {
    function SiteService() {
        this.subHeader = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        this.subHeaderBack = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        this.breadcrumbs = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
    }
    SiteService.prototype.set = function (key, val) {
        this[key].next(val);
    };
    return SiteService;
}());
SiteService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], SiteService);

//# sourceMappingURL=site.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/upload.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__common_constant__ = __webpack_require__("../../../../../src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UploadService = (function () {
    function UploadService(http) {
        this.http = http;
        this.resource = __WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "PafService";
    }
    UploadService.prototype.postSingle = function (file, type_string) {
        var formData = new FormData();
        formData.append('file', file, file.name);
        // let headers = new Headers()
        //headers.append('Content-Type', 'json');
        //headers.append('Accept', 'application/json');
        // let options = new RequestOptions({ headers: headers });
        return this.http
            .post(this.resource + "/FileUpload?Type=PAF|F10000044&PAT_TYPE=" + type_string, formData)
            .map(function (res) { return res.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); });
    };
    // public postSingle(file: File) : Observable<any> {
    //   let formData: FormData = new FormData();
    //   formData.append('uploadFile', file, file.name);
    //   // let headers = new Headers()
    //   //headers.append('Content-Type', 'json');
    //   //headers.append('Accept', 'application/json');
    //   // let options = new RequestOptions({ headers: headers });
    //   return this.http
    //     .post(`${BASE_API}api/Upload/anywhere`, formData)
    //     .map(res => res.json())
    //     .catch(error => Observable.throw(error));
    // }
    UploadService.prototype.post = function (event) {
        var fileList = event.target.files;
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData = new FormData();
            formData.append('uploadFile', file, file.name);
            // let headers = new Headers()
            //headers.append('Content-Type', 'json');
            //headers.append('Accept', 'application/json');
            // let options = new RequestOptions({ headers: headers });
            return this.http
                .post(__WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "api/Upload", formData)
                .map(function (res) { return res.json(); })
                .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(error); });
        }
    };
    UploadService.prototype.getList = function () {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "api/blob")
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    UploadService.prototype.get = function (id) {
        return this.http
            .get(__WEBPACK_IMPORTED_MODULE_1__common_constant__["a" /* BASE_API */] + "api/blob/" + id)
            .map(function (response) { return response.json(); })
            .catch(function (error) { return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw('Server error'); });
    };
    return UploadService;
}());
UploadService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["e" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]) === "function" && _a || Object])
], UploadService);

var _a;
//# sourceMappingURL=upload.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map