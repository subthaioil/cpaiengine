webpackJsonp(["crude-sale-approval.module"],{

/***/ "./src/app/common/cds-helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CDSHelper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);

var CDSHelper = /** @class */ (function () {
    function CDSHelper() {
    }
    CDSHelper.updatedModel = function (model, is_duplicate) {
        console.log("is_duplicate: " + is_duplicate);
        if (is_duplicate) {
            delete model.REQ_TRAN_ID;
            delete model.TRAN_ID;
            delete model.CDA_ROW_ID;
            delete model.CDA_FORM_ID;
            delete model.CDA_STATUS;
            delete model.CDA_REASON;
        }
        return model;
    };
    CDSHelper.updateCrudePurchaseDetail = function (model) {
        var first_crude = model.CDS_CRUDE_PURCHASE_DETAIL && model.CDS_CRUDE_PURCHASE_DETAIL.length > 0 && model.CDS_CRUDE_PURCHASE_DETAIL[0];
        model.CDS_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_0_lodash__["map"](model.CDS_BIDDING_ITEMS, function (item) {
            item.CBI_FEEDSTOCK = first_crude.FEEDSTOCK;
            item.CBI_FK_MATERIAL = first_crude.MET_NUM;
            if (!item.CBI_FK_MATERIAL) {
                item.CBI_MATERIAL_NAME_OTHER = first_crude.CRUDE_NAME;
                item.CBI_QUANTITY_UNIT = first_crude.QUANTITY_UNIT;
                item.CBI_MARGIN_UNIT = first_crude.MARGIN_VS_LP_UNIT;
            }
            return item;
        });
        model.CDS_PROPOSAL_ITEMS = __WEBPACK_IMPORTED_MODULE_0_lodash__["map"](model.CDS_PROPOSAL_ITEMS, function (item) {
            item.CSI_FK_MATERIAL = first_crude.MET_NUM;
            return item;
        });
        if (model.CDS_CRUDE_PURCHASE_DETAIL.length) {
            var CDA_AVERAGE_COST = (__WEBPACK_IMPORTED_MODULE_0_lodash__["reduce"](model.CDS_CRUDE_PURCHASE_DETAIL, function (s, e) {
                return s + (+e.PURCHASING_PRICE || 0);
            }, 0)) / (model.CDS_CRUDE_PURCHASE_DETAIL.length);
            console.log(CDA_AVERAGE_COST);
            model.CDA_AVERAGE_COST = __WEBPACK_IMPORTED_MODULE_0_lodash__["round"](CDA_AVERAGE_COST, 4);
        }
        model = CDSHelper.updateBiddingItem(model);
        return model;
    };
    CDSHelper.updateBiddingItem = function (model) {
        var first_row = model.CDS_BIDDING_ITEMS && model.CDS_BIDDING_ITEMS.length > 0 && model.CDS_BIDDING_ITEMS[0];
        for (var i = 0; i < model.CDS_BIDDING_ITEMS.length; i++) {
            model.CDS_BIDDING_ITEMS[i].CBI_FEEDSTOCK = first_row.CBI_FEEDSTOCK;
            model.CDS_BIDDING_ITEMS[i].CBI_FK_MATERIAL = first_row.CBI_FK_MATERIAL;
            if (!model.CDS_BIDDING_ITEMS[i].CBI_FK_MATERIAL) {
                model.CDS_BIDDING_ITEMS[i].CBI_MATERIAL_NAME_OTHER = first_row.CBI_MATERIAL_NAME_OTHER;
            }
            model.CDS_PROPOSAL_ITEMS[i].CSI_FK_CUSTOMER = model.CDS_BIDDING_ITEMS[i].CBI_FK_CUSTOMER;
            model.CDS_PROPOSAL_ITEMS[i].CSI_FK_MATERIAL = model.CDS_BIDDING_ITEMS[i].CBI_FK_MATERIAL;
            var rounds = __WEBPACK_IMPORTED_MODULE_0_lodash__["filter"](model.CDS_BIDDING_ITEMS[i].CDS_ROUND, function (e) {
                return e.CRN_VALUE;
            });
            var last_round = __WEBPACK_IMPORTED_MODULE_0_lodash__["last"](rounds);
            var last_round_value = (last_round && last_round.CRN_VALUE) || 0;
            model.CDS_BIDDING_ITEMS[i].CBI_MARGIN = __WEBPACK_IMPORTED_MODULE_0_lodash__["round"](last_round_value - +model.CDA_AVERAGE_COST, 4);
        }
        // model.CDS_BIDDING_ITEMS = _.map(model.CDS_BIDDING_ITEMS, (item: CDS_BIDDING_ITEMS) => {
        //
        //   const rounds = _.filter(item.CDS_ROUND, (e: CDS_ROUND) => {
        //     return e.CRN_VALUE;
        //   });
        //   const last_round: CDS_ROUND = _.last(rounds);
        //   const last_round_value: number = (last_round && last_round.CRN_VALUE) || 0;
        //   item.CBI_MARGIN = _.round( last_round_value - +model.CDA_AVERAGE_COST, 4);
        //   return item;
        // });
        return model;
    };
    return CDSHelper;
}());



/***/ }),

/***/ "./src/app/crude-sale-approval/components/cdp-reference/cdp-reference.component.html":
/***/ (function(module, exports) {

module.exports = "<button class=\"btn btn-primary\" [disabled]=\"disabled\" (click)=\"modal.show()\">Add Reference</button>\n<!--<button class=\"btn btn-primary btn-sm text-light\" [disabled]=\"disabled\" (click)=\"modal.show()\">Add Reference</button>-->\n<div bsModal #modal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h3 class=\"modal-title pull-left\">Crude Purchase Reference</h3>\n        <div class=\"exit\" (click)=\"modal.hide()\" role=\"button\" tabindex=\"0\"></div>\n        <!-- <button type=\"button\" class=\"close pull-right\" (click)=\"modal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> -->\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"row\">\n          <div class=\"col-lg-10 col-lg-offset-1\">\n            <div class=\"form-group\">\n              <div class=\"col-md-4\">\n                <label class=\"col-md-3 control-label\">Purchase No.</label>\n                <div class=\"col-md-7\">\n                  <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.PURCHASE_NO\">\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <label class=\"col-md-3 control-label\">Purchase Date.</label>\n                <div class=\"col-md-7\">\n                  <ptx-input-date-range\n                      [disabled]=\"false\"\n                      [start_date]=\"from\"\n                      [end_date]=\"to\"\n                      (modelChange)=\"model.PURCHASE_DATE_FROM = $event.start_date;model.PURCHASE_DATE_TO = $event.end_date;\"\n                  >\n                  </ptx-input-date-range>\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <label class=\"col-md-3 control-label\">Supplier</label>\n                <div class=\"col-md-7\">\n                  <ptx-input-suggestion-ui\n                    [model]=\"model.SUPPLIER\"\n                    [data_list]=\"master.SUPPLIER\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.SUPPLIER = $event;\"\n                  >\n                  </ptx-input-suggestion-ui>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <div class=\"col-md-4\">\n                <label class=\"col-md-3 control-label\">Feedstock</label>\n                <div class=\"col-md-7\">\n                  <select class=\"form-control\" [(ngModel)]=\"model.FEEDSTOCK\" [disabled]=\"model.IS_DISABLED\">\n                    <option value=\"\"> --- select All --- </option>\n                    <option [value]=\"elem.ID\" *ngFor=\"let elem of master.FEED_STOCK\">{{ elem.VALUE }}</option>\n                  </select>\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <label class=\"col-md-3 control-label\">Name</label>\n                <div class=\"col-md-7\">\n                  <ptx-input-suggestion-ui\n                    *ngIf=\"model.FEEDSTOCK == ''\"\n                    [model]=\"model.NAME\"\n                    [data_list]=\"master.MATERIALS\"\n                    [is_disabled]=\"true\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.NAME = $event;\"\n                  >\n                  </ptx-input-suggestion-ui>\n                  <ptx-input-suggestion-ui\n                    *ngIf=\"model.FEEDSTOCK == 'CRUDE'\"\n                    [model]=\"model.NAME\"\n                    [data_list]=\"master.MATERIALS\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.NAME = $event;\"\n                  >\n                  </ptx-input-suggestion-ui>\n                  <ptx-input-suggestion-ui\n                    *ngIf=\"model.FEEDSTOCK == 'LR'\"\n                    [model]=\"model.NAME\"\n                    [data_list]=\"master.LR\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.NAME = $event;\"\n                  >\n                  </ptx-input-suggestion-ui>\n                  <ptx-input-suggestion-ui\n                    *ngIf=\"model.FEEDSTOCK == 'VGO'\"\n                    [model]=\"model.NAME\"\n                    [data_list]=\"master.VGO\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [show_field]=\"'VALUE'\"\n                    select_field=\"ID\"\n                    (ModelChanged)=\"model.NAME = $event;\"\n                  >\n                  </ptx-input-suggestion-ui>\n                </div>\n              </div>\n            </div>\n            <div class=\"text-center\" id=\"buttonDiv\">\n              <button class=\"btn btn-success\" (click)=\"DoSearch()\"> Search </button>\n            </div>\n          </div>\n        </div>\n        <br />\n        <div class=\"table-responsive\">\n          <table id=\"ptxDataTable\" class=\"tble table-hover table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">\n          </table>\n        </div>\n        <br />\n        <div class=\"row\">\n          <div class=\"col-lg-10 col-lg-offset-1\">\n            <div class=\"text-center\" id=\"buttonDiv\">\n              <button class=\"btn btn-default\" (click)=\"Done()\"> Done </button>\n            </div>\n          </div>\n        </div>\n      </div>\n      <!--<div class=\"modal-footer\">-->\n      <!--n-->\n      <!--</div>-->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/crude-sale-approval/components/cdp-reference/cdp-reference.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CdpReferenceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_cdp_data_reference_service__ = __webpack_require__("./src/app/services/cdp-data-reference.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_cds_master__ = __webpack_require__("./src/app/models/cds-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CdpReferenceComponent = /** @class */ (function () {
    function CdpReferenceComponent(el, cdpDataReferenceService) {
        this.el = el;
        this.cdpDataReferenceService = cdpDataReferenceService;
        this.config = {
            animated: true,
            keyboard: true,
            backdrop: true,
            ignoreBackdropClick: true
        };
        this.model = {
            FEEDSTOCK: '',
            PURCHASE_DATE_FROM: null,
            PURCHASE_DATE_TO: null,
        };
        this.user_group = "";
        this.disabled = false;
        this.master = new __WEBPACK_IMPORTED_MODULE_3__models_cds_master__["a" /* CDS_MASTER */]();
        this.onSelectData = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    CdpReferenceComponent.prototype.ngOnInit = function () {
        this.updateTable();
    };
    CdpReferenceComponent.prototype.DoSearch = function () {
        var context = this;
        var model = context.model;
        console.log(model);
        // // https://www.gyrocode.com/projects/jquery-datatables-checkboxes/
        // var dialog = bootbox.dialog({
        //   title: 'Processing.....',
        //   message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
        // });
        // dialog.init(function(){
        //     context.cdpDataReferenceService.search(model).subscribe(
        //       (res) => {
        //         context.data_set = res;
        //         context.updateTable();
        //         // this.table.draw();
        //         console.log(context.data_set);
        //         dialog.modal('hide');
        //       },
        //       (error) => {
        //         console.log(error);
        //         dialog.modal('hide');
        //       }
        //     );
        // });
        context.cdpDataReferenceService.search(model).subscribe(function (res) {
            context.data_set = res;
            context.updateTable();
            // this.table.draw();
            console.log(context.data_set);
        }, function (error) {
            console.log(error);
        });
    };
    CdpReferenceComponent.prototype.updateTable = function () {
        var context = this;
        if (this.table) {
            this.table.destroy();
        }
        this.table = $(context.el.nativeElement).find("#ptxDataTable").DataTable({
            "data": context.data_set,
            "ordering": false,
            "bSort": true,
            "dom": '<"toolbar">rtip',
            "columnDefs": [
                { "className": "dt-center", "targets": "_all" },
                {
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }
                //  {
                //    'targets': 0,
                //    'render': function(data, type, row, meta){
                //       if(type === 'display'){
                //          data = '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                //       }
                //
                //       return data;
                //    },
                //    'checkboxes': {
                //       'selectRow': true,
                //       'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                //    }
                // }
            ],
            'select': 'multi',
            // select: {
            //     style:    'multi',
            //     selector: 'td:first-child'
            // },
            "columns": context.MappingColumn(),
        });
        // $(context.el.nativeElement).find('#ptxDataTable tbody').on('click', 'tr', function (event) {
        //   const select_data = context.table.row(this).data();
        //   context.onSelectData.emit(select_data);
        //   context.modal.hide();
        // });
    };
    CdpReferenceComponent.prototype.Done = function () {
        var context = this;
        var rows_selected = this.table.column(0).checkboxes.selected();
        console.log(rows_selected);
        if (rows_selected) {
            var select_list = __WEBPACK_IMPORTED_MODULE_4_lodash__["filter"](context.data_set, function (o) {
                return rows_selected.indexOf(o.CPD_FK_CDP_DATA) != -1;
            });
            context.onSelectData.emit(select_list);
        }
        context.data_set = [];
        context.updateTable();
        context.modal.hide();
    };
    CdpReferenceComponent.prototype.MappingColumn = function () {
        return [
            {
                'title': '',
                'data': 'CPD_FK_CDP_DATA'
            },
            {
                'title': 'Date',
                'data': 'SHOW_DOC_DATE'
            },
            {
                'title': 'Purchase No.',
                'data': 'PURCHASE_NO'
            },
            {
                'title': 'Feedstock',
                'data': 'FEEDSTOCK'
            },
            {
                'title': 'Name',
                'data': 'CRUDE_NAME'
            },
            {
                'title': 'Supplier',
                'data': 'SUPPLIER'
            },
            {
                'title': 'Quantity',
                'data': 'SHOW_QUANTITY'
            },
            {
                'title': 'Margin',
                'data': 'MARGIN'
            },
            {
                'title': 'Created By',
                'data': 'CREATED_BY'
            }
        ];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */])
    ], CdpReferenceComponent.prototype, "modal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CdpReferenceComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], CdpReferenceComponent.prototype, "user_group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], CdpReferenceComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], CdpReferenceComponent.prototype, "data_set", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__models_cds_master__["a" /* CDS_MASTER */])
    ], CdpReferenceComponent.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CdpReferenceComponent.prototype, "onSelectData", void 0);
    CdpReferenceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-cdp-reference',
            template: __webpack_require__("./src/app/crude-sale-approval/components/cdp-reference/cdp-reference.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_2__services_cdp_data_reference_service__["a" /* CdpDataReferenceService */]])
    ], CdpReferenceComponent);
    return CdpReferenceComponent;
}());



/***/ }),

/***/ "./src/app/crude-sale-approval/components/crude-purchase-table/crude-purchase-table.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"table-responsive\">\n  <table id=\"\" class=\"table table-bordered plutonyx-table\">\n  <!-- <table class=\"table table-bordered autoscroll text-center\"> -->\n    <thead>\n      <tr>\n        <th rowspan=\"2\">Purchase No.</th>\n        <th rowspan=\"2\">Feedstock</th>\n        <th rowspan=\"2\">Name</th>\n        <th rowspan=\"2\">Origin</th>\n        <th rowspan=\"2\">Supplier</th>\n        <th rowspan=\"2\">Contact Person</th>\n        <th rowspan=\"2\">Quantity ({{ ShowQuantityUnit() }})</th>\n        <th colspan=\"5\">Offer price or premium ({{ ShowPricePerQtty() }})</th>\n        <th rowspan=\"2\">Rank/ Round</th>\n        <th rowspan=\"2\">Margin vs LP ({{ ShowPricePerQtty() }})</th>\n        <th rowspan=\"2\">More Detail</th>\n        <th rowspan=\"2\">Delete</th>\n      </tr>\n      <tr>\n        <th>Incoterns</th>\n        <th>Purchasing Price</th>\n        <th>Market/ Source</th>\n        <th>LP Max Purchase Price</th>\n        <th>Benchmark price</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngIf=\"!data_list || data_list.length == 0\">\n        <td colspan=\"16\" class=\"text-center\">\n          No Reference\n        </td>\n      </tr>\n      <tr *ngFor=\"let elem of data_list;let index = index\">\n        <td class=\"min-150\">\n          <input class=\"form-control\" [ngModel]=\"elem.PURCHASE_NO\" type=\"text\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" [ngModel]=\"elem.FEEDSTOCK\" type=\"text\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-150\">\n          <input class=\"form-control\" [ngModel]=\"elem.CRUDE_NAME\" type=\"text\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" [ngModel]=\"elem.ORIGIN\" type=\"text\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-150\">\n          <input class=\"form-control\" [ngModel]=\"elem.SUPPLIER\" type=\"text\" [disabled]=\"true\">\n        </td>\n\n        <td class=\"min-150\">\n          <input class=\"form-control\" [ngModel]=\"elem.CONTACT_PERSON\" type=\"text\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" [ngModel]=\"elem.QUANTITY_MIN\" style=\"margin-bottom: 5px;\" type=\"text\" [disabled]=\"true\">\n          <input class=\"form-control\" [ngModel]=\"elem.QUANTITY_MAX\" type=\"text\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.INCOTERMS\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.PURCHASING_PRICE\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.MARKET_SOURCE\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.LP_MAX_PURCHASE_PRICE\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.BENCHMARK_PRICE\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.RANK_ROUND\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <input class=\"form-control\" type=\"text\" [ngModel]=\"elem.MARGIN_VS_LP\" [disabled]=\"true\">\n        </td>\n        <td class=\"min-100\">\n          <a [href]=\"elem.URL\" target=\"_blank\" style=\"color:#0a7ab1; font-size: 16px;\">\n            <i class=\"glyphicon glyphicon-file\"></i>\n          </a>\n        </td>\n        <td class=\"min-100\">\n          <a href=\"javascript:void(0)\" (click)=\"DeleteRow(elem)\" style=\"color:#0a7ab1; font-size: 16px;\">\n            <i class=\"glyphicon glyphicon-trash\"></i>\n          </a>\n        </td>\n      </tr>\n      <tr *ngIf=\"data_list && data_list.length > 0\">\n        <td colspan=\"7\"></td>\n        <td> Average </td>\n        <td> {{ average }}</td>\n        <td colspan=\"7\"></td>\n      </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "./src/app/crude-sale-approval/components/crude-purchase-table/crude-purchase-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CrudePurchaseTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_cds_master__ = __webpack_require__("./src/app/models/cds-master.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CrudePurchaseTableComponent = /** @class */ (function () {
    function CrudePurchaseTableComponent() {
        this.data_list = new Array();
        this.average = 0;
        this.need_delete = false;
        this.master = new __WEBPACK_IMPORTED_MODULE_1__models_cds_master__["a" /* CDS_MASTER */]();
        this.OnDelete = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.OnAverageChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    CrudePurchaseTableComponent.prototype.ngOnInit = function () {
    };
    CrudePurchaseTableComponent.prototype.DeleteRow = function (row) {
        this.OnDelete.emit(row);
    };
    CrudePurchaseTableComponent.prototype.ShowQuantityUnit = function () {
        var row = this.data_list && this.data_list.length > 0 && this.data_list[0];
        return (row && row.QUANTITY_UNIT) || '';
    };
    CrudePurchaseTableComponent.prototype.ShowPricePerQtty = function () {
        var row = this.data_list && this.data_list.length > 0 && this.data_list[0];
        return (row && row.MARGIN_VS_LP_UNIT) || '';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], CrudePurchaseTableComponent.prototype, "data_list", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], CrudePurchaseTableComponent.prototype, "average", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], CrudePurchaseTableComponent.prototype, "need_delete", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_cds_master__["a" /* CDS_MASTER */])
    ], CrudePurchaseTableComponent.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CrudePurchaseTableComponent.prototype, "OnDelete", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CrudePurchaseTableComponent.prototype, "OnAverageChange", void 0);
    CrudePurchaseTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-crude-purchase-table',
            template: __webpack_require__("./src/app/crude-sale-approval/components/crude-purchase-table/crude-purchase-table.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], CrudePurchaseTableComponent);
    return CrudePurchaseTableComponent;
}());



/***/ }),

/***/ "./src/app/crude-sale-approval/components/crude-sale-approval/crude-sale-approval.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<pre>{{model|json}}</pre>-->\n<div class=\"wrapper-detail\">\n  <style>.radio label:before{\n    top: -2.5px;\n    bottom: 0;\n  }</style>\n    <h2 class=\"first-title\">\n        <span class=\"note\">Crude Sale </span>\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n        <div class=\"row\">\n            <div class=\"col-xs-12 col-sm-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-4 control-label\">Request by</label>\n                    <div class=\"col-xs-7 pr-10\">\n                        <input class=\"form-control\"\n                               type=\"text\"\n                               [ngModel]=\"model.CDA_REQUEST_BY\"\n                               [disabled]=\"true\">\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-12 col-sm-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-4 control-label\">Sale No.</label>\n                    <div class=\"col-xs-7 pr-10\">\n                        <input class=\"form-control\"\n                               type=\"text\"\n                               [ngModel]=\"model.CDA_FORM_ID\"\n                               [disabled]=\"true\">\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-12 col-sm-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-4 control-label\">Date Sale  <span style=\"color:red;\">*</span></label>\n                    <div class=\"col-xs-7 pr-10\">\n                        <ptx-input-date\n                          [needtime]=\"true\"\n                          [date]=\"model.CDA_DOC_DATE\"\n                          (modelChange)=\"model.CDA_DOC_DATE = $event;\"\n                          [disabled]=\"model.IS_DISABLED\"\n                        ></ptx-input-date>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-12 col-sm-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-4 control-label\">TPC Plan <span style=\"color:red;\">*</span></label>\n                    <div class=\"col-xs-7 pr-10\">\n                        <ptx-input-month-year\n                          [model]=\"model.CDA_TPC_PLAN\"\n                          (modelChange)=\"model.CDA_TPC_PLAN = $event\"\n                        ></ptx-input-month-year>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-xs-12 col-sm-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-4 control-label\">Sale type <span style=\"color:red;\">*</span></label>\n                    <div class=\"col-xs-7 pr-10\">\n                        <select class=\"form-control\" [(ngModel)]=\"model.CDA_SALE_TYPE\">\n                            <option [value]=\"elem.ID\" *ngFor=\"let elem of master.SALE_TYPE\">{{ elem.VALUE }}</option>\n                        </select>\n                    </div>\n                </div>\n            </div>\n            <!--<div class=\"col-xs-12 col-sm-6\">-->\n                <!--<div class=\"form-group\">-->\n                    <!--<label class=\"col-xs-4 control-label\">Origin</label>-->\n                    <!--<div class=\"col-xs-7 pr-10\">-->\n                        <!--<select class=\"form-control\" [disabled]=\"true\">-->\n                        <!--<option value=\"\">MALAYSIA</option>-->\n                      <!--</select>-->\n                    <!--</div>-->\n                <!--</div>-->\n            <!--</div>-->\n            <div class=\"col-xs-12 col-sm-6\">\n                <div class=\"form-group\">\n                    <label class=\"col-xs-4 control-label\">Sale method <span style=\"color:red;\">*</span></label>\n                    <div class=\"col-xs-7 pr-10\">\n                      <select class=\"form-control\" [(ngModel)]=\"model.CDA_SALE_METHOD\">\n                        <option [value]=\"elem.ID\" *ngFor=\"let elem of master.SALE_METHOD\">{{ elem.VALUE }}</option>\n                      </select>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-xs-12 col-sm-6\">\n            </div>\n            <div class=\"col-xs-12 col-sm-6\" *ngIf=\"false\">\n                <div class=\"form-group\" *ngIf=\"false\">\n                    <label class=\"col-xs-4 control-label\">Optimization <span style=\"color:red;\">*</span></label>\n                    <div class=\"col-xs-7 pr-10\">\n                        <ptx-input-ratio\n                          [model_from]=\"model.CDA_OPTIMIZATION_SALE\"\n                          [model_to]=\"model.CDA_OPTIMIZATION_PURCHASE\"\n                          (modelChange)=\"model.CDA_OPTIMIZATION_SALE = $event.from;model.CDA_OPTIMIZATION_PURCHASE = $event.to\"\n                        ></ptx-input-ratio>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"form-horizontal mb-30\">\n        <div class=\"row\">\n            <h2 class=\"second-title\">\n                Brief Market Situation / Reason for selling\n            </h2>\n            <div class=\"form-horizontal mb-30\">\n                <ptx-freetext *ngIf=\"true\"\n                              [disabled]=\"model.IS_DISABLED_NOTE\"\n                              [elementId]=\"'CDA_BRIEF'\"\n                              [model]=\"model.CDA_BRIEF\"\n                              (HandleStateChange)=\"model.CDA_BRIEF = $event\"\n                >\n                </ptx-freetext>\n            </div>\n            <div class=\"form-horizontal mb-30 pb-30\">\n                <ptx-file-upload\n                  btnClass=\"text-left\"\n                  [model]=\"model.CDS_ATTACH_FILE\"\n                  [is_disabled]=\"model.IS_DISABLED\"\n                  [need_caption]=\"true\"\n                  [desc]=\"'Image Files, PDF'\"\n                  filterBy=\"BMS\"\n                  (handleModelChanged)=\"model.CDS_ATTACH_FILE = $event\"\n                  [SOURCE_FROM]=\"'CDS'\"\n\n                  [PATH_DEF] = \"'CAT_PATH'\"\n                  [INFO_DEF] = \"'CAT_INFO'\"\n                  [CAPTION_DEF] = \"'CAT_CAPTION'\"\n                  [FILTER_KEY_DEF] = \"'CAT_TYPE'\"\n                >\n                </ptx-file-upload>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <h2 class=\"second-title\">\n          Crude Purchase Detalil (For Reference)\n        </h2>\n        <div class=\"form-horizontal mb-30\">\n          <ptx-crude-purchase-table\n            [master]=\"master\"\n            [data_list]=\"model.CDS_CRUDE_PURCHASE_DETAIL\"\n            [average]=\"model.CDA_AVERAGE_COST\"\n            [need_delete]=\"true\"\n            (OnDelete)=\"OnDeleteCDPData($event)\"\n          ></ptx-crude-purchase-table>\n          <div class=\"row\">\n            <ptx-cdp-reference\n              [master]=\"master\"\n              [disabled]=\"model.IS_DISABLED\"\n              (onSelectData)=\"OnCDPSelectData($event)\"\n            ></ptx-cdp-reference>\n            <!--<button class=\"btn btn-primary\">Add Reference</button>-->\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"form-horizontal mb-30\">\n        <div class=\"row\">\n            <h2 class=\"second-title\">\n                Crude Sale Summary\n            </h2>\n            <div class=\"form-horizontal mb-30\">\n          <!--<div class=\"form-horizontal mb-30\">-->\n                <!--<table class=\"table table-bordered autoscroll text-center\">-->\n                <div class=\"table-responsive\">\n                    <table id=\"\" class=\"table table-bordered plutonyx-table\">\n                        <thead>\n                            <tr>\n                                <th rowspan=\"2\">Feedstock</th>\n                                <th rowspan=\"2\">Name</th>\n                                <th rowspan=\"2\">Customer</th>\n                                <th rowspan=\"2\">Contact Person</th>\n                                <th rowspan=\"2\">Quantity ({{ ShowQuantityUnit() }})</th>\n                                <th [colSpan]=\"(3 + GetRoundLength())\">Offer price or premium ({{ ShowPricePerQtty() }})</th>\n                                <th rowspan=\"2\">Margin vs Purchasing Price ({{ ShowPricePerQtty() }})</th>\n                                <th rowspan=\"2\">Delete</th>\n                            </tr>\n                            <tr>\n                                <th>Incoterm</th>\n                                <th *ngFor=\"let e of GetRound();let j = index\">\n                                  {{ GetRoundName((j+1)) }}\n                                  <a *ngIf=\"j !== 0\" href=\"javascript:void(0)\" (click)=\"RemoveRound(j)\" style=\"color:#0a7ab1; font-size: 16px;\">\n                                    <i class=\"glyphicon glyphicon-trash\"></i>\n                                  </a>\n                                </th>\n                                <th>Market/ Source</th>\n                                <th>Benchmark price</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <ng-template ngFor let-elem [ngForOf]=\"model.CDS_BIDDING_ITEMS\" let-index=\"index\">\n                                <tr>\n                                  <td class=\"min-150\">\n                                    <select class=\"form-control\"\n                                            [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_FEEDSTOCK\"\n                                            [disabled]=\"HasReferrence() || model.IS_DISABLED\"\n                                            (ngModelChange)=\"UpdateBiddingData()\"\n                                    >\n                                      <option [value]=\"elem.ID\" *ngFor=\"let elem of master.FEED_STOCK\">{{ elem.VALUE }}</option>\n                                    </select>\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <ng-template [ngIf]=\"model.CDS_BIDDING_ITEMS[index].CBI_FEEDSTOCK === 'CRUDE'\">\n                                      <ptx-input-suggestion-ui\n                                        [model]=\"model.CDS_BIDDING_ITEMS[index].CBI_FK_MATERIAL\"\n                                        [data_list]=\"master.MATERIALS\"\n                                        [is_disabled]=\"HasReferrence() || model.IS_DISABLED\"\n                                        [show_field]=\"'VALUE'\"\n                                        select_field=\"ID\"\n                                        (ModelChanged)=\"model.CDS_BIDDING_ITEMS[index].CBI_FK_MATERIAL = $event;UpdateBiddingData()\"\n                                      >\n                                      </ptx-input-suggestion-ui>\n                                    </ng-template>\n                                    <ng-template [ngIf]=\"model.CDS_BIDDING_ITEMS[index].CBI_FEEDSTOCK !== 'CRUDE'\">\n                                      <input class=\"form-control\"\n                                             type=\"text\"\n                                             [disabled]=\"HasReferrence() || model.IS_DISABLED\"\n                                             [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_MATERIAL_NAME_OTHER\"\n                                             (ngModelChange)=\"UpdateBiddingData()\"\n                                      >\n                                    </ng-template>\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <ptx-input-suggestion-ui\n                                      [model]=\"model.CDS_BIDDING_ITEMS[index].CBI_FK_CUSTOMER\"\n                                      [data_list]=\"master.CUSTOMERS\"\n                                      [is_disabled]=\"model.IS_DISABLED\"\n                                      [show_field]=\"'VALUE'\"\n                                      select_field=\"ID\"\n                                      (ModelChanged)=\"model.CDS_BIDDING_ITEMS[index].CBI_FK_CUSTOMER = $event;UpdateBiddingData()\"\n                                    >\n                                    </ptx-input-suggestion-ui>\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <input\n                                      type=\"text\"\n                                      class=\"form-control\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_CONTACT_PERSON\"\n                                      (ngModelChange)=\"UpdateBiddingData()\"\n                                    />\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <!--<input class=\"form-control\" style=\"margin-bottom: 5px\" type=\"text\">-->\n                                    <input\n                                      type=\"text\"\n                                      class=\"form-control\"\n                                      style=\"margin-bottom: 5px\"\n                                      ptxRestrictNumber\n                                      [fraction]=\"4\"\n                                      [padding]=\"0\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_QUANTITY_MIN\"\n                                      (ngModelChange)=\"UpdateBiddingData()\"\n                                    />\n                                    <input\n                                      type=\"text\"\n                                      class=\"form-control\"\n                                      ptxRestrictNumber\n                                      [fraction]=\"4\"\n                                      [padding]=\"0\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_QUANTITY_MAX\"\n                                      (ngModelChange)=\"UpdateBiddingData()\"\n                                    />\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <select class=\"form-control\"\n                                            [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_INCOTERM\"\n                                            (ngModelChange)=\"UpdateBiddingData()\"\n                                            [disabled]=\"model.IS_DISABLED\"\n                                    >\n                                      <option [value]=\"elem.ID\" *ngFor=\"let elem of master.INCOTERMS\">{{ elem.VALUE }}</option>\n                                    </select>\n                                  </td>\n                                  <td class=\"min-150\" *ngFor=\"let e of model.CDS_BIDDING_ITEMS[index].CDS_ROUND;let j = index\">\n                                    <input\n                                      type=\"text\"\n                                      class=\"form-control\"\n                                      ptxRestrictNumber\n                                      [fraction]=\"4\"\n                                      [padding]=\"0\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CDS_ROUND[j].CRN_VALUE\"\n                                      (ngModelChange)=\"UpdateBiddingData()\"\n                                    />\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <input\n                                      type=\"text\"\n                                      class=\"form-control\"\n                                      ptxRestrictNumber\n                                      [fraction]=\"4\"\n                                      [padding]=\"0\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                                      [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_MARKET_PRICE\"\n                                      (ngModelChange)=\"UpdateBiddingData()\"\n                                    />\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <select class=\"form-control\"\n                                            [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_BENCHMARK_PRICE\"\n                                            [disabled]=\"model.IS_DISABLED\"\n                                            (ngModelChange)=\"UpdateBiddingData()\"\n                                    >\n                                      <option [value]=\"elem.ID\" *ngFor=\"let elem of master.BENCHMARKS\">{{ elem.VALUE }}</option>\n                                    </select>\n                                  </td>\n                                  <td class=\"min-150\">\n                                    <input\n                                      type=\"text\"\n                                      class=\"form-control\"\n                                      ptxRestrictNumber\n                                      [fraction]=\"4\"\n                                      [padding]=\"0\"\n                                      [disabled]=\"true\"\n                                      [(ngModel)]=\"model.CDS_BIDDING_ITEMS[index].CBI_MARGIN\"\n                                      (ngModelChange)=\"UpdateBiddingData()\"\n                                    />\n                                  </td>\n                                  <td>\n                                    <a (click)=\"RemoveBiddingItems(model.CDS_BIDDING_ITEMS[index])\">\n                                      <span class=\"glyphicon glyphicon-trash\"></span>\n                                    </a>\n                                  </td>\n                              </tr>\n                            </ng-template>\n                        </tbody>\n                    </table>\n                </div>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <button class=\"btn btn-success\" (click)=\"AddBiddingItems()\">Add Customer</button>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <button class=\"btn btn-primary pull-right\" (click)=\"AddBidRound()\"> Add Bid Round</button>\n                  </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"form-horizontal mb-30\" *ngIf=\"false\">\n      <div class=\"row\">\n        <h2 class=\"second-title\">\n          Optimization Detail\n        </h2>\n        <div class=\"form-horizontal mb-30\">\n          <ptx-crude-purchase-table\n            *ngIf=\"false\"\n          ></ptx-crude-purchase-table>\n          <!--<table class=\"table table-bordered autoscroll text-center\">-->\n            <!--<thead>-->\n            <!--<tr>-->\n              <!--<th rowspan=\"2\">Purchase No.</th>-->\n              <!--<th rowspan=\"2\">Feedstock</th>-->\n              <!--<th rowspan=\"2\">Name</th>-->\n              <!--<th rowspan=\"2\">Origin</th>-->\n              <!--<th rowspan=\"2\">Supplier</th>-->\n              <!--<th rowspan=\"2\">Contact Person</th>-->\n              <!--<th rowspan=\"2\">Quantity (KBBL)</th>-->\n              <!--<th colspan=\"5\">Offer price or premium ($/bbl)</th>-->\n              <!--<th rowspan=\"2\">Rank/ Round</th>-->\n              <!--<th rowspan=\"2\">Margin vs LP ($/bbl)</th>-->\n              <!--<th rowspan=\"2\">More Detail</th>-->\n            <!--</tr>-->\n            <!--<tr>-->\n              <!--<th>Incoterns</th>-->\n              <!--<th>Purchasing Price</th>-->\n              <!--<th>Market/ Source</th>-->\n              <!--<th>LP Max Purchase Price</th>-->\n              <!--<th>Benchmark price</th>-->\n            <!--</tr>-->\n            <!--</thead>-->\n            <!--<tbody>-->\n            <!--<tr>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" style=\"margin-bottom: 5px\" type=\"text\" [disabled]=\"true\">-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<input class=\"form-control\" type=\"text\" [disabled]=\"true\">-->\n              <!--</td>-->\n              <!--<td>-->\n                <!--<a href=\"#\" style=\"color:#0a7ab1; font-size: 16px;\">-->\n                  <!--<i class=\"glyphicon glyphicon-file\"></i>-->\n                <!--</a>-->\n              <!--</td>-->\n            <!--</tr>-->\n            <!--</tbody>-->\n          <!--</table>-->\n        </div>\n      </div>\n    </div>\n\n    <div class=\"form-horizontal mb-30\">\n        <div class=\"row\">\n            <h2 class=\"second-title\">\n                Proposed for Approval\n            </h2>\n            <ng-template ngFor let-e [ngForOf]=\"model.CDS_PROPOSAL_ITEMS\" let-index=\"index\">\n              <div class=\"row\">\n                  <div class=\"col-xs-12 col-sm-6\">\n                      <div class=\"row\">\n                          <div class=\"form-group\">\n                              <label class=\"col-xs-4 control-label\">Customer</label>\n                              <div class=\"col-xs-6 pr-10\">\n                                  <input class=\"form-control\"\n                                         type=\"text\"\n                                         [disabled]=\"true\"\n                                         [ngModel]=\"ShowCustomerName(model.CDS_PROPOSAL_ITEMS[index].CSI_FK_CUSTOMER)\"\n                                  >\n                              </div>\n                          </div>\n                      </div>\n                      <div class=\"row\">\n                          <div class=\"form-group\">\n                              <label class=\"col-xs-4 control-label\">Contract Period (term)</label>\n                              <div class=\"col-xs-6 pr-10\">\n                                  <ptx-input-date-range\n                                    [disabled]=\"model.IS_DISABLED\"\n                                    [start_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_CONTRACT_PERIOD_FROM\"\n                                    [end_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_CONTRACT_PERIOD_TO\"\n                                    (modelChange)=\"model.CDS_PROPOSAL_ITEMS[index].CSI_CONTRACT_PERIOD_FROM = $event.start_date;model.CDS_PROPOSAL_ITEMS[index].CSI_CONTRACT_PERIOD_TO = $event.end_date;\"\n                                  >\n                                  </ptx-input-date-range>\n                              </div>\n                          </div>\n                      </div>\n                      <div class=\"row\">\n                          <div class=\"form-group\">\n                              <label class=\"col-xs-4 control-label\">Loading Period</label>\n                              <div class=\"col-xs-6 pr-10\">\n                                <ptx-input-date-range\n                                  [disabled]=\"model.IS_DISABLED\"\n                                  [start_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_LOADING_PERIOD_FROM\"\n                                  [end_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_LOADING_PERIOD_TO\"\n                                  (modelChange)=\"model.CDS_PROPOSAL_ITEMS[index].CSI_LOADING_PERIOD_FROM = $event.start_date;model.CDS_PROPOSAL_ITEMS[index].CSI_LOADING_PERIOD_TO = $event.end_date;\"\n                                >\n                                </ptx-input-date-range>\n                              </div>\n                          </div>\n                      </div>\n                      <div class=\"row\">\n                          <div class=\"form-group\">\n                              <label class=\"col-xs-4 control-label\">Discharging Period</label>\n                              <div class=\"col-xs-6 pr-10\">\n                                <ptx-input-date-range\n                                  [disabled]=\"model.IS_DISABLED\"\n                                  [start_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_DISCHARGING_PERIOD_FROM\"\n                                  [end_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_DISCHARGING_PERIOD_TO\"\n                                  (modelChange)=\"model.CDS_PROPOSAL_ITEMS[index].CSI_DISCHARGING_PERIOD_FROM = $event.start_date;model.CDS_PROPOSAL_ITEMS[index].CSI_DISCHARGING_PERIOD_TO = $event.end_date;\"\n                                >\n                                </ptx-input-date-range>\n                              </div>\n                          </div>\n                      </div>\n                      <div class=\"row\">\n                          <div class=\"form-group\">\n                              <label class=\"col-xs-4 control-label\">GT&C</label>\n                              <div class=\"col-xs-6 pr-10\">\n                                  <input class=\"form-control\"\n                                         type=\"text\"\n                                         [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_GT_AND_C\"\n                                         [disabled]=\"model.IS_DISABLED\"\n                                  >\n                              </div>\n                          </div>\n                      </div>\n                  </div>\n                  <div class=\"col-xs-12 col-sm-6\">\n                      <div class=\"row\">\n                          <label class=\"control-label\">Any other special terms and conditions:</label>\n                          <textarea class=\"form-control\"\n                                    rows=\"5\"\n                                    [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_OTHER_SPECIAL_TERM\"\n                                    [disabled]=\"model.IS_DISABLED\"\n                          ></textarea>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"row\">\n                <div class=\"col-xs-12 col-sm-6\">\n                  <div class=\"row\">\n                    <div class=\"form-group\">\n                      <label class=\"col-xs-4 control-label\">Payment Term</label>\n                      <div class=\"col-xs-6 pr-10\">\n                        <select class=\"form-control\"\n                                [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM\"\n                                [disabled]=\"model.IS_DISABLED\"\n                        >\n                          <option [value]=\"elem.ID\" *ngFor=\"let elem of master.PAYMENT_TERM\">{{ elem.VALUE }}</option>\n                        </select>\n                      </div>\n                      <div class=\"col-xs-2 pr-10\">\n                        <input type=\"text\"\n                               class=\"form-control text-center mb-15\"\n                               ptxRestrictNumber\n                               [fraction]=\"0\"\n                               [padding]=\"0\"\n                               [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_DETAIL\"\n                               [disabled]=\"model.IS_DISABLED\"\n                        >\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col-xs-12 col-sm-6 pl-15\">\n                  <div class=\"row\">\n                    <div class=\"col-xs-6\">\n                      <div class=\"row\">\n                        <div class=\"radio\">\n                          <input id=\"cds0000000000001\"\n                                 [disabled]=\"model.IS_DISABLED\"\n                                 name=\"CSI_PAYMENT_TERM_SUB_DETAIL\"\n                                 type=\"radio\"\n                                 checked\n                                 [value]=\"'0000000000001'\"\n                                 [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_SUB_DETAIL\"\n                          >\n                          <label for=\"cds0000000000001\"> days from B/L Date (B/L Date = Day One)</label>\n                        </div>\n                      </div>\n                      <div class=\"row\">\n                        <div class=\"radio\">\n                          <input id=\"cds0000000000003\"\n                                 [disabled]=\"model.IS_DISABLED\"\n                                 name=\"CSI_PAYMENT_TERM_SUB_DETAIL\"\n                                 type=\"radio\"\n                                 checked\n                                 [value]=\"'0000000000003'\"\n                                 [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_SUB_DETAIL\"\n                          >\n                          <label for=\"cds0000000000003\"> days from NOR Date (NOR Date = Day One)</label>\n                        </div>\n                      </div>\n                      <div class=\"row\">\n                        <div class=\"row\">\n                          <div class=\"col-xs-4\">\n                            <div class=\"radio\">\n                              <input id=\"cds0000000000005\"\n                                     [disabled]=\"model.IS_DISABLED\"\n                                     name=\"CSI_PAYMENT_TERM_SUB_DETAIL\"\n                                     type=\"radio\"\n                                     checked\n                                     [value]=\"'0000000000005'\"\n                                     [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_SUB_DETAIL\"\n                              >\n                              <label for=\"cds0000000000005\"> Others</label>\n                            </div>\n                          </div>\n                          <div class=\"col-xs-8\">\n                            <input class=\"form-control\"\n                                   *ngIf=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_SUB_DETAIL === '0000000000005'\"\n                                   type=\"text\"\n                                   [disabled]=\"model.IS_DISABLED\"\n                                   [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_OTHER\"\n                            >\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <div class=\"col-xs-6\">\n                      <div class=\"row\">\n                        <div class=\"radio\">\n                          <input id=\"cds0000000000002\"\n                                 [disabled]=\"model.IS_DISABLED\"\n                                 name=\"CSI_PAYMENT_TERM_SUB_DETAIL\"\n                                 type=\"radio\"\n                                 checked\n                                 [value]=\"'0000000000002'\"\n                                 [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_SUB_DETAIL\"\n                          >\n                          <label for=\"cds0000000000002\" style=\"font-size: 18px;\"> days from B/L Date (B/L Date = Day Zero)</label>\n                        </div>\n                      </div>\n                      <div class=\"row\">\n                        <div class=\"radio\">\n                          <input id=\"cds0000000000004\"\n                                 [disabled]=\"model.IS_DISABLED\"\n                                 name=\"CSI_PAYMENT_TERM_SUB_DETAIL\"\n                                 type=\"radio\"\n                                 checked\n                                 [value]=\"'0000000000004'\"\n                                 [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PAYMENT_TERM_SUB_DETAIL\"\n                          >\n                          <label for=\"cds0000000000004\" style=\"font-size: 18px;\"> days from NOR Date (NOR Date = Day Zero)</label>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </ng-template>\n        </div>\n\n        <div class=\"form-horizontal mb-30\">\n            <div class=\"row\">\n                <h2 class=\"second-title\">\n                    Proposal\n                </h2>\n                <ng-template ngFor let-e [ngForOf]=\"model.CDS_PROPOSAL_ITEMS\" let-index=\"index\">\n                    <div class=\"row\">\n                        <div class=\"col-xs-12 col-sm-6\">\n                            <div class=\"row\">\n                                <div class=\"form-group\">\n                                    <label class=\"col-xs-4 control-label\">Award to</label>\n                                    <div class=\"col-xs-6 pr-10\">\n                                        <input class=\"form-control\"\n                                               type=\"text\"\n                                               [disabled]=\"true\"\n                                               [ngModel]=\"ShowMaterialName(model.CDS_PROPOSAL_ITEMS[index].CSI_FK_MATERIAL)\"\n                                        >\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"row\">\n                                <div class=\"form-group\">\n                                    <label class=\"col-xs-4 control-label\">Customer</label>\n                                    <div class=\"col-xs-6 pr-10\">\n                                      <input class=\"form-control\"\n                                             type=\"text\"\n                                             [disabled]=\"true\"\n                                             [ngModel]=\"ShowCustomerName(model.CDS_PROPOSAL_ITEMS[index].CSI_FK_CUSTOMER)\"\n                                      >\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-xs-12 col-sm-12\">\n                          <div class=\"row\">\n                            <div class=\"form-group\">\n                              <label class=\"col-xs-2 control-label\">Reason</label>\n                              <div class=\"col-xs-3 pr-10\">\n                                <select class=\"form-control\"\n                                        [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_REASON\"\n                                        [disabled]=\"model.IS_DISABLED\"\n                                >\n                                  <option [value]=\"op.ID\" *ngFor=\"let op of master.PROPOSAL_REASON\"> {{ op.VALUE }}</option>\n                                </select>\n                              </div>\n                              <div class=\"col-xs-5\" *ngIf=\"model.CDS_PROPOSAL_ITEMS[index].CSI_REASON === 'other'\">\n                                <input class=\"form-control\"\n                                       type=\"text\"\n                                       [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_REASON_OTHER\"\n                                       [disabled]=\"model.IS_DISABLED\"\n                                >\n                              </div>\n                            </div>\n                          </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                      <div class=\"col-xs-12 col-sm-12\">\n                        <div class=\"row\">\n                          <div class=\"form-group\">\n                            <label class=\"col-xs-2 control-label\">Lower than Purchasing Price by</label>\n                            <div class=\"col-xs-3 pr-10\">\n                              <input class=\"form-control\"\n                                     type=\"text\"\n                                     [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_LOWER_THAN_CDP_PRICE\"\n                                     [disabled]=\"true || model.IS_DISABLED\"\n                              >\n                            </div>\n                            <div class=\"col-xs-1 text-right\" style=\"padding-right: 20px;\">{{ model.CDS_PROPOSAL_ITEMS[index].CSI_HIGHER_THAN_CDP_PRICE_UNIT}} or </div>\n                            <div class=\"col-xs-4\">\n                              <input class=\"form-control\"\n                                     type=\"text\"\n                                     [ngModel]=\"ShowHigherThanCDPPriceUSD(model.CDS_PROPOSAL_ITEMS[index])\"\n                                     [disabled]=\"true || model.IS_DISABLED\"\n                              >\n                            </div>\n                            <div class=\"col-xs-2 text-left\" style=\"padding-right: 20px;\">US$</div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                    <div class=\"row\">\n                      <div class=\"col-xs-12 col-sm-12\">\n                        <div class=\"row\">\n                          <div class=\"form-group\">\n                            <label class=\"col-xs-2 control-label\">Pricing Period</label>\n                            <div class=\"col-xs-3 pr-10\">\n                              <ptx-input-date-range\n                                [disabled]=\"model.IS_DISABLED\"\n                                [start_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PRICING_PERIOD_FROM\"\n                                [end_date]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PRICING_PERIOD_TO\"\n                                (modelChange)=\"model.CDS_PROPOSAL_ITEMS[index].CSI_PRICING_PERIOD_FROM = $event.start_date;model.CDS_PROPOSAL_ITEMS[index].CSI_PRICING_PERIOD_TO = $event.end_date;\"\n                              >\n                              </ptx-input-date-range>\n                            </div>\n                            <label class=\"col-xs-2 text-right\">Quantity ({{ model.CDS_PROPOSAL_ITEMS[index].CSI_QUANTITY_UNIT }})</label>\n                            <div class=\"col-xs-2\">\n                              <input class=\"form-control\"\n                                     type=\"text\"\n                                     [ngModel]=\"ShowQuantity(model.CDS_PROPOSAL_ITEMS[index])\"\n                                     [disabled]=\"true || model.IS_DISABLED\"\n                              >\n                            </div>\n                            <label class=\"col-xs-1 text-right\">Tolerance (%)</label>\n                            <div class=\"col-xs-1 pr-10\">\n                              <select class=\"form-control\"\n                                      [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_TOLERANCE_SIGN\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                              >\n                                <option [value]=\"op.ID\" *ngFor=\"let op of master.TOLERANCE\"> {{ op.VALUE }}</option>\n                              </select>\n                            </div>\n                            <div class=\"col-xs-1\">\n                              <input class=\"form-control\"\n                                     type=\"text\"\n                                     [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_TOLERANCE\"\n                                     [disabled]=\"model.IS_DISABLED\"\n                              >\n                            </div>\n                          </div>\n                        </div>\n                        <div class=\"row\">\n                          <div class=\"form-group\">\n                            <label class=\"col-xs-10 control-label\"></label>\n                            <div class=\"col-xs-2 pr-10\">\n                              <select class=\"form-control\"\n                                      [(ngModel)]=\"model.CDS_PROPOSAL_ITEMS[index].CSI_TOLERANCE_OPTION\"\n                                      [disabled]=\"model.IS_DISABLED\"\n                              >\n                                <option [value]=\"op.ID\" *ngFor=\"let op of master.TOLERANCE_OPTION\"> {{ op.VALUE }}</option>\n                              </select>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                </ng-template>\n            </div>\n        </div>\n\n        <div class=\"form-horizontal mb-30\">\n            <div class=\"row\">\n                <h2 class=\"second-title\">\n                    Note\n                </h2>\n                <div class=\"row\">\n                    <textarea rows=\"5\"\n                              class=\"form-control\"\n                              [(ngModel)]=\"model.CDA_NOTE\"\n                    ></textarea>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"form-horizontal mb-30\">\n            <div class=\"row\">\n                <h2 class=\"second-title\">\n                    Reason for Approval/Reject\n                </h2>\n                <div class=\"row\">\n                    <div class=\"col-xs-6\">\n                        <textarea rows=\"2\"\n                                  class=\"form-control\"\n                                  [disabled]=\"true\"\n                                  [ngModel]=\"model.CDA_REASON\"\n                        ></textarea>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"form-horizontal mb-30\">\n            <div class=\"row\">\n                <h2 class=\"second-title\">\n                    Attach File for Reference\n                </h2>\n              <div class=\"row\">\n                <div class=\"form-horizontal mb-30 pb-30\">\n                  <ptx-file-upload\n                    btnClass=\"text-left\"\n                    [model]=\"model.CDS_ATTACH_FILE\"\n                    [is_disabled]=\"model.IS_DISABLED\"\n                    [need_caption]=\"false\"\n                    [desc]=\"'PDF, Word, Excel, Image, Outlook'\"\n                    filterBy=\"REF\"\n                    SOURCE_FROM=\"CDS\"\n                    (handleModelChanged)=\"model.CDS_ATTACH_FILE = $event\"\n\n                    [PATH_DEF] = \"'CAT_PATH'\"\n                    [INFO_DEF] = \"'CAT_INFO'\"\n                    [CAPTION_DEF] = \"'CAT_CAPTION'\"\n                    [FILTER_KEY_DEF] = \"'CAT_TYPE'\"\n                  >\n                  </ptx-file-upload>\n                </div>\n              </div>\n            </div>\n        </div>\n\n      <div class=\"wrapper-button\" id=\"buttonDiv\">\n        <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft(model)\"> SAVE DRAFT </button>\n        <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit(model)\"> SAVE & SUBMIT </button>\n        <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify(model)\"> VERIFY </button>\n        <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse(model)\"> ENDORSE </button>\n        <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved(model)\"> APPROVE </button>\n        <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject(model)\"> REJECT </button>\n        <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel(model)\"> CANCEL </button>\n        <!--<button class=\"btn btn-default green\" (click)=\"DoGenerateEXCEL(model)\"> Export to Excel  </button>-->\n        <button class=\"btn btn-default\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>\n        <!--<button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>-->\n\n      </div>\n\n    </div>\n    <!-- end -->\n</div>\n"

/***/ }),

/***/ "./src/app/crude-sale-approval/components/crude-sale-approval/crude-sale-approval.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CrudeSaleApprovalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_cds_data__ = __webpack_require__("./src/app/models/cds-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loader_service__ = __webpack_require__("./src/app/services/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_cds_service__ = __webpack_require__("./src/app/services/cds.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_utility__ = __webpack_require__("./src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_permission_store__ = __webpack_require__("./src/app/common/permission-store.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__common_button__ = __webpack_require__("./src/app/common/button.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_sweetalert__ = __webpack_require__("./node_modules/sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__common_constant__ = __webpack_require__("./src/app/common/constant.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__models_cds_master__ = __webpack_require__("./src/app/models/cds-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_cds_master_service__ = __webpack_require__("./src/app/services/cds-master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__models_cds_bidding_items__ = __webpack_require__("./src/app/models/cds-bidding-items.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__common_cds_helper__ = __webpack_require__("./src/app/common/cds-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__models_cds_round__ = __webpack_require__("./src/app/models/cds-round.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__models_cds_proposal_items__ = __webpack_require__("./src/app/models/cds-proposal-items.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var CrudeSaleApprovalComponent = /** @class */ (function () {
    function CrudeSaleApprovalComponent(route, router, loaderService, masterService, cdsService, decimalPipe) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.cdsService = cdsService;
        this.decimalPipe = decimalPipe;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_cds_data__["a" /* CDS_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_13__models_cds_master__["a" /* CDS_MASTER */]();
        this.is_duplicate = false;
    }
    CrudeSaleApprovalComponent.prototype.canDeactivate = function () {
        // insert logic to check if there are pending changes here;
        // returning true will navigate without confirmation
        // returning false will show a confirm alert before navigating away
    };
    // @HostListener allows us to also guard against browser refresh, close, etc.
    CrudeSaleApprovalComponent.prototype.unloadNotification = function ($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = 'This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)';
        }
    };
    CrudeSaleApprovalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['req_transaction_id'];
            _this.transaction_id = params['transaction_id'];
            // this.user_group = params['user_group'];
            _this.is_duplicate = params['is_duplicate'];
            // this.model.CDA_USER_GROUP = this.user_group.toUpperCase();
            _this.load();
        });
    };
    CrudeSaleApprovalComponent.prototype.ShowValueVessel = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].getTextByID(id, this.master.VESSELS);
    };
    CrudeSaleApprovalComponent.prototype.ShowValue = function (id) {
        return __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].getTextByID(id, this.master.COMPANY);
    };
    CrudeSaleApprovalComponent.prototype.ShowQuantityUnit = function () {
        var row = this.model.CDS_CRUDE_PURCHASE_DETAIL && this.model.CDS_CRUDE_PURCHASE_DETAIL.length > 0 && this.model.CDS_CRUDE_PURCHASE_DETAIL[0];
        return (row && row.QUANTITY_UNIT) || '';
    };
    CrudeSaleApprovalComponent.prototype.ShowPricePerQtty = function () {
        var row = this.model.CDS_CRUDE_PURCHASE_DETAIL && this.model.CDS_CRUDE_PURCHASE_DETAIL.length > 0 && this.model.CDS_CRUDE_PURCHASE_DETAIL[0];
        return (row && row.MARGIN_VS_LP_UNIT) || '';
    };
    CrudeSaleApprovalComponent.prototype.load = function () {
        var _this = this;
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = res;
            _this.master.REAL_CUSTOMER = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(_this.master.CUSTOMERS);
        }, function (error) {
            console.log(error);
        });
        if (this.req_transaction_id) {
            this.cdsService.GetByTransactionId(this.transaction_id).subscribe(function (res) {
                _this.model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(res);
                console.log(_this.model);
                _this.model.TRAN_ID = _this.transaction_id;
                _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                if (_this.is_duplicate) {
                    _this.model.Buttons = __WEBPACK_IMPORTED_MODULE_8__common_button__["a" /* Button */].getDefaultButton();
                    _this.model.IS_DISABLED = false;
                    _this.model.CDA_REASON = '';
                    _this.model.CDA_FORM_ID = '';
                    _this.model.CDA_STATUS = '';
                    _this.model.CDA_DOC_DATE = __WEBPACK_IMPORTED_MODULE_11_moment_moment__().format('YYYY-MM-DD HH:mm:ss');
                }
                _this.loaderService.display(false);
            }, function (err) {
                console.log(err);
                _this.loaderService.display(false);
            });
        }
        else {
            this.model.Buttons = __WEBPACK_IMPORTED_MODULE_8__common_button__["a" /* Button */].getDefaultButton();
            this.loaderService.display(false);
        }
    };
    CrudeSaleApprovalComponent.prototype.log = function () {
        console.log(this.model);
    };
    CrudeSaleApprovalComponent.prototype.HasReferrence = function () {
        return this.model.CDS_CRUDE_PURCHASE_DETAIL.length > 0 || this.model.CDS_BIDDING_ITEMS.length > 1;
    };
    CrudeSaleApprovalComponent.prototype.AddBiddingItems = function () {
        var prepare_item = new __WEBPACK_IMPORTED_MODULE_15__models_cds_bidding_items__["a" /* CDS_BIDDING_ITEMS */]();
        var first_row = this.model.CDS_BIDDING_ITEMS && this.model.CDS_BIDDING_ITEMS.length > 0 && this.model.CDS_BIDDING_ITEMS[0];
        prepare_item.CBI_FK_MATERIAL = __WEBPACK_IMPORTED_MODULE_9_lodash__["clone"](first_row.CBI_FK_MATERIAL);
        prepare_item.CBI_FEEDSTOCK = __WEBPACK_IMPORTED_MODULE_9_lodash__["clone"](first_row.CBI_FEEDSTOCK);
        if (!first_row.CBI_FK_MATERIAL) {
            prepare_item.CBI_MATERIAL_NAME_OTHER = first_row.CBI_MATERIAL_NAME_OTHER;
        }
        this.model.CDS_BIDDING_ITEMS.push(prepare_item);
        var prepare_item_proposal = new __WEBPACK_IMPORTED_MODULE_18__models_cds_proposal_items__["a" /* CDS_PROPOSAL_ITEMS */]();
        prepare_item_proposal.CSI_FK_MATERIAL = prepare_item.CBI_FK_MATERIAL;
        this.model.CDS_PROPOSAL_ITEMS.push(prepare_item_proposal);
        this.UpdateRound();
        this.model = __WEBPACK_IMPORTED_MODULE_16__common_cds_helper__["a" /* CDSHelper */].updateCrudePurchaseDetail(this.model);
    };
    CrudeSaleApprovalComponent.prototype.AddBidRound = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        // let count_product = this.criteria.products.length;
        this.model.CDS_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_9_lodash__["map"](this.model.CDS_BIDDING_ITEMS, function (elem) {
            elem.CDS_ROUND = __WEBPACK_IMPORTED_MODULE_9_lodash__["extend"]([], elem.CDS_ROUND);
            elem.CDS_ROUND.push(__WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(new __WEBPACK_IMPORTED_MODULE_17__models_cds_round__["a" /* CDS_ROUND */]()));
            return elem;
        });
    };
    CrudeSaleApprovalComponent.prototype.UpdateBiddingData = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_16__common_cds_helper__["a" /* CDSHelper */].updateBiddingItem(this.model);
    };
    CrudeSaleApprovalComponent.prototype.UpdateRound = function () {
        var template_count = this.model.CDS_BIDDING_ITEMS[0].CDS_ROUND.length;
        this.model.CDS_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_9_lodash__["map"](this.model.CDS_BIDDING_ITEMS, function (elem) {
            elem.CDS_ROUND = __WEBPACK_IMPORTED_MODULE_9_lodash__["extend"]([], elem.CDS_ROUND);
            elem.CDS_ROUND = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].FillArray(elem.CDS_ROUND, {}, template_count);
            return elem;
        });
    };
    CrudeSaleApprovalComponent.prototype.RemoveRound = function (index) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var popup_content = "Are you sure you want to remove this bid round?";
        __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.CDS_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_9_lodash__["map"](context.model.CDS_BIDDING_ITEMS, function (elem) {
                elem.CDS_ROUND = __WEBPACK_IMPORTED_MODULE_9_lodash__["extend"]([], elem.CDS_ROUND);
                elem.CDS_ROUND.splice(index, 1);
                return elem;
            });
        });
    };
    CrudeSaleApprovalComponent.prototype.RemoveBiddingItems = function (target) {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        // const {customer, product} = PAFStore.GetPAFBidderItemsSummary(target, this.master, this.is_cmps_dom);
        var customer = '';
        var popup_content = "Are you sure you want to remove this record?<br/><br/>\n      <b>Customer:</b> " + customer + "<br/>\n    ";
        __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.CDS_BIDDING_ITEMS = __WEBPACK_IMPORTED_MODULE_9_lodash__["reject"](context.model.CDS_BIDDING_ITEMS, function (elem) {
                return elem == target;
            });
            _this.UpdateBiddingData();
        });
    };
    CrudeSaleApprovalComponent.prototype.GetCustomers = function (company) {
        company = company || '';
        // console
        var customers = __WEBPACK_IMPORTED_MODULE_9_lodash__["filter"](this.master.REAL_CUSTOMER, function (e) {
            return e.COMPANY_CODE && e.COMPANY_CODE.trim() == company.trim();
        });
        customers = __WEBPACK_IMPORTED_MODULE_9_lodash__["uniqBy"](customers, 'ID');
        console.log(company, customers);
        // console.log(company, customers);
        // console.log(company, customers);
        return customers;
    };
    CrudeSaleApprovalComponent.prototype.OnChangeCompany = function (d) {
        // this.master.CUSTOMERS = _.
        // this.master.CUSTOMERS = this.GetCustomers(this.model.CDA_FOR_COMPANY);
        console.log(d);
    };
    CrudeSaleApprovalComponent.prototype.OnCDPSelectData = function (targets) {
        var _this = this;
        console.log(targets);
        __WEBPACK_IMPORTED_MODULE_9_lodash__["each"](targets, function (e, k) {
            var found = __WEBPACK_IMPORTED_MODULE_9_lodash__["find"](_this.model.CDS_CRUDE_PURCHASE_DETAIL, function (f) {
                return f.PURCHASE_NO === e.PURCHASE_NO;
            });
            if (!found) {
                _this.model.CDS_CRUDE_PURCHASE_DETAIL.push(e);
            }
            _this.model = __WEBPACK_IMPORTED_MODULE_16__common_cds_helper__["a" /* CDSHelper */].updateCrudePurchaseDetail(_this.model);
        });
    };
    CrudeSaleApprovalComponent.prototype.OnDeleteCDPData = function (target) {
        console.log(target);
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var popup_content = "Are you sure you want to remove this bid round?";
        __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.CDS_CRUDE_PURCHASE_DETAIL = __WEBPACK_IMPORTED_MODULE_9_lodash__["reject"](context.model.CDS_CRUDE_PURCHASE_DETAIL, function (elem) {
                return elem == target;
            });
        });
    };
    CrudeSaleApprovalComponent.prototype.VerifiedInput = function () {
        var model = this.model;
        // const found_not_set_port = _.find(model.CDS_PORT, (e) => {
        //   return !(e.DPT_LOAD_PORT || e.DPT_LOAD_PORT_FK_JETTY);
        // });
        var show_error = function (m) {
            __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
                title: '',
                text: m,
                type: '',
                html: true,
            }, function () {
                return false;
            });
            return;
        };
        //
        // if (!model.CDA_FOR_COMPANY) {
        //   show_error('<b>Please enter your "For Company"</b>');
        //   return false;
        // } else if (!model.CDA_COUNTERPARTY_TYPE) {
        //   show_error('<b>Please enter your "Counter Party"</b>');
        //   return false;
        // } else if (!(model.CDA_SHIP_OWNER || model.CDA_SUPPLIER || model.CDA_CUSTOMER)) {
        //   show_error('<b>Please enter your "Counterparty"</b>');
        //   return false;
        // } else if (!(model.CDA_VESSEL)) {
        //   show_error('<b>Please enter your "Vessel"</b>');
        //   return false;
        // } else if (!(model.CDA_LAYTIME_CONTRACT_HRS)) {
        //   show_error('<b>Please enter your "Laytime Contract"</b>');
        //   return false;
        // } else if (!(model.CDA_DEMURAGE_RATE)) {
        //   show_error('<b>Please enter your "Demurrage Rate"</b>');
        //   return false;
        // } else if (model.CDS_PORT.length === 0 || found_not_set_port) {
        //   show_error('<b>Please enter your "Port"</b>');
        //   return false;
        // }
        return true;
    };
    CrudeSaleApprovalComponent.prototype.GetRoundName = function (n) {
        var s = ["th", "st", "nd", "rd"];
        var v = n % 100;
        return n + (s[(v - 20) % 10] || s[v] || s[0]);
    };
    CrudeSaleApprovalComponent.prototype.ShowCustomerName = function (fk_customer) {
        return __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].getTextByID(fk_customer, this.master.CUSTOMERS);
    };
    CrudeSaleApprovalComponent.prototype.ShowMaterialName = function (fk) {
        return __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].getTextByID(fk, this.master.MATERIALS);
    };
    CrudeSaleApprovalComponent.prototype.ShowHigherThanCDPPriceUSD = function (elem) {
        // elem.CSI_LOWER_THAN_CDP_PRICE_MIN_USD
        // elem.CSI_LOWER_THAN_CDP_PRICE_MAX_USD
        return '';
    };
    CrudeSaleApprovalComponent.prototype.ShowQuantity = function (elem) {
        // elem.CSI_QUANTITY_MIN
        // elem.CSI_QUANTITY_
        return '';
    };
    CrudeSaleApprovalComponent.prototype.GetRoundLength = function () {
        // const l: Array<CDS_BIDDING_ITEMS> = this.model.CDS_BIDDING_ITEMS && this.model.CDS_BIDDING_ITEMS.length > 0 && this.model.CDS_BIDDING_ITEMS;
        // return this.model.CDS_BIDDING_ITEMS.length || 0;
        return this.GetRound().length;
    };
    CrudeSaleApprovalComponent.prototype.GetRound = function () {
        var l = this.model.CDS_BIDDING_ITEMS && this.model.CDS_BIDDING_ITEMS.length > 0 && this.model.CDS_BIDDING_ITEMS[0];
        var round = l.CDS_ROUND || [];
        return round;
    };
    CrudeSaleApprovalComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_16__common_cds_helper__["a" /* CDSHelper */].updatedModel(model, this.is_duplicate);
        var popup = new Promise(function (resolver) {
            __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
                title: 'Save',
                text: 'Are you sure you want to save?',
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, resolver);
        })
            .then(function () {
            return context.cdsService.SaveDraft(model).toPromise();
        })
            .then(function (res) {
            // console.log('====', res);
            return new Promise(function (resolver) {
                __WEBPACK_IMPORTED_MODULE_10_sweetalert__({ title: 'Saved!', type: 'success' }, function () { return resolver(res); });
            });
        })
            .then(function (res) {
            context.router.navigate(["/crude-sale-approval/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
        })
            .catch(function (err) {
            // console.log('eeee', err);
            __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'save error');
        });
        //
        // swal({
        //     title: 'Save',
        //     text: 'Are you sure you want to save?',
        //     type: 'info',
        //     showCancelButton: true,
        //     closeOnConfirm: false,
        //     showLoaderOnConfirm: true,
        //   },
        //   () => {
        //     context.cdsService.SaveDraft(model).subscribe(
        //       (res) => {
        //         swal({title: 'Saved!', type: 'success'}, () => {
        //           context.router.navigate([`/crude-sale-approval/${res.req_transaction_id}/${res.transaction_id}/edit`]);
        //           // context.router.navigate(['/cmps/search'])
        //         });
        //       },
        //       (err) => {
        //         // console.log(err)
        //         swal('Something went wrong!', 'save error');
        //       }
        //     );
        //   });
    };
    CrudeSaleApprovalComponent.prototype.DoSubmit = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(context.model);
        // model = CDSHelper.updatedModel(model, this.is_duplicate);
        if (!this.VerifiedInput()) {
            return;
        }
        __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
            title: 'Submit',
            text: 'Do you confirm to submit for approving?',
            type: 'info',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.cdsService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                _this.req_transaction_id = res.req_transaction_id;
                _this.transaction_id = res.transaction_id;
                context.cdsService.Submit(model).subscribe(function (res2) {
                    __WEBPACK_IMPORTED_MODULE_10_sweetalert__({ title: 'Submitted!', type: 'success' }, function () {
                        context.load();
                        context.router.navigate(["/crude-sale-approval/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'submit error');
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'save error');
            });
        });
    };
    CrudeSaleApprovalComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
            title: 'Verify',
            text: 'Are you sure you want to verify?',
            type: 'info',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.cdsService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_10_sweetalert__({ title: 'Verified!', type: 'success' }, function () {
                    // context.router.navigate(['/cmps/search'])
                    context.load();
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'submit error');
            });
        });
        // this.cdsService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CrudeSaleApprovalComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
            title: 'Endorse',
            text: 'Are you sure you want to endorse?',
            type: 'info',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.cdsService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_10_sweetalert__({ title: 'Endorsed!', type: 'success' }, function () {
                    context.load();
                    // context.router.navigate(['/cmps/search'])
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'submit error');
            });
        });
        // this.cdsService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    CrudeSaleApprovalComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_8__common_button__["a" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === '') {
                    if (result === '') {
                        bootbox.alert('Please Key Reason for Reject.', function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.CDA_REASON = result;
                        _this.cdsService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
                                title: 'Rejected',
                                text: '',
                                type: 'success'
                            }, function () {
                                context.load();
                                // window.location.href = `${BASE_API}/../../`;
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'submit error');
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    CrudeSaleApprovalComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_8__common_button__["a" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === '') {
                    if (result === '') {
                        bootbox.alert('Please Key Reason for Cancel.', function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.CDA_REASON = result;
                        _this.cdsService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
                                title: 'Cancelled',
                                text: '',
                                type: 'success'
                            }, function () {
                                context.load();
                                // window.location.href = `../../web/MainBoards.aspx`;
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'submit error');
                        });
                    });
                }
            }
        });
    };
    CrudeSaleApprovalComponent.prototype.DoApproved = function () {
        var _this = this;
        // const context = this;
        // const model = Utility.clone(this.model);
        // swal({
        //     title: "Approve",
        //     text: "Are you sure you want to approve?",
        //     type: "info",
        //     showCancelButton: true,
        //     closeOnConfirm: false,
        //     showLoaderOnConfirm: true,
        //   },
        //   () => {
        //     context.cdsService.Approve(model).subscribe(
        //       (res) => {
        //         swal({title:"Approved!", type: "success"}, () => {
        //           window.location.href = `${BASE_API}/../../`;
        //         });
        //       },
        //       (err) => {
        //         swal("Something went wrong!", "submit error");
        //       }
        //     );
        //   });
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_8__common_button__["a" /* Button */].getApproveMessage(),
            inputType: 'textarea',
            callback: function (result) {
                console.log(result);
                if (result === '') {
                    result = '-';
                }
                if (result === null || result === '') {
                    if (result === '') {
                        bootbox.alert('Please Key Reason for Approve.', function () { });
                    }
                }
                else {
                    var loading_3 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_3.init(function () {
                        model.CDA_REASON = result;
                        _this.cdsService.Approve(model).subscribe(function (res) {
                            loading_3.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
                                title: 'Approved',
                                text: '',
                                type: 'success'
                            }, function () {
                                context.load();
                                // window.location.href = `../../web/MainBoards.aspx`;
                            });
                        }, function (err) {
                            loading_3.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'submit error');
                        });
                    });
                }
            }
        });
    };
    CrudeSaleApprovalComponent.prototype.DoGeneratePDF = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(context.model);
        if (model.CDA_STATUS === '' || !model.CDA_STATUS || model.CDA_STATUS === 'DRAFT') {
            // model = CDSHelper.updatedModel(model, this.is_duplicate);
            __WEBPACK_IMPORTED_MODULE_10_sweetalert__({
                title: 'Save',
                text: 'Are you sure you want to save?',
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: true,
            }, function () {
                context.cdsService.SaveDraft(model).subscribe(function (res) {
                    console.log(res);
                    context.model.TRAN_ID = res.transaction_id;
                    model.TRAN_ID = res.transaction_id;
                    _this.cdsService.GeneratePDF(model).subscribe(function (res2) {
                        window.open(__WEBPACK_IMPORTED_MODULE_12__common_constant__["a" /* BASE_API */] + "/../../web/report/tmpfile/" + res2, '_blank');
                        context.router.navigate(["/crude-sale-approval/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    }, function (error) {
                        context.router.navigate(["/crude-sale-approval/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_10_sweetalert__('Something went wrong!', 'save error');
                });
            });
        }
        else {
            this.cdsService.GeneratePDF(model).subscribe(function (res) {
                window.open(__WEBPACK_IMPORTED_MODULE_12__common_constant__["a" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            });
        }
    };
    // public DoGenerateEXCEL(): void {
    //   const context = this;
    //   let model = Utility.clone(this.model);
    //   if (model.CDA_STATUS === '' || !model.CDA_STATUS || model.CDA_STATUS === 'DRAFT') {
    //     // model = CDSHelper.updatedModel(model, this.is_duplicate);
    //
    //     swal({
    //         title: 'Save',
    //         text: 'Are you sure you want to save?',
    //         type: 'info',
    //         showCancelButton: true,
    //         closeOnConfirm: true,
    //         // showLoaderOnConfirm: true,
    //       },
    //       () => {
    //         context.cdsService.SaveDraft(model).subscribe(
    //           (res) => {
    //             console.log(res);
    //             context.model.TRAN_ID = res.transaction_id;
    //             model.TRAN_ID = res.transaction_id;
    //             this.cdsService.GenExcel(model).subscribe(
    //               (res2) => {
    //                 window.open(res2);
    //                 context.router.navigate([`/crude-sale-approval/${res.req_transaction_id}/${res.transaction_id}/edit`]);
    //               },
    //               (error) => {
    //                 context.router.navigate([`/crude-sale-approval/${res.req_transaction_id}/${res.transaction_id}/edit`]);
    //               }
    //             );
    //           },
    //           (err) => {
    //             swal('Something went wrong!', 'save error');
    //           }
    //         );
    //       });
    //   } else {
    //     this.cdsService.GenExcel(model).subscribe(
    //       (res) => {
    //         window.open(res);
    //       }
    //     );
    //   }
    // }
    CrudeSaleApprovalComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToReject(button_list);
    };
    CrudeSaleApprovalComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_7__common_permission_store__["a" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('window:beforeunload', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], CrudeSaleApprovalComponent.prototype, "unloadNotification", null);
    CrudeSaleApprovalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-crude-sale-approval',
            template: __webpack_require__("./src/app/crude-sale-approval/components/crude-sale-approval/crude-sale-approval.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__services_loader_service__["a" /* LoaderService */],
            __WEBPACK_IMPORTED_MODULE_14__services_cds_master_service__["a" /* CdsMasterService */],
            __WEBPACK_IMPORTED_MODULE_4__services_cds_service__["a" /* CdsService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DecimalPipe */]])
    ], CrudeSaleApprovalComponent);
    return CrudeSaleApprovalComponent;
}());



/***/ }),

/***/ "./src/app/crude-sale-approval/crude-sale-approval-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CrudeSaleApprovalRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_crude_sale_approval_crude_sale_approval_component__ = __webpack_require__("./src/app/crude-sale-approval/components/crude-sale-approval/crude-sale-approval.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: ':req_transaction_id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_2__components_crude_sale_approval_crude_sale_approval_component__["a" /* CrudeSaleApprovalComponent */],
        data: {
            title: 'Crude Sale Approval Form'
        }
    },
    {
        path: ':req_transaction_id/:transaction_id/edit/:is_duplicate',
        component: __WEBPACK_IMPORTED_MODULE_2__components_crude_sale_approval_crude_sale_approval_component__["a" /* CrudeSaleApprovalComponent */],
        data: {
            title: 'Crude Sale Approval Form'
        }
    },
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__components_crude_sale_approval_crude_sale_approval_component__["a" /* CrudeSaleApprovalComponent */],
        data: {
            title: 'Crude Sale Approval Form'
        }
    }
];
var CrudeSaleApprovalRoutingModule = /** @class */ (function () {
    function CrudeSaleApprovalRoutingModule() {
    }
    CrudeSaleApprovalRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], CrudeSaleApprovalRoutingModule);
    return CrudeSaleApprovalRoutingModule;
}());



/***/ }),

/***/ "./src/app/crude-sale-approval/crude-sale-approval.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrudeSaleApprovalModule", function() { return CrudeSaleApprovalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__crude_sale_approval_routing_module__ = __webpack_require__("./src/app/crude-sale-approval/crude-sale-approval-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_crude_sale_approval_crude_sale_approval_component__ = __webpack_require__("./src/app/crude-sale-approval/components/crude-sale-approval/crude-sale-approval.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__ = __webpack_require__("./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_crude_purchase_table_crude_purchase_table_component__ = __webpack_require__("./src/app/crude-sale-approval/components/crude-purchase-table/crude-purchase-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_cdp_reference_cdp_reference_component__ = __webpack_require__("./src/app/crude-sale-approval/components/cdp-reference/cdp-reference.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var CrudeSaleApprovalModule = /** @class */ (function () {
    function CrudeSaleApprovalModule() {
    }
    CrudeSaleApprovalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__crude_sale_approval_routing_module__["a" /* CrudeSaleApprovalRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__shared_shared_module__["a" /* SharedModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_ngx_bootstrap__["a" /* ModalModule */].forRoot(),
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__components_crude_sale_approval_crude_sale_approval_component__["a" /* CrudeSaleApprovalComponent */], __WEBPACK_IMPORTED_MODULE_7__components_crude_purchase_table_crude_purchase_table_component__["a" /* CrudePurchaseTableComponent */], __WEBPACK_IMPORTED_MODULE_8__components_cdp_reference_cdp_reference_component__["a" /* CdpReferenceComponent */]]
        })
    ], CrudeSaleApprovalModule);
    return CrudeSaleApprovalModule;
}());



/***/ }),

/***/ "./src/app/models/cds-bidding-items.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CDS_BIDDING_ITEMS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cds_round__ = __webpack_require__("./src/app/models/cds-round.ts");

/**
 * Created by chusri on 16/4/2018 AD.
 */
var CDS_BIDDING_ITEMS = /** @class */ (function () {
    function CDS_BIDDING_ITEMS() {
        this.CDS_ROUND = new Array();
        this.CDS_ROUND.push(new __WEBPACK_IMPORTED_MODULE_0__cds_round__["a" /* CDS_ROUND */]());
    }
    return CDS_BIDDING_ITEMS;
}());



/***/ }),

/***/ "./src/app/models/cds-data.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CDS_DATA; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cds_bidding_items__ = __webpack_require__("./src/app/models/cds-bidding-items.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cds_proposal_items__ = __webpack_require__("./src/app/models/cds-proposal-items.ts");



/**
 * Created by chusri on 16/4/2018 AD.
 */
var CDS_DATA = /** @class */ (function () {
    function CDS_DATA() {
        this.CDA_SALE_TYPE = 'term';
        this.CDA_SALE_METHOD = 'customer-tender';
        this.CDA_DOC_DATE = __WEBPACK_IMPORTED_MODULE_0_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.CDA_TPC_PLAN = __WEBPACK_IMPORTED_MODULE_0_moment__().format('YYYY-MM-01');
        this.CDS_ATTACH_FILE = new Array();
        this.CDS_BIDDING_ITEMS = new Array();
        this.CDS_BIDDING_ITEMS.push(new __WEBPACK_IMPORTED_MODULE_1__cds_bidding_items__["a" /* CDS_BIDDING_ITEMS */]());
        this.CDS_CRUDE_PURCHASE_DETAIL = new Array();
        this.CDS_PROPOSAL_ITEMS = new Array();
        this.CDS_PROPOSAL_ITEMS.push(new __WEBPACK_IMPORTED_MODULE_2__cds_proposal_items__["a" /* CDS_PROPOSAL_ITEMS */]());
    }
    return CDS_DATA;
}());



/***/ }),

/***/ "./src/app/models/cds-master.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CDS_MASTER; });
/**
 * Created by chusri on 16/4/2018 AD.
 */
var CDS_MASTER = /** @class */ (function () {
    function CDS_MASTER() {
    }
    return CDS_MASTER;
}());



/***/ }),

/***/ "./src/app/models/cds-proposal-items.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CDS_PROPOSAL_ITEMS; });
/**
 * Created by chusri on 16/4/2018 AD.
 */
var CDS_PROPOSAL_ITEMS = /** @class */ (function () {
    function CDS_PROPOSAL_ITEMS() {
    }
    return CDS_PROPOSAL_ITEMS;
}());



/***/ }),

/***/ "./src/app/models/cds-round.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CDS_ROUND; });
/**
 * Created by chusri on 22/4/2018 AD.
 */
var CDS_ROUND = /** @class */ (function () {
    function CDS_ROUND() {
    }
    return CDS_ROUND;
}());



/***/ })

});
//# sourceMappingURL=crude-sale-approval.module.chunk.js.map