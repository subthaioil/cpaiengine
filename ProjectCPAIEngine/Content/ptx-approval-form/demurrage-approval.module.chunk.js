webpackJsonp(["demurrage-approval.module"],{

/***/ "./src/app/common/daf-helper.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAFHelper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__date_helper__ = __webpack_require__("./src/app/common/date-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash__);


/**
 * Created by chusri on 25/11/2017 AD.
 */
var DAFHelper = /** @class */ (function () {
    function DAFHelper() {
    }
    DAFHelper.updatedModel = function (model, is_duplicate) {
        console.log("is_duplicate: " + is_duplicate);
        if (is_duplicate) {
            delete model.REQ_TRAN_ID;
            delete model.TRAN_ID;
            delete model.DDA_ROW_ID;
            delete model.DDA_FORM_ID;
            delete model.DDA_STATUS;
            delete model.DDA_REASON;
        }
        return model;
    };
    DAFHelper.CalculateTime = function (model) {
        // let result = Utility.clone(model);
        model.DDA_LAYTIME_CONTRACT_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetSumDiffHrsString(+model.DDA_LAYTIME_CONTRACT_HRS, 0);
        model.DAF_PORT = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](model.DAF_PORT, function (e) {
            // Origin
            e.DPT_RT_ORIGIN_CLAIM_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_ORIGIN_CLAIM_T_START, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            e.DPT_RT_ORIGIN_CLAIM_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_ORIGIN_CLAIM_T_START, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            // Top
            e.DPT_RT_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_TOP_REVIEWED_T_START, e.DPT_RT_TOP_REVIEWED_T_STOP);
            e.DPT_RT_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_TOP_REVIEWED_T_START, e.DPT_RT_TOP_REVIEWED_T_STOP);
            // Settle
            e.DPT_RT_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_SETTLED_T_START, e.DPT_RT_SETTLED_T_STOP);
            e.DPT_RT_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_SETTLED_T_START, e.DPT_RT_SETTLED_T_STOP);
            // saving
            e.DPT_RT_SAVING_T_START_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_SETTLED_T_START, e.DPT_RT_ORIGIN_CLAIM_T_START);
            e.DPT_RT_SAVING_T_START_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_SETTLED_T_START, e.DPT_RT_ORIGIN_CLAIM_T_START);
            e.DPT_RT_SAVING_T_STOP_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(e.DPT_RT_SETTLED_T_STOP, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            e.DPT_RT_SAVING_T_STOP_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(e.DPT_RT_SETTLED_T_STOP, e.DPT_RT_ORIGIN_CLAIM_T_STOP);
            e.DPT_RT_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDiffHrsString(e.DPT_RT_SAVING_T_START_HRS, e.DPT_RT_SAVING_T_STOP_HRS);
            e.DPT_RT_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDiffHrsHours(e.DPT_RT_SAVING_T_START_HRS, e.DPT_RT_SAVING_T_STOP_HRS);
            e.DAF_DEDUCTION_ACTIVITY = __WEBPACK_IMPORTED_MODULE_1_lodash__["map"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                if (f.DDD_TYPE === 'fixed') {
                    f.DDD_OWNER_DEDUCTION_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetHoursFromText(f.DDD_OWNER_DEDUCTION_T_TEXT);
                    f.DDD_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetHoursFromText(f.DDD_TOP_REVIEWED_T_TEXT);
                    f.DDD_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetHoursFromText(f.DDD_SETTLED_T_TEXT);
                }
                else {
                    f.DDD_OWNER_DEDUCTION_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(f.DDD_OWNER_DEDUCTION_T_START, f.DDD_OWNER_DEDUCTION_T_STOP);
                    f.DDD_OWNER_DEDUCTION_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(f.DDD_OWNER_DEDUCTION_T_START, f.DDD_OWNER_DEDUCTION_T_STOP);
                    f.DDD_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(f.DDD_TOP_REVIEWED_T_START, f.DDD_TOP_REVIEWED_T_STOP);
                    f.DDD_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(f.DDD_TOP_REVIEWED_T_START, f.DDD_TOP_REVIEWED_T_STOP);
                    f.DDD_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffDateString(f.DDD_SETTLED_T_START, f.DDD_SETTLED_T_STOP);
                    f.DDD_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHours(f.DDD_SETTLED_T_START, f.DDD_SETTLED_T_STOP);
                }
                f.DDD_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsText(f.DDD_SETTLED_T_HRS, f.DDD_OWNER_DEDUCTION_T_HRS);
                f.DDD_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(f.DDD_SETTLED_T_HRS, f.DDD_OWNER_DEDUCTION_T_HRS);
                return f;
            });
            e.DPT_D_OWNER_DEDUCTION_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_OWNER_DEDUCTION_T_HRS || 0;
            });
            e.DPT_D_OWNER_DEDUCTION_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_OWNER_DEDUCTION_T_HRS);
            e.DPT_D_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_TOP_REVIEWED_T_HRS || 0;
            });
            e.DPT_D_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_TOP_REVIEWED_T_HRS);
            e.DPT_D_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_SETTLED_T_HRS || 0;
            });
            e.DPT_D_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_SETTLED_T_HRS);
            e.DPT_D_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](e.DAF_DEDUCTION_ACTIVITY, function (f) {
                return +f.DDD_SAVING_T_HRS || 0;
            });
            e.DPT_D_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_D_SAVING_T_HRS);
            e.DPT_NTS_CLAIM_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_RT_ORIGIN_CLAIM_T_HRS, e.DPT_D_OWNER_DEDUCTION_T_HRS);
            e.DPT_NTS_CLAIM_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_CLAIM_T_HRS);
            e.DPT_NTS_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_RT_TOP_REVIEWED_T_HRS, e.DPT_D_TOP_REVIEWED_T_HRS);
            e.DPT_NTS_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_TOP_REVIEWED_T_HRS);
            e.DPT_NTS_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_RT_SETTLED_T_HRS, e.DPT_D_SETTLED_T_HRS);
            e.DPT_NTS_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_SETTLED_T_HRS);
            e.DPT_NTS_SAVING_T_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(e.DPT_NTS_SETTLED_T_HRS, e.DPT_NTS_CLAIM_T_HRS);
            e.DPT_NTS_SAVING_T_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(e.DPT_NTS_SAVING_T_HRS);
            return e;
        });
        // model = DAFHelper.SetValuePairForTextAndHours(model, 'DDA_TRT_ORIGIN_CLAIM');
        model.DDA_TRT_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_ORIGIN_CLAIM_T_HRS || 0;
        });
        model.DDA_TRT_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_ORIGIN_CLAIM_HRS);
        model.DDA_TRT_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_TOP_REVIEWED_T_HRS || 0;
        });
        model.DDA_TRT_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_TOP_REVIEWED_HRS);
        model.DDA_TRT_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_SETTLED_T_HRS || 0;
        });
        model.DDA_TRT_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_SETTLED_HRS);
        model.DDA_TRT_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_RT_SAVING_T_HRS || 0;
        });
        model.DDA_TRT_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TRT_SAVING_HRS);
        model.DDA_TDT_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_OWNER_DEDUCTION_T_HRS || 0;
        });
        model.DDA_TDT_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_ORIGIN_CLAIM_HRS);
        model.DDA_TDT_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_TOP_REVIEWED_T_HRS || 0;
        });
        model.DDA_TDT_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_TOP_REVIEWED_HRS);
        model.DDA_TDT_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_SETTLED_T_HRS || 0;
        });
        model.DDA_TDT_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_SETTLED_HRS);
        model.DDA_TDT_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_1_lodash__["sumBy"](model.DAF_PORT, function (e) {
            return +e.DPT_D_SAVING_T_HRS || 0;
        });
        model.DDA_TDT_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TDT_SAVING_HRS);
        model.DDA_NTS_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TRT_ORIGIN_CLAIM_HRS, model.DDA_TDT_ORIGIN_CLAIM_HRS);
        model.DDA_NTS_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_ORIGIN_CLAIM_HRS);
        model.DDA_NTS_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TRT_TOP_REVIEWED_HRS, model.DDA_TDT_TOP_REVIEWED_HRS);
        model.DDA_NTS_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_TOP_REVIEWED_HRS);
        model.DDA_NTS_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TRT_SETTLED_HRS, model.DDA_TDT_SETTLED_HRS);
        model.DDA_NTS_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_SETTLED_HRS);
        model.DDA_NTS_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_SETTLED_HRS, model.DDA_NTS_ORIGIN_CLAIM_HRS);
        model.DDA_NTS_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_NTS_SAVING_HRS);
        model.DDA_LY_ORIGIN_CLAIM_HRS = model.DDA_LAYTIME_CONTRACT_HRS;
        model.DDA_LY_ORIGIN_CLAIM_TIME_TEXT = model.DDA_LAYTIME_CONTRACT_TEXT;
        model.DDA_LY_TOP_REVIEWED_HRS = model.DDA_LAYTIME_CONTRACT_HRS;
        model.DDA_LY_TOP_REVIEWED_TIME_TEXT = model.DDA_LAYTIME_CONTRACT_TEXT;
        model.DDA_LY_SETTLED_HRS = model.DDA_LAYTIME_CONTRACT_HRS;
        model.DDA_LY_SETTLED_TIME_TEXT = model.DDA_LAYTIME_CONTRACT_TEXT;
        model.DDA_LY_SAVING_HRS = 0;
        model.DDA_LY_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(0);
        model.DDA_TOD_ORIGIN_CLAIM_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_ORIGIN_CLAIM_HRS, model.DDA_LY_ORIGIN_CLAIM_HRS);
        model.DDA_TOD_ORIGIN_CLAIM_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_ORIGIN_CLAIM_HRS);
        model.DDA_TOD_TOP_REVIEWED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_TOP_REVIEWED_HRS, model.DDA_LY_TOP_REVIEWED_HRS);
        model.DDA_TOD_TOP_REVIEWED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_TOP_REVIEWED_HRS);
        model.DDA_TOD_SETTLED_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_NTS_SETTLED_HRS, model.DDA_LY_SETTLED_HRS);
        model.DDA_TOD_SETTLED_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_SETTLED_HRS);
        model.DDA_TOD_SAVING_HRS = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetDiffHrsHours(model.DDA_TOD_SETTLED_HRS, model.DDA_TOD_ORIGIN_CLAIM_HRS);
        model.DDA_TOD_SAVING_TIME_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TOD_SAVING_HRS);
        model.DDA_TIME_ON_DEM_HRS = model.DDA_TOD_SETTLED_HRS;
        model.DDA_TIME_ON_DEM_TEXT = __WEBPACK_IMPORTED_MODULE_0__date_helper__["a" /* DateHelper */].GetTextFromHours(model.DDA_TIME_ON_DEM_HRS);
        model = DAFHelper.CalValue(model);
        return model;
    };
    DAFHelper.CalGross = function (hrs, pdpr, fraction) {
        var phpr = (pdpr || 0) / 24;
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["round"]((hrs * phpr), fraction);
        // return (hrs * phpr);
    };
    DAFHelper.CalAddressCom = function (value, address_com, fraction) {
        value = value || 0;
        address_com = address_com || 0;
        var result = value * address_com / 100;
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["round"]((result), fraction);
        // return (result);
    };
    DAFHelper.CalSharingToTop = function (value, percentage, fraction) {
        value = value || 0;
        percentage = percentage || 0;
        var result = value * percentage / 100;
        return __WEBPACK_IMPORTED_MODULE_1_lodash__["round"]((result), fraction);
        // return result;
    };
    DAFHelper.CalValue = function (model) {
        model.DDA_GD_ORIGIN_CLAIM_VALUE = DAFHelper.CalGross(model.DDA_TOD_ORIGIN_CLAIM_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_GD_TOP_REVIEWED_VALUE = DAFHelper.CalGross(model.DDA_TOD_TOP_REVIEWED_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_GD_SETTLED_VALUE = DAFHelper.CalGross(model.DDA_TOD_SETTLED_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_GD_SAVING_VALUE = DAFHelper.CalGross(model.DDA_TOD_SAVING_HRS, model.DDA_DEMURAGE_RATE, model.DDA_DECIMAL);
        model.DDA_AC_ORIGIN_CLAIM_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_ORIGIN_CLAIM_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_AC_TOP_REVIEWED_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_TOP_REVIEWED_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_AC_SETTLED_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_SETTLED_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_AC_SAVING_VALUE = DAFHelper.CalAddressCom(model.DDA_GD_SAVING_VALUE, model.DDA_ADDRESS_COMMISSION_PERCEN, model.DDA_DECIMAL);
        model.DDA_STD_ORIGIN_CLAIM_VALUE = model.DDA_GD_ORIGIN_CLAIM_VALUE - model.DDA_AC_ORIGIN_CLAIM_VALUE;
        model.DDA_STD_TOP_REVIEWED_VALUE = model.DDA_GD_TOP_REVIEWED_VALUE - model.DDA_AC_TOP_REVIEWED_VALUE;
        model.DDA_STD_SETTLED_VALUE = model.DDA_GD_SETTLED_VALUE - model.DDA_AC_SETTLED_VALUE;
        model.DDA_STD_SAVING_VALUE = model.DDA_GD_SAVING_VALUE - model.DDA_AC_SAVING_VALUE;
        model.DDA_DST_ORIGIN_CLAIM_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_ORIGIN_CLAIM_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_DST_TOP_REVIEWED_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_TOP_REVIEWED_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_DST_SETTLED_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_SETTLED_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_DST_SAVING_VALUE = DAFHelper.CalSharingToTop(model.DDA_STD_SAVING_VALUE, model.DDA_SHARING_TO_TOP_PERCEN, model.DDA_DECIMAL);
        model.DDA_STD_ORIGIN_CLAIM_VALUE = model.DDA_STD_ORIGIN_CLAIM_VALUE || 0;
        model.DDA_DST_ORIGIN_CLAIM_VALUE = model.DDA_DST_ORIGIN_CLAIM_VALUE || 0;
        model.DDA_STD_TOP_REVIEWED_VALUE = model.DDA_STD_TOP_REVIEWED_VALUE || 0;
        model.DDA_DST_TOP_REVIEWED_VALUE = model.DDA_DST_TOP_REVIEWED_VALUE || 0;
        model.DDA_STD_SETTLED_VALUE = model.DDA_STD_SETTLED_VALUE || 0;
        model.DDA_DST_SETTLED_VALUE = model.DDA_DST_SETTLED_VALUE || 0;
        model.DDA_DST_SAVING_VALUE = model.DDA_DST_SAVING_VALUE || 0;
        model.DDA_STD_SAVING_VALUE = model.DDA_STD_SAVING_VALUE || 0;
        model.DDA_ND_ORIGIN_CLAIM_VALUE = +model.DDA_STD_ORIGIN_CLAIM_VALUE - +model.DDA_DST_ORIGIN_CLAIM_VALUE;
        model.DDA_ND_TOP_REVIEWED_VALUE = +model.DDA_STD_TOP_REVIEWED_VALUE - +model.DDA_DST_TOP_REVIEWED_VALUE;
        model.DDA_ND_SETTLED_VALUE = +model.DDA_STD_SETTLED_VALUE - +model.DDA_DST_SETTLED_VALUE;
        model.DDA_ND_SAVING_VALUE = +model.DDA_STD_SAVING_VALUE - +model.DDA_DST_SAVING_VALUE;
        model.DDA_GD_SETTLED_VALUE = model.DDA_GD_SETTLED_VALUE || 0;
        if (model.DDA_IS_BROKER_COMMISSION === 'Y') {
            model.DDA_BROKER_COMMISSION_PERCEN = model.DDA_BROKER_COMMISSION_PERCEN || null;
        }
        if (model.DDA_GD_SETTLED_VALUE && model.DDA_BROKER_COMMISSION_PERCEN) {
            // model.DDA_BROKER_COMMISSION_VALUE = _.round(model.DDA_GD_SETTLED_VALUE * model.DDA_BROKER_COMMISSION_PERCEN / 100, model.DDA_DECIMAL);
            // model.DDA_BROKER_COMMISSION_VALUE = model.DDA_GD_SETTLED_VALUE * model.DDA_BROKER_COMMISSION_PERCEN / 100;
            model.DDA_BROKER_COMMISSION_VALUE = __WEBPACK_IMPORTED_MODULE_1_lodash__["round"](model.DDA_GD_SETTLED_VALUE * model.DDA_BROKER_COMMISSION_PERCEN / 100, 2);
        }
        else {
            model.DDA_BROKER_COMMISSION_VALUE = 0;
        }
        model.DDA_NET_PAYMENT_DEM_VALUE = __WEBPACK_IMPORTED_MODULE_1_lodash__["clone"](model.DDA_ND_SETTLED_VALUE);
        model.DDA_NET_PAYMENT_DEM_VALUE = __WEBPACK_IMPORTED_MODULE_1_lodash__["round"](model.DDA_NET_PAYMENT_DEM_VALUE, 2);
        model.DDA_NET_PAYMENT_DEM_VALUE_OV = model.DDA_IS_OVERRIDE_NET_PAYMENT == 'Y' ? __WEBPACK_IMPORTED_MODULE_1_lodash__["round"](model.DDA_NET_PAYMENT_DEM_VALUE_OV, 2) : __WEBPACK_IMPORTED_MODULE_1_lodash__["round"](__WEBPACK_IMPORTED_MODULE_1_lodash__["clone"](model.DDA_NET_PAYMENT_DEM_VALUE), 2);
        return model;
    };
    return DAFHelper;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/cip-reference/cip-reference.component.html":
/***/ (function(module, exports) {

module.exports = "<button class=\"btn btn-success btn-sm text-light\" [disabled]=\"disabled\" (click)=\"modal.show()\">Search</button>\n\n\n<ng-template [ngIf]=\"IsCMMT()\">\n  <div bsModal #modal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h3 class=\"modal-title pull-left\">Search</h3>\n          <div class=\"exit\" (click)=\"modal.hide()\" role=\"button\" tabindex=\"0\"></div>\n          <!-- <button type=\"button\" class=\"close pull-right\" (click)=\"modal.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button> -->\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"row\">\n            <div class=\"col-lg-10 col-lg-offset-1\">\n              <div class=\"form-group\">\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Document No</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.DOC_NO\">\n                      </div>\n                  </div>\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Vessel Name</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.VESSEL_NAME\">\n                      </div>\n                  </div>\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Charterer</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.CHARTERER\">\n                      </div>\n                  </div>\n              </div>\n              <div class=\"form-group\">\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Laycan</label>\n                      <div class=\"col-md-7\">\n                        <ptx-input-date-range\n                            [disabled]=\"false\"\n                            [start_date]=\"from\"\n                            [end_date]=\"to\"\n                            (modelChange)=\"model.LAYCAN_FROM = $event.start_date;model.LAYCAN_TO = $event.end_date;\"\n                        >\n                        </ptx-input-date-range>\n                      </div>\n                  </div>\n                  <div class=\"col-md-4\">\n                      <label class=\"col-md-3 control-label\">Create By</label>\n                      <div class=\"col-md-7\">\n                            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.CREATED_BY\">\n                      </div>\n                  </div>\n              </div>\n              <div class=\"text-center\" id=\"buttonDiv\">\n                <button class=\"btn btn-success\" (click)=\"DoSearch()\"> Search </button>\n              </div>\n            </div>\n          </div>\n          <br />\n          <div class=\"table-responsive\">\n            <table id=\"ptxDataTable\" class=\"tble table-hover table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">\n            </table>\n          </div>\n        </div>\n        <!--<div class=\"modal-footer\">-->\n          <!--n-->\n        <!--</div>-->\n      </div>\n    </div>\n  </div>\n</ng-template>\n\n<ng-template [ngIf]=\"!IsCMMT()\">\n  <div bsModal #modal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-lg\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h3 class=\"modal-title pull-left\">Search</h3>\n          <div class=\"exit\" (click)=\"modal.hide()\" role=\"button\" tabindex=\"0\"></div>\n          <!-- <button type=\"button\" class=\"close pull-right\" (click)=\"modal.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button> -->\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"row\">\n            <div class=\"col-lg-10 col-lg-offset-1\">\n              <div class=\"form-group\">\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Trip No</label>\n                  <div class=\"col-md-7\">\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.DOC_NO\">\n                  </div>\n                </div>\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Vessel Name</label>\n                  <div class=\"col-md-7\">\n                    <input type=\"text\" class=\"form-control\"  [(ngModel)]=\"model.VESSEL_NAME\">\n                  </div>\n                </div>\n                <!--<div class=\"col-md-4\">-->\n                  <!--<label class=\"col-md-3 control-label\">Supplier</label>-->\n                  <!--<div class=\"col-md-7\">-->\n                    <!--<input type=\"text\" class=\"form-control\"  [(ngModel)]=\"model.SUPPLIER_NAME\">-->\n                  <!--</div>-->\n                <!--</div>-->\n              </div>\n              <div class=\"form-group\">\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Arrival Date</label>\n                  <div class=\"col-md-7\">\n                    <ptx-input-date-range\n                      [disabled]=\"false\"\n                      [start_date]=\"model.DISCHARGE_FROM\"\n                      [end_date]=\"model.DISCHARGE_TO\"\n                      (modelChange)=\"model.DISCHARGE_FROM = $event.start_date;model.DISCHARGE_TO = $event.end_date;\"\n                    >\n                    </ptx-input-date-range>\n                  </div>\n                </div>\n                <div class=\"col-md-5\">\n                  <label class=\"col-md-3 control-label\">Loading Date</label>\n                  <div class=\"col-md-7\">\n                    <ptx-input-date-range\n                      [disabled]=\"false\"\n                      [start_date]=\"model.LOADING_FROM\"\n                      [end_date]=\"model.LOADING_TO\"\n                      (modelChange)=\"model.LOADING_FROM = $event.start_date;model.LOADING_TO = $event.end_date;\"\n                    >\n                    </ptx-input-date-range>\n                  </div>\n                </div>\n              </div>\n              <div class=\"text-center\" id=\"buttonDiv\">\n                <button class=\"btn btn-success\" (click)=\"DoSearch()\"> Search </button>\n              </div>\n            </div>\n          </div>\n          <br />\n          <div class=\"table-responsive\">\n            <table id=\"ptxDataTable\" class=\"tble table-hover table-striped table-bordered\" cellspacing=\"0\" width=\"100%\">\n            </table>\n          </div>\n        </div>\n        <!--<div class=\"modal-footer\">-->\n        <!--n-->\n        <!--</div>-->\n      </div>\n    </div>\n  </div>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/cip-reference/cip-reference.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CipReferenceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_cip_reference_service__ = __webpack_require__("./src/app/services/cip-reference.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_cho_reference_service__ = __webpack_require__("./src/app/services/cho-reference.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CipReferenceComponent = /** @class */ (function () {
    function CipReferenceComponent(el, cipReferenceService, choReferenceService) {
        this.el = el;
        this.cipReferenceService = cipReferenceService;
        this.choReferenceService = choReferenceService;
        this.config = {
            animated: true,
            keyboard: true,
            backdrop: true,
            ignoreBackdropClick: true
        };
        this.model = {};
        this.user_group = "";
        this.disabled = false;
        this.onSelectData = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.onSearchCIPRef = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    CipReferenceComponent.prototype.ngOnInit = function () {
        this.updateTable();
        // const context = this;
        // $(() => {
        //   const ptxDataTable = $(context.el.nativeElement).find("#ptxDataTable").DataTable({
        //     "data": context.data_set,
        //     "ordering": false,
        //     "bSort": false,
        //     "dom": '<"toolbar">rtip',
        //     "columnDefs": [
        //       {"className": "dt-center", "targets": "_all"}
        //     ],
        //     "columns": [
        //         {
        //           "title": "Trip No.",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Vessel Name",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Supplier",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Crude Name",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Incoterms",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Arrival Date",
        //           "data": "id"
        //         },
        //         {
        //           "title": "Loading Date",
        //           "data": "id"
        //         },
        //     ],
        //   });
        //
        //   $(context.el.nativeElement).find("#ptxDataTable tbody tr").on("click", function(event){
        //     const select_data = ptxDataTable.row(this).data();
        //     context.onSelectData.emit(select_data);
        //     context.modal.hide();
        //   });
        //
        //   this.table = ptxDataTable;
        // });
    };
    CipReferenceComponent.prototype.IsCMMT = function () {
        return this.user_group.toUpperCase() === 'CMMT';
    };
    CipReferenceComponent.prototype.DoSearch = function () {
        var context = this;
        var model = context.model;
        console.log(model);
        var dialog = bootbox.dialog({
            title: 'Processing.....',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
        });
        dialog.init(function () {
            if (context.user_group.toUpperCase() === 'CMMT') {
                context.choReferenceService.search(model).subscribe(function (res) {
                    context.data_set = res;
                    context.updateTable();
                    // this.table.draw();
                    console.log(context.data_set);
                    dialog.modal('hide');
                }, function (error) {
                    console.log(error);
                    dialog.modal('hide');
                });
            }
            else if (context.user_group.toUpperCase() === 'CMCS') {
                context.cipReferenceService.getCIPCrudeData(model).subscribe(function (res) {
                    context.data_set = res;
                    context.updateTable();
                    // this.table.draw();
                    dialog.modal('hide');
                    console.log(context.data_set);
                }, function (error) {
                    console.log(error);
                    console.log(context.data_set);
                    dialog.modal('hide');
                });
            }
            else if (context.user_group.toUpperCase() === 'CMPS') {
                context.cipReferenceService.getCIPProductData(model).subscribe(function (res) {
                    context.data_set = res;
                    context.updateTable();
                    // this.table.draw();
                    dialog.modal('hide');
                    console.log(context.data_set);
                }, function (error) {
                    console.log(error);
                    console.log(context.data_set);
                    dialog.modal('hide');
                });
            }
            // setTimeout(function(){
            // dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');
            // }, 3000);
        });
    };
    CipReferenceComponent.prototype.updateTable = function () {
        var context = this;
        if (this.table) {
            this.table.destroy();
        }
        this.table = $(context.el.nativeElement).find("#ptxDataTable").DataTable({
            "data": context.data_set,
            "ordering": false,
            "bSort": true,
            "dom": '<"toolbar">rtip',
            "columnDefs": [
                { "className": "dt-center", "targets": "_all" }
            ],
            "columns": context.MappingColumn(),
        });
        // $(context.el.nativeElement).find("#ptxDataTable tbody tr").on("click", function(event){
        //   const select_data = context.table.row(this).data();
        //   context.onSelectData.emit(select_data);
        //   context.modal.hide();
        // });
        $(context.el.nativeElement).find('#ptxDataTable tbody').on('click', 'tr', function (event) {
            var select_data = context.table.row(this).data();
            context.onSelectData.emit(select_data);
            context.modal.hide();
        });
    };
    CipReferenceComponent.prototype.MappingColumn = function () {
        var result;
        if (this.IsCMMT()) {
            result = this.MappingCMMT();
        }
        else {
            result = this.MappingOther();
        }
        return result;
    };
    CipReferenceComponent.prototype.MappingCMMT = function () {
        return [
            {
                'title': 'Date',
                'data': 'SHOW_DOC_DATE'
            },
            {
                'title': 'Documnent No.',
                'data': 'TRIP_NO'
            },
            {
                'title': 'Vessel Name',
                'data': 'VESSEL_NAME'
            },
            {
                'title': 'Laycan',
                'data': 'SHOW_LAYCAN_DATE'
            },
            {
                'title': 'Charterer',
                'data': 'CUSTOMER_NAME'
            },
            {
                'title': 'Created By',
                'data': 'CREATED_BY'
            }
        ];
    };
    CipReferenceComponent.prototype.MappingOther = function () {
        return [
            {
                'title': 'Trip No.',
                'data': 'TRIP_NO'
            },
            {
                'title': 'Vessel Name',
                'data': 'VESSEL_NAME'
            },
            // {
            //   'title': 'Supplier',
            //   'data': 'SUPPLIER_NAME'
            // },
            {
                'title': 'Crude Name',
                'data': 'CRUDE_NAMES'
            },
            {
                'title': 'Incoterms',
                'data': 'INCOTERMS'
            },
            {
                'title': 'Arrival Date',
                'data': 'SHOW_ACTUAL_ETA'
            },
            {
                'title': 'Loading Date',
                'data': 'SHOW_LOADING_LAYCAN'
            },
            {
                'title': 'Created By',
                'data': 'CREATED_BY'
            }
        ];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["b" /* ModalDirective */])
    ], CipReferenceComponent.prototype, "modal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CipReferenceComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], CipReferenceComponent.prototype, "user_group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], CipReferenceComponent.prototype, "disabled", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], CipReferenceComponent.prototype, "data_set", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CipReferenceComponent.prototype, "onSelectData", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CipReferenceComponent.prototype, "onSearchCIPRef", void 0);
    CipReferenceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-cip-reference',
            template: __webpack_require__("./src/app/demurrage-approval/components/cip-reference/cip-reference.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */],
            __WEBPACK_IMPORTED_MODULE_2__services_cip_reference_service__["a" /* CipReferenceService */],
            __WEBPACK_IMPORTED_MODULE_3__services_cho_reference_service__["a" /* ChoReferenceService */]])
    ], CipReferenceComponent);
    return CipReferenceComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/deduction-modal/deduction-modal.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<button type=\"button\" class=\"btn btn-primary\" (click)=\"openModalWithClass(template)\">Open modal with custom css class</button>-->\n<div class=\"row mt-15 text-center\"><a class=\"btn btn-default btn-sm\" (click)=\"lgModal.show()\">Manage Activity</a></div>\n\n\n<div bsModal #lgModal=\"bs-modal\" class=\"modal fade modal-fullscreen\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h3 class=\"modal-title pull-left\">Deduction</h3>\n        <div class=\"exit\" (click)=\"lgModal.hide()\" role=\"button\" tabindex=\"0\"></div>\n        <!--<button type=\"button\" class=\"close pull-right\" (click)=\"lgModal.hide()\" aria-label=\"Close\">-->\n          <!--<span aria-hidden=\"true\">&times;</span>-->\n        <!--</button>-->\n      </div>\n      <div class=\"modal-body\">\n        <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n          <tbody>\n            <tr>\n              <td class=\"text-bold text-center ptx-w-300\">Activity</td>\n              <td class=\"text-bold text-center ptx-w-100\"></td>\n              <td class=\"text-bold text-center ptx-w-300\">Owner Deduction</td>\n              <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n              <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n              <td class=\"text-bold text-center ptx-w-300\">Settled</td>\n              <td class=\"ptx-w-50 text-center\"></td>\n            </tr>\n            <ng-template ngFor let-e [ngForOf]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY\" let-j=\"index\">\n              <tr *ngIf=\"e.DDD_TYPE == 'fixed'\">\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <input type=\"text\"\n                           class=\"form-control\"\n                           [disabled]=\"model.IS_DISABLED\"\n                           [(ngModel)]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_ACTIVITY\"\n                    >\n                  </div>\n                </td>\n                <td valign=\"top\" >\n                </td>\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'days')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_OWNER_DEDUCTION_T_TEXT', 'days', $event);HandleCopyChange(index, j, 'origin');\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Day</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'hrs')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_OWNER_DEDUCTION_T_TEXT', 'hrs', $event);HandleCopyChange(index, j, 'origin');\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Hrs</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'mins')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_OWNER_DEDUCTION_T_TEXT', 'mins', $event);HandleCopyChange(index, j, 'origin');\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline\">Mins</div>\n                  </div>\n                </td>\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'days')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_TOP_REVIEWED_T_TEXT', 'days', $event);HandleCopyChange(index, j, 'top');OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Day</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'hrs')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_TOP_REVIEWED_T_TEXT', 'hrs', $event);HandleCopyChange(index, j, 'top');OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Hrs</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'mins')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_TOP_REVIEWED_T_TEXT', 'mins', $event);HandleCopyChange(index, j, 'top');OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline\">Mins</div>\n                  </div>\n                </td>\n                <td valign=\"top\" >\n                  <div class=\"row mb-5\">\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'days')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_SETTLED_T_TEXT', 'days', $event);OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Day</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'hrs')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_SETTLED_T_TEXT', 'hrs', $event);OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline mr-10\">Hrs</div>\n                    <div class=\"form-inline\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm ptx-w-60\"\n                             [ngModel]=\"GetText(model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'mins')\"\n                             (ngModelChange)=\"SetText(index, j, 'DDD_SETTLED_T_TEXT', 'mins', $event);OnModelChange();\"\n                             [disabled]=\"model.IS_DISABLED\"\n                      >\n                    </div>\n                    <div class=\"text-inline\">Mins</div>\n                  </div>\n                </td>\n                <td class=\"ptx-w-50 text-center\">\n                  <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"RemoveRow(j)\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </td>\n              </tr>\n              <tr *ngIf=\"e.DDD_TYPE == 'interval'\">\n                <td valign=\"top\" >\n                  <input type=\"text\"\n                         class=\"form-control\"\n                         [disabled]=\"model.IS_DISABLED\"\n                         [(ngModel)]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_ACTIVITY\"\n                  >\n                </td>\n                <td>\n                  <div class=\"row mb-5\">\n                    <div class=\"col-xs-12 pl-0\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm\"\n                             value=\"Start\"\n                             disabled>\n                    </div>\n                  </div>\n                  <div class=\"row mb-5\">\n                    <div class=\"col-xs-12 pl-0\">\n                      <input type=\"text\"\n                             class=\"form-control text-center text-sm\"\n                             value=\"Stop\"\n                             disabled>\n                    </div>\n                  </div>\n                </td>\n                <td>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_START\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_START = $event; HandleCopyChange(index, j, 'origin'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_STOP\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_STOP = $event; HandleCopyChange(index, j, 'origin'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                </td>\n                <td>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START = $event; HandleCopyChange(index, j, 'top'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP = $event; HandleCopyChange(index, j, 'top'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                </td>\n                <td>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_START\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_START = $event; HandleCopyChange(index, j, 'settled'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                  <ptx-input-date-time\n                    [input]=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_STOP\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    (handleModelChange)=\"model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_STOP = $event; HandleCopyChange(index, j, 'settled'); OnModelChange();\"\n                  ></ptx-input-date-time>\n                </td>\n                <td class=\"ptx-w-50 text-center\">\n                  <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"RemoveRow(j)\">\n                    <span class=\"glyphicon glyphicon-trash\"></span>\n                  </a>\n                </td>\n              </tr>\n            </ng-template>\n            <!--<tr class=\"border-bottom-0 border-top-0\">-->\n              <!--<td valign=\"top\">-->\n              <!--</td>-->\n            <!--</tr>-->\n            <!--<tr class=\"border-top-0\">-->\n              <!--<td valign=\"top\">-->\n              <!--</td>-->\n            <!--</tr>-->\n            <!--<tr>-->\n              <!--<td valign=\"top\">-->\n              <!--</td>-->\n            <!--</tr>-->\n          </tbody>\n        </table>\n        <div class=\"wrapper-button text-left\">\n          <button class=\"btn btn-primary btn-xs\" (click)=\"DoAddFixedTime(index, j)\"> Add Fixed Time </button>\n          <button class=\"btn btn-primary btn-xs\" (click)=\"DoAddInterValTime(index, j)\"> Add Time Interval</button>\n        </div>\n        <div class=\"wrapper-button\" id=\"buttonDiv\">\n          <button class=\"btn btn-success\" (click)=\"OnDone();lgModal.hide()\"> Done </button>\n          <!--<button class=\"btn btn-default\" (click)=\"lgModal.hide()\"> Cancel </button>-->\n        </div>\n      </div>\n      <!--<div class=\"modal-footer text-center\">-->\n      <!--n-->\n      <!--</div>-->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/deduction-modal/deduction-modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeductionModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__ = __webpack_require__("./node_modules/ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_deduction_activity__ = __webpack_require__("./src/app/models/daf-deduction-activity.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_date_helper__ = __webpack_require__("./src/app/common/date-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__ = __webpack_require__("./src/app/common/daf-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_utility__ = __webpack_require__("./src/app/common/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DeductionModalComponent = /** @class */ (function () {
    function DeductionModalComponent(modalService) {
        this.modalService = modalService;
        this.model = new __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */]();
        this.index = 0;
        this.config = {
            animated: true,
            keyboard: true,
            backdrop: true,
            ignoreBackdropClick: true
        };
    }
    DeductionModalComponent.prototype.HandleCopyChange = function (index, j, type) {
        if (type === 'origin') {
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_START);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_STOP);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_HRS = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_HRS);
            this.HandleCopyChange(index, j, 'top');
        }
        else if (type === 'top') {
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_START = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_START);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_STOP = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_STOP);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT);
            this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_HRS = __WEBPACK_IMPORTED_MODULE_7__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_HRS);
        }
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    DeductionModalComponent.prototype.DoAddFixedTime = function (index, j) {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY.push(new __WEBPACK_IMPORTED_MODULE_3__models_daf_deduction_activity__["a" /* DAF_DEDUCTION_ACTIVITY */]('fixed'));
    };
    DeductionModalComponent.prototype.DoAddInterValTime = function (index, j) {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY.push(new __WEBPACK_IMPORTED_MODULE_3__models_daf_deduction_activity__["a" /* DAF_DEDUCTION_ACTIVITY */]('interval'));
    };
    DeductionModalComponent.prototype.GetText = function (text, type) {
        return __WEBPACK_IMPORTED_MODULE_4__common_date_helper__["a" /* DateHelper */].GetNumberFromTextByType(text, type);
    };
    DeductionModalComponent.prototype.SetText = function (index, j, key, type, value) {
        var old = this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j][key];
        this.model.DAF_PORT[index].DAF_DEDUCTION_ACTIVITY[j][key] = __WEBPACK_IMPORTED_MODULE_4__common_date_helper__["a" /* DateHelper */].SetNumberToTextByType(old, type, value);
        this.OnModelChange();
    };
    DeductionModalComponent.prototype.OnModelChange = function () {
        console.log('sss');
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    DeductionModalComponent.prototype.RemoveRow = function (j) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var target = this.model.DAF_PORT[this.index].DAF_DEDUCTION_ACTIVITY[j];
        this.model.DAF_PORT[this.index].DAF_DEDUCTION_ACTIVITY = __WEBPACK_IMPORTED_MODULE_6_lodash__["reject"](this.model.DAF_PORT[this.index].DAF_DEDUCTION_ACTIVITY, function (e) {
            return e == target;
        });
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    DeductionModalComponent.prototype.openModal = function (template) {
        this.modalRef = this.modalService.show(template, this.config);
    };
    DeductionModalComponent.prototype.openModalWithClass = function (template) {
        this.modalRef = this.modalService.show(template, Object.assign({}, this.config, { class: 'gray modal-fullscreen' }));
    };
    DeductionModalComponent.prototype.OnDone = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_5__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */])
    ], DeductionModalComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], DeductionModalComponent.prototype, "index", void 0);
    DeductionModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-deduction-modal',
            template: __webpack_require__("./src/app/demurrage-approval/components/deduction-modal/deduction-modal.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal__["a" /* BsModalService */]])
    ], DeductionModalComponent);
    return DeductionModalComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/demurage-approval/demurrage-approval.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<pre>{{ model | json }}</pre>-->\n<div class=\"wrapper-detail\">\n  <h2 class=\"first-title\">\n    <span class=\"note\">Demurrage Approval Form </span> Please fill in form as relevant\n  </h2>\n  <div class=\"form-horizontal mb-30\">\n    <div class=\"row\">\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Form ID</label>\n          <div class=\"col-xs-5 pr-10\">\n            <input\n              class=\"form-control\"\n              [(ngModel)]=\"model.DDA_FORM_ID\"\n              type=\"text\"\n              [disabled]=\"true\"\n            >\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Create Date</label>\n          <div class=\"col-xs-5 pr-10\">\n            <ptx-input-date\n              [needtime]=\"true\"\n              [date]=\"model.DDA_DOC_DATE\"\n              (modelChange)=\"model.DDA_DOC_DATE = $event;\"\n              [disabled]=\"model.IS_DISABLED\"\n            ></ptx-input-date>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">For <span style=\"color:red\">*</span></label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_FOR_COMPANY\"\n                    (ngModelChange)=\"OnChangeCompany(model.DDA_FOR_COMPANY)\"\n            >\n              <option value=\"\">-- Select --</option>\n              <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.COMPANY\">{{ elem.VALUE }}</option>\n            </select>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Type of Transaction</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_TYPE_OF_TRANSECTION\"\n            >\n              <option value=\"\">-- select --</option>\n              <option value=\"Sale\">Sale</option>\n              <option value=\"Purchase\">Purchase</option>\n            </select>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Incoterms</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_INCOTERM\"\n            >\n              <option value=\"\">-- Select --</option>\n              <option [ngValue]=\"elem.ID\" *ngFor=\"let elem of master.INCOTERMS\">{{ elem.VALUE }}</option>\n            </select>\n            <!--<ptx-input-suggestion-ui-->\n            <!--[model]=\"model.DDA_INCOTERM\"-->\n            <!--[data_list]=\"master.INCOTERMS\"-->\n            <!--[is_disabled]=\"model.IS_DISABLED\"-->\n            <!--[show_field]=\"'VALUE'\"-->\n            <!--select_field=\"ID\"-->\n            <!--(ModelChanged)=\"model.DDA_INCOTERM = $event;\"-->\n            <!--&gt;-->\n            <!--</ptx-input-suggestion-ui>-->\n          </div>\n        </div>\n      </div>\n      <div class=\"col-xs-12 col-sm-4\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Demurrage</label>\n          <div class=\"col-xs-5 pr-10\">\n            <select class=\"form-control\"\n                    [disabled]=\"model.IS_DISABLED\"\n                    [(ngModel)]=\"model.DDA_DEMURAGE_TYPE\"\n                    (ngModelChange)=\"onUpdateDemType()\"\n            >\n              <option value=\"\">-- select --</option>\n              <option value=\"Paid\">Paid</option>\n              <option value=\"Received\">Received</option>\n            </select>\n          </div>\n        </div>\n      </div>\n    </div>\n    <ng-template [ngIf]=\"IsNeedReferrence()\">\n      <h2 class=\"second-title\">\n        Reference\n      </h2>\n      <div class=\"form-horizontal mb-30\">\n\n        <div class=\"row\">\n          <div class=\"col-xs-12 col-sm-4\">\n            <div class=\"form-group\">\n              <label class=\"col-xs-5 control-label\">Trip No./Document No.</label>\n              <div class=\"col-xs-5\">\n                <input type=\"text\"\n                       class=\"form-control\"\n                       [ngModel]=\"model.DDA_REF_DOC_NO\"\n                       [disabled]=\"model.IS_DISABLED\"\n                >\n              </div>\n              <div class=\"col-xs-2 p-0\">\n                <ptx-cip-reference\n                  [user_group]=\"model.DDA_USER_GROUP\"\n                  [disabled]=\"model.IS_DISABLED\"\n                  [data_set] = \"cip_data\"\n                  (onSelectData)=\"OnCIPSelectData($event)\"\n                  (onSearchCIPRef)=\"OnSearchCIPRef($event)\"\n                ></ptx-cip-reference>\n                <!--<button class=\"btn btn-success btn-sm text-light\">Search</button>-->\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </ng-template>\n    <h2 class=\"second-title\">\n      Details\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-4\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-5 control-label\">Counterparty<span style=\"color:red\">*</span></label>\n            <div class=\"col-xs-7\">\n              <div class=\"radio\">\n                <input id=\"ship_owner\"\n                       [disabled]=\"model.IS_DISABLED\"\n                       name=\"DDA_COUNTERPARTY_TYPE\"\n                       type=\"radio\"\n                       [(ngModel)]=\"model.DDA_COUNTERPARTY_TYPE\"\n                       [value]=\"'Ship Owner'\"\n                >\n                <label for=\"ship_owner\">Ship Owner</label>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-7\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-5\">\n              <input type=\"text\"\n                     class=\"form-control\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_SHIP_OWNER\"\n              >\n            </div>\n            <div class=\"col-xs-2 p-0 text-center\">\n              <div class=\"text text-black\">via Broker</div>\n            </div>\n            <div class=\"col-xs-5\">\n              <ptx-input-suggestion-ui\n                [model]=\"model.DDA_BROKER\"\n                [data_list]=\"master.BROKERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_BROKER = $event;\"\n              >\n              </ptx-input-suggestion-ui>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-4\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-5 control-label\"></label>\n            <div class=\"col-xs-7\">\n              <div class=\"radio\">\n                <input id=\"supplier\"\n                       name=\"DDA_COUNTERPARTY_TYPE\"\n                       type=\"radio\"\n                       [disabled]=\"model.IS_DISABLED\"\n                       [(ngModel)]=\"model.DDA_COUNTERPARTY_TYPE\"\n                       [value]=\"'Supplier as charterer'\"\n                >\n                <label for=\"supplier\">Supplier as charterer</label>\n                <!--<input id=\"radiobox1\" name=\"test1\"  type=\"radio\">-->\n                <!--<label for=\"radiobox1\">Supplier as charterer</label>-->\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-7\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-12\">\n              <ptx-input-suggestion-ui\n                [model]=\"model.DDA_SUPPLIER\"\n                [data_list]=\"master.BROKERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_SUPPLIER = $event;\"\n              >\n              </ptx-input-suggestion-ui>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-4\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-5 control-label\"></label>\n            <div class=\"col-xs-7\">\n              <div class=\"radio\">\n                <input id=\"customer\"\n                       [disabled]=\"model.IS_DISABLED\"\n                       name=\"DDA_COUNTERPARTY_TYPE\"\n                       type=\"radio\"\n                       [(ngModel)]=\"model.DDA_COUNTERPARTY_TYPE\"\n                       [value]=\"'Customer'\"\n                >\n                <label for=\"customer\">Customer</label>\n                <!--<input id=\"radiobox1\" name=\"test1\"  type=\"radio\">-->\n                <!--<label for=\"radiobox1\">Customer</label>-->\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-7\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-5\">\n              <ptx-input-suggestion-ui\n                [model]=\"model.DDA_CUSTOMER\"\n                [data_list]=\"master.CUSTOMERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_CUSTOMER = $event;\"\n              >\n              </ptx-input-suggestion-ui>\n            </div>\n            <div class=\"col-xs-2 p-0 text-center\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n              <div class=\"text text-black\">via Broker</div>\n            </div>\n            <div class=\"col-xs-5\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n              <ptx-input-suggestion-ui\n                [model]=\"model.DDA_CUSTOMER_BROKER\"\n                [data_list]=\"master.BROKERS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_CUSTOMER_BROKER = $event;\"\n              >\n              </ptx-input-suggestion-ui>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">GT&amp;C or CP Type</label>\n            <div class=\"col-xs-7\">\n              <textarea class=\"form-control\"\n                        rows=\"4\"\n                        style=\"min-height: auto; padding-top: 7px;\"\n                        [disabled]=\"model.IS_DISABLED\"\n                        [(ngModel)]=\"model.DDA_GT_C\"\n              ></textarea>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Laytime Contract <span style=\"color:red\">*</span></label>\n            <div class=\"col-xs-6\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [padding]=\"0\"\n                     [fraction]=\"4\"\n                     [(ngModel)]=\"model.DDA_LAYTIME_CONTRACT_HRS\"\n                     (ngModelChange)=\"OnModelChange();\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [(ngModel)]=\"model.DDA_LAYTIME_CONTRACT_TEXT\"\n                     disabled\n              >\n            </div>\n            <div class=\"col-xs-2 p-0\">\n              <div class=\"text text-black\">Hrs</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Vessel Name <span style=\"color:red\">*</span></label>\n            <div class=\"col-xs-7\">\n              <ptx-input-suggestion-ui\n                [model]=\"model.DDA_VESSEL\"\n                [data_list]=\"master.VESSELS\"\n                [is_disabled]=\"model.IS_DISABLED\"\n                [show_field]=\"'VALUE'\"\n                select_field=\"ID\"\n                (ModelChanged)=\"model.DDA_VESSEL = $event;\"\n              >\n              </ptx-input-suggestion-ui>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <div class=\"col-xs-4\"></div>\n            <div class=\"col-xs-6\">\n              <div class=\"form-inline\" *ngIf=\"IsCMLAGroup() || IsCMMTGroup()\">\n                <input type=\"text\"\n                       class=\"form-control\"\n                       placeholder=\"MT/HOURS\"\n                       style=\"width: 49%;\"\n                       ptxRestrictNumber\n                       [(ngModel)]=\"model.DDA_LAYTIME_QUANTITY_RATE\"\n                       (ngModelChange)=\"doUpdateLaytime()\"\n                       [disabled]=\"model.IS_DISABLED\"\n                >\n                <input type=\"text\"\n                       class=\"form-control\"\n                       placeholder=\"Quantity\"\n                       style=\"width: 49%;\"\n                       ptxRestrictNumber\n                       [(ngModel)]=\"model.DDA_LAYTIME_QUANTITY\"\n                       (ngModelChange)=\"doUpdateLaytime()\"\n                       [disabled]=\"model.IS_DISABLED\"\n                >\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">CP Date/Contract Date</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date\n                [date]=\"model.DDA_CONTRACT_DATE\"\n                (modelChange)=\"model.DDA_CONTRACT_DATE = $event;\"\n                [disabled]=\"model.IS_DISABLED\"\n              ></ptx-input-date>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Currency Unit</label>\n            <div class=\"col-xs-6\">\n              <select class=\"form-control\"\n                      [(ngModel)]=\"model.DDA_CURRENCY_UNIT\"\n                      [disabled]=\"model.IS_DISABLED\"\n              >\n                <option [value]=\"elem.ID\" *ngFor=\"let elem of master.DAF_CURRENCY_UNIT\">{{ elem.VALUE }}</option>\n              </select>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n            <label class=\"col-xs-4 control-label\">Agreed Loading Laycan</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date-range\n                [disabled]=\"model.IS_DISABLED\"\n                [start_date]=\"model.DDA_AGREED_LOADING_LAYCAN_FROM\"\n                [end_date]=\"model.DDA_AGREED_LOADING_LAYCAN_TO\"\n                (modelChange)=\"model.DDA_AGREED_LOADING_LAYCAN_FROM = $event.start_date;model.DDA_AGREED_LOADING_LAYCAN_TO = $event.end_date;\"\n              ></ptx-input-date-range>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Demurrage Rate <span style=\"color:red\">*</span></label>\n            <div class=\"col-xs-6\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"2\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_DEMURAGE_RATE\"\n                     (ngModelChange)=\"OnModelChange()\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n            <div class=\"col-xs-2 p-0\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }} PDPR</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group mb-15\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n            <label class=\"col-xs-4 control-label\">Agreed Discharging Laycan</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date-range\n                [disabled]=\"model.IS_DISABLED\"\n                [start_date]=\"model.DDA_AGREED_DC_LAYCAN_FROM\"\n                [end_date]=\"model.DDA_AGREED_DC_LAYCAN_TO\"\n                (modelChange)=\"model.DDA_AGREED_DC_LAYCAN_FROM = $event.start_date;model.DDA_AGREED_DC_LAYCAN_TO = $event.end_date;\"\n              ></ptx-input-date-range>\n            </div>\n          </div>\n          <div class=\"form-group \" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n            <label class=\"col-xs-4 control-label\">Agreed Laycan</label>\n            <div class=\"col-xs-7\">\n              <ptx-input-date-range\n                [disabled]=\"model.IS_DISABLED\"\n                [start_date]=\"model.DDA_AGREED_LAYCAN_FROM\"\n                [end_date]=\"model.DDA_AGREED_LAYCAN_TO\"\n                (modelChange)=\"model.DDA_AGREED_LAYCAN_FROM = $event.start_date;model.DDA_AGREED_LAYCAN_TO = $event.end_date;\"\n              ></ptx-input-date-range>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n        </div>\n      </div>\n      <div class=\"row mt-10\">\n        <ptx-demurrage-material\n          [model]=\"model\"\n          [master]=\"master\"\n        >\n        </ptx-demurrage-material>\n        <div class=\"col-xs-12 col-sm-4\"></div>\n      </div>\n      <div class=\"row mt-10\">\n        <div class=\"col-xs-12\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-2 control-label\">Sailing Route</label>\n            <div class=\"col-xs-8\">\n              <input type=\"text\"\n                     class=\"form-control\"\n                     [(ngModel)]=\"model.DDA_SAILING_ROUTE\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <h2 class=\"second-title\">\n      Calculation\n    </h2>\n\n    <ptx-demurrage-port\n      [user_group]=\"user_group\"\n      title=\"Load Port\"\n      button_title=\"Add Load Port\"\n      type=\"load\"\n      [model]=\"model\"\n      [master]=\"master\"\n      (handleModelChange)=\"OnModelChange();\"\n    ></ptx-demurrage-port>\n\n    <ptx-demurrage-port\n      [user_group]=\"user_group\"\n      title=\"Discharge Port\"\n      button_title=\"Add Discharge Port\"\n      type=\"discharge\"\n      [model]=\"model\"\n      [master]=\"master\"\n      (handleModelChange)=\"OnModelChange();\"\n    ></ptx-demurrage-port>\n\n    <ptx-demurrage-time-summary\n      [model]=\"model\"\n      [master]=\"master\"\n    >\n    </ptx-demurrage-time-summary>\n\n    <ptx-demurrage-value-usd-summary\n      [model]=\"model\"\n      [master]=\"master\"\n    ></ptx-demurrage-value-usd-summary>\n\n    <h2 class=\"second-title\">\n      Explanation\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n      <ptx-freetext\n        *ngIf=\"true\"\n        [disabled]=\"model.IS_DISABLED_NOTE\"\n        [elementId]=\"'DDA_EXPLANATION'\"\n        [model]=\"model.DDA_EXPLANATION\"\n        (HandleStateChange)=\"model.DDA_EXPLANATION = $event\">\n      </ptx-freetext>\n    </div>\n\n    <h2 class=\"second-title\">\n      Proposal for approval\n    </h2>\n    <div class=\"form-horizontal mb-30\">\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Demurrage</label>\n            <div class=\"col-xs-4 pr-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [ngModel]=\"ShowValue(model.DDA_FOR_COMPANY)\"\n                     disabled>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-2 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Paid to</label>\n            <label class=\"col-xs-2 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Received From</label>\n            <div class=\"col-xs-10 pr-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [ngModel]=\"ShowCharterer()\"\n                     disabled>\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"row\">\n        <div class=\"col-xs-12 mb-15 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Vessel</label>\n            <div class=\"col-xs-4 pr-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center\"\n                     [ngModel]=\"ShowValueVessel(model.DDA_VESSEL)\"\n                     disabled>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Time on Demurrage</label>\n            <div class=\"col-xs-4 p-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-bold\"\n                     [ngModel]=\"model.DDA_TIME_ON_DEM_TEXT\"\n                     disabled>\n            </div>\n            <div class=\"col-xs-2\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     [ngModel]=\"ShowHrs(model.DDA_TIME_ON_DEM_HRS)\"\n                     disabled\n              >\n              <!--<input type=\"text\"-->\n              <!--class=\"form-control text-center mb-15\"-->\n              <!--ptxRestrictNumber-->\n              <!--[fraction]=\"4\"-->\n              <!--[padding]=\"0\"-->\n              <!--[(ngModel)]=\"model.DDA_TIME_ON_DEM_HRS\"-->\n              <!--disabled-->\n              <!--&gt;-->\n            </div>\n            <div class=\"col-xs-1\">\n              <div class=\"text text-black\">Hrs</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <!--<label class=\"col-xs-4 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Net receiving demurrage value</label>-->\n            <!--<label class=\"col-xs-4 control-label\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Net receiving demurrage value</label>-->\n            <label class=\"col-xs-4 control-label\">Demurrage {{ GetSettled() }}</label>\n            <div class=\"col-xs-4\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_NET_PAYMENT_DEM_VALUE\"\n                     (ngModelChange)=\"onUpdateExchange()\"\n                     [disabled]=\"model.IS_DISABLED || true\"\n              >\n            </div>\n            <div class=\"col-xs-1\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n            <!--<div class=\"col-xs-2\">-->\n            <!--<div class=\"form-group\">-->\n            <!--<div class=\"col-xs-12\">-->\n            <!--<button class=\"btn btn-success btn-sm\" (click)=\"DoRecal()\" >Re - Calculate</button>-->\n            <!--</div>-->\n            <!--</div>-->\n            <!--</div>-->\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Demurrage {{ GetSettled() }} <br> (Manual input)</label>\n            <div class=\"col-xs-4\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_NET_PAYMENT_DEM_VALUE_OV\"\n                     (ngModelChange)=\"onUpdateNetPayment();onUpdateExchange()\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n            <div class=\"col-xs-1\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"model.DDA_CURRENCY_UNIT != 'THB'\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Exchange rate - For settlement in Thai Baht</label>\n            <div class=\"col-xs-4 p-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"2\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_EXCHANGE_RATE\"\n                     (ngModelChange)=\"onUpdateExchange()\"\n                     [disabled]=\"model.IS_DISABLED\"\n              >\n            </div>\n            <div class=\"col-xs-2\">\n              <div class=\"text text-black\">THB/{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-sm-6\" *ngIf=\"model.DDA_SETTLEMENT_AMOUNT_THB !== null\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Settlement amount</label>\n            <div class=\"col-xs-4\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [ngModel]=\"model.DDA_SETTLEMENT_AMOUNT_THB\"\n                     disabled\n              >\n            </div>\n            <div class=\"col-xs-2\">\n              <div class=\"text text-black\">THB</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"model.DDA_IS_BROKER_COMMISSION === 'Y'\">\n        <div class=\"col-xs-12 col-sm-6\">\n          <div class=\"form-group\">\n            <label class=\"col-xs-4 control-label\">Broker Commission</label>\n            <!-- <label class=\"col-xs-4 control-label\">Broker Commission<br/>(TOP/TM paid to broker)</label> -->\n            <div class=\"col-xs-4 p-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center mb-15\"\n                     ptxRestrictNumber\n                     [fraction]=\"model.DDA_DECIMAL\"\n                     [padding]=\"0\"\n                     [(ngModel)]=\"model.DDA_BROKER_COMMISSION_VALUE\"\n                     disabled\n              >\n            </div>\n            <div class=\"col-xs-2\">\n              <div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <h2 class=\"second-title\">\n      Reference Attachment\n    </h2>\n    <div class=\"form-horizontal mb-30 pb-30\">\n      <ptx-file-upload\n        btnClass=\"text-left\"\n        [model]=\"model.DAF_ATTACH_FILE\"\n        [is_disabled]=\"model.IS_DISABLED\"\n        [need_caption]=\"false\"\n        [desc]=\"'PDF, Word, Excel, Image, Outlook (Max file size : 10 mb)'\"\n        filterBy=\"BMS\"\n        (handleModelChanged)=\"model.DAF_ATTACH_FILE = $event\"\n      >\n      </ptx-file-upload>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-xs-12\">\n        <div class=\"form-group\">\n          <label class=\"control-label\">Reason for Approval/Reject</label>\n          <textarea\n            class=\"form-control inline-textarea\"\n            [disabled]=\"true\"\n            [(ngModel)]=\"model.DDA_REASON\"></textarea>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"wrapper-button\" id=\"buttonDiv\">\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSaveDraft(model.Buttons)\" (click)=\"DoSaveDraft(model)\"> SAVE DRAFT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToSubmit(model.Buttons)\" (click)=\"DoSubmit(model)\"> SAVE & SUBMIT </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToVerify(model.Buttons)\" (click)=\"DoVerify(model)\"> VERIFY </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToEndorse(model.Buttons)\" (click)=\"DoEndorse(model)\"> ENDORSE </button>\n      <button class=\"btn btn-success\" *ngIf=\"IsAbleToApprove(model.Buttons)\" (click)=\"DoApproved(model)\"> APPROVE </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToReject(model.Buttons)\" (click)=\"DoReject(model)\"> REJECT </button>\n      <button class=\"btn btn-default\" *ngIf=\"IsAbleToCancel(model.Buttons)\" (click)=\"DoCancel(model)\"> CANCEL </button>\n      <button class=\"btn btn-default green\" (click)=\"DoGenerateEXCEL(model)\"> Export to Excel  </button>\n      <button class=\"btn btn-default\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>\n      <!--<button class=\"btn btn-default\" *ngIf=\"IsAbleToGenPDF(model.Buttons)\" (click)=\"DoGeneratePDF(model)\"> PRINT </button>-->\n\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/demurage-approval/demurrage-approval.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageApprovalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_master__ = __webpack_require__("./src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_loader_service__ = __webpack_require__("./src/app/services/loader.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_master_service__ = __webpack_require__("./src/app/services/master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_daf_service__ = __webpack_require__("./src/app/services/daf.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_cip_reference_service__ = __webpack_require__("./src/app/services/cip-reference.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__common_utility__ = __webpack_require__("./src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__common_permission_store__ = __webpack_require__("./src/app/common/permission-store.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__common_button__ = __webpack_require__("./src/app/common/button.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__ = __webpack_require__("./src/app/common/daf-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_sweetalert__ = __webpack_require__("./node_modules/sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_moment_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_moment_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_moment_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__models_daf_material__ = __webpack_require__("./src/app/models/daf-material.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__models_daf_port__ = __webpack_require__("./src/app/models/daf-port.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__common_constant__ = __webpack_require__("./src/app/common/constant.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var DemurrageApprovalComponent = /** @class */ (function () {
    function DemurrageApprovalComponent(route, router, loaderService, masterService, dafService, cipReferenceService, decimalPipe) {
        this.route = route;
        this.router = router;
        this.loaderService = loaderService;
        this.masterService = masterService;
        this.dafService = dafService;
        this.cipReferenceService = cipReferenceService;
        this.decimalPipe = decimalPipe;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_2__models_daf_master__["a" /* DAFMaster */]();
        this.is_duplicate = false;
        this.cip_data = new Array();
    }
    DemurrageApprovalComponent.prototype.canDeactivate = function () {
        // insert logic to check if there are pending changes here;
        // returning true will navigate without confirmation
        // returning false will show a confirm alert before navigating away
    };
    // @HostListener allows us to also guard against browser refresh, close, etc.
    DemurrageApprovalComponent.prototype.unloadNotification = function ($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = 'This message is displayed to the user in IE and Edge when they navigate without using Angular routing (type another URL/close the browser/etc)';
        }
    };
    DemurrageApprovalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.req_transaction_id = params['req_transaction_id'];
            _this.transaction_id = params['transaction_id'];
            _this.user_group = params['user_group'];
            _this.is_duplicate = params['is_duplicate'];
            _this.model.DDA_USER_GROUP = _this.user_group.toUpperCase();
            _this.load();
        });
    };
    DemurrageApprovalComponent.prototype.ShowValueVessel = function (id) {
        return __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].getTextByID(id, this.master.VESSELS);
    };
    DemurrageApprovalComponent.prototype.ShowValue = function (id) {
        return __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].getTextByID(id, this.master.COMPANY);
    };
    DemurrageApprovalComponent.prototype.ShowCharterer = function () {
        var result = '';
        var counter_party_type = this.model.DDA_COUNTERPARTY_TYPE;
        counter_party_type = counter_party_type && counter_party_type.toLowerCase();
        switch (counter_party_type) {
            case 'ship owner':
                var DDA_SHIP_OWNER = this.model.DDA_SHIP_OWNER || '';
                if (DDA_SHIP_OWNER !== '') {
                    result += DDA_SHIP_OWNER + ' (Ship Owner)';
                }
                var broker = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].getTextByID(this.model.DDA_BROKER, this.master.BROKERS);
                if (broker !== '') {
                    result += ' via ' + broker + ' (Broker)';
                }
                break;
            case 'supplier as charterer':
                var vendor = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].getTextByID(this.model.DDA_SUPPLIER, this.master.BROKERS);
                if (vendor !== '') {
                    result = vendor + ' (Supplier as charterer)';
                }
                break;
            case 'customer':
                var customer = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].getTextByID(this.model.DDA_CUSTOMER, this.master.CUSTOMERS);
                if (customer !== '') {
                    result = customer + ' (Customer)';
                }
                var customer_broker = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].getTextByID(this.model.DDA_CUSTOMER_BROKER, this.master.BROKERS);
                if (customer_broker !== '') {
                    result += ' via ' + customer_broker + ' (Broker)';
                }
                break;
        }
        // console.log(`ShowCharterer ${result}`);
        return result;
    };
    DemurrageApprovalComponent.prototype.load = function () {
        var _this = this;
        this.loaderService.display(true);
        this.masterService.getList().subscribe(function (res) {
            _this.master = res;
            _this.master.REAL_CUSTOMER = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(_this.master.CUSTOMERS);
            if (_this.req_transaction_id) {
                _this.dafService.GetByTransactionId(_this.transaction_id).subscribe(function (res) {
                    _this.model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(res);
                    _this.model.TRAN_ID = _this.transaction_id;
                    _this.model.REQ_TRAN_ID = _this.req_transaction_id;
                    if (_this.is_duplicate) {
                        _this.model.Buttons = __WEBPACK_IMPORTED_MODULE_11__common_button__["a" /* Button */].getDefaultButton();
                        _this.model.IS_DISABLED = false;
                        _this.model.DDA_REASON = '';
                        _this.model.DDA_FORM_ID = '';
                        _this.model.DDA_STATUS = '';
                        _this.model.DDA_DOC_DATE = __WEBPACK_IMPORTED_MODULE_15_moment_moment__().format('YYYY-MM-DD HH:mm:ss');
                    }
                    _this.model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].CalculateTime(_this.model);
                    _this.model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].CalValue(_this.model);
                    _this.OnChangeCompany(_this.model.DDA_FOR_COMPANY);
                    // this.model.SHOW_COUTER_PARTY = this.ShowCharterer();
                    _this.OverrideGroupValue(_this.model);
                    _this.loaderService.display(false);
                }, function (err) {
                    console.log(err);
                    _this.loaderService.display(false);
                });
            }
            else {
                _this.model.Buttons = __WEBPACK_IMPORTED_MODULE_11__common_button__["a" /* Button */].getDefaultButton();
                if (_this.user_group.toUpperCase() === 'CMMT') {
                    _this.model.DDA_DEMURAGE_TYPE = 'Received';
                }
                _this.loaderService.display(false);
            }
            _this.OverrideGroupValue(_this.model);
        }, function (error) {
            console.log(error);
        });
    };
    DemurrageApprovalComponent.prototype.OverrideGroupValue = function (model) {
        switch (this.user_group.toUpperCase()) {
            case 'CMPS':
            case 'CMLA':
                model.DDA_MATERIAL_TYPE = 'Product';
                break;
            case 'CMMT':
                model.DDA_MATERIAL_TYPE = 'Product';
                break;
            default:
                model.DDA_MATERIAL_TYPE = 'Crude & F/S';
                break;
        }
        return model;
    };
    DemurrageApprovalComponent.prototype.ShowHrs = function (hrs) {
        var r = this.decimalPipe.transform(hrs, '1.10-10');
        return r;
    };
    DemurrageApprovalComponent.prototype.IsNeedReferrence = function () {
        var result = this.user_group.toUpperCase() !== 'CMLA';
        return result;
    };
    DemurrageApprovalComponent.prototype.IsCMLAGroup = function () {
        return this.user_group.toUpperCase() === 'CMLA';
    };
    DemurrageApprovalComponent.prototype.IsCMCSGroup = function () {
        return this.user_group.toUpperCase() === 'CMCS';
    };
    DemurrageApprovalComponent.prototype.ISCMPSGroup = function () {
        return this.user_group.toUpperCase() === 'CMPS';
    };
    DemurrageApprovalComponent.prototype.IsCMMTGroup = function () {
        return this.user_group.toUpperCase() === 'CMMT';
    };
    DemurrageApprovalComponent.prototype.OnModelChange = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
        // this.model.SHOW_COUTER_PARTY = this.ShowCharterer();
        // this.model = DAFHelper.CalValue(this.model);
    };
    DemurrageApprovalComponent.prototype.GetSettled = function () {
        // if (this.model.DDA_DEMURAGE_TYPE === 'Received' && this.model.DDA_COUNTERPARTY_TYPE === 'Supplier as charterer') {
        //   return 'Sharing Settled';
        // } else if (this.model.DDA_DEMURAGE_TYPE === 'Received' && this.model.DDA_COUNTERPARTY_TYPE === 'Customer') {
        //   return 'Received Settled';
        // } else
        if (this.model.DDA_DEMURAGE_TYPE === 'Received') {
            return 'Received Settled';
        }
        else {
            return 'Paid Settled';
        }
        // return 'Settled';
    };
    DemurrageApprovalComponent.prototype.log = function () {
        console.log(this.model);
    };
    DemurrageApprovalComponent.prototype.GetCustomers = function (company) {
        company = company || '';
        // console
        var customers = __WEBPACK_IMPORTED_MODULE_13_lodash__["filter"](this.master.REAL_CUSTOMER, function (e) {
            return e.COMPANY_CODE && e.COMPANY_CODE.trim() == company.trim();
        });
        customers = __WEBPACK_IMPORTED_MODULE_13_lodash__["uniqBy"](customers, 'ID');
        console.log(company, customers);
        // console.log(company, customers);
        // console.log(company, customers);
        return customers;
    };
    DemurrageApprovalComponent.prototype.OnChangeCompany = function (d) {
        // this.master.CUSTOMERS = _.
        this.master.CUSTOMERS = this.GetCustomers(this.model.DDA_FOR_COMPANY);
        console.log(d);
    };
    DemurrageApprovalComponent.prototype.AddMaterials = function (e) {
        var _this = this;
        if (e && e.CIP_MATERIALS) {
            this.model.DAF_MATERIAL = [];
            __WEBPACK_IMPORTED_MODULE_13_lodash__["each"](e.CIP_MATERIALS, function (f) {
                var mat = new __WEBPACK_IMPORTED_MODULE_16__models_daf_material__["a" /* DAF_MATERIAL */]();
                mat.DMT_MET_NUM = f.MET_NUM;
                mat.DMT_BL_DATE = f.BL_DATE;
                _this.model.DAF_MATERIAL.push(mat);
            });
        }
    };
    DemurrageApprovalComponent.prototype.OnCIPSelectData = function (e) {
        var _this = this;
        console.log(e);
        this.model.DDA_REF_DOC_NO = e && e.TRIP_NO;
        // this.model.DDA_AGREED_LAYCAN_FROM = e && e.LAYCAN_FROM;
        // this.model.DDA_AGREED_LAYCAN_TO = e && e.LAYCAN_TO;
        this.model.DDA_VESSEL = e && e.VESSEL_CODE;
        console.log(this.IsCMCSGroup());
        if (this.IsCMCSGroup() || this.ISCMPSGroup()) {
            this.model.DDA_COUNTERPARTY_TYPE = '';
            this.model.DDA_SHIP_OWNER = null;
            this.model.DDA_SUPPLIER = null;
            this.model.DDA_BROKER = null;
            this.model.DDA_VESSEL = null;
            this.model.DDA_DEMURAGE_RATE = null;
            this.model.DDA_LAYTIME_CONTRACT_HRS = null;
            this.model.DAF_MATERIAL = [];
            var is_cfr = __WEBPACK_IMPORTED_MODULE_13_lodash__["includes"](['CFR', 'DDP', 'DDU', 'DEG', 'DES', 'CIF', 'CIP', 'CPT', 'DAF', 'DAP'], this.model.DDA_INCOTERM);
            var is_fob = !is_cfr; //_.includes(['FOB'], this.model.DDA_INCOTERM);
            if (this.model.DDA_DEMURAGE_TYPE === 'Paid' && is_fob && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                console.log('ss');
                this.model.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
                this.model.DDA_SHIP_OWNER = e.SUPPLIER_NAME;
                this.model.DDA_BROKER = e.BROKER_CODE;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT && e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
                // this.model.DDA_SHIP_OWNER = e.
                console.log(e);
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Paid' && is_fob && this.model.DDA_TYPE_OF_TRANSECTION === 'Sale') {
                console.log('ss');
                this.model.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
                this.model.DDA_SHIP_OWNER = e.SUPPLIER_NAME;
                this.model.DDA_BROKER = e.BROKER_CODE;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT && e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
                // this.model.DDA_SHIP_OWNER = e.
                console.log(e);
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Received' && is_cfr && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                this.model.DDA_COUNTERPARTY_TYPE = 'Supplier as charterer';
                this.model.DDA_SUPPLIER = e.SUPPLIER_MAT_CODE;
                this.model.DDA_GT_C = e.GT_C;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_AGREED_LAYCAN_FROM = e.DISCHARGE_LAYCAN_FROM;
                this.model.DDA_AGREED_LAYCAN_TO = e.DISCHARGE_LAYCAN_TO;
                // this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                // this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Received' && is_fob && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                this.model.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
                this.model.DDA_SHIP_OWNER = e.SUPPLIER_NAME;
                this.model.DDA_BROKER = e.BROKER_CODE;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT && e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
            }
            else if (this.model.DDA_DEMURAGE_TYPE === 'Paid' && is_cfr && this.model.DDA_TYPE_OF_TRANSECTION === 'Purchase') {
                this.model.DDA_COUNTERPARTY_TYPE = 'Supplier as charterer';
                this.model.DDA_SUPPLIER = e.SUPPLIER_MAT_CODE;
                this.model.DDA_GT_C = e.GT_C;
                this.model.DDA_VESSEL = e.VESSEL_CODE;
                this.model.DDA_AGREED_DC_LAYCAN_FROM = e.DISCHARGE_LAYCAN_FROM;
                this.model.DDA_AGREED_DC_LAYCAN_TO = e.DISCHARGE_LAYCAN_TO;
                // this.model.DDA_DEMURAGE_RATE = e.DEMURRAGE;
                // this.model.DDA_LAYTIME_CONTRACT_HRS = e.CIP_FREIGHT.LAYTIME_VALUE;
                this.AddMaterials(e);
                this.OnModelChange();
            }
            return;
        }
        else if (this.IsCMMTGroup()) {
            this.model.DDA_COUNTERPARTY_TYPE = '';
            this.model.DDA_SHIP_OWNER = null;
            this.model.DDA_SUPPLIER = null;
            this.model.DDA_BROKER = null;
            this.model.DDA_VESSEL = null;
            this.model.DDA_DEMURAGE_RATE = null;
            this.model.DDA_LAYTIME_CONTRACT_HRS = null;
            this.model.DAF_MATERIAL = [];
            this.model.DAF_PORT = [];
            this.model.DDA_VESSEL = e && e.VESSEL_CODE;
            this.model.DDA_CUSTOMER = e && e.CUSTOMER_CODE;
            this.model.DDA_CUSTOMER_BROKER = e && e.BROKER_CODE;
            this.model.DDA_COUNTERPARTY_TYPE = 'Customer';
            this.model.DDA_AGREED_LAYCAN_FROM = e && e.LAYCAN_FROM;
            this.model.DDA_AGREED_LAYCAN_TO = e && e.LAYCAN_TO;
            this.model.DDA_CONTRACT_DATE = e && e.CP_DATE;
            this.model.DDA_LAYTIME_CONTRACT_HRS = e && e.LAYTIME;
            this.model.DDA_DEMURAGE_RATE = e && e.DEMURRAGE;
            if (e && e.CHO_CARGOS) {
                this.model.DAF_MATERIAL = [];
                __WEBPACK_IMPORTED_MODULE_13_lodash__["each"](e.CHO_CARGOS, function (f) {
                    var mat = new __WEBPACK_IMPORTED_MODULE_16__models_daf_material__["a" /* DAF_MATERIAL */]();
                    mat.DMT_MET_NUM = f.MAT_NUM;
                    // mat.DMT_BL_DATE = f.;
                    _this.model.DAF_MATERIAL.push(mat);
                });
            }
            if (e && e.CHO_PORTS_D) {
                __WEBPACK_IMPORTED_MODULE_13_lodash__["each"](e.CHO_PORTS_D, function (f) {
                    var port = new __WEBPACK_IMPORTED_MODULE_17__models_daf_port__["a" /* DAF_PORT */]('discharge');
                    port.DPT_LOAD_PORT_FK_JETTY = f.PORT_ID;
                    _this.model.DAF_PORT.push(port);
                });
            }
            if (e && e.CHO_PORTS_L) {
                __WEBPACK_IMPORTED_MODULE_13_lodash__["each"](e.CHO_PORTS_L, function (f) {
                    var port = new __WEBPACK_IMPORTED_MODULE_17__models_daf_port__["a" /* DAF_PORT */]('load');
                    port.DPT_LOAD_PORT_FK_JETTY = f.PORT_ID;
                    _this.model.DAF_PORT.push(port);
                });
            }
            this.OnModelChange();
            return;
        }
        this.model.DDA_SUPPLIER = e && e.SUPPLIER_CODE;
        this.model.DDA_COUNTERPARTY_TYPE = 'Supplier as charterer';
        this.model.DDA_AGREED_LOADING_LAYCAN_FROM = e && e.LOADING_DATE_FROM;
        this.model.DDA_AGREED_LOADING_LAYCAN_TO = e && e.LOADING_DATE_TO;
        var mat = new __WEBPACK_IMPORTED_MODULE_16__models_daf_material__["a" /* DAF_MATERIAL */]();
        if (e && e.CRUDE_NAMES) {
            this.model.DAF_MATERIAL = [];
            this.model.DAF_MATERIAL.push(mat);
        }
    };
    DemurrageApprovalComponent.prototype.onUpdateDemType = function () {
        if (this.model.DDA_DEMURAGE_TYPE === 'Paid') {
            this.model.DDA_IS_BROKER_COMMISSION = 'N';
            this.model.DDA_IS_SHARING_TO_TOP = 'N';
            this.model.DDA_BROKER_COMMISSION_VALUE = 0;
        }
    };
    DemurrageApprovalComponent.prototype.onUpdateExchange = function () {
        var DDA_EXCHANGE_RATE = this.model.DDA_EXCHANGE_RATE;
        var DDA_NET_PAYMENT_DEM_VALUE = this.model.DDA_NET_PAYMENT_DEM_VALUE_OV;
        // DDA_EXCHANGE_RATE = DDA_EXCHANGE_RATE || '';
        console.log(DDA_EXCHANGE_RATE, DDA_EXCHANGE_RATE);
        if (DDA_EXCHANGE_RATE == null || DDA_EXCHANGE_RATE.toString() === '') {
            this.model.DDA_SETTLEMENT_AMOUNT_THB = null;
        }
        else {
            DDA_EXCHANGE_RATE = DDA_EXCHANGE_RATE || 0;
            DDA_NET_PAYMENT_DEM_VALUE = DDA_NET_PAYMENT_DEM_VALUE || 0;
            this.model.DDA_SETTLEMENT_AMOUNT_THB = __WEBPACK_IMPORTED_MODULE_13_lodash__["round"](+DDA_EXCHANGE_RATE * +DDA_NET_PAYMENT_DEM_VALUE, 2);
        }
        return this.model.DDA_SETTLEMENT_AMOUNT_THB;
    };
    DemurrageApprovalComponent.prototype.onUpdateNetPayment = function () {
        this.model.DDA_IS_OVERRIDE_NET_PAYMENT = 'Y';
    };
    DemurrageApprovalComponent.prototype.DoRecal = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
        this.model.DDA_IS_OVERRIDE_NET_PAYMENT = 'N';
        this.onUpdateExchange();
    };
    DemurrageApprovalComponent.prototype.doUpdateLaytime = function () {
        var DDA_LAYTIME_QUANTITY = this.model.DDA_LAYTIME_QUANTITY;
        var DDA_LAYTIME_QUANTITY_RATE = this.model.DDA_LAYTIME_QUANTITY_RATE;
        if (DDA_LAYTIME_QUANTITY && DDA_LAYTIME_QUANTITY_RATE) {
            this.model.DDA_LAYTIME_CONTRACT_HRS = __WEBPACK_IMPORTED_MODULE_13_lodash__["round"](DDA_LAYTIME_QUANTITY / DDA_LAYTIME_QUANTITY_RATE, 4);
            this.model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].CalculateTime(this.model);
        }
    };
    //
    // CalSettleMent(): number {
    //   let DDA_EXCHANGE_RATE = this.model.DDA_EXCHANGE_RATE;
    //   let DDA_NET_PAYMENT_DEM_VALUE = this.model.DDA_NET_PAYMENT_DEM_VALUE;
    //
    //   if(DDA_EXCHANGE_RATE === null || DDA_EXCHANGE_RATE === '') {
    //     this.model.DDA_SETTLEMENT_AMOUNT_THB = null;
    //   } else {
    //     DDA_EXCHANGE_RATE = DDA_EXCHANGE_RATE || 0;
    //     DDA_NET_PAYMENT_DEM_VALUE = DDA_NET_PAYMENT_DEM_VALUE || 0;
    //     this.model.DDA_SETTLEMENT_AMOUNT_THB = +DDA_EXCHANGE_RATE * +DDA_NET_PAYMENT_DEM_VALUE;
    //   }
    //
    //   return this.model.DDA_SETTLEMENT_AMOUNT_THB;
    // }
    DemurrageApprovalComponent.prototype.OnSearchCIPRef = function (criteria) {
        var _this = this;
        // var dialog = bootbox.dialog({
        //     title: 'Processing.....',
        //     message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
        // });
        // dialog.init(function(){
        this.cipReferenceService.search({}).subscribe(function (res) {
            _this.cip_data = res;
            console.log(_this.cip_data);
        }, function (error) {
            console.log(error);
        });
        // setTimeout(function(){
        //     // dialog.find('.bootbox-body').html('I was loaded after the dialog was shown!');
        // }, 3000);
        // });
    };
    DemurrageApprovalComponent.prototype.VerifiedInput = function () {
        var model = this.model;
        var found_not_set_port = __WEBPACK_IMPORTED_MODULE_13_lodash__["find"](model.DAF_PORT, function (e) {
            return !(e.DPT_LOAD_PORT || e.DPT_LOAD_PORT_FK_JETTY);
        });
        var show_error = function (m) {
            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                title: '',
                text: m,
                type: '',
                html: true,
            }, function () {
                return false;
            });
            return;
        };
        if (!model.DDA_FOR_COMPANY) {
            show_error('<b>Please enter your "For Company"</b>');
            return false;
        }
        else if (!model.DDA_COUNTERPARTY_TYPE) {
            show_error('<b>Please enter your "Counter Party"</b>');
            return false;
        }
        else if (!(model.DDA_SHIP_OWNER || model.DDA_SUPPLIER || model.DDA_CUSTOMER)) {
            show_error('<b>Please enter your "Counterparty"</b>');
            return false;
        }
        else if (!(model.DDA_VESSEL)) {
            show_error('<b>Please enter your "Vessel"</b>');
            return false;
        }
        else if (!(model.DDA_LAYTIME_CONTRACT_HRS)) {
            show_error('<b>Please enter your "Laytime Contract"</b>');
            return false;
        }
        else if (!(model.DDA_DEMURAGE_RATE)) {
            show_error('<b>Please enter your "Demurrage Rate"</b>');
            return false;
        }
        else if (model.DAF_PORT.length === 0 || found_not_set_port) {
            show_error('<b>Please enter your "Port"</b>');
            return false;
        }
        return true;
    };
    DemurrageApprovalComponent.prototype.DoSaveDraft = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
        var popup = new Promise(function (resolver) {
            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                title: 'Save',
                text: 'Are you sure you want to save?',
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, resolver);
        })
            .then(function (confirmed) {
            if (confirmed) {
                return context.dafService.SaveDraft(model).toPromise();
            }
            else {
                new Error('unconfirmed');
            }
        })
            .then(function (res) {
            // console.log('====', res);
            return new Promise(function (resolver) {
                __WEBPACK_IMPORTED_MODULE_14_sweetalert__({ title: 'Saved!', type: 'success' }, function () { return resolver(res); });
            });
        })
            .then(function (res) {
            context.router.navigate(["/demurrage-approval/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
        })
            .catch(function (err) {
            // console.log('eeee', err);
            __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'save error');
        });
        //
        // swal({
        //     title: 'Save',
        //     text: 'Are you sure you want to save?',
        //     type: 'info',
        //     showCancelButton: true,
        //     closeOnConfirm: false,
        //     showLoaderOnConfirm: true,
        //   },
        //   () => {
        //     context.dafService.SaveDraft(model).subscribe(
        //       (res) => {
        //         swal({title: 'Saved!', type: 'success'}, () => {
        //           context.router.navigate([`/demurrage-approval/${res.result_code}/${res.req_transaction_id}/${res.transaction_id}/edit`]);
        //           // context.router.navigate(['/cmps/search'])
        //         });
        //       },
        //       (err) => {
        //         // console.log(err)
        //         swal('Something went wrong!', 'save error');
        //       }
        //     );
        //   });
    };
    DemurrageApprovalComponent.prototype.DoSubmit = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(context.model);
        model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
        if (!this.VerifiedInput()) {
            return;
        }
        __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
            title: 'Submit',
            text: 'Do you confirm to submit for approving?',
            type: 'info',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.SaveDraft(model).subscribe(function (res) {
                model.REQ_TRAN_ID = res.req_transaction_id;
                model.TRAN_ID = res.transaction_id;
                _this.req_transaction_id = res.req_transaction_id;
                _this.transaction_id = res.transaction_id;
                context.dafService.Submit(model).subscribe(function (res2) {
                    __WEBPACK_IMPORTED_MODULE_14_sweetalert__({ title: 'Submitted!', type: 'success' }, function () {
                        context.load();
                        context.router.navigate(["/demurrage-approval/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'submit error');
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'save error');
            });
        });
    };
    DemurrageApprovalComponent.prototype.DoVerify = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
            title: 'Verify',
            text: 'Are you sure you want to verify?',
            type: 'info',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.Verify(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_14_sweetalert__({ title: 'Verified!', type: 'success' }, function () {
                    // context.router.navigate(['/cmps/search'])
                    context.load();
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'submit error');
            });
        });
        // this.dafService.Verify(model).subscribe(
        //   (res) => {
        //     swal({title:"Verified!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    DemurrageApprovalComponent.prototype.DoEndorse = function () {
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(this.model);
        __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
            title: 'Endorse',
            text: 'Are you sure you want to endorse?',
            type: 'info',
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            context.dafService.Endorse(model).subscribe(function (res) {
                __WEBPACK_IMPORTED_MODULE_14_sweetalert__({ title: 'Endorsed!', type: 'success' }, function () {
                    context.load();
                    // context.router.navigate(['/cmps/search'])
                    // window.location.href = `${BASE_API}/../../web/MainBoards.aspx`;
                });
            }, function (err) {
                __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'submit error');
            });
        });
        // this.dafService.Endorse(model).subscribe(
        //   (res) => {
        //     swal({title:"Endorsed!", type: "success"}, () => {
        //       window.location.href = `${BASE_API}/../../`;
        //       // context.router.navigate([`/cmps/search`])
        //     });
        //   }
        // )
    };
    DemurrageApprovalComponent.prototype.DoReject = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_11__common_button__["a" /* Button */].getRejectMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === '') {
                    if (result === '') {
                        bootbox.alert('Please Key Reason for Reject.', function () { });
                    }
                }
                else {
                    var loading_1 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_1.init(function () {
                        model.DDA_REASON = result;
                        _this.dafService.Reject(model).subscribe(function (res) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                                title: 'Rejected',
                                text: '',
                                type: 'success'
                            }, function () {
                                context.load();
                                // window.location.href = `${BASE_API}/../../`;
                            });
                        }, function (err) {
                            loading_1.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'submit error');
                        });
                    });
                    // LoadingProcess();
                }
            }
        });
    };
    DemurrageApprovalComponent.prototype.DoCancel = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_11__common_button__["a" /* Button */].getCancelMessage(),
            inputType: 'textarea',
            callback: function (result) {
                if (result === null || result === '') {
                    if (result === '') {
                        bootbox.alert('Please Key Reason for Cancel.', function () { });
                    }
                }
                else {
                    var loading_2 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_2.init(function () {
                        model.DDA_REASON = result;
                        _this.dafService.Cancel(model).subscribe(function (res) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                                title: 'Cancelled',
                                text: '',
                                type: 'success'
                            }, function () {
                                context.load();
                                // window.location.href = `../../web/MainBoards.aspx`;
                            });
                        }, function (err) {
                            loading_2.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'submit error');
                        });
                    });
                }
            }
        });
    };
    DemurrageApprovalComponent.prototype.DoApproved = function () {
        var _this = this;
        // const context = this;
        // const model = Utility.clone(this.model);
        // swal({
        //     title: "Approve",
        //     text: "Are you sure you want to approve?",
        //     type: "info",
        //     showCancelButton: true,
        //     closeOnConfirm: false,
        //     showLoaderOnConfirm: true,
        //   },
        //   () => {
        //     context.dafService.Approve(model).subscribe(
        //       (res) => {
        //         swal({title:"Approved!", type: "success"}, () => {
        //           window.location.href = `${BASE_API}/../../`;
        //         });
        //       },
        //       (err) => {
        //         swal("Something went wrong!", "submit error");
        //       }
        //     );
        //   });
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(this.model);
        bootbox.prompt({
            title: __WEBPACK_IMPORTED_MODULE_11__common_button__["a" /* Button */].getApproveMessage(),
            inputType: 'textarea',
            callback: function (result) {
                console.log(result);
                if (result === '') {
                    result = '-';
                }
                if (result === null || result === '') {
                    if (result === '') {
                        bootbox.alert('Please Key Reason for Approve.', function () { });
                    }
                }
                else {
                    var loading_3 = bootbox.dialog({
                        title: 'Processing ....',
                        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
                    });
                    loading_3.init(function () {
                        model.DDA_REASON = result;
                        _this.dafService.Approve(model).subscribe(function (res) {
                            loading_3.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                                title: 'Approved',
                                text: '',
                                type: 'success'
                            }, function () {
                                context.load();
                                // window.location.href = `../../web/MainBoards.aspx`;
                            });
                        }, function (err) {
                            loading_3.modal('hide');
                            __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'submit error');
                        });
                    });
                }
            }
        });
    };
    DemurrageApprovalComponent.prototype.DoGeneratePDF = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(context.model);
        if (model.DDA_STATUS === '' || !model.DDA_STATUS || model.DDA_STATUS === 'DRAFT') {
            model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                title: 'Save',
                text: 'Are you sure you want to save?',
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: true,
            }, function () {
                context.dafService.SaveDraft(model).subscribe(function (res) {
                    console.log(res);
                    context.model.TRAN_ID = res.transaction_id;
                    model.TRAN_ID = res.transaction_id;
                    _this.dafService.GeneratePDF(model).subscribe(function (res2) {
                        window.open(__WEBPACK_IMPORTED_MODULE_18__common_constant__["a" /* BASE_API */] + "/../../web/report/tmpfile/" + res2, '_blank');
                        context.router.navigate(["/demurrage-approval/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    }, function (error) {
                        context.router.navigate(["/demurrage-approval/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'save error');
                });
            });
        }
        else {
            this.dafService.GeneratePDF(model).subscribe(function (res) {
                window.open(__WEBPACK_IMPORTED_MODULE_18__common_constant__["a" /* BASE_API */] + "/../../web/report/tmpfile/" + res, '_blank');
            });
        }
    };
    DemurrageApprovalComponent.prototype.DoGenerateEXCEL = function () {
        var _this = this;
        var context = this;
        var model = __WEBPACK_IMPORTED_MODULE_9__common_utility__["a" /* Utility */].clone(this.model);
        if (model.DDA_STATUS === '' || !model.DDA_STATUS || model.DDA_STATUS === 'DRAFT') {
            model = __WEBPACK_IMPORTED_MODULE_12__common_daf_helper__["a" /* DAFHelper */].updatedModel(model, this.is_duplicate);
            __WEBPACK_IMPORTED_MODULE_14_sweetalert__({
                title: 'Save',
                text: 'Are you sure you want to save?',
                type: 'info',
                showCancelButton: true,
                closeOnConfirm: true,
            }, function () {
                context.dafService.SaveDraft(model).subscribe(function (res) {
                    console.log(res);
                    context.model.TRAN_ID = res.transaction_id;
                    model.TRAN_ID = res.transaction_id;
                    _this.dafService.GenExcel(model).subscribe(function (res2) {
                        window.open(res2);
                        context.router.navigate(["/demurrage-approval/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    }, function (error) {
                        context.router.navigate(["/demurrage-approval/" + res.result_code + "/" + res.req_transaction_id + "/" + res.transaction_id + "/edit"]);
                    });
                }, function (err) {
                    __WEBPACK_IMPORTED_MODULE_14_sweetalert__('Something went wrong!', 'save error');
                });
            });
        }
        else {
            this.dafService.GenExcel(model).subscribe(function (res) {
                window.open(res);
            });
        }
    };
    DemurrageApprovalComponent.prototype.IsAbleToSaveDraft = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToSaveDraft(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToSubmit = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToSubmit(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToVerify = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToVerify(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToEndorse = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToEndorse(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToApprove = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToApprove(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToCancel = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToCancel(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToReject = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToReject(button_list);
    };
    DemurrageApprovalComponent.prototype.IsAbleToGenPDF = function (button_list) {
        return __WEBPACK_IMPORTED_MODULE_10__common_permission_store__["a" /* PermissionStore */].IsAbleToGenPDF(button_list);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostListener */])('window:beforeunload', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], DemurrageApprovalComponent.prototype, "unloadNotification", null);
    DemurrageApprovalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-demurrage-approval',
            template: __webpack_require__("./src/app/demurrage-approval/components/demurage-approval/demurrage-approval.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__services_loader_service__["a" /* LoaderService */],
            __WEBPACK_IMPORTED_MODULE_5__services_master_service__["a" /* MasterService */],
            __WEBPACK_IMPORTED_MODULE_6__services_daf_service__["a" /* DafService */],
            __WEBPACK_IMPORTED_MODULE_7__services_cip_reference_service__["a" /* CipReferenceService */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common__["d" /* DecimalPipe */]])
    ], DemurrageApprovalComponent);
    return DemurrageApprovalComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-material/demurrage-material.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-xs-4 col-sm-2\">\n  <div class=\"form-group\">\n    <label class=\"col-xs-12 control-label pt-0\">\n      <div class=\"text-inline\">Type of</div>\n      <div class=\"form-inline\">\n        <select class=\"form-control\"\n                [disabled]=\"model.IS_DISABLED\"\n                [(ngModel)]=\"model.DDA_MATERIAL_TYPE\"\n        >\n          <option value=\"Crude & F/S\"> Crude & F/S </option>\n          <option value=\"Product\"> Product </option>\n        </select>\n      </div>\n    </label>\n  </div>\n</div>\n\n<div class=\"col-xs-6 col-sm-10\">\n  <!--<div class=\"form-group\">-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<select class=\"form-control\">-->\n        <!--<option value=\"\"> Product </option>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0 text-right\">-->\n      <!--<div class=\"text text-black\">BL Date</div>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<ptx-input-date-->\n        <!--[date]=\"model.DOC_DATE\"-->\n        <!--(modelChange)=\"model.DOC_DATE = $event;\"-->\n        <!--[disabled]=\"true\"-->\n      <!--&gt;</ptx-input-date>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0\">-->\n\n    <!--</div>-->\n  <!--</div>-->\n  <!--<div class=\"form-group\">-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<select class=\"form-control\">-->\n        <!--<option value=\"\"> Product </option>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0 text-right\">-->\n      <!--<div class=\"text text-black\">BL Date</div>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<ptx-input-date-->\n        <!--[date]=\"model.DOC_DATE\"-->\n        <!--(modelChange)=\"model.DOC_DATE = $event;\"-->\n        <!--[disabled]=\"true\"-->\n      <!--&gt;</ptx-input-date>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0\">-->\n      <!--<a class=\"btn-icon red\" href=\"javascript:void(0)\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></a>-->\n    <!--</div>-->\n  <!--</div>-->\n  <!--<div class=\"form-group\">-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<select class=\"form-control\">-->\n        <!--<option value=\"\"> Product </option>-->\n      <!--</select>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0 text-right\">-->\n      <!--<div class=\"text text-black\">BL Date</div>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-4\">-->\n      <!--<ptx-input-date-->\n        <!--[date]=\"model.DOC_DATE\"-->\n        <!--(modelChange)=\"model.DOC_DATE = $event;\"-->\n        <!--[disabled]=\"true\"-->\n      <!--&gt;</ptx-input-date>-->\n    <!--</div>-->\n    <!--<div class=\"col-xs-2 p-0\">-->\n      <!--<a class=\"btn-icon red\" href=\"javascript:void(0)\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></a>-->\n    <!--</div>-->\n  <!--</div>-->\n\n  <div class=\"form-group\" *ngFor=\"let elem of model.DAF_MATERIAL; let index = index;\">\n    <div class=\"col-xs-4\">\n      <ptx-input-suggestion-ui\n        [model]=\"model.DAF_MATERIAL[index].DMT_MET_NUM\"\n        [data_list]=\"master.MATERIALS\"\n        [is_disabled]=\"model.IS_DISABLED\"\n        [show_field]=\"'VALUE'\"\n        select_field=\"ID\"\n        (ModelChanged)=\"model.DAF_MATERIAL[index].DMT_MET_NUM = $event;\"\n      >\n      </ptx-input-suggestion-ui>\n    </div>\n    <div class=\"col-xs-2 p-0 text-right\">\n      <div class=\"text text-black\">B/L Date</div>\n    </div>\n    <div class=\"col-xs-4\">\n      <ptx-input-date\n        [date]=\"model.DAF_MATERIAL[index].DMT_BL_DATE\"\n        (modelChange)=\"model.DAF_MATERIAL[index].DMT_BL_DATE = $event;\"\n        [disabled]=\"model.IS_DISABLED\"\n      ></ptx-input-date>\n    </div>\n    <div class=\"col-xs-2 p-0\">\n      <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"DoRemove(index, elem)\">\n        <span class=\"glyphicon glyphicon-trash\"></span>\n      </a>\n    </div>\n  </div>\n  <div class=\"form-group\">\n    <div class=\"col-xs-12\">\n      <button class=\"btn btn-success btn-sm\" (click)=\"DoAdd()\">Add</button>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-material/demurrage-material.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageMaterialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_material__ = __webpack_require__("./src/app/models/daf-material.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_master__ = __webpack_require__("./src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert__ = __webpack_require__("./node_modules/sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_utility__ = __webpack_require__("./src/app/common/utility.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DemurrageMaterialComponent = /** @class */ (function () {
    function DemurrageMaterialComponent(utility) {
        this.utility = utility;
    }
    DemurrageMaterialComponent.prototype.ngOnInit = function () {
    };
    DemurrageMaterialComponent.prototype.DoAdd = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        var mat = new __WEBPACK_IMPORTED_MODULE_2__models_daf_material__["a" /* DAF_MATERIAL */]();
        this.model.DAF_MATERIAL.push(mat);
    };
    DemurrageMaterialComponent.prototype.DoRemove = function (index, e) {
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var met_num = e && e.DMT_MET_NUM;
        console.log(met_num);
        var mat = this.utility.getTextByID(met_num, this.master.MATERIALS);
        console.log(mat);
        var popup_content = "Are you sure you want to remove this product/crude?<br /><br /><b>Product/Crude Name:</b> " + mat;
        __WEBPACK_IMPORTED_MODULE_4_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            var mat = context.model.DAF_MATERIAL[index];
            context.model.DAF_MATERIAL = __WEBPACK_IMPORTED_MODULE_5_lodash__["reject"](context.model.DAF_MATERIAL, function (e) {
                return e === mat;
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], DemurrageMaterialComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__models_daf_master__["a" /* DAFMaster */])
    ], DemurrageMaterialComponent.prototype, "master", void 0);
    DemurrageMaterialComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-material',
            template: __webpack_require__("./src/app/demurrage-approval/components/demurrage-material/demurrage-material.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */]])
    ], DemurrageMaterialComponent);
    return DemurrageMaterialComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-port/demurrage-port.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"p-0 col-xs-12 mb-30\">\n  <div class=\"text-bold\"> {{ title }}</div>\n  <div *ngFor=\"let e of model.DAF_PORT|portsFilter:type\" class=\"table-responsive mt-15 mb-30 pb-30\">\n    <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n      <tbody>\n      <tr>\n        <td rowspan=\"3\" class=\"rotate bg-navy ptx-w-40 pl-0 pr-0 text-white\"><div><span>Running Time</span></div></td>\n        <td class=\"text-bold text-center ptx-w-150\">{{ title }} Name</td>\n        <td class=\"text-bold text-center ptx-w-250\">Activity (Free text)</td>\n        <td class=\"text-bold text-center ptx-w-320\">Original Claim/Original Time Spent</td>\n        <td class=\"text-bold text-center ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n        <td class=\"text-bold text-center ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n        <td class=\"text-bold text-center ptx-w-320\">Settled</td>\n        <td class=\"text-bold text-center ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Original)</td>\n        <td rowspan=\"7\" class=\"ptx-w-50 text-center\">\n          <a class=\"remove-icon\" href=\"javascript:void(0)\" (click)=\"removePort(e)\">\n            <span class=\"glyphicon glyphicon-trash\"></span>\n          </a>\n          <!--<a class=\"btn-icon red\" href=\"javascript:void(0)\" (click)=\"removePort.emit($event);\">-->\n            <!--<i class=\"fa fa-times\" aria-hidden=\"true\"></i>-->\n          <!--</a>-->\n        </td>\n      </tr>\n      <tr class=\"border-bottom-0\">\n        <td class=\"text-center\" valign=\"top\">\n          <ptx-input-suggestion-ui\n            *ngIf=\"IsCMCS()\"\n            [is_start_with]=\"false\"\n            [model]=\"e.DPT_LOAD_PORT\"\n            [data_list]=\"master.PORTS\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT = $event;\"\n          >\n          </ptx-input-suggestion-ui>\n          <ptx-input-suggestion-ui\n            *ngIf=\"!IsCMCS() && type === 'load'\"\n            [is_start_with]=\"false\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_L\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion-ui>\n          <ptx-input-suggestion-ui\n            *ngIf=\"!IsCMCS() && type === 'discharge'\"\n            [is_start_with]=\"false\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_D\"\n            [is_disabled]=\"model.IS_DISABLED\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion-ui>\n        </td>\n        <td>\n          <div class=\"row mb-5\">\n            <div class=\"col-xs-3 pl-0\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm\"\n                     value=\"Start\"\n                     disabled>\n            </div>\n            <div class=\"col-xs-9\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_T_START_ACTIVITY\"\n              >\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-xs-3 pl-0\">\n              <input type=\"text\" class=\"form-control text-center text-sm\" value=\"Stop\" disabled>\n            </div>\n            <div class=\"col-xs-9\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_T_STOP_ACTIVITY\"\n              >\n            </div>\n          </div>\n        </td>\n        <!--origin-->\n        <td>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_START\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_START = $event; HandleCopyChange(e, 'origin'); OnModelChange();\"\n          ></ptx-input-date-time>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_STOP\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_STOP = $event; HandleCopyChange(e, 'origin'); OnModelChange();\"\n          ></ptx-input-date-time>\n        </td>\n        <!--top review-->\n        <td>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START = $event; HandleCopyChange(e, 'top'); OnModelChange();\"\n          ></ptx-input-date-time>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP = $event; HandleCopyChange(e, 'top'); OnModelChange();\"\n          ></ptx-input-date-time>\n        </td>\n        <!--Settled review-->\n        <td>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_START\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_START = $event; HandleCopyChange(e, 'settled'); OnModelChange();\"\n          ></ptx-input-date-time>\n          <ptx-input-date-time\n            [disabled]=\"model.IS_DISABLED\"\n            [input]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_STOP\"\n            (handleModelChange)=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_STOP = $event; HandleCopyChange(e, 'settled'); OnModelChange();\"\n          ></ptx-input-date-time>\n        </td>\n        <!--saving-->\n        <td *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row mb-5\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_TEXT\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_START_HRS)\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n          </div>\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_TEXT\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\"-->\n                     <!--class=\"form-control text-center text-sm\"-->\n                     <!--[ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_STOP_HRS)\"-->\n                     <!--disabled-->\n              <!--&gt;-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <tr class=\"border-top-0\">\n        <td class=\"text-center\">\n          Total Running Time\n        </td>\n        <td>\n        </td>\n        <!--origin-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--top review-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_TOP_REVIEWED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--Settled review-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SETTLED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--saving-->\n        <td class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-7\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-5 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_RT_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n      </tr>\n\n      <!--deduction-->\n      <tr>\n        <td rowspan=\"3\" class=\"rotate bg-red ptx-w-40 pl-0 pr-0 text-white\"><div><span>Deduction</span></div></td>\n        <td class=\"text-bold text-center ptx-w-200\">{{title}} Name</td>\n        <td class=\"text-bold text-center ptx-w-400\">Activity</td>\n        <td class=\"text-bold text-center ptx-w-300\">Owner Deduction</td>\n        <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n        <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n        <td class=\"text-bold text-center ptx-w-300\">Settled</td>\n        <td class=\"text-bold text-center ptx-w-300\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Owner)</td>\n      </tr>\n      <tr class=\"border-bottom-0\">\n        <td class=\"text-center\" valign=\"top\">\n          <!--<ptx-input-suggestion-ui-->\n            <!--[model]=\"e.DPT_LOAD_PORT\"-->\n            <!--[data_list]=\"master.PORTS\"-->\n            <!--[is_disabled]=\"true\"-->\n            <!--[show_field]=\"'VALUE'\"-->\n            <!--select_field=\"ID\"-->\n            <!--(ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT = $event;\"-->\n          <!--&gt;-->\n          <!--</ptx-input-suggestion-ui>-->\n          <ptx-input-suggestion-ui\n            *ngIf=\"IsCMCS()\"\n            [is_start_with]=\"false\"\n            [model]=\"e.DPT_LOAD_PORT\"\n            [data_list]=\"master.PORTS\"\n            [is_disabled]=\"true\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT = $event;\"\n          >\n          </ptx-input-suggestion-ui>\n          <ptx-input-suggestion-ui\n            *ngIf=\"!IsCMCS() && type === 'load'\"\n            [is_start_with]=\"false\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_L\"\n            [is_disabled]=\"true\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion-ui>\n          <ptx-input-suggestion-ui\n            *ngIf=\"!IsCMCS() && type === 'discharge'\"\n            [is_start_with]=\"false\"\n            [model]=\"e.DPT_LOAD_PORT_FK_JETTY\"\n            [data_list]=\"master.JETTIES_D\"\n            [is_disabled]=\"true\"\n            [show_field]=\"'VALUE'\"\n            select_field=\"ID\"\n            (ModelChanged)=\"model.DAF_PORT[GetIndex(e)].DPT_LOAD_PORT_FK_JETTY = $event;\"\n          >\n          </ptx-input-suggestion-ui>\n        </td>\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <input type=\"text\"\n                   class=\"form-control text-left text-sm\"\n                   [ngModel]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_ACTIVITY\"\n                   disabled>\n          </div>\n          <ptx-deduction-modal\n            [model]=\"model\"\n            [index]=\"GetIndex(e)\"\n          >\n          </ptx-deduction-modal>\n        </td>\n\n        <!--Owner-->\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'days')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'hrs')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_OWNER_DEDUCTION_T_TEXT, 'mins')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n\n        <!--top review-->\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'days')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'hrs')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_TOP_REVIEWED_T_TEXT, 'mins')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n\n        <!--settle-->\n        <td valign=\"top\">\n          <div class=\"row mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'days')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Day</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'hrs')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline mr-10\">Hrs</div>\n            <div class=\"form-inline\">\n              <input type=\"text\"\n                     class=\"form-control text-center text-sm ptx-w-60\"\n                     [ngModel]=\"GetText(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SETTLED_T_TEXT, 'mins')\"\n                     disabled>\n            </div>\n            <div class=\"text-inline\">Mins</div>\n          </div>\n        </td>\n\n        <!--saving-->\n        <td valign=\"top\" class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row  mb-5\" *ngFor=\"let elem of model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY; let j=index;\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DAF_DEDUCTION_ACTIVITY[j].DDD_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <tr class=\"border-top-0\">\n        <td class=\"text-center\">\n          Total Deduction Time\n        </td>\n        <td>\n\n        </td>\n        <!--Owner-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_OWNER_DEDUCTION_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--top-review-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_TOP_REVIEWED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n\n        <!--settle-->\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_SETTLED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n\n        <!--saving-->\n        <td valign=\"top\" class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_D_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <tr>\n        <td class=\"border-left-0\"></td>\n        <td colspan=\"2\" class=\"border-right-0 text-left\">Net Time Spent</td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_CLAIM_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_TOP_REVIEWED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n        <td class=\"text-bold\">\n          <div class=\"row\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_SETTLED_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n        <td class=\"text-bold\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">\n          <div class=\"row mb-5\">\n            <ptx-show-text-and-hrs\n              [text]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_TEXT\"\n              [hrs]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_HRS\"\n            ></ptx-show-text-and-hrs>\n            <!--<div class=\"col-xs-8\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_TEXT\" disabled>-->\n            <!--</div>-->\n            <!--<div class=\"col-xs-4 pl-0\">-->\n              <!--<input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" [ngModel]=\"ShowHrs(model.DAF_PORT[GetIndex(e)].DPT_NTS_SAVING_T_HRS)\" disabled>-->\n            <!--</div>-->\n          </div>\n        </td>\n      </tr>\n      <!--<tr>-->\n        <!--<td class=\"border-left-0\"></td>-->\n        <!--<td colspan=\"2\" class=\"border-right-0 text-left\">Net Time Spent</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n        <!--<td class=\"text-bold\">-->\n          <!--<div class=\"row\">-->\n            <!--<div class=\"col-xs-8\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"2 Days  16 Hrs  23 Mins\" disabled></div>-->\n            <!--<div class=\"col-xs-4 pl-0\"><input type=\"text\" class=\"form-control text-center text-sm bg-yellow\" value=\"64.3833 Hrs\" disabled></div>-->\n          <!--</div>-->\n        <!--</td>-->\n      <!--</tr>-->\n      </tbody>\n    </table>\n  </div>\n  <div class=\"mb-30\">\n    <button class=\"btn btn-success m-0\" (click)=\"addLoadPort()\">\n      {{ button_title }}\n    </button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-port/demurrage-port.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurragePortComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_master__ = __webpack_require__("./src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_port__ = __webpack_require__("./src/app/models/daf-port.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert__ = __webpack_require__("./node_modules/sweetalert/lib/sweetalert.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_utility__ = __webpack_require__("./src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_date_helper__ = __webpack_require__("./src/app/common/date-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DemurragePortComponent = /** @class */ (function () {
    function DemurragePortComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_2__models_daf_master__["a" /* DAFMaster */]();
        this.title = '';
        this.button_title = '';
        this.type = '';
        this.user_group = '';
        this.handleModelChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    DemurragePortComponent.prototype.ngOnInit = function () {
    };
    DemurragePortComponent.prototype.GetIndex = function (elem) {
        return __WEBPACK_IMPORTED_MODULE_4_lodash__["findIndex"](this.model.DAF_PORT, function (e) {
            return elem === e;
        });
    };
    DemurragePortComponent.prototype.addLoadPort = function () {
        if (this.model.IS_DISABLED) {
            return;
        }
        this.model.DAF_PORT.push(new __WEBPACK_IMPORTED_MODULE_3__models_daf_port__["a" /* DAF_PORT */](this.type));
    };
    DemurragePortComponent.prototype.OnModelChange = function () {
        this.handleModelChange.emit();
        // this.model = DAFHelper.CalculateTime(this.model);
    };
    DemurragePortComponent.prototype.HandleCopyChange = function (e, type) {
        if (type === 'origin') {
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_START);
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_ORIGIN_CLAIM_T_STOP);
            this.HandleCopyChange(e, 'top');
        }
        else if (type === 'top') {
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_SETTLED_T_START = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_START);
            this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_SETTLED_T_STOP = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].clone(this.model.DAF_PORT[this.GetIndex(e)].DPT_RT_TOP_REVIEWED_T_STOP);
        }
    };
    DemurragePortComponent.prototype.GetText = function (text, type) {
        return __WEBPACK_IMPORTED_MODULE_7__common_date_helper__["a" /* DateHelper */].GetNumberFromTextByType(text, type);
    };
    DemurragePortComponent.prototype.IsCMCS = function () {
        return this.user_group === 'CMCS';
    };
    DemurragePortComponent.prototype.ShowHrs = function (hrs) {
        hrs = hrs || 0;
        // const r = this.decimalPipe.transform(hrs, '1.4-4');
        // return `${r} Hrs`;
        return "" + hrs;
    };
    DemurragePortComponent.prototype.removePort = function (e) {
        var _this = this;
        if (this.model.IS_DISABLED) {
            return;
        }
        var context = this;
        var id = e && e.DPT_LOAD_PORT;
        var name = __WEBPACK_IMPORTED_MODULE_6__common_utility__["a" /* Utility */].getTextByID(id, this.master.PORTS);
        console.log(name);
        var popup_content = "Are you sure you want to remove this port?<br /><br /><b>Port Name:</b> " + name;
        __WEBPACK_IMPORTED_MODULE_5_sweetalert__({
            title: "Remove",
            text: popup_content,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, remove it!",
            closeOnConfirm: true,
            html: true,
        }, function () {
            context.model.DAF_PORT = __WEBPACK_IMPORTED_MODULE_4_lodash__["reject"](context.model.DAF_PORT, function (o) {
                return o === e;
            });
            _this.handleModelChange.emit();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], DemurragePortComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_daf_master__["a" /* DAFMaster */])
    ], DemurragePortComponent.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "button_title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "type", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], DemurragePortComponent.prototype, "user_group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], DemurragePortComponent.prototype, "handleModelChange", void 0);
    DemurragePortComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-port',
            template: __webpack_require__("./src/app/demurrage-approval/components/demurrage-port/demurrage-port.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__angular_common__["d" /* DecimalPipe */]])
    ], DemurragePortComponent);
    return DemurragePortComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-time-summary/demurrage-time-summary.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"text-bold mt-30 pt-30\">Demurrage Time Summary</div>\n<div class=\"table-responsive mt-15 mb-30 pb-30\">\n  <table class=\"table-bordered plutonyx-table-2 autoscroll text-center\">\n    <thead>\n    <tr>\n      <td class=\"text-bold ptx-w-200\"></td>\n      <td class=\"text-bold ptx-w-320\">Original Claim/Original Time Spent</td>\n      <td class=\"text-bold ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n      <td class=\"text-bold ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n      <td class=\"text-bold ptx-w-320\">Settled</td>\n      <td class=\"text-bold ptx-w-320\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Original) </td>\n    </tr>\n    </thead>\n    <tbody>\n    <tr>\n      <td class=\"text-left\">Total Running Time <br>({{ GetPortLabel() }})</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TRT_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TRT_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TRT_SETTLED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n          key=\"DDA_TRT_SAVING\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Total Deduction Time <br>({{ GetPortLabel() }})</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TDT_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TDT_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_TDT_SETTLED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n          key=\"DDA_TDT_SAVING\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Net Time Spent</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_NTS_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_NTS_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_NTS_SETTLED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n          key=\"DDA_NTS_SAVING\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Laytime Contract</td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_LY_ORIGIN_CLAIM\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n        <ptx-show-table-col-text-and-hrs\n          key=\"DDA_LY_TOP_REVIEWED\"\n          [model]=\"model\"\n        >\n        </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        key=\"DDA_LY_SETTLED\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n        key=\"DDA_LY_SAVING\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    <tr>\n      <td class=\"text-left\">Time On Demurrage</td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_ORIGIN_CLAIM\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_TOP_REVIEWED\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_SETTLED\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n      <td>\n      <ptx-show-table-col-text-and-hrs\n        *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\"\n        className=\"bg-yellow\"\n        key=\"DDA_TOD_SAVING\"\n        [model]=\"model\"\n      >\n      </ptx-show-table-col-text-and-hrs>\n      </td>\n    </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-time-summary/demurrage-time-summary.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageTimeSummaryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_master__ = __webpack_require__("./src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__("./node_modules/lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DemurrageTimeSummaryComponent = /** @class */ (function () {
    function DemurrageTimeSummaryComponent() {
        this.master = new __WEBPACK_IMPORTED_MODULE_1__models_daf_master__["a" /* DAFMaster */]();
        this.model = new __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */]();
    }
    DemurrageTimeSummaryComponent.prototype.ngOnInit = function () {
    };
    DemurrageTimeSummaryComponent.prototype.GetPortLabel = function () {
        var result = '';
        var is_load_port = false;
        var is_discharge_port = false;
        __WEBPACK_IMPORTED_MODULE_3_lodash__["each"](this.model.DAF_PORT, function (e) {
            is_load_port = is_load_port || e.DPT_TYPE === 'load';
            is_discharge_port = is_discharge_port || e.DPT_TYPE === 'discharge';
        });
        if (is_load_port && is_discharge_port) {
            result += 'Load Port + Discharge Port';
        }
        else if (is_load_port) {
            result += 'Load Port';
        }
        else if (is_discharge_port) {
            result += 'Discharge Port';
        }
        return result;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_master__["a" /* DAFMaster */])
    ], DemurrageTimeSummaryComponent.prototype, "master", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__models_daf_data__["a" /* DAF_DATA */])
    ], DemurrageTimeSummaryComponent.prototype, "model", void 0);
    DemurrageTimeSummaryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-time-summary',
            template: __webpack_require__("./src/app/demurrage-approval/components/demurrage-time-summary/demurrage-time-summary.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], DemurrageTimeSummaryComponent);
    return DemurrageTimeSummaryComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"p-0 col-xs-12 mb-30\">\n  <div class=\"text-bold\">Demurrage Value Summary</div>\n  <div class=\"form-horizontal mt-30 mb-15\">\n    <div class=\"row\">\n      <div class=\"col-xs-12 col-sm-6\">\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Demurrage Rate ({{ model.DDA_CURRENCY_UNIT }} PDPR)</label>\n          <div class=\"col-xs-6\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [padding]=\"0\"\n                   [ngModel]=\"model.DDA_DEMURAGE_RATE\"\n                   disabled>\n          </div>\n          <!--<div class=\"col-xs-1\">-->\n            <!--<div class=\"text text-black\">{{ model.DDA_CURRENCY_UNIT }}</div>-->\n          <!--</div>-->\n        </div>\n        <div class=\"form-group\">\n          <label class=\"col-xs-5 control-label\">Address Commission ({{ model.DDA_CURRENCY_UNIT }})</label>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_ADDRESS_COMMISSION_N\"\n                     name=\"DDA_IS_ADDRESS_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_ADDRESS_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'N'\"\n              >\n              <label for=\"DDA_IS_ADDRESS_COMMISSION_N\">No</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_ADDRESS_COMMISSION_Y\"\n                     name=\"DDA_IS_ADDRESS_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_ADDRESS_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'Y'\"\n              >\n              <label for=\"DDA_IS_ADDRESS_COMMISSION_Y\">Yes</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [max]=\"100\"\n                   [disabled]=\"model.DDA_IS_ADDRESS_COMMISSION == 'N' || model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_ADDRESS_COMMISSION_PERCEN\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">%</div>\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n          <label class=\"col-xs-5 control-label\">Broker Commission ({{ model.DDA_CURRENCY_UNIT }})</label>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_BROKER_COMMISSION_N\"\n                     name=\"DDA_IS_BROKER_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_BROKER_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'N'\"\n              >\n              <label for=\"DDA_IS_BROKER_COMMISSION_N\">No</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_BROKER_COMMISSION_Y\"\n                     name=\"DDA_IS_BROKER_COMMISSION\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_BROKER_COMMISSION\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'Y'\"\n              >\n              <label for=\"DDA_IS_BROKER_COMMISSION_Y\">Yes</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <!--<input type=\"text\"-->\n                   <!--class=\"form-control text-center\"-->\n                   <!--ptxRestrictNumber-->\n                   <!--[fraction]=\"2\"-->\n                   <!--[max]=\"100\"-->\n                   <!--[disabled]=\"model.DDA_IS_BROKER_COMMISSION == 'N' || model.IS_DISABLED\"-->\n                   <!--[(ngModel)]=\"model.DDA_BROKER_COMMISSION_PERCEN\"-->\n                   <!--(ngModelChange)=\"onUpdateCal()\"-->\n                   <!--(ngModelChange)=\"OnModelChange();\"-->\n            <!--&gt;-->\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [max]=\"100\"\n                   [disabled]=\"model.DDA_IS_BROKER_COMMISSION == 'N' || model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_BROKER_COMMISSION_PERCEN\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">%</div>\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">\n          <label class=\"col-xs-5 control-label\">Demurrage Sharing to {{ GetCompanyName(model.DDA_FOR_COMPANY) }} ({{ model.DDA_CURRENCY_UNIT }})</label>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_SHARING_TO_TOP_N\"\n                     name=\"DDA_IS_SHARING_TO_TOP\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_SHARING_TO_TOP\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'N'\"\n              >\n              <label for=\"DDA_IS_SHARING_TO_TOP_N\">No</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <div class=\"radio\">\n              <input id=\"DDA_IS_SHARING_TO_TOP_Y\"\n                     name=\"DDA_IS_SHARING_TO_TOP\"\n                     type=\"radio\"\n                     [disabled]=\"model.IS_DISABLED\"\n                     [(ngModel)]=\"model.DDA_IS_SHARING_TO_TOP\"\n                     (ngModelChange)=\"onUpdateCal()\"\n                     [value]=\"'Y'\"\n              >\n              <label for=\"DDA_IS_SHARING_TO_TOP_Y\">Yes</label>\n            </div>\n          </div>\n          <div class=\"col-xs-2\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"2\"\n                   [max]=\"100\"\n                   [disabled]=\"model.DDA_IS_SHARING_TO_TOP == 'N' || model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_SHARING_TO_TOP_PERCEN\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">%</div>\n          </div>\n        </div>\n        <div class=\"form-group\" *ngIf=\"false\">\n          <label class=\"col-xs-5 control-label\">Decimal</label>\n          <div class=\"col-xs-6\">\n            <input type=\"text\"\n                   class=\"form-control text-center\"\n                   ptxRestrictNumber\n                   [fraction]=\"0\"\n                   [max]=\"7\"\n                   [disabled]=\"model.IS_DISABLED\"\n                   [(ngModel)]=\"model.DDA_DECIMAL\"\n                   (ngModelChange)=\"OnModelChange();\"\n            >\n          </div>\n          <div class=\"col-xs-1\">\n            <div class=\"text text-black\">Digit</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <table class=\"mb-30 pb-30 table table-bordered plutonyx-table-2 text-center\">\n    <thead>\n    <tr>\n      <td class=\"text-bold\" width=\"30%\"></td>\n      <td class=\"text-bold\" width=\"17.5%\">Original Claim/Original Time Spent</td>\n      <td class=\"text-bold\" width=\"17.5%\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">TOP Reviewed</td>\n      <td class=\"text-bold\" width=\"17.5%\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Received'\">Customer/Charterer Counter</td>\n      <td class=\"text-bold\" width=\"17.5%\">Settled</td>\n      <td class=\"text-bold\" width=\"17.5%\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">Saving (Settled vs Original)</td>\n    </tr>\n    </thead>\n    <tbody>\n    <tr *ngIf=\"model.DDA_IS_ADDRESS_COMMISSION === 'Y' || model.DDA_IS_SHARING_TO_TOP === 'Y'\">\n      <td class=\"text-left\">Gross Demurrage ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_GD_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_GD_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_GD_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_GD_SAVING_VALUE ) }}</td>\n    </tr>\n    <tr *ngIf=\"model.DDA_IS_ADDRESS_COMMISSION === 'Y'\">\n      <td class=\"text-left\">Less Address Commission ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_AC_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_AC_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_AC_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_AC_SAVING_VALUE) }}</td>\n    </tr>\n    <tr class=\"bg-blue\" *ngIf=\"model.DDA_IS_ADDRESS_COMMISSION === 'Y' && model.DDA_IS_SHARING_TO_TOP === 'Y'\">\n      <td class=\"text-left\">Sub Total Demurrage ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_STD_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_STD_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_STD_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_STD_SAVING_VALUE) }}</td>\n    </tr>\n    <tr *ngIf=\"model.DDA_IS_SHARING_TO_TOP === 'Y'\">\n      <td class=\"text-left\">Demurrage Sharing to TOP ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_DST_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_DST_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_DST_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_DST_SAVING_VALUE) }}</td>\n    </tr>\n    <tr class=\"bg-yellow\">\n      <td class=\"text-left\">Net Demurrage ({{ model.DDA_CURRENCY_UNIT }})</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_ND_ORIGIN_CLAIM_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_ND_TOP_REVIEWED_VALUE) }}</td>\n      <td class=\"text-right\">{{ ShowNumber(model.DDA_ND_SETTLED_VALUE) }}</td>\n      <td class=\"text-right\" *ngIf=\"model.DDA_DEMURAGE_TYPE === 'Paid'\">{{ ShowNumber(model.DDA_ND_SAVING_VALUE) }}</td>\n    </tr>\n    </tbody>\n  </table>\n</div>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageValueUsdSummaryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_daf_helper__ = __webpack_require__("./src/app/common/daf-helper.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_daf_master__ = __webpack_require__("./src/app/models/daf-master.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_utility__ = __webpack_require__("./src/app/common/utility.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DemurrageValueUsdSummaryComponent = /** @class */ (function () {
    function DemurrageValueUsdSummaryComponent(decimalPipe) {
        this.decimalPipe = decimalPipe;
        this.model = new __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */]();
        this.master = new __WEBPACK_IMPORTED_MODULE_3__models_daf_master__["a" /* DAFMaster */]();
    }
    DemurrageValueUsdSummaryComponent.prototype.ngOnInit = function () {
    };
    DemurrageValueUsdSummaryComponent.prototype.onUpdateCal = function () {
        var DDA_IS_BROKER_COMMISSION = this.model.DDA_IS_BROKER_COMMISSION === 'Y';
        var DDA_IS_ADDRESS_COMMISSION = this.model.DDA_IS_ADDRESS_COMMISSION === 'Y';
        var DDA_IS_SHARING_TO_TOP = this.model.DDA_IS_SHARING_TO_TOP === 'Y';
        if (!DDA_IS_BROKER_COMMISSION) {
            this.model.DDA_BROKER_COMMISSION_PERCEN = null;
        }
        if (!DDA_IS_ADDRESS_COMMISSION) {
            this.model.DDA_ADDRESS_COMMISSION_PERCEN = null;
        }
        if (!DDA_IS_SHARING_TO_TOP) {
            this.model.DDA_SHARING_TO_TOP_PERCEN = null;
        }
        this.OnModelChange();
    };
    DemurrageValueUsdSummaryComponent.prototype.ShowNumber = function (num) {
        num = num || 0;
        var r = this.decimalPipe.transform(num, '1.2-2');
        return "" + r;
    };
    DemurrageValueUsdSummaryComponent.prototype.GetCompanyName = function (comcode) {
        return __WEBPACK_IMPORTED_MODULE_4__common_utility__["a" /* Utility */].getTextByID(comcode, this.master.COMPANY);
    };
    DemurrageValueUsdSummaryComponent.prototype.OnModelChange = function () {
        this.model = __WEBPACK_IMPORTED_MODULE_2__common_daf_helper__["a" /* DAFHelper */].CalValue(this.model);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], DemurrageValueUsdSummaryComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3__models_daf_master__["a" /* DAFMaster */])
    ], DemurrageValueUsdSummaryComponent.prototype, "master", void 0);
    DemurrageValueUsdSummaryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-demurrage-value-usd-summary',
            template: __webpack_require__("./src/app/demurrage-approval/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DecimalPipe */]])
    ], DemurrageValueUsdSummaryComponent);
    return DemurrageValueUsdSummaryComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/components/show-table-col-text-and-hrs/show-table-col-text-and-hrs.component.html":
/***/ (function(module, exports) {

module.exports = "<ptx-show-text-and-hrs\n  [className]=\"className\"\n  [text]=\"model[text_key]\"\n  [hrs]=\"model[hrs_key]\"\n></ptx-show-text-and-hrs>\n"

/***/ }),

/***/ "./src/app/demurrage-approval/components/show-table-col-text-and-hrs/show-table-col-text-and-hrs.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowTableColTextAndHrsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_daf_data__ = __webpack_require__("./src/app/models/daf-data.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowTableColTextAndHrsComponent = /** @class */ (function () {
    function ShowTableColTextAndHrsComponent() {
        this.className = '';
        this.need_br = false;
        this.text_key = '';
        this.hrs_key = '';
    }
    ShowTableColTextAndHrsComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(ShowTableColTextAndHrsComponent.prototype, "setKey", {
        set: function (k) {
            this.text_key = k + '_TIME_TEXT';
            this.hrs_key = k + '_HRS';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_daf_data__["a" /* DAF_DATA */])
    ], ShowTableColTextAndHrsComponent.prototype, "model", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ShowTableColTextAndHrsComponent.prototype, "className", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], ShowTableColTextAndHrsComponent.prototype, "need_br", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('key'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], ShowTableColTextAndHrsComponent.prototype, "setKey", null);
    ShowTableColTextAndHrsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'ptx-show-table-col-text-and-hrs',
            template: __webpack_require__("./src/app/demurrage-approval/components/show-table-col-text-and-hrs/show-table-col-text-and-hrs.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], ShowTableColTextAndHrsComponent);
    return ShowTableColTextAndHrsComponent;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/demurrage-approval-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemurrageApprovalRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_demurage_approval_demurrage_approval_component__ = __webpack_require__("./src/app/demurrage-approval/components/demurage-approval/demurrage-approval.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: ':user_group/:req_transaction_id/:transaction_id/edit',
        component: __WEBPACK_IMPORTED_MODULE_2__components_demurage_approval_demurrage_approval_component__["a" /* DemurrageApprovalComponent */],
        data: {
            title: 'Demurrage Approval Form'
        }
    },
    {
        path: ':user_group/:req_transaction_id/:transaction_id/edit/:is_duplicate',
        component: __WEBPACK_IMPORTED_MODULE_2__components_demurage_approval_demurrage_approval_component__["a" /* DemurrageApprovalComponent */],
        data: {
            title: 'Demurrage Approval Form'
        }
    },
    {
        path: ':user_group',
        component: __WEBPACK_IMPORTED_MODULE_2__components_demurage_approval_demurrage_approval_component__["a" /* DemurrageApprovalComponent */],
        data: {
            title: 'Demurrage Approval Form'
        }
    }
];
var DemurrageApprovalRoutingModule = /** @class */ (function () {
    function DemurrageApprovalRoutingModule() {
    }
    DemurrageApprovalRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], DemurrageApprovalRoutingModule);
    return DemurrageApprovalRoutingModule;
}());



/***/ }),

/***/ "./src/app/demurrage-approval/demurrage-approval.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DemurrageApprovalModule", function() { return DemurrageApprovalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__demurrage_approval_routing_module__ = __webpack_require__("./src/app/demurrage-approval/demurrage-approval-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_demurage_approval_demurrage_approval_component__ = __webpack_require__("./src/app/demurrage-approval/components/demurage-approval/demurrage-approval.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_demurrage_material_demurrage_material_component__ = __webpack_require__("./src/app/demurrage-approval/components/demurrage-material/demurrage-material.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_demurrage_time_summary_demurrage_time_summary_component__ = __webpack_require__("./src/app/demurrage-approval/components/demurrage-time-summary/demurrage-time-summary.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_demurrage_port_demurrage_port_component__ = __webpack_require__("./src/app/demurrage-approval/components/demurrage-port/demurrage-port.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_demurrage_value_usd_summary_demurrage_value_usd_summary_component__ = __webpack_require__("./src/app/demurrage-approval/components/demurrage-value-usd-summary/demurrage-value-usd-summary.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_show_table_col_text_and_hrs_show_table_col_text_and_hrs_component__ = __webpack_require__("./src/app/demurrage-approval/components/show-table-col-text-and-hrs/show-table-col-text-and-hrs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_cip_reference_cip_reference_component__ = __webpack_require__("./src/app/demurrage-approval/components/cip-reference/cip-reference.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_deduction_modal_deduction_modal_component__ = __webpack_require__("./src/app/demurrage-approval/components/deduction-modal/deduction-modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ngx_bootstrap__ = __webpack_require__("./node_modules/ngx-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var DemurrageApprovalModule = /** @class */ (function () {
    function DemurrageApprovalModule() {
    }
    DemurrageApprovalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_2__demurrage_approval_routing_module__["a" /* DemurrageApprovalRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_13_ngx_bootstrap__["a" /* ModalModule */].forRoot(),
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__components_demurage_approval_demurrage_approval_component__["a" /* DemurrageApprovalComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_demurrage_material_demurrage_material_component__["a" /* DemurrageMaterialComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_demurrage_time_summary_demurrage_time_summary_component__["a" /* DemurrageTimeSummaryComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_demurrage_port_demurrage_port_component__["a" /* DemurragePortComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_demurrage_value_usd_summary_demurrage_value_usd_summary_component__["a" /* DemurrageValueUsdSummaryComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_show_table_col_text_and_hrs_show_table_col_text_and_hrs_component__["a" /* ShowTableColTextAndHrsComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_cip_reference_cip_reference_component__["a" /* CipReferenceComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_deduction_modal_deduction_modal_component__["a" /* DeductionModalComponent */],
            ]
        })
    ], DemurrageApprovalModule);
    return DemurrageApprovalModule;
}());



/***/ }),

/***/ "./src/app/models/daf-data.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_DATA; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment__ = __webpack_require__("./node_modules/moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment__);
/**
 * Created by chusri on 23/11/2017 AD.
 */

var DAF_DATA = /** @class */ (function () {
    function DAF_DATA() {
        this.IS_DISABLED = false;
        this.DDA_FOR_COMPANY = '';
        this.DDA_TYPE_OF_TRANSECTION = '';
        this.DDA_DEMURAGE_TYPE = '';
        this.DDA_COUNTERPARTY_TYPE = 'Ship Owner';
        this.SHOW_COUTER_PARTY = '';
        this.DAF_ATTACH_FILE = new Array();
        this.DAF_MATERIAL = new Array();
        this.DAF_PORT = new Array();
        this.DDA_DOC_DATE = __WEBPACK_IMPORTED_MODULE_0_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.DDA_CURRENCY_UNIT = 'USD';
        this.DDA_IS_ADDRESS_COMMISSION = 'N';
        this.DDA_IS_BROKER_COMMISSION = 'N';
        this.DDA_IS_SHARING_TO_TOP = 'N';
        this.DDA_DECIMAL = 2;
        this.DDA_MATERIAL_TYPE = 'Crude & F/S';
        this.DDA_INCOTERM = '';
        this.DDA_DEMURAGE_TYPE = 'Paid';
        this.DDA_SETTLEMENT_AMOUNT_THB = null;
        this.DDA_IS_OVERRIDE_NET_PAYMENT = 'N';
        // this.DAF_PORT.push(new DAF_PORT('load'));
        // this.DAF_PORT.push(new DAF_PORT('discharge'));
        // this = Helper.Deserialize(a);
    }
    return DAF_DATA;
}());



/***/ }),

/***/ "./src/app/models/daf-deduction-activity.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_DEDUCTION_ACTIVITY; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var DAF_DEDUCTION_ACTIVITY = /** @class */ (function () {
    function DAF_DEDUCTION_ACTIVITY(type) {
        this.DDD_TYPE = type;
    }
    return DAF_DEDUCTION_ACTIVITY;
}());



/***/ }),

/***/ "./src/app/models/daf-master.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAFMaster; });
var DAFMaster = /** @class */ (function () {
    function DAFMaster() {
    }
    return DAFMaster;
}());



/***/ }),

/***/ "./src/app/models/daf-material.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_MATERIAL; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var DAF_MATERIAL = /** @class */ (function () {
    function DAF_MATERIAL() {
    }
    return DAF_MATERIAL;
}());



/***/ }),

/***/ "./src/app/models/daf-port.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DAF_PORT; });
/**
 * Created by chusri on 24/11/2017 AD.
 */
var DAF_PORT = /** @class */ (function () {
    function DAF_PORT(type) {
        this.DPT_RT_ORIGIN_CLAIM_T_TEXT = '';
        this.DAF_DEDUCTION_ACTIVITY = new Array();
        this.DPT_TYPE = type;
        this.DPT_RT_T_START_ACTIVITY = 'NOR+6';
        this.DPT_RT_T_STOP_ACTIVITY = 'HOSE DISCONNECTED';
        // this.DPT_RT_ORIGIN_CLAIM_T_START = moment();
        // this.DPT_RT_ORIGIN_CLAIM_T_STOP = moment();
    }
    return DAF_PORT;
}());



/***/ })

});
//# sourceMappingURL=demurrage-approval.module.chunk.js.map