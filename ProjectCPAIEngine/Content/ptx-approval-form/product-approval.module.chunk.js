webpackJsonp(["product-approval.module"],{

/***/ "./src/app/product-approval/components/product-approval/product-approval.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  product-approval works!\n</p>\n"

/***/ }),

/***/ "./src/app/product-approval/components/product-approval/product-approval.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductApprovalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductApprovalComponent = /** @class */ (function () {
    function ProductApprovalComponent() {
    }
    ProductApprovalComponent.prototype.ngOnInit = function () {
    };
    ProductApprovalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-product-approval',
            template: __webpack_require__("./src/app/product-approval/components/product-approval/product-approval.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], ProductApprovalComponent);
    return ProductApprovalComponent;
}());



/***/ }),

/***/ "./src/app/product-approval/product-approval-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductApprovalRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_product_approval_product_approval_component__ = __webpack_require__("./src/app/product-approval/components/product-approval/product-approval.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__components_product_approval_product_approval_component__["a" /* ProductApprovalComponent */],
        data: {
            title: 'Demurrage Approval Form'
        }
    },
];
var ProductApprovalRoutingModule = /** @class */ (function () {
    function ProductApprovalRoutingModule() {
    }
    ProductApprovalRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], ProductApprovalRoutingModule);
    return ProductApprovalRoutingModule;
}());



/***/ }),

/***/ "./src/app/product-approval/product-approval.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductApprovalModule", function() { return ProductApprovalModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product_approval_routing_module__ = __webpack_require__("./src/app/product-approval/product-approval-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_product_approval_product_approval_component__ = __webpack_require__("./src/app/product-approval/components/product-approval/product-approval.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ProductApprovalModule = /** @class */ (function () {
    function ProductApprovalModule() {
    }
    ProductApprovalModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__product_approval_routing_module__["a" /* ProductApprovalRoutingModule */],
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__components_product_approval_product_approval_component__["a" /* ProductApprovalComponent */]]
        })
    ], ProductApprovalModule);
    return ProductApprovalModule;
}());



/***/ })

});
//# sourceMappingURL=product-approval.module.chunk.js.map