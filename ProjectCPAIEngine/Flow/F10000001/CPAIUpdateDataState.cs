﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using Oracle.ManagedDataAccess.Client;
using System.Web.Script.Serialization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Flow.F10000001
{
    public class CPAIUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }
                //set index
                BunkerPurchase dataDetail = JSonConvertUtil.jsonToModel<BunkerPurchase>(item);


                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if(prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });


                //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) ||
                //    (dataDetail.date_purchase != null && dataDetail.vessel != null && dataDetail.supplying_location != null && DateTimeHelper.isParseToDate(dataDetail.date_purchase, "dd/MM/yyyy")))
                //{

                #region setDataHistory
                //add data history
                DataHistoryDAL dthMan = new DataHistoryDAL();
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region setDataDetail
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    dataDetail.reason = note;

                    //set data detail (json when action = approve_1 or DRAFT only)
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    bool isInsertDB = false;
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") ||
                    (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_2") && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                    (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_REJECT)))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isInsertDB = DeleteAndInsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail, prevStatus);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                    {
                        //Verify can edit >> explanation, note
                        isInsertDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.explanation, dataDetail.notes);
                    }
                    else
                    {
                        //other status  ==> UPDATE
                       // isInsertDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note);
                        isInsertDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.explanation, dataDetail.notes);
                        //reject data in data history
                        #region rejectFlag
                        if (isInsertDB)
                        {
                            //if current action is reject, clear reject flag in table data history
                            if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                //set reject flag
                                dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                            }
                        }
                        #endregion
                }
                    #endregion

                    if (isInsertDB) {
                        #region  json
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") ||
                        (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_2") && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                        {
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = dataDetail.vessel, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = dataDetail.date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.SupplyLocation, new ExtendValue { value = dataDetail.supplying_location, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.TripNo, new ExtendValue { value = dataDetail.trip_no, encryptFlag = "N" });
                            //set offersItem
                            foreach (OffersItem of in dataDetail.offers_items)
                            {
                                if (!string.IsNullOrEmpty(of.final_flag) && of.final_flag.Equals("Y"))
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = of.supplier, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.FinalPrice, new ExtendValue { value = of.final_price, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.TotalPrice, new ExtendValue { value = of.total_price, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.OtherCostSur, new ExtendValue { value = of.others_cost_surveyor, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.OtherCostBar, new ExtendValue { value = of.others_cost_barge, encryptFlag = "N" });
                                    break;
                                }
                            }
                            //add products , volumes
                            if (dataDetail.product != null && dataDetail.product.Count > 0)
                            {
                                string products = "";
                                string volumes = "";
                                for (var i = 0; i < dataDetail.product.Count; i++)
                                {
                                    products = products + dataDetail.product[i].product_name + "|";
                                    volumes = volumes + dataDetail.product[i].product_qty + "|";
                                }
                                etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = products, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Volumes, new ExtendValue { value = volumes, encryptFlag = "N" });
                            }
                        }
                        else
                        {
                            if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                            {
                                string dataDetail_a = JSonConvertUtil.modelToJson(dataDetail);
                                etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_a, encryptFlag = "N" });
                            }
                            BunkerPurchase dataDetail_temp = JSonConvertUtil.jsonToModel<BunkerPurchase>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                            dataDetail_temp.reason = note;
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail_temp);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        }
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        #endregion
                        //response
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                //}
                //else
                //{
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                //}

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAIUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {

                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState # :: Exception >>> " + tem);
                log.Error("# Error CPAIUpdateDataState # :: Exception >>> " , ex);
                log.Error("CPAIUpdateDataState::Exception >>> "+ ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool DeleteAndInsertDB(string txId, Dictionary<string, ExtendValue> etxValue, BunkerPurchase dataDetail,string prevStatus)
        {
            log.Info("# Start State CPAIUpdateDataState >> DeleteAndInsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        BNK_DATA_DAL bnkDataDal = new BNK_DATA_DAL();
                        string explantonDetail = "";
                        string noteDetail = "";
                        //if (prevStatus != "NEW")
                        //{
                        //    var bnkDetail = context.BNK_DATA.SingleOrDefault(a => a.BDA_ROW_ID == txId);
                        //    if(bnkDetail != null)
                        //    {
                        //        explantonDetail = bnkDetail.BDA_EXPLANATION;
                        //        noteDetail = bnkDetail.BDA_NOTE;
                        //    }
                        //}
                        // delete old data
                        bool isDeleted = bnkDataDal.DeleteByTransactionId(txId, context);
                        
                        BNK_PRODUCT_ITEMS_DAL bnkProductItemsDal = new BNK_PRODUCT_ITEMS_DAL();
                        BNK_OFFER_ITEMS_DAL bnkOfferItemsDal = new BNK_OFFER_ITEMS_DAL();
                        BNK_ROUND_ITEMS_DAL bnkRoundItemsDal = new BNK_ROUND_ITEMS_DAL();
                        BNK_ROUND_DAL bnkRoundDal = new BNK_ROUND_DAL();
                        try
                        {
                            #region BNK_DATA
                            BNK_DATA bnkData = new BNK_DATA();
                            bnkData.BDA_ROW_ID = txId;
                            bnkData.BDA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            if (!string.IsNullOrEmpty(dataDetail.date_purchase))
                                bnkData.BDA_PURCHASE_DATE = DateTimeHelper.parsePurchaseDate(dataDetail.date_purchase);
                            bnkData.BDA_FK_VESSEL = dataDetail.vessel;
                            bnkData.BDA_VESSEL_OTHERS = dataDetail.vesselothers;
                            bnkData.BDA_TRIP_NO = dataDetail.trip_no;
                            bnkData.BDA_TYPE_OF_PURCHASE = dataDetail.type_of_purchase;
                            bnkData.BDA_CONTRACT_TYPE = dataDetail.contract_type;
                            if (!string.IsNullOrEmpty(dataDetail.delivery_date_range.date_from))
                                bnkData.BDA_DELIVERY_DATE_FROM = DateTimeHelper.parsePurchaseDate(dataDetail.delivery_date_range.date_from);
                            if (!string.IsNullOrEmpty(dataDetail.delivery_date_range.date_to))
                                bnkData.BDA_DELIVERY_DATE_TO = DateTimeHelper.parsePurchaseDate(dataDetail.delivery_date_range.date_to);
                            bnkData.BDA_VOYAGE = dataDetail.voyage;
                            bnkData.BDA_SUPPLYING_LOCATION = dataDetail.supplying_location;
                            bnkData.BDA_OTHER_SPECIAL = dataDetail.remark;

                            if (prevStatus == "NEW" || prevStatus == "DRAFT")
                            {
                                bnkData.BDA_EXPLANATION = dataDetail.explanation;
                                bnkData.BDA_NOTE = dataDetail.notes;
                            }
                            
                            if (dataDetail.payment_terms != null)
                            {
                                bnkData.BDA_PAYMENT_TERMS = dataDetail.payment_terms.payment_term;
                                bnkData.BDA_SUB_PAYMENT_TERMS = dataDetail.payment_terms.sub_payment_term;
                                bnkData.BDA_PAYMENT_TERMS_DETAIL = dataDetail.payment_terms.payment_term_detail;
                            }
                            if (dataDetail.proposal != null)
                            {
                                bnkData.BDA_PROPOSAL_AWARD_TO = dataDetail.proposal.award_to;
                                bnkData.BDA_PROPOSAL_AWARD_PRICE = dataDetail.proposal.award_price;
                                bnkData.BDA_PROPOSAL_REASON = dataDetail.proposal.reason;
                                bnkData.BDA_PROPOSAL_DETAIL = dataDetail.proposal.reasondetail;
                            }
                            if (dataDetail.market_price != null)
                            {
                                bnkData.BDA_MARKET_PRICE_DATE = dataDetail.market_price.market_price_date;
                                bnkData.BDA_MARKET_PRICE_NSFO380CST = dataDetail.market_price.market_price_nsfo380cst;
                                bnkData.BDA_MARKET_PRICE_NSFO180CST = dataDetail.market_price.market_price_nsfo180cst;
                                bnkData.BDA_MARKET_PRICE_MGO = dataDetail.market_price.market_price_mgo;
                            }
                           
                            bnkData.BDA_REQUESTED_DATE = dtNow;
                            bnkData.BDA_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            bnkData.BDA_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                            bnkData.BDA_REASON = dataDetail.reason;
                            bnkData.BDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            bnkData.BDA_CREATED = dtNow;
                            bnkData.BDA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            bnkData.BDA_UPDATED = dtNow;
                            bnkData.BDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            //add currency by poo
                            bnkData.BDA_CURRENCY_CODE = dataDetail.currency;
                            bnkData.BDA_CURRENCY_SYMBOL = dataDetail.currency_symbol;

                            bnkDataDal.Save(bnkData, context);
                            #endregion

                            #region BNK_PRODUCT_ITEMS
                            foreach (Product product in dataDetail.product)
                            {
                                BNK_PRODUCT_ITEMS bnkProductItems = new BNK_PRODUCT_ITEMS();
                                bnkProductItems.BPI_ROW_ID = Guid.NewGuid().ToString("N");
                                bnkProductItems.BPI_FK_BNK_DATA = bnkData.BDA_ROW_ID;
                                bnkProductItems.BPI_ORDER = product.product_order;
                                bnkProductItems.BPI_PRODUCT_NAME = product.product_name;
                                bnkProductItems.BPI_PRODUCT_SPEC = product.product_spec;
                                bnkProductItems.BPI_PRODUCT_QTY = product.product_qty;
                                bnkProductItems.BPI_PRODUCT_UNIT = product.product_unit;
                                bnkProductItems.BPI_CREATED = dtNow;
                                bnkProductItems.BPI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                bnkProductItems.BPI_UPDATED = dtNow;
                                bnkProductItems.BPI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                bnkProductItemsDal.Save(bnkProductItems, context);
                            }
                            #endregion

                            #region BNK_OFFER_ITEMS
                            foreach (OffersItem offerItem in dataDetail.offers_items)
                            {
                                BNK_OFFER_ITEMS bnkOfferItems = new BNK_OFFER_ITEMS();
                                bnkOfferItems.BOI_ROW_ID = Guid.NewGuid().ToString("N");
                                bnkOfferItems.BOI_FK_BNK_DATA = bnkData.BDA_ROW_ID;
                                bnkOfferItems.BOI_FK_VENDOR = offerItem.supplier;
                                bnkOfferItems.BOI_CONTACT_PERSON = offerItem.contact_person;
                                bnkOfferItems.BOI_PURCHASING_TERM = offerItem.purchasing_term;
                                bnkOfferItems.BOI_OTHERS_COST_BARGE = offerItem.others_cost_barge;
                                bnkOfferItems.BOI_OTHERS_COST_SURVEYOR = offerItem.others_cost_surveyor;
                                bnkOfferItems.BOI_FINAL_PRICE = offerItem.final_price;
                                bnkOfferItems.BOI_TOTLA_PRICE = offerItem.total_price;
                                bnkOfferItems.BOI_FINAL_FLAG = offerItem.final_flag;
                                bnkOfferItems.BOI_NOTE = offerItem.note;
                                bnkOfferItems.BOI_CREATED = dtNow;
                                bnkOfferItems.BOI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                bnkOfferItems.BOI_UPDATED = dtNow;
                                bnkOfferItems.BOI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                bnkOfferItemsDal.Save(bnkOfferItems, context);

                                #region BNK_ROUND_ITEMS
                                foreach (RoundItem roundItem in offerItem.round_items)
                                {
                                    BNK_ROUND_ITEMS bnkRoundItems = new BNK_ROUND_ITEMS();

                                    bnkRoundItems.BRI_ROW_ID = Guid.NewGuid().ToString("N");
                                    bnkRoundItems.BRI_FK_OFFER_ITEMS = bnkOfferItems.BOI_ROW_ID;
                                    bnkRoundItems.BRI_ROUND_NO = roundItem.round_no;
                                    bnkRoundItems.BRI_CREATED = dtNow;
                                    bnkRoundItems.BRI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    bnkRoundItems.BRI_UPDATED = dtNow;
                                    bnkRoundItems.BRI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    bnkRoundItemsDal.Save(bnkRoundItems, context);

                                    #region BNK_ROUND
                                    foreach (RoundInfo roundInfo in roundItem.round_info)
                                    {
                                        foreach (RoundValues roundValue in roundInfo.round_value)
                                        {
                                            BNK_ROUND bnkRound = new BNK_ROUND();
                                            bnkRound.BRD_ROW_ID = Guid.NewGuid().ToString("N");
                                            bnkRound.BRD_FK_ROUND_ITEMS = bnkRoundItems.BRI_ROW_ID;
                                            bnkRound.BRD_ROUND_TYPE = roundInfo.round_type;
                                            bnkRound.BRD_ROUND_VALUE = roundValue.value;
                                            bnkRound.BRD_ROUND_ORDER = roundValue.order;
                                            bnkRound.BRD_CREATED = dtNow;
                                            bnkRound.BRD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            bnkRound.BRD_UPDATED = dtNow;
                                            bnkRound.BRD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            bnkRoundDal.Save(bnkRound, context);
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            #endregion

                            #region BNK_ATTACH_FILE
                            BNK_ATTACH_FILE_DAL dataAttachFileDAL = new BNK_ATTACH_FILE_DAL();
                            BNK_ATTACH_FILE dataBNKAF;
                            foreach (var item in dataDetail.explanationAttach.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    string[] text = item.Split(':');
                                    dataBNKAF = new BNK_ATTACH_FILE();
                                    dataBNKAF.BAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataBNKAF.BAF_FK_BNK_DATA = bnkData.BDA_ROW_ID;
                                    dataBNKAF.BAF_PATH = text[0];
                                    dataBNKAF.BAF_INFO = text[1];
                                    dataBNKAF.BAF_TYPE = "EXT";
                                    dataBNKAF.BAF_CREATED = dtNow;
                                    dataBNKAF.BAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataBNKAF.BAF_UPDATED = dtNow;
                                    dataBNKAF.BAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataBNKAF, context);
                                }
                            }

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                            foreach (var item in Att.attach_items)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    dataBNKAF = new BNK_ATTACH_FILE();
                                    dataBNKAF.BAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataBNKAF.BAF_FK_BNK_DATA = bnkData.BDA_ROW_ID;
                                    dataBNKAF.BAF_PATH = item;
                                    dataBNKAF.BAF_INFO = "";
                                    dataBNKAF.BAF_TYPE = "ATT";
                                    dataBNKAF.BAF_CREATED = dtNow;
                                    dataBNKAF.BAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataBNKAF.BAF_UPDATED = dtNow;
                                    dataBNKAF.BAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataBNKAF, context);
                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                            log.Info("# End State CPAIUpdateDataState >> DeleteAndInsertDB # ");
                           
                        }
                        catch (Exception ex)
                        {

                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> " + tem);
                            log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error CPAIUpdateDataState >> DeleteAndInsertDB # :: Exception >>> " + tem);
                log.Error("# Error CPAIUpdateDataState >> DeleteAndInsertDB # :: Exception >>> ", e);
                log.Error("CPAIUpdateDataState::DeleteAndInsertDB >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue,string Reason)
        {
            log.Info("# Start State CPAIUpdateDataState >> UpdateDB #  ");
            
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            BNK_DATA_DAL dataDAL = new BNK_DATA_DAL();
                            BNK_DATA dataBNK = new BNK_DATA();

                            dataBNK.BDA_ROW_ID = TransID;
                            dataBNK.BDA_REASON = Reason;
                            dataBNK.BDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataBNK.BDA_UPDATED = DateTime.Now;
                            dataBNK.BDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataBNK, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> " + tem);
                            log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIUpdateDataState >> UpdateDB #  ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>> " , ex);
                log.Error("CPAIUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note, string explanation = null, string note_2 = null)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIUpdateDataState >> UpdateDB when verify #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            
                            BNK_DATA_DAL dataDAL = new BNK_DATA_DAL();
                            BNK_DATA dataBNK = new BNK_DATA();
                            dataBNK.BDA_ROW_ID = TransID;
                            dataBNK.BDA_REASON = note;
                            //if (explanation != null)
                            //{
                            //    dataBNK.BDA_EXPLANATION = explanation;
                            //}
                            //if (note_2 != null)
                            //{
                            //    dataBNK.BDA_NOTE = note_2;
                            //}
                            dataBNK.BDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataBNK.BDA_UPDATED = DateTime.Now;
                            dataBNK.BDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataBNK, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                            log.Error("# Error CPAIUpdateCDPHDataState # :: Rollback >>> ", ex);
                            log.Error("CPAIUpdateCDPHDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }

                    }
                }

                log.Info("# End State CPAIUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error CPAIUpdateDataState # :: UpdateDB >>> ", ex);
                log.Error("CPAIUpdateDataState::UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }


    }
}
