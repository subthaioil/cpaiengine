﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000002
{
    public class CPAICheckResendTxnState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICheckResendTxnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {

                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string transId = etxValue.GetValue(CPAIConstantUtil.ReSendTransactionID);

                string queryString = string.Format("SELECT * FROM FUNCTION_TRANSACTION WHERE FTX_REQ_TRANS = '{0}' AND FTX_INDEX4 LIKE '%WAITING_%'", transId);
               
                
                // TODO: get value
                string fncFunctionCode = etxValue.GetValue(CPAIConstantUtil.FNC_FUNCTION_CODE);
                etxValue.SetValue(CPAIConstantUtil.FNC_FUNCTION_CODE, fncFunctionCode);

                string ftxIndex1 = etxValue.GetValue(CPAIConstantUtil.FTX_INDEX1);
                etxValue.SetValue(CPAIConstantUtil.System, ftxIndex1);

                string ftxIndex2 = etxValue.GetValue(CPAIConstantUtil.FTX_INDEX2);
                etxValue.SetValue(CPAIConstantUtil.Type, ftxIndex2);

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

           
                
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICheckResendTxnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICheckResendTxnState # :: Exception >>> " + ex);
                log.Error("CPAICheckResendTxnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
