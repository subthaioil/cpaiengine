﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.downstream;
using DSAd.service;
using DSAd.model;
using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Flow.Model;
using System.Diagnostics;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.dao;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000003
{
    public class CPAICheckADState : BasicBean, StateFlowAction
    {
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICheckADState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                //call downstream send mail
                DownstreamController<ADServiceConnectorImpl, ResponseLoginADModel> connector = new DownstreamController<ADServiceConnectorImpl, ResponseLoginADModel>(new ADServiceConnectorImpl(), new ResponseLoginADModel());
                //find by config and content from DB
                ConfigManagement configManagement = new ConfigManagement();
                String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
                String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);
                Debug.WriteLine("config : " + config + "content : " + content);
                var contentObj = JSonConvertUtil.jsonToModel<ADContent>(content);
                contentObj.username = etxValue.GetValue(CPAIConstantUtil.UserId);

                string decoded = DecodeUrlString(etxValue.GetValue(CPAIConstantUtil.UserPassword));
                string realPassword = decryptRSAPassword(stateModel, decoded);

                contentObj.pwd = realPassword;

                String finalContent = JSonConvertUtil.modelToJson(contentObj);

                Debug.WriteLine("config : " + config + "finalContent : " + finalContent);

                //call downstream
                DownstreamResponse <ResponseLoginADModel> downResp = connector.invoke(stateModel, config, finalContent);
                currentCode = downResp.getResultCode();
                stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICheckADState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICheckADState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private String decryptRSAPassword(StateModel stateModel, string password)
        {
            log.Info("# Start State CPAICheckADState >> decryptRSAPassword #  ");
            AppUserDao appUserDao = new AppUserDaoImpl();
            SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
            APP_USER appUser = appUserDao.findByAppId(stateModel.EngineModel.appUser);
            if (appUser != null)
            {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PRIVATE_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                {
                    String priKey = scn.SCN_VALUE;
                    String cipherText = password.Trim();
                    try
                    {
                        String decryptMsg = RSAHelper.DecryptText(cipherText, priKey);
                        log.Info("# End State CPAICheckADState >> decryptRSAPassword # :: Code >>> " + currentCode);
                        return decryptMsg;
                    }
                    catch (Exception ex)
                    {
                        currentCode = CPAIConstantRespCodeUtil.CANNOT_DECRYPT_MESSAGE_RESP_CODE;
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        log.Error("# Error CPAICheckADState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                        throw new Exception();
                    }
                }
                else
                {
                    //can not get priKey
                    currentCode = ConstantRespCodeUtil.CAN_NOT_GET_KEY_ENGINE_RESP_CODE[0];
                    log.Info("# Error CPAICheckADState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                    throw new Exception();
                }
            }
            else
            {
                currentCode = ConstantRespCodeUtil.NOT_FOUND_APP_USER_ENGINE_RESP_CODE[0];
                log.Info("# Error CPAICheckADState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                throw new Exception();
            }
           
        }
        private string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }
    }
}
