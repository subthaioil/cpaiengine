﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.dao;
using ProjectCPAIEngine.Utilities;
using System.ComponentModel;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.DAL.DALMaster;

namespace ProjectCPAIEngine.Flow.F10000003
{
    public class CPAICheckMobileFlagState : BasicBean, StateFlowAction
    {
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICheckMobileFlagState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;


            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string channel = etxValue.GetValue(CPAIConstantUtil.Channel);
                string UserMobileFlag = etxValue.GetValue(CPAIConstantUtil.UserMobileFlag);
                string UserAppVersion = etxValue.GetValue(CPAIConstantUtil.UserAppVersion);
                string UserOS = etxValue.GetValue(CPAIConstantUtil.UserOS);

                if (channel.Equals(CPAIConstantUtil.MOBILE))
                {
                    if (UserMobileFlag.Equals(CPAIConstantUtil.YES_FLAG))
                    {
                        string current_version = MasterData.GetConfigData(CPAIConstantUtil.GC_IOS_VERSION)[0].GCG_VALUE;
                        if (UserOS.ToUpper().Equals(CPAIConstantUtil.ANDROID))
                        {
                            current_version = MasterData.GetConfigData(CPAIConstantUtil.GC_ANDROID_VERSION)[0].GCG_VALUE;
                        }
                        //check mobile version
                        if (!UserAppVersion.Equals(current_version))
                        {
                            currentCode = CPAIConstantRespCodeUtil.UPDATE_MOBILE_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.NOT_AUTHORIZED_RESP_CODE;
                    }
                }
                else
                {
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICheckMobileFlagState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICheckMobileFlagState # :: Exception >>> " + ex);
                log.Error("CPAICheckMobileFlagState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private String decryptRSAPassword(StateModel stateModel, string password)
        {
            log.Info("# Start State CPAICheckMobileFlagState >> decryptRSAPassword #  ");
            AppUserDao appUserDao = new AppUserDaoImpl();
            SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
            APP_USER appUser = appUserDao.findByAppId(stateModel.EngineModel.appUser);
            if (appUser != null)
            {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PRIVATE_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                {
                    String priKey = scn.SCN_VALUE;
                    String cipherText = password.Trim();
                    try
                    {
                        String decryptMsg = RSAHelper.DecryptText(cipherText, priKey);
                        log.Info("# End State CPAICheckMobileFlagState >> decryptRSAPassword # :: Code >>> " + currentCode);
                        return decryptMsg;
                    }
                    catch (Exception ex)
                    {
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        currentCode = CPAIConstantRespCodeUtil.CANNOT_DECRYPT_MESSAGE_RESP_CODE;
                        log.Info("# Error CPAICheckMobileFlagState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                        throw new Exception();
                    }
                }
                else
                {
                    
                    //can not get priKey
                    currentCode = ConstantRespCodeUtil.CAN_NOT_GET_KEY_ENGINE_RESP_CODE[0];
                    log.Info("# Error CPAICheckMobileFlagState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                    throw new Exception();
                }
            }
            else
            {               
                currentCode = ConstantRespCodeUtil.NOT_FOUND_APP_USER_ENGINE_RESP_CODE[0];
                log.Info("# Error CPAICheckMobileFlagState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                throw new Exception();
            }
            
        }
        private string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }
    }
}
