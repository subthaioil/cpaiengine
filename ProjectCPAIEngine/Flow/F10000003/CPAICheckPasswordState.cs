﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.dao;
using ProjectCPAIEngine.Utilities;
using System.ComponentModel;
using com.pttict.downstream.common.utilities;

namespace ProjectCPAIEngine.Flow.F10000003
{
    public class CPAICheckPasswordState : BasicBean, StateFlowAction
    {
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICheckPasswordState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;


            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //string publicKey = "MTAyNCE8UlNBS2V5VmFsdWU+PE1vZHVsdXM+dnFCOW0vdmN3WFRzTG9JTE5ScFZuK3l3RGI1a3N2THNadzkyTS9La3VXQXY2SUlRckhtRzZKTmVPZVlVQU5FQjJTRFFBVmtyamYxaTZXSWVCSzZ6UFUxQXRyOHoyZUNRSUlTTFd6YThuc2ZOcDVzc3NsdGRUd0crUGUvNXJYUUZQMlFpbXRXRVFlQjFQVXZhd1JIUEIxMFh6TmhnUk5UZVVOQjZNNjhWWWRjPTwvTW9kdWx1cz48RXhwb25lbnQ+QVFBQjwvRXhwb25lbnQ+PC9SU0FLZXlWYWx1ZT4=";
                //string cipher = RSAHelper.EncryptText(etxValue.GetValue(CPAIConstantUtil.UserPassword), publicKey);
                //string realPassword = decryptRSAPassword(stateModel, cipher);
                string decoded = DecodeUrlString(etxValue.GetValue(CPAIConstantUtil.UserPassword));
                string realPassword = decryptRSAPassword(stateModel, decoded);
                string md5password = Cryptography.GetMD5(realPassword);
                string passwordFromDb = etxValue.GetValue(CPAIConstantUtil.LoginUserPassword);

                if (string.Equals(md5password, passwordFromDb, StringComparison.OrdinalIgnoreCase))
                {
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_PASSWORD_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICheckPasswordState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICheckPasswordState # :: Exception >>> " + ex);
                log.Error("CPAICheckPasswordState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private String decryptRSAPassword(StateModel stateModel, string password)
        {
            log.Info("# Start State CPAICheckPasswordState #  ");
            AppUserDao appUserDao = new AppUserDaoImpl();
            SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
            APP_USER appUser = appUserDao.findByAppId(stateModel.EngineModel.appUser);
            if (appUser != null)
            {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PRIVATE_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                {
                    String priKey = scn.SCN_VALUE;
                    String cipherText = password.Trim();
                    try
                    {
                        String decryptMsg = RSAHelper.DecryptText(cipherText, priKey);
                        log.Info("# End State CPAICheckPasswordState >> decryptRSAPassword # :: Code >>> " + currentCode);
                        return decryptMsg;
                    }
                    catch (Exception ex)
                    {
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        currentCode = CPAIConstantRespCodeUtil.CANNOT_DECRYPT_MESSAGE_RESP_CODE;
                        log.Info("# Error CPAICheckPasswordState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                        throw new Exception();
                    }
                }
                else
                {
                    //can not get priKey
                    currentCode = ConstantRespCodeUtil.CAN_NOT_GET_KEY_ENGINE_RESP_CODE[0];
                    log.Info("# Error CPAICheckPasswordState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                    throw new Exception();
                }
            }
            else
            {
                currentCode = ConstantRespCodeUtil.NOT_FOUND_APP_USER_ENGINE_RESP_CODE[0];
                log.Info("# Error CPAICheckPasswordState >> decryptRSAPassword # :: Exception >>> " + currentCode);
                throw new Exception();
            }
        }
        private string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }
    }
}
