﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;

namespace ProjectCPAIEngine.Flow.F10000003
{
    public class CPAIGetUserDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGetUserDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string userId = etxValue.GetValue(CPAIConstantUtil.UserId);
                string userSystem = etxValue.GetValue(CPAIConstantUtil.UserSystem);

                UsersDAL userMan = new UsersDAL();
                List<USERS> results = userMan.findByLoginAndSystem(userId, userSystem);
                if (results.Count != 1)
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_ID_RESP_CODE;
                }
                else
                {
                    USERS user = results[0];
                    String mobile_flag = user.USR_MOBILE_FLAG != null ? user.USR_MOBILE_FLAG : "";
                    String topics = user.USR_NOTI_TOPICS != null ? user.USR_NOTI_TOPICS : "";
                    //by poo add message flag
                    String noti_flag = user.USR_NOTI_FLAG != null ? user.USR_NOTI_FLAG : "";
                    String sms_flag = user.USR_SMS_FLAG != null ? user.USR_SMS_FLAG : "";
                    String email_flag = user.USR_EMAIL_FLAG != null ? user.USR_EMAIL_FLAG : "";

                    ExtendValue extPwd = new ExtendValue { value = user.USR_PASSWORD };
                    //ExtendValue extUserGroup = new ExtendValue { value = user.USR_GROUP };
                    ExtendValue extADLoginFlag = new ExtendValue { value = user.USR_AD_LOGIN_FLAG };
                    ExtendValue extNotificationTopics = new ExtendValue { value = topics };
                    ExtendValue extMobileFlag = new ExtendValue { value = mobile_flag };

                    //by poo add message flag
                    ExtendValue extNotiFlag = new ExtendValue { value = noti_flag };
                    ExtendValue extSmsFlag = new ExtendValue { value = sms_flag };
                    ExtendValue extEmailFlag = new ExtendValue { value = email_flag };
                    ExtendValue extMobileMenu = new ExtendValue { value = "{}" };
                    ExtendValue extWebMenu = new ExtendValue { value = "{}" };

                    //etxValue.Add(CPAIConstantUtil.LoginUserGroup, extUserGroup);
                    etxValue.Add(CPAIConstantUtil.UserADFlag, extADLoginFlag);
                    etxValue.Add(CPAIConstantUtil.LoginUserPassword, extPwd);
                    etxValue.Add(CPAIConstantUtil.UserMobileFlag, extMobileFlag);

                    //by poo add message flag
                    etxValue.Add(CPAIConstantUtil.NotiFlag, extNotiFlag);
                    etxValue.Add(CPAIConstantUtil.SmsFlag, extSmsFlag);
                    etxValue.Add(CPAIConstantUtil.EmailFlag, extEmailFlag);

                    // set response
                    //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.UserGroupRespValue, extUserGroup);
                    etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.UserNotiTopicsRespValue, extNotificationTopics);

                    //set menu deligate by poo 03082017
                    UserGroupDAL service = new UserGroupDAL();
                    List<CPAI_USER_GROUP> dlg = service.findByUserDelegate(etxValue.GetValue(CPAIConstantUtil.User));
                    string menu_mobile = user.USR_MOBILE_MENU != null ? user.USR_MOBILE_MENU : "";
                    string menu_web = user.USR_WEB_MENU != null ? user.USR_WEB_MENU : "";
                    if (dlg != null && dlg.Count > 0)
                    {
                        MenuTab menu_mobile_model = JSonConvertUtil.jsonToModel<MenuTab>(menu_mobile) != null ? JSonConvertUtil.jsonToModel<MenuTab>(menu_mobile) : new MenuTab();
                        MenuTab menu_web_model = JSonConvertUtil.jsonToModel<MenuTab>(menu_web) != null ? JSonConvertUtil.jsonToModel<MenuTab>(menu_web) : new MenuTab();
                        //have delegate
                        foreach (CPAI_USER_GROUP tem in dlg)
                        {
                            string dlg_system = tem.USG_USER_SYSTEM;  //map to name tag in menu
                            string dlg_by = tem.USG_FK_DELEGATE_BY;
                            //get dlg_menu
                            List<USERS> dlg_users = userMan.findByRowId(dlg_by);
                            if (dlg_users.Count > 0)
                            {
                                USERS dlg_user = dlg_users[0];
                                if (!menu_mobile.Contains(dlg_system))
                                {
                                    string dlg_mobile_menu = dlg_user.USR_MOBILE_MENU;

                                    if (!string.IsNullOrEmpty(dlg_mobile_menu))
                                    {
                                        MenuTab dlg_menu_mobile_model = JSonConvertUtil.jsonToModel<MenuTab>(dlg_mobile_menu);
                                        List<Menu> tem_m = menu_mobile_model.menu != null ? menu_mobile_model.menu : new List<Menu>();
                                        foreach (Menu tem1 in dlg_menu_mobile_model.menu)
                                        {
                                            if (tem1.name == dlg_system)
                                            {
                                                tem_m.Add(tem1);
                                            }
                                        }
                                        menu_mobile_model.menu = tem_m;
                                        menu_mobile = JSonConvertUtil.modelToJson(menu_mobile_model);
                                    }
                                }

                                if (!menu_web.Contains(dlg_system))
                                {
                                    string dlg_web_menu = dlg_user.USR_WEB_MENU;
                                    MenuTab dlg_menu_web_model = JSonConvertUtil.jsonToModel<MenuTab>(dlg_web_menu);
                                    List<Menu> tem_m2 = menu_web_model.menu != null ? menu_web_model.menu : new List<Menu>();
                                    foreach (Menu tem2 in dlg_menu_web_model.menu)
                                    {
                                        if (tem2.name == dlg_system)
                                        {
                                            tem_m2.Add(tem2);
                                        }
                                    }
                                    menu_web_model.menu = tem_m2;
                                    menu_web = JSonConvertUtil.modelToJson(menu_web_model);

                                }
                            }
                        }
                    }
                    //End

                    if (user.USR_MOBILE_FLAG != null && user.USR_MOBILE_FLAG.Equals(CPAIConstantUtil.YES_FLAG))
                    {
                        string os = etxValue.GetValue(CPAIConstantUtil.UserOS) != null ? etxValue.GetValue(CPAIConstantUtil.UserOS) : "";
                        string app_version = etxValue.GetValue(CPAIConstantUtil.UserAppVersion) != null ? etxValue.GetValue(CPAIConstantUtil.UserAppVersion) : "";
                        string noti_id = etxValue.GetValue(CPAIConstantUtil.UserNotificationId) != null ? etxValue.GetValue(CPAIConstantUtil.UserNotificationId) : "";
                        user.USR_OS = os;
                        user.USR_APP_VERSION = app_version;
                        user.USR_NOTI_ID = noti_id;
                        userMan.Update(user);
                        //set mobile menu
                        extMobileMenu = new ExtendValue { value = menu_mobile };
                        etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.UserMobileMenu, extMobileMenu);
                    }
                    //set web menu
                    extWebMenu = new ExtendValue { value = menu_web };
                    etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.UserWebMenu, extWebMenu);

                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIGetUserDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIGetUserDataState # :: Exception >>> " + ex);
                log.Error("CPAIGetUserDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
