﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.dal.dao;

namespace ProjectCPAIEngine.Flow.F10000004
{
    public class CPAIListTxnState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIListTxnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            //string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();
                List<CPAI_ACTION_FUNCTION> result;

                if (String.IsNullOrWhiteSpace(function_code))
                {
                    //result = acfMan.findByUserGroupAndSystem(userGroup, system);
                    result = acfMan.findByUserGroupAndSystemDelegate(lstUserGroup, system);//set user group deligate by poo 25072017
                }
                else
                {
                    //result = acfMan.findByUserGroupAndSystemAndFuncitonId(userGroup, system, function_code);
                    result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, function_code);//set user group deligate by poo 25072017
                }

                List<Transaction> lstAllTx = new List<Transaction>();
                if (result.Count == 0)
                {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                else if (result.Count == 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else
                {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER))
                        {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null)
                    {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    }
                    else
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                        }
                        setExtraXml(stateModel, lstAllTx);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIListTxnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIListTxnState # :: Exception >>> " + ex);
                log.Error("CPAIListTxnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public List<Transaction> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction)
        {
            log.Info("# Start State CPAIListTxnState >> getTransaction #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
            List<FunctionTransaction> result;
            List<Transaction> list = new List<Transaction>();

            string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
            string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);
            string status = etxValue.GetValue(CPAIConstantUtil.Status);
            string mobileFlag = etxValue.GetValue(CPAIConstantUtil.MobileFlag);


            List<string> lstStatus = new List<string>();

            if (string.IsNullOrWhiteSpace(status))
            {
                if (mobileFlag.Equals(CPAIConstantUtil.YES_FLAG))
                {
                    if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS_MB))
                    {
                        lstStatus.Add("ALL");
                    }
                    else
                    {
                        status = actionFunction.ACF_FUNCTION_STATUS_MB;
                        lstStatus = new List<string>(status.Split('|'));
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS))
                    {
                        lstStatus.Add("ALL");
                    }
                    else
                    {
                        status = actionFunction.ACF_FUNCTION_STATUS;
                        lstStatus = new List<string>(status.Split('|'));
                    }
                }

            }
            else
            {
                lstStatus = new List<string>(status.Split('|'));
            }

            // create index to make search criteria
            var lstIndexValues = new List<string>();
            lstIndexValues.Add(actionFunction.ACF_SYSTEM);
            lstIndexValues.Add(actionFunction.ACF_TYPE);
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index3));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index4));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index5));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index6));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index7));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index8));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index9));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index10));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index11));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index12));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index13));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index14));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index15));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index16));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index17));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index18));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index19));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index20));

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                result = ftxMan.findCPAIBunkerTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, fromDate, toDate);
            }
            else
            {
                result = ftxMan.findCPAIBunkerTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, null, null);
            }

            if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.BUNKER_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.supplying_location = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                    m.products = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.volumes = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.supplier_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX12 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.supplier = ftx.SUPPLIER_NAME == null ? "" : ftx.SUPPLIER_NAME;
                    m.trip_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX13 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                    m.others_cost_barge = ftx.FUNCTION_TRANSACTION.FTX_INDEX16 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                    m.others_cost_surveyor = ftx.FUNCTION_TRANSACTION.FTX_INDEX15 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                    /* if (!String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX17) && !ftx.FUNCTION_TRANSACTION.FTX_INDEX17.Equals(" "))
                     {
                         m.final_price = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX17) || ftx.FUNCTION_TRANSACTION.FTX_INDEX17.Equals(" ") ? "-" : encryptRSAData(stateModel, ftx.FUNCTION_TRANSACTION.FTX_INDEX17); //Total price
                     }
                     else
                     {
                         m.final_price = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX14) || ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Equals(" ") ? "-" : encryptRSAData(stateModel, ftx.FUNCTION_TRANSACTION.FTX_INDEX14);

                     }*/

                    m.final_price = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX14) || ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Equals(" ") ? "-" : encryptRSAData(stateModel, ftx.FUNCTION_TRANSACTION.FTX_INDEX14);

                    m.delivery_date_from = ftx.DELIVERY_DATE_FROM == null ? "" : ftx.DELIVERY_DATE_FROM;
                    m.delivery_date_to = ftx.DELIVERY_DATE_TO == null ? "" : ftx.DELIVERY_DATE_TO;
                    m.unit_price_order = ftx.UNIT_PRICE_ORDER == null ? "" : ftx.UNIT_PRICE_ORDER;
                    m.unit_price_value = ftx.UNIT_PRICE_VALUE == null ? "" : ftx.UNIT_PRICE_VALUE;
                    m.unit_price_type = ftx.UNIT_PRICE_TYPE == null ? "" : ftx.UNIT_PRICE_TYPE;
                    m.currency_symbol = ftx.CURRENCY_SYMBOL == null ? "" : ftx.CURRENCY_SYMBOL;
                    list.Add(m);
                }
            }
            else if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.CHARTER_IN_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    //m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    //m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    //m.vessel = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.flat_rate = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                    m.laycan_from = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                    m.laycan_to = ftx.FUNCTION_TRANSACTION.FTX_INDEX20;
                    m.cust_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX11 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.cust_name = ftx.BROKER_NAME != null ? ftx.BROKER_NAME : ftx.CHARTERER_NAME != null ? ftx.CHARTERER_NAME : "";
                    m.broker_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.broker_name = ftx.BROKER_NAME;
                    m.owner = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                    m.final_price = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX14) || ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Equals(" ") ? "-" : encryptRSAData(stateModel, ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Replace("\r\n", "|"));
                    m.exten_cost = ftx.EXTEN_COST == null ? "" : ftx.EXTEN_COST;
                    m.est_freight = ftx.EST_FREIGHT == null ? "" : ftx.EST_FREIGHT;
                    list.Add(m);
                }
            }
            else if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.CHARTER_IN_CMMT_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    //m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    //m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    m.vessel = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.flat_rate = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                    m.laycan_from = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                    m.laycan_to = ftx.FUNCTION_TRANSACTION.FTX_INDEX20;
                    m.cust_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX11 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.cust_name = ftx.BROKER_NAME != null ? ftx.BROKER_NAME : ftx.CHARTERER_NAME != null ? ftx.CHARTERER_NAME : "";
                    m.broker_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.broker_name = ftx.BROKER_NAME;
                    m.owner = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                    m.final_price = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX14) || ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Equals(" ") ? "-" : encryptRSAData(stateModel, ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Replace("\r\n", "|"));
                    m.exten_cost = ftx.EXTEN_COST == null ? "" : ftx.EXTEN_COST;
                    m.est_freight = ftx.EST_FREIGHT == null ? "" : ftx.EST_FREIGHT;
                    m.charterer = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.charterer_name = ftx.CHARTERER_NAME == null ? "" : ftx.CHARTERER_NAME;
                    string unit = ftx.FUNCTION_TRANSACTION.FTX_INDEX17;
                    m.unit_price_value = unit;
                    m.load_port = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.discharge_port = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    list.Add(m);
                }
            }
            else if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.CHARTER_OUT_CMMT_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                    //
                    m.broker_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.broker_name = ftx.BROKER_NAME;
                    //
                    m.cust_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX11 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.cust_name = ftx.CHARTERER_NAME == null ? "" : ftx.CHARTERER_NAME;
                    //
                    m.laycan_from = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                    m.laycan_to = ftx.FUNCTION_TRANSACTION.FTX_INDEX20;
                    //
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                    //
                    m.no_total_ex = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                    m.total_ex = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                    m.net_benefit = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                    m.result_flag = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    string unit = ftx.FUNCTION_TRANSACTION.FTX_INDEX17;
                    m.unit_price_value = unit;
                    m.load_port = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.discharge_port = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.est_freight = ftx.FUNCTION_TRANSACTION.FTX_INDEX18;
                    //
                    list.Add(m);
                }
            }
            else if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.CHARTER_OUT_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.flat_rate = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                    m.laycan_from = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                    m.laycan_to = ftx.FUNCTION_TRANSACTION.FTX_INDEX20;
                    m.cust_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX11 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.cust_name = ftx.CHARTERER_NAME == null ? "" : ftx.CHARTERER_NAME;
                    m.broker_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.broker_name = ftx.BROKER_NAME == null ? "" : ftx.BROKER_NAME;
                    //m.min_load = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                    //m.final_price = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX14) || ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Equals(" ") ? "-" : encryptRSAData(stateModel, ftx.FUNCTION_TRANSACTION.FTX_INDEX14.Replace("\r\n", "|"));
                    m.ws = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.route = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    m.no_total_ex = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                    m.total_ex = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                    m.net_benefit = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                    list.Add(m);
                }
            }
            else if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.FREIGHT_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                    m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                    m.update_date = ftx.FUNCTION_TRANSACTION.FTX_UPDATED == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_UPDATED.ToString("dd/MM/yyyy HH:mm");
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                    // by function
                    m.laycan_from = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                    m.laycan_to = ftx.FUNCTION_TRANSACTION.FTX_INDEX20;
                    m.laytime = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                    m.charterer = ftx.FUNCTION_TRANSACTION.FTX_INDEX10 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.charterer_name = ftx.CHARTERER_NAME == null ? "" : ftx.CHARTERER_NAME;
                    m.broker = ftx.FUNCTION_TRANSACTION.FTX_INDEX12 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.broker_name = ftx.BROKER_NAME != null && !string.IsNullOrEmpty(ftx.BROKER_NAME) ? ftx.BROKER_NAME : m.broker;
                    m.cargo = ftx.FUNCTION_TRANSACTION.FTX_INDEX11 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.cargo_name = ftx.CARGO_NAME == null ? "" : ftx.CARGO_NAME;
                    m.load_port = ftx.FUNCTION_TRANSACTION.FTX_INDEX13 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    m.load_port_name = ftx.LOAD_PORT_NAME == null ? "" : ftx.LOAD_PORT_NAME;
                    m.discharge_port = ftx.FUNCTION_TRANSACTION.FTX_INDEX14 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                    m.discharge_port_name = ftx.DISCHARGE_PORT_NAME == null ? "" : ftx.DISCHARGE_PORT_NAME;
                    //

                    list.Add(m);
                }
            }
            else if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.CRUDE_PURCHASE_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);

                    // By Function
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.feed_stock = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;

                    m.products = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.product_name = ftx.FUNCTION_TRANSACTION.FTX_INDEX7.Equals("CRUDE") ? ftx.PRODUCT_NAME : ftx.FUNCTION_TRANSACTION.FTX_INDEX17;
                    m.volumes = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.supplier = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.supplier_name = ftx.SUPPLIER_NAME;
                    m.margin = ftx.FUNCTION_TRANSACTION.FTX_INDEX18;
                    /*
                      "index_13" : "incoterm",
                      "index_14" : "purchase_p",
                      "index_15" : "market_p",
                      "index_16" : "lp_p",
                     */
                    m.incoterm = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    //set "," String.Format("{0:n}", t2)
                    m.purchase_p = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                    m.market_p = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                    m.lp_p = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                    //
                    string t1 = ftx.LOADING_PERIOD_FROM != null && !String.IsNullOrEmpty(ftx.LOADING_PERIOD_FROM) ? ftx.LOADING_PERIOD_FROM : "-";
                    string t2 = ftx.LOADING_PERIOD_TO != null && !String.IsNullOrEmpty(ftx.LOADING_PERIOD_TO) ? ftx.LOADING_PERIOD_TO : "-";
                    string t3 = ftx.DISCHARGING_PERIOD_FROM != null && !String.IsNullOrEmpty(ftx.DISCHARGING_PERIOD_FROM) ? ftx.DISCHARGING_PERIOD_FROM : "-";
                    string t4 = ftx.DISCHARGING_PERIODE_TO != null && !String.IsNullOrEmpty(ftx.DISCHARGING_PERIODE_TO) ? ftx.DISCHARGING_PERIODE_TO : "-";
                    if (t2.Equals("-"))
                    {
                        m.loading_period = t1;
                    }
                    else
                    {
                        m.loading_period = t1 + " to " + t2;
                    }

                    if (t4.Equals("-"))
                    {
                        m.discharging_period = t3;
                    }
                    else
                    {
                        m.discharging_period = t3 + " to " + t4;
                    }
                    list.Add(m);
                }
            }
            log.Info("# End State CPAIListTxnState >> getTransaction # :: Code >>> " + currentCode);
            return list;
        }

        public void setExtraXml(StateModel stateModel, List<Transaction> lstAllTx)
        {
            log.Info("# Start State CPAIListTxnState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            TransactionList li = new TransactionList();
            li.trans = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAIListTxnState >> setExtraXml # :: Code >>> " + currentCode);
        }

        public string getReason(string row_id)
        {
            log.Info("# Start State CPAIListTxnState >> getReason #  ");
            string reason = "-";
            ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
            var etxResult = etxMan.findByFtxRowIdAndKey(row_id, CPAIConstantUtil.Note);
            if (etxResult != null && etxResult.Count > 0)
            {
                reason = etxResult[0].ETX_VALUE;
            }

            log.Info("# End State CPAIListTxnState >> getReason # :: Code >>> " + currentCode);
            return reason;
        }

        private String encryptRSAData(StateModel stateModel, string data)
        {
            log.Info("# Start State CPAIListTxnState >> encryptRSAData #  ");
            AppUserDao appUserDao = new AppUserDaoImpl();
            SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
            APP_USER appUser = appUserDao.findByAppId(stateModel.EngineModel.appUser);
            if (appUser != null)
            {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PUBLIC_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                {
                    String pubKey = scn.SCN_VALUE;
                    String cipherText = data.Trim();
                    try
                    {
                        String encryptMsg = RSAHelper.EncryptText(cipherText, pubKey);
                        log.Info("# End State CPAIListTxnState >> encryptRSAData # :: Code >>> " + currentCode);
                        return encryptMsg;
                    }
                    catch (Exception ex)
                    {
                        currentCode = CPAIConstantRespCodeUtil.CANNOT_ENCRYPT_MESSAGE_RESP_CODE;
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        log.Error("# Error CPAIListTxnState >> encryptRSAData # :: Exception >>> " + currentCode + " : " + ex.Message);
                        throw new Exception();
                    }
                }
                else
                {
                    //can not get priKey
                    currentCode = ConstantRespCodeUtil.CAN_NOT_GET_KEY_ENGINE_RESP_CODE[0];
                    log.Error("# Error CPAIListTxnState >> encryptRSAData # :: Exception >>> " + currentCode);
                    throw new Exception();
                }
            }
            else
            {
                currentCode = ConstantRespCodeUtil.NOT_FOUND_APP_USER_ENGINE_RESP_CODE[0];
                log.Error("# Error CPAIListTxnState >> encryptRSAData # :: Exception >>> " + currentCode);
                throw new Exception();
            }
        }

    }
}
