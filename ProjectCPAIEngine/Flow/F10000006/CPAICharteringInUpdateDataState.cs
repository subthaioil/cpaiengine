﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000006
{
    public class CPAICharteringInUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICharteringInUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }

                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    ChiRootObject dataDetail = JSonConvertUtil.jsonToModel<ChiRootObject>(item);

                    //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) || (dataDetail.chi_proposed_for_approve.vessel_name != null
                    //    && dataDetail.chi_proposed_for_approve.owner != null
                    //    && dataDetail.chi_proposed_for_approve.owner_broker != null
                    //    && dataDetail.chi_evaluating.date_purchase != null
                    //    && dataDetail.chi_proposed_for_approve.laycan_from != null))
                    //{
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                        #region add data history
                        //add data history
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = "N";
                        dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Insert Update DB
                        //update reason only in data_detail 
                        string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                        dataDetail.chi_reason = note;

                        bool isRunDB = false;
                        //if ((nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)) || nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CERTIFIED))
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) || 
                        (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2) && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                        (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)))
                        {
                            //DRAFT && SUBMIT ==> DELETE , INSERT
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                        }
                        else if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                        {
                            //Verify can edit >> explanation, note
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.chi_chartering_method.reason, dataDetail.chi_note);
                        }
                        else
                        {
                            //OTHER ==> UPDATE
                            //isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note);
                             isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.chi_chartering_method.reason, dataDetail.chi_note);
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                            }
                            #endregion
                        }
                        #endregion

                        #region INSERT JSON
                        if (isRunDB)
                        {
                            //set data detail (json when action = approve_1 or DRAFT only)

                            if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") ||
                            (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_2") && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                            currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                            {
                                string vessel = dataDetail.chi_proposed_for_approve.vessel_name != null ? dataDetail.chi_proposed_for_approve.vessel_name : "";
                                string broker = dataDetail.chi_proposed_for_approve.charterer_broker != null ? dataDetail.chi_proposed_for_approve.charterer_broker : "";
                                string owner = dataDetail.chi_proposed_for_approve.owner != null ? dataDetail.chi_proposed_for_approve.owner : "";
                                string date_pn = dataDetail.chi_evaluating.date_purchase != null ? dataDetail.chi_evaluating.date_purchase : "";
                                string flatrate = dataDetail.chi_evaluating.flat_rate != null ? dataDetail.chi_evaluating.flat_rate : "";
                                string laycan_from = dataDetail.chi_proposed_for_approve.laycan_from != null ? dataDetail.chi_proposed_for_approve.laycan_from : "";
                                string laycan_to = dataDetail.chi_proposed_for_approve.laycan_to != null ? dataDetail.chi_proposed_for_approve.laycan_to : "";
                                string vessel_size = dataDetail.chi_evaluating.vessel_size;
                                string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                                 //
                                string freight_type  = dataDetail.chi_evaluating.freight != null ? dataDetail.chi_evaluating.freight : "";

                                etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                                //set data
                                //pForApproveDetail = JSonConvertUtil.jsonToModel<ChiProposedForApprove>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                                etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = vessel, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Broker, new ExtendValue { value = broker, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Owner, new ExtendValue { value = owner, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Vessel_Size, new ExtendValue { value = vessel_size, encryptFlag = "N" });

                                etxValue.SetValue(CPAIConstantUtil.Freight_type, new ExtendValue { value = freight_type, encryptFlag = "N" });

                                //evalDetail = JSonConvertUtil.jsonToModel<ChiEvaluating>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                                etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_pn, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.FlatRate, new ExtendValue { value = flatrate, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });

                                //set offersItem
                                foreach (ChiOffersItem of in dataDetail.chi_negotiation_summary.chi_offers_items)
                                {
                                    if (!string.IsNullOrEmpty(of.final_flag) && of.final_flag.Equals("Y"))
                                    {
                                        //etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = of.route_vessel, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.FinalPrice, new ExtendValue { value = of.final_deal, encryptFlag = "N" });
                                        //etxValue.SetValue(CPAIConstantUtil.OtherCostSur, new ExtendValue { value = of.others_cost_surveyor, encryptFlag = "N" });
                                        //etxValue.SetValue(CPAIConstantUtil.OtherCostBar, new ExtendValue { value = of.others_cost_barge, encryptFlag = "N" });
                                        break;
                                    }
                                }

                            }
                            else
                            {
                                ChiRootObject dataDetail_temp = JSonConvertUtil.jsonToModel<ChiRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                                dataDetail_temp.chi_reason = note;
                                string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail_temp);
                                etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            }
                            etxValue.SetValue(CPAIConstantUtil.Status,new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Action,new ExtendValue { value = currentAction, encryptFlag = "N" });
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                        }
                        #endregion
                    //}
                    //else
                    //{
                    //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                    //}
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICharteringInUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICharteringInUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICharteringInUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, ChiRootObject dataDetail)
        {
            log.Info("# Start State CPAICharteringInUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHI_DATA_DAL dataDAL = new CHI_DATA_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CHI_DATA
                            CHI_DATA dataCHI = new CHI_DATA();
                            dataCHI.IDA_ROW_ID = TransID;
                            dataCHI.IDA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            if (!string.IsNullOrEmpty(dataDetail.chi_evaluating.date_purchase))
                                dataCHI.IDA_DATE_PURCHASE = DateTime.ParseExact(dataDetail.chi_evaluating.date_purchase, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dataCHI.IDA_LOADING_MONTH = dataDetail.chi_evaluating.loading_month;
                            dataCHI.IDA_LOADING_YEAR = dataDetail.chi_evaluating.loading_year;
                            //dataCHI.IDA_LAYCAN = "19/10/2016";// dataDetail.chi_evaluating.laycan; //In DB 20
                            ShareFn fn = new ShareFn();
                            if (!string.IsNullOrEmpty(dataDetail.chi_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(dataDetail.chi_proposed_for_approve.laycan_to))
                            {                           
                                dataCHI.IDA_P_LAYCAN_FROM = DateTime.ParseExact(dataDetail.chi_proposed_for_approve.laycan_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataCHI.IDA_P_LAYCAN_TO = DateTime.ParseExact(dataDetail.chi_proposed_for_approve.laycan_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            //dataCHI.IDA_LAYCAN_FROM = Convert.ToDateTime(dataDetail.chi_evaluating.laycan_from).ToString("dd/MM/yyyy");
                            dataCHI.IDA_FREIGHT = dataDetail.chi_evaluating.freight;
                            dataCHI.IDA_VESSEL_SIZE = dataDetail.chi_evaluating.vessel_size;
                            dataCHI.IDA_YEAR = dataDetail.chi_evaluating.year;
                            //dataCHI.IDA_FLAT_RATE = dataDetail.chi_evaluating.flat_rate;

                            dataCHI.IDA_TD_MARKET = dataDetail.chi_evaluating.market_ref;

                            dataCHI.IDA_ROUTE = dataDetail.chi_cargo_detail.route;
                            dataCHI.IDA_EXPLANATION = dataDetail.chi_chartering_method.reason;
                            foreach (var item in dataDetail.chi_chartering_method.chi_type_of_fixings)
                            {
                                if (item.type_flag.Equals("Y"))
                                {
                                    dataCHI.IDA_TYPE_OF_FIXINGS = item.type_value;
                                }
                            }
                            //dataCHI.IDA_TYPE_OF_FIXINGS = "Y";//dataDetail.chi_chartering_method.chi_type_of_fixings;
                            dataCHI.IDA_P_OWNER = dataDetail.chi_proposed_for_approve.owner;
                            dataCHI.IDA_P_CHARTER_PATRY = dataDetail.chi_proposed_for_approve.charter_patry;
                            dataCHI.IDA_P_OWNER_BROKER = dataDetail.chi_proposed_for_approve.owner_broker;
                            dataCHI.IDA_P_FK_CHARTERER_BROKER = dataDetail.chi_proposed_for_approve.charterer_broker;
                            dataCHI.IDA_P_FK_VESSEL_NAME = dataDetail.chi_proposed_for_approve.vessel_name;
                            //dataCHI.IDA_P_LAYCAN = dataDetail.chi_proposed_for_approve.laycan; //pls edit
                            dataCHI.IDA_P_LOADING = dataDetail.chi_proposed_for_approve.loading;
                            dataCHI.IDA_P_DISCHARGING = dataDetail.chi_proposed_for_approve.discharging;
                            dataCHI.IDA_P_LAYTIME = dataDetail.chi_proposed_for_approve.laytime;
                            dataCHI.IDA_P_CRUDE_OIL_WASHING = dataDetail.chi_proposed_for_approve.crude_oil_washing;
                            //add  special_requirment
                            dataCHI.IDA_P_SPECIAL_REQUIRMENT = dataDetail.chi_proposed_for_approve.special_requirment;

                            dataCHI.IDA_P_VESSEL_BUILT = dataDetail.chi_proposed_for_approve.vessel_built;
                            dataCHI.IDA_P_CAPACITY = dataDetail.chi_proposed_for_approve.capacity;
                            dataCHI.IDA_P_LADEN_SPEED = dataDetail.chi_proposed_for_approve.laden_speed;

                            dataCHI.IDA_ADDRESS_COM = dataDetail.chi_proposed_for_approve.address_com;
                            dataCHI.IDA_ADDRESS_COM_PERCENT = dataDetail.chi_proposed_for_approve.address_com_percent;
                            dataCHI.IDA_NET_FREIGHT = dataDetail.chi_proposed_for_approve.net_freight;

                            dataCHI.IDA_P_EXTEN_COST = dataDetail.chi_proposed_for_approve.exten_cost;
                            /*dataCHI.IDA_P_ARM_GUARD = dataDetail.chi_proposed_for_approve.arm_guard;
                            dataCHI.IDA_P_WAR_RISK = dataDetail.chi_proposed_for_approve.war_risk;
                            dataCHI.IDA_P_DEVIATION = dataDetail.chi_proposed_for_approve.deviation;*/

                            dataCHI.IDA_P_FLAT_RATE = dataDetail.chi_proposed_for_approve.flat_rate;
                            //add freight_rate
                            dataCHI.IDA_P_FREIGHT_RATE = dataDetail.chi_proposed_for_approve.freight_rate;

                            dataCHI.IDA_P_EST_TOTAL = dataDetail.chi_proposed_for_approve.est_total;
                            dataCHI.IDA_P_EST_FREIGHT = dataDetail.chi_proposed_for_approve.est_freight;
                            dataCHI.IDA_REASON = dataDetail.chi_reason;
                            dataCHI.IDA_REQUESTED_DATE = dtNow;
                            dataCHI.IDA_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);         
                            dataCHI.IDA_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                            //dataCHI.IDA_REMARK = etxValue.GetValue(CPAIConstantUtil.Note); 
                            dataCHI.IDA_REMARK = dataDetail.chi_note;
                            dataCHI.IDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);                  
                            dataCHI.IDA_CREATED = dtNow;
                            dataCHI.IDA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHI.IDA_UPDATED = dtNow;
                            dataCHI.IDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHI.IDA_TRIPNO = dataDetail.tripNumber;
                            dataDAL.Save(dataCHI, context);
                            #endregion

                            #region CHI_LOAD_PORT
                            CHI_LOAD_PORT_DAL dataLoadPortDAL = new CHI_LOAD_PORT_DAL();
                            CHI_LOAD_PORT dataCHILP;
                            foreach (var itemLoadPort in dataDetail.chi_cargo_detail.chi_load_ports)
                            {
                                dataCHILP = new CHI_LOAD_PORT();
                                dataCHILP.IPT_ROW_ID = Guid.NewGuid().ToString("N");
                                dataCHILP.IPT_FK_CHI_DATA = TransID;
                                dataCHILP.IPT_ORDER_PORT = itemLoadPort.order_port;
                                if (itemLoadPort.port != "")
                                {
                                    dataCHILP.IPT_FK_PORT = Convert.ToDecimal(itemLoadPort.port);
                                }
                                dataCHILP.IPT_FK_MATERIALS = itemLoadPort.crude;
                                dataCHILP.IPT_FK_VENDOR = itemLoadPort.supplier;
                                dataCHILP.IPT_KBBLS = itemLoadPort.kbbls;
                                dataCHILP.IPT_KTONS = itemLoadPort.ktons;
                                //add mtons
                                dataCHILP.IPT_MTONS = itemLoadPort.mtons;

                                dataCHILP.IPT_TOLERANCE = itemLoadPort.tolerance;
                                dataCHILP.IPT_DATE = itemLoadPort.date;
                                dataCHILP.IPT_CREATED = dtNow;
                                dataCHILP.IPT_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCHILP.IPT_UPDATED = dtNow;
                                dataCHILP.IPT_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataLoadPortDAL.Save(dataCHILP, context);
                            }
                            #endregion

                            #region CHI_DISCHARGE_PORT
                            CHI_DISCHARGE_PORT_DAL dalDischargePort = new CHI_DISCHARGE_PORT_DAL();
                            CHI_DISCHARGE_PORT dataDisPort;
                            foreach (var itemPort in dataDetail.chi_cargo_detail.discharge_ports)
                            {
                                dataDisPort = new CHI_DISCHARGE_PORT();
                                dataDisPort.IDP_ROW_ID = Guid.NewGuid().ToString("N");
                                dataDisPort.IDP_FK_CHI_DATA = TransID;
                                dataDisPort.IDP_ORDER_PORT = itemPort.dis_order_port;
                                dataDisPort.IDP_PORT = itemPort.dis_port; //Port name In DB 5Byte
                                dataDisPort.IDP_CREATED = dtNow;
                                dataDisPort.IDP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataDisPort.IDP_UPDATED = dtNow;
                                dataDisPort.IDP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dalDischargePort.Save(dataDisPort, context);
                            }
                            #endregion

                            #region CHI_OFFER_ITEMS
                            CHI_OFFER_ITEMS_DAL dalOfferItems = new CHI_OFFER_ITEMS_DAL();
                            CHI_OFFER_ITEMS dataOfferItems;
                            string rowIDOffer = string.Empty;
                            string rowIDRoundItems = string.Empty;
                            foreach (var itemPort in dataDetail.chi_negotiation_summary.chi_offers_items)
                            {
                                rowIDOffer = Guid.NewGuid().ToString("N");
                                dataOfferItems = new CHI_OFFER_ITEMS();
                                dataOfferItems.IOI_ROW_ID = rowIDOffer;
                                dataOfferItems.IOI_FK_CHI_DATA = TransID;
                                dataOfferItems.IOI_FK_VENDOR = itemPort.broker;
                                dataOfferItems.IOI_FK_VEHICLE = itemPort.vessel;
                                dataOfferItems.IOI_YEAR = itemPort.year;
                                dataOfferItems.IOI_KDWT = itemPort.KDWT;
                                //add type
                                dataOfferItems.IOI_TYPE = itemPort.type;

                                dataOfferItems.IOI_OWNER = itemPort.owner;
                                dataOfferItems.IOI_FINAL_FLAG = itemPort.final_flag;
                                dataOfferItems.IOI_FINAL_DEAL = itemPort.final_deal;
                                dataOfferItems.IOI_CREATED = dtNow;
                                dataOfferItems.IOI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataOfferItems.IOI_UPDATED = dtNow;
                                dataOfferItems.IOI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dalOfferItems.Save(dataOfferItems, context);

                                #region CHI_OFFER_ITEMS
                                CHI_ROUND_ITEMS_DAL dalRoundItems = new CHI_ROUND_ITEMS_DAL();
                                CHI_ROUND_ITEMS dataRoundItems;
                                foreach (var itemRoundItems in itemPort.chi_round_items)
                                {
                                    rowIDRoundItems = Guid.NewGuid().ToString("N");
                                    dataRoundItems = new CHI_ROUND_ITEMS();
                                    dataRoundItems.IRI_ROW_ID = rowIDRoundItems;
                                    dataRoundItems.IRI_FK_OFFER_ITEMS = rowIDOffer;
                                    dataRoundItems.IRI_ROUND_NO = itemRoundItems.round_no;
                                    dataRoundItems.IRI_CREATED = dtNow;
                                    dataRoundItems.IRI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataRoundItems.IRI_UPDATED = dtNow;
                                    dataRoundItems.IRI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                    dalRoundItems.Save(dataRoundItems, context);

                                    #region CHI_OFFER_ITEMS
                                    CHI_ROUND_DAL dalRound = new CHI_ROUND_DAL();
                                    CHI_ROUND dataRound;
                                    foreach (var itemRound in itemRoundItems.chi_round_info)
                                    {
                                        dataRound = new CHI_ROUND();
                                        dataRound.IIR_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataRound.IIR_FK_ROUND_ITEMS = rowIDRoundItems;
                                        dataRound.IRR_ORDER = itemRound.order;
                                        dataRound.IIR_ROUND_TYPE = itemRound.type;
                                        dataRound.IIR_ROUND_OFFER = itemRound.offer;
                                        dataRound.IIR_ROUND_COUNTER = itemRound.counter;
                                        dataRound.IIR_CREATED = dtNow;
                                        dataRound.IIR_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataRound.IIR_UPDATED = dtNow;
                                        dataRound.IIR_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                        dalRound.Save(dataRound, context);
                                    }
                                    #endregion
                                }
                                #endregion
                            }


                            #endregion

                            #region CHI_ATTACH_FILE
                            CHI_ATTACH_FILE_DAL dataAttachFileDAL = new CHI_ATTACH_FILE_DAL();
                            CHI_ATTACH_FILE dataCHIAF;
                            foreach (var item in dataDetail.explanationAttach.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataCHIAF = new CHI_ATTACH_FILE();
                                    dataCHIAF.IAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHIAF.IAF_FK_CHI_DATA = TransID;
                                    dataCHIAF.IAF_PATH = text[0];
                                    dataCHIAF.IAF_INFO = text[1];
                                    dataCHIAF.IAF_TYPE = "EXT";
                                    dataCHIAF.IAF_CREATED = dtNow;
                                    dataCHIAF.IAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHIAF.IAF_UPDATED = dtNow;
                                    dataCHIAF.IAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataCHIAF, context);
                                }
                            }

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                            foreach (var item in Att.attach_items)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    dataCHIAF = new CHI_ATTACH_FILE();
                                    dataCHIAF.IAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHIAF.IAF_FK_CHI_DATA = TransID;
                                    dataCHIAF.IAF_PATH = item;
                                    dataCHIAF.IAF_INFO = "";
                                    dataCHIAF.IAF_TYPE = "ATT";
                                    dataCHIAF.IAF_CREATED = dtNow;
                                    dataCHIAF.IAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHIAF.IAF_UPDATED = dtNow;
                                    dataCHIAF.IAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataCHIAF, context);
                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error InsertDB >> Rollback # :: Exception >>> ", e);
                log.Error("InsertDB::Rollback >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAICharteringInUpdateDataState >> InsertDB # ");
            return isSuccess;
        }


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue,string note)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringInUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHI_DATA_DAL dataDAL = new CHI_DATA_DAL();
                            CHI_DATA dataCHI = new CHI_DATA();
                            dataCHI.IDA_ROW_ID = TransID;
                            dataCHI.IDA_REASON = note;
                            dataCHI.IDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHI.IDA_UPDATED = DateTime.Now;
                            dataCHI.IDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHI, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateDB >> UpdateDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICharteringInUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICharteringInUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateDB >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICharteringInUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note, string explanation = null, string note_2 = null)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIUpdateCDPHDataState >> UpdateDB when verify #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHI_DATA_DAL dataDAL = new CHI_DATA_DAL();
                            CHI_DATA data = new CHI_DATA();
                            data.IDA_ROW_ID = TransID;
                            data.IDA_REASON = note;
                            //if (explanation != null)
                            //{
                            //    data.IDA_EXPLANATION = explanation;
                            //}
                            //if (note_2 != null)
                            //{
                            //    data.IDA_REASON = note_2;
                            //}
                            data.IDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.IDA_UPDATED = DateTime.Now;
                            data.IDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(data, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                            log.Error("# Error CPAIUpdateCDPHDataState # :: Rollback >>> ", ex);
                            log.Error("CPAIUpdateCDPHDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }

                    }
                }

                log.Info("# End State CPAIUpdateCDPHDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error CPAIUpdateCDPHDataState # :: UpdateDB >>> ", ex);
                log.Error("CPAIUpdateCDPHDataState::UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}



     

