﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using DSMail.service;
using DSMail.model;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.model;
using com.pttict.engine.downstream;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000006
{
    public class CPAISendMailTCEWSState : BasicBean, StateFlowAction
    {
        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private string system = CPAIConstantUtil.SYSTEM_TCE_WS;
        private string type = CPAIConstantUtil.CRUDE_TYPE;
        private string function_id = CPAIConstantUtil.F100000012;
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAISendMailTCEWSState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string tce_action = etxValue.GetValue(CPAIConstantUtil.TCE_Action);

                string action = tce_action;
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                string charterFor = etxValue.GetValue(CPAIConstantUtil.CharterFor);
                ActionMessageDAL amsMan = new ActionMessageDAL();

                List<CPAI_ACTION_MESSAGE> results = amsMan.findUserGroupMail(action, system, type, function_id, prevStatus, charterFor);

                if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else
                {
                    for (int i = 0; i < results.Count; i++)
                    {
                        findUserAndSendMail(stateModel, results[i], system);
                    }
                    currentCode = stateModel.BusinessModel.currentCode;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAISendMailTCEWSState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAISendMailTCEWSState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }


        public void findUserAndSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system)
        {

            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER))
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                List<USERS> userResult = userGMan.findUserMailByGroupSystem(userGroup, system);
                if (userResult.Count == 0)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                List<USERS> userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }

        }
        public void prepareSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail)
        {

            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;
            string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            //get user
            List<USERS> userResult = userMan.findByLogin(tem);

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystem(tem, etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0)
            {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("login user not found");
            }

            string charter_in_no    = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
            string charter_in_txn   = stateModel.EngineModel.ftxTransId;
            string status           = actionMessage.AMS_ACTION.Equals(CPAIConstantUtil.ACTION_SUBMIT) ? CPAIConstantUtil.ACTION_SUBMIT : CPAIConstantUtil.ACTION_CANCEL;
            string reason           = etxValue.GetValue(CPAIConstantUtil.TCE_Reason);
            CPAI_TCE_WS tce_ws      = CPAI_TCE_WS_DAL.getTceTxn(charter_in_txn,status);
            string pn = "";
            string id = "";
            string tce_ws_txn = "";
            if (tce_ws != null)
            {
                 pn = tce_ws.CTW_VOY_NO;
                 id = tce_ws.CTW_FK_VEHICLE;
                 tce_ws_txn = tce_ws.CTW_ROW_ID;
            }

            //# pn of tce ws
            //# reason
            //# charterIn
            //# vessel
            if (subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", pn);
            }
            if (body.Contains("#pn"))
            {
                body = body.Replace("#pn", pn);
            }
            //
            if (subject.Contains("#user"))
            {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user"))
            {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user"))
            {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user"))
            {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            //charterIn
            if (body.Contains("#charterIn"))
            {
                body = body.Replace("#charterIn", charter_in_no);
            }
            if (body.Contains("#vessel"))
            {
                if (!String.IsNullOrEmpty(id))
                {
                    MT_VEHICLE vessel = VehicleDAL.GetVehicleById(id);
                    body = body.Replace("#vessel", vessel.VEH_VEH_TEXT);
                }
                else
                {
                    body = body.Replace("#vessel", id);
                }
            }
            //
            if (body.Contains("#reason"))
            {
                body = body.Replace("#reason", reason);
            }
            //
            if (body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = body;
                foreach (USERS um in userMail)
                {
                    temp_body = body;
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = type;
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = tce_ws_txn;
                    at.TOK_USER_SYSTEM = system;
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    string out_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, subject, out_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body)
        {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentCode = downResp.getResultCode();
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
        }
    }
}