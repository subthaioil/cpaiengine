﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using System.Linq;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Flow.F10000006
{
    public class CPAITCEWSCancelState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAITCEWSCancelState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                //set tce_action of send email
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string tce_action = CPAIConstantUtil.ACTION_FAIL_2;
                string tce_reason = "System Error";
                ExtendValue extTemp = new ExtendValue { value = tce_action };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Action, extTemp);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Action] = extTemp;
                }
                ExtendValue extReason = new ExtendValue { value = tce_reason };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Reason, extReason);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Reason] = extReason;
                }

                //string p_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                string chi_txn_id = stateModel.EngineModel.ftxTransId;
                string create_by = etxValue.GetValue(CPAIConstantUtil.CreateBy);
                string note = etxValue.GetValue(CPAIConstantUtil.Note);
                //get tce ws
                CPAI_TCE_WS_DAL cpaiTceWs = new CPAI_TCE_WS_DAL();
                CPAI_TCE_WS SSCHIData = cpaiTceWs.GetTCEByCharterIn(chi_txn_id);
                string txn_id = SSCHIData.CTW_ROW_ID;
                //get request txn
                FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
                FUNCTION_TRANSACTION ft = ftxMan.GetFnTranDetail(txn_id);
                string req_txn_id = ft.FTX_REQ_TRANS;
                //call function F10000012 : CANCEL
                RequestCPAI req = new RequestCPAI();
                req.Function_id = ConstantPrm.FUNCTION.F10000012;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = req_txn_id;
                req.State_name = "CPAITceVerifyRequireInputContinueState";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_CANCEL });
                req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.ACTION_CANCEL });
                req.Req_parameters.P.Add(new P { K = "user", V = create_by });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_WS });
                req.Req_parameters.P.Add(new P { K = "note", V = note });
                //req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                //req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                req.Extra_xml = "";

                ResponseData resData = new ResponseData();
                RequestData reqData = new RequestData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = service.CallService(reqData);
                log.Debug(" resp function F10000012 >> " + resData.result_code);
                if (resData != null && resData.result_code != null && resData.result_code == "1")
                {
                    //respons from function F10000012
                    tce_reason = note;
                    tce_action = CPAIConstantUtil.ACTION_CANCEL;
                    currentCode = resData.result_code;
                }
                else
                {
                    tce_reason = resData.result_desc;
                    currentCode = resData.result_code;
                }

                extTemp = new ExtendValue { value = tce_action };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Action, extTemp);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Action] = extTemp;
                }
                extReason = new ExtendValue { value = tce_reason };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Reason, extReason);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Reason] = extReason;
                }
                

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAITCEWSCancelState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAITCEWSCancelState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}