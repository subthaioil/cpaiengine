﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using System.Linq;
namespace ProjectCPAIEngine.Flow.F10000006
{
    public class CPAITCEWSCreateState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAITCEWSCreateState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                
                //set tce_action of send email
                Dictionary <string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string tce_action = CPAIConstantUtil.ACTION_FAIL_1;
                string tce_reason = "System Error";
                ExtendValue extTemp = new ExtendValue { value = tce_action };
                if (etxValue.GetValue(CPAIConstantUtil.Vessel_Size) == "VLCC")
                {                    
                    try
                    {
                        etxValue.Add(CPAIConstantUtil.TCE_Action, extTemp);
                    }
                    catch (ArgumentException)
                    {
                        etxValue[CPAIConstantUtil.TCE_Action] = extTemp;
                    }
                    ExtendValue extReason = new ExtendValue { value = tce_reason };
                    try
                    {
                        etxValue.Add(CPAIConstantUtil.TCE_Reason, extReason);
                    }
                    catch (ArgumentException)
                    {
                        etxValue[CPAIConstantUtil.TCE_Reason] = extReason;
                    }

                    string p_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                    string create_by = etxValue.GetValue(CPAIConstantUtil.CreateBy);

                    //
                    CHI_DATA_DAL chiDataDAL = new CHI_DATA_DAL();
                    //CHI_DATA SSCHIData = chiDataDAL.GetCHIDATAByPurchaseNo(p_no); //not edit 
                    CHI_DATA SSCHIData = chiDataDAL.GetCHIDataByPn(p_no);

                    string tem = SSCHIData.IDA_ROW_ID;

                    //prepare for function F10000012
                    TceRootObject tce = new TceRootObject();
                    TceDetail tce_detail = new TceDetail();
                    tce_detail.voy_no = "";
                    tce_detail.chi_data_id = SSCHIData.IDA_ROW_ID;
                    tce_detail.flat_rate = SSCHIData.IDA_P_FLAT_RATE;
                    tce_detail.extra_cost = SSCHIData.IDA_P_EXTEN_COST;
                    tce_detail.laycan_load_from = ((SSCHIData.IDA_P_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(SSCHIData.IDA_P_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    tce_detail.laycan_load_to = ((SSCHIData.IDA_P_LAYCAN_TO == null) ? "" : Convert.ToDateTime(SSCHIData.IDA_P_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    tce_detail.date_fixture = (SSCHIData.IDA_DATE_PURCHASE == null) ? "" : Convert.ToDateTime(SSCHIData.IDA_DATE_PURCHASE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    tce_detail.td = String.IsNullOrEmpty(SSCHIData.IDA_TD_MARKET) ? "TD2" : SSCHIData.IDA_TD_MARKET;
                    tce_detail.reason = SSCHIData.IDA_REASON;

                    var OfferFinal = SSCHIData.CHI_OFFER_ITEMS.Count > 0 ? SSCHIData.CHI_OFFER_ITEMS.Where(x => x.IOI_FINAL_FLAG == "Y").ToList() : null;
                    if (OfferFinal != null && OfferFinal.Count > 0)
                    {
                        tce_detail.ship_name = OfferFinal[0].IOI_FK_VEHICLE;
                        tce_detail.ship_broker = OfferFinal[0].IOI_FK_VENDOR;
                        tce_detail.ship_owner = OfferFinal[0].IOI_OWNER;
                        var RoundItem = OfferFinal[0].CHI_ROUND_ITEMS.OrderByDescending(x => x.IRI_ROUND_NO).ToList();
                        //var RoundItem = (from chi_roun in context.CHI_ROUND where chi_roun.IIR_FK_ROUND_ITEMS == IRI_ROW_ID select chi_roun).ToList();

                        tce_detail.dem = RoundItem[0].CHI_ROUND.Where(x => x.IIR_ROUND_TYPE.ToUpper() == "DEM").ToList().Select(x => x.IIR_ROUND_OFFER).FirstOrDefault();
                        tce_detail.min_load = RoundItem[0].CHI_ROUND.Where(x => x.IIR_ROUND_TYPE.ToUpper() == "MIN LOADED").ToList().Select(x => x.IIR_ROUND_OFFER).FirstOrDefault();
                        tce_detail.top_fixture_value = RoundItem[0].CHI_ROUND.Where(x => x.IIR_ROUND_TYPE.ToUpper() == "WS").ToList().Select(x => x.IIR_ROUND_OFFER).FirstOrDefault();
                    }
                    tce.tce_detail = tce_detail;
                    var json = new JavaScriptSerializer().Serialize(tce);
                    log.Debug(" json function F10000012 >> " + json);

                    //call function F10000012 : SUMMIT
                    RequestCPAI req = new RequestCPAI();
                    req.Function_id = ConstantPrm.FUNCTION.F10000012;
                    req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                    req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                    req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                    req.State_name = "";
                    req.Req_parameters = new Req_parameters();
                    req.Req_parameters.P = new List<P>();
                    req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                    req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_SUBMIT });
                    req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.ACTION_SUBMIT });
                    req.Req_parameters.P.Add(new P { K = "user", V = create_by });
                    req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_WS });
                    req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                    req.Req_parameters.P.Add(new P { K = "note", V = "Auto Create" });
                    req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                    req.Extra_xml = "";

                    ResponseData resData = new ResponseData();
                    RequestData reqData = new RequestData();
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                    var xml = ShareFunction.XMLSerialize(req);
                    reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                    resData = service.CallService(reqData);
                    log.Debug(" resp function F10000012 >> " + resData.result_code);
                    if (resData != null && resData.result_code != null && resData.result_code == "1")
                    {
                        //respons from function F10000012
                        tce_reason = "-";
                        tce_action = CPAIConstantUtil.ACTION_SUBMIT;
                        currentCode = resData.result_code;
                    }
                    else
                    {
                        tce_reason = resData.result_desc;
                        //CPAIConstantUtil.Message
                        if (resData.resp_parameters != null && resData.resp_parameters[0] != null)
                        {
                            foreach (p data in resData.resp_parameters)
                            {
                                if (data.k.Equals(CPAIConstantUtil.Message))
                                {
                                    tce_reason = data.v;
                                    break;
                                }
                            }
                        }
                        currentCode = resData.result_code;
                        tce_action = CPAIConstantUtil.ACTION_FAIL_1;
                    }

                    extTemp = new ExtendValue { value = tce_action };
                    try
                    {
                        etxValue.Add(CPAIConstantUtil.TCE_Action, extTemp);
                    }
                    catch (ArgumentException)
                    {
                        etxValue[CPAIConstantUtil.TCE_Action] = extTemp;
                    }
                    extReason = new ExtendValue { value = tce_reason };
                    try
                    {
                        etxValue.Add(CPAIConstantUtil.TCE_Reason, extReason);
                    }
                    catch (ArgumentException)
                    {
                        etxValue[CPAIConstantUtil.TCE_Reason] = extReason;
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_INPUT_DATE_RESP_CODE;// Vessel_Size != VLCC
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                if (!string.IsNullOrEmpty(tce_reason))
                    stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + tce_reason + "]";
                log.Info("# End State CPAITCEWSCreateState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAITCEWSCreateState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}