﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000007
{
    public class CPAICharteringOutUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICharteringOutUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                var item_o = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    ChotObjectRoot dataDetail = JSonConvertUtil.jsonToModel<ChotObjectRoot>(item);

                    //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) || (
                    //    dataDetail.chot_evaluating.vessel_id != null
                    //    && dataDetail.chot_evaluating.chr_party_date != null
                    //    && dataDetail.chot_proposed_for_approve.charterer != null
                    //    && dataDetail.chot_proposed_for_approve.laycan_from != null
                    //    && dataDetail.chot_proposed_for_approve.laycan_to != null
                    //    ))
                    //{

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    dataDetail.chot_reason = note;

                    bool isRunDB = false;
                    //if ((nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)) || nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CERTIFIED))
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) || (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                    {
                        //Verify can edit >> explanation, note
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.chot_reason_explanation, dataDetail.chot_note);
                    }
                    else
                    {
                        //OTHER ==> UPDATE
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue, note, dataDetail.chot_reason_explanation, dataDetail.chot_note);
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        {
                            dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                        }
                        #endregion
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = approve_1 or DRAFT only)
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") || currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                        {
                            //string flatrate = dataDetail.chot_evaluating.flat_rate != null ? dataDetail.chot_evaluating.flat_rate : "";
                            //string min_load = dataDetail.chot_negotiation_summary.chot_offers_items != null ? dataDetail.chot_negotiation_summary.chot_offers_items.Where(p => p.final_flag.Equals("Y")).FirstOrDefault().chot_round_items.LastOrDefault().chot_round_info.Where(p => p.type.Equals("Min loaded")).FirstOrDefault().offer : "";
                            string vessel = dataDetail.vessel_id != null ? dataDetail.vessel_id : "";
                            string date_pn = dataDetail.chot_date != null ? dataDetail.chot_date : "";
                            string laycan_from = "";
                            string laycan_to = "";
                            string charterer = "";
                            string broker = "";
                            string owner = "";
                            string no_total_ex = "";
                            string total_ex = "";
                            string net_benefit = "";
                            string result_flag = "";
                            string result_a = "";
                            string result_b = "";
                            string freight = "0";
                            string unit = "USD";
                            if (dataDetail.chot_summary != null)
                            {
                                laycan_from = dataDetail.chot_summary.laycan_from != null ? dataDetail.chot_summary.laycan_from : "";
                                laycan_to = dataDetail.chot_summary.laycan_to != null ? dataDetail.chot_summary.laycan_to : "";
                                charterer = dataDetail.chot_summary.charterer != null ? dataDetail.chot_summary.charterer : "";
                                broker = dataDetail.chot_summary.charterer_broker != null ? dataDetail.chot_summary.charterer_broker : "";
                                owner = dataDetail.chot_summary.owner_broker != null ? dataDetail.chot_summary.owner_broker : "";
                            }
                            if (dataDetail.chot_charter_out_case_b != null && dataDetail.chot_charter_out_case_b.chot_est_income_b_one != null)
                            {
                                unit = dataDetail.chot_charter_out_case_b.chot_est_income_b_one.offer != null ? dataDetail.chot_charter_out_case_b.chot_est_income_b_one.offer : "USD";
                                if (unit.Contains("/"))
                                {
                                    unit = unit.Substring(0, 3);
                                }
                                freight = dataDetail.chot_charter_out_case_b.chot_est_income_b_one.total_freight != null ? dataDetail.chot_charter_out_case_b.chot_est_income_b_one.total_freight : "0";
                            }
                            if (dataDetail.chot_result != null)
                            {
                                /*
                                no_total_ex = dataDetail.chot_result.a_total_expense != null ? dataDetail.chot_result.a_total_expense : "";
                                total_ex = dataDetail.chot_result.b_total_income != null ? dataDetail.chot_result.b_total_income : "";
                                net_benefit = dataDetail.chot_result.c_total_net_income != null ? dataDetail.chot_result.c_total_net_income : "";
                                if (dataDetail.chot_result.chot_d_result != null)
                                {
                                    result_flag = dataDetail.chot_result.chot_d_result.flag != null ? dataDetail.chot_result.chot_d_result.flag : "";
                                    if (result_flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_SAVING))
                                    {
                                        if (dataDetail.chot_result.chot_d_result.chot_saving != null)
                                        {
                                            result_a = dataDetail.chot_result.chot_d_result.chot_saving.saving_a_b != null ? dataDetail.chot_result.chot_d_result.chot_saving.saving_a_b : "";
                                            result_b = dataDetail.chot_result.chot_d_result.chot_saving.saving_a_c != null ? dataDetail.chot_result.chot_d_result.chot_saving.saving_a_c : "";
                                        }
                                    }
                                    if (result_flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_PROFIT))
                                    {
                                        if (dataDetail.chot_result.chot_d_result.profit != null)
                                        {
                                            result_a = dataDetail.chot_result.chot_d_result.profit.profit_b_a != null ? dataDetail.chot_result.chot_d_result.profit.profit_b_a : "";
                                            result_b = dataDetail.chot_result.chot_d_result.profit.profit_c_a != null ? dataDetail.chot_result.chot_d_result.profit.profit_c_a : "";
                                        }
                                    }
                                }
                                */
                                result_flag = dataDetail.chot_result.flag != null ? dataDetail.chot_result.flag : "";
                                if (result_flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_SAVING))
                                {
                                    if (dataDetail.chot_result.chot_saving != null)
                                    {
                                        no_total_ex = dataDetail.chot_result.chot_saving.saving_total_expense != null ? dataDetail.chot_result.chot_saving.saving_total_expense : "";
                                        total_ex = dataDetail.chot_result.chot_saving.saving_exp_inc != null ? dataDetail.chot_result.chot_saving.saving_exp_inc : "";
                                        net_benefit = dataDetail.chot_result.chot_saving.saving_total != null ? dataDetail.chot_result.chot_saving.saving_total : "";
                                    }
                                }
                                else if (result_flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_PROFIT))
                                {
                                    if (dataDetail.chot_result.chot_profit != null)
                                    {
                                        no_total_ex = dataDetail.chot_result.chot_profit.profit_total_expense != null ? dataDetail.chot_result.chot_profit.profit_total_expense : "";
                                        total_ex = dataDetail.chot_result.chot_profit.profit_inc_var != null ? dataDetail.chot_result.chot_profit.profit_inc_var : "";
                                        net_benefit = dataDetail.chot_result.chot_profit.profit_total != null ? dataDetail.chot_result.chot_profit.profit_total : "";
                                    }
                                }
                            }
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            //set data
                            etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = vessel, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_pn, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.FlatRate, new ExtendValue { value = flatrate, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.Min_Load, new ExtendValue { value = min_load, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Charterer, new ExtendValue { value = charterer, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Broker, new ExtendValue { value = broker, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Owner, new ExtendValue { value = owner, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.no_total_ex, new ExtendValue { value = no_total_ex, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.total_ex, new ExtendValue { value = total_ex, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.net_benefit, new ExtendValue { value = net_benefit, encryptFlag = "N" });
                            //
                            etxValue.SetValue(CPAIConstantUtil.result_flag, new ExtendValue { value = result_flag, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.result_a, new ExtendValue { value = result_a, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.result_b, new ExtendValue { value = result_b, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.result, new ExtendValue { value = result_a + "|" + result_b, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.freight, new ExtendValue { value = freight, encryptFlag = "N" });
                            //unit
                            etxValue.SetValue(CPAIConstantUtil.Unit, new ExtendValue { value = unit, encryptFlag = "N" });

                            //load port ,dischart port
                            List<ChotLoadingPort> chotLoadingPortList = dataDetail.chot_summary.chot_loading_port;
                            string load_port = "";
                            if (chotLoadingPortList != null && chotLoadingPortList.Count > 0)
                            {
                                foreach (ChotLoadingPort clp in chotLoadingPortList)
                                {
                                    load_port += clp.port_full_name + "|";
                                }
                            }
                            List<ChotDischargingPort> chotDischargingPort = dataDetail.chot_summary.chot_discharging_port;
                            string dis_port = "";
                            if (chotDischargingPort != null && chotDischargingPort.Count > 0)
                            {
                                foreach (ChotDischargingPort clp in chotDischargingPort)
                                {
                                    dis_port += clp.port_full_name + "|";
                                }
                            }
                            etxValue.SetValue(CPAIConstantUtil.Discharge_port, new ExtendValue { value = dis_port, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Load_port, new ExtendValue { value = load_port, encryptFlag = "N" });
                        }
                        else
                        {
                            var item_1 = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                            ChotObjectRoot dataDetail_temp = JSonConvertUtil.jsonToModel<ChotObjectRoot>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                            dataDetail_temp.chot_reason = note;
                            string dataDetail_old = JSonConvertUtil.modelToJson(dataDetail_temp);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_old, encryptFlag = "N" });
                        }
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                    //}
                    //else
                    //{
                    //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                    //}
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICharteringOutUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAICharteringOutUpdateDataState # :: Exception >>> " + tem);
                log.Error("# Error CPAICharteringOutUpdateDataState # :: Exception >>> ", ex);
                log.Error("CPAICharteringOutUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, ChotObjectRoot dataDetail)
        {
            log.Info("# Start State CPAICharteringOutUpdateDataState #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOT_DATA_DAL dataCHOTDAL = new CHOT_DATA_DAL();
                            dataCHOTDAL.Delete(TransID, context);

                            #region chot_evaluating

                            CHOT_DATA dataCHOT = new CHOT_DATA();

                            //dataCHOT.OTDA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            //if (!string.IsNullOrEmpty(dataDetail.chot_evaluating.chr_party_date))
                            //    dataCHOT.OTDA_DATE_PURCHASE = DateTime.ParseExact(dataDetail.chot_evaluating.chr_party_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataCHOT.OTDA_ROW_ID = TransID;
                            dataCHOT.OTDA_DATE = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_date);//
                            dataCHOT.OTDA_FK_VESSEL = dataDetail.vessel_id;
                            dataCHOT.OTDA_FK_FREIGHT = dataDetail.freight_id;
                            dataCHOT.OTDA_REASON = dataDetail.chot_reason;
                            if (dataDetail.chot_no_charter_out_case_a != null)
                            {
                                dataCHOT.OTDA_CASE_A_PERIOD_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_no_charter_out_case_a.period_from);//
                                dataCHOT.OTDA_CASE_A_PERIOD_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_no_charter_out_case_a.period_to);//
                                dataCHOT.OTDA_CASE_A_TC_HIRE = dataDetail.chot_no_charter_out_case_a.tc_hire;
                                dataCHOT.OTDA_CASE_A_NO_DAY = dataDetail.chot_no_charter_out_case_a.no_day;
                                dataCHOT.OTDA_CASE_A_TOTAL_TC_HIRE = dataDetail.chot_no_charter_out_case_a.tc_hire_val;
                                dataCHOT.OTDA_CASE_A_IDLING = dataDetail.chot_no_charter_out_case_a.idling;
                                dataCHOT.OTDA_CASE_A_BUNKER = dataDetail.chot_no_charter_out_case_a.bunker;
                                dataCHOT.OTDA_CASE_A_TOTAL_EXPENSE = dataDetail.chot_no_charter_out_case_a.total_expense;
                            }
                            if (dataDetail.chot_charter_out_case_b != null)
                            {
                                if (dataDetail.chot_charter_out_case_b.chot_est_income_b_one != null)
                                {
                                    dataCHOT.OTDA_CASE_B1_PERIOD_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_charter_out_case_b.chot_est_income_b_one.period_from);//
                                    dataCHOT.OTDA_CASE_B1_PERIOD_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_charter_out_case_b.chot_est_income_b_one.period_to);//
                                }
                            }
                            if (dataDetail.chot_charter_out_case_b != null)
                            {
                                if (dataDetail.chot_charter_out_case_b.chot_est_expense_b_two != null)
                                {
                                    dataCHOT.OTDA_CASE_B2_PERIOD_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_charter_out_case_b.chot_est_expense_b_two.period_from);//
                                    dataCHOT.OTDA_CASE_B2_PERIOD_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_charter_out_case_b.chot_est_expense_b_two.period_to);//
                                }
                            }
                            if (dataDetail.chot_summary != null)
                            {
                                dataCHOT.OTDA_FK_CHARTERER = dataDetail.chot_summary.charterer;
                                dataCHOT.OTDA_CHARTERER_PARTY_FORM = dataDetail.chot_summary.charterer_party_form;
                                dataCHOT.OTDA_CP_DATE = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.chot_summary.cp_date);//
                            }
                            if (dataDetail.chot_result != null)
                            {
                                /*
                                dataCHOT.OTDA_TOTAL_EXPENSE = dataDetail.chot_result.a_total_expense;
                                dataCHOT.OTDA_TOTAL_INCOME = dataDetail.chot_result.b_total_income;
                                dataCHOT.OTDA_TOTAL_NET_INCOME = dataDetail.chot_result.c_total_net_income;
                                if (dataDetail.chot_result.chot_d_result != null)
                                {
                                    dataCHOT.OTDA_RESULT_FLAG = dataDetail.chot_result.chot_d_result.flag;
                                    if (dataCHOT.OTDA_RESULT_FLAG.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_SAVING))
                                    {
                                        if (dataDetail.chot_result.chot_d_result.chot_saving != null)
                                        {
                                            dataCHOT.OTDA_RESULT_A = dataDetail.chot_result.chot_d_result.chot_saving.saving_a_b != null ? dataDetail.chot_result.chot_d_result.chot_saving.saving_a_b : "";
                                            dataCHOT.OTDA_RESULT_B = dataDetail.chot_result.chot_d_result.chot_saving.saving_a_c != null ? dataDetail.chot_result.chot_d_result.chot_saving.saving_a_c : "";
                                        }
                                    }
                                    if (dataCHOT.OTDA_RESULT_FLAG.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_PROFIT))
                                    {
                                        if (dataDetail.chot_result.chot_d_result.profit != null)
                                        {
                                            dataCHOT.OTDA_RESULT_A = dataDetail.chot_result.chot_d_result.profit.profit_b_a != null ? dataDetail.chot_result.chot_d_result.profit.profit_b_a : "";
                                            dataCHOT.OTDA_RESULT_B = dataDetail.chot_result.chot_d_result.profit.profit_c_a != null ? dataDetail.chot_result.chot_d_result.profit.profit_c_a : "";
                                        }
                                    }

                                }
                                */
                                dataCHOT.OTDA_RESULT_FLAG = dataDetail.chot_result.flag;
                                if (dataDetail.chot_result.flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_PROFIT))
                                {
                                    if (dataDetail.chot_result.chot_profit != null)
                                    {
                                        dataCHOT.OTDA_TOTAL_EXPENSE = dataDetail.chot_result.chot_profit.profit_total_expense;
                                        dataCHOT.OTDA_TOTAL_INCOME = dataDetail.chot_result.chot_profit.profit_inc_var;
                                        dataCHOT.OTDA_TOTAL_NET_INCOME = dataDetail.chot_result.chot_profit.profit_total;
                                    }                                    
                                }
                                else if (dataDetail.chot_result.flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_SAVING))
                                {
                                    if (dataDetail.chot_result.chot_saving != null)
                                    {
                                        dataCHOT.OTDA_TOTAL_EXPENSE = dataDetail.chot_result.chot_saving.saving_total_expense;
                                        dataCHOT.OTDA_TOTAL_INCOME = dataDetail.chot_result.chot_saving.saving_exp_inc;
                                        dataCHOT.OTDA_TOTAL_NET_INCOME = dataDetail.chot_result.chot_saving.saving_total;
                                    }                                    
                                }
                            }

                            dataCHOT.OTDA_NOTE = dataDetail.chot_note;
                            dataCHOT.OTDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHOT.OTDA_REQUESTED = dtNow;
                            dataCHOT.OTDA_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHOT.OTDA_CREATED = dtNow;
                            dataCHOT.OTDA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHOT.OTDA_UPDATED = dtNow;
                            dataCHOT.OTDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            string purchase_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            dataCHOT.OTDA_PURCHASE_NO = purchase_no;
                            dataCHOT.OTDA_OWNER_BROKER = dataDetail.chot_summary.owner_broker;
                            dataCHOT.OTDA_OWNER_BROKER_COMM = dataDetail.chot_summary.owner_broker_comm;
                            dataCHOT.OTDA_EXPLANATION = dataDetail.chot_reason_explanation;
                            dataCHOTDAL.Save(dataCHOT, context);

                            #endregion

                            #region chot_a_other_costs_dal

                            if (dataDetail.chot_no_charter_out_case_a != null && dataDetail.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs != null)
                            {
                                CHOT_A_OTHER_COSTS_DAL dataCHOTAOCDAL = new CHOT_A_OTHER_COSTS_DAL();
                                CHOT_A_OTHER_COSTS dataCHOTAOC = new CHOT_A_OTHER_COSTS();
                                int i = 1;
                                foreach (var itemChotAOtherCost in dataDetail.chot_no_charter_out_case_a.chot_charter_out_case_a_other_costs)
                                {
                                    dataCHOTAOC = new CHOT_A_OTHER_COSTS();
                                    dataCHOTAOC.OMAO_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHOTAOC.OMAO_FK_CHOT_DATA = TransID;
                                    dataCHOTAOC.OMAO_ORDER = i.ToString();
                                    dataCHOTAOC.OMAO_DETAIL = itemChotAOtherCost.detail;
                                    dataCHOTAOC.OMAO_VALUE = itemChotAOtherCost.value;
                                    dataCHOTAOC.OMAO_CREATED = dtNow;
                                    dataCHOTAOC.OMAO_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHOTAOC.OMAO_UPDATED = dtNow;
                                    dataCHOTAOC.OMAO_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHOTAOCDAL.Save(dataCHOTAOC, context);
                                    i++;
                                }
                            }

                            #endregion

                            #region CHOT_ATTACH_FILE
                            CHOT_ATTACH_FILE_DAL dataAttachFileDAL = new CHOT_ATTACH_FILE_DAL();
                            CHOT_ATTACH_FILE dataCHIAF;
                            foreach (var item in dataDetail.explanationAttach.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataCHIAF = new CHOT_ATTACH_FILE();
                                    dataCHIAF.OTAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHIAF.OTAF_FK_CHI_DATA = TransID;
                                    dataCHIAF.OTAF_PATH = text[0];
                                    dataCHIAF.OTAF_INFO = text[1];
                                    dataCHIAF.OTAF_TYPE = "EXT";
                                    dataCHIAF.OTAF_CREATED = dtNow;
                                    dataCHIAF.OTAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHIAF.OTAF_UPDATED = dtNow;
                                    dataCHIAF.OTAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataCHIAF, context);
                                }
                            }

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            if (!string.IsNullOrEmpty(attach_items))
                            {
                                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                                foreach (var item in Att.attach_items)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataCHIAF = new CHOT_ATTACH_FILE();
                                        dataCHIAF.OTAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCHIAF.OTAF_FK_CHI_DATA = TransID;
                                        dataCHIAF.OTAF_PATH = item;
                                        dataCHIAF.OTAF_INFO = "";
                                        dataCHIAF.OTAF_TYPE = "ATT";
                                        dataCHIAF.OTAF_CREATED = dtNow;
                                        dataCHIAF.OTAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCHIAF.OTAF_UPDATED = dtNow;
                                        dataCHIAF.OTAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataCHIAF, context);
                                    }
                                }
                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error Rollback # :: Exception >>> " + tem);
                            log.Error("# Error Rollback # :: Exception >>> ", ex);
                            log.Error("Rollback::Exception >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error Rollback # :: Exception >>> " + tem);
                log.Error("# Error Exception # :: Exception >>> ", e);
                log.Error("Exception >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAICharteringOutUpdateDataState # ");
            return isSuccess;
        }


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringOutUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOT_DATA_DAL dataDAL = new CHOT_DATA_DAL();
                            CHOT_DATA dataCHO = new CHOT_DATA();
                            dataCHO.OTDA_ROW_ID = TransID;
                            dataCHO.OTDA_REASON = note;
                            dataCHO.OTDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHO.OTDA_UPDATED = DateTime.Now;
                            dataCHO.OTDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHO, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error Rollback # :: Exception >>>  " + tem);
                            log.Error("# Error Rollback # :: Exception >>> ", ex);
                            log.Error("Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICharteringOutUpdateDataState >> UpdateDB #");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAICharteringOutUpdateDataState # :: UpdateDB >>> " + tem);
                log.Error("# Error CPAICharteringOutUpdateDataState >> UpdateDB # :: Exception >>> ", ex);
                log.Error("UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note, string explanation = null, string note_2 = null)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringOutUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOT_DATA_DAL dataDAL = new CHOT_DATA_DAL();
                            CHOT_DATA dataCHO = new CHOT_DATA();
                            dataCHO.OTDA_ROW_ID = TransID;
                            dataCHO.OTDA_REASON = note;
                            if (explanation != null)
                            {
                                dataCHO.OTDA_EXPLANATION = explanation;
                            }
                            //if (note_2 != null)
                            //{
                            //    dataCHO.OTDA_REASON = note_2;
                            //}
                            dataCHO.OTDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHO.OTDA_UPDATED = DateTime.Now;
                            dataCHO.OTDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHO, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error Rollback # :: Exception >>>  " + tem);
                            log.Error("# Error Rollback # :: Exception >>> ", ex);
                            log.Error("Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICharteringOutUpdateDataState >> UpdateDB #");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAICharteringOutUpdateDataState # :: UpdateDB >>> " + tem);
                log.Error("# Error CPAICharteringOutUpdateDataState >> UpdateDB # :: Exception >>> ", ex);
                log.Error("UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}