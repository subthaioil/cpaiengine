﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000008
{
    public class CPAIFreightVerifyRequireInputContinueState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIFreightVerifyRequireInputContinueState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.CurrentAction)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CURRENT_ACTION_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.NextStatus)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_SYSTEM_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Note)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_NOTE_RESP_CODE;
                else
                {
                    string action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    if (action.Equals(CPAIConstantUtil.ACTION_DRAFT) || action.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.DataDetailInput)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                        }
                        else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Type)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_TYPE_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else
                    {
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                }

                //get ip address
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIFreightVerifyRequireInputContinueState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIFreightVerifyRequireInputContinueState # :: Exception >>> " + ex);
                log.Error("CPAIFreightVerifyRequireInputContinueState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}