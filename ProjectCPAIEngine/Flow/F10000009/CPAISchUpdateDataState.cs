﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000009
{
    public class CPAISchUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISchUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch);



                SchRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<SchRootObject>(item);
                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string User = etxValue.GetValue(CPAIConstantUtil.User);
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL) ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_ACT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))

                    {
                        #region add data history
                        //add data history
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = User;
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_ACT))
                        {
                            dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act);
                        }
                        else
                        {
                            dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch);
                        }
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = User;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = User;
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Set detail to Value
                        string sVessel = dataDetail.sch_detail.vessel != null ? dataDetail.sch_detail.vessel : "";
                        string sVendor = dataDetail.sch_detail.vendor != null ? dataDetail.sch_detail.vendor : "";
                        string sVendorColor = dataDetail.sch_detail.vendor_color != null ? dataDetail.sch_detail.vendor_color : "";
                        string date_start = dataDetail.sch_detail.date_start;
                        string date_end = dataDetail.sch_detail.date_end;
                        string remark = dataDetail.sch_detail.note != null ? dataDetail.sch_detail.note : "";
                        string laycan_from = dataDetail.sch_detail.laycan_from != null ? dataDetail.sch_detail.laycan_from : "";
                        string laycan_to = dataDetail.sch_detail.laycan_to != null ? dataDetail.sch_detail.laycan_to : "";
                        string sStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                        #endregion

                        //Check Schedule+Activity
                        #region Check Schedule+Activity


                        if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) || currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))
                        {
                            CPAI_VESSEL_SCHEDULE_DAL schDAL = new CPAI_VESSEL_SCHEDULE_DAL();
                            string tmpdate_start = _FN.ConvertDateFormat(dataDetail.sch_detail.date_start, true).Replace("-", "/");
                            string tmpdate_end = _FN.ConvertDateFormat(dataDetail.sch_detail.date_end, true).Replace("-", "/");

                            bool isValidateSchedule = schDAL.checkValidSchedule(stateModel.EngineModel.ftxTransId, sVessel, CPAIConstantUtil.ACTION_SUBMIT, tmpdate_start, tmpdate_end);
                            if (!isValidateSchedule)
                            {
                                currentCode = CPAIConstantRespCodeUtil.CANNOT_VALIDATE_SCHEDULE_CODE;
                                stateModel.BusinessModel.currentCode = currentCode;
                                respCodeManagement.setCurrentCodeMapping(stateModel);
                                return;
                            }


                            if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))
                            {
                                bool isValidateActivity = schDAL.checkValidActivity(stateModel.EngineModel.ftxTransId);
                                if (!isValidateActivity)
                                {
                                    currentCode = CPAIConstantRespCodeUtil.CANNOT_VALIDATE_ACTIVITY_CODE;
                                    stateModel.BusinessModel.currentCode = currentCode;
                                    respCodeManagement.setCurrentCodeMapping(stateModel);
                                    return;
                                }
                            }
                        }

                        #endregion



                        #region Insert Update DB
                        bool isRunDB = false;
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
                        {
                            //OTHER ==> UPDATE
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue);
                        }
                        else if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_ACT))
                        {
                            var itemAct = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act);
                            if (itemAct != null)
                            {

                                SchRootObject dataActDetail = JSonConvertUtil.jsonToModel<SchRootObject>(itemAct);
                                isRunDB = InsertDBAct(stateModel.EngineModel.ftxTransId, etxValue, dataActDetail);
                            }
                        }
                        else
                        {
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                        }
                        #endregion


                        //check db by next status
                        #region INSERT JSON
                        if (isRunDB)
                        {
                            //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                            #region Set dataDetail
                            //set status
                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            //set action
                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            //set data_detail_sch
                            string dataDetailString = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch);
                            if (!string.IsNullOrEmpty(dataDetailString))
                            {
                                etxValue.SetValue(CPAIConstantUtil.Data_detail_sch, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                            }
                            //set data_detail_act
                            string dataDetailACTString = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act);
                            if (!string.IsNullOrEmpty(dataDetailACTString))
                            {
                                etxValue.SetValue(CPAIConstantUtil.Data_detail_act, new ExtendValue { value = dataDetailACTString, encryptFlag = "N" });
                            }
                            // if ((currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
                            //        currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) ||
                            //        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH) ||
                            //        currentAction.Equals("-")) &&
                            //         !string.IsNullOrEmpty(dataDetail.sch_detail.vessel) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.vendor) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.vendor_color) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.date_start) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.date_end) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.note))


                            //set data
                            etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = sVessel, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Vendor, new ExtendValue { value = sVendor, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Vendor_Color, new ExtendValue { value = sVendorColor, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Date_Start, new ExtendValue { value = date_start, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Date_End, new ExtendValue { value = date_end, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.REMARK, new ExtendValue { value = remark, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });

                            #endregion

                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                        }
                        #endregion
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAISchUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAISchUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAISchUpdateDataState::Exception >>> ", ex);
                //log.Error("CPAIUpdateDataState::Exception >>> ", e);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, SchRootObject dataDetail)
        {
            log.Info("# Start State CPAISchUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);
                            CPAI_VESSEL_SCHEDULE_DAL dataDAL = new CPAI_VESSEL_SCHEDULE_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_VESSEL_SCHEDULE
                            CPAI_VESSEL_SCHEDULE dataSCH = new CPAI_VESSEL_SCHEDULE();
                            dataSCH.VSD_ROW_ID = TransID;
                            dataSCH.VSD_FK_VEHICLE = dataDetail.sch_detail.vessel;
                            if (dataDetail.sch_detail.schedule_type == "Schedule")
                            {
                                dataSCH.VSD_FK_CUST = dataDetail.sch_detail.vendor;
                                dataSCH.VSD_FK_FREIGHT = dataDetail.sch_detail.freight;
                                if (!string.IsNullOrEmpty(dataDetail.sch_detail.date_start))
                                    dataSCH.VSD_DATE_START = DateTime.ParseExact(dataDetail.sch_detail.date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.sch_detail.date_end))
                                    dataSCH.VSD_DATE_END = DateTime.ParseExact(dataDetail.sch_detail.date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.sch_detail.laycan_from))
                                    dataSCH.VSD_LAYCAN_FROM = DateTime.ParseExact(dataDetail.sch_detail.laycan_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.sch_detail.laycan_to))
                                    dataSCH.VSD_LAYCAN_TO = DateTime.ParseExact(dataDetail.sch_detail.laycan_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataSCH.VSD_TENTATIVE_FLAG = dataDetail.sch_detail.tentative ? "Y" : "N";
                            }
                            else if (dataDetail.sch_detail.schedule_type != "Schedule")
                            {
                                if (!string.IsNullOrEmpty(dataDetail.sch_detail.date_start))
                                    dataSCH.VSD_DATE_START = DateTime.ParseExact(dataDetail.sch_detail.date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.sch_detail.date_end))
                                    dataSCH.VSD_DATE_END = DateTime.ParseExact(dataDetail.sch_detail.date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataSCH.VSD_DETAIL = dataDetail.sch_detail.detail;
                                dataSCH.VSD_COLOR = dataDetail.sch_detail.color;
                            }
                            dataSCH.VSD_NOTE = dataDetail.sch_detail.note;
                            dataSCH.VSD_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataSCH.VSD_CREATED_DATE = dtNow;
                            dataSCH.VSD_CREATED_BY = User;
                            dataSCH.VSD_UPDATED_DATE = dtNow;
                            dataSCH.VSD_UPDATED_BY = User;
                            dataSCH.VSD_SCH_TYPE = dataDetail.sch_detail.schedule_type;
                            dataDAL.Save(dataSCH, context);

                            if (dataDetail.sch_detail.schedule_type == "Schedule")
                            {
                                #region CPAI_VESSEL_SCHEDULE_PORT
                                CPAI_VESSEL_SCHEDULE_PORT_DAL dataSCHPortDAL = new CPAI_VESSEL_SCHEDULE_PORT_DAL();
                                CPAI_VESSEL_SCHEDULE_PORT dataSCHPort;
                                foreach (var item in dataDetail.sch_detail.sch_load_ports)
                                {
                                    dataSCHPort = new CPAI_VESSEL_SCHEDULE_PORT();
                                    dataSCHPort.VSP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataSCHPort.VSP_FK_VESSEL_SCHEDULE = TransID;
                                    dataSCHPort.VSP_ORDER_PORT = item.order;
                                    dataSCHPort.VSP_PORT_NAME = item.port;//??
                                    dataSCHPort.VSP_PORT_TYPE = "L";
                                    dataSCHPort.VSP_CREATED = dtNow;
                                    dataSCHPort.VSP_CREATED_BY = User;
                                    dataSCHPort.VSP_UPDATED = dtNow;
                                    dataSCHPort.VSP_UPDATED_BY = User;
                                    dataSCHPortDAL.Save(dataSCHPort, context);
                                }

                                foreach (var item in dataDetail.sch_detail.sch_dis_ports)
                                {
                                    dataSCHPort = new CPAI_VESSEL_SCHEDULE_PORT();
                                    dataSCHPort.VSP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataSCHPort.VSP_FK_VESSEL_SCHEDULE = TransID;
                                    dataSCHPort.VSP_ORDER_PORT = item.order;
                                    dataSCHPort.VSP_PORT_NAME = item.port;
                                    dataSCHPort.VSP_PORT_TYPE = "D";
                                    dataSCHPort.VSP_CREATED = dtNow;
                                    dataSCHPort.VSP_CREATED_BY = User;
                                    dataSCHPort.VSP_UPDATED = dtNow;
                                    dataSCHPort.VSP_UPDATED_BY = User;
                                    dataSCHPortDAL.Save(dataSCHPort, context);
                                }
                                #endregion

                                #region CPAI_VESSEL_SCHEDULE_CARGO
                                CPAI_VESSEL_SCHEDULE_CARGO_DAL dataSCHCargoDAL = new CPAI_VESSEL_SCHEDULE_CARGO_DAL();
                                CPAI_VESSEL_SCHEDULE_CARGO dataSCHCargo;
                                foreach (var item in dataDetail.sch_detail.sch_cargo)
                                {
                                    dataSCHCargo = new CPAI_VESSEL_SCHEDULE_CARGO();
                                    dataSCHCargo.VSCG_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataSCHCargo.VSCG_FK_VESSEL_SCHEDULE = TransID;
                                    dataSCHCargo.VSCG_FK_MT_MATERIALS = item.cargo;
                                    dataSCHCargo.VSCG_ORDER = item.order;
                                    dataSCHCargo.VSCG_QUANTITY = item.qty;
                                    dataSCHCargo.VSCG_UNIT = item.unit;
                                    dataSCHCargo.VSCG_OTHERS = item.others;
                                    dataSCHCargo.VSCG_TOLERANCE = item.tolerance;
                                    dataSCHCargo.VSCG_CREATED = dtNow;
                                    dataSCHCargo.VSCG_CREATED_BY = User;
                                    dataSCHCargo.VSCG_UPDATED = dtNow;
                                    dataSCHCargo.VSCG_UPDATED_BY = User;
                                    dataSCHCargoDAL.Save(dataSCHCargo, context);
                                }
                                #endregion
                            }

                            #endregion

                            #region CHOT_ATTACH_FILE
                            CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL dataAttachFileDAL = new CPAI_VESSEL_SCHEDULE_M_ATTACH_DAL();
                            CPAI_VESSEL_SCHEDULE_M_ATTACH dataCHIAF;

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            if (!string.IsNullOrEmpty(attach_items))
                            {
                                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                                foreach (var item in Att.attach_items)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataCHIAF = new CPAI_VESSEL_SCHEDULE_M_ATTACH();
                                        dataCHIAF.VSMF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCHIAF.VSMF_FK_VESSEL_SCHEDULE = TransID;
                                        dataCHIAF.VSMF_PATH = item;
                                        dataCHIAF.VSMF_INFO = "";
                                        dataCHIAF.VSMF_TYPE = "ATT";
                                        dataCHIAF.VSMF_CREATED = dtNow;
                                        dataCHIAF.VSMF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCHIAF.VSMF_UPDATED = dtNow;
                                        dataCHIAF.VSMF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataCHIAF, context);
                                    }
                                }
                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
            }
            #endregion
            log.Info("# End State CPAISchUpdateDataState >> InsertDB # ");
            return isSuccess;
        }


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAISchUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_VESSEL_SCHEDULE
                            CPAI_VESSEL_SCHEDULE_DAL dataDAL = new CPAI_VESSEL_SCHEDULE_DAL();
                            CPAI_VESSEL_SCHEDULE dataCHI = new CPAI_VESSEL_SCHEDULE();
                            dataCHI.VSD_ROW_ID = TransID;
                            dataCHI.VSD_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHI.VSD_UPDATED_DATE = DateTime.Now;
                            dataCHI.VSD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHI, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAISchUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAISchUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAISchUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool InsertDBAct(string TransID, Dictionary<string, ExtendValue> etxValue, SchRootObject dataDetail)
        {
            log.Info("# Start State CPAISchUpdateDataState >> InsertDBAct #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);
                            CPAI_VESSEL_SCHEDULE_ACTIVITYS_DAL dataDAL = new CPAI_VESSEL_SCHEDULE_ACTIVITYS_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_VESSEL_SCHEDULE_ACTIVITYS
                            CPAI_VESSEL_SCHEDULE_ACTIVITYS dataSCH;

                            CPAI_VESSEL_SCHEDULE_ATTACH_DAL dataAttachFileDAL = new CPAI_VESSEL_SCHEDULE_ATTACH_DAL();
                            CPAI_VESSEL_SCHEDULE_ATTACH dataACTAF;

                            CPAI_VESSEL_SCHEDULE_DAL scheDAL = new CPAI_VESSEL_SCHEDULE_DAL();
                            CPAI_VESSEL_SCHEDULE dataSCHE = new CPAI_VESSEL_SCHEDULE();
                            dataSCHE = context.CPAI_VESSEL_SCHEDULE.SingleOrDefault(a => a.VSD_ROW_ID == TransID);

                            foreach (var item in dataDetail.sch_activitys)
                            {
                                dataSCH = new CPAI_VESSEL_SCHEDULE_ACTIVITYS();
                                var row_id = Guid.NewGuid().ToString("N");
                                dataSCH.VAS_ROW_ID = row_id;
                                dataSCH.VAS_FK_VESSEL_SCHEDULE = TransID;
                                dataSCH.VAS_FK_VESSEL_ACTIVITY = item.activity;
                                dataSCH.VAS_ORDER = item.order;
                                if (!string.IsNullOrEmpty(item.date))
                                {
                                    dataSCH.VAS_END_DATE = DateTime.ParseExact(item.date, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }
                                if (!string.IsNullOrEmpty(item.start_date))
                                {
                                    dataSCH.VAS_START_DATE = DateTime.ParseExact(item.start_date, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }
                                dataSCH.VAS_ACTIVITY_OTHER = item.other_activity;
                                dataSCH.VAS_NOTE = item.act_note;
                                dataSCH.VSA_CREATED = dtNow;
                                dataSCH.VSA_CREATED_BY = User;
                                dataSCH.VSA_UPDATED = dtNow;
                                dataSCH.VSA_UPDATED_BY = User;
                                dataDAL.Save(dataSCH, context);
                                if (!string.IsNullOrEmpty(item.explanationAttach))
                                {
                                    foreach (var data in item.explanationAttach.Split('|'))
                                    {
                                        if (!string.IsNullOrEmpty(data))
                                        {
                                            dataACTAF = new CPAI_VESSEL_SCHEDULE_ATTACH();
                                            dataACTAF.VSAF_ROW_ID = Guid.NewGuid().ToString("N");
                                            dataACTAF.VSAF_FK_VESSEL_SCHEDULE_ACT = row_id;
                                            dataACTAF.VSAF_TYPE = "EXT";
                                            dataACTAF.VSAF_PATH = data;
                                            dataACTAF.VSAF_INFO = "";
                                            dataACTAF.VSAF_UPDATED = dtNow;
                                            dataACTAF.VSAF_UPDATED_BY = User;
                                            dataACTAF.VSAF_CREATED = dtNow;
                                            dataACTAF.VSAF_CREATED_BY = User;
                                            dataAttachFileDAL.Save(dataACTAF, context);
                                        }
                                    }
                                }
                                isSuccess = true;
                            }
                            if (dataSCHE != null)
                            {
                                dataSCHE.VSD_UPDATED_DATE = dtNow;
                                dataSCHE.VSD_UPDATED_BY = User;
                                scheDAL.Update(dataSCHE, context);
                            }

                            #endregion
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAISchUpdateDataState >> InsertDBAct #");
            return isSuccess;
        }
    }
}