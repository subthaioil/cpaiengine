﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000009
{
    public class CPAISchVerifyRequireInputContinueState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISchVerifyRequireInputContinueState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.CurrentAction)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CURRENT_ACTION_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.NextStatus)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_PRE_DEALS_SYSTEM_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Note)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_NOTE_RESP_CODE;

                else
                {
                    string action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    if (action.Equals(CPAIConstantUtil.ACTION_SUBMIT) || action.Equals(CPAIConstantUtil.ACTION_DRAFT))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                        }
                        else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Type)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_PRE_DEALS_TYPE_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else if (action.Equals(CPAIConstantUtil.ACTION_CANCEL))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Action)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else if (action.Equals(CPAIConstantUtil.ACTION_EDIT_SCH ))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else if (action.Equals(CPAIConstantUtil.ACTION_EDIT_ACT))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                    }
                }

                //get ip address
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAISchVerifyRequireInputContinueState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAISchVerifyRequireInputContinueState # :: Exception >>> " + ex);
                log.Error("CPAISchVerifyRequireInputContinueState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
