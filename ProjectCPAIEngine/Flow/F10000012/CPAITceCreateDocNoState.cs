﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000012
{
    public class CPAITceCreateDocNoState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAITceCreateDocNoState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            //test
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string LastSEQ = MasterData.GetSeqTce();
                string docNo = "TCEWS-" + DateTime.Now.ToString("yyMM") + "-" + LastSEQ;

                ExtendValue extDocNumber = new ExtendValue();
                extDocNumber.value = docNo;
                //doc no.
                etxValue.Add(CPAIConstantUtil.DocNo, extDocNumber);
                etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.DocNo, extDocNumber);
                //create by
                ExtendValue extUser = new ExtendValue();
                extUser.value = etxValue.GetValue(CPAIConstantUtil.User);
                etxValue.Add(CPAIConstantUtil.CreateBy, extUser);

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAITceCreateDocNoState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAITceCreateDocNoState # :: Exception >>> " + ex);
                log.Error("CPAITceCreateDocNoState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }

        }
    }
}