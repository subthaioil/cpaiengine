﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000012
{
    public class CPAITceUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAITceUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();

            string rtnMessage = "";

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
               
                TceRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<TceRootObject>(item);
                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string User = etxValue.GetValue(CPAIConstantUtil.User);
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT) || (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)))

                    {
                        #region add data history
                        //add data history
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = User;
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = User;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = User;
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Set detail to Value
                        string dataDetailString = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        string laycan_from = dataDetail.tce_detail.laycan_load_from;
                        string laycan_to = dataDetail.tce_detail.laycan_load_to;
                        string date_fixture = dataDetail.tce_detail.date_fixture;
                        //string date_end = dataDetail.tce_detail.date_end_fixture;
                        string charterIn = dataDetail.tce_detail.chi_data_id != null ? dataDetail.tce_detail.chi_data_id : "";
                        string sVessel = dataDetail.tce_detail.ship_name != null ? dataDetail.tce_detail.ship_name : "";

                        string sStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                        #endregion

                        //check charter in voy duplicate
                        Boolean b = CPAI_TCE_WS_DAL.isDuplicate(dataDetail.tce_detail.chi_data_id, CPAIConstantUtil.ACTION_SUBMIT);
                        //if b = true : dup when ACTION_DRAFT,ACTION_SUBMIT,ACTION_EDIT
                        if (!b 
                            || (b && currentAction.Equals(CPAIConstantUtil.ACTION_EDIT))
                            || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)
                            ) { 
                        /*if ( (!b && currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                            || (!b && currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                            || (!b && currentAction.Equals(CPAIConstantUtil.ACTION_EDIT))
                            || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)
                            ) {*/
                            #region Insert Update DB
                            bool isRunDB = false;
                            if (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
                            {
                                //OTHER ==> UPDATE
                                isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue);
                            }
                            else 
                            {
                                double? avgWS = 0;
                                
                                if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                                {
                                    CPAI_TCE_WS_DAL dataDAL = new CPAI_TCE_WS_DAL();
                                    var TD = dataDetail.tce_detail.td;
                                    var dt = DateTime.ParseExact(dataDetail.tce_detail.laycan_load_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    var ty = "UK";

                                    var pair = dataDAL.CalEverAgeWS(dt, TD, ty);
                                    avgWS = pair.Key;
                                    rtnMessage = pair.Value;
                                    etxValue.SetValue(CPAIConstantUtil.Resp + CPAIConstantUtil.Message, new ExtendValue { value = rtnMessage, encryptFlag = "N" });

                                    if (avgWS == null)
                                    {
                                        currentCode = CPAIConstantRespCodeUtil.NOT_CAL_AVG_WS_RESP_CODE;
                                        goto EndState;
                                    }
                                }
                                
                               
                                //laycan_from;
                                isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail, Convert.ToDouble(avgWS));
                                    
                            }

                            #endregion

                            //check db by next status
                            #region INSERT JSON
                            if (isRunDB)
                            {
                                //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                                #region Set dataDetail
                                //set status
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                //set action
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                //set data_detail_sch
                                etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetailString, encryptFlag = "N" });

                                //set data
                                etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Date_fixture, new ExtendValue { value = date_fixture, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Charter_in, new ExtendValue { value = charterIn, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = sVessel, encryptFlag = "N" });

                                #endregion

                                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            }
                            else
                            {
                                currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                            }
                            #endregion
                        }
                        else
                        {
                            //duplicate charter in data
                            currentCode = CPAIConstantRespCodeUtil.DUP_DATA_RESP_CODE;
                        }
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                EndState:
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                if(!string.IsNullOrEmpty(rtnMessage))
                    stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + rtnMessage + "]";
                log.Info("# End State CPAITceUpdateDataState # :: Code >>> " + currentCode + " : " + stateModel.BusinessModel.currentDesc);

            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAITceUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAITceUpdateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, TceRootObject dataDetail, double AvgWs)
        {
            log.Info("# Start State CPAITceUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User= etxValue.GetValue(CPAIConstantUtil.User);
                            CPAI_TCE_WS_DAL dataDAL = new CPAI_TCE_WS_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_TCE_WS
                            DateTime dtLaycanLoadFrom = DateTime.MinValue;
                            CPAI_TCE_WS dataSCH = new CPAI_TCE_WS();
                            dataSCH.CTW_ROW_ID = TransID;
                            dataSCH.CTW_FIXTURE_DATE = dataDetail.tce_detail.date_fixture;
                            dataSCH.CTW_FK_CHI_DATA = dataDetail.tce_detail.chi_data_id;
                            dataSCH.CTW_VOY_NO = etxValue.GetValue(CPAIConstantUtil.DocNo);
                            dataSCH.CTW_FK_VENDOR = dataDetail.tce_detail.ship_broker;
                            dataSCH.CTW_FK_VEHICLE = dataDetail.tce_detail.ship_name;
                            dataSCH.CTW_OWNER = dataDetail.tce_detail.ship_owner;
                            dataSCH.CTW_FLAT_RATE = dataDetail.tce_detail.flat_rate;

                            if (!string.IsNullOrEmpty(dataDetail.tce_detail.laycan_load_from))
                            {
                                dtLaycanLoadFrom = DateTime.ParseExact(dataDetail.tce_detail.laycan_load_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataSCH.CTW_LAYCAN_FROM = dtLaycanLoadFrom;
                            }
                           
                            if (!string.IsNullOrEmpty(dataDetail.tce_detail.laycan_load_to))
                                dataSCH.CTW_LAYCAN_TO = DateTime.ParseExact(dataDetail.tce_detail.laycan_load_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dataSCH.CTW_EXTRA_COST = dataDetail.tce_detail.extra_cost;
                            dataSCH.CTW_DEM = dataDetail.tce_detail.dem;

                            if (dtLaycanLoadFrom != DateTime.MinValue)
                            {
                                #region Calcualate
                                //dateTarget = laycan_load_from - 20days;
                                //TopFixture = (A) input key;
                                //AvgValue =  (B) Cal Avg WS 3 days --> dateTarget next 3 days
                                //Benefit = (B) - (A)

                                DateTime dateTarget = DateTime.MinValue;
                                //double AvgWs = 0.0;
                                string sTD = dataDetail.tce_detail.td;
                                double TopFixture = dataDetail.tce_detail.top_fixture_value != "" ? Convert.ToDouble(dataDetail.tce_detail.top_fixture_value) : 0.0;
                                double WsSaving = 0.0;
                                double SavingIn = 0.0;
                                double MinLoad = dataDetail.tce_detail.min_load != "" ? Convert.ToDouble(dataDetail.tce_detail.min_load) : 0.0;
                                double FlatRate = dataDetail.tce_detail.flat_rate != "" ? Convert.ToDouble(dataDetail.tce_detail.flat_rate) : 0.0;


                                dateTarget = dataDAL.CalTargetDate(dtLaycanLoadFrom);
                                //AvgWs = dataDAL.CalEverageWS(dateTarget, sTD);
                                WsSaving = AvgWs - TopFixture;
                                SavingIn = WsSaving * ( MinLoad * 10 * FlatRate);
                                //var result = Math.Round(SavingIn, 2, MidpointRounding.AwayFromZero);
                                //decimal newVat = decimal.Parse(SavingIn.ToString(), System.Globalization.NumberStyles.Float);
                                //var str = String.Format("{0:0.00}", result);

                                dataSCH.CTW_DATE_TARGET = dateTarget;//Cal
                                dataSCH.CTW_FIXTURE_VALUE = dataDetail.tce_detail.top_fixture_value; //(A)
                                dataSCH.CTW_AVG_VALUE = AvgWs.ToString();//Cal Avg WS 3 days  (B)
                                dataSCH.CTW_BENEFIT_VALUE = WsSaving.ToString();//cal WS saving (B) - (A)
                                dataSCH.CTW_BENEFIT_NET_VALUE = SavingIn.ToString();//cal Saving in $
                                #endregion
                            }
                            dataSCH.CTW_TD_TYPE = dataDetail.tce_detail.td;
                            dataSCH.CTW_MIN_LOAD = dataDetail.tce_detail.min_load;

                            dataSCH.CTW_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataSCH.CTW_CREATED_DATE = dtNow;
                            dataSCH.CTW_CREATED_BY = User;
                            dataSCH.CTW_UPDATED_DATE = dtNow;
                            dataSCH.CTW_UPDATED_BY = User;

                            dataDAL.Save(dataSCH, context);
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
            }
            #endregion
            log.Info("# End State CPAITceUpdateDataState >> InsertDB #");
            return isSuccess;
        }


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAITceUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_TCE_WS
                            CPAI_TCE_WS_DAL dataDAL = new CPAI_TCE_WS_DAL();
                            CPAI_TCE_WS data = new CPAI_TCE_WS();
                            data.CTW_ROW_ID = TransID;
                            data.CTW_REASON = etxValue.GetValue(CPAIConstantUtil.Note);
                            data.CTW_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.CTW_UPDATED_DATE = DateTime.Now;
                            data.CTW_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(data, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAITceUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAITceUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAITceUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}