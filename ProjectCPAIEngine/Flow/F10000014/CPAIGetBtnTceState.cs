﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000014
{
    public class CPAIGetBtnTceState : BasicBean,StateFlowAction
    {
        private const String TYPE_1 = "1";
        private FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
        private ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
        private ActionFunctionDAL acfMan = new ActionFunctionDAL();
        private UsersDAL usrMan = new UsersDAL();
        private ActionControlDAL actMan = new ActionControlDAL();
        private ActionOverrideDAL acoMan = new ActionOverrideDAL();
        private ActionButtonDAL abtMan = new ActionButtonDAL();
        private DataHistoryDAL dthMan = new DataHistoryDAL();

        private FUNCTION_TRANSACTION currentFtx;

        private String currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGetBtnTceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
           
                getButtonList(stateModel.BusinessModel.etxValue);
                getTce(stateModel.BusinessModel.etxValue);
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIGetBtnTceState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIGetBtnTceState # :: Exception >>> " + ex);
                log.Error("CPAIGetBtnTceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private void getButtonList(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnTceState >> getButtonList #  ");
            List<CPAI_ACTION_BUTTON> lstAllButton = new List<CPAI_ACTION_BUTTON>();

            // find function transaction by "transaction_id" : (1)
            var ftxResult = ftxMan.findByTransactionIdSystemType(etxValue.GetValue(CPAIConstantUtil.TransactionId),
                etxValue.GetValue(CPAIConstantUtil.System), etxValue.GetValue(CPAIConstantUtil.Type));
            if (ftxResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.FUNCTION_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            currentFtx = ftxResult[0];
            string currentStatus = currentFtx.FTX_INDEX4;
            //if (currentStatus.Contains("APPROVE"))
            //{
            //    currentStatus = "APPROVE";
            //}
            // find user record by "user" : (2)
            var usrResult = usrMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (usrResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                throw new Exception();
            }
            USERS users = usrResult[0];
            string userRowId = users.USR_ROW_ID;
            string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
            string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
            string system = etxValue.GetValue(CPAIConstantUtil.System);
            string type = etxValue.GetValue(CPAIConstantUtil.Type);

            // find CPAI_ACTION_FUNCTION by "FUNCTION_TRANSACTION.FTX_FK_FUNCTION" and "USERS.USR_ROW_ID" and other params from request : (3)
            List<CPAI_ACTION_FUNCTION> acfResult = new List<CPAI_ACTION_FUNCTION>();
            if (!string.IsNullOrEmpty(userGroupDelegate)) //set user group deligate by poo 25072017
            {
                List<string> userGroups = new List<string>();
                userGroups.Add(userGroup.ToUpper());
                userGroups.Add(userGroupDelegate.ToUpper());
                acfResult = acfMan.findAcfDelegate(currentFtx.FTX_FK_FUNCTION, userGroups, userRowId, system, type);
            }
            else
            {
                acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, type);
            }
            if (acfResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            List<String> lstAcfFunction = new List<string>();
            for (int i = 0; i < acfResult.Count; i++)
            {
                lstAcfFunction.Add(acfResult[i].ACF_ROW_ID);
            }

            // find CPAI_ACTION_CONTROL by "FUNCTION_TRANSACTION.FTX_INDEX3" and "CPAI_ACTION_FUNCTION.ACF_ROW_ID"
            var actResult = actMan.findByCurrentStatusAndFunction(currentStatus, lstAcfFunction);
            if (actResult.Count == 0)
            {
                // not found any button, do nothing
            }
            for (int i = 0; i < actResult.Count; i++)
            {
                string fkActionButton = actResult[i].ACT_FK_ACTION_BUTTON;

                // find CPAI_ACTION_OVERRIDE record by CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON value
                var acoResult = acoMan.findByFkActionControl(fkActionButton);

                //if override not found, use button value from CPAI_ACTION_BUTTON
                if (acoResult.Count == 0)
                {
                    //find by row id an type of button
                    var abtResult = abtMan.findByRowIdAndType(actResult[i].ACT_FK_ACTION_BUTTON, etxValue.GetValue(CPAIConstantUtil.Button_Type));
                    if (abtResult.Count == 0)
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
                        throw new Exception();
                    }
                    else
                    {
                        lstAllButton.Add(abtResult[0]);
                    }
                }
                // if override found, use button value from CPAI_ACTION_OVERRIDE
                else
                {
                    var acoType = acoResult[0].ACO_OVERRIDE_TYPE;
                    if (acoType.Equals(TYPE_1))
                    {
                        string user = etxValue.GetValue(CPAIConstantUtil.User);
                        if (string.Equals(user, acoResult[0].ACO_OVERRIDE_VALUE, StringComparison.InvariantCultureIgnoreCase))
                        {
                            var abtResult = abtMan.findByRowId(acoResult[0].ACO_FK_ACTION_BUTTON);
                            if (abtResult.Count == 0)
                            {
                                currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
                                throw new Exception();
                            }
                            else
                            {
                                lstAllButton.Add(abtResult[0]);
                            }
                        }
                    }
                }
            }

            // set extra xml <button_detail> 
            List<Button> list = new List<Button>();
            string tem = "";
            for (int i = 0; i < lstAllButton.Count; i++)
            {
                CPAI_ACTION_BUTTON abt = lstAllButton[i];
                if (!tem.Contains(abt.ABT_BUTTON_NAME))
                {
                    Button m = new Button();
                    m.name = abt.ABT_BUTTON_NAME;
                    m.page_url = abt.ABT_PAGE_URL;
                    m.call_xml = abt.ABT_XML;
                    tem = tem + "|" + abt.ABT_BUTTON_NAME;
                    list.Add(m);
                }
            }

            ButtonList li = new ButtonList();
            li.lstButton = list;

            String xml = ShareFunction.XMLSerialize<ButtonList>(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            log.Info("# End State CPAIGetBtnTceState >> getButtonList # :: Code >>> " + currentCode);
        }
        private void getTce(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnTceState >> getTce #  ");
            var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.DataDetail);
            if (etxResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }

            string dataDetail = "";

            if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.TCE_WS_FUNCTION_CODE)
            {
                dataDetail =  TceServiceModel.getTransactionByID(currentFtx.FTX_TRANS_ID);
            }
            else
            {
                dataDetail = etxResult[0].ETX_VALUE;
            }

            DataDetail respDataDetail = new DataDetail();
            respDataDetail.Text = dataDetail;

            // set extra xml 
            String xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);

            string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            var newXml = oldXml + xml;

            ExtendValue respTrans = new ExtendValue();
            respTrans.value = newXml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            log.Info("# End State CPAIGetBtnTceState >> getTce # :: Code >>> " + currentCode);
        }


    }
}