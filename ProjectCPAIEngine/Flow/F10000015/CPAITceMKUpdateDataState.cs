﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000015
{
    public class CPAITceMKUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAITceMKUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
               
               

                TceMkRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<TceMkRootObject>(item);
                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string User = etxValue.GetValue(CPAIConstantUtil.User);
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT) || (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)))
                         
                    {
                        #region add data history
                        //add data history
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = User;
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = User;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = User;
                        dataHistory.DTH_UPDATED_DATE = now;                        
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Set detail to Value
                        string dataDetailString = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        string month = dataDetail.tce_mk_detail.month;
                        string date_from = dataDetail.tce_mk_detail.date_from;
                        string date_to = dataDetail.tce_mk_detail.date_to;
                        string sVessel = dataDetail.tce_mk_detail.vessel != null ? dataDetail.tce_mk_detail.vessel : "";

                        string sStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                        #endregion
                        //check charter in voy duplicate
                        Boolean b = CPAI_TCE_MK_DAL.isDuplicate(dataDetail.tce_mk_detail.vessel, dataDetail.tce_mk_detail.month, CPAIConstantUtil.ACTION_SUBMIT);
                        //check data 
                        if (!b
                            || currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT)
                           || (b && currentAction.Equals(CPAIConstantUtil.ACTION_EDIT))
                           || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)
                           )
                        {
                            #region Insert Update DB
                            bool isRunDB = false;
                                if (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
                                {
                                    //OTHER ==> UPDATE
                                    isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue);
                                }
                                else
                                {
                                    double? avgWS = 0;
                                    if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                                    {
                                        CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
                                        var TD = dataDetail.tce_mk_detail.td;
                                    
                                    // set start date from month
                                    //var dt = DateTime.ParseExact(dataDetail.tce_mk_detail.month, "MM/yyyy", CultureInfo.InvariantCulture);
                                    
                                    // Change to start date
                                    var dt = DateTime.ParseExact(dataDetail.tce_mk_detail.loadingdate_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    var ty = "UK";
                                        avgWS = dataDAL.CalEverageWS(dt, TD, ty);

                                        if (avgWS == null)
                                        {
                                            currentCode = CPAIConstantRespCodeUtil.NOT_CAL_AVG_WS_BITR_TD2_RESP_CODE;
                                            goto EndState;
                                        }
                                    }
                                    
                                    isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail, Convert.ToDouble(avgWS));
                                }
                                #endregion
                                //check db by next status
                                #region INSERT JSON
                                if (isRunDB)
                                {
                                    //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                                    #region Set dataDetail
                                    //set status
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    //set action
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    //set data_detail_sch
                                    etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                          
                                    //set data
                                    etxValue.SetValue(CPAIConstantUtil.Month, new ExtendValue { value = month, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Date_from, new ExtendValue { value = date_from, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Date_to, new ExtendValue { value = date_to, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = sVessel, encryptFlag = "N" });

                                    #endregion

                                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                                }
                                else
                                {
                                    currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                                }
                                    #endregion
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.DUP_DATA_RESP_CODE;
                        }

                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                EndState:
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAITceMKUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAITceMKUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAITceMKUpdateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, TceMkRootObject dataDetail, double AvgWS)
        {
            log.Info("# Start State CPAITceMKUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User= etxValue.GetValue(CPAIConstantUtil.User);
                            CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_VESSEL_SCHEDULE
                            DateTime dtLaycanLoadFrom = DateTime.MinValue;
                            CPAI_TCE_MK dataSCH = new CPAI_TCE_MK();
                            dataSCH.CTM_ROW_ID = TransID;
                            dataSCH.CTM_TASK_NO = etxValue.GetValue(CPAIConstantUtil.DocNo);
                            dataSCH.CTM_FK_MT_VEHICLE = dataDetail.tce_mk_detail.vessel;

                            if (!string.IsNullOrEmpty(dataDetail.tce_mk_detail.date_from))
                                dataSCH.CTM_DATE_FROM = DateTime.ParseExact(dataDetail.tce_mk_detail.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.tce_mk_detail.date_to))
                                dataSCH.CTM_DATE_TO = DateTime.ParseExact(dataDetail.tce_mk_detail.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            if (!string.IsNullOrEmpty(dataDetail.tce_mk_detail.loadingdate_from))
                                dataSCH.CTM_LOADING_DATE_FROM = DateTime.ParseExact(dataDetail.tce_mk_detail.loadingdate_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.tce_mk_detail.loadingdate_to))
                                dataSCH.CTM_LOADING_DATE_TO = DateTime.ParseExact(dataDetail.tce_mk_detail.loadingdate_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataSCH.CTM_TD = dataDetail.tce_mk_detail.td;
                            dataSCH.CTM_MONTH = dataDetail.tce_mk_detail.month;
                            dataSCH.CTM_TC_RATE = dataDetail.tce_mk_detail.tc_rate;
                            dataSCH.CTM_OPERATING_DAYS = dataDetail.tce_mk_detail.operating_days;
                            dataSCH.CTM_BUNKER_FO = dataDetail.tce_mk_detail.bunker_cost_fo;
                            dataSCH.CTM_BUNKER_GO = dataDetail.tce_mk_detail.bunker_cost_go;
                            dataSCH.CTM_OTHER_COST = dataDetail.tce_mk_detail.other_cost;
                            dataSCH.CTM_FLAT_RATE = dataDetail.tce_mk_detail.flat_rate;
                            dataSCH.CTM_MINLOAD = dataDetail.tce_mk_detail.minload;
                            dataSCH.CTM_THEORITICAL_DAYS = dataDetail.tce_mk_detail.theoritical_days;
                            dataSCH.CTM_FO_MT = dataDetail.tce_mk_detail.fo_mt;
                            dataSCH.CTM_GO_MT = dataDetail.tce_mk_detail.go_mt;
                            dataSCH.CTM_DEMURRAGE = dataDetail.tce_mk_detail.demurrage;

                            dataSCH.CTM_TC_NUM = dataDetail.tce_mk_detail.tc_num;
                            dataSCH.CTM_FO_UNIT_PRICE = dataDetail.tce_mk_detail.fo_unit_price;
                            dataSCH.CTM_GO_UNIT_PRICE = dataDetail.tce_mk_detail.go_unit_price;

                            #region Calcualate
                            //Time charter cost = TC rate - Operating days;
                            //Total Port Cost (incl.Discharge Port) = Port charge 1 + Port charge 2 + Port charge 3 + Discharge port
                            //Total Bunker cost = Bunker used - FO + Bunker used - GO
                            //TOTAL COST = Time charter cost + Port charge 1 + Port charge 2 + Discharge port + Total Bunker cost
                            //WS BITR TD (M-1) = (SELECT MCR_TD?? -> CTM_TD FROM MT_CRUDE_ROUTES WHERE CTM_MONTH = MCR_DATE.MONTH - 1) / COUNT
                            //Total Market Cost (2) By Annie (2015-2016) = (WS BITR TD (M-1) / 100) * Flat rate * 265000
                            //Saving/Loss from Market (2)-(1) = Total Market Cost (2) By Annie (2015-2016) - TOTAL COST
                            //SPOT Variable cost (bunker+P/D) = Total Port Cost (incl.Discharge Port) - Total Bunker cost
                            //TCE benefit/Day = ((Total Market Cost (2) By Annie (2015-2016) / 35) - (SPOT Variable cost (bunker+P/D) / Operating days)) - TC rate
                            //TCE benefit/Voy = TCE benefit/Day * Operating days
                            //TCE / Day = TC rate + TCE benefit/Day

                            //initial values
                            double tc_rate = 0, operating_days = 0, bunker_fo = 0, bunker_go = 0, flat_rate = 0;
                            double.TryParse(dataDetail.tce_mk_detail.tc_rate, out tc_rate);
                            double.TryParse(dataDetail.tce_mk_detail.operating_days, out operating_days);
                            double.TryParse(dataDetail.tce_mk_detail.bunker_cost_fo, out bunker_fo);
                            double.TryParse(dataDetail.tce_mk_detail.bunker_cost_go, out bunker_go);
                            double.TryParse(dataDetail.tce_mk_detail.flat_rate, out flat_rate);

                            int theoritical_days = 0, minload = 0;
                            int.TryParse(dataDetail.tce_mk_detail.theoritical_days, out theoritical_days);
                            int.TryParse(dataDetail.tce_mk_detail.minload, out minload);

                            List<double> port_charge = new List<double>();
                            if (dataDetail.tce_mk_detail.load_ports != null)
                            {
                                foreach (TceMkLoadPort p in dataDetail.tce_mk_detail.load_ports)
                                {
                                    double port_value = 0;
                                    double.TryParse(p.port_value, out port_value);
                                    port_charge.Add(port_value);
                                }
                            }
                            
                            List<double> discharge_port = new List<double>();
                            if (dataDetail.tce_mk_detail.dis_ports != null)
                            {
                                foreach (TceMkDisPort p in dataDetail.tce_mk_detail.dis_ports)
                                {
                                    double port_value = 0;
                                    double.TryParse(p.port_value, out port_value);
                                    discharge_port.Add(port_value);
                                }
                            }

                            //Time charter cost
                            double timeCharterCost = tc_rate * operating_days;
                            double totalLoadPort = port_charge.Count> 0 ? port_charge.Sum() : 0;
                            double totalDisPort = discharge_port.Count > 0 ? discharge_port.Sum() : 0;
                            double totalPortCost = totalLoadPort + totalDisPort;
                            double totalBunkerCost = bunker_go + bunker_fo;

                            //Total freight cost (1)
                            double totalCost = timeCharterCost + totalLoadPort + totalDisPort + totalBunkerCost;

                            //WS BITR TD2 (M-1)
                            double WS_BITR_TD = AvgWS;  //double WS_BITR_TD = dataDAL.CalEverageWS(DateTime.ParseExact(dataDetail.tce_mk_detail.month, "MM/yyyy", CultureInfo.InvariantCulture), dataDetail.tce_mk_detail.td);

                            //Total Market cost (2)
                            double totalMarketCost = (WS_BITR_TD / 100) * flat_rate * minload;

                            //Saving/Loss from Market (2)-(1)
                            double SavingLossFromMarket = totalMarketCost - totalCost;

                            //SPOT Variable cost (bunker+P/D)
                            double SPOTVariableCost = totalPortCost + totalBunkerCost;

                            //TCE benefit/day
                            double TCEBenefitDay = ((totalMarketCost / theoritical_days) - (SPOTVariableCost / operating_days)) - tc_rate;
                            double TCEBenefitVoy = TCEBenefitDay * operating_days;
                            double TCEDay = tc_rate + TCEBenefitDay;

                            #endregion

                            dataSCH.CTM_TIME_CHARTER_COST = timeCharterCost.ToString();
                            dataSCH.CTM_TOTAL_PORT_COST = totalPortCost.ToString();
                            dataSCH.CTM_TOTAL_BUNKER_COST = totalBunkerCost.ToString();
                            dataSCH.CTM_TOTAL_COST = totalCost.ToString();
                            dataSCH.CTM_WS_BITR_TD = WS_BITR_TD.ToString();
                            dataSCH.CTM_TOTAL_MARKET_COST = totalMarketCost.ToString();
                            dataSCH.CTM_SAVING_LOSS_MARKET = SavingLossFromMarket.ToString();
                            dataSCH.CTM_SPOT_VARIABLE_COST = SPOTVariableCost.ToString();
                            dataSCH.CTM_TCE_BENEFIT_DAY = TCEBenefitDay.ToString();
                            dataSCH.CTM_TCE_BENEFIT_VOY = TCEBenefitVoy.ToString();
                            dataSCH.CTM_TCE_DAY = TCEDay.ToString();
                            dataSCH.CTM_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataSCH.CTM_CREATED_DATE = dtNow;
                            dataSCH.CTM_CREATED_BY = User;
                            dataSCH.CTM_UPDATED_DATE = dtNow;
                            dataSCH.CTM_UPDATED_BY = User;

                            dataDAL.Save(dataSCH, context);
                            #endregion

                            #region TCE_MK_LOAD_PORT
                            CPAI_TCE_MK_PORT_DAL dataLoadPortDAL = new CPAI_TCE_MK_PORT_DAL();
                            CPAI_TCE_MK_PORT dataTCEMKLP;
                            foreach (var itemLoadPort in dataDetail.tce_mk_detail.load_ports)
                            {
                                dataTCEMKLP = new CPAI_TCE_MK_PORT();
                                dataTCEMKLP.TMP_ROW_ID = Guid.NewGuid().ToString("N");
                                dataTCEMKLP.TMP_FK_TCE_MK = TransID;
                                dataTCEMKLP.TMP_ORDER_PORT = itemLoadPort.port_order;
                                dataTCEMKLP.TMP_POST_COST = itemLoadPort.port_value;
                                if (itemLoadPort.port != "")
                                {
                                    dataTCEMKLP.TMP_FK_PORT = Convert.ToDecimal(itemLoadPort.port);
                                }
                                dataTCEMKLP.TMP_PORT_TYPE = "L";
                                dataTCEMKLP.TMP_CREATED_DATE = dtNow;
                                dataTCEMKLP.TMP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataTCEMKLP.TMP_UPDATED = dtNow;
                                dataTCEMKLP.TMP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataLoadPortDAL.Save(dataTCEMKLP, context);
                            }
                            #endregion

                            #region TCE_MK_DIS_PORT
                            CPAI_TCE_MK_PORT_DAL dataDisPortDAL = new CPAI_TCE_MK_PORT_DAL();
                            CPAI_TCE_MK_PORT dataTCEMKDP;
                            foreach (var itemDisPort in dataDetail.tce_mk_detail.dis_ports)
                            {
                                dataTCEMKDP = new CPAI_TCE_MK_PORT();
                                dataTCEMKDP.TMP_ROW_ID = Guid.NewGuid().ToString("N");
                                dataTCEMKDP.TMP_FK_TCE_MK = TransID;
                                dataTCEMKDP.TMP_ORDER_PORT = itemDisPort.port_order;
                                dataTCEMKDP.TMP_POST_COST = itemDisPort.port_value;
                                if (itemDisPort.port != "")
                                {
                                    dataTCEMKDP.TMP_FK_PORT = Convert.ToDecimal(itemDisPort.port);
                                }
                                dataTCEMKDP.TMP_PORT_TYPE = "D";
                                dataTCEMKDP.TMP_CREATED_DATE = dtNow;
                                dataTCEMKDP.TMP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataTCEMKDP.TMP_UPDATED = dtNow;
                                dataTCEMKDP.TMP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataLoadPortDAL.Save(dataTCEMKDP, context);
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAITceMKUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAITceMKUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_TCE_MK
                            CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
                            CPAI_TCE_MK data = new CPAI_TCE_MK();
                            data.CTM_ROW_ID = TransID;
                            data.CTM_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.CTM_UPDATED_DATE = DateTime.Now;
                            data.CTM_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(data, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAITceMKUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAITceMKUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAITceMKUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}