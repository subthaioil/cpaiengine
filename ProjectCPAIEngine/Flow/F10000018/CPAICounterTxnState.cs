﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;

using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ProjectCPAIEngine.Flow.F10000018
{
    public class CPAICounterTxnState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICounterTxnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                string jsonXML = "";
                jsonXML = "{\"counter_txn\": []}";
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
                string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);

                MasterData acfMan = new MasterData();
                ActionFunctionDAL afd = new ActionFunctionDAL();
                ActionMobileControlDAL amc = new ActionMobileControlDAL();
                PAFCounterHelper pch = new PAFCounterHelper();
                DAFCounterHelper dch = new DAFCounterHelper();

                //fix function code = schedule
                List<CPAI_USER_GROUP> result = acfMan.getUserList(user, system);


                List<Transaction> lstAllTx = new List<Transaction>();
                if (result != null && result.Count > 0)
                {
                    string user_group = result[0].USG_USER_GROUP;
                    //get type by user_group and system
                    List<CPAI_ACTION_FUNCTION> caf = new List<CPAI_ACTION_FUNCTION>();
                    for (int i = 0; i < result.Count; i++)
                    {
                        caf.AddRange(afd.findByUserGroupAndSystem(result[i].USG_USER_GROUP, system));
                    }
                    if (caf != null && caf.Count > 0)
                    {
                        List<CPAI_MOBILE_CONTROL> cmc = new List<CPAI_MOBILE_CONTROL>();
                        string type = "";
                        if (caf.Count == 1)
                        {
                             type = caf[0].ACF_TYPE;
                             cmc = amc.findByUserGroupTypeAndSystem(user_group, system, type);
                        }
                        else
                        {
                            //for EVPC have 2 type : crude , product
                            int i = 0;
                            foreach (CPAI_ACTION_FUNCTION tem in caf)
                            {
                                i++;
                                if (caf.Count == i)
                                {
                                    type = type + tem.ACF_TYPE;
                                }
                                else
                                {
                                    type = type + tem.ACF_TYPE + "|";
                                }
                            }
                            for (int j = 0; j < result.Count; j++)
                            {
                                cmc.AddRange(amc.findByUserGroupAndSystem(result[j].USG_USER_GROUP, system));
                            }

                        }
                        if (cmc != null && cmc.Count > 0)
                        {
                            string function = string.Join("|", cmc.Select(x => x.AMC_FUN_CT));
                            string index = string.Join("|", cmc.Select(x => x.AMC_CT));
                            if (function != "" && index != "")
                            {
                                FunctionTransactionDAL fn = new FunctionTransactionDAL();
                                List<FUNCTION_TRANSACTION> tran = fn.FindByTransactionByFuncIndex(function.Split('|'), index.Split('|'),type.Split('|'));
                                //add check date from - to
                                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                                {
                                    DateTime fDate = parseInputDate(fromDate);
                                    DateTime tDate = parseInputDate(toDate);
                                    tran = tran.Where(x => x.FTX_INDEX6 != null && (parsePurchaseDate(x.FTX_INDEX6) >= fDate) && (parsePurchaseDate(x.FTX_INDEX6) <= tDate) ).ToList();
                                }

                                if(system == ConstantPrm.SYSTEM.PAF)
                                {
                                    tran = pch.FindByTransactionByFuncIndex(function.Split('|'), index.Split('|'), type.Split('|'), user_group, fromDate, toDate);
                                }

                                if (system == ConstantPrm.SYSTEM.DAF)
                                {
                                    tran = dch.FindByTransactionByFuncIndex(function.Split('|'), index.Split('|'), type.Split('|'), user_group, fromDate, toDate);
                                }


                                var temp = tran.GroupBy(x => x.FTX_FK_FUNCTION).Select(n => new
                                {
                                    FTX_FK_FUNCTION = n.Key,
                                    Count = n.Count()
                                });


                                string bodyXml = "";
                                foreach (var i in temp)
                                {

                                    string detail = (bodyXml != "" ? "," : "") + "{	\"function\":\"" + i.FTX_FK_FUNCTION + "\",	\"counter\":\"" + i.Count.ToString() + "\"   }";

                                    bodyXml += detail;
                                }
                                jsonXML = "{\"counter_txn\": [" + bodyXml + "]}";
                                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            }
                            else
                            {
                                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            }
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else
                    {
                        // action function not found
                        currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                    }
                }
                else
                {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                setExtraXml(stateModel, jsonXML);
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICounterTxnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICounterTxnState # :: Exception >>> " + ex);
                log.Error("CPAICounterTxnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
        public void setExtraXml(StateModel stateModel, string lstAllTx)
        {
            log.Info("# Start State CPAICounterTxnState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            
            DataDetail respDataDetail = new DataDetail();
            respDataDetail.Text = lstAllTx;

            // set extra xml "approve_items"
            String xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);

            string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            var newXml = oldXml + xml;

            ExtendValue respTrans = new ExtendValue();
            respTrans.value = newXml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            //ExtendValue extRowsPerPage = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extPageNumber = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extStatus = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extSystem = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extFromDate = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extToDate = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            //// set response
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAICounterTxnState >> setExtraXml # ");
        }

        public DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DateTime parsePurchaseDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = new DateTime();
                if (dateTimeString.Contains('-'))
                {
                    if (dateTimeString.Contains(':'))
                    {
                        r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (dateTimeString.Contains(':'))
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                //DateTime r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

}
