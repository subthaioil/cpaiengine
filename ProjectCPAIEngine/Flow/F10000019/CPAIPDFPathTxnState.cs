﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Web.Report;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.dal.Entity;
using System.IO;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.DALBunker;

namespace ProjectCPAIEngine.Flow.F10000019
{
    public class CPAIPDFPathTxnState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIPDFPathTxnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {

                FUNCTION_TRANSACTION tempFunc =getTransactionByID(stateModel);
                if (stateModel.BusinessModel.currentCode == ConstantRespCodeUtil.SUCCESS_RESP_CODE[0])
                {
                    string sourcePath = generatePDF(stateModel, tempFunc);
                    if (stateModel.BusinessModel.currentCode == ConstantRespCodeUtil.SUCCESS_RESP_CODE[0])
                    {
                        string destinationPath = enCryptPDF(stateModel, sourcePath);
                        if(destinationPath != "")
                        {
                            genOutPathFile(stateModel, destinationPath);
                        }
                        else
                        {
                            stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_DESTINTION_RESP_CODE;
                        }
                    }
                }
                //map response code to response description
                //stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIPDFPathTxnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIPDFPathTxnState # :: Exception >>> " + ex);
                log.Error("CPAIPDFPathTxnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public FUNCTION_TRANSACTION getTransactionByID(StateModel stateModel)
        {
            log.Info("# Start State CPAIPDFPathTxnState >> getTransactionByID #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            string tranID = etxValue.GetValue(CPAIConstantUtil.TransactionId);
            FunctionTransactionDAL fn = new FunctionTransactionDAL();
            FUNCTION_TRANSACTION func = fn.GetFnTranDetail(tranID);
            if(func.FTX_FK_FUNCTION != "" && func.FTX_INDEX2 != "")
            {
                stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
            }
            else
            {
                stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_TXN_ID_RESP_CODE;
            }
            log.Info("# End State CPAIPDFPathTxnState >> getTransactionByID # ");
            return func;
        }

        public string generatePDF(StateModel stateModel, FUNCTION_TRANSACTION func)
        {
            log.Info("# Start State CPAIPDFPathTxnState >> generatePDF #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            string tranID = etxValue.GetValue(CPAIConstantUtil.TransactionId);
            string userName = etxValue.GetValue(CPAIConstantUtil.User);
            string path = String.Empty;
            if (func.FTX_FK_FUNCTION == "1" && func.FTX_INDEX2 == "VESSEL")
            {
                BunkerPredueCMMTReport rp = new BunkerPredueCMMTReport();
                path = rp.LoadDataBunker(tranID, false, userName);
                if(path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else if (func.FTX_FK_FUNCTION == "1" && func.FTX_INDEX2 == "CRUDE")
            {
                BunkerPredueCMCSReport rp = new BunkerPredueCMCSReport();
                path = rp.LoadDataBunker(tranID, false, userName);
                if (path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else if (func.FTX_FK_FUNCTION == "6")
            {
                CharteringInReport rp = new CharteringInReport();
                path = rp.LoadDataCharter(tranID, false, userName);
                if (path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else if (func.FTX_FK_FUNCTION == "25")
            {
                CharteringInCMMTReport rp = new CharteringInCMMTReport();
                path = rp.LoadDataCharter(tranID, false, userName);
                if (path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else if (func.FTX_FK_FUNCTION == "26")
            {
                CharteringOutReport rp = new CharteringOutReport();
                path = rp.LoadDataCharter(tranID, false, userName);
                if (path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else if (func.FTX_FK_FUNCTION == "7")
            {
                CharteringOutCMMTReport rp = new CharteringOutCMMTReport();
                path = rp.LoadDataCharter(tranID, false, userName);
                if (path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else if (func.FTX_FK_FUNCTION == "23")
            {
                CrudePurchaseReport rp = new CrudePurchaseReport();
                path = rp.LoadData(tranID, false, userName);
                if (path != "")
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_PATH_RESP_CODE;
                }
            }
            else
            {
                stateModel.BusinessModel.currentCode = CPAIConstantRespCodeUtil.GEN_PDF_INVALID_FUNCTION_RESP_CODE;
            }
            log.Info("# End State CPAIPDFPathTxnState >> generatePDF #");
            return path;

        }

        public string enCryptPDF(StateModel stateModel, string sourcePath)
        {
            log.Info("# Start State CPAIPDFPathTxnState >> enCryptPDF #  ");
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFileEnc")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFileEnc"));
            }

            CSEncryptDecrypt encryptPDF = new CSEncryptDecrypt();
            ShareFn fn = new ShareFn();
            //Delete this row
            //sourcePath = fn.GetSiteRootUrl("Web/Report/TmpFile/").Replace("\\", "/") + "2016_11.pdf";
            log.Info("# Start State CPAIPDFPathTxnState >> 1 #  ");
            string[] fileName = sourcePath.Split('\\');
            string sourceFile = @"" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile") + "\\" + fileName[fileName.Length - 1];
            string descFile = @"" + Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFileEnc") + "\\" + fileName[fileName.Length - 1];
            log.Info("# Start State CPAIPDFPathTxnState >> 2 #  ");
            string sourceDestination = fn.GetSiteRootUrl("Web/Report/TmpFileEnc/").Replace("\\", "/") + fileName[fileName.Length-1];
            //string sourceDestination = fileName[fileName.Length - 1]; 
            log.Info("# Start State CPAIPDFPathTxnState >> 3 #  ");
            string pdfAseKey = MasterData.GetJsonMasterSetting("PDF_ASE_KEY");
            encryptPDF.Encrypt(sourceFile, descFile, pdfAseKey);

            
            if (Directory.Exists(Path.GetDirectoryName(sourceFile)))
            {
                File.Delete(sourceFile);
            }
            log.Info("# End State CPAIPDFPathTxnState >> enCryptPDF # ");
            return sourceDestination;
        }

        public void genOutPathFile(StateModel stateModel,string destinationPath)
        {
            log.Info("# Start State CPAIPDFPathTxnState >> genOutPathFile #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            ExtendValue desPath = new ExtendValue
            { value = destinationPath, encryptFlag = ConstantDBUtil.NO_FLAG };
            
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PDF_Path, desPath);
          
            stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
            log.Info("# End State CPAIPDFPathTxnState >> genOutPathFile # ");
        }
        
    }

}
