﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Flow.F10000020
{
    public class CPAIListMasterDataTxnState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string pKey = etxValue.GetValue(CPAIConstantUtil.Key);
                string pSystem = etxValue.GetValue(CPAIConstantUtil.System);
                string pType = etxValue.GetValue(CPAIConstantUtil.Type);

                string json = "";

                if (stateModel.BusinessModel.currentCode == ConstantRespCodeUtil.SUCCESS_RESP_CODE[0])
                {
                    if(pKey == "VENDOR")
                    {
                        json = VendorServiceModel.getVendorDDLJson(pSystem, pType);
                    }
                    else if (pKey == "CUST")
                    {
                        json = CustomerServiceModel.getCustDDLJson(pSystem, pType);
                    }
                    else if (pKey == "MATERIALS")
                    {
                        json = MaterialsServiceModel.getMaterialsDDLJson(pSystem);
                    }
                    else if (pKey == "PORT")
                    {
                        json = PortServiceModel.getPortDDLJson(pSystem);
                    }
                    else if (pKey == "VEHICLE")
                    {
                        json = VehicleServiceModel.getVehicleDDLJson(pSystem, pType);
                    }
                    else if (pKey == "PRODUCT_BUNKER")
                    {
                        json = ProductBunkerServiceModel.GetAllProductNamesJson();
                    }
                    else if (pKey == "COOL_MATERIALS")
                    {
                        json = MaterialsServiceModel.getMaterialsDDLCoolJson();
                    }
                    else if (pKey == "ORIGIN")
                    {
                        json = MaterialsServiceModel.getMaterialsCountryDDLCoolJson();
                    }
                    setExtraXml(stateModel, json);
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                //log.Error("CPAIVerifyReqireInputState::Exception >>> ", e);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
        public void setExtraXml(StateModel stateModel, string lstAllTx)
        {
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            
            DataDetail respDataDetail = new DataDetail();
            respDataDetail.Text = lstAllTx;

            // set extra xml "approve_items"
            String xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);

            string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            var newXml = oldXml + xml;

            ExtendValue respTrans = new ExtendValue();
            respTrans.value = newXml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

        }
       
    }

}
