﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000022
{
    public class CPAIMessageControlTxnState : StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string pKey = etxValue.GetValue(CPAIConstantUtil.Key);
                string pFlag = etxValue.GetValue(CPAIConstantUtil.Set_flag);
                string pUser = etxValue.GetValue(CPAIConstantUtil.User);

                string json = "";

                if (stateModel.BusinessModel.currentCode == ConstantRespCodeUtil.SUCCESS_RESP_CODE[0])
                {
                    if (pKey == CPAIConstantUtil.MOBILE || pKey == CPAIConstantUtil.EMAIL || pKey == CPAIConstantUtil.SMS || pKey == CPAIConstantUtil.NOTI)
                    {
                        if (pFlag == CPAIConstantUtil.YES_FLAG || pFlag == CPAIConstantUtil.NO_FLAG)
                        {
                            json = UsersServiceModel.SetUserFlag(pUser, pFlag, pKey);
                            if (json == "update_failed")
                            {
                                currentCode = CPAIConstantRespCodeUtil.UPDATE_DATA_ERROR_RESP_CODE;
                            } else if (json == "user_not_found")
                            {
                                currentCode = CPAIConstantRespCodeUtil.USER_NOT_FOUND_RESP_CODE;
                            }
                        }
                    }

                    setExtraXml(stateModel, json);
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
            }
            catch (Exception ex)
            {
                //log.Error("CPAIVerifyReqireInputState::Exception >>> ", e);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
        public void setExtraXml(StateModel stateModel, string lstAllTx)
        {
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            
            DataDetail respDataDetail = new DataDetail();
            respDataDetail.Text = lstAllTx;

            // set extra xml "approve_items"
            String xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);

            string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            var newXml = oldXml + xml;

            ExtendValue respTrans = new ExtendValue();
            respTrans.value = newXml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

        }
       
    }

}
