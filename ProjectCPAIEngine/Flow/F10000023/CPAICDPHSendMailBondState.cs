﻿using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.downstream;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using DSMail.model;
using DSMail.service;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCDPH;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAICDPHSendMailBondState : BasicBean, StateFlowAction
    {

        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();

        private string CRUDE_PURCHASE_EMAIL_MSG = "CRUDE_PURCHASE_EMAIL_MSG";
        private string CRUDE_PURCHASE_EMAIL_ADDR = "CRUDE_PURCHASE_EMAIL_ADDR";

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICDHSendMailBondState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);

                string msgJson = JSONSetting.getGlobalConfig(CRUDE_PURCHASE_EMAIL_MSG);

                findUserAndSendMail(stateModel, msgJson, system);

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICDHSendMailBondState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICDHSendMailBondState # :: Exception >>> " + ex);
                log.Error("CPAICDHSendMailBondState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendMail(StateModel stateModel, string actionMessage, string system)
        {
            string gc = JSONSetting.getGlobalConfig(CRUDE_PURCHASE_EMAIL_ADDR);

            List<string> lstEmailAddrs = new JavaScriptSerializer().Deserialize<List<string>>(gc).ToList();
            prepareSendMail(stateModel, actionMessage, lstEmailAddrs);
        }
        public void prepareSendMail(StateModel stateModel, string actionMessage, List<string> emailAddresses)
        {

            string jsonText = Regex.Replace(actionMessage, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            // Do replacement here
            string subject = detail.subject;
            string body = detail.body;
            //

            if (subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            //get create by
            string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            List<USERS> create_by = userMan.findByLogin(tem);
            string create_by_v = "";
            if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            {
                //string last_name = create_by[0].USR_LAST_NAME_EN != null ? create_by[0].USR_LAST_NAME_EN  : "";
                //create_by_v = string.Format("{0} {1}", create_by[0].USR_FIRST_NAME_EN, last_name);
                create_by_v = create_by[0].USR_FIRST_NAME_EN;
            }
            if (body.Contains("#create_by"))
            {
                body = body.Replace("#create_by", create_by_v);
            }
            //crude
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000023))
            {
                body = mms.getBodyF10000023(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }
            //
            sendMail(stateModel, emailAddresses, subject, body);

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body)
        {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            currentCode = downResp.getResultCode();
        }

        //string currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
}
}