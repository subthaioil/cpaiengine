﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCDPH;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAICDPHSetBondStatusState: BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICDPHSetBondStatusState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                string item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }
                log.Debug("DataDetailInput : " + item);
                string nextStatus = CPAIConstantUtil.STATUS_WAITING_BOND_DOC;

                if (item != null && !string.IsNullOrEmpty(item))
                {
                    CdpRootObject dataDetail = JSonConvertUtil.jsonToModel<CdpRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = CPAIConstantUtil.ACTION_REQUEST_BOND;
                    string tUser = etxValue.GetValue(CPAIConstantUtil.User);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB

                    bool isRunDB = false;
                    bool hasBond = false;
                    if (etxValue.GetValue(CPAIConstantUtil.Action).Equals(CPAIConstantUtil.ACTION_APPROVE_4)&& !string.IsNullOrEmpty(dataDetail.proposal.performance_bond))
                    {
                        hasBond = true;
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, currentAction, tUser);
                    }else
                    {
                        hasBond = false;
                    }
                    #endregion

                    #region INSERT JSON

                    if (isRunDB)
                    {
                        if (hasBond)
                        {
                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        }
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICDPHSetBondStatusState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                log.Error("# Error CPAICDPHSetBondStatusState # :: Exception >>> ", ex);
                log.Error("CPAICDPHSetBondStatusState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private bool UpdateDB(string pTransId, string pNextStatus, string pCurrentAction, string pUser)
        {
            bool rslt = false;
            try
            {
                DateTime dtNow = DateTime.Now;
                using (var tContext = new EntityCPAIEngine())
                {
                    using (var tDbTran = tContext.Database.BeginTransaction())
                    {
                        //*** Updating status
                        UpdateStatus(pTransId, pNextStatus, pCurrentAction,pUser,tContext);

                        tDbTran.Commit();
                        rslt = true;
                    }
                }
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

        private void UpdateStatus(string pTransId, string pNextStatus, string pCurrentAction,string pUser,EntityCPAIEngine pContext) {
            DateTime dtNow = DateTime.Now;
            CDP_DATA_DAL dataDAL = new CDP_DATA_DAL();
            CDP_DATA data = new CDP_DATA();
            data = dataDAL.GetByID(pTransId);
            data.CDA_STATUS = pNextStatus;
            data.CDA_UPDATED = dtNow;
            data.CDA_UPDATED_BY = pUser;
            dataDAL.Update(data, pContext);
        }
    }
}