﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALCDPH;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAICallVCoolState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICallVCoolState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                CdpRootObject dataDetail = JSonConvertUtil.jsonToModel<CdpRootObject>(item);
                if (dataDetail.feedstock == "CRUDE" && dataDetail.term == "Term")
                {
                    CDP_DATA_DAL CrudeDataDAL = new CDP_DATA_DAL();
                    CDP_DATA CrudeData = CrudeDataDAL.GetByID(stateModel.EngineModel.ftxTransId);

                    USERS_DAL UserDAL = new USERS_DAL();
                    ROLE role = UserDAL.GetUserRoleByUserName(CrudeData.CDA_CREATED_BY);

                    VcoRootObject vco = new VcoRootObject();
                    vco.requester_info = new VcoRequesterInfo();
                    vco.requester_info.area_unit = role.ROL_NAME;
                    vco.requester_info.date_purchase = String.Format("{0:" + VCoolViewModel.FORMAT_DATETIME + "}", DateTime.Now);
                    vco.requester_info.name = CrudeData.CDA_CREATED_BY;
                    vco.requester_info.workflow_priority = "Medium";
                    vco.requester_info.workflow_status = CPAIConstantUtil.STATUS_WAITING_APPROVE_RUN_LP;

                    vco.crude_info = new VcoCrudeInfo();
                    vco.crude_info.crude_name = !string.IsNullOrEmpty(dataDetail.crude_name_others) ? dataDetail.crude_name_others : dataDetail.crude_name;
                    vco.crude_info.crude_ref = vco.crude_info.crude_name;
                    vco.crude_info.origin = dataDetail.origin;
                    vco.crude_info.purchased_by = "P";
                    vco.crude_info.purchase_date = DateTime.ParseExact(dataDetail.date_purchase, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy");
                    vco.crude_info.tpc_plan_month = dataDetail.plan_month;
                    vco.crude_info.tpc_plan_year = dataDetail.plan_year;
                    vco.crude_info.purchase_type = dataDetail.term;
                    vco.crude_info.purchase_method = dataDetail.purchase;
                    vco.crude_info.supplier = dataDetail.offers_items.Where(x => x.final_flag == "Y").Select(y => y.supplier).FirstOrDefault();
                    if (dataDetail.loading_period != null)
                    {
                        vco.crude_info.loading_date_from = string.IsNullOrEmpty(dataDetail.loading_period.date_from) ? null : DateTime.ParseExact(dataDetail.loading_period.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy");
                        vco.crude_info.loading_date_to = string.IsNullOrEmpty(dataDetail.loading_period.date_to) ? null : DateTime.ParseExact(dataDetail.loading_period.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy");
                    }

                    CdpOffersItem offer_item = dataDetail.offers_items.Where(x => x.final_flag == "Y").FirstOrDefault();
                    if (dataDetail.discharging_period != null)
                    {
                        vco.crude_info.discharging_date_from = string.IsNullOrEmpty(dataDetail.discharging_period.date_from) ? null : DateTime.ParseExact(dataDetail.discharging_period.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy");
                        vco.crude_info.discharging_date_to = string.IsNullOrEmpty(dataDetail.discharging_period.date_to) ? null : DateTime.ParseExact(dataDetail.discharging_period.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd-MMM-yyyy");
                    }

                    if (offer_item != null)
                    {
                        vco.crude_info.benchmark_price = offer_item.bechmark_price;

                        vco.crude_info.premium = offer_item.round_items.Where(x => x.round_value != null).OrderByDescending(x => x.round_no).FirstOrDefault() == null ? "" : offer_item.round_items.Where(x => x.round_value != null).OrderByDescending(x => x.round_no).FirstOrDefault().round_value[0];
                        vco.crude_info.incoterm = offer_item.incoterms;
                        vco.crude_info.benchmark_price = offer_item.bechmark_price;
                        vco.crude_info.formula_price = offer_item.bechmark_price + "+" + vco.crude_info.premium + " " + offer_item.incoterms;
                        if (offer_item.quantity_unit == "KBBL")
                        {
                            vco.crude_info.quantity_kbbl_max = offer_item.quantity_max;
                            vco.crude_info.quantity_kt_max = String.Format("{0:#,###.####}", double.Parse(offer_item.quantity_max) * 0.13);
                        }
                        else
                        {
                            vco.crude_info.quantity_kt_max = offer_item.quantity_max;
                            vco.crude_info.quantity_kbbl_max = String.Format("{0:#,###.####}", double.Parse(offer_item.quantity_max) / 0.13);
                        }
                    }

                    vco.comment = new VcoComment();
                    vco.comment.reason_for_purchasing = dataDetail.explanation;
                    vco.comment.lp_run_note = dataDetail.notes;
                    vco.comment.evpc = dataDetail.reason;

                    var json = new JavaScriptSerializer().Serialize(vco);

                    //call function F10000063 : APPROVE_1
                    RequestCPAI req = new RequestCPAI();
                    req.Function_id = ConstantPrm.FUNCTION.F10000063;
                    req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                    req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                    req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                    req.State_name = "";
                    req.Req_parameters = new Req_parameters();
                    req.Req_parameters.P = new List<P>();
                    req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                    req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_1 });
                    req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_RUN_LP });
                    req.Req_parameters.P.Add(new P { K = "user", V = user });
                    req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                    req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                    req.Req_parameters.P.Add(new P { K = "note", V = stateModel.EngineModel.ftxTransId });
                    req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                    req.Extra_xml = "";

                    ResponseData resData = new ResponseData();
                    RequestData reqData = new RequestData();
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                    var xml = ShareFunction.XMLSerialize(req);
                    reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                    resData = service.CallService(reqData);
                    log.Debug(" resp function F10000063 >> " + resData.result_code);

                    currentCode = resData.result_code;
                }                

                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICallVCoolState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICallVCoolState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}