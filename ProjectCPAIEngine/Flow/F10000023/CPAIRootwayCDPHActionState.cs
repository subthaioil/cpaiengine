﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAIRootwayCDPHActionState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIRootwayCDPHActionState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);

                if (action.Equals(CPAIConstantUtil.ACTION_DRAFT))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_DRAFT_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_CANCEL))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_CANCEL_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_SH_APPROVE_RESP_CODE;
                }
                else if (action.Contains(CPAIConstantUtil.ACTION_APPROVE))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_REJECT))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_REJECT_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_UPLOAD) || action.Equals(CPAIConstantUtil.ACTION_WAIVE))
                {
                    currentCode = CPAIConstantRespCodeUtil.CRUDE_PURCHASE_BOND_STATUS_UPDATE_RESP_CODE;
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAIRootwayCDPHActionState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                log.Info("# Error CPAIRootwayCDPHActionState # :: Exception >>> " + ex);
                log.Error("CPAIRootwayCDPHActionState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}