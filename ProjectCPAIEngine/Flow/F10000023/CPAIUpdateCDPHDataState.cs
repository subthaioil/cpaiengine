﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCDPH;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.DALVCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAIUpdateCDPHDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIUpdateCDPHDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                string item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }
                log.Debug("DataDetailInput : " + item);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null && !string.IsNullOrEmpty(item))
                {
                    CdpRootObject dataDetail = JSonConvertUtil.jsonToModel<CdpRootObject>(item);

                    //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT))
                    //{
                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    dataDetail.reason = note;

                    bool isRunDB = false;
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) ||
                        (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2) && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                        (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isRunDB = DeleteAndInsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                    {
                        //Verify can edit >> explanation, note
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.explanation, dataDetail.notes);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_UPLOAD)
                        || currentAction.Equals(CPAIConstantUtil.ACTION_WAIVE))
                    {
                        var tBondFiles = dataDetail.bond_documents.Split('|');
                        isRunDB = UpdateStatus_Bond(stateModel.EngineModel.ftxTransId, nextStatus, currentAction, tBondFiles, etxValue.GetValue(CPAIConstantUtil.User));
                    }
                    else
                    {
                        //OTHER ==> UPDATE
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.explanation, dataDetail.notes);
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        {
                            //OTHER ==> UPDATE
                            //isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note);
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.explanation, dataDetail.notes);
                            #region Set Reject flag
                            //if current action is reject clear reject flag in table data history
                            if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                            }
                            #endregion
                        }
                        #endregion
                    }
                    #endregion

                    #region INSERT JSON

                    if (isRunDB)
                    {
                        //set data detail (json when action = approve_1 or DRAFT only)
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) ||
                            (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2) && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                            currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                        {

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            //set data
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = dataDetail.date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.FeedStock, new ExtendValue { value = dataDetail.feedstock, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber), encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.CreateBy, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.User), encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = dataDetail.crude_id, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.crude_name_others, new ExtendValue { value = dataDetail.crude_name_others, encryptFlag = "N" });
                            //set offersItem
                            if (dataDetail.offers_items != null)
                            {
                                //foreach (CdpOffersItem of in dataDetail.offers_items)
                                //{
                                //    if (!string.IsNullOrEmpty(of.final_flag) && of.final_flag.Equals("Y"))
                                //    {
                                //        etxValue.SetValue(CPAIConstantUtil.Volumes, new ExtendValue { value = of.quantity, encryptFlag = "N" });
                                //        etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = of.supplier, encryptFlag = "N" });
                                //        etxValue.SetValue(CPAIConstantUtil.Margin, new ExtendValue { value = of.margin_lp, encryptFlag = "N" });
                                //        break;
                                //    }
                                //}
                                CdpOffersItem of = dataDetail.offers_items.Where(x => x.final_flag == "Y").FirstOrDefault();
                                if (of != null)
                                {
                                    //
                                    string supplier = string.Empty;
                                    string volume = string.Empty;
                                    string incoterm = string.Empty;
                                    string purchas_p = string.Empty;
                                    string market_p = string.Empty;
                                    string lp_p = string.Empty;
                                    string ben_p = string.Empty;
                                    string bechmark_price_p = string.Empty;
                                    supplier = of.supplier != null ? of.supplier : "";
                                    if (of.quantity != null && of.quantity_max != null && of.quantity.Equals(of.quantity_max))
                                    {
                                        volume = of.quantity + " " + of.quantity_unit;
                                    }
                                    else
                                    {
                                        volume = of.quantity + " - " + of.quantity_max + " " + of.quantity_unit;
                                    }
                                    incoterm = of.incoterms != null ? of.incoterms : "";
                                    //
                                    //double t1 = !string.IsNullOrEmpty(of.market_source) ? double.Parse(of.market_source) : 0;
                                    //double t2 = !string.IsNullOrEmpty(of.market_source) ? double.Parse(of.latest_lp_plan_price) : 0;
                                    //
                                    bechmark_price_p = of.bechmark_price;
                                    string magin_unit = of.margin_unit;
                                    //market_p = bechmark_price_p + ((t1 > 0) ? "+" + String.Format("{0:n}", t1) : String.Format("{0:n}", t1)) + " " + magin_unit;
                                    //lp_p = bechmark_price_p + ((t2 > 0) ? "+" + String.Format("{0:n}", t2) : String.Format("{0:n}", t2)) + " " + magin_unit;

                                    //edit 7/6/2017 by pan : LR,VGO
                                    /*
                                    if (!String.IsNullOrEmpty(of.market_source))
                                    {
                                        var t1 = !String.IsNullOrEmpty(of.market_source) ? double.Parse(of.market_source) : 0;
                                        var t2 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(of.latest_lp_plan_price) ? double.Parse(of.latest_lp_plan_price) : 0) : 0;
                                        market_p = bechmark_price_p + ((t1 > 0) ? "+" + String.Format("{0:n}", t1) : String.Format("{0:n}", t1)) + " " + magin_unit;
                                        lp_p = bechmark_price_p + ((t2 > 0) ? "+" + String.Format("{0:n}", t2) : String.Format("{0:n}", t2)) + " " + magin_unit;
                                    }
                                    else
                                    {
                                        var t1 = !String.IsNullOrEmpty(of.market_source) ? of.market_source : "";
                                        var t2 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(of.latest_lp_plan_price) ? of.latest_lp_plan_price : "") : "";
                                        market_p = bechmark_price_p + " " + t1 + " " + magin_unit;
                                        lp_p = bechmark_price_p + " " + t2 + " " + magin_unit;
                                    }
                                    */
                                    var t1 = !String.IsNullOrEmpty(of.market_source) ? of.market_source : "";
                                    var t2 = !String.IsNullOrEmpty(of.latest_lp_plan_price) ? of.latest_lp_plan_price : "";
                                    market_p = !String.IsNullOrEmpty(t1) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t1))) + " " + magin_unit : "n/a";
                                    lp_p = !String.IsNullOrEmpty(t2) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t2))) + " " + magin_unit : "n/a";


                                    //market_p = bechmark_price_p + ((t1 > 0) ? "+" + String.Format("{0:n}", t1) : String.Format("{0:n}", t1)) + " " + CPAIConstantUtil.USD_bbl;
                                    //lp_p = bechmark_price_p + ((t2 > 0) ? "+" + String.Format("{0:n}", t2) : String.Format("{0:n}", t2)) + " " + CPAIConstantUtil.USD_bbl;

                                    //market_p = CPAIConstantUtil.OSP + ((of.market_source != null && Int32.Parse(of.market_source) > 0) ? "+" + of.market_source : of.market_source) + " " + CPAIConstantUtil.USD_bbl;
                                    //lp_p = CPAIConstantUtil.OSP + ((of.latest_lp_plan_price != null && Int32.Parse(of.latest_lp_plan_price) > 0) ? "+" + of.latest_lp_plan_price : of.latest_lp_plan_price) + " " + CPAIConstantUtil.USD_bbl;
                                    //round max
                                    if (of.round_items != null && of.round_items.Count > 0)
                                    {
                                        foreach (CdpRoundItem rt in of.round_items)
                                        {
                                            int no_before = 0;
                                            int no = 0;
                                            if (Int32.TryParse(rt.round_no, out no))
                                            {
                                                if (no > no_before)
                                                {
                                                    if (!string.IsNullOrEmpty(rt.round_value[0]))
                                                    {
                                                        //edit 7/6/2017 by pan : LR,VGO
                                                        var t3 = !String.IsNullOrEmpty(rt.round_value[0]) ? rt.round_value[0] : "";
                                                        purchas_p = !String.IsNullOrEmpty(t3) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t3))) + " " + magin_unit : "n/a";
                                                        /*
                                                        if (!String.IsNullOrEmpty(of.market_source))
                                                        {
                                                            var t3 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(rt.round_value[0]) ? double.Parse(rt.round_value[0]) : 0) : 0;
                                                            purchas_p = bechmark_price_p + ((t3 > 0) ? "+" + String.Format("{0:n}", t3) : String.Format("{0:n}", t3)) + " " + magin_unit;
                                                        }
                                                        else
                                                        {
                                                            var t3 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(rt.round_value[0]) ? rt.round_value[0] : "") : "";
                                                            purchas_p = bechmark_price_p + " " + t3 + " " + magin_unit;

                                                        }
                                                        */

                                                        //double t3 = !string.IsNullOrEmpty(of.market_source) ? double.Parse(rt.round_value[0]) : 0;
                                                        //purchas_p = bechmark_price_p + ((t3 > 0) ? "+" + String.Format("{0:n}", t3) : String.Format("{0:n}", t3)) + " " + magin_unit;
                                                        //purchas_p = bechmark_price_p + ((t3 > 0) ? "+" + String.Format("{0:n}", t3) : String.Format("{0:n}", t3)) + " " + CPAIConstantUtil.USD_bbl;
                                                        //purchas_p = CPAIConstantUtil.OSP + ((Int32.Parse(rt.round_value[0]) > 0) ? "+" + rt.round_value[0] : rt.round_value[0]) + " " + CPAIConstantUtil.USD_bbl;
                                                        Int32.TryParse(rt.round_no, out no_before);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //ben_p = dataDetail.proposal.reason_ref2_bbl + " " + CPAIConstantUtil.USD_bbl + " or " + dataDetail.proposal.reason_ref2_us + " " + CPAIConstantUtil.currency_symbol; //**0.13 USD/bbl or 65,000 USD

                                    ben_p = dataDetail.proposal.reason_ref2_bbl + " " + magin_unit + " or " + dataDetail.proposal.reason_ref2_us + " " + CPAIConstantUtil.currency_symbol; //**0.13 USD/bbl or 65,000 USD

                                    etxValue.SetValue(CPAIConstantUtil.Volumes, new ExtendValue { value = volume, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = supplier, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Margin, new ExtendValue { value = ben_p, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.lp_p, new ExtendValue { value = lp_p, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.incoterm, new ExtendValue { value = incoterm, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.purchase_p, new ExtendValue { value = purchas_p, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.market_p, new ExtendValue { value = market_p, encryptFlag = "N" });
                                }
                                else
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Volumes, new ExtendValue { value = "", encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = "", encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Margin, new ExtendValue { value = "", encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.lp_p, new ExtendValue { value = "", encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.incoterm, new ExtendValue { value = "", encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.purchase_p, new ExtendValue { value = "", encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.market_p, new ExtendValue { value = "", encryptFlag = "N" });
                                }
                            }
                        }
                        else if (!currentAction.Equals(CPAIConstantUtil.ACTION_UPLOAD) && !currentAction.Equals(CPAIConstantUtil.ACTION_WAIVE))
                        {
                            CdpRootObject dataDetail_temp = JSonConvertUtil.jsonToModel<CdpRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                            dataDetail_temp.reason = note;
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail_temp);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        }
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                    //}
                    //else
                    //{
                    //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                    //}
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIUpdateCDPHDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                log.Error("# Error CPAIUpdateCDPHDataState # :: Exception >>> ", ex);
                log.Error("CPAIUpdateCDPHDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool DeleteAndInsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, CdpRootObject dataDetail)
        {
            log.Info("# Start State CPAIUpdateCDPHDataState >> DeleteAndInsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            VCO_DATA_DAL vcoDal = new VCO_DATA_DAL();
                            vcoDal.RemoveCrudePurchaseFK(TransID, context);


                            CDO_OPTIMIZATION_DETAIL data_OP_DETAIL_DELETE = new CDO_OPTIMIZATION_DETAIL();
                            try
                            {
                                context.CDO_OPTIMIZATION_DETAIL.RemoveRange(context.CDO_OPTIMIZATION_DETAIL.Where(x => x.COO_FK_CDP_DATA == TransID));
                                context.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw ex;
                            }

                            CDP_DATA_DAL dataDAL = new CDP_DATA_DAL();
                            bool isDeleted = dataDAL.DeleteByTransactionId(TransID, context);
                            if (isDeleted == false)
                            {
                                dbContextTransaction.Rollback();
                                isSuccess = false;
                                log.Info("# End State CPAIUpdateCDPHDataState >> DeleteAndInsertDB # ");
                                return isSuccess;
                            }

                            #region CDP_DATA

                            CDP_DATA data = new CDP_DATA();
                            data.CDA_ROW_ID = TransID;
                            data.CDA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            if (!string.IsNullOrEmpty(dataDetail.date_purchase))
                            {
                                data.CDA_PURCHASE_DATE = DateTime.ParseExact(dataDetail.date_purchase, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }


                            data.CDA_TPC_PLAN_MONTH = dataDetail.plan_month;
                            data.CDA_TPC_PLAN_YEAR = dataDetail.plan_year;

                            data.CDA_FEED_STOCK = dataDetail.feedstock;
                            data.CDA_FEED_STOCK_OTHERS = dataDetail.feedstock_others;
                            data.CDA_FOR_FEED_STOCK = dataDetail.for_feedStock;


                            data.CDA_FK_MATERIALS = string.IsNullOrEmpty(dataDetail.crude_name_others) == true ? dataDetail.crude_id : "";
                            data.CDA_MATERIALS_OTHERS = string.IsNullOrEmpty(dataDetail.crude_name_others) == false ? dataDetail.crude_name_others : "";

                            data.CDA_ORIGIN = dataDetail.origin_id;

                            data.CDA_TERMS = dataDetail.term;
                            data.CDA_TERMS_OTHERS = dataDetail.term_others;

                            data.CDA_PURCHASE = dataDetail.purchase;
                            data.CDA_SUPPLIER_SOURCE = dataDetail.supply_source;
                            data.CDA_EXPLANATION = dataDetail.explanation;

                            ShareFn fn = new ShareFn();
                            if (!string.IsNullOrEmpty(dataDetail.contract_period.date_from) && !string.IsNullOrEmpty(dataDetail.contract_period.date_to))
                            {
                                data.CDA_CONTRACT_DATE_FROM = DateTime.ParseExact(dataDetail.contract_period.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                data.CDA_CONTRACT_DATE_TO = DateTime.ParseExact(dataDetail.contract_period.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            if (!string.IsNullOrEmpty(dataDetail.loading_period.date_from) && !string.IsNullOrEmpty(dataDetail.loading_period.date_to))
                            {
                                data.CDA_LOADING_DATE_FROM = DateTime.ParseExact(dataDetail.loading_period.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                data.CDA_LOADING_DATE_TO = DateTime.ParseExact(dataDetail.loading_period.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            if (!string.IsNullOrEmpty(dataDetail.discharging_period.date_from) && !string.IsNullOrEmpty(dataDetail.discharging_period.date_to))
                            {
                                data.CDA_DISCHARGING_DATE_FROM = DateTime.ParseExact(dataDetail.discharging_period.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                data.CDA_DISCHARGING_DATE_TO = DateTime.ParseExact(dataDetail.discharging_period.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            if (!string.IsNullOrEmpty(dataDetail.proposal.price_period_from) && !string.IsNullOrEmpty(dataDetail.proposal.price_period_to))
                            {
                                data.CDA_PRICING_PERIOD_FROM = DateTime.ParseExact(dataDetail.proposal.price_period_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                data.CDA_PRICING_PERIOD_TO = DateTime.ParseExact(dataDetail.proposal.price_period_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }

                            data.CDA_GTC = dataDetail.gtc;

                            data.CDA_OTHER_CONDITION = dataDetail.other_condition;

                            data.CDA_PAYMENT_TERMS = dataDetail.payment_terms.payment_term;
                            data.CDA_PAYMENT_TERMS_DETAIL = dataDetail.payment_terms.payment_term_detail;
                            data.CDA_SUB_PAYMENT_TERMS = dataDetail.payment_terms.sub_payment_term;
                            data.CDA_PAYMENT_TERMS_OTHERS = dataDetail.payment_terms.payment_term_others;

                            data.CDA_PROPOSAL_AWARD_TO = dataDetail.proposal.award_to;
                            data.CDA_PROPOSAL_REF1_BBL = dataDetail.proposal.reason_ref1_bbl;
                            data.CDA_PROPOSAL_REF1_US = dataDetail.proposal.reason_ref1_us;
                            data.CDA_PROPOSAL_REF2_BBL = dataDetail.proposal.reason_ref2_bbl;
                            data.CDA_PROPOSAL_REF2_US = dataDetail.proposal.reason_ref2_us;
                            data.CDA_PROPOSAL_REASON = dataDetail.proposal.reason;
                            data.CDA_PROPOSAL_REASON_OTHERS = dataDetail.proposal.reason_others;
                            data.CDA_PROPOSAL_SUPPLIER = dataDetail.proposal.supplier;
                            data.CDA_PERFORMANCE_BOND = dataDetail.proposal.performance_bond;
                            data.CDA_VOLUME = dataDetail.proposal.volume;
                            data.CDA_TOLERANCE = dataDetail.proposal.tolerance;
                            data.CDA_TOLERANCE_OPTION = dataDetail.proposal.tolerance_option;
                            data.CDA_TOLERANCE_TYPE = dataDetail.proposal.tolerance_type;

                            data.CDA_NOTE = dataDetail.notes;
                            data.CDA_REQUESTED_DATE = dtNow;
                            data.CDA_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.CDA_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                            data.CDA_REASON = dataDetail.reason;

                            data.CDA_OFFERED_BY_PTT = dataDetail.offered_by_ptt;
                            data.CDA_REASON_OFFER = dataDetail.reasonPTToffer;
                            data.CDA_OFFER_VIA = dataDetail.offerVia;
                            data.CDA_REASON_VIA = dataDetail.reasonOfferVia;
                            data.CDA_OFFER_DATE = dataDetail.offerDate;

                            data.CDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.CDA_CREATED = dtNow;
                            data.CDA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.CDA_UPDATED = dtNow;
                            data.CDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                            data.CDA_OPTIMIZATION = dataDetail.CrudeSale_Optimization;

                            dataDAL.Save(data, context);
                            #endregion

                            #region CDO_DATA
                            if(dataDetail.CrudeSale_Optimization == "Y")
                            {
                                CDO_OPTIMIZATION_DETAIL data_OP_DETAIL = new CDO_OPTIMIZATION_DETAIL();
                                //data_OP_DETAIL.COO_ROW_ID = "123";
                                data_OP_DETAIL.COO_ROW_ID = PAFHelper.GetSeqId("CDO_OPTIMIZATION_DETAIL");
                                data_OP_DETAIL.COO_FK_CDO_DATA = dataDetail.CrudeSale_Ref; //CDA_ROW_ID in View
                                                                                           //data_OP_DETAIL.COO_FK_CDO_DATA = "1000";
                                data_OP_DETAIL.COO_FK_CDP_DATA = data.CDA_ROW_ID;
                                data_OP_DETAIL.COO_CREATED = data.CDA_CREATED;
                                data_OP_DETAIL.COO_CREATED_BY = data.CDA_CREATED_BY;
                                data_OP_DETAIL.COO_UPDATED = data.CDA_UPDATED;
                                data_OP_DETAIL.COO_UPDATED_BY = data.CDA_UPDATED_BY;
                                try
                                {
                                    context.CDO_OPTIMIZATION_DETAIL.Add(data_OP_DETAIL);
                                    context.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                            

                            #endregion

                            #region CDP_OFFER_ITEMS

                            CDP_OFFER_ITEMS_DAL dalOfferItems = new CDP_OFFER_ITEMS_DAL();
                            CDP_OFFER_ITEMS dataOfferItems;
                            string rowIDOffer = string.Empty;
                            string rowIDRoundItems = string.Empty;

                            if (dataDetail.offers_items != null)
                            {
                                foreach (var item in dataDetail.offers_items)
                                {
                                    rowIDOffer = Guid.NewGuid().ToString("N");
                                    dataOfferItems = new CDP_OFFER_ITEMS();
                                    dataOfferItems.COI_ROW_ID = rowIDOffer;
                                    dataOfferItems.COI_FK_CDP_DATA = TransID;
                                    dataOfferItems.COI_FK_VENDOR = item.supplier;

                                    dataOfferItems.COI_CONTACT_PERSON = item.contact_person;
                                    dataOfferItems.COI_QUANTITY = item.quantity;
                                    dataOfferItems.COI_QUANTITY_MAX = item.quantity_max;
                                    dataOfferItems.COI_QUANTITY_UNIT = item.quantity_unit;
                                    dataOfferItems.COI_INCOTERMS = item.incoterms;
                                    dataOfferItems.COI_MARKET_SOURCE = item.market_source;

                                    dataOfferItems.COI_LATEST_LP_PLAN_PRICE = item.latest_lp_plan_price;
                                    dataOfferItems.COI_BECHMARK_PRICE = item.bechmark_price;
                                    dataOfferItems.COI_RANK_ROUND = item.rank_round;
                                    dataOfferItems.COI_MARGIN_LP = item.margin_lp;
                                    dataOfferItems.COI_FINAL_FLAG = item.final_flag;

                                    dataOfferItems.COI_CREATED = dtNow;
                                    dataOfferItems.COI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataOfferItems.COI_UPDATED = dtNow;
                                    dataOfferItems.COI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dalOfferItems.Save(dataOfferItems, context);

                                    #region CDP_ROUND_ITEMS
                                    CDP_ROUND_ITEMS_DAL dalRoundItems = new CDP_ROUND_ITEMS_DAL();
                                    CDP_ROUND_ITEMS dataRoundItems;
                                    foreach (var itemRoundItems in item.round_items)
                                    {
                                        rowIDRoundItems = Guid.NewGuid().ToString("N");
                                        dataRoundItems = new CDP_ROUND_ITEMS();
                                        dataRoundItems.CRI_ROW_ID = rowIDRoundItems;
                                        dataRoundItems.CRI_FK_OFFER_ITEMS = rowIDOffer;
                                        dataRoundItems.CRI_ROUND_NO = itemRoundItems.round_no;
                                        dataRoundItems.CRI_CREATED = dtNow;
                                        dataRoundItems.CRI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataRoundItems.CRI_UPDATED = dtNow;
                                        dataRoundItems.CRI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                        dalRoundItems.Save(dataRoundItems, context);

                                        #region CDP_ROUND

                                        CDP_ROUND_DAL dalRound = new CDP_ROUND_DAL();
                                        CDP_ROUND dataRound;
                                        int order = 1;
                                        foreach (var round in itemRoundItems.round_value)
                                        {
                                            dataRound = new CDP_ROUND();
                                            dataRound.CRD_ROW_ID = Guid.NewGuid().ToString("N");
                                            dataRound.CRD_FK_ROUND_ITEMS = rowIDRoundItems;
                                            dataRound.CRD_ROUND_ORDER = order.ToString();
                                            dataRound.CRD_ROUND_VALUE = round;

                                            dataRound.CRD_CREATED = dtNow;
                                            dataRound.CRD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataRound.CRD_UPDATED = dtNow;
                                            dataRound.CRD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                            dalRound.Save(dataRound, context);

                                            order++;
                                        }


                                        #endregion
                                    }
                                    #endregion
                                }
                            }

                            #endregion

                            #region CDP_OFFER_COMPET_ITEMS

                            if (dataDetail.compet_offers_items != null)
                            {
                                CDP_OFFER_COMPET_ITEMS_DAL dalOfferCompetItems = new CDP_OFFER_COMPET_ITEMS_DAL();
                                CDP_OFFER_COMPET_ITEMS dataOfferCompetItems;
                                string rowIDOfferCompet = string.Empty;
                                string rowIDRoundCompetItems = string.Empty;
                                foreach (var item in dataDetail.compet_offers_items)
                                {
                                    rowIDOfferCompet = Guid.NewGuid().ToString("N");
                                    dataOfferCompetItems = new CDP_OFFER_COMPET_ITEMS();
                                    dataOfferCompetItems.CCI_ROW_ID = rowIDOfferCompet;
                                    dataOfferCompetItems.CCI_FK_CDP_DATA = TransID;
                                    dataOfferCompetItems.CCI_FK_MATERIALS = item.crude_id;
                                    dataOfferCompetItems.CCI_FK_VENDOR = item.supplier;
                                    dataOfferCompetItems.CCI_CRUDE_NAME_OTHERS = item.crude_name_others;

                                    dataOfferCompetItems.CCI_CONTACT_PERSON = item.contact_person;
                                    dataOfferCompetItems.CCI_QUANTITY = item.quantity;
                                    dataOfferCompetItems.CCI_QUANTITY_MAX = item.quantity_max;
                                    dataOfferCompetItems.CCI_QUANTITY_UNIT = item.quantity_unit;
                                    dataOfferCompetItems.CCI_INCOTERMS = item.incoterms;
                                    dataOfferCompetItems.CCI_MARKET_SOURCE = item.market_source;

                                    dataOfferCompetItems.CCI_LATEST_LP_PLAN_PRICE = item.latest_lp_plan_price;
                                    dataOfferCompetItems.CCI_BECHMARK_PRICE = item.bechmark_price;
                                    dataOfferCompetItems.CCI_RANK_ROUND = item.rank_round;
                                    dataOfferCompetItems.CCI_MARGIN_LP = item.margin_lp;
                                    dataOfferCompetItems.CCI_MARGIN_UNIT = item.margin_unit;

                                    dataOfferCompetItems.CCI_CREATED = dtNow;
                                    dataOfferCompetItems.CCI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataOfferCompetItems.CCI_UPDATED = dtNow;
                                    dataOfferCompetItems.CCI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dalOfferCompetItems.Save(dataOfferCompetItems, context);

                                    #region CDP_ROUND_COMPET_ITEMS
                                    CDP_ROUND_COMPET_ITEMS_DAL dalRoundCompetItems = new CDP_ROUND_COMPET_ITEMS_DAL();
                                    CDP_ROUND_COMPET_ITEMS dataRoundCompetItems;
                                    foreach (var itemRoundItems in item.round_items)
                                    {
                                        rowIDRoundCompetItems = Guid.NewGuid().ToString("N");
                                        dataRoundCompetItems = new CDP_ROUND_COMPET_ITEMS();
                                        dataRoundCompetItems.CIC_ROW_ID = rowIDRoundCompetItems;
                                        dataRoundCompetItems.CIC_FK_OFFER_COMPET_ITEMS = rowIDOfferCompet;
                                        dataRoundCompetItems.CIC_ROUND_NO = itemRoundItems.round_no;
                                        dataRoundCompetItems.CIC_CREATED = dtNow;
                                        dataRoundCompetItems.CIC_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataRoundCompetItems.CIC_UPDATED = dtNow;
                                        dataRoundCompetItems.CIC_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                        dalRoundCompetItems.Save(dataRoundCompetItems, context);

                                        #region CDP_ROUND_COMPET

                                        CDP_ROUND_COMPET_DAL dalRoundCompet = new CDP_ROUND_COMPET_DAL();
                                        CDP_ROUND_COMPET dataRoundCompet;
                                        int order = 1;
                                        foreach (var round in itemRoundItems.round_value)
                                        {
                                            dataRoundCompet = new CDP_ROUND_COMPET();
                                            dataRoundCompet.CRC_ROW_ID = Guid.NewGuid().ToString("N");
                                            dataRoundCompet.CRC_FK_ROUND_COMPET_ITEMS = rowIDRoundCompetItems;
                                            dataRoundCompet.CRC_ROUND_ORDER = order.ToString();
                                            dataRoundCompet.CRC_ROUND_VALUE = round;

                                            dataRoundCompet.CRC_CREATED = dtNow;
                                            dataRoundCompet.CRC_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataRoundCompet.CRC_UPDATED = dtNow;
                                            dataRoundCompet.CRC_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                            dalRoundCompet.Save(dataRoundCompet, context);

                                            order++;
                                        }


                                        #endregion
                                    }
                                    #endregion
                                }
                            }

                            #endregion

                            #region CDP_ATTACH_FILE

                            CDP_ATTACH_FILE_DAL dataAttachFileDAL = new CDP_ATTACH_FILE_DAL();
                            CDP_ATTACH_FILE dataCDPAF;
                            foreach (var item in dataDetail.explanationAttach.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataCDPAF = new CDP_ATTACH_FILE();
                                    dataCDPAF.CAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCDPAF.CAF_FK_CDP_DATA = TransID;
                                    dataCDPAF.CAF_PATH = text[0];
                                    dataCDPAF.CAF_INFO = text[1];
                                    dataCDPAF.CAF_TYPE = "EXT";
                                    dataCDPAF.CAF_CREATED = dtNow;
                                    dataCDPAF.CAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCDPAF.CAF_UPDATED = dtNow;
                                    dataCDPAF.CAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataCDPAF, context);
                                }
                            }

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                            if (Att != null)
                            {
                                foreach (var item in Att.attach_items)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataCDPAF = new CDP_ATTACH_FILE();
                                        dataCDPAF.CAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCDPAF.CAF_FK_CDP_DATA = TransID;
                                        dataCDPAF.CAF_PATH = item;
                                        dataCDPAF.CAF_INFO = "";
                                        dataCDPAF.CAF_TYPE = "ATT";
                                        dataCDPAF.CAF_CREATED = dtNow;
                                        dataCDPAF.CAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCDPAF.CAF_UPDATED = dtNow;
                                        dataCDPAF.CAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataCDPAF, context);
                                    }
                                }
                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                            log.Error("# Error CPAIUpdateCDPHDataState # :: Rollback >>> ", ex);
                            log.Error("CPAIUpdateCDPHDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("# Error CPAIUpdateCDPHDataState # :: Exception >>> ", e);
                log.Error("CPAIUpdateCDPHDataState::Exception >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAIUpdateCDPHDataState >> DeleteAndInsertDB # ");
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note, string explanation = null, string note_2 = null)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIUpdateCDPHDataState >> UpdateDB when verify #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CDP_DATA_DAL dataDAL = new CDP_DATA_DAL();
                            CDP_DATA data = new CDP_DATA();
                            data.CDA_ROW_ID = TransID;
                            data.CDA_REASON = note;
                            //if (explanation != null)
                            //{
                            //    data.CDA_EXPLANATION = explanation;
                            //}
                            //if (note_2 != null)
                            //{
                            //    data.CDA_NOTE = note_2;
                            //}
                            data.CDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.CDA_UPDATED = DateTime.Now;
                            data.CDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(data, context);

                            VCO_DATA_DAL vcdDal = new VCO_DATA_DAL();
                            VCO_DATA vcd = vcdDal.GetByCrudePurchaseID(TransID);

                            if (vcd != null)
                            {
                                VCO_COMMENT_DAL vccDal = new VCO_COMMENT_DAL();
                                VCO_COMMENT vcc = vccDal.GetByDataID(vcd.VCDA_ROW_ID);
                                vcc.VCCO_EVPC = etxValue.GetValue(CPAIConstantUtil.Note);
                                vccDal.Update(vcc, context);
                            }

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                            log.Error("# Error CPAIUpdateCDPHDataState # :: Rollback >>> ", ex);
                            log.Error("CPAIUpdateCDPHDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }

                    }
                }

                log.Info("# End State CPAIUpdateCDPHDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error CPAIUpdateCDPHDataState # :: UpdateDB >>> ", ex);
                log.Error("CPAIUpdateCDPHDataState::UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        private bool UpdateStatus_Bond(string pTransId, string pNextStatus, string pCurrentAction, string[] pFiles, string pUser)
        {
            bool rslt = false;
            try
            {
                DateTime dtNow = DateTime.Now;
                using (var tContext = new EntityCPAIEngine())
                {
                    using (var tDbTran = tContext.Database.BeginTransaction())
                    {
                        if (pCurrentAction.Equals("UPLOAD"))
                        {
                            //*** Insert a new record into CDP_ATTACH_FILE
                            AddBondDocuments(pTransId, pFiles, pUser, tContext);
                        }
                        //*** Updating statsu
                        SetStatus(pTransId, pNextStatus, pCurrentAction, pUser, tContext);

                        tDbTran.Commit();
                        rslt = true;
                    }
                }
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

        private void SetStatus(string pTransId, string pNextStatus, string pCurrentAction, string pUser, EntityCPAIEngine pContext)
        {
            DateTime dtNow = DateTime.Now;
            CDP_DATA_DAL dataDAL = new CDP_DATA_DAL();
            CDP_DATA data = new CDP_DATA();
            data = dataDAL.GetByID(pTransId);
            data.CDA_STATUS = pNextStatus;
            data.CDA_UPDATED = dtNow;
            data.CDA_UPDATED_BY = pUser;
            dataDAL.Update(data, pContext);
        }

        private void AddBondDocuments(string pTransId, string[] pFiles, string pUser, EntityCPAIEngine pContext)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    DateTime dtNow = DateTime.Now;
                    CDP_ATTACH_FILE_DAL afd = new CDP_ATTACH_FILE_DAL();
                    //var oldFile = context.CDP_ATTACH_FILE.Where(a => a.CAF_FK_CDP_DATA == pTransId).ToList();
                    var oldFile = context.CDP_ATTACH_FILE.Where(x => x.CAF_FK_CDP_DATA == pTransId && x.CAF_TYPE == "BND").ToList();
                    //var oldfile = context.CDP_ATTACH_FILE.Where(a => a.CAF_FK_CDP_DATA == tranId && a.CAF_PATH == filename && a.CAF_TYPE == "BND").ToList();
                    if (oldFile.Count() == 0)
                    {
                        foreach (string tFile in pFiles)
                        {
                            CDP_ATTACH_FILE af = new CDP_ATTACH_FILE();
                            af.CAF_ROW_ID = Guid.NewGuid().ToString().Replace("-", "");
                            af.CAF_FK_CDP_DATA = pTransId;
                            af.CAF_TYPE = "BND";
                            af.CAF_PATH = tFile;
                            af.CAF_CREATED = dtNow;
                            af.CAF_CREATED_BY = pUser;
                            af.CAF_UPDATED = dtNow;
                            af.CAF_UPDATED_BY = pUser;
                            afd.Save(af, pContext);
                        }
                    }
                    else
                    {
                        foreach(var item in oldFile)
                        {
                            foreach (string tFile in pFiles)
                            {
                                item.CAF_UPDATED = dtNow;
                                item.CAF_UPDATED_BY = pUser;
                                item.CAF_PATH = item.CAF_PATH + "|" + tFile;
                                afd.UpdateAttachFile(item);
                            }
                        }                        
                    }
                }                
            }
            catch (Exception ex)
            {

            }
        }
    }
}





