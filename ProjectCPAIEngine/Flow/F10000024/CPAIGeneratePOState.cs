﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;

namespace ProjectCPAIEngine.Flow.F10000024
{
    public class CPAIGeneratePOState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICounterTxnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
                string channel = etxValue.GetValue(CPAIConstantUtil.Channel);
                string system = etxValue.GetValue(CPAIConstantUtil.System);


                ///
                string strConnString = "Data Source=TSR-DCPAI-DB.thaioil.localnet:1521/CMDBDEV;User Id=CMDB;Password=cmdb;";
                string oradb = strConnString;
                OracleConnection conn = new OracleConnection(oradb);
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;

                // Perform insert using parameters (bind variables)
                DateTime x = DateTime.Now;
                cmd.CommandText = "Insert into Z_TEST VALUES ('1', '2', '" + x.ToString()+"')";

                int rowsUpdated = cmd.ExecuteNonQuery();
                conn.Dispose();
                ////

                // don't forget to perform any clean-up as necessary

                //MasterData acfMan = new MasterData();

                ////fix function code = schedule
                //List<CPAI_USER_GROUP> result = acfMan.getUserList(user, system);


                //List<Transaction> lstAllTx = new List<Transaction>();
                //if (result.Count > 0)
                //{
                //    string functionKey = "FUN_COUNTER_TXN";
                //    var cf = MasterData.GetConfigData(result, functionKey);
                //    if (cf != null && cf.Count > 0)
                //    {
                //        string function = "";
                //        string index = "";
                //        foreach (var i in cf)
                //        {
                //            if (i.GCG_KEY == functionKey)
                //            {
                //                index = i.GCG_VALUE;
                //            }
                //            else
                //            {
                //                function = i.GCG_VALUE;
                //            }
                //        }
                //        if (function != "" && index != "")
                //        {
                //            FunctionTransactionDAL fn = new FunctionTransactionDAL();
                //            List<FUNCTION_TRANSACTION> tran = fn.FindByTransactionByFuncIndex(function.Split('|'), index.Split('|'));

                //            var temp = tran.GroupBy(x => x.FTX_FK_FUNCTION).Select(n => new
                //            {
                //                FTX_FK_FUNCTION = n.Key,
                //                Count = n.Count()
                //            });


                //            string bodyXml = "";
                //            foreach (var i in temp)
                //            {

                //                string detail = (bodyXml != "" ? "," : "")+"{	\"function\":\"" + i.FTX_FK_FUNCTION + "\",	\"counter\":\""+i.Count.ToString()+"\"   }";

                //                bodyXml += detail;
                //            }

                //            string jsonXML = "";
                //            jsonXML = "{\"counter_txn\": ["+ bodyXml + "]}";
                //            setExtraXml(stateModel, jsonXML);

                //            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                //        }
                //        else
                //        {
                //            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                //        }
                //    }
                //    else
                //    {
                //        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                //    }
                //}
                //else
                //{
                //    // action function not found
                //    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                //}

                ////map response code to response description
                //stateModel.BusinessModel.currentCode = currentCode;
                //respCodeManagement.setCurrentCodeMapping(stateModel);
                //log.Info("# End State CPAICounterTxnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICounterTxnState # :: Exception >>> " + ex);
                log.Error("CPAICounterTxnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
        public void setExtraXml(StateModel stateModel, string lstAllTx)
        {
            log.Info("# Start State CPAICounterTxnState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            
            DataDetail respDataDetail = new DataDetail();
            respDataDetail.Text = lstAllTx;

            // set extra xml "approve_items"
            String xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);

            string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            var newXml = oldXml + xml;

            ExtendValue respTrans = new ExtendValue();
            respTrans.value = newXml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            //ExtendValue extRowsPerPage = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extPageNumber = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extStatus = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extSystem = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extFromDate = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            //ExtendValue extToDate = new ExtendValue
            //{ value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            //// set response
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            //etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAICounterTxnState >> setExtraXml # ");
        }
       
    }

}
