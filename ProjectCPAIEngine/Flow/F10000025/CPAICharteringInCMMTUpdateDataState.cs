﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000025
{
    public class CPAICharteringInCMMTUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICharteringInCMMTUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (stateModel.EngineModel.ftxTransId != null)
                {
                    using (var context = new EntityCPAIEngine())
                    {
                        var chitDetail = context.CHIT_DATA.Where(a => a.IDAT_ROW_ID == stateModel.EngineModel.ftxTransId).ToList();
                        foreach (var z in chitDetail)
                        {
                            etxValue.SetValue(CPAIConstantUtil.CharterFor, new ExtendValue { value = z.IDAT_CHARTER_FOR, encryptFlag = "N" });
                        }
                    }
                }
                if (item != null)
                {
                    ChitRootObject dataDetail = JSonConvertUtil.jsonToModel<ChitRootObject>(item);

                    //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) || (dataDetail.chit_proposed_for_approve.vessel_name != null
                    //    && dataDetail.chit_proposed_for_approve.owner != null
                    //    && dataDetail.chit_evaluating.date_purchase != null
                    //    && dataDetail.chit_proposed_for_approve.laycan_from != null))
                    //{
                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    dataDetail.chit_reason = note;

                    bool isRunDB = false;
                    //if ((nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)) || nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CERTIFIED))
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) || (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                    {
                        //Verify can edit >> explanation, note
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, prevStatus, etxValue, note, dataDetail.chit_detail.Reason, dataDetail.chit_note);
                    }
                    else
                    {
                        //OTHER ==> UPDATE
                        //isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note);
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, prevStatus, etxValue, note, dataDetail.chit_detail.Reason, dataDetail.chit_note);
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT) || currentAction.Equals("REJECT_CON_SH") || currentAction.Equals("REJECT_CON"))
                        {
                            if (prevStatus == "WAITING CERTIFIED" || prevStatus == "WAITING APPROVE")
                            {
                                dthMan.UpdateRejectFlag2(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                            }
                            else
                            {
                                dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                            }                            
                        }
                        #endregion
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = approve_1 or DRAFT only)

                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") || currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                        {
                            string vessel = dataDetail.chit_proposed_for_approve.vessel_name != null ? dataDetail.chit_proposed_for_approve.vessel_name : "";
                            string broker = (dataDetail.chit_negotiation_summary.chit_offers_items.Where(x => x.final_flag.ToUpper() == "Y").ToList().Count > 0) ? dataDetail.chit_negotiation_summary.chit_offers_items.Where(x => x.final_flag.ToUpper() == "Y").ToList()[0].broker : "";
                            string owner = dataDetail.chit_proposed_for_approve.broker_nameText != null ? dataDetail.chit_proposed_for_approve.broker_nameText : "";
                            string date_pn = (dataDetail.chit_detail.date != null) ? dataDetail.chit_detail.date : "";
                            string charter = (dataDetail.chit_detail.date != null) ? dataDetail.chit_detail.charterer : "";
                            //string flatrate = dataDetail.chit_evaluating.flat_rate != null ? dataDetail.chit_evaluating.flat_rate : "";
                            string laycan_from = dataDetail.chit_proposed_for_approve.laycan_from != null ? dataDetail.chit_proposed_for_approve.laycan_from : "";
                            string laycan_to = dataDetail.chit_proposed_for_approve.laycan_to != null ? dataDetail.chit_proposed_for_approve.laycan_to : "";
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            //set data
                            etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = vessel, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Broker, new ExtendValue { value = broker, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Owner, new ExtendValue { value = owner, encryptFlag = "N" });

                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_pn, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.FlatRate, new ExtendValue { value = flatrate, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Charterer, new ExtendValue { value = charter, encryptFlag = "N" });
                            string unit = "USD";
                            if (dataDetail.chit_detail != null)
                            {
                                unit = dataDetail.chit_detail.NegoUnit != null ? dataDetail.chit_detail.NegoUnit : "USD";
                            }
                            //load port ,dischart port
                            List<ChitLoadPortControl> load_port_ls = dataDetail.chit_detail.chit_load_port_control;
                            string load_port = "";
                            if (load_port_ls != null && load_port_ls.Count > 0)
                            {
                                foreach (ChitLoadPortControl clp in load_port_ls)
                                {
                                    load_port += clp.PortName + "|";
                                }
                            }
                            List<ChitDischargePortControl> discharge_port_ls = dataDetail.chit_detail.chit_discharge_port_control;
                            string dis_port = "";
                            if (discharge_port_ls != null && discharge_port_ls.Count > 0)
                            {
                                foreach (ChitDischargePortControl clp1 in discharge_port_ls)
                                {
                                    dis_port += clp1.PortName + "|";
                                }
                            }
                            etxValue.SetValue(CPAIConstantUtil.Discharge_port, new ExtendValue { value = dis_port, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Load_port, new ExtendValue { value = load_port, encryptFlag = "N" });

                            //set offersItem
                            foreach (ChitOffersItem of in dataDetail.chit_negotiation_summary.chit_offers_items)
                            {
                                if (!string.IsNullOrEmpty(of.final_flag) && of.final_flag.Equals("Y"))
                                {
                                    etxValue.SetValue(CPAIConstantUtil.FinalPrice, new ExtendValue { value = of.final_deal, encryptFlag = "N" });
                                    break;
                                }
                            }

                        }
                        else
                        {
                            ChitRootObject dataDetail_temp = JSonConvertUtil.jsonToModel<ChitRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                            dataDetail_temp.chit_reason = note;
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail_temp);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        }
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //}
                //else
                //{
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                //}
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICharteringInCMMTUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: Exception >>> ", ex);
                log.Error("CPAICharteringInCMMTUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, ChitRootObject dataDetail)
        {
            log.Info("# Start State CPAICharteringInCMMTUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_DATA_DAL dataDAL = new CHIT_DATA_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CHIT_DATA
                            // CHIT_evaluating
                            CHIT_DATA dataCHIT = new CHIT_DATA();

                            dataCHIT.IDAT_ROW_ID = TransID;

                            dataCHIT.IDAT_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            // On Subject Date
                            if (!string.IsNullOrEmpty(dataDetail.chit_detail.date))
                                dataCHIT.IDAT_DATE_PURCHASE = DateTime.ParseExact(dataDetail.chit_detail.date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataCHIT.IDAT_FK_CUST = "";

                            // Vessel Laycan
                            ShareFn fn = new ShareFn();
                            if (!string.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.laycan_to))
                            {
                                dataCHIT.IDAT_LAYCAN_FROM = DateTime.ParseExact(dataDetail.chit_proposed_for_approve.laycan_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataCHIT.IDAT_LAYCAN_TO = DateTime.ParseExact(dataDetail.chit_proposed_for_approve.laycan_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            dataCHIT.IDAT_FK_CUST = dataDetail.chit_detail.charterer;
                            //dataCHIT.IDAT_D_FK_CHARTERER_BROKER = dataDetail.chit_proposed_for_approve.charterer_broker;
                            dataCHIT.IDAT_D_CHARTERER_BROKER = dataDetail.chit_proposed_for_approve.charterer_broker;
                            dataCHIT.IDAT_BSS = dataDetail.chit_detail.bss;
                            dataCHIT.IDAT_D_CHARTER_PATRY_FROM = dataDetail.chit_proposed_for_approve.charter_patry;
                            dataCHIT.IDAT_D_VESSEL = dataDetail.chit_proposed_for_approve.vessel_name;
                            dataCHIT.IDAT_D_OWNER_BROKER = dataDetail.chit_proposed_for_approve.broker_name;
                            dataCHIT.IDAT_D_FREIGHT = dataDetail.chit_proposed_for_approve.freight;
                            dataCHIT.IDAT_D_LAYTIME = dataDetail.chit_proposed_for_approve.laytime;
                            dataCHIT.IDAT_D_DEM = dataDetail.chit_proposed_for_approve.demurrage;
                            dataCHIT.IDAT_D_PAYMENT_TERM = dataDetail.chit_proposed_for_approve.payment_term;
                            dataCHIT.IDAT_D_CHARTER_BROKER_COM = dataDetail.chit_proposed_for_approve.charterer_brokerCom;
                            dataCHIT.IDAT_D_OWNER_BROKER_COM = dataDetail.chit_proposed_for_approve.broker_commission;
                            dataCHIT.IDAT_D_ADDRESS_COM = dataDetail.chit_proposed_for_approve.address_commission;
                            dataCHIT.IDAT_D_WITHHOLDING_TAX = dataDetail.chit_proposed_for_approve.withholding_tax;
                            dataCHIT.IDAT_D_VESSEL_BUILT = dataDetail.chit_proposed_for_approve.vessel_built;
                            dataCHIT.IDAT_D_KDWT = dataDetail.chit_proposed_for_approve.capacity;
                            dataCHIT.IDAT_D_OTHERS = dataDetail.chit_proposed_for_approve.Other;
                            // chit_chartering_method
                            dataCHIT.IDAT_EXPLANATION = dataDetail.chit_detail.Reason;
                            dataCHIT.IDAT_TOTAL_FREIGHT = dataDetail.chit_proposed_for_approve.total_freight;
                            dataCHIT.IDAT_CHARTER_FOR = dataDetail.chit_detail.charterFor;
                            dataCHIT.IDAT_NET_FREIGHT = dataDetail.chit_proposed_for_approve.net_freight;
                            //dataCHIT.IDAT_SELECT_CHARTER_FOR = dataDetail.chit_negotiation_summary.select_charter_for;
                            //
                            //string Route = dataDetail.chit_cargo_detail.route;


                            dataCHIT.IDAT_NEGOTIATION_UNIT = dataDetail.chit_detail.NegoUnit;
                            dataCHIT.IDAT_REASON = dataDetail.chit_reason;
                            dataCHIT.IDAT_REQUESTED_DATE = dtNow;
                            dataCHIT.IDAT_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHIT.IDAT_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);

                            dataCHIT.IDAT_REMARK = dataDetail.chit_note;
                            dataCHIT.IDAT_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHIT.IDAT_CREATED = dtNow;
                            dataCHIT.IDAT_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHIT.IDAT_UPDATED = dtNow;
                            dataCHIT.IDAT_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Save(dataCHIT, context);
                            #endregion

                            #region CHI_PORT
                            CHIT_PORT_DAL dataLoadPortDAL = new CHIT_PORT_DAL();
                            CHIT_PORT dataCHITP;
                            if (dataDetail.chit_detail != null && dataDetail.chit_detail.chit_load_port_control != null)
                            {
                                foreach (var itemLoadPort in dataDetail.chit_detail.chit_load_port_control)
                                {
                                    dataCHITP = new CHIT_PORT();
                                    dataCHITP.FDP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHITP.CHTP_FK_CHIT_DATA = TransID;

                                    dataCHITP.CHTP_ORDER_PORT = itemLoadPort.PortOrder;
                                    dataCHITP.CHTP_PORT_TYPE = "l";
                                    //dataCHITP.CHTP_FK_PORT = (itemLoadPort.PortName == null) ? "" : (itemLoadPort.PortName.Split('|').Length >= 2) ? itemLoadPort.PortName.Split('|')[1] : "";
                                    dataCHITP.CHTP_OTHER_DESC = (itemLoadPort.PortOtherDesc == null) ? "" : itemLoadPort.PortOtherDesc;
                                    dataCHITP.CHTP_VALUE = itemLoadPort.PortName;



                                    dataCHITP.CHTP_CREATED = dtNow;
                                    dataCHITP.CHTP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHITP.CHTP_UPDATED = dtNow;
                                    dataCHITP.CHTP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataLoadPortDAL.Save(dataCHITP, context);
                                }
                            }
                            if (dataDetail.chit_detail != null && dataDetail.chit_detail.chit_discharge_port_control != null)
                            {
                                foreach (var itemLoadPort in dataDetail.chit_detail.chit_discharge_port_control)
                                {
                                    dataCHITP = new CHIT_PORT();
                                    dataCHITP.FDP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHITP.CHTP_FK_CHIT_DATA = TransID;

                                    dataCHITP.CHTP_ORDER_PORT = itemLoadPort.PortOrder;
                                    dataCHITP.CHTP_PORT_TYPE = "d";
                                    //dataCHITP.CHTP_FK_PORT = (itemLoadPort.PortName == null) ? "" : (itemLoadPort.PortName.Split('|').Length >= 2) ? itemLoadPort.PortName.Split('|')[1] : "";
                                    dataCHITP.CHTP_OTHER_DESC = (itemLoadPort.PortOtherDesc == null) ? "" : itemLoadPort.PortOtherDesc;
                                    dataCHITP.CHTP_VALUE = itemLoadPort.PortName;



                                    dataCHITP.CHTP_CREATED = dtNow;
                                    dataCHITP.CHTP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHITP.CHTP_UPDATED = dtNow;
                                    dataCHITP.CHTP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataLoadPortDAL.Save(dataCHITP, context);
                                }
                            }
                            #endregion

                            #region CHI_CARGO
                            CHIT_CARGO_DAL dalDischargePort = new CHIT_CARGO_DAL();
                            CHIT_CARGO dataCargo;
                            if (dataDetail.chit_detail != null && dataDetail.chit_detail.chit_cargo_qty != null)
                            {
                                foreach (var itemCargo in dataDetail.chit_detail.chit_cargo_qty)
                                {
                                    dataCargo = new CHIT_CARGO();
                                    dataCargo.CHTC_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCargo.CHTC_ORDER = itemCargo.order;
                                    dataCargo.CHTC_QUANTITY = itemCargo.qty;
                                    dataCargo.CHTC_TOLERANCE = itemCargo.tolerance;
                                    dataCargo.CHTC_FK_CARGO = itemCargo.cargo;
                                    dataCargo.CHTC_UNIT = itemCargo.unit;
                                    dataCargo.CHTC_FK_CHIT_DATA = TransID;
                                    dataCargo.CHTC_UNIT_OTHERS = itemCargo.Others;
                                    dataCargo.CHTC_CREATED = dtNow;
                                    dataCargo.CHTC_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCargo.CHTC_UPDATED = dtNow;
                                    dataCargo.CHTC_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dalDischargePort.Save(dataCargo, context);
                                }
                            }
                            #endregion

                            #region CHI_OFFER_ITEMS
                            CHIT_OFFER_ITEMS_DAL dalOfferItems = new CHIT_OFFER_ITEMS_DAL();
                            CHIT_OFFER_ITEMS dataOfferItems;
                            string rowIDOffer = string.Empty;
                            string rowIDRoundItems = string.Empty;
                            int idx_offer_item = 1;
                            foreach (var itemPort in dataDetail.chit_negotiation_summary.chit_offers_items)
                            {
                                rowIDOffer = Guid.NewGuid().ToString("N");
                                dataOfferItems = new CHIT_OFFER_ITEMS();
                                dataOfferItems.ITOI_ROW_ID = rowIDOffer;
                                dataOfferItems.ITOI_FK_CHIT_DATA = TransID;
                                dataOfferItems.ITOI_FK_VENDOR_BK = (string.IsNullOrEmpty(itemPort.broker)) ? null : itemPort.broker;
                                dataOfferItems.ITOI_ORDER = idx_offer_item.ToString();
                                dataOfferItems.ITOI_VEHICLE = itemPort.vessel;
                                dataOfferItems.ITOI_YEAR = itemPort.year;
                                dataOfferItems.ITOI_KDWT = itemPort.KDWT;
                                //add type
                                dataOfferItems.ITOI_TYPE = itemPort.type;

                                dataOfferItems.ITOI_VENDOR_OWNER = itemPort.owner;
                                dataOfferItems.ITOI_FINAL_FLAG = itemPort.final_flag;
                                dataOfferItems.ITOI_FINAL_DEAL = itemPort.final_deal;
                                dataOfferItems.ITOI_CREATED = dtNow;
                                dataOfferItems.ITOI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataOfferItems.ITOI_UPDATED = dtNow;
                                dataOfferItems.ITOI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataOfferItems.ITOI_REMARK = itemPort.finalremark;
                                dalOfferItems.Save(dataOfferItems, context);
                                idx_offer_item++;
                                #region CHI_OFFER_ITEMS
                                CHIT_ROUND_ITEMS_DAL dalRoundItems = new CHIT_ROUND_ITEMS_DAL();
                                CHIT_ROUND_ITEMS dataRoundItems;
                                foreach (var itemRoundItems in itemPort.chit_round_items)
                                {
                                    rowIDRoundItems = Guid.NewGuid().ToString("N");
                                    dataRoundItems = new CHIT_ROUND_ITEMS();
                                    dataRoundItems.ITRI_ROW_ID = rowIDRoundItems;
                                    dataRoundItems.ITRI_FK_OFFER_ITEMS = rowIDOffer;
                                    dataRoundItems.ITRI_ROUND_NO = itemRoundItems.round_no;
                                    dataRoundItems.ITRI_CREATED = dtNow;
                                    dataRoundItems.ITRI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataRoundItems.ITRI_UPDATED = dtNow;
                                    dataRoundItems.ITRI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                    dalRoundItems.Save(dataRoundItems, context);

                                    #region CHI_OFFER_ITEMS
                                    CHIT_ROUND_DAL dalRound = new CHIT_ROUND_DAL();
                                    CHIT_ROUND dataRound;
                                    foreach (var itemRound in itemRoundItems.chit_round_info)
                                    {
                                        dataRound = new CHIT_ROUND();
                                        dataRound.ITIR_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataRound.ITIR_FK_ROUND_ITEMS = rowIDRoundItems;
                                        dataRound.ITIR_ORDER = itemRound.order;
                                        dataRound.ITIR_ROUND_TYPE = itemRound.type;
                                        dataRound.ITIR_TYPE_OTHERS = itemRound.typeOther;
                                        dataRound.ITIR_ROUND_OFFER = itemRound.offer;
                                        dataRound.ITIR_ROUND_COUNTER = itemRound.counter;
                                        dataRound.ITIR_CREATED = dtNow;
                                        dataRound.ITIR_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataRound.ITIR_UPDATED = dtNow;
                                        dataRound.ITIR_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                                        dalRound.Save(dataRound, context);
                                    }
                                    #endregion
                                }
                                #endregion
                            }

                            //if(!String.IsNullOrEmpty(offerRowId))
                            //{
                            //    dataCHIT = new CHIT_DATA();
                            //    dataCHIT.IDAT_ROW_ID = TransID;
                            //    dataCHIT.IDAT_SELECT_CHARTER_FOR = offerRowId;
                            //    dataDAL = new CHIT_DATA_DAL();
                            //    dataDAL.UpdateSelect(dataCHIT, context);
                            //}


                            #endregion

                            #region CHI_ATTACH_FILE
                            CHIT_ATTACH_FILE_DAL dataAttachFileDAL = new CHIT_ATTACH_FILE_DAL();
                            CHIT_ATTACH_FILE dataCHIAF;
                            foreach (var item in dataDetail.explanationAttach.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataCHIAF = new CHIT_ATTACH_FILE();
                                    dataCHIAF.ITAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCHIAF.ITAF_FK_CHIT_DATA = TransID;
                                    dataCHIAF.ITAF_PATH = text[0];
                                    dataCHIAF.ITAF_INFO = text[1];
                                    dataCHIAF.ITAF_TYPE = "EXT";
                                    dataCHIAF.ITAF_CREATED = dtNow;
                                    dataCHIAF.ITAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCHIAF.ITAF_UPDATED = dtNow;
                                    dataCHIAF.ITAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAttachFileDAL.Save(dataCHIAF, context);
                                }
                            }

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            if (!string.IsNullOrEmpty(attach_items))
                            {
                                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                                foreach (var item in Att.attach_items)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataCHIAF = new CHIT_ATTACH_FILE();
                                        dataCHIAF.ITAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCHIAF.ITAF_FK_CHIT_DATA = TransID;
                                        dataCHIAF.ITAF_PATH = item;
                                        dataCHIAF.ITAF_INFO = "";
                                        dataCHIAF.ITAF_TYPE = "ATT";
                                        dataCHIAF.ITAF_CREATED = dtNow;
                                        dataCHIAF.ITAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCHIAF.ITAF_UPDATED = dtNow;
                                        dataCHIAF.ITAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataCHIAF, context);
                                    }
                                }
                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: Rollback >>> ", ex);
                            log.Error("CPAICharteringInCMMTUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: Exception >>> ", ex);
                log.Error("CPAICharteringInCMMTUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAICharteringInCMMTUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringInCMMTUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHIT_DATA_DAL dataDAL = new CHIT_DATA_DAL();
                            CHIT_DATA dataCHIT = new CHIT_DATA();
                            dataCHIT.IDAT_ROW_ID = TransID;
                            dataCHIT.IDAT_REASON = note;
                            dataCHIT.IDAT_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHIT.IDAT_UPDATED = DateTime.Now;
                            dataCHIT.IDAT_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHIT, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: UpdateDB >>> ", ex);
                            log.Error("CPAICharteringInCMMTUpdateDataState::UpdateDB >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }

                }
                log.Info("# End State CPAICharteringInCMMTUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: UpdateDB >>> ", ex);
                log.Error("CPAICharteringInCMMTUpdateDataState::UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, string prevStatus, Dictionary<string, ExtendValue> etxValue, string note, string explanation = null, string note_2 = null)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringInCMMTUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        { 
                            CHIT_DATA_DAL dataDAL = new CHIT_DATA_DAL();
                            CHIT_DATA dataCHIT = new CHIT_DATA();
                            dataCHIT.IDAT_ROW_ID = TransID;
                            dataCHIT.IDAT_REASON = note;
                            //if (explanation != null)
                            //{
                            //    dataCHIT.IDAT_EXPLANATION = explanation;
                            //}
                            //if (note_2 != null)
                            //{
                            //    dataCHIT.IDAT_REMARK = note_2;
                            //}
                            //if((prevStatus == "WAITING_CONFIRM_SH" && NextStatus == "WAITING_CONFIRM") || (prevStatus == "WAITING_APPROVE" && NextStatus == "WAITING_CONFIRM") || (prevStatus == "APPROVED" && NextStatus == "WAITING_CONFIRM"))
                            //{
                            //    dataCHIT.IDAT_SELECT_CHARTER_FOR = "";
                            //}
                            //if(NextStatus == "WAITING_CONFIRM_SH")
                            //{
                            //    dataCHIT.IDAT_SELECT_CHARTER_FOR = "0e446fbbce05446b86f9d5543f96135f";
                            //} 
                            dataCHIT.IDAT_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHIT.IDAT_UPDATED = DateTime.Now;
                            dataCHIT.IDAT_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHIT, context);
 
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: UpdateDB >>> ", ex);
                            log.Error("CPAICharteringInCMMTUpdateDataState::UpdateDB >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }

                }
                log.Info("# End State CPAICharteringInCMMTUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICharteringInCMMTUpdateDataState # :: UpdateDB >>> ", ex);
                log.Error("CPAICharteringInCMMTUpdateDataState::UpdateDB >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}