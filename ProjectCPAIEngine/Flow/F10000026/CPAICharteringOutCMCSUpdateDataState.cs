﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000026
{
    public class CPAICharteringOutCMCSUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICharteringOutCMCSUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (string.IsNullOrEmpty(item))
                {
                    item = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                }
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    ChosRootObject dataDetail = JSonConvertUtil.jsonToModel<ChosRootObject>(item);

                    //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) || (dataDetail.chos_evaluating.vessel_id != null
                    //    && dataDetail.chos_evaluating.date_purchase != null))
                    //{
                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    dataDetail.chos_reason = note;

                    bool isRunDB = false;
                    //if ((nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)) || nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CERTIFIED))
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) ||
                        (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2) && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                        (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2))
                    {
                        //Verify can edit >> explanation, note
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.chos_chartering_method.explanation, dataDetail.chos_note);
                    }
                    else
                    {
                        //OTHER ==> UPDATE
                        //isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note);
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note, dataDetail.chos_chartering_method.explanation, dataDetail.chos_note);
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        {
                            dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                        }
                        #endregion
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = approve_1 or DRAFT only)

                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") ||
                            (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE + "_2") && etxValue.GetValue(CPAIConstantUtil.Type) == "CRUDE") ||
                            currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                        {
                            string vessel = dataDetail.chos_evaluating.vessel_id != null ? dataDetail.chos_evaluating.vessel_id : "";
                            string date_pn = dataDetail.chos_evaluating.date_purchase != null ? dataDetail.chos_evaluating.date_purchase : "";
                            string flat_rate = dataDetail.chos_evaluating.flat_rate != null ? dataDetail.chos_evaluating.flat_rate : "";
                            //string charterer = dataDetail.chos_proposed_for_approve.charterer != null ? dataDetail.chos_proposed_for_approve.charterer : "";
                            string laycan_from = dataDetail.chos_proposed_for_approve.laycan_from != null ? dataDetail.chos_proposed_for_approve.laycan_from : "";
                            string laycan_to = dataDetail.chos_proposed_for_approve.laycan_to != null ? dataDetail.chos_proposed_for_approve.laycan_to : "";
                            string broker = dataDetail.chos_evaluating.broker_id != null ? dataDetail.chos_evaluating.broker_id : "";
                            string charterer = dataDetail.chos_proposed_for_approve.charterer_id != null ? dataDetail.chos_proposed_for_approve.charterer_id : "";
                            //for mobile by poo
                            string ws = dataDetail.chos_evaluating.freight_detail != null ? dataDetail.chos_evaluating.freight_detail : "";
                            string route = dataDetail.chos_proposed_for_approve.route != null ? dataDetail.chos_proposed_for_approve.route : "";
                            string no_total_ex = dataDetail.chos_benefit.chos_total_expense_no_charter != null ? dataDetail.chos_benefit.chos_total_expense_no_charter : "";
                            string total_ex = dataDetail.chos_benefit.chos_total_expense != null ? dataDetail.chos_benefit.chos_total_expense : "";
                            string net_benefit = dataDetail.chos_benefit.chos_net_benefit != null ? dataDetail.chos_benefit.chos_net_benefit : "";

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            //set data
                            etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = vessel, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_pn, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.FlatRate, new ExtendValue { value = flat_rate, encryptFlag = "N" });
                            //etxValue.SetValue(CPAIConstantUtil.Charterer, new ExtendValue { value = charterer, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Broker, new ExtendValue { value = broker, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Charterer, new ExtendValue { value = charterer, encryptFlag = "N" });
                            //for mobile by poo
                            etxValue.SetValue(CPAIConstantUtil.ws, new ExtendValue { value = ws, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.route, new ExtendValue { value = route, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.no_total_ex, new ExtendValue { value = no_total_ex, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.total_ex, new ExtendValue { value = total_ex, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.net_benefit, new ExtendValue { value = net_benefit, encryptFlag = "N" });

                        }
                        else
                        {
                            ChosRootObject dataDetail_temp = JSonConvertUtil.jsonToModel<ChosRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                            dataDetail_temp.chos_reason = note;
                            dataDetail_temp.chos_note = dataDetail.chos_note;
                            dataDetail_temp.chos_chartering_method.explanation = dataDetail.chos_chartering_method.explanation;
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail_temp);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        }
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //}
                //else
                //{
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                //}
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICharteringOutCMCSUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAICharteringOutCMCSUpdateDataState # :: Exception >>> " + tem);
                log.Error("# Error CPAICharteringOutCMCSUpdateDataState # :: Exception >>> ", ex);
                log.Error("CPAICharteringOutCMCSUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, ChosRootObject dataDetail)
        {
            log.Info("# Start State CPAICharteringOutCMCSUpdateDataState >> InsertDB #  ");
            
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOS_DATA_DAL dataDAL = new CHOS_DATA_DAL();
                            dataDAL.Delete(TransID, context);

                            #region --- CHOS_DATA ---

                            #region chos_evaluating

                            CHOS_DATA dataCHOS = new CHOS_DATA();
                            dataCHOS.OSDA_ROW_ID = TransID;
                            dataCHOS.OSDA_FK_VESSEL = dataDetail.chos_evaluating.vessel_id;
                            dataCHOS.OSDA_VOYAGE_NO = dataDetail.chos_evaluating.voyage_no;

                            dataCHOS.OSDA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            if (!string.IsNullOrEmpty(dataDetail.chos_evaluating.date_purchase))
                                dataCHOS.OSDA_DATE_PURCHASE = DateTime.ParseExact(dataDetail.chos_evaluating.date_purchase, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataCHOS.OSDA_FK_BROKER = dataDetail.chos_evaluating.broker_id;
                            dataCHOS.OSDA_VESSEL_SIZE = dataDetail.chos_evaluating.vessel_size;
                            dataCHOS.OSDA_FREIGHT = dataDetail.chos_evaluating.freight;
                            dataCHOS.OSDA_FLAT_RATE = dataDetail.chos_evaluating.flat_rate;
                            dataCHOS.OSDA_YEAR = dataDetail.chos_evaluating.year;
                            dataCHOS.OSDA_UNIT = dataDetail.chos_evaluating.unit;

                            if (!string.IsNullOrEmpty(dataDetail.chos_evaluating.period_date_from) && !string.IsNullOrEmpty(dataDetail.chos_evaluating.period_date_to))
                            {
                                dataCHOS.OSDA_PERIOD_DATE_FROM = DateTime.ParseExact(dataDetail.chos_evaluating.period_date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataCHOS.OSDA_PERIOD_DATE_TO = DateTime.ParseExact(dataDetail.chos_evaluating.period_date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            #endregion

                            #region chos_chartering_method

                            if (dataDetail.chos_chartering_method != null)
                            {
                                dataCHOS.OSDA_EXPLANATION = dataDetail.chos_chartering_method.explanation;
                                foreach (var item in dataDetail.chos_chartering_method.chos_type_of_fixings)
                                {
                                    if (item.type_flag.Equals("Y"))
                                    {
                                        dataCHOS.OSDA_TYPE_OF_FIXINGS = item.type_value;
                                    }
                                }
                            }

                            #endregion

                            #region chos_proposed_for_approve

                            if (dataDetail.chos_proposed_for_approve != null)
                            {
                                dataCHOS.OSDA_P_FK_CHARTERER = dataDetail.chos_proposed_for_approve.charterer_id;
                                dataCHOS.OSDA_P_CHARTER_PARTY = dataDetail.chos_proposed_for_approve.charter_party;
                                dataCHOS.OSDA_P_CHARTERER_BROKER = dataDetail.chos_proposed_for_approve.charterer_broker;
                                dataCHOS.OSDA_P_OWNER_BROKER = dataDetail.chos_proposed_for_approve.owner_broker;
                                dataCHOS.OSDA_P_ROUTE = dataDetail.chos_proposed_for_approve.route;

                                if (!string.IsNullOrEmpty(dataDetail.chos_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(dataDetail.chos_proposed_for_approve.laycan_to))
                                {
                                    dataCHOS.OSDA_P_LAYCAN_FROM = DateTime.ParseExact(dataDetail.chos_proposed_for_approve.laycan_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataCHOS.OSDA_P_LAYCAN_TO = DateTime.ParseExact(dataDetail.chos_proposed_for_approve.laycan_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }

                                dataCHOS.OSDA_P_LOADING = dataDetail.chos_proposed_for_approve.loading_ports;
                                dataCHOS.OSDA_P_DISCHARGING = dataDetail.chos_proposed_for_approve.discharging_ports;

                                if (!string.IsNullOrEmpty(dataDetail.chos_proposed_for_approve.a_date_from) && !string.IsNullOrEmpty(dataDetail.chos_proposed_for_approve.a_date_to))
                                {
                                    dataCHOS.OSDA_A_DATE_FROM = DateTime.ParseExact(dataDetail.chos_proposed_for_approve.a_date_from, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                    dataCHOS.OSDA_A_DATE_TO = DateTime.ParseExact(dataDetail.chos_proposed_for_approve.a_date_to, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }
                                if (!string.IsNullOrEmpty(dataDetail.chos_proposed_for_approve.b_date_from) && !string.IsNullOrEmpty(dataDetail.chos_proposed_for_approve.b_date_to))
                                {
                                    dataCHOS.OSDA_B_DATE_FROM = DateTime.ParseExact(dataDetail.chos_proposed_for_approve.b_date_from, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                    dataCHOS.OSDA_B_DATE_TO = DateTime.ParseExact(dataDetail.chos_proposed_for_approve.b_date_to, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }
                            }

                            #endregion

                            #region chos_benefit
                            if (dataDetail.chos_benefit != null)
                            {
                                dataCHOS.OSDA_TOTAL_EXPENS_NC = dataDetail.chos_benefit.chos_total_expense_no_charter;
                                dataCHOS.OSDA_TOTAL_EXPENSE = dataDetail.chos_benefit.chos_total_expense;
                                dataCHOS.OSDA_NET_BENEFIT = dataDetail.chos_benefit.chos_net_benefit;
                            }
                            #endregion

                            dataCHOS.OSDA_REASON = dataDetail.chos_reason;
                            dataCHOS.OSDA_REQUESTED_DATE = dtNow;
                            dataCHOS.OSDA_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHOS.OSDA_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);

                            dataCHOS.OSDA_REMARK = dataDetail.chos_note;
                            dataCHOS.OSDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHOS.OSDA_CREATED = dtNow;
                            dataCHOS.OSDA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHOS.OSDA_UPDATED = dtNow;
                            dataCHOS.OSDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCHOS.OSDA_FREIGHT_DETAIL = dataDetail.chos_evaluating.freight_detail;
                            dataDAL.Save(dataCHOS, context);

                            #endregion

                            #region --- CHOS_CASE_A, CHOS_A_EST_EXPENSE ---

                            if (dataDetail.chos_no_charter_out_case_a != null)
                            {
                                CHOS_CASE_A_DAL dataCaseADAL = new CHOS_CASE_A_DAL();
                                CHOS_CASE_A dataCaseA = new CHOS_CASE_A();
                                dataCaseA.OSAO_ROW_ID = Guid.NewGuid().ToString("N");
                                dataCaseA.OSAO_FK_CHOS_DATA = TransID;
                                dataCaseA.OSAO_ORDER = "1";
                                dataCaseA.OSAO_TOTAL_EXPENSE_NC = dataDetail.chos_no_charter_out_case_a.total_expense_no_charter;
                                dataCaseA.OSAO_CREATED = dtNow;
                                dataCaseA.OSAO_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCaseA.OSAO_UPDATED = dtNow;
                                dataCaseA.OSAO_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                
                                dataCaseADAL.Save(dataCaseA, context);

                                CHOS_A_EST_EXPENSE_DAL dataAExpenseDAL = new CHOS_A_EST_EXPENSE_DAL();
                                CHOS_A_EST_EXPENSE dataAExpense;
                                foreach (var item in dataDetail.chos_no_charter_out_case_a.chos_est_expense)
                                {
                                    dataAExpense = new CHOS_A_EST_EXPENSE();
                                    dataAExpense.OSEE_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataAExpense.OSEE_FK_CHOS_CASE_A = dataCaseA.OSAO_ROW_ID;
                                    dataAExpense.OSEE_ORDER = item.order;
                                    if (!string.IsNullOrEmpty(item.chos_period_date_from) && !string.IsNullOrEmpty(item.chos_period_date_to))
                                    {
                                        dataAExpense.OSEE_PERIOD_DATE_FROM = DateTime.ParseExact(item.chos_period_date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        dataAExpense.OSEE_PERIOD_DATE_TO = DateTime.ParseExact(item.chos_period_date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    dataAExpense.OSEE_TC_HIRE_NO = item.tc_hire_no;
                                    dataAExpense.OSEE_TC_HIRE_COST = item.tc_hire_cost;
                                    dataAExpense.OSEE_PORT_CHARGE = item.port_charge;
                                    dataAExpense.OSEE_BUNKER_COST = item.bunker_cost;
                                    dataAExpense.OSEE_OTHER_COSTS = item.other_costs;
                                    dataAExpense.OSEE_OTHER_REMARKS = item.other_remarks;
                                    dataAExpense.OSEE_TOTAL_TC_COST = item.total_tc_cost;
                                    dataAExpense.OSEE_CREATED = dtNow;
                                    dataAExpense.OSEE_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAExpense.OSEE_UPDATED = dtNow;
                                    dataAExpense.OSEE_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataAExpense.OSEE_REMARK = item.remarks;
                                    dataAExpenseDAL.Save(dataAExpense, context);
                                }
                            }


                            #endregion

                            #region --- CHOS_CASE_B, CHOS_B_ONE_EST_INCOME, CHOS_B_ONE_OTHER_EXPENSE ---

                            if (dataDetail.chos_charter_out_case_b != null)
                            {
                                CHOS_CASE_B_DAL dataCaseBDAL = new CHOS_CASE_B_DAL();
                                CHOS_CASE_B dataCaseB = new CHOS_CASE_B();

                                dataCaseB.OSBO_ROW_ID = Guid.NewGuid().ToString("N");
                                dataCaseB.OSBO_FK_CHOS_DATA = TransID;
                                dataCaseB.OSBO_ORDER = "1";
                                dataCaseB.OSBO_TOTAL_INCOME_B_ONE = dataDetail.chos_charter_out_case_b.total_income_b_one;
                                dataCaseB.OSBO_TOTAL_EXPENSE_B_TWO = dataDetail.chos_charter_out_case_b.total_expense_b_two;
                                dataCaseB.OSBO_CREATED = dtNow;
                                dataCaseB.OSBO_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCaseB.OSBO_UPDATED = dtNow;
                                dataCaseB.OSBO_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCaseBDAL.Save(dataCaseB, context);

                                #region --- CHOS_B_ONE_EST_INCOME, CHOS_B_ONE_OTHER_EXPENSE ---

                                if (dataDetail.chos_charter_out_case_b.chos_charter_out_case_b_one != null)
                                {

                                    CHOS_B_ONE_EST_INCOME_DAL dataBOneIncomeDAL = new CHOS_B_ONE_EST_INCOME_DAL();
                                    CHOS_B_ONE_EST_INCOME dataBOneIncome;
                                    foreach (var itemBOneIncome in dataDetail.chos_charter_out_case_b.chos_charter_out_case_b_one)
                                    {
                                        dataBOneIncome = new CHOS_B_ONE_EST_INCOME();
                                        dataBOneIncome.OSEI_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataBOneIncome.OSEI_FK_CHOS_CASE_B = dataCaseB.OSBO_ROW_ID;
                                        dataBOneIncome.OSEI_ORDER = itemBOneIncome.order;
                                        if (!string.IsNullOrEmpty(itemBOneIncome.chos_period_date_from) && !string.IsNullOrEmpty(itemBOneIncome.chos_period_date_to))
                                        {
                                            dataBOneIncome.OSEI_PERIOD_DATE_FROM = DateTime.ParseExact(itemBOneIncome.chos_period_date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            dataBOneIncome.OSEI_PERIOD_DATE_TO = DateTime.ParseExact(itemBOneIncome.chos_period_date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        dataBOneIncome.OSEI_FK_VESSEL = itemBOneIncome.vessel_id;
                                        dataBOneIncome.OSEI_VOYAGE_NO = itemBOneIncome.voyage_no;
                                        dataBOneIncome.OSEI_FIXING_WS = itemBOneIncome.fixing_ws;
                                        dataBOneIncome.OSEI_EST_FLAT_RATE = itemBOneIncome.est_flat_rate;
                                        dataBOneIncome.OSEI_MIN_LOADED = itemBOneIncome.min_loaded;
                                        //dataBOneIncome.OSEI_DEDUCT = itemBOneIncome.deduct;

                                       
                                        dataBOneIncome.OSEI_FREIGHT = itemBOneIncome.freight;
                                        dataBOneIncome.OSEI_TOTAL_INCOME = itemBOneIncome.total_income;

                                        dataBOneIncome.OSEI_CREATED = dtNow;
                                        dataBOneIncome.OSEI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataBOneIncome.OSEI_UPDATED = dtNow;
                                        dataBOneIncome.OSEI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataBOneIncomeDAL.Save(dataBOneIncome, context);

                                        if (itemBOneIncome.chos_b_one_est_income_deduct != null)
                                        {
                                            CHOS_B_ONE_EST_INCOME_DEDUCT dataDeduct;
                                            CHOS_B_ONE_EST_INCOME_DEDUCT_DAL dataDeductDAL = new CHOS_B_ONE_EST_INCOME_DEDUCT_DAL();
                                            int order_deduct = 0;
                                            foreach (var itemdeduct in itemBOneIncome.chos_b_one_est_income_deduct)
                                            {
                                                order_deduct = order_deduct + 1;
                                                dataDeduct = new CHOS_B_ONE_EST_INCOME_DEDUCT();
                                                dataDeduct.OSEID_ROW_ID = Guid.NewGuid().ToString("N");
                                                dataDeduct.OSEID_FK_CHOS_B_ONE_EST_INCOME = dataBOneIncome.OSEI_ROW_ID;
                                                dataDeduct.OSEID_ORDER = order_deduct.ToString();
                                                dataDeduct.OSEID_DEDUCT = itemdeduct.deduct;
                                                dataDeduct.OSEID_CREATED = dtNow;
                                                dataDeduct.OSEID_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataDeduct.OSEID_UPDATED = dtNow;
                                                dataDeduct.OSEID_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataDeduct.OSEID_DEDUCT_NAME = itemdeduct.name;
                                                dataDeductDAL.Save(dataDeduct, context);
                                            }

                                        }

                                        if (itemBOneIncome.other_expense != null)
                                        {
                                            CHOS_B_ONE_OTHER_EXPENSE_DAL dataBOneOtherExpenseDAL = new CHOS_B_ONE_OTHER_EXPENSE_DAL();
                                            CHOS_B_ONE_OTHER_EXPENSE dataBOneOtherExpense;

                                            foreach (var itemBOneOther in itemBOneIncome.other_expense)
                                            {
                                                dataBOneOtherExpense = new CHOS_B_ONE_OTHER_EXPENSE();

                                                dataBOneOtherExpense.OSNE_ROW_ID = Guid.NewGuid().ToString("N");
                                                dataBOneOtherExpense.OSNE_FK_CHOS_B_ONE = dataBOneIncome.OSEI_ROW_ID;
                                                //dataBOneOtherExpense.OSNE_TYPE = "EX";
                                                dataBOneOtherExpense.OSNE_ORDER = itemBOneOther.order;
                                                dataBOneOtherExpense.OSNE_OTHER_EXPENSE = itemBOneOther.other_key;
                                                dataBOneOtherExpense.OSNE_AMOUNT = itemBOneOther.other_value;

                                                dataBOneOtherExpense.OSNE_CREATED = dtNow;
                                                dataBOneOtherExpense.OSNE_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataBOneOtherExpense.OSNE_UPDATED = dtNow;
                                                dataBOneOtherExpense.OSNE_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataBOneOtherExpenseDAL.Save(dataBOneOtherExpense, context);

                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region --- CHOS_B_TWO_COST_OF_MT, CHOS_B_TWO_SPOT_IN_VESSEL}, CHOS_B_TWO_OTHER_EXPENSE ---

                                if (dataDetail.chos_charter_out_case_b.chos_charter_out_case_b_two != null)
                                {

                                    if(dataDetail.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt != null)
                                    {
                                        ChosCaseCostOfMt itemCostOfMT = dataDetail.chos_charter_out_case_b.chos_charter_out_case_b_two.chos_case_cost_of_mt;
                                        
                                        CHOS_B_TWO_COST_OF_MT_DAL dataCostOfMTDAL = new CHOS_B_TWO_COST_OF_MT_DAL();
                                        CHOS_B_TWO_COST_OF_MT dataCostOfMT;
                                        dataCostOfMT = new CHOS_B_TWO_COST_OF_MT();

                                        dataCostOfMT.OSCM_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCostOfMT.OSCM_FK_CHOS_CASE_B = dataCaseB.OSBO_ROW_ID;

                                        dataCostOfMT.OSCM_ORDER = itemCostOfMT.order;
                                        if (!string.IsNullOrEmpty(itemCostOfMT.chos_period_date_from) && !string.IsNullOrEmpty(itemCostOfMT.chos_period_date_to))
                                        {
                                            dataCostOfMT.OSCM_PERIOD_DATE_FROM = DateTime.ParseExact(itemCostOfMT.chos_period_date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            dataCostOfMT.OSCM_PERIOD_DATE_TO = DateTime.ParseExact(itemCostOfMT.chos_period_date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        dataCostOfMT.OSCM_TC_HIRE_NO = itemCostOfMT.tc_hire_no;
                                        dataCostOfMT.OSCM_TC_HIRE_COST_DAY = itemCostOfMT.tc_hire_cost_day;
                                        dataCostOfMT.OSCM_TC_HIRE_COST_VOY = itemCostOfMT.tc_hire_cost_voy;
                                        dataCostOfMT.OSCM_PORT_CHARGE = itemCostOfMT.port_charge;
                                        dataCostOfMT.OSCM_BUNKER_COST = itemCostOfMT.bunker_cost;
                                        dataCostOfMT.OSCM_OTHER_COSTS = itemCostOfMT.other_costs;
                                        dataCostOfMT.OSCM_OTHER_REMARKS = itemCostOfMT.other_remarks;
                                        dataCostOfMT.OSCM_TOTAL_TC_COST = itemCostOfMT.total_tc_cost;

                                        dataCostOfMT.OSCM_CREATED = dtNow;
                                        dataCostOfMT.OSCM_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCostOfMT.OSCM_UPDATED = dtNow;
                                        dataCostOfMT.OSCM_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCostOfMTDAL.Save(dataCostOfMT, context);

                                        if (itemCostOfMT.chos_spot_in_vessel != null)
                                        {
                                            CHOS_B_TWO_SPOT_IN_VESSEL_DAL dataSportInVesselDAL = new CHOS_B_TWO_SPOT_IN_VESSEL_DAL();
                                            CHOS_B_TWO_SPOT_IN_VESSEL dataSportInVessel;
                                            foreach (var itemSportInVessel in itemCostOfMT.chos_spot_in_vessel)
                                            {

                                                dataSportInVessel = new CHOS_B_TWO_SPOT_IN_VESSEL();
                                                dataSportInVessel.OSSV_ROW_ID = Guid.NewGuid().ToString("N");
                                                dataSportInVessel.OSCM_FK_CHOS_CASE_B_TWO = dataCostOfMT.OSCM_ROW_ID;

                                                dataSportInVessel.OSSV_FK_VESSEL = itemSportInVessel.vessel_id;
                                                dataSportInVessel.OSSV_ORDER = itemSportInVessel.order;
                                                //if(!string.IsNullOrEmpty(itemSportInVessel.loading_date) )
                                                //{
                                                //    dataSportInVessel.OSSV_LOADING_DATE_FROM = DateTime.ParseExact(itemSportInVessel.loading_date, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                                //}

                                                if (!string.IsNullOrEmpty(itemSportInVessel.loading_date_from) && !string.IsNullOrEmpty(itemSportInVessel.loading_date_to))
                                                {
                                                    dataSportInVessel.OSSV_LOADING_DATE_FROM = DateTime.ParseExact(itemSportInVessel.loading_date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                    dataSportInVessel.OSSV_LOADING_DATE_TO = DateTime.ParseExact(itemSportInVessel.loading_date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                }

                                                dataSportInVessel.OSSV_WS = itemSportInVessel.ws;
                                                dataSportInVessel.OSSV_FLAT_RATE = itemSportInVessel.flat_rate;
                                                dataSportInVessel.OSSV_MIN_LOADED = itemSportInVessel.min_loaded;
                                                dataSportInVessel.OSSV_FREIGHT = itemSportInVessel.freight;
                                                //dataSportInVessel.OSSV_DEDUCT = itemSportInVessel.deduct;
                                                dataSportInVessel.OSSV_WS_PERCENT = itemSportInVessel.ws_percent;
                                                dataSportInVessel.OSSV_ADDITIONAL = itemSportInVessel.additional;
                                                dataSportInVessel.OSSV_DEDUCT_ACTION = itemSportInVessel.deduct_action;
                                                dataSportInVessel.OSSV_ADDITIONAL_NAME  = itemSportInVessel.additional_name;
                                                dataSportInVessel.OSSV_TOTAL = itemSportInVessel.freight_before_deduct;
                                                dataSportInVessel.OSSV_TOTAL_DEDUCT = itemSportInVessel.total_deduct_value;
                                                dataSportInVessel.OSSV_CREATED = dtNow;
                                                dataSportInVessel.OSSV_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataSportInVessel.OSSV_UPDATED = dtNow;
                                                dataSportInVessel.OSSV_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataSportInVesselDAL.Save(dataSportInVessel, context);

                                                if (itemSportInVessel.chos_b_two_spot_in_deduct != null)
                                                {
                                                    CHOS_B_TWO_SPOT_IN_DEDUCT dataDeduct;
                                                    CHOS_B_TWO_SPOT_IN_DEDUCT_DAL dataDeductDAL = new CHOS_B_TWO_SPOT_IN_DEDUCT_DAL();
                                                    int order_deduct = 0;
                                                    foreach (var itemdeduct in itemSportInVessel.chos_b_two_spot_in_deduct)
                                                    {
                                                        order_deduct = order_deduct + 1;
                                                        dataDeduct = new CHOS_B_TWO_SPOT_IN_DEDUCT();
                                                        dataDeduct.OSSVD_ROW_ID = Guid.NewGuid().ToString("N");
                                                        dataDeduct.OSSVD_FK_CHOS_B_TWO_SPOT_IN = dataSportInVessel.OSSV_ROW_ID;
                                                        dataDeduct.OSSVD_ORDER = order_deduct.ToString();
                                                        dataDeduct.OSSVD_DEDUCT_NAME = itemdeduct.name;
                                                        dataDeduct.OSSVD_DEDUCT = itemdeduct.deduct;
                                                        dataDeduct.OSSVD_CREATED = dtNow;
                                                        dataDeduct.OSSVD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                        dataDeduct.OSSVD_UPDATED = dtNow;
                                                        dataDeduct.OSSVD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                        
                                                        dataDeductDAL.Save(dataDeduct, context);
                                                    }

                                                }

                                            }
                                        }

                                        if (itemCostOfMT.chos_other_expense != null)
                                        {
                                            CHOS_B_TWO_OTHER_EXPENSE_DAL dataOtherExpenseDAL = new CHOS_B_TWO_OTHER_EXPENSE_DAL();
                                            CHOS_B_TWO_OTHER_EXPENSE dataOtherExpense;

                                            foreach (var itemOtherExpense in itemCostOfMT.chos_other_expense)
                                            {
                                                dataOtherExpense = new CHOS_B_TWO_OTHER_EXPENSE();
                                                dataOtherExpense.OSNE_ROW_ID = Guid.NewGuid().ToString("N");
                                                dataOtherExpense.OSNE_FK_CHOS_B_TWO = dataCostOfMT.OSCM_ROW_ID;

                                                //dataOtherExpense.OSNE_TYPE = "EX";

                                                dataOtherExpense.OSNE_ORDER = itemOtherExpense.order;
                                                dataOtherExpense.OSNE_OTHER_EXPENSE = itemOtherExpense.other_expense;
                                                dataOtherExpense.OSNE_AMOUNT = itemOtherExpense.amount;

                                                dataOtherExpense.OSNE_CREATED = dtNow;
                                                dataOtherExpense.OSNE_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataOtherExpense.OSNE_UPDATED = dtNow;
                                                dataOtherExpense.OSNE_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                dataOtherExpenseDAL.Save(dataOtherExpense, context);
                                                

                                            }
                                        }
                                    }
                                }
                        
                                #endregion
                            }

                            #endregion

                            #region --- CHOS_ATTACH_FILE ---

                            CHOS_ATTACH_FILE_DAL dataAttachFileDAL = new CHOS_ATTACH_FILE_DAL();
                            CHOS_ATTACH_FILE dataCHOAF;
                            if (dataDetail.explanationAttach != null)
                            {
                                foreach (var item in dataDetail.explanationAttach.Split('|'))
                                {
                                    if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                    {
                                        List<string> text = item.Split(':').ToList();
                                        dataCHOAF = new CHOS_ATTACH_FILE();
                                        dataCHOAF.OSAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCHOAF.OSAF_FK_CHOS_DATA = TransID;
                                        dataCHOAF.OSAF_PATH = text[0];
                                        dataCHOAF.OSAF_INFO = text[1];
                                        dataCHOAF.OSAF_TYPE = "EXT";
                                        dataCHOAF.OSAF_CREATED = dtNow;
                                        dataCHOAF.OSAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCHOAF.OSAF_UPDATED = dtNow;
                                        dataCHOAF.OSAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataCHOAF, context);
                                    }
                                }
                            }

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                            if (Att != null)
                            {
                                foreach (var item in Att.attach_items)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataCHOAF = new CHOS_ATTACH_FILE();
                                        dataCHOAF.OSAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataCHOAF.OSAF_FK_CHOS_DATA = TransID;
                                        dataCHOAF.OSAF_PATH = item;
                                        dataCHOAF.OSAF_INFO = "";
                                        dataCHOAF.OSAF_TYPE = "ATT";
                                        dataCHOAF.OSAF_CREATED = dtNow;
                                        dataCHOAF.OSAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCHOAF.OSAF_UPDATED = dtNow;
                                        dataCHOAF.OSAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataCHOAF, context);
                                    }
                                }
                            }

                            #endregion


                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAICharteringOutCMCSUpdateDataState # :: Rollback >>> " + tem);
                            log.Error("# Error CPAICharteringOutCMCSUpdateDataState # :: Rollback >>> ", ex);
                            log.Error("CPAICharteringOutCMCSUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAICharteringOutCMCSUpdateDataState  :: Exception >>>  " + tem);
                log.Error("# Error CPAICharteringOutCMCSUpdateDataState # :: Exception >>> ", ex);
                log.Error("CPAICharteringOutCMCSUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                log.Info("# Error InsertDB # :: Exception >>> " + ex);
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAICharteringOutCMCSUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringOutCMCSUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOS_DATA_DAL dataDAL = new CHOS_DATA_DAL();
                            CHOS_DATA data = new CHOS_DATA();
                            data.OSDA_ROW_ID = TransID;
                            data.OSDA_REASON = note;
                            data.OSDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.OSDA_UPDATED = DateTime.Now;
                            data.OSDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(data, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error UpdateDB # :: Exception >>>  " + tem);
                            log.Error("# Error UpdateDB # :: Rollback >>> ", ex);
                            log.Error("UpdateDB::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }

                log.Info("# End State CPAICharteringOutCMCSUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error UpdateDB # :: Exception >>>  " + tem);
                log.Error("# Error UpdateDB # :: Exception >>> ", ex);
                log.Error("UpdateDB::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string note, string explanation = null, string note_2 = null)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICharteringOutCMCSUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            CHOS_DATA_DAL dataDAL = new CHOS_DATA_DAL();
                            CHOS_DATA data = new CHOS_DATA();
                            data.OSDA_ROW_ID = TransID;
                            data.OSDA_REASON = note;
                            //if (explanation != null)
                            //{
                            //    data.OSDA_EXPLANATION = explanation;
                            //}
                            //if (note_2 != null)
                            //{
                            //    data.OSDA_REMARK = note_2;
                            //}
                            data.OSDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.OSDA_UPDATED = DateTime.Now;
                            data.OSDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(data, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error UpdateDB # :: Exception >>>  " + tem);
                            log.Error("# Error UpdateDB # :: Rollback >>> ", ex);
                            log.Error("UpdateDB::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }

                log.Info("# End State CPAICharteringOutCMCSUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error UpdateDB # :: Exception >>>  " + tem);
                log.Error("# Error UpdateDB # :: Exception >>> ", ex);
                log.Error("UpdateDB::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}





