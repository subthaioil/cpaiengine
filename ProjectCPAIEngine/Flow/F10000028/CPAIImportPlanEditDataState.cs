﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPCF;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000028
{
    public class CPAIImportPlanEditDataState : BasicBean, StateFlowAction
    {

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIImportPlanEditDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                bool isRunDB = false;
                String tripNo = "";
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string data_status = etxValue.GetValue(CPAIConstantUtil.Status);
                
                CrudeImportPlanViewModel dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<CrudeImportPlanViewModel>(item);
                }

                if (data_status == "sendbook") {
                    isRunDB = UpdateSendToBooking(dataDetail);
                }

                if (isRunDB)
                {
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                }


                //ExtendValue extTripNo = new ExtendValue();
                //extTripNo.value = tripNo;
                ////Trip no.
                //etxValue.Add(CPAIConstantUtil.PurchaseNumber, extTripNo);

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIImportPlanEditDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIImportPlanEditDataState # :: Exception >>> " + ex);
                log.Error("CPAIImportPlanEditDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateSendToBooking(CrudeImportPlanViewModel dataDetail)
        {
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    CRUDE_IMPORT_PLAN_DAL dataDAL = new CRUDE_IMPORT_PLAN_DAL();

                    foreach (var itemHead in dataDetail.crude_approve_detail.dDetail_Header) {
                        dataDAL.UpdateSendToBooking(itemHead.PHE_TRIP_NO, context);
                    }
                }

                isSuccess = true;
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }


            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            //try
            //{
            //    log.Info("# Start State CPAIImportPlanEditDataState >> UpdateDB #  ");
            //    using (var context = new EntityCPAIEngine())
            //    {
            //        using (var dbContextTransaction = context.Database.BeginTransaction())
            //        {
            //            try
            //            {
            //                //update CPAI_TCE_MK
            //                CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
            //                CPAI_TCE_MK data = new CPAI_TCE_MK();
            //                data.CTM_ROW_ID = TransID;
            //                data.CTM_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
            //                data.CTM_UPDATED_DATE = DateTime.Now;
            //                data.CTM_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
            //                dataDAL.Update(data);
            //                isSuccess = true;
            //            }
            //            catch (Exception ex)
            //            {
            //                string res = ex.Message;
            //                dbContextTransaction.Rollback();
            //                isSuccess = false;
            //            }
            //        }
            //    }
            //    log.Info("# End State CPAIImportPlanEditDataState >> UpdateDB # ");
            //}
            //catch (Exception ex)
            //{
            //    log.Info("# Error CPAIImportPlanEditDataState >> UpdateDB # :: Exception >>> " + ex);
            //    string res = ex.Message;
            //    isSuccess = false;
            //}
            return isSuccess;
        }
    }
}