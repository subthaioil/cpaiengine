﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;

namespace ProjectCPAIEngine.Flow.F10000030
{
    public class CPAIChangePOState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIChangePOState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

            try
            {
                if (etxValue.GetValue(CPAIConstantUtil.Status).ToLower() == "change")
                {
                    ConfigManagement configManagement = new ConfigManagement();
                    String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
                    String content = stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

                    PurchasingChangeServiceConnectorImpl POService = new PurchasingChangeServiceConnectorImpl();
                    //PurchasingChangeModel model = CreateModel(poViewModel, false);
                    DownstreamResponse<string> res = POService.connect(config, content);
                    

                    stateModel.BusinessModel.currentCode = res.resultCode;
                    respCodeManagement.setCurrentCodeMapping(stateModel);
                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIChangePOState # :: Exception >>> " + ex);
                log.Error("CPAIChangePOState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
       
    }

}
