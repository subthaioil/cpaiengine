﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000031
{
    public class CPAIGeneratePOSetRespState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIChangeCancelPOSetRespState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                CreatePOGoodIntransit tempData = new CreatePOGoodIntransit();
                tempData = JSonConvertUtil.jsonToModel<CreatePOGoodIntransit>(item);

                JavaScriptSerializer js = new JavaScriptSerializer();
                string newXml = js.Serialize(tempData.ItemList);
                ExtendValue respTrans = new ExtendValue();
                respTrans.value = newXml;
                respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;

                etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIChangeCancelPOSetRespState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIChangeCancelPOSetRespState # :: Exception >>> " + ex);
                log.Error("CPAIChangeCancelPOSetRespState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

    }
}
