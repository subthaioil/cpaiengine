﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000034
{
    public class CPAISchCMCSRootwayActionState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISchCMCSRootwayActionState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);

                if (action.Equals(CPAIConstantUtil.ACTION_DRAFT))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_DRAFT_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_CANCEL))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_CANCEL_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_EDIT_SCH_RESP_CODE;
                }
                else if (action.Equals(CPAIConstantUtil.ACTION_EDIT_ACT))
                {
                    currentCode = CPAIConstantRespCodeUtil.ACTION_EDIT_ACT_RESP_CODE;
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAISchCMCSRootwayActionState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAISchCMCSRootwayActionState # :: Exception >>> " + ex);
                log.Error("CPAISchCMCSRootwayActionState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}