﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000034
{
    public class CPAISchCMCSUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISchCMCSUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch);

                SchsRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<SchsRootObject>(item);
                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string User = etxValue.GetValue(CPAIConstantUtil.User);
                    if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL) ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_ACT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
                        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))

                    {
                        #region add data history
                        //add data history
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = User;
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_ACT))
                        {
                            dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act);
                        }
                        else
                        {
                            dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch);
                        }
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = User;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = User;
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Set detail to Value
                        string sVessel = dataDetail.schs_detail.vessel != null ? dataDetail.schs_detail.vessel : "";
                        string sTC = dataDetail.schs_detail.tc != null ? dataDetail.schs_detail.tc : "";
                        string date_start = dataDetail.schs_detail.date_start;
                        string date_end = dataDetail.schs_detail.date_end;
                        string remark = dataDetail.schs_detail.note != null ? dataDetail.schs_detail.note : "";
                        string sStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                        #endregion

                        //Check Schedule+Activity
                        #region Check Schedule+Activity


                        if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) || currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))
                        {
                            CPAI_VESSEL_SCH_S_DAL schsDAL = new CPAI_VESSEL_SCH_S_DAL();
                            //string tmpdate_start = _FN.ConvertDateFormat(dataDetail.schs_detail.date_start, true).Replace("-", "/");
                            //string tmpdate_end = _FN.ConvertDateFormat(dataDetail.schs_detail.date_end, true).Replace("-", "/");

                            string tmpdate_start = dataDetail.schs_detail.date_start;
                            string tmpdate_end = dataDetail.schs_detail.date_end;

                            bool isValidateSchedule = schsDAL.checkValidSchedule(stateModel.EngineModel.ftxTransId, sVessel, CPAIConstantUtil.ACTION_SUBMIT, tmpdate_start, tmpdate_end);
                            if (!isValidateSchedule)
                            {
                                currentCode = CPAIConstantRespCodeUtil.CANNOT_VALIDATE_SCHEDULE_CODE;
                                stateModel.BusinessModel.currentCode = currentCode;
                                respCodeManagement.setCurrentCodeMapping(stateModel);
                                return;
                            }


                            if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH))
                            {
                                bool isValidateActivity = schsDAL.checkValidActivity(stateModel.EngineModel.ftxTransId);
                                if (!isValidateActivity)
                                {
                                    currentCode = CPAIConstantRespCodeUtil.CANNOT_VALIDATE_ACTIVITY_CODE;
                                    stateModel.BusinessModel.currentCode = currentCode;
                                    respCodeManagement.setCurrentCodeMapping(stateModel);
                                    return;
                                }
                            }
                        }

                        #endregion


                        #region Insert Update DB
                        bool isRunDB = false;
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
                        {
                            //OTHER ==> UPDATE
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue);
                        }
                        else if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_ACT))
                        {
                            var itemAct = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act);
                            if (itemAct != null)
                            {

                                SchsRootObject dataActDetail = JSonConvertUtil.jsonToModel<SchsRootObject>(itemAct);
                                isRunDB = InsertDBAct(stateModel.EngineModel.ftxTransId, etxValue, dataActDetail);
                            }
                        }
                        else
                        {
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                        }
                        #endregion


                        //check db by next status
                        #region INSERT JSON
                        if (isRunDB)
                        {
                            //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                            #region Set dataDetail
                            //set status
                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            //set action
                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            //set data_detail_sch
                            string dataDetailString = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_sch);
                            if (!string.IsNullOrEmpty(dataDetailString))
                            {
                                etxValue.SetValue(CPAIConstantUtil.Data_detail_sch, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                            }
                            //set data_detail_act
                            string dataDetailACTString = etxValue.GetValue(CPAIConstantUtil.Data_detail_input_act);
                            if (!string.IsNullOrEmpty(dataDetailACTString))
                            {
                                etxValue.SetValue(CPAIConstantUtil.Data_detail_act, new ExtendValue { value = dataDetailACTString, encryptFlag = "N" });
                            }
                            // if ((currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
                            //        currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) ||
                            //        currentAction.Equals(CPAIConstantUtil.ACTION_EDIT_SCH) ||
                            //        currentAction.Equals("-")) &&
                            //         !string.IsNullOrEmpty(dataDetail.sch_detail.vessel) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.vendor) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.vendor_color) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.date_start) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.date_end) &&
                            //!string.IsNullOrEmpty(dataDetail.sch_detail.note))


                            //set data
                            etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = sVessel, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.TC, new ExtendValue { value = sTC, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Date_Start, new ExtendValue { value = date_start, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Date_End, new ExtendValue { value = date_end, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.REMARK, new ExtendValue { value = remark, encryptFlag = "N" });

                            #endregion

                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                        }
                        #endregion
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAISchCMCSUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAISchCMCSUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAISchCMCSUpdateDataState::Exception >>> ", ex);
                //log.Error("CPAIUpdateDataState::Exception >>> ", e);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, SchsRootObject dataDetail)
        {
            log.Info("# Start State CPAISchCMCSUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);
                            CPAI_VESSEL_SCH_S_DAL dataDAL = new CPAI_VESSEL_SCH_S_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_VESSEL_SCH_S

                            CPAI_VESSEL_SCH_S dataSCH = new CPAI_VESSEL_SCH_S();
                            dataSCH.VSDS_ROW_ID = TransID;
                            dataSCH.VSDS_FK_VEHICLE = dataDetail.schs_detail.vessel;
                           
                            if (!string.IsNullOrEmpty(dataDetail.schs_detail.date_start))
                                dataSCH.VSDS_DATE_START = DateTime.ParseExact(dataDetail.schs_detail.date_start, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.schs_detail.date_end))
                                dataSCH.VSDS_DATE_END = DateTime.ParseExact(dataDetail.schs_detail.date_end, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                           
                            dataSCH.VSDS_TC = dataDetail.schs_detail.tc;
                            dataSCH.VSDS_NOTE = dataDetail.schs_detail.note;
                            dataSCH.VSDS_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataSCH.VSDS_CREATED_DATE = dtNow;
                            dataSCH.VSDS_CREATED_BY = User;
                            dataSCH.VSDS_UPDATED_DATE = dtNow;
                            dataSCH.VSDS_UPDATED_BY = User;
                            dataDAL.Save(dataSCH, context);

                            #region CPAI_VESSEL_SCH_S_PORT

                            CPAI_VESSEL_SCH_S_PORT_DAL dataSCHPortDAL = new CPAI_VESSEL_SCH_S_PORT_DAL();
                            CPAI_VESSEL_SCH_S_PORT dataSCHPort;
                            foreach (var item in dataDetail.schs_detail.schs_load_ports)
                            {
                                dataSCHPort = new CPAI_VESSEL_SCH_S_PORT();

                                dataSCHPort.VSPS_ROW_ID = Guid.NewGuid().ToString("N");
                                dataSCHPort.VSPS_FK_VESSEL_SCH_S = TransID;
                                dataSCHPort.VSPS_ORDER_PORT = item.order;
                                dataSCHPort.VSPS_FK_PORT = item.port != "" ? Convert.ToDecimal(item.port) : 0;//??
                                dataSCHPort.VSPS_PORT_TYPE = "L";
                                dataSCHPort.VSPS_CREATED = dtNow;
                                dataSCHPort.VSPS_CREATED_BY = User;
                                dataSCHPort.VSPS_UPDATED = dtNow;
                                dataSCHPort.VSPS_UPDATED_BY = User;
                                dataSCHPortDAL.Save(dataSCHPort, context);

                                #region CPAI_VESSEL_SCH_S_PRODUCTS
                                CPAI_VESSEL_SCH_S_PRODUCTS_DAL dataSCHProductDAL = new CPAI_VESSEL_SCH_S_PRODUCTS_DAL();
                                CPAI_VESSEL_SCH_S_PRODUCTS dataSCHProduct;
                                foreach (var itemP in item.schs_crude)
                                {
                                    dataSCHProduct = new CPAI_VESSEL_SCH_S_PRODUCTS();
                                    dataSCHProduct.VSPS_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataSCHProduct.VSPS_FK_VESSEL_SCH_S_PORT = dataSCHPort.VSPS_ROW_ID;
                                    dataSCHProduct.VSPS_FK_MATERIAL = itemP.crude;
                                    dataSCHProduct.VSPS_QTY = itemP.qty;

                                    if (!string.IsNullOrEmpty(itemP.nominate_date_start))
                                        dataSCHProduct.VSPS_NOMINATE_DATE_START = DateTime.ParseExact(itemP.nominate_date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    if (!string.IsNullOrEmpty(itemP.nominate_date_end))
                                        dataSCHProduct.VSPS_NOMINATE_DATE_END = DateTime.ParseExact(itemP.nominate_date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    if (!string.IsNullOrEmpty(itemP.received_date_start))
                                        dataSCHProduct.VSPS_RECEIVED_DATE_START = DateTime.ParseExact(itemP.received_date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    if (!string.IsNullOrEmpty(itemP.received_date_end))
                                        dataSCHProduct.VSPS_RECEIVED_DATE_END = DateTime.ParseExact(itemP.received_date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                                    dataSCHProduct.VSPS_CREATED = dtNow;
                                    dataSCHProduct.VSPS_CREATED_BY = User;
                                    dataSCHProduct.VSPS_UPDATED = dtNow;
                                    dataSCHProduct.VSPS_UPDATED_BY = User;
                                    dataSCHProductDAL.Save(dataSCHProduct, context);
                                }
                                #endregion
                            }

                            foreach (var item in dataDetail.schs_detail.schs_dis_ports)
                            {
                                dataSCHPort = new CPAI_VESSEL_SCH_S_PORT();

                                dataSCHPort.VSPS_ROW_ID = Guid.NewGuid().ToString("N");
                                dataSCHPort.VSPS_FK_VESSEL_SCH_S = TransID;
                                dataSCHPort.VSPS_ORDER_PORT = item.order;
                                dataSCHPort.VSPS_FK_PORT = item.port != "" ? Convert.ToDecimal(item.port) : 0;//??
                                dataSCHPort.VSPS_PORT_TYPE = "D";
                                dataSCHPort.VSPS_CREATED = dtNow;
                                dataSCHPort.VSPS_CREATED_BY = User;
                                dataSCHPort.VSPS_UPDATED = dtNow;
                                dataSCHPort.VSPS_UPDATED_BY = User;
                                dataSCHPortDAL.Save(dataSCHPort, context);
                            }
                            #endregion



                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAISchCMCSUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAISchCMCSUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_VESSEL_SCH_S
                            CPAI_VESSEL_SCH_S_DAL dataDAL = new CPAI_VESSEL_SCH_S_DAL();
                            CPAI_VESSEL_SCH_S dataCHI = new CPAI_VESSEL_SCH_S();
                            dataCHI.VSDS_ROW_ID = TransID;
                            dataCHI.VSDS_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCHI.VSDS_UPDATED_DATE = DateTime.Now;
                            dataCHI.VSDS_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCHI, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            log.Info("# Error CPAISchCMCSUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAISchCMCSUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAISchCMCSUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool InsertDBAct(string TransID, Dictionary<string, ExtendValue> etxValue, SchsRootObject dataDetail)
        {
            log.Info("# Start State CPAISchCMCSUpdateDataState >> InsertDBAct #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);
                            CPAI_VESSEL_SCH_S_ACTIVITYS_DAL dataDAL = new CPAI_VESSEL_SCH_S_ACTIVITYS_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_VESSEL_SCHEDULE_ACTIVITYS
                            CPAI_VESSEL_SCH_S_ACTIVITYS dataSCH;
                            foreach (var item in dataDetail.schs_activitys)
                            {
                                dataSCH = new CPAI_VESSEL_SCH_S_ACTIVITYS();

                                dataSCH.VASS_ROW_ID = Guid.NewGuid().ToString("N");
                                dataSCH.VASS_FK_VESSEL_SCH_S = TransID;
                                dataSCH.VASS_FK_VESSEL_ACTIVITY = item.activity;
                                dataSCH.VASS_ORDER = item.order;
                                //if (!string.IsNullOrEmpty(item.date))
                                dataSCH.VASS_DATE_START = DateTime.ParseExact(item.date_start, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                if(item.date_end != null)
                                {
                                    dataSCH.VASS_DATE_END = DateTime.ParseExact(item.date_end, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                                }                                
                                dataSCH.VASS_PORT = item.portName;
                                dataSCH.VASS_REF = item.@ref;
                                dataSCH.VASS_CREATED = dtNow;
                                dataSCH.VASS_CREATED_BY = User;
                                dataSCH.VASS_UPDATED = dtNow;
                                dataSCH.VASS_UPDATED_BY = User;
                                dataDAL.Save(dataSCH, context);
                                isSuccess = true;
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAISchCMCSUpdateDataState >> InsertDBAct #");
            return isSuccess;
        }

    }
}