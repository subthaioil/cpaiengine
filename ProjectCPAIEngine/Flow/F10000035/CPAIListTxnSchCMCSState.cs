﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Flow.F10000035
{
    public class CPAIListTxnSchCMCSState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIListTxnSchCMCSState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();

                //fix function code = schedule
                List<CPAI_ACTION_FUNCTION> result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, CPAIConstantUtil.SCHEDULE_S_FUNCTION_CODE);//set user group deligate by poo 25072017


                List<Transaction> lstAllTx = new List<Transaction>();
                if (result.Count == 0)
                {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                else if (result.Count == 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else
                {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER))
                        {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null)
                    {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    }
                    else
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                        }
                        setExtraXml(stateModel, lstAllTx);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIListTxnSchCMCSState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIListTxnSchCMCSState # :: Exception >>> " + ex);
                log.Error("CPAIListTxnSchCMCSState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public List<Transaction> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction)
        {
            log.Info("# Start State CPAIListTxnSchCMCSState >> getTransaction #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
            List<FunctionTransaction> result;
            List<Transaction> list = new List<Transaction>();

            string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
            string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);
            string status = etxValue.GetValue(CPAIConstantUtil.Status);

            List<string> lstStatus = new List<string>();

            if (string.IsNullOrWhiteSpace(status))
            {
                if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS))
                {
                    lstStatus.Add("ALL");
                }
                else
                {
                    status = actionFunction.ACF_FUNCTION_STATUS;
                    lstStatus = new List<string>(status.Split('|'));
                }
            }
            else
            {
                lstStatus = new List<string>(status.Split('|'));
            }

            // create index to make search criteria
            var lstIndexValues = new List<string>();
            lstIndexValues.Add(actionFunction.ACF_SYSTEM);
            lstIndexValues.Add(actionFunction.ACF_TYPE);
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index3));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index4));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index5));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index6));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index7));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index8));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index9));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index10));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index11));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index12));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index13));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index14));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index15));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index16));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index17));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index18));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index19));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index20));

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                result = ftxMan.findCPAISchCMCSTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, fromDate, toDate);
            }
            else
            {
                result = ftxMan.findCPAISchCMCSTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, null, null);
            }
            for (int i = 0; i < result.Count; i++)
            {
                FunctionTransaction ftx = result[i];
                Transaction m = new Transaction();
                m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                m.vessel_id = ftx.FUNCTION_TRANSACTION.FTX_INDEX5 == null ? "" : ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                m.vessel = ftx.VESSEL_NAME == null ? "" : ftx.VESSEL_NAME;
                m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                m.tc = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                m.remark = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                m.reason = getReason(ftx.FUNCTION_TRANSACTION.FTX_ROW_ID);
                m.date_start = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                m.date_end = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                m.vessel_activity = SchedulerServiceModel.getTransactionCMCSActByID(ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                list.Add(m);
            }
            log.Info("# End State CPAIListTxnSchCMCSState >> getTransaction # ");
            return list;
        }

        public void setExtraXml(StateModel stateModel, List<Transaction> lstAllTx)
        {
            log.Info("# Start State CPAIListTxnSchCMCSState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            TransactionList li = new TransactionList();
            li.trans = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAIListTxnSchCMCSState >> setExtraXml #");
        }

        public string getReason(string row_id)
        {
            log.Info("# Start State CPAIListTxnSchCMCSState >> getReason #  ");
            string reason = "-";
            ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
            var etxResult = etxMan.findByFtxRowIdAndKey(row_id, CPAIConstantUtil.Note);
            if (etxResult != null && etxResult.Count > 0)
            {
                reason = etxResult[0].ETX_VALUE;
            }
            log.Info("# End State CPAIListTxnSchCMCSState >> getReason #");
            return reason;
        }
    }
}