﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Flow.F10000037
{
    public class CPAIHedgingDealCommitteeFrameworkVerifyState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingDealCommitteeFrameworkVerifyState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                HedgDealRootObject dataDetail = null;

                bool isRunDB = false;
                string VerifyStatus = "";
                string VerifyReason = "";
                string VerifyFile = "";
                string VerifyRemark = "";
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<HedgDealRootObject>(item);
                    if (dataDetail.hedging_deal.verify != null)
                    {
                        VerifyReason = dataDetail.hedging_deal.verify.committee_framework_reason;
                        VerifyFile = dataDetail.hedging_deal.verify.committee_framework_file;
                        VerifyStatus = dataDetail.hedging_deal.verify.committee_framework;
                        VerifyRemark = dataDetail.hedging_deal.verify.committee_framework_remark;
                    }
                    else
                    {
                        dataDetail.hedging_deal.verify = new Areas.CPAIMVC.ViewModels.Verify();
                    }
                    // Bypass
                    if (dataDetail.hedging_deal.status == CPAIConstantUtil.STATUS_HOLD)
                    {
                        if (!string.IsNullOrEmpty(VerifyReason))
                        {
                            VerifyStatus = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            VerifyRemark = "";
                        }
                    }
                    else
                    {
                        decimal price = string.IsNullOrEmpty(dataDetail.hedging_deal.price) == true ? 0 : Convert.ToDecimal(dataDetail.hedging_deal.price);
                        ReturnValue rtn = new ReturnValue();
                        HedgingDealServiceModel service = new HedgingDealServiceModel();

                        rtn = service.VerifyCommitteeFramework(dataDetail.hedging_deal.counter_parties, price);
                        if (rtn.Status == false)
                        {
                            // SET Verify flag to "NO" is SUBMIT : "YES" is HOLD
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_Verify_Flag, new ExtendValue { value = CPAIConstantUtil.YES_FLAG, encryptFlag = "N" });
                            VerifyStatus = CPAIConstantUtil.Hedg_Verify_Status_NotPass;
                            VerifyRemark = rtn.Message;
                        }
                        else
                        {
                            VerifyStatus = CPAIConstantUtil.Hedg_Verify_Status_Pass;
                            VerifyRemark = rtn.Message;
                        }
                    }

                    isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, VerifyStatus, VerifyRemark, VerifyReason, VerifyFile, etxValue);

                    if (isRunDB)
                    {
                        dataDetail.hedging_deal.verify.committee_framework_remark = VerifyRemark;
                        dataDetail.hedging_deal.verify.committee_framework = VerifyStatus;
                        dataDetail.hedging_deal.verify.committee_framework_file = VerifyFile;
                        dataDetail.hedging_deal.verify.committee_framework_reason = VerifyReason;

                        string dataDetailString = JSonConvertUtil.modelToJson(dataDetail);
                        etxValue.SetValue(CPAIConstantUtil.DataDetailInput, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }

                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingDealCommitteeFrameworkVerifyState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingDealCommitteeFrameworkVerifyState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingDealCommitteeFrameworkVerifyState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateDB(string TransID, string VerifyStatus, string VerifyRemark, string VerifyReason, string VerifyFile, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIHedgingDealCommitteeFrameworkVerifyState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            HEDG_DEAL_DATA dataHDDA = new HEDG_DEAL_DATA();
                            HEDG_DEAL_DATA_DAL dataDAL = new HEDG_DEAL_DATA_DAL();
                            dataHDDA.HDDA_ROW_ID = TransID;
                            dataHDDA.HDDA_VRF_COMMITTEE_FW_STATUS = VerifyStatus;
                            dataHDDA.HDDA_VRF_COMMITTEE_FW_REASON = VerifyReason;
                            dataHDDA.HDDA_VRF_COMMITTEE_FW_REMARK = VerifyRemark;
                            dataHDDA.HDDA_VRF_COMMITTEE_FW_FILE = VerifyFile;
                           
                            dataDAL.UpdateVerifyCommitteeFW(dataHDDA, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIHedgingDealCommitteeFrameworkVerifyState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIHedgingDealCommitteeFrameworkVerifyState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingDealCommitteeFrameworkVerifyState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}