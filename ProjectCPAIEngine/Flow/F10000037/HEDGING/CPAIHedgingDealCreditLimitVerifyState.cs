﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Utilities;

using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Flow.F10000037
{
    public class CPAIHedgingDealCreditLimitVerifyState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingDealCreditLimitVerifyState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                HedgDealRootObject dataDetail = null;

                bool isRunDB = false;
                string VerifyStatus = "";
                string VerifyReason = "";
                string VerifyFile = "";
                string VerifyRemark = "";

                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<HedgDealRootObject>(item);

                    if (dataDetail.hedging_deal.verify != null)
                    {
                        VerifyReason = dataDetail.hedging_deal.verify.credit_limit_reason;
                        VerifyFile = dataDetail.hedging_deal.verify.credit_limit_file;
                        VerifyStatus = dataDetail.hedging_deal.verify.credit_limit;
                        VerifyRemark = dataDetail.hedging_deal.verify.credit_limit_remark;
                    }
                    else
                    {
                        dataDetail.hedging_deal.verify = new Areas.CPAIMVC.ViewModels.Verify();
                    }
                    // Bypass
                    if (dataDetail.hedging_deal.status == CPAIConstantUtil.STATUS_HOLD )
                    {
                        if(!string.IsNullOrEmpty(VerifyReason))
                        {
                            VerifyStatus = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            VerifyRemark = "";
                        }
                    }
                    else
                    {
                        decimal vol = string.IsNullOrEmpty(dataDetail.hedging_deal.volume_month) == true ? 0 : Convert.ToDecimal(dataDetail.hedging_deal.volume_month);
                        ReturnValue rtn = new ReturnValue();
                        HedgingDealServiceModel service = new HedgingDealServiceModel();

                        rtn = service.VerifyCreditLimit(dataDetail.hedging_deal.counter_parties, vol);
                        if (rtn.Status == false)
                        {
                            // SET Verify flag to "NO" is SUBMIT : "YES" is HOLD
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_Verify_Flag, new ExtendValue { value = CPAIConstantUtil.YES_FLAG, encryptFlag = "N" });
                            VerifyStatus = CPAIConstantUtil.Hedg_Verify_Status_NotPass;
                            VerifyRemark = rtn.Message;

                        }
                        else
                        {
                            VerifyStatus = CPAIConstantUtil.Hedg_Verify_Status_Pass;
                            VerifyRemark = rtn.Message;
                        }
                    }

                    isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, VerifyStatus, VerifyRemark, VerifyReason, VerifyFile, etxValue);

                    if (isRunDB)
                    {
                        
                        dataDetail.hedging_deal.verify.credit_limit_remark = VerifyRemark;
                        dataDetail.hedging_deal.verify.credit_limit = VerifyStatus;
                        dataDetail.hedging_deal.verify.credit_limit_file = VerifyFile;
                        dataDetail.hedging_deal.verify.credit_limit_reason = VerifyReason;

                        string dataDetailString = JSonConvertUtil.modelToJson(dataDetail);
                        etxValue.SetValue(CPAIConstantUtil.DataDetailInput, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }

                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }


                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingDealCreditLimitVerifyState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingDealCreditLimitVerifyState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingDealCreditLimitVerifyState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateDB(string TransID, string VerifyStatus, string VerifyRemark, string VerifyReason, string VerifyFile, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIHedgingDealCreditLimitVerifyState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_HEDG_DAIL 
                            HEDG_DEAL_DATA dataHDDA = new HEDG_DEAL_DATA();
                            HEDG_DEAL_DATA_DAL dataDAL = new HEDG_DEAL_DATA_DAL();
                            dataHDDA.HDDA_ROW_ID = TransID;
                            dataHDDA.HDDA_VRF_CREDIT_LM_STATUS = VerifyStatus;
                            dataHDDA.HDDA_VRF_CREDIT_LM_REMARK = VerifyRemark;
                            dataHDDA.HDDA_VRF_CREDIT_LM_REASON = VerifyReason;
                            dataHDDA.HDDA_VRF_CREDIT_LM_FILE = VerifyFile;
                         
                            dataDAL.UpdateVerifyCreditLimit(dataHDDA, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIHedgingDealCreditLimitVerifyState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIHedgingDealCreditLimitVerifyState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingDealCreditLimitVerifyState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}