﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DSMail.model;
using DSMail.service;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.downstream;
using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;

namespace ProjectCPAIEngine.Flow.F10000037
{
    public class CPAIHedgingSendMailState : BasicBean, StateFlowAction
    {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingSendMailState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string tCurrentStatus = etxValue.GetValue(CPAIConstantUtil.Status);

                Dictionary<string, string> tActionMapping = new Dictionary<string, string>();
                tActionMapping.Add("WAITING VERIFY", "VERIFY");
                tActionMapping.Add("WAITING ENDORSE", "ENDORSE");
                tActionMapping.Add("WAITING APPROVE", "APPROVE");

                string tTicketStage = "";

                ActionMessageDAL amsMan = new ActionMessageDAL();

                List<CPAI_ACTION_MESSAGE> results;
                if (system.Equals (ConstantPrm.SYSTEM.HEDG_TCKT) && action.Equals(CPAIConstantUtil.ACTION_REJECT) || action.Equals(CPAIConstantUtil.ACTION_CANCEL))
                {
                    tTicketStage = tActionMapping.SingleOrDefault(p => p.Key == tCurrentStatus).Value;
                    results = amsMan.findUserGroupMail(action+"_"+ tTicketStage , system, type, stateModel.EngineModel.functionId);
                }
                else {
                    results = amsMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId);
                }
                

                if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else
                {
                    HEDG_DEAL_DATA_DAL DEAL_DAL = new HEDG_DEAL_DATA_DAL();
                    HEDG_DEAL_DATA data = DEAL_DAL.GetDEALDATAByID(etxValue.GetValue(CPAIConstantUtil.DocNo));
                    for (int i = 0; i < results.Count; i++)
                    {
                        string jsonText = Regex.Replace(results[i].AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
                        MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
                        MailServiceModel service = new MailServiceModel();
                        string data_detail = etxValue.GetValue(CPAIConstantUtil.TransactionId);
                        string deal_no = etxValue.GetValue(CPAIConstantUtil.DocNo);
                        detail.subject = mms.getsubjectF10000037(detail.subject, etxValue.GetValue(CPAIConstantUtil.DataDetailInput), deal_no);
                        detail.body = mms.getBodyF10000037(detail.body, etxValue.GetValue(CPAIConstantUtil.DataDetailInput), deal_no);

                        if (system.Equals(ConstantPrm.SYSTEM.HEDG_TCKT)) {
                            detail.subject = detail.subject.Replace("#TICKET_NO", data.HDDA_TICKET_NO);
                            detail.body = detail.subject.Replace("#TICKET_STAGE", tTicketStage);
                        }

                        detail.createby = etxValue.GetValue(CPAIConstantUtil.CreateBy);
                        detail.system = etxValue.GetValue(CPAIConstantUtil.System);
                        detail.type = etxValue.GetValue(CPAIConstantUtil.Type);
                        detail.user = etxValue.GetValue(CPAIConstantUtil.User);
                        currentCode = service.findUserAndSendMail(stateModel, results[i], ref detail);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingSendMailState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingSendMailState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingSendMailState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}