﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000038
{
    public class CPAIHedgingTicketRootwayActionState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingTicketRootwayActionState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);

                if (action.Equals(CPAIConstantUtil.ACTION_DRAFT)) {
                    //10000021
                    currentCode = CPAIConstantRespCodeUtil.ACTION_DRAFT_RESP_CODE;
                } else if (action.Equals(CPAIConstantUtil.ACTION_CANCEL)) {
                    //10000022
                    currentCode = CPAIConstantRespCodeUtil.ACTION_CANCEL_RESP_CODE;
                }else if (action.Equals(CPAIConstantUtil.ACTION_APPROVE_1)||
                    action.Equals(CPAIConstantUtil.ACTION_APPROVE_2)||
                    action.Equals(CPAIConstantUtil.ACTION_APPROVE_3) ||
                    action.Equals(CPAIConstantUtil.ACTION_APPROVE_4)
                    )
                {
                    //10000020
                    currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_RESP_CODE;
                }else if (action.Equals(CPAIConstantUtil.ACTION_REJECT))
                {
                    //10000023
                    currentCode = CPAIConstantRespCodeUtil.ACTION_REJECT_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingTicketRootwayActionState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingTicketRootwayActionState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingTicketRootwayActionState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}