﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.dao;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000039
{
    public class CPAIListTxnCoolState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIListTxnCoolState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            //string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();
                List<CPAI_ACTION_FUNCTION> result;

                if (String.IsNullOrWhiteSpace(function_code))
                {
                    //result = acfMan.findByUserGroupAndSystem(userGroup, system);
                    result = acfMan.findByUserGroupAndSystemDelegate(lstUserGroup, system);//set user group deligate by job 25072017
                }
                else
                {
                    //result = acfMan.findByUserGroupAndSystemAndFuncitonId(userGroup, system, function_code);
                    result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, function_code);//set user group deligate by job 25072017
                }

                List<Transaction> lstAllTx = new List<Transaction>();
                if (result.Count == 0)
                {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                else if (result.Count == 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else if (result.Count >= 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = new CPAI_ACTION_FUNCTION();
                    if (lstUserGroup.Contains("SCEP"))
                    {
                        actionFunction = result.Where(x => x.ACF_FK_FUNCTION == "40").FirstOrDefault();
                    } else
                    {
                        actionFunction = result.Where(x => x.ACF_FK_FUNCTION != "40").FirstOrDefault();
                    }
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else
                {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER))
                        {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null)
                    {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    }
                    else
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                            if (acf.ACF_USR_GROUP == "SCEP") break;
                        }
                        setExtraXml(stateModel, lstAllTx);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIListTxnCoolState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIListTxnCoolState # :: Exception >>> " + ex);
                log.Error("CPAIListTxnCoolState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public List<Transaction> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction)
        {
            log.Info("# Start State CPAIListTxnCoolState >> getTransaction #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
            List<FunctionTransaction> result;
            List<Transaction> list = new List<Transaction>();

            string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
            string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);
            string status = etxValue.GetValue(CPAIConstantUtil.Status);
            string mobileFlag = etxValue.GetValue(CPAIConstantUtil.MobileFlag);
            string UserName = etxValue.GetValue(CPAIConstantUtil.User);

            List<string> lstStatus = new List<string>();

            if (string.IsNullOrWhiteSpace(status))
            {
                if (mobileFlag.Equals(CPAIConstantUtil.YES_FLAG))
                {
                    if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS_MB))
                    {
                        lstStatus.Add("ALL");
                    }
                    else
                    {
                        status = actionFunction.ACF_FUNCTION_STATUS_MB;
                        lstStatus = new List<string>(status.Split('|'));
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS))
                    {
                        lstStatus.Add("ALL");
                    }
                    else
                    {
                        status = actionFunction.ACF_FUNCTION_STATUS;
                        lstStatus = new List<string>(status.Split('|'));
                    }
                }

            }
            else
            {
                lstStatus = new List<string>(status.Split('|'));
            }

            // create index to make search criteria
            var lstIndexValues = new List<string>();
            lstIndexValues.Add(actionFunction.ACF_SYSTEM);
            lstIndexValues.Add(actionFunction.ACF_TYPE);
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index3));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index4));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index5));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index6));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index7));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index8));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index9));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index10));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index11));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index12));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index13));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index14));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index15));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index16));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index17));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index18));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index19));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index20));


            List<string> units = etxValue.GetValue(CPAIConstantUtil.Units) == null ? new List<string>() : etxValue.GetValue(CPAIConstantUtil.Units).Split('|').ToList();
            List<string> areas = etxValue.GetValue(CPAIConstantUtil.Areas) == null ? new List<string>() : etxValue.GetValue(CPAIConstantUtil.Areas).Split('|').ToList();
            bool isTracking = etxValue.GetValue(CPAIConstantUtil.Tracking) != null ? etxValue.GetValue(CPAIConstantUtil.Tracking).ToString() == "T" ? true : false : false;
            bool isBoarding = etxValue.GetValue(CPAIConstantUtil.Boarding) != null ? etxValue.GetValue(CPAIConstantUtil.Boarding).ToString() == "Y" ? true : false : false;

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                result = ftxMan.findCPAICoolTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, fromDate, toDate, units, areas, isTracking, UserName, isBoarding);
            }
            else
            {
                result = ftxMan.findCPAICoolTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, null, null, units, areas, isTracking, UserName, isBoarding);
            }

            if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.COOL_FUNCTION_CODE) || actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.COOL_EXPERT_FUNCTION_CODE)
                || actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.COOL_EXPERT_SH_FUNCTION_CODE))
            {
                for (int i = 0; i < result.Count; i++)
                {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.assay_ref = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.products = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                    m.origin = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    m.categories = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                    m.kerogen = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                    m.characteristic = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                    m.maturity = ftx.FUNCTION_TRANSACTION.FTX_INDEX17;
                    m.assay_date = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                    m.assay_from = ftx.ASSAY_FROM;
                    m.assay_file = ftx.ASSAY_FILE;
                    m.support_doc_file = ftx.SUPPORT_FILE;
                    m.density = ftx.DENSITY;
                    m.total_sulphur = ftx.SULPHUR;
                    m.total_acid_number = ftx.ACID;
                    m.expert_units = ftx.UNIT;
                    m.requester_name = ftx.REQUESTER_NAME;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.draft_cam_file = ftx.DRAFT_CAM_FILE;
                    m.final_cam_file = ftx.FINAL_CAM_FILE;
                    m.draft_cam_status = ftx.DRAFT_CAM_STATUS;
                    m.final_cam_status = ftx.FINAL_CAM_STATUS;
                    m.note = ftx.NOTE;
                    m.updated_date = ftx.UPDATED_DATE;
                    m.updated_by = ftx.UPDATED_BY;
                    m.experts_support_doc_file = ftx.COO_EXPERT;
                    m.created_by = ftx.CREATED_BY;
                    m.created_date = ftx.CREATED_DATE;

                    list.Add(m);
                }
                        
            }
            log.Info("# End State CPAIListTxnCoolState >> getTransaction # :: Code >>> " + currentCode);
            return list;
        }

        public void setExtraXml(StateModel stateModel, List<Transaction> lstAllTx)
        {
            log.Info("# Start State CPAIListTxnCoolState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            TransactionList li = new TransactionList();
            li.trans = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAIListTxnCoolState >> setExtraXml # :: Code >>> " + currentCode);
        }

        public string getReason(string row_id)
        {
            log.Info("# Start State CPAIListTxnCoolState >> getReason #  ");
            string reason = "-";
            ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
            var etxResult = etxMan.findByFtxRowIdAndKey(row_id, CPAIConstantUtil.Note);
            if (etxResult != null && etxResult.Count > 0)
            {
                reason = etxResult[0].ETX_VALUE;
            }

            log.Info("# End State CPAIListTxnCoolState >> getReason # :: Code >>> " + currentCode);
            return reason;
        }

        private String encryptRSAData(StateModel stateModel, string data)
        {
            log.Info("# Start State CPAIListTxnCoolState >> encryptRSAData #  ");
            AppUserDao appUserDao = new AppUserDaoImpl();
            SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
            APP_USER appUser = appUserDao.findByAppId(stateModel.EngineModel.appUser);
            if (appUser != null)
            {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PUBLIC_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                {
                    String pubKey = scn.SCN_VALUE;
                    String cipherText = data.Trim();
                    try
                    {
                        String encryptMsg = RSAHelper.EncryptText(cipherText, pubKey);
                        log.Info("# End State CPAIListTxnCoolState >> encryptRSAData # :: Code >>> " + currentCode);
                        return encryptMsg;
                    }
                    catch (Exception ex)
                    {
                        currentCode = CPAIConstantRespCodeUtil.CANNOT_ENCRYPT_MESSAGE_RESP_CODE;
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        log.Error("# Error CPAIListTxnCoolState >> encryptRSAData # :: Exception >>> " + currentCode + " : " + ex.Message);
                        throw new Exception();
                    }
                }
                else
                {
                    //can not get priKey
                    currentCode = ConstantRespCodeUtil.CAN_NOT_GET_KEY_ENGINE_RESP_CODE[0];
                    log.Error("# Error CPAIListTxnCoolState >> encryptRSAData # :: Exception >>> " + currentCode);
                    throw new Exception();
                }
            }
            else
            {
                currentCode = ConstantRespCodeUtil.NOT_FOUND_APP_USER_ENGINE_RESP_CODE[0];
                log.Error("# Error CPAIListTxnCoolState >> encryptRSAData # :: Exception >>> " + currentCode);
                throw new Exception();
            }
        }
    }
}