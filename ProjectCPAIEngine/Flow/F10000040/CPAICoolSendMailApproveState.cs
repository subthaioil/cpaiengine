﻿using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.downstream;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using DSMail.model;
using DSMail.service;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000040
{
    public class CPAICoolSendMailApproveState : BasicBean, StateFlowAction
    {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISendMailApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                string charterFor = etxValue.GetValue(CPAIConstantUtil.CharterFor);
                ActionMessageDAL amsMan = new ActionMessageDAL();

                List<CPAI_ACTION_MESSAGE> results = new List<CPAI_ACTION_MESSAGE>();
                //F42 can be approved and rejected at the same time.
                if (stateModel.EngineModel.functionId == CPAIConstantUtil.F100000042 && action == CPAIConstantUtil.ACTION_APPROVE_3)
                {
                    results.AddRange(amsMan.findUserGroupMail(CPAIConstantUtil.ACTION_APPROVE_3, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor));
                    results.AddRange(amsMan.findUserGroupMail(CPAIConstantUtil.ACTION_REJECT_3, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor));
                }
                else
                {
                    results = amsMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                }

                if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else
                {
                    for (int i = 0; i < results.Count; i++)
                    {
                        findUserAndSendMail(stateModel, results[i], system, action);
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAISendMailApproveState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAISendMailApproveState # :: Exception >>> " + ex);
                log.Error("CPAISendMailApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system, string action)
        {

            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER))
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                //List<USERS> userResult = userMan.findByUserGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroupSystem(userGroup, system);
                if (stateModel.EngineModel.functionId == CPAIConstantUtil.F100000041)
                {
                    List<USERS> userResult = new List<USERS>();
                    Dictionary<string, string> unit_user = new Dictionary<string, string>();
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                    if (userGroup == "EXPERT_SH")
                    {
                        if (dataDetail.areas != null && dataDetail.areas.area != null)
                        {
                            for (int i = 0; i < dataDetail.areas.area.Count; i++)
                            {
                                for (int j = 0; j < dataDetail.areas.area[i].unit.Count; j++)
                                {
                                    if (dataDetail.areas.area[i].unit[j].isSelected_expert == "Y" && action == actionMessage.AMS_ACTION)
                                    {
                                        List<USERS> users = userGMan.findUserMailByGroupSystem(dataDetail.areas.area[i].name, system);
                                        for (int k = 0; k < users.Count; k++)
                                        {
                                            if (userResult.Where(x => x.USR_LOGIN == users[k].USR_LOGIN).Count() == 0)
                                            {
                                                userResult.Add(users[k]);
                                            }
                                            if (unit_user.Where(x => x.Key == users[k].USR_LOGIN).Count() == 0)
                                            {
                                                unit_user.Add(users[k].USR_LOGIN, dataDetail.areas.area[i].unit[j].name);
                                            }
                                            else
                                            {
                                                unit_user[users[k].USR_LOGIN] += ", " + dataDetail.areas.area[i].unit[j].name;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } else if (userGroup == "EXPERT")
                    {
                        if (dataDetail.experts != null && dataDetail.experts.areas != null)
                        {
                            for (int i = 0; i < dataDetail.experts.areas.Count; i++)
                            {
                                for (int j = 0; j < dataDetail.experts.areas[i].units.Count; j++)
                                {
                                    if (dataDetail.experts.areas[i].units[j].value == "Y" && action == actionMessage.AMS_ACTION)
                                    {
                                        List<USERS> users = userGMan.findUserMailByGroupSystem(dataDetail.experts.areas[i].units[j].name, system);
                                        for (int k = 0; k < users.Count; k++)
                                        {
                                            if (userResult.Where(x => x.USR_LOGIN == users[k].USR_LOGIN).Count() == 0)
                                            {
                                                userResult.Add(users[k]);
                                            }
                                            if (unit_user.Where(x => x.Key == users[k].USR_LOGIN).Count() == 0)
                                            {
                                                unit_user.Add(users[k].USR_LOGIN, dataDetail.experts.areas[i].units[j].name);
                                            }
                                            else
                                            {
                                                unit_user[users[k].USR_LOGIN] += ", " + dataDetail.experts.areas[i].units[j].name;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (userResult.Count == 0)
                    {
                        // user not found
                        //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                        //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                        //throw new Exception("user not found");
                        log.Info("user not found");
                    }
                    else
                    {
                        prepareSendMailExpert(stateModel, actionMessage, userResult, unit_user);
                    }

                }
                else
                if (stateModel.EngineModel.functionId == CPAIConstantUtil.F100000042 && userGroup == "EXPERT")
                {
                    List<USERS> userResult = new List<USERS>();
                    Dictionary<string, string> unit_user = new Dictionary<string, string>();
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                    if (dataDetail.areas != null && dataDetail.areas.area != null)
                    {
                        for (int i = 0; i < dataDetail.areas.area.Count; i++)
                        {
                            for (int j = 0; j < dataDetail.areas.area[i].unit.Count; j++)
                            {
                                if (dataDetail.areas.area[i].unit[j].isSelected_expert_sh == "Y" && actionMessage.AMS_ACTION.Contains(dataDetail.areas.area[i].unit[j].next_status))
                                {
                                    List<USERS> users = userGMan.findUserMailByGroupSystem(dataDetail.areas.area[i].unit[j].name, system);
                                    for (int k = 0; k < users.Count; k++)
                                    {
                                        if (userResult.Where(x => x.USR_LOGIN == users[k].USR_LOGIN).Count() == 0)
                                        {
                                            userResult.Add(users[k]);
                                        }
                                        if (unit_user.Where(x => x.Key == users[k].USR_LOGIN).Count() == 0)
                                        {
                                            unit_user.Add(users[k].USR_LOGIN, dataDetail.areas.area[i].unit[j].name);
                                        }
                                        else
                                        {
                                            unit_user[users[k].USR_LOGIN] += ", " + dataDetail.areas.area[i].unit[j].name;
                                        }
                                    }
                                }
                            }
                        }

                        if (userResult.Count == 0)
                        {
                            // user not found
                            //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                            //throw new Exception("user not found");
                            log.Info("user not found");
                        }
                        else
                        {
                            prepareSendMailExpert(stateModel, actionMessage, userResult, unit_user);
                        }
                    }
                }
                else
                if (stateModel.EngineModel.functionId == CPAIConstantUtil.F100000040 && userGroup == "EXPERT_SH")
                {
                    List<USERS> userResult = new List<USERS>();
                    Dictionary<string, string> area_user = new Dictionary<string, string>();
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                    if (dataDetail.areas != null && dataDetail.areas.area != null)
                    {
                        for (int i = 0; i < dataDetail.areas.area.Count; i++)
                        {
                            if (dataDetail.areas.area[i].isSelected == "Y")
                            {
                                List<USERS> users = userGMan.findUserMailByGroupSystem(dataDetail.areas.area[i].name, system);
                                for (int k = 0; k < users.Count; k++)
                                {
                                    if (userResult.Where(x => x.USR_LOGIN == users[k].USR_LOGIN).Count() == 0)
                                    {
                                        userResult.Add(users[k]);
                                    }
                                    if (area_user.Where(x => x.Key == users[k].USR_LOGIN).Count() == 0)
                                    {
                                        area_user.Add(users[k].USR_LOGIN, dataDetail.areas.area[i].name);
                                    }
                                    else
                                    {
                                        area_user[users[k].USR_LOGIN] += ", " + dataDetail.areas.area[i].name;
                                    }
                                }
                            }
                        }

                        if (userResult.Count == 0)
                        {
                            // user not found
                            //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                            //throw new Exception("user not found");
                            log.Info("user not found");
                        }
                        else
                        {
                            prepareSendMailExpertSH(stateModel, actionMessage, userResult, area_user);
                        }
                    }
                }
                else
                if (stateModel.EngineModel.functionId == CPAIConstantUtil.F100000040 && userGroup == "EXPERT")
                {
                    List<USERS> userResult = new List<USERS>();
                    Dictionary<string, string> unit_user = new Dictionary<string, string>();
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(etxValue.GetValue(CPAIConstantUtil.DataDetail));
                    if (dataDetail.areas != null && dataDetail.areas.area != null)
                    {
                        for (int i = 0; i < dataDetail.areas.area.Count; i++)
                        {
                            for (int j = 0; j < dataDetail.areas.area[i].unit.Count; j++)
                            {
                                if (dataDetail.areas.area[i].unit[j].isSelected_expert_sh == "Y")
                                {
                                    List<USERS> users = userGMan.findUserMailByGroupSystem(dataDetail.areas.area[i].unit[j].name, system);
                                    for (int k = 0; k < users.Count; k++)
                                    {
                                        if (userResult.Where(x => x.USR_LOGIN == users[k].USR_LOGIN).Count() == 0)
                                        {
                                            userResult.Add(users[k]);
                                        }
                                        if (unit_user.Where(x => x.Key == users[k].USR_LOGIN).Count() == 0)
                                        {
                                            unit_user.Add(users[k].USR_LOGIN, dataDetail.areas.area[i].unit[j].name);
                                        }
                                        else
                                        {
                                            unit_user[users[k].USR_LOGIN] += ", " + dataDetail.areas.area[i].unit[j].name;
                                        }
                                    }
                                }
                            }
                        }

                        if (userResult.Count == 0)
                        {
                            // user not found
                            //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                            //throw new Exception("user not found");
                            log.Info("user not found");
                        }
                        else
                        {
                            prepareSendMailExpert(stateModel, actionMessage, userResult, unit_user);
                        }
                    }
                }
                else
                {
                    List<USERS> userResult = userGMan.findUserMailByGroupSystem(userGroup, system);

                    if (userResult.Count == 0)
                    {
                        // user not found
                        //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                        //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                        //throw new Exception("user not found");
                        log.Info("user not found");
                    }
                    else
                    {
                        prepareSendMail(stateModel, actionMessage, userResult);
                    }
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                List<USERS> userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }

        }
        public void prepareSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail)
        {

            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystemCool(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0)
            {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("login user not found");
            }


            //
            if (subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            //
            if (subject.Contains("#user"))
            {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user"))
            {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user"))
            {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user"))
            {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            //get create by
            //string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            //List<USERS> create_by = userMan.findByLogin(tem);
            //string create_by_v = "";
            //if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            //{
            //    //string last_name = create_by[0].USR_LAST_NAME_EN != null ? create_by[0].USR_LAST_NAME_EN  : "";
            //    //create_by_v = string.Format("{0} {1}", create_by[0].USR_FIRST_NAME_EN, last_name);
            //    create_by_v = create_by[0].USR_FIRST_NAME_EN;
            //}
            //if (body.Contains("#create_by"))
            //{
            //    body = body.Replace("#create_by", create_by_v);
            //}

            //cool
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000040) || stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000042))
            {
                subject = mms.getSubjectF10000040(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000040(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            //
            if (body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = body;
                foreach (USERS um in userMail)
                {
                    temp_body = body;
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    string out_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, subject, out_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void prepareSendMailExpert(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail, Dictionary<string, string> user_unit)
        {

            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystemCool(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0)
            {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("login user not found");
            }


            //
            if (subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            //
            if (subject.Contains("#user"))
            {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user"))
            {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user"))
            {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user"))
            {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            //get create by
            //string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            //List<USERS> create_by = userMan.findByLogin(tem);
            //string create_by_v = "";
            //if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            //{
            //    //string last_name = create_by[0].USR_LAST_NAME_EN != null ? create_by[0].USR_LAST_NAME_EN  : "";
            //    //create_by_v = string.Format("{0} {1}", create_by[0].USR_FIRST_NAME_EN, last_name);
            //    create_by_v = create_by[0].USR_FIRST_NAME_EN;
            //}
            //if (body.Contains("#create_by"))
            //{
            //    body = body.Replace("#create_by", create_by_v);
            //} 

            //cool expert
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000040) || stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000041) || stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000042))
            {
                subject = mms.getSubjectF10000041(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000041(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));

                using (var context = new EntityCPAIEngine())
                {
                    var purNo = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                    var coolDetail = context.COO_DATA.SingleOrDefault(a => a.CODA_PURCHASE_NO == purNo);
                    if(coolDetail != null)
                    {
                        stateModel.EngineModel.ftxTransId = coolDetail.CODA_ROW_ID;
                    }
                }
            }

            //
            if (body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = body;
                string temp_subject = subject;
                foreach (USERS um in userMail)
                {
                    temp_body = body;
                    temp_subject = subject;
                    if (temp_subject.Contains("#unit"))
                    {
                        temp_subject = temp_subject.Replace("#unit", user_unit.Where(x => x.Key == um.USR_LOGIN).First().Value);
                    }
                    if (body.Contains("#unit"))
                    {
                        temp_body = temp_body.Replace("#unit", user_unit.Where(x => x.Key == um.USR_LOGIN).First().Value);
                    }
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    temp_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, temp_subject, temp_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void prepareSendMailExpertSH(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail, Dictionary<string, string> user_area)
        {

            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystemCool(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0)
            {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("login user not found");
            }


            //
            if (subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            //
            if (subject.Contains("#user"))
            {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user"))
            {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user"))
            {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user"))
            {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            //get create by
            //string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            //List<USERS> create_by = userMan.findByLogin(tem);
            //string create_by_v = "";
            //if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            //{
            //    //string last_name = create_by[0].USR_LAST_NAME_EN != null ? create_by[0].USR_LAST_NAME_EN  : "";
            //    //create_by_v = string.Format("{0} {1}", create_by[0].USR_FIRST_NAME_EN, last_name);
            //    create_by_v = create_by[0].USR_FIRST_NAME_EN;
            //}
            //if (body.Contains("#create_by"))
            //{
            //    body = body.Replace("#create_by", create_by_v);
            //}

            //cool expert
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000040))
            {
                subject = mms.getSubjectF10000040(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000040(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            //
            if (body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = body;
                string temp_subject = subject;
                foreach (USERS um in userMail)
                {
                    temp_body = body;
                    temp_subject = subject;
                    if (temp_subject.Contains("#area_name"))
                    {
                        temp_subject = temp_subject.Replace("#area_name", user_area.Where(x => x.Key == um.USR_LOGIN).First().Value);
                    }
                    if (body.Contains("#area_name"))
                    {
                        temp_body = temp_body.Replace("#area_name", user_area.Where(x => x.Key == um.USR_LOGIN).First().Value);
                    }
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    temp_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, temp_subject, temp_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body)
        {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            currentCode = downResp.getResultCode();
        }
    }
}