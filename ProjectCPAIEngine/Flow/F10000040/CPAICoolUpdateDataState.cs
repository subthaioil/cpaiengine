﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using Excel;
using OfficeOpenXml;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000040
{
    public class CPAICoolUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICoolUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    //update reason only in data_detail 
                    //string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? Uri.UnescapeDataString(etxValue.GetValue(CPAIConstantUtil.Note)) : "-";

                    bool isRunDB = false;
                    if ((nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)) || nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_DRAFT_CAM))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                    }
                    else if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_FINAL_CAM) && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_5))
                    {
                        isRunDB = UpdateDetailDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, dataDetail);
                    }
                    else
                    {
                        //OTHER ==> UPDATE
                        string draft_cam = dataDetail.data == null ? "" : dataDetail.data.draft_cam_file;
                        string final_cam = dataDetail.data == null ? "" : dataDetail.data.final_cam_file;
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        {
                            if (dataDetail.data == null)
                            {
                                dataDetail.data = new CooData();
                            }
                        }
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, dataDetail.comment_draft_cam, dataDetail.comment_final_cam, draft_cam, final_cam, dataDetail.note);
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        {
                            dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                        }
                        #endregion
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = approve_1 or DRAFT only)

                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) || (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EXPERT_COMMENT)))
                        {
                            string date_purchase = dataDetail.data.date_purchase != null ? dataDetail.data.date_purchase : "";
                            string assay_ref = dataDetail.data.assay_ref != null ? dataDetail.data.assay_ref : "";
                            string assay_date = dataDetail.data.assay_date != null ? dataDetail.data.assay_date : "";
                            string products = dataDetail.data.crude_name != null ? dataDetail.data.crude_name : "";
                            string origin = dataDetail.data.country != null ? dataDetail.data.country : "";
                            string categories = dataDetail.data.crude_categories != null ? dataDetail.data.crude_categories : "";
                            string kerogen = dataDetail.data.crude_kerogen != null ? dataDetail.data.crude_kerogen : "";
                            string characteristic = dataDetail.data.crude_characteristic != null ? dataDetail.data.crude_characteristic : "";
                            string maturity = dataDetail.data.crude_maturity != null ? dataDetail.data.crude_maturity : "";
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            //set data
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Assay_ref, new ExtendValue { value = assay_ref, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Assay_date, new ExtendValue { value = assay_date, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = products, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Origin, new ExtendValue { value = origin, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Categories, new ExtendValue { value = categories, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Kerogen, new ExtendValue { value = kerogen, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Characteristic, new ExtendValue { value = characteristic, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Maturity, new ExtendValue { value = maturity, encryptFlag = "N" });

                        }
                        else
                        {
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        }
                        if (!nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_FINAL_CAM))
                        {
                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        }
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICoolUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICoolUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, CooRootObject dataDetail)
        {
            log.Info("# Start State CPAICoolUpdateDataState >> InsertDB #  ");
            string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            COO_DATA_DAL dataDAL = new COO_DATA_DAL();
                            dataDAL.Delete(TransID, context);

                            #region COO_DATA
                            COO_DATA dataCOO = new COO_DATA();
                            dataCOO.CODA_ROW_ID = TransID;
                            dataCOO.CODA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            if (!string.IsNullOrEmpty(dataDetail.data.date_purchase))
                                dataCOO.CODA_REQUESTED_DATE = DateTime.ParseExact(dataDetail.data.date_purchase, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.data.approval_date))
                                dataCOO.CODA_COMPLETE_DATE = DateTime.ParseExact(dataDetail.data.approval_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.data.assay_date))
                                dataCOO.CODA_ASSAY_DATE = DateTime.ParseExact(dataDetail.data.assay_date, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                            dataCOO.CODA_REQUESTED_BY = dataDetail.data.name;
                            dataCOO.CODA_AREA_UNIT = dataDetail.data.area_unit;
                            dataCOO.CODA_PRIORITY = dataDetail.data.workflow_priority;

                            dataCOO.CODA_ASSAY_TYPE = dataDetail.data.assay_from;
                            dataCOO.CODA_ASSAY_REF_NO = dataDetail.data.assay_ref;
                            dataCOO.CODA_CRUDE_NAME = dataDetail.data.crude_name;
                            //dataCOO.CODA_FK_MATERIALS = dataDetail.data.crude_id;
                            dataCOO.CODA_CRUDE_CATEGORIES = dataDetail.data.crude_categories;
                            dataCOO.CODA_CRUDE_CHARACTERISTIC = dataDetail.data.crude_characteristic;
                            dataCOO.CODA_CRUDE_KEROGEN_TYPE = dataDetail.data.crude_kerogen;
                            dataCOO.CODA_CRUDE_MATURITY = dataDetail.data.crude_maturity;
                            dataCOO.CODA_CRUDE_REFERENCE = dataDetail.data.update_from_crude;
                            dataCOO.CODA_COUNTRY = dataDetail.data.country;
                            //dataCOO.CODA_FK_COUNTRY = dataDetail.data.origin;
                            dataCOO.CODA_TYPE_OF_CAM = dataDetail.data.type_of_cam;
                            dataCOO.CODA_FOULING_POSSIBILITY = dataDetail.data.fouling_possibility;
                            dataCOO.CODA_COMMENT_DRAFT_CAM = dataDetail.comment_draft_cam;
                            dataCOO.CODA_COMMENT_FINAL_CAM = dataDetail.comment_final_cam;
                            dataCOO.CODA_DRAFT_CAM_PATH = dataDetail.data.draft_cam_file;
                            dataCOO.CODA_FINAL_CAM_PATH = dataDetail.data.final_cam_file;
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_DRAFT_CAM))
                            {
                                dataCOO.CODA_DRAFT_SUBMIT_DATE = DateTime.Now;
                                if (!string.IsNullOrEmpty(dataDetail.data.approve_date))
                                    dataCOO.CODA_DRAFT_APPROVE_DATE = DateTime.ParseExact(dataDetail.data.approve_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.data.reject_date))
                                    dataCOO.CODA_DRAFT_REJECT_DATE = DateTime.ParseExact(dataDetail.data.reject_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                            }
                            else
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE))
                            {
                                dataCOO.CODA_DRAFT_APPROVE_DATE = DateTime.Now;
                                if (!string.IsNullOrEmpty(dataDetail.data.submit_date))
                                    dataCOO.CODA_DRAFT_SUBMIT_DATE = DateTime.ParseExact(dataDetail.data.submit_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.data.reject_date))
                                    dataCOO.CODA_DRAFT_REJECT_DATE = DateTime.ParseExact(dataDetail.data.reject_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                            }
                            else
                            if (nextStatus.Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                dataCOO.CODA_DRAFT_REJECT_DATE = DateTime.Now;
                                if (!string.IsNullOrEmpty(dataDetail.data.approve_date))
                                    dataCOO.CODA_DRAFT_APPROVE_DATE = DateTime.ParseExact(dataDetail.data.approve_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                                if (!string.IsNullOrEmpty(dataDetail.data.submit_date))
                                    dataCOO.CODA_DRAFT_SUBMIT_DATE = DateTime.ParseExact(dataDetail.data.submit_date, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                            }

                            dataCOO.CODA_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                            dataCOO.CODA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataCOO.CODA_NOTE = dataDetail.note;
                            dataCOO.CODA_CREATED = dtNow;
                            dataCOO.CODA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCOO.CODA_UPDATED = dtNow;
                            dataCOO.CODA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);

                            //MT_MATERIALS mat = MaterialsDAL.GetMaterialByNameAndOrigin(dataDetail.data.crude_name, dataDetail.data.country);
                            //if (mat == null)
                            //{
                            //    MT_MATERIALS_DAL matDal = new MT_MATERIALS_DAL();
                            //    mat = new MT_MATERIALS();
                            //    mat.MET_NUM = ShareFn.GenerateCodeByDate("CPAI");
                            //    mat.MET_MAT_DES_ENGLISH = dataDetail.data.crude_name;
                            //    mat.MET_ORIG_CRTY = dataDetail.data.country;
                            //    mat.MET_CREATE_TYPE = ConstantPrm.SYSTEM.COOL;
                            //    mat.MET_CREATED_DATE = dtNow;
                            //    mat.MET_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            //    mat.MET_UPDATED_DATE = dtNow;
                            //    mat.MET_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            //    matDal.Save(mat);                               
                            //}

                            //dataCOO.CODA_FK_MATERIALS = mat.MET_NUM;
                            //dataCOO.CODA_FK_COUNTRY = dataDetail.data.country;

                            dataDAL.Save(dataCOO, context);
                            #endregion

                            #region COO_EXPERT
                            if (dataDetail.experts != null)
                            {
                                if (dataDetail.experts.areas != null)
                                {
                                    COO_EXPERT_DAL dataExpertDAL = new COO_EXPERT_DAL();
                                    MT_UNIT_DAL unitDAL = new MT_UNIT_DAL();
                                    if (dataDetail.experts.areas != null)
                                    {
                                        foreach (CooArea area in dataDetail.experts.areas)
                                        {
                                            if (area.units != null)
                                            {
                                                foreach (CooUnit unit in area.units)
                                                {
                                                    COO_EXPERT dataExpert = new COO_EXPERT();
                                                    MT_UNIT mt_unit = unitDAL.getUnit(unit.key);
                                                    if (mt_unit != null)
                                                    {
                                                        dataExpert.COEX_QUESTION_FLAG = unit.question_flag;
                                                    }
                                                    dataExpert.COEX_ROW_ID = Guid.NewGuid().ToString("N");
                                                    dataExpert.COEX_FK_COO_DATA = TransID;
                                                    dataExpert.COEX_FK_MT_UNIT = unit.key;
                                                    dataExpert.COEX_UNIT = unit.name;
                                                    dataExpert.COEX_UNIT_ORDER = unit.order;
                                                    dataExpert.COEX_AREA = area.name;
                                                    dataExpert.COEX_AREA_ORDER = area.order;
                                                    dataExpert.COEX_ACTIVATION_STATUS = unit.value == "Y" ? "INACTIVE" : "UNSELECTED";
                                                    dataExpert.COEX_CREATED = dtNow;
                                                    dataExpert.COEX_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataExpert.COEX_UPDATED = dtNow;
                                                    dataExpert.COEX_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataExpertDAL.Save(dataExpert, context);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region COO_ASSAY
                            COO_ASSAY_DAL dataAssayDAL = new COO_ASSAY_DAL();
                            COO_ASSAY dataAssay = new COO_ASSAY();
                            dataAssay.COAS_ROW_ID = Guid.NewGuid().ToString("N");
                            dataAssay.COAS_FK_COO_DATA = TransID;
                            dataAssay.COAS_FILE_PATH = dataDetail.data.assay_file;
                            dataAssay.COAS_CREATED = dtNow;
                            dataAssay.COAS_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataAssay.COAS_UPDATED = dtNow;
                            dataAssay.COAS_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataAssayDAL.Save(dataAssay, context);
                            #endregion

                            #region COO_CAM, COO_SPIRAL
                            MT_CAM_TEMP camTemp = MT_CAM_TEMP_DAL.GetCamTemplate();
                            MT_SPEC spec = MT_SPEC_DAL.GetSpec(dataDetail.data.crude_id, dataCOO.CODA_FK_COUNTRY);

                            COO_CAM_DAL dataCamDAL = new COO_CAM_DAL();
                            COO_CAM dataCam = new COO_CAM();
                            dataCam.COCA_ROW_ID = Guid.NewGuid().ToString("N");
                            dataCam.COCA_FK_COO_DATA = TransID;
                            dataCam.COCA_FK_MT_CAM_TEMP = camTemp == null ? "" : camTemp.COCT_ROW_ID;
                            dataCam.COCA_FK_MT_SPEC = spec == null ? "" : spec.MSP_ROW_ID;
                            dataCam.COCA_FILE_PATH = dataDetail.data.cam_file;
                            dataCam.COCA_CREATED = dtNow;
                            dataCam.COCA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCam.COCA_UPDATED = dtNow;
                            dataCam.COCA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataCamDAL.Save(dataCam, context);

                            COO_SPIRAL_DAL spiralDAL = new COO_SPIRAL_DAL();
                            COO_SPIRAL spiral = new COO_SPIRAL();
                            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "COOL", "SPIRAL", dataDetail.data.cam_file);
                            if (!string.IsNullOrEmpty(dataDetail.data.cam_file))
                            {
                                if (File.Exists(path))
                                {
                                    WebRequest request = WebRequest.Create(path);
                                    using (var response = request.GetResponse())
                                    {
                                        using (Stream stream = response.GetResponseStream())
                                        {
                                            IExcelDataReader reader = null;

                                            if (dataDetail.data.cam_file.EndsWith(".xls"))
                                            {
                                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                                            }
                                            else if (dataDetail.data.cam_file.EndsWith(".xlsx"))
                                            {
                                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                                            }

                                            reader.IsFirstRowAsColumnNames = true;

                                            DataSet ds = reader.AsDataSet();
                                            reader.Close();
                                            if (ds.Tables.Count > 0)
                                            {
                                                DataTable dt = ds.Tables[0];
                                                int pointer = 0;
                                                string header = "";
                                                for (int i = 0; i < dt.Rows.Count; i++)
                                                {
                                                    if (dt.Rows[i][0].ToString() == "" && pointer == 0)
                                                    {
                                                        pointer = 1;
                                                        continue;
                                                    }
                                                    if (pointer == 1)
                                                    {
                                                        header = dt.Rows[i][0].ToString();
                                                        pointer = 2;
                                                    }
                                                    else
                                                    {
                                                        if (dt.Rows[i][0].ToString() != "")
                                                        {
                                                            spiral = new COO_SPIRAL();
                                                            spiral.COSP_ROW_ID = Guid.NewGuid().ToString("N");
                                                            spiral.COSP_FK_COO_CAM = dataCam.COCA_ROW_ID;
                                                            spiral.COSP_HEADER = header;
                                                            spiral.COSP_KEY = dt.Rows[i][0].ToString();
                                                            spiral.COSP_VALUE = dt.Rows[i][1].ToString();
                                                            spiral.COSP_EXCEL_POSITION = "B" + (i + 2);
                                                            spiral.COSP_CREATED = dtNow;
                                                            spiral.COSP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                            spiral.COSP_UPDATED = dtNow;
                                                            spiral.COSP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                            spiralDAL.Save(spiral, context);
                                                        }
                                                        pointer = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            #endregion

                            #region COO_SUP_DOC
                            COO_SUP_DOC_DAL dataSupDocDAL = new COO_SUP_DOC_DAL();
                            COO_SUP_DOC dataSupDoc;
                            int supDoc_count = 1;
                            foreach (var item in dataDetail.support_doc_file.Split('|'))
                            {
                                if (!string.IsNullOrEmpty(item) && item.Contains(":"))
                                {
                                    List<string> text = item.Split(':').ToList();
                                    dataSupDoc = new COO_SUP_DOC();
                                    dataSupDoc.COSD_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataSupDoc.COSD_FK_COO_DATA = TransID;
                                    dataSupDoc.COSD_ORDER = supDoc_count + "";
                                    dataSupDoc.COSD_FILE_PATH = text[0];
                                    dataSupDoc.COSD_FILE_TYPE = text[1];
                                    dataSupDoc.COSD_CREATED = dtNow;
                                    dataSupDoc.COSD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataSupDoc.COSD_UPDATED = dtNow;
                                    dataSupDoc.COSD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataSupDocDAL.Save(dataSupDoc, context);
                                    supDoc_count++;
                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICoolUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAICoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error InsertDB >> Rollback # :: Exception >>> ", e);
                log.Error("InsertDB::Rollback >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAICoolUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool UpdateDetailDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, CooRootObject dataDetail)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICoolUpdateDataState >> UpdateDB #  ");
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            bool hasReject = false;
                            if (dataDetail.areas != null && dataDetail.areas.area != null && dataDetail.areas.area.Count > 0)
                            {
                                foreach (var area in dataDetail.areas.area)
                                {
                                    if (area.isSelected == "Y")
                                    {
                                        hasReject = true;
                                        COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                                        List<COO_EXPERT> experts = expertDal.GetByArea(TransID, area.name);
                                        if (experts != null && experts.Count > 0)
                                        {
                                            foreach (var expert in experts)
                                            {
                                                expert.COEX_STATUS = "APPROVE_3";
                                                expert.COEX_UPDATED = DateTime.Now;
                                                expert.COEX_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                expertDal.Update(expert, context);
                                            }
                                        }
                                    }
                                }
                            }

                            COO_DATA_DAL dataDAL = new COO_DATA_DAL();
                            COO_DATA dataCOO = dataDAL.GetByID(TransID);
                            dataCOO.CODA_NOTE = dataDetail.note;
                            dataCOO.CODA_COMMENT_FINAL_CAM = dataDetail.comment_final_cam;
                            if (hasReject)
                            {
                                dataCOO.CODA_STATUS = CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE;
                                dataCOO.CODA_UPDATED = DateTime.Now;
                                dataCOO.CODA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataDAL.Update(dataCOO, context);
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_REJECT_3, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE, encryptFlag = "N" });
                            }
                            else
                            {
                                dataCOO.CODA_FINAL_SUBMIT_DATE = DateTime.Now;
                                dataCOO.CODA_STATUS = NextStatus;
                                dataCOO.CODA_UPDATED = DateTime.Now;
                                dataCOO.CODA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataDAL.Update(dataCOO, context);
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_5, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_APPROVE_FINAL_CAM, encryptFlag = "N" });
                            }
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateDB >> UpdateDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateDB >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string draft_comment, string final_comment, string draft_cam, string final_cam, string note)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICoolUpdateDataState >> UpdateDB #  ");
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            COO_DATA_DAL dataDAL = new COO_DATA_DAL();
                            COO_DATA dataCOO = dataDAL.GetByID(TransID);
                            if (!string.IsNullOrEmpty(draft_comment))
                                dataCOO.CODA_COMMENT_DRAFT_CAM = draft_comment;
                            if (!string.IsNullOrEmpty(final_comment))
                                dataCOO.CODA_COMMENT_FINAL_CAM = final_comment;
                            if (!string.IsNullOrEmpty(draft_cam))
                            {
                                dataCOO.CODA_DRAFT_CAM_PATH = draft_cam;
                            }
                            if (!string.IsNullOrEmpty(final_cam))
                            {
                                dataCOO.CODA_FINAL_CAM_PATH = final_cam;
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) && currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                dataCOO.CODA_DRAFT_REJECT_DATE = DateTime.Now;
                            }
                            else if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_DRAFT_CAM))
                            {
                                dataCOO.CODA_DRAFT_SUBMIT_DATE = DateTime.Now;
                            }
                            else if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE))
                            {
                                dataCOO.CODA_DRAFT_APPROVE_DATE = DateTime.Now;
                                dataCOO.CODA_DRAFT_APPROVED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_GENERATE_FINAL_CAM) && currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                dataCOO.CODA_FINAL_REJECT_DATE = DateTime.Now;
                            }
                            else if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_FINAL_CAM))
                            {
                                dataCOO.CODA_FINAL_SUBMIT_DATE = DateTime.Now;
                            }
                            else if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED))
                            {
                                MT_MATERIALS mat = MaterialsDAL.GetMaterialByNameAndOrigin(dataCOO.CODA_CRUDE_NAME, dataCOO.CODA_COUNTRY);
                                if (mat == null)
                                {
                                    MT_MATERIALS_DAL matDal = new MT_MATERIALS_DAL();
                                    mat = new MT_MATERIALS();
                                    mat.MET_NUM = ShareFn.GenerateCodeByDate("CPAI");
                                    mat.MET_MAT_DES_ENGLISH = dataCOO.CODA_CRUDE_NAME;
                                    mat.MET_ORIG_CRTY = dataCOO.CODA_COUNTRY;
                                    mat.MET_CREATE_TYPE = ConstantPrm.SYSTEM.COOL;
                                    mat.MET_CREATED_DATE = DateTime.Now;
                                    mat.MET_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    mat.MET_UPDATED_DATE = DateTime.Now;
                                    mat.MET_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    matDal.Save(mat);
                                }

                                dataCOO.CODA_FINAL_APPROVE_DATE = DateTime.Now;
                                dataCOO.CODA_FINAL_APPROVED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            }

                            dataCOO.CODA_NOTE = note;
                            dataCOO.CODA_STATUS = NextStatus;
                            dataCOO.CODA_UPDATED = DateTime.Now;
                            dataCOO.CODA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataCOO, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateDB >> UpdateDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateDB >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}