﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000041
{
    public class CPAICoolExpertUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICoolExpertUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                    FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);
                    string ref_id = stateModel.EngineModel.ftxTransId;
                    if (ft != null && ft.FTX_PARENT_TRX_ID != null)
                    {
                        ref_id = ft.FTX_PARENT_TRX_ID;
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = parent_ft.FTX_INDEX8, encryptFlag = "N" });
                    }

                    bool isRunDB = false;
                    if ((nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE) && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1)))
                    {
                        //APPROVE_1 => Update status = ACTIVE
                        isRunDB = UpdateStatusDB(ref_id, etxValue, dataDetail);
                    }
                    else if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                    {
                        isRunDB = UpdateRejectDB(ref_id, etxValue, dataDetail);
                        dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                    }
                    else
                    {
                        //OTHER ==> UPDATE
                        isRunDB = UpdateDetailDB(ref_id, currentAction, etxValue, dataDetail);
                        //#region Set Reject flag
                        ////if current action is reject clear reject flag in table data history
                        //if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        //{
                        //    dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                        //}
                        //#endregion
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail
                        string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                        etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                        ExtendValue isSuccess = new ExtendValue();
                        isSuccess.value = "T";
                        etxValue.Remove(ConstantUtil.RESP + CPAIConstantUtil.Update_flag);
                        etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Update_flag, isSuccess);
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICoolExpertUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICoolExpertUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateStatusDB(string TransID, Dictionary<string, ExtendValue> etxValue, CooRootObject dataDetail)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICoolExpertUpdateDataState >> UpdateStatusDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                            List<COO_EXPERT> experts = expertDal.GetAllByID(TransID);
                            if (experts != null)
                            {
                                if (experts.Count > 0)
                                {
                                    foreach(var expert in experts)
                                    {
                                        if (expert.COEX_ACTIVATION_STATUS != "UNSELECTED")
                                        {
                                            expert.COEX_ACTIVATION_STATUS = "ACTIVE";
                                            expert.COEX_STATUS = CPAIConstantUtil.ACTION_APPROVE_2;
                                            expert.COEX_UPDATED = DateTime.Now;
                                            expert.COEX_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            expertDal.Update(expert, context);
                                        }
                                    }
                                }
                            }
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateStatusDB >> UpdateStatusDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertUpdateDataState >> UpdateStatusDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateStatusDB >> UpdateStatusDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateRejectDB(string TransID, Dictionary<string, ExtendValue> etxValue, CooRootObject dataDetail)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAICoolExpertUpdateDataState >> UpdateRejectDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                            List<COO_EXPERT> experts = expertDal.GetAllByID(TransID, true);
                            string user = etxValue.GetValue(CPAIConstantUtil.User);
                            UserGroupDAL ugDAL = new UserGroupDAL();
                            List<CPAI_USER_GROUP> ug = ugDAL.getUserList(user, ConstantPrm.SYSTEM.COOL);
                            if (experts != null && ug != null && ug.Count > 0)
                            {
                                if (experts.Count > 0)
                                {
                                    foreach (var expert in experts)
                                    {
                                        if (ug.Where(x => x.USG_USER_GROUP == expert.COEX_UNIT).Count() > 0 && expert.COEX_STATUS == CPAIConstantUtil.ACTION_APPROVE_3)
                                        {
                                            expert.COEX_STATUS = CPAIConstantUtil.ACTION_REJECT;
                                            expert.COEX_REJECT_DATE = DateTime.Now;
                                            expert.COEX_UPDATED = DateTime.Now;
                                            expert.COEX_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            expertDal.Update(expert, context);
                                        }
                                    }
                                }
                            }
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateRejectDB >> UpdateRejectDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertUpdateDataState >> UpdateRejectDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateRejectDB >> UpdateRejectDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateDetailDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, CooRootObject dataDetail)
        {
            log.Info("# Start State CPAICoolExpertUpdateDataState >> UpdateDetailDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            #region COO_EXPERT, COO_EXPERT_ITEMS
                            COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                            List<COO_EXPERT> experts = expertDal.GetAllByID(TransID, true);
                            string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                            if (dataDetail.areas != null && dataDetail.areas.area.Count > 0)
                            {
                                foreach (var area in dataDetail.areas.area)
                                {
                                    if (area.unit != null && area.unit.Count > 0)
                                    {
                                        foreach (var unit in area.unit)
                                        {
                                            COO_EXPERT expert = experts.Where(x => x.COEX_AREA == area.name && x.COEX_UNIT == unit.name).FirstOrDefault();
                                            if (expert != null && (expert.COEX_STATUS == CPAIConstantUtil.ACTION_APPROVE_2 || expert.COEX_STATUS == CPAIConstantUtil.ACTION_REJECT))
                                            {
                                                expert.COEX_AGREED_SCORE = unit.comment.agreed_score;
                                                expert.COEX_AVERAGE_SCORE = unit.comment.average_score;
                                                expert.COEX_COMMENT_EXPERT = unit.comment.comment_expert;
                                                expert.COEX_RECOMMENDED_SCORE = unit.comment.recommended_score;
                                                expert.COEX_SCORE_FLAG = unit.comment.score_flag;
                                                expert.COEX_SCORE_REASON = unit.comment.recommended_score_reason;
                                                expert.COEX_PROCESSING_FLAG = unit.comment.processing_flag;
                                                expert.COEX_PROCESSING_NOTE = unit.comment.processing_note;
                                                expert.COEX_ATTACHED_FILE = unit.attached_file;
                                                expert.COEX_EXPERT_SELECTED = unit.isSelected_expert;
                                                if (unit.isSelected_expert == "Y" && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_3))
                                                {
                                                    expert.COEX_STATUS = NextStatus;
                                                    expert.COEX_SUBMIT_DATE = dtNow;
                                                    expert.COEX_SUBMITTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                }
                                                expert.COEX_UPDATED = dtNow;
                                                expert.COEX_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                expertDal.UpdateExpert(expert, context);

                                                COO_EXPERT_ITEMS_DAL expertItemDal = new COO_EXPERT_ITEMS_DAL();
                                                expertItemDal.Delete(expert.COEX_ROW_ID, context);
                                                if (unit.comment_items != null && unit.comment_items.Count > 0)
                                                {
                                                    foreach (var item in unit.comment_items)
                                                    {
                                                        COO_EXPERT_ITEMS expertItem = new COO_EXPERT_ITEMS();
                                                        expertItem.COEI_ROW_ID = Guid.NewGuid().ToString("N");
                                                        expertItem.COEI_FK_COO_EXPERT = expert.COEX_ROW_ID;
                                                        expertItem.COEI_CAL_RESULT = item.calculation_result;
                                                        expertItem.COEI_CAL_DETAIL = item.calculation_detail;
                                                        expertItem.COEI_CAL_SCORE = item.weight_score;
                                                        expertItem.COEI_OP_JSON = new JavaScriptSerializer().Serialize(item);
                                                        expertItem.COEI_CREATED = DateTime.Now;
                                                        expertItem.COEI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                        expertItem.COEI_UPDATED = DateTime.Now;
                                                        expertItem.COEI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                        expertItemDal.Save(expertItem, context);
                                                    }
                                                }

                                            }
                                        }

                                    }

                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICoolExpertUpdateDataState >> UpdateDetailDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertUpdateDataState >> UpdateDetailDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateCommentDB >> UpdateCommentDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }

    }
}