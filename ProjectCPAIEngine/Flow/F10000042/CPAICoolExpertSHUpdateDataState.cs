﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000042
{
    public class CPAICoolExpertSHUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICoolExpertSHUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                    FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);
                    string ref_id = stateModel.EngineModel.ftxTransId;
                    if (ft != null && ft.FTX_PARENT_TRX_ID != null)
                    {
                        ref_id = ft.FTX_PARENT_TRX_ID;
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = parent_ft.FTX_INDEX8, encryptFlag = "N" });
                    }

                    bool isRunDB = false;
                    bool hasReject = false;
                    if ((nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE) && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_2)))
                    {
                        //Do nothing
                        isRunDB = true;
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                    } else
                    {
                        //APPROVE_2 => Update data
                        isRunDB = UpdateDetailDB(ref_id, etxValue, dataDetail, out hasReject);
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail
                        string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                        etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                        ExtendValue isSuccess = new ExtendValue();
                        isSuccess.value = "T";
                        etxValue.Remove(ConstantUtil.RESP + CPAIConstantUtil.Update_flag);
                        etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Update_flag, isSuccess);
                    }
                    else
                    {
                        if (hasReject)
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_STATUS_RESP_CODE;
                        } else
                        {
                            currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                        }
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICoolExpertSHUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAICoolExpertSHUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertSHUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateDetailDB(string TransID, Dictionary<string, ExtendValue> etxValue, CooRootObject dataDetail, out bool hasReject)
        {
            log.Info("# Start State CPAICoolExpertSHUpdateDataState >> UpdateDetailDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            hasReject = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region COO_EXPERT, COO_EXPERT_ITEMS
                            COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                            List<COO_EXPERT> experts = expertDal.GetAllByID(TransID, true);
                            string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                            if (dataDetail.areas != null)
                            {
                                if (dataDetail.areas.area.Count > 0)
                                {
                                    foreach (var area in dataDetail.areas.area)
                                    {
                                        if (area.unit != null)
                                        {
                                            if (area.unit.Count > 0)
                                            {
                                                if (area.unit.Where(x => x.status == CPAIConstantUtil.ACTION_REJECT).Count() > 0)
                                                {
                                                    hasReject = true;
                                                    throw new Exception();
                                                }
                                                foreach (var unit in area.unit)
                                                {
                                                    COO_EXPERT expert = experts.Where(x => x.COEX_AREA == area.name && x.COEX_UNIT == unit.name).FirstOrDefault();
                                                    if (expert != null && expert.COEX_STATUS == CPAIConstantUtil.ACTION_APPROVE_3)
                                                    {
                                                        expert.COEX_AGREED_SCORE = unit.comment.agreed_score;
                                                        expert.COEX_AVERAGE_SCORE = unit.comment.average_score;
                                                        expert.COEX_RECOMMENDED_SCORE = unit.comment.recommended_score;
                                                        expert.COEX_SCORE_FLAG = unit.comment.score_flag;
                                                        expert.COEX_SCORE_REASON = unit.comment.recommended_score_reason;
                                                        expert.COEX_PROCESSING_FLAG = unit.comment.processing_flag;
                                                        expert.COEX_PROCESSING_NOTE = unit.comment.processing_note;
                                                        expert.COEX_COMMENT_EXPERT = unit.comment.comment_expert;
                                                        expert.COEX_EXPERT_SH_SELECTED = unit.isSelected_expert_sh;
                                                        expert.COEX_ATTACHED_FILE = unit.attached_file;
                                                        if (unit.isSelected_expert_sh == "Y" && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_4))
                                                        {
                                                            expert.COEX_EXPERT_SELECTED = unit.next_status == CPAIConstantUtil.ACTION_REJECT ? "N" : "Y";
                                                            if (unit.next_status == CPAIConstantUtil.ACTION_REJECT)
                                                            {
                                                                expert.COEX_STATUS = CPAIConstantUtil.ACTION_REJECT;
                                                                expert.COEX_REJECT_DATE = dtNow;
                                                            }
                                                            else if (unit.next_status == CPAIConstantUtil.ACTION_APPROVE)
                                                            {
                                                                expert.COEX_STATUS = CPAIConstantUtil.STATUS_APPROVED;
                                                                expert.COEX_APPROVE_DATE = dtNow;
                                                                expert.COEX_APPROVED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                            }
                                                        }
                                                        expert.COEX_UPDATED = dtNow;
                                                        expert.COEX_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                        expertDal.UpdateExpertSH(expert, context);

                                                        COO_EXPERT_ITEMS_DAL expertItemDal = new COO_EXPERT_ITEMS_DAL();
                                                        expertItemDal.Delete(expert.COEX_ROW_ID, context);
                                                        if (unit.comment_items != null && unit.comment_items.Count > 0)
                                                        {
                                                            foreach (var item in unit.comment_items)
                                                            {
                                                                COO_EXPERT_ITEMS expertItem = new COO_EXPERT_ITEMS();
                                                                expertItem.COEI_ROW_ID = Guid.NewGuid().ToString("N");
                                                                expertItem.COEI_FK_COO_EXPERT = expert.COEX_ROW_ID;
                                                                expertItem.COEI_CAL_RESULT = item.calculation_result;
                                                                expertItem.COEI_CAL_DETAIL = item.calculation_detail;
                                                                expertItem.COEI_CAL_SCORE = item.weight_score;
                                                                expertItem.COEI_OP_JSON = new JavaScriptSerializer().Serialize(item);
                                                                expertItem.COEI_CREATED = DateTime.Now;
                                                                expertItem.COEI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                                expertItem.COEI_UPDATED = DateTime.Now;
                                                                expertItem.COEI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                                expertItemDal.Save(expertItem, context);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            //Check if all experts are approved
                            COO_EXPERT new_experts = experts.Where(x => x.COEX_STATUS != CPAIConstantUtil.STATUS_APPROVED && x.COEX_ACTIVATION_STATUS == CPAIConstantUtil.ACTIVE).FirstOrDefault();
                            if (new_experts == null && currentAction == CPAIConstantUtil.ACTION_APPROVE_4)
                            {
                                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                                COO_DATA data = dataDal.GetByID(TransID);
                                data.CODA_STATUS = CPAIConstantUtil.STATUS_WAITING_GENERATE_FINAL_CAM;
                                dataDal.UpdateStatus(data, context);
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_4, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_GENERATE_FINAL_CAM, encryptFlag = "N" });
                            } else
                            {
                                if (currentAction == CPAIConstantUtil.ACTION_APPROVE_4)
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_3, encryptFlag = "N" });
                                } else
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.CurrentAction), encryptFlag = "N" });
                                }
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE, encryptFlag = "N" });
                            }
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICoolExpertSHUpdateDataState >> UpdateDetailDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertSHUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertSHUpdateDataState >> UpdateDetailDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateCommentDB >> UpdateCommentDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertSHUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }
    }
}