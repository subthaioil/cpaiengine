﻿
using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Flow.Model;
using DSMail.model;
using com.pttict.downstream.common.model;
using DSMail.service;
using com.pttict.engine.downstream;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using DSFcm.service;
using DSFcm.model;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.DAL.DALPAF;

namespace ProjectCPAIEngine.Flow.F10000044
{

    public class CPAIApprovalFormSendNotiApproveState : BasicBean, StateFlowAction
    {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISendNotiApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                string CreateBy = etxValue.GetValue(CPAIConstantUtil.CreateBy);

                PAF_ACTION_MESSAGE_DAL amsMan = new PAF_ACTION_MESSAGE_DAL();
                string group = PAFHelper.GetUserGroup(CreateBy, "PAF");
                group = group.ToUpper().Substring(0, 4);
                List<CPAI_ACTION_MESSAGE> results = amsMan.findUserGroupNoti(action, system, type, stateModel.EngineModel.functionId, group);
                if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_NOTI_RESP_CODE;
                }
                else
                {
                    for (int i = 0; i < results.Count; i++)
                    {
                        findUserAndSendNoti(stateModel, results[i], system);
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAISendNotiApproveState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);

                log.Info("# Error CPAISendNotiApproveState # :: Exception >>> " + ex);
                log.Error("CPAISendNotiApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                                                                     //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendNoti(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system)
        {
            log.Info("# Start State CPAISendNotiApproveState >> findUserAndSendNoti #  ");
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER))
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                //List<USERS> userResult = userMan.findByUserGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroupSystem(userGroup, system);
                List<USERS> userResult = userGMan.findUserNotiByGroupSystem(userGroup, system);

                if (userResult.Count == 0)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_NOTI_RESP_CODE;
                    log.Info("user not found");
                }
                else
                {
                    Dictionary<string, string> lstNotiTo = new Dictionary<string, string>();
                    for (int i = 0; i < userResult.Count; i++)
                    {
                        if (userResult[i].USR_NOTI_ID != null && userResult[i].USR_OS != null)
                        {
                            lstNotiTo[userResult[i].USR_NOTI_ID] = userResult[i].USR_OS;
                        }
                    }
                    prepareSendNoti(stateModel, lstNotiTo, actionMessage);
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                List<USERS> userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    Dictionary<string, string> lstNotiTo = new Dictionary<string, string>();
                    for (int i = 0; i < userResult.Count; i++)
                    {
                        lstNotiTo[userResult[i].USR_NOTI_ID] = userResult[i].USR_OS;
                    }
                    prepareSendNoti(stateModel, lstNotiTo, actionMessage);
                }
            }

            log.Info("# End State CPAISendNotiApproveState >> findUserAndSendNoti # :: Code >>> " + currentCode);
        }
        public void prepareSendNoti(StateModel stateModel, Dictionary<string, string> lstNotiTo, CPAI_ACTION_MESSAGE actionMessage)
        {
            log.Info("# Start State CPAISendNotiApproveState >> prepareSendNoti #  ");
            string jsonText = Regex.Replace(actionMessage.AMS_NOTIFICATION_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            NotiDetail detail = JSonConvertUtil.jsonToModel<NotiDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;
            string action_data = detail.action_data;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (userResult != null && userResult.Count == 0)
            {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("invalid user");
            }
            else
            {
                log.Info("found user");
            }
            //
            if (subject != null && subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body != null && body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (action_data != null && action_data.Contains("#pn"))
            {
                action_data = action_data.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }

            /*
            //
            if (subject.Contains("#user"))
            {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user"))
            {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#user_group"))
            {
                subject = subject.Replace("#user_group", etxValue.GetValue(CPAIConstantUtil.UserGroup));
            }
            if (body.Contains("#user_group"))
            {
                body = body.Replace("#user_group", etxValue.GetValue(CPAIConstantUtil.UserGroup));
            }
            //
            if (body.Contains("#token"))
            {
                //get user approve
                UserGroupDAL ugDal = new UserGroupDAL();
                CPAI_USER_GROUP ug = ugDal.findByUserGroupAndSystem(actionMessage.AMS_USR_GROUP, actionMessage.AMS_SYSTEM);
                if (ug == null)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    throw new Exception("send mail user not found");
                }
                else
                {
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = ug.USG_FK_USERS;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    body = body.Replace("#token", token);
                }
            }
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000001))
            {
                body = getBodyF10000001(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000006) 
                || stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F100000007))
            {
                subject = getSubjectF10000006(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = getBodyF10000006(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }
            */
            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
            foreach (var item in lstNotiTo)
            {
                log.Debug("sendNoti");
                var key = item.Key;
                var value = item.Value;
                sendNoti(stateModel, key, value, subject, body, action_data);
            }

            log.Info("# End State CPAISendNotiApproveState >> prepareSendNoti # :: Code >>> " + currentCode);
        }

        public void sendNoti(StateModel stateModel, string key, string value, string subject, string body, string action_data)
        {
            log.Info("# Start State CPAISendNotiApproveState >> sendNoti #  ");
            if (key != null && !String.IsNullOrEmpty(key))
            {
                //call downstream send mail
                DownstreamController<FCMServiceConnectorImpl, string> connector = new DownstreamController<FCMServiceConnectorImpl, string>(new FCMServiceConnectorImpl(), "");
                //find by config and content from DB
                ConfigManagement configManagement = new ConfigManagement();
                String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
                String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

                var configObj = JSonConvertUtil.jsonToModel<RequestFcmModel>(config);
                configObj.os = value;

                String finalConfig = JSonConvertUtil.modelToJson(configObj);

                var contentObj = JSonConvertUtil.jsonToModel<ContentFcmModel>(content);
                contentObj.deviceId = key;
                contentObj.body_i = body;
                contentObj.title_i = subject;
                contentObj.action_data_i = action_data;

                String finalContent = JSonConvertUtil.modelToJson(contentObj);

                //call downstream
                DownstreamResponse<string> downResp = connector.invoke(stateModel, finalConfig, finalContent);
                currentCode = downResp.getResultCode();
                stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            }
            else
            {
                //null noti id
                currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_NOTI_ID_RESP_CODE;
            }

            log.Info("# End State CPAISendNotiApproveState >> sendNoti # :: Code >>> " + currentCode);
        }

    }
}
