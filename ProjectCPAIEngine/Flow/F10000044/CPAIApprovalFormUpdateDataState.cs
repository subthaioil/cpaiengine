﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000044
{
    public class CPAIApprovalFormUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIApprovalFormUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            DATA_HISTORY_DAL dthMan = new DATA_HISTORY_DAL();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                PAF_DATA data = JsonConvert.DeserializeObject<PAF_DATA>(etxValue.GetValue(CPAIConstantUtil.DataDetailInput));
                if(data == null)
                {
                    data = new PAF_DATA();
                    data.TRAN_ID = stateModel.EngineModel.ftxTransId;
                }
                PAF_DATA_DAL.CreateDataHistory(data, etxValue.GetValue(CPAIConstantUtil.CurrentAction), etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.Note), stateModel.EngineModel.ftxTransId, etxValue.GetValue(CPAIConstantUtil.CurrentAction) == ConstantPrm.ACTION.REJECT);
                if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_REJECT))
                {
                    dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxTransId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                }
                if (etxValue.GetValue(CPAIConstantUtil.NextStatus) != "DRAFT")
                {
                    data.PDA_UPDATED = DateTime.Now;
                    data.PDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                }
                //PAF_DATA data = JSonConvertUtil.jsonToModel<PAF_DATA>(etxValue.GetValue(CPAIConstantUtil.DataDetailInput));
                data.PDA_FORM_ID = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                data.PDA_REASON = etxValue.GetValue(CPAIConstantUtil.Note);
                data.PDA_CREATED_BY = data.PDA_CREATED_BY ?? etxValue.GetValue(CPAIConstantUtil.User);
                FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);
                PAF_DATA_DAL.InsertOrUpdate(etxValue.GetValue(CPAIConstantUtil.NextStatus), data, stateModel.EngineModel.ftxTransId);
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIApprovalFormUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIApprovalFormUpdateDataState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}