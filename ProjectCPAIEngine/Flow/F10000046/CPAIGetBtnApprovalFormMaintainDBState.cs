﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;


namespace ProjectCPAIEngine.Flow.F10000046
{
    public class CPAIGetBtnApprovalFormMaintainDBState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGetBtnApprovalFormMaintainDBState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;


            try
            {
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIGetBtnApprovalFormMaintainDBState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIGetBtnApprovalFormMaintainDBState # :: Exception >>> " + ex);
                log.Error("CPAIGetBtnApprovalFormMaintainDBState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}