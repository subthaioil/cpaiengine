﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPCF;
using System.Globalization;

namespace ProjectCPAIEngine.Flow.F10000047
{
    public class CPAICustomVatDataState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICustomVatDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    CustomVat dataDetail = JSonConvertUtil.jsonToModel<CustomVat>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    bool isRunDB = false;

                    isRunDB = InsertUpdateDB(dataDetail);

                    if (isRunDB)
                    {
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion


                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICustomVatDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICustomVatDataState # :: Exception >>> " + ex);
                log.Error("CPAICustomVatDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
        public bool InsertUpdateDB(CustomVat dataDetail)
        {
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            bool isSuccess = false;
            
            try
            {
                CUSTOMS_VAT_DAL dalCustomVat = new CUSTOMS_VAT_DAL();
                PCF_CUSTOMS_VAT entCustomVat = new PCF_CUSTOMS_VAT();

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            foreach (var item in dataDetail.CustomVat_Calc)
                            {                                
                                entCustomVat.TRIP_NO = item.TripNo;
                                entCustomVat.MAT_ITEM_NO = Int32.Parse(String.IsNullOrEmpty(item.MatItemNo) ? "0" : item.MatItemNo);
                                entCustomVat.CUSTOMS_PRICE = Decimal.Parse(String.IsNullOrEmpty(item.CustomsPrice)? "0" : item.CustomsPrice);
                                entCustomVat.FREIGHT_AMOUNT = Decimal.Parse(String.IsNullOrEmpty(item.FRT) ? "0" : item.FRT);
                                entCustomVat.INSURANCE_AMOUNT = Decimal.Parse(String.IsNullOrEmpty(item.INS) ? "0" : item.INS);
                                entCustomVat.INSURANCE_RATE = Decimal.Parse(String.IsNullOrEmpty(dataDetail.sInsuranceRate)? "0" : dataDetail.sInsuranceRate);
                                entCustomVat.ROE = Decimal.Parse(String.IsNullOrEmpty(dataDetail.sROE) ? "1" : dataDetail.sROE);
                                entCustomVat.FOB = Decimal.Parse(String.IsNullOrEmpty(item.FOB) ? "0" : item.FOB);
                                entCustomVat.FRT = Decimal.Parse(String.IsNullOrEmpty(item.FRT) ? "0" : item.FRT);
                                entCustomVat.CFR = Decimal.Parse(String.IsNullOrEmpty(item.CFR) ? "0" : item.CFR);
                                entCustomVat.INS = Decimal.Parse(String.IsNullOrEmpty(item.INS) ? "0" : item.INS);
                                entCustomVat.CIF_SUM_INS = Decimal.Parse(String.IsNullOrEmpty(item.CIF) ? "0" : item.CIF);
                                entCustomVat.PREMIUM = Decimal.Parse(String.IsNullOrEmpty(item.Premium) ? "0" : item.Premium);
                                entCustomVat.TOTAL_USD_100 = Decimal.Parse(String.IsNullOrEmpty(item.TotalUSD100) ? "0" : item.TotalUSD100);
                                entCustomVat.BAHT_100 = Decimal.Parse(String.IsNullOrEmpty(item.Baht100) ? "0" : item.Baht100);
                                entCustomVat.BAHT_105 = Decimal.Parse(String.IsNullOrEmpty(item.Baht105) ? "0" : item.Baht105);
                                entCustomVat.VAT = Decimal.Parse(String.IsNullOrEmpty(item.VAT) ? "0" : item.VAT);
                                entCustomVat.TAX_BASE = Decimal.Parse(String.IsNullOrEmpty(item.TaxBase) ? "0" : item.TaxBase);
                                entCustomVat.CORRECT_VAT = Decimal.Parse(String.IsNullOrEmpty(item.CorrectVAT) ? "0" : item.CorrectVAT);
                                entCustomVat.IMPORT_DUTY = Decimal.Parse(String.IsNullOrEmpty(item.ImportDuty) ? "0" : item.ImportDuty);
                                entCustomVat.EXCISE_TAX = Decimal.Parse(String.IsNullOrEmpty(item.ExciseTax) ? "0" : item.ExciseTax);
                                entCustomVat.MUNICIPAL_TAX = Decimal.Parse(String.IsNullOrEmpty(item.MunicipalTax) ? "0" : item.MunicipalTax);
                                entCustomVat.OIL_FUEL_FUND = Decimal.Parse(String.IsNullOrEmpty(item.OilFuelFund) ? "0" : item.OilFuelFund);
                                entCustomVat.ENERY_OIL_FUEL_FUND = Decimal.Parse(String.IsNullOrEmpty(item.EnergyOilFuelFund) ? "0" : item.EnergyOilFuelFund);
                                entCustomVat.DEPOSIT_OF_EXCISE_TAX = Decimal.Parse(String.IsNullOrEmpty(item.DepositOfExciseTax) ? "0" : item.DepositOfExciseTax);
                                entCustomVat.DEPOSIT_OF_MUNICIPAL_TAX = Decimal.Parse(String.IsNullOrEmpty(item.DepositOfMunicipalTax) ? "0" : item.DepositOfMunicipalTax);
                                entCustomVat.DEPOSIT_OF_IMPORT_DUTY = Decimal.Parse(String.IsNullOrEmpty(item.DepositOfImportDuty) ? "0" : item.DepositOfImportDuty);
                                entCustomVat.CAL_VOLUME_UNIT = String.IsNullOrEmpty(dataDetail.sCalculationUnit) ? "" : dataDetail.sCalculationUnit;

                                if (String.IsNullOrEmpty(dataDetail.sDocumentDate))
                                {
                                    entCustomVat.DOCUMENT_DATE = null;
                                }
                                else
                                {
                                    entCustomVat.DOCUMENT_DATE = DateTime.ParseExact(dataDetail.sDocumentDate, format, provider);
                                }
                                if (String.IsNullOrEmpty(dataDetail.sPostingDate))
                                {
                                    entCustomVat.POSTING_DATE = null;
                                }
                                else
                                {
                                    entCustomVat.POSTING_DATE = DateTime.ParseExact(dataDetail.sPostingDate, format, provider);
                                }

                                var _qry = context.PCF_CUSTOMS_VAT.Find(item.TripNo, Int32.Parse(item.MatItemNo));
                                if (_qry == null)
                                {
                                    entCustomVat.SAP_FI_DOC = "";
                                    entCustomVat.SAP_FI_DOC_STATUS = "";
                                    entCustomVat.SAP_FI_DOC_ERROR = "";
                                    entCustomVat.SAP_INVOICE_DOC = "";
                                    entCustomVat.SAP_INVOICE_DOC_STATUS = "";
                                    entCustomVat.SAP_INVOICE_DOC_ERROR = "";
                                    entCustomVat.SAP_FI_DOC_INVOICE = "";
                                    entCustomVat.SAP_FI_DOC_INVOICE_STATUS = "";
                                    entCustomVat.SAP_FI_DOC_INVOICE_ERROR = "";

                                    dalCustomVat.Save(entCustomVat);
                                }
                                else
                                {
                                    entCustomVat.SAP_FI_DOC = item.FIDocVAT;
                                    entCustomVat.SAP_FI_DOC_STATUS = item.FIDocVATStatus;
                                    entCustomVat.SAP_FI_DOC_ERROR = item.FIDocVATMessage;
                                    entCustomVat.SAP_INVOICE_DOC = item.InvoiceDocMIRO;
                                    entCustomVat.SAP_INVOICE_DOC_STATUS = item.InvoiceDocMIROStatus;
                                    entCustomVat.SAP_INVOICE_DOC_ERROR = item.InvoiceDocMIROMessage;
                                    entCustomVat.SAP_FI_DOC_INVOICE = item.FIDocMIRO;
                                    entCustomVat.SAP_FI_DOC_INVOICE_STATUS = item.FIDocMIROStatus;
                                    entCustomVat.SAP_FI_DOC_INVOICE_ERROR = item.FIDocMIROMessage;

                                    dalCustomVat.Update(entCustomVat);
                                }

                            }
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            String innerMessage = (ex.InnerException != null)
                              ? ex.InnerException.Message
                              : "";
                            string res = ex.Message;
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("Exception : " + innerMessage + " : " + res);
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        }
       
    }

}
