﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;

namespace ProjectCPAIEngine.Flow.F10000050
{
    public class CPAIListTxnHedgingTicketState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIListTxnHedgingTicketState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();           
            string currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
            string user, userGroup, system, function_code;

            try
            {                
                user = etxValue.GetValue(CPAIConstantUtil.User);
                userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                system = etxValue.GetValue(CPAIConstantUtil.System);
                function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                List<CPAI_ACTION_FUNCTION> result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, CPAIConstantUtil.HEDG_TICKET_FUNCTION_CODE);

                List<Transaction> lstAllTx = new List<Transaction>();
                if (result.Count == 0)
                {                    
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                else if (result.Count == 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else
                {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER))
                        {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null)
                    {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    }
                    else
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                        }
                        setExtraXml(stateModel, lstAllTx);
                    }
                }                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIListTxnHedgingTicketState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIListTxnHedgingTicketState # :: Exception >>> " + ex);
                log.Error("CPAIListTxnHedgingTicketState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;                
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private List<Transaction> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction)
        {
            log.Info("# Start State CPAIListTxnHedgingTicketState >> getTransaction #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
            List<FunctionTransaction> result;
            List<Transaction> list = new List<Transaction>();
            List<string> lstStatus = new List<string>();
            string status = etxValue.GetValue(CPAIConstantUtil.Status);

            if (string.IsNullOrWhiteSpace(status))
            {
                if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS))
                {
                    lstStatus.Add("ALL");
                }
                else
                {
                    status = actionFunction.ACF_FUNCTION_STATUS;
                    lstStatus = new List<string>(status.Split('|'));
                }
            }
            else
            {
                lstStatus = new List<string>(status.Split('|'));
            }
            var lstIndexValues = new List<string>();
            lstIndexValues.Add(actionFunction.ACF_SYSTEM);
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Type));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index3));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index4));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index5));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index6));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index7));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index8));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index9));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index10));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index11));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index12));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index13));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index14));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index15));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index16));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index17));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index18));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index19));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index20));
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////            
            result = ftxMan.findCPAIHedgTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues);
            for (int i = 0; i < result.Count; i++)
            {
                FunctionTransaction ftx = result[i];                
                Transaction m = new Transaction();

                m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;

                m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                m.deal_date = ftx.FUNCTION_TRANSACTION.FTX_INDEX5;
                m.deal_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                m.ticket_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX7;
                m.hedg_type = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                m.tool = ftx.FUNCTION_TRANSACTION.FTX_INDEX9;
                m.underlying = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                m.underlying_vs = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                m.ticket_date = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                m.trade_for = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                m.deal_type = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                m.counter_party = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                m.create_by = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;                

                m.underlying_name = ftx.UNDERLYING_NAME;

                list.Add(m);                
            }
            log.Info("# End State CPAIListTxnHedgingTicketState >> getTransaction # ");
            return list;
        }

        private void setExtraXml(StateModel stateModel, List<Transaction> lstAllTx)
        {
            log.Info("# Start State CPAIListTxnHedgingTicketState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            TransactionList li = new TransactionList();
            li.trans = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);

            log.Info("# End State CPAIListTxnHedgingTicketState >> setExtraXml #");
        }
    }    
}
