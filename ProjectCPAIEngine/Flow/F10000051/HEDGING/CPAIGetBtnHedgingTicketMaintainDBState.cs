﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000051
{
    public class CPAIGetBtnHedgingTicketMaintainDBState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGetBtnHedgingTicketMaintainDBState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;


            try
            {
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIGetBtnHedgingTicketMaintainDBState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {                
                log.Info("# Error CPAIGetBtnHedgingTicketMaintainDBState # :: Exception >>> " + ex);
                log.Error("CPAIGetBtnHedgingTicketMaintainDBState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}