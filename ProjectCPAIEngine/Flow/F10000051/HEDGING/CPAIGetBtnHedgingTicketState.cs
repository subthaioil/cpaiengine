﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000051
{
    public class CPAIGetBtnHedgingTicketState : BasicBean,StateFlowAction
    {
        private const String TYPE_1 = "1";
        private String currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        private FUNCTION_TRANSACTION currentFtx;
        private FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
        private ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
        private ActionFunctionDAL acfMan = new ActionFunctionDAL();
        private UsersDAL usrMan = new UsersDAL();
        private ActionControlDAL actMan = new ActionControlDAL();
        private ActionOverrideDAL acoMan = new ActionOverrideDAL();
        private ActionButtonDAL abtMan = new ActionButtonDAL();
        private DataHistoryDAL dthMan = new DataHistoryDAL();

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGetBtnHedgingTicketState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            try
            {           
                getButtonList(stateModel.BusinessModel.etxValue);
                getDetail(stateModel.BusinessModel.etxValue);
                getAttachItems(stateModel.BusinessModel.etxValue);
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIGetBtnHedgingTicketState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {                
                log.Info("# Error CPAIGetBtnHedgingTicketState # :: Exception >>> " + ex);
                log.Error("CPAIGetBtnHedgingTicketState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private void getButtonList(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnHedgingTicketState >> getButtonList #  ");
            List<CPAI_ACTION_BUTTON> lstAllButton = new List<CPAI_ACTION_BUTTON>();
            List<String> lstAcfFunction = new List<string>();
            List<Button> list = new List<Button>();
            ButtonList li = new ButtonList();
            ExtendValue respTrans = new ExtendValue();
            USERS users;
            String xml;            
            string currentStatus, userRowId, userGroup, system, type;

            // find function transaction by "transaction_id" : (1)
            var ftxResult = ftxMan.findByTransactionId(etxValue.GetValue(CPAIConstantUtil.TransactionId));
            if (ftxResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.FUNCTION_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            currentFtx = ftxResult[0];
            currentStatus = currentFtx.FTX_INDEX4;

            // find user record by "user" : (2)
            var usrResult = usrMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (usrResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                throw new Exception();
            }
            var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.Type);
            if (etxResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }

            string dataType = etxResult[0].ETX_VALUE;
            users = usrResult[0];
            userRowId = users.USR_ROW_ID;
            userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
            string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
            system = etxValue.GetValue(CPAIConstantUtil.System);
            type = dataType;

            // find CPAI_ACTION_FUNCTION by "FUNCTION_TRANSACTION.FTX_FK_FUNCTION" and "USERS.USR_ROW_ID" and other params from request : (3)
            List<CPAI_ACTION_FUNCTION> acfResult = new List<CPAI_ACTION_FUNCTION>();
            if (!string.IsNullOrEmpty(userGroupDelegate)) //set user group deligate by poo 25072017
            {
                List<string> userGroups = new List<string>();
                userGroups.Add(userGroup.ToUpper());
                userGroups.Add(userGroupDelegate.ToUpper());
                acfResult = acfMan.findAcfDelegate(currentFtx.FTX_FK_FUNCTION, userGroups, userRowId, system, type);
            }
            else
            {
                acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, type);
            }
            if (acfResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }            
            for (int i = 0; i < acfResult.Count; i++)
            {
                lstAcfFunction.Add(acfResult[i].ACF_ROW_ID);
            }

            // find CPAI_ACTION_CONTROL by "FUNCTION_TRANSACTION.FTX_INDEX3" and "CPAI_ACTION_FUNCTION.ACF_ROW_ID"
            var actResult = actMan.findByCurrentStatusAndFunction(currentStatus, lstAcfFunction);            
            for (int i = 0; i < actResult.Count; i++)
            {                
                var acoResult = acoMan.findByFkActionControl(actResult[i].ACT_FK_ACTION_BUTTON);                
                if (acoResult.Count == 0)
                {                    
                    var abtResult = abtMan.findByRowIdAndType(actResult[i].ACT_FK_ACTION_BUTTON, etxValue.GetValue(CPAIConstantUtil.Button_Type));
                    if (abtResult.Count == 0)
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
                        throw new Exception();
                    }
                    else
                    {
                        if (ftxResult[0].FTX_INDEX4 == "DRAFT")
                        {
                            if (abtResult[0].ABT_BUTTON_NAME != "REJECT")
                            {
                                lstAllButton.Add(abtResult[0]);
                            }
                        }
                        else
                        {
                            lstAllButton.Add(abtResult[0]);
                        }
                }
                }                
                else
                {
                    var acoType = acoResult[0].ACO_OVERRIDE_TYPE;
                    if (acoType.Equals(TYPE_1))
                    {
                        string user = etxValue.GetValue(CPAIConstantUtil.User);
                        if (string.Equals(user, acoResult[0].ACO_OVERRIDE_VALUE, StringComparison.InvariantCultureIgnoreCase))
                        {
                            var abtResult = abtMan.findByRowId(acoResult[0].ACO_FK_ACTION_BUTTON);
                            if (abtResult.Count == 0)
                            {
                                currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
                                throw new Exception();
                            }
                            else
                            {
                                lstAllButton.Add(abtResult[0]);
                            }
                        }
                    }
                }
            }
            
            string tem = "";
            for (int i = 0; i < lstAllButton.Count; i++)
            {
                CPAI_ACTION_BUTTON abt = lstAllButton[i];
                if (!tem.Contains(abt.ABT_BUTTON_NAME))
                {
                    Button m = new Button();
                    m.name = abt.ABT_BUTTON_NAME;
                    m.page_url = abt.ABT_PAGE_URL;
                    m.call_xml = abt.ABT_XML;
                    tem = tem + "|" + abt.ABT_BUTTON_NAME;
                    list.Add(m);
                }
            }

            li.lstButton = list;
            xml = ShareFunction.XMLSerialize<ButtonList>(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);            
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            log.Info("# End State CPAIGetBtnHedgingTicketState >> getButtonList # :: Code >>> " + currentCode);
        }

        private void getDetail(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnHedgingTicketState >> getDetail #  ");
            ExtendValue respTrans = new ExtendValue();
            DataDetail respDataDetail = new DataDetail();            
            String xml;
            string oldXml;
            string dataDetail = string.Empty;

            //var dthResult = dthMan.findApproveByDthTxnRef(currentFtx.FTX_ROW_ID);
            //if (dthResult.Count == 0)
            //{
            //    currentCode = CPAIConstantRespCodeUtil.DATA_HISTORY_NOT_FOUND_RESP_CODE;
            //    //throw new Exception();
            //}
            //List<string> lstApproveItem = new List<string>();
            //for (int i = 0; i < dthResult.Count; i++)
            //{
            //    ApproveItem m = new ApproveItem();
            //    m.approve_action = dthResult[i].DTH_ACTION;
            //    m.approve_action_by = dthResult[i].DTH_ACTION_BY;
            //    lstApproveItem.Add(JSonConvertUtil.modelToJson(m));
            //}

            var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.DataDetailInput);
            if (etxResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.HEDG_TICKET_FUNCTION_CODE)
            {
                dataDetail =  HedgingTicketServiceModel.getTransactionByID(currentFtx.FTX_TRANS_ID);
            }
            else
            {
                dataDetail = etxResult[0].ETX_VALUE;
            }

            //// remove old approve_items
            //dataDetail = dataDetail.Replace(",\"approve_items\":null", "");

            //// create approve xml
            //var a = dataDetail.ReplaceLast("}", ",'approve_items': [");
            //for (int i = 0; i < lstApproveItem.Count; i++)
            //{
            //    a = a + lstApproveItem[i];
            //    if (i != lstApproveItem.Count - 1)
            //    {
            //        a = a + ",";
            //    }
            //}
            //a = a + "]}";

            respDataDetail.Text = dataDetail;            
            xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
            var newXml = oldXml + xml;
            
            respTrans.value = newXml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            log.Info("# End State CPAIGetBtnHedgingTicketState >> getTce # :: Code >>> " + currentCode);
        }

        private void getAttachItems(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnHedgingTicketState >> getAttachItems #  ");
            var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.AttachItems);
            if (etxResult.Count == 0)
            {
                // do nothing
            }
            else
            {
                AttachItems attachItems = new AttachItems();
                string attachItemsValue = "";
                if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.HEDG_TICKET_FUNCTION_CODE )
                {
                    attachItemsValue = HedgingTicketServiceModel.getAttFileByID(currentFtx.FTX_TRANS_ID);
                }
                else
                {
                    attachItemsValue = etxResult[0].ETX_VALUE;
                }

                attachItems.Text = attachItemsValue;
                String xml = ShareFunction.XMLSerialize<AttachItems>(attachItems);
                xml = CPAIXMLParser.RemoveXmlDefinition(xml);

                string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
                etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
                var newXml = oldXml + xml;

                ExtendValue respTrans = new ExtendValue();
                respTrans.value = newXml;
                respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
                etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            }
            log.Info("# End State CPAIGetBtnHedgingTicketState >> getAttachItems # :: Code >>> " + currentCode);
        }
    }
}