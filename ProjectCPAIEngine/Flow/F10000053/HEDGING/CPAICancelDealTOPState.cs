﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Flow.F10000053
{
    public class CPAICancelDealTOPState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICancelDealTOPState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string deal_action = CPAIConstantUtil.ACTION_FAIL;
                string deal_reason = "System Error";

                string pre_deal_no = etxValue.GetValue(CPAIConstantUtil.Hedg_Pre_Deal_No);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string create_by = etxValue.GetValue(CPAIConstantUtil.CreateBy);
                string json = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

                string trx_id = etxValue.GetValue(CPAIConstantUtil.Hedg_Create_Deal_Top_Tran_ID);

                ExtendValue extTemp = new ExtendValue { value = "" };

                HedgDealRootObject objRoot = new HedgDealRootObject();
                ResponseData resData = new ResponseData();
                resData = CancelDeal(trx_id, type, create_by,"Cancel deal by pre-deal");

                if (resData != null && resData.result_code != null && resData.result_code == "1")
                {
                    //respons from function F10000012
                  
                    currentCode = resData.result_code;
                }
                else
                {
                    deal_reason = resData.result_desc;
                    currentCode = resData.result_code;
                }

                extTemp = new ExtendValue { value = deal_action };
                try
                {
                    etxValue.Add(CPAIConstantUtil.Hedg_Deal_Action, extTemp);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.Hedg_Deal_Action] = extTemp;
                }
                extTemp = new ExtendValue { value = deal_reason };
                try
                {
                    etxValue.Add(CPAIConstantUtil.Hedg_Deal_Reason, extTemp);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.Hedg_Deal_Reason] = extTemp;
                }


                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
               

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICancelDealTOPState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICancelDealTOPState # :: Exception >>> " + ex);
                log.Error("CPAICancelDealTOPState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public ResponseData CancelDeal(string txn_id, string type, string create_by, string note)
        {
            log.Debug("F10000053 resp call function F10000037 >> " + "CancelDeal");

            //get request txn
            FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
            FUNCTION_TRANSACTION ft = ftxMan.GetFnTranDetail(txn_id);
            string req_txn_id = ft.FTX_REQ_TRANS;
            //call function F10000012 : CANCEL
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000037;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = req_txn_id;
            req.State_name = "CPAIVerifyRequireInputContinueState";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_FAIL });
            req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.ACTION_FAIL });
            req.Req_parameters.P.Add(new P { K = "user", V = create_by });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_DEAL });
            req.Req_parameters.P.Add(new P { K = "note", V = note });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            log.Debug("F10000053 resp call function F10000037 >> " + resData.result_code);

            return resData;
        }
    }
}