﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using System.Web.Script.Serialization;


namespace ProjectCPAIEngine.Flow.F10000053
{
    public class CPAICreateDealTOPState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICreateDealTOPState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                //string deal_action = CPAIConstantUtil.ACTION_FAIL_1;
                string deal_reason = "System Error";
               
                string pre_deal_no = etxValue.GetValue(CPAIConstantUtil.Hedg_Pre_Deal_No);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string create_by = etxValue.GetValue(CPAIConstantUtil.CreateBy);
                string json = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

                HedgDealRootObject objRoot = new HedgDealRootObject();
                objRoot = JSonConvertUtil.jsonToModel<HedgDealRootObject>(json);

                ResponseData resData = new ResponseData();
                objRoot = JSonConvertUtil.jsonToModel<HedgDealRootObject>(json);
                objRoot.hedging_deal.ref_pre_deal_no = pre_deal_no;
                objRoot.hedging_deal.labix_trade_thru_top = "F";
                objRoot.hedging_deal.labix_fee = "";
                objRoot.hedging_deal.labix_fee_unit = "";

                json = new JavaScriptSerializer().Serialize(objRoot);
                resData = CreateDeal(json, type, create_by);

                if (resData != null && resData.result_code != null && resData.result_code == "1")
                {
                    //respons from function F10000012
                    deal_reason = "DEAL for TOP transaction id :" + resData.transaction_id;
                    //deal_action = CPAIConstantUtil.ACTION_SUBMIT;
                    currentCode = resData.result_code;
                    ExtendValue extFlag = new ExtendValue();
                    extFlag.value = CPAIConstantUtil.YES_FLAG;
                    etxValue.Add(CPAIConstantUtil.Hedg_Create_Deal_Top, extFlag);

                    extFlag.value = resData.transaction_id;
                    etxValue.Add(CPAIConstantUtil.Hedg_Create_Deal_Top_Tran_ID, extFlag);

                    
                }
                else
                {
                    deal_reason = resData.result_desc;
                    //CPAIConstantUtil.Message
                    if (resData.resp_parameters != null && resData.resp_parameters[0] != null)
                    {
                        foreach (p data in resData.resp_parameters)
                        {
                            if (data.k.Equals(CPAIConstantUtil.Message))
                            {
                                deal_reason = data.v;
                                break;
                            }
                        }
                    }
                    currentCode = resData.result_code;
                    //deal_action = CPAIConstantUtil.ACTION_FAIL_1;

                    ExtendValue extFlag = new ExtendValue();
                    extFlag.value = CPAIConstantUtil.NO_FLAG;
                    etxValue.Add(CPAIConstantUtil.Hedg_Create_Deal_Top, extFlag);

                    extFlag.value = CPAIConstantUtil.ACTION_FAIL;
                    etxValue.Add(CPAIConstantUtil.NextStatus, extFlag);
                }


                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                if (!string.IsNullOrEmpty(deal_reason))
                    stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + deal_reason + "]";
                log.Info("# End State CPAICreateDealTOPState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICreateDealTOPState # :: Exception >>> " + ex);
                log.Error("CPAICreateDealTOPState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public ResponseData CreateDeal(string json, string type, string create_by)
        {
            log.Debug("resp F10000053 call function F10000037 >> " + "CreateDealTOP");

            //call function F10000037 : SUMMIT
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000037;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_SUBMIT });
            req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.ACTION_SUBMIT });
            req.Req_parameters.P.Add(new P { K = "user", V = create_by });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_DEAL });
            req.Req_parameters.P.Add(new P { K = "type", V = type });
            req.Req_parameters.P.Add(new P { K = "note", V = "Auto Create" });
            req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            log.Debug("resp F10000053 call function F10000037 >> " + resData.result_code);

            return resData;
        }

    }
}