﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.Flow.F10000053
{
    public class CPAIHedgingPreDealUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingPreDealUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                DateTime now = DateTime.Now;
                string nowStr = ShareFn.ConvertDateTimeToDateStringFormat(now, "dd/MM/yyyy HH:mm");

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

                HedgDealRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<HedgDealRootObject>(item);

                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string user = etxValue.GetValue(CPAIConstantUtil.User);

                    if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
                    {
                        
                        string t_deal_date = string.IsNullOrEmpty(dataDetail.hedging_deal.deal_date) ? "" : dataDetail.hedging_deal.deal_date;
                        string t_pre_deal_no = etxValue.GetValue(CPAIConstantUtil.DocNo);
                        string t_trade_for = string.IsNullOrEmpty(dataDetail.hedging_deal.labix_trade_thru_top) ? "" : dataDetail.hedging_deal.labix_trade_thru_top;
                        string t_hedg_type = string.IsNullOrEmpty(dataDetail.hedging_deal.fw_hedged_type) ? "" : dataDetail.hedging_deal.fw_hedged_type;
                        string t_tool = string.IsNullOrEmpty(dataDetail.hedging_deal.tool) ? "" : dataDetail.hedging_deal.tool;
                        string t_underlying = string.IsNullOrEmpty(dataDetail.hedging_deal.underlying) ? "" : dataDetail.hedging_deal.underlying;
                        string t_underlying_vs = string.IsNullOrEmpty(dataDetail.hedging_deal.underlying_vs) ? "" : dataDetail.hedging_deal.underlying_vs;
                        string t_company = string.IsNullOrEmpty(dataDetail.hedging_deal.company) ? "" : dataDetail.hedging_deal.company;
                        string t_counterparty = string.IsNullOrEmpty(dataDetail.hedging_deal.counter_parties) ? "" : dataDetail.hedging_deal.counter_parties;

                        //set reject flag
                        //dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");

                        //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                        #region Set dataDetail

                        etxValue.SetValue(CPAIConstantUtil.System, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Type, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.Type), encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_Date, new ExtendValue { value = t_deal_date, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Pre_Deal_No, new ExtendValue { value = t_pre_deal_no, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Trade_For, new ExtendValue { value = t_trade_for, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Type, new ExtendValue { value = t_hedg_type, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Tools, new ExtendValue { value = t_tool, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Underlying, new ExtendValue { value = t_underlying, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Underlying_vs, new ExtendValue { value = t_underlying_vs, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Company, new ExtendValue { value = t_company, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Counterpartie, new ExtendValue { value = t_counterparty, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Create_by, new ExtendValue { value = user, encryptFlag = "N" });

                        //set data_detail
                        if (dataDetail != null)
                        {
                            dataDetail.hedging_deal.status = nextStatus;
                            dataDetail.hedging_deal.ref_pre_deal_no = t_pre_deal_no;
                            dataDetail.hedging_deal.update_by = user;
                            dataDetail.hedging_deal.update_date = nowStr;

                            if (dataDetail.hedging_deal.create_by == null)
                                dataDetail.hedging_deal.create_by = user;
                            if (dataDetail.hedging_deal.create_date == null)
                                dataDetail.hedging_deal.create_date = nowStr;

                            if (dataDetail.hedging_deal.revision == null)
                                dataDetail.hedging_deal.revision = "0";

                            string dataDetailString = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetailInput, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                        }

                        #endregion

                        #region add data history
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = user;
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = user;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = user;
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                }

                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingPreDealUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingPreDealUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingPreDealUpdateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}