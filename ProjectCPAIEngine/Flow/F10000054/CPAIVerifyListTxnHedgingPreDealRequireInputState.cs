﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using System.DirectoryServices;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.Flow.F10000054
{
    public class CPAIVerifyListTxnHedgingPreDealRequireInputState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyListTxnHedgingPreDealRequireInputState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            string currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

            try
            {                
                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Channel)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CHANNEL_RESP_CODE;
                }
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_SYSTEM_RESP_CODE;
                }
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                }
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.PageNumber)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_PAGE_NUMBER_RESP_CODE;
                }
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.RowsPerPage)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_ROWS_PER_PAGE_RESP_CODE;
                }
                /*else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.FromDate)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_INPUT_DATE_RESP_CODE;
                }
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.ToDate)))
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_INPUT_DATE_RESP_CODE;
                }*/
                else
                {
                    /*
                    ExtendValue extStatus = new ExtendValue();
                    UserGroupDAL service = new UserGroupDAL();
                    string user = etxValue.GetValue(CPAIConstantUtil.User);
                    string system = etxValue.GetValue(CPAIConstantUtil.System);
                    CPAI_USER_GROUP r = service.findByUserAndSystem(user, system);
                    if (r != null && r.USG_USER_GROUP != null)
                    {
                        extStatus.value = r.USG_USER_GROUP;
                        etxValue.SetValue(CPAIConstantUtil.UserGroup, extStatus);
                        //
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                    }
                    */

                    //set user group
                    ExtendValue extStatus = new ExtendValue();
                    UserGroupDAL service = new UserGroupDAL();
                    CPAI_USER_GROUP r = service.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    //set user group deligate by poo 25072017
                    CPAI_USER_GROUP dlg = service.findByUserAndSystemDelegate(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (r != null || dlg != null)
                    {
                        if (r != null && r.USG_USER_GROUP != null)
                        {
                            extStatus.value = r.USG_USER_GROUP.ToUpper();
                            etxValue.SetValue(CPAIConstantUtil.UserGroup, extStatus);
                        }
                        if (dlg != null && dlg.USG_USER_GROUP != null)
                        {
                            extStatus = new ExtendValue();
                            extStatus.value = dlg.USG_USER_GROUP.ToUpper();
                            etxValue.Add(CPAIConstantUtil.UserGroupDelegate, extStatus);
                        }
                        //

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                    }
                }                
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyListTxnHedgingPreDealRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIVerifyListTxnHedgingPreDealRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyListTxnHedgingPreDealRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
