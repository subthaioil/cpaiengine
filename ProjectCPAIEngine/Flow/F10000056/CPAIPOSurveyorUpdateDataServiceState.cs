﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using static ProjectCPAIEngine.Model.VatpostingCreateModel;
using static ProjectCPAIEngine.Model.VatMIROInvoiceServiceModel;
using System.Globalization;
using ProjectCPAIEngine.DAL.DALPCF;

namespace ProjectCPAIEngine.Flow.F10000056
{
    public class CPAIPOSurveyorUpdateDataServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAIPOSurveyorUpdateDataServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            //CultureInfo provider = new CultureInfo("en-US");
            //string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                POSuveyorModel dataDetail = JSonConvertUtil.jsonToModel<POSuveyorModel>(item); 
                var itemItem = dataDetail.mPOITEM.FirstOrDefault();

                string StatusState = etxValue.GetValue(CPAIConstantUtil.Status);
                string Sap_logon = etxValue.GetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_logon);
                string sap_status_msg = etxValue.GetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_status_msg);
                string poSuveyor = etxValue.GetValue(ConstantUtil.RESP + CPAIConstantUtil.POSuveyor);

                UpdateResult(StatusState, Sap_logon, sap_status_msg, poSuveyor, itemItem.RET_ITEM, itemItem.TRACKINGNO);
                // Create, Change, Delete


                stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                respCodeManagement.setCurrentCodeMapping(stateModel);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIPOSurveyorUpdateDataServiceState # :: Exception >>> " + ex);
                log.Error("CPAIPOSurveyorUpdateDataServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private Boolean UpdateResult(string ActionType, string SapSTS, string SapMsg, string SapPO, string itemNo, string tripno)
        {
            Boolean ret = true;
            if (!SapSTS.Equals("S")) {
                return false;
            }

            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    CRUDE_IMPORT_PLAN_DAL dataDAL = new CRUDE_IMPORT_PLAN_DAL();

                    if (ActionType.Equals("Create") || ActionType.Equals("Change"))
                    {
                        dataDAL.UpdateSurveyorResult(tripno, itemNo, SapPO, context);
                    }
                    else if (ActionType.Equals("Delete"))
                    {
                        dataDAL.DeleteSurveyorResult(tripno, itemNo, SapPO, context);
                    }

                }
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }


    }
}
