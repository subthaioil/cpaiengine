﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using static ProjectCPAIEngine.Model.POSuveyorModel;

using System.Globalization;

namespace ProjectCPAIEngine.Flow.F10000056
{
    public class CPAIPOSurveyorUpdateServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAIPOSurveyorUpdateServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {

                    POSuveyorModel dataDetail = JSonConvertUtil.jsonToModel<POSuveyorModel>(item);


                    //POHeader
                    var cfgPoHeader = getPoHeader();
                    POSuveyorModel temp = new POSuveyorModel();
                    temp.mPOHEADER = new List<BAPIMEPOHEADER>();
                    var itemFirst = dataDetail.mPOHEADER.FirstOrDefault();
                    temp.PURCHASEORDER = itemFirst.PO_NUMBER;

                    Boolean nopono = String.IsNullOrEmpty(itemFirst.PO_NUMBER);
                    Boolean noMflag = String.IsNullOrEmpty(itemFirst.Mflag);
                    if ((!nopono && noMflag)) //Change F&T=F
                    {
                        
                        

                        //POITEM
                        var cfgPoItem = getPoItem();
                        temp.mPOITEM = new List<BAPIMEPOITEM>();
                        var itemPt = dataDetail.mPOITEM.FirstOrDefault();
                        BAPIMEPOITEM tmpPt = new BAPIMEPOITEM();
                        tmpPt.PO_ITEM = itemPt.PO_ITEM;//Change
                        tmpPt.SHORT_TEXT = itemPt.SHORT_TEXT;//PCF_PO_SURVEYOR.REMARK
                        tmpPt.PLANT = itemPt.PLANT;
                        tmpPt.TRACKINGNO = itemPt.TRACKINGNO;
                        tmpPt.MATL_GROUP = cfgPoItem.MATL_GROUP;
                        tmpPt.QUANTITY = cfgPoItem.QUANTITY;
                        tmpPt.PO_UNIT = cfgPoItem.PO_UNIT;
                        tmpPt.NET_PRICE = itemPt.NET_PRICE;//PCF_PO_SURVEYOR.AMOUNT
                        tmpPt.ACCTASSCAT = cfgPoItem.ACCTASSCAT;
                        temp.mPOITEM.Add(tmpPt);

                        //POSCHEDULE
                        var cfgPoSchedule = getPoSchedule();
                        temp.mPOSCHEDULE = new List<BAPIMEPOSCHEDULE>();
                        var itemPsche = dataDetail.mPOSCHEDULE.FirstOrDefault();
                        BAPIMEPOSCHEDULE tmpPsche = new BAPIMEPOSCHEDULE();
                        tmpPsche.PO_ITEM = itemPsche.PO_ITEM;//Change
                        tmpPsche.SCHED_LINE = cfgPoSchedule.SCHED_LINE;
                        tmpPsche.DEL_DATCAT_EXT = cfgPoSchedule.DEL_DATCAT_EXT;
                        tmpPsche.DELIVERY_DATE = itemPsche.DELIVERY_DATE;// //PCF_PO_SURVEYOR.LOADIG_DATE หรือ DISCHARGING_DATE
                        tmpPsche.QUANTITY = cfgPoSchedule.QUANTITY;
                        temp.mPOSCHEDULE.Add(tmpPsche);

                        //POScreate = true;

                        ConfigManagement configManagement = new ConfigManagement();
                        String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        String content = js.Serialize(temp);
                        var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        var jsonConfig = javaScriptSerializer.Deserialize<ConfigServiceModel>(config); //convert jason


                        POSuveyorChangeServiceConnectorImpl POSuveyorChangeService = new POSuveyorChangeServiceConnectorImpl();
                        String confChange = js.Serialize(jsonConfig.SUVEYORchange);
                        DownstreamResponse<string> resChange = POSuveyorChangeService.connect(confChange, content);


                        ExtendValue extRowsStatusState = new ExtendValue
                        { value = "Change", encryptFlag = ConstantDBUtil.NO_FLAG };
                        etxValue.Add(CPAIConstantUtil.Status, extRowsStatusState);

                        //etxValue  
                        string Sap_logon = resChange.responseData.ToString().Substring(resChange.responseData.ToString().IndexOf("|") + 1, resChange.responseData.ToString().IndexOf("M") - resChange.responseData.ToString().IndexOf("|") - 1);
                        string Sap_status_msg = resChange.responseData.ToString().Substring(resChange.responseData.ToString().IndexOf("M") + 1, resChange.responseData.ToString().IndexOf("R") - resChange.responseData.ToString().IndexOf("M") - 1);
                        string POSuveyor_change = resChange.responseData.ToString().Substring(resChange.responseData.ToString().IndexOf("R") + 1);

                        ExtendValue extRowsSaplogon = new ExtendValue
                        { value = Sap_logon, encryptFlag = ConstantDBUtil.NO_FLAG };
                        etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_logon, extRowsSaplogon);

                        ExtendValue extRowsStatus = new ExtendValue
                        { value = Sap_status_msg, encryptFlag = ConstantDBUtil.NO_FLAG };
                        etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_status_msg, extRowsStatus);

                        ExtendValue extRowsPOsuveyorChange = new ExtendValue
                        { value = POSuveyor_change, encryptFlag = ConstantDBUtil.NO_FLAG };
                        etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.POSuveyor, extRowsPOsuveyorChange);

                    }

                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0]; ;
                    respCodeManagement.setCurrentCodeMapping(stateModel);

                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIPOSurveyorUpdateServiceState # :: Exception >>> " + ex);
                log.Error("CPAIPOSurveyorUpdateServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
           
    public POHEADER getPoHeader()
    {
        var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

        string JsonD = MasterData.GetJsonMasterSetting("CIP_Suveyor");
        JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
        GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));
        return dataList.POHEADER;
    }

    public POACCOUNT getPoAccount()
    {
        var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

        string JsonD = MasterData.GetJsonMasterSetting("CIP_Suveyor");
        JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
        GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

        return dataList.POACCOUNT;
    }

    public POCOND getPoCond()
    {
        var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

        string JsonD = MasterData.GetJsonMasterSetting("CIP_Suveyor");
        JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
        GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

        return dataList.POCOND;
    }
    public POITEM getPoItem()
    {
        var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

        string JsonD = MasterData.GetJsonMasterSetting("CIP_Suveyor");
        JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
        GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

        return dataList.POITEM;
    }


    public POSCHEDULE getPoSchedule()
    {
        var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

        string JsonD = MasterData.GetJsonMasterSetting("CIP_Suveyor");
        JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
        GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

        return dataList.POSCHEDULE;
    }


    [Serializable]
    public class GlobalConfigSuveyor
    {
        public POHEADER POHEADER { get; set; }
        public POACCOUNT POACCOUNT { get; set; }
        public POCOND POCOND { get; set; }
        public POITEM POITEM { get; set; }
        public POSCHEDULE POSCHEDULE { get; set; }
    }

    public class POHEADER
    {
        public string DOC_TYPE { get; set; }
        public string PMNTTRMS { get; set; }
        public string PUR_GROUP_CMCS { get; set; }
        public string PUR_GROUP_CMPS { get; set; }
    }

    public class POACCOUNT
    {
        public string SERIAL_NO { get; set; }
        public string QUANTITY { get; set; }
        public string GL_ACCOUNT { get; set; }
        public string COSTCENTER { get; set; }
        public string CO_AREA { get; set; }

    }

    public class POCOND
    {
        public string COND_ST_NO { get; set; }
        public string CONDCOUNT { get; set; }
        public string CHANGE_ID { get; set; }

    }

    public class POITEM
    {
        public string MATL_GROUP { get; set; }
        public decimal QUANTITY { get; set; }
        public string PO_UNIT { get; set; }
        public string ACCTASSCAT { get; set; }

    }

    public class POSCHEDULE
    {
        public string SCHED_LINE { get; set; }
        public string DEL_DATCAT_EXT { get; set; }
        public decimal QUANTITY { get; set; }
    }

    public class ConfigModel
    {
        public string sap_url { get; set; }
        public string sap_user { get; set; }
        public string sap_pass { get; set; }
        public string connect_time_out { get; set; }
    }

    public class ConfigServiceModel
    {
        public ConfigModel SUVEYORcreate { get; set; }
        public ConfigModel SUVEYORchange { get; set; }
    }

}
}
