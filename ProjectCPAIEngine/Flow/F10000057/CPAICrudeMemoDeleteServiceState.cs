﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using System.Globalization;
using static ProjectCPAIEngine.Model.POSuveyorModel;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Flow.F10000057
{
    public class CPAICrudeMemoDeleteServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICrudeMemoDeleteServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                ConfigManagement configManagement = new ConfigManagement();
                String content = stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (item != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    CrudeApproveFormViewModel_Detail model = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(item);

                    CrudeImportPlanMemoServiceModel serviceModel = new CrudeImportPlanMemoServiceModel();
                    var resultModel = serviceModel.DeleteMemoRecord(model);

                    string newXml = js.Serialize(resultModel);
                    ExtendValue respTrans = new ExtendValue();
                    respTrans.value = newXml;
                    respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
                    etxValue.SetValue(CPAIConstantUtil.DataDetailInput, respTrans);

                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    stateModel.BusinessModel.currentCode = currentCode;
                    respCodeManagement.setCurrentCodeMapping(stateModel);
                }
                else
                {
                    throw new Exception("Data input is null");
                }

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICrudeMemoDeleteServiceState # :: Exception >>> " + ex);
                log.Error("CPAICrudeMemoDeleteServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public POHEADER getPoHeader()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SUVEYOR");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));
            return dataList.POHEADER;
        }

        public POACCOUNT getPoAccount()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SUVEYOR");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

            return dataList.POACCOUNT;
        }

        public POCOND getPoCond()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SUVEYOR");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

            return dataList.POCOND;
        }
        public POITEM getPoItem()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SUVEYOR");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

            return dataList.POITEM;
        }


        public POSCHEDULE getPoSchedule()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SUVEYOR");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSuveyor dataList = (GlobalConfigSuveyor)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSuveyor));

            return dataList.POSCHEDULE;
        }


        [Serializable]
        public class GlobalConfigSuveyor
        {
            public POHEADER POHEADER { get; set; }
            public POACCOUNT POACCOUNT { get; set; }
            public POCOND POCOND { get; set; }
            public POITEM POITEM { get; set; }
            public POSCHEDULE POSCHEDULE { get; set; }
        }

        public class POHEADER
        {
            public string DOC_TYPE { get; set; }
            public string PMNTTRMS { get; set; }
            public string PUR_GROUP_CMCS { get; set; }
            public string PUR_GROUP_CMPS { get; set; }
        }

        public class POACCOUNT
        {
            public string SERIAL_NO { get; set; }
            public string QUANTITY { get; set; }
            public string GL_ACCOUNT { get; set; }
            public string COSTCENTER { get; set; }
            public string CO_AREA { get; set; }

        }

        public class POCOND
        {
            public string COND_ST_NO { get; set; }
            public string CONDCOUNT { get; set; }
            public string CHANGE_ID { get; set; }

        }

        public class POITEM
        {
            public string MATL_GROUP { get; set; }
            public decimal QUANTITY { get; set; }
            public string PO_UNIT { get; set; }
            public string ACCTASSCAT { get; set; }

        }

        public class POSCHEDULE
        {
            public string SCHED_LINE { get; set; }
            public string DEL_DATCAT_EXT { get; set; }
        }

        public class ConfigModel
        {
            public string sap_url { get; set; }
            public string sap_user { get; set; }
            public string sap_pass { get; set; }
            public string connect_time_out { get; set; }
        }

        public class ConfigServiceModel
        {
            public ConfigModel SUVEYORcrete { get; set; }
            public ConfigModel SUVEYORchange { get; set; }
        }

    }
}
