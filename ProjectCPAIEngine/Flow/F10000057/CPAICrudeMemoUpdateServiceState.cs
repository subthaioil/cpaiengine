﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using static ProjectCPAIEngine.Model.VatpostingCreateModel;
using static ProjectCPAIEngine.Model.VatMIROInvoiceServiceModel;
using System.Globalization;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Flow.F10000057
{
    public class CPAICrudeMemoUpdateServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICrudeMemoUpdateServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                ConfigManagement configManagement = new ConfigManagement();
                String content = stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (item != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    CrudeApproveFormViewModel_Detail model = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(item);

                    CrudeImportPlanMemoServiceModel serviceModel = new CrudeImportPlanMemoServiceModel();
                    var resultModel = serviceModel.UpdateCrudeMemoRecord(model);

                    string newXml = js.Serialize(resultModel);
                    ExtendValue respTrans = new ExtendValue();
                    respTrans.value = newXml;
                    respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
                    etxValue.SetValue(CPAIConstantUtil.DataDetailInput, respTrans);

                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    stateModel.BusinessModel.currentCode = currentCode;
                    respCodeManagement.setCurrentCodeMapping(stateModel);
                }
                else
                {
                    throw new Exception("Data input is null");
                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICrudeMemoUpdateServiceState # :: Exception >>> " + ex);
                log.Error("CPAICrudeMemoUpdateServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
    }
}
