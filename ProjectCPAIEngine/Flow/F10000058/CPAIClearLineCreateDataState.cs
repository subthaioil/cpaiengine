﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPCF;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000058
{
    public class CPAIClearLineCreateDataState : BasicBean, StateFlowAction
    {

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIClearLineCreateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    ClearLine.ClearLineDetailModel dataDetail = JSonConvertUtil.jsonToModel<ClearLine.ClearLineDetailModel>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    bool isRunDB = false;

                    isRunDB = InsertUpdateDB(dataDetail);

                    if (isRunDB)
                    {
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion

                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                //stateModel.BusinessModel.etxValue = etxValue;
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIClearLineCreateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIClearLineCreateDataState # :: Exception >>> " + ex);
                log.Error("CPAIClearLineCreateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertUpdateDB(ClearLine.ClearLineDetailModel dataDetail)
        {
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            bool isSuccess = false;

            try
            {
                CLEAR_LINE_DAL dalClearLine = new CLEAR_LINE_DAL();
                CLEAR_LINE_CRUDE entClearLineCrude = new CLEAR_LINE_CRUDE();                

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var _qry = context.CLEAR_LINE_CRUDE.Where(z => z.CLC_TRIP_NO.ToUpper().Equals(dataDetail.sTripNo.ToUpper())
                                                                && z.CLC_CRUDE_TYPE_ID.ToUpper().Equals(dataDetail.sCrude.ToUpper())
                                                                && z.CLC_CUST_NUM.ToUpper().Equals(dataDetail.sCustomer.ToUpper())
                                                                );
                            if (_qry.ToList().Count > 0)
                            {
                                entClearLineCrude = (CLEAR_LINE_CRUDE)_qry.ToList()[0];
                            }
                            //else
                            //{
                            //    entClearLineCrude.CLC_TRIP_NO = genTripNo(dataDetail.sPlant, context);
                            //}

                            entClearLineCrude.CLC_TRIP_NO = dataDetail.sTripNo;
                            entClearLineCrude.CLC_COMPANY = dataDetail.sCompany;
                            entClearLineCrude.CLC_PO_NO = dataDetail.sPONo;
                            entClearLineCrude.CLC_CUST_NUM = dataDetail.sCustomer;
                            if (String.IsNullOrEmpty(dataDetail.sDeliveryDate)) { entClearLineCrude.CLC_DELIVERY_DATE = null; }else { entClearLineCrude.CLC_DELIVERY_DATE= DateTime.ParseExact(dataDetail.sDeliveryDate, format, provider); }
                            if (String.IsNullOrEmpty(dataDetail.sDueDate)) { entClearLineCrude.CLC_DUE_DATE = null; } else { entClearLineCrude.CLC_DUE_DATE = DateTime.ParseExact(dataDetail.sDueDate, format, provider); }
                            entClearLineCrude.CLC_PLANT = dataDetail.sPlant;
                            entClearLineCrude.CLC_CRUDE_TYPE_ID = dataDetail.sCrude;
                            entClearLineCrude.CLC_VESSEL_ID = dataDetail.sVesselName;
                            entClearLineCrude.CLC_SALE_UNIT = dataDetail.sSaleUnitType;
                            entClearLineCrude.CLC_INVOICE_FIGURE = dataDetail.sInvoiceFigureType;


                            if (String.IsNullOrEmpty(dataDetail.sVolumeBBL)) { entClearLineCrude.CLC_VOLUME_BBL = null; }else { entClearLineCrude.CLC_VOLUME_BBL = Decimal.Parse(dataDetail.sVolumeBBL); }
                            if (String.IsNullOrEmpty(dataDetail.sVolumeLitres)) { entClearLineCrude.CLC_VOLUME_LITE = null; } else { entClearLineCrude.CLC_VOLUME_LITE = Decimal.Parse(dataDetail.sVolumeLitres); }
                            if (String.IsNullOrEmpty(dataDetail.sVolumeMT)) { entClearLineCrude.CLC_VOLUME_MT = null; } else { entClearLineCrude.CLC_VOLUME_MT = Decimal.Parse(dataDetail.sVolumeMT); }
                            if (String.IsNullOrEmpty(dataDetail.sOutturnBBL)) { entClearLineCrude.CLC_OUTTURN_BBL = null; } else { entClearLineCrude.CLC_OUTTURN_BBL = Decimal.Parse(dataDetail.sOutturnBBL); }
                            if (String.IsNullOrEmpty(dataDetail.sOutturnLitres)) { entClearLineCrude.CLC_OUTTURN_LITE = null; } else { entClearLineCrude.CLC_OUTTURN_LITE = Decimal.Parse(dataDetail.sOutturnLitres); }
                            if (String.IsNullOrEmpty(dataDetail.sOutturnMT)) { entClearLineCrude.CLC_OUTTURN_MT = null; } else { entClearLineCrude.CLC_OUTTURN_MT = Decimal.Parse(dataDetail.sOutturnMT); }                                                        
                            entClearLineCrude.CLC_UNIT_TOTAL = dataDetail.sTotalUnit;
                            if (String.IsNullOrEmpty(dataDetail.sExchangeRate)) { entClearLineCrude.CLC_EXCHANGE_RATE = null; } else { entClearLineCrude.CLC_EXCHANGE_RATE = Decimal.Parse(dataDetail.sExchangeRate); }
                            if (String.IsNullOrEmpty(dataDetail.sTotalAmount)) { entClearLineCrude.CLC_TOTAL = null; } else { entClearLineCrude.CLC_TOTAL = Decimal.Parse(dataDetail.sTotalAmount); }                            
                            entClearLineCrude.CLC_REMARK = dataDetail.sRemark;
                            entClearLineCrude.CLC_PROCESS_TYPE = dataDetail.sProcessType;
                            
                            if(_qry.ToList().Count == 0)
                            {
                                entClearLineCrude.CLC_CREATE_DATE = DateTime.Now.Date;
                                entClearLineCrude.CLC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");
                                entClearLineCrude.CLC_CREATE_BY = Const.User.UserName;

                                dalClearLine.SaveClearLineCrude(entClearLineCrude);
                                int k = 1;
                                foreach(var item in dataDetail.PriceList)
                                {
                                    CLEAR_LINE_PRICE entClearLinePrice = new CLEAR_LINE_PRICE();
                                    entClearLinePrice.CLP_TRIP_NO = dataDetail.sTripNo;
                                    //entClearLinePrice.CLP_NUM = k;
                                    entClearLinePrice.CLP_NUM = Decimal.Parse(item.sNum ?? "0");
                                    if (String.IsNullOrEmpty(item.sDueDate)) { entClearLinePrice.CLP_DATE_PRICE = null; }else { entClearLinePrice.CLP_DATE_PRICE = DateTime.ParseExact(item.sDueDate, format, provider); }
                                    if (String.IsNullOrEmpty(item.sPrice)) { entClearLinePrice.CLP_PRICE = null; }else { entClearLinePrice.CLP_PRICE = Decimal.Parse(item.sPrice); }                                    
                                    entClearLinePrice.CLP_UNIT_ID = item.sPriceUnit;
                                    if (String.IsNullOrEmpty(item.sInvoice)) { entClearLinePrice.CLP_INVOICE = null; } else { entClearLinePrice.CLP_INVOICE = Decimal.Parse(item.sInvoice); }                                    
                                    entClearLinePrice.CLP_UNIT_INV = item.sInvUnit;

                                    dalClearLine.SaveClearLinePrice(entClearLinePrice);
                                    k++;
                                }
                            }
                            else
                            {
                                //entClearLineCrude.CLC_PRICE_PER = dataDetail.sPricePer;
                                //if (String.IsNullOrEmpty(dataDetail.sPrice)) { entClearLineCrude.CLC_PRICE = null; }else { entClearLineCrude.CLC_PRICE = Decimal.Parse(dataDetail.sPrice); }
                                //entClearLineCrude.CLC_UNIT_ID = dataDetail.sUnitID;
                                //entClearLineCrude.CLC_TYPE = dataDetail.sType;
                                //if (String.IsNullOrEmpty(dataDetail.sCreateDate)) { entClearLineCrude.CLC_CREATE_DATE = null; }else { entClearLineCrude.CLC_CREATE_DATE = DateTime.ParseExact(dataDetail.sCreateDate, format, provider); }
                                //entClearLineCrude.CLC_CREATE_BY = dataDetail.sCreateBy;
                                //entClearLineCrude.CLC_CREATE_TIME = dataDetail.sCreateTime;
                                //entClearLineCrude.CLC_LAST_MODIFY_DATE = DateTime.ParseExact(DateTime.Now.ToString(), format, provider);
                                //entClearLineCrude.CLC_LAST_MODIFY_TIME = DateTime.Now.ToString("HH:mm:ss");
                                //entClearLineCrude.CLC_LAST_MODIFY_BY = Const.User.UserName.Substring(0,20);
                                //entClearLineCrude.CLC_STATUS = dataDetail.sStatus;
                                //entClearLineCrude.CLC_RELEASE = dataDetail.sRelease;
                                //entClearLineCrude.CLC_TANK = dataDetail.sTank;
                                //entClearLineCrude.CLC_SALEORDER = dataDetail.sSaleOrder;
                                //entClearLineCrude.CLC_PRICE_RELEASE = dataDetail.sPriceRelease;
                                //entClearLineCrude.CLC_DISTRI_CHANN = dataDetail.sDistriChann;
                                //entClearLineCrude.CLC_TABLE_NAME = dataDetail.sTableName;
                                //entClearLineCrude.CLC_MEMO = dataDetail.sMemo;
                                //entClearLineCrude.CLC_DATE_SAP = dataDetail.sDateSAP;
                                //entClearLineCrude.CLC_FI_DOC = dataDetail.sFIDoc;
                                //entClearLineCrude.CLC_DO_NO = dataDetail.sDONo;
                                //entClearLineCrude.CLC_STATUS_TO_SAP = dataDetail.sStatusToSAP;
                                //entClearLineCrude.CLC_FI_DOC_REVERSE = dataDetail.sFIDocReverse;
                                //entClearLineCrude.CLC_INCOTERM_TYPE = dataDetail.sIncoTermType;
                                //entClearLineCrude.CLC_TEMP = Decimal.Parse(dataDetail.sTemp);
                                //entClearLineCrude.CLC_DENSITY = Decimal.Parse(dataDetail.sDensity);
                                //entClearLineCrude.CLC_STORAGE_LOCATION = dataDetail.sStorageLocation;
                                //entClearLineCrude.CLC_UPDATE_DO = dataDetail.sUpdateDO;
                                //entClearLineCrude.CLC_UPDATE_GI = dataDetail.sUpdateGI;
                                entClearLineCrude.CLC_LAST_MODIFY_BY = Const.User.UserName;
                                entClearLineCrude.CLC_LAST_MODIFY_DATE = DateTime.Now.Date;
                                entClearLineCrude.CLC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");

                                dalClearLine.UpdateClearLineCrude(entClearLineCrude, context);

                                foreach(var item in dataDetail.PriceList)
                                {
                                    CLEAR_LINE_PRICE entClearLinePrice = new CLEAR_LINE_PRICE();
                                    entClearLinePrice.CLP_TRIP_NO = dataDetail.sTripNo;
                                    if (String.IsNullOrEmpty(item.sPrice)) { entClearLinePrice.CLP_PRICE = null; }else { entClearLinePrice.CLP_PRICE = Decimal.Parse(item.sPrice);}
                                    if (String.IsNullOrEmpty(item.sDueDate)) { entClearLinePrice.CLP_DATE_PRICE = null; }else { entClearLinePrice.CLP_DATE_PRICE = DateTime.ParseExact(item.sDueDate, format, provider); }
                                    if (String.IsNullOrEmpty(item.sNum)) { entClearLinePrice.CLP_NUM = 0; }else { entClearLinePrice.CLP_NUM = Decimal.Parse(item.sNum ?? "0"); }                                    
                                    entClearLinePrice.CLP_UNIT_ID = item.sPriceUnit;
                                    //entClearLinePrice.CLP_RELEASE = item.sRelease;
                                    if (String.IsNullOrEmpty(item.sInvoice)) { entClearLinePrice.CLP_INVOICE = 0; } else { entClearLinePrice.CLP_INVOICE = Decimal.Parse(item.sInvoice ?? "0"); }                                    
                                    //entClearLinePrice.CLP_MEMO = item.sMemo;
                                    entClearLinePrice.CLP_UNIT_INV = item.sInvUnit;

                                    dalClearLine.UpdateClearLinePrice(entClearLinePrice, context);
                                }

                            }
                            
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            String innerMessage = (ex.InnerException != null)
                              ? ex.InnerException.Message
                              : "";
                            string res = ex.Message;
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("Exception : " + innerMessage + " : " + res);
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

        private string genTripNo(string com_code, EntityCPAIEngine context)
        {
            string ret = "CL";
            com_code = com_code.Substring(0, 2);
            string yy = DateTime.Now.ToString("yyMM", new System.Globalization.CultureInfo("en-US"));

            string TripPrefix = ret + com_code + yy;
            try
            {
                var query = (from v in context.CLEAR_LINE_CRUDE
                             where v.CLC_TRIP_NO.Contains(TripPrefix)
                             orderby v.CLC_TRIP_NO descending
                             select v.CLC_TRIP_NO).ToList();


                if (query != null && query.Count() > 0)
                {
                    string lastTrip = query[0];
                    if (lastTrip != "")
                        lastTrip = lastTrip.Replace(TripPrefix, "");

                    string nextTrip = (int.Parse(lastTrip) + 1).ToString().PadLeft(2,'0');

                    ret = TripPrefix + nextTrip;
                }
                else
                {
                    ret = TripPrefix + "01";
                }
                
            }
            catch (Exception ex) { }

            return ret;
        }
    }
}