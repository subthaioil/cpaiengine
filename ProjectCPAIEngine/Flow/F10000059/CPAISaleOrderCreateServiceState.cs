﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using System.Globalization;
using static ProjectCPAIEngine.Model.SaleOrderModel;
using ProjectCPAIEngine.DAL.DALPCF;

namespace ProjectCPAIEngine.Flow.F10000059
{
    public class CPAISaleOrderCreateServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAISaleOrderCreateServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {

                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string status = etxValue.GetValue(CPAIConstantUtil.Status);
                if (item != null)
                {

                    SaleOrderSalePriceModel dataDetail = JSonConvertUtil.jsonToModel<SaleOrderSalePriceModel>(item);


                    SaleOrderModel temp = new SaleOrderModel();
                    //temp.SALESDOCUMENT_IN = dataDetail.SaleOrder.SALESDOCUMENT_IN;
                    //temp.Flag = dataDetail.SaleOrder.Flag;
                    string Sap_logon = "";
                    string SALESDOCUMENT = "NOT FOUND";
                    string DO_doc = "NOT FOUND";
                    string Sap_status_msg = "";
                    Boolean noSaleDoc = String.IsNullOrEmpty(dataDetail.SaleOrder.SALESDOCUMENT_IN);
                    Boolean noflag = String.IsNullOrEmpty(dataDetail.SaleOrder.Flag);
                    //if (noSaleDoc && noflag) //create
                    if (status == "CREATE")
                    {

                        var cfgOrderHeader = getORDER_HEADER_IN();
                        temp.mORDER_HEADER_IN = new List<BAPISDHD1>();
                        var itemOrderHeader = dataDetail.SaleOrder.mORDER_HEADER_IN.FirstOrDefault();
                        BAPISDHD1 tmpOHeader = new BAPISDHD1();
                        tmpOHeader.DOC_TYPE = itemOrderHeader.DOC_TYPE;
                        tmpOHeader.SALES_ORG = itemOrderHeader.SALES_ORG;
                        tmpOHeader.DISTR_CHAN = itemOrderHeader.DISTR_CHAN;
                        tmpOHeader.DIVISION = cfgOrderHeader.DIVISION;//Global Config
                        tmpOHeader.REQ_DATE_H = itemOrderHeader.REQ_DATE_H;
                        tmpOHeader.DATE_TYPE = cfgOrderHeader.DATE_TYPE;//Global Config
                        tmpOHeader.PURCH_NO_C = itemOrderHeader.PURCH_NO_C;
                        tmpOHeader.PURCH_NO_S = itemOrderHeader.PURCH_NO_S;
                        tmpOHeader.CURRENCY = itemOrderHeader.CURRENCY;
                        temp.mORDER_HEADER_IN.Add(tmpOHeader);

                        //Conditions use for SWAP,Throughput only
                        var cfgOConditions = getORDER_CONDITIONS_IN();
                        temp.mORDER_CONDITIONS_IN = new List<BAPICOND>();
                        var itemOrderCondi = dataDetail.SaleOrder.mORDER_CONDITIONS_IN.FirstOrDefault();
                        BAPICOND tmpOCondi = new BAPICOND();
                        tmpOCondi.COND_TYPE = cfgOConditions.COND_TYPE;//Global Config
                        tmpOCondi.COND_VALUE = itemOrderCondi.COND_VALUE;
                        tmpOCondi.CURRENCY = itemOrderCondi.CURRENCY;
                        tmpOCondi.COND_P_UNT = cfgOConditions.COND_P_UNT;//Global Config
                        temp.mORDER_CONDITIONS_IN.Add(tmpOCondi);



                        var cfgOItems = getORDER_ITEMS_IN();
                        temp.mORDER_ITEMS_IN = new List<BAPISDITM>();
                        var itemOrderItems = dataDetail.SaleOrder.mORDER_ITEMS_IN.FirstOrDefault();
                        BAPISDITM tmpOItems = new BAPISDITM();
                        tmpOItems.MATERIAL = itemOrderItems.MATERIAL;
                        tmpOItems.PLANT = itemOrderItems.PLANT;
                        if (itemOrderHeader.DOC_TYPE == "Z1V1")
                        {
                            tmpOItems.ITEM_CATEG = cfgOItems.ITEM_CATEG_Z1V1;//Global Config
                        }
                        else
                        {
                            tmpOItems.ITEM_CATEG = cfgOItems.ITEM_CATEG;//Global Config
                        }
                        tmpOItems.SALES_UNIT = itemOrderItems.SALES_UNIT;
                        temp.mORDER_ITEMS_IN.Add(tmpOItems);



                        var cfgOPartners = getORDER_PARTNERS();
                        temp.mORDER_PARTNERS = new List<BAPIPARNR>();
                        var itemOrderPartners = dataDetail.SaleOrder.mORDER_PARTNERS.FirstOrDefault();
                        BAPIPARNR[] tmpOPartners = new BAPIPARNR[4];
                        tmpOPartners[0] = new BAPIPARNR();
                        tmpOPartners[0].PARTN_ROLE_SP = cfgOPartners.PARTN_ROLE_SP;//Global Config
                        tmpOPartners[0].PARTN_NUMB = itemOrderPartners.PARTN_NUMB;
                        tmpOPartners[0].ITM_NUMBER = cfgOPartners.ITM_NUMBER;//Global Config

                        tmpOPartners[1] = new BAPIPARNR();
                        tmpOPartners[1].PARTN_ROLE_BP = cfgOPartners.PARTN_ROLE_BP;//Global Config
                        tmpOPartners[1].PARTN_NUMB = itemOrderPartners.PARTN_NUMB;
                        tmpOPartners[1].ITM_NUMBER = cfgOPartners.ITM_NUMBER;//Global Config

                        tmpOPartners[2] = new BAPIPARNR();
                        tmpOPartners[2].PARTN_ROLE_PY = cfgOPartners.PARTN_ROLE_PY;//Global Config
                        tmpOPartners[2].PARTN_NUMB = itemOrderPartners.PARTN_NUMB;
                        tmpOPartners[2].ITM_NUMBER = cfgOPartners.ITM_NUMBER;//Global Config

                        tmpOPartners[3] = new BAPIPARNR();
                        tmpOPartners[3].PARTN_ROLE_SH = cfgOPartners.PARTN_ROLE_SH;//Global Config
                        tmpOPartners[3].PARTN_NUMB = itemOrderPartners.PARTN_NUMB;
                        tmpOPartners[3].ITM_NUMBER = cfgOPartners.ITM_NUMBER;//Global Config

                        //temp.mORDER_PARTNERS.Add(tmpOPartners);
                        int i;
                        for (i = 0; i < 4; i++)
                        {
                            temp.mORDER_PARTNERS.Add(tmpOPartners[i]);
                        }



                        var cfgOScheduls = getORDER_SCHEDULES_IN();
                        temp.mORDER_SCHEDULES_IN = new List<BAPISCHDL>();
                        var itemOrderSchedules = dataDetail.SaleOrder.mORDER_SCHEDULES_IN.FirstOrDefault();
                        BAPISCHDL tmpOSchedules = new BAPISCHDL();
                        tmpOSchedules.REQ_DATE = itemOrderSchedules.REQ_DATE;
                        tmpOSchedules.DATE_TYPE = cfgOScheduls.DATE_TYPE;//Global Config
                        tmpOSchedules.REQ_QTY = Math.Round(itemOrderSchedules.REQ_QTY, 3, MidpointRounding.AwayFromZero);
                        temp.mORDER_SCHEDULES_IN.Add(tmpOSchedules);


                        ConfigManagement configManagement = new ConfigManagement();
                        String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        String content = js.Serialize(temp);
                        var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        var jsonConfig = javaScriptSerializer.Deserialize<ConfigServiceModel>(config); //convert jason

                        SaleOrderCreateServiceConnectorImpl SalesOrderCreateService = new SaleOrderCreateServiceConnectorImpl();
                        String conf = js.Serialize(jsonConfig.SaleOrderCreate);
                        DownstreamResponse<string> res = SalesOrderCreateService.connect(conf, content);


                        ////etxValue
                        Sap_logon = res.resultCode;//res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|") + 1, res.responseData.ToString().IndexOf("#") - res.responseData.ToString().IndexOf("|") - 1);
                        Sap_status_msg = res.resultDesc; //res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("#") + 1, res.responseData.ToString().IndexOf("$") - res.responseData.ToString().IndexOf("#") - 1);
                        if (Sap_logon == "1")
                        {
                            if (res.responseData.ToString().IndexOf("$") > 0)
                                SALESDOCUMENT = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("$") + 1, 10);

                            if (res.responseData.ToString().IndexOf("delivery ") > 0)
                                DO_doc = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("delivery ") + 9, res.responseData.ToString().IndexOf(" created") - res.responseData.ToString().IndexOf("delivery ") - 9);
                            else if (res.responseData.ToString().IndexOf("|") > 0)
                                DO_doc = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|") + 1);

                            ExtendValue extRowsSalesDoc = new ExtendValue
                            { value = SALESDOCUMENT, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.SALESDOCUMENT, extRowsSalesDoc);

                            ExtendValue extRowsDeliveryDoc = new ExtendValue
                            { value = DO_doc, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.DO_doc, extRowsDeliveryDoc);

                            ExtendValue extRowsSap_status_msg = new ExtendValue
                            { value = Sap_status_msg, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_status_msg, extRowsSap_status_msg);

                            ExtendValue extRowsSap_logon = new ExtendValue
                            { value = Sap_logon, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_logon, extRowsSap_logon);

                            //CLEAR_LINE_CRUDE dalClearLine = new CLEAR_LINE_CRUDE();
                            //dalClearLine.CLC_TRIP_NO = etxValue.GetValue(CPAIConstantUtil.TripNo);
                            //dalClearLine.CLC_CRUDE_TYPE_ID = dataDetail.SalePrice[0].MATNR;
                            //dalClearLine.CLC_CUST_NUM = dataDetail.SalePrice[0].KUNNR;
                            //dalClearLine.CLC_FI_DOC = SALESDOCUMENT;
                            //dalClearLine.CLC_DO_NO = DO_doc;

                            CLEAR_LINE_DAL dalClearLine = new CLEAR_LINE_DAL();
                            CLEAR_LINE_CRUDE entClearLineCrude = new CLEAR_LINE_CRUDE();

                            using (var context = new EntityCPAIEngine())
                            {
                                string sTripNo = etxValue.GetValue(CPAIConstantUtil.TripNo).ToUpper();
                                string sCrudeType = dataDetail.SalePrice[0].MATNR.ToUpper();
                                string sCustNum = dataDetail.SalePrice[0].KUNNR.ToUpper();
                                var _qry = context.CLEAR_LINE_CRUDE.Where(z => z.CLC_TRIP_NO.ToUpper().Equals(sTripNo)
                                                                    && z.CLC_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeType)
                                                                    && z.CLC_CUST_NUM.ToUpper().Equals(sCustNum)
                                                                    ).ToList();
                                if (_qry.Count > 0)
                                {
                                    entClearLineCrude = (CLEAR_LINE_CRUDE)_qry[0];
                                    entClearLineCrude.CLC_SALEORDER = SALESDOCUMENT;
                                    entClearLineCrude.CLC_DO_NO = DO_doc;
                                    entClearLineCrude.CLC_LAST_MODIFY_DATE = DateTime.Now;
                                    entClearLineCrude.CLC_LAST_MODIFY_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dalClearLine.UpdateClearLineCrude(entClearLineCrude);
                                }
                            }
                        }

                        if (Sap_logon != "1")
                        {
                            ExtendValue extRowsSap_StatusCode = new ExtendValue
                            { value = res.resultCode, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Status, extRowsSap_StatusCode);

                            ExtendValue extRowsSap_StatusDesc = new ExtendValue
                            { value = res.resultDesc, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.DataDetail, extRowsSap_StatusDesc);

                            stateModel.BusinessModel.currentCode = currentCode;
                            stateModel.BusinessModel.currentDesc = Sap_status_msg;
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }
                        else
                        {
                            stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }

                    }
                    else
                    {
                        stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        respCodeManagement.setCurrentCodeMapping(stateModel);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAISaleOrderCreateServiceState # :: Exception >>> " + ex);
                log.Error("CPAISaleOrderCreateServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB

            }
        }


        public jORDER_HEADER_IN getORDER_HEADER_IN()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALEORDER");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSaleOrder dataList = (GlobalConfigSaleOrder)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSaleOrder));
            return dataList.ORDER_HEADER_IN;
        }

        public jORDER_CONDITIONS_IN getORDER_CONDITIONS_IN()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALEORDER");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSaleOrder dataList = (GlobalConfigSaleOrder)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSaleOrder));
            return dataList.ORDER_CONDITIONS_IN;
        }

        public jORDER_ITEMS_IN getORDER_ITEMS_IN()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALEORDER");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSaleOrder dataList = (GlobalConfigSaleOrder)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSaleOrder));
            return dataList.ORDER_ITEMS_IN;
        }

        public jORDER_PARTNERS getORDER_PARTNERS()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALEORDER");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSaleOrder dataList = (GlobalConfigSaleOrder)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSaleOrder));
            return dataList.ORDER_PARTNERS;
        }

        public jORDER_SCHEDULES_IN getORDER_SCHEDULES_IN()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALEORDER");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSaleOrder dataList = (GlobalConfigSaleOrder)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSaleOrder));
            return dataList.ORDER_SCHEDULES_IN;
        }


        [Serializable]
        public class GlobalConfigSaleOrder
        {
            public jORDER_HEADER_IN ORDER_HEADER_IN { get; set; }
            public jORDER_CONDITIONS_IN ORDER_CONDITIONS_IN { get; set; }
            public jORDER_ITEMS_IN ORDER_ITEMS_IN { get; set; }
            public jORDER_PARTNERS ORDER_PARTNERS { get; set; }
            public jORDER_SCHEDULES_IN ORDER_SCHEDULES_IN { get; set; }

        }

        public class jORDER_HEADER_IN
        {
            public string DIVISION { get; set; }
            public string DATE_TYPE { get; set; }
        }

        public class jORDER_CONDITIONS_IN
        {
            public string COND_TYPE { get; set; }
            public decimal COND_P_UNT { get; set; }

        }

        public class jORDER_ITEMS_IN
        {
            public string ITEM_CATEG { get; set; }
            public string ITEM_CATEG_Z1V1 { get; set; }
            public string REASON_REJ { get; set; }

        }

        public class jORDER_PARTNERS
        {
            public string PARTN_ROLE_SP { get; set; }
            public string PARTN_ROLE_BP { get; set; }
            public string PARTN_ROLE_PY { get; set; }
            public string PARTN_ROLE_SH { get; set; }
            public string ITM_NUMBER { get; set; }
        }

        public class jORDER_SCHEDULES_IN
        {
            public string DATE_TYPE { get; set; }
        }

        public class ConfigModel
        {
            public string sap_url { get; set; }
            public string sap_user { get; set; }
            public string sap_pass { get; set; }
            public string connect_time_out { get; set; }
        }

        public class ConfigServiceModel
        {
            public ConfigModel SaleOrderCreate { get; set; }
            public ConfigModel SaleOrderChange { get; set; }
        }
    }
}
