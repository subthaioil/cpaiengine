﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using System.Globalization;
using static ProjectCPAIEngine.Model.SaleOrderModel;
using ProjectCPAIEngine.DAL.DALPCF;

namespace ProjectCPAIEngine.Flow.F10000059
{
    public class CPAISaleOrderDeleteServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAISaleOrderDeleteServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string status = etxValue.GetValue(CPAIConstantUtil.Status);
                if (item != null)
                {

                    SaleOrderSalePriceModel dataDetail = JSonConvertUtil.jsonToModel<SaleOrderSalePriceModel>(item);

                    SaleOrderModel temp = new SaleOrderModel();
                    temp.SALESDOCUMENT_IN = dataDetail.SaleOrder.SALESDOCUMENT_IN;
                    //temp.Flag = dataDetail.SaleOrder.Flag;

                    Boolean noSaleDoc = String.IsNullOrEmpty(temp.SALESDOCUMENT_IN);
                    Boolean noflag = String.IsNullOrEmpty(dataDetail.SaleOrder.Flag);
                    //if (!noSaleDoc && !noflag) //Del
                    if (status == "CANCEL")
                    {

                        temp.mORDER_HEADER_IN = new List<BAPISDHD1>();
                        var itemOrderHeader = dataDetail.SaleOrder.mORDER_HEADER_IN.FirstOrDefault();
                        BAPISDHD1 tmpOHeader = new BAPISDHD1();
                        tmpOHeader.DOC_TYPE = itemOrderHeader.DOC_TYPE;
                        tmpOHeader.REQ_DATE_H = itemOrderHeader.REQ_DATE_H;
                        temp.mORDER_HEADER_IN.Add(tmpOHeader);

                        var cfgOItems = getORDER_ITEMS_IN();
                        temp.mORDER_ITEMS_IN = new List<BAPISDITM>();
                        var itemOrderItems = dataDetail.SaleOrder.mORDER_ITEMS_IN.FirstOrDefault();
                        BAPISDITM tmpOItems = new BAPISDITM();
                        tmpOItems.ITM_NUMBER = itemOrderItems.ITM_NUMBER;
                        tmpOItems.MATERIAL = itemOrderItems.MATERIAL;
                        tmpOItems.REASON_REJ = cfgOItems.REASON_REJ; //Global config
                        tmpOItems.PLANT = itemOrderItems.PLANT;
                        if (itemOrderHeader.DOC_TYPE == "Z1V1")
                        {
                            tmpOItems.ITEM_CATEG = cfgOItems.ITEM_CATEG_Z1V1;//Global Config
                        }
                        else
                        {
                            tmpOItems.ITEM_CATEG = cfgOItems.ITEM_CATEG;//Global Config
                        }
                        tmpOItems.SALES_UNIT = itemOrderItems.SALES_UNIT;
                        temp.mORDER_ITEMS_IN.Add(tmpOItems);


                        temp.mORDER_SCHEDULES_IN = new List<BAPISCHDL>();
                        var itemOrderSchedules = dataDetail.SaleOrder.mORDER_SCHEDULES_IN.FirstOrDefault();
                        BAPISCHDL tmpOSchedules = new BAPISCHDL();
                        tmpOSchedules.ITM_NUMBER = itemOrderSchedules.ITM_NUMBER;
                        tmpOSchedules.SCHED_LINE = itemOrderSchedules.SCHED_LINE;
                        tmpOSchedules.REQ_DATE = itemOrderSchedules.REQ_DATE;
                        tmpOSchedules.REQ_QTY = Math.Round(itemOrderSchedules.REQ_QTY, 3, MidpointRounding.AwayFromZero);
                        temp.mORDER_SCHEDULES_IN.Add(tmpOSchedules);


                        ConfigManagement configManagement = new ConfigManagement();
                        String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        String content = js.Serialize(temp);
                        var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        var jsonConfig = javaScriptSerializer.Deserialize<ConfigServiceModel>(config); //convert jason

                        SaleOrderDeleteServiceConnectorImpl SalesOrderDelService = new SaleOrderDeleteServiceConnectorImpl();
                        String conDel = js.Serialize(jsonConfig.SaleOrderChange);
                        DownstreamResponse<string> res = SalesOrderDelService.connect(conDel, content);


                        ////etxValue
                        string Sap_logon = res.resultCode;
                        string Sap_status_msg = res.resultDesc;
                        string SALESDOCUMENT = temp.SALESDOCUMENT_IN;
                        if (Sap_logon == "1")
                        {
           
                            ExtendValue extRowsSap_status_msg = new ExtendValue
                            { value = Sap_status_msg, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_status_msg, extRowsSap_status_msg);

                            ExtendValue extRowsSap_logon = new ExtendValue
                            { value = Sap_logon, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_logon, extRowsSap_logon);

                            CLEAR_LINE_DAL dalClearLine = new CLEAR_LINE_DAL();
                            CLEAR_LINE_CRUDE entClearLineCrude = new CLEAR_LINE_CRUDE();

                            using (var context = new EntityCPAIEngine())
                            {
                                string sTripNo = etxValue.GetValue(CPAIConstantUtil.TripNo).ToUpper();
                                var _qry = context.CLEAR_LINE_CRUDE.Where(z => z.CLC_TRIP_NO.ToUpper().Equals(sTripNo)
                                                                    && z.CLC_SALEORDER.ToUpper().Equals(SALESDOCUMENT)
                                                                    ).ToList();
                                if (_qry.Count > 0)
                                {
                                    entClearLineCrude = (CLEAR_LINE_CRUDE)_qry[0];
                                    entClearLineCrude.CLC_SALEORDER = "";
                                    entClearLineCrude.CLC_DO_NO = "";
                                    entClearLineCrude.CLC_LAST_MODIFY_DATE = DateTime.Now;
                                    entClearLineCrude.CLC_LAST_MODIFY_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dalClearLine.UpdateClearLineCrude(entClearLineCrude);
                                }
                            }

                            stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }
                        else
                        {
                            stateModel.BusinessModel.currentCode = currentCode;
                            stateModel.BusinessModel.currentDesc = Sap_status_msg;
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }
                        if (Sap_logon != "1")
                        {
                            ExtendValue extRowsSap_StatusCode = new ExtendValue
                            { value = res.resultCode, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Status, extRowsSap_StatusCode);

                            ExtendValue extRowsSap_StatusDesc = new ExtendValue
                            { value = res.resultDesc, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.DataDetail, extRowsSap_StatusDesc);

                            stateModel.BusinessModel.currentCode = currentCode;
                            stateModel.BusinessModel.currentDesc = Sap_status_msg;
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }
                        else
                        {
                            stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }
                    }
                    else
                    {
                        stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0]; ;
                        respCodeManagement.setCurrentCodeMapping(stateModel);
                    }

                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAISaleOrderDeleteServiceState # :: Exception >>> " + ex);
                log.Error("CPAISaleOrderDeleteServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }


        public ORDER_ITEMS_IN getORDER_ITEMS_IN()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALEORDER");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSaleOrder dataList = (GlobalConfigSaleOrder)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSaleOrder));
            return dataList.ORDER_ITEMS_IN;
        }




        [Serializable]
        public class GlobalConfigSaleOrder
        {
            public ORDER_ITEMS_IN ORDER_ITEMS_IN { get; set; }

        }

        public class ORDER_ITEMS_IN
        {
            public string ITEM_CATEG { get; set; }
            public string ITEM_CATEG_Z1V1 { get; set; }
            public string REASON_REJ { get; set; }

        }

        public class ConfigModel
        {
            public string sap_url { get; set; }
            public string sap_user { get; set; }
            public string sap_pass { get; set; }
            public string connect_time_out { get; set; }
        }

        public class ConfigServiceModel
        {
            public ConfigModel SaleOrderCreate { get; set; }
            public ConfigModel SaleOrderChange { get; set; }
        }
    }
}
