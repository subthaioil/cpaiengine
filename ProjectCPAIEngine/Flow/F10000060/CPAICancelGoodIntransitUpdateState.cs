﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using ProjectCPAIEngine.DAL.DALPCF;

namespace ProjectCPAIEngine.Flow.F10000060
{
    public class CPAICancelGoodIntransitUpdateState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICancelGoodIntransitUpdateState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                CreatePOGoodIntransit tempData = new CreatePOGoodIntransit();
                tempData = JSonConvertUtil.jsonToModel<CreatePOGoodIntransit>(item);

                UpdateResult(tempData);

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);



                //if (stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.Status).ToLower() == "change")
                //{
                //    ConfigManagement configManagement = new ConfigManagement();
                //    String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
                //    String content = stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

                //    PurchasingChangeServiceConnectorImpl POService = new PurchasingChangeServiceConnectorImpl();
                //    //PurchasingChangeModel model = CreateModel(poViewModel, false);
                //    DownstreamResponse<string> res = POService.connect(config, content);

                //}
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICancelGoodIntransitUpdateState # :: Exception >>> " + ex);
                log.Error("CPAICancelGoodIntransitUpdateState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private void UpdateResult(CreatePOGoodIntransit tempData)
        {
            try
            {
                CRUDE_IMPORT_PLAN_DAL dataDAL = new CRUDE_IMPORT_PLAN_DAL();

                using (var context = new EntityCPAIEngine())
                {
                    var query = tempData.ItemList.dDetail_Material.Where(p => p.RET_STS != null && p.RET_STS.Equals("S")).ToList();
                    foreach (var itmMat in query)
                    {
                        dataDAL.UpdateCancelGITResult(itmMat.PMA_TRIP_NO, itmMat.PMA_ITEM_NO, itmMat.PMA_GIT_NO, context);
                    }

                }


            }
            catch (Exception ex) { }
        }


    }

}
