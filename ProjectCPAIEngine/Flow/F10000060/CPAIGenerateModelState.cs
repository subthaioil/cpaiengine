﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000060
{
    public class CPAIGenerateModelState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGenerateModelState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                ConfigManagement configManagement = new ConfigManagement();
                String content = stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string fixtripno = etxValue.GetValue("trip_no");
                string fixitemno = etxValue.GetValue("item_no");

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (item != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    CreatePOGoodIntransit tempData = new CreatePOGoodIntransit();
                    CrudeApproveFormViewModel_Detail crudeItem = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(item);
                    tempData.ItemList = new CrudeApproveFormViewModel_Detail();
                    tempData.ItemList = crudeItem;

                    if (tempData.ItemList != null)
                    {
                        tempData.goodIntransitCancel = new List<GoodIntransitCancel>();
                        //Crate data for service Good Intran
                        tempData.goodIntransitCancel = GoodIntransitItem(tempData, fixtripno, fixitemno);

                        string newXml = js.Serialize(tempData);
                        etxValue.SetValue(CPAIConstantUtil.DataDetailInput, newXml);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIGenerateModelState # :: Exception >>> " + ex);
                log.Error("CPAIGenerateModelState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public List<GoodIntransitCancel> GoodIntransitItem(CreatePOGoodIntransit temp, string fixTripNo, string fixItemNo)
        {
            var qHead = temp.ItemList.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixItemNo != "")
                qHead = temp.ItemList.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixTripNo)).ToList();

            if (qHead == null)
                return null;

            if (qHead.Count == 0)
                return null;

            string UserName = Const.User.Name;

            List<GoodIntransitCancel> itemList = new List<GoodIntransitCancel>();
            foreach (var itmHead in qHead)
            {
                string tripno = itmHead.PHE_TRIP_NO;

                var qMaterial = temp.ItemList.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && !string.IsNullOrEmpty(p.PMA_GIT_NO) && !p.PMA_GIT_NO.Equals("0")).ToList();
                if (fixItemNo != "")
                    qMaterial = temp.ItemList.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && p.PMA_ITEM_NO.Equals(fixItemNo) && !string.IsNullOrEmpty(p.PMA_GIT_NO) && !p.PMA_GIT_NO.Equals("0")).ToList();

                foreach (var itmMat in qMaterial)
                {

                    string sBL_Year = "";
                    string sBL_Date = "";
                    if (!string.IsNullOrEmpty(itmMat.PMA_BL_DATE))
                    {
                        DateTime dBL_Date = DateTime.ParseExact(itmMat.PMA_BL_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        sBL_Date = dBL_Date.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

                        sBL_Year = dBL_Date.ToString("yyyy", new System.Globalization.CultureInfo("en-US"));
                    }
                         
                    GoodIntransitCancel i = new GoodIntransitCancel();
                    i.tripNo = itmMat.PMA_TRIP_NO;
                    i.item = itmMat.PMA_ITEM_NO;
                    i.intansitCancel = new IntansitCancelModel();
                    i.intansitCancel.GM_ITEM_04 = new BAPI2017_GM_ITEM_04();
                    i.intansitCancel.GM_HEAD_RET = new BAPI2017_GM_HEAD_RET();
                    i.intansitCancel.more_detai = new MORE_DETAI();
                    i.intansitCancel.RETURN = new List<BAPIRET2>();

                    BAPI2017_GM_ITEM_04 gm_item_04 = new BAPI2017_GM_ITEM_04();
                    gm_item_04.MATDOC_ITEM = "0010";
                    i.intansitCancel.GM_ITEM_04 = gm_item_04;

                    MORE_DETAI more = new MORE_DETAI();
                    more.MATERIAL_DOCUMENT = itmMat.PMA_GIT_NO;// "5000002996";
                    more.DATE = sBL_Date; // "2017-05-02";
                    more.YEAR = sBL_Year; // "2017";
                    more.USER_NAME = UserName;
                    i.intansitCancel.more_detai = more;

                    itemList.Add(i);
                }

            }


            return itemList;
        }
    }

}
