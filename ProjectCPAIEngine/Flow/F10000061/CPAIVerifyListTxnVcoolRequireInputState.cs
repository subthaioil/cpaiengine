﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000061
{
    public class CPAIVerifyListTxnVcoolRequireInputState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyListTxnVcoolRequireInputState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
            ExtendValue extStatus = new ExtendValue();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                Boolean datePassed = true;
                if (!string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.FromDate)))
                {
                    // check date
                    try
                    {
                        DateTime dt = DateTime.ParseExact(etxValue.GetValue(CPAIConstantUtil.FromDate), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        datePassed = false;
                    }
                }
                if (!string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.ToDate)))
                {
                    // check date
                    try
                    {
                        DateTime dt = DateTime.ParseExact(etxValue.GetValue(CPAIConstantUtil.ToDate), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        datePassed = false;
                    }
                }
                if (!string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.MobileFlag)))
                {
                    // check mobile flag
                    try
                    {
                        extStatus.value = CPAIConstantUtil.NO_FLAG;
                        etxValue.SetValue(CPAIConstantUtil.MobileFlag, extStatus);
                    }
                    catch (Exception ex)
                    {
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        datePassed = false;
                    }
                }

                if (datePassed)
                {
                    if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Channel)))
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_CHANNEL_RESP_CODE;
                    }
                    else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.RowsPerPage)))
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_ROWS_PER_PAGE_RESP_CODE;
                    }
                    else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.PageNumber)))
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_PAGE_NUMBER_RESP_CODE;
                    }
                    else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    }
                    else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_SYSTEM_RESP_CODE;
                    }
                    else
                    {
                        /*
                        //set user group
                        extStatus = new ExtendValue();
                        UserGroupDAL service = new UserGroupDAL();
                        string user = etxValue.GetValue(CPAIConstantUtil.User);
                        string system = etxValue.GetValue(CPAIConstantUtil.System);
                        CPAI_USER_GROUP r = service.findByUserAndSystem(user, system);
                        if (r != null && r.USG_USER_GROUP != null)
                        {
                            extStatus.value = r.USG_USER_GROUP;
                            etxValue.SetValue(CPAIConstantUtil.UserGroup, extStatus);
                            //
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                        }
                        */

                        //set user group
                        extStatus = new ExtendValue();
                        UserGroupDAL service = new UserGroupDAL();
                        CPAI_USER_GROUP r = service.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                        //set user group deligate by poo 25072017
                        CPAI_USER_GROUP dlg = service.findByUserAndSystemDelegate(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                        if (r != null || dlg != null)
                        {
                            if (r != null && r.USG_USER_GROUP != null)
                            {
                                extStatus.value = r.USG_USER_GROUP.ToUpper();
                                etxValue.SetValue(CPAIConstantUtil.UserGroup, extStatus);
                            }
                            if (dlg != null && dlg.USG_USER_GROUP != null)
                            {
                                extStatus = new ExtendValue();
                                extStatus.value = dlg.USG_USER_GROUP.ToUpper();
                                etxValue.Add(CPAIConstantUtil.UserGroupDelegate, extStatus);
                            }
                            //

                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                        }
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_INPUT_DATE_RESP_CODE;
                }

                //get ip address
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyListTxnVcoolRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIVerifyListTxnVcoolRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyListTxnVcoolRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}