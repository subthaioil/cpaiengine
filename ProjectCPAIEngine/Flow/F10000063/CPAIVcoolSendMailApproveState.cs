﻿using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.downstream;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using DSMail.model;
using DSMail.service;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000063
{
    public class CPAIVcoolSendMailApproveState : BasicBean, StateFlowAction
    {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVcoolSendMailApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                string charterFor = etxValue.GetValue(CPAIConstantUtil.CharterFor);
                ActionMessageDAL amsMan = new ActionMessageDAL();

                List<CPAI_ACTION_MESSAGE> results = new List<CPAI_ACTION_MESSAGE>();
                string checkSendMail = etxValue.GetValue(CPAIConstantUtil.CheckSendMail_M05);
                if (checkSendMail == "T")
                {
                    if (action == "APPROVE_3")
                    {
                        List<CPAI_ACTION_MESSAGE> newResults = new List<CPAI_ACTION_MESSAGE>();
                        newResults = amsMan.findUserGroupMail_M05("M_05", system, type, stateModel.EngineModel.functionId, prevStatus, checkSendMail); //Get Email Detail. 
                        if (newResults.Count == 0)
                        {
                            currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                        }
                        else
                        {
                            for (int i = 0; i < newResults.Count; i++)
                            {
                                findUserAndSendMail(stateModel, newResults[i], system, action);
                            }
                        }
                    }
                }
                results = amsMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor); //Get Email Detail.

                if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else
                {
                    UserGroupDAL ugDAL = new UserGroupDAL();
                    List<CPAI_USER_GROUP> ug = ugDAL.getUserList(etxValue.GetValue(CPAIConstantUtil.User), ConstantPrm.SYSTEM.VCOOL);
                    if (action == "REJECT_3")
                    {
                        if (ug.Where(x => x.USG_USER_GROUP.ToUpper().Contains("SCVP")).Any())
                        {
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "SCSC_SH").ToList();
                        }
                        else if(ug.Where(x => x.USG_USER_GROUP.ToUpper().Contains("CMVP")).Any())
                        {
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "CMCS").ToList();
                        }
                        else if (ug.Where(x => x.USG_USER_GROUP.ToUpper().Contains("TNVP")).Any())
                        {
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "TNPB").ToList();
                        }
                    }
                    for (int i = 0; i < results.Count; i++)
                    {
                        findUserAndSendMail(stateModel, results[i], system, action);
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVcoolSendMailApproveState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIVcoolSendMailApproveState # :: Exception >>> " + ex);
                log.Error("CPAIVcoolSendMailApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system, string action)
        {
            List<USERS> userResult = new List<USERS>();
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER)) //ไม่ได้ส่งให้ใครเป็นพิเศษ
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                userResult = userGMan.findUserMailByGroupSystem(userGroup, system);

                if (userResult.Count == 0)
                {
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }

        }

        public void prepareSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail)
        {

            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0)
            {
                log.Info("login user not found");
            }

            if (subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            //
            if (subject.Contains("#user"))
            {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user"))
            {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user"))
            {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user"))
            {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            //get create by
            //string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            //List<USERS> create_by = userMan.findByLogin(tem);
            //string create_by_v = "";
            //if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            //{
            //    //string last_name = create_by[0].USR_LAST_NAME_EN != null ? create_by[0].USR_LAST_NAME_EN  : "";
            //    //create_by_v = string.Format("{0} {1}", create_by[0].USR_FIRST_NAME_EN, last_name);
            //    create_by_v = create_by[0].USR_FIRST_NAME_EN;
            //}
            //if (body.Contains("#create_by"))
            //{
            //    body = body.Replace("#create_by", create_by_v);
            //}

            //vcool
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000063))
            {
                subject = mms.getSubjectF10000063(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000063(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000064))
            {
                subject = mms.getSubjectF10000064(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000064(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000065))
            {
                subject = mms.getSubjectF10000065(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000065(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            //
            if (body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = body;
                foreach (USERS um in userMail)
                {
                    temp_body = body;
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    string out_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, subject, out_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body)
        {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            currentCode = downResp.getResultCode();
        }
    }
}