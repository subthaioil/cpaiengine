﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using Excel;
using OfficeOpenXml;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALVCOOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000063
{
    public class CPAIVcoolUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVcoolUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string currentStatus = etxValue.GetValue(CPAIConstantUtil.Status); // current status
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);

                if (item != null)
                {
                    VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string action = etxValue.GetValue(CPAIConstantUtil.Action);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                    if (prevStatus == null)
                    {
                        prevStatus = "NEW";
                    }
                    etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    //update reason only in data_detail 
                    //string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? Uri.UnescapeDataString(etxValue.GetValue(CPAIConstantUtil.Note)) : "-";

                    UserGroupDAL userDal = new UserGroupDAL();
                    CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (r != null && r.USG_USER_GROUP != null)
                    {
                        userGroup = r.USG_USER_GROUP;
                    }
                    CPAI_USER_GROUP dlg = userDal.findByUserAndSystemDelegate(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (dlg != null && dlg.USG_USER_GROUP != null)
                    {
                        userGroup = dlg.USG_USER_GROUP;
                    }

                    bool isRunDB = false;
                    if ((currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT)) || currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1))
                    {
                        //DRAFT && SUBMIT ==> DELETE , INSERT
                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                    }
                    else if (!currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
                    {
                        //OTHER ==> UPDATE
                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, currentStatus, nextStatus, etxValue, dataDetail);
                        if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL) && isRunDB)
                        {
                            isRunDB = InsertDB_ETA_Date_History(stateModel.EngineModel.ftxTransId, userGroup, etxValue, dataDetail);
                        }
                        #region Set Reject flag
                        //if current action is reject clear reject flag in table data history
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                        {
                            dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                        }
                        #endregion
                    }
                    else
                    {
                        isRunDB = UpdateCancelDB(stateModel.EngineModel.ftxTransId, currentStatus, nextStatus, etxValue, dataDetail);
                    }
                    #endregion

                    #region INSERT JSON FOR INDEX
                    if (isRunDB)
                    {
                        //set data detail (json when action = approve_1 or DRAFT only)
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) || currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT))
                        {
                            string date_purchase = dataDetail.crude_info.purchase_date != null ? dataDetail.crude_info.purchase_date : "";
                            string products = dataDetail.crude_info.crude_name != null ? dataDetail.crude_info.crude_name : "";
                            string origin = dataDetail.crude_info.origin != null ? dataDetail.crude_info.origin : "";
                            string tpc_plan_month = dataDetail.crude_info.tpc_plan_month != null ? dataDetail.crude_info.tpc_plan_month : "";
                            string tpc_plan_year = dataDetail.crude_info.tpc_plan_year != null ? dataDetail.crude_info.tpc_plan_year : "";
                            string loading_date_from = dataDetail.crude_info.loading_date_from != null ? dataDetail.crude_info.loading_date_from : "";
                            string loading_date_to = dataDetail.crude_info.loading_date_from != null ? dataDetail.crude_info.loading_date_from : "";
                            string incoterm = dataDetail.crude_info.incoterm != null ? dataDetail.crude_info.incoterm : "";
                            string formula_p = dataDetail.crude_info.formula_price != null ? dataDetail.crude_info.formula_price : "";
                            string discharging_from = dataDetail.crude_info.discharging_date_from != null ? dataDetail.crude_info.discharging_date_from : "";
                            string discharging_to = dataDetail.crude_info.discharging_date_to != null ? dataDetail.crude_info.discharging_date_to : "";
                            string supplier = dataDetail.crude_info.supplier != null ? dataDetail.crude_info.supplier : "";
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                            //set data
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = products, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Origin, new ExtendValue { value = origin, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_month, new ExtendValue { value = tpc_plan_month, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_year, new ExtendValue { value = tpc_plan_year, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_from, new ExtendValue { value = loading_date_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_to, new ExtendValue { value = loading_date_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.incoterm, new ExtendValue { value = incoterm, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Formula_p, new ExtendValue { value = formula_p, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_from, new ExtendValue { value = discharging_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_to, new ExtendValue { value = discharging_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = supplier, encryptFlag = "N" });
                        } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_PRICE) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_SUMMARY))
                        {
                            if (dataDetail != null && dataDetail.crude_info != null)
                            {
                                string supplier = dataDetail.crude_info.supplier != null ? dataDetail.crude_info.supplier : "";
                                etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = supplier, encryptFlag = "N" });
                            }
                        } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE))
                        {
                            if (dataDetail != null && dataDetail.crude_info != null)
                            {
                                string discharging_from = dataDetail.crude_info.discharging_date_from != null ? dataDetail.crude_info.discharging_date_from : "";
                                string discharging_to = dataDetail.crude_info.discharging_date_to != null ? dataDetail.crude_info.discharging_date_to : "";

                                etxValue.SetValue(CPAIConstantUtil.Discharging_from, new ExtendValue { value = discharging_from, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Discharging_to, new ExtendValue { value = discharging_to, encryptFlag = "N" });
                            }
                        }

                        if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL))
                        {
                            etxValue.SetValue(CPAIConstantUtil.Update_flag, new ExtendValue { value = "Y", encryptFlag = "N" });
                        } else
                        {
                            etxValue.SetValue(CPAIConstantUtil.Update_flag, new ExtendValue { value = "N", encryptFlag = "N" });
                        }
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVcoolUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAIVcoolUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail)
        {
            log.Info("# Start State CPAIVcoolUpdateDataState >> InsertDB #  ");
            string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
            string currentStatus = etxValue.GetValue(CPAIConstantUtil.Status);

            #region Insert Data into DB            
            bool isSuccess = false;
            DateTime dtNow;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dtNow = DateTime.Now;
                            VCO_DATA_DAL dataDal = new VCO_DATA_DAL();
                            dataDal.Delete(TransID, context);
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            #region VCO_DATA
                            VCO_DATA dataVCO = new VCO_DATA();
                            dataVCO.VCDA_ROW_ID = TransID;
                            dataVCO.VCDA_PURCHASE_NO = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                            dataVCO.VCDA_FK_CDP_DATA = etxValue.GetValue(CPAIConstantUtil.Note).Replace("-", "");
                            dataVCO.VCDA_AREA_UNIT = dataDetail.requester_info.area_unit;
                            dataVCO.VCDA_DATE_PURCHASE = DateTime.ParseExact(dataDetail.requester_info.date_purchase, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                            dataVCO.VCDA_PRIORITY = dataDetail.requester_info.workflow_priority;

                            dataVCO.VCDA_TPC_PLAN_MONTH = dataDetail.crude_info.tpc_plan_month;
                            dataVCO.VCDA_TPC_PLAN_YEAR = dataDetail.crude_info.tpc_plan_year;
                            dataVCO.VCDA_LOADING_DATE_FROM = string.IsNullOrEmpty(dataDetail.crude_info.loading_date_from) ? (DateTime?)null : DateTime.ParseExact(dataDetail.crude_info.loading_date_from, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                            dataVCO.VCDA_LOADING_DATE_TO =string.IsNullOrEmpty(dataDetail.crude_info.loading_date_to) ? (DateTime?)null : DateTime.ParseExact(dataDetail.crude_info.loading_date_to, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                            dataVCO.VCDA_PURCHASED_BY = dataDetail.crude_info.purchased_by;                            
                            dataVCO.VCDA_CRUDE_NAME = dataDetail.crude_info.crude_name;
                            dataVCO.VCDA_PURCHASE_TYPE = dataDetail.crude_info.purchase_type;
                            dataVCO.VCDA_ORIGIN = dataDetail.crude_info.origin;
                            dataVCO.VCDA_PURCHASE_METHOD = dataDetail.crude_info.purchase_method;
                            dataVCO.VCDA_SUPPLIER = dataDetail.crude_info.supplier;
                            dataVCO.VCDA_PURCHASE_DATE = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.purchase_date, "dd-MMM-yyyy");
                            dataVCO.VCDA_DISCHARGING_DATE_FROM = string.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? (DateTime?)null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                            dataVCO.VCDA_DISCHARGING_DATE_TO = string.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? (DateTime?)null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                            if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_RUN_LP))
                            {
                                /// Ice Edit Fix Not Save Processing date from
                                dataVCO.VCDA_PROCESSING_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                dataVCO.VCDA_PROCESSING_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                /// End Ice Fix
                            }
                            dataVCO.VCDA_BENCHMARK_PRICE = dataDetail.crude_info.benchmark_price;
                            dataVCO.VCDA_PREMIUM = dataDetail.crude_info.premium;
                            dataVCO.VCDA_INCOTERM = dataDetail.crude_info.incoterm;
                            dataVCO.VCDA_FORMULA_PRICE = dataDetail.crude_info.formula_price;
                            dataVCO.VCDA_QUANTITY_KBBL_MAX = dataDetail.crude_info.quantity_kbbl_max;
                            dataVCO.VCDA_QUANTITY_KT_MAX = dataDetail.crude_info.quantity_kt_max;

                            dataVCO.VCDA_PREMIUM_MAXIMUM = dataDetail.crude_info.premium_maximum;
                            dataVCO.VCDA_PURCHASE_RESULT = dataDetail.crude_info.purchase_result;

                            dataVCO.VCDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataVCO.VCDA_CREATED = dtNow;
                            dataVCO.VCDA_CREATED_BY = dataDetail.requester_info.name;
                            dataVCO.VCDA_UPDATED = dtNow;
                            dataVCO.VCDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                           // dataVCO.VCDA_SCSC_SH_NOTE = dataDetail.requester_info.reason_reject_SCSC_SH;
                            dataDal.Save(dataVCO, context);
                            #endregion

                            #region VCO_COMMENT
                            VCO_COMMENT_DAL commentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT commentVCO = new VCO_COMMENT();
                            commentDal.Delete(TransID, context);
                            commentVCO.VCCO_ROW_ID = Guid.NewGuid().ToString("N");
                            commentVCO.VCCO_FK_VCO_DATA = TransID;
                            commentVCO.VCCO_CMCS_REQUEST = dataDetail.comment.cmcs_request;
                            commentVCO.VCCO_EVPC = dataDetail.comment.evpc;


                            ///LP Run Comment
                            commentVCO.VCCO_LP_RUN_NOTE = dataDetail.comment.lp_run_note;
                            commentVCO.VCCO_LP_RUN_SUMMARY = dataDetail.comment.lp_run_summary;
                            commentVCO.VCCO_LP_RUN_SUMMARY_MOBILE = dataDetail.comment.lp_run_summary_mobile;
                            commentVCO.VCCO_REASON_FOR_PURCHASING = dataDetail.comment.reason_for_purchasing;
                            commentVCO.VCCO_SCEP = dataDetail.comment.scep;
                            commentVCO.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;

                            commentVCO.VCCO_CREATED = dtNow;
                            commentVCO.VCCO_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            commentVCO.VCCO_UPDATED = dtNow;
                            commentVCO.VCCO_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            commentDal.Save(commentVCO, context);
                            #endregion
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAIVcoolUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIVcoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error InsertDB >> Rollback # :: Exception >>> ", e);
                log.Error("InsertDB::Rollback >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAIVcoolUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool InsertDB_ETA_Date_History(string TransID, string UserGroup, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail)
        {
            log.Info("# Start State CPAIVcoolUpdateDataState >> InsertDB #  ");
            string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);

            #region Insert Data into DB            
            bool isSuccess = false;
            DateTime dtNow;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dtNow = DateTime.Now;
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            #region VCO_ETA_DATE_HISTORY
                            VCO_ETA_DATE_HISTORY_DAL etaDateHistoryDal = new VCO_ETA_DATE_HISTORY_DAL();
                            VCO_ETA_DATE_HISTORY etaDateHistory = new VCO_ETA_DATE_HISTORY();
                            etaDateHistory.VCDH_ROW_ID = Guid.NewGuid().ToString("N");
                            etaDateHistory.VCDH_FK_VCO_DATA = TransID;
                            etaDateHistory.VCDH_USER_GROUP = UserGroup;
                            etaDateHistory.VCDH_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                            etaDateHistory.VCDH_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");

                            etaDateHistory.VCDH_CREATED = dtNow;
                            etaDateHistory.VCDH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            etaDateHistory.VCDH_UPDATED = dtNow;
                            etaDateHistory.VCDH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            etaDateHistoryDal.Save(etaDateHistory, context);
                            #endregion                            
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAIVcoolUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIVcoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error InsertDB >> Rollback # :: Exception >>> ", e);
                log.Error("InsertDB::Rollback >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAIVcoolUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool UpdateDB(string TransID, string Status, string NextStatus, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail)
        {
            bool isSuccess = false;
            DateTime dtNow;

            try
            {
                log.Info("# Start State CPAIVcoolUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dtNow = DateTime.Now;
                            VCO_DATA_DAL dataDal = new VCO_DATA_DAL();
                            VCO_DATA data = dataDal.GetByID(TransID);
                            if (dataDetail != null)
                                data.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : "";
                            if(!String.IsNullOrEmpty(data.VCDA_PREMIUM) && !String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum))
                            {
                                if(Convert.ToDecimal(data.VCDA_PREMIUM) > Convert.ToDecimal(dataDetail.crude_info.premium_maximum))
                                {
                                    etxValue.SetValue(CPAIConstantUtil.CheckSendMail_M05, new ExtendValue { value = "T", encryptFlag = "N" });
                                }
                                else
                                {
                                    etxValue.SetValue(CPAIConstantUtil.CheckSendMail_M05, new ExtendValue { value = "F", encryptFlag = "N" });
                                }
                            }

                            data.VCDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.VCDA_UPDATED = dtNow;
                            data.VCDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.VCDA_SCSC_SH_NOTE = dataDetail.requester_info.reason_reject_SCSC_SH;
                            data.VCDA_TNVP_NOTE = dataDetail.requester_info.reason_reject_TNVP;
                            data.VCDA_SCVP_NOTE = dataDetail.requester_info.reason_reject_SCVP;
                            dataDal.Update(data, context);

                            
                            VCO_CRUDE_COMPARISON_DAL data_compare_dal = new VCO_CRUDE_COMPARISON_DAL();
                            data_compare_dal.Delete(TransID, context);

                            if (dataDetail != null && dataDetail.crude_compare != null)
                            {
                                int i = 0;
                                foreach (var item in dataDetail.crude_compare.yield)
                                {
                                    VCO_CRUDE_COMPARISON data_compare = new VCO_CRUDE_COMPARISON();
                                    data_compare.VCCP_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + i++;
                                    data_compare.VCCP_ORDER = item.order;
                                    data_compare.VCCP_YIELD = item.yield;
                                    //data_compare.VCCP_BASE_CRUDE 
                                    data_compare.VCCP_BASE_VALUE = item.base_value;
                                    data_compare.VCCP_COMPARE_CRUDE = item.compare_crude;
                                    data_compare.VCCP_COMPARE_VALUE = item.compare_value;
                                    //data_compare.VCCP_DELTA_CRUDE 
                                    data_compare.VCCP_DELTA_VALUE = item.delta_value;
                                    data_compare.VCCP_CREATED = dtNow;
                                    data_compare.VCCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    data_compare.VCCP_UPDATED = dtNow;
                                    data_compare.VCCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    data_compare.VCCP_FK_VCO_DATA = TransID;
                                    data_compare_dal.Save(data_compare, context);                                    
                                }

                            }

                            VCO_COMMENT_DAL commentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT comment = commentDal.GetByDataID(TransID);
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if (dataDetail != null && Status.Equals(CPAIConstantUtil.STATUS_DRAFT))
                            {
                                /// Ice Edit Fix Case Draft not save data
                                comment.VCCO_LP_RUN_NOTE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_note)) ? comment.VCCO_LP_RUN_NOTE : dataDetail.comment.lp_run_note;
                                comment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? comment.VCCO_LP_RUN_SUMMARY : dataDetail.comment.lp_run_summary;
                                comment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? comment.VCCO_LP_RUN_SUMMARY_MOBILE : dataDetail.comment.lp_run_summary_mobile;
                                comment.VCCO_REASON_FOR_PURCHASING = (String.IsNullOrEmpty(dataDetail.comment.reason_for_purchasing)) ? comment.VCCO_REASON_FOR_PURCHASING : dataDetail.comment.reason_for_purchasing;
                                comment.VCCO_LP_RUN_ATTACH_FILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_attach_file)) ? comment.VCCO_LP_RUN_ATTACH_FILE : dataDetail.comment.lp_run_attach_file;

                                comment.VCCO_CMCS_NOTE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_note)) ? comment.VCCO_CMCS_NOTE : dataDetail.comment.cmcs_note;
                                comment.VCCO_CMCS_REQUEST = (String.IsNullOrEmpty(dataDetail.comment.cmcs_request)) ? comment.VCCO_CMCS_REQUEST : dataDetail.comment.cmcs_request;
                                comment.VCCO_CMCS_PROPOSE_FINAL = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_final)) ? comment.VCCO_CMCS_PROPOSE_FINAL : dataDetail.comment.cmcs_propose_final;
                                comment.VCCO_CMCS_PROPOSE_DISCHARGE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_discharge)) ? comment.VCCO_CMCS_PROPOSE_DISCHARGE : dataDetail.comment.cmcs_propose_discharge;

                                comment.VCCO_TN = (String.IsNullOrEmpty(dataDetail.comment.tn)) ? comment.VCCO_TN : dataDetail.comment.tn;
                                comment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? comment.VCCO_SCEP : dataDetail.comment.scep;
                                comment.VCCO_SCVP = (String.IsNullOrEmpty(dataDetail.comment.scvp)) ? comment.VCCO_SCVP : dataDetail.comment.scvp;
                                comment.VCCO_EVPC = (String.IsNullOrEmpty(dataDetail.comment.evpc)) ? comment.VCCO_EVPC : dataDetail.comment.evpc;
                                comment.VCCO_TNVP = (String.IsNullOrEmpty(dataDetail.comment.tnvp)) ? comment.VCCO_TNVP : dataDetail.comment.tnvp;
                                comment.VCCO_CMVP = (String.IsNullOrEmpty(dataDetail.comment.cmvp)) ? comment.VCCO_CMVP : dataDetail.comment.cmvp;

                                data.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? data.VCDA_DISCHARGING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                data.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? data.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                data.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? data.VCDA_DISCHARGING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                                data.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? data.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");

                                data.VCDA_PREMIUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium)) ? data.VCDA_PREMIUM : dataDetail.crude_info.premium;
                                data.VCDA_INCOTERM = (String.IsNullOrEmpty(dataDetail.crude_info.incoterm)) ? data.VCDA_INCOTERM : dataDetail.crude_info.incoterm;
                                data.VCDA_FK_SUPPLIER = (String.IsNullOrEmpty(dataDetail.crude_info.supplier)) ? data.VCDA_FK_SUPPLIER : dataDetail.crude_info.supplier;
                                data.VCDA_PURCHASE_TYPE = (String.IsNullOrEmpty(dataDetail.crude_info.purchase_type)) ? data.VCDA_PURCHASE_TYPE : dataDetail.crude_info.purchase_type;
                                data.VCDA_BENCHMARK_PRICE = (String.IsNullOrEmpty(dataDetail.crude_info.benchmark_price)) ? data.VCDA_BENCHMARK_PRICE : dataDetail.crude_info.benchmark_price;                                                                
                            }
                            else if (dataDetail != null && (Status.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) || Status.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE)
                                || Status.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK) || Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK)
                                || Status.Equals(CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL)))
                            {
                                comment.VCCO_REASON_FOR_PURCHASING = (String.IsNullOrEmpty(dataDetail.comment.reason_for_purchasing)) ? comment.VCCO_REASON_FOR_PURCHASING : dataDetail.comment.reason_for_purchasing;

                                comment.VCCO_CMCS_NOTE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_note)) ? comment.VCCO_CMCS_NOTE : dataDetail.comment.cmcs_note;
                                comment.VCCO_CMCS_REQUEST = (String.IsNullOrEmpty(dataDetail.comment.cmcs_request)) ? comment.VCCO_CMCS_REQUEST : dataDetail.comment.cmcs_request;
                                comment.VCCO_CMCS_PROPOSE_FINAL = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_final)) ? comment.VCCO_CMCS_PROPOSE_FINAL : dataDetail.comment.cmcs_propose_final;
                                comment.VCCO_CMCS_PROPOSE_DISCHARGE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_discharge)) ? comment.VCCO_CMCS_PROPOSE_DISCHARGE : dataDetail.comment.cmcs_propose_discharge;

                                data.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? data.VCDA_DISCHARGING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                data.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? data.VCDA_DISCHARGING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                                data.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? data.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                data.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? data.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");

                                data.VCDA_PURCHASE_RESULT = (String.IsNullOrEmpty(dataDetail.crude_info.purchase_result)) ? data.VCDA_PURCHASE_RESULT : dataDetail.crude_info.purchase_result;
                                data.VCDA_INCOTERM = (String.IsNullOrEmpty(dataDetail.crude_info.incoterm)) ? data.VCDA_INCOTERM : dataDetail.crude_info.incoterm;
                                data.VCDA_FK_SUPPLIER = (String.IsNullOrEmpty(dataDetail.crude_info.supplier)) ? data.VCDA_FK_SUPPLIER : dataDetail.crude_info.supplier;
                                data.VCDA_BENCHMARK_PRICE = (String.IsNullOrEmpty(dataDetail.crude_info.benchmark_price)) ? data.VCDA_BENCHMARK_PRICE : dataDetail.crude_info.benchmark_price;

                                // ABLE TO SAVE DRAFT NULLABLE VALUE
                                if (Status.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE))
                                {
                                    comment.VCCO_CMCS_PROPOSE_DISCHARGE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_discharge)) ? string.Empty : dataDetail.comment.cmcs_propose_discharge;
                                    data.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                    data.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                                }
                                else if (Status.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE))
                                {
                                    comment.VCCO_CMCS_PROPOSE_DISCHARGE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_discharge)) ? string.Empty : dataDetail.comment.cmcs_propose_discharge;
                                    data.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                    data.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                                }
                            }
                            else if (dataDetail != null && (Status.Equals(CPAIConstantUtil.STATUS_WAITING_RUN_LP) || Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_RUN_LP)))
                            {
                                comment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? comment.VCCO_SCEP : dataDetail.comment.scep;
                                comment.VCCO_LP_RUN_NOTE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_note)) ? comment.VCCO_LP_RUN_NOTE : dataDetail.comment.lp_run_note;
                                comment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? comment.VCCO_LP_RUN_SUMMARY : dataDetail.comment.lp_run_summary;
                                comment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? comment.VCCO_LP_RUN_SUMMARY_MOBILE : dataDetail.comment.lp_run_summary_mobile;
                                comment.VCCO_REASON_FOR_PURCHASING = (String.IsNullOrEmpty(dataDetail.comment.reason_for_purchasing)) ? comment.VCCO_REASON_FOR_PURCHASING : dataDetail.comment.reason_for_purchasing;
                                comment.VCCO_LP_RUN_ATTACH_FILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_attach_file)) ? comment.VCCO_LP_RUN_ATTACH_FILE : dataDetail.comment.lp_run_attach_file;
                                data.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? data.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                data.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? data.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                data.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? data.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");

                                // ABLE TO SAVE DRAFT NULLABLE VALUE
                                if (Status.Equals(CPAIConstantUtil.STATUS_WAITING_RUN_LP))
                                {
                                    comment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                    comment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                    comment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                    data.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? string.Empty : dataDetail.crude_info.premium_maximum;
                                    data.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                    data.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                }
                                else if (Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_RUN_LP))
                                {
                                    comment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                    comment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                    comment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                    data.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? string.Empty : dataDetail.crude_info.premium_maximum;
                                    data.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                    data.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                    data.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? string.Empty : dataDetail.requester_info.reason_reject_SCEP_SH;
                                }
                            }
                            else if (dataDetail != null && (Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_PRICE) || Status.Equals(CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE)))
                            {
                                data.VCDA_PURCHASE_RESULT = (String.IsNullOrEmpty(dataDetail.crude_info.purchase_result)) ? data.VCDA_PURCHASE_RESULT : dataDetail.crude_info.purchase_result;
                                data.VCDA_SUPPLIER = (String.IsNullOrEmpty(dataDetail.crude_info.supplier)) ? data.VCDA_SUPPLIER : dataDetail.crude_info.supplier;
                                data.VCDA_FINAL_PREMIUM = (String.IsNullOrEmpty(dataDetail.crude_info.final_premium)) ? data.VCDA_FINAL_PREMIUM : dataDetail.crude_info.final_premium;
                                data.VCDA_FINAL_INCOTERM = (String.IsNullOrEmpty(dataDetail.crude_info.final_incoterm)) ? data.VCDA_FINAL_INCOTERM : dataDetail.crude_info.final_incoterm;
                                data.VCDA_FINAL_BENCHMARK_PRICE = (String.IsNullOrEmpty(dataDetail.crude_info.final_benchmark_price)) ? data.VCDA_FINAL_BENCHMARK_PRICE : dataDetail.crude_info.final_benchmark_price;

                                comment.VCCO_CMCS_REQUEST = (String.IsNullOrEmpty(dataDetail.comment.cmcs_request)) ? comment.VCCO_CMCS_REQUEST : dataDetail.comment.cmcs_request;
                                comment.VCCO_CMCS_PROPOSE_FINAL = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_final)) ? comment.VCCO_CMCS_PROPOSE_FINAL : dataDetail.comment.cmcs_propose_final;
                                comment.VCCO_CMCS_PROPOSE_DISCHARGE = (String.IsNullOrEmpty(dataDetail.comment.cmcs_propose_discharge)) ? comment.VCCO_CMCS_PROPOSE_DISCHARGE : dataDetail.comment.cmcs_propose_discharge;
                                comment.VCCO_LP_RUN_ATTACH_FILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_attach_file)) ? comment.VCCO_LP_RUN_ATTACH_FILE : dataDetail.comment.lp_run_attach_file;

                                comment.VCCO_TN = (String.IsNullOrEmpty(dataDetail.comment.tn)) ? comment.VCCO_TN : dataDetail.comment.tn;
                                comment.VCCO_SCVP = (String.IsNullOrEmpty(dataDetail.comment.scvp)) ? comment.VCCO_SCVP : dataDetail.comment.scvp;
                                comment.VCCO_TNVP = (String.IsNullOrEmpty(dataDetail.comment.tnvp)) ? comment.VCCO_TNVP : dataDetail.comment.tnvp;
                                comment.VCCO_CMVP = (String.IsNullOrEmpty(dataDetail.comment.cmvp)) ? comment.VCCO_CMVP : dataDetail.comment.cmvp;
                                comment.VCCO_EVPC = (String.IsNullOrEmpty(dataDetail.comment.evpc)) ? comment.VCCO_EVPC : dataDetail.comment.evpc;

                                // ABLE TO SAVE DRAFT NULLABLE VALUE
                                if (Status.Equals(CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE))
                                {
                                    comment.VCCO_CMCS_REQUEST = (String.IsNullOrEmpty(dataDetail.comment.cmcs_request)) ? string.Empty : dataDetail.comment.cmcs_request;
                                }
                                else if(Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_PRICE))
                                {
                                    comment.VCCO_EVPC = (String.IsNullOrEmpty(dataDetail.comment.evpc)) ? string.Empty : dataDetail.comment.evpc;
                                    data.VCDA_REASON_CMCS_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_CMCS_SH)) ? data.VCDA_REASON_CMCS_SH : dataDetail.requester_info.reason_reject_CMCS_SH;
                                }
                            }
                            else if (dataDetail != null && Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_SUMMARY))
                            {
                                data.VCDA_PREMIUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium)) ? data.VCDA_PREMIUM : dataDetail.crude_info.premium;
                                data.VCDA_PURCHASE_RESULT = (String.IsNullOrEmpty(dataDetail.crude_info.purchase_result)) ? data.VCDA_PURCHASE_RESULT : dataDetail.crude_info.purchase_result;
                                data.VCDA_INCOTERM = (String.IsNullOrEmpty(dataDetail.crude_info.incoterm)) ? data.VCDA_INCOTERM : dataDetail.crude_info.incoterm;
                                data.VCDA_SUPPLIER = (String.IsNullOrEmpty(dataDetail.crude_info.supplier)) ? data.VCDA_SUPPLIER : dataDetail.crude_info.supplier;
                                data.VCDA_BENCHMARK_PRICE = (String.IsNullOrEmpty(dataDetail.crude_info.benchmark_price)) ? data.VCDA_BENCHMARK_PRICE : dataDetail.crude_info.benchmark_price;
                                data.VCDA_PURCHASE_DATE = (String.IsNullOrEmpty(dataDetail.crude_info.purchase_date)) ? data.VCDA_PURCHASE_DATE : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.purchase_date, "dd-MMM-yyyy");
                                comment.VCCO_LP_RUN_ATTACH_FILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_attach_file)) ? comment.VCCO_LP_RUN_ATTACH_FILE : dataDetail.comment.lp_run_attach_file;
                                comment.VCCO_EVPC = (String.IsNullOrEmpty(dataDetail.comment.evpc)) ? comment.VCCO_EVPC : dataDetail.comment.evpc;

                                // ABLE TO SAVE DRAFT NULLABLE VALUE
                                if (Status.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_SUMMARY))
                                {
                                    data.VCDA_PURCHASE_DATE = (String.IsNullOrEmpty(dataDetail.crude_info.purchase_date)) ? null : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.purchase_date, "dd-MMM-yyyy");
                                }
                            }
                            comment.VCCO_UPDATED = dtNow;
                            comment.VCCO_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDal.Update(data, context);
                            commentDal.Update(comment, context);
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                            if (dataDetail != null)
                            {
                                dataDetail.comment.cmcs_request = comment.VCCO_CMCS_REQUEST;
                                dataDetail.comment.cmvp = comment.VCCO_CMVP;
                                dataDetail.comment.tnvp = comment.VCCO_TNVP;
                                dataDetail.comment.scvp = comment.VCCO_SCVP;
                                dataDetail.comment.evpc = comment.VCCO_EVPC;
                            }

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateDB >> UpdateDB # :: Rollback >>>> ", ex);
                            log.Error("CPAIVcoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIVcoolUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateDB >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateCancelDB(string TransID, string Status, string NextStatus, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail)
        {
            bool isSuccess = false;
            DateTime dtNow;

            try
            {
                log.Info("# Start State CPAIVcoolUpdateDataState >> UpdateCancelDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dtNow = DateTime.Now;
                            VCO_DATA_DAL dataDal = new VCO_DATA_DAL();
                            VCO_DATA data = dataDal.GetByID(TransID);
                            data.VCDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.VCDA_UPDATED = dtNow;
                            data.VCDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDal.Update(data, context);

                            VCO_COMMENT_DAL commentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT comment = commentDal.GetByDataID(TransID);
                            if (dataDetail != null)
                            {
                                comment.VCCO_EVPC = dataDetail.comment.evpc;
                                commentDal.Update(comment, context);
                            }

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error UpdateCancelDB >> UpdateCancelDB # :: Rollback >>>> ", ex);
                            log.Error("CPAIVcoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIVcoolUpdateDataState >> UpdateCancelDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateCancelDB >> UpdateCancelDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}