﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALVCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000064
{
    public class CPAIVcoolExpertUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVcoolExpertUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string currentStatus = etxValue.GetValue(CPAIConstantUtil.Status); // current status
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string action = etxValue.GetValue(CPAIConstantUtil.Action); //current action
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction); //next action

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                    FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);
                    string ref_id = stateModel.EngineModel.ftxTransId;
                    if (ft != null && ft.FTX_PARENT_TRX_ID != null)
                    {
                        ref_id = ft.FTX_PARENT_TRX_ID;
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = parent_ft.FTX_INDEX8, encryptFlag = "N" });
                    }

                    UserGroupDAL userDal = new UserGroupDAL();
                    CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (r != null && r.USG_USER_GROUP != null)
                    {
                        userGroup = r.USG_USER_GROUP;
                    }
                    CPAI_USER_GROUP dlg = userDal.findByUserAndSystemDelegate(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (dlg != null && dlg.USG_USER_GROUP != null)
                    {
                        userGroup = dlg.USG_USER_GROUP;
                    }

                    bool isRunDB = false;
                    bool callF63Flag = false;

                    /*
                        action => currentAction
                        APPROVE_4 => APPROVE_4 = SAVE DRAFT
                        APPROVE_4 => APPROVE_5 = Submit (Agree/Disagree)
                        APPROVE_5 => APPROVE_4 = Rejected by SCSC Section Head
                        APPROVE_5 => APPROVE_8 = Approve (Agree/Disagree)
                        APPROVE_7 => APPROVE_4 = date is not in period
                        APPROVE_7 => APPROVE_8 = date is in period
                        APPROVE_8 => APPROVE_5 = Rejected by SCVP
                        APPROVE_8 => APPROVE_6 = Rejected by TNVP
                        APPROVE_8 => APPROVE_7 = Rejected by CMVP
                        APPROVE_8 => APPROVE_9 = APPROVED
                    */
                    isRunDB = UpdateDetailDB(ref_id, nextStatus, currentStatus, currentAction, etxValue, dataDetail, ref callF63Flag);
                    if (((currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE)) 
                        || (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK))
                        || (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK)))
                        && isRunDB)
                    {
                        isRunDB = InsertDB_ETA_Date_History(ref_id, userGroup, etxValue, dataDetail);
                    }
                    if (callF63Flag)
                    {
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        var json = new JavaScriptSerializer().Serialize(dataDetail);
                        RequestCPAI req = new RequestCPAI();
                        req.Function_id = ConstantPrm.FUNCTION.F10000063;
                        req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req.Req_transaction_id = parent_ft.FTX_REQ_TRANS;
                        req.State_name = "CPAIVerifyRequireInputContinueState";
                        req.Req_parameters = new Req_parameters();
                        req.Req_parameters.P = new List<P>();
                        req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                        req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_9 });
                        req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE });
                        req.Req_parameters.P.Add(new P { K = "user", V = etxValue.GetValue(CPAIConstantUtil.User) });
                        req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                        req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                        req.Req_parameters.P.Add(new P { K = "note", V = "Go to Waiting Confirm Price Status" });
                        req.Req_parameters.P.Add(new P { K = "attach_items", V = "" });
                        req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                        req.Extra_xml = "";

                        ResponseData resData = new ResponseData();
                        RequestData reqData = new RequestData();
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                        var xml = ShareFunction.XMLSerialize(req);
                        reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                        resData = service.CallService(reqData);
                        log.Debug(" resp function F10000063 >> " + resData.result_code);
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //First time when this transaction is initialized.
                        if (action.Equals("") && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_4))
                        {
                            string date_purchase = dataDetail.crude_info.purchase_date != null ? dataDetail.crude_info.purchase_date : "";
                            string products = dataDetail.crude_info.crude_name != null ? dataDetail.crude_info.crude_name : "";
                            string origin = dataDetail.crude_info.origin != null ? dataDetail.crude_info.origin : "";
                            string tpc_plan_month = dataDetail.crude_info.tpc_plan_month != null ? dataDetail.crude_info.tpc_plan_month : "";
                            string tpc_plan_year = dataDetail.crude_info.tpc_plan_year != null ? dataDetail.crude_info.tpc_plan_year : "";
                            string loading_from = dataDetail.crude_info.loading_date_from != null ? dataDetail.crude_info.loading_date_from : "";
                            string loading_to = dataDetail.crude_info.loading_date_to != null ? dataDetail.crude_info.loading_date_to : "";
                            string incoterm = dataDetail.crude_info.incoterm != null ? dataDetail.crude_info.incoterm : "";
                            string formula_p = dataDetail.crude_info.formula_price != null ? dataDetail.crude_info.formula_price : "";
                            string discharging_from = dataDetail.crude_info.discharging_date_from != null ? dataDetail.crude_info.discharging_date_from : "";
                            string discharging_to = dataDetail.crude_info.discharging_date_to != null ? dataDetail.crude_info.discharging_date_to : "";
                            string supplier = dataDetail.crude_info.supplier != null ? dataDetail.crude_info.supplier : "";
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                            //set data
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = products, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Origin, new ExtendValue { value = origin, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_month, new ExtendValue { value = tpc_plan_month, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_year, new ExtendValue { value = tpc_plan_year, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_from, new ExtendValue { value = loading_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_to, new ExtendValue { value = loading_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.incoterm, new ExtendValue { value = incoterm, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Formula_p, new ExtendValue { value = formula_p, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_from, new ExtendValue { value = discharging_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_to, new ExtendValue { value = discharging_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = supplier, encryptFlag = "N" });
                        }

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVcoolExpertUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAIVcoolExpertUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateDetailDB(string TransID, string nextStatus, string currentStatus, string currentAction, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail, ref bool flag)
        {
            log.Info("# Start State CPAIVcoolExpertUpdateDataState >> UpdateDetailDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            string tUser = etxValue.GetValue(CPAIConstantUtil.User);
            
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            #region Comment
                            VCO_COMMENT_DAL tCommentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT tComment = tCommentDal.GetByDataID(TransID);

                            VCO_DATA_DAL tDataDal = new VCO_DATA_DAL();
                            VCO_DATA tData = tDataDal.GetByID(TransID);


                            DateTime? discharging_from = null;
                            DateTime? discharging_to = null ;

                            if (tData != null)
                            {
                                discharging_from = tData.VCDA_DISCHARGING_DATE_FROM;
                                discharging_to = tData.VCDA_DISCHARGING_DATE_TO;
                            }                            

                            if (dataDetail != null)
                            {
                                tData.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? tData.VCDA_DISCHARGING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                tData.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? tData.VCDA_DISCHARGING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");


                                tComment.VCCO_CMCS_NOTE = dataDetail.comment.cmcs_note;
                                tComment.VCCO_CMCS_REQUEST = dataDetail.comment.cmcs_request; //CMCS Comment on request date.
                                tComment.VCCO_SCSC = dataDetail.comment.scsc;
                                tComment.VCCO_SCSC_AGREE_FLAG = dataDetail.comment.scsc_agree_flag.ToUpper().Equals(CPAIConstantUtil.VCOOL_SCSC_DISAGREE) ? "N" : "Y";
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE))
                                    tComment.VCCO_CMCS_PROPOSE_DISCHARGE = dataDetail.comment.cmcs_propose_discharge;
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP))
                                    tComment.VCCO_CMVP = dataDetail.comment.cmvp;
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP))
                                    tComment.VCCO_SCVP = dataDetail.comment.scvp;
                            }

                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP))
                            {
                                tComment.VCCO_CMVP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP))
                            {
                                tComment.VCCO_SCVP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED))
                            {
                                tComment.VCCO_CMVP_FLAG = "Y";
                                tComment.VCCO_SCVP_FLAG = "Y";
                            }

                            tComment.VCCO_UPDATED_BY = tUser;
                            tComment.VCCO_UPDATED = dtNow;

                            tCommentDal.Update(tComment, context);
                            #endregion

                            /*
                                action => currentAction
                                APPROVE_4 => APPROVE_4 = SAVE DRAFT
                                APPROVE_4 => APPROVE_5 = Submit (Agree/Disagree)
                                APPROVE_5 => APPROVE_4 = Rejected by SCSC Section Head
                                APPROVE_5 => APPROVE_8 = Approve (Agree/Disagree)
                                APPROVE_7 => APPROVE_4 = date is not in period
                                APPROVE_7 => APPROVE_8 = date is in period
                                APPROVE_8 => APPROVE_5 = Rejected by SCVP
                                APPROVE_8 => APPROVE_6 = Rejected by TNVP
                                APPROVE_8 => APPROVE_7 = Rejected by CMVP
                                APPROVE_8 => APPROVE_9 = APPROVED
                            */
                            string newNextStatus = nextStatus;
                            if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK))
                            {
                                if (tComment.VCCO_SCSC_AGREE_FLAG == "Y")
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                }
                                else
                                {
                                    newNextStatus = CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE;
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_7, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE, encryptFlag = "N" });
                                }
                            }
                            else if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK))
                            {
                                if (tData.VCDA_DISCHARGING_DATE_FROM >= tData.VCDA_SCSC_REVISED_DATE_FROM && tData.VCDA_DISCHARGING_DATE_TO <= tData.VCDA_SCSC_REVISED_DATE_TO)
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                }
                                else if (tData.VCDA_DISCHARGING_DATE_FROM >= discharging_from && tData.VCDA_DISCHARGING_DATE_TO <= discharging_to)
                                {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                }
                                else
                                {
                                    newNextStatus = CPAIConstantUtil.STATUS_WAITING_CHECK_TANK;
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_4, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_CHECK_TANK, encryptFlag = "N" });
                                }


                                

                            }
                            else if (nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED))
                            {
                               
                                bool vpFlag = tComment.VCCO_SCVP_FLAG == "Y" && tComment.VCCO_TNVP_FLAG == "Y" && tComment.VCCO_CMVP_FLAG == "Y" ? true : false;
                                flag = vpFlag;
                                //call F63 to update main transaction status
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            }
                            else
                            {
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            }                      

                            tData.VCDA_SC_STATUS = newNextStatus;
                            tData.VCDA_UPDATED = dtNow;
                            tData.VCDA_UPDATED_BY = tUser;
                            if(dataDetail != null && dataDetail.comment.scsc_agree_flag == CPAIConstantUtil.VCOOL_SCSC_DISAGREE)
                            {
                                tData.VCDA_SCSC_REVISED_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from,"dd-MMM-yyyy");
                                tData.VCDA_SCSC_REVISED_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                            }
                            tData.VCDA_SCSC_SH_NOTE = dataDetail.requester_info.reason_reject_SCSC_SH;
                            tData.VCDA_TNVP_NOTE = dataDetail.requester_info.reason_reject_TNVP;
                            tData.VCDA_SCVP_NOTE = dataDetail.requester_info.reason_reject_SCVP;
                            tDataDal.UpdateSCStatus(tData, context);

                            if (dataDetail != null)
                            {
                                dataDetail.comment.cmcs_request = tComment.VCCO_CMCS_REQUEST;
                                dataDetail.comment.cmvp = tComment.VCCO_CMVP;
                                dataDetail.comment.tnvp = tComment.VCCO_TNVP;
                                dataDetail.comment.scvp = tComment.VCCO_SCVP;
                                dataDetail.comment.evpc = tComment.VCCO_EVPC;
                            }

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICoolExpertUpdateDataState >> UpdateDetailDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertUpdateDataState >> UpdateDetailDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateCommentDB >> UpdateCommentDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }

        public bool InsertDB_ETA_Date_History(string TransID, string UserGroup, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail)
        {
            log.Info("# Start State CPAIVcoolUpdateDataState >> InsertDB #  ");
            string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);

            #region Insert Data into DB            
            bool isSuccess = false;
            DateTime dtNow;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            dtNow = DateTime.Now;
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            #region VCO_ETA_DATE_HISTORY
                            VCO_ETA_DATE_HISTORY_DAL etaDateHistoryDal = new VCO_ETA_DATE_HISTORY_DAL();
                            VCO_ETA_DATE_HISTORY etaDateHistory = new VCO_ETA_DATE_HISTORY();
                            etaDateHistory.VCDH_ROW_ID = Guid.NewGuid().ToString("N");
                            etaDateHistory.VCDH_FK_VCO_DATA = TransID;
                            etaDateHistory.VCDH_USER_GROUP = UserGroup;
                            if (UserGroup.Contains("SC"))
                            {
                                etaDateHistory.VCDH_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from, "dd-MMM-yyyy");
                                etaDateHistory.VCDH_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                            } else
                            {
                                etaDateHistory.VCDH_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                                etaDateHistory.VCDH_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                            }

                            etaDateHistory.VCDH_CREATED = dtNow;
                            etaDateHistory.VCDH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            etaDateHistory.VCDH_UPDATED = dtNow;
                            etaDateHistory.VCDH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            etaDateHistoryDal.Save(etaDateHistory, context);
                            #endregion                            
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAIVcoolUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIVcoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error InsertDB >> Rollback # :: Exception >>> ", e);
                log.Error("InsertDB::Rollback >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAIVcoolUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

    }
}