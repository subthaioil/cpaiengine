﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALVCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000065
{
    public class CPAIVcoolExpertSHUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVcoolExpertSHUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                if (prevStatus == null)
                {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });

                if (item != null)
                {
                    VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string action = etxValue.GetValue(CPAIConstantUtil.Action); //current action
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction); //next action

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                    FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);
                    string ref_id = stateModel.EngineModel.ftxTransId;
                    if (ft != null && ft.FTX_PARENT_TRX_ID != null)
                    {
                        ref_id = ft.FTX_PARENT_TRX_ID;
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = parent_ft.FTX_INDEX8, encryptFlag = "N" });
                    }

                    bool isRunDB = false;
                    bool callF63Flag = false;

                    isRunDB = UpdateDetailDB(ref_id, nextStatus, etxValue, dataDetail, ref callF63Flag);
                    if (callF63Flag)
                    {
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        var json = new JavaScriptSerializer().Serialize(dataDetail);
                        RequestCPAI req = new RequestCPAI();
                        req.Function_id = ConstantPrm.FUNCTION.F10000063;
                        req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req.Req_transaction_id = parent_ft.FTX_REQ_TRANS;
                        req.State_name = "CPAIVerifyRequireInputContinueState";
                        req.Req_parameters = new Req_parameters();
                        req.Req_parameters.P = new List<P>();
                        req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                        req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_9 });
                        req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE });
                        req.Req_parameters.P.Add(new P { K = "user", V = etxValue.GetValue(CPAIConstantUtil.User) });
                        req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                        req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                        req.Req_parameters.P.Add(new P { K = "note", V = "Go to Waiting Confirm Price Status" });
                        req.Req_parameters.P.Add(new P { K = "attach_items", V = "" });
                        req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                        req.Extra_xml = "";

                        ResponseData resData = new ResponseData();
                        RequestData reqData = new RequestData();
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                        var xml = ShareFunction.XMLSerialize(req);
                        reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                        resData = service.CallService(reqData);
                        log.Debug(" resp function F10000063 >> " + resData.result_code);
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //First time when this transaction is initialized.
                        if (action.Equals("") && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_6))
                        {
                            string date_purchase = dataDetail.crude_info.purchase_date != null ? dataDetail.crude_info.purchase_date : "";
                            string products = dataDetail.crude_info.crude_name != null ? dataDetail.crude_info.crude_name : "";
                            string origin = dataDetail.crude_info.origin != null ? dataDetail.crude_info.origin : "";
                            string tpc_plan_month = dataDetail.crude_info.tpc_plan_month != null ? dataDetail.crude_info.tpc_plan_month : "";
                            string tpc_plan_year = dataDetail.crude_info.tpc_plan_year != null ? dataDetail.crude_info.tpc_plan_year : "";
                            string loading_from = dataDetail.crude_info.loading_date_from != null ? dataDetail.crude_info.loading_date_from : "";
                            string loading_to = dataDetail.crude_info.loading_date_to != null ? dataDetail.crude_info.loading_date_to : "";
                            string incoterm = dataDetail.crude_info.incoterm != null ? dataDetail.crude_info.incoterm : "";
                            string formula_p = dataDetail.crude_info.formula_price != null ? dataDetail.crude_info.formula_price : "";
                            string discharging_from = dataDetail.crude_info.discharging_date_from != null ? dataDetail.crude_info.discharging_date_from : "";
                            string discharging_to = dataDetail.crude_info.discharging_date_to != null ? dataDetail.crude_info.discharging_date_to : "";
                            string supplier = dataDetail.crude_info.supplier != null ? dataDetail.crude_info.supplier : "";
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                            //set data
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = products, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Origin, new ExtendValue { value = origin, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_month, new ExtendValue { value = tpc_plan_month, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_year, new ExtendValue { value = tpc_plan_year, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_from, new ExtendValue { value = loading_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_to, new ExtendValue { value = loading_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.incoterm, new ExtendValue { value = incoterm, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Formula_p, new ExtendValue { value = formula_p, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_from, new ExtendValue { value = discharging_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_to, new ExtendValue { value = discharging_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = supplier, encryptFlag = "N" });
                        }

                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVcoolExpertSHUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAIVcoolExpertSHUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolExpertSHUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateDetailDB(string TransID, string nextStatus, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail, ref bool flag)
        {
            log.Info("# Start State CPAIVcoolExpertSHUpdateDataState >> UpdateDetailDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            string tUser = etxValue.GetValue(CPAIConstantUtil.User);
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            VCO_COMMENT_DAL tCommentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT tComment = tCommentDal.GetByDataID(TransID);

                            if (dataDetail != null)
                            {
                                tComment.VCCO_TN = dataDetail.comment.tn;
                                tComment.VCCO_TNVP = dataDetail.comment.tnvp;
                            }

                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_IMPACT_APPROVED))
                            {
                                tComment.VCCO_TNVP_FLAG = "Y";
                            }

                            tComment.VCCO_UPDATED_BY = tUser;
                            tComment.VCCO_UPDATED = dtNow;

                            tCommentDal.Update(tComment, context);

                            string newNextStatus = nextStatus;
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_IMPACT_APPROVED))
                            {
                                bool vpFlag = tComment.VCCO_SCVP_FLAG == "Y" && tComment.VCCO_TNVP_FLAG == "Y" && tComment.VCCO_CMVP_FLAG == "Y" ? true : false;
                                flag = vpFlag;
                                
                                //call F63 to update main transaction status                                                               
                            }

                            VCO_DATA_DAL tDataDal = new VCO_DATA_DAL();
                            VCO_DATA tData = tDataDal.GetByID(TransID);

                            tData.VCDA_TN_STATUS = newNextStatus;
                            tData.VCDA_UPDATED = dtNow;
                            tData.VCDA_UPDATED_BY = tUser;
                            tData.VCDA_SCSC_SH_NOTE = dataDetail.requester_info.reason_reject_SCSC_SH;
                            tData.VCDA_TNVP_NOTE = dataDetail.requester_info.reason_reject_TNVP;
                            tData.VCDA_SCVP_NOTE = dataDetail.requester_info.reason_reject_SCVP;
                            tDataDal.UpdateTNStatus(tData, context);

                            if (dataDetail != null)
                            {
                                dataDetail.comment.cmcs_request = tComment.VCCO_CMCS_REQUEST;
                                dataDetail.comment.cmvp = tComment.VCCO_CMVP;
                                dataDetail.comment.tnvp = tComment.VCCO_TNVP;
                                dataDetail.comment.scvp = tComment.VCCO_SCVP;
                                dataDetail.comment.evpc = tComment.VCCO_EVPC;
                            }

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAIVcoolExpertSHUpdateDataState >> UpdateDetailDB # :: Rollback >>>> ", ex);
                            log.Error("CPAIVcoolExpertSHUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIVcoolExpertSHUpdateDataState >> UpdateDetailDB # ");
            }
            catch (Exception ex)
            {
                log.Error("# Error UpdateCommentDB >> UpdateCommentDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolExpertSHUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }
    }
}