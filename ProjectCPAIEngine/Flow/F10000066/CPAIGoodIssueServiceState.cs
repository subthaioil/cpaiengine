﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using static ProjectCPAIEngine.Model.GoodsIssueUpdateModel;

namespace ProjectCPAIEngine.Flow.F10000066
{
    public class CPAIGoodIssueServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGoodIssueServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {

                    GoodsIssueUpdateModel dataDetail = JSonConvertUtil.jsonToModel<GoodsIssueUpdateModel>(item);
                    GoodsIssueUpdateModel temp = new GoodsIssueUpdateModel();

                    temp.ACT_GI_DATE = dataDetail.ACT_GI_DATE;
                    temp.mDELIVERY_HEADER = new List<LIKP>();
                    var DelivHeader = dataDetail.mDELIVERY_HEADER.FirstOrDefault();
                    LIKP tmpDHeader = new LIKP();
                    tmpDHeader.VBELN = DelivHeader.VBELN;
                    tmpDHeader.TRATY = DelivHeader.TRATY;
                    temp.mDELIVERY_HEADER.Add(tmpDHeader);


                    var cfgItem = getDELIVERY_ITEM();
                    var cfgCCodePlant = getPlant();
                    temp.mDELIVERY_ITEM = new List<ZDO_ITEM>();
                    var DelivItem = dataDetail.mDELIVERY_ITEM;
                    List<ZDO_ITEM> tmpDItem = new List<ZDO_ITEM>();
                    foreach(var z in DelivItem)
                    {
                        tmpDItem.Add(new ZDO_ITEM
                        {
                            POSNR_VL = z.POSNR_VL,
                            MATNR = z.MATNR,
                            WERKS = z.WERKS,
                            LIANP = cfgItem.LIANP,
                            LFIMG = z.LFIMG,
                            CustomerCode = z.CustomerCode,
                            SALES_ORG = z.SALES_ORG,
                            ZTEMP = z.ZTEMP,
                            ZDENS_15C = z.ZDENS_15C,

                        });
                    }
                    temp.mDELIVERY_ITEM = tmpDItem;
                    var cfgItemAddQTY = getDELIVERY_ITEM_ADD_QTY();
                    temp.mDELIVERY_ITEM_ADD_QTY = new List<LIPSO2>();  
                    LIPSO2[] tmpDAdd = new LIPSO2[dataDetail.mDELIVERY_ITEM_ADD_QTY.Count];
                     
                    int i = 0; 
                    foreach (LIPSO2 DelivIAdd in dataDetail.mDELIVERY_ITEM_ADD_QTY)
                    {
                        tmpDAdd[i] = new LIPSO2();
                        tmpDAdd[i].POSNR = DelivIAdd.POSNR;
                        tmpDAdd[i].MSEHI = DelivIAdd.MSEHI;//"BBL";
                        tmpDAdd[i].ADQNT = DelivIAdd.ADQNT;//1600;
                        tmpDAdd[i].MANEN = cfgItemAddQTY.MANEN;//"X";//Global Config
                        temp.mDELIVERY_ITEM_ADD_QTY.Add(tmpDAdd[i]);
                        i++;
                    }                   
                    ConfigManagement configManagement = new ConfigManagement();
                    String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    String content = js.Serialize(temp);
                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(config); //convert jason

                    GoodsIssueUpdateServiceConnectorImpl GIUpdateService = new GoodsIssueUpdateServiceConnectorImpl();
                    String con = js.Serialize(jsonConfig);
                    DownstreamResponse<string> res = GIUpdateService.connect(con, content);


                    ////etxValue
                    //string logon_DO = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|") + 1, res.responseData.ToString().IndexOf("R") - res.responseData.ToString().IndexOf("|") - 1);
                    //string UPD_DO_No = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("R") + 1, res.responseData.ToString().IndexOf("|D|") - res.responseData.ToString().IndexOf("R") - 1);
                    //string Msg_DO = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|D|") + 3, res.responseData.ToString().IndexOf("|G|") - res.responseData.ToString().IndexOf("|D|") - 3);

                    //string logon_GI = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|") + 1, res.responseData.ToString().IndexOf("R") - res.responseData.ToString().IndexOf("|") - 1);
                    //string UPD_GI_No = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("R") + 1, res.responseData.ToString().IndexOf("|G|") - res.responseData.ToString().IndexOf("R") - 1);
                    //string Msg_GI = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|G|") + 3, res.responseData.ToString().IndexOf("|D|") - res.responseData.ToString().IndexOf("|G|") - 3);

                    //ExtendValue extRowsSap_logon_DO = new ExtendValue
                    //{ value = logon_DO, encryptFlag = ConstantDBUtil.NO_FLAG };
                    //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.LogOnDo, extRowsSap_logon_DO);

                    //ExtendValue extRowsDOno = new ExtendValue
                    //{ value = UPD_DO_No, encryptFlag = ConstantDBUtil.NO_FLAG };
                    //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.UPD_DO_IND, extRowsDOno);

                    //ExtendValue extRowsDOmsg = new ExtendValue
                    //{ value = Msg_DO, encryptFlag = ConstantDBUtil.NO_FLAG };
                    //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.msgDO, extRowsDOmsg);


                    //ExtendValue extRowsSap_logon_GI = new ExtendValue
                    //{ value = logon_GI, encryptFlag = ConstantDBUtil.NO_FLAG };
                    //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.LogOnGI, extRowsSap_logon_GI);

                    //ExtendValue extRowsGIno = new ExtendValue
                    //{ value = UPD_GI_No, encryptFlag = ConstantDBUtil.NO_FLAG };
                    //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.UPD_GI_IND, extRowsGIno);

                    //ExtendValue extRowsGImsg = new ExtendValue
                    //{ value = Msg_GI, encryptFlag = ConstantDBUtil.NO_FLAG };
                    //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.msgGI, extRowsGImsg);

                    if(res.resultCode == "1")
                    {
                        stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        stateModel.BusinessModel.currentCode = res.resultCode;
                        if(res.resultCode == "900001")
                        {
                            stateModel.BusinessModel.currentDesc = ServiceConstant.RESP_SYETEM_ERROR[1];
                        }
                        else if(res.resultCode == "900002")
                        {
                            stateModel.BusinessModel.currentDesc = ServiceConstant.RESP_ADDRESS_ERROR[1];
                        }
                        else if(res.resultCode == "900003")
                        {
                            stateModel.BusinessModel.currentDesc = ServiceConstant.RESP_MESSAGE_ERROR[1];
                        }
                        else
                        {
                            stateModel.BusinessModel.currentDesc = ServiceConstant.RESP_SAP_ERROR[1];
                        }                        
                        stateModel.BusinessModel.currentMessage = res.resultDesc;
                    } 
                    //respCodeManagement.setCurrentCodeMapping(stateModel);
                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIGoodIssueServiceState # :: Exception >>> " + ex);
                log.Error("CPAIGoodIssueServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }


        public PLANT getPlant()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_GI_LGORT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigGI dataList = (GlobalConfigGI)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigGI));
            return dataList.PLANT;
        }
        public DELIVERY_ITEM getDELIVERY_ITEM()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_GI_LGORT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigGI dataList = (GlobalConfigGI)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigGI));
            return dataList.DELIVERY_ITEM;
        }
        public DELIVERY_ITEM_ADD_QTY getDELIVERY_ITEM_ADD_QTY()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_GI_LGORT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigGI dataList = (GlobalConfigGI)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigGI));
            return dataList.DELIVERY_ITEM_ADD_QTY;
        }



        [Serializable]
        public class GlobalConfigGI
        {
            public PLANT PLANT { get; set; }
            public DELIVERY_ITEM DELIVERY_ITEM { get; set; }
            public DELIVERY_ITEM_ADD_QTY DELIVERY_ITEM_ADD_QTY { get; set; }

        }

        public class PLANT
        {
            public PL1200 PL1200 { get; set; }
            public PL4200 PL4200 { get; set; }
        }

        public class PL1200
        {
            public string SaleOrg { get; set; }
            public string PLANT { get; set; }
            public string Customer01 { get; set; }
            public string LGORT01 { get; set; }
            public string Customer02 { get; set; }
            public string LGORT02 { get; set; }
        }

        public class PL4200
        {
            public string SaleOrg { get; set; }
            public string PLANT { get; set; }
            public string Customer03 { get; set; }
            public string LGORT03 { get; set; }
            public string Customer04 { get; set; }
            public string LGORT04 { get; set; }
        }

        public class DELIVERY_ITEM
        {
            public string LIANP { get; set; }
        }
        public class DELIVERY_ITEM_ADD_QTY
        {
            public string MANEN { get; set; }
        }


        public class ConfigModel
        {
            public string sap_url { get; set; }
            public string sap_user { get; set; }
            public string sap_pass { get; set; }
            public string connect_time_out { get; set; }
        }
    }

}
