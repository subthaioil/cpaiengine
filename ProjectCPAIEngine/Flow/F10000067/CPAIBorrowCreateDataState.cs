﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPCF;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000067
{
    public class CPAIBorrowCreateDataState : BasicBean, StateFlowAction
    {

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIBorrowCreateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    Borrow data = JSonConvertUtil.jsonToModel<Borrow>(item);
                    
                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    bool isRunDB = false;

                    isRunDB = InsertUpdateDB(data);
                    
                    if (isRunDB)
                    {
                            currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }

                    
                    #endregion

                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                //stateModel.BusinessModel.etxValue = etxValue;
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIBorrowCreateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIBorrowCreateDataState # :: Exception >>> " + ex);
                log.Error("CPAIBorrowCreateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertUpdateDB(Borrow data)
        {
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            bool isSuccess = false;
            
            try
            {
                BORROW_DAL dalBorrow = new BORROW_DAL();
                BORROW_CRUDE entBorrowCrude = new BORROW_CRUDE();

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            
                            BorrowDetailModel dataDetailS = new BorrowDetailModel();

                            #region Sale
                            dataDetailS = data.BorrowDetailModelSale;
                            //if ((data.sTripNo != null)&&(dataDetailS.sType !=null))
                            //{
                            if (dataDetailS.sCustomer != null)
                            {
                                var _qry = context.BORROW_CRUDE.Where(z => z.BRC_TRIP_NO.ToUpper().Equals(data.sTripNo.ToUpper())
                                                                        && z.BRC_CRUDE_TYPE_ID.ToUpper().Equals(dataDetailS.sCrude.ToUpper())
                                                                        && z.BRC_TYPE.ToUpper().Equals(dataDetailS.sType.ToUpper())
                                                                        );                   

                                if (_qry.ToList().Count > 0)
                                {
                                    entBorrowCrude = (BORROW_CRUDE)_qry.ToList()[0];
                                }


                                entBorrowCrude.BRC_TRIP_NO = data.sTripNo;
                                entBorrowCrude.BRC_PO_NO = dataDetailS.sPONo;
                                entBorrowCrude.BRC_TANK = dataDetailS.sTank;
                                entBorrowCrude.BRC_CUST_NUM = dataDetailS.sCustomer;
                                entBorrowCrude.BRC_TYPE = dataDetailS.sType;
                                if (String.IsNullOrEmpty(dataDetailS.sDeliveryDate))
                                {
                                    entBorrowCrude.BRC_DELIVERY_DATE = null;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_DELIVERY_DATE = DateTime.ParseExact(dataDetailS.sDeliveryDate, format, provider);
                                }
                                if (String.IsNullOrEmpty(dataDetailS.sDueDate))
                                {
                                    entBorrowCrude.BRC_DUE_DATE = null;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_DUE_DATE = DateTime.ParseExact(dataDetailS.sDueDate, format, provider);
                                }
                                entBorrowCrude.BRC_COMPANY = dataDetailS.sSaleOrg;
                                entBorrowCrude.BRC_PLANT = dataDetailS.sPlant;
                                entBorrowCrude.BRC_CRUDE_TYPE_ID = dataDetailS.sCrude;                               
                                if (String.IsNullOrEmpty(dataDetailS.sVolumeBBL))
                                {
                                    entBorrowCrude.BRC_VOLUME_BBL = 0;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_VOLUME_BBL = Decimal.Parse(dataDetailS.sVolumeBBL);
                                }
                                if (String.IsNullOrEmpty(dataDetailS.sVolumeLitres))
                                {
                                    entBorrowCrude.BRC_VOLUME_LITE = null;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_VOLUME_LITE = Decimal.Parse(dataDetailS.sVolumeLitres);
                                }
                                if (String.IsNullOrEmpty(dataDetailS.sVolumeMT))
                                {
                                    entBorrowCrude.BRC_VOLUME_MT = null;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_VOLUME_MT = Decimal.Parse(dataDetailS.sVolumeMT);
                                }                          
                                entBorrowCrude.BRC_UNIT_TOTAL = dataDetailS.sTotalUnit;
                                if (String.IsNullOrEmpty(dataDetailS.sExchangeRate))
                                {
                                    entBorrowCrude.BRC_EXCHANGE_RATE = null;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_EXCHANGE_RATE = Decimal.Parse(dataDetailS.sExchangeRate);
                                }
                                if (String.IsNullOrEmpty(dataDetailS.sTotalAmount))
                                {
                                    entBorrowCrude.BRC_TOTAL = null;
                                }
                                else
                                {
                                    entBorrowCrude.BRC_TOTAL = Decimal.Parse(dataDetailS.sTotalAmount);
                                }
                                entBorrowCrude.BRC_REMARK = dataDetailS.sRemark;


                                if (_qry.ToList().Count == 0)
                                {
                                    entBorrowCrude.BRC_CREATE_DATE = DateTime.Now.Date;
                                    entBorrowCrude.BRC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");
                                    entBorrowCrude.BRC_CREATE_BY = Const.User.UserName;

                                    dalBorrow.SaveBorrowCrude(entBorrowCrude);
                                    
                                    foreach (var item in dataDetailS.PriceList)
                                    {
                                        BORROW_PRICE entBorrowPrice = new BORROW_PRICE();
                                        entBorrowPrice.BRP_TRIP_NO = data.sTripNo;
                                        
                                        entBorrowPrice.BRP_NUM = Decimal.Parse(item.sNum);
                                        if (String.IsNullOrEmpty(item.sDueDate)) { entBorrowPrice.BRP_DATE_PRICE = null; } else { entBorrowPrice.BRP_DATE_PRICE = DateTime.ParseExact(item.sDueDate, format, provider); }
                                        if (String.IsNullOrEmpty(item.sPrice)) { entBorrowPrice.BRP_PRICE = null; } else { entBorrowPrice.BRP_PRICE = Decimal.Parse(item.sPrice); }
                                        entBorrowPrice.BRP_UNIT_ID = item.sPriceUnit;
                                        if (String.IsNullOrEmpty(item.sInvoice)) { entBorrowPrice.BRP_INVOICE = null; } else { entBorrowPrice.BRP_INVOICE = Decimal.Parse(item.sInvoice); }
                                        entBorrowPrice.BRP_UNIT_INV = item.sInvUnit;

                                        dalBorrow.SaveBorrowPrice(entBorrowPrice);
                                        
                                    }
                                }
                                else
                                {

                                    entBorrowCrude.BRC_LAST_MODIFY_BY = Const.User.UserName;
                                    entBorrowCrude.BRC_LAST_MODIFY_DATE = DateTime.Now.Date;
                                    entBorrowCrude.BRC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");

                                    dalBorrow.UpdateBorrowCrude(entBorrowCrude, context);

                                    foreach (var item in dataDetailS.PriceList.OrderBy(p => p.sPriceType))
                                    {
                                        BORROW_PRICE entBorrowPrice = new BORROW_PRICE();
                                        entBorrowPrice.BRP_TRIP_NO = data.sTripNo;
                                        if (String.IsNullOrEmpty(item.sPrice))
                                        {
                                            entBorrowPrice.BRP_PRICE = null;
                                        }
                                        else
                                        {
                                            entBorrowPrice.BRP_PRICE = Decimal.Parse(item.sPrice);
                                        }
                                        if (String.IsNullOrEmpty(item.sDueDate))
                                        {
                                            entBorrowPrice.BRP_DATE_PRICE = null;
                                        }
                                        else
                                        {
                                            entBorrowPrice.BRP_DATE_PRICE = DateTime.ParseExact(item.sDueDate, format, provider);
                                        }
                                        if (String.IsNullOrEmpty(item.sNum))
                                        {
                                            entBorrowPrice.BRP_NUM = 0;
                                        }
                                        else
                                        {
                                            entBorrowPrice.BRP_NUM = Decimal.Parse(item.sNum ?? "0");
                                        }
                                        entBorrowPrice.BRP_UNIT_ID = item.sPriceUnit;
                                        //entBorrowPrice.CLP_RELEASE = item.sRelease;
                                        if (String.IsNullOrEmpty(item.sInvoice))
                                        {
                                            entBorrowPrice.BRP_INVOICE = 0;
                                        }
                                        else
                                        {
                                            entBorrowPrice.BRP_INVOICE = Decimal.Parse(item.sInvoice ?? "0");
                                        }
                                        //entBorrowPrice.CLP_MEMO = item.sMemo;
                                        entBorrowPrice.BRP_UNIT_INV = item.sInvUnit;

                                        dalBorrow.UpdateBorrowPrice(entBorrowPrice, context);
                                    }

                                }
                               }
                            
                                #endregion







                                #region Purchase
                                dataDetailS = new BorrowDetailModel();                               
                                dataDetailS =  data.BorrowDetailModelPO;

                                if (dataDetailS.sCustomer != null)
                                {

                                var _qry1 = context.BORROW_CRUDE.Where(z => z.BRC_TRIP_NO.ToUpper().Equals(data.sTripNo.ToUpper())
                                                                && z.BRC_CRUDE_TYPE_ID.ToUpper().Equals(dataDetailS.sCrude.ToUpper())
                                                                && z.BRC_TYPE.ToUpper().Equals(dataDetailS.sType.ToUpper())
                                                                );
                            
                                if (_qry1.ToList().Count > 0)
                                    {
                                        entBorrowCrude = (BORROW_CRUDE)_qry1.ToList()[0];
                                    }
                                    //else
                                    //{
                                    //    entBorrowCrude.CLC_TRIP_NO = genTripNo(dataDetailS.sPlant, context);
                                    //}

                                    entBorrowCrude.BRC_TRIP_NO = data.sTripNo;
                                    entBorrowCrude.BRC_PO_NO = dataDetailS.sPONo;
                                    entBorrowCrude.BRC_TANK = dataDetailS.sTank;
                                    entBorrowCrude.BRC_CUST_NUM = dataDetailS.sCustomer;                                 
                                    entBorrowCrude.BRC_TYPE = dataDetailS.sType;

                                    if (String.IsNullOrEmpty(dataDetailS.sDeliveryDate))
                                    {
                                        entBorrowCrude.BRC_DELIVERY_DATE = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_DELIVERY_DATE = DateTime.ParseExact(dataDetailS.sDeliveryDate, format, provider);
                                    }
                                    if (String.IsNullOrEmpty(dataDetailS.sDueDate))
                                    {
                                        entBorrowCrude.BRC_DUE_DATE = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_DUE_DATE = DateTime.ParseExact(dataDetailS.sDueDate, format, provider);
                                    }
                                    entBorrowCrude.BRC_COMPANY = dataDetailS.sSaleOrg;
                                    entBorrowCrude.BRC_PLANT = dataDetailS.sPlant;

                                    if (String.IsNullOrEmpty(dataDetailS.sCrude))
                                    {
                                      entBorrowCrude.BRC_CRUDE_TYPE_ID = data.BorrowDetailModelSale.sCrude;
                                    }
                                    else
                                    {
                                       entBorrowCrude.BRC_CRUDE_TYPE_ID = dataDetailS.sCrude;
                                    }
                                   
                                    // entBorrowCrude.BRC_VESSEL_ID = dataDetailS.sVesselName;
                                    if (String.IsNullOrEmpty(dataDetailS.sVolumeBBL))
                                    {
                                        entBorrowCrude.BRC_VOLUME_BBL = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_VOLUME_BBL = Decimal.Parse(dataDetailS.sVolumeBBL);
                                    }
                                    if (String.IsNullOrEmpty(dataDetailS.sVolumeLitres))
                                    {
                                        entBorrowCrude.BRC_VOLUME_LITE = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_VOLUME_LITE = Decimal.Parse(dataDetailS.sVolumeLitres);
                                    }
                                    if (String.IsNullOrEmpty(dataDetailS.sVolumeMT))
                                    {
                                        entBorrowCrude.BRC_VOLUME_MT = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_VOLUME_MT = Decimal.Parse(dataDetailS.sVolumeMT);
                                    }

                                    entBorrowCrude.BRC_UNIT_TOTAL = dataDetailS.sTotalUnit;
                                    if (String.IsNullOrEmpty(dataDetailS.sExchangeRate))
                                    {
                                        entBorrowCrude.BRC_EXCHANGE_RATE = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_EXCHANGE_RATE = Decimal.Parse(dataDetailS.sExchangeRate);
                                    }
                                    if (String.IsNullOrEmpty(dataDetailS.sTotalAmount))
                                    {
                                        entBorrowCrude.BRC_TOTAL = null;
                                    }
                                    else
                                    {
                                        entBorrowCrude.BRC_TOTAL = Decimal.Parse(dataDetailS.sTotalAmount);
                                    }
                                    entBorrowCrude.BRC_REMARK = dataDetailS.sRemark;


                                    if (_qry1.ToList().Count == 0)
                                    {
                                        entBorrowCrude.BRC_CREATE_DATE = DateTime.Now.Date;
                                        entBorrowCrude.BRC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");
                                        entBorrowCrude.BRC_CREATE_BY = Const.User.UserName;

                                        dalBorrow.SaveBorrowCrude(entBorrowCrude);
                                       
                                        foreach (var item in dataDetailS.PriceList)
                                        {
                                            BORROW_PRICE entBorrowPrice = new BORROW_PRICE();
                                            entBorrowPrice.BRP_TRIP_NO = data.sTripNo;
                                            
                                            entBorrowPrice.BRP_NUM = Decimal.Parse(item.sNum);
                                            if (String.IsNullOrEmpty(item.sDueDate)) { entBorrowPrice.BRP_DATE_PRICE = null; } else { entBorrowPrice.BRP_DATE_PRICE = DateTime.ParseExact(item.sDueDate, format, provider); }
                                            if (String.IsNullOrEmpty(item.sPrice)) { entBorrowPrice.BRP_PRICE = null; } else { entBorrowPrice.BRP_PRICE = Decimal.Parse(item.sPrice); }
                                            entBorrowPrice.BRP_UNIT_ID = item.sPriceUnit;
                                            if (String.IsNullOrEmpty(item.sInvoice)) { entBorrowPrice.BRP_INVOICE = null; } else { entBorrowPrice.BRP_INVOICE = Decimal.Parse(item.sInvoice); }
                                            entBorrowPrice.BRP_UNIT_INV = item.sInvUnit;

                                            dalBorrow.SaveBorrowPrice(entBorrowPrice);
                                       
                                        }
                                    }
                                    else
                                    {

                                        entBorrowCrude.BRC_LAST_MODIFY_BY = Const.User.UserName;
                                        entBorrowCrude.BRC_LAST_MODIFY_DATE = DateTime.Now.Date;
                                        entBorrowCrude.BRC_CREATE_TIME = DateTime.Now.ToString("HH:mm:ss");

                                        dalBorrow.UpdateBorrowCrude(entBorrowCrude, context);


                                        foreach (var item in dataDetailS.PriceList)
                                        {
                                            BORROW_PRICE entBorrowPrice = new BORROW_PRICE();
                                            entBorrowPrice.BRP_TRIP_NO = data.sTripNo;
                                            if (String.IsNullOrEmpty(item.sPrice))
                                            {
                                                entBorrowPrice.BRP_PRICE = null;
                                            }
                                            else
                                            {
                                                entBorrowPrice.BRP_PRICE = Decimal.Parse(item.sPrice);
                                            }
                                            if (String.IsNullOrEmpty(item.sDueDate))
                                            {
                                                entBorrowPrice.BRP_DATE_PRICE = null;
                                            }
                                            else
                                            {
                                                entBorrowPrice.BRP_DATE_PRICE = DateTime.ParseExact(item.sDueDate, format, provider);
                                            }
                                            if (String.IsNullOrEmpty(item.sNum))
                                            {
                                                entBorrowPrice.BRP_NUM = 0;
                                            }
                                            else
                                            {
                                                entBorrowPrice.BRP_NUM = Decimal.Parse(item.sNum ?? "0");
                                            }
                                            entBorrowPrice.BRP_UNIT_ID = item.sPriceUnit;
                                            //entBorrowPrice.CLP_RELEASE = item.sRelease;
                                            if (String.IsNullOrEmpty(item.sInvoice))
                                            {
                                                entBorrowPrice.BRP_INVOICE = 0;
                                            }
                                            else
                                            {
                                                entBorrowPrice.BRP_INVOICE = Decimal.Parse(item.sInvoice ?? "0");
                                            }
                                            //entBorrowPrice.CLP_MEMO = item.sMemo;
                                            entBorrowPrice.BRP_UNIT_INV = item.sInvUnit;

                                            dalBorrow.UpdateBorrowPrice(entBorrowPrice, context);
                                        }

                                    }
                                }
                                #endregion



                                dbContextTransaction.Commit();
                                isSuccess = true;
                            }
                        catch (Exception ex)
                        {
                            String innerMessage = (ex.InnerException != null)
                              ? ex.InnerException.Message
                              : "";
                            string res = ex.Message;
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("Exception : " + innerMessage + " : " + res);
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

        private string genTripNo(string com_code, EntityCPAIEngine context)
        {
            string ret = "BO";
            com_code = com_code.Substring(0, 2);
            string yy = DateTime.Now.ToString("yyMM", new System.Globalization.CultureInfo("en-US"));

            string TripPrefix = ret + com_code + yy;
            try
            {
                var query = (from v in context.BORROW_CRUDE
                             where v.BRC_TRIP_NO.Contains(TripPrefix)
                             orderby v.BRC_TRIP_NO descending
                             select v.BRC_TRIP_NO).ToList();


                if (query != null && query.Count() > 0)
                {
                    string lastTrip = query[0];
                    if (lastTrip != "")
                        lastTrip = lastTrip.Replace(TripPrefix, "");

                    string nextTrip = (int.Parse(lastTrip) + 1).ToString().PadLeft(2,'0');

                    ret = TripPrefix + nextTrip;
                }
                else
                {
                    ret = TripPrefix + "01";
                }
                
            }
            catch (Exception ex) { }

            return ret;
        }
    }
}