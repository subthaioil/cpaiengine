﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using System.DirectoryServices;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000067
{
    public class CPAIVerifyBorrowRequireInputState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyBorrowRequireInputState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_SYSTEM_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.TripNo)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_TRIPNO_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Channel))) //WEB
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CHANNEL_RESP_CODE;
                else
                {
                    //if (action.Equals(CPAIConstantUtil.ACTION_DRAFT) || action.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                    //{
                    if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.DataDetailInput)))
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                    }
                    else
                    {
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    //}
                }

                //get ip address
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());

                

                //map response code to response description

                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyBorrowRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIVerifyBorrowRequireInputState # :: Exception >>>  " + tem);
                log.Error("# Error CPAIVerifyBorrowRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyBorrowRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
