﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALHedg;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000070
{
    public class CPAIHedgingAnnualUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingAnnualUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    HedgingRootObject dataDetail = JSonConvertUtil.jsonToModel<HedgingRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    bool isRunDB = false;
                    if (currentAction.Contains(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Contains(CPAIConstantUtil.ACTION_SUBMIT))
                    {
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                        {
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, ref dataDetail);
                        }
                        else
                        {
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, ref dataDetail, true);
                        }
                    }
                    #endregion


                    //check db by next status
                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                        #region Set dataDetail
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                        {
                            string create_by = etxValue.GetValue(CPAIConstantUtil.User);
                            etxValue.SetValue(CPAIConstantUtil.CreateBy, new ExtendValue { value = create_by, encryptFlag = "N" });
                            string purchase_no = dataDetail.hedg_annual_submit_note.ref_no != null ? dataDetail.hedg_annual_submit_note.ref_no : "";
                            etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = purchase_no, encryptFlag = "N" });
                        }

                        string company = dataDetail.hedg_annual_data.trading_book != null ? dataDetail.hedg_annual_data.trading_book : "";
                        string approved_date = dataDetail.hedg_annual_data.approved_date != null ? dataDetail.hedg_annual_data.approved_date : "";
                        string version = dataDetail.hedg_annual_data.version != null ? dataDetail.hedg_annual_data.version : "";
                        string activation_status = dataDetail.hedg_annual_data.status;
                        string updated_date = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        string updated_by = etxValue.GetValue(CPAIConstantUtil.User);
                        string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                        //set data
                        etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Company, new ExtendValue { value = company, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Approved_Date, new ExtendValue { value = approved_date, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Version, new ExtendValue { value = version, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Activation_Status, new ExtendValue { value = activation_status, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Updated_Date, new ExtendValue { value = updated_date, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Updated_By, new ExtendValue { value = updated_by, encryptFlag = "N" });
                        //set status
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        //set action
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });

                        #endregion

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingAnnualUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingAnnualUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingAnnualUpdateDataState::Exception >>> ", ex);
                //log.Error("CPAIUpdateDataState::Exception >>> ", e);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, ref HedgingRootObject dataDetail, bool hasRef = false)
        {
            log.Info("# Start State CPAIHedgingAnnualUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
            dataDetail.hedg_annual_data.status = currentAction.Contains(CPAIConstantUtil.ACTION_DRAFT) ? "DRAFT" : "ACTIVE";

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region HEDG_ANNUAL_FW
                            HEDG_ANNUAL_FW_DAL dataDal = new HEDG_ANNUAL_FW_DAL();
                            HEDG_ANNUAL_FW data = dataDal.GetByID(TransID);
                            if (data == null)
                            {
                                data = new HEDG_ANNUAL_FW();
                            }
                            HEDG_ANNUAL_FW old_data = new HEDG_ANNUAL_FW();
                            string tranID = etxValue.GetValue(CPAIConstantUtil.Note).Replace("-", "");

                            if (!hasRef)
                            {
                                dataDal.Delete(TransID, context);
                                if (!string.IsNullOrEmpty(tranID))
                                {
                                    dataDal.Delete(tranID, context);
                                }
                                data.HAF_STATUS = dataDetail.hedg_annual_data.status;
                                dataDetail.hedg_annual_data.version = "1";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(data.HAF_HISTORY_REF))
                                {
                                    tranID = data.HAF_HISTORY_REF;
                                }

                                old_data = dataDal.GetByID(tranID);
                                dataDetail.hedg_annual_data.version = old_data.HAF_VERSION;

                                if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT_2))
                                {
                                    FunctionTransactionDAL funDal = new FunctionTransactionDAL();

                                    if (old_data.HAF_STATUS != CPAIConstantUtil.STATUS_DRAFT)
                                    {
                                        funDal.UpdateHedgActivationStatus(tranID, CPAIConstantUtil.INACTIVE);
                                        old_data.HAF_STATUS = ConstantPrm.ACTION.INACTIVE;
                                        dataDal.Update(old_data, context);
                                        dataDal.Delete(TransID, context);
                                        dataDetail.hedg_annual_data.version = (Convert.ToDecimal(old_data.HAF_VERSION) + 1).ToString();
                                    }
                                    data.HAF_HISTORY_REF = tranID;
                                    data.HAF_STATUS = CPAIConstantUtil.ACTIVE;
                                } else
                                {
                                    if (data.HAF_STATUS == CPAIConstantUtil.STATUS_DRAFT)
                                    {
                                        dataDal.Delete(TransID, context);
                                    }
                                    dataDetail.hedg_annual_data.version = (Convert.ToDecimal(old_data.HAF_VERSION) + 1).ToString();
                                    data.HAF_HISTORY_REF = tranID;
                                    data.HAF_STATUS = CPAIConstantUtil.STATUS_DRAFT;
                                }
                            }

                            data.HAF_ROW_ID = TransID;
                            data.HAF_FK_MT_COMPANY = dataDetail.hedg_annual_data.trading_book;
                            if (!string.IsNullOrEmpty(dataDetail.hedg_annual_data.approved_date))
                                data.HAF_APPROVED_DATE = DateTime.ParseExact(dataDetail.hedg_annual_data.approved_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            data.HAF_NOTE = dataDetail.hedg_annual_note;
                            data.HAF_VERSION = dataDetail.hedg_annual_data.version;
                            data.HAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.HAF_CREATED_DATE = dtNow;
                            data.HAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.HAF_UPDATED_DATE = dtNow;
                            dataDal.Save(data, context);
                            #endregion

                            #region HEDG_ANNUAL_HEDGE_TYPE
                            HEDG_ANNUAL_FW_HEDGE_TYPE_DAL dataHTDal = new HEDG_ANNUAL_FW_HEDGE_TYPE_DAL();
                            if (dataDetail.hedg_annual_type != null && dataDetail.hedg_annual_type.hedg_type_data != null)
                            {
                                for (int i = 0; i < dataDetail.hedg_annual_type.hedg_type_data.Count; i++)
                                {
                                    HEDG_ANNUAL_FW_HEDGE_TYPE dataHT = new HEDG_ANNUAL_FW_HEDGE_TYPE();
                                    dataHT.HAFH_ROW_ID = string.Format("{0}{1}", ConstantPrm.GetDynamicCtrlID(), i);
                                    if (dataHT.HAFH_ROW_ID.Length > 32) { dataHT.HAFH_ROW_ID = dataHT.HAFH_ROW_ID.Substring(dataHT.HAFH_ROW_ID.Length - 32, 32);}
                                    dataHT.HAFH_ORDER = (i + 1).ToString();
                                    dataHT.HAFH_FK_HEDG_ANNUAL_FW = TransID;
                                    dataHT.HAFH_FK_HEDG_MT_HEDGE_TYPE = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_key;
                                    dataHT.HAFH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataHT.HAFH_CREATED_DATE = dtNow;
                                    dataHT.HAFH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataHT.HAFH_UPDATED_DATE = dtNow;
                                    dataHTDal.Save(dataHT, context);

                                    if (dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail != null)
                                    {
                                        HEDG_ANNUAL_FW_DETAIL_DAL dataFDDal = new HEDG_ANNUAL_FW_DETAIL_DAL();
                                        for (int j = 0; j < dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail.Count; j++)
                                        {
                                            HEDG_ANNUAL_FW_DETAIL dataFD = new HEDG_ANNUAL_FW_DETAIL();
                                            dataFD.HAFD_ROW_ID = string.Format("{0}{1}{2}", ConstantPrm.GetDynamicCtrlID(), i, j);
                                            if (dataFD.HAFD_ROW_ID.Length > 32) { dataFD.HAFD_ROW_ID = dataFD.HAFD_ROW_ID.Substring(dataFD.HAFD_ROW_ID.Length - 32, 32); }
                                            dataFD.HAFD_ORDER = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].order;
                                            dataFD.HAFD_FK_ANNUAL_HEDGE_TYPE = dataHT.HAFH_ROW_ID;
                                            dataFD.HAFD_FK_HEDG_PRODUCT_FRONT = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].product_front;
                                            dataFD.HAFD_FK_HEDG_PRODUCT_BACK = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].product_back;
                                            if (!string.IsNullOrEmpty(dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].key))
                                            {
                                                HEDG_ANNUAL_FW_DETAIL tempFD = dataFDDal.GetByID(dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].key);
                                                decimal version = Convert.ToDecimal(tempFD.HAFD_VERSION);
                                                string this_key = tempFD.HAFD_ROW_ID;
                                                if (tempFD != null)
                                                {
                                                    #region Check Alternation
                                                    bool isChange = false;
                                                    if (tempFD.HAFD_APPROVED_DATE.HasValue && tempFD.HAFD_APPROVED_DATE.Value.ToString("dd/MM/yyyy") != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_date)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_APPROVED_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_volume)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_FK_HEDG_PRODUCT_BACK != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].product_back)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_FK_HEDG_PRODUCT_FRONT != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].product_front)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_LIMIT_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].limit_volume)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_MIN_MAX != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].min_max)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_NOTE != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].note)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_PRICE_Q1 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q1)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_PRICE_Q2 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q2)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_PRICE_Q3 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q3)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_PRICE_Q4 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q4)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_PRICE_YEAR != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_year)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_PRODUCTION_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].prod_volume)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_TITLE != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].title)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_UNIT_PRICE != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].unit_price)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_UNIT_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].unit_volume)
                                                    {
                                                        isChange = true;
                                                    }

                                                    if (tempFD.HAFD_YEAR != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].year)
                                                    {
                                                        isChange = true;
                                                    }

                                                    #endregion

                                                    dataFD.HAFD_VERSION = tempFD.HAFD_VERSION;

                                                    HEDG_ANNUAL_FW_DETAIL tempFD2 = dataFDDal.GetByID(tempFD.HAFD_HISTORY_REF);
                                                    if (tempFD2 != null)
                                                    {
                                                        if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT_2))
                                                        {
                                                            version = Convert.ToDecimal(tempFD2.HAFD_VERSION);
                                                            if (tempFD2.HAFD_VERSION == tempFD.HAFD_VERSION)
                                                            {
                                                                version -= 1;
                                                            }
                                                        }

                                                        dataFD.HAFD_VERSION = (version + 1).ToString();

                                                        if (tempFD2.HAFD_VERSION == tempFD.HAFD_VERSION)
                                                        {
                                                            this_key = tempFD2.HAFD_ROW_ID;
                                                            isChange = false;
                                                            if (tempFD2.HAFD_APPROVED_DATE.HasValue && tempFD2.HAFD_APPROVED_DATE.Value.ToString("dd/MM/yyyy") != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_date)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_APPROVED_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_volume)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_FK_HEDG_PRODUCT_BACK != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].product_back)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_FK_HEDG_PRODUCT_FRONT != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].product_front)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_LIMIT_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].limit_volume)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_MIN_MAX != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].min_max)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_NOTE != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].note)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_PRICE_Q1 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q1)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_PRICE_Q2 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q2)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_PRICE_Q3 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q3)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_PRICE_Q4 != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q4)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_PRICE_YEAR != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_year)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_PRODUCTION_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].prod_volume)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_TITLE != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].title)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_UNIT_PRICE != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].unit_price)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_UNIT_VOLUME != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].unit_volume)
                                                            {
                                                                isChange = true;
                                                            }

                                                            if (tempFD2.HAFD_YEAR != dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].year)
                                                            {
                                                                isChange = true;
                                                            }
                                                        }
                                                    }

                                                    if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT_2))
                                                    {
                                                        //update hedge type fk and continue if there is no change.
                                                        if (!isChange)
                                                        {
                                                            HEDG_ANNUAL_FW_DETAIL fwd = new HEDG_ANNUAL_FW_DETAIL();
                                                            fwd.HAFD_ROW_ID = this_key;
                                                            fwd.HAFD_FK_ANNUAL_HEDGE_TYPE = dataHT.HAFH_ROW_ID;
                                                            fwd.HAFD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                            fwd.HAFD_UPDATED_DATE = dtNow;

                                                            dataFDDal.UpdateHedgeType(fwd, context);
                                                            continue;
                                                        }
                                                    }

                                                    if (tempFD2 != null && tempFD2.HAFD_VERSION == tempFD.HAFD_VERSION)
                                                    {
                                                        dataFD.HAFD_HISTORY_REF = tempFD.HAFD_HISTORY_REF;
                                                    }
                                                    else
                                                    {
                                                        if (dataDetail.hedg_annual_data.version != "1")
                                                        {
                                                            dataFD.HAFD_HISTORY_REF = tempFD.HAFD_ROW_ID;
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    dataFD.HAFD_VERSION = "1";
                                                }
                                            }
                                            else
                                                dataFD.HAFD_VERSION = "1";
                                            dataFD.HAFD_TITLE = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].title;
                                            dataFD.HAFD_UNIT_PRICE = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].unit_price;
                                            dataFD.HAFD_UNIT_VOLUME = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].unit_volume;
                                            dataFD.HAFD_YEAR = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].year;
                                            dataFD.HAFD_PRICE_YEAR = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_year;
                                            dataFD.HAFD_PRICE_Q1 = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q1;
                                            dataFD.HAFD_PRICE_Q2 = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q2;
                                            dataFD.HAFD_PRICE_Q3 = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q3;
                                            dataFD.HAFD_PRICE_Q4 = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].price_Q4;
                                            dataFD.HAFD_PRODUCTION_VOLUME = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].prod_volume;
                                            dataFD.HAFD_LIMIT_VOLUME = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].limit_volume;
                                            dataFD.HAFD_APPROVED_VOLUME = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_volume;
                                            dataFD.HAFD_NOTE = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].note;
                                            dataFD.HAFD_MIN_MAX = dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].min_max;
                                            if (!string.IsNullOrEmpty(dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_date))
                                                dataFD.HAFD_APPROVED_DATE = DateTime.ParseExact(dataDetail.hedg_annual_type.hedg_type_data[i].hedg_type_detail[j].approved_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                            dataFD.HAFD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataFD.HAFD_CREATED_DATE = dtNow;
                                            dataFD.HAFD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataFD.HAFD_UPDATED_DATE = dtNow;
                                            dataFDDal.Save(dataFD, context);
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region HEDG_ANNUAL_ATTACH_FILE

                            HEDG_ANNUAL_ATTACH_FILE_DAL dataAttachFileDAL = new HEDG_ANNUAL_ATTACH_FILE_DAL();
                            HEDG_ANNUAL_ATTACH_FILE dataHAAF;
                            if (!string.IsNullOrEmpty(dataDetail.attach_file))
                            {
                                foreach (var item in dataDetail.attach_file.Split('|'))
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataHAAF = new HEDG_ANNUAL_ATTACH_FILE();
                                        dataHAAF.HAAF_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        dataHAAF.HAAF_FK_HEDG_ANNUAL = TransID;
                                        dataHAAF.HAAF_PATH = item;
                                        dataHAAF.HAAF_INFO = "";
                                        dataHAAF.HAAF_TYPE = "EXT";
                                        dataHAAF.HAAF_CREATED = dtNow;
                                        dataHAAF.HAAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataHAAF.HAAF_UPDATED = dtNow;
                                        dataHAAF.HAAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataHAAF, context);
                                    }
                                }
                            }
                            #endregion

                            #region HEDG_ANNUAL_SUBMIT_NOTE
                            HEDG_ANNUAL_SUBMIT_NOTE_DAL dataSNDal = new HEDG_ANNUAL_SUBMIT_NOTE_DAL();
                            if (dataDetail.hedg_annual_submit_note != null)
                            {
                                HEDG_ANNUAL_SUBMIT_NOTE dataSN = new HEDG_ANNUAL_SUBMIT_NOTE();
                                dataSN.HASN_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                dataSN.HASN_FK_HEDG_ANNUAL = TransID;
                                dataSN.HASN_SUBJECT = dataDetail.hedg_annual_submit_note.subject;
                                if (!string.IsNullOrEmpty(dataDetail.hedg_annual_submit_note.approved_date))
                                    dataSN.HASN_APPROVED_DATE = DateTime.ParseExact(dataDetail.hedg_annual_submit_note.approved_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataSN.HASN_REF_NO = dataDetail.hedg_annual_submit_note.ref_no;
                                dataSN.HASN_NOTE = dataDetail.hedg_annual_submit_note.note;
                                dataSN.HASN_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataSN.HASN_CREATED_DATE = dtNow;
                                dataSN.HASN_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataSN.HASN_UPDATED_DATE = dtNow;
                                dataSNDal.Save(dataSN, context);

                                #region HEDG_ANNUAL_SN_ATTACH_FILE

                                HEDG_ANNUAL_SN_ATTACH_FILE_DAL dataSNAttachFileDAL = new HEDG_ANNUAL_SN_ATTACH_FILE_DAL();
                                HEDG_ANNUAL_SN_ATTACH_FILE dataHASF;

                                if (!string.IsNullOrEmpty(dataDetail.hedg_annual_submit_note.attach_file))
                                {
                                    foreach (var item in dataDetail.hedg_annual_submit_note.attach_file.Split('|'))
                                    {
                                        if (!string.IsNullOrEmpty(item))
                                        {
                                            dataHASF = new HEDG_ANNUAL_SN_ATTACH_FILE();
                                            dataHASF.HASF_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataHASF.HASF_FK_HEDG_ANNUAL_SN = dataSN.HASN_ROW_ID;
                                            dataHASF.HASF_PATH = item;
                                            dataHASF.HASF_INFO = "";
                                            dataHASF.HASF_TYPE = "EXT";
                                            dataHASF.HASF_CREATED_DATE = dtNow;
                                            dataHASF.HASF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataHASF.HASF_UPDATED_DATE = dtNow;
                                            dataHASF.HASF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataSNAttachFileDAL.Save(dataHASF, context);
                                        }
                                    }
                                }

                                #endregion

                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {                
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAIHedgingAnnualUpdateDataState >> InsertDB # ");


            return isSuccess;
        }


    }
}