﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000072
{
    public class CPAIVerifyGetBtnHedgingSTRCMTRequireInputState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyGetBtnHedgingSTRCMTRequireInputState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Channel)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CHANNEL_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.TransactionId)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_TRANSACTION_ID_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserGroup)))
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_SYSTEM_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Type)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_TYPE_RESP_CODE;
                else
                {
                    //set user group
                    ExtendValue extStatus = new ExtendValue();
                    UserGroupDAL service = new UserGroupDAL();
                    string r = service.findByUserAndSystems(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    CPAI_USER_GROUP dlg = service.findByUserAndSystemDelegateVCool(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));

                    if (r != null || dlg != null)
                    {
                        if (r != null)
                        {
                            extStatus.value = r;
                            etxValue.SetValue(CPAIConstantUtil.UserGroup, extStatus);
                        }

                        if (dlg != null && dlg.USG_USER_GROUP != null)
                        {
                            extStatus = new ExtendValue();
                            extStatus.value = dlg.USG_USER_GROUP.ToUpper();
                            etxValue.Add(CPAIConstantUtil.UserGroupDelegate, extStatus);
                        }
                        //
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                    }
                }

                //get ip address
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyGetBtnHedgingSTRCMTRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIVerifyGetBtnHedgingSTRCMTRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyGetBtnHedgingSTRCMTRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}