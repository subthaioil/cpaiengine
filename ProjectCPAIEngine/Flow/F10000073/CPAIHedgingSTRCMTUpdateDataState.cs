﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALHedg;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000073
{
    public class CPAIHedgingSTRCMTUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingSTRCMTUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    HedgingRootObject dataDetail = JSonConvertUtil.jsonToModel<HedgingRootObject>(item);

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Insert Update DB
                    bool isRunDB = false;
                    if (currentAction.Contains(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Contains(CPAIConstantUtil.ACTION_SUBMIT))
                    {
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                        {
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, ref dataDetail);
                        }
                        else
                        {
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, ref dataDetail, true);
                        }
                    }
                    #endregion


                    //check db by next status
                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                        #region Set dataDetail
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                        {
                            string create_by = etxValue.GetValue(CPAIConstantUtil.User);
                            etxValue.SetValue(CPAIConstantUtil.CreateBy, new ExtendValue { value = create_by, encryptFlag = "N" });
                            string purchase_no = dataDetail.hedg_STRCMT_submit_note.ref_no != null ? dataDetail.hedg_STRCMT_submit_note.ref_no : "";
                            etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = purchase_no, encryptFlag = "N" });
                        }

                        string company = dataDetail.hedg_STRCMT_data.trading_book != null ? dataDetail.hedg_annual_data.trading_book : "";
                        string approved_date = dataDetail.hedg_STRCMT_data.approved_date != null ? dataDetail.hedg_annual_data.approved_date : "";
                        string version = dataDetail.hedg_STRCMT_data.version != null ? dataDetail.hedg_annual_data.version : "";
                        string activation_status = dataDetail.hedg_STRCMT_data.status;
                        string updated_date = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                        string updated_by = etxValue.GetValue(CPAIConstantUtil.User);
                        string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                        string package_name = "";
                        if (dataDetail.hedg_STRCMT_main_choice != null && dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail != null)
                        {
                            package_name = String.Join(", ", dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail.Select(x => x.package_name));
                        }

                        string instrument = "";
                        if (dataDetail.hedg_STRCMT_tool != null && dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail != null)
                        {
                            instrument = String.Join(", ", dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail.Select(x => x.name));
                        }

                        string hedge_type = dataDetail.hedg_STRCMT_data.hedge_type;
                        string tenor_from = dataDetail.hedg_STRCMT_data.tenor_period_from;
                        string tenor_to = dataDetail.hedg_STRCMT_data.tenor_period_to;


                        //set data
                        etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Company, new ExtendValue { value = company, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Approved_Date, new ExtendValue { value = approved_date, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Version, new ExtendValue { value = version, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Activation_Status, new ExtendValue { value = activation_status, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Updated_Date, new ExtendValue { value = updated_date, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Updated_By, new ExtendValue { value = updated_by, encryptFlag = "N" });

                        etxValue.SetValue(CPAIConstantUtil.Hedg_Package_Name, new ExtendValue { value = package_name, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Hedge_Type, new ExtendValue { value = hedge_type, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Instrument, new ExtendValue { value = instrument, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Tenor_From, new ExtendValue { value = tenor_from, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Tenor_To, new ExtendValue { value = tenor_to, encryptFlag = "N" });

                        //set status
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        //set action
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });

                        #endregion

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingSTRCMTUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingSTRCMTUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingSTRCMTUpdateDataState::Exception >>> ", ex);
                //log.Error("CPAIUpdateDataState::Exception >>> ", e);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, ref HedgingRootObject dataDetail, bool hasRef = false)
        {
            log.Info("# Start State CPAIHedgingSTRCMTUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
            dataDetail.hedg_STRCMT_data.status = currentAction.Contains(CPAIConstantUtil.ACTION_DRAFT) ? "DRAFT" : "ACTIVE";

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            #region HEDG_STR_CMT_FW
                            HEDG_STR_CMT_FW_DAL dataDal = new HEDG_STR_CMT_FW_DAL();
                            HEDG_STR_CMT_FW data = new HEDG_STR_CMT_FW();
                            if (data == null)
                            {
                                data = new HEDG_STR_CMT_FW();
                            }
                            HEDG_STR_CMT_FW old_data = new HEDG_STR_CMT_FW();
                            string tranID = etxValue.GetValue(CPAIConstantUtil.Note).Replace("-", "");

                            if (!hasRef)
                            {
                                dataDal.Delete(TransID, context);
                                if (!string.IsNullOrEmpty(tranID))
                                {
                                    dataDal.Delete(tranID, context);
                                }
                                data.HSCF_STATUS = dataDetail.hedg_STRCMT_data.status;
                                dataDetail.hedg_STRCMT_data.version = "1";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(data.HSCF_HISTORY_REF))
                                {
                                    tranID = data.HSCF_HISTORY_REF;
                                }

                                old_data = dataDal.GetByID(tranID);
                                dataDetail.hedg_STRCMT_data.version = old_data.HSCF_VERSION;

                                if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT_2))
                                {
                                    FunctionTransactionDAL funDal = new FunctionTransactionDAL();

                                    if (old_data.HSCF_STATUS != CPAIConstantUtil.STATUS_DRAFT)
                                    {
                                        funDal.UpdateHedgActivationStatus(tranID, CPAIConstantUtil.INACTIVE);
                                        old_data.HSCF_STATUS = ConstantPrm.ACTION.INACTIVE;
                                        dataDal.Update(old_data, context);
                                        dataDal.Delete(TransID, context);
                                        dataDetail.hedg_STRCMT_data.version = (Convert.ToDecimal(old_data.HSCF_VERSION) + 1).ToString();
                                    }
                                    data.HSCF_HISTORY_REF = tranID;
                                    data.HSCF_STATUS = CPAIConstantUtil.ACTIVE;
                                }
                                else
                                {
                                    if (data.HSCF_STATUS == CPAIConstantUtil.STATUS_DRAFT)
                                    {
                                        dataDal.Delete(TransID, context);
                                    }
                                    dataDetail.hedg_STRCMT_data.version = (Convert.ToDecimal(old_data.HSCF_VERSION) + 1).ToString();
                                    data.HSCF_HISTORY_REF = tranID;
                                    data.HSCF_STATUS = CPAIConstantUtil.STATUS_DRAFT;
                                }
                            }

                            data.HSCF_ROW_ID = TransID;
                            data.HSCF_FK_MT_COMPANY = dataDetail.hedg_STRCMT_data.trading_book;
                            data.HSCF_FK_HEDG_MT_HEDGE_TYPE = dataDetail.hedg_STRCMT_data.hedge_type;
                            if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_data.approved_date))
                                data.HSCF_APPROVED_DATE = DateTime.ParseExact(dataDetail.hedg_STRCMT_data.approved_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            data.HSCF_CONSECUTIVE_FLAG = dataDetail.hedg_STRCMT_data.consecutive_flag;
                            data.HSCF_M1_M_FLAG = dataDetail.hedg_STRCMT_data.m1_m_flag;
                            data.HSCF_NOTE = dataDetail.hedg_str_cmt_note;
                            data.HSCF_REF_NO = dataDetail.hedg_STRCMT_data.ref_no;
                            data.HSCF_STATUS = dataDetail.hedg_STRCMT_data.status;
                            data.HSCF_TEMPLATE = dataDetail.hedg_STRCMT_data.template;
                            //if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_data.tenor_period_from))
                            //    data.HSCF_TENOR_PERIOD_FROM = DateTime.ParseExact(dataDetail.hedg_STRCMT_data.tenor_period_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            //if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_data.tenor_period_to))
                            //    data.HSCF_TENOR_PERIOD_TO = DateTime.ParseExact(dataDetail.hedg_STRCMT_data.tenor_period_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            data.HSCF_TIME_SPREAD_FLAG = dataDetail.hedg_STRCMT_data.time_spread;
                            data.HSCF_VERSION = dataDetail.hedg_STRCMT_data.version;
                            data.HSCF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.HSCF_CREATED_DATE = dtNow;
                            data.HSCF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.HSCF_UPDATED_DATE = dtNow;
                            dataDal.Save(data, context);
                            #endregion

                            #region HEDG_STR_CHOICE_TYPE
                            HEDG_STR_CHOICE_TYPE_DAL dataCTDal = new HEDG_STR_CHOICE_TYPE_DAL();

                            #region MAIN_CHOICE
                            if (dataDetail.hedg_STRCMT_main_choice != null)
                            {
                                HEDG_STR_CHOICE_TYPE dataCT = new HEDG_STR_CHOICE_TYPE();
                                dataCT.HSCT_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                dataCT.HSCT_FK_HEDG_STR_CMT_FW = TransID;
                                dataCT.HSCT_TYPE = CPAIConstantUtil.HEDG_CONST_MAIN;
                                dataCT.HSCT_UNIT_PRICE = dataDetail.hedg_STRCMT_main_choice.unit;
                                dataCT.HSCT_VOLUME = dataDetail.hedg_STRCMT_main_choice.main_volume;
                                dataCT.HSCT_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCT.HSCT_CREATED_DATE = dtNow;
                                dataCT.HSCT_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCT.HSCT_UPDATED_DATE = dtNow;
                                dataCTDal.Save(dataCT, context);

                                if (dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail != null)
                                {
                                    HEDG_STR_CHOICE_DETAIL_DAL dataCDDal = new HEDG_STR_CHOICE_DETAIL_DAL();
                                    HEDG_STR_CHOICE_ROLE_DAL dataCRDal = new HEDG_STR_CHOICE_ROLE_DAL();
                                    HEDG_STR_CHOICE_PRODUCT_DAL dataCPDal = new HEDG_STR_CHOICE_PRODUCT_DAL();

                                    for (int i = 0; i < dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail.Count; i++)
                                    {
                                        HEDG_STR_CHOICE_DETAIL dataCD = new HEDG_STR_CHOICE_DETAIL();
                                        dataCD.HSCD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        dataCD.HSCD_FK_HEDG_STR_CHOICE_TYPE = dataCT.HSCT_ROW_ID;
                                        dataCD.HSCD_CHOICE_NAME = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].name;
                                        dataCD.HSCD_NET_AF_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].net_af_price;
                                        dataCD.HSCD_OPERATION = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].operation;
                                        dataCD.HSCD_PACKAGE_NAME = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].package_name;
                                        dataCD.HSCD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCD.HSCD_CREATED_DATE = dtNow;
                                        dataCD.HSCD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCD.HSCD_UPDATED_DATE = dtNow;
                                        dataCDDal.Save(dataCD, context);

                                        //subject = primary
                                        if (dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct != null)
                                        {
                                            HEDG_STR_CHOICE_ROLE dataCR = new HEDG_STR_CHOICE_ROLE();
                                            dataCR.HSCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataCR.HSCR_FK_HEDG_STR_CHOICE_DETAIL = dataCD.HSCD_ROW_ID;
                                            dataCR.HSCR_ROLE = CPAIConstantUtil.HEDG_CONST_SUBJECT;
                                            dataCR.HSCR_AVG_AF_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.avg_af_price;
                                            dataCR.HSCR_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_CREATED_DATE = dtNow;
                                            dataCR.HSCR_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_UPDATED_DATE = dtNow;
                                            dataCRDal.Save(dataCR, context);

                                            if (dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail != null)
                                            {                                               

                                                for (int j = 0; j < dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail.Count; j++)
                                                {
                                                    HEDG_STR_CHOICE_PRODUCT dataCP = new HEDG_STR_CHOICE_PRODUCT();
                                                    dataCP.HSCP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                    dataCP.HSCP_FK_HEDG_STR_CHOICE_ROLE = dataCR.HSCR_ROW_ID;
                                                    dataCP.HSCP_ORDER = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].order;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_FRONT = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_BACK = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_AF_FRAME = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].af_frame;
                                                    dataCP.HSCP_WEIGHT_PERCENT = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].weight_percent;
                                                    dataCP.HSCP_WEIGHT_VOLUME = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].weight_volume;
                                                    dataCP.HSCP_AF_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].af_price;
                                                    dataCP.HSCP_UNIT_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].price_unit;
                                                    //dataCP.HSCP_TYPE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].type;
                                                    dataCP.HSCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_CREATED_DATE = dtNow;
                                                    dataCP.HSCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_UPDATED_DATE = dtNow;
                                                    dataCPDal.Save(dataCP, context);

                                                }
                                            }                           
                                        }

                                        //object = secondary
                                        if (dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct != null)
                                        {
                                            HEDG_STR_CHOICE_ROLE dataCR = new HEDG_STR_CHOICE_ROLE();
                                            dataCR.HSCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataCR.HSCR_FK_HEDG_STR_CHOICE_DETAIL = dataCD.HSCD_ROW_ID;
                                            dataCR.HSCR_ROLE = CPAIConstantUtil.HEDG_CONST_OBJECT;
                                            dataCR.HSCR_AVG_AF_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.avg_af_price;
                                            dataCR.HSCR_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_CREATED_DATE = dtNow;
                                            dataCR.HSCR_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_UPDATED_DATE = dtNow;
                                            dataCRDal.Save(dataCR, context);

                                            if (dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail != null)
                                            {

                                                for (int j = 0; j < dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail.Count; j++)
                                                {
                                                    HEDG_STR_CHOICE_PRODUCT dataCP = new HEDG_STR_CHOICE_PRODUCT();
                                                    dataCP.HSCP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                    dataCP.HSCP_FK_HEDG_STR_CHOICE_ROLE = dataCR.HSCR_ROW_ID;
                                                    dataCP.HSCP_ORDER = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].order;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_FRONT = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_BACK = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_AF_FRAME = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].af_frame;
                                                    dataCP.HSCP_WEIGHT_PERCENT = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].weight_percent;
                                                    dataCP.HSCP_WEIGHT_VOLUME = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].weight_volume;
                                                    dataCP.HSCP_AF_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].af_price;
                                                    dataCP.HSCP_UNIT_PRICE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].price_unit;
                                                    //dataCP.HSCP_TYPE = dataDetail.hedg_STRCMT_main_choice.HedgSTRCMTMainChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].type;
                                                    dataCP.HSCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_CREATED_DATE = dtNow;
                                                    dataCP.HSCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_UPDATED_DATE = dtNow;
                                                    dataCPDal.Save(dataCP, context);

                                                }
                                            }
                                        }
                                    }
                                }                               
                            }
                            #endregion

                            #region EXTEND_CHOICE
                            if (dataDetail.hedg_STRCMT_extend_choice != null)
                            {
                                HEDG_STR_CHOICE_TYPE dataCT = new HEDG_STR_CHOICE_TYPE();
                                dataCT.HSCT_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                dataCT.HSCT_FK_HEDG_STR_CMT_FW = TransID;
                                dataCT.HSCT_TYPE = CPAIConstantUtil.HEDG_CONST_MAIN;
                                dataCT.HSCT_UNIT_PRICE = dataDetail.hedg_STRCMT_extend_choice.unit;
                                dataCT.HSCT_VOLUME = dataDetail.hedg_STRCMT_extend_choice.extend_volume;
                                dataCT.HSCT_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCT.HSCT_CREATED_DATE = dtNow;
                                dataCT.HSCT_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataCT.HSCT_UPDATED_DATE = dtNow;
                                dataCTDal.Save(dataCT, context);

                                if (dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail != null)
                                {
                                    HEDG_STR_CHOICE_DETAIL_DAL dataCDDal = new HEDG_STR_CHOICE_DETAIL_DAL();
                                    HEDG_STR_CHOICE_ROLE_DAL dataCRDal = new HEDG_STR_CHOICE_ROLE_DAL();
                                    HEDG_STR_CHOICE_PRODUCT_DAL dataCPDal = new HEDG_STR_CHOICE_PRODUCT_DAL();

                                    for (int i = 0; i < dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail.Count; i++)
                                    {
                                        HEDG_STR_CHOICE_DETAIL dataCD = new HEDG_STR_CHOICE_DETAIL();
                                        dataCD.HSCD_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        dataCD.HSCD_FK_HEDG_STR_CHOICE_TYPE = dataCT.HSCT_ROW_ID;
                                        dataCD.HSCD_CHOICE_NAME = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].name;
                                        dataCD.HSCD_NET_AF_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].net_af_price;
                                        dataCD.HSCD_OPERATION = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].operation;
                                        dataCD.HSCD_PACKAGE_NAME = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].package_name;
                                        dataCD.HSCD_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCD.HSCD_CREATED_DATE = dtNow;
                                        dataCD.HSCD_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataCD.HSCD_UPDATED_DATE = dtNow;
                                        dataCDDal.Save(dataCD, context);

                                        //subject = primary
                                        if (dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct != null)
                                        {
                                            HEDG_STR_CHOICE_ROLE dataCR = new HEDG_STR_CHOICE_ROLE();
                                            dataCR.HSCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataCR.HSCR_FK_HEDG_STR_CHOICE_DETAIL = dataCD.HSCD_ROW_ID;
                                            dataCR.HSCR_ROLE = CPAIConstantUtil.HEDG_CONST_SUBJECT;
                                            dataCR.HSCR_AVG_AF_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.avg_af_price;
                                            dataCR.HSCR_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_CREATED_DATE = dtNow;
                                            dataCR.HSCR_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_UPDATED_DATE = dtNow;
                                            dataCRDal.Save(dataCR, context);

                                            if (dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail != null)
                                            {

                                                for (int j = 0; j < dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail.Count; j++)
                                                {
                                                    HEDG_STR_CHOICE_PRODUCT dataCP = new HEDG_STR_CHOICE_PRODUCT();
                                                    dataCP.HSCP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                    dataCP.HSCP_FK_HEDG_STR_CHOICE_ROLE = dataCR.HSCR_ROW_ID;
                                                    dataCP.HSCP_ORDER = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].order;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_FRONT = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_BACK = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_AF_FRAME = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].af_frame;
                                                    dataCP.HSCP_WEIGHT_PERCENT = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].weight_percent;
                                                    dataCP.HSCP_WEIGHT_VOLUME = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].weight_volume;
                                                    dataCP.HSCP_AF_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].af_price;
                                                    dataCP.HSCP_UNIT_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].price_unit;
                                                    //dataCP.HSCP_TYPE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTSubjectProduct.HedgSTRCMTProductDetail[j].type;
                                                    dataCP.HSCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_CREATED_DATE = dtNow;
                                                    dataCP.HSCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_UPDATED_DATE = dtNow;
                                                    dataCPDal.Save(dataCP, context);

                                                }
                                            }
                                        }

                                        //object = secondary
                                        if (dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct != null)
                                        {
                                            HEDG_STR_CHOICE_ROLE dataCR = new HEDG_STR_CHOICE_ROLE();
                                            dataCR.HSCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataCR.HSCR_FK_HEDG_STR_CHOICE_DETAIL = dataCD.HSCD_ROW_ID;
                                            dataCR.HSCR_ROLE = CPAIConstantUtil.HEDG_CONST_OBJECT;
                                            dataCR.HSCR_AVG_AF_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.avg_af_price;
                                            dataCR.HSCR_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_CREATED_DATE = dtNow;
                                            dataCR.HSCR_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataCR.HSCR_UPDATED_DATE = dtNow;
                                            dataCRDal.Save(dataCR, context);

                                            if (dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail != null)
                                            {

                                                for (int j = 0; j < dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail.Count; j++)
                                                {
                                                    HEDG_STR_CHOICE_PRODUCT dataCP = new HEDG_STR_CHOICE_PRODUCT();
                                                    dataCP.HSCP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                    dataCP.HSCP_FK_HEDG_STR_CHOICE_ROLE = dataCR.HSCR_ROW_ID;
                                                    dataCP.HSCP_ORDER = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].order;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_FRONT = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_FK_HEDG_PRODUCT_BACK = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].product_front;
                                                    dataCP.HSCP_AF_FRAME = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].af_frame;
                                                    dataCP.HSCP_WEIGHT_PERCENT = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].weight_percent;
                                                    dataCP.HSCP_WEIGHT_VOLUME = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].weight_volume;
                                                    dataCP.HSCP_AF_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].af_price;
                                                    dataCP.HSCP_UNIT_PRICE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].price_unit;
                                                    //dataCP.HSCP_TYPE = dataDetail.hedg_STRCMT_extend_choice.HedgSTRCMTExtendChoiceDetail[i].HedgSTRCMTObjectProduct.HedgSTRCMTProductDetail[j].type;
                                                    dataCP.HSCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_CREATED_DATE = dtNow;
                                                    dataCP.HSCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataCP.HSCP_UPDATED_DATE = dtNow;
                                                    dataCPDal.Save(dataCP, context);

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #endregion

                            #region HEDG_STR_TOOL

                            HEDG_STR_TOOL_DAL dataTDal = new HEDG_STR_TOOL_DAL();
                            //HEDG_STR_TOOL_EXTENSION_DAL dataTEDal = new HEDG_STR_TOOL_EXTENSION_DAL();
                            HEDG_STR_TOOL_OPTION_DAL dataTODal = new HEDG_STR_TOOL_OPTION_DAL();
                            HEDG_STR_TOOL_KIO_OPTION_DAL dataTKDal = new HEDG_STR_TOOL_KIO_OPTION_DAL();
                            HEDG_STR_TOOL_FORMULA_DAL dataTFDal = new HEDG_STR_TOOL_FORMULA_DAL();
                            HEDG_STR_TOOL_NET_PREMIUM_DAL dataTNDal = new HEDG_STR_TOOL_NET_PREMIUM_DAL();
                            HEDG_STR_TOOL_NP_ITEM_DAL dataTIDal = new HEDG_STR_TOOL_NP_ITEM_DAL();

                            if (dataDetail.hedg_STRCMT_tool != null && dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail != null)
                            {
                                for (int i = 0; i < dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail.Count; i++)
                                {
                                    HEDG_STR_TOOL dataT = new HEDG_STR_TOOL();
                                    dataT.HST_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    //dataT.HST_FK_HEDG_STR_CMT_FW = TransID;
                                    dataT.HST_ORDER = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].order;
                                    dataT.HST_NAME = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].name;
                                    dataT.HST_USE_NET_PREMIUM = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].use_net_premium_flag;
                                    //dataT.HST_NET_PREMIUM_TYPE = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].net_premium_type;
                                    //dataT.HST_NET_PREMIUM_VOLUME = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].net_premium_volume;
                                    dataT.HST_USE_FORMULA = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].use_formula_flag;
                                    if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula != null)
                                        dataT.HST_EXPRESSION = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.expression;
                                    dataT.HST_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataT.HST_CREATED_DATE = dtNow;
                                    dataT.HST_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataT.HST_UPDATED_DATE = dtNow;
                                    dataTDal.Save(dataT, context);

                                    //#region HEDG_STR_TOOL_EXTENSION
                                    //if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension != null &&
                                    //    dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension.Count > 0)
                                    //{
                                    //    for (int j = 0; j < dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension.Count; j++)
                                    //    {
                                    //        HEDG_STR_TOOL_EXTENSION dataTE = new HEDG_STR_TOOL_EXTENSION();
                                    //        dataTE.HSTE_FK_HEDG_STR_TOOL = dataT.HST_ROW_ID;
                                    //        dataTE.HSTE_ORDER = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].order;
                                    //        if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].extend_period_from))
                                    //            dataTE.HSTE_EXTEND_PERIOD_FROM = DateTime.ParseExact(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].extend_period_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    //        if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].extend_period_to))
                                    //            dataTE.HSTE_EXTEND_PERIOD_TO = DateTime.ParseExact(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].extend_period_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    //        dataTE.HSTE_EXTEND_VOLUME = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].extend_volume;
                                    //        if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].exercise_date))
                                    //            dataTE.HSTE_EXCERCISE_DATE = DateTime.ParseExact(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTExtension[j].exercise_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    //        dataTE.HSTE_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    //        dataTE.HSTE_CREATED_DATE = dtNow;
                                    //        dataTE.HSTE_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    //        dataTE.HSTE_UPDATED_DATE = dtNow;
                                    //    }
                                    //}
                                    //#endregion

                                    #region HEDG_STR_TOOL_OPTION
                                    HEDG_STR_TOOL_OPTION dataTO = new HEDG_STR_TOOL_OPTION();
                                    HEDG_STR_TOOL_KIO_OPTION dataTK = new HEDG_STR_TOOL_KIO_OPTION();
                                    dataTO.HSTO_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    dataTO.HSTO_FK_HEDG_STR_TOOL = dataT.HST_ROW_ID;
                                    //dataTO.HSTO_CAPPED_VALUE = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].capped_value;
                                    if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].swaption_exercise_date))
                                        dataTO.HSTO_EXCERCISE_DATE = DateTime.ParseExact(dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].swaption_exercise_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    //dataTO.HSTO_GAIN_LOSS = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].target_redemption_gain_loss;
                                    dataTO.HSTO_VALUE_PER_UNIT = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].target_redemption_per_unit;
                                    if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_by_market == "Y")
                                    {
                                        dataTO.HSTO_KNOCK_IN_OUT_BY = "MARKET";
                                        if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option != null)
                                        {
                                            dataTK.HSTK_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataTK.HSTK_FK_HEDG_STR_TOOL_OPTION = dataTO.HSTO_ROW_ID;
                                            dataTK.HSTK_OPTION = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.option;
                                            dataTK.HSTK_ACTIVATED_OPERATOR = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.activated_operation;
                                            dataTK.HSTK_ACTIVATED_OPTION = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.activated_option;
                                            dataTK.HSTK_HEDGE_PRICE = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.hedge_price;

                                            dataTK.HSTK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTK.HSTK_CREATED_DATE = dtNow;
                                            dataTK.HSTK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTK.HSTK_UPDATED_DATE = dtNow;
                                        }
                                    }
                                    if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_by_package == "Y")
                                    {
                                        dataTO.HSTO_KNOCK_IN_OUT_BY = "PACKAGE";
                                        if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_package_option != null)
                                        {
                                            dataTK.HSTK_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataTK.HSTK_FK_HEDG_STR_TOOL_OPTION = dataTO.HSTO_ROW_ID;
                                            dataTK.HSTK_OPTION = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.option;
                                            dataTK.HSTK_ACTIVATED_OPERATOR = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.activated_operation;
                                            dataTK.HSTK_ACTIVATED_OPTION = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.activated_option;
                                            dataTK.HSTK_HEDGE_PRICE = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].knock_in_out_market_option.hedge_price;

                                            dataTK.HSTK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTK.HSTK_CREATED_DATE = dtNow;
                                            dataTK.HSTK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTK.HSTK_UPDATED_DATE = dtNow;
                                        }
                                    }
                                    dataTO.HSTO_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataTO.HSTO_CREATED_DATE = dtNow;
                                    dataTO.HSTO_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataTO.HSTO_UPDATED_DATE = dtNow;
                                    dataTODal.Save(dataTO, context);
                                    dataTKDal.Save(dataTK, context);
                                    #endregion

                                    #region HEDG_STR_TOOL_FORMULA
                                    if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula != null &&
                                        dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail.Count > 0)
                                    {

                                        for (int j = 0; j < dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail.Count; j++)
                                        {
                                            HEDG_STR_TOOL_FORMULA dataTF = new HEDG_STR_TOOL_FORMULA();
                                            dataTF.HSTF_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataTF.HSTF_FK_HEDG_STR_TOOL = dataT.HST_ROW_ID;
                                            dataTF.HSTF_ORDER = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail[j].order;
                                            dataTF.HSTF_TYPE = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail[j].type;
                                            dataTF.HSTF_VALUE = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.HedgSTRCMTFormulaDetail[j].value;
                                            dataTF.HSTF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTF.HSTF_CREATED_DATE = dtNow;
                                            dataTF.HSTF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTF.HSTF_UPDATED_DATE = dtNow;
                                            dataTFDal.Save(dataTF, context);
                                        }
                                        
                                    }
                                    #endregion

                                    #region HEDG_STR_TOOL_NET_PREMIUM
                                    if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium != null &&
                                        dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption != null &&
                                        dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption.Count > 0
                                        )
                                    {
                                        //dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTFormula.expression;
                                        for (int j = 0; j < dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption.Count; j++)
                                        {
                                            HEDG_STR_TOOL_NET_PREMIUM dataTN = new HEDG_STR_TOOL_NET_PREMIUM();
                                            dataTN.HSTN_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataTN.HSTN_FK_HEDG_STR_TOOL = dataT.HST_ROW_ID;
                                            dataTN.HSTN_ORDER = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].order;
                                            dataTN.HSTN_OPTION = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].name;
                                            dataTN.HSTN_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTN.HSTN_CREATED_DATE = dtNow;
                                            dataTN.HSTN_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataTN.HSTN_UPDATED_DATE = dtNow;
                                            dataTNDal.Save(dataTN, context);

                                            if (dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].HedgSTRCMTOptionDetail != null)
                                            {
                                                for (int k = 0; k < dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].HedgSTRCMTOptionDetail.Count; k++)
                                                {
                                                    HEDG_STR_TOOL_NP_ITEM dataTI = new HEDG_STR_TOOL_NP_ITEM();
                                                    dataTI.HSNI_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                                    dataTI.HSNI_FK_HEDG_STR_TOOL_NP = dataT.HST_ROW_ID;
                                                    dataTI.HSNI_NET_TARGET = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].HedgSTRCMTOptionDetail[k].net_target_value;
                                                    dataTI.HSNI_ORDER = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].HedgSTRCMTOptionDetail[k].order;
                                                    dataTI.HSNI_PREMIUM = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].HedgSTRCMTOptionDetail[k].premium_value;
                                                    dataTI.HSNI_RETURN = dataDetail.hedg_STRCMT_tool.HedgSTRCMTToolDetail[i].HedgSTRCMTNetPremium.HedgSTRCMTOption[j].HedgSTRCMTOptionDetail[k].return_value;
                                                    dataTI.HSNI_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataTI.HSNI_CREATED_DATE = dtNow;
                                                    dataTI.HSNI_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                                    dataTI.HSNI_UPDATED_DATE = dtNow;
                                                    dataTIDal.Save(dataTI, context);
                                                }
                                            }
                                            
                                        }

                                    }
                                    #endregion
                                }
                                #endregion

                            }

                            #region HEDG_STR_CMT_ATTACH_FILE

                            HEDG_STR_CMT_ATTACH_FILE_DAL dataAttachFileDAL = new HEDG_STR_CMT_ATTACH_FILE_DAL();
                            HEDG_STR_CMT_ATTACH_FILE dataHSAF;
                            if (!string.IsNullOrEmpty(dataDetail.attach_file))
                            {
                                foreach (var item in dataDetail.attach_file.Split('|'))
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataHSAF = new HEDG_STR_CMT_ATTACH_FILE();
                                        dataHSAF.HSAF_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                        dataHSAF.HSAF_FK_HEDG_STR_CMT_FW = TransID;
                                        dataHSAF.HSAF_PATH = item;
                                        dataHSAF.HSAF_INFO = "";
                                        dataHSAF.HSAF_TYPE = "EXT";
                                        dataHSAF.HSAF_CREATED_DATE = dtNow;
                                        dataHSAF.HSAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataHSAF.HSAF_UPDATED_DATE = dtNow;
                                        dataHSAF.HSAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataHSAF, context);
                                    }
                                }
                            }
                            #endregion

                            #region HEDG_STR_CMT_SUBMIT_NOTE
                            HEDG_STR_CMT_SUBMIT_NOTE_DAL dataSNDal = new HEDG_STR_CMT_SUBMIT_NOTE_DAL();
                            if (dataDetail.hedg_STRCMT_submit_note != null)
                            {
                                HEDG_STR_CMT_SUBMIT_NOTE dataSN = new HEDG_STR_CMT_SUBMIT_NOTE();
                                dataSN.HSSN_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                dataSN.HSSN_FK_HEDG_STR_CMT_FW = TransID;
                                dataSN.HSSN_SUBJECT = dataDetail.hedg_STRCMT_submit_note.subject;
                                if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_submit_note.approved_date))
                                    dataSN.HSSN_APPROVED_DATE = DateTime.ParseExact(dataDetail.hedg_STRCMT_submit_note.approved_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataSN.HSSN_REF_NO = dataDetail.hedg_STRCMT_submit_note.ref_no;
                                dataSN.HSSN_NOTE = dataDetail.hedg_STRCMT_submit_note.note;
                                dataSN.HSSN_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataSN.HSSN_CREATED_DATE = dtNow;
                                dataSN.HSSN_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataSN.HSSN_UPDATED_DATE = dtNow;
                                dataSNDal.Save(dataSN, context);

                                #region HEDG_ANNUAL_SN_ATTACH_FILE

                                HEDG_STR_CMT_SN_ATTACH_FILE_DAL dataSNAttachFileDAL = new HEDG_STR_CMT_SN_ATTACH_FILE_DAL();
                                HEDG_STR_CMT_SN_ATTACH_FILE dataHASF;

                                if (!string.IsNullOrEmpty(dataDetail.hedg_STRCMT_submit_note.attach_file))
                                {
                                    foreach (var item in dataDetail.hedg_STRCMT_submit_note.attach_file.Split('|'))
                                    {
                                        if (!string.IsNullOrEmpty(item))
                                        {
                                            dataHASF = new HEDG_STR_CMT_SN_ATTACH_FILE();
                                            dataHASF.HSSF_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                            dataHASF.HSSF_FK_HEDG_STR_CMT_SN = dataSN.HSSN_ROW_ID;
                                            dataHASF.HSSF_PATH = item;
                                            dataHASF.HSSF_INFO = "";
                                            dataHASF.HSSF_TYPE = "EXT";
                                            dataHASF.HSSF_CREATED_DATE = dtNow;
                                            dataHASF.HSSF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataHASF.HSSF_UPDATED_DATE = dtNow;
                                            dataHASF.HSSF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                            dataSNAttachFileDAL.Save(dataHASF, context);
                                        }
                                    }
                                }

                                #endregion

                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAIHedgingSTRCMTUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        
    }
}