using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Bunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000080
{
    public class CPAIDAFCheckStatusInitState : BasicBean,StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICheckStatusInitState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                //check status CPAI_ACTION_STATUS
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string status = etxValue.GetValue(CPAIConstantUtil.NextStatus); //current status
                string next_status = etxValue.GetValue(CPAIConstantUtil.NextStatus); //next status input
                string action = etxValue.GetValue(CPAIConstantUtil.CurrentAction); //current action
                string current_action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);//next action input
                string function_code = stateModel.EngineModel.functionId;

                // TODO: Hack
                //query data
                ActionStatusDAL service = new ActionStatusDAL();
                //CPAI_ACTION_STATUS cas = service.findActionStatus(function_code, status,action, next_status, current_action);
                //if (cas != null) {
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                //}
                //else
                //{
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_STATUS_RESP_CODE;
                //}

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICheckStatusInitState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICheckStatusInitState # :: Exception >>> " + ex);
                log.Error("CPAICheckStatusInitState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
