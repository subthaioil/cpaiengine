﻿using System;
using System.Linq;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALDAF;

namespace ProjectCPAIEngine.Flow.F10000082
{
    public class CPAIDAFGetBtnState : BasicBean, StateFlowAction
    {
        private const String TYPE_1 = "1";
        private FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
        private ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
        private ActionFunctionDAL acfMan = new ActionFunctionDAL();
        private UsersDAL usrMan = new UsersDAL();
        private ActionControlDAL actMan = new ActionControlDAL();
        private ActionOverrideDAL acoMan = new ActionOverrideDAL();
        private ActionButtonDAL abtMan = new ActionButtonDAL();
        private DataHistoryDAL dthMan = new DataHistoryDAL();

        private FUNCTION_TRANSACTION currentFtx;

        private String currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIDAFGetBtnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string channel = etxValue.GetValue(CPAIConstantUtil.Channel);

                getButtonList(stateModel.BusinessModel.etxValue);
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIDAFGetBtnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIDAFGetBtnState # :: Exception >>> " + ex);
                log.Error("CPAIDAFGetBtnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }


        private void getButtonList(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnApprovalFormState >> getButtonList #  ");
            List<CPAI_ACTION_BUTTON> lstAllButton = new List<CPAI_ACTION_BUTTON>();
            List<String> lstAcfFunction = new List<string>();
            List<Button> list = new List<Button>();
            ButtonList li = new ButtonList();
            ExtendValue respTrans = new ExtendValue();
            DAF_DATA_DAL daf_dal = new DAF_DATA_DAL();
            USERS users;
            String xml;
            string currentStatus, userRowId, userGroup, system, type;

            // find function transaction by "transaction_id" : (1)
            var ftxResult = ftxMan.findByTransactionId(etxValue.GetValue(CPAIConstantUtil.TransactionId));
            if (ftxResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.FUNCTION_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            currentFtx = ftxResult[0];
            currentStatus = daf_dal.GetStatus(currentFtx.FTX_TRANS_ID);

            // find user record by "user" : (2)
            var usrResult = usrMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (usrResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                throw new Exception();
            }
            var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.Type);
            if (etxResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }

            string dataType = etxResult[0].ETX_VALUE;
            users = usrResult[0];
            userRowId = users.USR_ROW_ID;
            userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
            string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
            system = etxValue.GetValue(CPAIConstantUtil.System);
            type = dataType;

            // find CPAI_ACTION_FUNCTION by "FUNCTION_TRANSACTION.FTX_FK_FUNCTION" and "USERS.USR_ROW_ID" and other params from request : (3)
            List<CPAI_ACTION_FUNCTION> acfResult = new List<CPAI_ACTION_FUNCTION>();
            if (!string.IsNullOrEmpty(userGroupDelegate)) //set user group deligate by poo 25072017
            {
                List<string> userGroups = new List<string>();
                userGroups.Add(userGroup.ToUpper());
                userGroups.Add(userGroupDelegate.ToUpper());
                acfResult = acfMan.findAcfDelegate(currentFtx.FTX_FK_FUNCTION, userGroups, userRowId, system, type);
            }
            else
            {
                acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, type);
                if (acfResult.Count == 0)
                {
                    acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, "OTHER");
                }
            }
            if (acfResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            for (int i = 0; i < acfResult.Count; i++)
            {
                lstAcfFunction.Add(acfResult[i].ACF_ROW_ID);
            }

            lstAllButton = daf_dal.GetButtonByUserID(users.USR_ROW_ID);
            // find CPAI_ACTION_CONTROL by "FUNCTION_TRANSACTION.FTX_INDEX3" and "CPAI_ACTION_FUNCTION.ACF_ROW_ID"
            var actResult = actMan.findByCurrentStatusAndFunction(currentStatus, lstAcfFunction);
            //for (int i = 0; i < actResult.Count; i++)
            //{                
            //    var acoResult = acoMan.findByFkActionControl(actResult[i].ACT_FK_ACTION_BUTTON);                
            //    if (acoResult.Count == 0)
            //    {                    
            //        var abtResult = abtMan.findByRowIdAndType(actResult[i].ACT_FK_ACTION_BUTTON, etxValue.GetValue(CPAIConstantUtil.Button_Type));
            //        if (abtResult.Count == 0)
            //        {
            //            currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
            //            throw new Exception();
            //        }
            //        else
            //        {
            //            if (ftxResult[0].FTX_INDEX4 == "DRAFT")
            //            {
            //                if (abtResult[0].ABT_BUTTON_NAME != "REJECT")
            //                {
            //                    lstAllButton.Add(abtResult[0]);
            //                }
            //            }
            //            else
            //            {
            //                lstAllButton.Add(abtResult[0]);
            //            }
            //        }
            //    }                
            //    else
            //    {
            //        var acoType = acoResult[0].ACO_OVERRIDE_TYPE;
            //        if (acoType.Equals(TYPE_1))
            //        {
            //            string user = etxValue.GetValue(CPAIConstantUtil.User);
            //            if (string.Equals(user, acoResult[0].ACO_OVERRIDE_VALUE, StringComparison.InvariantCultureIgnoreCase))
            //            {
            //                var abtResult = abtMan.findByRowId(acoResult[0].ACO_FK_ACTION_BUTTON);
            //                if (abtResult.Count == 0)
            //                {
            //                    currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
            //                    throw new Exception();
            //                }
            //                else
            //                {
            //                    lstAllButton.Add(abtResult[0]);
            //                }
            //            }
            //        }
            //    }
            //}

            string tem = "";
            for (int i = 0; i < lstAllButton.Count; i++)
            {
                CPAI_ACTION_BUTTON abt = lstAllButton[i];
                if (!tem.Contains(abt.ABT_BUTTON_NAME))
                {
                    Button m = new Button();
                    m.name = abt.ABT_BUTTON_NAME;
                    m.page_url = abt.ABT_PAGE_URL;
                    m.call_xml = abt.ABT_XML;
                    tem = tem + "|" + abt.ABT_BUTTON_NAME;
                    list.Add(m);
                }
            }

            list = FilterButton(currentStatus, list);
            li.lstButton = list;
            xml = ShareFunction.XMLSerialize<ButtonList>(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            log.Info("# End State CPAIGetBtnApprovalFormState >> getButtonList # :: Code >>> " + currentCode);
        }

        public List<Button> FilterButton(string status, List<Button> sources)
        {
            List<Button> list = new List<Button>();
            string[] draft_btns = { "SAVE DRAFT", "SUBMIT", "CANCEL", };
            string[] waiting_verify_btns = { "VERIFY", "REJECT", "CANCEL", };
            string[] waiting_endorse_btns = { "ENDORSE", "REJECT", "CANCEL", };
            string[] waiting_approve_btns = { "APPROVE", "REJECT", "CANCEL", };
            string[] approved_btns = { "REJECT", "CANCEL", };
            string[] cancel_btns = { };

            if (status == ConstantPrm.ACTION.DRAFT)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT).ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => draft_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_VERIFY)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_verify_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_ENDORSE)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY" || m.name == "ENDORSE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_endorse_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_APPROVE)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY" || m.name == "ENDORSE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_approve_btns.Contains(m.name)).ToList();
                }

                list = sources.Where(m => m.name == "APPROVE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_approve_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.CANCEL)
            {
                return list;
            }
            else if (status == ConstantPrm.ACTION.APPROVE || status == "APPROVED")
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY" || m.name == "ENDORSE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => approved_btns.Contains(m.name) && m.name != "REJECT").ToList();
                }

                list = sources.Where(m => m.name == "APPROVE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => approved_btns.Contains(m.name)).ToList();
                }
            }
            else
            {

            }
            return list;
        }

        public bool MatchButton(string status, string button_name)
        {
            string[] draft_btns = { "SAVE DRAFT", "SUBMIT", "CANCEL", };
            string[] waiting_verify_btns = { "VERIFY", "REJECT", "CANCEL", };
            string[] waiting_endorse_btns = { "ENDORSE", "REJECT", "CANCEL", };
            string[] waiting_approve_btns = { "APPROVE", "REJECT", "CANCEL", };
            string[] approved_btns = { "REJECT", "CANCEL", };
            string[] cancel_btns = { };

            if (status == ConstantPrm.ACTION.DRAFT)
            {
                if (draft_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_VERIFY)
            {
                if (waiting_verify_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_ENDORSE)
            {
                if (waiting_endorse_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_APPROVE)
            {
                if (waiting_approve_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.CANCEL)
            {
                if (cancel_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else if (status == ConstantPrm.ACTION.APPROVE)
            {
                if (approved_btns.Contains(button_name))
                {
                    return true;
                }
            }
            else
            {

            }
            return false;
        }

        //private void getButtonList(Dictionary<string, ExtendValue> etxValue)
        //{
        //    log.Info("# Start State CPAIDAFGetBtnState >> getButtonList #  ");
        //    List<CPAI_ACTION_BUTTON> lstAllButton = new List<CPAI_ACTION_BUTTON>();
        //    //ignore button for mobile 
        //    string mobileFlag = etxValue.GetValue(CPAIConstantUtil.MobileFlag) != null ? etxValue.GetValue(CPAIConstantUtil.MobileFlag) : "";

        //    // find function transaction by "transaction_id" : (1)
        //    var ftxResult = ftxMan.findByTransactionIdSystemType(etxValue.GetValue(CPAIConstantUtil.TransactionId),
        //        etxValue.GetValue(CPAIConstantUtil.System), etxValue.GetValue(CPAIConstantUtil.Type));

        //    if (ftxResult.Count != 1)
        //    {
        //        currentCode = CPAIConstantRespCodeUtil.FUNCTION_TRANSACTION_NOT_FOUND_RESP_CODE;
        //        throw new Exception();
        //    }
        //    currentFtx = ftxResult[0];
        //    string currentStatus = currentFtx.FTX_INDEX4;
        //    //if (currentStatus.Contains("APPROVE"))
        //    //{
        //    //    currentStatus = "APPROVE";
        //    //}
        //    // find user record by "user" : (2)
        //    var usrResult = usrMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
        //    if (usrResult.Count != 1)
        //    {
        //        currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
        //        throw new Exception();
        //    }
        //    USERS users = usrResult[0];
        //    string userRowId = users.USR_ROW_ID;
        //    string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
        //    string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
        //    string system = etxValue.GetValue(CPAIConstantUtil.System);
        //    string type = etxValue.GetValue(CPAIConstantUtil.Type);

        //    // find CPAI_ACTION_FUNCTION by "FUNCTION_TRANSACTION.FTX_FK_FUNCTION" and "USERS.USR_ROW_ID" and other params from request : (3)
        //    List<CPAI_ACTION_FUNCTION> acfResult = new List<CPAI_ACTION_FUNCTION>();
        //    if (!string.IsNullOrEmpty(userGroupDelegate)) //set user group deligate by poo 25072017
        //    {
        //        List<string> userGroups = new List<string>();
        //        userGroups.Add(userGroup.ToUpper());
        //        userGroups.Add(userGroupDelegate.ToUpper());
        //        acfResult = acfMan.findAcfDelegate(currentFtx.FTX_FK_FUNCTION, userGroups, userRowId, system, type);
        //    }
        //    else
        //    {
        //        acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, type);
        //    }
        //    if (acfResult.Count == 0)
        //    {
        //        currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
        //        throw new Exception();
        //    }
        //    List<String> lstAcfFunction = new List<string>();
        //    for (int i = 0; i < acfResult.Count; i++)
        //    {
        //        lstAcfFunction.Add(acfResult[i].ACF_ROW_ID);
        //    }

        //    // find CPAI_ACTION_CONTROL by "FUNCTION_TRANSACTION.FTX_INDEX3" and "CPAI_ACTION_FUNCTION.ACF_ROW_ID"
        //    List<CPAI_ACTION_CONTROL> actResult = new List<CPAI_ACTION_CONTROL>();
        //    if (mobileFlag.Equals(CPAIConstantUtil.YES_FLAG))
        //    {
        //        actResult = actMan.findByCurrentStatusAndFunctionMB(currentStatus, lstAcfFunction);
        //    }
        //    else
        //    {
        //        actResult = actMan.findByCurrentStatusAndFunction(currentStatus, lstAcfFunction);
        //    }


        //    if (actResult.Count == 0)
        //    {
        //        // not found any button, do nothing
        //    }
        //    for (int i = 0; i < actResult.Count; i++)
        //    {
        //        string fkActionButton = actResult[i].ACT_FK_ACTION_BUTTON;

        //        // find CPAI_ACTION_OVERRIDE record by CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON value
        //        var acoResult = acoMan.findByFkActionControl(fkActionButton);

        //        //if override not found, use button value from CPAI_ACTION_BUTTON
        //        if (acoResult.Count == 0)
        //        {
        //            var abtResult = abtMan.findByRowId(actResult[i].ACT_FK_ACTION_BUTTON);
        //            if (abtResult.Count == 0)
        //            {
        //                currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
        //                throw new Exception();
        //            }
        //            else
        //            {
        //                lstAllButton.Add(abtResult[0]);
        //            }
        //        }
        //        // if override found, use button value from CPAI_ACTION_OVERRIDE
        //        else
        //        {
        //            var acoType = acoResult[0].ACO_OVERRIDE_TYPE;
        //            if (acoType.Equals(TYPE_1))
        //            {
        //                string user = etxValue.GetValue(CPAIConstantUtil.User);
        //                if (string.Equals(user, acoResult[0].ACO_OVERRIDE_VALUE, StringComparison.InvariantCultureIgnoreCase))
        //                {
        //                    var abtResult = abtMan.findByRowId(acoResult[0].ACO_FK_ACTION_BUTTON);
        //                    if (abtResult.Count == 0)
        //                    {
        //                        currentCode = CPAIConstantRespCodeUtil.ACTION_BUTTON_NOT_FOUND_RESP_CODE;
        //                        throw new Exception();
        //                    }
        //                    else
        //                    {
        //                        lstAllButton.Add(abtResult[0]);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    // set extra xml <button_detail> 
        //    List<Button> list = new List<Button>();
        //    string tem = "";
        //    for (int i = 0; i < lstAllButton.Count; i++)
        //    {
        //        CPAI_ACTION_BUTTON abt = lstAllButton[i];
        //        if (!tem.Contains(abt.ABT_BUTTON_NAME))
        //        {
        //            Button m = new Button();
        //            m.name = abt.ABT_BUTTON_NAME;
        //            m.page_url = abt.ABT_PAGE_URL;
        //            m.call_xml = abt.ABT_XML;
        //            tem = tem + "|" + abt.ABT_BUTTON_NAME;
        //            list.Add(m);
        //        }
        //    }

        //    ButtonList li = new ButtonList();
        //    li.lstButton = list;

        //    String xml = ShareFunction.XMLSerialize<ButtonList>(li);
        //    xml = CPAIXMLParser.RemoveXmlDefinition(xml);
        //    ExtendValue respTrans = new ExtendValue();
        //    respTrans.value = xml;
        //    respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
        //    etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
        //    log.Info("# End State CPAIDAFGetBtnState >> getButtonList # :: Code >>> " + currentCode);
        //}

        //private void getApproveItems(Dictionary<string, ExtendValue> etxValue)
        //{
        //    log.Info("# Start State CPAIDAFGetBtnState >> getApproveItems #  ");
        //    var dthResult = dthMan.findApproveByDthTxnRef(currentFtx.FTX_ROW_ID);
        //    if (dthResult.Count == 0)
        //    {
        //        currentCode = CPAIConstantRespCodeUtil.DATA_HISTORY_NOT_FOUND_RESP_CODE;
        //        //throw new Exception();
        //    }
        //    List<string> lstApproveItem = new List<string>();
        //    for (int i = 0; i < dthResult.Count; i++)
        //    {
        //        ApproveItem m = new ApproveItem();
        //        m.approve_action = dthResult[i].DTH_ACTION;
        //        m.approve_action_by = dthResult[i].DTH_ACTION_BY;
        //        lstApproveItem.Add(JSonConvertUtil.modelToJson(m));
        //    }

        //    var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.DataDetail);
        //    if (etxResult.Count == 0)
        //    {
        //        currentCode = CPAIConstantRespCodeUtil.EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE;
        //        throw new Exception();
        //    }

        //    string dataDetail = "";
        //    if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_IN_FUNCTION_CODE)
        //    {
        //        dataDetail = CharterInServiceModel.getTransactionByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_IN_CMMT_FUNCTION_CODE)
        //    {
        //        dataDetail = CharterInServiceModel.getTransactionCMMTByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_OUT_FUNCTION_CODE)
        //    {
        //        dataDetail = CharterOutServiceModel.getTransactionCMCSByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_OUT_CMMT_FUNCTION_CODE)
        //    {
        //        dataDetail = CharterOutServiceModel.getTransactionCMMTByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.BUNKER_FUNCTION_CODE)
        //    {
        //        dataDetail = BunkerServiceModel.getTransactionByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CRUDE_PURCHASE_FUNCTION_CODE)
        //    {
        //        dataDetail = CrudePurchaseServiceModel.getTransactionByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.FREIGHT_FUNCTION_CODE)
        //    {
        //        dataDetail = EstimateFreightServiceModel.getTransactionByID(currentFtx.FTX_TRANS_ID);
        //    }
        //    else
        //    {
        //        dataDetail = etxResult[0].ETX_VALUE;
        //    }

        //    // remove old approve_items
        //    dataDetail = dataDetail.Replace(",\"approve_items\":null", "");

        //    // create approve xml
        //    var a = dataDetail.ReplaceLast("}", ",'approve_items': [");
        //    for (int i = 0; i < lstApproveItem.Count; i++)
        //    {
        //        a = a + lstApproveItem[i];
        //        if (i != lstApproveItem.Count - 1)
        //        {
        //            a = a + ",";
        //        }
        //    }
        //    a = a + "]}";

        //    DataDetail respDataDetail = new DataDetail();
        //    respDataDetail.Text = a;

        //    // set extra xml "approve_items"
        //    String xml = ShareFunction.XMLSerialize<DataDetail>(respDataDetail);
        //    xml = CPAIXMLParser.RemoveXmlDefinition(xml);

        //    string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
        //    etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
        //    var newXml = oldXml + xml;

        //    ExtendValue respTrans = new ExtendValue();
        //    respTrans.value = newXml;
        //    respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
        //    etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

        //    ExtendValue extPurchaseNumber = new ExtendValue();
        //    extPurchaseNumber.value = currentFtx.FTX_INDEX8;
        //    //purchase no.
        //    etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PurchaseNumber, extPurchaseNumber);

        //    log.Info("# End State CPAIDAFGetBtnState >> getApproveItems # :: Code >>> " + currentCode);
        //}
        ////private void getAttachItems(Dictionary<string, ExtendValue> etxValue)
        ////{
        ////    log.Info("# Start State CPAIDAFGetBtnState >> getAttachItems #  ");
        ////    var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.AttachItems);
        ////    if (etxResult.Count == 0)
        ////    {
        ////        // do nothing
        ////    }
        ////    else
        ////    {
        ////        string attachItemsValue = etxResult[0].ETX_VALUE;
        ////        AttachItems attachItems = new AttachItems();
        ////        attachItems.Text = attachItemsValue;

        ////        String xml = ShareFunction.XMLSerialize<AttachItems>(attachItems);
        ////        xml = CPAIXMLParser.RemoveXmlDefinition(xml);

        ////        string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
        ////        etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
        ////        var newXml = oldXml + xml;

        ////        ExtendValue respTrans = new ExtendValue();
        ////        respTrans.value = newXml;
        ////        respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
        ////        etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
        ////    }
        ////    log.Info("# End State CPAIDAFGetBtnState >> getAttachItems # :: Code >>> " + currentCode);
        ////}

        //private void getAttachItems(Dictionary<string, ExtendValue> etxValue)
        //{
        //    log.Info("# Start State CPAIDAFGetBtnState >> getAttachItemsDB #  ");
        //    var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.AttachItems);
        //    if (etxResult.Count == 0)
        //    {
        //        // do nothing
        //    }
        //    else
        //    {
        //        AttachItems attachItems = new AttachItems();
        //        string attachItemsValue = "";
        //        if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_IN_FUNCTION_CODE || currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_IN_CMMT_FUNCTION_CODE)
        //        {
        //            attachItemsValue = CharterInServiceModel.getAttFileByID(currentFtx.FTX_TRANS_ID);
        //        }
        //        else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_OUT_FUNCTION_CODE)
        //        {
        //            attachItemsValue = CharterOutServiceModel.getAttFileCMCSByID(currentFtx.FTX_TRANS_ID);
        //        }
        //        else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CHARTER_OUT_CMMT_FUNCTION_CODE)
        //        {
        //            attachItemsValue = CharterOutServiceModel.getAttFileCMMTByID(currentFtx.FTX_TRANS_ID);
        //        }
        //        else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.BUNKER_FUNCTION_CODE)
        //        {
        //            attachItemsValue = BunkerServiceModel.getAttFileByID(currentFtx.FTX_TRANS_ID);
        //        }
        //        else if (currentFtx.FTX_FK_FUNCTION == CPAIConstantUtil.CRUDE_PURCHASE_FUNCTION_CODE)
        //        {
        //            attachItemsValue = CrudePurchaseServiceModel.getAttFileByID(currentFtx.FTX_TRANS_ID);
        //        }
        //        else
        //        {
        //            attachItemsValue = etxResult[0].ETX_VALUE;
        //        }

        //        attachItems.Text = attachItemsValue;
        //        String xml = ShareFunction.XMLSerialize<AttachItems>(attachItems);
        //        xml = CPAIXMLParser.RemoveXmlDefinition(xml);

        //        string oldXml = etxValue.GetValue(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
        //        etxValue.Remove(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE);
        //        var newXml = oldXml + xml;

        //        ExtendValue respTrans = new ExtendValue();
        //        respTrans.value = newXml;
        //        respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
        //        etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
        //    }
        //    log.Info("# End State CPAIDAFGetBtnState >> getAttachItemsDB # :: Code >>> " + currentCode);
        //}

        //private string encryptDataDetail(string dataDetail)
        //{
        //    string data_end = "";


        //    return data_end;
        //}
    }
}
