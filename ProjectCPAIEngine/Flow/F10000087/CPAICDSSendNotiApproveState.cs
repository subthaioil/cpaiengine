using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Flow.Model;
using DSMail.model;
using com.pttict.downstream.common.model;
using DSMail.service;
using com.pttict.engine.downstream;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using DSFcm.service;
using DSFcm.model;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.DALCOMMON;

namespace ProjectCPAIEngine.Flow.F10000087
{


    public class CPAICDSSendNotiApproveState : BasicBean, StateFlowAction
    {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISendNotiApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                string CreateBy = etxValue.GetValue(CPAIConstantUtil.CreateBy);

                GLOBAL_CONFIG_DAL gcMan = new GLOBAL_CONFIG_DAL();
                PTX_ACTION_MESSAGE_DAL amsMan = new PTX_ACTION_MESSAGE_DAL();
                string group = PAFHelper.GetUserGroup(CreateBy, "CDS");
                group = group.ToUpper().Substring(0, 4);
                List<CPAI_ACTION_MESSAGE> results = amsMan.findUserGroupNoti(action, system, type, stateModel.EngineModel.functionId, group);
                //results = results.FindAll(m => m.AMS_USR_GROUP.Contains(group.Substring(0, 4)) || m.AMS_USR_GROUP.ToUpper() == "CMVP" || m.AMS_USR_GROUP.ToUpper() == "EVPC");
                //bool is_bypass_sh = gcMan.Contain("CDS_GROUPS_BYPASS_SH", group);
                //if (!is_bypass_sh && action == CPAIConstantUtil.ACTION_SUBMIT)
                //{
                //    results = results.FindAll(m => m.AMS_USR_GROUP != "CMVP");
                //}
                if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_NOTI_RESP_CODE;
                }
                else
                {
                    for (int i = 0; i < results.Count; i++)
                    {
                        findUserAndSendNoti(stateModel, results[i], system);
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAISendNotiApproveState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);

                log.Info("# Error CPAISendNotiApproveState # :: Exception >>> " + ex);
                log.Error("CPAISendNotiApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendNoti(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system)
        {
            log.Info("# Start State CPAISendNotiApproveState >> findUserAndSendNoti #  ");
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER))
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                //List<USERS> userResult = userMan.findByUserGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroupSystem(userGroup, system);
                List<USERS> userResult = userGMan.findUserNotiByGroupSystem(userGroup, system);

                if (userResult.Count == 0)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_NOTI_RESP_CODE;
                    log.Info("user not found");
                }
                else
                {
                    Dictionary<string, string> lstNotiTo = new Dictionary<string, string>();
                    for (int i = 0; i < userResult.Count; i++)
                    {
                        if (userResult[i].USR_NOTI_ID != null && userResult[i].USR_OS != null)
                        {
                            lstNotiTo[userResult[i].USR_NOTI_ID] = userResult[i].USR_OS;
                        }
                    }
                    prepareSendNoti(stateModel, lstNotiTo, actionMessage);
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                List<USERS> userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    Dictionary<string, string> lstNotiTo = new Dictionary<string, string>();
                    for (int i = 0; i < userResult.Count; i++)
                    {
                        lstNotiTo[userResult[i].USR_NOTI_ID] = userResult[i].USR_OS;
                    }
                    prepareSendNoti(stateModel, lstNotiTo, actionMessage);
                }
            }

            log.Info("# End State CPAISendNotiApproveState >> findUserAndSendNoti # :: Code >>> " + currentCode);
        }
        public void prepareSendNoti(StateModel stateModel, Dictionary<string, string> lstNotiTo, CPAI_ACTION_MESSAGE actionMessage)
        {
            log.Info("# Start State CPAISendNotiApproveState >> prepareSendNoti #  ");
            string jsonText = Regex.Replace(actionMessage.AMS_NOTIFICATION_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            NotiDetail detail = JSonConvertUtil.jsonToModel<NotiDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;
            string action_data = detail.action_data;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (userResult != null && userResult.Count == 0)
            {
                log.Info("invalid user");
            }
            else
            {
                log.Info("found user");
            }
            //
            if (subject != null && subject.Contains("#pn"))
            {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body != null && body.Contains("#pn"))
            {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (action_data != null && action_data.Contains("#pn"))
            {
                action_data = action_data.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }

            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
            foreach (var item in lstNotiTo)
            {
                log.Debug("sendNoti");
                var key = item.Key;
                var value = item.Value;
                sendNoti(stateModel, key, value, subject, body, action_data);
            }

            log.Info("# End State CPAISendNotiApproveState >> prepareSendNoti # :: Code >>> " + currentCode);
        }

        public void sendNoti(StateModel stateModel, string key, string value, string subject, string body, string action_data)
        {
            log.Info("# Start State CPAISendNotiApproveState >> sendNoti #  ");
            if (key != null && !String.IsNullOrEmpty(key))
            {
                //call downstream send mail
                DownstreamController<FCMServiceConnectorImpl, string> connector = new DownstreamController<FCMServiceConnectorImpl, string>(new FCMServiceConnectorImpl(), "");
                //find by config and content from DB
                ConfigManagement configManagement = new ConfigManagement();
                String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
                String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

                var configObj = JSonConvertUtil.jsonToModel<RequestFcmModel>(config);
                configObj.os = value;

                String finalConfig = JSonConvertUtil.modelToJson(configObj);

                var contentObj = JSonConvertUtil.jsonToModel<ContentFcmModel>(content);
                contentObj.deviceId = key;
                contentObj.body_i = body;
                contentObj.title_i = subject;
                contentObj.action_data_i = action_data;

                String finalContent = JSonConvertUtil.modelToJson(contentObj);

                //call downstream
                DownstreamResponse<string> downResp = connector.invoke(stateModel, finalConfig, finalContent);
                currentCode = downResp.getResultCode();
                stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            }
            else
            {
                //null noti id
                currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_NOTI_ID_RESP_CODE;
            }

            log.Info("# End State CPAISendNotiApproveState >> sendNoti # :: Code >>> " + currentCode);
        }

    }
}
