using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using Oracle.ManagedDataAccess.Client;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
//using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.DALPAF;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.DALCOMMON;
using ProjectCPAIEngine.DAL.DALCDS;

namespace ProjectCPAIEngine.Flow.F10000087
{
    public class CPAICDSUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            DATA_HISTORY_DAL dthMan = new DATA_HISTORY_DAL();
            CDS_DATA_DAL cdsMan = new CDS_DATA_DAL();
            GLOBAL_CONFIG_DAL gcMan = new GLOBAL_CONFIG_DAL();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string usergroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
                CDS_DATA data = JsonConvert.DeserializeObject<CDS_DATA>(etxValue.GetValue(CPAIConstantUtil.DataDetailInput));
                // not need now
                //bool is_bypass_receive_flow = gcMan.Contain("CDS_GROUPS_BYPASS_RECEIVE_FLOW", usergroup);
                //if (false && is_bypass_receive_flow && currentAction == CPAIConstantUtil.ACTION_VERIFY && data.DDA_DEMURAGE_TYPE == "Received")
                //{
                //    nextStatus = CPAIConstantUtil.STATUS_APPROVED;
                //    ExtendValue extStatus = new ExtendValue();
                //    extStatus.value = nextStatus;
                //    if (etxValue.ContainsKey(CPAIConstantUtil.NextStatus))
                //    {
                //        etxValue.Remove(CPAIConstantUtil.NextStatus);
                //    }
                //    etxValue.Add(CPAIConstantUtil.NextStatus, extStatus);
                //}
                bool is_bypass_sh = gcMan.Contain("CDS_GROUPS_BYPASS_SH", usergroup);
                if (is_bypass_sh && currentAction == CPAIConstantUtil.ACTION_SUBMIT)
                {
                    nextStatus = CPAIConstantUtil.STATUS_WAITING_ENDORSE;
                    ExtendValue extStatus = new ExtendValue();
                    extStatus.value = nextStatus;
                    if (etxValue.ContainsKey(CPAIConstantUtil.NextStatus))
                    {
                        etxValue.Remove(CPAIConstantUtil.NextStatus);
                    }
                    etxValue.Add(CPAIConstantUtil.NextStatus, extStatus);
                }

                if (data == null)
                {
                    CDS_DATA tmp = cdsMan.GetWrap(stateModel.EngineModel.ftxTransId);
                    data = JsonConvert.DeserializeObject<CDS_DATA>(JsonConvert.SerializeObject(tmp));
                }
                data.CDA_FORM_ID = data.CDA_FORM_ID ?? etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                //data.CDA_USER_GROUP = data.CDA_USER_GROUP ?? etxValue.GetValue(CPAIConstantUtil.UserGroup).Substring(0, 4);
                string note = etxValue.GetValue(CPAIConstantUtil.Note);
                if (!string.IsNullOrEmpty(note))
                {
                    data.CDA_REASON = note;
                }
                cdsMan.CreateDataHistory(data, etxValue.GetValue(CPAIConstantUtil.CurrentAction), etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.Note), stateModel.EngineModel.ftxTransId, etxValue.GetValue(CPAIConstantUtil.CurrentAction) == ConstantPrm.ACTION.REJECT);
                if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_REJECT))
                {
                    dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxTransId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                }
                FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);
                cdsMan.InsertOrUpdate(nextStatus, data, stateModel.EngineModel.ftxTransId, etxValue.GetValue(CPAIConstantUtil.User));


                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];


                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAIUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                PAFHelper.log(ex);
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState # :: Exception >>> " + tem);
                log.Error("# Error CPAIUpdateDataState # :: Exception >>> " , ex);
                log.Error("CPAIUpdateDataState::Exception >>> "+ ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
