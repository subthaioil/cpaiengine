﻿using System;
using System.Linq;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALCDS;

namespace ProjectCPAIEngine.Flow.F10000089
{
    public class CPAICDSGetBtnState : BasicBean, StateFlowAction
    {
        private const String TYPE_1 = "1";
        private FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
        private ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
        private ActionFunctionDAL acfMan = new ActionFunctionDAL();
        private UsersDAL usrMan = new UsersDAL();
        private ActionControlDAL actMan = new ActionControlDAL();
        private ActionOverrideDAL acoMan = new ActionOverrideDAL();
        private ActionButtonDAL abtMan = new ActionButtonDAL();
        private DataHistoryDAL dthMan = new DataHistoryDAL();

        private FUNCTION_TRANSACTION currentFtx;

        private String currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICDSGetBtnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string channel = etxValue.GetValue(CPAIConstantUtil.Channel);

                getButtonList(stateModel.BusinessModel.etxValue);
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICDSGetBtnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICDSGetBtnState # :: Exception >>> " + ex);
                log.Error("CPAICDSGetBtnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }


        private void getButtonList(Dictionary<string, ExtendValue> etxValue)
        {
            log.Info("# Start State CPAIGetBtnApprovalFormState >> getButtonList #  ");
            List<CPAI_ACTION_BUTTON> lstAllButton = new List<CPAI_ACTION_BUTTON>();
            List<String> lstAcfFunction = new List<string>();
            List<Button> list = new List<Button>();
            ButtonList li = new ButtonList();
            ExtendValue respTrans = new ExtendValue();
            CDS_DATA_DAL cds_dal = new CDS_DATA_DAL();
            USERS users;
            String xml;
            string currentStatus, userRowId, userGroup, system, type;

            // find function transaction by "transaction_id" : (1)
            var ftxResult = ftxMan.findByTransactionId(etxValue.GetValue(CPAIConstantUtil.TransactionId));
            if (ftxResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.FUNCTION_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            currentFtx = ftxResult[0];
            currentStatus = cds_dal.GetStatus(currentFtx.FTX_TRANS_ID);

            // find user record by "user" : (2)
            var usrResult = usrMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (usrResult.Count != 1)
            {
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                throw new Exception();
            }
            var etxResult = etxMan.findByFtxRowIdAndKey(currentFtx.FTX_ROW_ID, CPAIConstantUtil.Type);
            if (etxResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }

            string dataType = etxResult[0].ETX_VALUE;
            users = usrResult[0];
            userRowId = users.USR_ROW_ID;
            userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);
            string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
            system = etxValue.GetValue(CPAIConstantUtil.System);
            type = dataType;

            // find CPAI_ACTION_FUNCTION by "FUNCTION_TRANSACTION.FTX_FK_FUNCTION" and "USERS.USR_ROW_ID" and other params from request : (3)
            List<CPAI_ACTION_FUNCTION> acfResult = new List<CPAI_ACTION_FUNCTION>();
            if (!string.IsNullOrEmpty(userGroupDelegate)) //set user group deligate by poo 25072017
            {
                List<string> userGroups = new List<string>();
                userGroups.Add(userGroup.ToUpper());
                userGroups.Add(userGroupDelegate.ToUpper());
                acfResult = acfMan.findAcfDelegate(currentFtx.FTX_FK_FUNCTION, userGroups, userRowId, system, type);
            }
            else
            {
                acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, type);
                if (acfResult.Count == 0)
                {
                    acfResult = acfMan.findCPAIBunkerAcf(currentFtx.FTX_FK_FUNCTION, userGroup, userRowId, system, "OTHER");
                }
            }
            if (acfResult.Count == 0)
            {
                currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                throw new Exception();
            }
            for (int i = 0; i < acfResult.Count; i++)
            {
                lstAcfFunction.Add(acfResult[i].ACF_ROW_ID);
            }

            lstAllButton = cds_dal.GetButtonByUserID(users.USR_ROW_ID);
            // find CPAI_ACTION_CONTROL by "FUNCTION_TRANSACTION.FTX_INDEX3" and "CPAI_ACTION_FUNCTION.ACF_ROW_ID"
            var actResult = actMan.findByCurrentStatusAndFunction(currentStatus, lstAcfFunction);
           
            string tem = "";
            for (int i = 0; i < lstAllButton.Count; i++)
            {
                CPAI_ACTION_BUTTON abt = lstAllButton[i];
                if (!tem.Contains(abt.ABT_BUTTON_NAME))
                {
                    Button m = new Button();
                    m.name = abt.ABT_BUTTON_NAME;
                    m.page_url = abt.ABT_PAGE_URL;
                    m.call_xml = abt.ABT_XML;
                    tem = tem + "|" + abt.ABT_BUTTON_NAME;
                    list.Add(m);
                }
            }

            list = FilterButton(currentStatus, list);
            li.lstButton = list;
            xml = ShareFunction.XMLSerialize<ButtonList>(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);
            log.Info("# End State CPAIGetBtnApprovalFormState >> getButtonList # :: Code >>> " + currentCode);
        }

        public List<Button> FilterButton(string status, List<Button> sources)
        {
            List<Button> list = new List<Button>();
            string[] draft_btns = { "SAVE DRAFT", "SUBMIT", "CANCEL", };
            string[] waiting_verify_btns = { "VERIFY", "REJECT", "CANCEL", };
            string[] waiting_endorse_btns = { "ENDORSE", "REJECT", "CANCEL", };
            string[] waiting_approve_btns = { "APPROVE", "REJECT", "CANCEL", };
            string[] approved_btns = { "REJECT", "CANCEL", };
            string[] cancel_btns = { };

            if (status == ConstantPrm.ACTION.DRAFT)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT).ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => draft_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_VERIFY)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_verify_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_ENDORSE)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY" || m.name == "ENDORSE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_endorse_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.WAITING_APPROVE)
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY" || m.name == "ENDORSE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_approve_btns.Contains(m.name)).ToList();
                }

                list = sources.Where(m => m.name == "APPROVE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => waiting_approve_btns.Contains(m.name)).ToList();
                }
            }
            else if (status == ConstantPrm.ACTION.CANCEL)
            {
                return list;
            }
            else if (status == ConstantPrm.ACTION.APPROVE || status == "APPROVED")
            {
                list = sources.Where(m => m.name == ConstantPrm.ACTION.SAVE_DRAFT || m.name == "VERIFY" || m.name == "ENDORSE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => approved_btns.Contains(m.name) && m.name != "REJECT").ToList();
                }

                list = sources.Where(m => m.name == "APPROVE").ToList();
                if (list.Count > 0)
                {
                    return sources.Where(m => approved_btns.Contains(m.name)).ToList();
                }
            }
            else
            {

            }
            return list;
        }



    }
}
