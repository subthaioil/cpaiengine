using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Flow.Model;
using DSMail.model;
using com.pttict.downstream.common.model;
using DSMail.service;
using com.pttict.engine.downstream;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALCDO;
using ProjectCPAIEngine.DAL.DALCOMMON;

namespace ProjectCPAIEngine.Flow.F10000090
{
    public class CPAICDOSendMailApproveState : BasicBean, StateFlowAction
    {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAISendMailApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                PTX_ACTION_MESSAGE_DAL ptxActionMan = new PTX_ACTION_MESSAGE_DAL();
                CDO_DATA_DAL dafMan = new CDO_DATA_DAL();
                PTX_USER_GROUP_DAL ugdMan = new PTX_USER_GROUP_DAL();
                GLOBAL_CONFIG_DAL gcMan = new GLOBAL_CONFIG_DAL();

                CDO_DATA data = dafMan.Get(stateModel.EngineModel.ftxTransId);
                string last_status = dafMan.GetLastStatus(stateModel.EngineModel.ftxTransId);
                List<CPAI_ACTION_MESSAGE> results = ptxActionMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId, last_status);

                //string group = ugdMan.GetUserGroup(data.CDA_CREATED_BY, "CDO");
                //results = results.FindAll(m => m.AMS_USR_GROUP.Contains(group.Substring(0, 4)) || m.AMS_USR_GROUP.ToUpper() == "CMVP" || m.AMS_USR_GROUP.ToUpper() == "EVPC");
                //bool is_bypass_sh = gcMan.Contain("CDO_GROUPS_BYPASS_SH", group);
                //if (!is_bypass_sh && action == CPAIConstantUtil.ACTION_SUBMIT)
                //{
                //    results = results.FindAll(m => m.AMS_USR_GROUP != "CMVP");
                //}

                string LAST_ACTION = dafMan.GetLastAction(stateModel.EngineModel.ftxTransId);
                LAST_ACTION = last_status ?? "";
                if (action == CPAIConstantUtil.ACTION_CANCEL && LAST_ACTION.ToLower().Trim() == CPAIConstantUtil.STATUS_DRAFT.ToLower().Trim())
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else if (action == CPAIConstantUtil.ACTION_CANCEL && LAST_ACTION.ToLower().Trim() == CPAIConstantUtil.ACTION_REJECT.ToLower().Trim())
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else if (results.Count == 0)
                {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                }
                else
                {
                    for (int i = 0; i < results.Count; i++)
                    {
                        findUserAndSendMail(stateModel, results[i], system);
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAISendMailApproveState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAISendMailApproveState # :: Exception >>> " + ex);
                log.Error("CPAISendMailApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system)
        {
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER))
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                //List<USERS> userResult = userMan.findByUserGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroupSystem(userGroup, system);
                List<USERS> userResult = userGMan.findUserMailByGroupSystem(userGroup, system);
                if (userResult.Count == 0)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                List<USERS> userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }

        }

        public void prepareSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail)
        {
            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            PTXMailDetail detail = JSonConvertUtil.jsonToModel<PTXMailDetail>(jsonText);
            string to = detail.to;
            string cc = detail.cc;
            string subject = detail.subject;
            string body = detail.body;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0)
            {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("login user not found");
            }

            CDO_MAIL_CONTENT mailMan = new CDO_MAIL_CONTENT(userResult[0], stateModel.EngineModel.ftxTransId);

            subject = mailMan.GetSubject(subject);
            body = mailMan.GetBody(body);

            //
            if (body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = body;
                foreach (USERS um in userMail)
                {
                    temp_body = body;
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    string out_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, subject, out_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body)
        {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            currentCode = downResp.getResultCode();
        }

    }
}
