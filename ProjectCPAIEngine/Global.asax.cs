﻿using ProjectCPAIEngine.ServiceProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using DevExpress.Web.Office;
using Common.Logging;

namespace ProjectCPAIEngine
{
    public class Global : System.Web.HttpApplication
    {
        ILog log = LogManager.GetLogger(typeof(Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            log.Debug("Application_Start");
            AreaRegistration.RegisterAllAreas();
            string ATStartQ = WebConfigurationManager.AppSettings["AutoStartQuartz"];
            string CronSchedule = WebConfigurationManager.AppSettings["CronSchedule"];
            log.Debug(ATStartQ + " : " + CronSchedule);
            if (bool.Parse(ATStartQ))
            {
                ProjService _service = new ProjService();
                _service.StartQuartz(CronSchedule);
                //_service.StartQuartz();
                // _service.StratPOSchedule();
            }
            DocumentManager.HibernationStoragePath = Server.MapPath("~/App_Data/HibernationStorage/"); //Required setting
            DocumentManager.HibernateTimeout = TimeSpan.FromMinutes(30); //Optional setting
            DocumentManager.HibernatedDocumentsDisposeTimeout = TimeSpan.FromDays(1); //Optional setting
            DocumentManager.HibernateAllDocumentsOnApplicationEnd = true; //Optional setting
            DocumentManager.EnableHibernation = true; //Required setting to turn the hibernation on

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}