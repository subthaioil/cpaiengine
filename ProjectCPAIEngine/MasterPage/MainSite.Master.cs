﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProjectCPAIEngine.DAL;
using System.IO;

using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.MasterPage
{
    public partial class MainSite : System.Web.UI.MasterPage
    {
        public StringBuilder HTMLMENU = new StringBuilder();
        DateTime _Dt = DateTime.Now;
        public string lbUserName = "";
        public string lbHeader = "";
        public string lbHeaderDetail = "";
        public string lbHeaderDay = "";
        public string lbHeaderDate = "";
        public string lbHeaderTime = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            //HTMLMENU.Append("<li><a href=\"#\">Can't Load Menu Please contact Admin!!!</a></li>");
            string[] _arrurl = this.Page.Request.FilePath.Split('/');
            string _url = (_arrurl.Length > 0) ? _arrurl[_arrurl.Length - 1] : "";
            //if (!CheckURLPermission(Const.User.MenuPermission, _url)) Response.Redirect("../web/mainboards.aspx");
            LoadUserInfo();
            GenMenu();

        }
        ShareFn _FN = new ShareFn();
        private void LoadUserInfo()
        {
            if (Const.User != null && Const.User.Name != null)
            {
                _Dt = DateTime.Now;
                lbUserName = Const.User.Name;
                empImage.Attributes.Add("src", _FN.GetPathofPersonalImg(Const.User.UserName));
                lbHeaderDay = _Dt.DayOfWeek.ToString();
                lbHeaderDate = string.Format("{0} {1}, {2}",_Dt.ToString("MMM"),_Dt.Day.ToString(),_Dt.ToString("yyyy"));
                lbHeaderTime = _Dt.ToString("hh:mm tt");
                GetHeaderDetail();
            }else
            {
                Response.Redirect("../web/login.aspx");
            }
        }

        // string format (MENU)
        //private string pstrMainMenuFormat = "<li><a href=\"{0}\" {1} class=\"dropdown-toggle\" data-toggle=\"dropdown\">{2}<span class=\"caret\"></span></a></li>";
        //private string pstrMainMenuWithSubMenuFormat = "<li><a href=\"{0}\" {1} class=\"dropdown-toggle\" data-toggle=\"dropdown\">{2}<span class=\"caret\"></span></a><ul class=\"dropdown-menu\">{3}</ul></li>";
        //private string pstrSubMenuFormat = "<li><a href=\"{0}\" {1}>{2}</a></li>";
        //private string pstrSubMenuWithSubMenuFormat = "<li class=\"dropdown-submenu\"><a href=\"{0}\" {1}>{2}</a><ul class=\"dropdown-menu\">{3}</ul>";

        private bool CheckURLPermission(List<MenuPermission> MenuPermission, string Pagename)
        {
            bool CanusePage = false;
            if (Pagename.ToUpper() != "mainboards.aspx".ToUpper())
            {
                foreach (var _item in MenuPermission)
                {
                    if (_item.MENU_CHILD.Count > 0)
                    {
                        CanusePage = CheckURLPermission(_item.MENU_CHILD, Pagename);
                        if (CanusePage) break;
                    }
                    else
                    {
                        if (_item.MEU_URL.ToUpper().IndexOf(Pagename.ToUpper()) > 0) { CanusePage = true; break; }
                    }
                }
            }
            else
            {
                CanusePage = true;
            }
            return CanusePage;
        }

        private void GenMenu()
        {
            if (Const.User != null)
            {
                //if (Const.User.MenuPermission.Count > 0)
                //{
                //    StringBuilder _html = new StringBuilder();
                //    List<MenuPermission> _lstMenu = Const.User.MenuPermission;

                //    var mainMenu = _lstMenu.Where(item => int.Parse(item.MEU_LEVEL).Equals(1) && item.MEU_CONTROL_TYPE.Equals("MENU")).OrderBy(x => int.Parse(x.MEU_LIST_NO));

                //    if (mainMenu != null)
                //    {
                //        foreach (var item in mainMenu)
                //        {
                //            string parentURL = GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
                //            string direct = checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";
                //            if (item.MENU_CHILD.Count > 0)
                //            {
                //                var subMenu = GetSubMenu(item.MENU_CHILD);
                //                _html.Append(string.Format(pstrMainMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));
                //            }
                //            else
                //            {
                //                _html.Append(string.Format(pstrMainMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
                //            }
                //        }
                //    }

                //    HTMLMENU = _html;

                //}

                HTMLMENU.Append(MenuServiceModel.GenMenu(Const.User));

            }
            else
            {
                Response.Redirect("../web/login.aspx");
            }
        }

        //public string GetSubMenu(List<MenuPermission> menuLst)
        //{
        //    StringBuilder _html = new StringBuilder();

        //    try
        //    {
        //       if (menuLst != null)
        //       {
        //            var query = menuLst.OrderBy(s => int.Parse(s.MEU_LIST_NO));

        //            foreach (var item in query)
        //            {
        //                //string parentURL = GetURL(iSub.MEU_URL, iSub.MEU_URL_DIRECT, iSub.LNG_DESCRIPTION);
        //                string parentURL = GetURL(item.MEU_URL, item.MEU_URL_DIRECT);
        //                string direct = checkURLDirect(item.MEU_URL, item.MEU_URL_DIRECT) == true ? "target =\"_blank\"" : "";

        //                if (item.MENU_CHILD.Count > 0)
        //                {
        //                    var subMenu = GetSubMenu(item.MENU_CHILD);
        //                    _html.Append(string.Format(pstrSubMenuWithSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION, subMenu));   
        //                }
        //                else
        //                {
        //                    _html.Append(string.Format(pstrSubMenuFormat, parentURL, direct, item.LNG_DESCRIPTION));
        //                }
        //            }
        //        }
        //        else
        //        {
        //            _html.Append("");
        //        }


        //        return _html.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        //private bool checkURLDirect(string MEU_URL, string MEU_URL_DIRECT)
        //{
        //    var check = false;
        //    if ((String.IsNullOrEmpty(MEU_URL_DIRECT) != true || Convert.ToString(MEU_URL_DIRECT) != "#") && Convert.ToString(MEU_URL) == "REDIRECT")
        //    {
        //        // New windows go to MEU_URL_DIRECT
        //        check = true;

        //    }
        //    return check;
        //}

        //private string GetURL(string strURL, string strURLDirect)
        //{

        //    string parentURL = "#";

        //    if (checkURLDirect(strURL, strURLDirect) == true)
        //    {
        //        // New windows go to MEU_URL_DIRECT
        //        if (strURLDirect != "#" && !string.IsNullOrEmpty(strURLDirect))
        //        {
        //            parentURL = strURLDirect;
        //        }

        //    }
        //    else
        //    {
        //        if (strURL != "#")
        //        {
        //            parentURL = _FN.GetSiteRootUrl(strURL);
        //        }
        //    }
        //    return parentURL;
        //}

        //private void GenMenu()
        //{
        //    if (Const.User != null )
        //    {
        //        if (Const.User.MenuPermission.Count > 0)
        //        {
        //            StringBuilder _html = new StringBuilder();
        //            List<MenuPermission> _lstMenu = Const.User.MenuPermission.OrderBy(x=>x.MEU_LIST_NO).ToList();
        //            foreach (var _mainMenu in _lstMenu)
        //            {
        //                LoadMenuPermission(1, _mainMenu, ref _html);
        //            }
        //            HTMLMENU = _html;
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect("../web/login.aspx");
        //    }
        //}

        //private void LoadMenuPermission(int LevelMenu,MenuPermission _Menu,ref StringBuilder _html)
        //{
        //    if (_Menu.MENU_CHILD.Count > 0)
        //    {
        //        string parentURL = "#";
        //        if(_Menu.MEU_URL != "#")
        //        {
        //            parentURL = _FN.GetSiteRootUrl(_Menu.MEU_URL);
        //        }
        //        _html.Append(string.Format("<li class=\"dropdown{0}\" style=\"font-size: 22px;\">", LevelMenu));
        //        if(LevelMenu == 2)
        //        {
        //            _html.Append(string.Format("<a href=\"{1}\">{0}</a>", _Menu.LNG_DESCRIPTION, parentURL));
        //        }
        //        else
        //        {
        //            _html.Append(string.Format("<a href=\"{0}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" style=\"font-size: 22px;\">{1}<span class=\"caret\"></span></a>",parentURL , _Menu.LNG_DESCRIPTION));
        //        }

        //        _html.Append(string.Format("<ul class=\"dropdown-menu\">", LevelMenu));
        //        LevelMenu++;
        //        foreach (var _item in _Menu.MENU_CHILD.OrderBy(x => x.MEU_LIST_NO).ToList())
        //        {
        //            LoadMenuPermission(LevelMenu, _item, ref _html);
        //        }
        //        _html.Append("</ul></li>");

        //    }
        //    else
        //    {


        //        if ((String.IsNullOrEmpty(_Menu.MEU_URL_DIRECT) != true || Convert.ToString(_Menu.MEU_URL_DIRECT) != "#") && Convert.ToString(_Menu.MEU_URL) == "REDIRECT")
        //        {
        //            // New windows go to MEU_URL_DIRECT
        //            _html.Append(string.Format(
        //          "<li style=\"font-size: 22px;\"> " +
        //          "<a href=\"{1}\" target=\"_blank\">" +
        //          "{0}</a>" +
        //          "</li>", _Menu.LNG_DESCRIPTION, _Menu.MEU_URL_DIRECT));

        //        }
        //        else
        //        {
        //            _html.Append(string.Format(
        //             "<li style=\"font-size: 22px;\"> " +
        //             "<a href=\"{1}\">" +
        //             "{0}</a>" +
        //             "</li>", _Menu.LNG_DESCRIPTION, _FN.GetSiteRootUrl(_Menu.MEU_URL)));
        //        }
        //    }


        //}

        private string GetHeaderDetail()
        {
            string _url = this.Page.Request.FilePath;
            if (_url.ToUpper().IndexOf("MAINBOARDS.ASPX") > 0)
            {
                lbHeader = "MY DASHBOARD";
                lbHeaderDetail = "";
            }
            else if (_url.ToUpper().IndexOf("BUNKERPREDUEPURCHASE.ASPX") > 0)
            {
                lbHeader = "Bunker <span>&</span> Marine Gas Oil";
                lbHeaderDetail = "Purchase Approval Form for Time Charter Vessel";
                
            }
            else if (_url.ToUpper().IndexOf("BUNKERPREDUEPURCHASECMMT.ASPX") > 0)
            {
                lbHeader = "Bunker <span>&</span> Marine Gas Oil(CMMT)";
                lbHeaderDetail = "Purchase Approval Form for Time Charter Vessel";

            }

            return "";
        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            Const.User = null;
            Response.Redirect("../web/login.aspx");
        }
    }
}