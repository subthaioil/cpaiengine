﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class ADContent
    {
        public String username { get; set; }
        public String pwd { get; set; }
        public String domainname { get; set; }
    }
}