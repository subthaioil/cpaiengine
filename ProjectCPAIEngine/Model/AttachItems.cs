﻿

using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "attach_items")]
    public class AttachItems
    {
        [XmlText]
        public string Text { get; set; }
    }
}