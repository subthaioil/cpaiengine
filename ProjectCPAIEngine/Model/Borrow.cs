﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class Borrow
    {
        public string sTripNo { get; set; }
        public BorrowDetailModel BorrowDetailModelPO { get; set; }
        public BorrowDetailModel BorrowDetailModelSale { get; set; }
    }

        public class BorrowDetailModel
        {
            // public string sTripNo { get; set; }
            public string sCustomer { get; set; }
            public string sDeliveryDate { get; set; }
            public string sCrude { get; set; }
            public string sPlant { get; set; }
            public string sSaleOrg { get; set; }
            public string sDueDate { get; set; }
            public string sVolumeBBL { get; set; }
            public string sVolumeLitres { get; set; }
            public string sVolumeMT { get; set; }
            public string sPONo { get; set; }
            public string sExchangeRate { get; set; }
            public string sTotalAmount { get; set; }
            public string sTotalUnit { get; set; }
            public string sRemark { get; set; }
            public string sPricePer { get; set; }
            public string sPrice { get; set; }
            public string sUnitID { get; set; }
            public string sType { get; set; }
            public string sStatus { get; set; }
            public string sRelease { get; set; }
            public string sTank { get; set; }
            public string sSaleOrder { get; set; }
            public string sPriceRelease { get; set; }
            public string sDistriChann { get; set; }
            public string sTableName { get; set; }
            public string sMemo { get; set; }
            public string sDateSAP { get; set; }
            public string sFIDoc { get; set; }
            public string sDONo { get; set; }
            public string sStatusToSAP { get; set; }
            public string sFIDocReverse { get; set; }
            public string sIncoTermType { get; set; }
            public string sTemp { get; set; }
            public string sDensity { get; set; }
            public string sStorageLocation { get; set; }
            public string sUpdateDO { get; set; }
            public string sUpdateGI { get; set; }
            public List<PriceModel> PriceList { get; set; }
            public string sCreateDate { get; set; }
            public string sCreateBy { get; set; }
            public string sLastModifyDate { get; set; }
            public string sLastModifyBy { get; set; }
            public string sCreateTime { get; set; }
            public string sLastModifyTime { get; set; }

        }


        public class PriceModel
        {

            public string sPriceType { get; set; }  // 1=Provisional sale , 2=Actual sale , 3=Provisional PO , 4=Actual PO
            public string sDueDate { get; set; }
            public string sPrice { get; set; }
            public string sPriceUnit { get; set; }
            public string sInvoice { get; set; }
            public string sInvUnit { get; set; }
            public string sTripNo { get; set; }
            public string sNum { get; set; }
            public string sRelease { get; set; }
            public string sMemo { get; set; }
            public decimal sumInvoice { get; set; }
    }
 }
