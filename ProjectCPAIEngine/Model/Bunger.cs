﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ProjectCPAIEngine.Model
{

    public class ReplaceParam
    {
        public string ParamName { get; set; }
        public string ParamVal { get; set; }
    }
    public class Bunger
    {
        public void CloneBungerProperties(OffersItemTmp source, OffersItemTmp target)
        {

            target.contact_person = source.contact_person;
            target.final_flag = source.final_flag;
            target.final_price = source.final_price;
            target.total_price = source.total_price;
            target.flagEdit = source.flagEdit;
            target.KeyItem = source.KeyItem;
            target.others_cost_barge = source.others_cost_barge;
            target.others_cost_surveyor = source.others_cost_surveyor;
            target.purchasing_term = source.purchasing_term;
            target.supplier = source.supplier;
            target.note = source.note;
            target.pricebasedate = source.pricebasedate;
            target.quantitys = new List<Quantity>();
            target.round_items = new List<RoundItem>();

            foreach (var _item in source.quantitys)
            {
                Quantity _q = new Quantity();
                _q.quantity_type = _item.quantity_type;
                List<string> lstqval = new List<string>();
                foreach (var _itm in _item.quantity_value)
                {

                    lstqval.Add(_itm);
                }
                _q.quantity_value = lstqval;
                target.quantitys.Add(_q);
            }
            foreach (var _item in source.round_items)
            {
                RoundItem _r = new RoundItem();
                _r.round_no = _item.round_no;
                List<RoundInfo> _rilst = new List<RoundInfo>();
                foreach (var _itm in _item.round_info)
                {
                    RoundInfo _ri = new Model.RoundInfo();
                    _ri.round_type = _itm.round_type;
                    List<RoundValues> lstrval = new List<RoundValues>();
                    foreach (var it in _itm.round_value)
                    {
                        lstrval.Add(it);
                    }
                    _ri.round_value = lstrval;
                    _rilst.Add(_ri);
                }
                _r.round_info = _rilst;
                target.round_items.Add(_r);
            }
        }

        public List<OffersItemTmp> CloneBungerPropertiesLst(List<OffersItemTmp> source)
        {
            List<OffersItemTmp> _lst = new List<Model.OffersItemTmp>();
            foreach (var _item in source)
            {
                OffersItemTmp _cloneitem = new Model.OffersItemTmp();
                CloneBungerProperties(_item, _cloneitem);
                _lst.Add(_cloneitem);
            }

            return _lst;
        }
    }

    public class OffersItemTmp
    {
        private List<Quantity> _quantitys;
        private List<RoundItem> _round_items;
        private bool _flagEdit = false;
        public string KeyItem { get; set; }
        public string supplier { get; set; }
        public string contact_person { get; set; }
        public List<Quantity> quantitys
        {
            get
            {
                if (_quantitys == null) return new List<Quantity>();
                else return _quantitys;
            }
            set { _quantitys = value; }
        }
        public string purchasing_term { get; set; }
        public string others_cost_barge { get; set; }
        public string others_cost_surveyor { get; set; }
        public string final_price { get; set; }
        public string total_price { get; set; }
        public string note { get; set; }
        public string pricebasedate { get; set; }
        public string final_flag { get; set; }
        public List<RoundItem> round_items
        {
            get
            {
                if (_round_items == null) return new List<RoundItem>();
                else return _round_items;
            }
            set { _round_items = value; }
        }
        public bool flagEdit { get { return _flagEdit; } set { _flagEdit = value; } }
    }

    public class LstDataObj{
        List<OfferSaveData> OfferItem { get; set;}
    }

    public class OfferSaveData
    {
        public string keyitem { get; set; }
        public string supplier { get; set; }
        public string contact_person { get; set; }
        public string purchasing_term { get; set; }
        public string others_cost_barge { get; set; }
        public string others_cost_surveyor { get; set; }
        public string final_price { get; set; }
        public string total_price { get; set; }
        public string item { get; set; }
        public string note { get; set; }
        public string pricebasedate { get; set; }

    }
}