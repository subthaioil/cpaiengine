﻿using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    public class Button
    {
        [XmlElement(ElementName = "name")]
        public string name { get; set; }
        [XmlElement(ElementName = "page_url")]
        public string page_url { get; set; }
        [XmlElement(ElementName = "call_xml")]
        public string call_xml { get; set; }
    }
}