﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "button_detail")]
    public class ButtonList
    {
        [XmlElement("button")]
        public List<Button> lstButton { get; set; }
    }
}