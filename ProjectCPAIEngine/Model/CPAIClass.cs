﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "ExtraXML")]
    public class ExtraXML
    {
        public string data_detail { get; set; }
        [XmlElement(ElementName = "button_detail")]
        public ButtonDetail buttonDetail { get; set; }
        [XmlElement(ElementName = "attach_items")]
        public string attachitems { get; set; }
    }

    [XmlRoot(ElementName = "ExtraXML")]
    public class ExtraXML_ACT
    {
        public string data_detail { get; set; }
        public string data_detail_act { get; set; }
        [XmlElement(ElementName = "button_detail")]
        public ButtonDetail buttonDetail { get; set; }
        [XmlElement(ElementName = "attach_items")]
        public string attachitems { get; set; }
    }

    public class ButtonDetail
    {
        [XmlElement(ElementName = "button")]
        public List<ButtonAction> Button { get; set; }

    } 
    
    public class ButtonAction
    {
        public string name { get; set; }
        public string page_url { get; set; }
        public string call_xml { get; set; }

    }

    public class Product
    {
        public string product_order { get; set; }
        public string product_name { get; set; }
        public string product_spec { get; set; }
        public string product_qty { get; set; }
        public string product_unit { get; set; }
    }

    public class DeliveryDateRange
    {
        public string date_from { get; set; }
        public string date_to { get; set; }
    }

    public class PaymentTerms
    {
        public string payment_term { get; set; }
        public string payment_term_detail { get; set; }
        public string sub_payment_term { get; set; }
    }

    public class MarketPrice
    {
        public string market_price_date { get; set; }
        public string market_price_nsfo380cst { get; set; }
        public string market_price_nsfo180cst { get; set; }
        public string market_price_mgo { get; set; }
    }

    public class Proposal
    {
        public string award_to { get; set; }
        public string award_price { get; set; }
        public string reason { get; set; }
        public string reasondetail { get; set; }
    }

    public class UserRoleMenu
    {
        public string MEU_GROUP_MENU { get; set; }
        public string MEU_PARENT_ID { get; set; }
        public string LNG_DESCRIPTION { get; set; }
        public string MEU_URL { get; set; }
        public string MEU_IMG { get; set; }
        public string MEU_LEVEL { get; set; }
        public string USR_ROW_ID { get; set; }
        public string URO_ROW_ID { get; set; }
        public string ROL_ROW_ID { get; set; }
        public string RMN_ROW_ID { get; set; }
        public string RMN_FK_MENU { get; set; }
        public string RMN_FK_ROLE { get; set; }
     
    }

   [Serializable]
  public class Quantity
   {
       private List<string> _quantity_value;
       public string quantity_type { get; set; }
       public List<string> quantity_value
       {
           get
           {
               if (_quantity_value == null) return new List<string>();
               else return _quantity_value;
           }
           set { _quantity_value = value; }
       }
   }
  

    [Serializable]
    public class RoundInfo
    {
        //private List<string> _round_value;
        public string round_type { get; set; }
        public List<RoundValues> round_value { get; set; }
    }
    public class RoundValues
    {
        public string value { get; set; }
        public string order { get; set; }
    }

        [Serializable]
    public class RoundItem
    {
        //private List<RoundInfo> _round_info;
        public string round_no { get; set; }
        public List<RoundInfo> round_info { get; set; }
    }

    [Serializable]
    public class OffersItem
    {
       
        //private List<RoundItem> _round_items;

        public string supplier { get; set; }
        public string contact_person { get; set; }       
        public string purchasing_term { get; set; }
        public string others_cost_barge { get; set; }
        public string others_cost_surveyor { get; set; }
        public string final_price { get; set; }
        public string total_price { get; set; }
        public string final_flag { get; set; }
        public string note { get; set; }
        public List<RoundItem> round_items{ get; set; }
    }
    [Serializable]
    public class ApproveItem
    {
        public string approve_action { get; set; }
        public string approve_action_by { get; set; }
    }

    public class BunkerPurchase
    {
        public string date_purchase { get; set; }
        public string vessel { get; set; }
        public string vesselothers { get; set; }
        public string trip_no { get; set; }
        public string type_of_purchase { get; set; }
        public string supplying_location { get; set; }
        public List<Product> product { get; set; }
        public string contract_type { get; set; }
        public DeliveryDateRange delivery_date_range { get; set; }
        public string voyage { get; set; }
        public string explanation { get; set; }
        public string explanationAttach { get; set; }
        public string remark { get; set; }
        public PaymentTerms payment_terms { get; set; }
        public MarketPrice market_price { get; set; }
        public Proposal proposal { get; set; }
        public string notes { get; set; }
        public string trader_by { get; set; }
        public string section_head_by { get; set; }
        public string request_date { get; set; }
        public string currency { get; set; }
        public string currency_symbol { get; set; }
        public List<OffersItem> offers_items { get; set; }
        public List<ApproveItem> approve_items { get; set; }
        public string reason { get; set; }
    }

    [XmlRoot(ElementName = "p")]
    public class P
    {
        [XmlAttribute(AttributeName = "k")]
        public string K { get; set; }
        [XmlAttribute(AttributeName = "v")]
        public string V { get; set; }
    }

    [XmlRoot(ElementName = "req_parameters")]
    public class Req_parameters
    {
        [XmlElement(ElementName = "p")]
        public List<P> P { get; set; }
    }

    [XmlRoot(ElementName = "request")]
    public class RequestCPAI
    {
        [XmlAttribute(AttributeName = "app_password")]
        public string App_password { get; set; }
        [XmlAttribute(AttributeName = "app_user")]
        public string App_user { get; set; }
        [XmlElement(ElementName = "extra_xml")]
        public string Extra_xml { get; set; }
        [XmlAttribute(AttributeName = "function_id")]
        public string Function_id { get; set; }
        [XmlElement(ElementName = "req_parameters")]
        public Req_parameters Req_parameters { get; set; }
        [XmlAttribute(AttributeName = "req_transaction_id")]
        public string Req_transaction_id { get; set; }
        [XmlAttribute(AttributeName = "state_name")]
        public string State_name { get; set; }
        [XmlAttribute(AttributeName = "parent_trx_id")]
        public string ParentTrxId { get; set; }
        [XmlAttribute(AttributeName = "parent_act_id")]
        public string ParentActId { get; set; }
    }



    public class attach_file
    {
        public List<string> attach_items { get; set; }
    }



}
