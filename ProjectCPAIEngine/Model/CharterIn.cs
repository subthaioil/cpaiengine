﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    #region --- Charterin CMCS ----

    public class ChiEvaluating
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_month { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_size { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flat_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string market_ref { get; set; } = "";

    }

    public class ChiLoadPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string portName { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crudeName { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplierName { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string kbbls { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ktons { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mtons { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date { get; set; } = "";
    }

    public class ChiDisPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string dis_order_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string dis_port { get; set; } = "";
    }

    public class ChiCargoDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string route { get; set; } = "";
        public List<ChiLoadPort> chi_load_ports { get; set; }
        public List<ChiDisPort> discharge_ports { get; set; }
    }

    public class ChiTypeOfFixing
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type_value { get; set; } = "";
    }

    public class ChiCharteringMethod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
        public List<ChiTypeOfFixing> chi_type_of_fixings { get; set; }
    }

    public class ChiRoundInfo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string counter { get; set; } = "";
    }

    public class ChiRoundItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string round_no { get; set; } = "";
        public List<ChiRoundInfo> chi_round_info { get; set; }
    }

    public class ChiOffersItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string KDWT { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_deal { get; set; } = "";
        public List<ChiRoundItem> chi_round_items { get; set; }
    }

    public class ChiNegotiationSummary
    {
        public List<ChiOffersItem> chi_offers_items { get; set; }
    }

    public class ChiProposedForApprove
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charter_patry { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_oil_washing { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string special_requirment { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_built { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string capacity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laden_speed { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string exten_cost { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flat_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_freight { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string address_com_percent { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string address_com { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string net_freight { get; set; } = "";
    }

    //public class ChiApproveItem
    //{
    //    [DisplayFormat(ConvertEmptyStringToNull = false)]
    //    public string approve_action { get; set; } = "";
    //    [DisplayFormat(ConvertEmptyStringToNull = false)]
    //    public string approve_action_by { get; set; } = "";
    //}

    public class ChiRootObject
    {
        public ChiEvaluating chi_evaluating { get; set; }
        public ChiCargoDetail chi_cargo_detail { get; set; }
        public ChiCharteringMethod chi_chartering_method { get; set; }
        public ChiNegotiationSummary chi_negotiation_summary { get; set; }
        public ChiProposedForApprove chi_proposed_for_approve { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chi_reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chi_note { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttach { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttachTrader { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)] ///Trip No
        public string tripNumber { get; set; } = "";
    }

    #endregion

    #region --- Charterin CMMT ----

    public class ChitEvaluating
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_month { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chr_party_date { get; set; } = "";
    }

    public class ChitDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chartererName { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ReasonTrader { get; set; } = "";
        public string TempPageMode { get; set; } = "";
        public List<ChitCargoQty> chit_cargo_qty { get; set; }

        public string bss { get; set; }
        public string NegoUnit { get; set; }
        public List<ChitLoadPortControl> chit_load_port_control { get; set; }
        public List<ChitDischargePortControl> chit_discharge_port_control { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterFor { get; set; } = "";

    }
  

    public class ChitCargoQty
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string qty { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Others { get; set; } = "";
    }

   

   
    public class ChitCharteringMethod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
    }

    public class ChitRoundInfo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string typeOther { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string counter { get; set; } = "";
    }

    public class ChitRoundItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string round_no { get; set; } = "";
        public List<ChitRoundInfo> chit_round_info { get; set; }
    }

    public class ChitOffersItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string row_Id { get; set; } = ""; 
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_init { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string KDWT { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_deal { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string finalremark { get; set; } = "";
        public List<ChitRoundItem> chit_round_items { get; set; }
    }

    public class ChitNegotiationSummary
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string select_charter_for { get; set; } = ""; 
        public List<ChitOffersItem> chit_offers_items { get; set; }
    }

    public class ChitProposedForApprove
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charter_patry { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_brokerCom { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string special_requirment { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_nameText { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_commission { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string demurrage { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_lumpsum { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_rate_mt { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_rate_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_others_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_lumpsum { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_rate_mt { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_rate_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_others_est_total { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string address_commission { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string net_freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freightUnit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Other { get; set; } = "";
         [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_built { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string capacity { get; set; } = "";

    }

    public class ChitRootObject
    {
        public ChitEvaluating chit_evaluating { get; set; }
        public ChitCharteringMethod chit_chartering_method { get; set; }
        public ChitNegotiationSummary chit_negotiation_summary { get; set; }
        public ChitProposedForApprove chit_proposed_for_approve { get; set; }
        public ChitDetail chit_detail { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chit_reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chit_note { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttach { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttachTrader { get; set; } = "";
    }

    public class ChitListDropDownKey
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }

    }
    public class ChitLoadPortControl
    {
        public string JettyPortKey { get; set; }
        public string PortType { get; set; }
        public string PortCountry { get; set; }
        public string PortLocation { get; set; }
        public string PortName { get; set; }
        public string PortFullName { get; set; }
        public string PortValue { get; set; }
        public string PortOrder { get; set; }
        public string PortOtherDesc { get; set; }
    }
    public class ChitDischargePortControl
    {
        public string JettyPortKey { get; set; }
        public string PortType { get; set; }
        public string PortCountry { get; set; }
        public string PortLocation { get; set; }
        public string PortName { get; set; }
        public string DischargeName { get; set; }
        public string DischargeFullName { get; set; }
        public string DischargeValue { get; set; }
        public string PortOrder { get; set; }
        public string PortOtherDesc { get; set; }
    }

    #endregion
}