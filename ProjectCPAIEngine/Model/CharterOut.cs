﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    #region --- Charter out old ---
    public class ChoEvaluating
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_month { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_size { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flat_rate { get; set; } = "";
    }

    public class ChoPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
    }
    public class ChoCargoDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string route { get; set; } = "";
        public List<ChoPort> cho_load_ports { get; set; }
        public List<ChoPort> discharge_ports { get; set; }
    }

    public class ChoTypeOfFixing
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type_value { get; set; } = "";
    }

    public class ChoCharteringMethod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
        public List<ChoTypeOfFixing> cho_type_of_fixings { get; set; }
    }

    public class ChoRoundInfo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string counter { get; set; } = "";
    }

    public class ChoRoundItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string round_no { get; set; } = "";
        public List<ChoRoundInfo> cho_round_info { get; set; }
    }

    public class ChoOffersItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string route_vessel { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_deal { get; set; } = "";
        public List<ChoRoundItem> cho_round_items { get; set; }
    }

    public class ChoNegotiationSummary
    {
        public List<ChoOffersItem> cho_offers_items { get; set; }
    }

    public class ChoFinalFixingDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charter_patry { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string subject_time { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
    }

    public class ChoEstimatedIncome
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fixing_ws { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_flat_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string min_loaded { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string add_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string income { get; set; } = "";
    }

    public class ChoEstimatedCost
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cost_of_mt { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string spot_in_1_ws { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string spot_in_1_flat_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string spot_in_1_min_loaded { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sum_spot_int_1 { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string spot_in_2_ws { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string spot_in_2_flat_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string spot_in_2_min_loaded { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sum_spot_int_2 { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string awaiting_time { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cost { get; set; } = "";
    }

    public class ChoTotal
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ten_preform_period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_cost_charter_out { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_cost_without { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_benefit { get; set; } = "";
    }

    public class ChoProposedForApprove
    {
        public ChoFinalFixingDetail cho_final_fixing_detail { get; set; }
        public ChoEstimatedIncome cho_estimated_income { get; set; }
        public ChoEstimatedCost cho_estimated_cost { get; set; }
        public ChoTotal cho_total { get; set; }
    }

    //public class ChoApproveItem
    //{
    //    [DisplayFormat(ConvertEmptyStringToNull = false)]
    //    public string approve_action { get; set; } = "";
    //    [DisplayFormat(ConvertEmptyStringToNull = false)]
    //    public string approve_action_by { get; set; } = "";
    //}

    public class ChoRootObject
    {
        public ChoEvaluating cho_evaluating { get; set; }
        public ChoCargoDetail cho_cargo_detail { get; set; }
        public ChoCharteringMethod cho_chartering_method { get; set; }
        public ChoNegotiationSummary cho_negotiation_summary { get; set; }
        public ChoProposedForApprove cho_proposed_for_approve { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cho_reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cho_note { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
    }

    #endregion

    #region --- Charter out CMCS ---
    public class ChosEvaluating
    {
        public string vessel_id { get; set; }
        public string vessel_name { get; set; }
        public string voyage_no { get; set; }
        public string date_purchase { get; set; }
        public string broker_id { get; set; }
        public string broker_name { get; set; }
        public string vessel_size { get; set; }
        public string freight { get; set; }
        public string flat_rate { get; set; }
        public string year { get; set; }
        public string unit { get; set; }
        public string period_date_from { get; set; }
        public string period_date_to { get; set; }
        public string freight_detail { get; set; }
    }

    public class ChosTypeOfFixing
    {
        public string type_flag { get; set; }
        public string type_value { get; set; }
    }

    public class ChosCharteringMethod
    {
        public string explanation { get; set; }
        public List<ChosTypeOfFixing> chos_type_of_fixings { get; set; }
    }

    public class ChosEstExpense
    {
        public string order { get; set; }
        public string chos_period_date_from { get; set; }
        public string chos_period_date_to { get; set; }
        public string tc_hire_no { get; set; }
        public string tc_hire_cost { get; set; }
        public string port_charge { get; set; }
        public string bunker_cost { get; set; }
        public string other_costs { get; set; }
        public string other_remarks { get; set; }
        public string total_tc_cost { get; set; }
        public string remarks { get; set; }
    }

    public class ChosNoCharterOutCaseA
    {
        public string total_expense_no_charter { get; set; }
        public List<ChosEstExpense> chos_est_expense { get; set; }
    }

    public class OtherExpense
    {
        public string order { get; set; }
        public string other_key { get; set; }
        public string other_value { get; set; }
    }

    public class ChosCharterOutCaseBOne
    {
        public string order { get; set; }
        public string chos_period_date_from { get; set; }
        public string chos_period_date_to { get; set; }
        public string vessel_id { get; set; }
        public string vessel_name { get; set; }
        public string voyage_no { get; set; }
        public string fixing_ws { get; set; }
        public string est_flat_rate { get; set; }
        public string min_loaded { get; set; }
        public List<ChosBOneEstIncomeDeduct> chos_b_one_est_income_deduct { get; set; }
        public string freight { get; set; }
        public string total_income { get; set; }
        public List<OtherExpense> other_expense { get; set; }
    }
    public class ChosBOneEstIncomeDeduct
    {
        public string order { get; set; }
        public string deduct { get; set; }
        public string name { get; set; }

    }
    public class ChosCaseCostOfMt
    {
        public string order { get; set; }
        public string chos_period_date_from { get; set; }
        public string chos_period_date_to { get; set; }
        public string vessel_id { get; set; }
        public string vessel_name { get; set; }
        public string voyage_no { get; set; }
        public string tc_hire_no { get; set; }
        public string tc_hire_cost_day { get; set; }
        public string tc_hire_cost_voy { get; set; }
        public string port_charge { get; set; }
        public string bunker_cost { get; set; }
        public string other_costs { get; set; }
        public string other_remarks { get; set; }
        public string total_tc_cost { get; set; }
        public string remarks { get; set; }
        public List<ChosSpotInVessel> chos_spot_in_vessel { get; set; }
        public List<ChosOtherExpense> chos_other_expense { get; set; }
    }

    public class ChosSpotInVessel
    {
        public string order { get; set; }
        public string vessel_id { get; set; }
        public string vessel_name { get; set; }
        public string loading_date_from { get; set; }
        public string ws { get; set; }
        public string flat_rate { get; set; }
        public string min_loaded { get; set; }
        public string deduct { get; set; }
        public List<ChosBTwoSpotInDeduct> chos_b_two_spot_in_deduct { get; set; }
        public string freight { get; set; }
        public string ws_percent { get; set; }
        public string additional { get; set; }
        public string loading_date_to { get; set; }
        public string deduct_action { get; set; }
        public string additional_name { get; set; }
        public string total_deduct_value { get; set; }
        public string freight_before_deduct { get; set; }
        

    }

    public class ChosBTwoSpotInDeduct

    {
        public string order { get; set; }
        public string deduct { get; set; }
        public string name { get; set; }

    }

    public class ChosOtherExpense
    {
        public string order { get; set; }
        public string other_expense { get; set; }
        public string amount { get; set; }
    }

    public class ChosCharterOutCaseBTwo
    {
        public ChosCaseCostOfMt chos_case_cost_of_mt { get; set; }

    }

    public class ChosCharterOutCaseB
    {
        public string total_income_b_one { get; set; }
        public string total_expense_b_two { get; set; }
        public List<ChosCharterOutCaseBOne> chos_charter_out_case_b_one { get; set; }
        public ChosCharterOutCaseBTwo chos_charter_out_case_b_two { get; set; }
    }

    public class ChosProposedForApprove
    {
        public string charterer_id { get; set; }
        public string charterer_name { get; set; }
        public string charter_party { get; set; }
        public string charterer_broker { get; set; }
        public string charterer_broker_name { get; set; }
        public string owner_broker { get; set; }
        public string owner_broker_name { get; set; }
        public string route { get; set; }
        public string laycan_from { get; set; }
        public string laycan_to { get; set; }
        public string loading_ports { get; set; }
        public string discharging_ports { get; set; }
        public string a_date_from { get; set; }
        public string a_date_to { get; set; }
        public string b_date_from { get; set; }
        public string b_date_to { get; set; }
    }

    public class ChosBenefit
    {
        public string chos_total_expense_no_charter { get; set; }
        public string chos_total_expense { get; set; }
        public string chos_net_benefit { get; set; }
    }

    public class ChosRootObject
    {
        public ChosEvaluating chos_evaluating { get; set; }
        public ChosCharteringMethod chos_chartering_method { get; set; }
        public ChosNoCharterOutCaseA chos_no_charter_out_case_a { get; set; }
        public ChosCharterOutCaseB chos_charter_out_case_b { get; set; }
        //public ChosCharterOutCaseBTwo chos_charter_out_case_b_two { get; set; }
        public ChosProposedForApprove chos_proposed_for_approve { get; set; }
        public ChosBenefit chos_benefit { get; set; }
        public string chos_reason { get; set; }
        public string chos_note { get; set; }
        public List<ApproveItem> approve_items { get; set; }
        public string explanationAttach { get; set; }
    }

    #endregion

    #region --- Charter out CMMT ---

    public class ChotEvaluating
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chr_party_date { get; set; } = "";
    }

    public class ChotLoadPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_name { get; set; } = "";
    }

    public class ChotDisPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_name { get; set; } = "";
    }

    public class ChotDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string BSS { get; set; } = "";
        public List<ChotLoadPort> chot_load_ports { get; set; }
        public List<ChotDisPort> chot_dis_ports { get; set; }
    }

    public class ChotProposedForApprove
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string qty_type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string volumn { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string disponent_owner { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string demurrage { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_commission { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string address_commission { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charter_party_form { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_lumpsum { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_rate_mt { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_rate_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_others_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_bunker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_port_change { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_total_commission { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string est_profit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_lumpsum { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_rate_mt { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_rate_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mrk_freight_others_est_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string witholding_tax { get; set; } = "";
    }

    public class ChotCharteringMethod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
    }

    public class ChotRoundInfo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string counter { get; set; } = "";
    }

    public class ChotRoundItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string round_no { get; set; } = "";
        public List<ChotRoundInfo> chot_round_info { get; set; }
    }

    public class ChotOffersItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_deal { get; set; } = "";
        public List<ChotRoundItem> chot_round_items { get; set; }
    }

    public class ChotNegotiationSummary
    {
        public List<ChotOffersItem> chot_offers_items { get; set; }
    }

    public class ChotRootObject
    {
        public ChotEvaluating chot_evaluating { get; set; }
        public ChotDetail chot_detail { get; set; }
        public ChotProposedForApprove chot_proposed_for_approve { get; set; }
        public ChotCharteringMethod chot_chartering_method { get; set; }
        public ChotNegotiationSummary chot_negotiation_summary { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chot_reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chot_note { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttach { get; set; } = "";
    }


    #endregion


    #region --- Charter out CMMT New ---
    public class ChotCharterOutCaseAOtherCost
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string detail { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string value { get; set; } = "";
    }

    public class ChotNoCharterOutCaseA
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string no_day { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tc_hire { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tc_hire_val { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string idling { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker { get; set; } = "";
        public List<ChotCharterOutCaseAOtherCost> chot_charter_out_case_a_other_costs { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_expense { get; set; } = "";
    }

    public class ChotEstIncomeBOne
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ex_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ex_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_income { get; set; } = "";
    }

    public class ChotEstExpenseBTwoPointOne
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_charge { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string commission_persent { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string commission_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax_persent { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_variable_costs { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string other { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string total_variable_cost { get; set; } = "";
    }

    public class ChotEstExpenseBTwoPointTwo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fix_costs { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_fixed_costs { get; set; } = "";
    }

    public class ChotEstExpenseBTwo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period_to { get; set; } = "";
        public ChotEstExpenseBTwoPointOne chot_est_expense_b_two_point_one { get; set; }
        public ChotEstExpenseBTwoPointTwo chot_est_expense_b_two_point_two { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_expense { get; set; } = "";
    }

    public class ChotCharterOutCaseB
    {
        public ChotEstIncomeBOne chot_est_income_b_one { get; set; }
        public ChotEstExpenseBTwo chot_est_expense_b_two { get; set; }
    }

    public class ChotCargo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_qty { get; set; } = "";        
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_unit_other { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_tolerance { get; set; } = "";
    }

    public class ChotLoadingPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_full_name { get; set; } = "";
    }

    public class ChotDischargingPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_full_name { get; set; } = "";
    }

    public class ChotSummary
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";        
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cp_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_party_form { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string demurrage { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string demurrage_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term { get; set; } = "";
        public List<ChotCargo> chot_cargo { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bss { get; set; } = "";
        public List<ChotLoadingPort> chot_loading_port { get; set; }
        public List<ChotDischargingPort> chot_discharging_port { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner_broker_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string addr_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax { get; set; } = "";
    }

    public class ChotSaving
    {
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string saving_a_b { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string saving_a_c { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string saving_total_expense { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string saving_exp_inc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string saving_total { get; set; } = "";
    }

    public class ChotProfit
    {
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string profit_b_a { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string profit_c_a { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string profit_total_expense { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string profit_inc_var { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string profit_total { get; set; } = "";
    }

    public class ChotDResult
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flag { get; set; } = "";
        public ChotSaving chot_saving { get; set; }
        public ChotProfit chot_profit { get; set; }
    }

    public class ChotResult
    {
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string a_total_expense { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string b_total_income { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string c_total_net_income { get; set; } = "";
        //public ChotDResult chot_d_result { get; set; }
        public string flag { get; set; } = "";
        public ChotSaving chot_saving { get; set; }
        public ChotProfit chot_profit { get; set; }
    }

    public class ChotObjectRoot
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chot_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chot_reason { get; set; } = "";
        public ChotNoCharterOutCaseA chot_no_charter_out_case_a { get; set; }
        public ChotCharterOutCaseB chot_charter_out_case_b { get; set; }
        public ChotSummary chot_summary { get; set; }
        public ChotResult chot_result { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chot_note { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string chot_reason_explanation { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttach { get; set; } = "";
    }

    /*
    public class ChotNoCharterOutCaseA
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tc_hire { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string no_day { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tc_hire_val { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string other_costs { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_expense { get; set; } = "";
    }

    public class ChotEstIncomeBOne
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ex_rate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ex_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_income { get; set; } = "";
    }

    public class ChotEstExpenseBTwoPointOne
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_charge { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string commission_persent { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string commission_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax_persent { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax_total { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_variable_costs { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string other { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_variable_cost { get; set; } = "";
    }

    public class ChotEstExpenseBTwoPointTwo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fix_costs { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_fixed_costs { get; set; } = "";
    }

    public class ChotEstExpenseBTwo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string period { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_expense { get; set; } = "";

        public ChotEstExpenseBTwoPointOne chot_est_expense_b_two_point_one { get; set; }
        public ChotEstExpenseBTwoPointTwo chot_est_expense_b_two_point_two { get; set; }

    }

    public class ChotCharterOutCaseB
    {
        public ChotEstIncomeBOne chot_est_income_b_one { get; set; }
        public ChotEstExpenseBTwo chot_est_expense_b_two { get; set; }
    }

    public class ChotCargo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_qty { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_unit_other { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo_tolerance { get; set; } = "";
    }

    public class ChotLoadingPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_full_name { get; set; } = "";
    }

    public class ChotDischargingPort
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port_full_name { get; set; } = "";
    }

    public class ChotSummary
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cp_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_party_form { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string demurrage { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term { get; set; } = "";        
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bss { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer_broker_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner_broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string owner_broker_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string addr_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withholding_tax { get; set; } = "";

        public List<ChotCargo> chot_cargo { get; set; }
        public List<ChotLoadingPort> chot_loading_port { get; set; }
        public List<ChotDischargingPort> chot_discharging_port { get; set; }
    }

    public class ChotSaving
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string saving_a_b { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string saving_a_c { get; set; } = "";
    }

    public class ChotProfit
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string profit_b_a { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string profit_c_a { get; set; } = "";
    }

    public class ChotDResult
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flag { get; set; } = "";
        public ChotSaving chot_saving { get; set; }
        public ChotProfit chot_profit { get; set; }
    }

    public class ChotResult
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string a_total_expense { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string b_total_income { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string c_total_net_income { get; set; } = "";

        public ChotDResult chot_d_result { get; set; }
    }

    public class ChotObjectRoot
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string note { get; set; } = "";

        public ChotNoCharterOutCaseA chot_no_charter_out_case_a { get; set; }
        public ChotCharterOutCaseB chot_charter_out_case_b { get; set; }
        public ChotSummary chot_summary { get; set; }
        public ChotResult chot_result { get; set; }
        public List<ApproveItem> approve_items { get; set; }
               
    }
    */
    #endregion
}