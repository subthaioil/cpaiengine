﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class CooData
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string area_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string approval_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_status { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_status_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_priority { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string assay_file { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string assay_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string assay_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string assay_ref { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string origin { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string country { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cam_file { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_categories { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_kerogen { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_characteristic { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_maturity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type_of_cam { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string update_from_crude { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string assay_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fouling_possibility { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_cam_file { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string draft_cam_file { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string submit_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string approve_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reject_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string created_by { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string created_date { get; set; } = "";
    }

    public class CooUnit
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string key { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string value { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string question_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string submit_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string approve_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reject_date { get; set; } = "";
    }

    public class CooArea
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        public List<CooUnit> units { get; set; }
    }

    public class CooExperts
    {
        public List<CooArea> areas { get; set; }
    }

    public class CooRootObject
    {
        public CooData data { get; set; }
        public CooExperts experts { get; set; }
        public CoexAreas areas { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string support_doc_file { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string comment_draft_cam { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string comment_final_cam { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string note { get; set; } = "";
    }
}