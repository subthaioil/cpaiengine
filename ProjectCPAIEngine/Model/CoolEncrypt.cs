﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    public class CoolGroupEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
        public string type_Encrypted { get; set; }
        public List<string> support_doc_file1 { get; set; }
        public List<string> support_doc_file2 { get; set; }
        public List<string> support_doc_file3 { get; set; }
        public List<string> support_doc_file4 { get; set; }
        public List<string> support_doc_file5 { get; set; }
        public List<string> support_doc_file6 { get; set; }
        public List<string> support_doc_file_ext1 { get; set; }
        public List<string> support_doc_file_ext2 { get; set; }
        public List<string> support_doc_file_ext3 { get; set; }
        public List<string> support_doc_file_ext4 { get; set; }
        public List<string> support_doc_file_ext5 { get; set; }
        public List<string> support_doc_file_ext6 { get; set; }
        public List<CoolEncrypt> CoolTransaction { get; set; }
    }

    [XmlRoot(ElementName = "transaction")]
    public class CoolEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
        public string type_Encrypted { get; set; }
        public List<string> support_doc_file1 { get; set; }
        public List<string> support_doc_file2 { get; set; }
        public List<string> support_doc_file3 { get; set; }
        public List<string> support_doc_file4 { get; set; }
        public List<string> support_doc_file5 { get; set; }
        public List<string> support_doc_file6 { get; set; }
        public List<string> support_doc_file_ext1 { get; set; }
        public List<string> support_doc_file_ext2 { get; set; }
        public List<string> support_doc_file_ext3 { get; set; }
        public List<string> support_doc_file_ext4 { get; set; }
        public List<string> support_doc_file_ext5 { get; set; }
        public List<string> support_doc_file_ext6 { get; set; }
    }

    #region-------------CoolList---------------------
    [XmlRoot(ElementName = "list_trx")]
    public class List_Cooltrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<CoolEncrypt> CoolTransaction { get; set; }
    }
    #endregion-----------------------------------------
}