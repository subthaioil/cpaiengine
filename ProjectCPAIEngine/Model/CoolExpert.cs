﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class CoexComment
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string score_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string processing_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string processing_note { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string question_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string comment_expert { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string comment_expert_sh { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string average_score { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string recommended_score { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string agreed_score { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string recommended_score_reason { get; set; } = "";
    }

    public class CoexScoreDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string score { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string description { get; set; } = "";
    }

    public class CoexCommentItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string operational_parameter { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string weight_factor { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string weight_score { get; set; } = "";
        public List<CoexScoreDetail> score_detail { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string calculation_result { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string calculation_detail { get; set; } = "";
    }

    public class CoexUnit
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string key { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string next_status { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string isSelected_expert { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string isSelected_expert_sh { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string attached_file { get; set; } = "";
        public CoexComment comment { get; set; }
        public List<CoexCommentItem> comment_items { get; set; }
    }

    public class CoexArea
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string isSelected { get; set; } = "";
        public List<CoexUnit> unit { get; set; }
    }

    public class CoexAreas
    {
        public List<CoexArea> area { get; set; }
    }

    public class CoexRootObject
    {
        public CoexAreas areas { get; set; }
        public List<ApproveItem> approve_items { get; set; }
    }
}