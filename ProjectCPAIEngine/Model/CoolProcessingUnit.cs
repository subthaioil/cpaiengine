﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class CoolProcessingUnit
    {
        public class CpuProcessingUnit
        {
            string date { get; set; }
            string abbrv { get; set; }
            string unit { get; set; }
        }

        public class CDU1
        {
            string name { get; set; }
            string abbrv { get; set; }
            string unit
            {
                get
                {
                    return "CDU-1";
                }
            }
        }

        public class CDU2
        {
            string name { get; set; }
            string abbrv { get; set; }
            string unit
            {
                get
                {
                    return "CDU-2";
                }
            }
        }

        public class CDU3
        {
            string name { get; set; }
            string abbrv { get; set; }
            string unit
            {
                get
                {
                    return "CDU-3";
                }
            }
        }
    }
}