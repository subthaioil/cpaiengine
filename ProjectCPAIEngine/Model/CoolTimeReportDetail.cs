﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class CoolTimeReportDetail
    {
        public string month_year { get; set; }
        public double working_time { get; set; }
        public List<Crude> crude { get; set; }

        public class Crude
        {
            public string name { get; set; }
            public string country { get; set; }
            public double a_time_day { get; set; }
            public double b_time_day { get; set; }
            public double c_time_day { get; set; }
            public double d_time_day { get; set; }
            public double a_time_hour { get; set; }
            public double b_time_hour { get; set; }
            public double c_time_hour { get; set; }
            public double d_time_hour { get; set; }
            public List<Unit> unit { get; set; }
        }

        public class Unit
        {
            public string name { get; set; }
            public double expert_time_hour { get; set; }
            public double expert_sh_time_hour { get; set; }
        }
    }
}