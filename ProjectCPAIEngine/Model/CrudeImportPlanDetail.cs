﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class HeadData
    {
        public string port { get; set; }
        public string port_order { get; set; }
        public string port_value { get; set; }
    }

    //public class TceMkDisPort
    //{
    //    public string port { get; set; }
    //    public string port_order { get; set; }
    //    public string port_value { get; set; }
    //}

    //public class TceMkDetail
    //{
    //    public string date_from { get; set; }
    //    public string date_to { get; set; }
    //    public string td { get; set; }
    //    public string vessel { get; set; }
    //    public string voy_no { get; set; }
    //    public string month { get; set; }
    //    public string tc_rate { get; set; }
    //    public string operating_days { get; set; }
    //    public string bunker_cost_fo { get; set; }
    //    public string bunker_cost_go { get; set; }
    //    public string other_cost { get; set; }
    //    public string flat_rate { get; set; }
    //    public string minload { get; set; }
    //    public string theoritical_days { get; set; }
    //    public string fo_mt { get; set; }
    //    public string go_mt { get; set; }
    //    public string demurrage { get; set; }

    //    public string tc_num { get; set; }
    //    public string fo_unit_price { get; set; }
    //    public string go_unit_price { get; set; }
    //    public List<TceMkLoadPort> load_ports { get; set; }
    //    public List<TceMkDisPort> dis_ports { get; set; }
    //}

    //public class TceMkRootObject
    //{
    //    public TceMkDetail tce_mk_detail { get; set; }
    //}
}