﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class CdpRoundItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string round_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public List<string> round_value { get; set; } = new List<string>();
    }

    public class CdpOffersItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string contact_person { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string incoterms { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string market_source { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string latest_lp_plan_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bechmark_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string rank_round { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string margin_lp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string margin_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_flag { get; set; } = "";
        public List<CdpRoundItem> round_items { get; set; }
    }

    public class CdpCompetOffersItem
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string contact_person { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string incoterms { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string market_source { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string latest_lp_plan_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bechmark_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string rank_round { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string margin_lp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string margin_unit { get; set; } = "";
        public List<CdpRoundItem> round_items { get; set; }
    }

    public class CdpContractPeriod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_to { get; set; } = "";
    }

    public class CdpLoadingPeriod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_to { get; set; } = "";
    }

    public class CdpDischargingPeriod
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_to { get; set; } = "";
    }

    public class CdpPaymentTerms
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term_detail { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sub_payment_term { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term_others { get; set; } = "";
    }

    public class CdpProposal
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string award_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_ref1_bbl { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_ref1_us { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_ref2_bbl { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_ref2_us { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price_period_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price_period_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string volume { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance_option { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance_type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string performance_bond { get; set; } = "";
    }

    public class CdpRootObject
    {
        public List<CdpOffersItem> offers_items { get; set; }
        public List<CdpCompetOffersItem> compet_offers_items { get; set; }
        public CdpContractPeriod contract_period { get; set; }
        public CdpLoadingPeriod loading_period { get; set; }
        public CdpDischargingPeriod discharging_period { get; set; }
        public CdpPaymentTerms payment_terms { get; set; }
        public CdpProposal proposal { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string feedstock { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string feedstock_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string for_feedStock { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string origin_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string origin { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string term { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string term_others { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supply_source { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanation { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string explanationAttach { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string other_condition { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string notes { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string gtc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bond_documents { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string performance_bond { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offered_by_ptt { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reasonPTToffer { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offerVia { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reasonOfferVia { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offerDate { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string plan_month { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string plan_year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string requested_by { get; set; } = "";








        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CrudeSale_Optimization { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CrudeSale_Ref { get; set; } = "";
    }
}