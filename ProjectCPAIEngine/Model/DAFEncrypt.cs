﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class DAFEncrypt : PAFTransaction
    {
        [XmlElement(ElementName = "from_id")]
        public string from_id { get; set; }
        [XmlElement(ElementName = "for_company")]
        public string for_company { get; set; }
        [XmlElement(ElementName = "type_of_transaction")]
        public string type_of_transaction { get; set; }

        [XmlElement(ElementName = "demurrage_type")]
        public string demurrage_type { get; set; }
        [XmlElement(ElementName = "counterparty")]
        public string counterparty { get; set; }
        [XmlElement(ElementName = "vessel_name")]
        public string vessel_name { get; set; }
        [XmlElement(ElementName = "cp_date_contract_date")]
        public string cp_date_contract_date { get; set; }
        [XmlElement(ElementName = "agreed_loading_laycan")]
        public string agreed_loading_laycan { get; set; }
        [XmlElement(ElementName = "agreed_discharging_laycan")]
        public string agreed_discharging_laycan { get; set; }
        [XmlElement(ElementName = "agreed_laycan")]
        public string agreed_laycan { get; set; }
        [XmlElement(ElementName = "product_crude")]
        public string product_crude { get; set; }

        [XmlElement(ElementName = "time_on_demurrage")]
        public string time_on_demurrage { get; set; }
        [XmlElement(ElementName = "address_commission")]
        public string address_commission { get; set; }
        [XmlElement(ElementName = "demurrage_sharing_to_top")]
        public string demurrage_sharing_to_top { get; set; }
        [XmlElement(ElementName = "net_payment_demurrage_value")]
        public string net_payment_demurrage_value { get; set; }
        [XmlElement(ElementName = "net_receiving_demurrage_value")]
        public string net_receiving_demurrage_value { get; set; }
        [XmlElement(ElementName = "broker_commission")]
        public string broker_commission { get; set; }


        [XmlElement(ElementName = "contract_laytime")]
        public string contract_laytime { get; set; }
        [XmlElement(ElementName = "actual_net_laytime")]
        public string actual_net_laytime { get; set; }
        

        [XmlElement(ElementName = "demurrage_paid_settled")]
        public string demurrage_paid_settled { get; set; }
        [XmlElement(ElementName = "demurrage_received_settled")]
        public string demurrage_received_settled { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_DAFtrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<DAFEncrypt> DAFTransaction { get; set; }
    }
}