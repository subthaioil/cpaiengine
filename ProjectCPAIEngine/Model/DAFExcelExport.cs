using OfficeOpenXml;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Style;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALDAF;

namespace ProjectCPAIEngine.Model
{
    public class DAFExcelExport
    {
        public static string getFormatDateTime(Nullable<DateTime> d)
        {
            if (d == null)
            {
                return "";
            }
            return ((DateTime)d).ToString("dd/MM/yyyy hh:mm");
        }
        public static string getFormatDate(Nullable<DateTime> d, bool dash_default = false)
        {
            if (d == null)
            {
                return dash_default? "-" : "";
            }
            return ((DateTime)d).ToString("dd/MM/yyyy");
        }
        public void setCellFillColor(ExcelRange cell, Color bgColor, Color fontColor, Boolean isBold, Boolean isCenter)
        {
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(bgColor);
            cell.Style.Font.Color.SetColor(fontColor);
            cell.Style.Font.Bold = isBold;

            cell.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            cell.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            cell.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            cell.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            if (isCenter)
            {
                cell.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            }

        }

        public string GenExcel(DAF_DATA data, string file_name)
        {
            DAF_DATA_DAL dafMan = new DAF_DATA_DAL();

            string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile",  file_name);
            ShareFn fn = new ShareFn();

            FileInfo excelFile = new FileInfo(file_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {

                ExcelWorksheet ws = package.Workbook.Worksheets.Add("demurrage");
                ws.DefaultRowHeight = 15;
                ws.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;



                ws.Column(1).Width = 41.33;
                ws.Column(2).Width = 22.17;
                ws.Column(3).Width = 19.33;
                ws.Column(4).Width = 22.83;
                ws.Column(5).Width = 22.83;
                ws.Column(6).Width = 12.17;
                ws.Row(1).Height = 27;
                ws.Row(2).Height = 21;

                ws.Cells.Style.Font.SetFromFont(new Font("Calibri", 10));

                int row_count = 1;
                ws.Cells["A1:E1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ws.Cells["A1:E1"].Merge = true;
                ws.Cells["A1:E1"].Style.Font.SetFromFont(new Font("Calibri", 20));
                ws.Cells["A1:E1"].Value = "LAYTIME / DEMURRAGE CALCULATION";// + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                ws.Cells["A1:E1"].Style.Font.Color.SetColor(ColorTranslator.FromHtml("#2e62b1"));
                ws.Cells["A1:E1"].Style.Font.UnderLine = true;
                ws.Cells["A1:E1"].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                ws.Cells["A1:E1"].Style.Border.Left.Style = ExcelBorderStyle.Thick;
                ws.Cells["A1:E1"].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                ws.Cells["A1:E1"].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                row_count+=2;
                ws.Cells["A" + row_count].Value = "COUNTERPARTY";
                ws.Cells["A" + row_count].Style.Font.Bold = true;
                ws.Cells["B" + row_count].Value = dafMan.GetCounterpartyByTypeAndId(data.DDA_COUNTERPARTY_TYPE, data.DDA_SHIP_OWNER, data.DDA_BROKER, data.DDA_SUPPLIER, data.DDA_CUSTOMER, data.DDA_CUSTOMER_BROKER);
                row_count ++;

                //ws.Cells["A4"].Value = "ACCOUNT";
                //ws.Cells["A4"].Style.Font.Bold = true;
                //ws.Cells["B4:C4"].Merge = true;
                //ws.Cells["B4"].Value = dafMan.GetCompanyFromID(data.DDA_FOR_COMPANY);

                ws.Cells["A" + row_count].Value = "VESSEL";
                ws.Cells["A" + row_count].Style.Font.Bold = true;
                ws.Cells["B" + row_count].Value = dafMan.GetVesselName(data.DDA_VESSEL);
                row_count++;
                List<DAF_MATERIAL> materials = data.DAF_MATERIAL.ToList();
                ws.Cells["A" + row_count].Value = "PRODUCT/CRUDE";
                for (int i = 0; i < materials.Count(); i++)
                {
                    string mat_name = dafMan.GetMaterialName(materials[i].DMT_MET_NUM);
                    ws.Cells["A" + row_count].Style.Font.Bold = true;
                    ws.Cells["B" + row_count].Value = mat_name;
                    ws.Cells["C" + row_count].Value = "B/L DATE " + getFormatDate(materials[i].DMT_BL_DATE, true);
                    row_count++;
                }

                ws.Cells["A" + row_count].Value = "DEMURRAGE RATE";
                ws.Cells["A" + row_count].Style.Font.Bold = true;
                ws.Cells["B" + row_count].Value = string.Format("{0:N2}", data.DDA_DEMURAGE_RATE) + " " + data.DDA_CURRENCY_UNIT + " PDPR";
                row_count++;

                ws.Cells["A"+ row_count].Value = "LAYTIME ALLOWED";
                ws.Cells["A"+ row_count].Style.Font.Bold = true;
                ws.Cells["B"+ row_count].Value = string.Format("{0:N4}", data.DDA_LAYTIME_CONTRACT_HRS) + " HRS";
                if (data.DDA_LAYTIME_QUANTITY != null || data.DDA_LAYTIME_QUANTITY_RATE != null)
                {
                    ws.Cells["B" + row_count].Value = string.Format("{0:N4}", data.DDA_LAYTIME_CONTRACT_HRS) + " HRS (" + data.DDA_LAYTIME_QUANTITY_RATE + " MT/HRS, QTTY " + data.DDA_LAYTIME_QUANTITY + " MT)";
                }
                row_count++;

                ws.Cells["A" + row_count].Value = "CP DATE/ CONTRACT DATE";
                ws.Cells["A" + row_count].Style.Font.Bold = true;
                ws.Cells["B" + row_count].Value = getFormatDate(data.DDA_CONTRACT_DATE, true);
                row_count++;

                if (data.DDA_DEMURAGE_TYPE == "Paid")
                {
                    ws.Cells["A" + row_count].Value = "AGREED LOADING LAYCAN";
                    ws.Cells["A" + row_count].Style.Font.Bold = true;
                    ws.Cells["B" + row_count].Value = getFormatDate(data.DDA_AGREED_LOADING_LAYCAN_FROM) + " - " + getFormatDate(data.DDA_AGREED_LOADING_LAYCAN_TO);
                    row_count++;

                    ws.Cells["A" + row_count].Value = "AGREED DISCHARGING LAYCAN";
                    ws.Cells["A" + row_count].Style.Font.Bold = true;
                    ws.Cells["B" + row_count].Value = getFormatDate(data.DDA_AGREED_DC_LAYCAN_FROM) + " - " + getFormatDate(data.DDA_AGREED_DC_LAYCAN_TO);
                    row_count++;
                }
                else
                {
                    ws.Cells["A" + row_count].Value = "AGREED LAYCAN";
                    ws.Cells["A" + row_count].Style.Font.Bold = true;
                    ws.Cells["B" + row_count].Value = getFormatDate(data.DDA_AGREED_LAYCAN_FROM) + " - " + getFormatDate(data.DDA_AGREED_LAYCAN_TO);
                    row_count++;
                }
                ws.Cells["A" + row_count].Value = "SAILING ROUTE";
                ws.Cells["A" + row_count].Style.Font.Bold = true;
                ws.Cells["B" + row_count].Value = data.DDA_SAILING_ROUTE;
                row_count += 2;

                foreach (DAF_PORT e in data.DAF_PORT.OrderBy(m => m.DPT_ROW_ID).ToList())
                {

                    setCellFillColor(ws.Cells["A" + row_count + ":C" + row_count], Color.FromArgb(211, 228, 197), Color.Black, true, false);
                    ws.Cells["A" + row_count].Value = e.DPT_TYPE == "load" ? "LOAD PORT" : "DISCHARGE PORT";
                    ws.Cells["A" + row_count].Style.Font.Bold = true;
                    ws.Cells["B" + row_count + ":C" + row_count].Value = dafMan.GetPortName(e);
                    ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                    row_count++;

                    setCellFillColor(ws.Cells["A"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                    ws.Cells["A"+ row_count].Value = "RUNNING TIME";
                    setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                    ws.Cells["B"+ row_count + ":C"+row_count].Value = "DATE/TIME";
                    ws.Cells["B"+ row_count + ":C"+row_count].Merge = true;
                    row_count++;

                    setCellFillColor(ws.Cells["A"+ row_count], Color.White, Color.Black, false, false);
                    ws.Cells["A"+ row_count].Value = e.DPT_RT_T_START_ACTIVITY;
                    setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.White, Color.Black, false, true);
                    ws.Cells["B" + row_count + ":C" + row_count].Value = DAFGenerator.getFormatDate(e.DPT_RT_TOP_REVIEWED_T_START);
                    ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                    row_count++;

                    setCellFillColor(ws.Cells["A" + row_count], Color.White, Color.Black, false, false);
                    ws.Cells["A" + row_count].Value = e.DPT_RT_T_STOP_ACTIVITY;
                    setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.White, Color.Black, false, true);
                    ws.Cells["B" + row_count + ":C" + row_count].Value = DAFGenerator.getFormatDate(e.DPT_RT_TOP_REVIEWED_T_STOP);
                    ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                    row_count++;


                    setCellFillColor(ws.Cells["A" + row_count], Color.White, Color.Black, true, false);
                    ws.Cells["A" + row_count].Value = "TOTAL RUNNING TIME";
                    setCellFillColor(ws.Cells["B" + row_count], Color.White, Color.Black, true, true);
                    ws.Cells["B" + row_count].Value = e.DPT_RT_TOP_REVIEWED_T_TEXT;
                    setCellFillColor(ws.Cells["C" + row_count], Color.White, Color.Black, true, true);
                    ws.Cells["C" + row_count].Value = string.Format("{0:N4}", e.DPT_RT_TOP_REVIEWED_T_HRS) + " Hrs";
                    row_count++;

                    setCellFillColor(ws.Cells["A"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                    ws.Cells["A"+row_count].Value = "DEDUCTION TIME";
                    setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                    ws.Cells["B" + row_count + ":C" + row_count].Value = "HRS";
                    ws.Cells["B" + row_count + ":C" + row_count].Merge = true;

                    setCellFillColor(ws.Cells["D"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                    ws.Cells["D"+row_count].Value = "START DATE/TIME";
                    setCellFillColor(ws.Cells["E"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                    ws.Cells["E"+ row_count].Value = "END DATE/TIME";
                    row_count++;

                    foreach (DAF_DEDUCTION_ACTIVITY ee in e.DAF_DEDUCTION_ACTIVITY.OrderBy(m => m.DDD_ROW_ID).ToList())
                    {
                        setCellFillColor(ws.Cells["A" + row_count], Color.White, Color.Black, false, false);
                        ws.Cells["A" + row_count].Value = ee.DDD_ACTIVITY;
                        setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.White, Color.Black, false, true);
                        ws.Cells["B" + row_count + ":C" + row_count].Value = string.Format("{0:N4}", ee.DDD_TOP_REVIEWED_T_HRS) + " Hrs"; // ee.DDD_TOP_REVIEWED_T_HRS;
                        ws.Cells["B" + row_count + ":C" + row_count].Merge = true;

                        setCellFillColor(ws.Cells["D" + row_count], Color.White, Color.Black, false, true);
                        setCellFillColor(ws.Cells["E" + row_count], Color.White, Color.Black, false, true);
                        ws.Cells["D" + row_count].Value = DAFGenerator.getFormatDate(ee.DDD_TOP_REVIEWED_T_START);
                        ws.Cells["E" + row_count].Value = DAFGenerator.getFormatDate(ee.DDD_TOP_REVIEWED_T_STOP);
                        row_count++;
                    }
                    //setCellFillColor(ws.Cells["A"+row_count], Color.White, Color.Black, false, false);
                    //setCellFillColor(ws.Cells["B"+row_count], Color.White, Color.Black, false, true);
                    //setCellFillColor(ws.Cells["C"+row_count], Color.White, Color.Black, false, true);
                    //setCellFillColor(ws.Cells["D"+ row_count], Color.White, Color.Black, false, true);
                    //ws.Cells["A"+row_count].Value = "2ND TIME SHIP/SHORE PERFORMED SAFETY CHECK LIST";
                    //ws.Cells["B"+row_count].Value = "1.5000";
                    //ws.Cells["C"+row_count].Value = "14/12/2017 10:00";
                    //ws.Cells["D"+ row_count].Value = "14/12/2017 11:30";
                    //row_count++;

                    //setCellFillColor(ws.Cells["A"+row_count], Color.White, Color.Black, false, false);
                    //setCellFillColor(ws.Cells["B"+row_count], Color.White, Color.Black, false, true);
                    //setCellFillColor(ws.Cells["C"+row_count], Color.White, Color.Black, false, false);
                    //setCellFillColor(ws.Cells["D"+row_count], Color.White, Color.Black, false, true);
                    //ws.Cells["A"+row_count].Value = "2ND TIME TANK AND PUMPROOM INSPECTION";
                    //ws.Cells["B"+ row_count].Value = "0.5000";
                    //row_count++;

                    setCellFillColor(ws.Cells["A"+ row_count], Color.White, Color.Black, true, false);
                    setCellFillColor(ws.Cells["C"+ row_count], Color.White, Color.Black, false, false);
                    setCellFillColor(ws.Cells["D"+ row_count], Color.White, Color.Black, false, true);
                    setCellFillColor(ws.Cells["E"+ row_count], Color.White, Color.Black, false, true);
                    ws.Cells["A"+ row_count].Value = "TOTAL DEDUCTION TIME";

                    setCellFillColor(ws.Cells["B"+ row_count], Color.White, Color.Black, true, true);
                    ws.Cells["B"+ row_count].Value = e.DPT_D_TOP_REVIEWED_T_TEXT;
                    setCellFillColor(ws.Cells["C" + row_count], Color.White, Color.Black, true, true);
                    ws.Cells["C" + row_count].Value = string.Format("{0:N4}", e.DPT_D_TOP_REVIEWED_T_HRS) + " Hrs";
                    row_count++;

                    setCellFillColor(ws.Cells["A"+row_count], Color.FromArgb(255, 255, 0), Color.Black, true, false);
                    setCellFillColor(ws.Cells["B"+row_count], Color.FromArgb(255, 255, 0), Color.Black, true, true);
                    setCellFillColor(ws.Cells["C"+row_count], Color.FromArgb(255, 255, 0), Color.Black, true, true);
                    setCellFillColor(ws.Cells["D"+ row_count], Color.FromArgb(255, 255, 0), Color.Black, true, true);
                    setCellFillColor(ws.Cells["E"+ row_count], Color.FromArgb(255, 255, 0), Color.Black, true, true);
                    ws.Cells["A" + row_count].Value = "TIME USED";
                    ws.Cells["B" + row_count].Value = e.DPT_NTS_TOP_REVIEWED_T_TEXT;
                    ws.Cells["C" + row_count].Value = string.Format("{0:N4}", e.DPT_NTS_TOP_REVIEWED_T_HRS) + " Hrs";
                    row_count+=2;
                }

                row_count += 3;

                //ws.Cells["A26"].Value = "DISCHARGE PORT";
                //ws.Cells["A26"].Style.Font.Bold = true;
                //ws.Cells["B26"].Value = "SRIRACHA";

                //setCellFillColor(ws.Cells["A27"], Color.FromArgb(51, 63, 79), Color.White, true, true);
                //setCellFillColor(ws.Cells["B27"], Color.FromArgb(51, 63, 79), Color.White, true, true);
                //ws.Cells["A27"].Value = "RUNNING TIME";
                //ws.Cells["B27"].Value = "DATE/TIME";

                //setCellFillColor(ws.Cells["A28"], Color.White, Color.Black, false, false);
                //setCellFillColor(ws.Cells["B28"], Color.White, Color.Black, false, true);
                //ws.Cells["A28"].Value = "NOR+6";
                //ws.Cells["B28"].Value = "13/12/2017 18:00";

                //setCellFillColor(ws.Cells["A29"], Color.White, Color.Black, false, false);
                //setCellFillColor(ws.Cells["B29"], Color.White, Color.Black, false, true);
                //ws.Cells["A29"].Value = "HOSE DISCONNECTED";
                //ws.Cells["B29"].Value = "14/12/2017 09:10";

                //setCellFillColor(ws.Cells["A30"], Color.White, Color.Black, true, false);
                //setCellFillColor(ws.Cells["B30"], Color.White, Color.Black, true, true);
                //ws.Cells["A30"].Value = "TOTAL RUNNING TIME";
                //ws.Cells["B30"].Value = "15.1667";

                //setCellFillColor(ws.Cells["A31"], Color.FromArgb(51, 63, 79), Color.White, true, true);
                //setCellFillColor(ws.Cells["B31"], Color.FromArgb(51, 63, 79), Color.White, true, true);
                //setCellFillColor(ws.Cells["C31"], Color.FromArgb(51, 63, 79), Color.White, true, true);
                //setCellFillColor(ws.Cells["D31"], Color.FromArgb(51, 63, 79), Color.White, true, true);
                //ws.Cells["A31"].Value = "DEDUCTION TIME";
                //ws.Cells["B31"].Value = "HRS";
                //ws.Cells["C31"].Value = "START";
                //ws.Cells["D31"].Value = "END";

                //setCellFillColor(ws.Cells["A32"], Color.White, Color.Black, false, false);
                //setCellFillColor(ws.Cells["B32"], Color.White, Color.Black, false, true);
                //setCellFillColor(ws.Cells["C32"], Color.White, Color.Black, false, true);
                //setCellFillColor(ws.Cells["D32"], Color.White, Color.Black, false, true);
                //ws.Cells["A32"].Value = "SHIFTING";
                //ws.Cells["B32"].Value = "2.0000";

                //setCellFillColor(ws.Cells["A33"], Color.White, Color.Black, false, false);
                //setCellFillColor(ws.Cells["B33"], Color.White, Color.Black, false, true);
                //setCellFillColor(ws.Cells["C33"], Color.White, Color.Black, true, false);
                //setCellFillColor(ws.Cells["D33"], Color.White, Color.Black, true, true);
                //ws.Cells["A33"].Value = "TOTAL DEDUCTION TIME";
                //ws.Cells["B33"].Value = "2.0000";

                //setCellFillColor(ws.Cells["A34"], Color.FromArgb(255, 255, 0), Color.Black, true, false);
                //setCellFillColor(ws.Cells["B34"], Color.FromArgb(255, 255, 0), Color.Black, true, true);
                //setCellFillColor(ws.Cells["C34"], Color.FromArgb(255, 255, 0), Color.Black, true, false);
                //setCellFillColor(ws.Cells["D34"], Color.FromArgb(255, 255, 0), Color.Black, true, true);
                //ws.Cells["A34"].Value = "TIME USED";
                //ws.Cells["B34"].Value = "13.1667";

                setCellFillColor(ws.Cells["A"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                setCellFillColor(ws.Cells["B"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                setCellFillColor(ws.Cells["C"+ row_count], Color.FromArgb(51, 63, 79), Color.White, true, true);
                ws.Cells["A"+ row_count].Value = "SUMMARY";
                //ws.Cells["B"+ row_count].Value = "HRS";
                row_count++;

                setCellFillColor(ws.Cells["A"+ row_count], Color.White, Color.Black, false, false);
                setCellFillColor(ws.Cells["B"+ row_count], Color.White, Color.Black, false, true);
                setCellFillColor(ws.Cells["C"+ row_count], Color.White, Color.Black, false, true);
                ws.Cells["A"+row_count].Value = "TOTAL TIME USED";
                ws.Cells["B"+ row_count].Value = data.DDA_NTS_TOP_REVIEWED_TIME_TEXT;
                ws.Cells["C"+ row_count].Value = string.Format("{0:N4}", data.DDA_NTS_TOP_REVIEWED_HRS) + " Hrs";
                row_count++;

                setCellFillColor(ws.Cells["A" + row_count], Color.White, Color.Black, false, false);
                setCellFillColor(ws.Cells["B" + row_count], Color.White, Color.Black, false, true);
                setCellFillColor(ws.Cells["C" + row_count], Color.White, Color.Black, false, true);
                ws.Cells["A" + row_count].Value = "ALLOWED LAYTIME";
                ws.Cells["B"+ row_count].Value = data.DDA_LAYTIME_CONTRACT_TEXT;
                ws.Cells["C" + row_count].Value = string.Format("{0:N4}", data.DDA_LAYTIME_CONTRACT_HRS) + " Hrs";
                row_count++;

                setCellFillColor(ws.Cells["A" + row_count], Color.FromArgb(194, 214, 236), Color.Black, false, false);
                setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.FromArgb(194, 214, 236), Color.Black, false, true);
                ws.Cells["A" + row_count].Value = "TIME ON DEMURRAGE";
                ws.Cells["B"+ row_count].Value = data.DDA_TOD_TOP_REVIEWED_TIME_TEXT;
                ws.Cells["C" + row_count].Value = string.Format("{0:N4}", data.DDA_TOD_TOP_REVIEWED_HRS) + " Hrs";
                row_count++;

                setCellFillColor(ws.Cells["A"+ row_count], Color.White, Color.Black, false, false);
                setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.White, Color.Black, false, true);
                ws.Cells["A"+ row_count].Value = "DEMURRAGE RATE (" + data.DDA_CURRENCY_UNIT + " PDPR)";
                ws.Cells["B" + row_count + ":C" + row_count].Value = string.Format("{0:N2}", data.DDA_DEMURAGE_RATE);
                ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                row_count++;

                if (data.DDA_IS_ADDRESS_COMMISSION == "Y")
                {
                    setCellFillColor(ws.Cells["A" + row_count], Color.White, Color.Black, false, false);
                    setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.White, Color.Black, false, true);
                    ws.Cells["A" + row_count].Value = "GROSS DEMURRAGE (" + data.DDA_CURRENCY_UNIT + ")";
                    ws.Cells["B" + row_count + ":C" + row_count].Value = string.Format("{0:N2}", data.DDA_GD_SETTLED_VALUE);
                    ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                    row_count++;

                    setCellFillColor(ws.Cells["A" + row_count], Color.White, Color.Black, false, false);
                    setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.White, Color.Black, false, true);
                    ws.Cells["A" + row_count].Value = "ADDRESS COMMISSION " + string.Format("{0:N2}", data.DDA_ADDRESS_COMMISSION_PERCEN) + '%' + " (" + data.DDA_CURRENCY_UNIT + ")";
                    ws.Cells["B" + row_count + ":C" + row_count].Value = string.Format("{0:N2}", data.DDA_AC_SETTLED_VALUE);
                    ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                    row_count++;
                }


                setCellFillColor(ws.Cells["A"+ row_count], Color.FromArgb(0, 176, 240), Color.Black, true, false);
                setCellFillColor(ws.Cells["B" + row_count + ":C" + row_count], Color.FromArgb(0, 176, 240), Color.Black, true, true);
                ws.Cells["A"+ row_count].Value = "NET DEMURRAGE (" + data.DDA_CURRENCY_UNIT + ")";
                ws.Cells["B" + row_count + ":C" + row_count].Value = string.Format("{0:N2}", data.DDA_NET_PAYMENT_DEM_VALUE);
                ws.Cells["B" + row_count + ":C" + row_count].Merge = true;
                row_count++;



                ws.PrinterSettings.Orientation = eOrientation.Landscape;
                ws.PrinterSettings.PaperSize = ePaperSize.A4;
                ws.PrinterSettings.FitToPage = true;
                ws.Column(45).PageBreak = true;

                package.Save();
            }

            string url = fn.GetSiteRootUrl("Web/Report/TmpFile/").Replace("\\", "/") + file_name;
            return url;

            //Response.Clear();
            //Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
            //Response.AddHeader("content-type", "application/Excel");
            //Response.WriteFile(file_path);
            //Response.End();

            //return View();
        }
    }
}
