﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class DAFGenerator
    {
        private static double NORMAL_ROW_HEIGHT = 10;
        private static ShareFn _FN = new ShareFn();

        public static String getFullNameCountry(String shortName)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            var d = db.Database.SqlQuery<string>("SELECT MCT_LANDX FROM MT_COUNTRY WHERE MCT_LAND1 = '" + shortName + "'");
            if (d != null)
            {
                return d.ElementAt(0);
            }
            return null;
        }

        public static String getMinMaxFormat(Nullable<Decimal> min, Nullable<Decimal> max)
        {
            if (min != max)
            {
                return string.Format("{0:n0}", min) + " - " + string.Format("{0:n0}", max);
            }
            else
            {
                return string.Format("{0:n0}", min);
            }
        }

        public static Nullable<decimal> getDemSettled(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            var d = db.Database.SqlQuery<Nullable<decimal>>("SELECT DDA_NET_PAYMENT_DEM_VALUE_OV FROM DAF_DATA WHERE DDA_ROW_ID = '" + id + "'");
            if (d != null)
            {
                return d.ElementAt(0);
            }
            return null;
        }

        public static String getPriceMinMaxFormat(Nullable<Decimal> min, Nullable<Decimal> max)
        {
            if (min != max)
            {
                return string.Format("{0:n}", min) + " - " + string.Format("{0:n}", max);
            }
            else
            {
                return string.Format("{0:n}", min);
            }
        }

        public static String getLoadPortName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            var list = db.Database.SqlQuery<string>("SELECT DPT_LOAD_PORT_NAME FROM DAF_PORT WHERE DPT_ROW_ID = " + id);
            if (list.Count() > 0)
            {
                if (list.ElementAt(0) != null)
                {
                    return list.ElementAt(0);
                }
            }
            return "";
        }

        public static String getNameFromCustNum(String custNum, String companyCode)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MT_CUST_DETAIL.Where(c => c.MCD_FK_CUS == custNum && c.MCD_FK_COMPANY == companyCode && c.MCD_NATION == "I").FirstOrDefault() == null)
            {
                return "";
            }
            else
            {
                MT_CUST_DETAIL cd = db.MT_CUST_DETAIL.Where(c => c.MCD_FK_CUS == custNum && c.MCD_FK_COMPANY == companyCode && c.MCD_NATION == "I").FirstOrDefault();
                String name = cd.MCD_NAME_1 + " " + cd.MCD_NAME_2;
                return name;
            }
        }

        public static String getNameFromVendor(String vendor)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            return db.MT_VENDOR.Where(c => c.VND_ACC_NUM_VENDOR == vendor).FirstOrDefault().VND_NAME1 + "";
        }

        public static String getNameFromBenchMark(decimal benchMark) // done
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MKT_MST_MOPS_PRODUCT.Where(c => c.M_MPR_ID == benchMark).FirstOrDefault() != null)
            {
                return db.MKT_MST_MOPS_PRODUCT.Where(c => c.M_MPR_ID == benchMark).FirstOrDefault().M_MPR_PRDNAME + "";
            }
            return "";
        }

        public static String getNameFromCountry(String country) // done
        {

            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MT_COUNTRY.Where(c => c.MCT_ROW_ID == country).FirstOrDefault() != null)
            {
                return db.MT_COUNTRY.Where(c => c.MCT_ROW_ID == country).FirstOrDefault().MCT_LANDX + "";
            }
            return "";
        }

        public static String getNameFromCompany(String company) // done
        {

            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MT_COMPANY.Where(c => c.MCO_COMPANY_CODE == company).FirstOrDefault() != null)
            {
                return db.MT_COMPANY.Where(c => c.MCO_COMPANY_CODE == company).FirstOrDefault().MCO_SHORT_NAME + "";
            }
            return "";
        }

        public static string getSignatureImg(string UserName)
        {
            List<string> extentions = new List<string>(); extentions.Add(".jpg"); extentions.Add(".png"); extentions.Add(".gif");
            ShareFn sFn = new ShareFn();
            string pathSignature = "";
            foreach (string _itemExt in extentions)
            {
                if (File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", UserName) + _itemExt))
                {
                    pathSignature = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", UserName) + _itemExt;
                    break;
                }
            }
            return pathSignature;
        }

        public static String getNameOfUser(string userName)
        {

            using (var context = new EntityCPAIEngine())
            {
                var users = context.USERS.Where(x => x.USR_LOGIN.ToUpper() == userName.ToUpper() && x.USR_STATUS == "ACTIVE").ToList();
                if (users.Count > 0)
                {
                    return users.FirstOrDefault().USR_FIRST_NAME_EN.ToUpper() + "";
                }
                else
                {
                    return "";
                }
            }


        }
        public static List<CPAI_DATA_HISTORY> getHistoryByTxns(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef && x.DTH_REJECT_FLAG == "N").ToList();
                if (results != null)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }

        public static String genPDF(DAF_DATA dafData)
        {
            dafData.DAF_ATTACH_FILE = dafData.DAF_ATTACH_FILE.OrderBy(b => b.DAT_ROW_ID).ToList();
            dafData.DAF_PORT = dafData.DAF_PORT.OrderBy(b => b.DPT_ROW_ID).ToList();
            dafData.DAF_MATERIAL = dafData.DAF_MATERIAL.OrderBy(b => b.DMT_ROW_ID).ToList();
            for (int i = 0; i < dafData.DAF_PORT.Count; i++)
            {
                dafData.DAF_PORT.ElementAt(i).DAF_DEDUCTION_ACTIVITY = dafData.DAF_PORT.ElementAt(i).DAF_DEDUCTION_ACTIVITY.OrderBy(b => b.DDD_ROW_ID).ToList();
            }


            Document document = new Document();
            document.DefaultPageSetup.RightMargin = 18;
            document.DefaultPageSetup.LeftMargin = 18;
            document.DefaultPageSetup.TopMargin = 18;
            document.DefaultPageSetup.BottomMargin = 25;
            document.AddSection();

            Style style = document.Styles["Normal"];
            style.Font.Name = "Tahoma";
            style.Font.Size = 5;

            var logoImage = document.LastSection.AddImage(_FN.GetPathLogo());
            string x = _FN.GetPathLogo();
            logoImage.WrapFormat.DistanceLeft = 460;
            logoImage.Width = "2.0cm";
            logoImage.LockAspectRatio = true;


            Paragraph header = document.LastSection.AddParagraph("Demurrage Approval Form");
            header.Format.Alignment = ParagraphAlignment.Center;
            header.Format.Font.Bold = true;
            header.Format.Font.Size = 9;

            genTable1(document, dafData);
            genTable2(document, dafData);
            genTable3(document, dafData);
            genTable4(document, dafData);
            genTable5(document, dafData);
            genTable6(document, dafData);
            genTable7(document, dafData);
            genTable8(document, dafData);
            genTable9(document, dafData);

            PdfDocumentRenderer docRend = new PdfDocumentRenderer(true);
            docRend.Document = document;
            docRend.RenderDocument();
            //string fileName = "C:\\test\\doc.pdf";
            String n = string.Format("DAF{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", n);
            docRend.PdfDocument.Save(fileName);
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.FileName = fileName;
            Process.Start(processInfo);

            return n;
        }

        public static void genTable1(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = 8;

            table.Format.Font.Bold = true;
            Column column1 = table.AddColumn(Unit.FromCentimeter(5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(11));
            Column column3 = table.AddColumn(Unit.FromCentimeter(4));
            Row row1 = table.AddRow();
            row1.Cells[1].Borders.Top.Visible = false;

            row1.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row1.Cells[0].AddParagraph("Reference No : " + dafData.DDA_FORM_ID);

            //if (dafData.DDA_CREATED != null)
            //{
            //    row1.Cells[2].AddParagraph("Date: " + ((DateTime)dafData.DDA_CREATED).ToString("dd-MMM-yyyy"));
            //}
            if (dafData.DDA_STATUS == "DRAFT")
            {
                row1.Cells[2].AddParagraph("Date: " + DateTime.Now.ToString("dd-MMM-yyyy"));
            }
            else
            {
                var listHistory = getHistoryByTxns(dafData.DDA_ROW_ID);
                var hSubmit = listHistory.Where(h => h.DTH_ACTION == "SUBMIT").ToList().FirstOrDefault();
                if (hSubmit != null)
                {
                    if (hSubmit.DTH_ACTION_DATE != null)
                    {
                        row1.Cells[2].AddParagraph("Date: " + ((DateTime)hSubmit.DTH_ACTION_DATE).ToString("dd-MMM-yyyy"));
                    }
                }
            }


            table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable2(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Font.Size = 8;

            Column column1 = table.AddColumn(Unit.FromCentimeter(5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(15));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();

            column1.Format.Font.Bold = true;

            column2.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("For:");
            row1.Cells[1].AddParagraph(getNameFromCompany(dafData.DDA_FOR_COMPANY) + "");

            row2.Cells[0].AddParagraph("Type of Transaction :");
            row2.Cells[1].AddParagraph(dafData.DDA_TYPE_OF_TRANSECTION + "");

            row3.Cells[0].AddParagraph("Incoterms :");
            row3.Cells[1].AddParagraph(dafData.DDA_INCOTERM + "");

            row4.Cells[0].AddParagraph("Demurrage :");
            row4.Cells[1].AddParagraph(dafData.DDA_DEMURAGE_TYPE + "");

            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.4;
            doc.LastSection.Add(table);
        }

        public static void genTable3(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Format.Font.Size = 8;
            table.Rows.Height = NORMAL_ROW_HEIGHT;

            Column column1 = table.AddColumn(Unit.FromCentimeter(4.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(7.4));
            Column column3 = table.AddColumn(Unit.FromCentimeter(4.5));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1.8));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.8));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();
            Row row5 = table.AddRow();
            Row row6 = table.AddRow();
            Row row7 = table.AddRow();
            Row row8 = table.AddRow();
            Row row9 = table.AddRow();
            Row row10 = table.AddRow();
            Row row11 = table.AddRow();

            column1.Borders.Right.Width = 0;
            column2.Borders.Left.Width = 0;

            column1.Format.Font.Bold = true;
            column3.Format.Font.Bold = true;
            column5.Format.Font.Bold = true;

            column4.Format.Alignment = ParagraphAlignment.Right;

            row3.Cells[2].MergeDown = 4;
            row3.Cells[3].MergeDown = 4;
            row3.Cells[4].MergeDown = 4;

            row3.Cells[2].Borders.Bottom.Width = 0.4;
            row3.Cells[3].Borders.Bottom.Width = 0.4;
            row3.Cells[4].Borders.Bottom.Width = 0.4;


            row2.Cells[0].AddParagraph("DETAILS");
            row2.Cells[0].AddParagraph("");
            row2.Cells[0].Format.Font.Underline = Underline.Single;

            row3.Cells[0].AddParagraph("Counterparty");
            row4.Cells[0].AddParagraph("GT&C or CP Type");
            row5.Cells[0].AddParagraph("Vessel Name");
            row6.Cells[0].AddParagraph("CP Date/Contract Date");

            if (dafData.DDA_SHIP_OWNER != null)
            {
                Paragraph p = row3.Cells[1].AddParagraph("");
                p.AddFormattedText("Ship Owner : ", TextFormat.Bold);
                p.AddFormattedText(dafData.DDA_SHIP_OWNER + "", TextFormat.NotBold);
                // Alt 255
                if (dafData.DDA_BROKER != null)
                {
                    Paragraph p1 = row3.Cells[1].AddParagraph("");
                    p1.AddFormattedText(" via Broker : ", TextFormat.Bold);
                    p1.AddFormattedText(getNameFromVendor(dafData.DDA_BROKER) + "", TextFormat.NotBold);
                }
            }
            else if (dafData.DDA_SUPPLIER != null)
            {
                Paragraph p = row3.Cells[1].AddParagraph("");
                p.AddFormattedText("Supplier as Charterer : ", TextFormat.Bold);
                p.AddFormattedText(getNameFromVendor(dafData.DDA_SUPPLIER) + "", TextFormat.NotBold);
            }
            else if (dafData.DDA_CUSTOMER != null)
            {
                Paragraph p = row3.Cells[1].AddParagraph("");
                p.AddFormattedText("Customer : ", TextFormat.Bold);
                p.AddFormattedText(getNameFromCustNum(dafData.DDA_CUSTOMER, dafData.DDA_FOR_COMPANY) + "");

                if (dafData.DDA_CUSTOMER_BROKER != null)
                {
                    Paragraph p1 = row3.Cells[1].AddParagraph("");
                    p1.AddFormattedText(" via Broker : ", TextFormat.Bold);
                    p1.AddFormattedText(getNameFromVendor(dafData.DDA_CUSTOMER_BROKER) + "", TextFormat.NotBold);
                }
            }
            else
            {
                row3.Cells[1].AddParagraph("-");
            }
            if (dafData.DDA_GT_C == null)
            {
                row4.Cells[1].AddParagraph("-");
            }
            else
            {
                row4.Cells[1].AddParagraph(dafData.DDA_GT_C + "");
            }
            if (dafData.MT_VEHICLE != null)
            {
                row5.Cells[1].AddParagraph(dafData.MT_VEHICLE.VEH_VEH_TEXT + "");
            }
            else
            {
                row5.Cells[1].AddParagraph("-");
            }

            if (dafData.DDA_CONTRACT_DATE != null)
            {
                row6.Cells[1].AddParagraph(((DateTime)dafData.DDA_CONTRACT_DATE).ToString("dd MMMM yyyy"));
            }
            else
            {
                row6.Cells[1].AddParagraph("-");
            }

            row3.Cells[2].AddParagraph("Laytime Contract");
            row3.Cells[2].AddParagraph("Net Time Spent");
            row3.Cells[2].AddParagraph("Time Settled");
            row3.Cells[2].AddParagraph("");

            row3.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_LAYTIME_CONTRACT_HRS));
            row3.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_NTS_SETTLED_HRS));
            row3.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_TOD_SETTLED_HRS));
            row3.Cells[3].AddParagraph("");

            row3.Cells[4].AddParagraph("Hrs");
            row3.Cells[4].AddParagraph("Hrs");
            row3.Cells[4].AddParagraph("Hrs");
            row3.Cells[4].AddParagraph("");

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                Row row12 = table.AddRow();

                row7.Cells[0].AddParagraph("Agreed Loading Laycan");
                row8.Cells[0].AddParagraph("Agreed Discharging Laycan");
                row9.Cells[0].AddParagraph("Type of Crude/Product");
                row10.Cells[0].AddParagraph("Sailing Route");

                if (dafData.DDA_AGREED_LOADING_LAYCAN_FROM != null && dafData.DDA_AGREED_LOADING_LAYCAN_TO != null)
                {
                    row7.Cells[1].AddParagraph(((DateTime)dafData.DDA_AGREED_LOADING_LAYCAN_FROM).ToString("dd MMMM yyyy") + " - " + ((DateTime)dafData.DDA_AGREED_LOADING_LAYCAN_TO).ToString("dd MMMM yyyy"));
                }
                else
                {
                    row7.Cells[1].AddParagraph("-");
                }
                if (dafData.DDA_AGREED_DC_LAYCAN_FROM != null && dafData.DDA_AGREED_DC_LAYCAN_TO != null)
                {
                    row8.Cells[1].AddParagraph(((DateTime)dafData.DDA_AGREED_DC_LAYCAN_FROM).ToString("dd MMMM yyyy") + " - " + ((DateTime)dafData.DDA_AGREED_DC_LAYCAN_TO).ToString("dd MMMM yyyy"));
                }
                else
                {
                    row8.Cells[1].AddParagraph("-");
                }
                for (int i = 0; i < dafData.DAF_MATERIAL.Count; i++)
                {
                    Paragraph p = row9.Cells[1].AddParagraph("");
                    
                    if (dafData.DAF_MATERIAL.ElementAt(i).MT_MATERIALS != null)
                    {
                        p.AddText(dafData.DAF_MATERIAL.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                        
                    }
                    p.AddLineBreak();
                    p.AddFormattedText(" B/L Date : ", TextFormat.Bold);

                    if (dafData.DAF_MATERIAL.ElementAt(i).DMT_BL_DATE != null)
                    {
                        p.AddFormattedText(((DateTime)dafData.DAF_MATERIAL.ElementAt(i).DMT_BL_DATE).ToString("dd MMMM yyyy"));
                    }
                    else
                    {
                        p.AddFormattedText("-");
                    }
                }
                if (dafData.DAF_MATERIAL.Count == 0)
                {
                    row9.Cells[1].AddParagraph("-");
                }

                if (dafData.DDA_SAILING_ROUTE == null)
                {
                    row10.Cells[1].AddParagraph("-");
                }
                else
                {
                    row10.Cells[1].AddParagraph(dafData.DDA_SAILING_ROUTE + "");
                }


                row8.Cells[2].MergeDown = 4;
                row8.Cells[3].MergeDown = 4;
                row8.Cells[4].MergeDown = 4;

                row8.Cells[2].AddParagraph("");
                row8.Cells[2].AddParagraph("Demurrage Rate");

                row8.Cells[2].AddParagraph("Original Demurrage Claimed");
                row8.Cells[2].AddParagraph("Demurrage Paid Settled");

                row8.Cells[2].AddParagraph("Demurrage Saving");
                row8.Cells[2].AddParagraph("");

                row8.Cells[3].AddParagraph("");
                row8.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_DEMURAGE_RATE));
                row8.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_ORIGIN_CLAIM_VALUE));
                row8.Cells[3].AddParagraph(string.Format("{0:N2}", getDemSettled(dafData.DDA_ROW_ID)));
                row8.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SAVING_VALUE));
                row8.Cells[3].AddParagraph("");

                row8.Cells[4].AddParagraph("");
                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + " PDPR");
                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + "");
                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + "");
                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + "");
            }
            else if (dafData.DDA_DEMURAGE_TYPE == "Received")
            {
                row7.Cells[0].AddParagraph("Agreed Laycan");
                row8.Cells[0].AddParagraph("Type of Crude/Product");
                row9.Cells[0].AddParagraph("Sailing Route");

                if (dafData.DDA_AGREED_LAYCAN_FROM != null && dafData.DDA_AGREED_LAYCAN_TO != null)
                {
                    row7.Cells[1].AddParagraph(((DateTime)dafData.DDA_AGREED_LAYCAN_FROM).ToString("dd MMMM yyyy") + " - " + ((DateTime)dafData.DDA_AGREED_LAYCAN_TO).ToString("dd MMMM yyyy"));
                }
                else
                {
                    row7.Cells[1].AddParagraph("-");
                }
                for (int i = 0; i < dafData.DAF_MATERIAL.Count; i++)
                {
                    Paragraph p = row8.Cells[1].AddParagraph();
                    
                    p.AddText(dafData.DAF_MATERIAL.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                    p.AddLineBreak();
                    p.AddFormattedText(" B/L Date : ", TextFormat.Bold);
                    if (dafData.DAF_MATERIAL.ElementAt(i).DMT_BL_DATE != null)
                    {
                        p.AddFormattedText(((DateTime)dafData.DAF_MATERIAL.ElementAt(i).DMT_BL_DATE).ToString("dd MMMM yyyy"));
                    }
                    else
                    {
                        p.AddFormattedText("-");
                    }
                }
                if (dafData.DAF_MATERIAL.Count == 0)
                {
                    row8.Cells[1].AddParagraph("-");
                }

                if (dafData.DDA_SAILING_ROUTE == null)
                {
                    row9.Cells[1].AddParagraph("-");
                }
                else
                {
                    row9.Cells[1].AddParagraph(dafData.DDA_SAILING_ROUTE + "");
                }


                row8.Cells[2].MergeDown = 3;
                row8.Cells[3].MergeDown = 3;
                row8.Cells[4].MergeDown = 3;

                row8.Cells[2].AddParagraph("Demurrage Rate");
                if (dafData.DDA_SUPPLIER != null)
                {
                    row8.Cells[2].AddParagraph("Original Demurrage Sharing");
                    row8.Cells[2].AddParagraph("Demurrage Received Settled");
                }
                else if (dafData.DDA_CUSTOMER != null)
                {
                    row8.Cells[2].AddParagraph("Original Demurrage Claimed");
                    row8.Cells[2].AddParagraph("Demurrage Received Settled");
                }
                else
                {
                    row8.Cells[2].AddParagraph("Original Demurrage Claimed");
                    row8.Cells[2].AddParagraph("Demurrage Settled");
                }
                row8.Cells[2].AddParagraph("");

                row8.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_DEMURAGE_RATE));
                row8.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_ORIGIN_CLAIM_VALUE));
                row8.Cells[3].AddParagraph(string.Format("{0:N2}", getDemSettled(dafData.DDA_ROW_ID)));
                row8.Cells[3].AddParagraph("");

                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + " PDPR");
                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + "");
                row8.Cells[4].AddParagraph(dafData.DDA_CURRENCY_UNIT + "");
            }

            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.4;
            doc.LastSection.Add(table);
        }

        public static void genTable4(Document doc, DAF_DATA dafData)
        {
            try
            {

                Table table = new Table();
                table.Borders.Left.Width = 0.4;
                table.Borders.Right.Width = 0.4;


                table.Rows.Height = NORMAL_ROW_HEIGHT;
                table.Rows.VerticalAlignment = VerticalAlignment.Center;
                table.Format.Alignment = ParagraphAlignment.Center;

                Column column1 = table.AddColumn();
                Column column2 = table.AddColumn();
                Column column3 = table.AddColumn();
                Column column4 = table.AddColumn();
                Column column5 = table.AddColumn();
                Column column6 = table.AddColumn();
                Column column7 = table.AddColumn();
                Column column8 = table.AddColumn();
                Column column9 = table.AddColumn();
                Column column10 = table.AddColumn();
                Column column11 = table.AddColumn();
                Column column12 = table.AddColumn();

                if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                {
                    column1.Width = Unit.FromCentimeter(1);
                    column2.Width = Unit.FromCentimeter(1.6);
                    column3.Width = Unit.FromCentimeter(1.4);
                    column4.Width = Unit.FromCentimeter(1.4);
                    column5.Width = Unit.FromCentimeter(2.3);
                    column6.Width = Unit.FromCentimeter(1.35);
                    column7.Width = Unit.FromCentimeter(2.3);
                    column8.Width = Unit.FromCentimeter(1.35);
                    column9.Width = Unit.FromCentimeter(2.3);
                    column10.Width = Unit.FromCentimeter(1.35);
                    column11.Width = Unit.FromCentimeter(2.3);
                    column12.Width = Unit.FromCentimeter(1.35);
                }
                else if (dafData.DDA_DEMURAGE_TYPE == "Received")
                {
                    column1.Width = Unit.FromCentimeter(1);
                    column2.Width = Unit.FromCentimeter(3);
                    column3.Width = Unit.FromCentimeter(1);
                    column4.Width = Unit.FromCentimeter(3);
                    column5.Width = Unit.FromCentimeter(2.5);
                    column6.Width = Unit.FromCentimeter(1.5);
                    column7.Width = Unit.FromCentimeter(2.5);
                    column8.Width = Unit.FromCentimeter(1.5);
                    column9.Width = Unit.FromCentimeter(2.5);
                    column10.Width = Unit.FromCentimeter(1.5);
                    column11.Width = Unit.FromCentimeter(0);
                    column12.Width = Unit.FromCentimeter(0);
                }

                column5.Borders.Right.Width = 0;
                column6.Borders.Left.Width = 0;
                column7.Borders.Right.Width = 0;
                column8.Borders.Left.Width = 0;
                column9.Borders.Right.Width = 0;
                column10.Borders.Left.Width = 0;
                column11.Borders.Right.Width = 0;
                column12.Borders.Left.Width = 0;

                for (int i = 0; i < dafData.DAF_PORT.Count; i++)
                {
                    DAF_PORT port = dafData.DAF_PORT.ElementAt(i);
                    Row row1 = table.AddRow();
                    Row row2 = table.AddRow();
                    Row row3 = table.AddRow();
                    Row row4 = table.AddRow();
                    Row row5 = table.AddRow();
                    Row row6 = table.AddRow();


                    row1.Borders.Top.Width = 0.4;
                    row1.Cells[0].MergeRight = 11;

                    row2.Cells[0].MergeDown = 3;

                    row2.Cells[2].MergeRight = 1;
                    row2.Cells[4].MergeRight = 1;
                    row2.Cells[6].MergeRight = 1;
                    row2.Cells[8].MergeRight = 1;
                    row2.Cells[10].MergeRight = 1;

                    row3.Cells[4].MergeRight = 1;
                    row3.Cells[6].MergeRight = 1;
                    row3.Cells[8].MergeRight = 1;

                    row4.Cells[4].MergeRight = 1;
                    row4.Cells[6].MergeRight = 1;
                    row4.Cells[8].MergeRight = 1;

                    row5.Cells[2].MergeRight = 1;

                    row6.Cells[2].MergeRight = 1;
                    row6.Cells[4].MergeRight = 1;
                    row6.Cells[6].MergeRight = 1;
                    row6.Cells[8].MergeRight = 1;
                    row6.Cells[10].MergeRight = 1;

                    row6.Cells[0].MergeDown = port.DAF_DEDUCTION_ACTIVITY.Count + 1;

                    if (port.DPT_TYPE == "load")
                    {
                        row1.Cells[0].AddParagraph("Load Port");
                        row2.Cells[1].AddParagraph("Load Port Name");
                    }
                    else if (port.DPT_TYPE == "discharge")
                    {
                        row1.Cells[0].AddParagraph("Discharge Port");
                        row2.Cells[1].AddParagraph("Discharge Port Name");
                    }
                    row1.Cells[0].Format.Alignment = ParagraphAlignment.Left;

                    row2.Cells[0].AddParagraph("Running Time");


                    row2.Cells[2].AddParagraph("Activity");
                    row2.Cells[4].AddParagraph("Original Claim/Original Time Spent");
                    if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                    {
                        row2.Cells[6].AddParagraph("TOP Reviewed");
                    }
                    else
                    {
                        row2.Cells[6].AddParagraph("Customer/Charterer Counter");
                    }

                    row2.Cells[8].AddParagraph("Settled");

                    if (port.DPT_LOAD_PORT != null)
                    {
                        row3.Cells[1].AddParagraph(port.MT_PORT.MLP_LOADING_PORT_NAME + "");
                    }
                    else if (port.DPT_LOAD_PORT_FK_JETTY != null)
                    {
                        if ((port.MT_JETTY.MTJ_JETTY_NAME + " " + port.MT_JETTY.MTJ_LOCATION + " " + getFullNameCountry(port.MT_JETTY.MTJ_MT_COUNTRY)).Contains("OTHER "))
                        {
                            row3.Cells[1].AddParagraph((port.MT_JETTY.MTJ_JETTY_NAME + " " + port.MT_JETTY.MTJ_LOCATION + " " + getFullNameCountry(port.MT_JETTY.MTJ_MT_COUNTRY)).Replace("OTHER ", ""));
                        }
                        else
                        {
                            row3.Cells[1].AddParagraph(port.MT_JETTY.MTJ_JETTY_NAME + " " + port.MT_JETTY.MTJ_LOCATION + " " + getFullNameCountry(port.MT_JETTY.MTJ_MT_COUNTRY));
                        }

                    }
                    row3.Cells[1].Format.Font.Size = 7;


                    row3.Cells[2].AddParagraph("Start");
                    row3.Cells[3].AddParagraph(port.DPT_RT_T_START_ACTIVITY + "");
                    row3.Cells[4].AddParagraph(getFormatDate(port.DPT_RT_ORIGIN_CLAIM_T_START));
                    row3.Cells[6].AddParagraph(getFormatDate(port.DPT_RT_TOP_REVIEWED_T_START));
                    row3.Cells[8].AddParagraph(getFormatDate(port.DPT_RT_SETTLED_T_START));

                    row4.Cells[2].AddParagraph("Stop");
                    row4.Cells[3].AddParagraph(port.DPT_RT_T_STOP_ACTIVITY + "");
                    row4.Cells[4].AddParagraph(getFormatDate(port.DPT_RT_ORIGIN_CLAIM_T_STOP));
                    row4.Cells[6].AddParagraph(getFormatDate(port.DPT_RT_TOP_REVIEWED_T_STOP));
                    row4.Cells[8].AddParagraph(getFormatDate(port.DPT_RT_SETTLED_T_STOP));

                    row5.Cells[2].AddParagraph("Total Running Time");
                    row5.Cells[4].AddParagraph(port.DPT_RT_ORIGIN_CLAIM_T_TEXT + "");
                    row5.Cells[5].AddParagraph(string.Format("{0:N4}", port.DPT_RT_ORIGIN_CLAIM_T_HRS) + " Hrs");
                    row5.Cells[6].AddParagraph(port.DPT_RT_TOP_REVIEWED_T_TEXT + "");
                    row5.Cells[7].AddParagraph(string.Format("{0:N4}", port.DPT_RT_TOP_REVIEWED_T_HRS) + " Hrs");
                    row5.Cells[8].AddParagraph(port.DPT_RT_SETTLED_T_TEXT + "");
                    row5.Cells[9].AddParagraph(string.Format("{0:N4}", port.DPT_RT_SETTLED_T_HRS) + " Hrs");

                    row6.Cells[0].AddParagraph("Deduction");
                    row6.Cells[2].AddParagraph("Activity");
                    row6.Cells[4].AddParagraph("Owner Deduction");
                    if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                    {
                        row6.Cells[6].AddParagraph("TOP Reviewed");
                    }
                    else
                    {
                        row6.Cells[6].AddParagraph("Customer/Charterer Counter");
                    }
                    row6.Cells[8].AddParagraph("Settled");


                    if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                    {

                        row2.Cells[10].AddParagraph("Saving (Settled vs Original)");
                        row3.Cells[10].AddParagraph(port.DPT_RT_SAVING_T_START_TEXT);
                        row3.Cells[11].AddParagraph(string.Format("{0:N4}", port.DPT_RT_SAVING_T_START_HRS) + " Hrs");

                        row4.Cells[10].AddParagraph(port.DPT_RT_SAVING_T_STOP_TEXT);
                        row4.Cells[11].AddParagraph(string.Format("{0:N4}", port.DPT_RT_SAVING_T_STOP_HRS) + " Hrs");

                        row5.Cells[10].AddParagraph(port.DPT_RT_SAVING_T_TEXT);
                        row5.Cells[11].AddParagraph(string.Format("{0:N4}", port.DPT_RT_SAVING_T_HRS) + " Hrs");
                        row6.Cells[10].AddParagraph("Saving (Settled vs Owner)");
                    }
                    for (int j = 0; j < dafData.DAF_PORT.ElementAt(i).DAF_DEDUCTION_ACTIVITY.Count; j++)
                    {
                        var activity = dafData.DAF_PORT.ElementAt(i).DAF_DEDUCTION_ACTIVITY.ElementAt(j);
                        Row row = table.AddRow();
                        row.Cells[2].MergeRight = 1;
                        row.Cells[4].MergeRight = 1;
                        row.Cells[6].MergeRight = 1;
                        row.Cells[8].MergeRight = 1;
                        row.Cells[2].AddParagraph(activity.DDD_ACTIVITY + "");
                        row.Cells[2].Format.Alignment = ParagraphAlignment.Left;
                        if (activity.DDD_OWNER_DEDUCTION_T_TEXT == null)
                        {
                            row.Cells[4].AddParagraph("0 Days 0 Hrs 0 Mins");
                        }
                        else
                        {
                            row.Cells[4].AddParagraph(activity.DDD_OWNER_DEDUCTION_T_TEXT + "");
                        }
                        if (activity.DDD_TOP_REVIEWED_T_TEXT == null)
                        {
                            row.Cells[6].AddParagraph("0 Days 0 Hrs 0 Mins");
                        }
                        else
                        {
                            row.Cells[6].AddParagraph(activity.DDD_TOP_REVIEWED_T_TEXT);
                        }
                        if (activity.DDD_SETTLED_T_TEXT == null)
                        {
                            row.Cells[8].AddParagraph("0 Days 0 Hrs 0 Mins");
                        }
                        else
                        {
                            row.Cells[8].AddParagraph(activity.DDD_SETTLED_T_TEXT + "");
                        }


                        if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                        {
                            row.Cells[10].AddParagraph(activity.DDD_SAVING_T_TEXT + "");
                            row.Cells[11].AddParagraph(string.Format("{0:N4}", activity.DDD_SAVING_T_HRS) + " Hrs");
                        }
                    }
                    Row rowTDT = table.AddRow();
                    rowTDT.Cells[2].MergeRight = 1;
                    rowTDT.Cells[2].AddParagraph("Total Deduction Time");
                    rowTDT.Cells[4].AddParagraph(port.DPT_D_OWNER_DEDUCTION_T_TEXT + "");
                    rowTDT.Cells[5].AddParagraph(string.Format("{0:N4}", port.DPT_D_OWNER_DEDUCTION_T_HRS) + " Hrs");
                    rowTDT.Cells[6].AddParagraph(port.DPT_D_TOP_REVIEWED_T_TEXT + "");
                    rowTDT.Cells[7].AddParagraph(string.Format("{0:N4}", port.DPT_D_TOP_REVIEWED_T_HRS) + " Hrs");
                    rowTDT.Cells[8].AddParagraph(port.DPT_D_SETTLED_T_TEXT + "");
                    rowTDT.Cells[9].AddParagraph(string.Format("{0:N4}", port.DPT_D_SETTLED_T_HRS) + " Hrs");
                    if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                    {
                        rowTDT.Cells[10].AddParagraph(port.DPT_D_SAVING_T_TEXT + "");
                        rowTDT.Cells[11].AddParagraph(string.Format("{0:N4}", port.DPT_D_SETTLED_T_HRS) + " Hrs");
                    }
                    Row latsRow = table.AddRow();
                    latsRow.Cells[0].MergeRight = 3;
                    latsRow.Cells[0].AddParagraph("Net Time Spent");
                    latsRow.Cells[4].AddParagraph(port.DPT_NTS_CLAIM_T_TEXT + "");
                    latsRow.Cells[5].AddParagraph(string.Format("{0:N4}", port.DPT_NTS_CLAIM_T_HRS) + " Hrs");
                    latsRow.Cells[6].AddParagraph(port.DPT_NTS_TOP_REVIEWED_T_TEXT + "");
                    latsRow.Cells[7].AddParagraph(string.Format("{0:N4}", port.DPT_NTS_TOP_REVIEWED_T_HRS) + " Hrs");
                    latsRow.Cells[8].AddParagraph(port.DPT_NTS_SETTLED_T_TEXT + "");
                    latsRow.Cells[9].AddParagraph(string.Format("{0:N4}", port.DPT_NTS_SETTLED_T_HRS) + " Hrs");
                    if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                    {
                        latsRow.Cells[10].AddParagraph(port.DPT_NTS_SAVING_T_TEXT + "");
                        latsRow.Cells[11].AddParagraph(string.Format("{0:N4}", port.DPT_NTS_SAVING_T_HRS) + " Hrs");
                    }

                    row1.Shading.Color = Color.FromRgb(123, 123, 123);
                    row1.Format.Font.Color = Colors.White;

                    row2.Cells[0].Shading.Color = Color.FromRgb(0, 112, 192);
                    row2.Cells[0].Format.Font.Color = Colors.White;

                    row6.Cells[0].Shading.Color = Color.FromRgb(255, 0, 0);
                    row6.Cells[0].Format.Font.Color = Colors.White;

                    row5.Cells[2].Shading.Color = Color.FromRgb(231, 230, 230);
                    row5.Cells[4].Shading.Color = Color.FromRgb(231, 230, 230);
                    row5.Cells[6].Shading.Color = Color.FromRgb(231, 230, 230);
                    row5.Cells[8].Shading.Color = Color.FromRgb(231, 230, 230);
                    row5.Cells[10].Shading.Color = Color.FromRgb(231, 230, 230);

                    row5.Cells[5].Shading.Color = Color.FromRgb(201, 201, 201);
                    row5.Cells[7].Shading.Color = Color.FromRgb(201, 201, 201);
                    row5.Cells[9].Shading.Color = Color.FromRgb(201, 201, 201);
                    row5.Cells[11].Shading.Color = Color.FromRgb(201, 201, 201);

                    table.Rows[table.Rows.Count - 2].Cells[2].Shading.Color = Color.FromRgb(231, 230, 230);
                    table.Rows[table.Rows.Count - 2].Cells[4].Shading.Color = Color.FromRgb(231, 230, 230);
                    table.Rows[table.Rows.Count - 2].Cells[6].Shading.Color = Color.FromRgb(231, 230, 230);
                    table.Rows[table.Rows.Count - 2].Cells[8].Shading.Color = Color.FromRgb(231, 230, 230);
                    table.Rows[table.Rows.Count - 2].Cells[10].Shading.Color = Color.FromRgb(231, 230, 230);

                    table.Rows[table.Rows.Count - 2].Cells[5].Shading.Color = Color.FromRgb(201, 201, 201);
                    table.Rows[table.Rows.Count - 2].Cells[7].Shading.Color = Color.FromRgb(201, 201, 201);
                    table.Rows[table.Rows.Count - 2].Cells[9].Shading.Color = Color.FromRgb(201, 201, 201);
                    table.Rows[table.Rows.Count - 2].Cells[11].Shading.Color = Color.FromRgb(201, 201, 201);

                    table.Rows[table.Rows.Count - 1].Shading.Color = Colors.Yellow;
                    table.Rows[table.Rows.Count - 1].Cells[5].Shading.Color = Color.FromRgb(248, 231, 8);
                    table.Rows[table.Rows.Count - 1].Cells[7].Shading.Color = Color.FromRgb(248, 231, 8);
                    table.Rows[table.Rows.Count - 1].Cells[9].Shading.Color = Color.FromRgb(248, 231, 8);
                    table.Rows[table.Rows.Count - 1].Cells[11].Shading.Color = Color.FromRgb(248, 231, 8);

                    row1.Format.Font.Bold = true;
                    row2.Format.Font.Bold = true;
                    row5.Format.Font.Bold = true;
                    row6.Format.Font.Bold = true;
                    table.Rows[table.Rows.Count - 2].Format.Font.Bold = true;
                    table.Rows[table.Rows.Count - 1].Format.Font.Bold = true;

                    row1.Borders.Bottom.Width = 0.4;
                    row2.Borders.Bottom.Width = 0.4;
                    row5.Borders.Bottom.Width = 0.4;
                    row6.Borders.Bottom.Width = 0.4;
                    table.Rows[table.Rows.Count - 1].Borders.Top.Width = 0.4;
                    table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.4;

                    row3.Cells[1].MergeDown = 4 + port.DAF_DEDUCTION_ACTIVITY.Count();
                    Row rowSpace = table.AddRow();
                    rowSpace.Borders.Left.Width = 0;
                    rowSpace.Borders.Right.Width = 0;
                    rowSpace.Borders.Bottom.Width = 0;
                }

                table.Rows[0].Borders.Top.Width = 0.4;

                doc.LastSection.AddParagraph("");
                doc.LastSection.AddParagraph("");
                Paragraph ph = doc.LastSection.AddParagraph("CALCULATION");
                ph.Format.Font.Size = 8;
                doc.LastSection.AddParagraph("");
                ph.Format.Font.Bold = true;
                ph.Format.Font.Underline = Underline.Single;
                doc.LastSection.Add(table);
            }
            catch (Exception e)
            {

            }

        }

        public static string getFormatDate(Nullable<DateTime> d)
        {
            if (d == null)
            {
                return "";
            }
            return ((DateTime)d).ToString("dd/MM/yyyy HH:mm");
        }
        public static void genTable5(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;

            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;

            Column column1 = table.AddColumn();
            Column column2 = table.AddColumn();
            Column column3 = table.AddColumn();
            Column column4 = table.AddColumn();
            Column column5 = table.AddColumn();
            Column column6 = table.AddColumn();
            Column column7 = table.AddColumn();
            Column column8 = table.AddColumn();
            Column column9 = table.AddColumn();
            Column column10 = table.AddColumn();

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                column1.Width = Unit.FromCentimeter(3.2);
                column2.Width = Unit.FromCentimeter(2.2);
                column3.Width = Unit.FromCentimeter(2.3);
                column4.Width = Unit.FromCentimeter(1.35);
                column5.Width = Unit.FromCentimeter(2.3);
                column6.Width = Unit.FromCentimeter(1.35);
                column7.Width = Unit.FromCentimeter(2.3);
                column8.Width = Unit.FromCentimeter(1.35);
                column9.Width = Unit.FromCentimeter(2.3);
                column10.Width = Unit.FromCentimeter(1.35);
            }
            else if (dafData.DDA_DEMURAGE_TYPE == "Received")
            {
                column1.Width = Unit.FromCentimeter(4);
                column2.Width = Unit.FromCentimeter(4);
                column3.Width = Unit.FromCentimeter(2.5);
                column4.Width = Unit.FromCentimeter(1.5);
                column5.Width = Unit.FromCentimeter(2.5);
                column6.Width = Unit.FromCentimeter(1.5);
                column7.Width = Unit.FromCentimeter(2.5);
                column8.Width = Unit.FromCentimeter(1.5);
                column9.Width = Unit.FromCentimeter(0);
                column10.Width = Unit.FromCentimeter(0);
            }

            column3.Borders.Right.Width = 0;
            column4.Borders.Left.Width = 0;
            column5.Borders.Right.Width = 0;
            column6.Borders.Left.Width = 0;
            column7.Borders.Right.Width = 0;
            column8.Borders.Left.Width = 0;
            column9.Borders.Right.Width = 0;
            column10.Borders.Left.Width = 0;

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();
            Row row5 = table.AddRow();
            Row row6 = table.AddRow();


            row1.Format.Font.Bold = true;
            column1.Format.Font.Bold = true;
            row6.Format.Font.Bold = true;

            column1.Format.Alignment = ParagraphAlignment.Left;

            row1.Cells[0].MergeRight = 1;
            row1.Cells[2].MergeRight = 1;
            row1.Cells[4].MergeRight = 1;
            row1.Cells[6].MergeRight = 1;
            row1.Cells[8].MergeRight = 1;

            row1.Borders.Bottom.Width = 0.4;

            row2.Cells[0].MergeRight = 1;
            row3.Cells[0].MergeRight = 1;
            row4.Cells[0].MergeRight = 1;
            row5.Cells[0].MergeRight = 1;
            row6.Cells[0].MergeRight = 1;

            bool hasDischarge = false;
            bool hasLoad = false;
            foreach (var p in dafData.DAF_PORT)
            {
                if (p.DPT_TYPE == "discharge")
                {
                    hasDischarge = true;
                }
                if (p.DPT_TYPE == "load")
                {
                    hasLoad = true;
                }
            }

            if (hasDischarge && hasLoad)
            {
                row2.Cells[0].AddParagraph("Total Running time (Load Port + Discharge Port)");
                row3.Cells[0].AddParagraph("Total Deduction time (Load Port + Discharge Port)");
            }
            else if (hasDischarge)
            {
                row2.Cells[0].AddParagraph("Total Running time (Discharge Port)");
                row3.Cells[0].AddParagraph("Total Deduction time (Discharge Port)");
            }
            else if (hasLoad)
            {
                row2.Cells[0].AddParagraph("Total Running time (Load Port)");
                row3.Cells[0].AddParagraph("Total Deduction time (Load Port)");
            }
            else
            {
                row2.Cells[0].AddParagraph("Total Running time ()");
                row3.Cells[0].AddParagraph("Total Deduction time ()");
            }

            row4.Cells[0].AddParagraph("Net Time Spent");
            row5.Cells[0].AddParagraph("Laytime Contract");
            row6.Cells[0].AddParagraph("Time On Demurrage");

            row1.Cells[2].AddParagraph("Original Claim/Original Time Spent");
            row2.Cells[2].AddParagraph(dafData.DDA_TRT_ORIGIN_CLAIM_TIME_TEXT + "");
            row3.Cells[2].AddParagraph(dafData.DDA_TDT_ORIGIN_CLAIM_TIME_TEXT + "");
            row4.Cells[2].AddParagraph(dafData.DDA_NTS_ORIGIN_CLAIM_TIME_TEXT + "");
            row5.Cells[2].AddParagraph(dafData.DDA_LY_ORIGIN_CLAIM_TIME_TEXT + "");
            row6.Cells[2].AddParagraph(dafData.DDA_TOD_ORIGIN_CLAIM_TIME_TEXT + "");

            row2.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_TRT_ORIGIN_CLAIM_HRS) + " Hrs");
            row3.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_TDT_ORIGIN_CLAIM_HRS) + " Hrs");
            row4.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_NTS_ORIGIN_CLAIM_HRS) + " Hrs");
            row5.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_LY_ORIGIN_CLAIM_HRS) + " Hrs");
            row6.Cells[3].AddParagraph(string.Format("{0:N4}", dafData.DDA_TOD_ORIGIN_CLAIM_HRS) + " Hrs");

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                row1.Cells[4].AddParagraph("TOP Reviewed");
            }
            else
            {
                row1.Cells[4].AddParagraph("Customer/Charterer Counter");
            }
            row2.Cells[4].AddParagraph(dafData.DDA_TRT_TOP_REVIEWED_TIME_TEXT + "");
            row3.Cells[4].AddParagraph(dafData.DDA_TDT_TOP_REVIEWED_TIME_TEXT + "");
            row4.Cells[4].AddParagraph(dafData.DDA_NTS_TOP_REVIEWED_TIME_TEXT + "");
            row5.Cells[4].AddParagraph(dafData.DDA_LY_TOP_REVIEWED_TIME_TEXT + "");
            row6.Cells[4].AddParagraph(dafData.DDA_TOD_TOP_REVIEWED_TIME_TEXT + "");

            row2.Cells[5].AddParagraph(string.Format("{0:N4}", dafData.DDA_TRT_TOP_REVIEWED_HRS) + " Hrs");
            row3.Cells[5].AddParagraph(string.Format("{0:N4}", dafData.DDA_TDT_TOP_REVIEWED_HRS) + " Hrs");
            row4.Cells[5].AddParagraph(string.Format("{0:N4}", dafData.DDA_NTS_TOP_REVIEWED_HRS) + " Hrs");
            row5.Cells[5].AddParagraph(string.Format("{0:N4}", dafData.DDA_LY_TOP_REVIEWED_HRS) + " Hrs");
            row6.Cells[5].AddParagraph(string.Format("{0:N4}", dafData.DDA_TOD_TOP_REVIEWED_HRS) + " Hrs");

            row1.Cells[6].AddParagraph("Settled");
            row2.Cells[6].AddParagraph(dafData.DDA_TRT_SETTLED_TIME_TEXT + "");
            row3.Cells[6].AddParagraph(dafData.DDA_TDT_SETTLED_TIME_TEXT + "");
            row4.Cells[6].AddParagraph(dafData.DDA_NTS_SETTLED_TIME_TEXT + "");
            row5.Cells[6].AddParagraph(dafData.DDA_LY_SETTLED_TIME_TEXT + "");
            row6.Cells[6].AddParagraph(dafData.DDA_TOD_SETTLED_TIME_TEXT + "");

            row2.Cells[7].AddParagraph(string.Format("{0:N4}", dafData.DDA_TRT_SETTLED_HRS) + " Hrs");
            row3.Cells[7].AddParagraph(string.Format("{0:N4}", dafData.DDA_TDT_SETTLED_HRS) + " Hrs");
            row4.Cells[7].AddParagraph(string.Format("{0:N4}", dafData.DDA_NTS_SETTLED_HRS) + " Hrs");
            row5.Cells[7].AddParagraph(string.Format("{0:N4}", dafData.DDA_LY_SETTLED_HRS) + " Hrs");
            row6.Cells[7].AddParagraph(string.Format("{0:N4}", dafData.DDA_TOD_SETTLED_HRS) + " Hrs");

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                row1.Cells[8].AddParagraph("Saving (Settled vs Original)");
                row2.Cells[8].AddParagraph(dafData.DDA_TRT_SAVING_TIME_TEXT + "");
                row3.Cells[8].AddParagraph(dafData.DDA_TDT_SAVING_TIME_TEXT + "");
                row4.Cells[8].AddParagraph(dafData.DDA_NTS_SAVING_TIME_TEXT + "");
                row5.Cells[8].AddParagraph(dafData.DDA_LY_SAVING_TIME_TEXT + "");
                row6.Cells[8].AddParagraph(dafData.DDA_TOD_SAVING_TIME_TEXT + "");

                row2.Cells[9].AddParagraph(string.Format("{0:N4}", dafData.DDA_TRT_SAVING_HRS) + " Hrs");
                row3.Cells[9].AddParagraph(string.Format("{0:N4}", dafData.DDA_TDT_SAVING_HRS) + " Hrs");
                row4.Cells[9].AddParagraph(string.Format("{0:N4}", dafData.DDA_NTS_SAVING_HRS) + " Hrs");
                row5.Cells[9].AddParagraph(string.Format("{0:N4}", dafData.DDA_LY_SAVING_HRS) + " Hrs");
                row6.Cells[9].AddParagraph(string.Format("{0:N4}", dafData.DDA_TOD_SAVING_HRS) + " Hrs");
            }

            row6.Shading.Color = Colors.Yellow;
            row2[3].Shading.Color = Color.FromRgb(201, 201, 201);
            row3[3].Shading.Color = Color.FromRgb(201, 201, 201);
            row4[3].Shading.Color = Color.FromRgb(201, 201, 201);
            row5[3].Shading.Color = Color.FromRgb(201, 201, 201);

            row2[5].Shading.Color = Color.FromRgb(201, 201, 201);
            row3[5].Shading.Color = Color.FromRgb(201, 201, 201);
            row4[5].Shading.Color = Color.FromRgb(201, 201, 201);
            row5[5].Shading.Color = Color.FromRgb(201, 201, 201);

            row2[7].Shading.Color = Color.FromRgb(201, 201, 201);
            row3[7].Shading.Color = Color.FromRgb(201, 201, 201);
            row4[7].Shading.Color = Color.FromRgb(201, 201, 201);
            row5[7].Shading.Color = Color.FromRgb(201, 201, 201);

            row2[9].Shading.Color = Color.FromRgb(201, 201, 201);
            row3[9].Shading.Color = Color.FromRgb(201, 201, 201);
            row4[9].Shading.Color = Color.FromRgb(201, 201, 201);
            row5[9].Shading.Color = Color.FromRgb(201, 201, 201);

            row6[3].Shading.Color = Color.FromRgb(248, 231, 8);
            row6[5].Shading.Color = Color.FromRgb(248, 231, 8);
            row6[7].Shading.Color = Color.FromRgb(248, 231, 8);
            row6[9].Shading.Color = Color.FromRgb(248, 231, 8);


            table.Rows[0].Borders.Top.Width = 0.4;
            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.4;

            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("DEMURRAGE TIME SUMMARY");
            ph.Format.Font.Size = 8;
            doc.LastSection.AddParagraph("");
            ph.Format.Font.Bold = true;
            ph.Format.Font.Underline = Underline.Single;
            doc.LastSection.Add(table);
        }

        public static void genTable6(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;

            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = 7;

            Column column1 = table.AddColumn();
            Column column2 = table.AddColumn();
            Column column3 = table.AddColumn();
            Column column4 = table.AddColumn();
            Column column5 = table.AddColumn();
            Column column6 = table.AddColumn();

            column2.Borders.Left.Width = 0;
            column1.Borders.Right.Width = 0;

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                column1.Width = Unit.FromCentimeter(5.4);
                column2.Width = Unit.FromCentimeter(0);
                column3.Width = Unit.FromCentimeter(3.65);
                column4.Width = Unit.FromCentimeter(3.65);
                column5.Width = Unit.FromCentimeter(3.65);
                column6.Width = Unit.FromCentimeter(3.65);

            }
            else if (dafData.DDA_DEMURAGE_TYPE == "Received")
            {
                column1.Width = Unit.FromCentimeter(8);
                column2.Width = Unit.FromCentimeter(0);
                column3.Width = Unit.FromCentimeter(4);
                column4.Width = Unit.FromCentimeter(4);
                column5.Width = Unit.FromCentimeter(4);
                column6.Width = Unit.FromCentimeter(0);
            }

            Row row1 = table.AddRow();
            row1.Format.Font.Size = 5;


            row1.Format.Font.Bold = true;
            column1.Format.Font.Bold = true;


            column1.Format.Alignment = ParagraphAlignment.Left;

            row1.Cells[0].MergeRight = 1;
            row1.Borders.Bottom.Width = 0.4;

            row1.Cells[2].AddParagraph("Original Claim/Original Time Spent");

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                row1.Cells[3].AddParagraph("TOP Reviewed");
            }
            else
            {
                row1.Cells[3].AddParagraph("Customer/Charterer Counter");
            }
            row1.Cells[4].AddParagraph("Settled");

            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                row1.Cells[5].AddParagraph("Saving (Settled vs Original)");

            }

            if (dafData.DDA_IS_ADDRESS_COMMISSION == "N" && dafData.DDA_IS_SHARING_TO_TOP == "N")
            {
                Row row2 = table.AddRow();
                row2.Cells[0].AddParagraph("Net Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row2.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_ORIGIN_CLAIM_VALUE));
                row2.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_TOP_REVIEWED_VALUE));
                row2.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SETTLED_VALUE));
                if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                {
                    row2.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SAVING_VALUE));
                }
            }
            else if (dafData.DDA_IS_ADDRESS_COMMISSION == "Y" && dafData.DDA_IS_SHARING_TO_TOP == "N")
            {
                Row row2 = table.AddRow();
                row2.Cells[0].AddParagraph("Gross Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row2.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_ORIGIN_CLAIM_VALUE));
                row2.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_TOP_REVIEWED_VALUE));
                row2.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_SETTLED_VALUE));

                Row row3 = table.AddRow();
                row3.Cells[0].AddParagraph("Less Address Commission " + dafData.DDA_ADDRESS_COMMISSION_PERCEN + "% (" + dafData.DDA_CURRENCY_UNIT + ")");
                row3.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_ORIGIN_CLAIM_VALUE));
                row3.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_TOP_REVIEWED_VALUE));
                row3.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_SETTLED_VALUE));

                Row row4 = table.AddRow();
                row4.Cells[0].AddParagraph("Net Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row4.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_ORIGIN_CLAIM_VALUE));
                row4.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_TOP_REVIEWED_VALUE));
                row4.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SETTLED_VALUE));

                if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                {
                    row2.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_SAVING_VALUE));
                    row3.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_SAVING_VALUE));
                    row4.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SAVING_VALUE));
                }

            }
            else if (dafData.DDA_IS_ADDRESS_COMMISSION == "N" && dafData.DDA_IS_SHARING_TO_TOP == "Y")
            {
                Row row2 = table.AddRow();
                row2.Cells[0].AddParagraph("Gross Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row2.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_ORIGIN_CLAIM_VALUE));
                row2.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_TOP_REVIEWED_VALUE));
                row2.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_SETTLED_VALUE));

                Row row3 = table.AddRow();
                row3.Cells[0].AddParagraph("Demurrage Sharing to TOP (" + dafData.DDA_CURRENCY_UNIT + ")");
                row3.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_ORIGIN_CLAIM_VALUE));
                row3.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_TOP_REVIEWED_VALUE));
                row3.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_SETTLED_VALUE));

                Row row4 = table.AddRow();
                row4.Cells[0].AddParagraph("Net Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row4.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_ORIGIN_CLAIM_VALUE));
                row4.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_TOP_REVIEWED_VALUE));
                row4.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SETTLED_VALUE));

                if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                {
                    row2.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_SAVING_VALUE));
                    row3.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_SAVING_VALUE));
                    row4.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SAVING_VALUE));
                }
            }
            else if (dafData.DDA_IS_ADDRESS_COMMISSION == "Y" && dafData.DDA_IS_SHARING_TO_TOP == "Y")
            {
                Row row2 = table.AddRow();
                row2.Cells[0].AddParagraph("Gross Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row2.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_ORIGIN_CLAIM_VALUE));
                row2.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_TOP_REVIEWED_VALUE));
                row2.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_SETTLED_VALUE));

                Row row3 = table.AddRow();
                row3.Cells[0].AddParagraph("Less Address Commission (" + dafData.DDA_CURRENCY_UNIT + ")");
                row3.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_ORIGIN_CLAIM_VALUE));
                row3.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_TOP_REVIEWED_VALUE));
                row3.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_SETTLED_VALUE));

                Row row4 = table.AddRow();
                row4.Cells[0].AddParagraph("Sub Total Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row4.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_STD_ORIGIN_CLAIM_VALUE));
                row4.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_STD_TOP_REVIEWED_VALUE));
                row4.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_STD_SETTLED_VALUE));
                row4.Shading.Color = Color.FromRgb(217, 225, 242);

                Row row5 = table.AddRow();
                row5.Cells[0].AddParagraph("Demurrage Sharing to TOP (" + dafData.DDA_CURRENCY_UNIT + ")");
                row5.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_ORIGIN_CLAIM_VALUE));
                row5.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_TOP_REVIEWED_VALUE));
                row5.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_SETTLED_VALUE));

                Row row6 = table.AddRow();
                row6.Cells[0].AddParagraph("Net Demurrage (" + dafData.DDA_CURRENCY_UNIT + ")");
                row6.Cells[2].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_ORIGIN_CLAIM_VALUE));
                row6.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_TOP_REVIEWED_VALUE));
                row6.Cells[4].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SETTLED_VALUE));

                if (dafData.DDA_DEMURAGE_TYPE == "Paid")
                {
                    row2.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_GD_SAVING_VALUE));
                    row3.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_AC_SAVING_VALUE));
                    row4.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_STD_SAVING_VALUE));
                    row5.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_DST_SAVING_VALUE));
                    row6.Cells[5].AddParagraph(string.Format("{0:N2}", dafData.DDA_ND_SAVING_VALUE));
                }
            }

            table.Rows[table.Rows.Count - 1].Shading.Color = Colors.Yellow;

            table.Rows[0].Borders.Top.Width = 0.4;
            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.4;
            table.Rows[table.Rows.Count - 1].Format.Font.Bold = true;

            doc.LastSection.AddParagraph("");
            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("DEMURRAGE VALUE SUMMARY");
            ph.Format.Font.Size = 8;
            doc.LastSection.AddParagraph("");
            ph.Format.Font.Bold = true;
            ph.Format.Font.Underline = Underline.Single;
            doc.LastSection.Add(table);
        }

        public static void genTable7(Document doc, DAF_DATA dafData)
        {

            Table table = new Table();

            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Format.Font.Size = 8;

            Column column1 = table.AddColumn(Unit.FromCentimeter(20));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            row2.Height = 10;

            row1.Borders.Top.Width = 0.4;
            row1.Cells[0].AddParagraph("");
            row1.Cells[0].AddParagraph("EXPLANATION");
            row1.Cells[0].AddParagraph("");
            row1.Cells[0].Format.Font.Size = 8;

            row1.Cells[0].Format.Font.Bold = true;
            row1.Cells[0].Format.Font.Underline = Underline.Single;

            if (!string.IsNullOrEmpty(dafData.DDA_EXPLANATION))
            {
                Paragraph p2 = row2.Cells[0].AddParagraph();
                HtmlRemoval htmlRemove = new HtmlRemoval();
                htmlRemove.ValuetoParagraph(dafData.DDA_EXPLANATION, p2);
            }
            else
            {
                row2.Cells[0].AddParagraph("");
            }

            row2.Borders.Bottom.Width = 0.4;

            doc.LastSection.AddParagraph("");
            doc.LastSection.Add(table);

        }

        public static void genTable8(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();

            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Font.Size = 7;

            Column column1 = table.AddColumn(Unit.FromCentimeter(4));
            Column column2 = table.AddColumn(Unit.FromCentimeter(6));
            Column column3 = table.AddColumn(Unit.FromCentimeter(6));
            Column column4 = table.AddColumn(Unit.FromCentimeter(4));
            Row row0 = table.AddRow();
            Row row1 = table.AddRow();
            Row rowv = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();


            column1.Borders.Left.Width = 0.4;
            column4.Borders.Right.Width = 0.4;

            row1.Cells[1].MergeRight = 2;

            row0.Cells[0].AddParagraph("");
            row0.Cells[0].AddParagraph("PROPOSAL");
            row0.Cells[0].Format.Font.Size = 8;
            row0.Cells[0].AddParagraph("");
            row0.Cells[0].Format.Font.Bold = true;
            row0.Cells[0].Format.Font.Underline = Underline.Single;

            column1.Format.Font.Bold = true;
            column2.Format.Font.Underline = Underline.Single;
            column3.Format.Font.Bold = true;
            column4.Format.Font.Underline = Underline.Single;


            row1.Cells[0].AddParagraph("Demurrage");
            Paragraph p10 = row1.Cells[1].AddParagraph();
            p10.AddFormattedText(getNameFromCompany(dafData.DDA_FOR_COMPANY), TextFormat.Underline);

            rowv.Cells[0].AddParagraph("Vessel");
            Paragraph pv = rowv.Cells[1].AddParagraph();
            if (dafData.MT_VEHICLE != null)
            {
                pv.AddFormattedText(dafData.MT_VEHICLE.VEH_VEH_TEXT + "", TextFormat.Underline);
            }

            row2.Cells[0].AddParagraph("Time on Demurrage");
            Paragraph pt = row2.Cells[1].AddParagraph(dafData.DDA_TIME_ON_DEM_TEXT);

            pt.AddFormattedText(" ", TextFormat.NoUnderline);
            pt.AddSpace(2);
            pt.AddText("/");
            pt.AddSpace(2);
            pt.AddFormattedText(string.Format("{0:N4}", dafData.DDA_TIME_ON_DEM_HRS) + " Hrs", TextFormat.Underline);
            if (dafData.DDA_DEMURAGE_TYPE == "Paid")
            {
                p10.AddSpace(1);
                p10.AddFormattedText(" ", TextFormat.NoUnderline | TextFormat.Bold);
                p10.AddFormattedText(" Paid to ", TextFormat.NoUnderline | TextFormat.Bold);
                p10.AddFormattedText(" ", TextFormat.NoUnderline | TextFormat.Bold);
                p10.AddSpace(1);
            }
            else if (dafData.DDA_DEMURAGE_TYPE == "Received")
            {
                p10.AddSpace(2);
                p10.AddFormattedText(" Received from ", TextFormat.NoUnderline | TextFormat.Bold);
                p10.AddSpace(2);
            }

            if (dafData.DDA_SHIP_OWNER != null)
            {
                p10.AddFormattedText(dafData.DDA_SHIP_OWNER + " (Ship Owner)", TextFormat.NotBold | TextFormat.Underline);
                if (dafData.DDA_BROKER != null)
                {
                    p10.AddFormattedText(" via " + getNameFromVendor(dafData.DDA_BROKER) + " (Broker)", TextFormat.NotBold | TextFormat.Underline);
                }
            }
            else if (dafData.DDA_SUPPLIER != null)
            {
                p10.AddFormattedText(getNameFromVendor(dafData.DDA_SUPPLIER) + " (Supplier as Charterer)", TextFormat.NotBold | TextFormat.Underline);
            }
            else if (dafData.DDA_CUSTOMER != null)
            {
                p10.AddFormattedText(getNameFromCustNum(dafData.DDA_CUSTOMER, dafData.DDA_FOR_COMPANY) + " (Customer)", TextFormat.NotBold | TextFormat.Underline);
                if (dafData.DDA_CUSTOMER_BROKER != null)
                {
                    p10.AddFormattedText(" via " + getNameFromVendor(dafData.DDA_CUSTOMER_BROKER) + " (Broker)", TextFormat.NotBold | TextFormat.Underline);
                }
            }

            if (dafData.DDA_DEMURAGE_TYPE == "Received" && dafData.DDA_SUPPLIER != null)
            {
                row3.Cells[0].AddParagraph("Demurrage Received Settled");
                row3.Cells[2].AddParagraph("Demurrage Received Settled (Manual Input)");
            }
            else if (dafData.DDA_DEMURAGE_TYPE == "Received" && dafData.DDA_CUSTOMER != null)
            {
                row3.Cells[0].AddParagraph("Demurrage Received Settled");
                row3.Cells[2].AddParagraph("Demurrage Received Settled (Manual Input)");
            }
            else
            {
                row3.Cells[0].AddParagraph("Demurrage Paid Settled");
                row3.Cells[2].AddParagraph("Demurrage Paid Settled (Manual Input)");
            }


            row3.Cells[1].AddParagraph(string.Format("{0:N2}", dafData.DDA_NET_PAYMENT_DEM_VALUE) + " " + dafData.DDA_CURRENCY_UNIT);


            row3.Cells[3].AddParagraph(string.Format("{0:N2}", getDemSettled(dafData.DDA_ROW_ID)) + " " + dafData.DDA_CURRENCY_UNIT);

            if (dafData.DDA_EXCHANGE_RATE != null)
            {
                Row row4 = table.AddRow();
                row4.Cells[0].AddParagraph("Exchange rate - For settlement in THB");
                row4.Cells[1].AddParagraph(string.Format("{0:N2}", dafData.DDA_EXCHANGE_RATE) + " THB/" + dafData.DDA_CURRENCY_UNIT);
                row4.Cells[2].AddParagraph("Settlement Amount");
                row4.Cells[3].AddParagraph(string.Format("{0:N2}", dafData.DDA_SETTLEMENT_AMOUNT_THB) + " THB");
            }


            if (dafData.DDA_IS_BROKER_COMMISSION == "Y")
            {
                Row row5 = table.AddRow();
                row5.Cells[0].AddParagraph("Broker Commission");
                row5.Cells[1].AddParagraph(string.Format("{0:N2}", dafData.DDA_BROKER_COMMISSION_VALUE) + " " + dafData.DDA_CURRENCY_UNIT);
            }

            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.2;
            
            doc.LastSection.Add(table);
        }

        //public static void genTable9(Document doc, DAF_DATA dafData)
        //{
        //    Table table = new Table();
        //    table.Borders.Left.Width = 0.4;
        //    table.Borders.Right.Width = 0.4;
        //    table.Borders.Top.Width = 0.4;
        //    table.Borders.Bottom.Width = 0.4;
        //    table.Format.Font.Bold = true;
        //    table.Format.Alignment = ParagraphAlignment.Center;
        //    table.Format.Font.Size = 7;


        //    Column column1 = table.AddColumn(Unit.FromCentimeter(1));
        //    Column column2 = table.AddColumn(Unit.FromCentimeter(4));
        //    Column column3 = table.AddColumn(Unit.FromCentimeter(1));
        //    Column column4 = table.AddColumn(Unit.FromCentimeter(4));
        //    Column column5 = table.AddColumn(Unit.FromCentimeter(5));
        //    Column column6 = table.AddColumn(Unit.FromCentimeter(5));
        //    Row row1 = table.AddRow();
        //    Row row2 = table.AddRow();
        //    Row row3 = table.AddRow();

        //    row1.Height = 10;
        //    row2.Height = 25;
        //    row3.Height = 10;

        //    row1.Borders.Bottom.Visible = false;
        //    row2.Borders.Bottom.Visible = false;

        //    row2.Borders.Top.Visible = false;
        //    row3.Borders.Top.Visible = false;
        //    var listHistory = getHistoryByTxns(dafData.DDA_ROW_ID);

        //    row1.Cells[0].MergeRight = 1;
        //    row1.Cells[0].AddParagraph("");
        //    row1.Cells[0].AddParagraph("Requested By");

        //    row1.Cells[2].MergeRight = 1;
        //    row1.Cells[2].AddParagraph("");
        //    row1.Cells[2].AddParagraph("Verified By");
        //    row1.Cells[4].AddParagraph("");
        //    row1.Cells[4].AddParagraph("Endorsed By");
        //    row1.Cells[5].AddParagraph("");
        //    row1.Cells[5].AddParagraph("Approved By");

        //    row2.Cells[0].Borders.Right.Visible = false;
        //    row2.Cells[2].Borders.Right.Visible = false;
        //    row2.Cells[1].Borders.Left.Visible = false;
        //    row2.Cells[3].Borders.Left.Visible = false;
        //    row2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
        //    row2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
        //    row2.Cells[2].VerticalAlignment = VerticalAlignment.Center;
        //    row2.Cells[3].VerticalAlignment = VerticalAlignment.Center;
        //    row2.Cells[4].VerticalAlignment = VerticalAlignment.Center;
        //    row2.Cells[5].VerticalAlignment = VerticalAlignment.Center;

        //    row3.Cells[0].MergeRight = 1;
        //    row3.Cells[2].MergeRight = 1;

        //    Paragraph p3 = row2.Cells[4].AddParagraph("");
        //    Paragraph p4 = row2.Cells[5].AddParagraph("");

        //    var hSubmit = listHistory.Where(h => h.DTH_ACTION == "SUBMIT").ToList();
        //    var hVerify = listHistory.Where(h => h.DTH_ACTION == "VERIFY").ToList();
        //    var hEndorse = listHistory.Where(h => h.DTH_ACTION == "ENDORSE").ToList();
        //    var hApprove = listHistory.Where(h => h.DTH_ACTION == "APPROVE").ToList();

        //    String section = dafData.DDA_USER_GROUP + "";

        //    if (hSubmit.Count > 0)
        //    {
        //        Paragraph p1 = row2.Cells[0].AddParagraph("");
        //        Paragraph p2 = row2.Cells[1].AddParagraph("");
        //        p2.Format.LeftIndent = -30;
        //        var img = p2.AddImage(getSignatureImg(hSubmit.FirstOrDefault().DTH_ACTION_BY));
        //        img.Width = "2.0cm";
        //        img.LockAspectRatio = true;
        //        row3.Cells[0].AddParagraph("( " + section + " - " + getNameOfUser(hSubmit.FirstOrDefault().DTH_ACTION_BY) + " )");
        //    }
        //    else
        //    {
        //        if (hVerify.Count > 0)
        //        {
        //            Paragraph p1 = row2.Cells[0].AddParagraph("");
        //            Paragraph p2 = row2.Cells[1].AddParagraph("");
        //            p2.Format.LeftIndent = -30;
        //            var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
        //            img.Width = "2.0cm";
        //            img.LockAspectRatio = true;
        //            row3.Cells[0].AddParagraph("( " + section + " - " + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
        //        }
        //        else
        //        {
        //            Paragraph p1 = row2.Cells[0].AddParagraph("");
        //            row3.Cells[0].AddParagraph("( " + section + " )");
        //        }

        //    }

        //    if (hVerify.Count > 0)
        //    {
        //        Paragraph p1 = row2.Cells[2].AddParagraph(" S/H:");
        //        Paragraph p2 = row2.Cells[3].AddParagraph("");
        //        p2.Format.LeftIndent = -30;
        //        var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
        //        img.Width = "2.0cm";
        //        img.LockAspectRatio = true;
        //        row3.Cells[2].AddParagraph("( " + section + " - " + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
        //    }
        //    else
        //    {
        //        if (hEndorse.Count > 0)
        //        {
        //            Paragraph p1 = row2.Cells[2].AddParagraph(" S/H:");
        //            Paragraph p2 = row2.Cells[3].AddParagraph("");
        //            p2.Format.LeftIndent = -30;
        //            var img = p2.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));
        //            img.Width = "2.0cm";
        //            img.LockAspectRatio = true;
        //            row3.Cells[2].AddParagraph("( " + section + " - " + getNameOfUser(hEndorse.FirstOrDefault().DTH_ACTION_BY) + " )");
        //        }
        //        else
        //        {
        //            Paragraph p2 = row2.Cells[2].AddParagraph(" S/H:");
        //            row3.Cells[2].AddParagraph("( " + section + " )");
        //        }

        //    }

        //    if (hEndorse.Count > 0)
        //    {
        //        var img = p3.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));

        //        img.Width = "2.0cm";
        //        img.LockAspectRatio = true;

        //    }
        //    else
        //    {
        //        if (hApprove.Count > 0)
        //        {
        //            var img = p3.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));

        //            img.Width = "2.0cm";
        //            img.LockAspectRatio = true;
        //        }
        //    }

        //    if (hApprove.Count > 0)
        //    {
        //        var img = p4.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));
        //        img.Width = "2.0cm";
        //        img.LockAspectRatio = true;
        //        //img.WrapFormat.DistanceLeft = 20;
        //    }


        //    row3.Cells[4].AddParagraph("CMVP");
        //    row3.Cells[5].AddParagraph("EVPC");

        //    doc.LastSection.Add(table);
        //}

        public static void genTable9(Document doc, DAF_DATA dafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.4;
            table.Format.Font.Bold = true;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = 7;
            


            Column column1 = table.AddColumn(Unit.FromCentimeter(5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(5));
            Column column4 = table.AddColumn(Unit.FromCentimeter(5));
            
            Row row1 = table.AddRow();
            

            row1.Height = 10;
           
            var listHistory = getHistoryByTxns(dafData.DDA_ROW_ID);


            row1.Cells[0].AddParagraph("");
            row1.Cells[0].AddParagraph("Requested By");
            

            row1.Cells[1].AddParagraph("");
            row1.Cells[1].AddParagraph("Verified By");
            row1.Cells[2].AddParagraph("");
            row1.Cells[2].AddParagraph("Endorsed By");
            row1.Cells[3].AddParagraph("");
            row1.Cells[3].AddParagraph("Approved By");
            


            

            Paragraph p3 = row1.Cells[2].AddParagraph("");
            Paragraph p4 = row1.Cells[3].AddParagraph("");
            

            var hSubmit = listHistory.Where(h => h.DTH_ACTION == "SUBMIT").ToList();
            var hVerify = listHistory.Where(h => h.DTH_ACTION == "VERIFY").ToList();
            var hEndorse = listHistory.Where(h => h.DTH_ACTION == "ENDORSE").ToList();
            var hApprove = listHistory.Where(h => h.DTH_ACTION == "APPROVE").ToList();

            String section = dafData.DDA_USER_GROUP + "";

            if (hSubmit.Count > 0)
            {
                
                Paragraph p2 = row1.Cells[0].AddParagraph("");
                
                var img = p2.AddImage(getSignatureImg(hSubmit.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.3cm";
                img.LockAspectRatio = true;
                row1.Cells[0].AddParagraph("( " + section + " - " + getNameOfUser(hSubmit.FirstOrDefault().DTH_ACTION_BY) + " )");
            }
            else
            {
                if (hVerify.Count > 0)
                {
                    
                    Paragraph p2 = row1.Cells[0].AddParagraph("");
                    
                    var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
                    img.Width = "2.3cm";
                    img.LockAspectRatio = true;
                    row1.Cells[0].AddParagraph("( " + section + " - " + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
                }
                else
                {
                    
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("( " + section + " )");
                }

            }

            if (hVerify.Count > 0)
            {

                Paragraph p2 = row1.Cells[1].AddParagraph(" S/H:");
                p2.AddSpace(8);

                p2.Format.LeftIndent = -40;
                var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.3cm";
                img.LockAspectRatio = true;
                row1.Cells[1].AddParagraph("( " + section + " - " + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
            }
            else
            {
                if (hEndorse.Count > 0)
                {

                    Paragraph p2 = row1.Cells[1].AddParagraph(" S/H:");
                    p2.AddSpace(8);
                    p2.Format.LeftIndent = -40;
                    var img = p2.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));
                    img.Width = "2.3cm";
                    img.LockAspectRatio = true;
                    row1.Cells[1].AddParagraph("( " + section + " - " + getNameOfUser(hEndorse.FirstOrDefault().DTH_ACTION_BY) + " )");
                }
                else
                {
                    row1.Cells[1].AddParagraph("");
                    row1.Cells[1].AddParagraph("");
                    row1.Cells[1].AddParagraph("");
                    Paragraph p2 = row1.Cells[1].AddParagraph(" S/H:");
                    p2.Format.LeftIndent = -120;
                    row1.Cells[1].AddParagraph("( " + section + " )");
                }

            }

            if (hEndorse.Count > 0)
            {
                var img = p3.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));

                img.Width = "2.3cm";
                img.LockAspectRatio = true;

            }
            else
            {
                if (hApprove.Count > 0)
                {
                    var img = p3.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));

                    img.Width = "2.3cm";
                    img.LockAspectRatio = true;
                }
                else {
                    row1.Cells[2].AddParagraph("");
                    row1.Cells[2].AddParagraph("");
                    row1.Cells[2].AddParagraph("");
                }
            }

            if (hApprove.Count > 0)
            {
                var img = p4.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.3cm";
                img.LockAspectRatio = true;
                //img.WrapFormat.DistanceLeft = 20;
            }
            else
            {
                row1.Cells[3].AddParagraph("");
                row1.Cells[3].AddParagraph("");
                row1.Cells[3].AddParagraph("");
            }


            row1.Cells[2].AddParagraph("CMVP");
            row1.Cells[3].AddParagraph("EVPC");

            doc.LastSection.Add(table);
        }

        public static void genTableAttachFile(Document doc, DAF_DATA dafData)
        {

            Table table = new Table();
            table.Format.Alignment = ParagraphAlignment.Center;
            //table.Borders.Width = 0.4;
            Column column = table.AddColumn(Unit.FromCentimeter(20));
            var listFile = dafData.DAF_ATTACH_FILE.ToList();
            if (listFile.Count > 0)
            {
                doc.AddSection();
                var logoImage = doc.LastSection.AddImage(_FN.GetPathLogo());
                logoImage.WrapFormat.DistanceLeft = 460;
                logoImage.Width = "2.0cm";
                logoImage.LockAspectRatio = true;

                Paragraph header;

                header = doc.LastSection.AddParagraph("Demurrage Approval Form");

                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(doc, dafData);
                genTable2(doc, dafData);


            }
            for (int i = 0; i < listFile.Count; i++)
            {
                Row row = table.AddRow();
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].Format.Font.Bold = true;
                //row.Format.Alignment = ParagraphAlignment.Center;
                Paragraph pImage = row.Cells[0].AddParagraph();
                pImage.Format.Borders.DistanceFromTop = 20;
                string URL = JSONSetting.getGlobalConfig("ROOT_URL");
                var image = pImage.AddImage(GetExplanImage(listFile.ElementAt(i).DAT_PATH + "", URL));
                row.Cells[0].AddParagraph();
                Paragraph pCap = row.Cells[0].AddParagraph(listFile.ElementAt(i).DAT_CAPTION + "");
                if (System.IO.File.Exists(GetExplanImage(listFile.ElementAt(i).DAT_PATH + "", URL)))
                {
                    System.Drawing.Image pic = System.Drawing.Image.FromFile(GetExplanImage(listFile.ElementAt(i).DAT_PATH + "", URL));
                    if (pic.Width > 700)
                    {
                        image.Width = "19.0cm";
                        image.LockAspectRatio = true;
                    }
                    else if (pic.Height > 800)
                    {
                        image.Height = "21.7cm";
                        image.LockAspectRatio = true;
                    }
                }

                image.WrapFormat.DistanceTop = 20;

            }
            doc.LastSection.Add(table);



        }

        public static string GetExplanImage(string PathExplan, string URL = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(URL))
                {
                    string path = Path.Combine(URL, PathExplan);
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(path, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathExplan));
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathExplan);
        }

    }
}