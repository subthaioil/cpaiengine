﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "data_detail")]
    public class DataDetail
    {
        [XmlText]
        public string Text { get; set; }
    }
}