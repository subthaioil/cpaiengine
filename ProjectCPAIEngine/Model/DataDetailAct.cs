﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "data_detail_act")]
    public class DataDetailAct
    {
        [XmlText]
        public string Text { get; set; }
    }
}