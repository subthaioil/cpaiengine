﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class FclVesselParticular
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_initial { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string built { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string age { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_time { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string dwt { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string capacity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string max_draft { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string coating { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string displacement { get; set; } = "";

    }

    public class FclPaymentTerms
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string charterer { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string cargo { get; set; } = "";
        //[DisplayFormat(ConvertEmptyStringToNull = false)]
        //public string quantity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bss { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string load_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharge_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string dem { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string dem_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string add_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string broker_comm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withold_tax { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string transit_loss { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string payment_term { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_comm { get; set; } = "";

        public List<FclCargo> Cargoes { get; set; }
        public List<Port_Control> ControlPort { get; set; }
        public List<Discharge_Control> ControlDischarge { get; set; }
        public string json_port_control { get; set; } = "";
        public string json_discharge_control { get; set; } = "";
    }

    public class FclCargo {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_other { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance { get; set; } = "";
    }

    public class ListDrowDownKey
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }

    }
    public class Port_Control
    {
        public string JettyPortKey { get; set; }
        public string PortType { get; set; }
        public string PortCountry { get; set; }
        public string PortLocation { get; set; }
        public string PortName { get; set; }
        public string PortFullName { get; set; }
        public string PortValue { get; set; }
        public string PortOrder { get; set; }
        public string PortOtherDesc { get; set; }
    }
    public class Discharge_Control
    {
        public string JettyPortKey { get; set; }
        public string PortType { get; set; }
        public string PortCountry { get; set; }
        public string PortLocation { get; set; }
        public string DischargeName { get; set; }
        public string DischargeFullName { get; set; }
        public string DischargeValue { get; set; }
        public string PortOrder { get; set; }
        public string PortOtherDesc { get; set; }
    }

    public class Jetty_Port
    {        
        public string PortType { get; set; }
        public string PortCountry { get; set; }
        public string PortLocation { get; set; }
        public string JettyPortName { get; set; }
    }

    public class FclTimeForOperation
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharge_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ballast { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laden { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tank_cleaning { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_duration { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string distance_ballast { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string distance_laden { get; set; } = "";
    }

    public class FclFull
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ballast { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laden { get; set; } = "";
    }

    public class FclEco
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ballast { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laden { get; set; } = "";
    }

    public class FclVesselSpeed
    {
        public FclFull fcl_full { get; set; } = new FclFull();
        public FclEco fcl_eco { get; set; } = new FclEco();
    }

    public class FclGradeFo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker_purno { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string place { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price { get; set; } = "";
    }

    public class FclGradeMgo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker_id { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string bunker_purno { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string place { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price { get; set; } = "";
    }

    public class FclLatestBunkerPrice
    {
        public FclGradeFo fcl_grade_fo { get; set; } = new FclGradeFo();
        public FclGradeMgo fcl_grade_mgo { get; set; } = new FclGradeMgo();
    }

    public class FclLoad
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclDischarge
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclSteamingBallast
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclSteamingLaden
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclIdle
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclTackCleaning
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclHeating
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclBunkerConsumption
    {
        public FclLoad fcl_load { get; set; } = new FclLoad();
        public FclDischarge fcl_discharge { get; set; } = new FclDischarge();
        public FclSteamingBallast fcl_steaming_ballast { get; set; } = new FclSteamingBallast();
        public FclSteamingLaden fcl_steaming_laden { get; set; } = new FclSteamingLaden();
        public FclIdle fcl_idle { get; set; } = new FclIdle();
        public FclTackCleaning fcl_tack_cleaning { get; set; } = new FclTackCleaning();
        public FclHeating fcl_heating { get; set; } = new FclHeating();
    }

    public class FclPrice
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mgo { get; set; } = "";
    }

    public class FclBunkerCost
    {
        public FclPrice fcl_price { get; set; } = new FclPrice();
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharge_port { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string steaming_ballast { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string steaming_laden { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tank_cleaning { get; set; } = "";
    }

    public class FclOtherCost
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fix_cost { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_fix_cost_per_day { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_commession { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string withold_tax { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string others { get; set; } = "0.0000";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_expenses { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string suggested_margin { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string suggested_margin_value { get; set; } = "";
    }

    public class OfferFreight
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer_by { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string offer_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string mt { get; set; } = "";
    }

    public class TotalVariableCost
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
    }

    public class ProfitOver
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string percent { get; set; } = "";
    }

    public class TotalFixCost
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
    }

    public class TotalCost
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
    }

    public class ProfitOverTotal
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string percent { get; set; } = "";
    }

    public class MarketRef
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string usd { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string baht { get; set; } = "";
    }

    public class FclResults
    {
        public OfferFreight offer_freight { get; set; } = new OfferFreight();
        public TotalVariableCost total_variable_cost { get; set; } = new TotalVariableCost();
        public ProfitOver profit_over { get; set; } = new ProfitOver();
        public TotalFixCost total_fix_cost { get; set; } = new TotalFixCost();
        public TotalCost total_cost { get; set; } = new TotalCost();
        public ProfitOverTotal profit_over_total { get; set; } = new ProfitOverTotal();
        public MarketRef market_ref { get; set; } = new MarketRef();
    }

    public class FclRootObject
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fcl_excel_file_name { get; set; } = "";
        public FclVesselParticular fcl_vessel_particular { get; set; }
        public FclPaymentTerms fcl_payment_terms { get; set; }
        public FclTimeForOperation fcl_time_for_operation { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_port_charge { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total_bunker_cost { get; set; } = "";
        public FclVesselSpeed fcl_vessel_speed { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fcl_exchange_rate { get; set; } = "";
        public string fcl_exchange_date { get; set; } = "";
        public FclLatestBunkerPrice fcl_latest_bunker_price { get; set; }
        public FclBunkerConsumption fcl_bunker_consumption { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fcl_pic_path { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fcl_remark { get; set; } = "";
        public List<FclBunkerCost> fcl_bunker_cost { get; set; }
        public FclOtherCost fcl_other_cost { get; set; }
        public FclResults fcl_results { get; set; }
        public string fcl_button_event { get; set; }
    }
}