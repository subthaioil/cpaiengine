﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    #region-------------FreightList---------------------
    [XmlRoot(ElementName = "transaction")]
    public class FreightTransaction : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }

    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_Freighttrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<FreightTransaction> FreightTransaction { get; set; }
    }
    #endregion-----------------------------------------
}