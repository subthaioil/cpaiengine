﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class HedgDealEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_HedgDealtrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<HedgDealEncrypt> HedgDealEncryptTransaction { get; set; }
    }

    [XmlRoot(ElementName = "transaction")]
    public class HedgTicketEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_HedgTickettrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<HedgTicketEncrypt> HedgTicketEncryptTransaction { get; set; }
    }

}