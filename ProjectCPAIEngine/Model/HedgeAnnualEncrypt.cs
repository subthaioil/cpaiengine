﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class HedgeAnnualEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
        public string type_Encrypted { get; set; }
        public string latest_version { get; set; }
    }

    #region------------HedgeAnnualList---------------------
    [XmlRoot(ElementName = "list_trx")]
    public class List_HedgeAnnualtrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<HedgeAnnualEncrypt> HedgeAnnualTransaction { get; set; }
    }
    #endregion-----------------------------------------
}