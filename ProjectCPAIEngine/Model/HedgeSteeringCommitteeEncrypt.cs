﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class HedgeSteeringCommitteeEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
        public string type_Encrypted { get; set; }
    }

    #region------------HedgeSteeringCommitteeList---------------------
    [XmlRoot(ElementName = "list_trx")]
    public class List_HedgeSteeringCommitteetrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<HedgeSteeringCommitteeEncrypt> HedgeSteeringCommitteeTransaction { get; set; }
    }
    #endregion-----------------------------------------
}