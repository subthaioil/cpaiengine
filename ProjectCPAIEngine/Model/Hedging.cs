﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    #region Hedg Annual Framework

    public class HedgAnnualData
    {
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string version { get; set; }
        public string status { get; set; }
    }

    public class HedgTypeDetail
    {
        public string key { get; set; }
        public string order { get; set; }
        public string product_front { get; set; }
        public string product_back { get; set; }
        public string title { get; set; }
        public string approved_date { get; set; }
        public string unit_price { get; set; }
        public string unit_volume { get; set; }
        public string year { get; set; }
        public string min_max { get; set; }
        public string price_year { get; set; }
        public string price_Q1 { get; set; }
        public string price_Q2 { get; set; }
        public string price_Q3 { get; set; }
        public string price_Q4 { get; set; }
        public string prod_volume { get; set; }
        public string limit_volume { get; set; }
        public string approved_volume { get; set; }
        public string note { get; set; }
    }

    public class HedgTypeData
    {
        public string hedg_type_key { get; set; }
        public List<HedgTypeDetail> hedg_type_detail { get; set; }
    }

    public class HedgAnnualType
    {
        public List<HedgTypeData> hedg_type_data { get; set; }
    }

    public class HedgAnnualSubmitNote
    {
        public string subject { get; set; }
        public string approved_date { get; set; }
        public string ref_no { get; set; }
        public string note { get; set; }
        public string attach_file { get; set; }
    }

    #endregion

    #region Hedg Steering Committee Framework

    public class HedgSTRCMTData
    {
        public string ref_no { get; set; }
        public string version { get; set; }
        public string trading_book { get; set; }
        public string approved_date { get; set; }
        public string tenor_period_from { get; set; }
        public string tenor_period_to { get; set; }
        public string status { get; set; }
        public string hedge_type { get; set; }
        public string m1_m_flag { get; set; }
        public string time_spread { get; set; }
        public string template { get; set; }
        public string consecutive_flag { get; set; }
        public string total_volume { get; set; }
    }

    public class HedgSTRCMTProductDetail
    {
        public string order { get; set; }
        public string af_frame { get; set; }
        public string product_front { get; set; }
        public string product_back { get; set; }
        public string weight_percent { get; set; }
        public string weight_volume { get; set; }
        public string af_price { get; set; }
        public string price_unit { get; set; }
        public string type { get; set; }
    }

    public class HedgSTRCMTSubjectProduct
    {
        public string avg_af_price { get; set; }
        public List<HedgSTRCMTProductDetail> HedgSTRCMTProductDetail { get; set; }
    }

    public class HedgSTRCMTObjectProduct
    {
        public string avg_af_price { get; set; }
        public List<HedgSTRCMTProductDetail> HedgSTRCMTProductDetail { get; set; }
    }

    public class HedgSTRCMTMainChoiceDetail
    {
        public string order { get; set; }
        public string name { get; set; }
        public string package_name { get; set; }
        public string type { get; set; }
        public string operation { get; set; }
        public string net_af_price { get; set; }
        public HedgSTRCMTSubjectProduct HedgSTRCMTSubjectProduct { get; set; }
        public HedgSTRCMTObjectProduct HedgSTRCMTObjectProduct { get; set; }
    }

    public class HedgSTRCMTMainChoice
    {
        public string unit { get; set; }
        public string main_volume { get; set; }
        public List<HedgSTRCMTMainChoiceDetail> HedgSTRCMTMainChoiceDetail { get; set; }
    }

    public class HedgSTRCMTExtension
    {
        public string order { get; set; }
        public string extend_period_from { get; set; }
        public string extend_period_to { get; set; }
        public string exercise_date { get; set; }
        public string extend_volume { get; set; }
    }

    public class HedgSTRCMTOptionDetail
    {
        public string order { get; set; }
        public string net_target_value { get; set; }
        public string return_value { get; set; }
        public string premium_value { get; set; }
    }

    public class HedgSTRCMTOption
    {
        public string order { get; set; }
        public string name { get; set; }
        public List<HedgSTRCMTOptionDetail> HedgSTRCMTOptionDetail { get; set; }
    }

    public class HedgSTRCMTNetPremium
    {
        public List<HedgSTRCMTOption> HedgSTRCMTOption { get; set; }
    }

    public class HedgSTRCMTFormulaDetail
    {
        public string order { get; set; }
        public string type { get; set; }
        public string value { get; set; }
    }

    public class HedgSTRCMTFormula
    {
        public string expression { get; set; }
        public List<HedgSTRCMTFormulaDetail> HedgSTRCMTFormulaDetail { get; set; }
    }

    public class HedgSTRCMTKIOOption
    {
        public string option { get; set; }
        public string activated_option { get; set; }
        public string activated_operation { get; set; }
        public string hedge_price { get; set; }
        public string deactivated_option { get; set; }
        public string deactivated_operation { get; set; }
    }

    public class HedgSTRCMTToolDetail
    {
        public string order { get; set; }
        public string name { get; set; }
        public string use_net_premium_flag { get; set; }
        public string net_premium_type { get; set; }
        public string net_premium_volume { get; set; }
        public string use_formula_flag { get; set; }
        public string target_redemption_gain_loss { get; set; }
        public string target_redemption_per_unit { get; set; }
        public string knock_in_out_by_package { get; set; }
        public string knock_in_out_by_market { get; set; }
        public string capped_value { get; set; }
        public string swaption_exercise_date { get; set; }
        public HedgSTRCMTKIOOption knock_in_out_package_option { get; set; }
        public HedgSTRCMTKIOOption knock_in_out_market_option { get; set; }
        public List<HedgSTRCMTExtension> HedgSTRCMTExtension { get; set; }
        public HedgSTRCMTNetPremium HedgSTRCMTNetPremium { get; set; }
        public HedgSTRCMTFormula HedgSTRCMTFormula { get; set; }
    }

    public class HedgSTRCMTTool
    {
        public List<HedgSTRCMTToolDetail> HedgSTRCMTToolDetail { get; set; }
    }

    public class HedgSTRCMTExtendChoiceDetail
    {
        public string order { get; set; }
        public string name { get; set; }
        public string package_name { get; set; }
        public string type { get; set; }
        public string operation { get; set; }
        public string net_af_price { get; set; }
        public HedgSTRCMTSubjectProduct HedgSTRCMTSubjectProduct { get; set; }
        public HedgSTRCMTObjectProduct HedgSTRCMTObjectProduct { get; set; }
    }

    public class HedgSTRCMTExtendChoice
    {
        public string unit { get; set; }
        public string extend_volume { get; set; }
        public List<HedgSTRCMTExtendChoiceDetail> HedgSTRCMTExtendChoiceDetail { get; set; }
    }

    public class HedgSTRCMTSubmitNote
    {
        public string subject { get; set; }
        public string approved_date { get; set; }
        public string ref_no { get; set; }
        public string note { get; set; }
        public string attach_file { get; set; }
    }

    #endregion

    public class HedgingRootObject
    {
        public HedgAnnualData hedg_annual_data { get; set; }
        public HedgAnnualType hedg_annual_type { get; set; }
        public HedgAnnualSubmitNote hedg_annual_submit_note { get; set; }
        public HedgSTRCMTData hedg_STRCMT_data { get; set; }
        public HedgSTRCMTMainChoice hedg_STRCMT_main_choice { get; set; }
        public HedgSTRCMTTool hedg_STRCMT_tool { get; set; }
        public HedgSTRCMTExtendChoice hedg_STRCMT_extend_choice { get; set; }
        public HedgSTRCMTSubmitNote hedg_STRCMT_submit_note { get; set; }
        public string hedg_annual_note { get; set; }
        public string hedg_str_cmt_note { get; set; }
        public string attach_file { get; set; }
    }
}