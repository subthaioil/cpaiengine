﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    #region-------------BungerList---------------------
    [XmlRoot(ElementName = "transaction")]
    public class BungerTransaction
    {
        [XmlAttribute(AttributeName = "function_id")]
        public string Function_id { get; set; }
        [XmlAttribute(AttributeName = "function_desc")]
        public string Function_desc { get; set; }
        [XmlAttribute(AttributeName = "result_namespace")]
        public string Result_namespace { get; set; }
        [XmlAttribute(AttributeName = "result_status")]
        public string Result_status { get; set; }
        [XmlAttribute(AttributeName = "result_code")]
        public string Result_code { get; set; }
        [XmlAttribute(AttributeName = "result_desc")]
        public string Result_desc { get; set; }
        [XmlAttribute(AttributeName = "req_transaction_id")]
        public string Req_transaction_id { get; set; }
        [XmlAttribute(AttributeName = "transaction_id")]
        public string Transaction_id { get; set; }
        [XmlAttribute(AttributeName = "response_message")]
        public string Response_message { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "action")]
        public string Action { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "date_purchase")]
        public string Date_purchase { get; set; }
        [XmlAttribute(AttributeName = "vessel")]
        public string Vessel { get; set; }
        [XmlAttribute(AttributeName = "supplying_location")]
        public string Supplying_location { get; set; }
        [XmlAttribute(AttributeName = "purchase_no")]
        public string Purchase_no { get; set; }
        [XmlAttribute(AttributeName = "products")]
        public string Products { get; set; }
        [XmlAttribute(AttributeName = "volumes")]
        public string Volumes { get; set; }
        [XmlAttribute(AttributeName = "create_by")]
        public string Create_by { get; set; }
        [XmlAttribute(AttributeName = "trip_no")]
        public string Trip_no { get; set; }
        [XmlAttribute(AttributeName = "reason")]
        public string Reason { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_Bunkertrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<BungerTransaction> BungerTransaction { get; set; }
    }
    #endregion-----------------------------------------

    #region-------------CharteringList---------------------
    [XmlRoot(ElementName = "transaction")]
    public class CharteringTransaction
    {
        [XmlAttribute(AttributeName = "function_id")]
        public string Function_id { get; set; }
        [XmlAttribute(AttributeName = "function_desc")]
        public string Function_desc { get; set; }
        [XmlAttribute(AttributeName = "result_namespace")]
        public string Result_namespace { get; set; }
        [XmlAttribute(AttributeName = "result_status")]
        public string Result_status { get; set; }
        [XmlAttribute(AttributeName = "result_code")]
        public string Result_code { get; set; }
        [XmlAttribute(AttributeName = "result_desc")]
        public string Result_desc { get; set; }
        [XmlAttribute(AttributeName = "req_transaction_id")]
        public string Req_transaction_id { get; set; }
        [XmlAttribute(AttributeName = "transaction_id")]
        public string Transaction_id { get; set; }
        [XmlAttribute(AttributeName = "response_message")]
        public string Response_message { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "action")]
        public string Action { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "date_purchase")]
        public string Date_purchase { get; set; }
        [XmlAttribute(AttributeName = "vessel")]
        public string Vessel { get; set; }
        [XmlAttribute(AttributeName = "cust_name")]
        public string Cust_name { get; set; }
        [XmlAttribute(AttributeName = "laycan_from")]
        public string Laycan_from { get; set; }
        [XmlAttribute(AttributeName = "laycan_to")]
        public string Laycan_to { get; set; }
        [XmlAttribute(AttributeName = "purchase_no")]
        public string Purchase_no { get; set; }
        [XmlAttribute(AttributeName = "create_by")]
        public string Create_by { get; set; }
        [XmlAttribute(AttributeName = "reason")]
        public string Reason { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_Charteringtrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<CharteringTransaction> CharteringTransaction { get; set; }
    }
    #endregion-----------------------------------------

    #region--------------TCE-----------------------
    [XmlRoot(ElementName = "list_trx")]
    public class List_TCEtrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<Transaction> TCETransaction { get; set; }
    }
    #endregion-----------------------------------------


   
    public class CounterTxn
    {
        public string function { get; set; }
        public string counter { get; set; }
    }

    public class CounterDetail
    {
        public List<CounterTxn> counter_txn { get; set; }
    }
}
