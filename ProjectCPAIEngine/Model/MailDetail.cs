﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class MailDetail
    {
        public String subject { get; set; }
        public String body { get; set; }

        public String user { get; set; }
        public String type { get; set; }
        public String system { get; set; }
        public String createby { get; set; }
    }
}