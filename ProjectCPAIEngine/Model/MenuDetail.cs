﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class MenuDetail
    {
        //public class Node2
        //{
        //    public string menu_id { get; set; }
        //    public string group_menu { get; set; }
        //    public string parent_id { get; set; }
        //    public string lng_description { get; set; }
        //    public string menu_url { get; set; }
        //    public string menu_img { get; set; }
        //    public string menu_level { get; set; }
        //    public string menu_list_no { get; set; }
        //    public string menu_control_type { get; set; }
        //    public string menu_action { get; set; }
        //    public string menu_url_direct { get; set; }
        //    public string parent_detail { get; set; }
        //    public string tags { get; set; }

        //    public string text { get; set; }
        //}

        //public class Node
        //{
        //    public string menu_id { get; set; }
        //    public string group_menu { get; set; }
        //    public string parent_id { get; set; }
        //    public string lng_description { get; set; }
        //    public string menu_url { get; set; }
        //    public string menu_img { get; set; }
        //    public string menu_level { get; set; }
        //    public string menu_list_no { get; set; }
        //    public string menu_control_type { get; set; }
        //    public string menu_action { get; set; }
        //    public string menu_url_direct { get; set; }
        //    public string parent_detail { get; set; }

        //    public string text { get; set; }
        //    public string tags { get; set; }

        //    public List<Node2> nodes { get; set; }
        //}

        public class Node
        {
            public string menu_id { get; set; }
            public string text { get; set; }
            public string icon { get; set; }
            //public string selectedIcon { get; set; }
            public string color { get; set; }
            public string backColor { get; set; }
            //public string href { get; set; }
            //public string selectable { get; set; }
            public List<string> tags { get; set; }
            public List<Node> nodes { get; set; }
        }

        public class MenuRootObject : Node
        {
            public string group_menu { get; set; }
            public string parent_id { get; set; }
            public string lng_description { get; set; }
            public string menu_url { get; set; }
            public string menu_img { get; set; }
            public string menu_level { get; set; }
            public string menu_list_no { get; set; }
            public string menu_control_type { get; set; }
            public string menu_active { get; set; }
            public string menu_url_direct { get; set; }
            public string parent_detail { get; set; }
          
        }
    }
}