﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{

    public class Menu
        {
            public string name { get; set; }
            public string tab_name { get; set; }
            public string tab { get; set; }
            public string status { get; set; }
        }

    public class MenuTab{
        public List<Menu> menu { get; set; }



    }

    
}