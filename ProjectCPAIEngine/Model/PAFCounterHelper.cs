﻿using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class PAFCounterHelper
    {
        EntitiesEngine entity = new EntitiesEngine();
        EntityCPAIEngine cpai_entity = new EntityCPAIEngine();

        public DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DateTime parsePurchaseDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = new DateTime();
                if (dateTimeString.Contains('-'))
                {
                    if (dateTimeString.Contains(':'))
                    {
                        r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (dateTimeString.Contains(':'))
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else
                {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                //DateTime r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FUNCTION_TRANSACTION> FindByTransactionByFuncIndex(string[] function, string[] index, string[] type, string user_group, string from_date_str, string to_date_str)
        {
            try
            {
                List<FUNCTION_TRANSACTION> lstFunction = new List<FUNCTION_TRANSACTION>();
                string query = @"SELECT FUNCTION_TRANSACTION.FTX_ROW_ID AS FTX_ROW_ID, FUNCTION_TRANSACTION.FTX_FK_FUNCTION, FUNCTION_TRANSACTION.FTX_TRANS_ID, FUNCTION_TRANSACTION.FTX_APP_ID, FUNCTION_TRANSACTION.FTX_REQ_TRANS, FUNCTION_TRANSACTION.FTX_RETURN_STATUS, FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE, FUNCTION_TRANSACTION.FTX_RETURN_CODE, FUNCTION_TRANSACTION.FTX_RETURN_DESC, FUNCTION_TRANSACTION.FTX_STATE_ACTION, FUNCTION_TRANSACTION.FTX_CURRENT_NAME_SPACE, FUNCTION_TRANSACTION.FTX_CURRENT_CODE, FUNCTION_TRANSACTION.FTX_CURRENT_DESC, FUNCTION_TRANSACTION.FTX_REQUEST_DATE, FUNCTION_TRANSACTION.FTX_RESPONSE_DATE, FUNCTION_TRANSACTION.FTX_ACCUM_RETRY, FUNCTION_TRANSACTION.FTX_RETRY_COUNT, FUNCTION_TRANSACTION.FTX_CURRENT_STATE, FUNCTION_TRANSACTION.FTX_NEXT_STATE, FUNCTION_TRANSACTION.FTX_WAKEUP, FUNCTION_TRANSACTION.FTX_FWD_RESP_CODE_FLAG, FUNCTION_TRANSACTION.FTX_CREATED, FUNCTION_TRANSACTION.FTX_CREATED_BY, FUNCTION_TRANSACTION.FTX_UPDATED, FUNCTION_TRANSACTION.FTX_UPDATED_BY, FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE, FUNCTION_TRANSACTION.FTX_PARENT_TRX_ID, FUNCTION_TRANSACTION.FTX_PARENT_ACT_ID, FUNCTION_TRANSACTION.FTX_INDEX1, FUNCTION_TRANSACTION.FTX_INDEX2, FUNCTION_TRANSACTION.FTX_INDEX3, PAF_DATA.PDA_STATUS AS FTX_INDEX4, FUNCTION_TRANSACTION.FTX_INDEX5, TO_CHAR( PAF_DATA.PDA_CREATED, 'DD/MM/YYYY HH24:MI:SS') AS FTX_INDEX6, FUNCTION_TRANSACTION.FTX_INDEX7, FUNCTION_TRANSACTION.FTX_INDEX8, FUNCTION_TRANSACTION.FTX_INDEX9,( SELECT SUBSTR( CPAI_USER_GROUP.USG_USER_GROUP, 0, 4) FROM CPAI_USER_GROUP WHERE CPAI_USER_GROUP.USG_FK_USERS = USERS.USR_ROW_ID AND ROWNUM = 1) AS FTX_INDEX10, FUNCTION_TRANSACTION.FTX_INDEX11, FUNCTION_TRANSACTION.FTX_INDEX12, FUNCTION_TRANSACTION.FTX_INDEX13, FUNCTION_TRANSACTION.FTX_INDEX14, FUNCTION_TRANSACTION.FTX_INDEX15, FUNCTION_TRANSACTION.FTX_INDEX16, FUNCTION_TRANSACTION.FTX_INDEX17, FUNCTION_TRANSACTION.FTX_INDEX18, FUNCTION_TRANSACTION.FTX_INDEX19, FUNCTION_TRANSACTION.FTX_INDEX20 FROM FUNCTION_TRANSACTION JOIN PAF_DATA ON PAF_DATA.PDA_ROW_ID = FUNCTION_TRANSACTION.FTX_TRANS_ID LEFT JOIN USERS ON LOWER( FUNCTION_TRANSACTION.FTX_INDEX9) = LOWER(USERS.USR_LOGIN)";
                lstFunction = entity.Database.SqlQuery<FUNCTION_TRANSACTION>(query).ToList();
                if(index.Count() > 0)
                {
                    lstFunction = lstFunction.Where(m => index.Contains(m.FTX_INDEX4)).ToList();
                }
                if (!string.IsNullOrEmpty(from_date_str) && !string.IsNullOrEmpty(to_date_str))
                {
                    DateTime fDate = parseInputDate(from_date_str);
                    DateTime tDate = parseInputDate(to_date_str);
                    lstFunction = lstFunction.Where(x => x.FTX_INDEX6 != null && (parsePurchaseDate(x.FTX_INDEX6) >= fDate) && (parsePurchaseDate(x.FTX_INDEX6) <= tDate)).ToList();
                }
                if (user_group.Contains("CMPS") || user_group.Contains("CMLA"))
                {
                    lstFunction = lstFunction.Where(m => (m.FTX_INDEX10 != null) && user_group.Contains(m.FTX_INDEX10)).ToList();
                }

                return lstFunction;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}