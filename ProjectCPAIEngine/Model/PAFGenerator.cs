﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class PAFGenerator
    {
        private static double NORMAL_ROW_HEIGHT = 10;
        private static ShareFn _FN = new ShareFn();

        //private static string LOGO_PATH = "C:\\test\\logo_thaioil.png";

        public static String getMinMaxFormat(Nullable<Decimal> min, Nullable<Decimal> max)
        {
            if (min != max)
            {
                return string.Format("{0:n0}", min) + " - " + string.Format("{0:n0}", max);
            }
            else
            {
                return string.Format("{0:n0}", min);
            }
        }

        public static String getPriceMinMaxFormat(Nullable<Decimal> min, Nullable<Decimal> max)
        {
            if (min != max)
            {
                return string.Format("{0:n}", min) + " - " + string.Format("{0:n}", max);
            }
            else
            {
                return string.Format("{0:n}", min);
            }
        }

        public static String getNameFromCustNum(String custNum, String companyCode)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MT_CUST_DETAIL.Where(c => c.MCD_FK_CUS == custNum && c.MCD_FK_COMPANY == companyCode && c.MCD_NATION == "I").FirstOrDefault() == null)
            {
                return "";
            }
            else
            {
                MT_CUST_DETAIL cd = db.MT_CUST_DETAIL.Where(c => c.MCD_FK_CUS == custNum && c.MCD_FK_COMPANY == companyCode && c.MCD_NATION == "I").FirstOrDefault();
                String name = cd.MCD_NAME_1 + " " + cd.MCD_NAME_2;
                return name;
            }
        }

        public static String getNameFromVendor(String vendor)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            return db.MT_VENDOR.Where(c => c.VND_ACC_NUM_VENDOR == vendor).FirstOrDefault().VND_NAME1 + "";
        }

        public static String getNameFromBenchMark(decimal benchMark) // done
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MKT_MST_MOPS_PRODUCT.Where(c => c.M_MPR_ID == benchMark).FirstOrDefault() != null)
            {
                return db.MKT_MST_MOPS_PRODUCT.Where(c => c.M_MPR_ID == benchMark).FirstOrDefault().M_MPR_PRDNAME + "";
            }
            return "";
        }

        public static String getNameFromCountry(String country) // done
        {

            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MT_COUNTRY.Where(c => c.MCT_ROW_ID == country).FirstOrDefault() != null)
            {
                return db.MT_COUNTRY.Where(c => c.MCT_ROW_ID == country).FirstOrDefault().MCT_LANDX + "";
            }
            return "";
        }

        public static String genPDF(PAF_DATA pafData)
        {
            pafData.PAF_ATTACH_FILE = pafData.PAF_ATTACH_FILE.OrderBy(b => b.PAT_ROW_ID).ToList();
            pafData.PAF_BIDDING_ITEMS = pafData.PAF_BIDDING_ITEMS.OrderBy(b => b.PBI_ROW_ID).ToList();
            pafData.PAF_PAYMENT_ITEMS = pafData.PAF_PAYMENT_ITEMS.OrderBy(b => b.PPI_ROW_ID).ToList();
            for (int i = 0; i < pafData.PAF_PAYMENT_ITEMS.Count; i++)
            {
                pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL = pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL.OrderBy(b => b.PPD_ROW_ID).ToList();
            }
            pafData.PAF_PROPOSAL_ITEMS = pafData.PAF_PROPOSAL_ITEMS.OrderBy(b => b.PSI_ROW_ID).ToList();
            for (int i = 0; i < pafData.PAF_PROPOSAL_ITEMS.Count; i++)
            {
                pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PAF_PROPOSAL_OTC_ITEMS = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PAF_PROPOSAL_OTC_ITEMS.OrderBy(b => b.PSO_ROW_ID).ToList();
            }
            for (int i = 0; i < pafData.PAF_PROPOSAL_ITEMS.Count; i++)
            {
                pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PAF_PROPOSAL_REASON_ITEMS = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PAF_PROPOSAL_REASON_ITEMS.OrderBy(b => b.PSR_ROW_ID).ToList();
            }
            pafData.PAF_REVIEW_ITEMS = pafData.PAF_REVIEW_ITEMS.OrderBy(b => b.PRI_ROW_ID).ToList();
            for (int i = 0; i < pafData.PAF_REVIEW_ITEMS.Count; i++)
            {
                pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL = pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.OrderBy(b => b.PRD_ROW_ID).ToList();
            }

            for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
            {
                pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND = pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.OrderBy(b => b.PRN_ROW_ID).ToList();
            }

            for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
            {
                pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK = pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK.OrderBy(b => b.PBB_ROW_ID).ToList();
            }

            pafData.PAF_BTM_GRADE_ITEMS = pafData.PAF_BTM_GRADE_ITEMS.OrderBy(b => b.PBG_ROW_ID).ToList();
            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.Count; i++)
            {
                pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS = pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS.OrderBy(b => b.PBA_ROW_ID).ToList();
            }
            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.Count; i++)
            {
                pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_GRADE_ASSUME_ITEMS = pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_GRADE_ASSUME_ITEMS.OrderBy(b => b.PGA_ROW_ID).ToList();
            }
            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.Count; i++)
            {
                for (int j = 0; j < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS.Count; j++)
                {
                    pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS.ElementAt(j).PAF_BTM_ALLOCATE_ITEMS_DETAIL = pafData.PAF_BTM_GRADE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS.ElementAt(j).PAF_BTM_ALLOCATE_ITEMS_DETAIL.OrderBy(b => b.PAD_ROW_ID).ToList();

                }
            }

            Document document = new Document();
            document.DefaultPageSetup.RightMargin = 18;
            document.DefaultPageSetup.LeftMargin = 18;
            document.DefaultPageSetup.TopMargin = 18;
            document.DefaultPageSetup.BottomMargin = 25;
            document.AddSection();

            Style style = document.Styles["Normal"];
            style.Font.Name = "Tahoma";
            style.Font.Size = 5;

            var logoImage = document.LastSection.AddImage(_FN.GetPathLogo());
            string x = _FN.GetPathLogo();
            logoImage.WrapFormat.DistanceLeft = 460;
            logoImage.Width = "2.0cm";
            logoImage.LockAspectRatio = true;

            if (pafData.PDA_TEMPLATE == "CMPS_DOM_SALE")
            {
                Paragraph header = document.LastSection.AddParagraph("Product Sale Approval Form");
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(document, pafData);
                genTable2(document, pafData);
                genTable3(document, pafData);
                if (pafData.PDA_INCLUDED_REVIEW_TABLE == "Y")
                {
                    genTable4(document, pafData);
                }
                if (pafData.PDA_INCLUDED_BIDDING_TABLE == "Y")
                {
                    genTable5(document, pafData);
                }
                genTable6(document, pafData);
                genTable7(document, pafData);
                genTable8(document, pafData);
                genTable9(document, pafData);
                genTable10(document, pafData);
                genTableAttachFile(document, pafData);

            }
            else if (pafData.PDA_TEMPLATE == "CMPS_INTER_SALE")
            {
                if (pafData.PAF_BIDDING_ITEMS.Count > 0)
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(0).PAF_ROUND.Count <= 3)
                    {
                        Paragraph header;
                        header = document.LastSection.AddParagraph("Product Sale Approval Form");

                        header.Format.Alignment = ParagraphAlignment.Center;
                        header.Format.Font.Bold = true;
                        header.Format.Font.Size = 6;

                        genTable1(document, pafData);
                        genTable2(document, pafData);
                        genTable3(document, pafData);
                        genTable11(document, pafData, 1);
                        genTable6(document, pafData);
                        genTable12(document, pafData);
                        genTable9(document, pafData);
                        genTable10(document, pafData);
                        genTableAttachFile(document, pafData);
                    }
                    else
                    {


                        Paragraph header;
                        header = document.LastSection.AddParagraph("Product Sale Approval Form");
                        header.Format.Alignment = ParagraphAlignment.Center;
                        header.Format.Font.Bold = true;
                        header.Format.Font.Size = 6;

                        genTable1(document, pafData);
                        genTable2(document, pafData);
                        genTable3(document, pafData);
                        genTable11(document, pafData, 1);
                        genTable6(document, pafData);
                        genTable12(document, pafData);
                        genTable9(document, pafData);

                        document.AddSection();

                        var logoImage2 = document.LastSection.AddImage(_FN.GetPathLogo());
                        logoImage2.WrapFormat.DistanceLeft = 460;
                        logoImage2.Width = "2.0cm";
                        logoImage2.LockAspectRatio = true;

                        Paragraph header2;
                        header2 = document.LastSection.AddParagraph("Product Sale Approval Form");
                        header2.Format.Alignment = ParagraphAlignment.Center;
                        header2.Format.Font.Bold = true;
                        header2.Format.Font.Size = 6;

                        genTable1(document, pafData);
                        genTable2(document, pafData);
                        genTable3(document, pafData);
                        genTable11(document, pafData, 4);
                        genTable6(document, pafData);
                        genTable12(document, pafData);
                        genTable9(document, pafData);
                        genTable10(document, pafData);

                        genTableAttachFile(document, pafData);
                    }
                }


            }
            else if (pafData.PDA_TEMPLATE == "CMPS_IMPORT")
            {
                if (pafData.PAF_BIDDING_ITEMS.Count > 0)
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(0).PAF_ROUND.Count <= 3)
                    {
                        Paragraph header;
                        header = document.LastSection.AddParagraph("Component Import Approval Form");

                        header.Format.Alignment = ParagraphAlignment.Center;
                        header.Format.Font.Bold = true;
                        header.Format.Font.Size = 6;

                        genTable1(document, pafData);
                        genTable2(document, pafData);
                        genTable3(document, pafData);
                        genTable11(document, pafData, 1);
                        genTable6(document, pafData);
                        genTable12(document, pafData);
                        genTable9(document, pafData);
                        genTable10(document, pafData);
                        genTableAttachFile(document, pafData);
                    }
                    else
                    {


                        Paragraph header;
                        header = document.LastSection.AddParagraph("Component Import Approval Form");
                        header.Format.Alignment = ParagraphAlignment.Center;
                        header.Format.Font.Bold = true;
                        header.Format.Font.Size = 6;

                        genTable1(document, pafData);
                        genTable2(document, pafData);
                        genTable3(document, pafData);
                        genTable11(document, pafData, 1);
                        genTable6(document, pafData);
                        genTable12(document, pafData);
                        genTable9(document, pafData);

                        document.AddSection();

                        var logoImage2 = document.LastSection.AddImage(_FN.GetPathLogo());
                        logoImage2.WrapFormat.DistanceLeft = 460;
                        logoImage2.Width = "2.0cm";
                        logoImage2.LockAspectRatio = true;

                        Paragraph header2;
                        header2 = document.LastSection.AddParagraph("Component Import Approval Form");
                        header2.Format.Alignment = ParagraphAlignment.Center;
                        header2.Format.Font.Bold = true;
                        header2.Format.Font.Size = 6;

                        genTable1(document, pafData);
                        genTable2(document, pafData);
                        genTable3(document, pafData);
                        genTable11(document, pafData, 4);
                        genTable6(document, pafData);
                        genTable12(document, pafData);
                        genTable9(document, pafData);
                        genTable10(document, pafData);

                        genTableAttachFile(document, pafData);
                    }
                }
            }
            else if (pafData.PDA_TEMPLATE == "CMLA_FORM_1")
            {
                Paragraph header = document.LastSection.AddParagraph("Product Sale Approval Form");
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(document, pafData);
                genTable2(document, pafData);
                genTable3(document, pafData);
                if (pafData.PDA_INCLUDED_REVIEW_TABLE == "Y")
                {
                    genTable13(document, pafData);
                }
                if (pafData.PDA_INCLUDED_BIDDING_TABLE == "Y")
                {
                    genTable14(document, pafData);
                }
                genTable15(document, pafData);
                genTable16(document, pafData);
                genTable9(document, pafData);
                genTable10(document, pafData);
                genTableAttachFile(document, pafData);
            }

            else if (pafData.PDA_TEMPLATE == "CMLA_FORM_2")
            {
                Paragraph header = document.LastSection.AddParagraph("Product Sale Approval Form");
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(document, pafData);
                genTable2(document, pafData);
                genTable3(document, pafData);
                if (pafData.PDA_INCLUDED_REVIEW_TABLE == "Y")
                {
                    genTable13(document, pafData);
                }
                if (pafData.PDA_INCLUDED_BIDDING_TABLE == "Y")
                {
                    genTable17(document, pafData);
                }
                genTable15(document, pafData);
                genTable16(document, pafData);
                genTable9(document, pafData);
                genTable10(document, pafData);
                genTableAttachFile(document, pafData);
            }
            else if (pafData.PDA_TEMPLATE == "CMLA_IMPORT")
            {
                Paragraph header = document.LastSection.AddParagraph("Product Purchasing Approval Form");
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(document, pafData);
                genTable2(document, pafData);
                genTable3(document, pafData);


                genTable18(document, pafData);

                genTable15(document, pafData);
                genTable16(document, pafData);
                genTable9(document, pafData);
                genTable10(document, pafData);
                genTableAttachFile(document, pafData);
            }

            else if (pafData.PDA_TEMPLATE == "CMLA_BITUMEN")
            {
                Paragraph header = document.LastSection.AddParagraph("Product Sale Approval Form (Bitumen)");
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(document, pafData);
                genTable19(document, pafData);
                genTable20(document, pafData);
                genTable21(document, pafData);
                genTable22(document, pafData);
                genTable9(document, pafData);
                genTable10(document, pafData);
                genTableAttachFile(document, pafData);
            }
            else if (pafData.PDA_TEMPLATE == "CMPS_OTHER")
            {
                Paragraph header = document.LastSection.AddParagraph("Product Approval Form");
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(document, pafData);
                genTable23(document, pafData);
                genTable3(document, pafData);
                genTable9(document, pafData);
                genTable10(document, pafData);
                genTableAttachFile(document, pafData);
            }

            PdfDocumentRenderer docRend = new PdfDocumentRenderer(true);
            docRend.Document = document;
            docRend.RenderDocument();
            //string fileName = "C:\\test\\doc.pdf";
            String n = string.Format("PAF{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", n);
            docRend.PdfDocument.Save(fileName);
            //ProcessStartInfo processInfo = new ProcessStartInfo();
            //processInfo.FileName = fileName;
            //Process.Start(processInfo);
            return n;
        }

        public static void genTable1(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(13));
            Column column2 = table.AddColumn(Unit.FromCentimeter(3));
            Column column3 = table.AddColumn(Unit.FromCentimeter(4));
            Row row1 = table.AddRow();
            row1.Cells[0].Borders.Left.Visible = false;
            row1.Cells[0].Borders.Top.Visible = false;
            row1.Cells[0].Borders.Bottom.Visible = false;

            row1.Cells[1].AddParagraph("Form ID : " + pafData.PDA_FORM_ID);
            if (pafData.PDA_CREATED != null)
            {
                row1.Cells[2].AddParagraph("Date: " + ((DateTime)pafData.PDA_CREATED).ToString("dd/MMM/yyyy"));
            }

            table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable2(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(4));
            Column column2 = table.AddColumn(Unit.FromCentimeter(16));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            column1.Format.Font.Bold = true;
            column1.Format.Alignment = ParagraphAlignment.Center;

            Paragraph p1 = row1.Cells[0].AddParagraph("For:");
            Paragraph p2 = row2.Cells[0].AddParagraph(pafData.PDA_TYPE + "");
            if (pafData.PDA_TYPE == null)
            {
                p2.AddText("Type of Sale");
            }


            if (pafData.MT_COMPANY != null)
            {
                Paragraph p3 = row1.Cells[1].AddParagraph(pafData.MT_COMPANY.MCO_SHORT_NAME + "");
            }
            if (pafData.PDA_TYPE_NOTE != null)
            {
                Paragraph p4 = row2.Cells[1].AddParagraph(pafData.PDA_SUB_TYPE + ": " + pafData.PDA_TYPE_NOTE);
            }
            else
            {
                Paragraph p4 = row2.Cells[1].AddParagraph(pafData.PDA_SUB_TYPE + "");
            }

            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.2;
            doc.LastSection.Add(table);
        }

        public static void genTable3(Document doc, PAF_DATA pafData)
        {

            Table table = new Table();
            table.Borders.Width = 0.4;

            Column column1 = table.AddColumn(Unit.FromCentimeter(20));
            Row row1 = table.AddRow();
            row1.Height = 60;


            Paragraph p1 = row1.Cells[0].AddParagraph("Brief Market Situation (as necessary)");
            p1.Format.Font.Bold = true;


            if (!string.IsNullOrEmpty(pafData.PDA_BRIEF))
            {
                Paragraph p2 = row1.Cells[0].AddParagraph();
                HtmlRemoval htmlRemove = new HtmlRemoval();
                htmlRemove.ValuetoParagraph(pafData.PDA_BRIEF, p2);
            }
            else
            {
                row1.Cells[0].AddParagraph("");
            }
            table.Rows[0].Borders.Top.Width = 0.2;
            doc.LastSection.Add(table);

        }

        public static void genTable4(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(2.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(2));
            Column column3 = table.AddColumn(Unit.FromCentimeter(1));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column6 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column7 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column8 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column9 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column10 = table.AddColumn(Unit.FromCentimeter(5.5));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Cells[0].MergeDown = 2;
            row1.Cells[1].MergeDown = 2;
            row1.Cells[2].MergeDown = 2;
            row1.Cells[9].MergeDown = 2;

            row1.Cells[3].MergeRight = 5;
            row2.Cells[3].MergeRight = 1;
            row2.Cells[5].MergeRight = 1;
            row2.Cells[7].MergeRight = 1;

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("Customer");
            row1.Cells[1].AddParagraph("Product");
            row1.Cells[2].AddParagraph("Unit");
            row1.Cells[3].AddParagraph("Contract").Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[9].AddParagraph("Notes");

            row2.Cells[3].AddParagraph("Existing");
            row2.Cells[5].AddParagraph("Customer proposal");
            row2.Cells[7].AddParagraph("Our proposal");

            row3.Cells[3].AddParagraph("Quantity");
            row3.Cells[4].AddParagraph("Price or formula");
            row3.Cells[5].AddParagraph("Quantity");
            row3.Cells[6].AddParagraph("Price or formula");
            row3.Cells[7].AddParagraph("Quantity");
            row3.Cells[8].AddParagraph("Price or formula");

            for (int i = 0; i < pafData.PAF_REVIEW_ITEMS.Count; i++)
            {
                Row row = table.AddRow();
                if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST != null)
                {
                    //row.Cells[0].AddParagraph(getNameFromCustNum(pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM, pafData.PDA_FOR_COMPANY));

                    if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        MT_CUST_DETAIL cd = pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        row.Cells[0].AddParagraph(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2);
                    }

                }
                if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_MATERIALS != null)
                {
                    row.Cells[1].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                }
                row.Cells[2].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PRI_QUANTITY_UNIT + "");

                for (int j = 0; j < pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.Count; j++)
                {
                    Row subRow;
                    if (j == 0)
                    {
                        subRow = row;
                    }
                    else
                    {
                        subRow = table.AddRow();
                    }
                    if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_EXISTING_QUANTITY_MIN != null && pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_EXISTING_QUANTITY_MAX != null)
                    {
                        subRow.Cells[3].AddParagraph(getMinMaxFormat(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_EXISTING_QUANTITY_MIN, pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_EXISTING_QUANTITY_MAX));
                    }
                    subRow.Cells[4].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_EXISTING_PRICE_FLOAT + "");

                    if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_CUS_PROPOSAL_QUANTITY_MIN != null && pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_CUS_PROPOSAL_QUANTITY_MAX != null)
                    {
                        subRow.Cells[5].AddParagraph(getMinMaxFormat(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_CUS_PROPOSAL_QUANTITY_MIN, pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_CUS_PROPOSAL_QUANTITY_MAX));
                    }
                    subRow.Cells[6].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_CUS_PROPOSAL_PRICE_FLOAT + "");

                    if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_OUR_PROPOSAL_QUANTITY_MIN != null && pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_OUR_PROPOSAL_QUANTITY_MAX != null)
                    {
                        subRow.Cells[7].AddParagraph(getMinMaxFormat(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_OUR_PROPOSAL_QUANTITY_MIN, pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_OUR_PROPOSAL_QUANTITY_MAX));
                    }
                    subRow.Cells[8].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(j).PRD_OUR_PROPOSAL_PRICE_FLOAT + "");

                }
                if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.Count > 0)
                {
                    row.Cells[0].MergeDown = pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.Count - 1;
                    row.Cells[1].MergeDown = pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.Count - 1;
                    row.Cells[2].MergeDown = pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.Count - 1;

                    row.Cells[9].MergeDown = pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.Count - 1;
                    row.Cells[9].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.ElementAt(0).PRD_NOTE + "");
                    row.Cells[9].Format.Alignment = ParagraphAlignment.Left;
                    row.Cells[9].VerticalAlignment = VerticalAlignment.Top;
                }
            }

            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Table1: Applied for \"Customers request to review the existing price\" ");
            ph.Format.Font.Bold = true;
            doc.LastSection.Add(table);
        }

        public static void genTable5(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.4;
            table.Borders.Bottom.Width = 0.2;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(2.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(2));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column6 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column7 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column8 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column9 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column10 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column11 = table.AddColumn(Unit.FromCentimeter(1.6));
            Column column12 = table.AddColumn(Unit.FromCentimeter(2.4));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            row1.Cells[0].MergeDown = 1;
            row1.Cells[1].MergeDown = 1;
            row1.Cells[2].MergeDown = 1;
            row1.Cells[3].MergeDown = 1;
            row1.Cells[4].MergeDown = 1;
            row1.Cells[11].MergeDown = 1;

            row1.Cells[5].MergeRight = 5;

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("Customer");
            row1.Cells[1].AddParagraph("Contact Person");
            row1.Cells[2].AddParagraph("Product");
            row1.Cells[3].AddParagraph("Unit");
            row1.Cells[4].AddParagraph("Quantity");
            row1.Cells[5].AddParagraph("Bid price  or formula (" + pafData.PDA_PRICE_UNIT + ")");
            row1.Cells[11].AddParagraph("Note");

            row2.Cells[5].AddParagraph("Pricing Period");
            row2.Cells[6].AddParagraph("Existing Price");
            row2.Cells[7].AddParagraph("Export Price");
            row2.Cells[8].AddParagraph("Delta Price");
            row2.Cells[9].AddParagraph("Market/Tier Price");
            row2.Cells[10].AddParagraph("Offer Price");


            for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
            {
                Row row = table.AddRow();
                if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST != null)
                {
                    //row.Cells[0].AddParagraph(getNameFromCustNum(pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM, pafData.PDA_FOR_COMPANY));

                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        MT_CUST_DETAIL cd = pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        row.Cells[0].AddParagraph(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2);
                    }

                }
                row.Cells[1].AddParagraph(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_CONTACT_PERSON + "");
                if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_MATERIALS != null)
                {
                    row.Cells[2].AddParagraph(pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                }
                row.Cells[3].AddParagraph(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_QUANTITY_UNIT + "");
                row.Cells[4].AddParagraph(String.Format("{0:n}", pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_QUANTITY_MIN));
                row.Cells[5].AddParagraph(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_PRICING_PERIOD + "");
                row.Cells[6].AddParagraph(String.Format("{0:n}", pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_EXISTING_PRICE));
                row.Cells[7].AddParagraph(String.Format("{0:n}", pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_EXPORT_PRICE));
                row.Cells[8].AddParagraph(String.Format("{0:n}", pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_DELTA_PRICE));
                row.Cells[9].AddParagraph(String.Format("{0:n}", pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_MARKET_PRICE));
                row.Cells[10].AddParagraph(String.Format("{0:n}", pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_OFFER_PRICE));
                row.Cells[11].AddParagraph(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_NOTE + "");
                row.Cells[11].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[11].VerticalAlignment = VerticalAlignment.Top;
            }

            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Table2: Applied for approval ");
            ph.Format.Font.Bold = true;
            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable6(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.2;
            table.Rows.Height = NORMAL_ROW_HEIGHT;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(3.5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(13));

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();

            row1.Cells[2].MergeDown = 3;

            column1.Format.Font.Bold = true;


            row1.Cells[0].AddParagraph("Contract Period :");
            row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[0].AddParagraph("Loading Period :");
            row2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row3.Cells[0].AddParagraph("Pricing Period :");
            row3.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row4.Cells[0].AddParagraph("Discharging Period :");
            row4.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            Paragraph p1 = row1.Cells[2].AddParagraph("Any other special term and condition :");
            p1.Format.Font.Bold = true;

            if (pafData.PDA_CONTRACT_DATE_FROM != null && pafData.PDA_CONTRACT_DATE_TO != null)
            {
                row1.Cells[1].AddParagraph(((DateTime)pafData.PDA_CONTRACT_DATE_FROM).ToString("dd/MMM/yyyy") + " - " + ((DateTime)pafData.PDA_CONTRACT_DATE_TO).ToString("dd/MMM/yyyy"));
                row1.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }
            if (pafData.PDA_LOADING_DATE_FROM != null && pafData.PDA_LOADING_DATE_TO != null)
            {
                row2.Cells[1].AddParagraph(((DateTime)pafData.PDA_LOADING_DATE_FROM).ToString("dd/MMM/yyyy") + " - " + ((DateTime)pafData.PDA_LOADING_DATE_TO).ToString("dd/MMM/yyyy"));
                row2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }
            row3.Cells[1].AddParagraph(pafData.PDA_PRICING_PERIOD + "");
            row3.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            if (pafData.PDA_DISCHARGING_DATE_FROM != null && pafData.PDA_DISCHARGING_DATE_TO != null)
            {
                row4.Cells[1].AddParagraph(((DateTime)pafData.PDA_DISCHARGING_DATE_FROM).ToString("dd/MMM/yyyy") + " - " + ((DateTime)pafData.PDA_DISCHARGING_DATE_TO).ToString("dd/MMM/yyyy"));
                row4.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            row1.Cells[2].AddParagraph(pafData.PDA_SPECIAL_TERMS_OR_CONDITION + "");


            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable7(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.2;
            table.Rows.Height = NORMAL_ROW_HEIGHT;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(16.5));

            Row row1 = table.AddRow();
            row1.Height = 50;
            row1.Cells[0].Format.Font.Bold = true;

            for (int i = 0; i < pafData.PAF_PAYMENT_ITEMS.Count; i++)
            {
                if (pafData.PAF_PAYMENT_ITEMS.ElementAt(i).MT_CUST != null)
                {
                    //Paragraph p1 = row1.Cells[1].AddParagraph(getNameFromCustNum(pafData.PAF_PAYMENT_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM, pafData.PDA_FOR_COMPANY));
                    //p1.Format.Font.Bold = true;
                    if (pafData.PAF_PAYMENT_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        MT_CUST_DETAIL cd = pafData.PAF_PAYMENT_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        Paragraph p1 = row1.Cells[1].AddParagraph(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2);
                        p1.Format.Font.Bold = true;
                    }
                }
                for (int j = 0; j < pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL.Count; j++)
                {
                    Paragraph p2 = row1.Cells[1].AddParagraph();
                    p2.AddText(" - " + pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_TYPE + "   ");
                    p2.AddFormattedText(pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_DETAIL + "   ", TextFormat.Underline);
                    if (pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL != null)
                    {
                        p2.AddText("days " + pafData.PAF_PAYMENT_ITEMS.ElementAt(i).PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL.PMC_NAME);
                    }
                }
                row1.Cells[1].AddParagraph();
            }

            Paragraph ph = row1.Cells[0].AddParagraph("Payment Terms");

            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            //table.Borders.Document.DefaultPageSetup.TopMargin = -10;
            doc.LastSection.Add(table);
        }

        public static void genTable8(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.2;

            Column column1 = table.AddColumn(Unit.FromCentimeter(20));


            Row row1 = table.AddRow();
            row1.Height = 40;

            Paragraph p1 = row1.Cells[0].AddParagraph("Proposal");
            p1.Format.Font.Bold = true;

            if (pafData.PAF_PROPOSAL_ITEMS.Count != 0)
            {
                if (pafData.PAF_PROPOSAL_ITEMS.FirstOrDefault().PSI_DETAIL != null)
                {
                    Paragraph p2 = row1.Cells[0].AddParagraph();
                    HtmlRemoval htmlRemove = new HtmlRemoval();
                    htmlRemove.ValuetoParagraph(pafData.PAF_PROPOSAL_ITEMS.FirstOrDefault().PSI_DETAIL, p2);
                }
                else
                {
                    row1.Cells[0].AddParagraph("");
                }

            }

            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable9(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.2;

            Column column1 = table.AddColumn(Unit.FromCentimeter(20));
            Row row1 = table.AddRow();
            row1.Height = 40;

            Paragraph p1 = row1.Cells[0].AddParagraph("Notes");
            p1.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph(pafData.PDA_NOTE + "");

            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable10(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.4;
            table.Format.Font.Bold = true;
            table.Format.Alignment = ParagraphAlignment.Center;


            Column column1 = table.AddColumn(Unit.FromCentimeter(1));
            Column column2 = table.AddColumn(Unit.FromCentimeter(4));
            Column column3 = table.AddColumn(Unit.FromCentimeter(1));
            Column column4 = table.AddColumn(Unit.FromCentimeter(4));
            Column column5 = table.AddColumn(Unit.FromCentimeter(5));
            Column column6 = table.AddColumn(Unit.FromCentimeter(5));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Height = 10;
            row2.Height = 25;
            row3.Height = 10;

            row1.Borders.Bottom.Visible = false;
            row2.Borders.Bottom.Visible = false;

            row2.Borders.Top.Visible = false;
            row3.Borders.Top.Visible = false;
            var listHistory = getHistoryByTxns(pafData.PDA_ROW_ID);

            row1.Cells[0].MergeRight = 1;
            row1.Cells[0].AddParagraph("");
            row1.Cells[0].AddParagraph("Requested By");
            row1.Cells[2].MergeRight = 1;
            row1.Cells[2].AddParagraph("");
            row1.Cells[2].AddParagraph("Verified By");
            row1.Cells[4].AddParagraph("");
            row1.Cells[4].AddParagraph("Endorsed By");
            row1.Cells[5].AddParagraph("");
            row1.Cells[5].AddParagraph("Approved By");

            row2.Cells[0].Borders.Right.Visible = false;
            row2.Cells[2].Borders.Right.Visible = false;
            row2.Cells[1].Borders.Left.Visible = false;
            row2.Cells[3].Borders.Left.Visible = false;
            row2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[5].VerticalAlignment = VerticalAlignment.Center;

            row3.Cells[0].MergeRight = 1;
            row3.Cells[2].MergeRight = 1;

            Paragraph p3 = row2.Cells[4].AddParagraph("");
            Paragraph p4 = row2.Cells[5].AddParagraph("");

            var hSubmit = listHistory.Where(h => h.DTH_ACTION == "SUBMIT").ToList();
            var hVerify = listHistory.Where(h => h.DTH_ACTION == "VERIFY").ToList();
            var hEndorse = listHistory.Where(h => h.DTH_ACTION == "ENDORSE").ToList();
            var hApprove = listHistory.Where(h => h.DTH_ACTION == "APPROVE").ToList();

            String section = "";
            if (pafData.PDA_TEMPLATE.Contains("CMPS"))
            {
                section = "CMPS";
            }
            else if (pafData.PDA_TEMPLATE.Contains("CMLA"))
            {
                section = "CMLA";
            }
            if (hSubmit.Count > 0)
            {
                Paragraph p1 = row2.Cells[0].AddParagraph(" Trader:");
                Paragraph p2 = row2.Cells[1].AddParagraph("");
                p2.Format.LeftIndent = -30;
                var img = p2.AddImage(getSignatureImg(hSubmit.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.0cm";
                img.LockAspectRatio = true;
                row3.Cells[0].AddParagraph("( " + section + " - " + getNameOfUser(hSubmit.FirstOrDefault().DTH_ACTION_BY) + " )");
            }
            else
            {
                if (hVerify.Count > 0)
                {
                    Paragraph p1 = row2.Cells[0].AddParagraph(" Trader:");
                    Paragraph p2 = row2.Cells[1].AddParagraph("");
                    p2.Format.LeftIndent = -30;
                    var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
                    img.Width = "2.0cm";
                    img.LockAspectRatio = true;
                    row3.Cells[0].AddParagraph("( " + section + " - " + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
                }
                else
                {
                    Paragraph p1 = row2.Cells[0].AddParagraph(" Trader:");
                    row3.Cells[0].AddParagraph("( " + section + " )");
                }

            }

            if (hVerify.Count > 0)
            {
                Paragraph p1 = row2.Cells[2].AddParagraph(" S/H:");
                Paragraph p2 = row2.Cells[3].AddParagraph("");
                p2.Format.LeftIndent = -30;
                var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.0cm";
                img.LockAspectRatio = true;
                row3.Cells[2].AddParagraph("( " + section + " - " + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
            }
            else
            {
                if (hEndorse.Count > 0)
                {
                    Paragraph p1 = row2.Cells[2].AddParagraph(" S/H:");
                    Paragraph p2 = row2.Cells[3].AddParagraph("");
                    p2.Format.LeftIndent = -30;
                    var img = p2.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));
                    img.Width = "2.0cm";
                    img.LockAspectRatio = true;
                    row3.Cells[2].AddParagraph("( " + section + " - " + getNameOfUser(hEndorse.FirstOrDefault().DTH_ACTION_BY) + " )");
                }
                else
                {
                    Paragraph p2 = row2.Cells[2].AddParagraph(" S/H:");
                    row3.Cells[2].AddParagraph("( " + section + " )");
                }

            }

            if (hEndorse.Count > 0)
            {
                var img = p3.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));

                img.Width = "2.0cm";
                img.LockAspectRatio = true;

            }
            else
            {
                if (hApprove.Count > 0)
                {
                    var img = p3.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));

                    img.Width = "2.0cm";
                    img.LockAspectRatio = true;
                }
            }

            if (hApprove.Count > 0)
            {
                var img = p4.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.0cm";
                img.LockAspectRatio = true;
                //img.WrapFormat.DistanceLeft = 20;
            }


            row3.Cells[4].AddParagraph("CMVP");
            row3.Cells[5].AddParagraph("EVPC");

            doc.LastSection.Add(table);
        }

        public static string getSignatureImg(string UserName)
        {
            List<string> extentions = new List<string>(); extentions.Add(".jpg"); extentions.Add(".png"); extentions.Add(".gif");
            ShareFn sFn = new ShareFn();
            string pathSignature = "";
            foreach (string _itemExt in extentions)
            {
                if (File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", UserName) + _itemExt))
                {
                    pathSignature = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", UserName) + _itemExt;
                    break;
                }
            }
            return pathSignature;
        }

        public static String getNameOfUser(string userName)
        {

            using (var context = new EntityCPAIEngine())
            {
                var users = context.USERS.Where(x => x.USR_LOGIN.ToUpper() == userName.ToUpper() && x.USR_STATUS == "ACTIVE").ToList();
                if (users.Count > 0)
                {
                    return users.FirstOrDefault().USR_FIRST_NAME_EN.ToUpper() + "";
                }
                else
                {
                    return "";
                }
            }


        }
        public static List<CPAI_DATA_HISTORY> getHistoryByTxns(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef && x.DTH_REJECT_FLAG == "N").ToList();
                if (results != null)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }

        public static void genTable11(Document doc, PAF_DATA pafData, int beginRound)
        {

            List<String> listP = new List<string>();
            List<String> listR = new List<string>();
            List<String> listC = new List<string>();
            List<String> listU = new List<string>();

            List<String> listPCode = new List<string>();
            List<String> listCCode = new List<string>();
            for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
            {
                String productName = "";
                if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_MATERIALS != null)
                {
                    productName = pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_MATERIALS.MT_MATERIALS_CONTROL.Where(m => m.MMC_SYSTEM == "PAF_CMPS_INTER_SALE").FirstOrDefault().MMC_NAME + "";
                }
                if ((!listP.Contains(productName)) && productName != "")
                {
                    listP.Add(productName);
                    listU.Add(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_QUANTITY_UNIT + "");
                    listPCode.Add(pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_MATERIALS.MT_MATERIALS_CONTROL.Where(m => m.MMC_SYSTEM == "PAF_CMPS_INTER_SALE").FirstOrDefault().MMC_FK_MATRIALS);
                }
                for (int j = 0; j < pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count; j++)
                {
                    if (!listR.Contains(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_COUNT + ""))
                    {
                        if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_COUNT >= beginRound && listR.Count < 3)
                        {
                            listR.Add(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_COUNT + "");
                        }
                    }
                }

                String customerName = "";
                if (pafData.PDA_TEMPLATE == "CMPS_IMPORT")
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_VENDOR != null)
                    {
                        customerName = pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_VENDOR.VND_NAME1 + "";
                    }
                    if ((!listC.Contains(customerName)) && customerName != "")
                    {
                        listC.Add(customerName);
                        listCCode.Add(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_FK_VENDOR + "");
                    }
                }
                else
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST != null)
                    {
                        if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                        {
                            MT_CUST_DETAIL cd = pafData.PAF_BIDDING_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                            customerName = cd.MCD_NAME_1 + " " + cd.MCD_NAME_2;
                        }
                    }
                    if ((!listC.Contains(customerName)) && customerName != "")
                    {
                        listC.Add(customerName);
                        listCCode.Add(pafData.PAF_BIDDING_ITEMS.ElementAt(i).PBI_FK_CUSTOMER + "");
                    }
                }

            }

            int p = 0;
            if (pafData.PDA_TEMPLATE == "CMPS_IMPORT")
            {
                p = 1;
            }
            int rCount = listR.Count;
            int pCount = listP.Count;
            int allColumn = 7 + (pCount * 5) + (pCount * rCount);
            //double ratio = 20.0 / allColumn;
            double ratio = 20.0 / (10.5 + (1 * ((pCount * 3) + (pCount * rCount))) + (1.3 * pCount) + (1.5 * pCount) + (p * 1));

            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.4;
            table.Borders.Bottom.Width = 0.2;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;


            /////////////////////////////// add column ////////////////////////////////////////
            List<Column> listColumn = new List<Column>();
            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.5 * ratio)));
            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.5 * ratio)));

            for (int i = 0; i < pCount; i++)
            {
                listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.3 * ratio)));
            }

            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.5 * ratio)));

            for (int i = 0; i < (pCount * rCount); i++)
            {
                listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            }

            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            if (p == 1)
            {
                listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            }

            for (int i = 0; i < (pCount); i++)
            {
                listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            }

            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.5 * ratio)));

            for (int i = 0; i < (pCount * 2); i++)
            {
                listColumn.Add(table.AddColumn(Unit.FromCentimeter(1 * ratio)));
            }

            for (int i = 0; i < (pCount); i++)
            {
                listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.5 * ratio)));
            }

            listColumn.Add(table.AddColumn(Unit.FromCentimeter(1.5 * ratio)));

            /////////////////////////////// merge ////////////////////////////////////////

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;

            row1.Cells[0].MergeDown = 2;
            if (p == 1)
            {
                row1.Cells[0].AddParagraph("Supplier");
            }
            else
            {
                row1.Cells[0].AddParagraph("Customer");
            }

            row1.Cells[1].MergeDown = 2;
            row1.Cells[1].AddParagraph("Contact Person");

            row1.Cells[2].MergeDown = 1;
            row1.Cells[2].MergeRight = pCount - 1;
            row1.Cells[2].AddParagraph("Quantity");
            for (int i = 0; i < pCount; i++)
            {
                row3.Cells[2 + i].AddParagraph(listP.ElementAt(i) + " (" + listU.ElementAt(i) + ")");
            }

            row1.Cells[2 + pCount].MergeDown = 2;
            row1.Cells[2 + pCount].AddParagraph("Pricing period");
            row1.Cells[3 + pCount].MergeDown = 2;
            row1.Cells[3 + pCount].AddParagraph("Inco terms");
            row1.Cells[4 + pCount].MergeDown = 2;
            row1.Cells[4 + pCount].AddParagraph("Country");

            row1.Cells[5 + pCount].MergeRight = table.Columns.Count - (7 + pCount);
            row1.Cells[5 + pCount].AddParagraph("Bid price  or formula (" + pafData.PDA_PRICE_UNIT + ")");
            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[5 + (pCount * (i + 1))].MergeRight = pCount - 1;
                row2.Cells[5 + (pCount * (i + 1))].AddParagraph(getRoundName(listR.ElementAt(i)));
                for (int j = 0; j < pCount; j++)
                {
                    row3.Cells[(5 + (pCount * (i + 1))) + j].AddParagraph(listP.ElementAt(j));
                }
            }

            row2.Cells[5 + pCount + (pCount * rCount)].MergeDown = 1;
            row2.Cells[5 + pCount + (pCount * rCount)].AddParagraph("Freight");
            if (p == 1)
            {
                row2.Cells[5 + pCount + (pCount * rCount) + p].MergeDown = 1;
                row2.Cells[5 + pCount + (pCount * rCount) + p].AddParagraph("Port");
            }
            row2.Cells[6 + pCount + (pCount * rCount) + p].MergeRight = pCount - 1;
            if (p == 0)
            {
                row2.Cells[6 + pCount + (pCount * rCount) + p].AddParagraph("Converted to FOB");
            }
            else
            {
                row2.Cells[6 + pCount + (pCount * rCount) + p].AddParagraph("Converted to CFR");
            }

            for (int i = 0; i < pCount; i++)
            {
                row3.Cells[(6 + pCount + (pCount * rCount)) + i + p].AddParagraph(listP.ElementAt(i));
            }

            row2.Cells[(6 + pCount + (pCount * rCount)) + pCount + p].MergeDown = 1;
            row2.Cells[(6 + pCount + (pCount * rCount)) + pCount + p].AddParagraph("Average");

            row2.Cells[(7 + pCount + (pCount * rCount)) + pCount + p].MergeRight = pCount - 1;
            row2.Cells[(7 + pCount + (pCount * rCount)) + pCount + p].AddParagraph("Market Price");
            for (int i = 0; i < pCount; i++)
            {
                row3.Cells[((7 + pCount + (pCount * rCount)) + pCount) + i + p].AddParagraph(listP.ElementAt(i));
            }
            row2.Cells[(7 + pCount + (pCount * rCount)) + (pCount * 2) + p].MergeRight = pCount - 1;
            row2.Cells[(7 + pCount + (pCount * rCount)) + (pCount * 2) + p].AddParagraph("Latest LP plan price");
            for (int i = 0; i < pCount; i++)
            {
                row3.Cells[((7 + pCount + (pCount * rCount)) + (pCount * 2)) + i + p].AddParagraph(listP.ElementAt(i));
            }
            row2.Cells[(7 + pCount + (pCount * rCount)) + (pCount * 3) + p].MergeRight = pCount - 1;
            row2.Cells[(7 + pCount + (pCount * rCount)) + (pCount * 3) + p].AddParagraph("Benchmark ");
            for (int i = 0; i < pCount; i++)
            {
                row3.Cells[((7 + pCount + (pCount * rCount)) + (pCount * 3)) + i + p].AddParagraph(listP.ElementAt(i));
            }

            row1.Cells[table.Columns.Count - 1].MergeDown = 2;
            row1.Cells[table.Columns.Count - 1].AddParagraph("Note");

            //////////////////////////// insert data to table //////////////////////////////

            for (int i = 0; i < listCCode.Count; i++)
            {
                List<PAF_BIDDING_ITEMS> listBidItem;
                if (p == 0)
                {
                    listBidItem = pafData.PAF_BIDDING_ITEMS.Where(c => c.MT_CUST.MCT_CUST_NUM == listCCode.ElementAt(i)).ToList();
                }
                else
                {
                    listBidItem = pafData.PAF_BIDDING_ITEMS.Where(c => c.PBI_FK_VENDOR == listCCode.ElementAt(i)).ToList();
                }
                Row row = table.AddRow();

                row.Cells[0].AddParagraph(listC.ElementAt(i));


                row.Cells[1].AddParagraph(listBidItem.FirstOrDefault().PBI_CONTACT_PERSON + "");
                decimal sumQMax = 0;
                decimal cqMin = 0;
                decimal cqMax = 0;
                bool haveSomeValue = false;
                for (int j = 0; j < listPCode.Count; j++)
                {
                    PAF_BIDDING_ITEMS bid;
                    if (p == 0)
                    {
                        bid = pafData.PAF_BIDDING_ITEMS.Where(b => b.MT_MATERIALS.MET_NUM == listPCode.ElementAt(j) && b.PBI_FK_CUSTOMER == listCCode.ElementAt(i)).FirstOrDefault();

                    }
                    else
                    {
                        bid = pafData.PAF_BIDDING_ITEMS.Where(b => b.MT_MATERIALS.MET_NUM == listPCode.ElementAt(j) && b.PBI_FK_VENDOR == listCCode.ElementAt(i)).FirstOrDefault();
                    }
                    row.Cells[2 + j].AddParagraph(getMinMaxFormat(bid.PBI_QUANTITY_MIN, bid.PBI_QUANTITY_MAX));




                    int lastRoundHaveValue = 1;

                    for (int k = 0; k < listR.Count; k++)
                    {

                        var round = bid.PAF_ROUND.Where(r => r.PRN_COUNT == int.Parse(listR.ElementAt(k))).FirstOrDefault();
                        //var round = bid.PAF_ROUND.ElementAt(k);
                        row.Cells[(5 + (pCount * (k + 1))) + j].AddParagraph(String.Format("{0:n}", round.PRN_FIXED));
                        if (round.PRN_FIXED != null)
                        {
                            if (round.PRN_COUNT != null)
                            {
                                lastRoundHaveValue = (int)round.PRN_COUNT;
                                haveSomeValue = true;
                            }
                        }
                    }
                    var fixedPrice = bid.PAF_ROUND.Where(r => r.PRN_COUNT == lastRoundHaveValue).FirstOrDefault().PRN_FIXED;
                    if (fixedPrice != null)
                    {
                        sumQMax = (sumQMax + bid.PBI_QUANTITY_MAX ?? 0);
                    }
                    if (pafData.PDA_TEMPLATE == "CMPS_INTER_SALE")
                    {
                        if (bid.MT_INCOTERMS != null)
                        {
                            if (bid.MT_INCOTERMS.PAF_MT_INCOTERMS != null)
                            {
                                if (bid.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "N")
                                {
                                    row.Cells[(6 + pCount + (pCount * rCount)) + j + p].AddParagraph(String.Format("{0:n}", fixedPrice));
                                    cqMin = cqMin + ((bid.PBI_QUANTITY_MIN ?? 0) * (fixedPrice ?? 0));
                                    cqMax = cqMax + ((bid.PBI_QUANTITY_MAX ?? 0) * (fixedPrice ?? 0));
                                }
                                else
                                {
                                    if (bid.PBI_FREIGHT_PRICE == null)
                                    {
                                        row.Cells[(6 + pCount + (pCount * rCount)) + j + p].AddParagraph(String.Format("{0:n}", fixedPrice - 0));
                                        cqMin = cqMin + ((bid.PBI_QUANTITY_MIN ?? 0) * (fixedPrice ?? 0));
                                        cqMax = cqMax + ((bid.PBI_QUANTITY_MAX ?? 0) * (fixedPrice ?? 0));
                                    }
                                    else
                                    {
                                        row.Cells[(6 + pCount + (pCount * rCount)) + j + p].AddParagraph(String.Format("{0:n}", fixedPrice - bid.PBI_FREIGHT_PRICE));
                                        if (fixedPrice != null)
                                        {
                                            cqMin = cqMin + ((bid.PBI_QUANTITY_MIN ?? 0) * ((fixedPrice ?? 0) - (bid.PBI_FREIGHT_PRICE ?? 0)));
                                            cqMax = cqMax + ((bid.PBI_QUANTITY_MAX ?? 0) * ((fixedPrice ?? 0) - (bid.PBI_FREIGHT_PRICE ?? 0)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (bid.MT_INCOTERMS != null)
                        {
                            if (bid.MT_INCOTERMS.PAF_MT_INCOTERMS != null)
                            {
                                if (bid.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y")
                                {
                                    row.Cells[(6 + pCount + (pCount * rCount)) + j + p].AddParagraph(String.Format("{0:n}", fixedPrice));
                                    cqMin = cqMin + ((bid.PBI_QUANTITY_MIN ?? 0) * (fixedPrice ?? 0));
                                    cqMax = cqMax + ((bid.PBI_QUANTITY_MAX ?? 0) * (fixedPrice ?? 0));
                                }
                                else
                                {
                                    if (bid.PBI_FREIGHT_PRICE == null)
                                    {
                                        row.Cells[(6 + pCount + (pCount * rCount)) + j + p].AddParagraph(String.Format("{0:n}", fixedPrice + 0));
                                        cqMin = cqMin + ((bid.PBI_QUANTITY_MIN ?? 0) * (fixedPrice ?? 0));
                                        cqMax = cqMax + ((bid.PBI_QUANTITY_MAX ?? 0) * (fixedPrice ?? 0));
                                    }
                                    else
                                    {
                                        row.Cells[(6 + pCount + (pCount * rCount)) + j + p].AddParagraph(String.Format("{0:n}", fixedPrice + bid.PBI_FREIGHT_PRICE));
                                        if (fixedPrice != null)
                                        {
                                            cqMin = cqMin + ((bid.PBI_QUANTITY_MIN ?? 0) * ((fixedPrice ?? 0) + (bid.PBI_FREIGHT_PRICE ?? 0)));
                                            cqMax = cqMax + ((bid.PBI_QUANTITY_MAX ?? 0) * ((fixedPrice ?? 0) + (bid.PBI_FREIGHT_PRICE ?? 0)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    row.Cells[((7 + pCount + (pCount * rCount)) + pCount) + j + p].AddParagraph(String.Format("{0:n}", bid.PBI_MARKET_PRICE));
                    row.Cells[((7 + pCount + (pCount * rCount)) + (pCount * 2)) + j + p].AddParagraph(String.Format("{0:n}", bid.PBI_LATEST_LP_PLAN_PRICE_FIXED));
                    if (bid.MKT_MST_MOPS_PRODUCT != null)
                    {

                        row.Cells[((7 + pCount + (pCount * rCount)) + (pCount * 3)) + j + p].AddParagraph(bid.MKT_MST_MOPS_PRODUCT.M_MPR_PRDNAME);
                    }
                }
                if (sumQMax == 0)
                {
                    sumQMax = 0.00000001M;
                }

                if (haveSomeValue == true)
                {
                    row.Cells[(6 + pCount + (pCount * rCount)) + pCount + p].AddParagraph(String.Format("{0:n}", cqMax / sumQMax));
                }
                row.Cells[2 + pCount].AddParagraph(listBidItem.FirstOrDefault().PBI_PRICING_PERIOD + "");
                row.Cells[3 + pCount].AddParagraph(listBidItem.FirstOrDefault().PBI_INCOTERM + "");
                if (listBidItem.FirstOrDefault().MT_COUNTRY != null)
                {
                    row.Cells[4 + pCount].AddParagraph(listBidItem.FirstOrDefault().MT_COUNTRY.MCT_LANDX + "");
                }
                row.Cells[5 + pCount + (pCount * rCount)].AddParagraph(String.Format("{0:n}", listBidItem.ElementAt(0).PBI_FREIGHT_PRICE));
                if (p == 1)
                {
                    row.Cells[5 + pCount + (pCount * rCount) + p].AddParagraph(listBidItem.ElementAt(0).PBI_PORT + "");
                }
                row.Cells[table.Columns.Count - 1].AddParagraph(listBidItem.ElementAt(0).PBI_NOTE + "");
            }


            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Applied for \"Product price bidding\"");
            ph.Format.Font.Bold = true;
            table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);

        }



        public static void genTable12(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0;
            table.Format.Font.Bold = true;

            List<Column> listColumn = new List<Column>();
            Column column1 = table.AddColumn(Unit.FromCentimeter(1));
            Column column2 = table.AddColumn(Unit.FromCentimeter(2.7));
            Column column3 = table.AddColumn(Unit.FromCentimeter(5));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1.2));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1));
            Column column6 = table.AddColumn(Unit.FromCentimeter(1));
            Column column7 = table.AddColumn(Unit.FromCentimeter(2.3));
            Column column8 = table.AddColumn(Unit.FromCentimeter(2.9));
            Column column9 = table.AddColumn(Unit.FromCentimeter(2.9));

            column1.Borders.Left.Width = 0.4;
            column9.Borders.Right.Width = 0.4;

            column2.Format.Alignment = ParagraphAlignment.Right;
            column4.Format.Alignment = ParagraphAlignment.Center;
            column5.Format.Alignment = ParagraphAlignment.Center;
            column6.Format.Alignment = ParagraphAlignment.Center;
            column7.Format.Alignment = ParagraphAlignment.Center;


            for (int i = 0; i < pafData.PAF_PROPOSAL_ITEMS.Count; i++)
            {
                int mergeCount = 0;
                Row row = table.AddRow();

                if (i == 0)
                {
                    Paragraph p1 = row.Cells[0].AddParagraph("Proposal");
                }
                var proposal = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i);

                Row row1 = table.AddRow();
                row1.Cells[1].AddParagraph("Award to:");



                if (proposal.MT_CUST != null)
                {
                    mergeCount++;
                    if (proposal.MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        Paragraph pa = row1.Cells[2].AddParagraph();
                        MT_CUST_DETAIL cd = proposal.MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        pa.AddFormattedText(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2, TextFormat.NotBold);
                    }
                }
                else if (proposal.MT_VENDOR != null)
                {
                    mergeCount++;

                    Paragraph pa = row1.Cells[2].AddParagraph();
                    pa.AddFormattedText(proposal.MT_VENDOR.VND_NAME1 + "", TextFormat.NotBold);

                }


                if (proposal.PAF_PROPOSAL_REASON_ITEMS.Count > 0)
                {
                    mergeCount++;
                    Row row2 = table.AddRow();
                    for (int j = 0; j < proposal.PAF_PROPOSAL_REASON_ITEMS.Count; j++)
                    {

                        if (j == 0)
                        {
                            Paragraph p = row2.Cells[1].AddParagraph("Reason:");
                        }

                        Paragraph pd = row2.Cells[2].AddParagraph();
                        pd.AddFormattedText("- " + proposal.PAF_PROPOSAL_REASON_ITEMS.ElementAt(j).PSR_REASON + "", TextFormat.NotBold);
                        pd.AddText(" ");
                        pd.AddFormattedText(proposal.PAF_PROPOSAL_REASON_ITEMS.ElementAt(j).PSR_REASON_DETAIL + "", TextFormat.NotBold | TextFormat.Underline);

                    }
                }

                if (proposal.PAF_PROPOSAL_OTC_ITEMS.Count > 0)
                {
                    mergeCount++;
                    Row row3 = table.AddRow();
                    for (int j = 0; j < proposal.PAF_PROPOSAL_OTC_ITEMS.Count; j++)
                    {
                        if (j == 0)
                        {
                            Paragraph p = row3.Cells[1].AddParagraph();
                            p.AddText("Other terms & Conditions:");
                        }

                        Paragraph pd = row3.Cells[2].AddParagraph();
                        pd.AddFormattedText("- " + proposal.PAF_PROPOSAL_OTC_ITEMS.ElementAt(j).PSO_OTHER_TERM_COND + "", TextFormat.NotBold);
                        pd.AddText(" ");
                        pd.AddFormattedText(proposal.PAF_PROPOSAL_OTC_ITEMS.ElementAt(j).PSO_OTHER_TERM_COND_DETAIL + "", TextFormat.NotBold | TextFormat.Underline);

                    }
                }



                if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_CUSTOMER == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_CUSTOMER && p.PPI_FK_CUSTOMER != null).FirstOrDefault() != null)
                {
                    mergeCount++;
                    Row row4 = table.AddRow();
                    string x = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM;
                    int count = pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (j == 0)
                        {
                            Paragraph p = row4.Cells[1].AddParagraph();
                            p.AddText("Payment Terms :");
                        }
                        Paragraph pd = row4.Cells[2].AddParagraph();

                        pd.AddFormattedText("- " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_TYPE + "   ", TextFormat.NotBold);
                        pd.AddFormattedText(pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_DETAIL + "   ", TextFormat.NotBold | TextFormat.Underline);
                        if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL != null)
                        {
                            pd.AddFormattedText("days " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL.PMC_NAME, TextFormat.NotBold);
                        }

                    }
                }
                else if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).FirstOrDefault() != null)
                {
                    mergeCount++;
                    Row row4 = table.AddRow();
                    string x = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR;
                    int count = pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (j == 0)
                        {
                            Paragraph p = row4.Cells[1].AddParagraph();
                            p.AddText("Payment Terms :");
                        }
                        Paragraph pd = row4.Cells[2].AddParagraph();

                        pd.AddFormattedText("- " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_TYPE + "   ", TextFormat.NotBold);
                        pd.AddFormattedText(pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_DETAIL + "   ", TextFormat.NotBold | TextFormat.Underline);
                        if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL != null)
                        {
                            pd.AddFormattedText("days " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL.PMC_NAME, TextFormat.NotBold);
                        }

                    }
                }
                Row rowAdd = table.AddRow();

                row.Cells[3].MergeDown = mergeCount;
                row.Cells[4].MergeDown = mergeCount;
                row.Cells[5].MergeDown = mergeCount;
                row.Cells[6].MergeDown = mergeCount;
                row.Cells[7].MergeDown = mergeCount;
                row.Cells[8].MergeDown = mergeCount;

                column5.Format.Font.Bold = false;
                column6.Format.Font.Bold = false;
                column7.Format.Font.Bold = false;
                column8.Format.Font.Bold = false;
                column9.Format.Font.Bold = false;

                int minus = 1;
                var biddingItemForProposal = pafData.PAF_BIDDING_ITEMS.Where(b => b.PBI_FK_CUSTOMER == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_CUSTOMER).ToList();
                if (pafData.PDA_TEMPLATE == "CMPS_IMPORT")
                {
                    minus = -1;
                    biddingItemForProposal = pafData.PAF_BIDDING_ITEMS.Where(b => b.PBI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).ToList();
                }


                //column3.Format.Alignment = ParagraphAlignment.Right;

                row.Cells[3].AddParagraph("");
                row.Cells[3].AddParagraph("");
                row.Cells[3].AddParagraph("");
                row.Cells[4].AddParagraph("");
                row.Cells[4].AddParagraph("Quantity").Format.Font.Bold = true;
                row.Cells[4].AddParagraph("(" + biddingItemForProposal.ElementAt(0).PBI_QUANTITY_UNIT + ")").Format.Font.Bold = true;
                row.Cells[5].AddParagraph("");
                row.Cells[5].AddParagraph("Tolerance").Format.Font.Bold = true;
                row.Cells[5].AddParagraph("");
                row.Cells[6].AddParagraph("");
                row.Cells[6].AddParagraph("Option").Format.Font.Bold = true;
                row.Cells[6].AddParagraph("");
                row.Cells[7].AddParagraph("");
                row.Cells[7].AddParagraph("Better than market").Format.Font.Bold = true;
                row.Cells[7].AddParagraph("(" + pafData.PDA_PRICE_UNIT + ")").Format.Font.Bold = true;
                row.Cells[8].AddParagraph("");
                row.Cells[8].AddParagraph("Better than LP Plan Price").Format.Font.Bold = true;
                row.Cells[8].AddParagraph("(" + pafData.PDA_PRICE_UNIT + ")").Format.Font.Bold = true;

                decimal freight = 0;
                if (pafData.PDA_TEMPLATE == "CMPS_INTER_SALE")
                {
                    if (biddingItemForProposal.ElementAt(0).MT_INCOTERMS != null)
                    {
                        if (biddingItemForProposal.ElementAt(0).MT_INCOTERMS.PAF_MT_INCOTERMS != null)
                        {
                            if (biddingItemForProposal.ElementAt(0).MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "N")
                            {
                                freight = 0;
                            }
                            else
                            {
                                freight = (biddingItemForProposal.ElementAt(0).PBI_FREIGHT_PRICE ?? 0);
                            }
                        }
                    }
                }
                else
                {
                    if (biddingItemForProposal.ElementAt(0).MT_INCOTERMS != null)
                    {
                        if (biddingItemForProposal.ElementAt(0).MT_INCOTERMS.PAF_MT_INCOTERMS != null)
                        {
                            if (biddingItemForProposal.ElementAt(0).MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y")
                            {
                                freight = 0;
                            }
                            else
                            {
                                freight = -(biddingItemForProposal.ElementAt(0).PBI_FREIGHT_PRICE ?? 0);
                            }
                        }
                    }
                }

                Decimal bm1 = 0;
                Decimal bl1 = 0;
                column4.Format.Alignment = ParagraphAlignment.Center;

                row.Cells[3].AddParagraph(biddingItemForProposal.ElementAt(0).MT_MATERIALS.MT_MATERIALS_CONTROL.Where(m => m.MMC_SYSTEM == "PAF_CMPS_INTER_SALE").FirstOrDefault().MMC_NAME + "");
                String q = getMinMaxFormat(biddingItemForProposal.ElementAt(0).PBI_AWARDED_QUANTITY_MIN, biddingItemForProposal.ElementAt(0).PBI_AWARDED_QUANTITY_MAX);

                row.Cells[4].AddParagraph(q);
                if (biddingItemForProposal.ElementAt(0).PBI_QUANTITY_TOLERANCE != null)
                {
                    row.Cells[5].AddParagraph(biddingItemForProposal.ElementAt(0).PBI_QUANTITY_TOLERANCE + "%");
                }
                else
                {
                    row.Cells[5].AddParagraph("");
                }
                row.Cells[6].AddParagraph(biddingItemForProposal.ElementAt(0).PBI_QUANTITY_TOLERANCE_OPTION + "");

                /////
                if (biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(0).PBI_MARKET_PRICE != null)
                {
                    bm1 = (decimal)(biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(0).PBI_MARKET_PRICE);
                    row.Cells[7].AddParagraph(String.Format("{0:n}", bm1 * minus));
                }
                if (biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(0).PBI_LATEST_LP_PLAN_PRICE_FIXED != null)
                {
                    bl1 = (decimal)(biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(0).PBI_LATEST_LP_PLAN_PRICE_FIXED);
                    row.Cells[8].AddParagraph(String.Format("{0:n}", bl1 * minus));
                }
                /////
                if (pafData.PDA_PRICE_UNIT == "USD/BBL")
                {
                    bm1 = (decimal)(biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(0).PBI_MARKET_PRICE);
                    //row.Cells[7].AddParagraph(String.Format("{0:n}", bm1 * minus));
                }
                else if (pafData.PDA_PRICE_UNIT == "USD/TON")
                {
                    if (biddingItemForProposal.ElementAt(0).MT_MATERIALS.MET_PRODUCT_DENSITY != null && biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(0).PBI_MARKET_PRICE != null)
                    {
                        bm1 = (decimal)(((Decimal)(biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(0).PBI_MARKET_PRICE)) / ((Decimal)6.29 / Decimal.Parse(biddingItemForProposal.ElementAt(0).MT_MATERIALS.MET_PRODUCT_DENSITY)));
                        //row.Cells[7].AddParagraph(String.Format("{0:n}", bm1 * minus));
                    }
                }
                if (pafData.PDA_PRICE_UNIT == "USD/BBL")
                {
                    bl1 = (decimal)(biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(0).PBI_LATEST_LP_PLAN_PRICE_FIXED);
                    //row.Cells[8].AddParagraph(String.Format("{0:n}", bl1 * minus));
                }
                else if (pafData.PDA_PRICE_UNIT == "USD/TON")
                {
                    if (biddingItemForProposal.ElementAt(0).MT_MATERIALS.MET_PRODUCT_DENSITY != null && biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(0).PBI_LATEST_LP_PLAN_PRICE_FIXED != null)
                    {
                        bl1 = (decimal)(((Decimal)(biddingItemForProposal.ElementAt(0).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(0).PBI_LATEST_LP_PLAN_PRICE_FIXED)) / ((Decimal)6.29 / Decimal.Parse(biddingItemForProposal.ElementAt(0).MT_MATERIALS.MET_PRODUCT_DENSITY)));
                        //row.Cells[8].AddParagraph(String.Format("{0:n}", bl1 * minus));
                    }
                }

                Decimal bm2 = 0;
                Decimal bl2 = 0;
                if (biddingItemForProposal.Count == 2)
                {
                    column5.Format.Alignment = ParagraphAlignment.Center;

                    row.Cells[3].AddParagraph(biddingItemForProposal.ElementAt(1).MT_MATERIALS.MT_MATERIALS_CONTROL.Where(m => m.MMC_SYSTEM == "PAF_CMPS_INTER_SALE").FirstOrDefault().MMC_NAME + "");
                    String q2 = getMinMaxFormat(biddingItemForProposal.ElementAt(1).PBI_AWARDED_QUANTITY_MIN, biddingItemForProposal.ElementAt(1).PBI_AWARDED_QUANTITY_MAX);

                    row.Cells[4].AddParagraph(q2);
                    if (biddingItemForProposal.ElementAt(1).PBI_QUANTITY_TOLERANCE != null)
                    {
                        row.Cells[5].AddParagraph(biddingItemForProposal.ElementAt(1).PBI_QUANTITY_TOLERANCE + "%");
                    }
                    else
                    {
                        row.Cells[5].AddParagraph("");
                    }
                    row.Cells[6].AddParagraph(biddingItemForProposal.ElementAt(1).PBI_QUANTITY_TOLERANCE_OPTION + "");
                    /////
                    if (biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(1).PBI_MARKET_PRICE != null)
                    {
                        bm2 = (decimal)(biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(1).PBI_MARKET_PRICE);
                        row.Cells[7].AddParagraph(String.Format("{0:n}", bm2 * minus));
                    }
                    if (biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(1).PBI_LATEST_LP_PLAN_PRICE_FIXED != null)
                    {
                        bl2 = (decimal)(biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(1).PBI_LATEST_LP_PLAN_PRICE_FIXED);
                        row.Cells[8].AddParagraph(String.Format("{0:n}", bl2 * minus));
                    }
                    /////
                    if (pafData.PDA_PRICE_UNIT == "USD/BBL")
                    {
                        bm2 = (decimal)(biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(1).PBI_MARKET_PRICE);
                        //row.Cells[7].AddParagraph(String.Format("{0:n}", bm2 * minus));
                    }
                    else if (pafData.PDA_PRICE_UNIT == "USD/TON")
                    {
                        if (biddingItemForProposal.ElementAt(1).MT_MATERIALS.MET_PRODUCT_DENSITY != null && biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(1).PBI_MARKET_PRICE != null)
                        {
                            bm2 = (decimal)(((Decimal)(biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(1).PBI_MARKET_PRICE)) / ((Decimal)6.29 / Decimal.Parse(biddingItemForProposal.ElementAt(1).MT_MATERIALS.MET_PRODUCT_DENSITY)));
                            //row.Cells[7].AddParagraph(String.Format("{0:n}", bm2 * minus));
                        }
                    }
                    if (pafData.PDA_PRICE_UNIT == "USD/BBL")
                    {
                        bl2 = (decimal)(biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(1).PBI_LATEST_LP_PLAN_PRICE_FIXED);
                        //row.Cells[8].AddParagraph(String.Format("{0:n}", bl2 * minus));
                    }
                    else if (pafData.PDA_PRICE_UNIT == "USD/TON")
                    {
                        if (biddingItemForProposal.ElementAt(1).MT_MATERIALS.MET_PRODUCT_DENSITY != null && biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(1).PBI_LATEST_LP_PLAN_PRICE_FIXED != null)
                        {
                            bl2 = (decimal)(((Decimal)(biddingItemForProposal.ElementAt(1).PAF_ROUND.LastOrDefault().PRN_FIXED - freight - biddingItemForProposal.ElementAt(1).PBI_LATEST_LP_PLAN_PRICE_FIXED)) / ((Decimal)6.29 / Decimal.Parse(biddingItemForProposal.ElementAt(1).MT_MATERIALS.MET_PRODUCT_DENSITY)));
                            //row.Cells[8].AddParagraph(String.Format("{0:n}", bl2 * minus));
                        }
                    }
                }

                column6.Format.Alignment = ParagraphAlignment.Center;
                column7.Format.Alignment = ParagraphAlignment.Center;
                column8.Format.Alignment = ParagraphAlignment.Center;
                column9.Format.Alignment = ParagraphAlignment.Center;


                row.Cells[6].AddParagraph("Better Value (USD)").Format.Font.Bold = true;
                Decimal betterMarket1 = 0;
                Decimal betterMarket2 = 0;
                Decimal betterLP1 = 0;
                Decimal betterLP2 = 0;
                bool isEqual = true;

                for (int j = 0; j < biddingItemForProposal.Count; j++)
                {

                    Decimal bm = 0;
                    Decimal bl = 0;
                    if (j == 0)
                    {
                        bm = bm1;
                        bl = bl1;
                    }
                    else if (j == 1)
                    {
                        bm = bm2;
                        bl = bl2;
                    }
                    if (biddingItemForProposal.FirstOrDefault().PBI_QUANTITY_UNIT == "KT")
                    {
                        betterMarket1 = betterMarket1 + (Decimal)(bm * (((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MIN ?? 0) * (Decimal)1000 * (Decimal)6.29) / Decimal.Parse(biddingItemForProposal.ElementAt(j).MT_MATERIALS.MET_PRODUCT_DENSITY ?? "0.0000000001")));
                        betterMarket2 = betterMarket2 + (Decimal)(bm * (((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MAX ?? 0) * (Decimal)1000 * (Decimal)6.29) / Decimal.Parse(biddingItemForProposal.ElementAt(j).MT_MATERIALS.MET_PRODUCT_DENSITY ?? "0.0000000001")));
                        betterLP1 = betterLP1 + (Decimal)(bl * (((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MIN ?? 0) * (Decimal)1000 * (Decimal)6.29) / Decimal.Parse(biddingItemForProposal.ElementAt(j).MT_MATERIALS.MET_PRODUCT_DENSITY ?? "0.0000000001")));
                        betterLP2 = betterLP2 + (Decimal)(bl * (((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MAX ?? 0) * (Decimal)1000 * (Decimal)6.29) / Decimal.Parse(biddingItemForProposal.ElementAt(j).MT_MATERIALS.MET_PRODUCT_DENSITY ?? "0.0000000001")));
                    }
                    else if (biddingItemForProposal.FirstOrDefault().PBI_QUANTITY_UNIT == "KBBL")
                    {
                        betterMarket1 = betterMarket1 + (Decimal)(bm * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MIN ?? 0) * (Decimal)1000));
                        betterMarket2 = betterMarket2 + (Decimal)(bm * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MAX ?? 0) * (Decimal)1000));
                        betterLP1 = betterLP1 + (Decimal)(bl * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MIN ?? 0) * (Decimal)1000));
                        betterLP2 = betterLP2 + (Decimal)(bl * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MAX ?? 0) * (Decimal)1000));
                    }
                    else if (biddingItemForProposal.FirstOrDefault().PBI_QUANTITY_UNIT == "MML")
                    {
                        betterMarket1 = betterMarket1 + (Decimal)(bm * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MIN ?? 0) / (Decimal)0.158984));
                        betterMarket2 = betterMarket2 + (Decimal)(bm * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MAX ?? 0) / (Decimal)0.158984));
                        betterLP1 = betterLP1 + (Decimal)(bl * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MIN ?? 0)) / (Decimal)0.158984);
                        betterLP2 = betterLP2 + (Decimal)(bl * ((biddingItemForProposal.ElementAt(j).PBI_AWARDED_QUANTITY_MAX ?? 0)) / (Decimal)0.158984);
                    }
                    for (int k = 0; k < biddingItemForProposal.Count; k++)
                    {
                        if (biddingItemForProposal.ElementAt(k).PBI_AWARDED_QUANTITY_MIN != biddingItemForProposal.ElementAt(k).PBI_AWARDED_QUANTITY_MAX)
                        {
                            isEqual = false;
                        }
                    }
                }
                if (isEqual)
                {
                    row.Cells[7].AddParagraph(String.Format("{0:n}", betterMarket1 * minus));
                    row.Cells[8].AddParagraph(String.Format("{0:n}", betterLP1 * minus));
                }
                else
                {
                    string sBetterMarket1 = String.Format("{0:n}", betterMarket1 * minus);
                    string sBetterMarket2 = String.Format("{0:n}", betterMarket2 * minus);
                    string sBetterLP1 = String.Format("{0:n}", betterLP1 * minus);
                    string sBetterLP2 = String.Format("{0:n}", betterLP2 * minus);
                    if (betterMarket1 * minus < 0)
                    {
                        sBetterMarket1 = "(" + String.Format("{0:n}", betterMarket1 * minus) + ")";
                    }
                    if (betterMarket2 * minus < 0)
                    {
                        sBetterMarket2 = "(" + String.Format("{0:n}", betterMarket2 * minus) + ")";
                    }
                    if (betterLP1 * minus < 0)
                    {
                        sBetterLP1 = "(" + String.Format("{0:n}", betterLP1 * minus) + ")";
                    }
                    if (betterLP2 * minus < 0)
                    {
                        sBetterLP2 = "(" + String.Format("{0:n}", betterLP2 * minus) + ")";
                    }
                    row.Cells[7].AddParagraph(sBetterMarket1 + " - " + sBetterMarket2);
                    row.Cells[8].AddParagraph(sBetterLP1 + " - " + sBetterLP2);


                }

                if (i < pafData.PAF_PROPOSAL_ITEMS.Count - 1)
                {
                    row1.Borders.Bottom.Visible = false;
                }
            }


            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable13(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(2));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column6 = table.AddColumn(Unit.FromCentimeter(2));
            Column column7 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column8 = table.AddColumn(Unit.FromCentimeter(2));
            Column column9 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column10 = table.AddColumn(Unit.FromCentimeter(2));
            Column column11 = table.AddColumn(Unit.FromCentimeter(3.5));

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Cells[0].MergeDown = 2;
            row1.Cells[1].MergeDown = 2;
            row1.Cells[2].MergeDown = 2;
            row1.Cells[3].MergeDown = 2;
            row1.Cells[10].MergeDown = 2;

            row1.Cells[4].MergeRight = 5;
            row2.Cells[4].MergeRight = 1;
            row2.Cells[6].MergeRight = 1;
            row2.Cells[8].MergeRight = 1;

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("Customer");
            row1.Cells[1].AddParagraph("Contact Person");
            row1.Cells[2].AddParagraph("Product");
            row1.Cells[3].AddParagraph("Unit");
            row1.Cells[4].AddParagraph("Contract (" + pafData.PDA_PRICE_UNIT + ")").Format.Alignment = ParagraphAlignment.Center;
            row1.Cells[10].AddParagraph("Notes");

            row2.Cells[4].AddParagraph("Existing");
            row2.Cells[6].AddParagraph("Customer proposal");
            row2.Cells[8].AddParagraph("Our proposal");

            row3.Cells[4].AddParagraph("Quantity");
            row3.Cells[5].AddParagraph("Price or formula");
            row3.Cells[6].AddParagraph("Quantity");
            row3.Cells[7].AddParagraph("Price or formula");
            row3.Cells[8].AddParagraph("Quantity");
            row3.Cells[9].AddParagraph("Price or formula");

            for (int i = 0; i < pafData.PAF_REVIEW_ITEMS.Count; i++)
            {
                Row row = table.AddRow();
                if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST != null)
                {
                    if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        MT_CUST_DETAIL cd = pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        row.Cells[0].AddParagraph(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2);
                    }
                }

                row.Cells[1].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PRI_CONTACT_PERSON + "");

                if (pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_MATERIALS != null)
                {
                    row.Cells[2].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                }
                row.Cells[3].AddParagraph(pafData.PAF_REVIEW_ITEMS.ElementAt(i).PRI_QUANTITY_UNIT + "");


                PAF_REVIEW_ITEMS_DETAIL rDetail = pafData.PAF_REVIEW_ITEMS.ElementAt(i).PAF_REVIEW_ITEMS_DETAIL.FirstOrDefault();
                if (rDetail != null)
                {
                    row.Cells[4].AddParagraph(showTolerance(rDetail.PRD_EXISTING_QTY_FLOAT) + "");

                    row.Cells[5].AddParagraph(showPrice(rDetail.PRD_EXISTING_PRICE_FLOAT, rDetail.PRD_EXISTING_PRICE_FIXED));

                    row.Cells[6].AddParagraph(showTolerance(rDetail.PRD_CUS_PROPOSAL_QTY_FLOAT) + "");

                    row.Cells[7].AddParagraph(showPrice(rDetail.PRD_CUS_PROPOSAL_PRICE_FLOAT, rDetail.PRD_CUS_PROPOSAL_PRICE_FIXED));

                    row.Cells[8].AddParagraph(showTolerance(rDetail.PRD_OUR_PROPOSAL_QTY_FLOAT) + "");

                    row.Cells[9].AddParagraph(showPrice(rDetail.PRD_OUR_PROPOSAL_PRICE_FLOAT, rDetail.PRD_OUR_PROPOSAL_PRICE_FIXED));

                    row.Cells[10].AddParagraph(rDetail.PRD_NOTE + "");
                    row.Cells[10].Format.Alignment = ParagraphAlignment.Left;
                    row.Cells[10].VerticalAlignment = VerticalAlignment.Top;
                }


            }

            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Table1: Applied for \"Customers request to review the existing price\" ");
            ph.Format.Font.Bold = true;
            doc.LastSection.Add(table);
        }

        public static String showPrice(String floatPrice, Nullable<decimal> fixedPrice)
        {
            if (floatPrice != null)
            {
                return floatPrice.Replace("|", " ").Replace("+ 0", "flat");
            }
            if (fixedPrice != null)
            {
                return String.Format("{0:n}", fixedPrice);
            }
            return "";
        }

        public static void genTable14(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            int addLP = 0;
            int rCount = 0;
            if (pafData.PAF_BIDDING_ITEMS.Count > 0)
            {
                for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count > rCount)
                    {
                        rCount = pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count;
                    }
                }
                if (pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PBI_LATEST_LP_PLAN_PRICE_FLOAT != null && pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PBI_LATEST_LP_PLAN_PRICE_FIXED != null)
                {
                    addLP = 1;
                }
            }

            int allColumn = 9 + rCount;
            double ratio = 20.0 / (16 + (1.5 * rCount));

            Column column1 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            Column column3 = table.AddColumn(Unit.FromCentimeter(2 * ratio));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));

            for (int i = 0; i < rCount; i++)
            {
                Column column = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            }

            Column column6 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));

            Column column7 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            if (addLP == 1)
            {
                Column columnAdd = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            }
            Column column8 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            if (addLP == 0)
            {
                Column column9 = table.AddColumn(Unit.FromCentimeter(4 * ratio));
            }
            else
            {
                Column column9 = table.AddColumn(Unit.FromCentimeter(2.5 * ratio));
            }



            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Cells[0].MergeDown = 2;
            row1.Cells[1].MergeDown = 2;
            row1.Cells[2].MergeDown = 2;
            row1.Cells[3].MergeDown = 2;
            row1.Cells[4].MergeDown = 2;

            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[5 + i].MergeDown = 1;
            }

            row2.Cells[5 + rCount].MergeDown = 1;
            if (addLP == 0)
            {
                row2.Cells[6 + rCount].MergeDown = 1;
            }
            else
            {
                row2.Cells[6 + rCount].MergeRight = 1;
            }



            row1.Cells[8 + rCount + addLP].MergeDown = 2;

            row1.Cells[5].MergeRight = rCount + 2;


            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("Customer");
            row1.Cells[1].AddParagraph("Contact Person");
            row1.Cells[2].AddParagraph("Product");
            row1.Cells[3].AddParagraph("Unit");
            row1.Cells[4].AddParagraph("Quantity");
            row1.Cells[5].AddParagraph("Bid price  or formula (" + pafData.PDA_PRICE_UNIT + ")");
            row1.Cells[8 + rCount + addLP].AddParagraph("Notes");

            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[5 + i].AddParagraph(getRoundName((i + 1) + ""));
            }

            row2.Cells[5 + rCount].AddParagraph("Market Price");
            row2.Cells[6 + rCount].AddParagraph("Latest LP plan price");
            if (addLP == 1)
            {
                row3.Cells[6 + rCount].AddParagraph("Float");
                row3.Cells[7 + rCount].AddParagraph("Fixed");
            }
            row2.Cells[7 + rCount + addLP].AddParagraph("Benchmark price");
            if (pafData.PAF_BIDDING_ITEMS.FirstOrDefault() != null)
            {
                if (pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault() != null)
                {
                    row3.Cells[7 + rCount + addLP].AddParagraph(pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault().PBB_NAME + "");
                }
            }


            List<PAF_BIDDING_ITEMS> bidItems = pafData.PAF_BIDDING_ITEMS.ToList();
            Row rowFirst = new Row();
            for (int i = 0; i < bidItems.Count; i++)
            {
                Row row = table.AddRow();
                if (i == 0)
                {
                    rowFirst = row;
                }
                if (bidItems.ElementAt(i).MT_CUST != null)
                {
                    if (bidItems.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        MT_CUST_DETAIL cd = bidItems.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        row.Cells[0].AddParagraph(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2);
                    }
                }
                row.Cells[1].AddParagraph(bidItems.ElementAt(i).PBI_CONTACT_PERSON + "");
                if (bidItems.ElementAt(i).MT_MATERIALS != null)
                {
                    row.Cells[2].AddParagraph(bidItems.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                }
                row.Cells[3].AddParagraph(bidItems.ElementAt(i).PBI_QUANTITY_UNIT + "");
                row.Cells[4].AddParagraph(showTolerance(bidItems.ElementAt(i).PBI_QUANTITY_FLOAT) + "");

                for (int j = 0; j < bidItems.ElementAt(i).PAF_ROUND.Count; j++)
                {
                    row.Cells[5 + j].AddParagraph(showPrice(bidItems.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_FLOAT, bidItems.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_FIXED));
                }
                //if (i == 0)
                //{
                row.Cells[5 + rCount].AddParagraph(String.Format("{0:n}", bidItems.ElementAt(i).PBI_MARKET_PRICE_FLOAT));
                if (addLP == 0)
                {
                    row.Cells[6 + rCount].AddParagraph(showPrice(bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FLOAT, bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FIXED));
                }
                else
                {

                    if (bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FLOAT != null)
                    {
                        row.Cells[6 + rCount].AddParagraph(bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FLOAT.Replace("|", " ").Replace("+ 0", "flat"));
                    }
                    row.Cells[7 + rCount].AddParagraph(bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FIXED + "");
                }
                if (bidItems.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault() != null)
                {
                    decimal testD;
                    if (decimal.TryParse(bidItems.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault().PBB_VALUE, out testD) == true)
                    {
                        row.Cells[7 + rCount + addLP].AddParagraph(String.Format("{0:n}", testD));
                    }
                    else
                    {
                        row.Cells[7 + rCount + addLP].AddParagraph(bidItems.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault().PBB_VALUE + "");
                    }

                }
                //}
                //if (i == bidItems.Count - 1)
                //{
                //    rowFirst.Cells[5 + rCount].MergeDown = i;
                //    rowFirst.Cells[6 + rCount].MergeDown = i;
                //    if (addLP == 1)
                //    {
                //        rowFirst.Cells[7 + rCount].MergeDown = i;
                //    }
                //    rowFirst.Cells[7 + rCount + addLP].MergeDown = i;
                //}
                row.Cells[8 + rCount + addLP].AddParagraph(bidItems.ElementAt(i).PBI_NOTE + "");
                row.Cells[8 + rCount + addLP].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[8 + rCount + addLP].VerticalAlignment = VerticalAlignment.Top;
            }



            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Applied for \"Product price bidding\"");
            ph.Format.Font.Bold = true;
            doc.LastSection.Add(table);
        }

        public static String showTolerance(String text)
        {
            if (text == null)
            {
                return "";
            }
            if (text.Contains("+/-"))
            {
                return text.Replace("+/-", "±") + "%";
            }
            return text;

        }

        public static void genTable15(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.2;
            table.Rows.Height = NORMAL_ROW_HEIGHT;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(3.5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(13));

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Cells[2].MergeDown = 2;

            column1.Format.Font.Bold = true;


            row1.Cells[0].AddParagraph("Contract Period :");
            row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row2.Cells[0].AddParagraph("Loading Period :");
            row2.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row3.Cells[0].AddParagraph("Discharging Period :");
            row3.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            Paragraph p1 = row1.Cells[2].AddParagraph("Any other special term and condition :");
            p1.Format.Font.Bold = true;

            if (pafData.PDA_CONTRACT_DATE_FROM != null && pafData.PDA_CONTRACT_DATE_TO != null)
            {
                row1.Cells[1].AddParagraph(((DateTime)pafData.PDA_CONTRACT_DATE_FROM).ToString("dd/MMM/yyyy") + " - " + ((DateTime)pafData.PDA_CONTRACT_DATE_TO).ToString("dd/MMM/yyyy"));
                row1.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }
            if (pafData.PDA_LOADING_DATE_FROM != null && pafData.PDA_LOADING_DATE_TO != null)
            {
                row2.Cells[1].AddParagraph(((DateTime)pafData.PDA_LOADING_DATE_FROM).ToString("dd/MMM/yyyy") + " - " + ((DateTime)pafData.PDA_LOADING_DATE_TO).ToString("dd/MMM/yyyy"));
                row2.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            if (pafData.PDA_DISCHARGING_DATE_FROM != null && pafData.PDA_DISCHARGING_DATE_TO != null)
            {
                row3.Cells[1].AddParagraph(((DateTime)pafData.PDA_DISCHARGING_DATE_FROM).ToString("dd/MMM/yyyy") + " - " + ((DateTime)pafData.PDA_DISCHARGING_DATE_TO).ToString("dd/MMM/yyyy"));
                row3.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            }

            row1.Cells[2].AddParagraph(pafData.PDA_SPECIAL_TERMS_OR_CONDITION + "");

            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable16(Document doc, PAF_DATA pafData)
        {


            Table table = new Table();
            table.Borders.Width = 0;
            table.Format.Font.Bold = true;

            List<Column> listColumn = new List<Column>();
            Column column1;
            Column column2;
            Column column3;
            Column column4;
            Column column5;
            Column column6;
            Column column7;
            Column column8;

            if (pafData.PDA_TEMPLATE == "CMLA_FORM_1")
            {
                column1 = table.AddColumn(Unit.FromCentimeter(1));
                column2 = table.AddColumn(Unit.FromCentimeter(1.8));
                column3 = table.AddColumn(Unit.FromCentimeter(3.9));
                column4 = table.AddColumn(Unit.FromCentimeter(3));
                column5 = table.AddColumn(Unit.FromCentimeter(1.3));
                column6 = table.AddColumn(Unit.FromCentimeter(4));
                column7 = table.AddColumn(Unit.FromCentimeter(2.5));
                column8 = table.AddColumn(Unit.FromCentimeter(2.5));
            }
            else
            {
                column1 = table.AddColumn(Unit.FromCentimeter(1));
                column2 = table.AddColumn(Unit.FromCentimeter(2));
                column3 = table.AddColumn(Unit.FromCentimeter(17));
                column4 = table.AddColumn(Unit.FromCentimeter(0));
                column5 = table.AddColumn(Unit.FromCentimeter(0));
                column6 = table.AddColumn(Unit.FromCentimeter(0));
                column7 = table.AddColumn(Unit.FromCentimeter(0));
                column8 = table.AddColumn(Unit.FromCentimeter(0));
            }


            column1.Borders.Left.Width = 0.4;
            column8.Borders.Right.Width = 0.4;

            column2.Format.Alignment = ParagraphAlignment.Right;
            column4.Format.Alignment = ParagraphAlignment.Center;
            column5.Format.Alignment = ParagraphAlignment.Center;
            column6.Format.Alignment = ParagraphAlignment.Center;
            column7.Format.Alignment = ParagraphAlignment.Center;
            column8.Format.Alignment = ParagraphAlignment.Center;

            for (int i = 0; i < pafData.PAF_PROPOSAL_ITEMS.Count; i++)
            {
                int mergeCount = 0;
                Row row = table.AddRow();

                if (i == 0)
                {
                    Paragraph p1 = row.Cells[0].AddParagraph("Proposal");
                }
                var proposal = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i);

                Row row1 = table.AddRow();
                row1.Cells[1].AddParagraph("Award to:");



                if (proposal.MT_CUST != null)
                {
                    mergeCount++;
                    if (proposal.MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        Paragraph pa = row1.Cells[2].AddParagraph();
                        MT_CUST_DETAIL cd = proposal.MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        pa.AddFormattedText(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2, TextFormat.NotBold);
                    }
                }
                else if (proposal.MT_VENDOR != null)
                {
                    mergeCount++;

                    Paragraph pa = row1.Cells[2].AddParagraph();
                    pa.AddFormattedText(proposal.MT_VENDOR.VND_NAME1 + "", TextFormat.NotBold);

                }


                if (proposal.PAF_PROPOSAL_REASON_ITEMS.Count > 0)
                {
                    mergeCount++;
                    Row row2 = table.AddRow();
                    for (int j = 0; j < proposal.PAF_PROPOSAL_REASON_ITEMS.Count; j++)
                    {

                        if (j == 0)
                        {
                            Paragraph p = row2.Cells[1].AddParagraph("Reason:");
                        }

                        Paragraph pd = row2.Cells[2].AddParagraph();
                        pd.AddFormattedText("- " + proposal.PAF_PROPOSAL_REASON_ITEMS.ElementAt(j).PSR_REASON + "", TextFormat.NotBold);
                        pd.AddText(" ");
                        pd.AddFormattedText(proposal.PAF_PROPOSAL_REASON_ITEMS.ElementAt(j).PSR_REASON_DETAIL + "", TextFormat.NotBold | TextFormat.Underline);

                    }
                }

                if (proposal.PAF_PROPOSAL_OTC_ITEMS.Count > 0)
                {
                    mergeCount++;
                    Row row3 = table.AddRow();
                    for (int j = 0; j < proposal.PAF_PROPOSAL_OTC_ITEMS.Count; j++)
                    {
                        if (j == 0)
                        {
                            Paragraph p = row3.Cells[1].AddParagraph();
                            p.AddText("Other terms & Conditions:");
                        }

                        Paragraph pd = row3.Cells[2].AddParagraph();
                        pd.AddFormattedText("- " + proposal.PAF_PROPOSAL_OTC_ITEMS.ElementAt(j).PSO_OTHER_TERM_COND + "", TextFormat.NotBold);
                        pd.AddText(" ");
                        pd.AddFormattedText(proposal.PAF_PROPOSAL_OTC_ITEMS.ElementAt(j).PSO_OTHER_TERM_COND_DETAIL + "", TextFormat.NotBold | TextFormat.Underline);

                    }
                }



                if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_CUSTOMER == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_CUSTOMER && p.PPI_FK_CUSTOMER != null).FirstOrDefault() != null)
                {
                    mergeCount++;
                    Row row4 = table.AddRow();
                    string x = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM;
                    int count = pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (j == 0)
                        {
                            Paragraph p = row4.Cells[1].AddParagraph();
                            p.AddText("Payment Terms :");
                        }
                        Paragraph pd = row4.Cells[2].AddParagraph();

                        pd.AddFormattedText("- " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_TYPE + "   ", TextFormat.NotBold);
                        pd.AddFormattedText(pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).MT_CUST.MCT_CUST_NUM).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_DETAIL + "   ", TextFormat.NotBold | TextFormat.Underline);
                        if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL != null)
                        {
                            pd.AddFormattedText("days " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.MT_CUST.MCT_CUST_NUM == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL.PMC_NAME, TextFormat.NotBold);
                        }

                    }
                }
                else if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).FirstOrDefault() != null)
                {
                    mergeCount++;
                    Row row4 = table.AddRow();
                    string x = pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR;
                    int count = pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (j == 0)
                        {
                            Paragraph p = row4.Cells[1].AddParagraph();
                            p.AddText("Payment Terms :");
                        }
                        Paragraph pd = row4.Cells[2].AddParagraph();

                        pd.AddFormattedText("- " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_TYPE + "   ", TextFormat.NotBold);
                        pd.AddFormattedText(pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_VENDOR).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PPD_PAYMENT_DETAIL + "   ", TextFormat.NotBold | TextFormat.Underline);
                        if (pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL != null)
                        {
                            pd.AddFormattedText("days " + pafData.PAF_PAYMENT_ITEMS.Where(p => p.PPI_FK_VENDOR == x).FirstOrDefault().PAF_PAYMENT_ITEMS_DETAIL.ElementAt(j).PAF_MT_CREDIT_DETAIL.PMC_NAME, TextFormat.NotBold);
                        }

                    }
                }
                Row rowAdd = table.AddRow();

                row.Cells[3].MergeDown = mergeCount;
                row.Cells[4].MergeDown = mergeCount;
                row.Cells[5].MergeDown = mergeCount;
                row.Cells[6].MergeDown = mergeCount;
                row.Cells[7].MergeDown = mergeCount;

                if (pafData.PDA_TEMPLATE == "CMLA_FORM_1")
                {
                    var biddingItemForProposal = pafData.PAF_BIDDING_ITEMS.Where(b => b.PBI_FK_CUSTOMER == pafData.PAF_PROPOSAL_ITEMS.ElementAt(i).PSI_FK_CUSTOMER).ToList();
                    column4.Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[3].AddParagraph("");
                    row.Cells[3].AddParagraph("");
                    row.Cells[3].AddParagraph("");
                    row.Cells[4].AddParagraph("");
                    row.Cells[5].AddParagraph("");

                    if (biddingItemForProposal.Count > 0)
                    {
                        row.Cells[4].AddParagraph("Quantity");
                        row.Cells[4].AddParagraph("(" + biddingItemForProposal.ElementAt(0).PBI_QUANTITY_UNIT + ")");
                    }
                    else
                    {
                        row.Cells[4].AddParagraph("Quantity");
                        row.Cells[4].AddParagraph("()");
                    }
                    row.Cells[5].AddParagraph("Final Price");
                    row.Cells[5].AddParagraph("");

                    row.Cells[7].AddParagraph("");
                    row.Cells[7].AddParagraph("Better than Benchmark");
                    row.Cells[7].AddParagraph("(" + pafData.PDA_PRICE_UNIT + ")");
                    if (pafData.PDA_TEMPLATE == "CMLA_FORM_1")
                    {
                        row.Cells[6].AddParagraph("");
                        row.Cells[6].AddParagraph("Better than LP Plan Price");
                        row.Cells[6].AddParagraph("(" + pafData.PDA_PRICE_UNIT + ")");
                    }

                    for (int j = 0; j < biddingItemForProposal.Count; j++)
                    {
                        if (biddingItemForProposal.ElementAt(j).PBI_AWARDED_BIDDER == "Y")
                        {

                            if (biddingItemForProposal.ElementAt(j).MT_MATERIALS != null)
                            {
                                row.Cells[3].AddParagraph(biddingItemForProposal.ElementAt(j).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                            }
                            else
                            {
                                row.Cells[3].AddParagraph("");
                            }
                            Paragraph p4 = row.Cells[4].AddParagraph();
                            p4.AddFormattedText(showTolerance(biddingItemForProposal.ElementAt(j).PBI_QUANTITY_FLOAT) + "", TextFormat.NotBold);
                            Paragraph pFP = row.Cells[5].AddParagraph();
                            if (biddingItemForProposal.ElementAt(j).PAF_ROUND.Count != 0)
                            {
                                pFP.AddFormattedText(showPrice(biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FLOAT, biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FIXED), TextFormat.NotBold);

                                if (biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FLOAT != null && biddingItemForProposal.ElementAt(j).PBI_LATEST_LP_PLAN_PRICE_FLOAT != null)
                                {
                                    string[] lastRound = biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FLOAT.Split('|');
                                    string[] lastedLP = biddingItemForProposal.ElementAt(j).PBI_LATEST_LP_PLAN_PRICE_FLOAT.Split('|');
                                    if (lastRound.Count() == 3 && lastedLP.Count() == 3)
                                    {
                                        if (lastRound[0] == lastedLP[0])
                                        {
                                            decimal diff = decimal.Parse(lastRound[1] + lastRound[2]) - decimal.Parse(lastedLP[1] + lastedLP[2]);
                                            Paragraph p5 = row.Cells[6].AddParagraph();
                                            p5.AddFormattedText(String.Format("{0:n}", diff), TextFormat.NotBold);
                                        }
                                        else
                                        {
                                            Paragraph p5 = row.Cells[6].AddParagraph();
                                            p5.AddFormattedText("n/a", TextFormat.NotBold);
                                        }
                                    }
                                }
                                else if (biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FIXED != null && biddingItemForProposal.ElementAt(j).PBI_LATEST_LP_PLAN_PRICE_FIXED != null)
                                {
                                    Paragraph p5 = row.Cells[6].AddParagraph();
                                    p5.AddFormattedText(String.Format("{0:n}", (biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FIXED - biddingItemForProposal.ElementAt(j).PBI_LATEST_LP_PLAN_PRICE_FIXED)), TextFormat.NotBold);
                                }
                                else
                                {
                                    Paragraph p5 = row.Cells[6].AddParagraph();
                                    p5.AddFormattedText("n/a", TextFormat.NotBold);
                                }

                                if (biddingItemForProposal.ElementAt(j).PAF_BIDDING_ITEMS_BENCHMARK.Count != 0)
                                {
                                    if (biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FIXED == null || biddingItemForProposal.ElementAt(j).PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault().PBB_VALUE == null)
                                    {
                                        Paragraph p6 = row.Cells[7].AddParagraph();
                                        p6.AddFormattedText("n/a", TextFormat.NotBold);
                                    }
                                    else
                                    {
                                        Paragraph p6 = row.Cells[7].AddParagraph();
                                        decimal testD = 0;
                                        if (decimal.TryParse(biddingItemForProposal.ElementAt(j).PAF_BIDDING_ITEMS_BENCHMARK.FirstOrDefault().PBB_VALUE, out testD) == true)
                                        {
                                            p6.AddFormattedText(String.Format("{0:n}", ((biddingItemForProposal.ElementAt(j).PAF_ROUND.LastOrDefault().PRN_FIXED) - testD)), TextFormat.NotBold);
                                        }
                                    }
                                }
                            }

                        }
                    }
                }


            }


            //table.Rows[table.Rows.Count - 1].Borders.Bottom.Visible = false;
            doc.LastSection.Add(table);
        }

        public static void genTable17(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            int rCount = 0;

            if (pafData.PAF_BIDDING_ITEMS.Count > 0)
            {
                for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count > rCount)
                    {
                        rCount = pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count;
                    }
                }
            }
            int bCount = 0;
            if (pafData.PAF_BIDDING_ITEMS.FirstOrDefault() != null)
            {
                bCount = pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PAF_BIDDING_ITEMS_BENCHMARK.Count;
            }

            int allColumn = 9 + rCount;
            double ratio = 20.0 / (11.8 + (1.1 * rCount) + (0.9 * bCount));

            Column column1 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column3 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column4 = table.AddColumn(Unit.FromCentimeter(0.6 * ratio));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.1 * ratio));
            Column column6 = table.AddColumn(Unit.FromCentimeter(1.2 * ratio));
            Column column7 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column8 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column9 = table.AddColumn(Unit.FromCentimeter(1 * ratio));

            for (int i = 0; i < rCount; i++)
            {
                Column column = table.AddColumn(Unit.FromCentimeter(1.1 * ratio));
            }

            for (int i = 0; i < bCount; i++)
            {
                Column column = table.AddColumn(Unit.FromCentimeter(0.9 * ratio));
            }

            Column column10 = table.AddColumn(Unit.FromCentimeter(1.1 * ratio));
            Column column11 = table.AddColumn(Unit.FromCentimeter(1.8 * ratio));


            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Cells[0].MergeDown = 2;
            row1.Cells[1].MergeDown = 2;
            row1.Cells[2].MergeDown = 2;
            row1.Cells[3].MergeDown = 2;
            row1.Cells[4].MergeDown = 2;
            row1.Cells[5].MergeDown = 2;
            row2.Cells[6].MergeDown = 1;
            row2.Cells[7].MergeDown = 1;
            row2.Cells[8].MergeDown = 1;
            row2.Cells[7].MergeRight = 1;

            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[9 + i].MergeDown = 1;
            }

            row2.Cells[9 + rCount + bCount].MergeDown = 1;
            row1.Cells[10 + rCount + bCount].MergeDown = 2;

            row1.Cells[6].MergeRight = rCount + bCount + 3;
            row2.Cells[9 + rCount].MergeRight = bCount - 1;

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("Customer");
            row1.Cells[1].AddParagraph("Contact Person");
            row1.Cells[2].AddParagraph("Product");
            row1.Cells[3].AddParagraph("Unit");
            row1.Cells[4].AddParagraph("Quantity");
            row1.Cells[5].AddParagraph("Destination");
            row1.Cells[6].AddParagraph("Bid price  or formula (" + pafData.PDA_PRICE_UNIT + ")");


            row2.Cells[6].AddParagraph("Packaging");
            row2.Cells[7].AddParagraph("Incoterm");
            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[9 + i].AddParagraph(getRoundName((i + 1) + ""));
            }
            row2.Cells[9 + rCount].AddParagraph("Market Price");

            row2.Cells[9 + rCount + bCount].AddParagraph("Latest LP Plan Price");
            row1.Cells[10 + rCount + bCount].AddParagraph("Notes");
            if (pafData.PAF_BIDDING_ITEMS.FirstOrDefault() != null)
            {
                if (pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PAF_BIDDING_ITEMS_BENCHMARK.Count > 0)
                {
                    for (int i = 0; i < bCount; i++)
                    {
                        row3.Cells[9 + rCount + i].AddParagraph(pafData.PAF_BIDDING_ITEMS.FirstOrDefault().PAF_BIDDING_ITEMS_BENCHMARK.ElementAt(i).PBB_NAME + "");
                    }
                }
            }


            List<PAF_BIDDING_ITEMS> bidItems = pafData.PAF_BIDDING_ITEMS.ToList();
            Row rowFirst = new Row();
            for (int i = 0; i < bidItems.Count; i++)
            {
                Row row = table.AddRow();
                if (i == 0)
                {
                    rowFirst = row;
                }
                if (bidItems.ElementAt(i).MT_CUST != null)
                {
                    if (bidItems.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault() != null)
                    {
                        MT_CUST_DETAIL cd = bidItems.ElementAt(i).MT_CUST.MT_CUST_DETAIL.Where(c => c.MCD_FK_COMPANY == pafData.PDA_FOR_COMPANY && c.MCD_NATION == "I").FirstOrDefault();
                        row.Cells[0].AddParagraph(cd.MCD_NAME_1 + " " + cd.MCD_NAME_2);
                    }
                }
                row.Cells[1].AddParagraph(bidItems.ElementAt(i).PBI_CONTACT_PERSON + "");
                if (bidItems.ElementAt(i).MT_MATERIALS != null)
                {
                    row.Cells[2].AddParagraph(bidItems.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                }
                row.Cells[3].AddParagraph(bidItems.ElementAt(i).PBI_QUANTITY_UNIT + "");
                row.Cells[4].AddParagraph(showTolerance(bidItems.ElementAt(i).PBI_QUANTITY_FLOAT) + "");
                if (bidItems.ElementAt(i).MT_COUNTRY != null)
                {
                    row.Cells[5].AddParagraph(bidItems.ElementAt(i).MT_COUNTRY.MCT_LANDX + "");
                }
                row.Cells[6].AddParagraph(bidItems.ElementAt(i).PBI_FK_PACKAGING + "");
                row.Cells[7].AddParagraph(bidItems.ElementAt(i).PBI_INCOTERM + "");
                row.Cells[8].AddParagraph(bidItems.ElementAt(i).PBI_PORT + "");

                for (int j = 0; j < bidItems.ElementAt(i).PAF_ROUND.Count; j++)
                {
                    row.Cells[9 + j].AddParagraph(showPrice(bidItems.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_FLOAT, bidItems.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_FIXED));
                }


                if (i == 0)
                {
                    for (int j = 0; j < bCount; j++)
                    {
                        decimal testD;
                        if (decimal.TryParse(bidItems.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK.ElementAt(j).PBB_VALUE, out testD) == true)
                        {
                            row.Cells[9 + rCount + j].AddParagraph(String.Format("{0:n}", testD));
                        }
                        else
                        {
                            row.Cells[9 + rCount + j].AddParagraph(bidItems.ElementAt(i).PAF_BIDDING_ITEMS_BENCHMARK.ElementAt(j).PBB_VALUE + "");
                        }

                    }
                    row.Cells[9 + rCount + bCount].AddParagraph(showPrice(bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FLOAT, bidItems.ElementAt(i).PBI_LATEST_LP_PLAN_PRICE_FIXED));
                }
                if (i == bidItems.Count - 1)
                {
                    for (int j = 0; j < bCount; j++)
                    {
                        rowFirst.Cells[9 + rCount + j].MergeDown = i;
                    }

                    rowFirst.Cells[9 + rCount + bCount].MergeDown = i;
                }

                row.Cells[10 + rCount + bCount].AddParagraph(bidItems.ElementAt(i).PBI_NOTE + "");
                row.Cells[10 + rCount + bCount].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[10 + rCount + bCount].VerticalAlignment = VerticalAlignment.Top;
            }



            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Applied for \"Product price bidding\"");
            ph.Format.Font.Bold = true;
            doc.LastSection.Add(table);
        }

        public static void genTable18(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            int rCount = 0;
            if (pafData.PAF_BIDDING_ITEMS.Count > 0)
            {
                for (int i = 0; i < pafData.PAF_BIDDING_ITEMS.Count; i++)
                {
                    if (pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count > rCount)
                    {
                        rCount = pafData.PAF_BIDDING_ITEMS.ElementAt(i).PAF_ROUND.Count;
                    }
                }
            }


            int allColumn = 9 + rCount;
            double ratio = 20.0 / (12.8 + (1.5 * rCount));

            Column column1 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column3 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column4 = table.AddColumn(Unit.FromCentimeter(1 * ratio));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.2 * ratio));
            Column column6 = table.AddColumn(Unit.FromCentimeter(1.1 * ratio));
            Column column7 = table.AddColumn(Unit.FromCentimeter(1 * ratio));

            for (int i = 0; i < rCount; i++)
            {
                Column column = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            }



            Column column8 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            Column column9 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
            Column column10 = table.AddColumn(Unit.FromCentimeter(2.5 * ratio));


            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Cells[0].MergeDown = 2;
            row1.Cells[1].MergeDown = 2;
            row1.Cells[2].MergeDown = 2;
            row1.Cells[3].MergeDown = 2;
            row1.Cells[4].MergeDown = 2;
            row2.Cells[5].MergeDown = 1;
            row2.Cells[6].MergeDown = 1;

            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[7 + i].MergeDown = 1;
            }

            row2.Cells[7 + rCount].MergeDown = 1;
            row2.Cells[8 + rCount].MergeDown = 1;
            row1.Cells[9 + rCount].MergeDown = 2;

            row1.Cells[5].MergeRight = rCount + 3;

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;

            row1.Cells[0].AddParagraph("Supplier");
            row1.Cells[1].AddParagraph("Contact Person");
            row1.Cells[2].AddParagraph("Product");
            row1.Cells[3].AddParagraph("Unit");
            row1.Cells[4].AddParagraph("Quantity");
            row1.Cells[5].AddParagraph("Bid price  or formula (" + pafData.PDA_PRICE_UNIT + ")");


            row2.Cells[5].AddParagraph("Packaging");
            row2.Cells[6].AddParagraph("Incoterm");
            for (int i = 0; i < rCount; i++)
            {
                row2.Cells[7 + i].AddParagraph(getRoundName((i + 1) + ""));
            }


            row2.Cells[7 + rCount].AddParagraph("Freight Cost");
            row2.Cells[8 + rCount].AddParagraph("Benefit above break even price");
            row1.Cells[9 + rCount].AddParagraph("Notes");



            List<PAF_BIDDING_ITEMS> bidItems = pafData.PAF_BIDDING_ITEMS.ToList();
            for (int i = 0; i < bidItems.Count; i++)
            {
                Row row = table.AddRow();

                if (bidItems.ElementAt(i).MT_VENDOR != null)
                {
                    row.Cells[0].AddParagraph(bidItems.ElementAt(i).MT_VENDOR.VND_NAME1 + "");
                }
                row.Cells[1].AddParagraph(bidItems.ElementAt(i).PBI_CONTACT_PERSON + "");
                if (bidItems.ElementAt(i).MT_MATERIALS != null)
                {
                    row.Cells[2].AddParagraph(bidItems.ElementAt(i).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                }
                row.Cells[3].AddParagraph(bidItems.ElementAt(i).PBI_QUANTITY_UNIT + "");
                row.Cells[4].AddParagraph(showTolerance(bidItems.ElementAt(i).PBI_QUANTITY_FLOAT) + "");
                row.Cells[5].AddParagraph(bidItems.ElementAt(i).PBI_FK_PACKAGING + "");
                row.Cells[6].AddParagraph(bidItems.ElementAt(i).PBI_INCOTERM + "");

                for (int j = 0; j < bidItems.ElementAt(i).PAF_ROUND.Count; j++)
                {
                    row.Cells[7 + j].AddParagraph(showPrice(bidItems.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_FLOAT, bidItems.ElementAt(i).PAF_ROUND.ElementAt(j).PRN_FIXED));
                }


                row.Cells[7 + rCount].AddParagraph(String.Format("{0:n}", bidItems.ElementAt(i).PBI_FREIGHT_PRICE));
                row.Cells[8 + rCount].AddParagraph(String.Format("{0:n}", bidItems.ElementAt(i).PBI_BENEFIT));
                row.Cells[9 + rCount].AddParagraph(bidItems.ElementAt(i).PBI_NOTE + "");
                row.Cells[9 + rCount].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[9 + rCount].VerticalAlignment = VerticalAlignment.Top;
            }



            doc.LastSection.AddParagraph("");
            Paragraph ph = doc.LastSection.AddParagraph("Applied for \"Product price bidding\"");
            ph.Format.Font.Bold = true;
            doc.LastSection.Add(table);
        }

        public static String getRoundName(String round)
        {
            if (round == "1")
            {
                return "1st Bid";
            }
            else if (round == "2")
            {
                return "2nd Bid";
            }
            else if (round == "3")
            {
                return "3rd Bid";
            }
            else if (round == "4")
            {
                return "4th Bid";
            }
            else if (round == "5")
            {
                return "5th Bid";
            }
            else if (round == "6")
            {
                return "6th Bid";
            }
            else
            {
                return "";
            }
        }
        public static void genTableAttachFile(Document doc, PAF_DATA pafData)
        {

            Table table = new Table();
            table.Format.Alignment = ParagraphAlignment.Center;
            //table.Borders.Width = 0.4;
            Column column = table.AddColumn(Unit.FromCentimeter(20));
            var listFile = pafData.PAF_ATTACH_FILE.Where(f => f.PAT_TYPE == "BMS").ToList();
            if (listFile.Count > 0)
            {
                doc.AddSection();
                var logoImage = doc.LastSection.AddImage(_FN.GetPathLogo());
                logoImage.WrapFormat.DistanceLeft = 460;
                logoImage.Width = "2.0cm";
                logoImage.LockAspectRatio = true;

                Paragraph header;
                if (pafData.PDA_TEMPLATE == "CMPS_DOM_SALE" || pafData.PDA_TEMPLATE == "CMPS_INTER_SALE" || pafData.PDA_TEMPLATE == "CMLA_FORM_1" || pafData.PDA_TEMPLATE == "CMLA_FORM_2")
                {
                    header = doc.LastSection.AddParagraph("Product Sale Approval Form");
                }
                else if (pafData.PDA_TEMPLATE == "CMPS_IMPORT")
                {
                    header = doc.LastSection.AddParagraph("Component Import Approval Form");
                }
                else if (pafData.PDA_TEMPLATE == "CMLA_IMPORT")
                {
                    header = doc.LastSection.AddParagraph("Product Purchasing Approval Form");
                }
                else if (pafData.PDA_TEMPLATE == "CMLA_BITUMEN")
                {
                    header = doc.LastSection.AddParagraph("Product Sale Approval Form (Bitumen)");
                }
                else
                {
                    header = doc.LastSection.AddParagraph("Product Approval Form");
                }
                header.Format.Alignment = ParagraphAlignment.Center;
                header.Format.Font.Bold = true;
                header.Format.Font.Size = 6;

                genTable1(doc, pafData);
                if (pafData.PDA_TEMPLATE == "CMPS_OTHER")
                {
                    genTable23(doc, pafData);
                }
                else
                {
                    genTable2(doc, pafData);
                }

            }
            for (int i = 0; i < listFile.Count; i++)
            {
                Row row = table.AddRow();
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].Format.Font.Bold = true;
                //row.Format.Alignment = ParagraphAlignment.Center;
                Paragraph pImage = row.Cells[0].AddParagraph();
                pImage.Format.Borders.DistanceFromTop = 20;
                string URL = JSONSetting.getGlobalConfig("ROOT_URL");
                //var image = pImage.AddImage(HttpContext.Current.Request.PhysicalApplicationPath + "/" + listFile.ElementAt(i).PAT_PATH + "");
                var image = pImage.AddImage(GetExplanImage(listFile.ElementAt(i).PAT_PATH + "", URL));
                row.Cells[0].AddParagraph();
                Paragraph pCap = row.Cells[0].AddParagraph(listFile.ElementAt(i).PAT_CAPTION + "");
                if (System.IO.File.Exists(GetExplanImage(listFile.ElementAt(i).PAT_PATH + "", URL)))
                {
                    System.Drawing.Image pic = System.Drawing.Image.FromFile(GetExplanImage(listFile.ElementAt(i).PAT_PATH + "", URL));
                    if (pic.Width > 700)
                    {
                        image.Width = "19.0cm";
                        image.LockAspectRatio = true;
                    }
                    else if (pic.Height > 800)
                    {
                        image.Height = "21.7cm";
                        image.LockAspectRatio = true;
                    }
                }

                image.WrapFormat.DistanceTop = 20;

            }
            doc.LastSection.Add(table);



        }

        public static string GetExplanImage(string PathExplan, string URL = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(URL))
                {
                    string path = Path.Combine(URL, PathExplan);
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(path, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathExplan));
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PathExplan);
        }

        public static void genTable19(Document doc, PAF_DATA pafData)
        {
            for (int g = 0; g < pafData.PAF_BTM_GRADE_ITEMS.Count; g++)
            {
                Table table = new Table();
                table.Borders.Width = 0.4;
                table.Format.Alignment = ParagraphAlignment.Center;
                table.Rows.Height = NORMAL_ROW_HEIGHT;
                table.Rows.VerticalAlignment = VerticalAlignment.Center;

                int aCount = pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.Count;


                int allColumn = 9 + aCount;
                double ratio = 20.0 / (9 + (1.5 * aCount));

                Column column1 = table.AddColumn(Unit.FromCentimeter(2 * ratio));
                Column column2 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
                Column column3 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
                Column column4 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
                Column column5 = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));

                for (int i = 0; i < aCount; i++)
                {
                    Column column = table.AddColumn(Unit.FromCentimeter(1.5 * ratio));
                }

                Column column6 = table.AddColumn(Unit.FromCentimeter(1 * ratio));


                Row row1 = table.AddRow();
                Row row2 = table.AddRow();
                Row row3 = table.AddRow();
                Row row4 = table.AddRow();

                row1.Cells[0].MergeDown = 1;
                row3.Cells[0].MergeDown = 1;

                row1.Cells[1].MergeRight = 1;
                row4.Cells[1].MergeRight = 1;

                row1.Cells[3].MergeDown = 1;
                row3.Cells[3].MergeDown = 1;

                row1.Cells[4].MergeRight = aCount;
                row3.Cells[4].MergeDown = 1;
                row4.Cells[5].MergeRight = aCount - 1;

                row1.Cells[5 + aCount].MergeDown = 1;
                row3.Cells[5 + aCount].MergeDown = 1;

                row1.Cells[0].AddParagraph("Bitumen Grade");
                row1.Cells[1].AddParagraph("O/S");
                row2.Cells[1].AddParagraph("O/S (Actual)");
                row2.Cells[2].AddParagraph("Safety Stock");
                row1.Cells[3].AddParagraph("Production");
                row1.Cells[4].AddParagraph("Allocation");
                row2.Cells[4].AddParagraph("Deferred Volume");
                row1.Cells[5 + aCount].AddParagraph("C/S");

                row3.Cells[0].AddParagraph(pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
                row3.Cells[1].AddParagraph(string.Format("{0:n0}", pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_OS_ACTUAL));
                row3.Cells[2].AddParagraph(string.Format("{0:n0}", pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_SAFETY_STOCK));
                row4.Cells[1].AddParagraph("Total O/S = " + string.Format("{0:n0}", pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_OS_ACTUAL + pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_SAFETY_STOCK) + " " + pafData.PAF_BTM_DETAIL.PBD_VOLUME_UNIT);
                row4.Cells[1].Format.Font.Bold = true;
                row3.Cells[3].AddParagraph(string.Format("{0:n0}", pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_PRODUCTION));
                row3.Cells[4].AddParagraph(string.Format("{0:n0}", pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_DEFFERED_VOLUME));
                Decimal aTotal = 0;
                for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.Count; i++)
                {
                    row2.Cells[5 + i].AddParagraph(getNameFromCustNum(pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PBA_FK_CUSTOMER, "1400"));
                    if (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAV_INFO != null)
                    {
                        row2.Cells[5 + i].AddParagraph("(" + pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAV_INFO + ")");
                    }
                    row3.Cells[5 + i].AddParagraph(string.Format("{0:n0}", pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAV_VOLUME));
                    aTotal = aTotal + (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAV_VOLUME ?? 0);
                }
                row4.Cells[5].AddParagraph("Total Allocation = " + string.Format("{0:n0}", aTotal) + " " + pafData.PAF_BTM_DETAIL.PBD_VOLUME_UNIT);
                Decimal cs = ((pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_OS_ACTUAL ?? 0)
                            + (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_SAFETY_STOCK ?? 0)
                            + (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_PRODUCTION) ?? 0)
                            - (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PBG_DEFFERED_VOLUME ?? 0)
                            - aTotal;
                row3.Cells[5 + aCount].AddParagraph(string.Format("{0:n0}", cs));

                row1.Format.Font.Bold = true;
                row2.Format.Font.Bold = true;
                row4.Cells[5].Format.Font.Bold = true;

                doc.LastSection.Add(table);
                doc.LastSection.AddParagraph("");
            }
        }

        public static void genTable20(Document doc, PAF_DATA pafData)
        {

            Table table = new Table();
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(7.5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(4.5));
            Column column4 = table.AddColumn(Unit.FromCentimeter(3.5));
            Column column5 = table.AddColumn(Unit.FromCentimeter(1.5));
            Column column6 = table.AddColumn(Unit.FromCentimeter(1.5));

            column1.Format.Alignment = ParagraphAlignment.Left;
            column2.Format.Alignment = ParagraphAlignment.Right;
            column3.Format.Alignment = ParagraphAlignment.Left;
            column4.Format.Alignment = ParagraphAlignment.Left;
            column5.Format.Alignment = ParagraphAlignment.Right;
            column6.Format.Alignment = ParagraphAlignment.Left;


            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();



            row1.Cells[0].AddParagraph("Reference Price");
            row2.Cells[0].AddParagraph("1) Argus price = To use Argus Bitumen at 3rd week of previous loading month ");
            string sDateFx = "";
            string sDateHsfo = "";
            if (pafData.PAF_BTM_DETAIL.PBD_FX_DATE_FROM != null && pafData.PAF_BTM_DETAIL.PBD_FX_DATE_TO != null)
            {
                sDateFx = " (" + ((DateTime)pafData.PAF_BTM_DETAIL.PBD_FX_DATE_FROM).ToString("dd/MM/yyyy") + " - " + ((DateTime)pafData.PAF_BTM_DETAIL.PBD_FX_DATE_TO).ToString("dd/MM/yyyy") + ")";
            }
            if (pafData.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM != null && pafData.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO != null)
            {
                sDateHsfo = " (" + ((DateTime)pafData.PAF_BTM_DETAIL.PBD_HSFO_DATE_FROM).ToString("dd/MM/yyyy") + " - " + ((DateTime)pafData.PAF_BTM_DETAIL.PBD_HSFO_DATE_TO).ToString("dd/MM/yyyy") + ")";
            }
            row3.Cells[0].AddParagraph("2) Estimated FX in previous loading month" + sDateFx);
            row4.Cells[0].AddParagraph("3) Avg. HSFO in previous loading month" + sDateHsfo);

            row2.Cells[1].AddParagraph(string.Format("{0:n}", pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
            row3.Cells[1].AddParagraph(string.Format("{0:n}", pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX));
            row4.Cells[1].AddParagraph(string.Format("{0:n}", pafData.PAF_BTM_DETAIL.PBD_AVG_HSFO));

            row2.Cells[2].AddParagraph(pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE_UNIT);
            row3.Cells[2].AddParagraph(pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX_UNIT);
            row4.Cells[2].AddParagraph(pafData.PAF_BTM_DETAIL.PBD_AVG_HSFO_UNIT);

            row1.Cells[3].AddParagraph("Weighted Bitumen Price");
            row2.Cells[3].AddParagraph("Simplan (production plan)");
            row3.Cells[3].AddParagraph("Corp. plan");

            row2.Cells[4].AddParagraph(string.Format("{0:n}", pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN));
            row3.Cells[4].AddParagraph(string.Format("{0:n}", pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN));

            row2.Cells[5].AddParagraph("USD/MT");
            row3.Cells[5].AddParagraph("USD/MT");

            row1.Format.Font.Bold = true;

            doc.LastSection.Add(table);
            doc.LastSection.AddParagraph("");

        }

        public static void genTable21(Document doc, PAF_DATA pafData)
        {

            Table table = new Table();
            table.Borders.Width = 0.4;

            Column column1 = table.AddColumn(Unit.FromCentimeter(20));
            Row row1 = table.AddRow();
            row1.Height = 60;

            if (!string.IsNullOrEmpty(pafData.PDA_BRIEF))
            {
                Paragraph p2 = row1.Cells[0].AddParagraph();
                HtmlRemoval htmlRemove = new HtmlRemoval();
                htmlRemove.ValuetoParagraph(pafData.PDA_BRIEF, p2);
            }
            else
            {
                row1.Cells[0].AddParagraph("");
            }

            doc.LastSection.Add(table);
            doc.LastSection.AddParagraph("");

        }

        public static void genTable22(Document doc, PAF_DATA pafData)
        {

            for (int g = 0; g < pafData.PAF_BTM_GRADE_ITEMS.Count; g++)
            {
                genTable22_1(doc, pafData, g);
                genTable22_2(doc, pafData, g);
                genTable22_3(doc, pafData, g);
                genTable22_4(doc, pafData, g);
            }

        }

        public static void genTable22_1(Document doc, PAF_DATA pafData, int g)
        {
            Table table = new Table();
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Top;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3));
            Column column2 = table.AddColumn(Unit.FromCentimeter(17));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            row1.Cells[0].AddParagraph(pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).MT_MATERIALS.MET_MAT_DES_ENGLISH + "");
            row1.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row1.Cells[0].Shading.Color = Colors.LightBlue;
            row2.Cells[0].AddParagraph("Assumption");

            row1.Cells[0].Format.Font.Bold = true;
            row2.Cells[0].Format.Font.Bold = true;

            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_GRADE_ASSUME_ITEMS.Count; i++)
            {
                row2.Cells[1].AddParagraph((i + 1) + ") " + pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_GRADE_ASSUME_ITEMS.ElementAt(i).PGA_DETAIL);
            }

            doc.LastSection.Add(table);
            doc.LastSection.AddParagraph("");
        }

        public static void genTable22_2(Document doc, PAF_DATA pafData, int g)
        {

            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3.2));
            Column column2 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column3 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column4 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column5 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column6 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column7 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column8 = table.AddColumn(Unit.FromCentimeter(2.4));

            column1.Format.Font.Bold = true;
            column8.Format.Font.Bold = true;

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            row1.Shading.Color = Colors.LightGray;
            row2.Shading.Color = Colors.LightGray;

            row1.Cells[0].MergeDown = 1;
            row1.Cells[1].MergeDown = 1;
            row1.Cells[2].MergeDown = 1;
            row1.Cells[3].MergeRight = 3;
            row1.Cells[7].MergeDown = 1;

            row1.Cells[0].AddParagraph("Customer");
            row1.Cells[1].AddParagraph("Tier");
            row1.Cells[2].AddParagraph("Volume (MT)");
            row1.Cells[3].AddParagraph("Pricing Structure");
            row2.Cells[3].AddParagraph("Formula");
            row2.Cells[4].AddParagraph("(USD/MT)");
            row2.Cells[5].AddParagraph("(Baht/MT)");
            row2.Cells[6].AddParagraph("Value (Baht)");
            row1.Cells[7].AddParagraph("Comparing to Argus");



            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.Count; i++)
            {
                if (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.Count > 0)
                {
                    int countMerge = 0;
                    Row firstRow = new Row();
                    decimal sumVolume = 0;
                    decimal sumValueUSD = 0;
                    decimal sumValueBaht = 0;
                    bool haveSum = true;
                    string group = "";
                    for (int j = 0; j < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.Count; j++)
                    {
                        PAF_BTM_ALLOCATE_ITEMS_DETAIL ad = pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.ElementAt(j);
                        Row row = table.AddRow();
                        if (firstRow.Cells.Count == 0)
                        {
                            firstRow = row;
                        }

                        row.Cells[0].AddParagraph(getNameFromCustNum(pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PBA_FK_CUSTOMER, "1400"));
                        row.Cells[1].AddParagraph(ad.PAF_MT_BTM_TIER.PMT_NAME + "");
                        row.Cells[2].AddParagraph(string.Format("{0:n0}", ad.PAD_TIER_VOLUME));
                        row.Cells[3].AddParagraph(ad.PAD_FORMULA + "");
                        row.Cells[4].AddParagraph(string.Format("{0:n0}", ad.PAD_USD_PER_UNIT));
                        row.Cells[5].AddParagraph(string.Format("{0:n0}", ad.PAD_USD_PER_UNIT * pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX));
                        row.Cells[6].AddParagraph(string.Format("{0:n0}", Math.Round((ad.PAD_USD_PER_UNIT ?? 0) * (pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX ?? 0)) * ad.PAD_TIER_VOLUME));
                        sumVolume = sumVolume + (ad.PAD_TIER_VOLUME ?? 0);
                        sumValueUSD = sumValueUSD + Math.Round((ad.PAD_USD_PER_UNIT ?? 0) * (ad.PAD_TIER_VOLUME ?? 0));
                        sumValueBaht = sumValueBaht + Math.Round((ad.PAD_USD_PER_UNIT ?? 0) * (pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX ?? 0)) * (ad.PAD_TIER_VOLUME ?? 0);

                        if (ad.PAD_USD_PER_UNIT - pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE < 0)
                        {
                            row.Cells[7].AddParagraph("Argus " + string.Format("{0:n0}", ad.PAD_USD_PER_UNIT - pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                        }
                        else
                        {
                            row.Cells[7].AddParagraph("Argus +" + string.Format("{0:n0}", ad.PAD_USD_PER_UNIT - pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                        }


                        if (group == "")
                        {
                            group = ad.PAF_MT_BTM_TIER.PMT_GROUP;
                        }
                        else
                        {
                            if (group != ad.PAF_MT_BTM_TIER.PMT_GROUP)
                            {
                                haveSum = false;
                            }
                        }
                        countMerge++;
                    }
                    if (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.Count == 1)
                    {
                        haveSum = false;
                    }
                    if (haveSum == true)
                    {
                        Row row = table.AddRow();
                        if (firstRow.Cells.Count == 0)
                        {
                            firstRow = row;
                        }
                        row.Cells[1].AddParagraph("SUM");
                        row.Cells[2].AddParagraph(string.Format("{0:n0}", sumVolume));
                        row.Cells[6].AddParagraph(string.Format("{0:n0}", sumValueBaht));

                        if (sumVolume != 0)
                        {
                            row.Cells[4].AddParagraph(string.Format("{0:n0}", sumValueUSD / sumVolume));
                            row.Cells[5].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) * pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX));
                            if (Math.Round(sumValueUSD / sumVolume) - pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE < 0)
                            {
                                row.Cells[7].AddParagraph("Argus " + string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                            }
                            else
                            {
                                row.Cells[7].AddParagraph("Argus +" + string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                            }
                        }
                        row.Cells[1].Shading.Color = Colors.Yellow;
                        row.Cells[2].Shading.Color = Colors.Yellow;
                        row.Cells[3].Shading.Color = Colors.Yellow;
                        row.Cells[4].Shading.Color = Colors.Yellow;
                        row.Cells[5].Shading.Color = Colors.Yellow;
                        row.Cells[6].Shading.Color = Colors.Yellow;
                        row.Cells[7].Shading.Color = Colors.Yellow;

                        row.Format.Font.Bold = true;
                        countMerge++;
                    }
                    if (firstRow != null && countMerge != 0)
                    {
                        firstRow.Cells[0].MergeDown = countMerge - 1;
                    }
                }
            }

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;



            doc.LastSection.Add(table);
            doc.LastSection.AddParagraph("");
        }

        public static void genTable22_3(Document doc, PAF_DATA pafData, int g)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3.2));
            Column column2 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column3 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column4 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column5 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column6 = table.AddColumn(Unit.FromCentimeter(3.6));
            Column column7 = table.AddColumn(Unit.FromCentimeter(3.6));

            column1.Format.Font.Bold = true;

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            row1.Shading.Color = Colors.Gray;
            row2.Shading.Color = Colors.LightGray;
            row3.Shading.Color = Colors.LightGray;

            row1.Cells[0].MergeRight = 6;
            row2.Cells[0].MergeDown = 1;
            row2.Cells[1].MergeDown = 1;
            row2.Cells[2].MergeDown = 1;
            row2.Cells[3].MergeRight = 1;
            row2.Cells[5].MergeDown = 1;
            row2.Cells[6].MergeDown = 1;

            row1.Cells[0].AddParagraph("Summary");
            row2.Cells[0].AddParagraph("Customer");
            row2.Cells[1].AddParagraph("Tier");
            row2.Cells[2].AddParagraph("Volume");
            row2.Cells[3].AddParagraph("Price");
            row3.Cells[3].AddParagraph("(Baht/MT)");
            row3.Cells[4].AddParagraph("(USD/MT)");
            row2.Cells[5].AddParagraph("Selling Price - Argus Bitumen");
            row2.Cells[6].AddParagraph("Selling Price  - HSFO Price");

            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.Count; i++)
            {
                Row firstRow = new Row();
                int mergeCount = 0;
                var sortForGroup = pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.OrderBy(b => b.PAF_MT_BTM_TIER.PMT_GROUP).ToList();
                String oldGroup = "";
                decimal sumVolume = 0;
                decimal sumValueUSD = 0;
                if (sortForGroup.Count > 0)
                {
                    oldGroup = sortForGroup.ElementAt(0).PAF_MT_BTM_TIER.PMT_GROUP;
                }


                for (int j = 0; j < sortForGroup.Count; j++)
                {

                    if (oldGroup == sortForGroup.ElementAt(j).PAF_MT_BTM_TIER.PMT_GROUP)
                    {
                        sumVolume = sumVolume + (sortForGroup.ElementAt(j).PAD_TIER_VOLUME ?? 0);
                        sumValueUSD = sumValueUSD + ((sortForGroup.ElementAt(j).PAD_USD_PER_UNIT ?? 0) * (sortForGroup.ElementAt(j).PAD_TIER_VOLUME ?? 0));
                    }
                    else
                    {

                        Row row = table.AddRow();
                        if (firstRow.Cells.Count == 0)
                        {
                            firstRow = row;
                        }
                        row.Cells[0].AddParagraph(getNameFromCustNum(pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PBA_FK_CUSTOMER, "1400"));
                        row.Cells[1].AddParagraph(sortForGroup.ElementAt(j - 1).PAF_MT_BTM_TIER.PMT_DETAIL + "");
                        row.Cells[2].AddParagraph(string.Format("{0:n0}", sumVolume));
                        if (sumVolume != 0)
                        {
                            row.Cells[3].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) * (pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX ?? 0)));
                            row.Cells[4].AddParagraph(string.Format("{0:n0}", sumValueUSD / sumVolume));
                            row.Cells[5].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - (decimal)pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                            row.Cells[6].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - (decimal)pafData.PAF_BTM_DETAIL.PBD_AVG_HSFO));

                        }

                        mergeCount++;
                        sumVolume = sortForGroup.ElementAt(j).PAD_TIER_VOLUME ?? 0;
                        sumValueUSD = (sortForGroup.ElementAt(j).PAD_USD_PER_UNIT ?? 0) * (sortForGroup.ElementAt(j).PAD_TIER_VOLUME ?? 0);
                    }
                    if (j == sortForGroup.Count - 1)
                    {
                        Row row = table.AddRow();
                        if (firstRow.Cells.Count == 0)
                        {
                            firstRow = row;
                        }
                        row.Cells[0].AddParagraph(getNameFromCustNum(pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PBA_FK_CUSTOMER, "1400"));
                        row.Cells[1].AddParagraph(sortForGroup.ElementAt(j).PAF_MT_BTM_TIER.PMT_DETAIL + "");
                        row.Cells[2].AddParagraph(string.Format("{0:n0}", sumVolume));
                        if (sumVolume != 0)
                        {
                            row.Cells[3].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) * (pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX ?? 0)));
                            row.Cells[4].AddParagraph(string.Format("{0:n0}", sumValueUSD / sumVolume));
                            row.Cells[5].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - (decimal)pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                            row.Cells[6].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - (decimal)pafData.PAF_BTM_DETAIL.PBD_AVG_HSFO));

                        }


                        mergeCount++;
                    }

                }
                if (mergeCount > 0)
                {
                    firstRow.Cells[0].MergeDown = mergeCount - 1;
                }

            }

            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;
            row3.Format.Font.Bold = true;


            doc.LastSection.Add(table);
            doc.LastSection.AddParagraph("");
        }

        public static void genTable22_4(Document doc, PAF_DATA pafData, int g)
        {
            Table table = new Table();
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(3.2));
            Column column2 = table.AddColumn(Unit.FromCentimeter(1.2));
            Column column3 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column4 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column5 = table.AddColumn(Unit.FromCentimeter(3.6));
            Column column6 = table.AddColumn(Unit.FromCentimeter(2.4));
            Column column7 = table.AddColumn(Unit.FromCentimeter(2.4));


            Row row1 = table.AddRow();
            Row row2 = table.AddRow();


            row1.Cells[0].AddParagraph("Weighted Price");

            row1.Cells[5].AddParagraph("Wgt. Price - Argus");
            row1.Cells[6].AddParagraph("Wgt. Price - HSFO");

            row1.Cells[5].Shading.Color = Colors.Gray;
            row1.Cells[6].Shading.Color = Colors.Gray;

            decimal sumVolume = 0;
            decimal sumValueUSD = 0;
            for (int i = 0; i < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.Count; i++)
            {
                decimal subSumVolume = 0;
                decimal subSumValueUSD = 0;
                for (int j = 0; j < pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.Count; j++)
                {
                    subSumVolume = subSumVolume + (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.ElementAt(j).PAD_TIER_VOLUME ?? 0);
                    subSumValueUSD = subSumValueUSD + (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.ElementAt(j).PAD_USD_PER_UNIT ?? 0) * (pafData.PAF_BTM_GRADE_ITEMS.ElementAt(g).PAF_BTM_ALLOCATE_ITEMS.ElementAt(i).PAF_BTM_ALLOCATE_ITEMS_DETAIL.ElementAt(j).PAD_TIER_VOLUME ?? 0);
                }
                subSumVolume = Math.Round(subSumVolume);
                subSumValueUSD = Math.Round(subSumValueUSD);
                decimal USDMT = 0;
                if (subSumVolume != 0)
                {
                    USDMT = Math.Round(subSumValueUSD / subSumVolume);
                }
                sumVolume = sumVolume + subSumVolume;
                sumValueUSD = sumValueUSD + (USDMT * subSumVolume);
            }

            row1.Cells[2].Borders.Width = 0.4;
            row1.Cells[3].Borders.Width = 0.4;
            row2.Cells[2].Borders.Width = 0.4;
            row2.Cells[3].Borders.Width = 0.4;

            row1.Cells[2].Borders.Right.Visible = false;
            row2.Cells[2].Borders.Right.Visible = false;
            row1.Cells[3].Borders.Left.Visible = false;
            row2.Cells[3].Borders.Left.Visible = false;

            row1.Cells[5].Borders.Width = 0.4;
            row1.Cells[6].Borders.Width = 0.4;
            row2.Cells[5].Borders.Width = 0.4;
            row2.Cells[6].Borders.Width = 0.4;

            row1.Cells[2].Format.Alignment = ParagraphAlignment.Right;
            row1.Cells[3].Format.Alignment = ParagraphAlignment.Left;
            row2.Cells[2].Format.Alignment = ParagraphAlignment.Right;
            row2.Cells[3].Format.Alignment = ParagraphAlignment.Left;
            row2.Cells[4].Format.Alignment = ParagraphAlignment.Left;

            if (sumVolume != 0)
            {
                row1.Cells[2].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) * pafData.PAF_BTM_DETAIL.PBD_ESTIMATED_FX));
                row2.Cells[2].AddParagraph(string.Format("{0:n0}", sumValueUSD / sumVolume));

                decimal wp = Math.Round(sumValueUSD / sumVolume);
                if (wp > pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp < pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("> Simplan, < Corp. Plan");
                }
                else if (wp < pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp > pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("< Simplan, > Corp. Plan");
                }
                else if (wp > pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp > pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("> Simplan, Corp. Plan");
                }
                else if (wp < pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp < pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("< Simplan, Corp. Plan");
                }
                else if (wp == pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp < pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("= Simplan, < Corp. Plan");
                }
                else if (wp == pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp > pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("= Simplan, > Corp. Plan");
                }
                else if (wp < pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp == pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("< Simplan, = Corp. Plan");
                }
                else if (wp > pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp == pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("> Simplan, = Corp. Plan");
                }
                else if (wp == pafData.PAF_BTM_DETAIL.PBD_SIM_PLAN && wp == pafData.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    row2.Cells[4].AddParagraph("= Simplan, Corp. Plan");
                }

                row2.Cells[5].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - (decimal)pafData.PAF_BTM_DETAIL.PBD_ARGUS_PRICE));
                row2.Cells[6].AddParagraph(string.Format("{0:n0}", Math.Round(sumValueUSD / sumVolume) - (decimal)pafData.PAF_BTM_DETAIL.PBD_AVG_HSFO));

            }

            row1.Cells[3].AddParagraph(" Baht/MT");
            row2.Cells[3].AddParagraph(" USD/MT");



            row1.Format.Font.Bold = true;
            row2.Format.Font.Bold = true;

            doc.LastSection.Add(table);
            doc.LastSection.AddParagraph("");
        }

        public static void genTable23(Document doc, PAF_DATA pafData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;

            Column column1 = table.AddColumn(Unit.FromCentimeter(4));
            Column column2 = table.AddColumn(Unit.FromCentimeter(16));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            column1.Format.Font.Bold = true;
            column1.Format.Alignment = ParagraphAlignment.Center;

            Paragraph p1 = row1.Cells[0].AddParagraph("For:");
            Paragraph p2 = row2.Cells[0].AddParagraph("Topic");



            if (pafData.MT_COMPANY != null)
            {
                Paragraph p3 = row1.Cells[1].AddParagraph(pafData.MT_COMPANY.MCO_SHORT_NAME + "");
            }
            Paragraph p4 = row2.Cells[1].AddParagraph(pafData.PDA_TOPIC + "");

            table.Rows[table.Rows.Count - 1].Borders.Bottom.Width = 0.2;
            doc.LastSection.Add(table);
        }




    }
}