﻿using ProjectCPAIEngine.DAL.DALCOMMON;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class PAFListHelper
    {
        public static string GetQuantity(string template, PAF_REVIEW_ITEMS elem, PAF_REVIEW_ITEMS_DETAIL ee)
        {
            string quantity = "";
            if (template == "CMLA_FORM_2")
            {
                try
                {
                    quantity = elem.PAF_REVIEW_ITEMS_DETAIL.First().PRD_OUR_PROPOSAL_QTY_FLOAT;
                }
                catch (Exception e)
                {

                }
                quantity = PAFHelper.ShowTolerance(quantity);
            }
            else if(template == "CMLA_FORM_1")
            {
                try
                {
                    quantity = elem.PAF_REVIEW_ITEMS_DETAIL.First().PRD_OUR_PROPOSAL_QTY_FLOAT;
                    quantity = PAFHelper.ShowTolerance(quantity);
                }
                catch (Exception e)
                {

                }
            }
            else
            {
                quantity = PAFHelper.GetMinMaxFormat(ee.PRD_OUR_PROPOSAL_QUANTITY_MIN, ee.PRD_OUR_PROPOSAL_QUANTITY_MAX);
            }

            quantity = quantity + " " + (elem.PRI_QUANTITY_UNIT ?? "");

            return quantity;
        }

        public static string GetBenchmark(PAF_BIDDING_ITEMS elem)
        {
            string benchmark = "";
            try
            {
                benchmark = elem.MKT_MST_MOPS_PRODUCT.M_MPR_PRDNAME;
            }
            catch (Exception e)
            {

            }
            return benchmark;
        }

        public static string GetPremiumDiscount(PAF_BIDDING_ITEMS elem, bool is_cmla = false)
        {
            string result = "";
            try
            {
                decimal lastround = 0;
                PAF_ROUND round = elem.PAF_ROUND.OrderByDescending(m => m.PRN_ROW_ID).FirstOrDefault();
                if (round != null)
                {
                    if (is_cmla && !string.IsNullOrEmpty(round.PRN_FLOAT))
                    {
                        //lastround = round.PRN_FLOAT;
                        result = round.PRN_FLOAT.Replace("|", " ");
                    }
                    else
                    {
                        lastround = (decimal)(round.PRN_FIXED ?? 0);
                        result = (String.Format("{0:n}", lastround) ?? "");
                    }
                }
            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static string GetFreight(PAF_BIDDING_ITEMS elem, bool is_import = false)
        {
            string result = "0";
            try
            {
                bool need_freight = false;
                decimal freight = 0;
                if (elem.MT_INCOTERMS != null && is_import)
                {
                    need_freight = elem.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "N";
                }
                else if (elem.MT_INCOTERMS != null && !is_import)
                {
                    need_freight = elem.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                }
                if (need_freight)
                {
                    freight = (decimal)elem.PBI_FREIGHT_PRICE;
                }
                else if (need_freight)
                {
                    freight = (decimal)elem.PBI_FREIGHT_PRICE;
                }
                result = (String.Format("{0:n}", freight) ?? "");
            }
            catch (Exception e)
            {

            }
            return result;
        }

        public static Nullable<Decimal> CalPrice(string template, PAF_BIDDING_ITEMS elem)
        {
            Decimal? convert_to_fob = null;
            #region price showing in awarded section
            if (template == "CMPS_INTER_SALE" || template == "CMPS_IMPORT")
            {
                bool need_freight = false;
                decimal freight = 0;
                decimal lastround = 0;
                PAF_ROUND round = elem.PAF_ROUND.OrderByDescending(m => m.PRN_ROW_ID).FirstOrDefault();
                if (round != null)
                {
                    lastround = (decimal)(round.PRN_FIXED ?? 0);
                }
                try
                {
                    if (elem.MT_INCOTERMS != null)
                    {
                        need_freight = elem.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                    }
                    if (!need_freight && template == "CMPS_IMPORT")
                    {
                        freight = (decimal)elem.PBI_FREIGHT_PRICE;
                        convert_to_fob = lastround + freight;
                    }
                    else if (need_freight && template == "CMPS_INTER_SALE")
                    {
                        freight = (decimal)elem.PBI_FREIGHT_PRICE;
                        convert_to_fob = lastround - freight;
                    }
                    else
                    {
                        convert_to_fob = lastround;
                    }
                }
                catch (Exception e)
                {

                }
            }
            else if (template != "CMLA_FORM_1" && template != "CMLA_FORM_2" && template != "CMLA_IMPORT")
            {
                convert_to_fob = elem.PBI_OFFER_PRICE;
            }
            
            #endregion
            return convert_to_fob;
        }
        public static string CalPriceString(string init_price, string template, PAF_BIDDING_ITEMS elem, string price_unit, out string signed)
        {
            signed = "";
            string result = "";
            Nullable<Decimal> convert_to_fob = CalPrice(template, elem);
            if (convert_to_fob != null)
            {
                if(convert_to_fob > 0)
                {
                    signed = "+";
                }
                result = (String.Format("{0:n}", convert_to_fob) ?? "") + " " + (price_unit ?? "");
            }
            else
            {
                result = init_price;
            }
            return result;
        }

        public static decimal GetLastSecondPrice(string template, List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS)
        {
            decimal last_price = 0;
            List<decimal> price_list = new List<decimal>();
            PAF_BIDDING_ITEMS =  PAF_BIDDING_ITEMS.Where(m => m.PBI_AWARDED_BIDDER != "Y").ToList();
            foreach(PAF_BIDDING_ITEMS elem in PAF_BIDDING_ITEMS)
            {
                PAF_ROUND round = elem.PAF_ROUND.OrderByDescending(m => m.PRN_ROW_ID).FirstOrDefault();
                Nullable<Decimal> res = CalPrice(template, elem);
                if(res != null)
                {
                    price_list.Add((decimal)res);
                }
            }

            if(template.Contains("IMPORT"))
            {
                last_price = price_list.OrderBy(m => m).FirstOrDefault();
            }
            else
            {
                last_price = price_list.OrderByDescending(m => m).FirstOrDefault();
            }

            return last_price;
        }
        public static string CalBenefitSecondBid(string template, PAF_BIDDING_ITEMS elem, string price_unit, List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS)
        {
            string result = "";
            decimal calculated = 0;
            decimal price = CalPrice(template, elem) ?? 0;
            decimal last_second_price = GetLastSecondPrice(template, PAF_BIDDING_ITEMS);
            calculated = (last_second_price - price);
            result = (String.Format("{0:n}", calculated) ?? "") + " " + (price_unit ?? "");
            PTXHelper.Log(elem.PAF_DATA.PDA_FORM_ID + " 2nd: " + last_second_price + "  1st: " + price);
            return result;
        }

        public static string CalBenefitMarketBid(string template, PAF_BIDDING_ITEMS elem, string price_unit)
        {
            string result = "";
            decimal market_price = 0;
            decimal price = CalPrice(template, elem) ?? 0;
            decimal calculated = 0;
            if (template == "CMPS_INTER_SALE" || template == "CMPS_IMPORT")
            {
                market_price = (elem.PBI_MARKET_PRICE ?? 0);
                calculated = price - market_price;
            }
            result = (String.Format("{0:n}", calculated) ?? "") + " " + (price_unit ?? "");
            return result;
        }
        
        public static List_PAFAwarded GetAwarded(PAF_DATA data)
        {
            List_PAFAwarded result = new List_PAFAwarded();
            List<PAFAwarded> wrap_record = new List<PAFAwarded>();
            List<string> bidding_items = data.PAF_PAYMENT_ITEMS.Select(m => m.PPI_FK_CUSTOMER).ToList();
            List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS = data.PAF_BIDDING_ITEMS.Where(m => bidding_items.Contains(m.PBI_FK_CUSTOMER)).OrderBy(m => m.PBI_ROW_ID).ToList();
            if (data.PDA_TEMPLATE == "CMPS_INTER_SALE" || data.PDA_TEMPLATE == "CMPS_IMPORT")
            {
                data.PDA_INCLUDED_BIDDING_TABLE = "Y";
            }
            if (data.PDA_INCLUDED_BIDDING_TABLE != "Y")
            {
                return result;
            }

            foreach (PAF_BIDDING_ITEMS elem in PAF_BIDDING_ITEMS)
            {
                PAFAwarded record = new PAFAwarded();
                if (elem.PBI_AWARDED_BIDDER != "Y")
                {
                    continue;
                }
                string customer = PAF_MASTER_DAL.GetCustomerName(elem.PBI_FK_CUSTOMER);
                string supplier = PAF_MASTER_DAL.GetVendorName(elem.PBI_FK_VENDOR);
                string product = PAF_MASTER_DAL.GetMaterailName(elem);
                string quantity = PAFHelper.GetMinMaxFormat(elem.PBI_QUANTITY_MIN, elem.PBI_QUANTITY_MAX);
                if (data.PDA_TEMPLATE == "CMPS_DOM")
                {

                }

                if (data.PDA_TEMPLATE == "CMLA_IMPORT" || data.PDA_TEMPLATE == "CMLA_FORM_1" || data.PDA_TEMPLATE == "CMLA_FORM_2")
                {
                    quantity = PAFHelper.ShowTolerance(elem.PBI_QUANTITY_FLOAT);
                }

                record.customer = customer;
                record.suplier = supplier;
                record.bidder = customer;
                if ((data.PDA_TEMPLATE ?? "").ToUpper().Contains("IMPORT"))
                {
                    record.bidder = supplier;
                }
                record.product = product;
                record.quantity = quantity + " " + (elem.PBI_QUANTITY_UNIT ?? "");
                record.incoterm = elem.PBI_INCOTERM ?? "";
                record.awarded_incoterm = elem.PBI_INCOTERM ?? "";
                record.packaging = elem.PBI_FK_PACKAGING ?? "";
                record.destination = elem.MT_COUNTRY != null ? elem.MT_COUNTRY.MCT_LANDX : "";
                record.benchmark = GetBenchmark(elem);
                record.freight = GetFreight(elem, data.PDA_TEMPLATE == "CMPS_IMPORT") + " " + (data.PDA_PRICE_UNIT ?? "");
                record.premium_discount = GetPremiumDiscount(elem, data.PDA_TEMPLATE.Contains("CMLA")) + " " + (data.PDA_PRICE_UNIT ?? "");
                record.price = record.premium_discount;

                string signed = "";
                record.price = CalPriceString(record.price, data.PDA_TEMPLATE, elem, data.PDA_PRICE_UNIT, out signed);
                if (!String.IsNullOrEmpty(record.benchmark))
                {
                    record.price = record.benchmark + signed + record.price;
                }
                record.benefit_2nd = CalBenefitSecondBid(data.PDA_TEMPLATE, elem, data.PDA_PRICE_UNIT, PAF_BIDDING_ITEMS);
                record.benefit_market = CalBenefitMarketBid(data.PDA_TEMPLATE, elem, data.PDA_PRICE_UNIT);
                wrap_record.Add(record);
            }
            result.awarded = wrap_record;
            return result;
        }
        public static List_PAFCustomerReview GetCustomerReview(PAF_DATA data)
        {
            List_PAFCustomerReview result = new List_PAFCustomerReview();
            List<PAFCustomerReview> wrap_record = new List<PAFCustomerReview>();
            List<string> rows = new List<string>();
            List<PAF_REVIEW_ITEMS> PAF_REVIEW_ITEMS = data.PAF_REVIEW_ITEMS.OrderBy(m => m.PRI_ROW_ID).ToList();

            if (data.PDA_INCLUDED_REVIEW_TABLE != "Y" || data.PDA_TEMPLATE == "CMLA_IMPORT")
            {
                return result;
            }

            if(data.PDA_FORM_ID == "PAF-1712-0791")
            {
                string notes = "";

            }
            foreach (PAF_REVIEW_ITEMS elem in PAF_REVIEW_ITEMS)
            {
                string note = "";
                List<PAF_REVIEW_ITEMS_DETAIL> PAF_REVIEW_ITEMS_DETAIL = elem.PAF_REVIEW_ITEMS_DETAIL.OrderBy(m => m.PRD_ROW_ID).ToList();
                if (PAF_REVIEW_ITEMS_DETAIL.Count > 0)
                {
                    note = PAF_REVIEW_ITEMS_DETAIL[0].PRD_NOTE;
                }
                string customer = PAF_MASTER_DAL.GetCustomerName(elem.PRI_FK_CUSTOMER);
                //string supplier = PAF_MASTER_DAL.GetVendorName(elem);
                string product = PAF_MASTER_DAL.GetMaterailName(elem);
                string price = "";
                int tier = 1;
                int count = 1;
                foreach (PAF_REVIEW_ITEMS_DETAIL ee in PAF_REVIEW_ITEMS_DETAIL)
                {
                    PAFCustomerReview record = new PAFCustomerReview();
                    string quantity = GetQuantity(data.PDA_TEMPLATE, elem, ee);

                    //if (String.IsNullOrEmpty(quantity) || String.IsNullOrEmpty(ee.PRD_OUR_PROPOSAL_PRICE_FLOAT))
                    if (String.IsNullOrEmpty(ee.PRD_OUR_PROPOSAL_PRICE_FLOAT) && ee.PRD_OUR_PROPOSAL_PRICE_FIXED == null)
                    {
                        tier++;
                        count++;
                        continue;
                    }
                    if (data.PDA_TEMPLATE != "CMLA_FORM_1" && data.PDA_TEMPLATE != "CMLA_FORM_2")
                    {
                        price = ee.PRD_OUR_PROPOSAL_PRICE_FLOAT;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ee.PRD_OUR_PROPOSAL_PRICE_FLOAT))
                        {
                            price = ee.PRD_OUR_PROPOSAL_PRICE_FLOAT.Replace("|", " ");
                        }
                        else if ( ee.PRD_OUR_PROPOSAL_PRICE_FIXED != null)
                        {
                            price = String.Format("{0:n}", ee.PRD_OUR_PROPOSAL_PRICE_FIXED);
                        }
                    }
                    record.tier = tier;
                    record.quantity = quantity;
                    record.customer = customer;
                    if (data.PDA_TEMPLATE == "CMLA_FORM_1")
                    {
                        record.product = product;
                        //record.product = product + ": " + quantity;
                    }
                    else
                    {
                        record.product = product;
                    }
                    record.quantity = quantity;
                    record.price = price + " " + data.PDA_PRICE_UNIT;
                    record.note = (String.IsNullOrEmpty(note) ? " - " : note);
                    wrap_record.Add(record);
                    count++;
                    tier++;
                }
            }
            result.customer_reviews = wrap_record;
            return result;
        }


        public static List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> GetGroupItemsList(List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> source)
        {
            List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> result = new List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>();
            Dictionary<string, List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>> group_result = new Dictionary<string, List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>>();

            foreach (PAF_BTM_ALLOCATE_ITEMS_DETAIL b in source)
            {
                if (b.PAF_MT_BTM_TIER == null)
                {
                    continue;
                }
                try
                {
                    group_result[b.PAF_MT_BTM_TIER.PMT_GROUP].Add(b);
                }
                catch (Exception e)
                {
                    if(b.PAF_MT_BTM_TIER.PMT_GROUP == null)
                    {
                        continue;
                    }
                    group_result[b.PAF_MT_BTM_TIER.PMT_GROUP] = new List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>();
                    group_result[b.PAF_MT_BTM_TIER.PMT_GROUP].Add(b);
                }
            }

            foreach (KeyValuePair<string, List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>> entry in group_result)
            {
                PAF_BTM_ALLOCATE_ITEMS_DETAIL a = new PAF_BTM_ALLOCATE_ITEMS_DETAIL();
                a.PAF_MT_BTM_TIER = entry.Value.FirstOrDefault().PAF_MT_BTM_TIER;
                a.PAD_TIER_VOLUME = entry.Value.Sum(m => m.PAD_TIER_VOLUME);
                if (a.PAD_TIER_VOLUME == 0)
                {
                    a.PAD_USD_PER_UNIT = 0;
                }
                else
                {
                    a.PAD_USD_PER_UNIT = (entry.Value.Sum(m => (m.PAD_USD_PER_UNIT * m.PAD_TIER_VOLUME))) / a.PAD_TIER_VOLUME;
                }

                a.PAD_USD_PER_UNIT = Math.Round((decimal)a.PAD_USD_PER_UNIT);
                result.Add(a);
            }

            return result;
        }

        public static BitumenData PrepareBitumenData(PAF_DATA data)
        {
            BitumenData result = new BitumenData();
            result.BitumenDataItems = new List<BitumenDataItems>();

            List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> pbais = new List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>();
            PAF_BTM_GRADE_ITEMS a = data.PAF_BTM_GRADE_ITEMS.FirstOrDefault();
            if (a == null)
            {
                return null;
            }
            foreach (PAF_BTM_ALLOCATE_ITEMS b in a.PAF_BTM_ALLOCATE_ITEMS.OrderBy(m => m.PBA_ROW_ID))
            {
                string customer_name = "";
                try
                {
                    MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(b.PBA_FK_CUSTOMER);
                    customer_name = customer.MCD_NAME_1 + (customer.MCD_NAME_2 ?? "");
                }
                catch (Exception e)
                {

                }

                List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> cs = GetGroupItemsList(b.PAF_BTM_ALLOCATE_ITEMS_DETAIL.OrderBy(m => m.PAD_ROW_ID).ToList());
                pbais.AddRange(cs);
                foreach (PAF_BTM_ALLOCATE_ITEMS_DETAIL c in cs)
                {
                    BitumenDataItems bdi = new BitumenDataItems();
                    string tier_name = "";
                    decimal fx_rate = 0.00000001M;
                    try
                    {
                        tier_name = c.PAF_MT_BTM_TIER.PMT_DETAIL;
                        fx_rate = (decimal)data.PAF_BTM_DETAIL.PBD_ESTIMATED_FX;
                    }
                    catch (Exception e)
                    {

                    }

                    decimal selling_argus = Math.Round((decimal)(c.PAD_USD_PER_UNIT), 0) - (decimal)data.PAF_BTM_DETAIL.PBD_ARGUS_PRICE;
                    decimal selling_hsfo = Math.Round((decimal)(c.PAD_USD_PER_UNIT), 0) - Math.Round((decimal)data.PAF_BTM_DETAIL.PBD_AVG_HSFO, 2);
                    selling_argus = Math.Round(selling_argus, 0);
                    selling_hsfo = Math.Round(selling_hsfo, 0);

                    bdi.customer = customer_name;
                    bdi.tier = tier_name;
                    bdi.quantity = (decimal)c.PAD_TIER_VOLUME;
                    bdi.price = (decimal)c.PAD_USD_PER_UNIT;
                    bdi.selling_price_argus = selling_argus;
                    bdi.selling_price_hsfo = selling_hsfo;
                    result.BitumenDataItems.Add(bdi);
                    //result += "Customer: " + customer_name + "<br/>";
                    //result += "Tier: " + tier_name + "<br/>";
                    //result += "Quantity: " + (String.Format("{0:n0}", c.PAD_TIER_VOLUME) ?? "") + " MT <br/>";
                    //result += "Price: " + (String.Format("{0:n0}", c.PAD_USD_PER_UNIT) ?? "") + " USD/MT<br/>";
                    //result += "Selling Price - Argus: " + String.Format("{0:n0}", selling_argus) + " USD/MT<br/>";
                    //result += "Selling Price - HSFO: " + String.Format("{0:n0}", selling_hsfo) + " USD/MT<br/>";
                    //result += "<br/>";
                }
            }


            decimal sum_volume = (decimal)pbais.Sum(m => m.PAD_TIER_VOLUME);
            decimal fx_rate_s = (decimal)data.PAF_BTM_DETAIL.PBD_ESTIMATED_FX;
            decimal weigted_price = 0;
            try
            {
                weigted_price = Math.Round(Math.Round((decimal)(pbais.Sum(m => m.PAD_USD_PER_UNIT * m.PAD_TIER_VOLUME) / sum_volume)));
            } catch(Exception e)
            {

            }
            string compare_plan = "";
            if (weigted_price < data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price < data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " < Simplan, Corp. Plan";
            }
            else if (weigted_price > data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price > data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " > Simplan, Corp. Plan";
            }
            else if (weigted_price < data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price > data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " < Simplan, > Corp. Plan";
            }
            else if (weigted_price > data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price < data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " > Simplan, < Corp. Plan";
            }
            else if (weigted_price > data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price == data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " > Simplan, = Corp. Plan";
            }
            else if (weigted_price == data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price > data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " = Simplan, > Corp. Plan";
            }
            else if (weigted_price == data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price < data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " = Simplan, < Corp. Plan";
            }
            else if (weigted_price == data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price == data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
            {
                compare_plan = " = Simplan, Corp. Plan";
            }

            result.price = weigted_price;
            result.compare_plan = compare_plan;
            result.weighted_price_diff_argus = (decimal) (weigted_price - data.PAF_BTM_DETAIL.PBD_ARGUS_PRICE ?? 0);
            result.weighted_price_diff_hsfo = (decimal) (weigted_price - data.PAF_BTM_DETAIL.PBD_AVG_HSFO ?? 0);
            return result;
        }


        public static string GetWeightedPrice(PAF_DATA data)
        {
            BitumenData bd = PrepareBitumenData(data);
            string result = "";
            result = (String.Format("{0:n0}", bd.price) ?? "") + " USD/MT " + bd.compare_plan;
            return result;
        }

        public static string GetWeightedDiffArgus(PAF_DATA data)
        {
            BitumenData bd = PrepareBitumenData(data);
            string result = "";
            result = (String.Format("{0:n0}", bd.weighted_price_diff_argus) ?? "") + " USD/MT";
            return result;
        }

        public static string GetWeightedPriceDiffHSFO(PAF_DATA data)
        {
            BitumenData bd = PrepareBitumenData(data);
            string result = "";
            result = (String.Format("{0:n0}", bd.weighted_price_diff_hsfo) ?? "") + " USD/MT";
            return result;
        }

        public static List_PAFBitumenSummary GetBitumenSummary(PAF_DATA data)
        {
            BitumenData bd = PrepareBitumenData(data);
            List_PAFBitumenSummary result = new List_PAFBitumenSummary();
            result.bitumen_summary = new List<PAFBitumenSummary>();
            foreach(BitumenDataItems elem in bd.BitumenDataItems)
            {
                PAFBitumenSummary e = new PAFBitumenSummary();
                e.customer = elem.customer;
                e.tier = elem.tier;
                e.quantity = (String.Format("{0:n0}", elem.quantity) ?? "") + " MT";
                e.price = (String.Format("{0:n0}", elem.price) ?? "") + " USD/MT";
                e.selling_price_diff_argus = (String.Format("{0:n0}", elem.selling_price_argus) ?? "") + " USD/MT";
                e.selling_price_diff_hsfo = (String.Format("{0:n0}", elem.selling_price_hsfo) ?? "") + " USD/MT";

                result.bitumen_summary.Add(e);
            }

            return result;
        }

        public static string GetBriefMarket(PAF_DATA data)
        {
            string result = "";
            if (data.PDA_BRIEF != null)
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(data.PDA_BRIEF);
                result = Convert.ToBase64String(plainTextBytes);
            }
            return result;
        }
    }


    public class BitumenData
    {
        public decimal price { get; set; }
        public string compare_plan { get; set; }
        public decimal weighted_price_diff_argus { get; set; }
        public decimal weighted_price_diff_hsfo { get; set; }
        public List<BitumenDataItems> BitumenDataItems { get; set; }
    }

    public class BitumenDataItems
    {
        public string customer { get; set; }
        public string tier { get; set; }
        public decimal quantity { get; set; }
        public decimal price { get; set; }
        public decimal selling_price_hsfo { get; set; }
        public decimal selling_price_argus { get; set; }
    }
}