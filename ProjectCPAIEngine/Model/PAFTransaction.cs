﻿using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class PAFTransaction
    {
        [XmlElement(ElementName = "function_id")]
        public string function_id { get; set; }
        [XmlElement(ElementName = "function_desc")]
        public string function_desc { get; set; }
        [XmlElement(ElementName = "result_namespace")]
        public string result_namespace { get; set; }
        [XmlElement(ElementName = "result_status")]
        public string result_status { get; set; }
        [XmlElement(ElementName = "result_code")]
        public string result_code { get; set; }
        [XmlElement(ElementName = "result_desc")]
        public string result_desc { get; set; }
        [XmlElement(ElementName = "req_transaction_id")]
        public string req_transaction_id { get; set; }
        [XmlElement(ElementName = "transaction_id")]
        public string transaction_id { get; set; }
        [XmlElement(ElementName = "response_message")]
        public string response_message { get; set; }
        [XmlElement(ElementName = "system")]
        public string system { get; set; }
        [XmlElement(ElementName = "type")]
        public string type { get; set; }
        [XmlElement(ElementName = "action")]
        public string action { get; set; }
        [XmlElement(ElementName = "status")]
        public string status { get; set; }
        [XmlElement(ElementName = "status_description")]
        public string status_description { get; set; }
        [XmlElement(ElementName = "status_tracking")]
        public string status_tracking { get; set; }
        [XmlElement(ElementName = "date_purchase")]
        public string date_purchase { get; set; }
        [XmlElement(ElementName = "vessel_id")]
        public string vessel_id { get; set; }
        [XmlElement(ElementName = "vessel")]
        public string vessel { get; set; }
        [XmlElement(ElementName = "supplying_location")]
        public string supplying_location { get; set; }
        [XmlElement(ElementName = "purchase_no")]
        public string purchase_no { get; set; }
        [XmlElement(ElementName = "products")]
        public string products { get; set; }
        [XmlElement(ElementName = "volumes")]
        public string volumes { get; set; }
        [XmlElement(ElementName = "create_by")]
        public string create_by { get; set; }
        [XmlElement(ElementName = "update_date")]
        public string update_date { get; set; }
        [XmlElement(ElementName = "supplier_id")]
        public string supplier_id { get; set; }
        [XmlElement(ElementName = "supplier")]
        public string supplier { get; set; }
        [XmlElement(ElementName = "supplier_color")]
        public string supplier_color { get; set; }
        [XmlElement(ElementName = "cust_id")]
        public string cust_id { get; set; }
        [XmlElement(ElementName = "cust_name")]
        public string cust_name { get; set; }
        [XmlElement(ElementName = "cust_short")]
        public string cust_short { get; set; }
        [XmlElement(ElementName = "flat_rate")]
        public string flat_rate { get; set; }
        [XmlElement(ElementName = "laycan_from")]
        public string laycan_from { get; set; }
        [XmlElement(ElementName = "laycan_to")]
        public string laycan_to { get; set; }
        [XmlElement(ElementName = "owner")]
        public string owner { get; set; }
        [XmlElement(ElementName = "min_load")]
        public string min_load { get; set; }
        [XmlElement(ElementName = "trip_no")]
        public string trip_no { get; set; }
        [XmlElement(ElementName = "reason")]
        public string reason { get; set; }
        [XmlElement(ElementName = "remark")]
        public string remark { get; set; }
        [XmlElement(ElementName = "date_start")]
        public string date_start { get; set; }
        [XmlElement(ElementName = "date_end")]
        public string date_end { get; set; }
        [XmlElement(ElementName = "others_cost_surveyor")]
        public string others_cost_surveyor { get; set; }
        [XmlElement(ElementName = "others_cost_barge")]
        public string others_cost_barge { get; set; }
        [XmlElement(ElementName = "final_price")]
        public string final_price { get; set; }
        [XmlElement(ElementName = "total_price")]
        public string total_price { get; set; }
        [XmlElement(ElementName = "charterer")]
        public string charterer { get; set; }

        [XmlElement(ElementName = "charterer_name")]
        public string charterer_name { get; set; }

        [XmlElement(ElementName = "cargo")]
        public string cargo { get; set; }

        [XmlElement(ElementName = "cargo_name")]
        public string cargo_name { get; set; }

        [XmlElement(ElementName = "broker")]
        public string broker { get; set; }

        [XmlElement(ElementName = "broker_name")]
        public string broker_name { get; set; }

        [XmlElement(ElementName = "load_port")]
        public string load_port { get; set; }

        [XmlElement(ElementName = "load_port_name")]
        public string load_port_name { get; set; }

        [XmlElement(ElementName = "discharge_port")]
        public string discharge_port { get; set; }

        [XmlElement(ElementName = "discharge_port_name")]
        public string discharge_port_name { get; set; }

        [XmlElement(ElementName = "laytime")]
        public string laytime { get; set; }

        [XmlElement(ElementName = "date_fixture")]
        public string date_fixture { get; set; }
        [XmlElement(ElementName = "target_days")]
        public string target_days { get; set; }
        [XmlElement(ElementName = "avg_ws")]
        public string avg_ws { get; set; }
        [XmlElement(ElementName = "ws_saving ")]
        public string ws_saving { get; set; }
        [XmlElement(ElementName = "saving_in")]
        public string saving_in { get; set; }
        [XmlElement(ElementName = "top_fixture_value")]
        public string top_fixture_value { get; set; }
        [XmlElement(ElementName = "laycan_load_from")]
        public string laycan_load_from { get; set; }
        [XmlElement(ElementName = "laycan_load_to")]
        public string laycan_load_to { get; set; }
        [XmlElement(ElementName = "charter_in")]
        public string charter_in { get; set; }
        [XmlElement(ElementName = "broker_id")]
        public string broker_id { get; set; }
        [XmlElement(ElementName = "dem")]
        public string dem { get; set; }
        [XmlElement(ElementName = "extra_cost")]
        public string extra_cost { get; set; }
        [XmlElement(ElementName = "date_from")]
        public string date_from { get; set; }
        [XmlElement(ElementName = "date_to")]
        public string date_to { get; set; }
        [XmlElement(ElementName = "benefit_day")]
        public string benefit_day { get; set; }
        [XmlElement(ElementName = "benefit_voy")]
        public string benefit_voy { get; set; }
        [XmlElement(ElementName = "tce_day")]
        public string tce_day { get; set; }
        [XmlElement(ElementName = "month")]
        public string month { get; set; }
        [XmlElement(ElementName = "td")]
        public string td { get; set; }
        [XmlElement(ElementName = "tc_rate")]
        public string tc_rate { get; set; }

        [XmlElement(ElementName = "delivery_date_from")]
        public string delivery_date_from { get; set; }
        [XmlElement(ElementName = "delivery_date_to")]
        public string delivery_date_to { get; set; }
        [XmlElement(ElementName = "unit_price_type")]
        public string unit_price_type { get; set; }
        [XmlElement(ElementName = "unit_price_value")]
        public string unit_price_value { get; set; }
        [XmlElement(ElementName = "unit_price_order")]
        public string unit_price_order { get; set; }
        [XmlElement(ElementName = "currency_symbol")]
        public string currency_symbol { get; set; }

        [XmlElement(ElementName = "exten_cost")]
        public string exten_cost { get; set; }
        [XmlElement(ElementName = "est_freight")]
        public string est_freight { get; set; }

        [XmlElement(ElementName = "feed_stock")]
        public string feed_stock { get; set; }

        [XmlElement(ElementName = "margin")]
        public string margin { get; set; }

        [XmlElement(ElementName = "product_name")]
        public string product_name { get; set; }
        [XmlElement(ElementName = "supplier_name")]
        public string supplier_name { get; set; }

        [XmlElement(ElementName = "incoterm")]
        public string incoterm { get; set; }
        [XmlElement(ElementName = "formula_p")]
        public string formula_p { get; set; }
        [XmlElement(ElementName = "purchase_p")]
        public string purchase_p { get; set; }
        [XmlElement(ElementName = "market_p")]
        public string market_p { get; set; }
        [XmlElement(ElementName = "lp_p")]
        public string lp_p { get; set; }
        [XmlElement(ElementName = "discharging_period")]
        public string discharging_period { get; set; }
        [XmlElement(ElementName = "loading_period")]
        public string loading_period { get; set; }
        [XmlElement(ElementName = "loading_prd")]
        public string loading_prd { get; set; }
        [XmlElement(ElementName = "maximum_buying_price")]
        public string maximum_buying_price { get; set; }
        [XmlElement(ElementName = "lp_result")]
        public string lp_result { get; set; }

        [XmlElement(ElementName = "tc")]
        public string tc { get; set; }

        [XmlElement(ElementName = "vessel_activity")]
        public string vessel_activity { get; set; }

        [XmlElement(ElementName = "assay_ref")]
        public string assay_ref { get; set; }

        [XmlElement(ElementName = "assay_date")]
        public string assay_date { get; set; }

        [XmlElement(ElementName = "origin")]
        public string origin { get; set; }

        [XmlElement(ElementName = "categories")]
        public string categories { get; set; }

        [XmlElement(ElementName = "kerogen")]
        public string kerogen { get; set; }

        [XmlElement(ElementName = "characteristic")]
        public string characteristic { get; set; }

        [XmlElement(ElementName = "maturity")]
        public string maturity { get; set; }

        [XmlElement(ElementName = "density")]
        public string density { get; set; }

        [XmlElement(ElementName = "total_acid_number")]
        public string total_acid_number { get; set; }

        [XmlElement(ElementName = "total_sulphur")]
        public string total_sulphur { get; set; }

        [XmlElement(ElementName = "final_cam_file")]
        public string final_cam_file { get; set; }

        [XmlElement(ElementName = "draft_cam_file")]
        public string draft_cam_file { get; set; }

        [XmlElement(ElementName = "assay_file")]
        public string assay_file { get; set; }

        [XmlElement(ElementName = "assay_from")]
        public string assay_from { get; set; }

        [XmlElement(ElementName = "support_doc_file")]
        public string support_doc_file { get; set; }
        [XmlElement(ElementName = "draft_cam_status")]
        public string draft_cam_status { get; set; }
        [XmlElement(ElementName = "final_cam_status")]
        public string final_cam_status { get; set; }
        [XmlElement(ElementName = "expert_units")]
        public string expert_units { get; set; }

        [XmlElement(ElementName = "requester_name")]
        public string requester_name { get; set; }
        [XmlElement(ElementName = "note")]
        public string note { get; set; }
        [XmlElement(ElementName = "created_by")]
        public string created_by { get; set; }
        [XmlElement(ElementName = "created_date")]
        public string created_date { get; set; }
        [XmlElement(ElementName = "updated_by")]
        public string updated_by { get; set; }
        [XmlElement(ElementName = "updated_date")]
        public string updated_date { get; set; }
        [XmlElement(ElementName = "experts_support_doc_file")]
        public string experts_support_doc_file { get; set; }

        [XmlElement(ElementName = "deal_date")]
        public string deal_date { get; set; }
        [XmlElement(ElementName = "deal_no")]
        public string deal_no { get; set; }
        [XmlElement(ElementName = "pre_deal_no")]
        public string pre_deal_no { get; set; }
        [XmlElement(ElementName = "ticket_no")]
        public string ticket_no { get; set; }
        [XmlElement(ElementName = "contact_no")]
        public string contact_no { get; set; }
        [XmlElement(ElementName = "ticket_date")]
        public string ticket_date { get; set; }
        [XmlElement(ElementName = "deal_type")]
        public string deal_type { get; set; }
        [XmlElement(ElementName = "hedg_type")]
        public string hedg_type { get; set; }
        [XmlElement(ElementName = "trade_for")]
        public string trade_for { get; set; }
        [XmlElement(ElementName = "tool")]
        public string tool { get; set; }
        [XmlElement(ElementName = "counter_party")]
        public string counter_party { get; set; }
        [XmlElement(ElementName = "underlying")]
        public string underlying { get; set; }
        [XmlElement(ElementName = "underlying_vs")]
        public string underlying_vs { get; set; }
        [XmlElement(ElementName = "underlying_name")]
        public string underlying_name { get; set; }
        [XmlElement(ElementName = "seller")]
        public string seller { get; set; }
        [XmlElement(ElementName = "buyer")]
        public string buyer { get; set; }
        [XmlElement(ElementName = "price")]
        public string price { get; set; }
        [XmlElement(ElementName = "volume_month")]
        public string volume_month { get; set; }
        [XmlElement(ElementName = "template_name")]
        public string template_name { get; set; }
        [XmlElement(ElementName = "volume_kbbl_min")]
        public string volume_kbbl_min { get; set; }
        [XmlElement(ElementName = "volume_kbbl_max")]
        public string volume_kbbl_max { get; set; }
        [XmlElement(ElementName = "volume_kt_min")]
        public string volume_kt_min { get; set; }
        [XmlElement(ElementName = "volume_kt_max")]
        public string volume_kt_max { get; set; }
        [XmlElement(ElementName = "user_group")]
        public string user_group { get; set; }
        [XmlElement(ElementName = "user_group_delegate")]
        public string user_group_delegate { get; set; }
        [XmlElement(ElementName = "ws")]
        public string ws { get; set; }

        [XmlElement(ElementName = "route")]
        public string route { get; set; }

        [XmlElement(ElementName = "no_total_ex")]
        public string no_total_ex { get; set; }
        [XmlElement(ElementName = "total_ex")]
        public string total_ex { get; set; }
        [XmlElement(ElementName = "net_benefit")]
        public string net_benefit { get; set; }
        [XmlElement(ElementName = "tpc_month")]
        public string tpc_month { get; set; }
        [XmlElement(ElementName = "tpc_year")]
        public string tpc_year { get; set; }
        [XmlElement(ElementName = "sc_status")]
        public string sc_status { get; set; }
        [XmlElement(ElementName = "tn_status")]
        public string tn_status { get; set; }

    }
}