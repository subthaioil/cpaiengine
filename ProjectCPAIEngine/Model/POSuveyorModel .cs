﻿using com.pttict.sap.Interface.sap.posurveyor.create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Model
{
    public class POSuveyorModel
    {
        public List<BAPIMEPOHEADER> mPOHEADER { get; set; }//POHEADER
        public List<BAPIMEPOACCOUNT> mPOACCOUNT { get; set; }//POACCOUNT
        public List<BAPIMEPOCOND> mPOCOND { get; set; }//POCOND
        public List<BAPIMEPOITEM> mPOITEM { get; set; }//POITEM
        public List<BAPIMEPOSCHEDULE> mPOSCHEDULE { get; set; }//POSCHEDULE
        public string PURCHASEORDER { get; set; }//PO


        public class BAPIMEPOHEADER //POHEADER
        {
            public string PO_NUMBER { get; set; }
            public string COMP_CODE { get; set; }
            public string Mflag { get; set; } //Mark Flag for del
            public string DOC_TYPE { get; set; }
            public string DELETE_IND { get; set; }
            public string VENDOR { get; set; }
            public string PMNTTRMS { get; set; }
            public string PURCH_ORG { get; set; }
            public string PUR_GROUP { get; set; }
        }

        public class BAPIMEPOACCOUNT//POACCOUNT
        {
            public string PO_ITEM { get; set; }
            public string SERIAL_NO { get; set; }
            public string DELETE_IND { get; set; }
            public decimal QUANTITY { get; set; }
            public string GL_ACCOUNT { get; set; }
            public string COSTCENTER { get; set; }
            public string ORDERID { get; set; }
            public string CO_AREA { get; set; }
           
        }

        public class BAPIMEPOCOND//POCOND
        {
            public string ITM_NUMBER { get; set; } //PO_ITEM
            public string COND_ST_NO { get; set; }
            public decimal COND_VALUE { get; set; }
            public string CURRENCY { get; set; }
            public string CONDCOUNT { get; set; }
            public string CHANGE_ID { get; set; }
        }


        public class BAPIMEPOITEM //POITEM
        {
            public string PO_ITEM { get; set; }
            public string DELETE_IND { get; set; }
            public string SHORT_TEXT { get; set; }
            public string PLANT { get; set; }
            public string TRACKINGNO { get; set; }
            public string MATL_GROUP { get; set; }
            public decimal QUANTITY { get; set; }
            public string PO_UNIT { get; set; }
            public decimal NET_PRICE { get; set; }
            public string ACCTASSCAT { get; set; }
            public string RET_ITEM { get; set; }
        }

        public class BAPIMEPOSCHEDULE //POSCHEDULE
        {
            public string PO_ITEM { get; set; }
            public string SCHED_LINE { get; set; }
            public string DEL_DATCAT_EXT { get; set; }
            public string DELIVERY_DATE { get; set; }
            public decimal QUANTITY { get; set; }

        }

    }
}
