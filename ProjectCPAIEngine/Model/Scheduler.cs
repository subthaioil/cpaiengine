﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using ProjectCPAIEngine.Flow.Model;

namespace ProjectCPAIEngine.Model
{
    #region --- SCHEDULE CMMT ---

    public class SchDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string schedule_type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string schedule_type_detail { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vendor { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vendor_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vendor_color { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string freight { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_start { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_end { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laycan_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sailing_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sailing_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string laytime { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string note { get; set; } = "";
        public bool tentative { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string detail { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string color { get; set; } = "";
       

        public List<SchLoadPorts> sch_load_ports { get; set; }
        public List<SchDisPorts> sch_dis_ports { get; set; }
        public List<SchCargo> sch_cargo { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string updateDate { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string UpdateBy { get; set; } = "";
    }

    public class SchLoadPorts
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
    }

    public class SchDisPorts
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
    }

    public class SchCargo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cargo { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string qty { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tolerance { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string others { get; set; } = "";
    }

    public class SchActivity
    {
        public string activity { get; set; }
        public string activity_name { get; set; }
        public string activity_color { get; set; }
        public string order { get; set; }
        public string date { get; set; }
        public string @ref { get; set; }
        public string start_date { get; set; }
        public string other_activity { get; set; }
        public string act_note { get; set; }
        public string explanationAttach { get; set; }
    }

    public class SchFreight
    {
        public string row_id { get; set; }
        public string doc_no { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string req_trans_id { get; set; }
    }

    public class SchActivityList
    {
        public List<SchActivity> sch_activitys { get; set; }
    }



    public class SchRootObject
    {
        public SchDetail sch_detail { get; set; }
        public List<SchActivity> sch_activitys { get; set; }

    }




    public class SchAttact
    {
        public string row_id { get; set; }
        public string activity { get; set; }
        public string type { get; set; }
        public string info { get; set; }
        public string path { get; set; }
    }

    #region-------------Schedule Transaction---------------------
    [XmlRoot(ElementName = "transaction")]
    public class ScheduleTransaction
    {
        [XmlAttribute(AttributeName = "function_id")]
        public string Function_id { get; set; }
        [XmlAttribute(AttributeName = "function_desc")]
        public string Function_desc { get; set; }
        [XmlAttribute(AttributeName = "result_namespace")]
        public string Result_namespace { get; set; }
        [XmlAttribute(AttributeName = "result_status")]
        public string Result_status { get; set; }
        [XmlAttribute(AttributeName = "result_code")]
        public string Result_code { get; set; }
        [XmlAttribute(AttributeName = "result_desc")]
        public string Result_desc { get; set; }
        [XmlAttribute(AttributeName = "req_transaction_id")]
        public string Req_transaction_id { get; set; }
        [XmlAttribute(AttributeName = "transaction_id")]
        public string Transaction_id { get; set; }
        [XmlAttribute(AttributeName = "response_message")]
        public string Response_message { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "action")]
        public string Action { get; set; }

        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "date_start")]
        public string Date_start { get; set; }
        [XmlAttribute(AttributeName = "date_end")]
        public string Date_end { get; set; }
        [XmlAttribute(AttributeName = "laycan_from")]
        public string Laycan_from { get; set; }
        [XmlAttribute(AttributeName = "laycan_to")]
        public string Laycan_to { get; set; }
        [XmlAttribute(AttributeName = "vessel_id")]
        public string Vessel_id { get; set; }
        [XmlAttribute(AttributeName = "vessel")]
        public string Vessel { get; set; }
        [XmlAttribute(AttributeName = "purchase_no")]
        public string Purchase_no { get; set; }
        [XmlAttribute(AttributeName = "remark")]
        public string Remark { get; set; }
        [XmlAttribute(AttributeName = "create_by")]
        public string Create_by { get; set; }
        [XmlAttribute(AttributeName = "supplier_id")]
        public string Supplier_id { get; set; }
        [XmlAttribute(AttributeName = "supplier")]
        public string Supplier { get; set; }
        [XmlAttribute(AttributeName = "supplier_color")]
        public string Supplier_color { get; set; }
        [XmlAttribute(AttributeName = "reason")]
        public string Reason { get; set; }
        [XmlAttribute(AttributeName = "tc")]
        public string TC { get; set; }

        [XmlAttribute(AttributeName = "vessel_activity")]
        public string Vessel_Activity { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_Scheduletrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<ScheduleTransaction> ScheduleTransaction { get; set; }
    }
    #endregion-----------------------------------------

    #endregion 

    #region --- SCHEDULE CMCS ---

    public class schsDetail
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_start { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_end { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string note { get; set; } = "";

        public List<SchsLoadPorts> schs_load_ports { get; set; }
        public List<SchsDisPorts> schs_dis_ports { get; set; }
        
    }

    public class SchsLoadPorts
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
        public List<SchsCrude> schs_crude { get; set; }
    }

    public class SchsDisPorts
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string port { get; set; } = "";
    }

    public class SchsCrude
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string qty { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string nominate_date_start { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string nominate_date_end { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string received_date_start { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string received_date_end { get; set; } = "";
    }

    public class SchsActivity
    {
        public string activity { get; set; }
        public string activity_color { get; set; }
        public string order { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
        public string @ref { get; set; }
        public string portName { get; set; }
    }

    public class SchsActivityList
    {
        public List<SchsActivity> schs_activitys { get; set; }
    }

    public class SchsRootObject
    {
        public schsDetail schs_detail { get; set; }
        public List<SchsActivity> schs_activitys { get; set; }
    }

    #region-------------Schedule Transaction CMCS---------------------
    [XmlRoot(ElementName = "transaction")]
    public class ScheduleCMCSTransaction
    {
        [XmlAttribute(AttributeName = "function_id")]
        public string Function_id { get; set; }
        [XmlAttribute(AttributeName = "function_desc")]
        public string Function_desc { get; set; }
        [XmlAttribute(AttributeName = "result_namespace")]
        public string Result_namespace { get; set; }
        [XmlAttribute(AttributeName = "result_status")]
        public string Result_status { get; set; }
        [XmlAttribute(AttributeName = "result_code")]
        public string Result_code { get; set; }
        [XmlAttribute(AttributeName = "result_desc")]
        public string Result_desc { get; set; }
        [XmlAttribute(AttributeName = "req_transaction_id")]
        public string Req_transaction_id { get; set; }
        [XmlAttribute(AttributeName = "transaction_id")]
        public string Transaction_id { get; set; }
        [XmlAttribute(AttributeName = "response_message")]
        public string Response_message { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string System { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "action")]
        public string Action { get; set; }

        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "date_start")]
        public string Date_start { get; set; }
        [XmlAttribute(AttributeName = "date_end")]
        public string Date_end { get; set; }
        [XmlAttribute(AttributeName = "vessel_id")]
        public string Vessel_id { get; set; }
        [XmlAttribute(AttributeName = "vessel")]
        public string Vessel { get; set; }
        [XmlAttribute(AttributeName = "purchase_no")]
        public string Purchase_no { get; set; }
        [XmlAttribute(AttributeName = "remark")]
        public string Remark { get; set; }
        [XmlAttribute(AttributeName = "create_by")]
        public string Create_by { get; set; }
        [XmlAttribute(AttributeName = "tc")]
        public string TC { get; set; }
        [XmlAttribute(AttributeName = "reason")]
        public string Reason { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_Scheduletrx_CMCS
    {
        [XmlElement(ElementName = "transaction")]
        public List<ScheduleCMCSTransaction> ScheduleCMCSTransaction { get; set; }
    }
    #endregion-----------------------------------------

    #endregion
}