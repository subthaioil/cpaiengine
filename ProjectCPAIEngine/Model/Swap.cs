﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class Swap
    {
        public SwapData data { get; set; }
        public List<SwapPrice> prices { get; set; }
    }
    public class SwapData
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string trip_no { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string item_no { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string po_no { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vessel_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sold_to { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ship_to { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string delivery_date { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string plant { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_type_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string volumn_bbl { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string volumn_mt { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string volumn_lite { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string outturn_bbl { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string outturn_mt { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string outturn_lite { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price_per { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string exchange_rate { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string total { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_total { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string create_date { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string create_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string create_by { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string last_modify_date { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string last_modify_time { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string last_modify_by { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string release { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tank { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string sale_order { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price_release { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string remark { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string distri_chann { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string table_name { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string memo { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_sap { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fi_doc { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string do_no { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_to_sap { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string seq { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string incoterm_type { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string temp { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string density { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string storage_location { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string distri_chann_sap { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string vat_nonvat { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string update_do { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string update_gi { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string fi_doc_reverse { get; set; }
    }

    public class SwapPrice
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string trip_no { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string item_no { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_type_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string num { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string type { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string price { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_price { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_id { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string release { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string invoice { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string memo { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string unit_inv { get; set; }
    }
}