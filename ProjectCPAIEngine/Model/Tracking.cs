﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class Tracking
    {
        public string unit { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string date { get; set; }
    }
}