﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "list_trx")]
    public class TransactionList
    {
        [XmlElement("transaction")]
        public List<Transaction> trans { get; set; }
    }
}