﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{    
    public class OutputResult
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }
}