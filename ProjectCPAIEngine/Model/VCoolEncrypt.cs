﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class VCoolEncrypt : Transaction
    {
        public string Transaction_id_Encrypted { get; set; }
        public string Req_transaction_id_Encrypted { get; set; }
        public string Purchase_no_Encrypted { get; set; }
        public string reason_Encrypted { get; set; }
        public string type_Encrypted { get; set; }

        public Tracking draft_tracking { get; set; }
        public Tracking final_tracking { get; set; }
        public List<Tracking> expert_tracking { get; set; }
    }

    #region-------------VCoolList---------------------
    [XmlRoot(ElementName = "list_trx")]
    public class List_VCooltrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<VCoolEncrypt> VCoolTransaction { get; set; }
    }
    #endregion-----------------------------------------
}