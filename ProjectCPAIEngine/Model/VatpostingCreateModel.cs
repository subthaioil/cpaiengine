﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Model
{
    public class VatpostingCreateModel
    {
        public List<BAPIACHE09> documentHeader { get; set; }
        public List<BAPIACGL09> accountGL { get; set; }

        public List<BAPIACAP09> accountPay { get; set; }

        //test1
        public List<BAPIACTX09> accountTax { get; set; }

        public List<BAPIACCR09> currencyAm { get; set; }

        public List<BAPIPAREX> extension2 { get; set; }

        public class BAPIACHE09
        {
            public string OBJ_TYPE { get; set; }
            public string OBJ_KEY { get; set; }
            public string BUS_ACT { get; set; }
            public string USERNAME { get; set; }
            public string HEADER_TXT { get; set; }
            public string COMP_CODE { get; set; }
            public string DOC_DATE { get; set; }
            public string PSTNG_DATE { get; set; }
            public string FISC_YEAR { get; set; }
            public string FIS_PERIOD { get; set; }
            public string DOC_TYPE { get; set; }
            public string REF_DOC_NO { get; set; }
        }

        public class BAPIACAP09//accountPay
        {
            //public string ITEMNO_ACC { get; set; }
            public string VENDOR_NO { get; set; }
            public string REF_KEY_1 { get; set; }
            public string PMNTTRMS { get; set; }
            public string ITEM_TEXT { get; set; }
            public string BUSINESSPLACE { get; set; }
            public string TAX_CODE { get; set; }
        }
        public class BAPIACGL09//accountGL
        {
           // public string ITEMNO_ACC { get; set; }
            public string GL_ACCOUNT { get; set; }
            public string ALLOC_NMBR { get; set; }
        }




        //test1d
        public class BAPIACTX09 //accountTax
        {
          //  public string ITEMNO_ACC { get; set; }
            public string GL_ACCOUNT { get; set; }
            public string TAX_CODE { get; set; }
        }

        public class BAPIACCR09//currencyAm
        {
         //   public string ITEMNO_ACC { get; set; }
            public string CURR_TYPE { get; set; }
            public string CURRENCY { get; set; }
            public decimal AMT_DOCCUR { get; set; }
            public decimal AMT_BASE { get; set; }
            public decimal DISC_BASE { get; set; }

        }

        public class BAPIPAREX//currencyAm
        {
            public string STRUCTURE { get; set; }
            public string VALUEPART1 { get; set; }
            public string VALUEPART2 { get; set; }

        }



    }

}
