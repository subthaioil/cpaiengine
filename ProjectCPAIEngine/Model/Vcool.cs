﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class VcoRequesterInfo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string area_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_status { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_status_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_priority { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_tn { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_tn_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_sc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_sc_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_SCEP_SH { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_SCSC_SH { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_TNVP { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_SCVP { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_CMCS_SH { get; set; }
    }

    public class VcoCrudeInfo
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tpc_plan_month { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tpc_plan_year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchased_by { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string origin { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string benchmark_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string incoterm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kbbl_min { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kbbl_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_date_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_ref { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_method { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging_date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging_date_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string premium { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string formula_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kt_min { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kt_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string former_formula_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string former_quantity_kt_min { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string former_quantity_kt_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string propcessing_period_form { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string propcessing_period_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revised_date_from  { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revised_date_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string premium_maximum { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_result { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_benchmark_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_incoterm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_premium { get; set; } = "";
    }
    
    public class VcoComment
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_for_purchasing { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_note { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_summary { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_summary_mobile { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scep { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scsc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tn { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_note { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_request { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_propose_discharge { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_propose_final { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string evpc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tnvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scsc_agree_flag { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scvp_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tnvp_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmvp_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_attach_file { get; set; } = "";
    }

    public class DateHistory
    {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string user_group { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_history { get; set; } = "";
    }

    
    public class VcoEtaDateHistory
    {
        public List<DateHistory> history { get; set; }
    }

    public class VcoCrudeYieldComparison
    {
        public List<VcoCrudeYield> yield { get; set; }
        public List<VcoCrudeYieldJson> comparison_json { get; set; }

        //public VcoCrudeYieldComparison()
        //{
        //    yield = new List<VcoCrudeYield>();
        //    comparison_json = new VcoCrudeYieldJson();
        //}
    }

    public class VcoCrudeYield
    {
        public string order { get; set; }
        public string yield { get; set; }
        public string base_value { get; set; }
        public string compare_crude { get; set; }
        public string compare_value { get; set; }
        public string delta_value { get; set; }
    }

    public class VcoCrudeYieldJson
    {
        public string selectedCrude { get; set; }
        public string isChecked { get; set; }
    }


    public class VcoRootObject
    {
        public VcoRequesterInfo requester_info { get; set; }
        public VcoCrudeInfo crude_info { get; set; }
        public VcoComment comment { get; set; }
        public VcoEtaDateHistory date_history { get; set; }
        public VcoCrudeYieldComparison crude_compare { get; set; }
    }
}