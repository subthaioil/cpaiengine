﻿using System.Xml.Serialization;

namespace ProjectCPAIEngine.Flow.Model
{
    [XmlRoot(ElementName = "transaction")]
    public class Transaction
    {
        [XmlAttribute(AttributeName = "function_id")]
        public string function_id { get; set; }
        [XmlAttribute(AttributeName = "function_desc")]
        public string function_desc { get; set; }
        [XmlAttribute(AttributeName = "result_namespace")]
        public string result_namespace { get; set; }
        [XmlAttribute(AttributeName = "result_status")]
        public string result_status { get; set; }
        [XmlAttribute(AttributeName = "result_code")]
        public string result_code { get; set; }
        [XmlAttribute(AttributeName = "result_desc")]
        public string result_desc { get; set; }
        [XmlAttribute(AttributeName = "req_transaction_id")]
        public string req_transaction_id { get; set; }
        [XmlAttribute(AttributeName = "transaction_id")]
        public string transaction_id { get; set; }
        [XmlAttribute(AttributeName = "response_message")]
        public string response_message { get; set; }
        [XmlAttribute(AttributeName = "system")]
        public string system { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string type { get; set; }
        [XmlAttribute(AttributeName = "action")]
        public string action { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string status { get; set; }
        [XmlAttribute(AttributeName = "status_description")]
        public string status_description { get; set; }
        [XmlAttribute(AttributeName = "status_tracking")]
        public string status_tracking { get; set; }
        [XmlAttribute(AttributeName = "date_purchase")]
        public string date_purchase { get; set; }
        [XmlAttribute(AttributeName = "vessel_id")]
        public string vessel_id { get; set; }
        [XmlAttribute(AttributeName = "vessel")]
        public string vessel { get; set; }
        [XmlAttribute(AttributeName = "supplying_location")]
        public string supplying_location { get; set; }
        [XmlAttribute(AttributeName = "purchase_no")]
        public string purchase_no { get; set; }
        [XmlAttribute(AttributeName = "products")]
        public string products { get; set; }
        [XmlAttribute(AttributeName = "volumes")]
        public string volumes { get; set; }
        [XmlAttribute(AttributeName = "create_by")]
        public string create_by { get; set; }
        [XmlAttribute(AttributeName = "update_date")]
        public string update_date { get; set; }
        [XmlAttribute(AttributeName = "supplier_id")]
        public string supplier_id { get; set; }
        [XmlAttribute(AttributeName = "supplier")]
        public string supplier { get; set; }
        [XmlAttribute(AttributeName = "supplier_color")]
        public string supplier_color { get; set; }
        [XmlAttribute(AttributeName = "cust_id")]
        public string cust_id { get; set; }
        [XmlAttribute(AttributeName = "cust_name")]
        public string cust_name { get; set; }
        [XmlAttribute(AttributeName = "cust_short")]
        public string cust_short { get; set; }
        [XmlAttribute(AttributeName = "flat_rate")]
        public string flat_rate { get; set; }
        [XmlAttribute(AttributeName = "laycan_from")]
        public string laycan_from { get; set; }
        [XmlAttribute(AttributeName = "laycan_to")]
        public string laycan_to { get; set; }
        [XmlAttribute(AttributeName = "owner")]
        public string owner { get; set; }
        [XmlAttribute(AttributeName = "min_load")]
        public string min_load { get; set; }
        [XmlAttribute(AttributeName = "trip_no")]
        public string trip_no { get; set; }
        [XmlAttribute(AttributeName = "reason")]
        public string reason { get; set; }
        [XmlAttribute(AttributeName = "remark")]
        public string remark { get; set; }
        [XmlAttribute(AttributeName = "date_start")]
        public string date_start { get; set; }
        [XmlAttribute(AttributeName = "date_end")]
        public string date_end { get; set; }
        [XmlAttribute(AttributeName = "others_cost_surveyor")]
        public string others_cost_surveyor { get; set; }
        [XmlAttribute(AttributeName = "others_cost_barge")]
        public string others_cost_barge { get; set; }
        [XmlAttribute(AttributeName = "final_price")]
        public string final_price { get; set; }
        [XmlAttribute(AttributeName = "total_price")]
        public string total_price { get; set; }
        [XmlAttribute(AttributeName = "charterer")]
        public string charterer { get; set; }

        [XmlAttribute(AttributeName = "charterer_name")]
        public string charterer_name { get; set; }

        [XmlAttribute(AttributeName = "cargo")]
        public string cargo { get; set; }

        [XmlAttribute(AttributeName = "cargo_name")]
        public string cargo_name { get; set; }

        [XmlAttribute(AttributeName = "broker")]
        public string broker { get; set; }

        [XmlAttribute(AttributeName = "broker_name")]
        public string broker_name { get; set; }

        [XmlAttribute(AttributeName = "load_port")]
        public string load_port { get; set; }

        [XmlAttribute(AttributeName = "load_port_name")]
        public string load_port_name { get; set; }

        [XmlAttribute(AttributeName = "discharge_port")]
        public string discharge_port { get; set; }

        [XmlAttribute(AttributeName = "discharge_port_name")]
        public string discharge_port_name { get; set; }

        [XmlAttribute(AttributeName = "laytime")]
        public string laytime { get; set; }

        [XmlAttribute(AttributeName = "date_fixture")]
        public string date_fixture { get; set; }
        [XmlAttribute(AttributeName = "target_days")]
        public string target_days { get; set; }
        [XmlAttribute(AttributeName = "avg_ws")]
        public string avg_ws { get; set; }
        [XmlAttribute(AttributeName = "ws_saving ")]
        public string ws_saving { get; set; }
        [XmlAttribute(AttributeName = "saving_in")]
        public string saving_in { get; set; }
        [XmlAttribute(AttributeName = "top_fixture_value")]
        public string top_fixture_value { get; set; }
        [XmlAttribute(AttributeName = "laycan_load_from")]
        public string laycan_load_from { get; set; }
        [XmlAttribute(AttributeName = "laycan_load_to")]
        public string laycan_load_to { get; set; }
        [XmlAttribute(AttributeName = "charter_in")]
        public string charter_in { get; set; }
        [XmlAttribute(AttributeName = "broker_id")]
        public string broker_id { get; set; }
        [XmlAttribute(AttributeName = "dem")]
        public string dem { get; set; }
        [XmlAttribute(AttributeName = "extra_cost")]
        public string extra_cost { get; set; }
        [XmlAttribute(AttributeName = "date_from")]
        public string date_from { get; set; }
        [XmlAttribute(AttributeName = "date_to")]
        public string date_to { get; set; }
        [XmlAttribute(AttributeName = "benefit_day")]
        public string benefit_day { get; set; }
        [XmlAttribute(AttributeName = "benefit_voy")]
        public string benefit_voy { get; set; }
        [XmlAttribute(AttributeName = "tce_day")]
        public string tce_day { get; set; }
        [XmlAttribute(AttributeName = "month")]
        public string month { get; set; }
        [XmlAttribute(AttributeName = "td")]
        public string td { get; set; }
        [XmlAttribute(AttributeName = "tc_rate")]
        public string tc_rate { get; set; }

        [XmlAttribute(AttributeName = "delivery_date_from")]
        public string delivery_date_from { get; set; }
        [XmlAttribute(AttributeName = "delivery_date_to")]
        public string delivery_date_to { get; set; }
        [XmlAttribute(AttributeName = "unit_price_type")]
        public string unit_price_type { get; set; }
        [XmlAttribute(AttributeName = "unit_price_value")]
        public string unit_price_value { get; set; }
        [XmlAttribute(AttributeName = "unit_price_order")]
        public string unit_price_order { get; set; }
        [XmlAttribute(AttributeName = "currency_symbol")]
        public string currency_symbol { get; set; }

        [XmlAttribute(AttributeName = "exten_cost")]
        public string exten_cost { get; set; }
        [XmlAttribute(AttributeName = "est_freight")]
        public string est_freight { get; set; }

        [XmlAttribute(AttributeName = "feed_stock")]
        public string feed_stock { get; set; }

        [XmlAttribute(AttributeName = "margin")]
        public string margin { get; set; }

        [XmlAttribute(AttributeName = "product_name")]
        public string product_name { get; set; }
        [XmlAttribute(AttributeName = "supplier_name")]
        public string supplier_name { get; set; }

        [XmlAttribute(AttributeName = "incoterm")]
        public string incoterm { get; set; }
        [XmlAttribute(AttributeName = "formula_p")]
        public string formula_p { get; set; }
        [XmlAttribute(AttributeName = "purchase_p")]
        public string purchase_p { get; set; }
        [XmlAttribute(AttributeName = "market_p")]
        public string market_p { get; set; }
        [XmlAttribute(AttributeName = "lp_p")]
        public string lp_p { get; set; }
        [XmlAttribute(AttributeName = "discharging_period")]
        public string discharging_period { get; set; }
        [XmlAttribute(AttributeName = "loading_period")]
        public string loading_period { get; set; }
        [XmlAttribute(AttributeName = "maximum_buying_price")]
        public string maximum_buying_price { get; set; }
        [XmlAttribute(AttributeName = "lp_result")]
        public string lp_result { get; set; }

        [XmlAttribute(AttributeName = "tc")]
        public string tc { get; set; }

        [XmlAttribute(AttributeName = "vessel_activity")]
        public string vessel_activity { get; set; }

        [XmlAttribute(AttributeName = "assay_ref")]
        public string assay_ref { get; set; }

        [XmlAttribute(AttributeName = "assay_date")]
        public string assay_date { get; set; }

        [XmlAttribute(AttributeName = "origin")]
        public string origin { get; set; }

        [XmlAttribute(AttributeName = "categories")]
        public string categories { get; set; }

        [XmlAttribute(AttributeName = "kerogen")]
        public string kerogen { get; set; }

        [XmlAttribute(AttributeName = "characteristic")]
        public string characteristic { get; set; }

        [XmlAttribute(AttributeName = "maturity")]
        public string maturity { get; set; }

        [XmlAttribute(AttributeName = "density")]
        public string density { get; set; }

        [XmlAttribute(AttributeName = "total_acid_number")]
        public string total_acid_number { get; set; }

        [XmlAttribute(AttributeName = "total_sulphur")]
        public string total_sulphur { get; set; }

        [XmlAttribute(AttributeName = "final_cam_file")]
        public string final_cam_file { get; set; }

        [XmlAttribute(AttributeName = "draft_cam_file")]
        public string draft_cam_file { get; set; }

        [XmlAttribute(AttributeName = "assay_file")]
        public string assay_file { get; set; }

        [XmlAttribute(AttributeName = "assay_from")]
        public string assay_from { get; set; }

        [XmlAttribute(AttributeName = "support_doc_file")]
        public string support_doc_file { get; set; }
        [XmlAttribute(AttributeName = "draft_cam_status")]
        public string draft_cam_status { get; set; }
        [XmlAttribute(AttributeName = "final_cam_status")]
        public string final_cam_status { get; set; }
        [XmlAttribute(AttributeName = "expert_units")]
        public string expert_units { get; set; }

        [XmlAttribute(AttributeName = "requester_name")]
        public string requester_name { get; set; }
        [XmlAttribute(AttributeName = "note")]
        public string note { get; set; }
        [XmlAttribute(AttributeName = "created_by")]
        public string created_by { get; set; }
        [XmlAttribute(AttributeName = "created_date")]
        public string created_date { get; set; }
        [XmlAttribute(AttributeName = "updated_by")]
        public string updated_by { get; set; }
        [XmlAttribute(AttributeName = "updated_date")]
        public string updated_date { get; set; }
        [XmlAttribute(AttributeName = "experts_support_doc_file")]
        public string experts_support_doc_file { get; set; }

        [XmlAttribute(AttributeName = "deal_date")]
        public string deal_date { get; set; }
        [XmlAttribute(AttributeName = "deal_no")]
        public string deal_no { get; set; }
        [XmlAttribute(AttributeName = "pre_deal_no")]
        public string pre_deal_no { get; set; }
        [XmlAttribute(AttributeName = "ticket_no")]
        public string ticket_no { get; set; }
        [XmlAttribute(AttributeName = "contact_no")]
        public string contact_no { get; set; }
        [XmlAttribute(AttributeName = "ticket_date")]
        public string ticket_date { get; set; }
        [XmlAttribute(AttributeName = "deal_type")]
        public string deal_type { get; set; }
        [XmlAttribute(AttributeName = "hedg_type")]
        public string hedg_type { get; set; }
        [XmlAttribute(AttributeName = "trade_for")]
        public string trade_for { get; set; }
        [XmlAttribute(AttributeName = "tool")]
        public string tool { get; set; }
        [XmlAttribute(AttributeName = "counter_party")]
        public string counter_party { get; set; }
        [XmlAttribute(AttributeName = "underlying")]
        public string underlying { get; set; }
        [XmlAttribute(AttributeName = "underlying_vs")]
        public string underlying_vs { get; set; }
        [XmlAttribute(AttributeName = "underlying_name")]
        public string underlying_name { get; set; }
        [XmlAttribute(AttributeName = "seller")]
        public string seller { get; set; }
        [XmlAttribute(AttributeName = "buyer")]
        public string buyer { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string price { get; set; }
        [XmlAttribute(AttributeName = "volume_month")]
        public string volume_month { get; set; }
        [XmlAttribute(AttributeName = "template_name")]
        public string template_name { get; set; }
        [XmlAttribute(AttributeName = "volume_kbbl_min")]
        public string volume_kbbl_min { get; set; }
        [XmlAttribute(AttributeName = "volume_kbbl_max")]
        public string volume_kbbl_max { get; set; }
        [XmlAttribute(AttributeName = "volume_kt_min")]
        public string volume_kt_min { get; set; }
        [XmlAttribute(AttributeName = "volume_kt_max")]
        public string volume_kt_max { get; set; }
        [XmlAttribute(AttributeName = "user_group")]
        public string user_group { get; set; }
        [XmlAttribute(AttributeName = "user_group_delegate")]
        public string user_group_delegate { get; set; }
        [XmlAttribute(AttributeName = "ws")]
        public string ws { get; set; }

        [XmlAttribute(AttributeName = "route")]
        public string route { get; set; }

        [XmlAttribute(AttributeName = "no_total_ex")]
        public string no_total_ex { get; set; }
        [XmlAttribute(AttributeName = "total_ex")]
        public string total_ex { get; set; }
        [XmlAttribute(AttributeName = "net_benefit")]
        public string net_benefit { get; set; }
        [XmlAttribute(AttributeName = "tpc_month")]
        public string tpc_month { get; set; }
        [XmlAttribute(AttributeName = "tpc_year")]
        public string tpc_year { get; set; }
        [XmlAttribute(AttributeName = "sc_status")]
        public string sc_status { get; set; }
        [XmlAttribute(AttributeName = "tn_status")]
        public string tn_status { get; set; }

        //for charter out cmmt
        [XmlAttribute(AttributeName = "result_flag")]
        public string result_flag { get; set; }
        [XmlAttribute(AttributeName = "result_a")]
        public string result_a { get; set; }
        [XmlAttribute(AttributeName = "result_b")]
        public string result_b { get; set; }

        //for hedge annual framework
        [XmlAttribute(AttributeName = "company")]
        public string company { get; set; }
        [XmlAttribute(AttributeName = "approved_date")]
        public string approved_date { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string version { get; set; }
        [XmlAttribute(AttributeName = "activation_status")]
        public string activation_status { get; set; }


        //for hedge steering committee framework
        [XmlAttribute(AttributeName = "year")]
        public string year { get; set; }
        [XmlAttribute(AttributeName = "package_name")]
        public string package_name { get; set; }
        [XmlAttribute(AttributeName = "hedge_type")]
        public string hedge_type { get; set; }
        [XmlAttribute(AttributeName = "instrument")]
        public string instrument { get; set; }
        [XmlAttribute(AttributeName = "tenor_from")]
        public string tenor_from { get; set; }
        [XmlAttribute(AttributeName = "tenor_to")]
        public string tenor_to { get; set; }
        [XmlAttribute(AttributeName = "exceed_flag")]
        public string exceed_flag { get; set; }

    }
}