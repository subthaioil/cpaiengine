﻿using com.pttict.engine.utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ProjectCPAIEngine.ServiceProvider
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProjService" in both code and config file together.
    [ServiceContract]
    public interface IProjService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Xml, RequestFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "CallService/")]
        [XmlSerializerFormat]
        ResponseData CallService(RequestData _request);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "CallSchedule/")]
        [XmlSerializerFormat]
        string CallSchedule();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "StartQuartz/")]
        [XmlSerializerFormat]
        string StartQuartz();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Xml, RequestFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "CallService2/")]
        [XmlSerializerFormat]
        ResponseData CallService2(RequestData _request);

    }
}
