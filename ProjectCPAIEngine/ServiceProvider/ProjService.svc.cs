﻿using com.pttict.engine;
using com.pttict.engine.utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;
using ProjectCPAIEngine.Utilities;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.ServiceProvider
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProjService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ProjService.svc or ProjService.svc.cs at the Solution Explorer and start debugging.
    public class ProjService : IProjService
    {
        EngineReceive _xml = new EngineReceive();
        QuartzService _Quartz = new QuartzService();
        public ResponseData CallService(RequestData _request)
        {
            return _xml.ResponseReceive(_request);
        }

        public string CallSchedule()
        {
            _xml.StartSheduler();
            return "Start Schecdule";
        }

        public string StartQuartz()
        {
            _Quartz.StartQuartz();
            return "Start Quartz";
        }

        public string StartQuartz(string time)
        {
            _Quartz.StartQuartz(time);
            return "Start Quartz by time";
        }

        //public string StratPOSchedule()
        //{
        //    string jsonTimeForGenPO = JSONSetting.getGlobalConfig("JSON_PIT_GENERATE_PO");

        //    var parsed = JObject.Parse(jsonTimeForGenPO);

        //    string FIND_DATE = parsed.SelectToken("FIND_DATE").Value<string>();

        //    QuartzService _Quartz = new QuartzService();
        //    for (int i = 0; i < parsed.SelectToken("DAILY_TIME_START").Count(); i++)
        //    {
        //        var timeText = parsed.SelectToken("DAILY_TIME_START[" + i + "].TIME").Value<string>().Split('.');
        //        _Quartz.POScheduleStart(timeText[0], timeText[1]);
        //    }

        //    return "Start PO Quartz";
        //}

        public ResponseData CallService2(RequestData _request)
        {
            string _response = string.Empty;
            Debug.WriteLine(_request);
            ResponseData ret = new ResponseData();
            ret.extra_xml = ShareFunction.XMLSerialize(_request);
            ret.result_desc = "Success";
            ret.result_code = "1";
            return ret;
        }


    }
}
