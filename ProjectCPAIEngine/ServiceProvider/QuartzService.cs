﻿using com.pttict.engine;
using Common.Logging;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectCPAIEngine.ServiceProvider
{
    public class QuartzService
    {
        ILog log = LogManager.GetLogger(typeof(QuartzService));
        public void StartQuartz()
        {
            log.Debug("#####StartQuartz####");
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler, start the schedular before triggers or anything else
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();

            // create job
            IJobDetail job = JobBuilder.Create<RunJobSchedult>().Build();

            // create trigger
            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInHours(24)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(07, 00))
                    .WithIntervalInSeconds(5)
                  )
                .Build();

            // Schedule the job using the job and trigger 
            sched.ScheduleJob(job, trigger);

            // **** Crude Purchase Bond Request
            //int[] tMs = new int[] { DateTime.Now.Hour, DateTime.Now.AddMinutes(1).Minute };
            int[] tMs = new int[] { 5, 0}; //<= Everyday at 5:00

            IJobDetail jobCpBondRequest = JobBuilder.Create<Batches.CrudePurchaseBondRequestStatusUpdate>().Build();
            ITrigger triggerCpBondRequest = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule(s => s.WithIntervalInHours(24)
                .OnEveryDay()
                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(tMs[0], tMs[1]))).Build(); 

            sched.ScheduleJob(jobCpBondRequest, triggerCpBondRequest);
            // **** [End] Crude Purchase Bond Request
        }

        public void StartQuartz(string time)
        {
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler, start the schedular before triggers or anything else
            IScheduler sched = schedFact.GetScheduler();
            sched.Start();

            // **** Engine job
            // create job
            IJobDetail job = JobBuilder.Create<RunJobSchedult>().Build();

            var trigger2 = (ICronTrigger)TriggerBuilder.Create()
                .WithIdentity("CRMEngine", "Engine")
                .WithCronSchedule(time) //visit http://www.cronmaker.com/
                .StartAt(DateTime.UtcNow)
                .WithPriority(1)
                .Build();

            // Schedule the job using the job and trigger 
            sched.ScheduleJob(job, trigger2);
            // **** [End] Engine job

            // **** Crude Purchase Bond Request
            //int[] tMs = new int[] { DateTime.Now.Hour, DateTime.Now.AddMinutes(1).Minute };
            int[] tMs = new int[] { 5, 0 }; //<= Everyday at 5:00

            IJobDetail jobCpBondRequest = JobBuilder.Create<Batches.CrudePurchaseBondRequestStatusUpdate>().Build();
            ITrigger triggerCpBondRequest = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule(s => s.WithIntervalInHours(24)
                .OnEveryDay()
                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(tMs[0], tMs[1]))).Build();

            sched.ScheduleJob(jobCpBondRequest, triggerCpBondRequest);
            // **** [End] Crude Purchase Bond Request
        }

        public class RunJobSchedult : IJob
        {
            EngineReceive _xml = new EngineReceive();
            void IJob.Execute(IJobExecutionContext context)
            {
                _xml.StartSheduler();
            }
        }


        //public void POScheduleStart(string timeInHours, string InSeconds)
        //{
        //    ISchedulerFactory schedFact = new StdSchedulerFactory();

        //    // get a scheduler, start the schedular before triggers or anything else
        //    IScheduler sched = schedFact.GetScheduler();
        //    sched.Start();

        //    // create job
        //    IJobDetail job = JobBuilder.Create<RunJobSchedult>().Build();

        //    // create trigger
        //    ITrigger trigger = TriggerBuilder.Create()
        //        .WithDailyTimeIntervalSchedule
        //          (s =>
        //             s.WithIntervalInHours(24)
        //            .OnEveryDay()
        //            .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(Convert.ToInt16(timeInHours), Convert.ToInt16(InSeconds)))
        //            .WithIntervalInSeconds(5)
        //          )
        //        .Build();

        //    // Schedule the job using the job and trigger 
        //    sched.ScheduleJob(job, trigger);
        //}


    }
}
