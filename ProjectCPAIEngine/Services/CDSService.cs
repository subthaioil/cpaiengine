﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectCPAIEngine.Services
{
    public class CDSService : BasicBean
    {
        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private ActionMessageDAL amsMan = new ActionMessageDAL();
        private HEDG_MT_CP_DAL cpDal = new HEDG_MT_CP_DAL();
        private HEDG_MT_CP_HISTORY_DAL cpHistoryDal = new HEDG_MT_CP_HISTORY_DAL();
        private HEDG_MT_CP_HISTORY_LOG_DAL cpHistoryLogDal = new HEDG_MT_CP_HISTORY_LOG_DAL();

        public class ResultMail
        {
            public List<CPAI_ACTION_MESSAGE> lstMsg { get; set; }
            public List<String> lstMailTo { get; set; }
        }

        public class ResultCheckCDS
        {
            public Boolean result { get; set; }
            public string message { get; set; }
            public string statusOld { get; set; }
            public string statusNew { get; set; }
            public string statusMessage { get; set; }
        }

        //action = 'CDS' ,system ='HEDG' , type = 'HEDG' , functionId= 'CDS' , usergroup = 'CDSMAIL'
        public ResultMail PrepareSendMail(string action, string system, string type, string functionId)
        {
            ResultMail result = new ResultMail();
            //list message
            List<CPAI_ACTION_MESSAGE> lstMsg = amsMan.findUserGroupMailDummy(action, system, type, functionId);

            //list mail to
            List<String> lstMailTo = new List<String>();

            if (lstMsg.Count == 0)
            {
                log.Info("not found message");
            }
            else
            {
                for (int i = 0; i < lstMsg.Count; i++)
                {
                    if (string.IsNullOrWhiteSpace(lstMsg[i].AMS_FK_USER))
                    {
                        //not override user
                        string userGroup = lstMsg[i].AMS_USR_GROUP;
                        List<USERS> userResult = userGMan.findUserMailByGroupSystem(userGroup, system);
                        if (userResult.Count == 0)
                        {
                            // user not found
                            log.Info("user not found");
                        }
                        else
                        {
                            for (int j = 0; j < userResult.Count; j++)
                            {
                                lstMailTo.Add(userResult[i].USR_EMAIL);
                            }
                        }
                    }
                    else
                    {
                        //override user
                        string usrRowId = lstMsg[i].AMS_FK_USER;
                        List<USERS> userResult = userMan.findByRowId(usrRowId);
                        if (userResult.Count != 1)
                        {
                            log.Info("user not found");
                        }
                        else
                        {
                            for (int j = 0; j < userResult.Count; j++)
                            {
                                lstMailTo.Add(userResult[i].USR_EMAIL);
                            }
                        }
                    }
                }
            }

            result.lstMsg = lstMsg;
            result.lstMailTo = lstMailTo;
            return result;
        }

        public ResultCheckCDS CheckCDS5(List<MT_VENDOR> counterparty, string user)
        {
            Boolean result = false;
            string messageReturn = "";
            ResultCheckCDS resp = new ResultCheckCDS();
            ResultMail resultMail = new ResultMail();
            if (counterparty == null || counterparty.Count == 0)
            {
                //get counter party by mt_vendor : system , code
                counterparty = new List<MT_VENDOR>();
                counterparty = MT_VENDOR_DAL.GetVerndorData("CPAI", "HEDG_MT_CP", "", "", "", CPAIConstantUtil.ACTIVE);
            }

            using (var context = new EntityCPAIEngine())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        //get config 
                        List<HEDG_CP_LIMIT_CDS> CdsConfigList = HEDG_CP_LIMIT_CDS_DAL.GetCdsConfig();
                        resultMail.lstMsg = amsMan.findUserGroupMailDummy(CPAIConstantUtil.CDS_ACTION, CPAIConstantUtil.CDS_SYS_HEDG, CPAIConstantUtil.CDS_TYP_HEDG, CPAIConstantUtil.FUN_CDS);

                        foreach (MT_VENDOR a in counterparty)
                        {
                            HEDG_MT_CP cp = HEDG_MT_CP_DAL.GetMtCp(a.VND_ACC_NUM_VENDOR);
                            string counterparty_name = a.VND_NAME1;
                            string cp_row_id = cp.HMCP_ROW_ID;
                            string cp_status = cp.HMCP_STATUS_TRADE;
                            string reason = "";
                            if (cp != null && !string.IsNullOrWhiteSpace(cp.HMCP_STATUS_TRADE))
                            {
                                foreach (var CdsConfig in CdsConfigList)
                                {
                                    if (HEDG_MT_CP_CDS_DAL.CheckCPStatus(cp_row_id, CdsConfig.HCLC_TYPE, Convert.ToInt32(CdsConfig.HCLC_DAY), Convert.ToDecimal(CdsConfig.HCLC_VALUE)))
                                    {
                                        if (CdsConfig.HCLC_ACTION.ToUpper() == cp.HMCP_STATUS_TRADE)
                                        {
                                            reason = "Do nothing";
                                        }
                                        else
                                        {
                                            if (CdsConfig.HCLC_ACTION.ToUpper().Equals(CPAIConstantUtil.NOTICE))
                                            {
                                                reason = "Notice";
                                            }
                                            else if (!CdsConfig.HCLC_ACTION.ToUpper().Equals(CPAIConstantUtil.NOTICE))
                                            {
                                                cp.HMCP_STATUS_TRADE = CdsConfig.HCLC_ACTION.ToUpper();
                                                cp.HMCP_UPDATED_BY = user;
                                                cp.HMCP_UPDATED_DATE = DateTime.Now;
                                                cpDal.UpdateFromCDSService(cp, context);
                                                reason = "Status have been changed";
                                            }

                                            messageReturn = messageReturn + "<br>" + " counterparty name : " + counterparty_name + " previous status : " + cp_status + " current status : " + cp.HMCP_STATUS_TRADE + " Description : " + CdsConfig.HCLC_DESC + " Reason : " + reason;
                                            resultMail.lstMailTo = CdsConfig.HCLC_EMAIL.Split(';').ToList();
                                            string htmlText = "";
                                            string jsonText = resultMail.lstMsg[0].AMS_MAIL_DETAIL.Replace("#result_detail", htmlText).Replace("#result_cds", messageReturn);
                                            var rSendMail = new SendMailService().sendMail(CPAIConstantUtil.CDS_MAIL, resultMail.lstMailTo, jsonText);
                                        }
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                messageReturn = messageReturn + "<br>" + " counterparty name : " + counterparty_name + " Not found counterparty";
                            }
                        }

                        dbContextTransaction.Commit();
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> " + tem);
                        log.Error("# Error CPAIUpdateDataState >> Rollback # :: Exception >>> ", ex);
                        log.Error("CPAIUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                        dbContextTransaction.Rollback();
                        result = false;
                        messageReturn = "Please contact admin (Error : " + tem + ")";
                    }
                }
            }

            resp.result = result;
            resp.message = messageReturn;
            return resp;
        }

        public ResultCheckCDS CheckCDSConfigByID(string pID, string pHisID, string user, EntityCPAIEngine context)
        {
            Boolean result = false;
            string messageReturn = "";
            ResultCheckCDS resp = new ResultCheckCDS();
            ResultMail resultMail = new ResultMail();
            HEDG_MT_CP_DAL cpDal = new HEDG_MT_CP_DAL();

            //get config 
            List<HEDG_CP_LIMIT_CDS> CdsConfigList = HEDG_CP_LIMIT_CDS_DAL.GetCdsConfig();
            resultMail.lstMsg = amsMan.findUserGroupMailDummy(CPAIConstantUtil.CDS_ACTION, CPAIConstantUtil.CDS_SYS_HEDG, CPAIConstantUtil.CDS_TYP_HEDG, CPAIConstantUtil.FUN_CDS);

            HEDG_MT_CP cp = cpDal.GetMtById(pID);
            string counterparty_name = cp.HMCP_NAME;
            string cp_row_id = cp.HMCP_ROW_ID;
            string cp_status = cp.HMCP_STATUS_TRADE;
            string reason = "";
            if (cp != null && !string.IsNullOrWhiteSpace(cp.HMCP_STATUS_TRADE))
            {
                foreach (var CdsConfig in CdsConfigList)
                {
                    if (HEDG_MT_CP_CDS_DAL.CheckCPStatus(cp_row_id, CdsConfig.HCLC_TYPE, Convert.ToInt32(CdsConfig.HCLC_DAY), Convert.ToDecimal(CdsConfig.HCLC_VALUE)))
                    {
                        if (CdsConfig.HCLC_ACTION.ToUpper() == cp.HMCP_STATUS_TRADE)
                        {
                            reason = "Do nothing";
                        }
                        else
                        {
                            if (CdsConfig.HCLC_ACTION.ToUpper().Equals(CPAIConstantUtil.NOTICE))
                            {
                                reason = "Notice";
                            }
                            else if (!CdsConfig.HCLC_ACTION.ToUpper().Equals(CPAIConstantUtil.NOTICE))
                            {
                                cp.HMCP_STATUS_TRADE = CdsConfig.HCLC_ACTION.ToUpper();
                                cp.HMCP_UPDATED_BY = user;
                                cp.HMCP_UPDATED_DATE = DateTime.Now;
                                cpDal.UpdateFromCDSService(cp, context);
                                reason = "Status have been changed";

                                resp.statusMessage = "Description : " + CdsConfig.HCLC_DESC + " Reason : " + reason;
                                resp.statusOld = cp_status;
                                resp.statusNew = cp.HMCP_STATUS_TRADE;
                                result = true;
                            }

                            string htmlText = "Counterparty name : " + counterparty_name + " Previous status : " + cp_status + " Current status : " + cp.HMCP_STATUS_TRADE;
                            messageReturn = "Description : " + CdsConfig.HCLC_DESC + " Reason : " + reason;
                            resultMail.lstMailTo = CdsConfig.HCLC_EMAIL.Split(';').ToList();
                            string jsonText = resultMail.lstMsg[0].AMS_MAIL_DETAIL.Replace("#result_detail", htmlText).Replace("#result_cds", messageReturn);
                            var rSendMail = new SendMailService().sendMail(CPAIConstantUtil.CDS_MAIL, resultMail.lstMailTo, jsonText);
                        }
                        break;
                    }
                }
            }
            resp.result = result;
            resp.message = messageReturn;
            return resp;
        }

        public static string saveToHis(string pID, string pCpID, string pReason, string pUser, EntityCPAIEngine context)
        {
            string result = "";

            DateTime now = DateTime.Now;
            HEDG_MT_CP_HISTORY_DAL cpHisDal = new HEDG_MT_CP_HISTORY_DAL();
            HEDG_MT_CP_HISTORY cpHisEnt = new HEDG_MT_CP_HISTORY();

            cpHisEnt = new HEDG_MT_CP_HISTORY();
            cpHisEnt.HMCH_ROW_ID = pID;
            cpHisEnt.HMCH_FK_HEDG_MT_CP = pCpID;
            cpHisEnt.HMCH_REASON = "UploadExcelFile";
            cpHisEnt.HMCH_CREATED_DATE = now;
            cpHisEnt.HMCH_CREATED_BY = pUser;
            cpHisEnt.HMCH_UPDATED_DATE = now;
            cpHisEnt.HMCH_UPDATED_BY = pUser;
            cpHisDal.Save(cpHisEnt, context);
            return result;
        }

        public static string saveToHisLog(string pID, string pOrder, string pType, string param1, string param2, string param3, string pUser, EntityCPAIEngine context)
        {
            string result = "";

            DateTime now = DateTime.Now;
            HEDG_MT_CP_HISTORY_LOG_DAL cpHisLogDal = new HEDG_MT_CP_HISTORY_LOG_DAL();
            HEDG_MT_CP_HISTORY_LOG cpHisLogEnt = new HEDG_MT_CP_HISTORY_LOG();

            cpHisLogEnt = new HEDG_MT_CP_HISTORY_LOG();
            cpHisLogEnt.HMHL_ROW_ID = ConstantPrm.GetDynamicCtrlID();
            cpHisLogEnt.HMHL_FK_CP_HISTORY = pID;
            cpHisLogEnt.HMHL_ORDER = pOrder;

            if (pType == "Limit")
            {
                cpHisLogEnt.HMHL_TITLE = "Credit Limit";
                cpHisLogEnt.HMHL_ACTION = "Edit";
                cpHisLogEnt.HMHL_NOTE = "Change volume from " + param1 + " to " + param2;
            }
            else if (pType == "Rating")
            {
                cpHisLogEnt.HMHL_TITLE = "Credit Rating";
                cpHisLogEnt.HMHL_ACTION = "Edit";
                cpHisLogEnt.HMHL_NOTE = "Change rating from " + param1 + " to " + param2;
            }
            else if (pType == "Cds")
            {
                cpHisLogEnt.HMHL_TITLE = "CDS";
                cpHisLogEnt.HMHL_ACTION = "ADD";
                cpHisLogEnt.HMHL_NOTE = "Add CDS data of " + param1 + " | VauleCount " + param2;
            }
            else if (pType == "CheckCDS")
            {
                cpHisLogEnt.HMHL_TITLE = "Change Status Trade";
                cpHisLogEnt.HMHL_ACTION = "Edit";
                cpHisLogEnt.HMHL_NOTE = "Change status trade from " + param1 + " to " + param2 + " | " + param3;
            }

            cpHisLogEnt.HMHL_CREATED_DATE = now;
            cpHisLogEnt.HMHL_CREATED_BY = pUser;
            cpHisLogEnt.HMHL_UPDATED_DATE = now;
            cpHisLogEnt.HMHL_UPDATED_BY = pUser;
            cpHisLogDal.Save(cpHisLogEnt, context);
            return result;
        }

    }
}