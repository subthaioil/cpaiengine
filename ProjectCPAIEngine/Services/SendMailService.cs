﻿using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.downstream;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using DSMail.model;
using DSMail.service;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Services
{
    public class SendMailService
    {
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        //downstreamCode = "MAIL_EWM"
        public DownstreamResponse<ResponseSendMailModel> sendMail(string downstreamCode, List<String> lstMailTo, string jsonText)
        {
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;

            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            //DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(downstreamCode);
            String content = configManagement.getDownstreamContent(downstreamCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            //DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            //stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            //currentCode = downResp.getResultCode();

            MailServiceConnectorImpl service = new MailServiceConnectorImpl();
            DownstreamResponse<ResponseSendMailModel> downResp = service.connect(config, finalContent);

            return downResp;
        }
    }
}