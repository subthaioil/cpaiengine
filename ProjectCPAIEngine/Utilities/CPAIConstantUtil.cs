﻿
namespace ProjectCPAIEngine.Flow.Utilities
{
    public class CPAIConstantUtil
    {
        public static readonly string delimeter_and = " and ";
        public static readonly string delimeter_MT = "MT";
        public static readonly string currency_symbol = "USD";
        public static readonly string ip_server = "ip_server";
        public static readonly string USD_bbl = "USD/bbl";
        public static readonly string OSP = "OSP";

        //mobile
        public static readonly string ANDROID = "ANDROID";
        public static readonly string IOS = "IOS";
        public static readonly string GC_ANDROID_VERSION = "ANDROID_VERSION";
        public static readonly string GC_IOS_VERSION = "IOS_VERSION";

        //function
        public static readonly string F100000001 = "F10000001";
        public static readonly string F100000009 = "F10000009";
        public static readonly string F100000006 = "F10000006";
        public static readonly string F100000007 = "F10000007";
        public static readonly string F100000025 = "F10000025";
        public static readonly string F100000026 = "F10000026";
        public static readonly string F100000023 = "F10000023";
        public static readonly string F100000012 = "F10000012";
        public static readonly string F100000034 = "F10000034";
        public static readonly string F100000037 = "F10000037";
        public static readonly string F100000040 = "F10000040";
        public static readonly string F100000041 = "F10000041";
        public static readonly string F100000042 = "F10000042";
        public static readonly string FUN_CDS = "CDS0001";


        //function VCool
        public static readonly string F10000063 = "F10000063";
        public static readonly string F10000064 = "F10000064";
        public static readonly string F10000065 = "F10000065";


        //channel
        public static readonly string MOBILE = "MOBILE";
        public static readonly string WEB = "WEB";


        public static readonly string BUNKER_FUNCTION_CODE = "1";
        public static readonly string CHARTER_IN_FUNCTION_CODE = "6";
        public static readonly string CHARTER_OUT_CMMT_FUNCTION_CODE = "7";
        public static readonly string FREIGHT_FUNCTION_CODE = "8";
        public static readonly string SCHEDULE_FUNCTION_CODE = "9";
        public static readonly string TCE_WS_FUNCTION_CODE = "12";
        public static readonly string TCE_WS_MK_FUNCTION_CODE = "15";
        public static readonly string CRUDE_PURCHASE_FUNCTION_CODE = "23";
        public static readonly string CHARTER_IN_CMMT_FUNCTION_CODE = "25";
        public static readonly string CHARTER_OUT_FUNCTION_CODE = "26";
        public static readonly string SCHEDULE_S_FUNCTION_CODE = "34";
        public static readonly string COOL_FUNCTION_CODE = "40";
        public static readonly string COOL_EXPERT_FUNCTION_CODE = "41";
        public static readonly string COOL_EXPERT_SH_FUNCTION_CODE = "42";
        public static readonly string HEDG_DEAL_FUNCTION_CODE = "37";
        public static readonly string HEDG_TICKET_FUNCTION_CODE = "38";
        public static readonly string PRE_DEAL_FUNCTION_CODE = "53";
        public static readonly string VCOOL_FUNCTION_CODE = "63";
        public static readonly string VCOOL_EXPERT_FUNCTION_CODE = "64";
        public static readonly string VCOOL_EXPERT_SH_FUNCTION_CODE = "65";
        public static readonly string HEDGE_ANNUAL_FUNCTION_CODE = "70";
        public static readonly string HEDGE_STR_CMT_FUNCTION_CODE = "73";


        //PROJECT NAME SPACE
        public static readonly string PROJECT_NAME_SPACE = "CPAI";

        public static readonly string PROJECT_NAME_PCF = "PCF";
        public static readonly string PROJECT_NAME_PIT = "PIT";

        public static readonly string PROJECT_NAME_MAT_CMCS = "MAT_CMCS";


        public static readonly string ACTIVE = "ACTIVE";
        public static readonly string INACTIVE = "INACTIVE";

        public static readonly string YES_FLAG = "Y";
        public static readonly string NO_FLAG = "N";

        public static readonly string CRUDE_TYPE = "CRUDE";
        public static readonly string PRODUCT_TYPE = "PRODUCT";
        public static readonly string CUS_TYPE = "CUS";
        public static readonly string BROKER_TYPE = "BROKER";
        public static readonly string FEED_TYPE = "FEED";
        public static readonly string VESSEL_TYPE = "VESSEL";

        public static readonly string NORMAL = "NORMAL";
        public static readonly string SUSPEND = "SUSPEND";
        public static readonly string SPECIAL = "SPECIAL";
        public static readonly string NOTICE = "NOTICE";

        public static readonly string CDS_OUT = "CDS OUT";
        public static readonly string CDS_IN = "CDS IN";


        public static readonly string Resp = "resp.";

        public static readonly string Channel = "channel";

        public static readonly string COLOR_DEFUALT = "#8f8f8f";

        public static readonly string NOTI = "NOTI";
        public static readonly string SMS = "SMS";
        public static readonly string EMAIL = "EMAIL";

        //for charter out cmmt
        public static readonly string RESUTL_FLAG_SAVING = "SAVING";
        public static readonly string RESUTL_FLAG_PROFIT = "PROFIT";

        // F1000001
        public static readonly string DataDetailInput = "data_detail_input";
        public static readonly string CurrentAction = "current_action";
        public static readonly string NextStatus = "next_status";
        public static readonly string User = "user";
        public static readonly string UserGroup = "user_group";
        public static readonly string UserGroupDelegate = "user_group_delegate";
        public static readonly string System = "system";
        public static readonly string Type = "type";
        public static readonly string Note = "note";
        public static readonly string Status = "status";
        public static readonly string PrevStatus = "previous_status";
        public static readonly string CharterFor = "charter_for";
        public static readonly string PurchaseNumber = "purchase_no";
        public static readonly string CreateBy = "create_by";
        public static readonly string DataDetail = "data_detail";
        public static readonly string AttachItems = "attach_items";
        public static readonly string FinalPrice = "final_price";
        public static readonly string OtherCostSur = "others_cost_surveyor";
        public static readonly string OtherCostBar = "others_cost_barge";
        public static readonly string TotalPrice = "total_price";

        public static readonly string Action = "action";
        public static readonly string Vessel = "vessel";
        public static readonly string DatePurchase = "date_purchase";
        public static readonly string SupplyLocation = "supplying_location";
        public static readonly string Supplier = "supplier";
        public static readonly string RejectFlag = "reject_flag";
        public static readonly string Products = "products";
        public static readonly string Volumes = "volumes";
        public static readonly string Message = "message";
        public static readonly string Update_flag = "update_flag";



        public static readonly string ACTION_REJECT = "REJECT";
        public static readonly string ACTION_DRAFT = "DRAFT";
        public static readonly string ACTION_VERIFY = "VERIFY";
        public static readonly string ACTION_ENDORSE = "ENDORSE";
        public static readonly string ACTION_APPROVE = "APPROVE";
        public static readonly string ACTION_CANCEL = "CANCEL";
        public static readonly string ACTION_SUBMIT = "SUBMIT";
        public static readonly string ACTION_EDIT = "EDIT";
        public static readonly string ACTION_FAIL = "FAIL";
        public static readonly string ACTION_COMPLETE = "COMPLETE";
        public static readonly string ACTION_APPROVE_1 = "APPROVE_1";
        public static readonly string ACTION_APPROVE_2 = "APPROVE_2";
        public static readonly string ACTION_APPROVE_3 = "APPROVE_3";
        public static readonly string ACTION_APPROVE_4 = "APPROVE_4";
        public static readonly string ACTION_APPROVE_5 = "APPROVE_5";
        public static readonly string ACTION_APPROVE_6 = "APPROVE_6";
        public static readonly string ACTION_APPROVE_7 = "APPROVE_7";
        public static readonly string ACTION_APPROVE_8 = "APPROVE_8";
        public static readonly string ACTION_APPROVE_9 = "APPROVE_9";
        public static readonly string ACTION_APPROVE_10 = "APPROVE_10";
        public static readonly string ACTION_APPROVE_11 = "APPROVE_11";
        public static readonly string ACTION_APPROVE_12 = "APPROVE_12";
        public static readonly string ACTION_FAIL_1 = "FAIL_1";
        public static readonly string ACTION_FAIL_2 = "FAIL_2";
        public static readonly string ACTION_CALLBACK = "CALLBACK";
        public static readonly string ACTION_REVISE = "REVISE";
        public static readonly string ACTION_EDIT_DEAL = "EDIT_DEAL";
        public static readonly string ACTION_EDIT_CONTRACT = "EDIT_CONTRACT";
        public static readonly string ACTION_WAIVE = "WAIVE";
        public static readonly string ACTION_UPLOAD = "UPLOAD";
        public static readonly string ACTION_REQUEST_BOND = "REQUEST_BOND";
        public static readonly string ACTION_DRAFT_2 = "DRAFT_2";
        public static readonly string ACTION_SUBMIT_2 = "SUBMIT_2";
        public static readonly string ACTION_REJECT_3 = "REJECT_3";

        public static readonly string STATUS_DRAFT = "DRAFT";
        public static readonly string STATUS_WAITING_VERIFY = "WAITING VERIFY";
        public static readonly string STATUS_WAITING_CERTIFIED = "WAITING CERTIFIED";
        public static readonly string STATUS_WAITING_ENDORSE = "WAITING ENDORSE";
        public static readonly string STATUS_APPROVED = "APPROVED";
        public static readonly string STATUS_COMPLETED = "COMPLETED";
        public static readonly string STATUS_WAITING_APPROVE_DRAFT_CAM = "WAITING APPROVE DRAFT CAM";
        public static readonly string STATUS_WAITING_EXPERT_APPROVE = "WAITING EXPERT APPROVAL";
        public static readonly string STATUS_WAITING_EXPERT_COMMENT = "WAITING EXPERT COMMENT";
        public static readonly string STATUS_WAITING_EXPERT_SECTION_HEAD_APPROVE = "WAITING EXPERT SECTION HEAD APPROVE";
        public static readonly string STATUS_WAITING_GENERATE_FINAL_CAM = "WAITING CREATE FINAL CAM";
        public static readonly string STATUS_WAITING_APPROVE_FINAL_CAM = "WAITING APPROVE FINAL CAM";
        public static readonly string STATUS_WAITING_RUN_LP = "WAITING RUN LP";
        public static readonly string STATUS_WAITING_APPROVE_RUN_LP = "WAITING APPROVE RUN LP";
        public static readonly string STATUS_WAITING_PROPOSE_ETA_DATE = "WAITING PROPOSE ETA DATE";
        public static readonly string STATUS_WAITING_EDIT_ETA_DATE = "WAITING EDIT ETA DATE";
        public static readonly string STATUS_WAITING_CHECK_TANK = "WAITING CHECK TANK";
        public static readonly string STATUS_WAITING_APPROVE_TANK = "WAITING APPROVE TANK";
        public static readonly string STATUS_WAITING_CHECK_IMPACT = "WAITING CHECK IMPACT";
        public static readonly string STATUS_CONSENSUS_APPROVAL = "CONSENSUS APPROVAL";
        public static readonly string STATUS_WAITING_CONFIRM_PRICE = "WAITING CONFIRM PRICE";
        public static readonly string STATUS_WAITING_APPROVE_PRICE = "WAITING APPROVE PRICE";
        public static readonly string STATUS_WAITING_APPROVE_SUMMARY = "WAITING APPROVE SUMMARY";
        public static readonly string STATUS_WAITING_VP_APPROVE_TANK = "WAITING CMVP SCVP APPROVE";
        public static readonly string STATUS_WAITING_VP_CHECK_IMPACT = "WAITING TNVP APPROVE";

        public static readonly string STATUS_CHECK_IMPACT_APPROVED = "CHECK IMPACT APPROVED";
        public static readonly string STATUS_CHECK_TANK_APPROVED = "CHECK TANK APPROVED";
        public static readonly string STATUS_APPROVED_BY_CMVP = "WAITING SCVP APPROVE";
        public static readonly string STATUS_APPROVED_BY_SCVP = "WAITING CMVP APPROVE";
        public static readonly string STATUS_WAITING_BOND_DOC = "WAITING BOND DOC";

        public static readonly string DESCRIPTION_WAITING_APPROVE_DRAFT_CAM = "WAITING DRAFT CAM APPROVAL";
        public static readonly string DESCRIPTION_WAITING_EXPERT_APPROVE = "WAITING EXPERT APPROVAL";
        public static readonly string DESCRIPTION_WAITING_APPROVE_FINAL_CAM = "WAITING FINAL CAM APPROVAL";

        public static readonly string DESCRIPTION_WAITING_CHECK_IMPACT = "WAITING TECHNOLOGIST COMMENT";
        public static readonly string DESCRIPTION_WAITING_VP_APPROVE_TANK = "WAITING VP APPROVAL";
        public static readonly string DESCRIPTION_WAITING_VP_CHECK_IMPACT = "WAITING VP APPROVAL";
        public static readonly string DESCRIPTION_WAITING_CHECK_TANK = "WAITING TANK CONFIRMATION";
        public static readonly string DESCRIPTION_WAITING_APPROVE_TANK = "WAITING TANK APPROVAL";
        public static readonly string DESCRIPTION_WAITING_CONFIRM_PRICE = "WAITING PRICE CONFIRMATION";
        public static readonly string DESCRIPTION_WAITING_APPROVE_PRICE = "WAITING VERIFY FINAL PRICE";
        public static readonly string DESCRIPTION_WAITING_APPROVE_SUMMARY = "WAITING EVPC APPROVAL";
        public static readonly string DESCRIPTION_WAITING_APPROVE_RUN_LP = "WAITING LP RESULT VERIFICATION";

        public static readonly string STATUS_SUBMIT = "SUBMIT";
        public static readonly string STATUS_HOLD = "HOLD";

        public static readonly string BUTTONT_SCH = "SCH";
        public static readonly string BUTTONT_ACT = "ACT";

        public static readonly string SYSTEM_TCE_WS = "TCE_WS";

        public static readonly string HEDG_CONST_MAIN = "MAIN";
        public static readonly string HEDG_CONST_EXTEND = "EXTEND";
        public static readonly string HEDG_CONST_SUBJECT = "SUBJECT";
        public static readonly string HEDG_CONST_OBJECT = "OBJECT";


        //F1000002
        public static readonly string ReSendTransactionID = "FTX_TRANS_ID";
        public static readonly string ReSendBy = "";
        public static readonly string FTX_INDEX1 = "FTX_INDEX1";
        public static readonly string FTX_INDEX2 = "FTX_INDEX2";

        public static readonly string FTX_INDEX4 = "FTX_INDEX4";
        public static readonly string FTX_INDEX5 = "FTX_INDEX5";
        public static readonly string FNC_FUNCTION_CODE = "FNC_FUNCTION_CODE";


        // F1000003
        public static readonly string UserId = "user";
        public static readonly string UserPassword = "password";
        public static readonly string UserSystem = "user_system";
        public static readonly string UserOS = "user_os";
        public static readonly string UserNotificationId = "user_noti_id";
        public static readonly string UserADFlag = "user_ad_flag";
        public static readonly string LoginUserPassword = "usr_password";
        public static readonly string LoginUserGroup = "usr_group";
        public static readonly string UserGroupRespValue = "user_group";
        public static readonly string UserNotiTopicsRespValue = "user_noti_topics";
        public static readonly string UserMobileFlag = "user_mobile_flag";
        public static readonly string UserMobileMenu = "user_mobile_menu";
        public static readonly string UserWebMenu = "user_web_menu";
        public static readonly string UserAppVersion = "app_version";

        public static readonly string NotiFlag = "noti_flag";
        public static readonly string SmsFlag = "sms_flag";
        public static readonly string EmailFlag = "email_flag";



        // F1000004
        public static readonly string FromDate = "from_date";
        public static readonly string ToDate = "to_date";
        public static readonly string RowsPerPage = "rows_per_page";
        public static readonly string PageNumber = "page_number";
        public static readonly string TripNo = "trip_no";
        public static readonly string Index1 = "index_1";
        public static readonly string Index2 = "index_2";
        public static readonly string Index3 = "index_3";
        public static readonly string Index4 = "index_4";
        public static readonly string Index5 = "index_5";
        public static readonly string Index6 = "index_6";
        public static readonly string Index7 = "index_7";
        public static readonly string Index8 = "index_8";
        public static readonly string Index9 = "index_9";
        public static readonly string Index10 = "index_10";
        public static readonly string Index11 = "index_11";
        public static readonly string Index12 = "index_12";
        public static readonly string Index13 = "index_13";
        public static readonly string Index14 = "index_14";
        public static readonly string Index15 = "index_15";
        public static readonly string Index16 = "index_16";
        public static readonly string Index17 = "index_17";
        public static readonly string Index18 = "index_18";
        public static readonly string Index19 = "index_19";
        public static readonly string Index20 = "index_20";
        public static readonly string Function_code = "function_code";
        public static readonly string MobileFlag = "mobile_flag";

        // F1000005
        public static readonly string TransactionId = "transaction_id";

        //F1000006
        public static readonly string Broker = "broker";
        public static readonly string Owner = "owner";
        public static readonly string FlatRate = "flat_rate";
        public static readonly string LayCan_From = "laycan_from";
        public static readonly string LayCan_To = "laycan_to";
        public static readonly string Min_Load = "min_load";
        public static readonly string Charterer = "charterer";
        public static readonly string TCE_Action = "tce_action";
        public static readonly string TCE_Reason = "tce_reason";
        public static readonly string Freight_type = "freight_type";
        public static readonly string Vessel_Size = "vessel_size";
        public static readonly string ws = "ws";
        public static readonly string route = "route";
        public static readonly string no_total_ex = "no_total_ex";
        public static readonly string total_ex = "total_ex";
        public static readonly string net_benefit = "net_benefit";
        //F1000007
        public static readonly string result_flag = "result_flag";
        public static readonly string result = "result";
        public static readonly string result_a = "result_a";
        public static readonly string result_b = "result_b";
        public static readonly string freight = "freight";
        

        //F10000008
        public static readonly string Date_Time = "date_time";
        public static readonly string Cargo = "cargo";
        public static readonly string DocNo = "doc_no";
        public static readonly string Laytime = "laytime";
        public static readonly string Load_port = "load_port";
        public static readonly string Discharge_port = "discharge_port";

        //F10000009
        public static readonly string Data_detail_input_sch = "data_detail_input_sch";
        public static readonly string Data_detail_sch = "data_detail_sch";
        public static readonly string Data_detail_input_act = "data_detail_input_act";
        public static readonly string Data_detail_act = "data_detail_act";
        public static readonly string Vendor = "vendor";
        public static readonly string Vendor_Color = "vendor_color";
        public static readonly string Date_Start = "date_start";
        public static readonly string Date_End = "date_end";
        public static readonly string ACTION_EDIT_SCH = "EDIT_SCH";
        public static readonly string ACTION_EDIT_ACT = "EDIT_ACT";
        public static readonly string REMARK = "remark";

        //F10000011
        public static readonly string Button_Type = "button_type";

        //F10000012
        public static readonly string Date_fixture = "date_fixture";
        public static readonly string Charter_in = "charter_in";

        //F10000015
        public static readonly string Month = "month";
        public static readonly string Date_from = "date_from";
        public static readonly string Date_to = "date_to";


        //F10000019
        public static readonly string PDF_Path = "pdf_path";

        //F10000020
        public static readonly string Key = "key";

        //F10000022
        public static readonly string Set_flag = "set_flag";

        //F10000023
        public static readonly string FeedStock = "feed_stock";
        public static readonly string incoterm = "incoterm";
        public static readonly string purchase_p = "purchase_p";
        public static readonly string market_p = "market_p";
        public static readonly string lp_p = "lp_p";
        public static readonly string Margin = "margin";
        public static readonly string crude_name_others = "crude_name_others";

        //F10000030
        public static readonly string PoNO = "po_no";

        //F10000034
        public static readonly string TC = "tc";

        //F10000037
        public static readonly string Hedg_Ticket_No = "ticket_no";
        public static readonly string Hedg_Deal_No = "deal_no";
        public static readonly string Hedg_Pre_Deal_Trans_Id = "pre_deal_trans_id";
        public static readonly string Hedg_Verify_Status_Pass = "PASS";
        public static readonly string Hedg_Verify_Status_NotPass = "NOTPASS";
        public static readonly string Hedg_Verify_Status_ByPass = "BYPASS";

        public static readonly string Hedg_Deal_Verify_Flag = "deal_verify_flag";
        public static readonly string Hedg_Deal_ContrackType_TopTrade = "TOP_TRADE";
        public static readonly string Hedg_Deal_ContrackType_LabixTrade = "LABIX_TRADE";

        //F10000040
        public static readonly string Assay_ref = "assay_ref";
        public static readonly string Assay_date = "assay_date";
        public static readonly string Origin = "origin";
        public static readonly string Categories = "categories";
        public static readonly string Kerogen = "kerogen";
        public static readonly string Characteristic = "characteristic";
        public static readonly string Maturity = "maturity";
        public static readonly string Density = "density";
        public static readonly string Total_acid_number = "total_acid_number";
        public static readonly string Total_sulphur = "total_sulphur";
        public static readonly string final_cam_file = "final_cam_file";
        public static readonly string Draft_cam_file = "draft_cam_file";
        public static readonly string Assay_file = "assay_file";
        public static readonly string Assay_from = "assay_from";
        public static readonly string Support_doc_file = "support_doc_file";

        //F10000041
        public static readonly string Unit = "unit";
        public static readonly string Units = "units";
        public static readonly string Areas = "areas";
        public static readonly string Tracking = "tracking";

        //CDS Sent Mail
        public static readonly string CDS_ACTION = "CDS";
        public static readonly string CDS_SYS_HEDG = "HEDG";
        public static readonly string CDS_TYP_HEDG = "HEDG";
        public static readonly string CDS_USERGROUP = "CDSMAIL";
        public static readonly string CDS_MAIL = "MAIL_EWM";
        
        //F10000052
        public static readonly string FIDoc_Vat = "fidoc_vat";
        public static readonly string FIDoc_Miro = "fidoc_miro";
        public static readonly string InvDoc_Miro = "invdoc_miro";
        public static readonly string Sapmsg_Vat = "sapmsg_vat";
        public static readonly string Saplogon_vat = "saplogon_vat";
        public static readonly string Sapmsg_Miro = "sapmsg_miro";
        public static readonly string Saplogon_miro = "saplogon_miro";

        //F10000053
        public static readonly string Hedg_Deal_Date = "deal_date";
        public static readonly string Hedg_Pre_Deal_No = "pre_deal_no";
        public static readonly string Hedg_Trade_For = "trade_for";
        public static readonly string Hedg_Type = "hedg_type";
        public static readonly string Hedg_Tools = "tool";
        public static readonly string Hedg_Underlying = "underlying";
        public static readonly string Hedg_Underlying_vs = "underlying_vs";
        public static readonly string Hedg_Company = "company";
        public static readonly string Hedg_Trading_book = "trading_book";
        public static readonly string Hedg_Counterpartie = "counterparties";
        public static readonly string Hedg_Create_by = "create_by";
        public static readonly string Hedg_Volume_mnt = "volume_mnt";
        public static readonly string Hedg_Price = "price";
        public static readonly string Hedg_Status_Tracking = "status_tracking";
        public static readonly string Hedg_Contract_no = "contract_no";


        public static readonly string Hedg_Create_Deal_Top = "create_deal_top";
        public static readonly string Hedg_Create_Deal_Other = "create_deal_other";
        public static readonly string Hedg_Create_Deal_Top_Tran_ID = "create_deal_top_tran_id";
        public static readonly string Hedg_Deal_Action = "deal_action";
        public static readonly string Hedg_Deal_Reason = "deal_reason";

        //F10000056
        public static readonly string POSuveyor = "poSuveyor";
        public static readonly string Sap_status_msg = "sap_status_msg";
        public static readonly string Sap_logon = "sap_logon";


        //F10000059
        public static readonly string SALESDOCUMENT = "SALESDOCUMENT";
        public static readonly string DO_doc = "delivery_doc";

        //F10000038
        public static readonly string Hedg_Ticket_Date = "ticket_date";
        public static readonly string Hedg_Ticket_Trader = "ticket_trader";
        public static readonly string Hedg_Deal_Type = "deal_type";
        public static readonly string Hedg_Counterparties = "ticket_counterparties";
        public static readonly string Hedg_Deal_Track_Status_Ticketed = "TICKETED";
        public static readonly string Hedg_Deal_Track_Status_Verified = "VERIFIED";
        public static readonly string Hedg_Deal_Track_Status_Endorsed = "ENDORSED";
        public static readonly string Hedg_Deal_Track_Status_Approved = "APPROVED";
        public static readonly string Hedg_Deal_Track_Status_Revised = "REVISED";

        //F10000066
        public static readonly string UPD_GI_IND = "Update_GI";
        public static readonly string UPD_DO_IND = "Update_DO";
        public static readonly string LogOnGI = "logonGI";
        public static readonly string LogOnDo = "logonDo";
        public static readonly string msgGI = "msg_gi";
        public static readonly string msgDO = "msg_do";

        //F10000063
        public static readonly string Formula_p = "formula_p";
        public static readonly string Tpc_plan_month = "tpc_plan_month";
        public static readonly string Tpc_plan_year = "tpc_plan_year";
        public static readonly string Loading_from = "loading_from";
        public static readonly string Loading_to = "loading_to";
        public static readonly string Discharging_from = "discharging_from";
        public static readonly string Discharging_to = "discharging_to";
        public static readonly string CheckSendMail_M05 = "checkSendMail_M05";
        public static readonly string Boarding = "boarding";

        //F10000064
        public static readonly string VCOOL_SCSC_AGREE = "AGREE";
        public static readonly string VCOOL_SCSC_DISAGREE = "DISAGREE";

        //F10000070
        public static readonly string Hedg_Updated_Date = "updated_date";
        public static readonly string Hedg_Updated_By = "updated_by";
        public static readonly string Hedg_Approved_Date = "approved_date";
        public static readonly string Hedg_Version = "version";
        public static readonly string Hedg_Activation_Status = "activation_status";

        //F10000073
        public static readonly string Hedg_Package_Name = "package_name";
        public static readonly string Hedg_Hedge_Type = "hedge_type";
        public static readonly string Hedg_Instrument = "instrument";
        public static readonly string Hedg_Tenor_From = "tenor_from";
        public static readonly string Hedg_Tenor_To = "tenor_to";
        public static readonly string Hedg_M1_M = "m1_m";
        public static readonly string Hedg_Time_Spread = "time_spread";
        public static readonly string Hedg_Total_Volume = "total_volume";
        public static readonly string Hedg_Exceed = "exceed";
    }

}