﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace ProjectCPAIEngine.Utilities
{

    enum DocumentActionStatus
    {
        Draft = 1,
        Submit = 2,
        DraftPreview = 3,
        Cancel = 4
    }

    public static class ConstantPrm
    {
        public static string AppName = System.Configuration.ConfigurationManager.AppSettings["AppName"];
        public static string URLEncryptKey = "CPAI";
        public static string EnginGetEngineID()
        {
            return Guid.NewGuid().ToString("N");
        }

        public static string GetDynamicCtrlID()
        {
            return Guid.NewGuid().ToString("N");
        }

        public static string ServiceModelMessageSuccess = "Successfully!"; // For return message

        public static class ENGINECONF
        {
            private const string _WEBChannel = "WEB";
            private const string _WEBMobile = "MOBILE";
            private const string _EnginAppID = "cpai";
            private const string _EnginAppPassword = "cpai132";
            private static string _appName = WebConfigurationManager.AppSettings["AutoStartQuartz"];

            public static string WEBChannel { get { return _WEBChannel; } }
            public static string WEBMobile { get { return _WEBMobile; } }
            public static string EnginAppID { get { return _EnginAppID; } }
            public static string EnginAppPassword { get { return _EnginAppPassword; } }
            public static string AppName { get { return _appName; } }
        }

        public static class SYSTEM
        {
            private const string _BUNKER = "BUNKER";
            private const string _CHARTERING = "CHARTERING";
            private const string _VESSEL = "VESSEL";
            private const string _CRUDE_PURCHASE = "CRUDE_P";
            private const string _CRUDE_IMPORT_PLAN = "CRUDE_IMPORT_PLAN";
            private const string _FREIGHT = "FREIGHT";
            private const string _SCHEDULE = "SCHEDULE";
            private const string _TCE_WS = "TCE_WS";
            private const string _TCE_MK = "TCE_MK";
            private const string _CON_TXN = "CON_TXN";
            private const string _COOL = "COOL";
            private const string _VCOOL = "VCOOL";
            private const string _HEDG = "HEDG";
            private const string _PAF = "PAF";
            private const string _DAF = "DAF";
            private const string _CDS = "CDS";

            private const string _HEDG_DEAL = "HEDG_DEAL";
            private const string _HEDG_TCKT = "HEDG_TCKT";

            public static string BUNKER { get { return _BUNKER; } }
            public static string CHARTERING { get { return _CHARTERING; } }
            public static string VESSEL { get { return _VESSEL; } }
            public static string CRUDE_PURCHASE { get { return _CRUDE_PURCHASE; } }
            public static string CRUDE_IMPORT_PLAN { get { return _CRUDE_IMPORT_PLAN; } }
            public static string FREIGHT { get { return _FREIGHT; } }
            public static string SCHEDULE { get { return _SCHEDULE; } }
            public static string TCE_WS { get { return _TCE_WS; } }
            public static string TCE_MK { get { return _TCE_MK; } }
            public static string CON_TXN { get { return _CON_TXN; } }
            public static string COOL { get { return _COOL; } }
            public static string VCOOL { get { return _VCOOL; } }
            public static string PAF { get { return _PAF; } }
            public static string HEDG { get { return _HEDG; } }
            public static string DAF { get { return _DAF; } }
            public static string CDS { get { return _CDS; } }

            public static string HEDG_DEAL { get { return _HEDG_DEAL; } }
            public static string HEDG_TCKT { get { return _HEDG_TCKT; } }

            private static List<string> Allsystem = new List<string>();
            public static List<string> GetAllSystem()
            {
                string Tabshow = JSONSetting.getGlobalConfig("TAB_SHOW");
                Allsystem.Clear();
                if (!string.IsNullOrEmpty(Tabshow))
                {
                    List<string> ListItem = Tabshow.Split('|').ToList();
                    foreach (var _item in ListItem)
                    {
                        if (_item.ToUpper() == _BUNKER.ToUpper()) Allsystem.Add(_BUNKER);
                        if (_item.ToUpper() == _CHARTERING.ToUpper()) Allsystem.Add(_CHARTERING);
                        if (_item.ToUpper() == _CRUDE_PURCHASE.ToUpper()) Allsystem.Add(_CRUDE_PURCHASE);
                        if (_item.ToUpper() == _CRUDE_IMPORT_PLAN.ToUpper()) Allsystem.Add(_CRUDE_IMPORT_PLAN);
                        if (_item.ToUpper() == _COOL.ToUpper()) Allsystem.Add(_COOL);
                    }
                }

                return Allsystem;

                //Allsystem.Add(_VESSEL);
                //Allsystem.Add(_FREIGHT);
                //Allsystem.Add(_SCHEDULE);
            }
        }

        public static class SYSTEMTYPE
        {
            private const string _CRUDE = "CRUDE";
            private const string _VESSEL = "VESSEL";
            private const string _CMCS = "CMCS";

            private const string _TOP = "TOP";
            private const string _OTHER = "OTHER";
            private const string _TICKET = "TICKET";

            public static string CRUDE { get { return _CRUDE; } }
            public static string VESSEL { get { return _VESSEL; } }
            public static string CMCS { get { return _CMCS; } }

            public static string TOP { get { return _TOP; } }
            public static string OTHER { get { return _OTHER; } }
            public static string TICKET { get { return _TICKET; } }
        }

        public static class ACTION
        {
            private const string _DRAFT = "DRAFT";
            private const string _WAITING = "WAITING";
            private const string _CANCEL = "CANCEL";
            private const string _APPROVE = "APPROVE";
            private const string _APPROVE_0 = "APPROVE_0";
            private const string _APPROVE_1 = "APPROVE_1";
            private const string _APPROVE_2 = "APPROVE_2";
            private const string _APPROVE_3 = "APPROVE_3";
            private const string _APPROVED = "APPROVED";
            private const string _WAITING_CERTIFIED = "WAITING CERTIFIED";
            private const string _WAITING_VERIFY = "WAITING VERIFY";
            private const string _WAITING_ENDORSE = "WAITING ENDORSE";
            private const string _WAITING_APPROVE = "WAITING APPROVE";
            private const string _SAVE_DRAFT = "SAVE DRAFT";
            private const string _SUBMIT = "SUBMIT";
            private const string _EDIT = "EDIT";
            private const string _VERIFIED = "VERIFIED";
            private const string _CERTIFIED = "CERTIFIED";
            private const string _WAITING_APPROVE_DRAFT_CAM = "WAITING APPROVE DRAFT CAM";
            private const string _WAITING_EXPERT_COMMENT = "WAITING EXPERT COMMENT";
            private const string _WAITING_EXPERT_APPROVE = "WAITING EXPERT APPROVAL";
            private const string _WAITING_EXPERT_SECTION_HEAD_APPROVE = "WAITING EXPERT SECTION HEAD APPROVE";
            private const string _WAITING_CREATE_FINAL_CAM = "WAITING CREATE FINAL CAM";
            private const string _WAITING_APPROVE_FINAL_CAM = "WAITING APPROVE FINAL CAM";
            private const string _WAITING_RUN_LP = "WAITING RUN LP";
            private const string _ACTIVE = "ACTIVE";
            private const string _INACTIVE = "INACTIVE";
            private const string _REJECT = "REJECT";
            private const string _VIEW = "VIEW";
            private const string _HISTORY = "HISTORY";

            public static string DRAFT { get { return _DRAFT; } }
            public static string WAITING { get { return _WAITING; } }
            public static string CANCEL { get { return _CANCEL; } }
            public static string APPROVE { get { return _APPROVE; } }
            public static string APPROVE_0 { get { return _APPROVE_0; } }
            public static string APPROVE_1 { get { return _APPROVE_1; } }
            public static string APPROVE_2 { get { return _APPROVE_2; } }
            public static string APPROVE_3 { get { return _APPROVE_3; } }
            public static string APPROVED { get { return _APPROVED; } }
            public static string WAITING_CERTIFIED { get { return _WAITING_CERTIFIED; } }
            public static string WAITING_VERIFY { get { return _WAITING_VERIFY; } }
            public static string WAITING_ENDORSE { get { return _WAITING_ENDORSE; } }
            public static string WAITING_APPROVE { get { return _WAITING_APPROVE; } }
            public static string SAVE_DRAFT { get { return _SAVE_DRAFT; } }
            public static string SUBMIT { get { return _SUBMIT; } }
            public static string EDIT { get { return _EDIT; } }
            public static string VERIFIED { get { return _VERIFIED; } }
            public static string CERTIFIED { get { return _CERTIFIED; } }
            public static string WAITING_APPROVE_DRAFT_CAM { get { return _WAITING_APPROVE_DRAFT_CAM; } }
            public static string WAITING_EXPERT_APPROVE { get { return _WAITING_EXPERT_APPROVE; } }
            public static string WAITING_EXPERT_COMMENT { get { return _WAITING_EXPERT_COMMENT; } }
            public static string WAITING_EXPERT_SECTION_HEAD_APPROVE { get { return _WAITING_EXPERT_SECTION_HEAD_APPROVE; } }
            public static string WAITING_CREATE_FINAL_CAM { get { return _WAITING_CREATE_FINAL_CAM; } }
            public static string WAITING_APPROVE_FINAL_CAM { get { return _WAITING_APPROVE_FINAL_CAM; } }
            public static string WAITING_RUN_LP { get { return _WAITING_RUN_LP; } }
            public static string ACTIVE { get { return _ACTIVE; } }
            public static string INACTIVE { get { return _INACTIVE; } }
            public static string REJECT { get { return _REJECT; } }
            public static string VIEW { get { return _VIEW; } }
            public static string HISTORY { get { return _HISTORY; } }

            private static List<string> AllStatus = new List<string>();
            public static List<string> GetAllStatus()
            {
                AllStatus.Clear();
                AllStatus.Add(DRAFT);
                AllStatus.Add(WAITING);
                AllStatus.Add(CANCEL);
                AllStatus.Add(APPROVE);
                return AllStatus;
            }
        }

        public static class FUNCTION
        {
            private const string _F10000001 = "F10000001";
            private const string _F10000003 = "F10000003";
            private const string _F10000004 = "F10000004";
            private const string _F10000005 = "F10000005";
            private const string _F10000006 = "F10000006";
            private const string _F10000007 = "F10000007";
            private const string _F10000008 = "F10000008";
            private const string _F10000009 = "F10000009";
            private const string _F10000010 = "F10000010";
            private const string _F10000011 = "F10000011";
            private const string _F10000012 = "F10000012";
            private const string _F10000013 = "F10000013";
            private const string _F10000014 = "F10000014";
            private const string _F10000015 = "F10000015";
            private const string _F10000016 = "F10000016";
            private const string _F10000017 = "F10000017";
            private const string _F10000018 = "F10000018";
            private const string _F10000023 = "F10000023";
            private const string _F10000025 = "F10000025";
            private const string _F10000026 = "F10000026";
            private const string _F10000027 = "F10000027";
            private const string _F10000028 = "F10000028";
            private const string _F10000030 = "F10000030";
            private const string _F10000031 = "F10000031";
            private const string _F10000032 = "F10000032";
            private const string _F10000034 = "F10000034";
            private const string _F10000035 = "F10000035";
            private const string _F10000036 = "F10000036";
            private const string _F10000037 = "F10000037";
            private const string _F10000038 = "F10000038";
            private const string _F10000039 = "F10000039";
            private const string _F10000040 = "F10000040";
            private const string _F10000041 = "F10000041";
            private const string _F10000042 = "F10000042";
            private const string _F10000043 = "F10000043";
            private const string _F10000044 = "F10000044";
            private const string _F10000045 = "F10000045";
            private const string _F10000046 = "F10000046";
            private const string _F10000047 = "F10000047";
            private const string _F10000048 = "F10000048";
            private const string _F10000049 = "F10000049";
            private const string _F10000050 = "F10000050";
            private const string _F10000051 = "F10000051";
            private const string _F10000052 = "F10000052";          
            private const string _F10000053 = "F10000053";
            private const string _F10000054 = "F10000054";
            private const string _F10000055 = "F10000055";
            private const string _F10000056 = "F10000056";
            private const string _F10000057 = "F10000057";
            private const string _F10000058 = "F10000058";
            private const string _F10000059 = "F10000059";
            private const string _F10000060 = "F10000060";
            private const string _F10000061 = "F10000061";
            private const string _F10000062 = "F10000062";
            private const string _F10000063 = "F10000063";
            private const string _F10000064 = "F10000064";
            private const string _F10000065 = "F10000065";
            private const string _F10000066 = "F10000066";
            private const string _F10000067 = "F10000067";
            private const string _F10000068 = "F10000068";
            private const string _F10000069 = "F10000069";
            private const string _F10000070 = "F10000070";
            private const string _F10000071 = "F10000071";
            private const string _F10000072 = "F10000072";
            private const string _F10000073 = "F10000073";
            private const string _F10000080 = "F10000080";
            private const string _F10000081 = "F10000081";
            private const string _F10000082 = "F10000082";
            private const string _F10000083 = "F10000083";
            private const string _F10000087 = "F10000087";
            private const string _F10000088 = "F10000088";
            private const string _F10000089 = "F10000089"; 
            private const string _F10000090 = "F10000090"; 
            private const string _F10000091 = "F10000091"; 
            private const string _F10000092 = "F10000092"; 

            public static string F10000001 { get { return _F10000001; } }
            public static string F10000003 { get { return _F10000003; } }
            public static string F10000004 { get { return _F10000004; } }
            public static string F10000005 { get { return _F10000005; } }
            public static string F10000006 { get { return _F10000006; } }
            public static string F10000007 { get { return _F10000007; } }
            public static string F10000008 { get { return _F10000008; } }
            public static string F10000009 { get { return _F10000009; } }
            public static string F10000010 { get { return _F10000010; } }
            public static string F10000011 { get { return _F10000011; } }
            public static string F10000012 { get { return _F10000012; } }
            public static string F10000013 { get { return _F10000013; } }
            public static string F10000014 { get { return _F10000014; } }
            public static string F10000015 { get { return _F10000015; } }
            public static string F10000016 { get { return _F10000016; } }
            public static string F10000017 { get { return _F10000017; } }
            public static string F10000018 { get { return _F10000018; } }
            public static string F10000023 { get { return _F10000023; } }
            public static string F10000025 { get { return _F10000025; } }
            public static string F10000026 { get { return _F10000026; } }
            public static string F10000027 { get { return _F10000027; } }
            public static string F10000028 { get { return _F10000028; } }
            public static string F10000030 { get { return _F10000030; } }
            public static string F10000031 { get { return _F10000031; } }
            public static string F10000032 { get { return _F10000032; } }
            public static string F10000034 { get { return _F10000034; } }
            public static string F10000035 { get { return _F10000035; } }
            public static string F10000036 { get { return _F10000036; } }
            public static string F10000037 { get { return _F10000037; } }
            public static string F10000038 { get { return _F10000038; } }            
            public static string F10000039 { get { return _F10000039; } }
            public static string F10000040 { get { return _F10000040; } }
            public static string F10000041 { get { return _F10000041; } }
            public static string F10000042 { get { return _F10000042; } }
            public static string F10000043 { get { return _F10000043; } }
            public static string F10000044 { get { return _F10000044; } }
            public static string F10000045 { get { return _F10000045; } }
            public static string F10000046 { get { return _F10000046; } }
            public static string F10000047 { get { return _F10000047; } }
            public static string F10000048 { get { return _F10000048; } }
            public static string F10000049 { get { return _F10000049; } }
            public static string F10000050 { get { return _F10000050; } }
            public static string F10000051 { get { return _F10000051; } }
            public static string F10000052 { get { return _F10000052; } }                        
            public static string F10000053 { get { return _F10000053; } }
            public static string F10000054 { get { return _F10000054; } }
            public static string F10000055 { get { return _F10000055; } }
            public static string F10000056 { get { return _F10000056; } }
            public static string F10000057 { get { return _F10000057; } }
            public static string F10000058 { get { return _F10000058; } }
            public static string F10000059 { get { return _F10000059; } }
            public static string F10000060 { get { return _F10000060; } }
            public static string F10000061 { get { return _F10000061; } }
            public static string F10000062 { get { return _F10000062; } }
            public static string F10000063 { get { return _F10000063; } }
            public static string F10000064 { get { return _F10000064; } }
            public static string F10000065 { get { return _F10000065; } }
            public static string F10000066 { get { return _F10000066; } }
            public static string F10000067 { get { return _F10000067; } }
            public static string F10000068 { get { return _F10000068; } }
            public static string F10000069 { get { return _F10000069; } }
            public static string F10000070 { get { return _F10000070; } }
            public static string F10000071 { get { return _F10000071; } }
            public static string F10000072 { get { return _F10000072; } }
            public static string F10000073 { get { return _F10000073; } }
            public static string F10000080 { get { return _F10000080; } }
            public static string F10000081 { get { return _F10000081; } }
            public static string F10000082 { get { return _F10000082; } }
            public static string F10000083 { get { return _F10000083; } }
            public static string F10000087 { get { return _F10000087; } }
            public static string F10000088 { get { return _F10000088; } }
            public static string F10000089 { get { return _F10000089; } }
            public static string F10000090 { get { return _F10000090; } }
            public static string F10000091 { get { return _F10000091; } }
            public static string F10000092 { get { return _F10000092; } }

        }
    }
}