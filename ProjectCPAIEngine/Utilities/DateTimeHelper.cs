﻿using log4net;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public class DateTimeHelper
    {
        public static bool isParseToDate(string date,string format)
        {
            bool r = false;
            try
            {
                
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime.ParseExact(dateTimeString, format , CultureInfo.InvariantCulture);
                r = true;
            }
            catch (Exception ex)
            {
                
            }
            return r;

        }

        public static DateTime parsePurchaseDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.CurrentCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //for hedg

        public static string getDataHEDG(DateTime date_from,DateTime date_to)
        {
            string result = "";

            return result;
        }

        public static string getYearsHEDG(DateTime date_from, DateTime date_to)
        {
            string pre_result = "";
            string mid_result = "";
            string post_result = "";
            string result = "";
            int type = 0;
            int day_to = date_to.Day;
            int day_from = date_from.Day;
            int year_to = date_to.Year;
            int year_from = date_from.Year;
            int all_day = DateTimeHelper.dayOfYears(date_from, date_to);
            #region  years
            //check years
            if (year_to - year_from > 0)
            {
                //many year
                TimeSpan x = date_to.Subtract(date_from);
                int daysCount = x.Days;
                if (all_day - daysCount != 0)
                {
                    //not full year
                    if (date_to.Day == 31 && date_to.Month == 12)
                    {
                        //end day 31/12
                        result = " , " + year_from + 1 + " - " + year_to;
                        date_to = new DateTime(date_from.Year, 12, 31);
                        type = 2; //goto check haft year with new date to | result is post fix
                        return getHalfYearsHEDG(date_from, date_to, type, result);
                    }
                    else if (date_from.Day == 1 && date_from.Month == 1)
                    {
                        //start day 1/1
                        result = year_from + " - " + (year_to - 1) + " , ";
                        date_to = new DateTime(date_to.Year - 1, 12, 31);
                        type = 1;
                        return getHalfYearsHEDG(date_from, date_to, type, result);
                    }
                    else
                    {
                        //start day other , end day other
                        date_to = new DateTime(date_from.Year, 12, 31);
                        pre_result = getHalfYearsHEDG(date_from, date_to, 1, result);

                        mid_result = (year_from + 1) + " - " + (year_to - 1);

                        date_from = new DateTime(date_to.Year, 1, 1);
                        post_result = getHalfYearsHEDG(date_from, date_to, 1, result);

                        return pre_result + mid_result + post_result;
                    }
                }
                else
                {
                    //full year
                    return year_from.ToString() + " - " + year_to.ToString();
                }
            }
            else
            {
                //one year
                TimeSpan x = date_to.Subtract(date_from);
                int daysCount = x.Days;
                if (all_day - daysCount != 0)
                {
                    type = 1; //goto check haft year with old date from , to | result is pre fix
                    return getHalfYearsHEDG(date_from, date_to, type, result);
                }
                else
                {
                    return year_to.ToString(); // one year
                }
            }
            #endregion
        }

        public static string getHalfYearsHEDG(DateTime date_from, DateTime date_to,int type, string result)
        {
            #region  half years
            //check half years
            int year_to = date_to.Year;
            int day_from = date_from.Day;
            int day_to = date_to.Day;
            int month_from = date_from.Month;
            int month_to = date_to.Month;
            int months = (date_to.Year - date_from.Year) * 12 + date_to.Month - date_from.Month;//count mount
            if (months >= 6)
            {
                if (day_from == 1 && day_to == 30 && month_from == 1 && month_to == 6) // 1 jan - 30 jun
                {
                    //first half years 
                    if (type == 1)
                    {
                        //type = 1
                        return result = result + "FIRST_HALF-" + year_to;
                    }
                    else
                    {
                        //type = 2
                        return result = "FIRST_HALF-" + year_to + result;
                    }
                }
                else if (day_from == 1 && day_to == 31 && month_from == 7 && month_to == 12)//1 july - 31 dec
                {
                    //last half years
                    if (type == 1)
                    {
                        //type = 1
                        return result = result + "LAST_HALF-" + year_to;
                    }
                    else
                    {
                        //type = 2
                        return result = "LAST_HALF-" + year_to + result;
                    }
                }
                else if (day_from == 1 && month_from == 1)// 1 jan - xxx
                {
                    //first half years 
                    date_from = new DateTime(date_to.Year, 7, 1);
                    if (type == 1)
                    {
                        //type = 1
                        type = 3; //goto check quarter
                        result = result + "FIRST_HALF-" + year_to + " , ";
                        return getQuarterHEDG(date_from, date_to, type, result);
                    }
                    else
                    {
                        //type = 2
                        type = 4; //goto check quarter
                        result = ", FIRST_HALF-" + year_to + result;
                        return getQuarterHEDG(date_from, date_to, type, result);
                    }
                }
                else if (day_to == 31 && month_to == 12)// xxx - 31 Dec 
                {
                    //last half years
                    date_to = new DateTime(date_to.Year, 6, 30);
                    if (type == 1)
                    {
                        //type = 1
                        type = 3; //goto check quarter
                        result = result + "LAST_HALF-" + year_to + " , "; //per
                        return getQuarterHEDG(date_from, date_to, type, result);
                    }
                    else
                    {
                        //type = 2
                        type = 4; //goto check quarter
                        result = ", LAST_HALF-" + year_to + result; //post
                        return getQuarterHEDG(date_from, date_to, type, result);
                    }
                }
                else
                {
                    //start day other , end day other
                    type = 3;
                    return getQuarterHEDG(date_from, date_to, type, result);
                }
            }
            else
            {
                //mounth < 6
                type = 3; //goto check quarter
                return getQuarterHEDG(date_from, date_to, type, result);
            }
            #endregion
        }

        public static string getQuarterHEDG(DateTime date_from, DateTime date_to, int type,string result)
        {
            #region  quarter
            //check quarter
            int day_from = date_from.Day;
            int day_to = date_to.Day;
            int month_from = date_from.Month;
            int month_to = date_to.Month;
            int months = (date_to.Year - date_from.Year) * 12 + date_to.Month - date_from.Month;//count mount
            if (months >= 4)
            {
                if (day_from == 1 && month_from == 1) //1 jan - xx
                {
                    //check start q1
                    if (day_to == 30 && month_to == 4)
                    {
                        if (type == 3)
                        {
                            //3
                            return result + "Q1";
                        }
                        else
                        {
                            //4
                            return "Q1" + result;
                        }
                    }
                    else
                    {
                        date_from = new DateTime(date_from.Year, 5, 1);
                        if (type == 3)
                        {
                            //3
                            result = result + "Q1";
                            type = 5;
                        }
                        else
                        {
                            //4
                            result = "Q1" + result;
                            type = 6;
                        }
                        return getMonthHEDG(date_from, date_to, type, result);
                    }
                }
                else if (day_from == 1 && month_from == 5)//1 may - xx
                {
                    //check start q2
                    if (day_to == 31 && month_to == 8)
                    {
                        if (type == 3)
                        {
                            //3
                            return result + "Q2";
                        }
                        else
                        {
                            //4
                            return "Q2" + result;
                        }
                    }
                    else
                    {
                        date_from = new DateTime(date_from.Year, 9, 1);
                        if (type == 3)
                        {
                            //3
                            result = result + "Q2";
                            type = 5;
                        }
                        else
                        {
                            //4
                            result = "Q2" + result;
                            type = 6;
                        }
                        return getMonthHEDG(date_from, date_to, type, result);
                    }
                }
                else if (day_from == 1 && month_from == 9)//1 sep - xx
                {
                    //check start q3
                    if (day_to == 31 && month_to == 8)
                    {
                        if (type == 3)
                        {
                            //3
                            return result + "Q2";
                        }
                        else
                        {
                            //4
                            return "Q2" + result;
                        }
                    }
                    else
                    {
                        date_from = new DateTime(date_from.Year + 1, 1, 1);
                        if (type == 3)
                        {
                            //3
                            result = result + "Q2";
                            type = 5;
                        }
                        else
                        {
                            //4
                            result = "Q2" + result;
                            type = 6;
                        }
                        return getMonthHEDG(date_from, date_to, type, result);
                    }
                }
                else if (day_to == 30 && month_to == 4)//30 April - xx
                {
                    //check end q1
                    if (day_from == 1 && month_from == 1)
                    {
                        if (type == 3)
                        {
                            //3
                            return result + "Q1";
                        }
                        else
                        {
                            //4
                            return "Q1" + result;
                        }
                    }
                    else
                    {
                        date_to = new DateTime(date_from.Year - 1, 12, 31);
                        if (type == 3)
                        {
                            //3
                            result = result + "Q1";
                            type = 5;
                        }
                        else
                        {
                            //4
                            result = "Q1" + result;
                            type = 6;
                        }
                        return getMonthHEDG(date_from, date_to, type, result);
                    }
                }
                else if (day_to == 31 && month_to == 8)//31 aug - xx
                {
                    //check end q2
                    if (day_from == 1 && month_from == 5)
                    {
                        if (type == 3)
                        {
                            //3
                            return result + "Q2";
                        }
                        else
                        {
                            //4
                            return "Q2" + result;
                        }
                    }
                    else
                    {
                        date_to = new DateTime(date_from.Year, 4, 30);
                        if (type == 3)
                        {
                            //3
                            result = result + "Q2";
                            type = 5;
                        }
                        else
                        {
                            //4
                            result = "Q2" + result;
                            type = 6;
                        }
                        return getMonthHEDG(date_from, date_to, type, result);
                    }
                }
                else if (day_to == 31 && month_to == 12)//31 dec - xx
                {
                    //check end q3
                    if (day_from == 1 && month_from == 9)
                    {
                        if (type == 3)
                        {
                            //3
                            return result + "Q3";
                        }
                        else
                        {
                            //4
                            return "Q3" + result;
                        }
                    }
                    else
                    {
                        date_to = new DateTime(date_from.Year, 8, 31);
                        if (type == 3)
                        {
                            //3
                            result = result + "Q3";
                            type = 5;
                        }
                        else
                        {
                            //4
                            result = "Q3" + result;
                            type = 6;
                        }
                        return getMonthHEDG(date_from, date_to, type, result);
                    }
                }
                else
                {
                    //not quarter
                    type = 5; //goto check month
                    return getMonthHEDG(date_from, date_to, type, result);
                }
            }
            else
            {
                //month < 4
                type = 5; //goto check month
                return getMonthHEDG(date_from, date_to, type, result);
            }
            #endregion
        }

        public static string getMonthHEDG(DateTime date_from, DateTime date_to, int type,string result)
        {
            #region  months
            int day_from = date_from.Day;
            int day_to = date_to.Day;
            int month_from = date_from.Month;
            int month_to = date_to.Month;
            int months = (date_to.Year - date_from.Year) * 12 + date_to.Month - date_from.Month;//count mount
            DateTime lastDay = new DateTime(date_to.Year, date_to.Month, DateTime.DaysInMonth(date_to.Year, date_to.Month));
            if (months > 1)
            {
                if (day_from == 1 && lastDay.Day == date_to.Day)
                {
                    if (type == 5)
                    {
                        return result + date_from.ToString("MMM") + " - " + date_to.ToString("MMM");
                    }
                    else
                    {
                        return date_from.ToString("MMM") + " - " + date_to.ToString("MMM") + result;
                    }
                }
                else
                {
                    if (type == 5)
                    {
                        type = 7;
                        result = result + date_from.ToString("MMM") + " - " + new DateTime(date_to.Year, month_to - 1, date_to.Day).ToString("MMM");
                    }
                    else
                    {
                        type = 8;
                        result = date_from.ToString("MMM") + " - " + new DateTime(date_to.Year, month_to - 1, date_to.Day).ToString("MMM") + result;
                    }
                    DateTime lastDay1 = new DateTime(date_to.Year, date_to.Month - 1, DateTime.DaysInMonth(date_to.Year, date_to.Month - 1));
                    date_from = new DateTime(date_to.Year, date_to.Month, 1);
                    return getDayHEDG(date_from, date_to, type, result);
                }
            }
            else
            {
                type = type + 1;
                return getDayHEDG(date_from, date_to, type, result);
            }
            #endregion
        }

        public static string getDayHEDG(DateTime date_from, DateTime date_to, int type, string result)
        {
            #region days
            //days
            if (type == 7)
            {
                return result + date_from.ToString("dd MMM yyyy") + " - " + date_to.ToString("dd MMM yyyy");
            }
            else
            {
                return date_from.ToString("dd MMM yyyy") + " - " + date_to.ToString("dd MMM yyyy") + result;
            }
            #endregion
        }

        public static int dayOfYears(DateTime date_from, DateTime date_to)
        {
            DateTime date_from_1 = new DateTime(date_from.Year, 1, 1);
            DateTime date_to_1 = new DateTime(date_to.Year, 12, 31);
            TimeSpan span = date_to_1.Subtract(date_from_1);
            //return Math.Abs(span.TotalDays);
            return (int)span.TotalDays;
        }

        public static int daysBetween(DateTime d1, DateTime d2)
        {
            TimeSpan span = d2.Subtract(d1);
            //return Math.Abs(span.TotalDays);
            return (int)span.TotalDays;
        }

        public static string getHEDGYear(DateTime date_to , DateTime date_from)
        {
            int month_to = date_to.Month;
            int year_to = date_to.Year;
            int month_from = date_from.Month;
            int year_from = date_from.Year;

            if (month_to - 1 > 0)
            {

            }

            return "";
        }
    }
}