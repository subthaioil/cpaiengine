﻿using com.pttict.engine.utility;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace ProjectCPAIEngine.Utilities
{
    //sample to call engine login
    public class EngineCall
    {

        private const string _WEBChannel        = "WEB";
        private const string _EnginAppID        = "cpai";
        private const string _EnginAppPassword  = "cpai132";
        private const string _F10000003         = "F10000003";
        private const string destinationUrl     = "http://tsr-dcpai-app.thaioil.localnet/CHR/ServiceProvider/ProjService.svc/CallService/";
        private const string priKey             = "xxx";

        public void doLogin(string userLogin, string password)
        {
            //RequestCPAI req = new RequestCPAI();
            RequestData req = new RequestData();
            ResponseData resData = new ResponseData();

            if (!string.IsNullOrEmpty(userLogin) && !string.IsNullOrEmpty(password))
            {

                string request_txn_id   = "";
          
                password = RSAHelper.EncryptText(password, priKey);

                req.function_id = _F10000003;
                req.app_user = _EnginAppID;
                req.app_password = _EnginAppPassword;
                req.req_transaction_id = request_txn_id;
                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();
                req.req_parameters.p.Add(new p { k = "channel", v = _WEBChannel });
                req.req_parameters.p.Add(new p { k = "user", v = userLogin });
                req.req_parameters.p.Add(new p { k = "password", v = password });
                req.req_parameters.p.Add(new p { k = "user_system", v = _EnginAppID });
                req.req_parameters.p.Add(new p { k = "user_os", v = "" });
                req.req_parameters.p.Add(new p { k = "app_version", v = "" });
                req.req_parameters.p.Add(new p { k = "user_noti_id", v = "" });
                req.state_name = "";

                var xml = ShareFunction.XMLSerialize(req);
                //post data
                string res_data_xml = postXMLData(destinationUrl, xml);
                resData = ShareFunction.DeserializeXMLFileToObject<ResponseData>(res_data_xml);


                if (resData != null && resData.result_desc.ToUpper() == "SUCCESS")
                {
                    if (resData.result_code.Equals("1"))
                    {
                        //success do something ....
                    }
                    else
                    {
                        //fail do something ....
                    }
                }
                else
                {
                    //null response data do something ....
                }
            }
            else
            {
                //null user , password data do something ....
            }
        }

        #region XML
        public string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }
        #endregion
    }
}