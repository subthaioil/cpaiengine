﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Flow.Model;
using DSMail.model;
using com.pttict.downstream.common.model;
using DSMail.service;
using com.pttict.engine.downstream;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Utilities
{
    public class MailMappingService : BasicBean
    {

        //Volume: 100.00 MT and 60.00 MT  = #volume
        //Unit Price : ……USD/MT and ….USD/MT  = #bnk_detail
        //Total Price: 70800.00 USD  = #price

        //<br>Volume: #volume  
        //<br>Unit Price : #bnk_detail 
        //<br>Final Price: #price 
        public string getBodyF10000001(string body, string json)
        {
            BunkerPurchase dataDetail = JSonConvertUtil.jsonToModel<BunkerPurchase>(json);
            string supplier = "";
            string grade = "";
            string volume = "";
            string spec = "";
            string price = "";
            int count = 0;
            int count1 = 0;
            try
            {

                //string currency_symbol = dataDetail.currency_symbol != null ? " " + dataDetail.currency_symbol + "/ ton" : " $/ ton";
                string currency_symbol = dataDetail.currency_symbol != null ? " " + dataDetail.currency_symbol : CPAIConstantUtil.currency_symbol;
                string product_unit = CPAIConstantUtil.delimeter_MT;
                foreach (Product pd in dataDetail.product)
                {
                    if (count == 0)
                    {
                        grade = pd.product_name;
                        volume = String.Format("{0:n}", double.Parse(pd.product_qty)) + " " + CPAIConstantUtil.delimeter_MT + " ";
                        spec = pd.product_spec;
                    }
                    else
                    {
                        grade = grade + CPAIConstantUtil.delimeter_and + pd.product_name;
                        volume = volume + CPAIConstantUtil.delimeter_and + String.Format("{0:n}", double.Parse(pd.product_qty)) + " " + CPAIConstantUtil.delimeter_MT + " ";
                        spec = spec + CPAIConstantUtil.delimeter_and + pd.product_spec;
                    }
                    product_unit = pd.product_unit;
                    count++;
                }

                foreach (OffersItem of in dataDetail.offers_items)
                {
                    if (of.final_flag.Equals("Y"))
                    {
                        supplier = of.supplier;
                        /*  if (of.total_price != null && of.total_price != String.Empty)
                          {
                              price = of.total_price + " " + currency_symbol;
                          }
                          else
                          {
                              price = of.final_price + " " + currency_symbol;
                          }
                          */
                        //price = of.final_price + " " + currency_symbol + "/" + product_unit;
                        price = of.final_price + " " + currency_symbol;

                        List<RoundInfo> rinfo = new List<RoundInfo>();
                        foreach (RoundItem rt in of.round_items)
                        {
                            int no_before = 0;
                            int no = 0;
                            if (Int32.TryParse(rt.round_no, out no))
                            {
                                if (no > no_before)
                                {
                                    rinfo = rt.round_info;
                                    Int32.TryParse(rt.round_no, out no_before);
                                }
                            }
                        }
                        string bnk_detail = "";
                        if (body.Contains("#bnk_detail"))
                        {
                            if (rinfo != null && rinfo.Count > 0)
                            {
                                count1 = 0;
                                foreach (RoundInfo r in rinfo)
                                {
                                    //bnk_detail = bnk_detail + r.round_type + " : " ; 
                                    List<RoundValues> r_tem = r.round_value;
                                    string bnk_round = "";
                                    int r_order = 0;
                                    foreach (RoundValues rt in r_tem)
                                    {
                                        int temp_order = Int32.Parse(rt.order);
                                        if (temp_order > r_order)
                                        {
                                            //bnk_round =  String.Format("{0:n}", double.Parse(rt.value)) + " USD/MT ";
                                            bnk_round = rt.value + currency_symbol + "/" + product_unit;
                                            r_order = temp_order;
                                        }
                                    }
                                    if (count1 == 0)
                                    {
                                        bnk_detail = bnk_detail + bnk_round;
                                    }
                                    else
                                    {
                                        if (bnk_round != String.Empty)
                                            bnk_detail = bnk_detail + CPAIConstantUtil.delimeter_and + bnk_round;
                                    }
                                    count1++;
                                }
                            }
                            else
                            {
                                bnk_detail = "not found data";
                            }
                            body = body.Replace("#bnk_detail", bnk_detail);
                        }
                        break;
                    }
                }

                if (body.Contains("#reason"))
                {
                    body = body.Replace("#reason", dataDetail.reason);
                }
                if (body.Contains("#vessel"))
                {
                    string id = dataDetail.vessel;
                    MT_VEHICLE vessel = VehicleDAL.GetVehicleById(id);
                    if (vessel != null && vessel.VEH_VEH_TEXT != null)
                    {
                        body = body.Replace("#vessel", vessel.VEH_VEH_TEXT);
                    }
                    else
                    {
                        body = body.Replace("#vessel", id);
                    }

                }
                if (grade != null && body.Contains("#grade"))
                {
                    body = body.Replace("#grade", grade);

                }
                if (supplier != null && body.Contains("#supplier"))
                {
                    MT_VENDOR vendor = VendorDAL.GetVendorById(supplier);
                    if (vendor != null && vendor.VND_NAME1 != null)
                    {
                        body = body.Replace("#supplier", vendor.VND_NAME1);
                    }
                    else
                    {
                        body = body.Replace("#supplier", supplier);
                    }
                }
                if (body.Contains("#volume"))
                {
                    body = body.Replace("#volume", volume);
                }
                if (body.Contains("#price"))
                {
                    body = body.Replace("#price", price);
                }
                if (body.Contains("#location"))
                {
                    body = body.Replace("#location", dataDetail.supplying_location);
                }
                if (body.Contains("#delivery_date"))
                {
                    //09/11/2016
                    String date_from = dataDetail.delivery_date_range.date_from != null ? dataDetail.delivery_date_range.date_from : "";
                    String date_to = dataDetail.delivery_date_range.date_to != null ? dataDetail.delivery_date_range.date_to : "";
                    String date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#delivery_date", date);
                }

                if (body.Contains("#tc"))
                {
                    body = body.Replace("#tc", dataDetail.trip_no);
                }

            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }

            return body;
        }

        public string getSubjectF10000006(string subject, string json)
        {
            ChiRootObject dataDetail = JSonConvertUtil.jsonToModel<ChiRootObject>(json);

            if (subject.Contains("#load_month"))
            {
                subject = subject.Replace("#load_month", dataDetail.chi_evaluating.loading_month + " " + dataDetail.chi_evaluating.loading_year);
            }
            return subject;
        }

        public string getBodyF10000006(string body, string json)
        {
            /*
                # load_month
                # reason
                # vessel
                # owner
                # broker

                # laycan
                # chi_detail
                # othercost
                # freight
            */
            ChiRootObject dataDetail = JSonConvertUtil.jsonToModel<ChiRootObject>(json);
            try
            {
                if (body.Contains("#load_month"))
                {
                    body = body.Replace("#load_month", dataDetail.chi_evaluating.loading_month + " " + dataDetail.chi_evaluating.loading_year);
                }
                if (body.Contains("#laycan"))
                {

                    String date_from = dataDetail.chi_evaluating.laycan_from != null ? dataDetail.chi_evaluating.laycan_from : "";
                    String date_to = dataDetail.chi_evaluating.laycan_to != null ? dataDetail.chi_evaluating.laycan_to : "";
                    String date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#laycan", date);
                }
                if (body.Contains("#othercost"))
                {
                    body = body.Replace("#othercost", dataDetail.chi_proposed_for_approve.exten_cost);
                }
                if (body.Contains("#freight"))
                {
                    body = body.Replace("#freight", dataDetail.chi_proposed_for_approve.est_freight);
                }
                if (body.Contains("#reason"))
                {
                    body = body.Replace("#reason", dataDetail.chi_reason);
                }

                string vessel = string.Empty;
                string broker = string.Empty;
                string owner = string.Empty;
                string final_deal = string.Empty;
                foreach (ChiOffersItem of in dataDetail.chi_negotiation_summary.chi_offers_items)
                {
                    if (of.final_flag.Equals("Y"))
                    {
                        vessel = of.vessel;
                        broker = of.broker;
                        owner = of.owner;
                        final_deal = of.final_deal;
                        List<ChiRoundInfo> rinfo = new List<ChiRoundInfo>();
                        foreach (ChiRoundItem rt in of.chi_round_items)
                        {
                            int no_before = 0;
                            int no = 0;
                            if (Int32.TryParse(rt.round_no, out no))
                            {
                                if (no > no_before)
                                {
                                    rinfo = rt.chi_round_info;
                                    Int32.TryParse(rt.round_no, out no_before);
                                }
                            }
                        }
                        string chi_detail = "";
                        if (body.Contains("#chi_detail"))
                        {
                            if (rinfo != null && rinfo.Count > 0)
                            {
                                chi_detail = final_deal.Replace("\r\n", "<br>");

                                //foreach (ChiRoundInfo r in rinfo)
                                //{
                                //    if (r.type.ToLower().Equals("min loaded"))
                                //    {                                         
                                //        if(String.IsNullOrEmpty(r.offer))
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", "-") + " KT <br>";
                                //        }
                                //        else
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " KT <br>";
                                //        }                                            
                                //    }
                                //    else if (r.type.ToLower().Equals("dem"))
                                //    {
                                //        if (String.IsNullOrEmpty(r.offer))
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", "-") + " KUSD / Day <br>";
                                //        }
                                //        else
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " KUSD / Day <br>";
                                //        }                                        
                                //    }
                                //    else if (r.type.ToLower().Equals("lumpsum"))
                                //    {
                                //        if (String.IsNullOrEmpty(r.offer))
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", "-") + " K USD <br>";
                                //        }
                                //        else
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " K USD <br>";
                                //        }                                        
                                //    }
                                //    else
                                //    {
                                //        if (String.IsNullOrEmpty(r.offer))
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", "-") + "<br>";
                                //        }
                                //        else
                                //        {
                                //            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + "<br>";
                                //        }                                        
                                //    }
                                //}
                            }
                            else
                            {
                                chi_detail = "not found final deal";
                            }
                            body = body.Replace("#chi_detail", chi_detail);
                        }
                        break;
                    }
                }
                if (body.Contains("#vessel"))
                {
                    string id = vessel;
                    MT_VEHICLE vessel_data = VehicleDAL.GetVehicleById(id);
                    if (vessel_data != null && vessel_data.VEH_VEH_TEXT != null)
                    {
                        body = body.Replace("#vessel", vessel_data.VEH_VEH_TEXT);
                    }
                    else
                    {
                        body = body.Replace("#vessel", id);
                    }
                }
                if (body.Contains("#owner"))
                {
                    body = body.Replace("#owner", owner);
                }
                if (body.Contains("#broker"))
                {
                    MT_VENDOR vendor = VendorDAL.GetVendorById(broker);
                    if (vendor != null && vendor.VND_NAME1 != null)
                    {
                        body = body.Replace("#broker", vendor.VND_NAME1);
                    }
                    else
                    {
                        body = body.Replace("#broker", broker);
                    }

                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }
            return body;
        }


        public string getSubjectF10000007(string subject, string json)
        {
            ChotRootObject dataDetail = JSonConvertUtil.jsonToModel<ChotRootObject>(json);
            return subject;
        }

        public string getBodyF10000007(string body, string json)
        {
            try
            {
                ChotObjectRoot dataDetail = JSonConvertUtil.jsonToModel<ChotObjectRoot>(json);
                string unit = dataDetail.chot_charter_out_case_b.chot_est_income_b_one.offer != null ? " " + dataDetail.chot_charter_out_case_b.chot_est_income_b_one.offer : " USD";
                if (unit.Contains("/"))
                {
                    unit = unit.Substring(0, 4);
                }
                /*if (body.Contains("#load_month"))
                {
                    body = body.Replace("#load_month", dataDetail.cho_evaluating.loading_month + " " + dataDetail.cho_evaluating.loading_year);
                }*/
                if (body.Contains("#laycan"))
                {

                    String date_from = dataDetail.chot_summary.laycan_from != null ? dataDetail.chot_summary.laycan_from : "";
                    String date_to = dataDetail.chot_summary.laycan_to != null ? dataDetail.chot_summary.laycan_to : "";
                    String date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "-";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "-";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#laycan", date);
                }
                /* 
               if (body.Contains("#freight_no"))
               {
                   string freight_no = dataDetail.freight_no != null ? dataDetail.freight_no : "";
                   body = body.Replace("#freight_no", freight_no);
               }
               */
                if (body.Contains("#loading_port"))
                {
                    List<ChotLoadingPort> chotLoadingPortList = dataDetail.chot_summary.chot_loading_port;
                    string load_port = "";
                    var count = 0;
                    if (chotLoadingPortList != null && chotLoadingPortList.Count > 0)
                    {
                        foreach (ChotLoadingPort clp in chotLoadingPortList)
                        {
                            if (chotLoadingPortList.Count == 1)
                            {
                                load_port += "<br> Loading Port : " + clp.port_full_name;
                            }
                            else
                            {
                                load_port += "<br> Loading Port " + (++count) + " : " + clp.port_full_name;
                            }
                        }
                    }
                    body = body.Replace("#loading_port", load_port);
                }
                if (body.Contains("#discharging_port"))
                {
                    List<ChotDischargingPort> chotDischargingPort = dataDetail.chot_summary.chot_discharging_port;
                    string dis_port = "";
                    var count = 0;
                    if (chotDischargingPort != null && chotDischargingPort.Count > 0)
                    {
                        foreach (ChotDischargingPort clp in chotDischargingPort)
                        {
                            if (chotDischargingPort.Count == 1)
                            {
                                dis_port += "<br> Discharging Port : " + clp.port_full_name;
                            }
                            else
                            {
                                dis_port += "<br> Discharging Port " + (++count) + " : " + clp.port_full_name;
                            }
                        }
                    }
                    body = body.Replace("#discharging_port", dis_port);
                }
                if (body.Contains("#freight"))
                {
                    string freight = "0";

                    if (dataDetail.chot_charter_out_case_b != null)

                    {
                        if (dataDetail.chot_charter_out_case_b.chot_est_income_b_one != null)
                        {
                            freight = dataDetail.chot_charter_out_case_b.chot_est_income_b_one.total_freight != null ? dataDetail.chot_charter_out_case_b.chot_est_income_b_one.total_freight : "0";
                        }
                    }
                    body = body.Replace("#freight", String.Format("{0:N4}", double.Parse(freight)) + unit);

                }
                if (body.Contains("#reason"))
                {
                    body = body.Replace("#reason", dataDetail.chot_reason);
                }

                if (body.Contains("#vessel"))
                {
                    string vessel_name = !String.IsNullOrEmpty(dataDetail.vessel_name) ? dataDetail.vessel_name : "";
                    if (String.IsNullOrEmpty(vessel_name))
                    {
                        MT_VEHICLE vessel = VehicleDAL.GetVehicleById(dataDetail.vessel_id);
                        vessel_name = vessel.VEH_VEH_TEXT;
                    }
                    vessel_name = !String.IsNullOrEmpty(vessel_name) ? vessel_name : "-";
                    body = body.Replace("#vessel", vessel_name);
                }
                if (body.Contains("#broker"))
                {
                    string broker = !String.IsNullOrEmpty(dataDetail.chot_summary.charterer_broker) ? dataDetail.chot_summary.charterer_broker : "-";
                    body = body.Replace("#broker", broker);
                }

                if (body.Contains("#charterer"))
                {
                    string charterer_name = !String.IsNullOrEmpty(dataDetail.chot_summary.charterer_name) ? dataDetail.chot_summary.charterer_name : "";
                    if (String.IsNullOrEmpty(charterer_name))
                    {
                        MT_CUST_DETAIL cust = CustDetailDAL.GetCustDetailByRowId(dataDetail.chot_summary.charterer);
                        charterer_name = cust.MCD_NAME_1;
                    }
                    charterer_name = !String.IsNullOrEmpty(charterer_name) ? charterer_name : "-";
                    body = body.Replace("#charterer", charterer_name);
                }
                if (body.Contains("#result"))
                {
                    string flag = dataDetail.chot_result.flag != null ? dataDetail.chot_result.flag : "-";
                    body = body.Replace("#result", flag);


                    string no_total_ex = "0";
                    string total_ex = "0";
                    string net_benefit = "0";
                    if (flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_SAVING))
                    {
                        no_total_ex = dataDetail.chot_result.chot_saving.saving_total_expense != null ? dataDetail.chot_result.chot_saving.saving_total_expense : "0";
                        total_ex = dataDetail.chot_result.chot_saving.saving_exp_inc != null ? dataDetail.chot_result.chot_saving.saving_exp_inc : "0";
                        net_benefit = dataDetail.chot_result.chot_saving.saving_total != null ? dataDetail.chot_result.chot_saving.saving_total : "0";
                    }
                    if (flag.ToUpper().Equals(CPAIConstantUtil.RESUTL_FLAG_PROFIT))
                    {
                        no_total_ex = dataDetail.chot_result.chot_profit.profit_total_expense != null ? dataDetail.chot_result.chot_profit.profit_total_expense : "0";
                        total_ex = dataDetail.chot_result.chot_profit.profit_inc_var != null ? dataDetail.chot_result.chot_profit.profit_inc_var : "0";
                        net_benefit = dataDetail.chot_result.chot_profit.profit_total != null ? dataDetail.chot_result.chot_profit.profit_total : "0";
                    }
                    if (body.Contains("#no_total_ex"))
                    {
                        body = body.Replace("#no_total_ex", String.Format("{0:N4}", double.Parse(no_total_ex)) + " USD");
                    }
                    if (body.Contains("#total_ex"))
                    {
                        body = body.Replace("#total_ex", String.Format("{0:N4}", double.Parse(total_ex)) + " USD");
                    }
                    if (body.Contains("#net_benefit"))
                    {
                        body = body.Replace("#net_benefit", String.Format("{0:N4}", double.Parse(net_benefit)) + " USD");
                    }
                }

            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }
            return body;
        }


        public string getSubjectF10000025(string subject, string json)
        {
            ChitRootObject dataDetail = JSonConvertUtil.jsonToModel<ChitRootObject>(json);

            if (subject.Contains("#load_month"))
            {
                subject = subject.Replace("#load_month", dataDetail.chit_evaluating.loading_month + " " + dataDetail.chit_evaluating.loading_year);
            }
            return subject;
        }

        public string getBodyF10000025(string body, string json, string purNo)
        {
            /*
                # load_month
                # reason
                # vessel
                # product
                # broker

                # laycan
                # chi_detail
                # freight_type
            */
            try
            {
                ChitRootObject dataDetail = JSonConvertUtil.jsonToModel<ChitRootObject>(json);
                if (body.Contains("#laycan"))
                {

                    //String date_from = dataDetail.chit_evaluating.laycan_from != null ? dataDetail.chit_evaluating.laycan_from : "";
                    //String date_to = dataDetail.chit_evaluating.laycan_to != null ? dataDetail.chit_evaluating.laycan_to : "";
                    string date_from = dataDetail.chit_proposed_for_approve.laycan_from != null ? dataDetail.chit_proposed_for_approve.laycan_from : "-";
                    string date_to = dataDetail.chit_proposed_for_approve.laycan_to != null ? dataDetail.chit_proposed_for_approve.laycan_to : "-";

                    string date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "-";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "-";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "-";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#laycan", date);
                }
                if (body.Contains("#charterer"))
                {
                    string charterer = !String.IsNullOrEmpty(dataDetail.chit_detail.chartererName) ? dataDetail.chit_detail.chartererName : "-";
                    if(charterer != "-")
                    {
                        if (!String.IsNullOrEmpty(purNo))
                        {
                            using (EntityCPAIEngine context = new EntityCPAIEngine())
                            {
                                var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_PURCHASE_NO == purNo);
                                if (chitDetail != null)
                                {
                                    var custDetail = context.MT_CUST_DETAIL.SingleOrDefault(a => a.MCD_ROW_ID == chitDetail.IDAT_FK_CUST);
                                    if(custDetail != null)
                                    {
                                        charterer = custDetail.MCD_NAME_1 + " " + custDetail.MCD_NAME_2;
                                    }
                                }
                            }
                        }
                    }
                    body = body.Replace("#charterer", charterer);
                }
                if (body.Contains("#vessel"))
                {
                    string vessel = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.vessel_name) ? dataDetail.chit_proposed_for_approve.vessel_name : "-";
                    body = body.Replace("#vessel", vessel);
                }
                if (body.Contains("#cmpsselect"))
                {
                    if (!String.IsNullOrEmpty(purNo))
                    {
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_PURCHASE_NO == purNo);
                            if (chitDetail != null)
                            {
                                var chit_offerDetail = context.CHIT_OFFER_ITEMS.Where(a => a.ITOI_ROW_ID == chitDetail.IDAT_SELECT_CHARTER_FOR).ToList();
                                foreach (var item in chit_offerDetail)
                                {
                                    body = body.Replace("#cmpsselect", item.ITOI_VEHICLE);
                                }
                            }
                        }
                    }
                }
                if (body.Contains("#charterFor"))
                {
                    if (!String.IsNullOrEmpty(purNo))
                    {
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_PURCHASE_NO == purNo);
                            if (chitDetail != null)
                            {
                                body = body.Replace("#charterFor", chitDetail.IDAT_CHARTER_FOR.Substring(0,4));
                            }
                        }
                    }
                }
                if (body.Contains("#broker"))
                {
                    string broker = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.broker_name) ? dataDetail.chit_proposed_for_approve.broker_name : "-";
                    body = body.Replace("#broker", broker);
                }
                if (body.Contains("#Freight"))
                {
                    string broker = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.freight) ? dataDetail.chit_proposed_for_approve.freight : "-";
                    body = body.Replace("#Freight", broker);
                }
                if (body.Contains("#AddressCommission"))
                {
                    string broker = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.address_commission) ? dataDetail.chit_proposed_for_approve.address_commission : "-";
                    body = body.Replace("#AddressCommission", broker);
                }
                if (body.Contains("#NetFreight"))
                {
                    string netFreight = "";
                    double net_freight;
                    string netUnit = "";
                    if (!String.IsNullOrEmpty(purNo))
                    {
                        using (EntityCPAIEngine context = new EntityCPAIEngine())
                        {
                            var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_PURCHASE_NO == purNo);
                            if (chitDetail != null)
                            {
                                netUnit = chitDetail.IDAT_NEGOTIATION_UNIT;
                            }
                        }
                    }
                  
                    if (!String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.net_freight))
                    {
                        net_freight = Convert.ToDouble(dataDetail.chit_proposed_for_approve.net_freight);
                        netFreight = net_freight.ToString("#,##0.##") + " " + netUnit;
                    }
                    else
                    {
                        netFreight = " - ";
                    }
                    //string netFreight = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.net_freight) ? String.Format("{0:n0}", dataDetail.chit_proposed_for_approve.net_freight) : ;
                    body = body.Replace("#NetFreight", netFreight);
                }

                //if (body.Contains("#CargoName"))
                //{
                //    string netFreight = "";
                //    double net_freight;
                //    if (!String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.net_freight))
                //    {
                //        net_freight = Convert.ToDouble(dataDetail.chit_proposed_for_approve.net_freight);
                //        netFreight = net_freight.ToString("#,##0.##");
                //    }
                //    else
                //    {
                //        netFreight = " - ";
                //    }
                //    body = body.Replace("#NetFreight", netFreight);
                //}
                if (body.Contains("#CargoQty"))
                {
                    string cargoQty = "";
                    double cargo_Qty;
                    if (dataDetail.chit_detail.chit_cargo_qty != null)
                    { 
                        foreach (var item in dataDetail.chit_detail.chit_cargo_qty)
                        {
                            if(!String.IsNullOrEmpty(item.cargo))
                            {
                                using (EntityCPAIEngine context = new EntityCPAIEngine())
                                {
                                    var matDetail = context.MT_MATERIALS.SingleOrDefault(a => a.MET_NUM == item.cargo);
                                    if (matDetail != null)
                                    {
                                        item.cargo = matDetail.MET_MAT_DES_ENGLISH;
                                    }
                                }
                            }
                            
                            cargoQty += "<br> Cargo : " + item.cargo;
                            if(string.IsNullOrEmpty(item.qty))
                            {
                                cargoQty += "<br> Quantity : " + "-";
                            }
                            else
                            {
                                cargo_Qty = Convert.ToDouble(item.qty);
                                cargoQty += "<br> Quantity : " + cargo_Qty.ToString("#,##0.##") + " " + item.unit;
                            }                            
                        } 
                    }
                    else
                    {
                        cargoQty += "<br> Cargo : -";
                        cargoQty += "<br> Quantity : -";
                    }
                    body = body.Replace("#CargoQty", cargoQty);
                }
                var ss = dataDetail.chit_detail.chit_cargo_qty;

                if (body.Contains("#Demurrage"))
                {
                    string broker = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.demurrage) ? dataDetail.chit_proposed_for_approve.demurrage : "-";
                    body = body.Replace("#Demurrage", broker);
                }
                if (body.Contains("#owner"))
                {
                    string owner = !String.IsNullOrEmpty(dataDetail.chit_proposed_for_approve.broker_nameText) ? dataDetail.chit_proposed_for_approve.broker_nameText : "-";
                    body = body.Replace("#owner", owner);
                }
                if (body.Contains("#reason"))
                {
                    string reason = !String.IsNullOrEmpty(dataDetail.chit_reason) ? dataDetail.chit_reason : "-";
                    body = body.Replace("#reason", dataDetail.chit_reason);
                }

                if (body.Contains("#loading_port"))
                {
                    List<ChitLoadPortControl> load_port_ls = dataDetail.chit_detail.chit_load_port_control;
                    if (load_port_ls != null && load_port_ls.Count > 0)
                    {
                        string load_port = "";
                        foreach (ChitLoadPortControl temp_load_port in load_port_ls)
                        {
                            if (load_port_ls.Count == 1)
                            {
                                load_port += "<br> Loading Port : " + temp_load_port.PortName;
                            }
                            else
                            {
                                load_port += "<br> Loading Port " + temp_load_port.PortOrder + " : " + temp_load_port.PortName;
                            }
                        }
                        body = body.Replace("#loading_port", load_port);
                    }
                }

                if (body.Contains("#discharging_port"))
                {
                    List<ChitDischargePortControl> discharge_port_ls = dataDetail.chit_detail.chit_discharge_port_control;
                    if (discharge_port_ls != null && discharge_port_ls.Count > 0)
                    {
                        string dis_port = "";
                        foreach (ChitDischargePortControl temp_dis_port in discharge_port_ls)
                        {
                            if (discharge_port_ls.Count == 1)
                            {
                                dis_port += "<br> Discharging Port : " + temp_dis_port.PortName;
                            }
                            else
                            {
                                dis_port += "<br> Discharging Port " + temp_dis_port.PortOrder + " : " + temp_dis_port.PortName;
                            }
                        }
                        body = body.Replace("#discharging_port", dis_port);
                    }
                }

                string final_deal = string.Empty;
                string NegoUnit = " " + dataDetail.chit_detail.NegoUnit;
                foreach (ChitOffersItem of in dataDetail.chit_negotiation_summary.chit_offers_items)
                {
                    if (of.final_flag.Equals("Y"))
                    {
                        final_deal = of.final_deal;
                        List<ChitRoundInfo> rinfo = new List<ChitRoundInfo>();
                        foreach (ChitRoundItem rt in of.chit_round_items)
                        {
                            int no_before = 0;
                            int no = 0;
                            if (Int32.TryParse(rt.round_no, out no))
                            {
                                if (no > no_before)
                                {
                                    rinfo = rt.chit_round_info;
                                    Int32.TryParse(rt.round_no, out no_before);
                                }
                            }
                        }
                        string chi_detail = "";
                        if (body.Contains("#chi_detail"))
                        {

                            if (final_deal != null)
                            {
                                chi_detail = final_deal.Replace("\r\n", "<br>");
                                /* 10/10/2017 change to get from final deal bypoo 
                                if (rinfo != null && rinfo.Count > 0)
                                {
                                    foreach (ChitRoundInfo r in rinfo)
                                    {

                                        if (r.type.ToUpper().Equals("MIN LOADED"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + r.offer + " K Ton <br>";
                                        }
                                        else if (r.type.ToUpper().Equals("DEMURRAGE"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + NegoUnit + " PDPR <br>";
                                        }
                                        else if (r.type.ToUpper().Equals("WS"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " <br>";
                                        }
                                        else if (r.type.ToUpper().Equals("LAYTIME"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " Hrs <br> ";
                                        }
                                        else if (r.type.ToUpper().Equals("FREIGHT (LUMPSUM)"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + NegoUnit + " <br> ";
                                        }
                                        else if (r.type.ToUpper().Equals("FREIGHT (USD/BBL)"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " USD/BBL <br> ";
                                        }
                                        else if (r.type.ToUpper().Equals("FREIGHT (THB/LITRE)"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " THB/LITRE <br> ";
                                        }
                                        else if (r.type.ToUpper().Equals("FREIGHT (USD/MT)"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " USD/MT <br> ";
                                            chi_detail = chi_detail + "Total Freight: " + String.Format("{0:n}", double.Parse(dataDetail.chit_proposed_for_approve.total_freight)) + " USD <br> ";
                                        }
                                        else if (r.type.ToUpper().Equals("OTHERS"))
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " <br> ";
                                        }
                                        else
                                        {
                                            chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " <br>";
                                        }
                                    }*/

                            }
                            else
                            {
                                chi_detail = "not found final deal";
                            }
                            body = body.Replace("#chi_detail", chi_detail);
                        }
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }
            return body;
        }

        public string getBodyF10000026(string body, string json)
        {
            /*
            
                Vessel name: BRISTISH CORMORANT 
                Broker : Itochu Corporation
                Flat Rate : 29
                At Year : 2016
                Freight Type: OTHERS
                Charterer : Navig8
                Period : 03-Apr-2017 to 06-Apr-2017
                Vessel Laycan: 03-Apr-2017 to 06-Apr-2017

                No Charter Out Total Expense: 1,435,130.00 USD
                Charter Out Total Expense : 2,435,130.00 USD
                Net Benefit : 435,130.00 USD

               x<br>Vessel name: #vessel
               x<br>Charterer : #charterer
               x<br>Vessel Laycan: #laycan 
               x<br>Broker :#broker
               x<br>Flat Rate :#flatRate
              x <br>At Year :#atYear
               x<br>Freight Type :#freightType
               x<br>Period :#period<br>
               <br>No Charter Out Total Expense : #nc
               <br>Charter Out Total Expense : #co
               <br>Net Benefit : #benefit
               <br>Requested by: #create_by
            */

            try
            {


                ChosRootObject dataDetail = JSonConvertUtil.jsonToModel<ChosRootObject>(json);

                if (body.Contains("#laycan"))
                {
                    String date_from = dataDetail.chos_proposed_for_approve.laycan_from != null ? dataDetail.chos_proposed_for_approve.laycan_from : "";
                    String date_to = dataDetail.chos_proposed_for_approve.laycan_to != null ? dataDetail.chos_proposed_for_approve.laycan_to : "";
                    String date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#laycan", date);
                }
                if (body.Contains("#vessel"))
                {
                    string vessel = dataDetail.chos_evaluating.vessel_name;
                    body = body.Replace("#vessel", vessel);
                }
                if (body.Contains("#broker"))
                {
                    string broker = dataDetail.chos_evaluating.broker_name;
                    body = body.Replace("#broker", broker);
                }
                if (body.Contains("#charterer"))
                {
                    string charterer = dataDetail.chos_proposed_for_approve.charterer_name;
                    body = body.Replace("#charterer", charterer);
                }
                if (body.Contains("#flatRate"))
                {
                    string flat_rate = dataDetail.chos_evaluating.flat_rate;
                    body = body.Replace("#flatRate", flat_rate);
                }
                if (body.Contains("#atYear"))
                {
                    string atYear = dataDetail.chos_evaluating.year;
                    body = body.Replace("#atYear", atYear);
                }
                if (body.Contains("#freightType"))
                {
                    body = body.Replace("#freightType", dataDetail.chos_evaluating.freight + " " + dataDetail.chos_evaluating.flat_rate);
                }
                if (body.Contains("#period"))

                {
                    String period_date_from = dataDetail.chos_evaluating.period_date_from != null ? dataDetail.chos_evaluating.period_date_from : "";
                    String period_date_to = dataDetail.chos_evaluating.period_date_to != null ? dataDetail.chos_evaluating.period_date_to : "";
                    String period_date = "";
                    ShareFn sf = new ShareFn();
                    if (period_date_to.Equals(""))
                    {
                        period_date_from = period_date_from != null ? ShareFn.ConvertStringDateToDateFormat(period_date_from, "dd/MM/yyyy", "dd-MMM-yyyy") : "";
                        period_date = period_date_from;
                    }
                    else
                    {
                        period_date_from = period_date_from != null ? ShareFn.ConvertStringDateToDateFormat(period_date_from, "dd/MM/yyyy", "dd-MMM-yyyy") : "";
                        period_date_to = period_date_to != null ? ShareFn.ConvertStringDateToDateFormat(period_date_to, "dd/MM/yyyy", "dd-MMM-yyyy") : "";
                        period_date = period_date_from + " to " + period_date_to;
                    }
                    body = body.Replace("#period", period_date);
                }
                string unit = dataDetail.chos_evaluating.unit != null ? dataDetail.chos_evaluating.unit : "USD";
                if (body.Contains("#nc"))
                {
                    // 2,435,130.00 USD
                    var total_expense_no_charter = dataDetail.chos_benefit.chos_total_expense_no_charter != null ? String.Format("{0:n}", double.Parse(dataDetail.chos_benefit.chos_total_expense_no_charter)) : "";
                    body = body.Replace("#nc", total_expense_no_charter + " " + unit);
                }

                if (body.Contains("#co"))
                {
                    // 2,435,130.00 USD
                    var chos_total_expense = dataDetail.chos_benefit.chos_total_expense != null ? String.Format("{0:n}", double.Parse(dataDetail.chos_benefit.chos_total_expense)) : "";
                    body = body.Replace("#co", chos_total_expense + " " + unit);
                }

                if (body.Contains("#benefit"))
                {
                    // 2,435,130.00 USD
                    var chos_net_benefit = dataDetail.chos_benefit.chos_net_benefit != null ? String.Format("{0:n}", double.Parse(dataDetail.chos_benefit.chos_net_benefit)) : "";
                    body = body.Replace("#benefit", chos_net_benefit + " " + unit);
                }

                if (body.Contains("#reason"))
                {
                    body = body.Replace("#reason", dataDetail.chos_reason);
                }

            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }

            return body;
        }
        public string getBodyF10000023(string body, string json)
        {
            /*
            Crude name: Murban
            Supplier: PTT
            Volume: 500 kbbl
            Incoterm: FOB
            Purchasing price: OSP+0.07 USD/bbl
            Market price: OSP+0.10 USD/bbl
            LP max price: OSP+0.20 USD/bbl
            Benefit vs LP max price: 0.13 USD/bbl or 65,000 USD
            Loading period: May'17
            Discharging period: -

            CdpRootObject
                #crude_name 
                #supplier                
                #volume
                #incoterm 
                #purchas_p 
                #market_p 
                #lp_p 
                #ben_p
                #load_period
                #discharg_period
            */
            try
            {
                CdpRootObject dataDetail = JSonConvertUtil.jsonToModel<CdpRootObject>(json);

                //crude_name
                if (body.Contains("#crude_name"))
                {
                    body = body.Replace("#crude_name", string.IsNullOrEmpty(dataDetail.crude_name_others) ? dataDetail.crude_name : dataDetail.crude_name_others);
                }
                string supplier = string.Empty;
                string volume = string.Empty;
                string incoterm = string.Empty;
                string purchas_p = string.Empty;
                string market_p = string.Empty;
                string lp_p = string.Empty;
                string bechmark_price_p = string.Empty;
                string magin_unit = "";
                foreach (CdpOffersItem of in dataDetail.offers_items)
                {
                    if (of.final_flag.Equals("Y"))
                    {
                        supplier = of.supplier_name;
                        if (of.quantity.Equals(of.quantity_max))
                        {
                            volume = String.Format("{0:n0}", Convert.ToDecimal(of.quantity)) + " " + String.Format("{0:n}", of.quantity_unit);
                        }
                        else
                        {
                            volume = String.Format("{0:n0}", Convert.ToDecimal(of.quantity)) + " - " + String.Format("{0:n0}", Convert.ToDecimal(of.quantity_max)) + " " + of.quantity_unit;
                        }
                        incoterm = of.incoterms;
                        //
                        //double t1 = of.market_source != null ? double.Parse(of.market_source) : 0;
                        //double t2 = of.market_source != null ? double.Parse(of.latest_lp_plan_price) : 0;
                        //
                        bechmark_price_p = of.bechmark_price;
                        magin_unit = of.margin_unit;
                        //
                        /*
                        if (!String.IsNullOrEmpty(of.market_source))
                        {
                            // var t1 = !String.IsNullOrEmpty(of.market_source) ? double.Parse(of.market_source) : 0;
                            //var t2 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(of.latest_lp_plan_price) ? double.Parse(of.latest_lp_plan_price) : 0) : 0;
                            //market_p = bechmark_price_p + ((t1 > 0) ? "+" + String.Format("{0:n}", t1) : String.Format("{0:n}", t1)) + " " + magin_unit;
                            //lp_p = bechmark_price_p + ((t2 > 0) ? "+" + String.Format("{0:n}", t2) : String.Format("{0:n}", t2)) + " " + magin_unit;

                            var t1 = !String.IsNullOrEmpty(of.market_source) ?of.market_source : "";
                            var t2 = !String.IsNullOrEmpty(of.latest_lp_plan_price) ? of.latest_lp_plan_price : "";
                            market_p = !String.IsNullOrEmpty(t1) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t1))) + " " + magin_unit : "n/a";
                            lp_p     = !String.IsNullOrEmpty(t2) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t2))) + " " + magin_unit : "n/a";
                        }
                        else
                        {
                            var t1 = !String.IsNullOrEmpty(of.market_source) ? of.market_source  : "";
                            var t2 = !String.IsNullOrEmpty(of.latest_lp_plan_price) ? of.latest_lp_plan_price : "";
                            market_p = !String.IsNullOrEmpty(t1) ? bechmark_price_p + " " + t1  + " " + magin_unit : "n/a";
                            lp_p = !String.IsNullOrEmpty(t2) ? bechmark_price_p + " " + t2 + " " + magin_unit : "n/a";
                        }
                        */

                        //edit 7/6/2017 by pan : LR,VGO
                        var t1 = !String.IsNullOrEmpty(of.market_source) ? of.market_source : "";
                        var t2 = !String.IsNullOrEmpty(of.latest_lp_plan_price) ? of.latest_lp_plan_price : "";
                        market_p = !String.IsNullOrEmpty(t1) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t1))) + " " + magin_unit : "N/A";
                        lp_p = !String.IsNullOrEmpty(t2) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t2))) + " " + magin_unit : "N/A";

                        //round max
                        foreach (CdpRoundItem rt in of.round_items)
                        {
                            int no_before = 0;
                            int no = 0;
                            if (Int32.TryParse(rt.round_no, out no))
                            {
                                if (no > no_before)
                                {
                                    /*
                                    if (!String.IsNullOrEmpty(of.market_source))
                                    {
                                        var t3 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(rt.round_value[0]) ? double.Parse(rt.round_value[0]) : 0) : 0;
                                        purchas_p = bechmark_price_p + ((t3 > 0) ? "+" + String.Format("{0:n}", t3) : String.Format("{0:n}", t3)) + " " + magin_unit;
                                    }
                                    else
                                    {
                                        var t3 = !String.IsNullOrEmpty(of.market_source) ? (!String.IsNullOrEmpty(rt.round_value[0]) ? rt.round_value[0] : "") : "";
                                        purchas_p = bechmark_price_p + " "+ t3 + " " + magin_unit;
                                    }
                                    */

                                    //edit 7/6/2017 by pan : LR,VGO
                                    var t3 = !String.IsNullOrEmpty(rt.round_value[0]) ? rt.round_value[0] : "";

                                    purchas_p = !String.IsNullOrEmpty(t3) ? bechmark_price_p + ("+" + String.Format("{0:n}", double.Parse(t3))) + " " + magin_unit : "N/A";

                                    Int32.TryParse(rt.round_no, out no_before);
                                }
                            }
                        }
                        break;
                    }
                }
                if (body.Contains("#supplier"))
                {
                    body = body.Replace("#supplier", supplier);
                }
                if (body.Contains("#volume"))
                {
                    body = body.Replace("#volume", volume);
                }
                if (body.Contains("#incoterm"))
                {
                    body = body.Replace("#incoterm", incoterm);
                }
                if (body.Contains("#purchas_p"))
                {
                    body = body.Replace("#purchas_p", purchas_p);
                }
                if (body.Contains("#market_p"))

                {
                    body = body.Replace("#market_p", market_p);
                }
                if (body.Contains("#lp_p"))
                {
                    body = body.Replace("#lp_p", lp_p);
                }
                //margin_lp
                if (body.Contains("#ben_p"))
                {

                    string ben_p = string.Empty;
                    ben_p = dataDetail.proposal.reason_ref2_bbl + " " + CPAIConstantUtil.USD_bbl + " or " + dataDetail.proposal.reason_ref2_us + " " + CPAIConstantUtil.currency_symbol; //**0.13 USD/bbl or 65,000 USD
                    body = body.Replace("#ben_p", ben_p);
                }

                if (body.Contains("#load_period"))

                {
                    String date_from = dataDetail.loading_period.date_from != null ? dataDetail.loading_period.date_from : "";
                    String date_to = dataDetail.loading_period.date_to != null ? dataDetail.loading_period.date_to : "";
                    String date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "-";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "-";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "-";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#load_period", date);

                }
                if (body.Contains("#discharg_period"))
                {
                    String date_from = dataDetail.discharging_period.date_from != null ? dataDetail.discharging_period.date_from : "";
                    String date_to = dataDetail.discharging_period.date_to != null ? dataDetail.discharging_period.date_to : "";
                    String date = "";
                    ShareFn sf = new ShareFn();
                    if (date_to.Equals(""))
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date = date_from;
                    }
                    else
                    {
                        date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                        date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                        date = date_from + " to " + date_to;
                    }
                    body = body.Replace("#discharg_period", date);
                }
                if (body.Contains("#reason"))
                {
                    body = body.Replace("#reason", dataDetail.reason);
                }
                if (body.Contains("#credit_condition"))
                {
                    string bond = !String.IsNullOrEmpty(dataDetail.proposal.performance_bond) ? dataDetail.proposal.performance_bond : "-";
                    body = body.Replace("#credit_condition", bond);
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }

            return body;
        }

        public string getSubjectF10000040(string subject, string json)
        {
            CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(json);

            if (subject.Contains("#crude_name"))
            {
                subject = subject.Replace("#crude_name", dataDetail.data.crude_name);
            }
            if (subject.Contains("#country"))
            {
                subject = subject.Replace("#country", dataDetail.data.country);
            }
            if (subject.Contains("#assay_ref"))
            {
                subject = subject.Replace("#assay_ref", dataDetail.data.assay_ref);
            }

            return subject;
        }

        public string getSubjectF10000041(string subject, string json)
        {
            CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(json);

            if (subject.Contains("#crude_name"))
            {
                subject = subject.Replace("#crude_name", dataDetail.data.crude_name);
            }
            if (subject.Contains("#assay_ref"))
            {
                subject = subject.Replace("#assay_ref", dataDetail.data.assay_ref);
            }

            return subject;
        }

        public string getBodyF10000040(string body, string json)
        {
            /*
            CooRootObject
                #crude_name 
                #country                
                #approve_date
                #assay_ref 
            */
            try
            {
                CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(json);

                //crude_name
                if (body.Contains("#crude_name"))
                {
                    body = body.Replace("#crude_name", dataDetail.data.crude_name);
                }
                if (body.Contains("#country"))
                {
                    body = body.Replace("#country", dataDetail.data.country);
                }
                if (body.Contains("#date"))
                {
                    body = body.Replace("#date", dataDetail.data.approval_date);
                }
                if (body.Contains("#assay_ref"))
                {
                    body = body.Replace("#assay_ref", dataDetail.data.assay_ref);
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }
            return body;
        }

        public string getBodyF10000041(string body, string json)
        {
            /*
            CooRootObject
                #crude_name 
                #country                
                #approve_date
                #assay_ref 
            */
            try
            {
                CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(json);

                //crude_name
                if (body.Contains("#crude_name"))
                {
                    body = body.Replace("#crude_name", dataDetail.data.crude_name);
                }
                if (body.Contains("#country"))
                {
                    body = body.Replace("#country", dataDetail.data.country);
                }
                if (body.Contains("#date"))
                {
                    body = body.Replace("#date", dataDetail.data.approval_date);
                }
                if (body.Contains("#assay_ref"))
                {
                    body = body.Replace("#assay_ref", dataDetail.data.assay_ref);
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }
            return body;
        }

        //public string getsubjectF10000037(string subject,string json,string deal_no)
        //{
        //    HedgDealRootObject dataDetail = JSonConvertUtil.jsonToModel<HedgDealRootObject>(json);
        //    if (subject.Contains("#dealno"))
        //    {
        //        subject = subject.Replace("#dealno", deal_no);
        //    }
        //    if (subject.Contains("#dealstatus"))
        //    {
        //        subject = subject.Replace("#dealstatus", dataDetail.hedging_deal.status);
        //    }
        //    return subject;
        //}

        //public string getBodyF10000037(string body,string json,string deal_no)
        //{
        //    HedgDealRootObject dataDetail  = JSonConvertUtil.jsonToModel<HedgDealRootObject>(json);
        //    if (body.Contains("#dealno"))
        //    {
        //        body = body.Replace("#dealno", deal_no);
        //    }
        //    if (body.Contains("#dealstatus"))
        //    {
        //        body = body.Replace("#dealstatus", dataDetail.hedging_deal.status);
        //    }
        //    return body;
        //}


        public string getSubjectF10000063(string subject, string json)
        {
            /*
             #crude_name : crude name
             #loading_period : Loading date from - Loading date to 
            */
            VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(json);
            if (subject.Contains("#crude_name"))
            {
                subject = subject.Replace("#crude_name", dataDetail.crude_info.crude_name);
            }
            if (subject.Contains("#loading_period"))
            {
                subject = subject.Replace("#loading_period", dataDetail.crude_info.loading_date_from + " to " + dataDetail.crude_info.loading_date_to);
            }
            return subject;
        }

        public string getBodyF10000063(string body, string json)
        {
            /*
             #crude_name : crude name

             #loading_period : Loading date from - Loading date to
             #tpc_month : TPC Month
             #tpc_year : TPC Year

             #CommentSCVP : Comment from SCVP
             #CommentCMVP : Comment from CMVP
             #CommentTNVP : Comment from TNVP
             #maximun_premium_price : Maximum buying price
             #premiun_price : Propose price
             #comment_from_evpc : Comment from EVPC
            */
            try
            {
                VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(json);
                if (body.Contains("#crude_name"))
                {
                    body = body.Replace("#crude_name", String.IsNullOrEmpty(dataDetail.crude_info.crude_name) ? "-" : dataDetail.crude_info.crude_name);
                }
                if (body.Contains("#CommentSCVP"))
                {
                    body = body.Replace("#CommentSCVP", String.IsNullOrEmpty(dataDetail.comment.scvp) ? "-" : dataDetail.comment.scvp);
                }
                if (body.Contains("#CommentCMVP"))
                {
                    body = body.Replace("#CommentCMVP", String.IsNullOrEmpty(dataDetail.comment.cmvp) ? "-" : dataDetail.comment.cmvp);
                }
                if (body.Contains("#CommentTNVP"))
                {
                    body = body.Replace("#CommentTNVP", String.IsNullOrEmpty(dataDetail.comment.tnvp) ? "-" : dataDetail.comment.tnvp);
                }
                if (body.Contains("#comment_from_evpc"))
                {
                    body = body.Replace("#comment_from_evpc", String.IsNullOrEmpty(dataDetail.comment.evpc) ? "-" : dataDetail.comment.evpc);
                }
                if (body.Contains("#maximun_premium_price"))
                {
                    body = body.Replace("#maximun_premium_price", String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum) ? "-" : dataDetail.crude_info.premium_maximum);
                }
                if (body.Contains("#premiun_price"))
                {
                    body = body.Replace("#premiun_price", String.IsNullOrEmpty(dataDetail.crude_info.premium) ? "-" : dataDetail.crude_info.premium);
                }
                if (body.Contains("#tpc_month"))
                {
                    body = body.Replace("#tpc_month", String.IsNullOrEmpty(dataDetail.crude_info.tpc_plan_month) ? "-" : dataDetail.crude_info.tpc_plan_month);
                }
                if (body.Contains("#tpc_year"))
                {
                    body = body.Replace("#tpc_year", String.IsNullOrEmpty(dataDetail.crude_info.tpc_plan_year) ? "-" : dataDetail.crude_info.tpc_plan_year);
                }
                if (body.Contains("#loading_period"))
                {
                    body = body.Replace("#loading_period", dataDetail.crude_info.loading_date_from + " to " + dataDetail.crude_info.loading_date_to);
                }

                if (body.Contains("#quantity"))
                {
                    body = body.Replace("#quantity", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kbbl_max) ? "-" : dataDetail.crude_info.quantity_kbbl_max);
                }
                if (body.Contains("#incoterm"))
                {
                    body = body.Replace("#incoterm", String.IsNullOrEmpty(dataDetail.crude_info.incoterm) ? "-" : dataDetail.crude_info.incoterm);
                }
                if (body.Contains("#benchmark"))
                {
                    body = body.Replace("#benchmark", String.IsNullOrEmpty(dataDetail.crude_info.benchmark_price) ? "-" : dataDetail.crude_info.benchmark_price);
                }
                if (body.Contains("#offerNegotiationRound"))
                {
                    body = body.Replace("#offerNegotiationRound", String.IsNullOrEmpty(dataDetail.crude_info.formula_price) ? "-" : dataDetail.crude_info.formula_price);
                }
                if (body.Contains("#maximumPurchasingPrice"))
                {
                    body = body.Replace("#maximumPurchasingPrice", String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum) ? "-" : dataDetail.crude_info.premium_maximum);
                }
                if (body.Contains("#loadingPeriodFrom"))
                {
                    body = body.Replace("#loadingPeriodFrom", String.IsNullOrEmpty(dataDetail.crude_info.loading_date_from) ? "-" : dataDetail.crude_info.loading_date_from);
                }
                if (body.Contains("#loadingPeriodTo"))
                {
                    body = body.Replace("#loadingPeriodTo", String.IsNullOrEmpty(dataDetail.crude_info.loading_date_to) ? "-" : dataDetail.crude_info.loading_date_to);
                }
                if (body.Contains("#dischargingPeriodTo"))
                {
                    body = body.Replace("#dischargingPeriodTo", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#dischargingPeriodFrom"))
                {
                    body = body.Replace("#dischargingPeriodFrom", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }

                if (body.Contains("#name"))
                {
                    body = body.Replace("#name", String.IsNullOrEmpty(dataDetail.requester_info.name) ? "-" : dataDetail.requester_info.name);
                }
                if (body.Contains("#reject_reason_from_SCVP "))
                {
                    body = body.Replace("#reject_reason_from_SCVP ", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCVP) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCVP)));
                }
                if (body.Contains("#reject_reason_from_CMCS_SH "))
                {
                    body = body.Replace("#reject_reason_from_CMCS_SH ", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_CMCS_SH) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_CMCS_SH)));
                }
                if (body.Contains("#reasonSCSCS_H"))
                {
                    body = body.Replace("#reasonSCSCS_H", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCSC_SH) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCSC_SH)));
                }
                if (body.Contains("#agree"))
                {
                    body = body.Replace("#agree", dataDetail.comment.scsc_agree_flag == "AGREE" ? "agree" : "disgree");
                }

                if (body.Contains("#CMCSloadingDischargingTo"))
                {
                    body = body.Replace("#CMCSloadingDischargingTo", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#CMCSloadingDischargingFrom"))
                {
                    body = body.Replace("#CMCSloadingDischargingFrom", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }
                if (body.Contains("#SCSCloadingDischargingTo"))
                {
                    body = body.Replace("#SCSCloadingDischargingTo", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_to) ? "-" : dataDetail.crude_info.revised_date_to);
                }
                if (body.Contains("#SCSCloadingDischargingFrom"))
                {
                    body = body.Replace("#SCSCloadingDischargingFrom", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_from) ? "-" : dataDetail.crude_info.revised_date_from);
                }

                if (body.Contains("#dischargingReasonSCSCS_H"))
                {
                    body = body.Replace("#dischargingReasonSCSCS_H", String.IsNullOrEmpty(dataDetail.comment.cmcs_request) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.comment.cmcs_request)));
                }
                if (body.Contains("#commentFromTNPB"))
                {
                    body = body.Replace("#commentFromTNPB", String.IsNullOrEmpty(dataDetail.comment.tn) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.comment.tn)));
                }
                if (body.Contains("#reject_reason_from_TNVP"))
                {
                    body = body.Replace("#reject_reason_from_TNVP", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_TNVP) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_TNVP)));
                }
                if (body.Contains("#rejectReasonSCEP"))
                {
                    body = body.Replace("#rejectReasonSCEP", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCEP_SH)));
                }
                if (body.Contains("#finalPurchasingPrice"))
                {
                    body = body.Replace("#finalPurchasingPrice", String.IsNullOrEmpty(dataDetail.crude_info.final_premium) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.crude_info.final_premium)));
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }
            return body;
        }

        private static string StripHTML(string input)
        {
            return Regex.Replace(input.Replace("&nbsp;", ""), "<.*?>", String.Empty);
        }


        public string getSubjectF10000064(string subject, string json)
        {
            /*
             #crude_name : crude name
             #loading_period : Loading date from - Loading date to 
            */

            VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(json);
            if (subject.Contains("#crude_name"))
            {
                subject = subject.Replace("#crude_name", dataDetail.crude_info.crude_name);
            }
            if (subject.Contains("#loading_period"))
            {
                subject = subject.Replace("#loading_period", dataDetail.crude_info.loading_date_from + " to " + dataDetail.crude_info.loading_date_to);
            }
            return subject;
        }

        public string getBodyF10000064(string body, string json)
        {
            /*
             #crude_name : crude name
             #loading_period : Loading date from - Loading date to
             #tpc_month : TPC Month
             #tpc_year : TPC Year

             #eta_date_from : ETA Date From
             #eta_date_to : ETA Date To 
             #revise_date_from : Revise Date From 
             #revise_date_to : Revise Date To

             #volumn_kt : volume (kT)
             #volume_kbbl : volume (kbbl)

             #comment_to_cmcs : Comment to CMCS
            */
            try
            {
                VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(json);
                if (body.Contains("#crude_name"))
                {
                    body = body.Replace("#crude_name", String.IsNullOrEmpty(dataDetail.crude_info.crude_name) ? "-" : dataDetail.crude_info.crude_name);
                }
                if (body.Contains("#CommentSCVP"))
                {
                    body = body.Replace("#CommentSCVP", String.IsNullOrEmpty(dataDetail.comment.scvp) ? "-" : dataDetail.comment.scvp);
                }
                if (body.Contains("#CommentCMVP"))
                {
                    body = body.Replace("#CommentCMVP", String.IsNullOrEmpty(dataDetail.comment.cmvp) ? "-" : dataDetail.comment.cmvp);
                }
                if (body.Contains("#CommentTNVP"))
                {
                    body = body.Replace("#CommentTNVP", String.IsNullOrEmpty(dataDetail.comment.tnvp) ? "-" : dataDetail.comment.tnvp);
                }
                if (body.Contains("#eta_date_from"))
                {
                    body = body.Replace("#eta_date_from", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }
                if (body.Contains("#eta_date_to"))
                {
                    body = body.Replace("#eta_date_to", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#revise_date_from"))
                {
                    body = body.Replace("#revise_date_from", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_from) ? "-" : dataDetail.crude_info.revised_date_from);
                }
                if (body.Contains("#revise_date_to"))
                {
                    body = body.Replace("#revise_date_to", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_to) ? "-" : dataDetail.crude_info.revised_date_to);
                }
                if (body.Contains("#comment_to_cmcs"))
                {
                    body = body.Replace("#comment_to_cmcs", String.IsNullOrEmpty(dataDetail.comment.cmcs_request) ? "-" : dataDetail.comment.cmcs_request);
                }
                if (body.Contains("#tpc_month"))
                {
                    body = body.Replace("#tpc_month", String.IsNullOrEmpty(dataDetail.crude_info.tpc_plan_month) ? "-" : dataDetail.crude_info.tpc_plan_month);
                }
                if (body.Contains("#tpc_year"))
                {
                    body = body.Replace("#tpc_year", String.IsNullOrEmpty(dataDetail.crude_info.tpc_plan_year) ? "-" : dataDetail.crude_info.tpc_plan_year);
                }
                if (body.Contains("#volumn_kt"))
                {
                    body = body.Replace("#volumn_kt", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kt_max) ? "-" : dataDetail.crude_info.quantity_kt_max);
                }
                if (body.Contains("#volume_kbbl"))
                {
                    body = body.Replace("#volume_kbbl", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kbbl_max) ? "-" : dataDetail.crude_info.quantity_kbbl_max);
                }
                if (body.Contains("#loading_period"))
                {
                    body = body.Replace("#loading_period", dataDetail.crude_info.loading_date_from + " to " + dataDetail.crude_info.loading_date_to);
                }

                if (body.Contains("#quantity"))
                {
                    body = body.Replace("#quantity", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kbbl_max) ? "-" : dataDetail.crude_info.quantity_kbbl_max);
                }
                if (body.Contains("#incoterm"))
                {
                    body = body.Replace("#incoterm", String.IsNullOrEmpty(dataDetail.crude_info.incoterm) ? "-" : dataDetail.crude_info.incoterm);
                }
                if (body.Contains("#benchmark"))
                {
                    body = body.Replace("#benchmark", String.IsNullOrEmpty(dataDetail.crude_info.benchmark_price) ? "-" : dataDetail.crude_info.benchmark_price);
                }
                if (body.Contains("#offerNegotiationRound"))
                {
                    body = body.Replace("#offerNegotiationRound", String.IsNullOrEmpty(dataDetail.crude_info.formula_price) ? "-" : dataDetail.crude_info.formula_price);
                }
                if (body.Contains("#maximumPurchasingPrice"))
                {
                    body = body.Replace("#maximumPurchasingPrice", String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum) ? "-" : dataDetail.crude_info.premium_maximum);
                }
                if (body.Contains("#loadingPeriodFrom"))
                {
                    body = body.Replace("#loadingPeriodFrom", String.IsNullOrEmpty(dataDetail.crude_info.loading_date_from) ? "-" : dataDetail.crude_info.loading_date_from);
                }
                if (body.Contains("#loadingPeriodTo"))
                {
                    body = body.Replace("#loadingPeriodTo", String.IsNullOrEmpty(dataDetail.crude_info.loading_date_to) ? "-" : dataDetail.crude_info.loading_date_to);
                }
                if (body.Contains("#dischargingPeriodTo"))
                {
                    body = body.Replace("#dischargingPeriodTo", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#dischargingPeriodFrom"))
                {
                    body = body.Replace("#dischargingPeriodFrom", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }

                if (body.Contains("#name"))
                {
                    body = body.Replace("#name", String.IsNullOrEmpty(dataDetail.requester_info.name) ? "-" : dataDetail.requester_info.name);
                }
                if (body.Contains("#reject_reason_from_SCVP "))
                {
                    body = body.Replace("#reject_reason_from_SCVP ", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCVP) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCVP)));
                }
                if (body.Contains("#reasonSCSCS_H"))
                {
                    body = body.Replace("#reasonSCSCS_H", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCSC_SH) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCSC_SH)));
                }
                if (body.Contains("#agree"))
                {
                    body = body.Replace("#agree", dataDetail.comment.scsc_agree_flag == "AGREE" ? "agree" : "disgree");
                }

                if (body.Contains("#CMCSloadingDischargingTo"))
                {
                    body = body.Replace("#CMCSloadingDischargingTo", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#CMCSloadingDischargingFrom"))
                {
                    body = body.Replace("#CMCSloadingDischargingFrom", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }
                if (body.Contains("#SCSCloadingDischargingTo"))
                {
                    body = body.Replace("#SCSCloadingDischargingTo", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_to) ? "-" : dataDetail.crude_info.revised_date_to);
                }
                if (body.Contains("#SCSCloadingDischargingFrom"))
                {
                    body = body.Replace("#SCSCloadingDischargingFrom", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_from) ? "-" : dataDetail.crude_info.revised_date_from);
                }

                if (body.Contains("#dischargingReasonSCSCS_H"))
                {
                    body = body.Replace("#dischargingReasonSCSCS_H", String.IsNullOrEmpty(dataDetail.comment.cmcs_request) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.comment.cmcs_request)));
                }
                if (body.Contains("#commentFromTNPB"))
                {
                    body = body.Replace("#commentFromTNPB", String.IsNullOrEmpty(dataDetail.comment.tn) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.comment.tn)));
                }
                if (body.Contains("#reject_reason_from_TNVP"))
                {
                    body = body.Replace("#reject_reason_from_TNVP", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_TNVP) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_TNVP)));
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }

            return body;
        }

        public string getSubjectF10000065(string subject, string json)
        {
            /*
             #crude_name : crude name
             #loading_period : Loading date from - Loading date to 
            */

            VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(json);
            if (subject.Contains("#crude_name"))
            {
                subject = subject.Replace("#crude_name", dataDetail.crude_info.crude_name);
            }
            if (subject.Contains("#loading_period"))
            {
                subject = subject.Replace("#loading_period", dataDetail.crude_info.loading_date_from + " to " + dataDetail.crude_info.loading_date_to);
            }

            return subject;
        }

        public string getBodyF10000065(string body, string json)
        {
            /*
             #crude_name : crude name
             #loading_period : Loading date from - Loading date to
             #tpc_month : TPC Month
             #tpc_year : TPC Year
             #eta_date_from : ETA Date From
             #eta_date_to   : ETA Date To
            */
            try
            {
                VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(json);
                if (body.Contains("#crude_name"))
                {
                    body = body.Replace("#crude_name", String.IsNullOrEmpty(dataDetail.crude_info.crude_name) ? "-" : dataDetail.crude_info.crude_name);
                }
                if (body.Contains("#eta_date_from"))
                {
                    body = body.Replace("#eta_date_from", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }
                if (body.Contains("#eta_date_to"))
                {
                    body = body.Replace("#eta_date_to", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#tpc_month"))
                {
                    body = body.Replace("#tpc_month", String.IsNullOrEmpty(dataDetail.crude_info.tpc_plan_month) ? "-" : dataDetail.crude_info.tpc_plan_month);
                }
                if (body.Contains("#tpc_year"))
                {
                    body = body.Replace("#tpc_year", String.IsNullOrEmpty(dataDetail.crude_info.tpc_plan_year) ? "-" : dataDetail.crude_info.tpc_plan_year);
                }
                if (body.Contains("#loading_period"))
                {
                    body = body.Replace("#loading_period", dataDetail.crude_info.loading_date_from + " to " + dataDetail.crude_info.loading_date_to);
                }
                if (body.Contains("#volumn_kt"))
                {
                    body = body.Replace("#volumn_kt", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kt_max) ? "-" : dataDetail.crude_info.quantity_kt_max);
                }
                if (body.Contains("#volume_kbbl"))
                {
                    body = body.Replace("#volume_kbbl", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kbbl_max) ? "-" : dataDetail.crude_info.quantity_kbbl_max);
                }

                if (body.Contains("#quantity"))
                {
                    body = body.Replace("#quantity", String.IsNullOrEmpty(dataDetail.crude_info.quantity_kbbl_max) ? "-" : dataDetail.crude_info.quantity_kbbl_max);
                }
                if (body.Contains("#incoterm"))
                {
                    body = body.Replace("#incoterm", String.IsNullOrEmpty(dataDetail.crude_info.incoterm) ? "-" : dataDetail.crude_info.incoterm);
                }
                if (body.Contains("#benchmark"))
                {
                    body = body.Replace("#benchmark", String.IsNullOrEmpty(dataDetail.crude_info.benchmark_price) ? "-" : dataDetail.crude_info.benchmark_price);
                }
                if (body.Contains("#offerNegotiationRound"))
                {
                    body = body.Replace("#offerNegotiationRound", String.IsNullOrEmpty(dataDetail.crude_info.formula_price) ? "-" : dataDetail.crude_info.formula_price);
                }
                if (body.Contains("#maximumPurchasingPrice"))
                {
                    body = body.Replace("#maximumPurchasingPrice", String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum) ? "-" : dataDetail.crude_info.premium_maximum);
                }
                if (body.Contains("#loadingPeriodFrom"))
                {
                    body = body.Replace("#loadingPeriodFrom", String.IsNullOrEmpty(dataDetail.crude_info.loading_date_from) ? "-" : dataDetail.crude_info.loading_date_from);
                }
                if (body.Contains("#loadingPeriodTo"))
                {
                    body = body.Replace("#loadingPeriodTo", String.IsNullOrEmpty(dataDetail.crude_info.loading_date_to) ? "-" : dataDetail.crude_info.loading_date_to);
                }
                if (body.Contains("#dischargingPeriodTo"))
                {
                    body = body.Replace("#dischargingPeriodTo", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#dischargingPeriodFrom"))
                {
                    body = body.Replace("#dischargingPeriodFrom", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }

                if (body.Contains("#name"))
                {
                    body = body.Replace("#name", String.IsNullOrEmpty(dataDetail.requester_info.name) ? "-" : dataDetail.requester_info.name);
                }
                if (body.Contains("#reject_reason_from_SCVP "))
                {
                    body = body.Replace("#reject_reason_from_SCVP ", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCVP) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCVP)));
                }
                if (body.Contains("#reasonSCSCS_H"))
                {
                    body = body.Replace("#reasonSCSCS_H", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCSC_SH) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_SCSC_SH)));
                }
                if (body.Contains("#agree"))
                {
                    body = body.Replace("#agree", dataDetail.comment.scsc_agree_flag == "AGREE" ? "agree" : "disgree");
                }

                if (body.Contains("#CMCSloadingDischargingTo"))
                {
                    body = body.Replace("#CMCSloadingDischargingTo", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to) ? "-" : dataDetail.crude_info.discharging_date_to);
                }
                if (body.Contains("#CMCSloadingDischargingFrom"))
                {
                    body = body.Replace("#CMCSloadingDischargingFrom", String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from) ? "-" : dataDetail.crude_info.discharging_date_from);
                }
                if (body.Contains("#SCSCloadingDischargingTo"))
                {
                    body = body.Replace("#SCSCloadingDischargingTo", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_to) ? "-" : dataDetail.crude_info.revised_date_to);
                }
                if (body.Contains("#SCSCloadingDischargingFrom"))
                {
                    body = body.Replace("#SCSCloadingDischargingFrom", String.IsNullOrEmpty(dataDetail.crude_info.revised_date_from) ? "-" : dataDetail.crude_info.revised_date_from);
                }

                if (body.Contains("#dischargingReasonSCSCS_H"))
                {
                    body = body.Replace("#dischargingReasonSCSCS_H", String.IsNullOrEmpty(dataDetail.comment.cmcs_request) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.comment.cmcs_request)));
                }
                if (body.Contains("#commentFromTNPB"))
                {
                    body = body.Replace("#commentFromTNPB", String.IsNullOrEmpty(dataDetail.comment.tn) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.comment.tn)));
                }
                if (body.Contains("#reject_reason_from_TNVP"))
                {
                    body = body.Replace("#reject_reason_from_TNVP", String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_TNVP) ? "-" : StripHTML(System.Web.HttpUtility.HtmlDecode(dataDetail.requester_info.reason_reject_TNVP)));
                }
            }
            catch (Exception e)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error MailMappingService # :: Exception >>> " + e);
            }

            return body;
        }

    }
}