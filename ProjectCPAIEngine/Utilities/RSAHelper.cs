﻿
using com.pttict.downstream.common.utilities;
using System;
using System.Security.Cryptography;
using System.Text;

namespace ProjectCPAIEngine.Utilities
{
    [Serializable]
    public class EncryptorRSAKeys
    {
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
    }
    public class RSAHelper : BasicBean
    {
        private static bool _optimalAsymmetricEncryptionPadding = false;

        private static  int[] BLOCK_SIZE_1024 = new int[]{1024, 117, 172}; //keysize as 1024 bits on OS window [EN(1char:1bytes) 117:172, TH(1char:3bytes) 39:172]
        private static  int[] BLOCK_SIZE_512 = new int[] { 512, 17, 88 }; // key size as 512 bits on OS window	[EN(1char:1bytes) 53:88, TH(1char:3bytes) 17:88]

        private static  int KEY_SIZE = BLOCK_SIZE_512[0]; // bits key size
        private static  int PLAINTEXT_BLOCK = BLOCK_SIZE_512[1]; // plain text block size
        private static  int CIPHERTEXT_BLOCK = BLOCK_SIZE_512[2]; // cipher text block size

        private static int KEY_SIZE_1024 = BLOCK_SIZE_1024[0]; // bits key size
        private static int PLAINTEXT_BLOCK_1024 = BLOCK_SIZE_1024[1]; // plain text block size
        private static int CIPHERTEXT_BLOCK_1024 = BLOCK_SIZE_1024[2]; // cipher text block size


        public static string EncryptText(string originalText, string publicKey)
        {
            int keySize = 0;
            string publicKeyXml = "";
            string encryptText = "";

                GetKeyFromEncryptionString(publicKey, out keySize, out publicKeyXml);

             if(keySize == 1024)
            {
                PLAINTEXT_BLOCK = PLAINTEXT_BLOCK_1024;
                KEY_SIZE = KEY_SIZE_1024;
                CIPHERTEXT_BLOCK = CIPHERTEXT_BLOCK_1024;
            }

              /* int maxLength = GetMaxDataLength(keySize);
                if (originalText.Length > maxLength)
                {*/
                    // encrypt per block
                    int in_len = originalText.Length;
                    int n_block = in_len / PLAINTEXT_BLOCK;
                    int n_block2 = in_len % PLAINTEXT_BLOCK;
                    int icount = 0;
                    for (int i = 0; i < n_block; i++)
                    {
                        String pl = originalText.Substring(icount, PLAINTEXT_BLOCK);
                        var encrypted = Encrypt(Encoding.UTF8.GetBytes(pl), keySize, publicKeyXml);
                        encryptText += Convert.ToBase64String(encrypted);
                        icount += PLAINTEXT_BLOCK;
                    }
                    if (n_block2 > 0)
                    {
                        String pl = originalText.Substring(icount, n_block2);
                        var encrypted = Encrypt(Encoding.UTF8.GetBytes(pl), keySize, publicKeyXml);
                        encryptText += Convert.ToBase64String(encrypted);
                    }
               /*}
                else
                {
                    var encrypted = Encrypt(Encoding.UTF8.GetBytes(originalText), keySize, publicKeyXml);
                    encryptText += Convert.ToBase64String(encrypted);
                }
                */

            return encryptText;
        }

        private static byte[] Encrypt(byte[] data, int keySize, string publicKeyXml)
        {
            if (data == null || data.Length == 0) throw new ArgumentException("Data are empty", "data");
            //int maxLength = GetMaxDataLength(keySize);
            //if (data.Length > maxLength) throw new ArgumentException(String.Format("Maximum data length is {0}", maxLength), "data");
            if (!IsKeySizeValid(keySize)) throw new ArgumentException("Key size is not valid", "keySize");
            if (String.IsNullOrEmpty(publicKeyXml)) throw new ArgumentException("Key is null or empty", "publicKeyXml");

            using (var provider = new RSACryptoServiceProvider(keySize))
            {
                provider.FromXmlString(publicKeyXml);
                return provider.Encrypt(data, _optimalAsymmetricEncryptionPadding);
            }
        }

        public static string DecryptText(string text, string privateKey)
        {
            int keySize = 0;
            string publicAndPrivateKeyXml = "";
            string decryptText = "";

            GetKeyFromEncryptionString(privateKey, out keySize, out publicAndPrivateKeyXml);

            if (keySize == 1024)
            {
                PLAINTEXT_BLOCK = PLAINTEXT_BLOCK_1024;
                KEY_SIZE = KEY_SIZE_1024;
                CIPHERTEXT_BLOCK = CIPHERTEXT_BLOCK_1024;
            }
            //var decrypted = Decrypt(Convert.FromBase64String(text), keySize, publicAndPrivateKeyXml);
            //decryptText = Encoding.UTF8.GetString(decrypted);
            //text = text.Replace("\n","").Replace("\r\n","");
            // decrypt per block
            int in_len = text.Length;
            int n_block = in_len / CIPHERTEXT_BLOCK;
            int icount = 0;
            for (int i = 0; i < n_block; i++)
            {
                String pl = text.Substring(icount,CIPHERTEXT_BLOCK);
                var decrypted = Decrypt(Convert.FromBase64String(pl), keySize, publicAndPrivateKeyXml);
                decryptText += Encoding.UTF8.GetString(decrypted);
                icount += CIPHERTEXT_BLOCK;
            }

            return decryptText;
        }

        private static byte[] Decrypt(byte[] data, int keySize, string publicAndPrivateKeyXml)
        {
            if (data == null || data.Length == 0) throw new ArgumentException("Data are empty", "data");
            if (!IsKeySizeValid(keySize)) throw new ArgumentException("Key size is not valid", "keySize");
            if (String.IsNullOrEmpty(publicAndPrivateKeyXml)) throw new ArgumentException("Key is null or empty", "publicAndPrivateKeyXml");

            using (var provider = new RSACryptoServiceProvider(keySize))
            {
                provider.FromXmlString(publicAndPrivateKeyXml);
                return provider.Decrypt(data, _optimalAsymmetricEncryptionPadding);
            }
        }
        public static int GetMaxDataLength(int keySize)
        {
            if (_optimalAsymmetricEncryptionPadding)
            {
                return ((keySize - 384) / 8) + 7;
            }
            return ((keySize - 384) / 8) + 37;
        }

        public static bool IsKeySizeValid(int keySize)
        {
            return keySize >= 384 &&
                    keySize <= 16384 &&
                    keySize % 8 == 0;
        }

        private static string IncludeKeyInEncryptionString(string publicKey, int keySize)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(keySize.ToString() + "!" + publicKey));
        }

        private static void GetKeyFromEncryptionString(string rawkey, out int keySize, out string xmlKey)
        {
            keySize = 0;
            xmlKey = "";

            if (rawkey != null && rawkey.Length > 0)
            {
                byte[] keyBytes = Convert.FromBase64String(rawkey);
                var stringKey = Encoding.UTF8.GetString(keyBytes);

                if (stringKey.Contains("!"))
                {
                    var splittedValues = stringKey.Split(new char[] { '!' }, 2);

                    try
                    {
                        keySize = int.Parse(splittedValues[0]);
                        xmlKey = splittedValues[1];
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }
        public static EncryptorRSAKeys GenerateKeys(int keySize)
        {
            if (keySize % 2 != 0 || keySize < 512)
                throw new Exception("Key should be multiple of two and greater than 512.");

            var response = new EncryptorRSAKeys();

            using (var provider = new RSACryptoServiceProvider(keySize))
            {
                var publicKey = provider.ToXmlString(false);
                var privateKey = provider.ToXmlString(true);

                var publicKeyWithSize = IncludeKeyInEncryptionString(publicKey, keySize);
                var privateKeyWithSize = IncludeKeyInEncryptionString(privateKey, keySize);

                response.PublicKey = publicKeyWithSize;
                response.PrivateKey = privateKeyWithSize;
            }

            return response;
        }

    }
}