﻿using MigraDoc.DocumentObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public class HtmlRemoval
    {
        public List<htmlTagList> RearrangeHtmlTagList(List<htmlTagList> Lst)
        {
            int sum_char = 0;
            //each array doesn't contain more than 10,000 characters.
            List<htmlTagList> list = new List<htmlTagList>();
            for (int i = 0; i < Lst.Count; i++)
            {
                int count = Lst[i].TextString.Length;
                if (count > 10000)
                {
                    for (int j = 0; j < count % 10000; j += 10000)
                    {
                        htmlTagList html = new htmlTagList();
                        html.TextString = Lst[i].TextString.Substring(j, count - j < 10000 ? count - j : 10000);
                        list.Add(html);
                        sum_char = count - j;
                    }
                }
                else
                {
                    if (count + sum_char > 10000)
                    {
                        list.Add(Lst[i]);
                        sum_char = count;
                    }
                    else
                    {
                        if (list.Count > 0)
                        {
                            list[list.Count - 1].TextString += Lst[i].TextString;
                        } else
                        {
                            list.Add(Lst[i]);
                        }
                        if (Lst[i].TextString.ToUpper().Contains("&nbsp;"))
                        {
                            sum_char += 200;
                        } else
                        {
                            sum_char += count;
                        }
                    }
                }
            }

            return list;
        }
        public void ValuetoParagraph(string source, Paragraph ph)
        {
            if(!String.IsNullOrEmpty(source))
            {
                List<htmlTagList> Lst = SplitHTMLTag(source);
                string FirstString = ""; ;
                for (int i = 0; i < Lst.Count; i++)
                {
                    if (i == 0 && Lst[i].TextString == Environment.NewLine.ToString())
                    {
                        //Not do
                    }
                    else
                    {
                        if (Lst[i].TextString.IndexOf(Environment.NewLine) >= 0 && FirstString == "")
                        {
                            Lst[i].TextString = Lst[i].TextString.Replace(Environment.NewLine, "");
                        }
                        Lst[i].TextString = HttpUtility.HtmlDecode(Lst[i].TextString).ToString();
                        FirstString += Lst[i].TextString;
                        MapFormatText(ph, Lst[i]);
                    }
                }
            }           
        }

        public void ValuetoParagraph(htmlTagList Lst, Paragraph ph)
        {
            //if (Lst.TextString.IndexOf(Environment.NewLine) >= 0)
            //{
            //    Lst.TextString = Lst.TextString.Replace(Environment.NewLine, "");
            //}
            MapFormatText(ph, Lst);
        }

        private  void MapFormatText(Paragraph ph, htmlTagList list)
        {//Verdana
            Font _font = new Font((list.Italic) ? "Arial" : "Tahoma");
            FormattedText fmText = ph.AddFormattedText(list.TextString.Replace("&nbsp;", " ").Replace("&nbsp;".ToUpper(), " "), _font);
            fmText.Color = list.FontColor;
            fmText.Italic = list.Italic;
            fmText.Bold = list.Bold;            
            fmText.Underline = (list.Underline)?Underline.Single: Underline.None;
            
            
        }

        public  List<htmlTagList>SplitHTMLTag(string html, bool isNewLine = true)
        {
            List<htmlTagList> htmlList = new List<htmlTagList>();
            if (isNewLine)
                html = Regex.Replace(html, @"(<p>)","<p>"+ Environment.NewLine);
            else
                html = Regex.Replace(html, @"(<p>)", "<p>");
            bool Isnumber = false;int No = 1;   
            if (html.IndexOf("<ol>") >= 0) { Isnumber = true; }
            List<string> htmlSplit = Regex.Split(html, @"(<[\s\S]+?>)").ToList();
            if (Isnumber)
            {
                for (int i = 0; i < htmlSplit.Count; i++)
                {
                    if (htmlSplit[i].IndexOf("<li>") >= 0)
                    {
                        htmlSplit[i] = Regex.Replace(htmlSplit[i], @"(<li>)", No.ToString()+".");
                        No++;
                    }
                    else if (htmlSplit[i].IndexOf("</li>") >= 0)
                    {
                        htmlSplit[i] = Regex.Replace(htmlSplit[i], @"(</li>)", "" + Environment.NewLine);
                    }
                }
            }
            int _startIndex = 0;
            if (htmlSplit != null && htmlSplit.Count > 0 && (htmlSplit[0] == Environment.NewLine||string.IsNullOrEmpty( htmlSplit[0]))) _startIndex = 1;
            CheckTagHtml(htmlSplit,ref htmlList,false,false,false,Colors.Black, _startIndex, htmlSplit.Count-1);
            return htmlList;

        }

        public void CheckTagHtml(List<string> htmlSplit, ref List<htmlTagList> htmlList, bool Bold, bool Itaric, bool UnderLine, Color Color, int indexNo, int LastIndexNo)
        {
            int EnTagIndex = LastIndexNo;
            for (int i = indexNo; i < htmlSplit.Count; i++)
            {
                bool _tmpBold = Bold; bool _tmpItaric = Itaric; bool _tmpUnderLine = UnderLine; Color _tmpColor = Color;
                if (i <= LastIndexNo)
                {

                    if (Regex.IsMatch(htmlSplit[i], @"(<[\s\S]+?>)"))
                    {
                        EnTagIndex = GetLastIndex(htmlSplit, i, LastIndexNo);
                    }
                    else
                    {

                        GetStyleFromParentTag(htmlSplit[i - 1], ref Bold, ref Itaric, ref UnderLine, ref Color);
                        htmlList.Add(new Utilities.HtmlRemoval.htmlTagList() { TextString = htmlSplit[i], Bold = Bold, Italic = Itaric, Underline = UnderLine, FontColor = Color });
                        CheckTagHtml(htmlSplit, ref htmlList, Bold, Itaric, UnderLine, Color, i + 1, EnTagIndex);
                        Bold = _tmpBold; Itaric = _tmpItaric; UnderLine = _tmpUnderLine; Color = _tmpColor;
                        if (EnTagIndex < i) EnTagIndex = i;
                        i = EnTagIndex;
                    }
                }
                else
                {
                    Bold = _tmpBold; Itaric = _tmpItaric; UnderLine = _tmpUnderLine; Color = _tmpColor;
                    break;
                }
            }
        }

        public  int GetLastIndex(List<string> htmlSplit, int index, int lastindex)
        {
            int lastin = lastindex;
            int Noogtag = 0;
            if (Regex.IsMatch(htmlSplit[index], @"(<[\s\S]+?>)"))
            {
                for (int i = index+1; i < lastindex; i++)
                {
                    if (Regex.IsMatch(htmlSplit[i], @"(</[\s\S]+?>)") && Noogtag == 0)
                    {
                        lastin = i;
                        break;
                    }
                    else if (Regex.IsMatch(htmlSplit[i], @"(</[\s\S]+?>)") && Noogtag != 0)
                    {
                        Noogtag--;
                        if(Noogtag==0) lastin = i;
                    }                    
                    else if (Regex.IsMatch(htmlSplit[i], @"(<[\s\S]+?>)")) { Noogtag++; }
                }
            }
            return lastin;
        }

        public  void GetStyleFromParentTag(string HTML,ref bool Bold,ref bool Itaric,ref bool UnderLine,ref Color Color)
        {
            if (HTML.Length>=8 && HTML.ToUpper().IndexOf("<strong>".ToUpper()) >=0) Bold = true;
            else if (HTML.Length >= 8 && Regex.IsMatch(HTML, @"(<strong [\s\S]+?>)")) Bold = true;
            if (HTML.Length >= 4 && HTML.ToUpper().IndexOf("<em>".ToUpper()) >= 0) Itaric = true;           
            if (HTML.Length >= 27 && HTML.ToUpper().IndexOf("text-decoration: underline;".ToUpper().Trim()) >= 0) UnderLine = true;
            else if (HTML.Length >= 3 && HTML.ToUpper().IndexOf("<u>".ToUpper()) >= 0) UnderLine = true;
            else if (HTML.Length >= 3 && Regex.IsMatch(HTML, @"(<u [\s\S]+?>)")) UnderLine = true;
            if (HTML.Length >= 17 && HTML.ToUpper().IndexOf("background-color:".ToUpper()) >= 0)
            {
                string stringColor = "";bool FronColorCode = false;
                if (HTML.IndexOf("#") >= 0)
                {
                    stringColor = HTML.Substring(HTML.IndexOf("#"));
                    FronColorCode = true;
                }
                else
                {
                    stringColor = HTML.Substring(HTML.IndexOf("color:"));
                    stringColor = stringColor.Replace("color:", "");
                    FronColorCode = false;
                }
                stringColor = stringColor.Substring(0, stringColor.IndexOf(";"));
                System.Drawing.Color _color = (FronColorCode)?(System.Drawing.Color)System.Drawing.ColorTranslator.FromHtml(stringColor): System.Drawing.Color.FromName(stringColor.Trim());
                Color _col = new Color(_color.A, _color.R, _color.G, _color.B);
                Color = _col;
            }
            else if (HTML.Length >= 7 && HTML.ToUpper().IndexOf("color:".ToUpper()) >= 0)
            {
                string stringColor = ""; bool FronColorCode = false;
                if (HTML.IndexOf("#") >= 0)
                {
                    stringColor = HTML.Substring(HTML.IndexOf("#"));
                    FronColorCode = true;
                }
                else
                {
                    stringColor = HTML.Substring(HTML.IndexOf("color:"));
                    stringColor = stringColor.Replace("color:", "");
                    FronColorCode = false;
                }
                stringColor = stringColor.Substring(0, stringColor.IndexOf(";"));
                System.Drawing.Color _color = (FronColorCode) ? (System.Drawing.Color)System.Drawing.ColorTranslator.FromHtml(stringColor) : System.Drawing.Color.FromName(stringColor.Trim());
                Color _col = new Color(_color.A, _color.R, _color.G, _color.B);
                Color = _col;
            }
        }

       

        public class htmlTagList
        {
            public string TextString { get; set; }
            public bool Italic { get; set; }
            public bool Bold { get; set; }
            public bool Underline { get; set; }
            public Color FontColor { get; set; }
        }
    }

  

}