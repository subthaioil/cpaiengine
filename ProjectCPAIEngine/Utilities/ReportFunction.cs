﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public enum ReportAlignment{
        LC=1,
        RC=2,
        CC=3,
        LT=4,
        RT=5,
        CT=6,
        LB=7,
        RB=8,
        CB=9
        }
    public class ReportFunction
    {
        public string FontName { get; set; }
        public Document document { get; set; }

        public void SetCellAlignment(Cell _cell, ReportAlignment _Alignment)
        {
            switch (_Alignment)
            {
                case ReportAlignment.LC:
                    _cell.VerticalAlignment = VerticalAlignment.Center;
                    _cell.Format.Alignment = ParagraphAlignment.Left;
                    break;
                case ReportAlignment.RC:
                    _cell.VerticalAlignment = VerticalAlignment.Center;
                    _cell.Format.Alignment = ParagraphAlignment.Right;
                    break;
                case ReportAlignment.CC:
                    _cell.VerticalAlignment = VerticalAlignment.Center;
                    _cell.Format.Alignment = ParagraphAlignment.Center;
                    break;
                case ReportAlignment.LB:
                    _cell.VerticalAlignment = VerticalAlignment.Bottom;
                    _cell.Format.Alignment = ParagraphAlignment.Left;
                    break;
                case ReportAlignment.RB:
                    _cell.VerticalAlignment = VerticalAlignment.Bottom;
                    _cell.Format.Alignment = ParagraphAlignment.Right;
                    break;
                case ReportAlignment.CB:
                    _cell.VerticalAlignment = VerticalAlignment.Bottom;
                    _cell.Format.Alignment = ParagraphAlignment.Center;
                    break;
                case ReportAlignment.LT:
                    _cell.VerticalAlignment = VerticalAlignment.Top;
                    _cell.Format.Alignment = ParagraphAlignment.Left;
                    break;
                case ReportAlignment.RT:
                    _cell.VerticalAlignment = VerticalAlignment.Top;
                    _cell.Format.Alignment = ParagraphAlignment.Right;
                    break;
                case ReportAlignment.CT:
                    _cell.VerticalAlignment = VerticalAlignment.Top;
                    _cell.Format.Alignment = ParagraphAlignment.Center;
                    break;
            }
         
        }
        

        public string AdjustIfTooWideToFitIn(Cell cell, string text, double Merge = 0)
        {
            if (text != null)
            {
                Column column = cell.Column;
                Unit availableWidth = column.Width - column.Table.Borders.Width - cell.Borders.Width;
                if (Merge > 0) availableWidth += (availableWidth * Merge);
                var tooWideWords = text.Split(" ".ToCharArray()).Distinct().Where(s => TooWide(s, availableWidth));

                var adjusted = new StringBuilder(text);
                foreach (string word in tooWideWords)
                {
                    var replacementWord = MakeFit(word, availableWidth);
                    adjusted.Replace(word, replacementWord);
                }

                return adjusted.ToString();
            }
            else return "";
        }
       

        private bool TooWide(string word, Unit width)
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;
            TextMeasurement tm = new TextMeasurement(document.Styles["Normal"].Font.Clone());
            float f = tm.MeasureString(word, UnitType.Point).Width;
            return f > width.Point;
        }

        /// <summary>
        /// Makes the supplied word fit into the available width
        /// </summary>
        /// <returns>modified version of the word with inserted Returns at appropriate points</returns>
        private string MakeFit(string word, Unit width)
        {
            var adjustedWord = new StringBuilder();
            var current = string.Empty;
            foreach (char c in word)
            {
                if (TooWide(current + c, width))
                {
                    adjustedWord.Append(current);
                    adjustedWord.Append(Chars.CR);
                    current = c.ToString();
                }
                else
                {
                    current += c;
                }
            }
            adjustedWord.Append(current);

            return adjustedWord.ToString();
        }
    }
}